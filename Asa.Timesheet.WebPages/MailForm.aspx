﻿<%@ Register TagPrefix="rade" Namespace="Telerik.WebControls" Assembly="RadEditor" %>
<%@ Register TagPrefix="uc1" TagName="PageHeader" Src="UserControls/PageHeader.ascx" %>
<%@ Page language="c#" Codebehind="MailForm.aspx.cs" AutoEventWireup="false" Inherits="Asa.Timesheet.WebPages.MailForm" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
	<HEAD>
		<title>Send Mail</title>
		<meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
		<meta content="C#" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<LINK href="styles/timesheet.css" type="text/css" rel="stylesheet">
		<script language="javascript">
		
		function MailDblClick()
		{
			var src, dest;
			
			src = event.srcElement;
			switch (src.id)
			{
				case "lbMails" : dest = document.getElementById("lbSelectedMails");
								 break;
				case "lbSelectedMails" : dest = document.getElementById("lbMails");
								break;
				case "lbSelectedUsersMails" : dest = document.getElementById("lbUsersMails");
								break;
				case "lbUsersMails" : dest = document.getElementById("lbSelectedUsersMails");
								break;
			}
			
			index = src.selectedIndex;
			if (index==-1) return;
			
			var email = src.options[index].innerText;
			var value = src.options[index].value;
			
			src.options.remove(index);
			var opt = document.createElement("OPTION");
			dest.options.add(opt); 
			opt.innerText = email;
			opt.value = value;
			
			UpdateContainer(src);
			UpdateContainer(dest);
		} 
		
		function ArrowButtonClick()
		{
			var btn = event.srcElement;
			var src, dest;
			
			switch (btn.id)
			{
				case "inputToRight" : src = document.getElementById("lbMails");
									  dest = document.getElementById("lbSelectedMails");
									  break;
				case "inputToLeft" :  dest = document.getElementById("lbMails");
									  src = document.getElementById("lbSelectedMails");
									  break;
				case "inputUsersToLeft" :   dest = document.getElementById("lbUsersMails");
											src = document.getElementById("lbSelectedUsersMails");
											break;
				case "inputUsersToRight" :  dest = document.getElementById("lbSelectedUsersMails");
											src = document.getElementById("lbUsersMails");
											break;
				
			}
			
			//for (i=src.options.length-1;i>=0 ;i--)
			
			for (i=0; i<src.options.length; i++)
			if (src.options[i].selected)
			{ 
				var email = src.options[i].innerText;
				var value = src.options[i].value;
				var opt = document.createElement("OPTION");
				dest.options.add(opt); 
				opt.innerText = email;
				opt.value = value;
			}
			
			for (i=src.options.length-1;i>=0 ;i--)
			if (src.options[i].selected) src.options.remove(i);
			
			UpdateContainer(dest);
			UpdateContainer(src);
			
		}
		
		function UpdateContainer(listBox)
		{
			var container = document.getElementById("hdn"+listBox.id);
			var arr = new Array();
			for (i=0; i<listBox.options.length; i++) arr.push(listBox.options[i].value+":"+listBox.options[i].innerText);
			container.value = arr.join(";");
		}
		
		function CheckSelected()
		{
			
			var project = document.getElementById("ddlProject");
			var errorMessages = Form1.hdnErrorMessages.value.split(';');
			
			if (project.value=="0")
			{
				document.getElementById("lblError").innerText = errorMessages[0];
				document.getElementById("lblProject").style.color = "red";
				return false;
			}
			else 
			{
				document.getElementById("lblError").innerText = "";
				document.getElementById("lblProject").style.color = "";
			}
			var user = document.getElementById("ddlUsers");
			if (user.value=="-1")
			{
				document.getElementById("lblError").innerText = errorMessages[1];
				document.getElementById("lblSelectUser").style.color = "red";
				return false;
			}
			else 
			{
				document.getElementById("lblError").innerText = "";
				document.getElementById("lblSelectUser").style.color = "";
			}
			
			return true;
		}
		
		</script>
	</HEAD>
	<body bottomMargin="0" leftMargin="0" topMargin="0" rightMargin="0">
		<form id="Form1" method="post" runat="server">
			<table height="100%" cellSpacing="0" cellPadding="0" width="100%" border="0">
				<tr height="1">
					<td><uc1:pageheader id="header" runat="server"></uc1:pageheader></td>
				</tr>
				<tr style="DISPLAY: none" height="80">
					<td background="images/copy1.gif">
						<TABLE id="Table2" height="100%" width="100%">
							<TR>
								<TD style="WIDTH: 106px">&nbsp;&nbsp;&nbsp;&nbsp; <IMG height="72" alt="" src="images/logo.jpg" width="58"></TD>
								<TD noWrap><asp:label id="lblTitle" runat="server" Font-Bold="True" Font-Size="X-Small"></asp:label><br>
									<asp:label id="lblUser" runat="server" Font-Bold="True" Font-Size="Medium" ForeColor="SaddleBrown"></asp:label></TD>
							</TR>
						</TABLE>
					</td>
				</tr>
				<TR height="1">
					<td noWrap background="images/line.gif" height="1"></td>
				</TR>
				<tr>
					<td>
						<table height="100%" cellSpacing="0" cellPadding="0" width="100%" border="0">
							<tr>
								<td style="BACKGROUND-IMAGE: url(images/left110.gif)" vAlign="top" noWrap width="110"><asp:placeholder id="menuHolder" runat="server"></asp:placeholder></td>
								<td noWrap width="1" bgColor="lightgrey"></td>
								<td style="PADDING-LEFT: 5px; PADDING-TOP: 3px" vAlign="top" noWrap>
									<TABLE id="tblInfo" height="30" cellSpacing="0" cellPadding="3" width="880" border="0">
										<TR>
											<TD vAlign="middle" noWrap><asp:label id="lblError" runat="server" ForeColor="Red" CssClass="ErrorLabel" EnableViewState="False"></asp:label><asp:label id="lblInfo" runat="server" Font-Size="Larger" CssClass="InfoLabel" EnableViewState="False"></asp:label><INPUT id="hdnErrorMessages" style="DISPLAY: none" type="text" runat="server"></TD>
										</TR>
									</TABLE>
									<TABLE id="Table3" cellSpacing="0" cellPadding="0" width="880" border="0">
										<TR>
											<TD style="HEIGHT: 5px" colSpan="2"></TD>
										</TR>
										<TR>
											<TD colSpan="2"><IMG height="1" src="images/dot.gif" width="100%"></TD>
										</TR>
										<TR>
											<TD style="WIDTH: 4px"></TD>
											<TD>
												<TABLE id="tblSelect" cellSpacing="0" cellPadding="0" width="100%" border="0" runat="server">
													<TR>
														<td colSpan="2" height="3"><asp:checkbox id="ckDates" runat="server" Width="144px" CssClass="EnterDataLabel" Text="За целия период"
																AutoPostBack="True" Checked="True"></asp:checkbox></td>
													</TR>
													<TR>
														<TD>
															<TABLE id="tblSelect1" cellSpacing="0" cellPadding="3" border="0">
																<TR id="SelectDateRow">
																	<TD style="WIDTH: 164px; HEIGHT: 4px" vAlign="top" align="right"><asp:label id="lblStartDay" runat="server" CssClass="EnterDataLabel">Ден, седмица, нач.дата:</asp:label><BR>
																		<asp:label id="lblSelStartDate" runat="server" Font-Bold="True" ForeColor="SaddleBrown"></asp:label></TD>
																	<TD style="WIDTH: 276px" vAlign="top">
																		<TABLE id="Table1" cellSpacing="0" cellPadding="0" border="0">
																			<TR>
																				<TD>
																					<asp:calendar id="calStartDate" runat="server" ForeColor="SaddleBrown" Width="210px" BorderColor="Tan"
																						CellPadding="1" BorderWidth="1px" BackColor="White" NextMonthText="<img src='images/monthright.gif' border=0/>"
																						PrevMonthText="<img src='images/monthleft.gif' border=0/>" DayNameFormat="FirstLetter" SelectWeekText="<img src='images/selweek.gif' border=0/>"
																						FirstDayOfWeek="Monday" Enabled="False" Height="150px" SelectionMode="DayWeek">
																						<TodayDayStyle ForeColor="SaddleBrown" BackColor="Wheat"></TodayDayStyle>
																						<SelectorStyle Font-Size="10pt" Font-Names="Verdana" ForeColor="SaddleBrown" BorderStyle="None"
																							BackColor="FloralWhite"></SelectorStyle>
																						<DayStyle Font-Size="8pt" Font-Names="Verdana" Font-Bold="True" BorderWidth="1px" BorderStyle="Solid"
																							BorderColor="#E0E0E0" BackColor="White"></DayStyle>
																						<NextPrevStyle Font-Bold="True" Wrap="False" HorizontalAlign="Center" Height="16px" BorderWidth="1px"
																							ForeColor="White" BorderStyle="None" VerticalAlign="Middle"></NextPrevStyle>
																						<DayHeaderStyle Font-Size="9pt" Font-Names="Verdana" Height="1px" ForeColor="SaddleBrown" BackColor="FloralWhite"></DayHeaderStyle>
																						<SelectedDayStyle Font-Bold="True" ForeColor="White" BorderColor="FloralWhite" BackColor="SandyBrown"></SelectedDayStyle>
																						<TitleStyle Font-Size="9pt" Font-Names="Verdana" Font-Bold="True" BorderWidth="1px" ForeColor="White"
																							BorderStyle="Solid" BorderColor="Tan" BackColor="Peru"></TitleStyle>
																						<WeekendDayStyle BackColor="AntiqueWhite"></WeekendDayStyle>
																						<OtherMonthDayStyle ForeColor="Tan"></OtherMonthDayStyle>
																					</asp:calendar></TD>
																				<TD style="WIDTH: 58px" vAlign="top" noWrap></TD>
																				<TD vAlign="top" noWrap align="right" colSpan="1" rowSpan="1"><asp:label id="Label4" runat="server" Width="98px" CssClass="EnterDataLabel">Крайна дата:</asp:label><BR>
																					<asp:label id="lblSelEndDate" runat="server" Font-Bold="True" ForeColor="SaddleBrown" Width="100px"></asp:label><BR>
																					<asp:label id="lbErrDate" runat="server" ForeColor="Red"></asp:label></TD>
																			</TR>
																		</TABLE>
																	</TD>
																	<TD style="WIDTH: 57px" vAlign="top">
																		<asp:calendar id="calEndDate" runat="server" Font-Size="8pt" ForeColor="SaddleBrown" Width="200px"
																			BorderColor="Tan" CellPadding="1" BorderWidth="1px" BackColor="White" NextMonthText="<img src='images/monthright.gif' border=0/>"
																			PrevMonthText="<img src='images/monthleft.gif' border=0/>" DayNameFormat="FirstLetter" Font-Names="Verdana"
																			SelectWeekText="<img src='../images/selweek.gif' border=0/>" FirstDayOfWeek="Monday" Enabled="False"
																			Height="150px">
																			<TodayDayStyle ForeColor="SaddleBrown" BackColor="Wheat"></TodayDayStyle>
																			<SelectorStyle ForeColor="SaddleBrown" BorderStyle="None" BackColor="FloralWhite"></SelectorStyle>
																			<DayStyle Font-Size="8pt" Font-Names="Verdana" Font-Bold="True" BorderWidth="1px" BorderStyle="Solid"
																				BorderColor="#E0E0E0" BackColor="White"></DayStyle>
																			<NextPrevStyle Font-Size="8pt" Font-Bold="True" Wrap="False" HorizontalAlign="Center" Height="16px"
																				BorderWidth="1px" ForeColor="White" BorderStyle="None" VerticalAlign="Middle"></NextPrevStyle>
																			<DayHeaderStyle Height="1px" ForeColor="SaddleBrown" BackColor="FloralWhite"></DayHeaderStyle>
																			<SelectedDayStyle Font-Bold="True" ForeColor="White" BorderColor="FloralWhite" BackColor="SandyBrown"></SelectedDayStyle>
																			<TitleStyle Font-Size="8pt" Font-Names="Verdana" Font-Bold="True" BorderWidth="1px" ForeColor="White"
																				BorderStyle="Solid" BorderColor="Tan" BackColor="Peru"></TitleStyle>
																			<WeekendDayStyle BackColor="AntiqueWhite"></WeekendDayStyle>
																			<OtherMonthDayStyle ForeColor="Tan"></OtherMonthDayStyle>
																		</asp:calendar></TD>
																	<TD style="WIDTH: 278px; HEIGHT: 4px" vAlign="top"></TD>
																</TR>
																<TR>
																	<TD style="WIDTH: 164px; HEIGHT: 4px"><asp:label id="lblProject" runat="server" Width="100%" CssClass="enterDataLabel">Проект:</asp:label></TD>
																	<TD style="WIDTH: 276px" vAlign="top" colspan="2">
																		<nobr>
																			<asp:dropdownlist id="ddlProject" runat="server" Width="288px" CssClass="EnterDataBox"></asp:dropdownlist>&nbsp;
																			<asp:dropdownlist id="ddlProjectStatus" runat="server" CssClass="EnterDataBox" AutoPostBack="True"
																				Width="250px">
																				<asp:ListItem Value="0">&lt;активни и пасивни проекти&gt;</asp:ListItem>
																				<asp:ListItem Value="1">&lt;активни проекти&gt;</asp:ListItem>
																				<asp:ListItem Value="2">&lt;пасивни проекти&gt;</asp:ListItem>
																			</asp:dropdownlist></nobr></TD> <!--
                                  <TD style="WIDTH: 92px" vAlign="top" colSpan="2">
                                    </TD>-->
																</TR>
																<TR>
																	<TD style="WIDTH: 164px; HEIGHT: 4px"><asp:label id="lblSelectUser" runat="server" Width="100%" CssClass="enterDataLabel" EnableViewState="False">Служител:</asp:label></TD>
																	<TD style="WIDTH: 276px" vAlign="top"><asp:dropdownlist id="ddlUsers" runat="server" Width="288px" CssClass="EnterDataBox"></asp:dropdownlist></TD>
																	<TD style="WIDTH: 92px" vAlign="top" colSpan="2"></TD>
																</TR>
																<TR height="5">
																	<TD></TD>
																	<TD></TD>
																	<TD></TD>
																</TR>
																<TR>
																	<TD style="WIDTH: 164px; HEIGHT: 4px"></TD>
																	<TD style="WIDTH: 276px" vAlign="top"><asp:button id="btnGen" runat="server" Width="170px" CssClass="ActionButton" Text="Генерирай текст"></asp:button></TD>
																	<TD style="WIDTH: 92px" vAlign="top" colSpan="2"></TD>
																</TR>
																<TR height="5">
																	<TD></TD>
																	<TD></TD>
																	<TD></TD>
																</TR>
															</TABLE>
														</TD>
													</TR>
													<TR>
														<TD><IMG height="1" src="images/dot.gif" width="100%"></TD>
													</TR>
												</TABLE>
												<TABLE id="tblData" cellSpacing="0" cellPadding="3" width="100%" border="0" runat="server">
													<TR style="DISPLAY: none">
														<TD style="WIDTH: 124px; HEIGHT: 126px"><asp:label id="lblName" runat="server" Width="100%" CssClass="enterDataLabel" Visible="False">Изберете получатели:</asp:label><BR>
															<asp:label id="lbDo" runat="server" Font-Size="XX-Small" Width="100%" CssClass="enterDataLabel"
																Visible="False">по подразбиране мейлът се изпраща до служителя</asp:label></TD>
														<TD style="HEIGHT: 126px">
															<TABLE id="Table5" cellSpacing="1" cellPadding="1" width="100%" border="0">
																<TR>
																	<TD style="WIDTH: 290px"><asp:listbox id="lsMails" runat="server" Width="286px" CssClass="enterDataBox" SelectionMode="Multiple"
																			Height="106px" Visible="False"></asp:listbox></TD>
																	<TD style="WIDTH: 34px">
																		<table id="Table6" border="0">
																			<tr>
																				<td><asp:button id="btnToRight" runat="server" Font-Bold="True" Width="26px" CssClass="ActionButton"
																						Text=">" ToolTip="Избор" Visible="False"></asp:button></td>
																			</tr>
																			<TR>
																				<td><asp:button id="btnToLeft" runat="server" Font-Bold="True" Width="26px" CssClass="ActionButton"
																						Text="<" ToolTip="Отказ от избор" Visible="False"></asp:button></td>
																			</TR>
																		</table>
																	</TD>
																	<TD><asp:listbox id="lsMailsSel" runat="server" Width="264px" CssClass="enterDataBox" SelectionMode="Multiple"
																			Height="104px" Visible="False"></asp:listbox></TD>
																</TR>
															</TABLE>
														</TD>
													</TR>
													<TR>
														<TD style="WIDTH: 124px" vAlign="top"><br>
															<asp:label id="Label5" runat="server" Width="100%" CssClass="enterDataLabel">Изберете получатели:</asp:label><asp:label id="lblDo1" runat="server" Font-Size="XX-Small" Width="100%" CssClass="enterDataLabel">по подразбиране мейлът се изпраща до служителя</asp:label></TD>
														<TD style="HEIGHT: 126px">
															<TABLE id="Table4" cellSpacing="1" cellPadding="1" width="100%" border="0">
																<TR>
																	<TD style="WIDTH: 290px"><asp:listbox id="lbMails" runat="server" Width="286px" CssClass="enterDataBox" SelectionMode="Multiple"
																			Height="106px"></asp:listbox></TD>
																	<TD style="WIDTH: 34px">
																		<TABLE id="Table7" border="0">
																			<TR>
																				<TD><INPUT class="ActionButton" id="inputToRight" style="WIDTH: 26px; HEIGHT: 18px" onclick="ArrowButtonClick();"
																						type="button" size="20" value=">"></TD>
																			</TR>
																			<TR>
																				<TD><INPUT class="ActionButton" id="inputToLeft" style="WIDTH: 26px; HEIGHT: 18px" onclick="ArrowButtonClick();"
																						type="button" size="20" value="<" name="Button1"></TD>
																			</TR>
																		</TABLE>
																	</TD>
																	<TD><asp:listbox id="lbSelectedMails" runat="server" Width="264px" CssClass="enterDataBox" SelectionMode="Multiple"
																			Height="104px"></asp:listbox><INPUT id="hdnlbMails" style="DISPLAY: none; WIDTH: 42px; HEIGHT: 19px" type="text" size="1"
																			runat="server"><INPUT id="hdnlbSelectedMails" style="DISPLAY: none; WIDTH: 34px; HEIGHT: 19px" type="text"
																			size="1" runat="server"></TD>
																</TR>
															</TABLE>
														</TD>
													</TR>
													<TR>
														<TD style="WIDTH: 124px; HEIGHT: 126px" vAlign="top"><br>
															<asp:label id="lblUsersEmails" runat="server" Width="100%" CssClass="enterDataLabel">Служители:</asp:label></TD>
														<TD style="HEIGHT: 126px">
															<TABLE id="Table8" cellSpacing="1" cellPadding="1" width="100%" border="0">
																<TR>
																	<TD style="WIDTH: 290px"><asp:listbox id="lbUsersMails" runat="server" Width="286px" CssClass="enterDataBox" SelectionMode="Multiple"
																			Height="106px"></asp:listbox></TD>
																	<TD style="WIDTH: 34px">
																		<TABLE id="Table9" border="0">
																			<TR>
																				<TD><INPUT class="ActionButton" id="inputUsersToRight" style="WIDTH: 26px; HEIGHT: 18px" onclick="ArrowButtonClick();"
																						type="button" size="20" value=">" name="Button2"></TD>
																			</TR>
																			<TR>
																				<TD><INPUT class="ActionButton" id="inputUsersToLeft" style="WIDTH: 26px; HEIGHT: 18px" onclick="ArrowButtonClick();"
																						type="button" size="20" value="<" name="Button1"></TD>
																			</TR>
																		</TABLE>
																	</TD>
																	<TD><asp:listbox id="lbSelectedUsersMails" runat="server" Width="264px" CssClass="enterDataBox" SelectionMode="Multiple"
																			Height="104px"></asp:listbox><INPUT id="hdnlbUsersMails" style="DISPLAY: none; WIDTH: 42px; HEIGHT: 19px" type="text"
																			size="1" name="Text1" runat="server"><INPUT id="hdnlbSelectedUsersMails" style="DISPLAY: none; WIDTH: 34px; HEIGHT: 19px" type="text"
																			size="1" name="Text2" runat="server"></TD>
																</TR>
															</TABLE>
														</TD>
													</TR>
													<TR>
														<TD style="WIDTH: 124px; HEIGHT: 49px"><asp:label id="lblCity" runat="server" Width="100%" CssClass="enterDataLabel">Други ел. адреси:</asp:label><asp:label id="Label3" runat="server" Font-Size="XX-Small" Width="100%" CssClass="enterDataLabel">(разделени с ";")</asp:label></TD>
														<TD style="HEIGHT: 49px"><asp:textbox id="txtEmail" runat="server" Width="595px" CssClass="enterDataBox"></asp:textbox></TD>
													</TR>
													<TR>
														<TD style="WIDTH: 124px" align="right"><asp:label id="Label2" runat="server" Width="96px" CssClass="enterDataLabel">Относно:</asp:label></TD>
														<TD><asp:textbox id="txtSubject" runat="server" Width="595px" CssClass="enterDataBox"></asp:textbox></TD>
													</TR>
													<TR>
														<TD style="WIDTH: 124px" vAlign="top"><asp:label id="Label1" runat="server" Width="100%" CssClass="enterDataLabel">Текст:</asp:label></TD>
														<TD><rade:radeditor id="ftb" Width="594px" Height="192px" LicenseFile="bin/LicenseFile.xml" Runat="server"
																ToolsWidth="594px" SaveInFile="False" SaveAsXhtml="True" ToolsHeight="25px" ShowHtmlMode="False"></rade:radeditor></TD>
													</TR>
												</TABLE>
											</TD>
										</TR>
										<TR>
											<TD style="HEIGHT: 5px" colSpan="2"></TD>
										</TR>
										<TR>
											<TD colSpan="2"><IMG height="1" src="images/dot.gif" width="100%"></TD>
										</TR>
										<TR>
											<td colSpan="2" height="3"></td>
										</TR>
										<TR>
											<TD style="WIDTH: 4px"></TD>
											<TD>
												<TABLE id="Table55" cellSpacing="0" cellPadding="0" border="0">
													<TR>
														<TD style="WIDTH: 129px"></TD>
														<TD style="WIDTH: 97px"><asp:button id="btnSave" runat="server" CssClass="ActionButton" Text="Прати мейл"></asp:button></TD>
														<TD style="WIDTH: 288px"><asp:button id="btnCancel" runat="server" CssClass="ActionButton" Text="Откажи"></asp:button></TD>
														<TD></TD>
													</TR>
												</TABLE>
											</TD>
										</TR>
										<TR>
											<TD height="3"></TD>
										</TR>
										<TR>
											<TD colSpan="2"><IMG height="1" src="images/dot.gif" width="100%"></TD>
										</TR>
									</TABLE>
									<br>
									&nbsp;
								</td>
							</tr>
						</table>
					</td>
				</tr>
			</table>
		</form>
	</body>
</HTML>
