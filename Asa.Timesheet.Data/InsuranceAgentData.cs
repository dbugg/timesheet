using System;
using System.Collections;
using System.Text;

namespace Asa.Timesheet.Data
{
	public class InsuranceAgentData
	{
		private int _insuranceAgentID = -1;
		private string _insuranceAgentName = String.Empty;
		private string _insurancePhones = String.Empty;
		private bool _isActive = true;

		/// <summary>
		/// Initialize an new empty Login object.
		/// </summary>
		public InsuranceAgentData()
		{
		}
		
		/// <summary>
		/// Initialize a new Login object with the given parameters.
		/// </summary>
		public InsuranceAgentData(int insuranceAgentID, string insuranceAgentName, string insurancePhones, bool isActive)
		{
			this._insuranceAgentID = insuranceAgentID;
			this._insuranceAgentName = insuranceAgentName;
			this._insurancePhones = insurancePhones;
			this._isActive = isActive;
		}	
		
	  
		public override string ToString()
		{
			StringBuilder builder = new StringBuilder();
			builder.Append("_insuranceAgentID: " + _insuranceAgentID.ToString()).Append("\r\n");
			builder.Append("_insuranceAgentName: " + _insuranceAgentName.ToString()).Append("\r\n");
			builder.Append("_insurancePhones: " + _insurancePhones.ToString()).Append("\r\n");
			builder.Append("_isActive: " + _isActive.ToString()).Append("\r\n");
			builder.Append("\r\n");
			return builder.ToString();
		}	
		public object GetByIndex(int nIndex)
		{
			switch(nIndex)
			{
				case 0:
					return _insuranceAgentID;
				case 1:
					return _insuranceAgentName;
				case 2:
					return _insurancePhones;
				case 3:
					return _isActive;
			}
			return null;
		}
		#region Properties
	
		public int InsuranceAgentID
		{
			get { return _insuranceAgentID; }
			set { _insuranceAgentID = value; }
		}
	
		public string InsuranceAgentName
		{
			get { return _insuranceAgentName; }
			set { _insuranceAgentName = value; }
		}
	
		public string InsurancePhones
		{
			get { return _insurancePhones; }
			set { _insurancePhones = value; }
		}
	
		public bool IsActive
		{
			get { return _isActive; }
			set { _isActive = value; }
		}
	
		#endregion
	}
}


