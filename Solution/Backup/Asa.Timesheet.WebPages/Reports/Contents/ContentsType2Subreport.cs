using System;
using Asa.Timesheet.Data;
using GrapeCity.ActiveReports;
using GrapeCity.ActiveReports.Controls;
using GrapeCity.ActiveReports.SectionReportModel;
using GrapeCity.ActiveReports.Document.Section;
using GrapeCity.ActiveReports.Document;

namespace Asa.Timesheet.WebPages.Reports
{
    public class ContentsType2Subreport : GrapeCity.ActiveReports.SectionReport
	{
		bool _IsOneBuilding = false;
		bool _IsCode = false;
		ChecksVector _checks = null;
        public ContentsType2Subreport(bool IsCode, ChecksVector checks, bool IsOneBuilding)
        {
            _IsCode = IsCode;
            _checks = checks;
            _IsOneBuilding = IsOneBuilding;
            InitializeComponent();
        }

        private void Detail_Format(object sender, System.EventArgs eArgs)
        {
            if (this.txtContentText.Text.Length == 0) this.txtContentText.Text = " ";
            if (!_IsCode)
            {
                this.Line19.Visible = false;
                this.txtOzn.Text = "";
            }
            else
            {
                int _cid = int.Parse(this.txtcontentid.Text);
                int index = _checks.IndexOf(_cid, true);
                if (index != -1)
                    if (!_checks[index].IsChecked)
                    {
                        this.txtOzn.Text = "";
                        this.txtREV.Text = "";
                    }
                if (this.txtREV.Text.Length != 0)
                    this.txtOzn.Text = string.Concat(this.txtOzn.Text, ".", this.txtREV.Text);
            }
            //			if(txtContentType.Text == Resource.ResourceManager["ContentSubrLong"])
            //			{
            //				this.Line17.Y2 *=2;
            //				this.Line12.Y2 *=2;
            //				this.Line16.Y2 *=2;
            //				this.Line18.Y2 *=2;
            //				this.Line19.Y2 *=2;
            //				this.txtContentType.Height *=2;
            //				this.txtScale.Height *=2;
            //				this.txtOzn.Height *=2;
            //				this.txtContentText.Height *=2;
            //				this.Line1.Y1 *=2;
            //				this.Line1.Y2 *=2;
            //			}
            this.Detail.SizeToFit(true);
        }


        private void GroupHeader1_Format(object sender, System.EventArgs eArgs)
        {
            if (!_IsCode)
            {
                this.Label9.Text = "";
                this.Line6.Visible = false;
            }
            if (_IsOneBuilding) lbSub.Visible = lbSubLabel.Visible = false;
            this.GroupHeader1.SizeToFit(true);
        }

		#region ActiveReports Designer generated code



























        public void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ContentsType2Subreport));
            this.Detail = new GrapeCity.ActiveReports.SectionReportModel.Detail();
            this.GroupHeader1 = new GrapeCity.ActiveReports.SectionReportModel.GroupHeader();
            this.GroupFooter1 = new GrapeCity.ActiveReports.SectionReportModel.GroupFooter();
            this.Label8 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label9 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label5 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label6 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Line6 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.Line3 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.Line14 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.Line15 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.Line8 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.Line13 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.lbSubLabel = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.lbSub = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtContentType = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.Line1 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.Line12 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.Line16 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.Line17 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.txtContentText = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.Line18 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.txtScale = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtOzn = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Line19 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.txtREV = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtcontentid = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            ((System.ComponentModel.ISupportInitialize)(this.Label8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbSubLabel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbSub)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtContentType)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtContentText)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtScale)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtOzn)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtREV)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtcontentid)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            // 
            // Detail
            // 
            this.Detail.ColumnSpacing = 0F;
            this.Detail.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
						this.txtContentType,
						this.Line1,
						this.Line12,
						this.Line16,
						this.Line17,
						this.txtContentText,
						this.Line18,
						this.txtScale,
						this.txtOzn,
						this.Line19,
						this.txtREV,
						this.txtcontentid});
            this.Detail.Height = 0.3534722F;
            this.Detail.Name = "Detail";
            // 
            // GroupHeader1
            // 
            this.GroupHeader1.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
						this.Label8,
						this.Label9,
						this.Label5,
						this.Label6,
						this.Line6,
						this.Line3,
						this.Line14,
						this.Line15,
						this.Line8,
						this.Line13,
						this.lbSubLabel,
						this.lbSub});
            this.GroupHeader1.DataField = "BuildingNumber";
            this.GroupHeader1.GroupKeepTogether = GrapeCity.ActiveReports.SectionReportModel.GroupKeepTogether.All;
            this.GroupHeader1.Height = 0.6875F;
            this.GroupHeader1.Name = "GroupHeader1";
            this.GroupHeader1.RepeatStyle = GrapeCity.ActiveReports.SectionReportModel.RepeatStyle.OnPageIncludeNoDetail;
            // 
            // GroupFooter1
            // 
            this.GroupFooter1.Height = 0.25F;
            this.GroupFooter1.Name = "GroupFooter1";
            // 
            // Label8
            // 
            this.Label8.Height = 0.2362205F;
            this.Label8.HyperLink = null;
            this.Label8.Left = 2.9F;
            this.Label8.Name = "Label8";
            this.Label8.Style = "font-size: 10pt; font-weight: bold; text-align: center; vertical-align: middle; d" +
                "do-char-set: 1";
            this.Label8.Text = "?????";
            this.Label8.Top = 0.257F;
            this.Label8.Width = 0.7F;
            // 
            // Label9
            // 
            this.Label9.Height = 0.236F;
            this.Label9.HyperLink = null;
            this.Label9.Left = 3.625F;
            this.Label9.Name = "Label9";
            this.Label9.Style = "font-size: 10pt; font-weight: bold; text-align: center; vertical-align: middle; d" +
                "do-char-set: 1";
            this.Label9.Text = "???:";
            this.Label9.Top = 0.257F;
            this.Label9.Width = 3F;
            // 
            // Label5
            // 
            this.Label5.Height = 0.236F;
            this.Label5.HyperLink = null;
            this.Label5.Left = 0.05F;
            this.Label5.Name = "Label5";
            this.Label5.Style = "font-size: 10pt; font-weight: bold; text-align: center; vertical-align: middle; d" +
                "do-char-set: 1";
            this.Label5.Text = "????????";
            this.Label5.Top = 0.257F;
            this.Label5.Width = 2.7625F;
            // 
            // Label6
            // 
            this.Label6.Height = 0.236F;
            this.Label6.HyperLink = null;
            this.Label6.Left = 2.25F;
            this.Label6.Name = "Label6";
            this.Label6.Style = "font-size: 12pt; font-weight: bold; text-align: center; vertical-align: middle";
            this.Label6.Text = "";
            this.Label6.Top = 0.257F;
            this.Label6.Width = 0.5F;
            // 
            // Line6
            // 
            this.Line6.Height = 0.2730556F;
            this.Line6.Left = 3.594F;
            this.Line6.LineWeight = 1F;
            this.Line6.Name = "Line6";
            this.Line6.Top = 0.25F;
            this.Line6.Width = 0F;
            this.Line6.X1 = 3.594F;
            this.Line6.X2 = 3.594F;
            this.Line6.Y1 = 0.25F;
            this.Line6.Y2 = 0.5230556F;
            // 
            // Line3
            // 
            this.Line3.Height = 0F;
            this.Line3.Left = 0F;
            this.Line3.LineWeight = 1F;
            this.Line3.Name = "Line3";
            this.Line3.Top = 0.25F;
            this.Line3.Width = 6.625F;
            this.Line3.X1 = 0F;
            this.Line3.X2 = 6.625F;
            this.Line3.Y1 = 0.25F;
            this.Line3.Y2 = 0.25F;
            // 
            // Line14
            // 
            this.Line14.Height = 0.2760001F;
            this.Line14.Left = 6.625F;
            this.Line14.LineWeight = 1F;
            this.Line14.Name = "Line14";
            this.Line14.Top = 0.25F;
            this.Line14.Width = 0F;
            this.Line14.X1 = 6.625F;
            this.Line14.X2 = 6.625F;
            this.Line14.Y1 = 0.25F;
            this.Line14.Y2 = 0.5260001F;
            // 
            // Line15
            // 
            this.Line15.Height = 0.2755905F;
            this.Line15.Left = 0F;
            this.Line15.LineWeight = 1F;
            this.Line15.Name = "Line15";
            this.Line15.Top = 0.25F;
            this.Line15.Width = 0F;
            this.Line15.X1 = 0F;
            this.Line15.X2 = 0F;
            this.Line15.Y1 = 0.25F;
            this.Line15.Y2 = 0.5255905F;
            // 
            // Line8
            // 
            this.Line8.Height = 0F;
            this.Line8.Left = 0F;
            this.Line8.LineWeight = 1F;
            this.Line8.Name = "Line8";
            this.Line8.Top = 0.5255905F;
            this.Line8.Width = 6.625F;
            this.Line8.X1 = 0F;
            this.Line8.X2 = 6.625F;
            this.Line8.Y1 = 0.5255905F;
            this.Line8.Y2 = 0.5255905F;
            // 
            // Line13
            // 
            this.Line13.Height = 0.2730556F;
            this.Line13.Left = 2.84F;
            this.Line13.LineWeight = 1F;
            this.Line13.Name = "Line13";
            this.Line13.Top = 0.25F;
            this.Line13.Width = 0F;
            this.Line13.X1 = 2.84F;
            this.Line13.X2 = 2.84F;
            this.Line13.Y1 = 0.25F;
            this.Line13.Y2 = 0.5230556F;
            // 
            // lbSubLabel
            // 
            this.lbSubLabel.Height = 0.1875F;
            this.lbSubLabel.Left = 0F;
            this.lbSubLabel.Name = "lbSubLabel";
            this.lbSubLabel.Style = "font-size: 11pt";
            this.lbSubLabel.Text = "????????:";
            this.lbSubLabel.Top = 0.01249999F;
            this.lbSubLabel.Width = 0.9842521F;
            // 
            // lbSub
            // 
            this.lbSub.DataField = "Signature";
            this.lbSub.Height = 0.2F;
            this.lbSub.Left = 1.0075F;
            this.lbSub.Name = "lbSub";
            this.lbSub.Style = "font-size: 11pt; vertical-align: bottom";
            this.lbSub.Top = 0F;
            this.lbSub.Width = 4.055F;
            // 
            // txtContentType
            // 
            this.txtContentType.DataField = "ContentType";
            this.txtContentType.Height = 0.2F;
            this.txtContentType.Left = 0.05F;
            this.txtContentType.Name = "txtContentType";
            this.txtContentType.OutputFormat = resources.GetString("txtContentType.OutputFormat");
            this.txtContentType.Style = "font-size: 9pt; text-align: left; ddo-char-set: 1";
            this.txtContentType.Top = 0F;
            this.txtContentType.Width = 1.8875F;
            // 
            // Line1
            // 
            this.Line1.Height = 0F;
            this.Line1.Left = 0F;
            this.Line1.LineWeight = 1F;
            this.Line1.Name = "Line1";
            this.Line1.Top = 0.275F;
            this.Line1.Width = 6.625F;
            this.Line1.X1 = 0F;
            this.Line1.X2 = 6.625F;
            this.Line1.Y1 = 0.275F;
            this.Line1.Y2 = 0.275F;
            // 
            // Line12
            // 
            this.Line12.Height = 0.276F;
            this.Line12.Left = 1.944F;
            this.Line12.LineWeight = 1F;
            this.Line12.Name = "Line12";
            this.Line12.Top = 0F;
            this.Line12.Width = 0F;
            this.Line12.X1 = 1.944F;
            this.Line12.X2 = 1.944F;
            this.Line12.Y1 = 0F;
            this.Line12.Y2 = 0.276F;
            // 
            // Line16
            // 
            this.Line16.Height = 0.275F;
            this.Line16.Left = 6.625F;
            this.Line16.LineWeight = 1F;
            this.Line16.Name = "Line16";
            this.Line16.Top = 0F;
            this.Line16.Width = 0F;
            this.Line16.X1 = 6.625F;
            this.Line16.X2 = 6.625F;
            this.Line16.Y1 = 0F;
            this.Line16.Y2 = 0.275F;
            // 
            // Line17
            // 
            this.Line17.Height = 0.2755906F;
            this.Line17.Left = 0F;
            this.Line17.LineWeight = 1F;
            this.Line17.Name = "Line17";
            this.Line17.Top = 0F;
            this.Line17.Width = 0F;
            this.Line17.X1 = 0F;
            this.Line17.X2 = 0F;
            this.Line17.Y1 = 0F;
            this.Line17.Y2 = 0.2755906F;
            // 
            // txtContentText
            // 
            this.txtContentText.CanGrow = false;
            this.txtContentText.DataField = "ContentText";
            this.txtContentText.Height = 0.2F;
            this.txtContentText.Left = 2F;
            this.txtContentText.Name = "txtContentText";
            this.txtContentText.Style = "font-size: 9pt; text-align: left; ddo-char-set: 1";
            this.txtContentText.Top = 0F;
            this.txtContentText.Width = 0.8125F;
            // 
            // Line18
            // 
            this.Line18.Height = 0.276F;
            this.Line18.Left = 2.84F;
            this.Line18.LineWeight = 1F;
            this.Line18.Name = "Line18";
            this.Line18.Top = 0F;
            this.Line18.Width = 0F;
            this.Line18.X1 = 2.84F;
            this.Line18.X2 = 2.84F;
            this.Line18.Y1 = 0F;
            this.Line18.Y2 = 0.276F;
            // 
            // txtScale
            // 
            this.txtScale.DataField = "Scale";
            this.txtScale.Height = 0.2F;
            this.txtScale.Left = 2.9F;
            this.txtScale.Name = "txtScale";
            this.txtScale.OutputFormat = resources.GetString("txtScale.OutputFormat");
            this.txtScale.Style = "font-size: 9pt; text-align: center; ddo-char-set: 1";
            this.txtScale.Top = 0F;
            this.txtScale.Width = 0.7F;
            // 
            // txtOzn
            // 
            this.txtOzn.DataField = "ContentCode";
            this.txtOzn.Height = 0.2F;
            this.txtOzn.HyperLink = null;
            this.txtOzn.Left = 3.625F;
            this.txtOzn.Name = "txtOzn";
            this.txtOzn.Style = "font-size: 9pt; text-align: center; ddo-char-set: 1";
            this.txtOzn.Text = "";
            this.txtOzn.Top = 0F;
            this.txtOzn.Width = 3F;
            // 
            // Line19
            // 
            this.Line19.Height = 0.276F;
            this.Line19.Left = 3.594F;
            this.Line19.LineWeight = 1F;
            this.Line19.Name = "Line19";
            this.Line19.Top = 0F;
            this.Line19.Width = 0F;
            this.Line19.X1 = 3.594F;
            this.Line19.X2 = 3.594F;
            this.Line19.Y1 = 0F;
            this.Line19.Y2 = 0.276F;
            // 
            // txtREV
            // 
            this.txtREV.DataField = "ContentRevision";
            this.txtREV.Height = 0.1F;
            this.txtREV.Left = 4.9375F;
            this.txtREV.Name = "txtREV";
            this.txtREV.Top = 0.0625F;
            this.txtREV.Visible = false;
            this.txtREV.Width = 0.1F;
            // 
            // txtcontentid
            // 
            this.txtcontentid.DataField = "ContentID";
            this.txtcontentid.Height = 0.1F;
            this.txtcontentid.Left = 4.625F;
            this.txtcontentid.Name = "txtcontentid";
            this.txtcontentid.Top = 0.0625F;
            this.txtcontentid.Visible = false;
            this.txtcontentid.Width = 0.1F;
            // 
            // ActiveReport1
            // 
            this.MasterReport = false;
            this.PageSettings.PaperHeight = 11F;
            this.PageSettings.PaperWidth = 8.5F;
            this.PrintWidth = 6.656001F;
            this.Sections.Add(this.GroupHeader1);
            this.Sections.Add(this.Detail);
            this.Sections.Add(this.GroupFooter1);
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule(resources.GetString("$this.StyleSheet"), "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: inherit; font-style: inherit; font-variant: inherit; font-weight: bo" +
                        "ld; font-size: 16pt; font-size-adjust: inherit; font-stretch: inherit", "Heading1", "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: Times New Roman; font-style: italic; font-variant: inherit; font-wei" +
                        "ght: bold; font-size: 14pt; font-size-adjust: inherit; font-stretch: inherit", "Heading2", "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: inherit; font-style: inherit; font-variant: inherit; font-weight: bo" +
                        "ld; font-size: 13pt; font-size-adjust: inherit; font-stretch: inherit", "Heading3", "Normal"));
            ((System.ComponentModel.ISupportInitialize)(this.Label8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbSubLabel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbSub)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtContentType)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtContentText)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtScale)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtOzn)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtREV)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtcontentid)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();

            // Attach Report Events
            this.Detail.Format += new System.EventHandler(this.Detail_Format);
            this.GroupHeader1.Format += new System.EventHandler(this.GroupHeader1_Format);
        }

		#endregion

        private GroupHeader GroupHeader1;
        private Label Label8;
        private Label Label9;
        private Label Label5;
        private Label Label6;
        private Line Line6;
        private Line Line3;
        private Line Line14;
        private Line Line15;
        private Line Line8;
        private Line Line13;
        private TextBox lbSubLabel;
        private TextBox lbSub;
        private Detail Detail;
        private TextBox txtContentType;
        private Line Line1;
        private Line Line12;
        private Line Line16;
        private Line Line17;
        private TextBox txtContentText;
        private Line Line18;
        private TextBox txtScale;
        private Label txtOzn;
        private Line Line19;
        private TextBox txtREV;
        private TextBox txtcontentid;
        private GroupFooter GroupFooter1;
	}
}
