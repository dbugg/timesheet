using System;
using System.Collections;
using System.Text;

namespace Asa.Timesheet.Data
{
	public class PaymentData
	{
		private int _paymentID = -1;
		private int _subprojectID = -1;
		private int _projectSubcontracterID = -1;
		private decimal _paymentPercent;
		private decimal _amount;
		private bool _done;
		private DateTime _paymentDate;
		private decimal _paidAmount;
		private bool _mailSent;
		private bool _isSubproject=false;
		private string _notes="";
		private int _authorsID=-1;
		private DateTime _authorsStartDate;
		private DateTime _authorsEndDate;
		private int _clientID;
		/// <summary>
		/// Initialize an new empty Login object.
		/// </summary>
		public PaymentData()
		{
		}
		
		/// <summary>
		/// Initialize a new Login object with the given parameters.
		/// </summary>
		public PaymentData(int paymentID, int subprojectID, int projectSubcontracterID, decimal paymentPercent, decimal amount, bool done, DateTime paymentDate, decimal paidAmount, bool mailSent,int clientID)
		{
			this._paymentID = paymentID;
			this._subprojectID = subprojectID;
			this._projectSubcontracterID = projectSubcontracterID;
			this._paymentPercent = paymentPercent;
			this._amount = amount;
			this._done = done;
			this._paymentDate = paymentDate;
			this._paidAmount = paidAmount;
			this._mailSent = mailSent;
			this._clientID = clientID;
		}	
		
	  
		public override string ToString()
		{
			StringBuilder builder = new StringBuilder();
			builder.Append("_paymentID: " + _paymentID.ToString()).Append("\r\n");
			builder.Append("_subprojectID: " + _subprojectID.ToString()).Append("\r\n");
			builder.Append("_projectSubcontracterID: " + _projectSubcontracterID.ToString()).Append("\r\n");
			builder.Append("_paymentPercent: " + _paymentPercent.ToString()).Append("\r\n");
			builder.Append("_amount: " + _amount.ToString()).Append("\r\n");
			builder.Append("_done: " + _done.ToString()).Append("\r\n");
			builder.Append("_paymentDate: " + _paymentDate.ToString()).Append("\r\n");
			builder.Append("_paidAmount: " + _paidAmount.ToString()).Append("\r\n");
			builder.Append("_mailSent: " + _mailSent.ToString()).Append("\r\n");
			builder.Append("\r\n");
			return builder.ToString();
		}	
		public object GetByIndex(int nIndex)
		{
			switch(nIndex)
			{
				case 0:
					return _paymentID;
				case 1:
					return _subprojectID;
				case 2:
					return _projectSubcontracterID;
				case 3:
					return _paymentPercent;
				case 4:
					return _amount;
				case 5:
					return _done;
				case 6:
					return _paymentDate;
				case 7:
					return _paidAmount;
				case 8:
					return _mailSent;
				case 9:
					return _clientID;
			}
			return null;
		}
		#region Properties
	
		public int PaymentID
		{
			get { return _paymentID; }
			set { _paymentID = value; }
		}
		public int AuthorsID
		{
			get { return _authorsID; }
			set { _authorsID = value; }
		}
		public bool IsSubproject
		{
			get { return _isSubproject; }
			set { _isSubproject = value; }
		}
		
		public string Notes
		{
			get { return _notes; }
			set { _notes = value; }
		}
	
		public int SubprojectID
		{
			get { return _subprojectID; }
			set { _subprojectID = value; }
		}
	
		public int ProjectSubcontracterID
		{
			get { return _projectSubcontracterID; }
			set { _projectSubcontracterID = value; }
		}
	
		public decimal PaymentPercent
		{
			get { return _paymentPercent; }
			set { _paymentPercent = value; }
		}
	
		public decimal Amount
		{
			get { return _amount; }
			set { _amount = value; }
		}
	
		public bool Done
		{
			get { return _done; }
			set { _done = value; }
		}
	
		public DateTime PaymentDate
		{
			get { return _paymentDate; }
			set { _paymentDate = value; }
		}
		public DateTime AuthorsStartDate
		{
			get { return _authorsStartDate; }
			set { _authorsStartDate = value; }
		}
		public DateTime AuthorsEndDate
		{
			get { return _authorsEndDate; }
			set { _authorsEndDate = value; }
		}
	
		public decimal PaidAmount
		{
			get { return _paidAmount; }
			set { _paidAmount = value; }
		}
	
		public bool MailSent
		{
			get { return _mailSent; }
			set { _mailSent = value; }
		}
		public int ClientID
		{
			get { return _clientID; }
			set { _clientID = value; }
		}
		#endregion
	}
}


