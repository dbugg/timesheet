<%@ Register TagPrefix="uc1" TagName="PageHeader" Src="UserControls/PageHeader.ascx" %>
<%@ Page language="c#" Codebehind="EditUser.aspx.cs" AutoEventWireup="false" Inherits="Asa.Timesheet.WebPages.EditUser" smartNavigation="True"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
	<HEAD>
		<TITLE>EditUser</TITLE>
		<meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
		<meta content="C#" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<LINK href="styles/timesheet.css" type="text/css" rel="stylesheet">
	</HEAD>
	<body bottomMargin="0" leftMargin="0" topMargin="0" onload="document.getElementById('txtPassword').value=document.getElementById('hdnPassword').value;"
		rightMargin="0">
		<form id="Form1" method="post" runat="server">
			<table height="100%" cellSpacing="0" cellPadding="0" width="100%" border="0">
				<tr height="1">
					<td><uc1:pageheader id="header" runat="server"></uc1:pageheader></td>
				</tr>
				<TR height="1">
					<td noWrap background="images/line.gif" height="1"></td>
				</TR>
				<tr>
					<td>
						<table height="100%" cellSpacing="0" cellPadding="0" width="100%" border="0">
							<tr>
								<td style="BACKGROUND-IMAGE: url(images/left110.gif)" vAlign="top" noWrap width="110"><asp:placeholder id="menuHolder" runat="server"></asp:placeholder></td>
								<td noWrap width="1" bgColor="lightgrey"></td>
								<td style="PADDING-LEFT: 5px; PADDING-TOP: 3px" vAlign="top" noWrap>
									<TABLE id="Table3" cellSpacing="0" width="880" border="0">
										<TR>
											<TD style="WIDTH: 4px"></TD>
											<TD>
												<TABLE id="Table4" cellSpacing="0" cellPadding="3" width="100%" border="0">
													<TR>
														<TD style="WIDTH: 146px"><asp:label id="lblFirstName" runat="server" CssClass="enterDataLabel">Име:</asp:label></TD>
														<TD><asp:textbox id="txtFirstName" runat="server" CssClass="enterDataBox" Width="200px"></asp:textbox></TD>
													</TR>
													<TR>
														<TD style="WIDTH: 146px"><asp:label id="lblSecondName" runat="server" CssClass="enterDataLabel"> Презиме:</asp:label></TD>
														<TD><asp:textbox id="txtSecondName" runat="server" CssClass="enterDataBox" Width="200px"></asp:textbox></TD>
													</TR>
													<TR>
														<TD style="WIDTH: 146px"><asp:label id="lblLastName" runat="server" CssClass="enterDataLabel" EnableViewState="False"
																Font-Bold="True"> Фамилия:</asp:label></TD>
														<TD><asp:textbox id="txtLastName" runat="server" CssClass="enterDataBox" Width="200px"></asp:textbox>&nbsp;<IMG height="16" alt="" src="images/required1.gif" width="16"></TD>
													</TR>
													<TR>
														<TD style="WIDTH: 146px"><asp:label id="lblAccount" runat="server" CssClass="enterDataLabel" EnableViewState="False"
																Font-Bold="True"> Акаунт:</asp:label></TD>
														<TD><asp:textbox id="txtAccount" runat="server" CssClass="enterDataBox" Width="248px"></asp:textbox>&nbsp;<IMG height="16" alt="" src="images/required1.gif" width="16"></TD>
													</TR>
													<TR>
														<TD style="WIDTH: 146px"><asp:label id="lblEmail" runat="server" CssClass="enterDataLabel" EnableViewState="False" Font-Bold="True">Електронен адрес:</asp:label></TD>
														<TD><asp:textbox id="txtEmail" runat="server" CssClass="enterDataBox" Width="248px"></asp:textbox>&nbsp;<IMG height="16" alt="" src="images/required1.gif" width="16"></TD>
													</TR>
													<TR>
														<TD style="WIDTH: 146px; HEIGHT: 21px"><asp:label id="lblRole" runat="server" CssClass="enterDataLabel" Font-Bold="True">Роля в системата: </asp:label></TD>
														<TD style="HEIGHT: 21px"><asp:dropdownlist id="ddlRole" runat="server" CssClass="enterDataBox" Width="248px"></asp:dropdownlist>&nbsp;<IMG height="16" alt="" src="images/required1.gif" width="16"></TD>
													</TR>
													<TR>
														<TD style="WIDTH: 146px"><asp:label id="lblPassword" runat="server" CssClass="enterDataLabel" Font-Bold="True">Парола:</asp:label></TD>
														<TD><asp:textbox id="txtPassword" runat="server" CssClass="enterDataBox" Width="200px" TextMode="Password"></asp:textbox>&nbsp;<IMG height="16" alt="" src="images/required1.gif" width="16">
															<asp:textbox id="txtHdnPassword" runat="server" Visible="False"></asp:textbox><INPUT id="hdnPassword" type="hidden" runat="server"></TD>
													</TR>
													<TR>
														<TD style="WIDTH: 146px"><asp:label id="Label2" runat="server" CssClass="enterDataLabel">Длъжност:</asp:label></TD>
														<TD><asp:dropdownlist id="ddlOccupation" runat="server" CssClass="enterDataBox" Width="248px">
																<asp:ListItem></asp:ListItem>
																<asp:ListItem Value="Архитект">Архитект</asp:ListItem>
																<asp:ListItem Value="Инженер">Инженер</asp:ListItem>
																<asp:ListItem Value="Техник">Техник</asp:ListItem>
															</asp:dropdownlist>&nbsp;<IMG height="16" alt="" src="images/required1.gif" width="16"></TD>
													</TR>
													<TR>
														<TD style="WIDTH: 146px"><asp:label id="Label9" runat="server" CssClass="enterDataLabel">Стажант:</asp:label></TD>
														<TD>
															<asp:CheckBox id="cbIsTraine" runat="server"></asp:CheckBox></TD>
													</TR>
													<TR>
														<TD style="WIDTH: 146px"><asp:label id="Label3" runat="server" CssClass="enterDataLabel">Част:</asp:label></TD>
														<TD><asp:dropdownlist id="ddlUserRole" runat="server" CssClass="enterDataBox" Width="248px">
																<asp:ListItem></asp:ListItem>
																<asp:ListItem Value="Архитектура">Архитектура</asp:ListItem>
																<asp:ListItem Value="ПСД">ПСД</asp:ListItem>
																<asp:ListItem Value="Конструктор">Конструктор</asp:ListItem>
															</asp:dropdownlist></TD>
													</TR>
													<TR>
														<TD style="WIDTH: 146px"><asp:label id="Label11" runat="server" CssClass="enterDataLabel">АСИ:</asp:label></TD>
														<TD>
															<asp:CheckBox id="cbIsASI" runat="server"></asp:CheckBox></TD>
													</TR>
													<TR>
														<TD style="WIDTH: 146px"><asp:label id="Label4" runat="server" CssClass="enterDataLabel">Вътрешен телефон:</asp:label></TD>
														<TD><asp:textbox id="txtExt" runat="server" CssClass="enterDataBox" Width="248px"></asp:textbox></TD>
													</TR>
													<TR>
														<TD style="WIDTH: 146px"><asp:label id="Label5" runat="server" CssClass="enterDataLabel">Мобилен телефон:</asp:label></TD>
														<TD><asp:textbox id="txtMobile" runat="server" CssClass="enterDataBox" Width="248px"></asp:textbox></TD>
													</TR>
													<TR>
														<TD style="WIDTH: 146px"><asp:label id="Label10" runat="server" CssClass="enterDataLabel">Личен мобилен:</asp:label></TD>
														<TD><asp:textbox id="txtMobilePersonal" runat="server" CssClass="enterDataBox" Width="248px"></asp:textbox></TD>
													</TR>
													<TR>
														<TD style="WIDTH: 146px"><asp:label id="Label6" runat="server" CssClass="enterDataLabel">Домашен телефон:</asp:label></TD>
														<TD><asp:textbox id="txtHome" runat="server" CssClass="enterDataBox" Width="248px"></asp:textbox></TD>
													</TR>
													<TR>
														<TD style="WIDTH: 146px"><asp:label id="Label1" runat="server" CssClass="enterDataLabel">Кратко CV:</asp:label></TD>
														<TD><asp:textbox id="txtCV" runat="server" CssClass="enterDataBox" Width="480px" TextMode="MultiLine"
																Height="96px"></asp:textbox></TD>
													</TR>
													<TR>
														<TD style="WIDTH: 146px"><asp:label id="lbHasWorkTime" runat="server" CssClass="enterDataLabel">Въвежда ли раб. време:</asp:label></TD>
														<TD>
															<asp:CheckBox id="cbHasWorkTime" runat="server" AutoPostBack="True"></asp:CheckBox></TD>
													</TR>
													<TR>
														<TD style="WIDTH: 146px"><asp:label id="lbSalary" runat="server" CssClass="enterDataLabel" Visible="False"> Заплата:</asp:label></TD>
														<TD></TD>
													</TR>
													<TR>
														<TD colSpan="2"><asp:datagrid id="grdSalaries" runat="server" CssClass="Grid" Width="100%" AutoGenerateColumns="False"
																CellPadding="4" PageSize="2" AllowSorting="True">
																<ItemStyle CssClass="GridItem"></ItemStyle>
																<HeaderStyle Wrap="False" CssClass="GridHeader1"></HeaderStyle>
																<Columns>
																	<asp:TemplateColumn HeaderText="Година">
																		<ItemTemplate>
																			<asp:Label runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.SalaryYear").ToString()+ " - " + GetSalaryType((int)DataBinder.Eval(Container, "DataItem.SalaryType")) %>'>
																			</asp:Label>
																		</ItemTemplate>
																	</asp:TemplateColumn>
																	<asp:TemplateColumn HeaderText="Яну">
																		<ItemStyle ForeColor="DimGray" Width="10px"></ItemStyle>
																		<ItemTemplate>
																			<asp:Textbox CssClass="textboxSalary" runat="server" Text='<%# GetSalary((int)DataBinder.Eval(Container, "DataItem.SalaryYear"),(int)DataBinder.Eval(Container, "DataItem.SalaryType"),1)  %>' ID="txtJan" >
																			</asp:Textbox>
																		</ItemTemplate>
																	</asp:TemplateColumn>
																	<asp:TemplateColumn HeaderText="Фев">
																		<ItemStyle ForeColor="DimGray" Width="10px"></ItemStyle>
																		<ItemTemplate>
																			<asp:Textbox CssClass="textboxSalary" runat="server" Text='<%# GetSalary((int)DataBinder.Eval(Container, "DataItem.SalaryYear"),(int)DataBinder.Eval(Container, "DataItem.SalaryType"),2)  %>' ID="txtFeb" >
																			</asp:Textbox>
																		</ItemTemplate>
																	</asp:TemplateColumn>
																	<asp:TemplateColumn HeaderText="Март">
																		<ItemStyle ForeColor="DimGray" Width="10px"></ItemStyle>
																		<ItemTemplate>
																			<asp:Textbox CssClass="textboxSalary" runat="server" Text='<%# GetSalary((int)DataBinder.Eval(Container, "DataItem.SalaryYear"),(int)DataBinder.Eval(Container, "DataItem.SalaryType"),3)  %>' ID="txtMarch" >
																			</asp:Textbox>
																		</ItemTemplate>
																	</asp:TemplateColumn>
																	<asp:TemplateColumn HeaderText="Апр">
																		<ItemStyle ForeColor="DimGray" Width="10px"></ItemStyle>
																		<ItemTemplate>
																			<asp:Textbox CssClass="textboxSalary" runat="server" Text='<%# GetSalary((int)DataBinder.Eval(Container, "DataItem.SalaryYear"),(int)DataBinder.Eval(Container, "DataItem.SalaryType"),4)  %>' ID="txtApril" >
																			</asp:Textbox>
																		</ItemTemplate>
																	</asp:TemplateColumn>
																	<asp:TemplateColumn HeaderText="Май">
																		<ItemStyle ForeColor="DimGray" Width="10px"></ItemStyle>
																		<ItemTemplate>
																			<asp:Textbox CssClass="textboxSalary" runat="server" Text='<%# GetSalary((int)DataBinder.Eval(Container, "DataItem.SalaryYear"),(int)DataBinder.Eval(Container, "DataItem.SalaryType"),5)  %>' ID="txtMay" >
																			</asp:Textbox>
																		</ItemTemplate>
																	</asp:TemplateColumn>
																	<asp:TemplateColumn HeaderText="Юни">
																		<ItemStyle ForeColor="DimGray" Width="10px"></ItemStyle>
																		<ItemTemplate>
																			<asp:Textbox CssClass="textboxSalary" runat="server" Text='<%# GetSalary((int)DataBinder.Eval(Container, "DataItem.SalaryYear"),(int)DataBinder.Eval(Container, "DataItem.SalaryType"),6)  %>' ID="txtJune" >
																			</asp:Textbox>
																		</ItemTemplate>
																	</asp:TemplateColumn>
																	<asp:TemplateColumn HeaderText="Юли">
																		<ItemStyle ForeColor="DimGray" Width="10px"></ItemStyle>
																		<ItemTemplate>
																			<asp:Textbox CssClass="textboxSalary" runat="server" Text='<%# GetSalary((int)DataBinder.Eval(Container, "DataItem.SalaryYear"),(int)DataBinder.Eval(Container, "DataItem.SalaryType"),7)  %>' ID="txtJuly" >
																			</asp:Textbox>
																		</ItemTemplate>
																	</asp:TemplateColumn>
																	<asp:TemplateColumn HeaderText="Авг">
																		<ItemStyle ForeColor="DimGray" Width="10px"></ItemStyle>
																		<ItemTemplate>
																			<asp:Textbox CssClass="textboxSalary" runat="server" Text='<%# GetSalary((int)DataBinder.Eval(Container, "DataItem.SalaryYear"),(int)DataBinder.Eval(Container, "DataItem.SalaryType"),8)  %>' ID="txtAug" >
																			</asp:Textbox>
																		</ItemTemplate>
																	</asp:TemplateColumn>
																	<asp:TemplateColumn HeaderText="Септ">
																		<ItemStyle ForeColor="DimGray" Width="10px"></ItemStyle>
																		<ItemTemplate>
																			<asp:Textbox CssClass="textboxSalary" runat="server" Text='<%# GetSalary((int)DataBinder.Eval(Container, "DataItem.SalaryYear"),(int)DataBinder.Eval(Container, "DataItem.SalaryType"),9)  %>' ID="txtSept" >
																			</asp:Textbox>
																		</ItemTemplate>
																	</asp:TemplateColumn>
																	<asp:TemplateColumn HeaderText="Окт">
																		<ItemStyle ForeColor="DimGray" Width="10px"></ItemStyle>
																		<ItemTemplate>
																			<asp:Textbox CssClass="textboxSalary" runat="server" Text='<%# GetSalary((int)DataBinder.Eval(Container, "DataItem.SalaryYear"),(int)DataBinder.Eval(Container, "DataItem.SalaryType"),10)  %>' ID="txtOct" >
																			</asp:Textbox>
																		</ItemTemplate>
																	</asp:TemplateColumn>
																	<asp:TemplateColumn HeaderText="Ное">
																		<ItemStyle ForeColor="DimGray" Width="10px"></ItemStyle>
																		<ItemTemplate>
																			<asp:Textbox CssClass="textboxSalary" runat="server" Text='<%# GetSalary((int)DataBinder.Eval(Container, "DataItem.SalaryYear"),(int)DataBinder.Eval(Container, "DataItem.SalaryType"),11)  %>' ID="txtNov" >
																			</asp:Textbox>
																		</ItemTemplate>
																	</asp:TemplateColumn>
																	<asp:TemplateColumn HeaderText="Дек">
																		<ItemStyle ForeColor="DimGray" Width="10px"></ItemStyle>
																		<ItemTemplate>
																			<asp:Textbox CssClass="textboxSalary" runat="server" Text='<%# GetSalary((int)DataBinder.Eval(Container, "DataItem.SalaryYear"),(int)DataBinder.Eval(Container, "DataItem.SalaryType"),12)  %>' ID="txtDec" >
																			</asp:Textbox>
																		</ItemTemplate>
																	</asp:TemplateColumn>
																	<asp:TemplateColumn HeaderText="Средно/Общо">
																		<ItemStyle ForeColor="DimGray" Width="10px"></ItemStyle>
																		<ItemTemplate>
																			<asp:Textbox CssClass="textboxSalary" runat="server" ID="Textbox12" Text='<%# GetTotal((int)DataBinder.Eval(Container, "DataItem.SalaryYear"),(int)DataBinder.Eval(Container, "DataItem.SalaryType"))  %>'>
																			</asp:Textbox>
																		</ItemTemplate>
																	</asp:TemplateColumn>
																</Columns>
															</asp:datagrid></TD>
													</TR>
												</TABLE>
												<table id="tbl" cellSpacing="0" cellPadding="3" runat="server">
													<TR>
														<TD style="WIDTH: 146px"><asp:label id="Label7" runat="server" CssClass="enterDataLabel" Visible="False"> Заплата:</asp:label></TD>
														<TD><asp:textbox id="txtSalary" runat="server" CssClass="enterDataBox" Width="100px" Visible="False"></asp:textbox></TD>
													</TR>
													<TR>
														<TD style="WIDTH: 146px"><asp:label id="Label8" runat="server" CssClass="enterDataLabel" Visible="False">Бонус:</asp:label></TD>
														<TD><asp:textbox id="txtBonus" runat="server" CssClass="enterDataBox" Width="100px" Visible="False"></asp:textbox></TD>
													</TR>
												</table>
											</TD>
										</TR>
										<TR>
											<TD style="HEIGHT: 5px" colSpan="2"></TD>
										</TR>
										<TR>
											<TD colSpan="2"><IMG height="1" src="images/dot.gif" width="100%"></TD>
										</TR>
										<TR>
											<td colSpan="2" height="3"></td>
										</TR>
										<TR>
											<TD style="WIDTH: 4px"></TD>
											<TD>
												<TABLE id="Table5" cellSpacing="0" cellPadding="0" border="0">
													<TR>
														<TD style="WIDTH: 97px" noWrap><asp:button id="btnSave" runat="server" CssClass="ActionButton" Text="Запиши"></asp:button></TD>
														<TD style="WIDTH: 216px" noWrap><asp:button id="btnCancel" runat="server" CssClass="ActionButton" Text="Обратно"></asp:button></TD>
														<TD noWrap><asp:button id="btnDelete" runat="server" CssClass="ActionButton" Text="Изтрий"></asp:button></TD>
													</TR>
												</TABLE>
											</TD>
										</TR>
										<TR>
											<TD style="WIDTH: 4px" height="3"></TD>
										</TR>
										<TR>
											<TD colSpan="2"><IMG height="1" src="images/dot.gif" width="100%"></TD>
										</TR>
									</TABLE>
									&nbsp;
									<asp:label id="lblInfo" runat="server" CssClass="InfoLabel" EnableViewState="False"></asp:label><asp:label id="lblError" runat="server" CssClass="ErrorLabel" EnableViewState="False" ForeColor="Red"></asp:label></td>
							</tr>
						</table>
					</td>
				</tr>
			</table>
		</form>
	</body>
</HTML>
