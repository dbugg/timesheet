using System;
using System.Data;
using System.Data.SqlClient; 
using System.Collections;
using Microsoft.ApplicationBlocks.Data;

namespace Asa.Timesheet.Data
{
	public class MeetingCategorieDAL
	{
		/// <summary>
		/// Loads a MeetingCategorieData object from the database using the given MeetingCategoryID
		/// </summary>
		/// <param name="meetingCategoryID">The primary key value</param>
		/// <returns>A MeetingCategorieData object</returns>
		public static MeetingCategorieData Load(int meetingCategoryID)
		{
			SqlParameter[] parameterValues = new SqlParameter[] { new SqlParameter("@MeetingCategoryID", meetingCategoryID) };
			using (SqlDataReader reader = SqlHelper.ExecuteReader(CommonDAL.ConnectionString, CommandType.StoredProcedure, "MeetingCategoriesSelProc", parameterValues))
			{
				if (reader.Read())
				{
					MeetingCategorieData result = new MeetingCategorieData();
					LoadFromReader(reader,result);
					return result;
				}
				else
					return null;
			}
		}

		/// <summary>
		/// Loads a MeetingCategorieData object from the database using the given where clause
		/// </summary>
		/// <param name="whereClause">The filter expression for the query</param>
		/// <returns>A MeetingCategorieData object</returns>
		public static MeetingCategorieData Load(string whereClause)
		{
			SqlParameter[] parameterValues = new SqlParameter[] { new SqlParameter("@WhereClause", whereClause) };
			using (SqlDataReader reader = SqlHelper.ExecuteReader(CommonDAL.ConnectionString, CommandType.StoredProcedure, "MeetingCategoriesListProc", parameterValues))
			{
				if (reader.Read())
				{
					MeetingCategorieData result = new MeetingCategorieData();
					LoadFromReader(reader,result);
					return result;
				}
				else
					return null;
			}
		}
		public static int Save( MeetingCategorieData source)
		{
			if (source.MeetingCategoryID == -1)
				return Insert(source);
			else
				return Update(source);	
		}

		/*public static void Delete( MeetingCategorieData source)
		{
			Delete(source.MeetingCategoryID);
		}*/
		/// <summary>
		/// Loads a collection of MeetingCategorieData objects from the database.
		/// </summary>
		/// <returns>A collection containing all of the MeetingCategorieData objects in the database.</returns>
		public static MeetingCategoriesVector LoadCollection()
		{
			SqlParameter[] parms = new SqlParameter[] {};
			return LoadCollection("MeetingCategoriesListProc", parms);
		}
	  
		/// <summary>
		/// Saves a collection of MeetingCategorieData objects to the database.
		/// </summary>
		/// <returns>A collection containing all of the MeetingCategorieData objects in the database.</returns>
		/*public static  void  SaveCollection(MeetingCategoriesVector col,string spName, SqlParameter[] parms)
		{
			MeetingCategoriesVector colOld = LoadCollection(spName,parms);
			UpdateCollection(colOld,col);
		}
		public static  void  SaveCollection(MeetingCategoriesVector col)
		{
			MeetingCategoriesVector colOld = LoadCollection();
			UpdateCollection(colOld,col);
		}*/
//		public static  void  SaveCollection(MeetingCategoriesVector col, string whereClause )
//		{
//			MeetingCategoriesVector colOld = LoadCollection(whereClause);
//			UpdateCollection(colOld,col);
//		}
		/// <summary>
		/// Loads a collection of MeetingCategorieData objects from the database.
		/// </summary>
		/// <returns>A collection containing all of the MeetingCategorieData objects in the database.</returns>
		public static MeetingCategoriesVector LoadCollection(int nMeetingID, int nProjectID)
		{
			SqlParameter[] parms = new SqlParameter[] { new SqlParameter("@MeetingID", nMeetingID),new SqlParameter("@ProjectID", nProjectID) };
			return LoadCollection("MeetingCategoriesListProc", parms);
		}

		/// <summary>
		/// Loads a collection of MeetingCategorieData objects from the database.
		/// </summary>
		/// <returns>A collection containing all of the MeetingCategorieData objects in the database.</returns>
		public static MeetingCategoriesVector LoadCollection(string spName, SqlParameter[] parms)
		{
			MeetingCategoriesVector result = new MeetingCategoriesVector();
			using (SqlDataReader reader = SqlHelper.ExecuteReader(CommonDAL.ConnectionString, CommandType.StoredProcedure, spName, parms))
			{
				while (reader.Read())
				{
					MeetingCategorieData tmp = new MeetingCategorieData();
					LoadFromReader(reader, tmp);
					result.Add(tmp);
				}
			}
			return result;
		}
		/// <summary>
		/// Deletes a MeetingCategorieData object from the database.
		/// </summary>
		/// <param name="meetingCategoryID">The primary key value</param>
		public static void Delete(int meetingID, int projectid)
		{
			SqlParameter[] parameterValues = new SqlParameter[] { new SqlParameter("@MeetingID", meetingID),new SqlParameter("@ProjectID", projectid) };
			SqlHelper.ExecuteNonQuery(CommonDAL.ConnectionString, CommandType.StoredProcedure, "MeetingCategoriesDelProc", parameterValues);
		}

		public static void LoadFromReader(SqlDataReader reader, MeetingCategorieData target)
		{
			if (reader != null && !reader.IsClosed)
			{
				//target.Key = reader.GetInt32(0);
				if (!reader.IsDBNull(0)) target.MeetingCategoryID = reader.GetInt32(0);
				if (!reader.IsDBNull(1)) target.MeetingID = reader.GetInt32(1);
				if (!reader.IsDBNull(2)) target.HotIssueCategoryID = reader.GetInt32(2);
			}
		}
		
	  
		public static int Insert( MeetingCategorieData source)
		{
			SqlParameter[] parameterValues = GetInsertParameterValues(source);
			SqlHelper.ExecuteNonQuery(CommonDAL.ConnectionString, CommandType.StoredProcedure, "MeetingCategoriesInsProc", parameterValues);
			source.MeetingCategoryID = (int) parameterValues[0].Value;
			return source.MeetingCategoryID;
		}

		public static int Update( MeetingCategorieData source)
		{
			SqlParameter[] parameterValues = GetUpdateParameterValues(source);
			SqlHelper.ExecuteNonQuery(CommonDAL.ConnectionString, CommandType.StoredProcedure,
				"MeetingCategoriesUpdProc", parameterValues);
			return source.MeetingCategoryID;
		}
		/*private static void UpdateCollection(MeetingCategoriesVector colOld,MeetingCategoriesVector col)
		{
			// Delete all old items
			foreach( MeetingCategorieData itemOld in colOld)
			{
				if(!ExistsKey(itemOld.MeetingCategoryID, col))
					Delete(itemOld.MeetingCategoryID);
			}
			foreach( MeetingCategorieData item in col)
			{
				if(item.MeetingCategoryID <= -1)
					Insert(item);
				else
					Update(item);
			}
		}*/
		private static bool ExistsKey(int Key, MeetingCategoriesVector col)
		{
			foreach( MeetingCategorieData item in col)
				if(item.MeetingCategoryID==Key)
					return true;
			return false;
		}

		private static SqlParameter GetSqlParameter(string name, ParameterDirection direction, object value)
		{
			SqlParameter p = new SqlParameter(name, value);
			p.Direction = direction;
			return p;
		}

		private static SqlParameter[] GetInsertParameterValues( MeetingCategorieData source)
		{
			SqlParameter[] prams = new SqlParameter[3];
			prams[0] = GetSqlParameter("@MeetingCategoryID", ParameterDirection.Output, source.MeetingCategoryID);
			prams[1] = GetSqlParameter("@MeetingID", ParameterDirection.Input, source.MeetingID);
			prams[2] = GetSqlParameter("@HotIssueCategoryID", ParameterDirection.Input, source.HotIssueCategoryID);
			return prams;
		}

		private static SqlParameter[] GetUpdateParameterValues( MeetingCategorieData source)
		{
			return new SqlParameter[] {
										  new SqlParameter("@MeetingCategoryID", source.MeetingCategoryID),
										  new SqlParameter("@MeetingID", source.MeetingID),
										  new SqlParameter("@HotIssueCategoryID", source.HotIssueCategoryID)
									  };
		}
	}
}   

