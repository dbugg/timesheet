using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Data.SqlClient;
using Asa.Timesheet.Data;
using Asa.Timesheet.Data.Entities;
using Asa.Timesheet.Data.Util;
using log4net;
using GrapeCity.ActiveReports.Export.Pdf.Section;
using GrapeCity.ActiveReports.Export.Excel.Section;

using Asa.Timesheet.WebPages.Reports;
using GrapeCity.ActiveReports.Export.Word.Section;
namespace Asa.Timesheet.WebPages
{
    /// <summary>
    /// Summary description for HotIssues.
    /// </summary>
    public class HotIssues : TimesheetPageBase
    {
        #region WebControls
        private static readonly ILog log = LogManager.GetLogger(typeof(HotIssues));
        protected System.Web.UI.WebControls.PlaceHolder menuHolder;
        protected System.Web.UI.WebControls.Label lblError;
        protected System.Web.UI.WebControls.Label lblInfo;
        protected System.Web.UI.WebControls.Label Label4;
        protected System.Web.UI.WebControls.TextBox txtStartDate;
        protected System.Web.UI.WebControls.TextBox txtEndDate;
        protected System.Web.UI.WebControls.Label lblProject;
        protected System.Web.UI.WebControls.DropDownList ddlProjectsStatus;
        protected System.Web.UI.WebControls.Label Label1;
        protected System.Web.UI.WebControls.DropDownList ddlBuildingTypes;
        protected System.Web.UI.WebControls.Label Label2;
        protected System.Web.UI.WebControls.DropDownList ddlProject;
        protected System.Web.UI.WebControls.Button btnSearch;
        protected System.Web.UI.WebControls.DataGrid grdMain;
        protected System.Web.UI.WebControls.Panel grid;
        protected System.Web.UI.WebControls.Button btnNew;
        protected System.Web.UI.WebControls.Button btnBackToProject;
        protected System.Web.UI.HtmlControls.HtmlImage lkCalendar1;
        protected System.Web.UI.WebControls.Label Label3;
        protected System.Web.UI.WebControls.DropDownList ddlCategory;
        protected System.Web.UI.WebControls.Label Label5;
        protected System.Web.UI.WebControls.Label Label6;
        protected System.Web.UI.WebControls.DropDownList ddlStatus;
        protected System.Web.UI.HtmlControls.HtmlImage lkCalendar2;
        protected System.Web.UI.WebControls.Button btnNew1;
        protected System.Web.UI.WebControls.ImageButton ibXlsExport;
        protected System.Web.UI.WebControls.ImageButton ibPdfExport;
        protected System.Web.UI.WebControls.ImageButton ibWordExport;
        protected System.Web.UI.WebControls.Label lbClient;
        protected System.Web.UI.WebControls.Label lbSubs;
        protected System.Web.UI.WebControls.Label lbTeam;
        protected System.Web.UI.WebControls.Label lbClientData;
        protected System.Web.UI.WebControls.Label lbSubsData;
        protected System.Web.UI.WebControls.Label lbTeamData;
        protected System.Web.UI.WebControls.Label lbBuilder;
        protected System.Web.UI.WebControls.Label lbBuilderData;
        protected System.Web.UI.WebControls.DataGrid grdTemi;
        protected System.Web.UI.WebControls.Panel Panel1;
        protected System.Web.UI.WebControls.CheckBoxList plcTeam;
        protected System.Web.UI.WebControls.CheckBoxList plcClients;
        protected System.Web.UI.WebControls.CheckBoxList plcBuilders;
        protected System.Web.UI.WebControls.TextBox txtDocuments;
        protected System.Web.UI.WebControls.TextBox txtID;
        protected System.Web.UI.WebControls.TextBox txtDate;
        protected System.Web.UI.WebControls.Label lblDocs;
        protected Asa.Timesheet.WebPages.UserControls.PageHeader header;

        #endregion

        #region enums
        private enum ColumnsTemi
        {
            Name,
            Delete
        }
        private enum gridColomns
        {
            HotIssueID = 0,
            HotIssueName,
            ProjectName,
            HotIssueCategoryName,
            HotIssueStatusID,
            CreatedDate,
            HotIssueDeadline,
            HotIssueAssignedTo,
            HotIssueAssignedToType,
            AssignedToText,
            HotIssueStatusName,
            Done,
            Delete,
        }

        #endregion

        #region Page_Load

        private void Page_Load(object sender, System.EventArgs e)
        {
            lkCalendar1.Attributes["onclick"] = TimeHelper.InvokePopupCal(txtStartDate);
            lkCalendar2.Attributes["onclick"] = TimeHelper.InvokePopupCal(txtEndDate);
            UIHelpers.CreateMenu(menuHolder, LoggedUser);

            if (!this.IsPostBack)
            {
                if (Page.Request.Params["ASA"] == "1")
                    header.PageTitle = Resource.ResourceManager["menuItem_HotIssuesASA"];
                else
                    header.PageTitle = Resource.ResourceManager["menuItem_HotIssues"];
                txtDate.Text = DateTime.Now.Date.ToString("d");
                header.UserName = this.LoggedUser.UserName;
                UIHelpers.LoadBuildingTypes(ddlBuildingTypes);
                UIHelpers.LoadProjectStatus(ddlProjectsStatus);
                LoadProjects();
                LoadHotIssuesObjects();
                if (UIHelpers.GetPIDParam() != -1 && ddlProject.Items.FindByValue(UIHelpers.GetPIDParam().ToString()) != null)
                {
                    ddlProject.SelectedValue = UIHelpers.GetPIDParam().ToString();
                    btnBackToProject.Visible = true;
                }
                else
                {
                    btnBackToProject.Visible = false;

                    if (SessionManager.CurrentHotIssueInfo != null)
                    {

                        if (ddlProjectsStatus.Items.FindByValue(SessionManager.CurrentHotIssueInfo.TypeID.ToString()) != null)
                            ddlProjectsStatus.SelectedValue = SessionManager.CurrentHotIssueInfo.TypeID.ToString();
                        if (ddlBuildingTypes.Items.FindByValue(SessionManager.CurrentHotIssueInfo.BuildingType) != null)
                            ddlBuildingTypes.SelectedValue = SessionManager.CurrentHotIssueInfo.BuildingType;
                        LoadProjects();
                        if (ddlProject.Items.FindByValue(SessionManager.CurrentHotIssueInfo.ProjectID.ToString()) != null)
                            ddlProject.SelectedValue = SessionManager.CurrentHotIssueInfo.ProjectID.ToString();
                        txtStartDate.Text = SessionManager.CurrentHotIssueInfo.StartDate;
                        txtEndDate.Text = SessionManager.CurrentHotIssueInfo.EndDate;
                    }
                }

            }
            else
                UIHelpers.LoadBuildingTypes(ddlBuildingTypes.SelectedValue, ddlBuildingTypes, (ProjectsData.ProjectsByStatus)int.Parse(ddlProjectsStatus.SelectedValue));
            if (!IsPostBack)
                BindGrid();

        }

        #endregion

        #region EventHandlers
        private void btnNew_Click(object sender, System.EventArgs e)
        {
            int nID = UIHelpers.ToInt(ddlProject.SelectedValue);
            string sASA = "";
            if (Page.Request.Params["ASA"] == "1")
                sASA = "&ASA=1";
            if (UIHelpers.GetPIDParam() != -1)
                Response.Redirect("EditHotIssue.aspx" + "?pid=" + UIHelpers.GetPIDParam() + sASA);
            else if (nID > 0)
                Response.Redirect("EditHotIssue.aspx" + "?pid=" + nID.ToString() + sASA);
            else
            {
                sASA = sASA.Replace("&", "?");
                Response.Redirect("EditHotIssue.aspx" + sASA);
            }

        }
        private void btnSearch_Click(object sender, System.EventArgs e)
        {
            BindGrid();
        }
        private void ddlProjectsStatus_SelectedIndexChanged(object sender, System.EventArgs e)
        {
            LoadProjects();
            UIHelpers.LoadHotIssueCategories(ddlCategory, UIHelpers.ToInt(ddlProject.SelectedValue), UIHelpers.ToInt(ddlBuildingTypes.SelectedValue), UIHelpers.ToInt(ddlProjectsStatus.SelectedValue));

        }
        private void ddlBuidingTypes_SelectedIndexChanged(object sender, System.EventArgs e)
        {
            LoadProjects();
            UIHelpers.LoadHotIssueCategories(ddlCategory, UIHelpers.ToInt(ddlProject.SelectedValue), UIHelpers.ToInt(ddlBuildingTypes.SelectedValue), UIHelpers.ToInt(ddlProjectsStatus.SelectedValue));

        }
        private void grdMain_ItemCommand(object source, System.Web.UI.WebControls.DataGridCommandEventArgs e)
        {
            if (e.CommandName == "Edit")
            {
                string SID = ((DataGrid)source).DataKeys[e.Item.ItemIndex].ToString();
                string sASA = "";
                if (Page.Request.Params["ASA"] == "1")
                    sASA = "&ASA=1";
                if (UIHelpers.GetPIDParam() != -1)
                    Response.Redirect("EditHotIssue.aspx" + "?id=" + SID + "&pid=" + UIHelpers.GetPIDParam() + sASA);
                else
                    Response.Redirect("EditHotIssue.aspx" + "?id=" + SID + sASA);
                return;
            }
            if (!(e.CommandSource is ImageButton))
                return;
            ImageButton ib = (ImageButton)e.CommandSource;
            string s = ib.ID;
            switch (ib.ID)
            {
                case "btnDelete":
                    int ID = (int)((DataGrid)source).DataKeys[e.Item.ItemIndex];
                    HotIssueDAL.Delete(ID);
                    log.Info(string.Format("HotIssue {0} has been DELETED by {1}", ID, string.Concat(LoggedUser.FullName, " ( userID ", LoggedUser.UserID, " )")));
                    BindGrid();
                    break;
                //				case "btnFile": 
                //					int id = (int)((DataGrid)source).DataKeys[e.Item.ItemIndex];
                //					
                //					string filePath = /*Server.MapPath(*/string.Concat(System.Configuration.ConfigurationManager.AppSettings["ScanPathMailing"],id,e.Item.Cells[(int)gridColumns.ext].Text);//);
                //			
                //					Response.Cache.SetExpires(DateTime.Now.AddSeconds(-1));
                //					Response.Cache.SetCacheability(HttpCacheability.Private);
                //					Response.AppendHeader("content-disposition", "attachment; filename="+id.ToString()+e.Item.Cells[(int)gridColumns.ext].Text);
                //					//Response.AppendHeader("content-disposition", "attachment; filename="+ID+e.Item.Cells[(int)gridColomns.Type].Text);
                //     
                //					Response.Charset = "";
                //					Response.TransmitFile( filePath );
                //					Response.End();
                //					
                //					break;

            }
        }
        private void grdMain_ItemDataBound(object sender, System.Web.UI.WebControls.DataGridItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Item
                || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                e.Item.Cells[(int)gridColomns.AssignedToText].Text = UIHelpers.GetHotIssueUserNameAssigned(UIHelpers.ToInt(e.Item.Cells[(int)gridColomns.HotIssueAssignedTo].Text), UIHelpers.ToInt(e.Item.Cells[(int)gridColomns.HotIssueAssignedToType].Text));
                System.Web.UI.WebControls.Image ImageDone = (System.Web.UI.WebControls.Image)e.Item.FindControl("imgDone");
                if (e.Item.Cells[(int)gridColomns.HotIssueStatusID].Text == System.Configuration.ConfigurationManager.AppSettings["HotIssueStatusFinished"])
                    ImageDone.ImageUrl = "images/done.jpg";
                else
                    ImageDone.ImageUrl = "";
            }

        }

        private void btnBackToProject_Click(object sender, System.EventArgs e)
        {
            Response.Redirect("EditProject.aspx" + "?pid=" + UIHelpers.GetPIDParam());
        }
        #endregion

        #region DateBase

        private void LoadProjects()
        {
            UIHelpers.LoadProjects(ddlProject, ddlProjectsStatus, ddlBuildingTypes);
        }


        private bool LoadHotIssuesObjects()
        {
            UIHelpers.LoadHotIssueCategories(ddlCategory);
            UIHelpers.LoadHotIssueStatuses(ddlStatus);

            return true;
        }

        private void BindGrid()
        {
            int nProjectID = UIHelpers.ToInt(ddlProject.SelectedValue);

            UIHelpers.LoadHotIssueCategories(grdTemi, nProjectID, (int)ColumnsTemi.Delete);
            string Builders, Subs, Team, Clients;
            UIHelpers.LoadItems(nProjectID, out Clients, out Team, out Subs, out Builders);

            if (nProjectID != 0)
            {
                string[] teamList = Team.TrimEnd(new char[] { ' ', ',' }).Split(',');
                int u = 0;
                foreach (string member in teamList)
                {
                    if (!string.IsNullOrEmpty(member))
                    {
                        plcTeam.Items.Add(new ListItem(member.Trim(), "cb_" + u.ToString()));
                        u++;
                    }
                }
                string[] buildList = Builders.TrimEnd(new char[] { ' ', ',' }).Split(',');
                foreach (string builder in buildList)
                {
                    if (!string.IsNullOrEmpty(builder))
                    {
                        plcClients.Items.Add(new ListItem(builder.Trim(), "cb_" + u.ToString()));
                        u++;
                    }
                }
                string[] clientList = Clients.TrimEnd(new char[] { ' ', ',' }).Split(',');
                foreach (string client in clientList)
                {
                    if (!string.IsNullOrEmpty(client))
                    {
                        plcBuilders.Items.Add(new ListItem(client.Trim(), "cb_" + u.ToString()));
                        u++;
                    }
                }
            }
            else
            {
                plcTeam.Items.Clear();
                plcClients.Items.Clear();
                plcBuilders.Items.Clear();
            }
            lbSubsData.Text = Subs.TrimEnd(new char[] { ' ', ',' });
            ibPdfExport.Visible = ibXlsExport.Visible = ibWordExport.Visible = nProjectID > 0;
            lbSubs.Visible = lbSubsData.Visible = nProjectID > 0;
            HotIssuesVector hiv = HotIssueDAL.LoadCollection(nProjectID, UIHelpers.ToInt(ddlCategory.SelectedValue),/*UIHelpers.ToInt(ddlPriority.SelectedValue),*/
            UIHelpers.ToInt(ddlStatus.SelectedValue), -1, true, UIHelpers.ToInt(ddlBuildingTypes.SelectedValue), ddlProjectsStatus.SelectedIndex, TimeHelper.GetDate(txtStartDate.Text), TimeHelper.GetDate(txtEndDate.Text), Page.Request.Params["ASA"] == "1", false, (string)ViewState["Sort"]);
            grdMain.DataSource = hiv;
            grdMain.DataKeyField = "HotIssueID";
            grdMain.DataBind();

            grdMain.Columns[(int)gridColomns.ProjectName].Visible = nProjectID <= 0;

            for (int i = 0; i < grdMain.Items.Count; i++)
            {
                if (!UIHelpers.CanEditIssue(hiv[i].CreatedDate))
                    grdMain.Items[i].Cells[(int)gridColomns.Delete].FindControl("btnDelete").Visible = false;
            }

            try
            {
                SetConfirmDelete(grdMain, "btnDelete", 1);
            }
            catch (Exception ex)
            {
                log.Error(ex.Message);
            }
            HotIssueInfo hi = new HotIssueInfo();

            hi.TypeID = int.Parse(ddlProjectsStatus.SelectedValue);
            hi.BuildingType = ddlBuildingTypes.SelectedValue;
            hi.ProjectID = int.Parse(ddlProject.SelectedValue);

            //hi.ThemeID = int.Parse(ddlCategory.SelectedValue);
            hi.StatusID = int.Parse(ddlStatus.SelectedValue);
            hi.StartDate = txtStartDate.Text;
            hi.EndDate = txtEndDate.Text;
            SessionManager.CurrentHotIssueInfo = hi;

        }

        #endregion

        #region Web Form Designer generated code
        override protected void OnInit(EventArgs e)
        {
            //
            // CODEGEN: This call is required by the ASP.NET Web Form Designer.
            //
            InitializeComponent();
            base.OnInit(e);
        }

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {

            this.ddlProjectsStatus.SelectedIndexChanged += new System.EventHandler(this.ddlProjectsStatus_SelectedIndexChanged);
            this.ddlBuildingTypes.SelectedIndexChanged += new System.EventHandler(this.ddlBuidingTypes_SelectedIndexChanged);
            this.ddlProject.SelectedIndexChanged += new System.EventHandler(this.ddlProject_SelectedIndexChanged);
            this.btnNew1.Click += new System.EventHandler(this.btnNew_Click);
            this.btnSearch.Click += new System.EventHandler(this.btnSearch_Click);
            this.ibPdfExport.Click += new System.Web.UI.ImageClickEventHandler(this.ibPdfExport_Click);
            this.ibXlsExport.Click += new System.Web.UI.ImageClickEventHandler(this.ibXlsExport_Click);
            this.ibWordExport.Click += new System.Web.UI.ImageClickEventHandler(this.ibWordExport_Click);
            this.grdMain.ItemCommand += new System.Web.UI.WebControls.DataGridCommandEventHandler(this.grdMain_ItemCommand);
            this.grdMain.SortCommand += new System.Web.UI.WebControls.DataGridSortCommandEventHandler(this.grdMain_SortCommand);
            this.grdMain.ItemDataBound += new System.Web.UI.WebControls.DataGridItemEventHandler(this.grdMain_ItemDataBound);
            this.btnNew.Click += new System.EventHandler(this.btnNew_Click);
            this.btnBackToProject.Click += new System.EventHandler(this.btnBackToProject_Click);
            this.grdTemi.ItemCommand += new System.Web.UI.WebControls.DataGridCommandEventHandler(this.grdTemi_ItemCommand);
            this.grdTemi.EditCommand += new System.Web.UI.WebControls.DataGridCommandEventHandler(this.grdTemi_EditCommand);
            this.grdTemi.DeleteCommand += new System.Web.UI.WebControls.DataGridCommandEventHandler(this.grdTemi_DeleteCommand);
            this.Load += new System.EventHandler(this.Page_Load);

        }
        #endregion

        private void ddlProject_SelectedIndexChanged(object sender, System.EventArgs e)
        {
            UIHelpers.LoadHotIssueCategories(ddlCategory, UIHelpers.ToInt(ddlProject.SelectedValue), UIHelpers.ToInt(ddlBuildingTypes.SelectedValue), UIHelpers.ToInt(ddlProjectsStatus.SelectedValue));
            BindGrid();
        }

        private void grdMain_SortCommand(object source, System.Web.UI.WebControls.DataGridSortCommandEventArgs e)
        {
            ViewState.Add("Sort", Sorting.SetSort(grdMain, (string)ViewState["Sort"], e.SortExpression));

            BindGrid();
        }

        private void ibPdfExport_Click(object sender, System.Web.UI.ImageClickEventArgs e)
        {
            ExportMinutes(false, false);
        }

        private void ibXlsExport_Click(object sender, System.Web.UI.ImageClickEventArgs e)
        {
            ExportMinutes(true, false);
        }


        private void ExportMinutes(bool bExcel, bool bWord)
        {

            //string PathOfPDF = string.Concat(System.Configuration.ConfigurationManager.AppSettings["MeetingHotIssuesFilesPath"],IDD,System.Configuration.ConfigurationManager.AppSettings["Extension"]);

            int nProjectID = UIHelpers.ToInt(ddlProject.SelectedValue);
            //Manual data entry for the current protocol DBUGG
            string[] DocumentData = new string[3];
            DocumentData[0] = txtID.Text;
            DocumentData[1] = txtDate.Text;
            DocumentData[2] = txtDocuments.Text;
            ////
            DataView dvProjects = UIHelpers.HotIssueCreateDVprojects(nProjectID, false);
            DataTable dtUsers = UIHelpers.HotIssueCreateDVusers(nProjectID);

            string selectedNames = "";

            foreach (ListItem c in plcTeam.Items)
            {
                if (((ListItem)c).Selected)
                {
                    selectedNames += c;
                }
            }
            foreach (ListItem c in plcBuilders.Items)
            {
                if (((ListItem)c).Selected)
                {
                    selectedNames += c;
                }
            }
            foreach (ListItem c in plcClients.Items)
            {
                if (((ListItem)c).Selected)
                {
                    selectedNames += c;
                }
            }
            DataTable tempUsers = dtUsers.Clone();

            string name = "";
            string from = "";
            foreach (DataRow r in dtUsers.Rows)
            {
                name = r.Field<string>("UserName");
                from = r.Field<string>("UserFrom");
                if (selectedNames.Contains(name) || selectedNames.Contains(from))
                    tempUsers.Rows.Add(r.ItemArray);
            }

            DataView dvUsers = new DataView(tempUsers);

            HotIssuesVector hiv = UIHelpers.HotIssueCreateHotIssuesVector(nProjectID, Page.Request.Params["ASA"] == "1", int.Parse(ddlCategory.SelectedValue), int.Parse(ddlStatus.SelectedValue), TimeHelper.GetDate(txtStartDate.Text), TimeHelper.GetDate(txtEndDate.Text), true, false);

            string meetingNumber = MeetingDAL.SelectMeetingNumber(nProjectID, Constants.DateMax);
            string projectName, projectCode, administrativeName, add;
            decimal area = 0;
            int clientID = 0;
            bool phases; bool isPUP;
            ProjectsData.SelectProject(nProjectID, out projectName, out  projectCode, out administrativeName, out area, out add,
                out clientID, out phases, out isPUP);
            string sFileName = DateTime.Now.ToString("yyMMdd") + "_" + projectCode + "_PCM" + meetingNumber;
            GrapeCity.ActiveReports.SectionReport report = new ReportMeetingHotIssuesAll(DateTime.Now.Date, "", "", "", "", dvProjects, dvUsers, hiv, "", true, "", "", nProjectID, LoggedUser.FullName, 0, false, DocumentData);
            report.PageSettings.Orientation = GrapeCity.ActiveReports.Document.Section.PageOrientation.Default;
            report.Run(false);
            report = new ReportMeetingHotIssuesAll(DateTime.Now.Date, "", "", "", "", dvProjects, dvUsers, hiv, "", true, "", "", nProjectID, LoggedUser.FullName, report.Document.Pages.Count, false, DocumentData);
            report.PageSettings.Orientation = GrapeCity.ActiveReports.Document.Section.PageOrientation.Default;
            report.Run();

            if (bExcel)
            {
                XlsExport xls = new XlsExport();
                System.IO.MemoryStream memoryFile = new System.IO.MemoryStream();

                xls.Export(report.Document, memoryFile);

                Response.Clear();
                Response.AppendHeader("content-disposition"
                    , "attachment; filename=" + sFileName + ".xls");
                Response.ContentType = "application/xls";

                memoryFile.WriteTo(Response.OutputStream);
                Response.End();
            }
            else if (bWord)
            {
                RtfExport rtf = new RtfExport();
                System.IO.MemoryStream memoryFile = new System.IO.MemoryStream();

                rtf.Export(report.Document, memoryFile);

                Response.Clear();
                Response.AppendHeader("content-disposition"
                    , "attachment; filename=" + sFileName + ".rtf");
                Response.ContentType = "application/rtf";

                memoryFile.WriteTo(Response.OutputStream);
                Response.End();
            }
            else
            {
                PdfExport pdf = new PdfExport();

                pdf.NeverEmbedFonts = "";
                System.IO.MemoryStream memoryFile = new System.IO.MemoryStream();

                pdf.Export(report.Document, memoryFile);

                Response.Clear();
                Response.AppendHeader("content-disposition"
                    , "attachment; filename=" + sFileName + ".pdf");
                Response.ContentType = "application/pdf";

                memoryFile.WriteTo(Response.OutputStream);
                Response.End();
            }

        }

        private void ibWordExport_Click(object sender, System.Web.UI.ImageClickEventArgs e)
        {
            ExportMinutes(false, true);
        }

        private void grdTemi_EditCommand(object source, System.Web.UI.WebControls.DataGridCommandEventArgs e)
        {
            string SID = ((DataGrid)source).DataKeys[e.Item.ItemIndex].ToString();

            Response.Redirect("EditCategory.aspx" + "?id=" + SID);
        }

        private void grdTemi_DeleteCommand(object source, System.Web.UI.WebControls.DataGridCommandEventArgs e)
        {
            int ID = (int)((DataGrid)source).DataKeys[e.Item.ItemIndex];
            HotIssueCategoryDAL.Delete(ID);
        }

        private void grdTemi_ItemCommand(object source, System.Web.UI.WebControls.DataGridCommandEventArgs e)
        {
            if (!(e.CommandSource is ImageButton))
                return;
            ImageButton ib = (ImageButton)e.CommandSource;
            string s = ib.ID;
            if (ib.ID == "btnDelete")
            {
                int ID = (int)((DataGrid)source).DataKeys[e.Item.ItemIndex];
                HotIssueCategoryDAL.Delete(ID);
                Response.Redirect("HotIssues.aspx");

            }
        }

        public ProjectsData.ProjectsByStatus ProjectsStatus
        {
            get
            {
                try
                {
                    return (ProjectsData.ProjectsByStatus)int.Parse(ddlProjectsStatus.SelectedValue);
                }
                catch { return ProjectsData.ProjectsByStatus.AllProjects; }
            }
        }
    }
}
