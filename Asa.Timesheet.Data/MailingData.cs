using System;
using System.Collections;
using System.Text;

namespace Asa.Timesheet.Data
{
	/// <summary>
	/// Summary description for MailingData.
	/// </summary>
	public class MailingData
	{
		private int _mailingID;
		private int _projectID = -1;
		private DateTime _mailingDate;
		private bool _send;
//		private int _mailingTypeID;
		private bool _hasScan;
		private int _mailingNumber;
		private string _notes = string.Empty;
		private string _projectName = string.Empty;
		//private string _mailingTypeName = string.Empty;
		private int _clientID = -1;
		private string _mailingTypes = string.Empty;
		private string _clientName = string.Empty;
		private string _ext = string.Empty;
		private string _person = string.Empty;
		/// <summary>
		/// Initialize an new empty Login object.
		/// </summary>
		public MailingData()
		{
		}
		
		/// <summary>
		/// Initialize a new Login object with the given parameters.
		/// </summary>
//		public MailingData(int mailingID, int projectID, DateTime mailingDate, bool send, int mailingTypeID, bool hasScan,int mailingNumber,string notes,int clientID)
//		{
//			this._mailingID = mailingID;
//			this._projectID = projectID;
//			this._mailingDate = mailingDate;
//			this._send = send;
//			this._mailingTypeID = mailingTypeID;
//			this._hasScan = hasScan;
//			this._mailingNumber = mailingNumber;
//			this._notes = notes;
//			this._clientID = clientID;
//		}	
		public MailingData(int mailingID, int projectID, DateTime mailingDate, bool send, bool hasScan,int mailingNumber,string notes,int clientID,string mailingTypes,string ext)
		{
			this._mailingID = mailingID;
			this._projectID = projectID;
			this._mailingDate = mailingDate;
			this._send = send;
			
			this._hasScan = hasScan;
			this._mailingNumber = mailingNumber;
			this._notes = notes;
			this._clientID = clientID;
			this._mailingTypes = mailingTypes;
			this._ext = ext;
		}	
		
	  
		public override string ToString()
		{
			StringBuilder builder = new StringBuilder();
			builder.Append("_mailingID: " + _mailingID.ToString()).Append("\r\n");
			builder.Append("_projectID: " + _projectID.ToString()).Append("\r\n");
			builder.Append("_mailingDate: " + _mailingDate.ToString()).Append("\r\n");
			builder.Append("_send: " + _send.ToString()).Append("\r\n");
			builder.Append("_mailingTypes: " + _mailingTypes.ToString()).Append("\r\n");
			builder.Append("_hasScan: " + _hasScan.ToString()).Append("\r\n");
			builder.Append("_mailingNumber: " + _mailingNumber.ToString()).Append("\r\n");
			builder.Append("_notes: " + _notes.ToString()).Append("\r\n");
			builder.Append("_clientID: " + _clientID.ToString()).Append("\r\n");
			builder.Append("_ext: " + _ext).Append("\r\n");
			builder.Append("\r\n");
			return builder.ToString();
		}	
		public object GetByIndex(int nIndex)
		{
			switch(nIndex)
			{
				case 0:
					return _mailingID;
				case 1:
					return _projectID;
				case 2:
					return _mailingDate;
				case 3:
					return _send;
				case 4:
					return _mailingTypes;
				case 5:
					return _hasScan;
				case 6:
					return _mailingNumber;
				case 7:
					return _notes;
				case 8:
					return _clientID;
				case 9:
					return _ext;
				case 10:
					return _projectName;
//				case 10:
//					return _mailingTypes;
//				case 11:
//					return _mailingTypeName;
				case 11:
					return _clientName;
			}
			return null;
		}
		#region Properties
	
		public int MailingID
		{
			get { return _mailingID; }
			set { _mailingID = value; }
		}
		
		public string Person
		{
			get { return _person; }
			set { _person = value; }
		}
		public DateTime MailingDate
		{
			get { return _mailingDate; }
			set { _mailingDate = value; }
		}
	
		public int ProjectID
		{
			get { return _projectID; }
			set { _projectID = value; }
		}
	
//		public int MailingTypeID
//		{
//			get { return _mailingTypeID; }
//			set { _mailingTypeID = value; }
//		}
	
		public bool Send
		{
			get { return _send; }
			set { _send = value; }
		}
		
		public bool HasScan
		{
			get { return _hasScan; }
			set { _hasScan = value; }
		}

		public int MailingNumber
		{
			get { return _mailingNumber; }
			set { _mailingNumber = value; }
		}

		public string Notes
		{
			get { return _notes; }
			set { _notes = value; }
		}

		public int ClientID
		{
			get { return _clientID; }
			set { _clientID = value; }
		}
		
		public string Ext
		{
			get { return _ext; }
			set { _ext = value; }
		}

		public string ProjectName
		{
			get { return _projectName; }
			set { _projectName = value; }
		}

//		public string MailingTypeName
//		{
//			get { return _mailingTypeName; }
//			set { _mailingTypeName = value; }
//		}

		public string ClientName
		{
			get { return _clientName; }
			set { _clientName = value; }
		}

		public string MailingTypes
		{
			get { return _mailingTypes; }
			set { _mailingTypes = value; }
		}

		#endregion
	}
}
