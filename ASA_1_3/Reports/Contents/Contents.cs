using System;
using System.Data;
using Asa.Timesheet.Data.Reports;
using Asa.Timesheet.Data;
using GrapeCity.ActiveReports;
using GrapeCity.ActiveReports.Controls;
using GrapeCity.ActiveReports.SectionReportModel;
using GrapeCity.ActiveReports.Document.Section;
using GrapeCity.ActiveReports.Document;

namespace Asa.Timesheet.WebPages.Reports
{
    public class Contents : GrapeCity.ActiveReports.SectionReport
	{
		//const int Constants.ArchitectConst = 8;
		//public const int Constants.ArchitectConstMaxPlus = 2;
		int _projectID = 0;
		int _nBuilding = 0;
		bool _isRtf = false;
		string _userName = String.Empty;
//		string _Sgn;
		ChecksVector _checks = null;
		string _phase = "";



        public Contents(int projectID, string userName, bool IsRtf, int nBuilding, ChecksVector checks, string phase)
        {
            _isRtf = IsRtf;
            _nBuilding = nBuilding;
            _projectID = projectID;
            _userName = userName;
            //			_Sgn = Sgn;
            _checks = checks;
            _phase = phase;


            InitializeComponent();
            this.ReportEnd += new EventHandler(Contents_ReportEnd);
        }

        private void Contents_ReportStart(object sender, System.EventArgs eArgs)
        {
            DataSet dsReport = ContentsReportData.SelectContentsReportData(_projectID, _nBuilding);
            if (_phase != "")
            {
                for (int i = 0; i < dsReport.Tables[1].Rows.Count; i++)
                {
                    string code = dsReport.Tables[1].Rows[i][10].ToString();
                    if (code.Length != 0)
                    {
                        code = string.Concat(_phase, ".", code);
                        dsReport.Tables[1].Rows[i][10] = code;
                    }
                }
                for (int i = 0; i < dsReport.Tables[2].Rows.Count; i++)
                {
                    string code = dsReport.Tables[2].Rows[i][10].ToString();
                    if (code.Length != 0)
                    {
                        code = string.Concat(_phase, ".", code);
                        dsReport.Tables[2].Rows[i][10] = code;
                    }
                }
                txtPhase.Text = TextPhase(_phase);
            }
            else
            {
                TextBox10.Visible = txtPhase.Visible = false;

            }
            this.DataSource = dsReport.Tables[0];
            //Check if there is code
            bool IsCode = false;
            bool IsOneBuilding = IsMaxBuildingOne(_projectID);
            int CountofContents = dsReport.Tables[1].Rows.Count + dsReport.Tables[2].Rows.Count;
            foreach (DataRow dr in dsReport.Tables[1].Rows)
            {
                if (dr[10].ToString() != "")
                {
                    int _cid = int.Parse(dr[0].ToString());
                    int index = _checks.IndexOf(_cid, true);
                    if (index == -1)
                    { IsCode = true; break; }
                    bool ischecked = _checks[index].IsChecked;
                    if (ischecked)
                    { IsCode = true; break; }
                }
            }
            string sign = "";
            if (dsReport.Tables[1].Rows.Count > 0)
            {
                if (dsReport.Tables[1].Rows[0]["Signature"] != DBNull.Value)
                    sign = (string)dsReport.Tables[1].Rows[0]["Signature"];
                this.SubReport1.Report = new ContentsType1Subreport(IsCode, _checks, IsOneBuilding, CountofContents);
                this.SubReport1.Report.DataSource = dsReport.Tables[1];
            }
            else this.SubReport1.Visible = false;
            IsCode = false;
            foreach (DataRow dr in dsReport.Tables[2].Rows)
            {
                if (dr[10].ToString() != "")
                {
                    int _cid = int.Parse(dr[0].ToString());
                    int index = _checks.IndexOf(_cid, true);
                    if (index == -1)
                    { IsCode = true; break; }
                    bool ischecked = _checks[index].IsChecked;
                    if (ischecked)
                    { IsCode = true; break; }
                }
            }
            if (dsReport.Tables[2].Rows.Count > 0)
            {
                //				if(dsReport.Tables[2].Rows[0]["Signature"]!=DBNull.Value)
                //					sign=(string)dsReport.Tables[2].Rows[0]["Signature"];
                this.SubReport2.Report = new ContentsType2Subreport(IsCode, _checks, IsOneBuilding);
                this.SubReport2.Report.DataSource = dsReport.Tables[2];
            }
            else this.SubReport2.Visible = false;
            //			if(sign!="")
            //				lbSub.Text=sign;
            //			else
            lbSub.Visible = lbSubLabel.Visible = false;



        }

        private void Contents_FetchData(object sender, FetchEventArgs eArgs)
        {
            if (eArgs.EOF) return;


        }

        private void ReportFooter_Format(object sender, System.EventArgs eArgs)
        {
            this.txtCreatedBy.Text = _userName;
        }

		#region ActiveReports Designer generated code
























        public void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Contents));
            this.Detail = new GrapeCity.ActiveReports.SectionReportModel.Detail();
            this.ReportHeader = new GrapeCity.ActiveReports.SectionReportModel.ReportHeader();
            this.ReportFooter = new GrapeCity.ActiveReports.SectionReportModel.ReportFooter();
            this.PageHeader = new GrapeCity.ActiveReports.SectionReportModel.PageHeader();
            this.PageFooter = new GrapeCity.ActiveReports.SectionReportModel.PageFooter();
            this.GroupHeader1 = new GrapeCity.ActiveReports.SectionReportModel.GroupHeader();
            this.GroupFooter1 = new GrapeCity.ActiveReports.SectionReportModel.GroupFooter();
            this.txtReportTitle = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCodeOfProject = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.SubReport1 = new GrapeCity.ActiveReports.SectionReportModel.SubReport();
            this.SubReport2 = new GrapeCity.ActiveReports.SectionReportModel.SubReport();
            this.lbSubLabel = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.lbSub = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.TextBox1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.TextBox2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.TextBox3 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.TextBox10 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtPhase = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.TextBox6 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.TextBox7 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.TextBox8 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.TextBox9 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.Label1 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.txtCreatedBy = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            ((System.ComponentModel.ISupportInitialize)(this.txtReportTitle)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCodeOfProject)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbSubLabel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbSub)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TextBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TextBox2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TextBox3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TextBox10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPhase)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TextBox6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TextBox7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TextBox8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TextBox9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCreatedBy)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            // 
            // Detail
            // 
            this.Detail.ColumnSpacing = 0F;
            this.Detail.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
						this.SubReport1,
						this.SubReport2,
						this.lbSubLabel,
						this.lbSub,
						this.TextBox1,
						this.TextBox2,
						this.TextBox3,
						this.TextBox10,
						this.txtPhase,
						this.TextBox6,
						this.TextBox7,
						this.TextBox8,
						this.TextBox9});
            this.Detail.Height = 2.260417F;
            this.Detail.Name = "Detail";
            // 
            // ReportHeader
            // 
            this.ReportHeader.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
						this.txtReportTitle,
						this.txtCodeOfProject});
            this.ReportHeader.Height = 0.6131945F;
            this.ReportHeader.Name = "ReportHeader";
            // 
            // ReportFooter
            // 
            this.ReportFooter.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
						this.Label1,
						this.txtCreatedBy});
            this.ReportFooter.Height = 0.9375F;
            this.ReportFooter.KeepTogether = true;
            this.ReportFooter.Name = "ReportFooter";
            // 
            // PageHeader
            // 
            this.PageHeader.Height = 0F;
            this.PageHeader.Name = "PageHeader";
            // 
            // PageFooter
            // 
            this.PageFooter.Height = 0.04166667F;
            this.PageFooter.Name = "PageFooter";
            // 
            // GroupHeader1
            // 
            this.GroupHeader1.DataField = "BuildingNumber";
            this.GroupHeader1.Height = 0F;
            this.GroupHeader1.Name = "GroupHeader1";
            // 
            // GroupFooter1
            // 
            this.GroupFooter1.Height = 0.25F;
            this.GroupFooter1.Name = "GroupFooter1";
            // 
            // txtReportTitle
            // 
            this.txtReportTitle.Height = 0.276F;
            this.txtReportTitle.Left = 0.063F;
            this.txtReportTitle.Name = "txtReportTitle";
            this.txtReportTitle.Style = "font-size: 15pt; font-weight: bold; text-align: center";
            this.txtReportTitle.Text = "??????????";
            this.txtReportTitle.Top = 0.276F;
            this.txtReportTitle.Width = 6.0625F;
            // 
            // txtCodeOfProject
            // 
            this.txtCodeOfProject.DataField = "ProjectCode";
            this.txtCodeOfProject.Height = 0.276F;
            this.txtCodeOfProject.Left = 0.0625F;
            this.txtCodeOfProject.Name = "txtCodeOfProject";
            this.txtCodeOfProject.Style = "font-size: 15pt; font-weight: bold; text-align: center; ddo-char-set: 1";
            this.txtCodeOfProject.Top = 0F;
            this.txtCodeOfProject.Width = 6.063001F;
            // 
            // SubReport1
            // 
            this.SubReport1.CloseBorder = false;
            this.SubReport1.Height = 0.1968504F;
            this.SubReport1.Left = 0.5625F;
            this.SubReport1.Name = "SubReport1";
            this.SubReport1.Report = null;
            this.SubReport1.Top = 1.5625F;
            this.SubReport1.Width = 6.875F;
            // 
            // SubReport2
            // 
            this.SubReport2.CloseBorder = false;
            this.SubReport2.Height = 0.1968504F;
            this.SubReport2.Left = 0.5625F;
            this.SubReport2.Name = "SubReport2";
            this.SubReport2.Report = null;
            this.SubReport2.Top = 1.9375F;
            this.SubReport2.Width = 6.875F;
            // 
            // lbSubLabel
            // 
            this.lbSubLabel.Height = 0.1875F;
            this.lbSubLabel.Left = 0.5625F;
            this.lbSubLabel.Name = "lbSubLabel";
            this.lbSubLabel.Style = "font-size: 11pt";
            this.lbSubLabel.Text = "????????:";
            this.lbSubLabel.Top = 1.3125F;
            this.lbSubLabel.Width = 0.9842521F;
            // 
            // lbSub
            // 
            this.lbSub.Height = 0.2F;
            this.lbSub.Left = 1.57F;
            this.lbSub.Name = "lbSub";
            this.lbSub.Style = "font-size: 11pt; vertical-align: bottom";
            this.lbSub.Text = "????????:";
            this.lbSub.Top = 1.2975F;
            this.lbSub.Width = 4.555F;
            // 
            // TextBox1
            // 
            this.TextBox1.Height = 0.2362205F;
            this.TextBox1.Left = 0.5905511F;
            this.TextBox1.Name = "TextBox1";
            this.TextBox1.Style = "font-size: 12pt";
            this.TextBox1.Text = "?????:";
            this.TextBox1.Top = 0F;
            this.TextBox1.Width = 0.9842521F;
            // 
            // TextBox2
            // 
            this.TextBox2.DataField = "AdministrativeName";
            this.TextBox2.Height = 0.2362205F;
            this.TextBox2.Left = 1.574803F;
            this.TextBox2.Name = "TextBox2";
            this.TextBox2.Style = "font-size: 11pt; font-weight: bold";
            this.TextBox2.Text = "TextBox2";
            this.TextBox2.Top = 0F;
            this.TextBox2.Width = 4.550197F;
            // 
            // TextBox3
            // 
            this.TextBox3.DataField = "AddInfo";
            this.TextBox3.Height = 0.2F;
            this.TextBox3.Left = 1.574803F;
            this.TextBox3.Name = "TextBox3";
            this.TextBox3.Style = "font-size: 11pt; font-weight: normal";
            this.TextBox3.Text = "TextBox3";
            this.TextBox3.Top = 0.265F;
            this.TextBox3.Width = 4.550197F;
            // 
            // TextBox10
            // 
            this.TextBox10.Height = 0.188F;
            this.TextBox10.Left = 0.59F;
            this.TextBox10.Name = "TextBox10";
            this.TextBox10.Style = "font-size: 11pt";
            this.TextBox10.Text = "????:";
            this.TextBox10.Top = 0.538F;
            this.TextBox10.Width = 1F;
            // 
            // txtPhase
            // 
            this.txtPhase.Height = 0.2F;
            this.txtPhase.Left = 1.591F;
            this.txtPhase.Name = "txtPhase";
            this.txtPhase.Style = "font-size: 11pt; vertical-align: bottom";
            this.txtPhase.Top = 0.53F;
            this.txtPhase.Width = 4.55F;
            // 
            // TextBox6
            // 
            this.TextBox6.Height = 0.188F;
            this.TextBox6.Left = 0.591F;
            this.TextBox6.Name = "TextBox6";
            this.TextBox6.Style = "font-size: 11pt";
            this.TextBox6.Text = "??.?????????:";
            this.TextBox6.Top = 0.798F;
            this.TextBox6.Width = 1F;
            // 
            // TextBox7
            // 
            this.TextBox7.Height = 0.2F;
            this.TextBox7.Left = 1.591F;
            this.TextBox7.Name = "TextBox7";
            this.TextBox7.Style = "font-size: 11pt; vertical-align: bottom";
            this.TextBox7.Text = "�?????? ????????? ????????? ???�";
            this.TextBox7.Top = 0.79F;
            this.TextBox7.Width = 4.55F;
            // 
            // TextBox8
            // 
            this.TextBox8.Height = 0.188F;
            this.TextBox8.Left = 0.591F;
            this.TextBox8.Name = "TextBox8";
            this.TextBox8.Style = "font-size: 11pt";
            this.TextBox8.Text = "??????????:";
            this.TextBox8.Top = 1.051F;
            this.TextBox8.Width = 1F;
            // 
            // TextBox9
            // 
            this.TextBox9.DataField = "Investor";
            this.TextBox9.Height = 0.2F;
            this.TextBox9.Left = 1.591F;
            this.TextBox9.Name = "TextBox9";
            this.TextBox9.Style = "font-size: 11pt; vertical-align: bottom";
            this.TextBox9.Top = 1.04F;
            this.TextBox9.Width = 4.55F;
            // 
            // Label1
            // 
            this.Label1.Height = 0.274F;
            this.Label1.HyperLink = null;
            this.Label1.Left = 0.35F;
            this.Label1.Name = "Label1";
            this.Label1.Style = "font-size: 11pt; text-align: left";
            this.Label1.Text = "????????: ......................................";
            this.Label1.Top = 0.375F;
            this.Label1.Width = 5.8F;
            // 
            // txtCreatedBy
            // 
            this.txtCreatedBy.Height = 0.2F;
            this.txtCreatedBy.Left = 0.5625F;
            this.txtCreatedBy.Name = "txtCreatedBy";
            this.txtCreatedBy.Style = "font-size: 10pt; font-weight: normal";
            this.txtCreatedBy.Text = "txtCreatedBy";
            this.txtCreatedBy.Top = 0.6456687F;
            this.txtCreatedBy.Width = 5.75F;
            // 
            // ActiveReport1
            // 
            this.MasterReport = false;
            this.PageSettings.PaperHeight = 11F;
            this.PageSettings.PaperWidth = 8.5F;
            this.PrintWidth = 7.604167F;
            this.Sections.Add(this.ReportHeader);
            this.Sections.Add(this.PageHeader);
            this.Sections.Add(this.GroupHeader1);
            this.Sections.Add(this.Detail);
            this.Sections.Add(this.GroupFooter1);
            this.Sections.Add(this.PageFooter);
            this.Sections.Add(this.ReportFooter);
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule(resources.GetString("$this.StyleSheet"), "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: inherit; font-style: inherit; font-variant: inherit; font-weight: bo" +
                        "ld; font-size: 16pt; font-size-adjust: inherit; font-stretch: inherit", "Heading1", "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: Times New Roman; font-style: italic; font-variant: inherit; font-wei" +
                        "ght: bold; font-size: 14pt; font-size-adjust: inherit; font-stretch: inherit", "Heading2", "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: inherit; font-style: inherit; font-variant: inherit; font-weight: bo" +
                        "ld; font-size: 13pt; font-size-adjust: inherit; font-stretch: inherit", "Heading3", "Normal"));
            ((System.ComponentModel.ISupportInitialize)(this.txtReportTitle)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCodeOfProject)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbSubLabel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbSub)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TextBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TextBox2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TextBox3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TextBox10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPhase)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TextBox6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TextBox7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TextBox8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TextBox9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCreatedBy)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();

            // Attach Report Events
            this.ReportStart += new System.EventHandler(this.Contents_ReportStart);
            this.FetchData += new GrapeCity.ActiveReports.SectionReport.FetchEventHandler(this.Contents_FetchData);
            this.ReportFooter.Format += new System.EventHandler(this.ReportFooter_Format);
        }

		#endregion

        private void Contents_ReportEnd(object sender, EventArgs e)
        {
            if (_isRtf)
                return;
            Logo logo = new Logo();
            logo.Run();

            for (int i = 0; i < this.Document.Pages.Count; i++)
                this.Document.Pages[i].Overlay(logo.Document.Pages[0]);
        }
        private bool IsMaxBuildingOne(int projectID)
        {
            ContentsVector cv1 = ContentDAL.LoadCollection("ContentsList0Proc", SQLParms.CreateContentsList0Proc(projectID));
            ContentsVector[] cv2 = new ContentsVector[Constants.ArchitectConst + Constants.ArchitectConstMaxPlus + 1];
            for (int j = 1; j <= Constants.ArchitectConst; j++)
            {
                ContentsVector cvFilter = ContentDAL.LoadCollection("ContentsListArchitectProc", SQLParms.CreateContentsListFilterProc(projectID, j));
                cv2[j - 1] = cvFilter;
            }
            ContentsVector cv3 = ContentDAL.LoadCollection("ContentsList8Proc", SQLParms.CreateContentsList0Proc(projectID));
            int nMax = cv1.GetMaxBuilding();
            for (int j = 1; j <= Constants.ArchitectConst; j++)
            {
                if (cv2[j - 1].GetMaxBuilding() > nMax)
                    nMax = cv2[j - 1].GetMaxBuilding();
            }
            if (cv3.GetMaxBuilding() > nMax)
                nMax = cv3.GetMaxBuilding();
            if (nMax < 2) return true;
            else return false;
        }
        private string TextPhase(string simbol)
        {
            switch (simbol)
            {
                case "A": return Resource.ResourceManager["Reports_TehnicheskiProekt"];
                case "P": return Resource.ResourceManager["Reports_IdeenProekt"];
                case "C": return Resource.ResourceManager["ItemWorkProject"];
                default: return Resource.ResourceManager["Reports_IdeenProekt"];
            }
        }

        private ReportHeader ReportHeader;
        private TextBox txtReportTitle;
        private TextBox txtCodeOfProject;
        private PageHeader PageHeader;
        private GroupHeader GroupHeader1;
        private Detail Detail;
        private SubReport SubReport1;
        private SubReport SubReport2;
        private TextBox lbSubLabel;
        private TextBox lbSub;
        private TextBox TextBox1;
        private TextBox TextBox2;
        private TextBox TextBox3;
        private TextBox TextBox10;
        private TextBox txtPhase;
        private TextBox TextBox6;
        private TextBox TextBox7;
        private TextBox TextBox8;
        private TextBox TextBox9;
        private GroupFooter GroupFooter1;
        private PageFooter PageFooter;
        private ReportFooter ReportFooter;
        private Label Label1;
        private TextBox txtCreatedBy;
	}
}
