using System;
using System.Data;
using System.Collections;
using Asa.Timesheet.Data;
using GrapeCity.ActiveReports;
using GrapeCity.ActiveReports.Controls;
using GrapeCity.ActiveReports.SectionReportModel;
using GrapeCity.ActiveReports.Document.Section;
using GrapeCity.ActiveReports.Document;

namespace Asa.Timesheet.WebPages.Reports
{
    public class SalaryRpt : GrapeCity.ActiveReports.SectionReport
	{
		private const string _styleDivision = " font-weight: bold; background-color: yellow; color: Black;  font-size: 9pt;";
		private const string _styleDivisionDefault = "background-color: white; color: Black;  font-size: 9pt;";

        public SalaryRpt(DataView dv)
        {
            InitializeComponent();
            this.DataSource = dv;
            this.lbSalaryTitle.Text = string.Concat(dv.Table.TableName);
            this.lbSalaryTitle.Style = _styleDivisionDefault;
            if (!this.lbSalaryTitle.Text.StartsWith(Resource.ResourceManager["SalaryText"]))
            {
                if (this.lbSalaryTitle.Text != Resource.ResourceManager["reports_Total"])
                    this.lbSalaryTitle.Text = string.Concat(Resource.ResourceManager["reports_Total"], " ", this.lbSalaryTitle.Text);
                this.lbSalaryTitle.Style = _styleDivision;
            }
            TakeBorder(lbSalaryTitle.Border);
            TakeBorder(this.Label15.Border);
            TakeBorder(this.Label16.Border);
            TakeBorder(this.Label17.Border);
            TakeBorder(this.Label18.Border);
            TakeBorder(this.Label19.Border);
            TakeBorder(this.Label20.Border);
            TakeBorder(this.Label21.Border);
            TakeBorder(this.Label22.Border);
            TakeBorder(this.Label23.Border);
            TakeBorder(this.Label24.Border);
            TakeBorder(this.Label25.Border);
            TakeBorder(this.Label26.Border);
            TakeBorder(this.Label27.Border);
            TakeBorder(this.Label28.Border);
        }



        private void TakeBorder(GrapeCity.ActiveReports.Border b)
        {
            b.BottomColor = System.Drawing.Color.Black;
            b.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            b.LeftColor = System.Drawing.Color.Black;
            b.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            b.RightColor = System.Drawing.Color.Black;
            b.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            b.TopColor = System.Drawing.Color.Black;
            b.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
        }



        private void Detail_Format(object sender, System.EventArgs eArgs)
        {
            TakeBorder(this.txtYear.Border);
            TakeBorder(this.txtMall.Border);
            TakeBorder(this.txtM1.Border);
            TakeBorder(this.txtM2.Border);
            TakeBorder(this.txtM3.Border);
            TakeBorder(this.txtM4.Border);
            TakeBorder(this.txtM5.Border);
            TakeBorder(this.txtM6.Border);
            TakeBorder(this.txtM7.Border);
            TakeBorder(this.txtM8.Border);
            TakeBorder(this.txtM9.Border);
            TakeBorder(this.txtM10.Border);
            TakeBorder(this.txtM11.Border);
            TakeBorder(this.txtM12.Border);
        }

		#region ActiveReports Designer generated code


































        public void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(SalaryRpt));
            this.Detail = new GrapeCity.ActiveReports.SectionReportModel.Detail();
            this.ReportHeader = new GrapeCity.ActiveReports.SectionReportModel.ReportHeader();
            this.ReportFooter = new GrapeCity.ActiveReports.SectionReportModel.ReportFooter();
            this.PageHeader = new GrapeCity.ActiveReports.SectionReportModel.PageHeader();
            this.PageFooter = new GrapeCity.ActiveReports.SectionReportModel.PageFooter();
            this.lbSalaryTitle = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label15 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label16 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label17 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label18 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label19 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label20 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label21 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label22 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label23 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label24 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label25 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label26 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label27 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label28 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.txtM4 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtM6 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtM9 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtM10 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtM11 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtM12 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtMall = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtYear = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtM1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtM2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtM3 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtM5 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtM7 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtM8 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            ((System.ComponentModel.ISupportInitialize)(this.lbSalaryTitle)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label15)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label16)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label17)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label18)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label19)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label20)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label21)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label22)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label23)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label24)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label25)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label26)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label27)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label28)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtM4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtM6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtM9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtM10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtM11)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtM12)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtMall)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtYear)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtM1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtM2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtM3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtM5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtM7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtM8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            // 
            // Detail
            // 
            this.Detail.ColumnSpacing = 0F;
            this.Detail.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
						this.txtM4,
						this.txtM6,
						this.txtM9,
						this.txtM10,
						this.txtM11,
						this.txtM12,
						this.txtMall,
						this.txtYear,
						this.txtM1,
						this.txtM2,
						this.txtM3,
						this.txtM5,
						this.txtM7,
						this.txtM8});
            this.Detail.Height = 0.2F;
            this.Detail.KeepTogether = true;
            this.Detail.Name = "Detail";
            // 
            // ReportHeader
            // 
            this.ReportHeader.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
						this.lbSalaryTitle,
						this.Label15,
						this.Label16,
						this.Label17,
						this.Label18,
						this.Label19,
						this.Label20,
						this.Label21,
						this.Label22,
						this.Label23,
						this.Label24,
						this.Label25,
						this.Label26,
						this.Label27,
						this.Label28});
            this.ReportHeader.Height = 0.4493056F;
            this.ReportHeader.Name = "ReportHeader";
            // 
            // ReportFooter
            // 
            this.ReportFooter.Height = 0F;
            this.ReportFooter.Name = "ReportFooter";
            // 
            // PageHeader
            // 
            this.PageHeader.Height = 0F;
            this.PageHeader.Name = "PageHeader";
            // 
            // PageFooter
            // 
            this.PageFooter.Height = 0F;
            this.PageFooter.Name = "PageFooter";
            // 
            // lbSalaryTitle
            // 
            this.lbSalaryTitle.Height = 0.2F;
            this.lbSalaryTitle.HyperLink = null;
            this.lbSalaryTitle.Left = 0F;
            this.lbSalaryTitle.Name = "lbSalaryTitle";
            this.lbSalaryTitle.Style = "";
            this.lbSalaryTitle.Text = "??????? ??";
            this.lbSalaryTitle.Top = 0F;
            this.lbSalaryTitle.Width = 8.01F;
            // 
            // Label15
            // 
            this.Label15.Height = 0.2F;
            this.Label15.HyperLink = null;
            this.Label15.Left = 0F;
            this.Label15.Name = "Label15";
            this.Label15.Style = "font-size: 9pt; font-weight: bold; text-align: center";
            this.Label15.Text = "??????";
            this.Label15.Top = 0.25F;
            this.Label15.Width = 1.1F;
            // 
            // Label16
            // 
            this.Label16.Height = 0.2F;
            this.Label16.HyperLink = null;
            this.Label16.Left = 1.1F;
            this.Label16.Name = "Label16";
            this.Label16.Style = "font-size: 9pt; font-weight: bold; text-align: center; ddo-char-set: 1";
            this.Label16.Text = "???";
            this.Label16.Top = 0.25F;
            this.Label16.Width = 0.5F;
            // 
            // Label17
            // 
            this.Label17.Height = 0.2F;
            this.Label17.HyperLink = null;
            this.Label17.Left = 1.6F;
            this.Label17.Name = "Label17";
            this.Label17.Style = "font-size: 9pt; font-weight: bold; text-align: center; ddo-char-set: 1";
            this.Label17.Text = "???";
            this.Label17.Top = 0.25F;
            this.Label17.Width = 0.5F;
            // 
            // Label18
            // 
            this.Label18.Height = 0.2F;
            this.Label18.HyperLink = null;
            this.Label18.Left = 2.1F;
            this.Label18.Name = "Label18";
            this.Label18.Style = "font-size: 9pt; font-weight: bold; text-align: center; ddo-char-set: 1";
            this.Label18.Text = "????";
            this.Label18.Top = 0.25F;
            this.Label18.Width = 0.5F;
            // 
            // Label19
            // 
            this.Label19.Height = 0.2F;
            this.Label19.HyperLink = null;
            this.Label19.Left = 2.6F;
            this.Label19.Name = "Label19";
            this.Label19.Style = "font-size: 9pt; font-weight: bold; text-align: center; ddo-char-set: 1";
            this.Label19.Text = "???";
            this.Label19.Top = 0.25F;
            this.Label19.Width = 0.5F;
            // 
            // Label20
            // 
            this.Label20.Height = 0.2F;
            this.Label20.HyperLink = null;
            this.Label20.Left = 3.1F;
            this.Label20.Name = "Label20";
            this.Label20.Style = "font-size: 9pt; font-weight: bold; text-align: center; ddo-char-set: 1";
            this.Label20.Text = "???";
            this.Label20.Top = 0.25F;
            this.Label20.Width = 0.5F;
            // 
            // Label21
            // 
            this.Label21.Height = 0.2F;
            this.Label21.HyperLink = null;
            this.Label21.Left = 3.6F;
            this.Label21.Name = "Label21";
            this.Label21.Style = "font-size: 9pt; font-weight: bold; text-align: center; ddo-char-set: 1";
            this.Label21.Text = "???";
            this.Label21.Top = 0.25F;
            this.Label21.Width = 0.5F;
            // 
            // Label22
            // 
            this.Label22.Height = 0.2F;
            this.Label22.HyperLink = null;
            this.Label22.Left = 4.1F;
            this.Label22.Name = "Label22";
            this.Label22.Style = "font-size: 9pt; font-weight: bold; text-align: center; ddo-char-set: 1";
            this.Label22.Text = "???";
            this.Label22.Top = 0.25F;
            this.Label22.Width = 0.5F;
            // 
            // Label23
            // 
            this.Label23.Height = 0.2F;
            this.Label23.HyperLink = null;
            this.Label23.Left = 4.6F;
            this.Label23.Name = "Label23";
            this.Label23.Style = "font-size: 9pt; font-weight: bold; text-align: center; ddo-char-set: 1";
            this.Label23.Text = "???";
            this.Label23.Top = 0.25F;
            this.Label23.Width = 0.5F;
            // 
            // Label24
            // 
            this.Label24.Height = 0.2F;
            this.Label24.HyperLink = null;
            this.Label24.Left = 5.1F;
            this.Label24.Name = "Label24";
            this.Label24.Style = "font-size: 9pt; font-weight: bold; text-align: center; ddo-char-set: 1";
            this.Label24.Text = "????";
            this.Label24.Top = 0.25F;
            this.Label24.Width = 0.5F;
            // 
            // Label25
            // 
            this.Label25.Height = 0.2F;
            this.Label25.HyperLink = null;
            this.Label25.Left = 5.6F;
            this.Label25.Name = "Label25";
            this.Label25.Style = "font-size: 9pt; font-weight: bold; text-align: center; ddo-char-set: 1";
            this.Label25.Text = "???";
            this.Label25.Top = 0.25F;
            this.Label25.Width = 0.5F;
            // 
            // Label26
            // 
            this.Label26.Height = 0.2F;
            this.Label26.HyperLink = null;
            this.Label26.Left = 6.1F;
            this.Label26.Name = "Label26";
            this.Label26.Style = "font-size: 9pt; font-weight: bold; text-align: center; ddo-char-set: 1";
            this.Label26.Text = "???";
            this.Label26.Top = 0.25F;
            this.Label26.Width = 0.5F;
            // 
            // Label27
            // 
            this.Label27.Height = 0.2F;
            this.Label27.HyperLink = null;
            this.Label27.Left = 6.6F;
            this.Label27.Name = "Label27";
            this.Label27.Style = "font-size: 9pt; font-weight: bold; text-align: center; ddo-char-set: 1";
            this.Label27.Text = "???";
            this.Label27.Top = 0.25F;
            this.Label27.Width = 0.5F;
            // 
            // Label28
            // 
            this.Label28.Height = 0.2F;
            this.Label28.HyperLink = null;
            this.Label28.Left = 7.1F;
            this.Label28.Name = "Label28";
            this.Label28.Style = "font-size: 9pt; font-weight: bold; text-align: center; ddo-char-set: 1";
            this.Label28.Text = "??????/????";
            this.Label28.Top = 0.25F;
            this.Label28.Width = 0.9F;
            // 
            // txtM4
            // 
            this.txtM4.CanGrow = false;
            this.txtM4.DataField = "4";
            this.txtM4.Height = 0.2F;
            this.txtM4.Left = 2.6F;
            this.txtM4.Name = "txtM4";
            this.txtM4.OutputFormat = resources.GetString("txtM4.OutputFormat");
            this.txtM4.Style = "font-size: 8pt; text-align: right";
            this.txtM4.Top = 0F;
            this.txtM4.Width = 0.5F;
            // 
            // txtM6
            // 
            this.txtM6.CanGrow = false;
            this.txtM6.DataField = "6";
            this.txtM6.Height = 0.2F;
            this.txtM6.Left = 3.6F;
            this.txtM6.Name = "txtM6";
            this.txtM6.OutputFormat = resources.GetString("txtM6.OutputFormat");
            this.txtM6.Style = "font-size: 8pt; text-align: right";
            this.txtM6.Top = 0F;
            this.txtM6.Width = 0.5F;
            // 
            // txtM9
            // 
            this.txtM9.CanGrow = false;
            this.txtM9.DataField = "9";
            this.txtM9.Height = 0.2F;
            this.txtM9.Left = 5.1F;
            this.txtM9.Name = "txtM9";
            this.txtM9.OutputFormat = resources.GetString("txtM9.OutputFormat");
            this.txtM9.Style = "font-size: 8pt; text-align: right";
            this.txtM9.Top = 0F;
            this.txtM9.Width = 0.5F;
            // 
            // txtM10
            // 
            this.txtM10.CanGrow = false;
            this.txtM10.DataField = "10";
            this.txtM10.Height = 0.2F;
            this.txtM10.Left = 5.6F;
            this.txtM10.Name = "txtM10";
            this.txtM10.OutputFormat = resources.GetString("txtM10.OutputFormat");
            this.txtM10.Style = "font-size: 8pt; text-align: right";
            this.txtM10.Top = 0F;
            this.txtM10.Width = 0.5F;
            // 
            // txtM11
            // 
            this.txtM11.CanGrow = false;
            this.txtM11.DataField = "11";
            this.txtM11.Height = 0.2F;
            this.txtM11.Left = 6.1F;
            this.txtM11.Name = "txtM11";
            this.txtM11.OutputFormat = resources.GetString("txtM11.OutputFormat");
            this.txtM11.Style = "font-size: 8pt; text-align: right";
            this.txtM11.Top = 0F;
            this.txtM11.Width = 0.5F;
            // 
            // txtM12
            // 
            this.txtM12.CanGrow = false;
            this.txtM12.DataField = "12";
            this.txtM12.Height = 0.2F;
            this.txtM12.Left = 6.6F;
            this.txtM12.Name = "txtM12";
            this.txtM12.OutputFormat = resources.GetString("txtM12.OutputFormat");
            this.txtM12.Style = "font-size: 8pt; text-align: right";
            this.txtM12.Top = 0F;
            this.txtM12.Width = 0.5F;
            // 
            // txtMall
            // 
            this.txtMall.CanGrow = false;
            this.txtMall.DataField = "13";
            this.txtMall.Height = 0.2F;
            this.txtMall.Left = 7.1F;
            this.txtMall.Name = "txtMall";
            this.txtMall.OutputFormat = resources.GetString("txtMall.OutputFormat");
            this.txtMall.Style = "font-size: 8pt; text-align: right";
            this.txtMall.Top = 0F;
            this.txtMall.Width = 0.9F;
            // 
            // txtYear
            // 
            this.txtYear.CanGrow = false;
            this.txtYear.DataField = "0";
            this.txtYear.Height = 0.2F;
            this.txtYear.Left = 0F;
            this.txtYear.Name = "txtYear";
            this.txtYear.OutputFormat = resources.GetString("txtYear.OutputFormat");
            this.txtYear.Style = "font-size: 8pt; text-align: left";
            this.txtYear.Top = 0F;
            this.txtYear.Width = 1.1F;
            // 
            // txtM1
            // 
            this.txtM1.CanGrow = false;
            this.txtM1.DataField = "1";
            this.txtM1.Height = 0.2F;
            this.txtM1.Left = 1.1F;
            this.txtM1.Name = "txtM1";
            this.txtM1.OutputFormat = resources.GetString("txtM1.OutputFormat");
            this.txtM1.Style = "font-size: 8pt; text-align: right";
            this.txtM1.Top = 0F;
            this.txtM1.Width = 0.5F;
            // 
            // txtM2
            // 
            this.txtM2.CanGrow = false;
            this.txtM2.DataField = "2";
            this.txtM2.Height = 0.2F;
            this.txtM2.Left = 1.6F;
            this.txtM2.Name = "txtM2";
            this.txtM2.OutputFormat = resources.GetString("txtM2.OutputFormat");
            this.txtM2.Style = "font-size: 8pt; text-align: right";
            this.txtM2.Top = 0F;
            this.txtM2.Width = 0.5F;
            // 
            // txtM3
            // 
            this.txtM3.CanGrow = false;
            this.txtM3.DataField = "3";
            this.txtM3.Height = 0.2F;
            this.txtM3.Left = 2.1F;
            this.txtM3.Name = "txtM3";
            this.txtM3.OutputFormat = resources.GetString("txtM3.OutputFormat");
            this.txtM3.Style = "font-size: 8pt; text-align: right";
            this.txtM3.Top = 0F;
            this.txtM3.Width = 0.5F;
            // 
            // txtM5
            // 
            this.txtM5.CanGrow = false;
            this.txtM5.DataField = "5";
            this.txtM5.Height = 0.2F;
            this.txtM5.Left = 3.1F;
            this.txtM5.Name = "txtM5";
            this.txtM5.OutputFormat = resources.GetString("txtM5.OutputFormat");
            this.txtM5.Style = "font-size: 8pt; text-align: right";
            this.txtM5.Top = 0F;
            this.txtM5.Width = 0.5F;
            // 
            // txtM7
            // 
            this.txtM7.CanGrow = false;
            this.txtM7.DataField = "7";
            this.txtM7.Height = 0.2F;
            this.txtM7.Left = 4.1F;
            this.txtM7.Name = "txtM7";
            this.txtM7.OutputFormat = resources.GetString("txtM7.OutputFormat");
            this.txtM7.Style = "font-size: 8pt; text-align: right";
            this.txtM7.Top = 0F;
            this.txtM7.Width = 0.5F;
            // 
            // txtM8
            // 
            this.txtM8.CanGrow = false;
            this.txtM8.DataField = "8";
            this.txtM8.Height = 0.2F;
            this.txtM8.Left = 4.6F;
            this.txtM8.Name = "txtM8";
            this.txtM8.OutputFormat = resources.GetString("txtM8.OutputFormat");
            this.txtM8.Style = "font-size: 8pt; text-align: right";
            this.txtM8.Top = 0F;
            this.txtM8.Width = 0.5F;
            // 
            // ActiveReport1
            // 
            this.MasterReport = false;
            this.PageSettings.PaperHeight = 11F;
            this.PageSettings.PaperWidth = 8.5F;
            this.PrintWidth = 8.05F;
            this.Sections.Add(this.ReportHeader);
            this.Sections.Add(this.PageHeader);
            this.Sections.Add(this.Detail);
            this.Sections.Add(this.PageFooter);
            this.Sections.Add(this.ReportFooter);
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule(resources.GetString("$this.StyleSheet"), "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: inherit; font-style: inherit; font-variant: inherit; font-weight: bo" +
                        "ld; font-size: 16pt; font-size-adjust: inherit; font-stretch: inherit", "Heading1", "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: Times New Roman; font-style: italic; font-variant: inherit; font-wei" +
                        "ght: bold; font-size: 14pt; font-size-adjust: inherit; font-stretch: inherit", "Heading2", "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: inherit; font-style: inherit; font-variant: inherit; font-weight: bo" +
                        "ld; font-size: 13pt; font-size-adjust: inherit; font-stretch: inherit", "Heading3", "Normal"));
            ((System.ComponentModel.ISupportInitialize)(this.lbSalaryTitle)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label15)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label16)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label17)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label18)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label19)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label20)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label21)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label22)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label23)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label24)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label25)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label26)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label27)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label28)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtM4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtM6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtM9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtM10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtM11)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtM12)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtMall)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtYear)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtM1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtM2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtM3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtM5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtM7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtM8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();

            // Attach Report Events
            this.Detail.Format += new System.EventHandler(this.Detail_Format);
        }

		#endregion

        private ReportHeader ReportHeader;
        private Label lbSalaryTitle;
        private Label Label15;
        private Label Label16;
        private Label Label17;
        private Label Label18;
        private Label Label19;
        private Label Label20;
        private Label Label21;
        private Label Label22;
        private Label Label23;
        private Label Label24;
        private Label Label25;
        private Label Label26;
        private Label Label27;
        private Label Label28;
        private PageHeader PageHeader;
        private Detail Detail;
        private TextBox txtM4;
        private TextBox txtM6;
        private TextBox txtM9;
        private TextBox txtM10;
        private TextBox txtM11;
        private TextBox txtM12;
        private TextBox txtMall;
        private TextBox txtYear;
        private TextBox txtM1;
        private TextBox txtM2;
        private TextBox txtM3;
        private TextBox txtM5;
        private TextBox txtM7;
        private TextBox txtM8;
        private PageFooter PageFooter;
        private ReportFooter ReportFooter;
	}
}
