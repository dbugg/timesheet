using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Data.SqlClient;
using Asa.Timesheet.Data;
using log4net;

namespace Asa.Timesheet.WebPages
{
	/// <summary>
	/// Summary description for CopyHours.
	/// </summary>
	public class CopyHours : TimesheetPageBase
	{
    protected System.Web.UI.WebControls.ListBox lbSelectedUsers;
    protected System.Web.UI.WebControls.ListBox lbUsers;
    protected System.Web.UI.HtmlControls.HtmlInputText hdnlbUsers;
    protected System.Web.UI.HtmlControls.HtmlInputText hdnlbSelectedUsers;
    protected System.Web.UI.WebControls.Button btnSave;
    protected System.Web.UI.WebControls.Button btnCancel;
    protected System.Web.UI.WebControls.Label lblTitle;
    protected System.Web.UI.WebControls.Label lblSaveInfo;

    private static readonly ILog log = LogManager.GetLogger(typeof(CopyHours));
  
		private void Page_Load(object sender, System.EventArgs e)
		{
			LoadUsers();

      lbUsers.Attributes["ondblclick"]="UserDblClick();";
      lbSelectedUsers.Attributes["ondblclick"] = "UserDblClick();";

      btnSave.Attributes["onclick"] = "SaveClick(); return false;";
      btnCancel.Attributes["onclick"] = "CancelClick(); return false;";

      lblSaveInfo.Text = Resource.ResourceManager["hoursPeriod_WillNotSaveAlreadyEnteredMsg1"]+"<br>";

      Page.RegisterStartupScript("","<script>RemoveCurrent();</script>");
		}

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
			this.Load += new System.EventHandler(this.Page_Load);

		}
		#endregion

    private bool LoadUsers()
    {

      SqlDataReader reader = null;
      try
      {
        reader = (this.LoggedUser.IsAccountant || this.LoggedUser.IsAdmin|| this.LoggedUser.IsLeader) ? 
          UsersData.SelectUserNames(false) : UsersData.SelectUserNames(false);

        lbUsers.DataSource = reader;
        lbUsers.DataValueField = "UserID";
        lbUsers.DataTextField = "FullName";
        lbUsers.DataBind();
				
      }
      catch (Exception ex)
      {
        log.Error(ex);
        return false;
      }
      finally
      {
        if (reader!=null) reader.Close();
      }

      return true;
    }

    private void Button1_Click(object sender, System.EventArgs e)
    {
      int i = 9;
    }
	}
}
