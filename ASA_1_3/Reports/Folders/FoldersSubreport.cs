using System;
using GrapeCity.ActiveReports;
using GrapeCity.ActiveReports.Controls;
using GrapeCity.ActiveReports.SectionReportModel;
using GrapeCity.ActiveReports.Document.Section;
using GrapeCity.ActiveReports.Document;

namespace Asa.Timesheet.WebPages.Reports
{
    public class FoldersSubreport : GrapeCity.ActiveReports.SectionReport
	{
		//int _givenTotal = 0, _archiveTotal = 0;

        public FoldersSubreport()
        {
            InitializeComponent();
        }

        private void GroupHeader1_Format(object sender, System.EventArgs eArgs)
        {
            this.GroupHeader1.SizeToFit(true);
        }

        private void Detail_Format(object sender, System.EventArgs eArgs)
        {
            this.Detail.SizeToFit(true);
        }

        private void FoldersSubreport_FetchData(object sender, FetchEventArgs eArgs)
        {
            if (eArgs.EOF) return;
        }

        private void GroupFooter1_Format(object sender, System.EventArgs eArgs)
        {

        }

		#region ActiveReports Designer generated code

































        public void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FoldersSubreport));
            this.Detail = new GrapeCity.ActiveReports.SectionReportModel.Detail();
            this.GroupHeader1 = new GrapeCity.ActiveReports.SectionReportModel.GroupHeader();
            this.GroupFooter1 = new GrapeCity.ActiveReports.SectionReportModel.GroupFooter();
            this.Label5 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label6 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label7 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Line4 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.Line5 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.Line6 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.Line7 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.Line8 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.Line3 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.Label9 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Line20 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.txtSubcontracter = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtFoldersGiven = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtFoldersArchive = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.Line1 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.Line9 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.Line10 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.Line11 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.Line12 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.txtSubcontracterType = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.Line18 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.Label8 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Line13 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.Line14 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.Line15 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.TextBox1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.TextBox2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.Line16 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.Line17 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.Line19 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            ((System.ComponentModel.ISupportInitialize)(this.Label5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSubcontracter)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtFoldersGiven)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtFoldersArchive)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSubcontracterType)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TextBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TextBox2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            // 
            // Detail
            // 
            this.Detail.ColumnSpacing = 0F;
            this.Detail.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
						this.txtSubcontracter,
						this.txtFoldersGiven,
						this.txtFoldersArchive,
						this.Line1,
						this.Line9,
						this.Line10,
						this.Line11,
						this.Line12,
						this.txtSubcontracterType,
						this.Line18});
            this.Detail.Height = 0.4784722F;
            this.Detail.Name = "Detail";
            // 
            // GroupHeader1
            // 
            this.GroupHeader1.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
						this.Label5,
						this.Label6,
						this.Label7,
						this.Line4,
						this.Line5,
						this.Line6,
						this.Line7,
						this.Line8,
						this.Line3,
						this.Label9,
						this.Line20});
            this.GroupHeader1.Height = 0.53125F;
            this.GroupHeader1.Name = "GroupHeader1";
            // 
            // GroupFooter1
            // 
            this.GroupFooter1.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
						this.Label8,
						this.Line13,
						this.Line14,
						this.Line15,
						this.TextBox1,
						this.TextBox2,
						this.Line16,
						this.Line17,
						this.Line19});
            this.GroupFooter1.Height = 1.177083F;
            this.GroupFooter1.Name = "GroupFooter1";
            // 
            // Label5
            // 
            this.Label5.Height = 0.2362205F;
            this.Label5.HyperLink = null;
            this.Label5.Left = 0F;
            this.Label5.Name = "Label5";
            this.Label5.Style = "font-size: 11pt; font-weight: bold; text-align: center; vertical-align: middle";
            this.Label5.Text = "?????????????";
            this.Label5.Top = 0F;
            this.Label5.Width = 1.968504F;
            // 
            // Label6
            // 
            this.Label6.Height = 0.2362205F;
            this.Label6.HyperLink = null;
            this.Label6.Left = 3.937008F;
            this.Label6.Name = "Label6";
            this.Label6.Style = "font-size: 11pt; font-weight: bold; text-align: center; vertical-align: middle";
            this.Label6.Text = "?????? ?????";
            this.Label6.Top = 0F;
            this.Label6.Width = 1.181102F;
            // 
            // Label7
            // 
            this.Label7.Height = 0.2362205F;
            this.Label7.HyperLink = null;
            this.Label7.Left = 5.118111F;
            this.Label7.Name = "Label7";
            this.Label7.Style = "font-size: 10pt; font-weight: bold; text-align: center; vertical-align: middle";
            this.Label7.Text = "????? ? ??????";
            this.Label7.Top = 0F;
            this.Label7.Width = 1.181102F;
            // 
            // Line4
            // 
            this.Line4.Height = 0.2755906F;
            this.Line4.Left = 0F;
            this.Line4.LineWeight = 1F;
            this.Line4.Name = "Line4";
            this.Line4.Top = 0F;
            this.Line4.Width = 0F;
            this.Line4.X1 = 0F;
            this.Line4.X2 = 0F;
            this.Line4.Y1 = 0F;
            this.Line4.Y2 = 0.2755906F;
            // 
            // Line5
            // 
            this.Line5.Height = 0.2755906F;
            this.Line5.Left = 6.299212F;
            this.Line5.LineWeight = 1F;
            this.Line5.Name = "Line5";
            this.Line5.Top = 0F;
            this.Line5.Width = 0F;
            this.Line5.X1 = 6.299212F;
            this.Line5.X2 = 6.299212F;
            this.Line5.Y1 = 0F;
            this.Line5.Y2 = 0.2755906F;
            // 
            // Line6
            // 
            this.Line6.Height = 0.2755906F;
            this.Line6.Left = 1.968504F;
            this.Line6.LineWeight = 1F;
            this.Line6.Name = "Line6";
            this.Line6.Top = 0F;
            this.Line6.Width = 0F;
            this.Line6.X1 = 1.968504F;
            this.Line6.X2 = 1.968504F;
            this.Line6.Y1 = 0F;
            this.Line6.Y2 = 0.2755906F;
            // 
            // Line7
            // 
            this.Line7.Height = 0.2755906F;
            this.Line7.Left = 5.118111F;
            this.Line7.LineWeight = 1F;
            this.Line7.Name = "Line7";
            this.Line7.Top = 0F;
            this.Line7.Width = 0F;
            this.Line7.X1 = 5.118111F;
            this.Line7.X2 = 5.118111F;
            this.Line7.Y1 = 0F;
            this.Line7.Y2 = 0.2755906F;
            // 
            // Line8
            // 
            this.Line8.Height = 0F;
            this.Line8.Left = 0F;
            this.Line8.LineWeight = 1F;
            this.Line8.Name = "Line8";
            this.Line8.Top = 0.2755906F;
            this.Line8.Width = 6.299212F;
            this.Line8.X1 = 0F;
            this.Line8.X2 = 6.299212F;
            this.Line8.Y1 = 0.2755906F;
            this.Line8.Y2 = 0.2755906F;
            // 
            // Line3
            // 
            this.Line3.Height = 0F;
            this.Line3.Left = 0F;
            this.Line3.LineWeight = 1F;
            this.Line3.Name = "Line3";
            this.Line3.Top = 0F;
            this.Line3.Width = 6.299212F;
            this.Line3.X1 = 0F;
            this.Line3.X2 = 6.299212F;
            this.Line3.Y1 = 0F;
            this.Line3.Y2 = 0F;
            // 
            // Label9
            // 
            this.Label9.Height = 0.2362205F;
            this.Label9.HyperLink = null;
            this.Label9.Left = 1.968504F;
            this.Label9.Name = "Label9";
            this.Label9.Style = "font-size: 11pt; font-weight: bold; text-align: center; vertical-align: middle";
            this.Label9.Text = "????";
            this.Label9.Top = 0F;
            this.Label9.Width = 1.968504F;
            // 
            // Line20
            // 
            this.Line20.Height = 0.2755906F;
            this.Line20.Left = 3.937008F;
            this.Line20.LineWeight = 1F;
            this.Line20.Name = "Line20";
            this.Line20.Top = 0F;
            this.Line20.Width = 0F;
            this.Line20.X1 = 3.937008F;
            this.Line20.X2 = 3.937008F;
            this.Line20.Y1 = 0F;
            this.Line20.Y2 = 0.2755906F;
            // 
            // txtSubcontracter
            // 
            this.txtSubcontracter.DataField = "SubcontracterName";
            this.txtSubcontracter.Height = 0.2F;
            this.txtSubcontracter.Left = 0F;
            this.txtSubcontracter.Name = "txtSubcontracter";
            this.txtSubcontracter.OutputFormat = resources.GetString("txtSubcontracter.OutputFormat");
            this.txtSubcontracter.Style = "font-size: 10pt";
            this.txtSubcontracter.Top = 0F;
            this.txtSubcontracter.Width = 1.968504F;
            // 
            // txtFoldersGiven
            // 
            this.txtFoldersGiven.DataField = "FoldersGiven";
            this.txtFoldersGiven.Height = 0.2F;
            this.txtFoldersGiven.Left = 3.937008F;
            this.txtFoldersGiven.Name = "txtFoldersGiven";
            this.txtFoldersGiven.Style = "text-align: center";
            this.txtFoldersGiven.Top = 0F;
            this.txtFoldersGiven.Width = 1.181102F;
            // 
            // txtFoldersArchive
            // 
            this.txtFoldersArchive.DataField = "FoldersArchive";
            this.txtFoldersArchive.Height = 0.2F;
            this.txtFoldersArchive.Left = 5.118111F;
            this.txtFoldersArchive.Name = "txtFoldersArchive";
            this.txtFoldersArchive.OutputFormat = resources.GetString("txtFoldersArchive.OutputFormat");
            this.txtFoldersArchive.Style = "font-size: 10pt; text-align: center";
            this.txtFoldersArchive.Top = 0F;
            this.txtFoldersArchive.Width = 1.181102F;
            // 
            // Line1
            // 
            this.Line1.Height = 0F;
            this.Line1.Left = 0F;
            this.Line1.LineWeight = 1F;
            this.Line1.Name = "Line1";
            this.Line1.Top = 0.2755906F;
            this.Line1.Width = 6.299212F;
            this.Line1.X1 = 0F;
            this.Line1.X2 = 6.299212F;
            this.Line1.Y1 = 0.2755906F;
            this.Line1.Y2 = 0.2755906F;
            // 
            // Line9
            // 
            this.Line9.Height = 0.2755906F;
            this.Line9.Left = 0F;
            this.Line9.LineWeight = 1F;
            this.Line9.Name = "Line9";
            this.Line9.Top = 0F;
            this.Line9.Width = 0F;
            this.Line9.X1 = 0F;
            this.Line9.X2 = 0F;
            this.Line9.Y1 = 0F;
            this.Line9.Y2 = 0.2755906F;
            // 
            // Line10
            // 
            this.Line10.Height = 0.2755906F;
            this.Line10.Left = 6.299212F;
            this.Line10.LineWeight = 1F;
            this.Line10.Name = "Line10";
            this.Line10.Top = 0F;
            this.Line10.Width = 0F;
            this.Line10.X1 = 6.299212F;
            this.Line10.X2 = 6.299212F;
            this.Line10.Y1 = 0F;
            this.Line10.Y2 = 0.2755906F;
            // 
            // Line11
            // 
            this.Line11.Height = 0.2755906F;
            this.Line11.Left = 5.118111F;
            this.Line11.LineWeight = 1F;
            this.Line11.Name = "Line11";
            this.Line11.Top = 0F;
            this.Line11.Width = 0F;
            this.Line11.X1 = 5.118111F;
            this.Line11.X2 = 5.118111F;
            this.Line11.Y1 = 0F;
            this.Line11.Y2 = 0.2755906F;
            // 
            // Line12
            // 
            this.Line12.Height = 0.2755906F;
            this.Line12.Left = 1.968504F;
            this.Line12.LineWeight = 1F;
            this.Line12.Name = "Line12";
            this.Line12.Top = 0F;
            this.Line12.Width = 0F;
            this.Line12.X1 = 1.968504F;
            this.Line12.X2 = 1.968504F;
            this.Line12.Y1 = 0F;
            this.Line12.Y2 = 0.2755906F;
            // 
            // txtSubcontracterType
            // 
            this.txtSubcontracterType.DataField = "SubcontracterType";
            this.txtSubcontracterType.Height = 0.2F;
            this.txtSubcontracterType.Left = 1.968504F;
            this.txtSubcontracterType.Name = "txtSubcontracterType";
            this.txtSubcontracterType.Top = 0F;
            this.txtSubcontracterType.Width = 1.968504F;
            // 
            // Line18
            // 
            this.Line18.Height = 0.2755906F;
            this.Line18.Left = 3.937008F;
            this.Line18.LineWeight = 1F;
            this.Line18.Name = "Line18";
            this.Line18.Top = 0F;
            this.Line18.Width = 0F;
            this.Line18.X1 = 3.937008F;
            this.Line18.X2 = 3.937008F;
            this.Line18.Y1 = 0F;
            this.Line18.Y2 = 0.2755906F;
            // 
            // Label8
            // 
            this.Label8.Height = 0.2362205F;
            this.Label8.HyperLink = null;
            this.Label8.Left = 0F;
            this.Label8.Name = "Label8";
            this.Label8.Style = "font-size: 11pt; font-weight: bold; text-align: center; vertical-align: middle";
            this.Label8.Text = "????:";
            this.Label8.Top = 0F;
            this.Label8.Width = 1.968504F;
            // 
            // Line13
            // 
            this.Line13.Height = 0.2755906F;
            this.Line13.Left = 1.968504F;
            this.Line13.LineWeight = 1F;
            this.Line13.Name = "Line13";
            this.Line13.Top = 0F;
            this.Line13.Width = 0F;
            this.Line13.X1 = 1.968504F;
            this.Line13.X2 = 1.968504F;
            this.Line13.Y1 = 0F;
            this.Line13.Y2 = 0.2755906F;
            // 
            // Line14
            // 
            this.Line14.Height = 0.2755906F;
            this.Line14.Left = 3.937008F;
            this.Line14.LineWeight = 1F;
            this.Line14.Name = "Line14";
            this.Line14.Top = 0F;
            this.Line14.Width = 0F;
            this.Line14.X1 = 3.937008F;
            this.Line14.X2 = 3.937008F;
            this.Line14.Y1 = 0F;
            this.Line14.Y2 = 0.2755906F;
            // 
            // Line15
            // 
            this.Line15.Height = 0F;
            this.Line15.Left = 0F;
            this.Line15.LineWeight = 1F;
            this.Line15.Name = "Line15";
            this.Line15.Top = 0.2755906F;
            this.Line15.Width = 6.299212F;
            this.Line15.X1 = 0F;
            this.Line15.X2 = 6.299212F;
            this.Line15.Y1 = 0.2755906F;
            this.Line15.Y2 = 0.2755906F;
            // 
            // TextBox1
            // 
            this.TextBox1.DataField = "FoldersGiven";
            this.TextBox1.Height = 0.2362205F;
            this.TextBox1.Left = 3.937008F;
            this.TextBox1.Name = "TextBox1";
            this.TextBox1.Style = "font-size: 11pt; font-weight: bold; text-align: center";
            this.TextBox1.SummaryGroup = "GroupHeader1";
            this.TextBox1.SummaryRunning = GrapeCity.ActiveReports.SectionReportModel.SummaryRunning.Group;
            this.TextBox1.SummaryType = GrapeCity.ActiveReports.SectionReportModel.SummaryType.SubTotal;
            this.TextBox1.Text = "txtGivenSummary";
            this.TextBox1.Top = 0F;
            this.TextBox1.Width = 1.181102F;
            // 
            // TextBox2
            // 
            this.TextBox2.DataField = "FoldersArchive";
            this.TextBox2.Height = 0.2362205F;
            this.TextBox2.Left = 5.118111F;
            this.TextBox2.Name = "TextBox2";
            this.TextBox2.Style = "font-size: 11pt; font-weight: bold; text-align: center";
            this.TextBox2.SummaryGroup = "GroupHeader1";
            this.TextBox2.SummaryRunning = GrapeCity.ActiveReports.SectionReportModel.SummaryRunning.Group;
            this.TextBox2.SummaryType = GrapeCity.ActiveReports.SectionReportModel.SummaryType.SubTotal;
            this.TextBox2.Text = "txtArchiveSummary";
            this.TextBox2.Top = 0F;
            this.TextBox2.Width = 1.181102F;
            // 
            // Line16
            // 
            this.Line16.Height = 0.2755906F;
            this.Line16.Left = 0F;
            this.Line16.LineWeight = 1F;
            this.Line16.Name = "Line16";
            this.Line16.Top = 0F;
            this.Line16.Width = 0F;
            this.Line16.X1 = 0F;
            this.Line16.X2 = 0F;
            this.Line16.Y1 = 0F;
            this.Line16.Y2 = 0.2755906F;
            // 
            // Line17
            // 
            this.Line17.Height = 0.2755906F;
            this.Line17.Left = 5.118111F;
            this.Line17.LineWeight = 1F;
            this.Line17.Name = "Line17";
            this.Line17.Top = 0F;
            this.Line17.Width = 0F;
            this.Line17.X1 = 5.118111F;
            this.Line17.X2 = 5.118111F;
            this.Line17.Y1 = 0F;
            this.Line17.Y2 = 0.2755906F;
            // 
            // Line19
            // 
            this.Line19.Height = 0.2755906F;
            this.Line19.Left = 6.299212F;
            this.Line19.LineWeight = 1F;
            this.Line19.Name = "Line19";
            this.Line19.Top = 0F;
            this.Line19.Width = 0F;
            this.Line19.X1 = 6.299212F;
            this.Line19.X2 = 6.299212F;
            this.Line19.Y1 = 0F;
            this.Line19.Y2 = 0.2755906F;
            // 
            // ActiveReport1
            // 
            this.MasterReport = false;
            this.PageSettings.DefaultPaperSize = false;
            this.PageSettings.Margins.Bottom = 0.8034722F;
            this.PageSettings.Margins.Left = 0.8034722F;
            this.PageSettings.Margins.Right = 0.8034722F;
            this.PageSettings.Margins.Top = 0.8034722F;
            this.PageSettings.PaperHeight = 11.69306F;
            this.PageSettings.PaperKind = System.Drawing.Printing.PaperKind.A4;
            this.PageSettings.PaperWidth = 8.268056F;
            this.PrintWidth = 6.299212F;
            this.Sections.Add(this.GroupHeader1);
            this.Sections.Add(this.Detail);
            this.Sections.Add(this.GroupFooter1);
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule(resources.GetString("$this.StyleSheet"), "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: inherit; font-style: inherit; font-variant: inherit; font-weight: bo" +
                        "ld; font-size: 16pt; font-size-adjust: inherit; font-stretch: inherit", "Heading1", "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: Times New Roman; font-style: italic; font-variant: inherit; font-wei" +
                        "ght: bold; font-size: 14pt; font-size-adjust: inherit; font-stretch: inherit", "Heading2", "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: inherit; font-style: inherit; font-variant: inherit; font-weight: bo" +
                        "ld; font-size: 13pt; font-size-adjust: inherit; font-stretch: inherit", "Heading3", "Normal"));
            ((System.ComponentModel.ISupportInitialize)(this.Label5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSubcontracter)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtFoldersGiven)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtFoldersArchive)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSubcontracterType)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TextBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TextBox2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();

            // Attach Report Events
            this.GroupHeader1.Format += new System.EventHandler(this.GroupHeader1_Format);
            this.Detail.Format += new System.EventHandler(this.Detail_Format);
            this.FetchData += new GrapeCity.ActiveReports.SectionReport.FetchEventHandler(this.FoldersSubreport_FetchData);
            this.GroupFooter1.Format += new System.EventHandler(this.GroupFooter1_Format);
        }

		#endregion

        private GroupHeader GroupHeader1;
        private Label Label5;
        private Label Label6;
        private Label Label7;
        private Line Line4;
        private Line Line5;
        private Line Line6;
        private Line Line7;
        private Line Line8;
        private Line Line3;
        private Label Label9;
        private Line Line20;
        private Detail Detail;
        private TextBox txtSubcontracter;
        private TextBox txtFoldersGiven;
        private TextBox txtFoldersArchive;
        private Line Line1;
        private Line Line9;
        private Line Line10;
        private Line Line11;
        private Line Line12;
        private TextBox txtSubcontracterType;
        private Line Line18;
        private GroupFooter GroupFooter1;
        private Label Label8;
        private Line Line13;
        private Line Line14;
        private Line Line15;
        private TextBox TextBox1;
        private TextBox TextBox2;
        private Line Line16;
        private Line Line17;
        private Line Line19;
	}
}
