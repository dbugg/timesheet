using System;
using System.Data;
using System.Data.SqlClient;
using Microsoft.ApplicationBlocks.Data;
using System.Xml;
using System.Collections;
using System.Collections.Specialized;
using System.Configuration;
using log4net;
namespace Asa.Timesheet.Data
{
	/// <summary>
	/// Summary description for WorkTimesData.
	/// </summary>
	public class WorkTimesData
	{
		private static readonly ILog log = LogManager.GetLogger(typeof(WorkTimesData));
		private const int MinutesMore_Auth=60;
		private enum WorkDayStatus
		{
		NotEntered = 0,
		EnteredByCurrent = 1,
		EnteredByOther = 2,
		Mixed = 3
		}

    #region Delete 

		public static void DeleteWorkTimesForDay(SqlTransaction trans, int userID, DateTime workDate)
		{
			string spName = "WorkTimesForDayDelProc";

			SqlParameter[] storedParams = SqlHelperParameterCache.GetSpParameterSet(
				DBManager.GetConnectionString(), spName);
				
			storedParams[0].Value = userID;
			storedParams[1].Value = workDate;

			SqlHelper.ExecuteNonQuery(trans, CommandType.StoredProcedure, spName, storedParams);
		}

    /*
    public static void DeleteWorkTimesForPeriod(SqlTransaction trans, int userID, DateTime startDate, DateTime endDate)
    {
      string spName = "WorkTimesForPeriodDelProc";

      SqlParameter[] storedParams = SqlHelperParameterCache.GetSpParameterSet(
        DBManager.GetConnectionString(), spName);
				
      storedParams[0].Value = userID;
      storedParams[1].Value = startDate;
      storedParams[2].Value = endDate;

      SqlHelper.ExecuteNonQuery(trans, DBManager.GetConnectionString(), spName, storedParams);
    }
    */

    public static void DeleteWorkTime(int workTimeID)
    {
      string spName = "WorkTimesDelProc";
      SqlParameter[] storedParams = SqlHelperParameterCache.GetSpParameterSet(DBManager.GetConnectionString(), spName);

      storedParams[0].Value = workTimeID;

      SqlHelper.ExecuteNonQuery(DBManager.GetConnectionString(), CommandType.StoredProcedure, spName, storedParams);
    }

    #endregion
		private static SqlCommand CreateWorkTimesSelAuthorsProc (int ActivityID, DateTime StartDate, DateTime EndDate, int ProjectID)
		{
			SqlCommand cmd = new SqlCommand();
			cmd.CommandType = System.Data.CommandType.StoredProcedure;
			cmd.CommandText = "WorkTimesSelAuthorsProc";
			cmd.Parameters.Add(new System.Data.SqlClient.SqlParameter("@ActivityID", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.Input, true, 10, 0, "ActivityID", DataRowVersion.Current, null));
			
			cmd.Parameters["@ActivityID"].Value = ActivityID;
			cmd.Parameters.Add(new System.Data.SqlClient.SqlParameter("@StartDate", System.Data.SqlDbType.SmallDateTime, 8, System.Data.ParameterDirection.Input, true, 0, 0, "StartDate", DataRowVersion.Current, null));
			
			cmd.Parameters["@StartDate"].Value = StartDate;
			cmd.Parameters.Add(new System.Data.SqlClient.SqlParameter("@EndDate", System.Data.SqlDbType.SmallDateTime, 8, System.Data.ParameterDirection.Input, true, 0, 0, "EndDate", DataRowVersion.Current, null));
			
			cmd.Parameters["@EndDate"].Value = EndDate;
			cmd.Parameters.Add(new System.Data.SqlClient.SqlParameter("@ProjectID", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.Input, true, 10, 0, "ProjectID", DataRowVersion.Current, null));
			
			cmd.Parameters["@ProjectID"].Value = ProjectID;
			cmd.Parameters.Add(new System.Data.SqlClient.SqlParameter("@SumMinutes", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.InputOutput, true, 10, 0, "SumMinutes", DataRowVersion.Current, null));
			
			
			return cmd;
		}
		public static int ExecuteWorkTimesSelAuthorsProc(int ActivityID, DateTime StartDate, DateTime EndDate, int ProjectID)
		{
			
			System.Data.SqlClient.SqlCommand cmd;
			cmd = CreateWorkTimesSelAuthorsProc(ActivityID, StartDate, EndDate, ProjectID);
			SqlConnection cnDAL = new SqlConnection();

			try
			{
				cnDAL =  CommonDAL.OpenConnection();
				cmd.Connection=cnDAL;
				cmd.ExecuteNonQuery();
				if(cmd.Parameters["@SumMinutes"].Value!=DBNull.Value)
					return (int)cmd.Parameters["@SumMinutes"].Value;
				return 0;
		
			}
			catch(SqlException ex)
			{

				throw new ApplicationException("WorkTimesSelAuthorsProc",ex);
			}

			finally
			{
				CommonDAL.CloseConnection(cnDAL);
			}
		}
		public static int ExecuteWorkTimesSelAuthorsProc(int ActivityID, DateTime StartDate, DateTime EndDate, int ProjectID,SqlConnection conn, SqlTransaction trans )
		{
			
			System.Data.SqlClient.SqlCommand cmd;
			cmd = CreateWorkTimesSelAuthorsProc(ActivityID, StartDate, EndDate, ProjectID);
			//SqlConnection cnDAL = new SqlConnection();

			try
			{
				//cnDAL =  CommonDAL.OpenConnection();
				cmd.Connection=conn;
				cmd.Transaction=trans;
				cmd.ExecuteNonQuery();
				if(cmd.Parameters["@SumMinutes"].Value!=DBNull.Value)
					return (int)cmd.Parameters["@SumMinutes"].Value;
				return 0;
		
			}
			catch(SqlException ex)
			{

				throw new ApplicationException("WorkTimesSelAuthorsProc",ex);
			}

			finally
			{
				//CommonDAL.CloseConnection(cnDAL);
			}
		}

    #region Insert

    public static void InsertWorkTimesForPeriod(int userID, DateTime startDate, DateTime endDate,
      ArrayList workTimesList, DateTime dateEntered, int enteredByUserID, bool overwritePrevious)
    {

      using (SqlConnection conn = new SqlConnection(DBManager.GetConnectionString()))
      {
        conn.Open();

        SqlTransaction trans = conn.BeginTransaction();
        try
        {
          DateTime dtTemp = startDate.Date;

          while (dtTemp<=endDate.Date)
          {
            if ( !CalendarDAL.IsHoliday(dtTemp)&& 
              GetWorkDayStatus(trans, userID, enteredByUserID, dtTemp) == WorkDayStatus.NotEntered)
            {
              DeleteWorkTimesForDay(trans, userID, dtTemp);

              for (int i=0; i<workTimesList.Count; i++)
              {
                Entities.WorkTimeInfo wti = (Entities.WorkTimeInfo)workTimesList[i];

                InsertWorkTime(trans, dtTemp, userID, wti.ProjectID, wti.ActivityID, wti.StartTimeID, wti.EndTimeID,
                  wti.Minutes, wti.AdminMinutes, DateTime.Now, enteredByUserID,-1); 
				  if(wti.ActivityID==int.Parse(System.Configuration.ConfigurationManager.AppSettings["AN"]))
						InsertAuthorsPayments(dtTemp,wti.ProjectID,conn,trans);
              }
            }

            dtTemp = dtTemp.AddDays(1);
          }

          trans.Commit();
        }
        catch (Exception ex)
        {
          trans.Rollback();
          throw ex;
        }
      }

    }

		public static void InsertWorkTimesForUser(int userID, DateTime workDate, ArrayList workTimesList, DateTime dateEntered, int enteredByUserID, System.Web.UI.HtmlControls.HtmlInputFile file)
		{
			string spDeleteName = "WorkTimesForDayDelProc";
			SqlParameter[] deleteParams = SqlHelperParameterCache.GetSpParameterSet(DBManager.GetConnectionString(), spDeleteName);
			deleteParams[0].Value = userID;
			deleteParams[1].Value = workDate;

			string spInsertName = "WorkTimesInsProc";
			SqlParameter[] insertParams = SqlHelperParameterCache.GetSpParameterSet(DBManager.GetConnectionString(), spInsertName);

			using (SqlConnection conn = new SqlConnection(DBManager.GetConnectionString()))
			{
				conn.Open();
				SqlTransaction trans = conn.BeginTransaction();
				try
				{
					SqlHelper.ExecuteNonQuery(trans, CommandType.StoredProcedure, spDeleteName, deleteParams);

					for (int i=0; i<workTimesList.Count; i++)
					{
						Entities.WorkTimeInfo wti = (Entities.WorkTimeInfo)workTimesList[i];

						insertParams[0].Value = workDate;
						insertParams[1].Value = userID;
						insertParams[2].Value = wti.ProjectID;
						int activityID = wti.ActivityID;
						if (activityID==0) insertParams[3].Value = null; 
						else insertParams[3].Value = activityID;
						insertParams[4].Value = wti.StartTimeID;
						insertParams[5].Value = wti.EndTimeID;

						insertParams[6].Value = wti.Minutes;
						insertParams[7].Value = wti.AdminMinutes;
						insertParams[8].Value = dateEntered;
						insertParams[9].Value = enteredByUserID;

						SqlHelper.ExecuteNonQuery(trans, CommandType.StoredProcedure, spInsertName, insertParams);
						if(wti.ActivityID==int.Parse(System.Configuration.ConfigurationManager.AppSettings["AN"]))
						{
							InsertAuthorsPayments(workDate,wti.ProjectID,conn,trans);

							InsertAuthorsMeeting(workDate,wti.ProjectID, wti.ActivityID, wti.StartTimeID,wti.EndTimeID, enteredByUserID, file);
						}
					}

					trans.Commit();
				}
				catch
				{
					trans.Rollback();
					throw;
				}
			}

		}
		private static void LoadLeadersOfficersMails(out StringCollection leadersNames , out StringCollection leadersEmails )
		{
			leadersEmails = new StringCollection();
			leadersNames = new StringCollection();

			SqlDataReader reader = null;

			try
			{
				reader = UsersData.SelectLeadersOfficersEmails();
				while (reader.Read())
				{
					leadersEmails.Add(reader.GetString(1));
					leadersNames.Add(reader.GetString(0));

				}

			}
			catch (Exception ex)
			{
				leadersEmails = null;
				leadersNames = null;
				log.Error(ex.ToString());
			}
			finally
			{
				if (reader!=null) reader.Close();
			}
		}
		private static void InsertAuthorsMeeting(DateTime workDate,int ProjectID, int nAuthors, int nStart,int nEnd, int nUserID,
		System.Web.UI.HtmlControls.HtmlInputFile FileCtrl)
		{
			MeetingData md = MeetingDAL.Load(workDate,ProjectID,nAuthors, nStart, nEnd);
			int ID=-1;
			if(md==null)
			{
				md = new MeetingData(-1, workDate, ProjectID,nAuthors,nStart, nEnd,"","",nUserID.ToString(),nUserID,true,nUserID);
				md.Notes=Resource.ResourceManager["reports_AN_Title"];
				ID=MeetingDAL.Save(md);
			}
			else
			{
				ID=md.MeetingID;
			}
			
			

			if(FileCtrl.PostedFile.FileName.Length!=0 && md.HasScan==false)
			{
				string name = FileCtrl.PostedFile.FileName;
				int n = name.LastIndexOf(".");
				if(n!=-1)
				{
					string ext = name.Substring(n);
					if(ext==System.Configuration.ConfigurationManager.AppSettings["Extension"])
					{					
						MeetingDocumentData mdd = new MeetingDocumentData(-1,ID,false);
						MeetingDocumentDAL.Save(mdd);
						log.Info(string.Format("ScanDoc {0} of Meeting {2} has been INSERTED by {1}",mdd.MeetingDocument,string.Concat(" ( userID ",nUserID," )"),ID.ToString()));			
						string path = string.Concat(System.Configuration.ConfigurationManager.AppSettings["ScanPath"],mdd.MeetingDocument,ext);
						string mapped = System.Web.HttpContext.Current.Request.MapPath(path);
						FileCtrl.PostedFile.SaveAs(mapped);											
						MeetingData md1=MeetingDAL.Load(ID);
						md1.HasScan=true;

						MeetingDAL.Save(md1);
					}
				}
			}
			
		}
		private static void InsertAuthorsPayments(DateTime workDate,int ProjectID,SqlConnection conn, SqlTransaction trans)
		{
			
				SqlDataReader reader = null;
				decimal dAuthors=0;
				string code="";
				string name="";
				try
				{
					reader = ProjectsData.SelectProject(ProjectID);
					if(reader.Read())
					{

						name= reader.GetString(1);
						code = reader.IsDBNull(2) ? String.Empty : reader.GetString(2);
						
						if(!reader.IsDBNull(28))
							dAuthors = reader.GetDecimal(28);
						
					}
				}
				catch (Exception ex)
				{
					log.Error(ex.ToString());
					return;
				}

				finally 
				{ 
					if (reader!=null) reader.Close(); 
				}
			
			DateTime dtStart = workDate;
			DateTime dtEnd = workDate;
			if(dtStart!=new DateTime())
			{
				int minutes= WorkTimesData.ExecuteWorkTimesSelAuthorsProc(int.Parse(System.Configuration.ConfigurationManager.AppSettings["AN"]),
					dtStart,dtEnd,ProjectID,conn,trans);
				if(minutes>0)
				{
					decimal dTotal=(minutes+MinutesMore_Auth)*dAuthors/60;
					PaymentData pd= new PaymentData(-1, -1,-1,0,dTotal,false,Constants.DateMax,0,true,-1);
					pd.AuthorsID=ProjectID;
					pd.AuthorsStartDate=dtStart;
					pd.AuthorsEndDate=dtEnd;
					PaymentDAL.Save(pd);
								
					StringCollection leadersMails = null, leadersNames = null;
					LoadLeadersOfficersMails(out leadersNames, out leadersMails);

				}
			}
		}
		public static int InsertWorkTime(SqlTransaction trans, DateTime workDate, int userID, int projectID, int activityID,
					int startTimeID, int endTimeID, string minutes, string adminMinutes, DateTime enteredDate, int enteredBy, int meetingID)
		{
      string spName = "WorkTimesInsProc";

      SqlParameter[] storedParams = SqlHelperParameterCache.GetSpParameterSet(
        DBManager.GetConnectionString(), spName);
      storedParams[0].Value = workDate;
      storedParams[1].Value = userID;
      storedParams[2].Value = projectID;
      if (activityID==0) storedParams[3].Value = null; 
      else storedParams[3].Value = activityID;
      storedParams[4].Value = startTimeID;
      storedParams[5].Value = endTimeID;
      storedParams[6].Value = minutes;
      storedParams[7].Value = adminMinutes;
      storedParams[8].Value = enteredDate;
      storedParams[9].Value = (enteredBy == 0) ? null : (object)enteredBy;
		if(meetingID>0)
			storedParams[10].Value = meetingID;
		if(trans==null)
			SqlHelper.ExecuteNonQuery(DBManager.GetConnectionString(),CommandType.StoredProcedure, spName, storedParams);
			else
      SqlHelper.ExecuteNonQuery(trans, CommandType.StoredProcedure, spName, storedParams);
	
      return (int)storedParams[11].Value;;
		}
		
/*
    public static int InsertWorkTime(DateTime workDate, int userID, int projectID, int activityID,
      int startTimeID, int endTimeID, string minutes, string adminMinutes, DateTime enteredDate, int enteredBy)
    {
      string spName = "WorkTimesInsProc";

      SqlParameter[] storedParams = SqlHelperParameterCache.GetSpParameterSet(
        DBManager.GetConnectionString(), spName);
      storedParams[0].Value = workDate;
      storedParams[1].Value = userID;
      storedParams[2].Value = projectID;
      if (activityID==0) storedParams[3].Value = null; 
      else storedParams[3].Value = activityID;
      storedParams[4].Value = startTimeID;
      storedParams[5].Value = endTimeID;
      storedParams[6].Value = minutes;
      storedParams[7].Value = adminMinutes;
      storedParams[8].Value = enteredDate;
      storedParams[9].Value = (enteredBy == 0) ? null : (object)enteredBy;
			
      SqlHelper.ExecuteNonQuery(DBManager.GetConnectionString(),CommandType.StoredProcedure, spName, storedParams);
	
      return (int)storedParams[10].Value;;
    }
*/
    #endregion
		private static SqlCommand CreateWorkTimesDelByMeetingProc (int MeetingID)
		{
			SqlCommand cmd = new SqlCommand();
			cmd.CommandType = System.Data.CommandType.StoredProcedure;
			cmd.CommandText = "WorkTimesDelByMeetingProc";
			cmd.Parameters.Add(new System.Data.SqlClient.SqlParameter("@MeetingID", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.Input, true, 10, 0, "MeetingID", DataRowVersion.Current, null));
			
				cmd.Parameters["@MeetingID"].Value = MeetingID;

			return cmd;
		}
		public static int ExecuteWorkTimesDelByMeetingProc(int MeetingID)
		{
			int RetObj;
			System.Data.SqlClient.SqlCommand cmd;
			cmd = CreateWorkTimesDelByMeetingProc(MeetingID);
			SqlConnection cnDAL = new SqlConnection();

			try
			{
				cnDAL =  CommonDAL.OpenConnection();
				cmd.Connection = cnDAL;
				RetObj = cmd.ExecuteNonQuery();
				return RetObj;
		
			}
			catch(SqlException ex)
			{

				throw new ApplicationException("WorkTimesDelByMeetingProc",ex);
			}

			finally
			{
				CommonDAL.CloseConnection(cnDAL);
			}
		}

		private static SqlCommand CreateWorkTimesListMeetingProc (DateTime MeetingDate, int ProjectID, int ActivityID, int StartTimeID, int EndTimeID)
		{
			SqlCommand cmd = new SqlCommand();
			cmd.CommandType = System.Data.CommandType.StoredProcedure;
			cmd.CommandText = "WorkTimesListMeetingProc";
			cmd.Parameters.Add(new System.Data.SqlClient.SqlParameter("@MeetingDate", System.Data.SqlDbType.SmallDateTime, 8, System.Data.ParameterDirection.Input, true, 0, 0, "MeetingDate", DataRowVersion.Current, null));
			if(MeetingDate!=Constants.DateMax)
				cmd.Parameters["@MeetingDate"].Value = MeetingDate;
			cmd.Parameters.Add(new System.Data.SqlClient.SqlParameter("@ProjectID", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.Input, true, 10, 0, "ProjectID", DataRowVersion.Current, null));
			
				cmd.Parameters["@ProjectID"].Value = ProjectID;
			cmd.Parameters.Add(new System.Data.SqlClient.SqlParameter("@ActivityID", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.Input, true, 10, 0, "ActivityID", DataRowVersion.Current, null));
			
				cmd.Parameters["@ActivityID"].Value = ActivityID;
			cmd.Parameters.Add(new System.Data.SqlClient.SqlParameter("@StartTimeID", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.Input, true, 10, 0, "StartTimeID", DataRowVersion.Current, null));
			
				cmd.Parameters["@StartTimeID"].Value = StartTimeID;
			cmd.Parameters.Add(new System.Data.SqlClient.SqlParameter("@EndTimeID", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.Input, true, 10, 0, "EndTimeID", DataRowVersion.Current, null));
			
				cmd.Parameters["@EndTimeID"].Value = EndTimeID;

			return cmd;
		}
		public static System.Data.DataView ExecuteWorkTimesListMeetingProc(DateTime MeetingDate, int ProjectID, int ActivityID, int StartTimeID, int EndTimeID)  
		{
			System.Data.SqlClient.SqlDataReader RetObj;
			System.Data.SqlClient.SqlCommand cmd;
			cmd = CreateWorkTimesListMeetingProc(MeetingDate, ProjectID, ActivityID, StartTimeID, EndTimeID);
			SqlConnection cnDAL = new SqlConnection();

			try
			{
				cnDAL =  CommonDAL.OpenConnection();
				cmd.Connection = cnDAL;
				RetObj = cmd.ExecuteReader();
				return new DataView(CommonDAL.CreateTableFromReader(RetObj));
		
			}
			catch(SqlException ex)
			{

				throw new ApplicationException("WorkTimesListMeetingProc",ex);
			}

			finally
			{
				CommonDAL.CloseConnection(cnDAL);
			}
		}

		public static ArrayList SelectWorkTimesNew(int userID)
		{
			string spName = "WorkTimesLastProjectsSelProc";

			SqlParameter[] storedParams = SqlHelperParameterCache.GetSpParameterSet(DBManager.GetConnectionString(), spName);
			storedParams[0].Value = userID;

			ArrayList workTimesList = new ArrayList();
			SqlDataReader reader = null;

			try
			{
				reader = SqlHelper.ExecuteReader(DBManager.GetConnectionString(), spName, storedParams);
				while (reader.Read())
				{
					Entities.WorkTimeInfo wti = new Entities.WorkTimeInfo();
					wti.WorkTimeID = reader.GetInt32(0);
					wti.ProjectID = reader.GetInt32(1);
					wti.ActivityID = reader.GetInt32(2);
					wti.StartTimeID = reader.GetInt32(3);
					wti.EndTimeID = reader.GetInt32(4);
					wti.Minutes = reader.IsDBNull(5) ? String.Empty : reader.GetString(5);
					wti.AdminMinutes = reader.IsDBNull(6) ? String.Empty : reader.GetString(6);

					workTimesList.Add(wti);
				}
			}
			finally
			{
				if (reader!=null) reader.Close();
			}

			return workTimesList;
		}

    public static ArrayList SelectWorkTimesNew(int userID, int currentUserID)
    {
      string spName = "WorkTimesLastProjectsSelProc";

      SqlParameter[] storedParams = SqlHelperParameterCache.GetSpParameterSet(DBManager.GetConnectionString(), spName);
      storedParams[0].Value = userID;
      storedParams[1].Value = currentUserID;


      ArrayList workTimesList = new ArrayList();
      SqlDataReader reader = null;

      try
      {
        reader = SqlHelper.ExecuteReader(DBManager.GetConnectionString(), spName, storedParams);
        while (reader.Read())
        {
          Entities.WorkTimeInfo wti = new Entities.WorkTimeInfo();
          wti.WorkTimeID = reader.GetInt32(0);
          wti.ProjectID = reader.GetInt32(1);
          wti.ActivityID = reader.GetInt32(2);
          wti.StartTimeID = reader.GetInt32(3);
          wti.EndTimeID = reader.GetInt32(4);
          wti.Minutes = reader.IsDBNull(5) ? String.Empty : reader.GetString(5);
          wti.AdminMinutes = reader.IsDBNull(6) ? String.Empty : reader.GetString(6);

          workTimesList.Add(wti);
        }
      }
      finally
      {
        if (reader!=null) reader.Close();
      }

      return workTimesList;
    }
		

		public static ArrayList SelectWorkTimesForDay(int userID, DateTime workDate)
		{
			string spName = "WorkTimesForDayListProc";

			SqlParameter[] storedParams = SqlHelperParameterCache.GetSpParameterSet(
										DBManager.GetConnectionString(), spName);
			storedParams[0].Value = userID;
			storedParams[1].Value = workDate;

			ArrayList workTimesList = new ArrayList();
			SqlDataReader reader = null;

			try
			{
				reader = SqlHelper.ExecuteReader(DBManager.GetConnectionString(), spName, storedParams);
				while (reader.Read())
				{
					Entities.WorkTimeInfo wti = new Entities.WorkTimeInfo();
					wti.WorkTimeID = reader.GetInt32(0);
					wti.ProjectID = reader.GetInt32(3);
					wti.ActivityID = reader.IsDBNull(4) ? 0 : reader.GetInt32(4);
					wti.StartTimeID = reader.GetInt32(5);
					wti.EndTimeID = reader.GetInt32(6);
					wti.Minutes = reader.IsDBNull(7) ? String.Empty : reader.GetString(7);
					wti.AdminMinutes = reader.IsDBNull(8) ? String.Empty : reader.GetString(8);
                    wti.DateEntered = reader.IsDBNull(10) ? DateTime.MaxValue : reader.GetDateTime(10);
                    wti.MeetingId = reader.IsDBNull(11) ? 0 : reader.GetInt32(11);

					workTimesList.Add(wti);
				}
			}
			finally
			{
				if (reader!=null) reader.Close();
			}

			return workTimesList;

		}

		public static bool IsWorkDayEntered(int userID, DateTime workDate, bool excludeToday)
		{
			string spName = "WorkTimesIsDayEnteredProc";
			SqlParameter[] storedParams = SqlHelperParameterCache.GetSpParameterSet(DBManager.GetConnectionString(), spName);
			storedParams[0].Value = userID;
			storedParams[1].Value = workDate;

			SqlHelper.ExecuteNonQuery(DBManager.GetConnectionString(), CommandType.StoredProcedure, spName, storedParams);

			return (bool)storedParams[2].Value;
		}

    private static WorkDayStatus GetWorkDayStatus(SqlTransaction trans, int userID, int currentUserID, DateTime workDate)
    {
      string spName = "WorkTimesDayStatus";
      SqlParameter[] storedParams = SqlHelperParameterCache.GetSpParameterSet(DBManager.GetConnectionString(), spName);
      storedParams[0].Value = userID;
      storedParams[1].Value = currentUserID;
      storedParams[2].Value = workDate;

      SqlHelper.ExecuteNonQuery(trans, CommandType.StoredProcedure, spName, storedParams);

      return (WorkDayStatus)storedParams[3].Value;
    }

		public static void GetWorkTimesDateEnteredAt(int userID, DateTime workDate, out DateTime enteredAt, out int enteredBy)
		{
			string spName = "WorkTimesGetDateEnteredAt";
			SqlParameter[] storedParams = SqlHelperParameterCache.GetSpParameterSet(DBManager.GetConnectionString(), spName);
			storedParams[0].Value = userID;
			storedParams[1].Value = workDate;

			SqlDataReader reader = null;
			try
			{
				reader = SqlHelper.ExecuteReader(DBManager.GetConnectionString(), CommandType.StoredProcedure, spName, storedParams);
				if (!reader.HasRows) 
				{ 
					enteredAt = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day); 
					enteredBy = 0; 
				}
				else
				{
					reader.Read();
					enteredAt = reader.IsDBNull(0)?new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day):reader.GetDateTime(0);
					enteredBy = reader.IsDBNull(1)?0:reader.GetInt32(1);
				}
			}
			catch (Exception ex)
			{
				throw ex;
			}
			finally
			{
				if (reader!=null) reader.Close();
			}
		}
		
		#region unused

		public static DataSet SelectWorkTimesNewDS(int userID)
		{
			string spName = "WorkTimesLastProjectsSelProc";

			SqlParameter[] storedParams = SqlHelperParameterCache.GetSpParameterSet(
				DBManager.GetConnectionString(), spName);
				
			storedParams[0].Value = userID;

			return SqlHelper.ExecuteDataset(DBManager.GetConnectionString(), spName, storedParams);

		}

		public static DataSet SelectWorkTimesForDayDS(int userID, DateTime workDate)
		{
			string spName = "WorkTimesForDayListProc";

			SqlParameter[] storedParams = SqlHelperParameterCache.GetSpParameterSet(
				DBManager.GetConnectionString(), spName);
				
			storedParams[0].Value = userID;
			storedParams[1].Value = workDate;

			return SqlHelper.ExecuteDataset(DBManager.GetConnectionString(), spName, storedParams);

		}
	
		#endregion
		
		public static SqlDataReader SelectedEnteredDays(int userID, DateTime startDate, DateTime endDate)
		{
			string spName = "WorkTimesSelectEnteredDates";

			SqlParameter[] storedParams = SqlHelperParameterCache.GetSpParameterSet(DBManager.GetConnectionString(), spName);

			storedParams[0].Value = userID;
			storedParams[1].Value = startDate;
			storedParams[2].Value = endDate;

			return SqlHelper.ExecuteReader(DBManager.GetConnectionString(), CommandType.StoredProcedure, spName, storedParams);
		}
		
    /*
		public static void UpdateWorkTime(int workTimeID, DateTime workDate, int userID, int projectID, int activityID,
			int startTimeID, int endTimeID, string minutes, string adminMinutes, DateTime enteredDate)
		{
			string spName = "WorkTimesUpdProc";

			SqlParameter[] storedParams = SqlHelperParameterCache.GetSpParameterSet(DBManager.GetConnectionString(), spName);

			storedParams[0].Value = workTimeID;
			storedParams[1].Value = workDate;
			storedParams[2].Value = userID;
			storedParams[3].Value = projectID;
			if (activityID==0) storedParams[4].Value = null;
			else storedParams[4].Value = activityID;
			storedParams[5].Value = startTimeID;
			storedParams[6].Value = endTimeID;
			storedParams[7].Value = (minutes==String.Empty) ? null : minutes;
			storedParams[8].Value = (adminMinutes==String.Empty)?null: adminMinutes;
			storedParams[9].Value = enteredDate;
			
			SqlHelper.ExecuteNonQuery(DBManager.GetConnectionString(),CommandType.StoredProcedure, spName, storedParams);
		}
    */

    
    public static SqlDataReader SelectProjectsForPeriod(int showMode)
    {
      string spName = "WorkTimesSelectProjectsForPeriod";

      SqlParameter[] storedParams = SqlHelperParameterCache.GetSpParameterSet(DBManager.GetConnectionString(), spName);
      storedParams[0].Value = showMode;

      return SqlHelper.ExecuteReader(DBManager.GetConnectionString(), CommandType.StoredProcedure, spName, storedParams);
    }
    

		public static SqlDataReader SelectProjectsForDay(DateTime workDay, bool bGrad)
		{
			string spName = "WorkTimesSelectProjectsForDay";

			SqlParameter[] storedParams = SqlHelperParameterCache.GetSpParameterSet(DBManager.GetConnectionString(), spName);
			storedParams[0].Value = workDay;
			storedParams[1].Value =bGrad;
			return SqlHelper.ExecuteReader(DBManager.GetConnectionString(), CommandType.StoredProcedure, spName, storedParams);
		}
		public static SqlDataReader SelectAllProjectsForDay(DateTime workDay, bool bGrad)
		{
			string spName = "WorkTimesSelectAllProjectsForDay";

			SqlParameter[] storedParams = SqlHelperParameterCache.GetSpParameterSet(DBManager.GetConnectionString(), spName);
			storedParams[0].Value = workDay;
			storedParams[1].Value =bGrad;
			return SqlHelper.ExecuteReader(DBManager.GetConnectionString(), CommandType.StoredProcedure, spName, storedParams);
		}
		public static SqlDataReader SelectProjectsForDayAcc(DateTime workDay)
		{
			string spName = "WorkTimesSelectProjectsForDayAcc";

			SqlParameter[] storedParams = SqlHelperParameterCache.GetSpParameterSet(DBManager.GetConnectionString(), spName);
			storedParams[0].Value = workDay;

			return SqlHelper.ExecuteReader(DBManager.GetConnectionString(), CommandType.StoredProcedure, spName, storedParams);
		}
		public static SqlDataReader SelectProjectsForDayAccMain(DateTime workDay)
		{
			string spName = "WorkTimesSelectProjectsForDayAccMain";

			SqlParameter[] storedParams = SqlHelperParameterCache.GetSpParameterSet(DBManager.GetConnectionString(), spName);
			storedParams[0].Value = workDay;

			return SqlHelper.ExecuteReader(DBManager.GetConnectionString(), CommandType.StoredProcedure, spName, storedParams);
		}

		//peppi
		public static Entities.WorkTimeInfo SelectWorkTimesForID(int WorkTimeID)
		{
			string spName = "WorkTimesForIDListProc";

			SqlParameter[] storedParams = SqlHelperParameterCache.GetSpParameterSet(
				DBManager.GetConnectionString(), spName);
			storedParams[0].Value = WorkTimeID;
			
			Entities.WorkTimeInfo wti = new Entities.WorkTimeInfo();
			
			SqlDataReader reader = null;

			try
			{
				reader = SqlHelper.ExecuteReader(DBManager.GetConnectionString(), spName, storedParams);
				if (reader.Read())
				{
					wti.WorkTimeID = reader.GetInt32(0);
					wti.WorkDate = reader.GetDateTime(1);
					wti.UserID = reader.GetInt32(2);
					wti.ProjectID = reader.GetInt32(3);
					wti.ActivityID = reader.IsDBNull(4) ? 0 : reader.GetInt32(4);
					wti.StartTimeID = reader.GetInt32(5);
					wti.EndTimeID = reader.GetInt32(6);
					wti.Minutes = reader.IsDBNull(7) ? String.Empty : reader.GetString(7);
					wti.AdminMinutes = reader.IsDBNull(8) ? String.Empty : reader.GetString(8);
				}
				else return null;
			}

			finally
			{
				if (reader!=null) reader.Close();
			}

			return wti;

		}

/*
		public static DataSet SelectUsersNotEnteredDay(DateTime day, string cs)
		{
			string spName = "WorkTimesUsersNotEnteredDayListProc";

			SqlParameter[] storedParams = SqlHelperParameterCache.GetSpParameterSet(cs, spName);
			storedParams[0].Value = day;

			return SqlHelper.ExecuteDataset(cs, CommandType.StoredProcedure, spName, storedParams);
		}

		public static DataSet SelectUsersNotEnteredDayDS(DateTime day)
		{
			string spName = "WorkTimesUsersNotEnteredDayListProc";

			SqlParameter[] storedParams = SqlHelperParameterCache.GetSpParameterSet(DBManager.GetConnectionString(), spName);
			storedParams[0].Value = day;

			return SqlHelper.ExecuteDataset(DBManager.GetConnectionString(), CommandType.StoredProcedure, spName, storedParams);
		}*/

		public static SqlDataReader SelectUsersNotEnteredDay(DateTime day)
		{
			string spName = "WorkTimesUsersNotEnteredDayListProc";

			SqlParameter[] storedParams = SqlHelperParameterCache.GetSpParameterSet(DBManager.GetConnectionString(), spName);
			storedParams[0].Value = day;

			return SqlHelper.ExecuteReader(DBManager.GetConnectionString(), CommandType.StoredProcedure, spName, storedParams);
		}
		public static SqlDataReader SelectUsersEnteredOnNextDay(DateTime day)
		{
			string spName = "WorkTimesUsersEnteredOnNextDayListProc";

			SqlParameter[] storedParams = SqlHelperParameterCache.GetSpParameterSet(DBManager.GetConnectionString(), spName);
			storedParams[0].Value = day;

			return SqlHelper.ExecuteReader(DBManager.GetConnectionString(), CommandType.StoredProcedure, spName, storedParams);
		}
	}


}
