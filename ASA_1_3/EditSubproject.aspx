﻿<%@ Register TagPrefix="rade" Namespace="Telerik.WebControls" Assembly="RadEditor" %>
<%@ Register TagPrefix="uc1" TagName="EditForm" Src="UserControls/EditForm.ascx" %>
<%@ Page language="c#" Codebehind="EditSubproject.aspx.cs" AutoEventWireup="false" Inherits="Asa.Timesheet.WebPages.EditSubproject" %>
<%@ Register TagPrefix="uc1" TagName="PageHeader" Src="UserControls/PageHeader.ascx" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
	<head runat="server">
		<title>EditProject</title>
		<script language="javascript" src="PopupCalendar.js"></script>
		<meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
		<meta content="C#" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<LINK href="styles/timesheet.css" type="text/css" rel="stylesheet">
	</HEAD>
	<body bottomMargin="0" leftMargin="0" topMargin="0" rightMargin="0">
		<form id="Form1" method="post" runat="server">
			<table height="100%" cellSpacing="0" cellPadding="0" width="100%" border="0">
				<tr height="1">
					<td><uc1:pageheader id="header" runat="server"></uc1:pageheader>
						<uc1:EditForm id="editCtrl" runat="server"></uc1:EditForm></td>
				</tr>
				<tr>
					<td>
						<table height="100%" cellSpacing="2" cellPadding="2" width="800" border="0">
							<tr>
								<td class="menuLeft"><asp:placeholder id="menuHolder" runat="server"></asp:placeholder></td>
								
								<td style="PADDING-LEFT: 5px; PADDING-TOP: 3px" vAlign="top" noWrap>
									<TABLE id="Table3" cellSpacing="0" width="880" border="0">
										<TBODY>
											<TR>
												<td style="HEIGHT: 5px" colSpan="2"></TD>
											</TR>
											<TR>
												<td colSpan="2"><IMG height="1" src="images/dot.gif" width="100%"></TD>
											</TR>
											<TR>
												<td colSpan="2" height="3"></td>
											</TR>
											<TR>
												<td style="WIDTH: 4px"></TD>
												<td>
													<TABLE id="Table6" cellSpacing="0" cellPadding="0" border="0">
														<TR>
															<td style="WIDTH: 97px" noWrap><asp:button id="btnSaveUP" runat="server" CssClass="ActionButton" Text="Запиши"></asp:button><asp:button id="btnEditUP" style="DISPLAY: none" runat="server" CssClass="ActionButton" Text="Редактирай"></asp:button></TD>
															<td style="WIDTH: 321px" noWrap><asp:button id="btnCancelUP" runat="server" CssClass="ActionButton" Text="Откажи"></asp:button>&nbsp;
																</TD>
															<td></TD>
														</TR>
													</TABLE>
												</TD>
											</TR>
											<TR>
												<td style="WIDTH: 4px" height="3"></TD>
											</TR>
											<TR>
												<td colSpan="2"><IMG height="1" src="images/dot.gif" width="100%"></TD>
											</TR>
											<TR>
												<td style="WIDTH: 4px"></TD>
												<td>
													<TABLE id="tblForm" cellSpacing="0" cellPadding="3" width="800" border="0">
														<TBODY>
															<TR>
																<td style="WIDTH: 174px" colSpan="1" rowSpan="1"><asp:label id="lblName" runat="server" EnableViewState="False" Font-Bold="True" CssClass="enterDataLabel">Име на проект:</asp:label></TD>
																<td><asp:label id="lbProject" runat="server" EnableViewState="False" Font-Bold="True" CssClass="enterDataBox"></asp:label>&nbsp;</TD>
															</TR>
															<TR>
																<td style="WIDTH: 174px" colSpan="1" rowSpan="1"><asp:label id="lblProjectCode" runat="server" Font-Bold="True" CssClass="enterDataLabel">Фаза:</asp:label></TD>
																<td><asp:dropdownlist id="ddlTypes" runat="server" CssClass="enterDataBox" Width="350px"></asp:dropdownlist>&nbsp;<IMG height="16" alt="" src="images/required1.gif" width="16"></TD>
															</TR>
															<TR>
																<td vAlign="top"><asp:label id="lblAdministrativeName" runat="server" CssClass="enterDataLabel"> Текст об. записка:</asp:label></TD>
																<td><rade:radeditor id="ftb" Width="594px" Height="150px" LicenseFile="bin/LicenseFile.xml" Runat="server"
																		ToolsWidth="594px" SaveInFile="False" SaveAsXhtml="True" ToolsHeight="25px" ShowHtmlMode="False"
																		Visible="False"></rade:radeditor>
																	<asp:textbox id="txtNotes" runat="server" CssClass="enterDataBox" Width="592px" Height="234px"
																		Rows="3" TextMode="MultiLine"></asp:textbox></TD>
															</TR>
															<TR>
																<td style="WIDTH: 174px" vAlign="top" colSpan="1" rowSpan="1"></TD>
												</TD>
												<td></TD>
											</TR>
							</tr>
							<TR>
								<td style="WIDTH: 174px" vAlign="top" colSpan="1" rowSpan="1"><asp:label id="lblStartDate" runat="server" CssClass="enterDataLabel">Начална дата:</asp:label></TD>
								<td><asp:textbox id="txtStartDate" runat="server" CssClass="enterDataBox" Width="88px" MaxLength="10"></asp:textbox>&nbsp;<IMG id="lkCalendar1" alt="Calendar" src="images/calendar.ico" width="18" align="absBottom"
										runat="server"></TD>
							</TR>
							<TR>
								<td style="WIDTH: 174px" vAlign="top" colSpan="1" rowSpan="1"><asp:label id="Label1" runat="server" CssClass="enterDataLabel">Крайна дата:</asp:label></TD>
								<td><asp:textbox id="txtEndDate" runat="server" CssClass="enterDataBox" Width="88px" MaxLength="10"></asp:textbox>&nbsp;<IMG id="lkCalendar2" alt="Calendar" src="images/calendar.ico" width="18" align="absBottom"
										runat="server"></TD>
							</TR>
							<TR>
								<td style="WIDTH: 174px" colSpan="1" rowSpan="1"><asp:label id="lblArea" runat="server" CssClass="enterDataLabel">Разгъната площ:</asp:label></TD>
								<td><asp:textbox id="txtArea" runat="server" CssClass="enterDataBox" Width="184px"></asp:textbox></TD>
								<td></TD>
							</TR>
						</table>
						<table id="tbl" runat="server">
							<TR>
								<td style="WIDTH: 174px; HEIGHT: 15px"><asp:label id="lblClient" runat="server" CssClass="enterDataLabel">Схема на плащане: </asp:label></TD>
								<td style="HEIGHT: 15px"><asp:dropdownlist id="ddlScheme" runat="server" CssClass="enterDataBox" Width="350px"></asp:dropdownlist>&nbsp;<IMG height="16" alt="" src="images/required1.gif" width="16"></TD>
								<td style="HEIGHT: 15px"></TD>
							</TR>
							<TR>
								<td style="WIDTH: 174px"><asp:label id="lblAddress" runat="server" CssClass="enterDataLabel">Договорена цена:</asp:label></TD>
								<td><asp:textbox id="txtRate" runat="server" CssClass="enterDataBox" Width="184px"></asp:textbox></TD>
								<td></TD>
							</TR>
							<TR>
								<td style="WIDTH: 174px"></TD>
								<td><asp:button id="btn" runat="server" CssClass="ActionButton" Text="Преизчисли"></asp:button></TD>
								<td></TD>
							</TR>
							<TR>
								<td style="WIDTH: 174px"><asp:label id="lblManager" runat="server" EnableViewState="False" CssClass="enterDataLabel">Обща сума(EUR):</asp:label></TD>
								<td><asp:label id="lbEUR" runat="server" EnableViewState="False" CssClass="enterDataBox"></asp:label>&nbsp;&nbsp;</TD>
								<td></TD>
							</TR>
							<TR>
								<td style="WIDTH: 174px"><asp:label id="Label3" runat="server" EnableViewState="False" CssClass="enterDataLabel">Обща сума(BGN):</asp:label></TD>
								<td><asp:label id="lbBGN" runat="server" EnableViewState="False" CssClass="enterDataBox"></asp:label></TD>
								<td></TD>
							</TR>
						</table>
						<table id="tblPayments" runat="server">
							<TR>
								<td style="WIDTH: 174px"></TD>
								<td><asp:label id="Label2" runat="server" EnableViewState="False" CssClass="enterDataBox">Разпределение на плащанията:</asp:label></TD>
								<td></TD>
							</TR>
							<TR>
								<td style="WIDTH: 174px" vAlign="top"><asp:label id="Label4" runat="server" CssClass="enterDataLabel" Height="24px">Процент:</asp:label><asp:label id="Label5" runat="server" CssClass="enterDataLabel" Height="26px"> Сума:</asp:label><asp:label id="Label6" runat="server" CssClass="enterDataLabel" Height="22px"> Платено:</asp:label><asp:label id="Label7" runat="server" CssClass="enterDataLabel" Height="22px">Дата на плащане:</asp:label></TD>
								<td><asp:datalist id="dlPayments" runat="server" RepeatDirection="Horizontal">
										<ItemTemplate>
											<asp:textbox id=txtPer runat="server" CssClass="enterDataBox" Width="100px" Height="21px" Text='<%#Asa.Timesheet.WebPages.UIHelpers.FormatDecimaln((decimal) DataBinder.Eval(Container, "DataItem.PaymentPercent")) %>' BackColor="#FFFF99">
											</asp:textbox><BR>
											<asp:textbox id=txtAmount runat="server" CssClass="enterDataBox" Width="100px" Height="20px" Text='<%# Asa.Timesheet.WebPages.UIHelpers.FormatDecimal2((decimal)DataBinder.Eval(Container, "DataItem.Amount")) %>'>
											</asp:textbox><BR>
											<asp:CheckBox id="ckPaid" runat="server" Text="ДА" Checked='<%# DataBinder.Eval(Container, "DataItem.Done") %>'>
											</asp:CheckBox><BR>
											<asp:textbox id=txtDate runat="server" CssClass="enterDataBox" Width="80px" Height="20px" Text='<%#  Asa.Timesheet.Data.Util.TimeHelper.FormatDate((DateTime)DataBinder.Eval(Container, "DataItem.PaymentDate")) %>'>
											</asp:textbox><IMG id="lkCal" alt="Calendar" src="images/calendar.ico" width="18" align="absBottom"
												runat="server"><br>
											<asp:textbox id="txtNotes1" runat="server" CssClass="enterDataBox" TextMode=MultiLine Width="100px" Height="60px" Text='<%#  DataBinder.Eval(Container, "DataItem.Notes") %>'>
											</asp:textbox>
										</ItemTemplate>
									</asp:datalist><asp:button id="btnNewPayment" runat="server" CssClass="ActionButton" Width="144px"></asp:button></TD>
								<td></TD>
							</TR>
						</table>
					</td>
				</tr>
				<TR>
					<td style="HEIGHT: 5px" colSpan="2"></TD>
				</TR>
				<TR>
					<td colSpan="2"><IMG height="1" src="images/dot.gif" width="100%"></TD>
				</TR>
				<TR>
					<td colSpan="2" height="3"></td>
				</TR>
				<TR>
					<td style="WIDTH: 4px"></TD>
					<td>
						<TABLE id="Table5" cellSpacing="0" cellPadding="0" border="0">
							<TR>
								<td style="WIDTH: 97px" noWrap><asp:button id="btnSave" runat="server" CssClass="ActionButton" Text="Запиши"></asp:button><asp:button id="btnEdit" style="DISPLAY: none" runat="server" CssClass="ActionButton" Text="Редактирай"></asp:button></TD>
								<td style="WIDTH: 321px" noWrap><asp:button id="btnCancel" runat="server" CssClass="ActionButton" Text="Откажи"></asp:button>&nbsp;
									<asp:button id="btnNote" runat="server" CssClass="ActionButton" Text="Об. записка"></asp:button></TD>
								<td><asp:button id="btnDelete" runat="server" CssClass="ActionButton" Text="Изтрий"></asp:button></TD>
							</TR>
						</TABLE>
					</TD>
				</TR>
				<TR>
					<td style="WIDTH: 4px" height="3"></TD>
				</TR>
				<TR>
					<td colSpan="2"><IMG height="1" src="images/dot.gif" width="100%"></TD>
				</TR>
			</table>
			&nbsp;
			<asp:label id="lblInfo" runat="server" EnableViewState="False" CssClass="InfoLabel"></asp:label><asp:label id="lblError" runat="server" EnableViewState="False" CssClass="ErrorLabel" ForeColor="Red"></asp:label><INPUT id="hdnInfo" type="hidden" name="Hidden1" runat="server"><INPUT id="hdnClientID" type="hidden" name="Hidden1" runat="server">
			<asp:CheckBox id="ckPhases" runat="server" Visible="False"></asp:CheckBox></TD></TR></TBODY></TABLE></TD></TR></TBODY></TABLE></form>
	</body>
</HTML>
