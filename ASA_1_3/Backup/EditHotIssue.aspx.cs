using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using log4net;
using Asa.Timesheet.Data;
using Asa.Timesheet.Data.Entities;
using Asa.Timesheet.Data.Util;
using Asa.Timesheet.WebPages.Reports;
using System.Data.SqlClient;

namespace Asa.Timesheet.WebPages
{
	/// <summary>
	/// Summary description for EditHotIssue.
	/// </summary>
	public enum AssignedTo
	{
		User =1,
		Client,
		Subcontracter
	}
	public class EditHotIssue : TimesheetPageBase	
	{
	
		#region WebControls
		private static readonly ILog log = LogManager.GetLogger(typeof(EditHotIssue));
		protected System.Web.UI.WebControls.PlaceHolder menuHolder;
		protected System.Web.UI.WebControls.Label Label1;
		protected System.Web.UI.WebControls.DropDownList ddlProjectsStatus;
		protected System.Web.UI.WebControls.Label Label8;
		protected System.Web.UI.WebControls.DropDownList ddlBuildingTypes;
		protected System.Web.UI.WebControls.DropDownList ddlProject;
		protected System.Web.UI.WebControls.Label lbMailingType;
		protected System.Web.UI.WebControls.Label lbNotes;
		protected System.Web.UI.WebControls.TextBox txtNotes;
		protected System.Web.UI.WebControls.Label lbScanDocNew;
		protected System.Web.UI.WebControls.Label lbSendLetter;
		protected System.Web.UI.WebControls.Button btnSave;
		protected System.Web.UI.WebControls.Button btnDelete;
		protected System.Web.UI.WebControls.Label lblInfo;
		protected System.Web.UI.WebControls.Label lblError;
		protected System.Web.UI.HtmlControls.HtmlImage lkCalendar1;
		protected System.Web.UI.WebControls.DropDownList ddlClients;
		protected System.Web.UI.WebControls.DropDownList Dropdownlist1;
		protected System.Web.UI.WebControls.DropDownList Dropdownlist2;
		protected System.Web.UI.WebControls.DropDownList Dropdownlist3;
		protected System.Web.UI.WebControls.Label lbHotIssueName;
		protected System.Web.UI.WebControls.Label lbHotIssueIme;
		protected System.Web.UI.WebControls.TextBox txtHotIssueIme;
		protected System.Web.UI.WebControls.Label lbHotIssueCategory;
		protected System.Web.UI.WebControls.DropDownList ddlHotIssueCategory;
		protected System.Web.UI.WebControls.Label lbDiscription;
		protected System.Web.UI.WebControls.TextBox txtDiscription;
		protected System.Web.UI.WebControls.Label lbAssigned;
		protected System.Web.UI.WebControls.DropDownList ddlAssigned;
		protected System.Web.UI.WebControls.Label lbDeadline;
		protected System.Web.UI.WebControls.TextBox txtDeadline;
		//protected System.Web.UI.WebControls.Label lbPriority;
		//protected System.Web.UI.WebControls.DropDownList ddlPriority;
		protected System.Web.UI.WebControls.Label lbStatus;
		protected System.Web.UI.WebControls.DropDownList ddlStatus;
		protected System.Web.UI.HtmlControls.HtmlImage Img3;
		protected System.Web.UI.HtmlControls.HtmlImage Img4;
		protected System.Web.UI.HtmlControls.HtmlImage Img5;
		protected System.Web.UI.HtmlControls.HtmlImage Img6;
		protected Asa.Timesheet.WebPages.UserControls.PageHeader header;
		protected System.Web.UI.WebControls.Label lbProjectName;
		protected System.Web.UI.WebControls.Label lbMainID;
		protected System.Web.UI.WebControls.Label Label2;
		protected System.Web.UI.WebControls.Label Label5;
		protected System.Web.UI.WebControls.Label Label6;
		protected System.Web.UI.WebControls.Label Label7;
		protected System.Web.UI.WebControls.Label Label9;
		protected System.Web.UI.WebControls.Label Label10;
		protected System.Web.UI.WebControls.Label Label12;
		protected System.Web.UI.WebControls.Label Label13;
		protected System.Web.UI.WebControls.Label DLlbName;
		protected System.Web.UI.WebControls.Label DLlbProjectName;
		protected System.Web.UI.WebControls.Label DLlbCategoryName;
		protected System.Web.UI.WebControls.Label DLlbDiscription;
		protected System.Web.UI.WebControls.Label DLlbAssignedTo;
		protected System.Web.UI.WebControls.Label DLlbDeadline;
		//protected System.Web.UI.WebControls.Label DLlbPriorityName;
		protected System.Web.UI.WebControls.Label DLlbStatus;
		protected Asa.Timesheet.WebPages.UserControls.EditForm editCtrl;
		protected System.Web.UI.WebControls.DataList dlOldIssues;
		protected System.Web.UI.WebControls.Label lbComment;
		protected System.Web.UI.WebControls.TextBox  txtComment;
		protected System.Web.UI.WebControls.Label lbHotIssueImeText;
		protected System.Web.UI.WebControls.Label lbDiscriptionText;
		protected System.Web.UI.WebControls.Label lbToClient;
		protected System.Web.UI.WebControls.CheckBox cbToClient;
		protected System.Web.UI.WebControls.Label txtProjectNameText;
		protected System.Web.UI.WebControls.Label lbCategorySet;
		protected System.Web.UI.WebControls.Button btnEdit;
		protected System.Web.UI.WebControls.Label lbAssignedBy;
		protected System.Web.UI.WebControls.Label lbBy;
		protected System.Web.UI.WebControls.CheckBox ckStop;
		protected System.Web.UI.WebControls.Label lbDate;
		protected System.Web.UI.WebControls.Button Button1;
		protected System.Web.UI.WebControls.TextBox txtAssignedToText;
		protected System.Web.UI.WebControls.LinkButton linkBack;
		protected System.Web.UI.WebControls.ImageButton imgBack;
		protected System.Web.UI.WebControls.Label Label11;
		protected System.Web.UI.WebControls.Label Label14;
		protected System.Web.UI.WebControls.DropDownList ddlStandardCat;
		protected System.Web.UI.WebControls.Label lbCreated;
		protected System.Web.UI.WebControls.TextBox txtCreated;
		protected System.Web.UI.WebControls.Label Label15;
		protected System.Web.UI.HtmlControls.HtmlImage lkDateCreated;
		//protected System.Web.UI.WebControls.TextBox txtPosition;
		protected System.Web.UI.HtmlControls.HtmlImage imgRequired;
		protected System.Web.UI.WebControls.TextBox txtPosition;
		protected System.Web.UI.WebControls.TextBox txtCategoryNew;


		#endregion
		
		#region PageLoad

        private void Page_Load(object sender, System.EventArgs e)
        {
            //if(!LoggedUser.IsLeader) ErrorRedirect("No right to current page!");

            lkCalendar1.Attributes["onclick"] = TimeHelper.InvokePopupCal(txtDeadline);
            lkDateCreated.Attributes["onclick"] = TimeHelper.InvokePopupCal(txtCreated);

            if (!this.IsPostBack)
            {
                if (Page.IsPostBack == false)
                {
                    // Set up form for data change checking when
                    // first loaded.
                    this.CheckForDataChanges = true;
                    this.BypassPromptIds =
                        new string[] { "btnDel", "ckStop", "btnSave", "btnEdit", "btnDelete", "ddlProject", "ddlBuildingTypes", "ddlProjectsStatus", "btnChooseNew" };
                }
                header.UserName = this.LoggedUser.UserName;
                UIHelpers.LoadBuildingTypes(ddlBuildingTypes);
                UIHelpers.LoadProjectStatus(ddlProjectsStatus);
                LoadProjects();
                LoadHotIssuesObjects();
                if (UIHelpers.GetPIDParam() != -1 && ddlProject.Items.FindByValue(UIHelpers.GetPIDParam().ToString()) != null)
                {
                    ddlProject.SelectedValue = UIHelpers.GetPIDParam().ToString();
                    //ddlProject.Enabled = false;
                    //Label1.Visible = Label8.Visible = ddlProjectsStatus.Visible = ddlBuildingTypes.Visible = false;
                    UIHelpers.LoadHotIssueCategories(ddlHotIssueCategory, UIHelpers.ToInt(ddlProject.SelectedValue));
                    if (Request.Params["themeID"] != "")
                    {
                        if (ddlHotIssueCategory.Items.FindByValue(Request.Params["themeID"]) != null)
                            ddlHotIssueCategory.SelectedValue = Request.Params["themeID"];
                    }
                    UIHelpers.LoadHotIssueAssigneds(ddlAssigned, UIHelpers.ToInt(ddlProject.SelectedValue));
                }


                //InitEdit();
                if (UIHelpers.GetIDParam() > 0)
                {
                    DateTime dtTask = LoadFromID(UIHelpers.GetIDParam());
                    BindGrid();
                    //					BindGrid();				
                    if (Page.Request.Params["ASA"] == "1")
                        header.PageTitle = Resource.ResourceManager["editHotIssue_PageTitle_ASA"];
                    else
                        header.PageTitle = Resource.ResourceManager["editHotIssue_PageTitle"];
                    if (UIHelpers.CanEditIssue(dtTask))
                    {
                        txtHotIssueIme.Visible = txtDiscription.Visible =
                            Label1.Visible = ddlProjectsStatus.Visible = Label8.Visible =
                            ddlBuildingTypes.Visible = ddlProject.Visible = 
                            ddlHotIssueCategory.Visible =
                             true;
                        txtCategoryNew.Visible = true;
                        lbDiscriptionText.Visible = txtProjectNameText.Visible = lbCategorySet.Visible = lbHotIssueImeText.Visible = false;
                        //SetChooseNew();
                    }
                    else
                    {
                        txtHotIssueIme.Visible = txtDiscription.Visible =
                            Label1.Visible = ddlProjectsStatus.Visible = Label8.Visible =
                            ddlBuildingTypes.Visible = ddlProject.Visible = 
                            ddlHotIssueCategory.Visible = txtCategoryNew.Visible =
                            false;
                    }
                    //cbToClient.Enabled=false;

                }
                else
                {
                    Button1.Visible = false;
                    lbAssignedBy.Visible = lbBy.Visible = false;
                    btnSave.Text = Resource.ResourceManager["btnNewMeeting"];
                    ddlProjectsStatus.SelectedIndex = 1;
                    if (Page.Request.Params["ASA"] == "1")
                        header.PageTitle = Resource.ResourceManager["newHotIssue_PageTitle_ASA"];
                    else
                        header.PageTitle = Resource.ResourceManager["newHotIssue_PageTitle"];
                    txtProjectNameText.Visible = lbHotIssueImeText.Visible = lbDiscriptionText.Visible =
                        dlOldIssues.Visible = lbCategorySet.Visible = false;
                    //SetChooseNew();
                }

                //				if(UIHelpers.GetPIDParam() != -1)
                //				{
                //					lblName.Visible = ddlProject.Visible = 
                //						Label8.Visible = ddlBuildingTypes.Visible =
                //						ddlProjectsStatus.Visible = Label1.Visible = false;
                //					ListItem li = ddlProject.Items.FindByValue(UIHelpers.GetPIDParam().ToString());
                //					if(li!=null)
                //					{
                //						ddlProject.SelectedIndex = ddlProject.Items.IndexOf(li);
                //					}
                //					else
                //						ErrorRedirect(Resource.ResourceManager["ErrorLoadProject"]);
                //					ddlProject.Enabled = false;
                //					
                //					
                //header.PageTitle =Resource.ResourceManager["editCorespondency_PageTitle"]+ " - " +ddlProject.SelectedItem.Text;
                //
                //				}
                //				if(!(LoggedUser.HasPaymentRights || LoggedUser.IsSecretary|| LoggedUser.IsSecretaryReally||LoggedUser.IsLayer))
                //				{
                //					ErrorRedirect(Resource.ResourceManager["noRightsAlert"]);
                //				}
            }
            else

                UIHelpers.LoadBuildingTypes(ddlBuildingTypes.SelectedValue, ddlBuildingTypes, (ProjectsData.ProjectsByStatus)int.Parse(ddlProjectsStatus.SelectedValue));

            UIHelpers.CreateMenu(menuHolder, LoggedUser);

        }

        protected string AssignedToText(object userID, object userType, object text)
        {
            if ((!(userID is int)) || (!(userType is int)) || (!(text is string)))
                return "";
            string sText = (string)text;
            if (sText.Trim().Length > 0)
            {
                sText = ", " + sText;
            }
            return UIHelpers.GetHotIssueUserNameAssigned((int)userID, (int)userType) + sText;
        }
	
		#endregion
	
		#region Web Form Designer generated code
        override protected void OnInit(EventArgs e)
        {
            //
            // CODEGEN: This call is required by the ASP.NET Web Form Designer.
            //
            InitializeComponent();
            base.OnInit(e);
        }
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
        private void InitializeComponent()
        {
            this.linkBack.Click += new System.EventHandler(this.linkBack_Click);
            this.ddlProjectsStatus.SelectedIndexChanged += new System.EventHandler(this.ddlProjectsStatus_SelectedIndexChanged);
            this.ddlBuildingTypes.SelectedIndexChanged += new System.EventHandler(this.ddlBuildingTypes_SelectedIndexChanged);
            this.ddlProject.SelectedIndexChanged += new System.EventHandler(this.ddlProject_SelectedIndexChanged);
            this.ckStop.CheckedChanged += new System.EventHandler(this.ckStop_CheckedChanged);
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            this.Button1.Click += new System.EventHandler(this.Button1_Click);
            this.Load += new System.EventHandler(this.Page_Load);

        }
		#endregion

		#region EventHandlers
        private void ddlProjectsStatus_SelectedIndexChanged(object sender, System.EventArgs e)
        {
            LoadProjects();
        }

        private void ddlBuildingTypes_SelectedIndexChanged(object sender, System.EventArgs e)
        {
            LoadProjects();
        }

        private void btnSave_Click(object sender, System.EventArgs e)
        {
            #region validate
            if (txtHotIssueIme.Text.Length == 0)
            {
                AlertFieldNotEntered(lbHotIssueIme);
                Dirty = true;
                return;
            }
            if (!(UIHelpers.ToInt(ddlProject.SelectedValue) > 0))
            {
                AlertFieldNotEntered(lbProjectName);
                Dirty = true;
                return;
            }
            if (!(UIHelpers.ToInt(ddlHotIssueCategory.SelectedValue) > 0) && !(UIHelpers.ToInt(ddlStandardCat.SelectedValue) > 0)
                && txtCategoryNew.Text.Length == 0)
            {
                AlertFieldNotEntered(lbHotIssueCategory);
                Dirty = true;
                return;
            }

            //		//	if(ddlAssigned.SelectedValue=="0")
            //			{
            //				AlertFieldNotEntered(lbAssigned);
            //				Dirty = true;
            //				return;
            //			}
            //	if (txtDeadline.Text.Trim()=="" ||UIHelpers.GetDate(txtDeadline)==Constants.DateMax) 
            //	{
            //		AlertFieldNotEntered(lbDeadline);
            //		Dirty=true;
            //		return;
            //	}
            //			if(!(UIHelpers.ToInt(ddlPriority.SelectedValue)>0)) 
            //			{
            //				AlertFieldNotEntered(lbPriority);
            //				Dirty=true;
            //				return;
            //			}		
            if (!(UIHelpers.ToInt(ddlStatus.SelectedValue) > 0))
            {
                AlertFieldNotEntered(lbStatus);
                Dirty = true;
                return;
            }
            #endregion

            int index = ddlAssigned.SelectedValue.IndexOf("_");
            int userType = -1;
            int userID = -1;
            if (index != -1)
            {
                userType = UIHelpers.ToInt(ddlAssigned.SelectedValue.Substring(0, index));
                userID = UIHelpers.ToInt(ddlAssigned.SelectedValue.Substring(index + 1));
            }
            if (UIHelpers.ToInt(lbMainID.Text) > 0)
                HotIssueDAL.Delete(UIHelpers.GetIDParam());
            int categoryID = UIHelpers.ToInt(ddlHotIssueCategory.SelectedValue);
            int standardCatID = UIHelpers.ToInt(ddlStandardCat.SelectedValue);
            if (txtCategoryNew.Text.Trim() != "")
            {
                categoryID = HotIssueCategoryDAL.Insert(new HotIssueCategoryData(-1, txtCategoryNew.Text, UIHelpers.ToInt(ddlProject.SelectedValue)));
                if (UIHelpers.GetMIDParam() != -1)
                {
                    MeetingCategorieData mcd = new MeetingCategorieData(-1, UIHelpers.GetMIDParam(), categoryID);
                    MeetingCategorieDAL.Save(mcd);
                }
            }
            else if (standardCatID > 0 && ddlStandardCat.SelectedItem.Text.ToLower() != ddlHotIssueCategory.SelectedItem.Text.ToLower())
            {
                ListItem li = ddlHotIssueCategory.Items.FindByText(ddlStandardCat.SelectedItem.Text);
                if (li != null)
                    categoryID = int.Parse(li.Value);
                else
                {
                    categoryID = HotIssueCategoryDAL.Insert(new HotIssueCategoryData(-1, ddlStandardCat.SelectedItem.Text, UIHelpers.ToInt(ddlProject.SelectedValue)));
                    if (UIHelpers.GetMIDParam() != -1)
                    {
                        MeetingCategorieData mcd = new MeetingCategorieData(-1, UIHelpers.GetMIDParam(), categoryID);
                        MeetingCategorieDAL.Save(mcd);
                    }
                }

            }
            HotIssueData hid = new HotIssueData(-1, UIHelpers.ToInt(ddlProject.SelectedValue), categoryID, txtHotIssueIme.Text, txtDiscription.Text, txtComment.Text, userID, userType, TimeHelper.GetDate(txtDeadline.Text)/*,UIHelpers.ToInt(ddlPriority.SelectedValue)*/, UIHelpers.ToInt(ddlStatus.SelectedValue), UIHelpers.ToInt(lbMainID.Text), true, LoggedUser.FullName, DateTime.Now, cbToClient.Checked);
            hid.ASA = Page.Request.Params["ASA"] == "1";
            //hid.HotIssuePosition=txtPosition.Text;
            if (txtCreated.Text != "")
                hid.CreatedDate = UIHelpers.GetDate(txtCreated);
            hid.AssignedBy = LoggedUser.UserID;
            hid.AssignedToMore = txtAssignedToText.Text;
            if (ckStop.Checked)
            {
                hid.ExecutionStopped = DateTime.Now;
                if (lbDate.Text != "")
                    hid.ExecutionStopped = UIHelpers.GetDate(lbDate.Text);
            }
            HotIssueDAL.Save(hid);
            HotIssueUDL.ExecuteHotIssueUpdateClientProc(hid.HotIssueIDMain, cbToClient.Checked);

                UserInfo uiTo = UsersData.SelectUserByID(hid.AssignedTo);
                string sAssignedTo = "";
                string sAssignedToMail = "";
                if (uiTo != null && uiTo.UserID > 0)
                {
                    sAssignedTo = uiTo.FullName;
                    sAssignedToMail = uiTo.Mail;
                }


                UserInfo ui = UsersData.SelectUserByID(hid.AssignedBy);
                string sAssignedBy = "";
                string sAssignedByMail = "";
                if (ui != null && ui.UserID > 0)
                {
                    sAssignedBy = ui.FullName;
                    sAssignedByMail = ui.Mail;
                }

                MailBO.SendMailASAHotIssue(ddlProject.SelectedItem.Text, txtCategoryNew.Text, txtHotIssueIme.Text, txtDiscription.Text, txtComment.Text, sAssignedBy, sAssignedByMail, sAssignedTo, sAssignedToMail, ddlStatus.SelectedItem.Text, hid.HotIssueID.ToString());


            log.Info(string.Format("HotIssue {0} has been INSERTED by {1}", hid.HotIssueID, string.Concat(LoggedUser.FullName, " ( userID ", LoggedUser.UserID, " )")));
            //string sASA="";
            //			if(hid.ASA)
            //				sASA="&ASA=1";
            if (UIHelpers.GetMIDParam() != -1)
            {
                //if(UIHelpers.GetIDParam()<=0)
                {
                    MeetingHotIssueData mht = new MeetingHotIssueData(-1, UIHelpers.GetMIDParam(), hid.HotIssueID);
                    MeetingHotIssueDAL.Save(mht);
                }
                Response.Redirect("MeetingIssues.aspx" + "?id=" + UIHelpers.GetMIDParam());
            }
            else if (UIHelpers.GetPIDParam() != -1)
                Response.Redirect("HotIssues.aspx" + "?pid=" + UIHelpers.GetPIDParam());
            else
                Response.Redirect("HotIssues.aspx");
        }

        private void btnCancel_Click(object sender, System.EventArgs e)
        {
            if (UIHelpers.GetMIDParam() != -1)
                Response.Redirect("MeetingIssues.aspx" + "?id=" + UIHelpers.GetMIDParam());
            else if (UIHelpers.GetPIDParam() != -1)
                Response.Redirect("HotIssues.aspx" + "?pid=" + UIHelpers.GetPIDParam());
            else
                Response.Redirect("HotIssues.aspx");
        }
		
//		private void btnChooseNew_Click(object sender, System.EventArgs e)
//		{
//			SetChooseNew();
//		}
		
//		private void SetChooseNew()
//		{
//			if(txtCategoryNew.Visible)
//			{
//				txtCategoryNew.Visible = false;
//				ddlHotIssueCategory.Visible = true;
//				btnChooseNew.Text= Resource.ResourceManager["btnChooseNew_Choose"];
//			}
//			else
//			{
//				txtCategoryNew.Visible = true;
//				ddlHotIssueCategory.Visible = false;
//				btnChooseNew.Text= Resource.ResourceManager["btnChooseNew_New"];
//			}
//		}
		#endregion
	
		#region DataBase
        private DateTime LoadFromID(int ID)
        {
            HotIssueData hid = HotIssueDAL.Load(ID);
            lbMainID.Text = hid.HotIssueIDMain.ToString();
            txtHotIssueIme.Text = hid.HotIssueName;
            lbHotIssueImeText.Text = hid.HotIssueName;
            if (hid == null)
                ErrorRedirect("Error load HotIssue.");
            if (ddlProject.Items.FindByValue(hid.ProjectID.ToString()) != null)
                ddlProject.SelectedValue = hid.ProjectID.ToString();
            txtProjectNameText.Text = hid.ProjectName;
            UIHelpers.LoadHotIssueCategories(ddlHotIssueCategory, UIHelpers.ToInt(ddlProject.SelectedValue));
            if (ddlHotIssueCategory.Items.FindByValue(hid.HotIssueCategoryID.ToString()) != null)
                ddlHotIssueCategory.SelectedValue = hid.HotIssueCategoryID.ToString();
            if (ddlStandardCat.Items.FindByText(hid.HotIssueCategoryName) != null)
            {
                ListItem li = ddlStandardCat.Items.FindByText(hid.HotIssueCategoryName);
                ddlStandardCat.SelectedValue = li.Value;
            }
            lbCategorySet.Text = ddlHotIssueCategory.SelectedItem.Text;
            //			if(ddlPriority.Items.FindByValue(hid.HotIssuePriorityID.ToString())!=null)
            //				ddlPriority.SelectedValue=hid.HotIssuePriorityID.ToString();
            if (ddlStatus.Items.FindByValue(hid.HotIssueStatusID.ToString()) != null)
                ddlStatus.SelectedValue = hid.HotIssueStatusID.ToString();
            txtDeadline.Text = TimeHelper.FormatDate(hid.HotIssueDeadline);
            txtDiscription.Text = hid.HotIssueDiscription;

            txtAssignedToText.Text = hid.AssignedToMore;
            lbDiscriptionText.Text = hid.HotIssueDiscription;
            UIHelpers.LoadHotIssueAssigneds(ddlAssigned, UIHelpers.ToInt(ddlProject.SelectedValue));
            string AssTo = string.Concat(hid.AssignedToType, "_", hid.AssignedTo);
            if (ddlAssigned.Items.FindByValue(AssTo) != null)
                ddlAssigned.SelectedValue = AssTo;
            cbToClient.Checked = hid.ToClient;
            UserInfo ui = UsersData.SelectUserByID(hid.AssignedBy);
            if (ui != null && ui.UserID > 0)
                lbBy.Text = ui.FullName;
            if (hid.ExecutionStopped != new DateTime() && hid.ExecutionStopped != Constants.DateMax)
            {
                ckStop.Checked = true;
                lbDate.Text = UIHelpers.FormatDate(hid.ExecutionStopped);
            }
            return hid.CreatedDate;
            //txtComment.Text = hid.HotIssueComment;
        }
        public string FormatDate(object d)
        {
            if (d == null || d == DBNull.Value || !(d is DateTime))
                return "";
            DateTime dt = (DateTime)d;
            if (dt == Constants.DateMin || dt == Constants.DateMax || dt == new DateTime())
                return "";
            return dt.ToString("dd.MM.yyyy");
        }
        private void LoadProjects()
        {
            UIHelpers.LoadProjects(ddlProject, ddlProjectsStatus, ddlBuildingTypes);

        }
        private bool LoadHotIssuesObjects()
        {
            UIHelpers.LoadHotIssueStatuses(ddlStatus);
            ddlStandardCat.DataSource = SubprojectsUDL.SelectSubcontracterTypes();
            ddlStandardCat.DataValueField = "SubcontracterTypeID";
            ddlStandardCat.DataTextField = "SubcontracterType";
            ddlStandardCat.DataBind();
            ddlStandardCat.Items.Insert(0, new ListItem("<" + Resource.ResourceManager["btnChooseNew_New"] + ">", "-1"));
            //UIHelpers.LoadHotIssuePriorities(ddlPriority);
            return true;
        }
        private void BindGrid()
        {
            HotIssuesVector hiv = HotIssueDAL.LoadCollectionOldIssues(UIHelpers.ToInt(lbMainID.Text), Page.Request.Params["ASA"] == "1");
            dlOldIssues.DataSource = hiv;
            dlOldIssues.DataBind();
        }
		#endregion

        private void ddlProject_SelectedIndexChanged(object sender, System.EventArgs e)
        {
            UIHelpers.LoadHotIssueCategories(ddlHotIssueCategory, UIHelpers.ToInt(ddlProject.SelectedValue));
            UIHelpers.LoadHotIssueAssigneds(ddlAssigned, UIHelpers.ToInt(ddlProject.SelectedValue));

        }

        private void ckStop_CheckedChanged(object sender, System.EventArgs e)
        {
            if (!ckStop.Checked && lbDate.Text != "")
            {
                DateTime dtDeadline = UIHelpers.GetDate(txtDeadline.Text);
                DateTime dtDate = UIHelpers.GetDate(lbDate.Text);
                TimeSpan ts = DateTime.Now - dtDate;
                if (ts.TotalDays > 0)
                {
                    txtDeadline.Text = UIHelpers.FormatDate(dtDeadline.AddDays(ts.TotalDays));
                }
            }
        }

        private void Button1_Click(object sender, System.EventArgs e)
        {
            string sASA = "";
            if (Page.Request.Params["ASA"] == "1")
                sASA = "&ASA=1";
            string sPID = ddlProject.SelectedValue;
            string sThemeID = ddlHotIssueCategory.SelectedValue;
            Response.Redirect("EditHotIssue.aspx?&pid=" + sPID + sASA + "&themeID=" + sThemeID);

        }

        private void linkBack_Click(object sender, System.EventArgs e)
        {
            if (UIHelpers.GetMIDParam() != -1)
                Response.Redirect("MeetingIssues.aspx" + "?id=" + UIHelpers.GetMIDParam());
            else if (UIHelpers.GetPIDParam() != -1)
                Response.Redirect("HotIssues.aspx" + "?pid=" + UIHelpers.GetPIDParam());
            else
                Response.Redirect("HotIssues.aspx");
        }

	
//		private void InitEdit()
//		{
//			if (UIHelpers.GetIDParam()<=0) return;
//
//			string vmButtons = this.btnEdit.ClientID;
//			string emButtons = this.btnSave.ClientID+";"+this.btnDelete.ClientID;
//
//			string exl = string.Empty;
//
//			editCtrl.InitEdit("tblForm", vmButtons, emButtons, exl);
//		}
		
	}


}
