using System;
using System.Collections;
using System.Text;

namespace Asa.Timesheet.Data
{
	public class ProjectSubcontracterActivitieData
	{
		private int _projectSubcontracterActivityID = -1;
		private int _projectSubcontracterID = -1;
		private int _subactivityID = -1;
		private bool _done;
		private string _subactivity = "";
		/// <summary>
		/// Initialize an new empty Login object.
		/// </summary>
		public ProjectSubcontracterActivitieData()
		{
		}
		
		/// <summary>
		/// Initialize a new Login object with the given parameters.
		/// </summary>
		public ProjectSubcontracterActivitieData(int projectSubcontracterActivityID, int projectSubcontracterID, int subactivityID, bool done)
		{
			this._projectSubcontracterActivityID = projectSubcontracterActivityID;
			this._projectSubcontracterID = projectSubcontracterID;
			this._subactivityID = subactivityID;
			this._done = done;
		}	
		
	  
		public override string ToString()
		{
			StringBuilder builder = new StringBuilder();
			builder.Append("_projectSubcontracterActivityID: " + _projectSubcontracterActivityID.ToString()).Append("\r\n");
			builder.Append("_projectSubcontracterID: " + _projectSubcontracterID.ToString()).Append("\r\n");
			builder.Append("_subactivityID: " + _subactivityID.ToString()).Append("\r\n");
			builder.Append("_done: " + _done.ToString()).Append("\r\n");
			builder.Append("\r\n");
			return builder.ToString();
		}	
		public object GetByIndex(int nIndex)
		{
			switch(nIndex)
			{
				case 0:
					return _projectSubcontracterActivityID;
				case 1:
					return _projectSubcontracterID;
				case 2:
					return _subactivityID;
				case 3:
					return _done;
			}
			return null;
		}
		#region Properties
	
		public int ProjectSubcontracterActivityID
		{
			get { return _projectSubcontracterActivityID; }
			set { _projectSubcontracterActivityID = value; }
		}
		public string Subactivity
		{
			get { return _subactivity; }
			set { _subactivity = value; }
		}
	
		public int ProjectSubcontracterID
		{
			get { return _projectSubcontracterID; }
			set { _projectSubcontracterID = value; }
		}
	
		public int SubactivityID
		{
			get { return _subactivityID; }
			set { _subactivityID = value; }
		}
	
		public bool Done
		{
			get { return _done; }
			set { _done = value; }
		}
	
		#endregion
	}
}


