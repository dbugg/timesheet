using System;
using System.Collections;
using System.Text;

namespace Asa.Timesheet.Data
{
	public class AutomobileData
	{
		private int _automobileID = -1;
		private int _userID = -1;
		private string _name = String.Empty;
		private string _regNumber = String.Empty;
		private DateTime _civilDate;
		private int _civilInsuranceAgentID = -1;
		private DateTime _kaskoDate;
		private int _kaskoInsuranceAgentID = -1;
		private DateTime _greencardDate;
		private int _greencardInsuranceAgentID = -1;
		private DateTime _annualCheckDate;
		private DateTime _oilChangeDate;
		private string _oilChangeKm = String.Empty;
		private DateTime _nextChangeDate;
		private string _nextChangeKm = String.Empty;
		private int _autoServiceID = -1;
		private bool _isActive = true;
		private string _civilIns = String.Empty;
		private string _kaskoIns = String.Empty;
		private string _greenIns = String.Empty;
		private string _autoService = String.Empty;

		/// <summary>
		/// Initialize an new empty Login object.
		/// </summary>
		public AutomobileData()
		{
		}
		
		/// <summary>
		/// Initialize a new Login object with the given parameters.
		/// </summary>
		public AutomobileData(int automobileID, int userID, string name, string regNumber, DateTime civilDate, int civilInsuranceAgentID, DateTime kaskoDate, int kaskoInsuranceAgentID, DateTime greencardDate, int greencardInsuranceAgentID, DateTime annualCheckDate, DateTime oilChangeDate, string oilChangeKm, DateTime nextChangeDate, string nextChangeKm, int autoServiceID, bool isActive)
		{
			this._automobileID = automobileID;
			this._userID = userID;
			this._name = name;
			this._regNumber = regNumber;
			this._civilDate = civilDate;
			this._civilInsuranceAgentID = civilInsuranceAgentID;
			this._kaskoDate = kaskoDate;
			this._kaskoInsuranceAgentID = kaskoInsuranceAgentID;
			this._greencardDate = greencardDate;
			this._greencardInsuranceAgentID = greencardInsuranceAgentID;
			this._annualCheckDate = annualCheckDate;
			this._oilChangeDate = oilChangeDate;
			this._oilChangeKm = oilChangeKm;
			this._nextChangeDate = nextChangeDate;
			this._nextChangeKm = nextChangeKm;
			this._autoServiceID = autoServiceID;
			this._isActive = isActive;
		}	
		
	  
		public override string ToString()
		{
			StringBuilder builder = new StringBuilder();
			builder.Append("_automobileID: " + _automobileID.ToString()).Append("\r\n");
			builder.Append("_userID: " + _userID.ToString()).Append("\r\n");
			builder.Append("_name: " + _name.ToString()).Append("\r\n");
			builder.Append("_regNumber: " + _regNumber.ToString()).Append("\r\n");
			builder.Append("_civilDate: " + _civilDate.ToString()).Append("\r\n");
			builder.Append("_civilInsuranceAgentID: " + _civilInsuranceAgentID.ToString()).Append("\r\n");
			builder.Append("_kaskoDate: " + _kaskoDate.ToString()).Append("\r\n");
			builder.Append("_kaskoInsuranceAgentID: " + _kaskoInsuranceAgentID.ToString()).Append("\r\n");
			builder.Append("_greencardDate: " + _greencardDate.ToString()).Append("\r\n");
			builder.Append("_greencardInsuranceAgentID: " + _greencardInsuranceAgentID.ToString()).Append("\r\n");
			builder.Append("_annualCheckDate: " + _annualCheckDate.ToString()).Append("\r\n");
			builder.Append("_oilChangeDate: " + _oilChangeDate.ToString()).Append("\r\n");
			builder.Append("_oilChangeKm: " + _oilChangeKm.ToString()).Append("\r\n");
			builder.Append("_nextChangeDate: " + _nextChangeDate.ToString()).Append("\r\n");
			builder.Append("_nextChangeKm: " + _nextChangeKm.ToString()).Append("\r\n");
			builder.Append("_autoServiceID: " + _autoServiceID.ToString()).Append("\r\n");
			builder.Append("_isActive: " + _isActive.ToString()).Append("\r\n");
			builder.Append("\r\n");
			return builder.ToString();
		}	
		public object GetByIndex(int nIndex)
		{
			switch(nIndex)
			{
				case 0:
					return _automobileID;
				case 1:
					return _userID;
				case 2:
					return _name;
				case 3:
					return _regNumber;
				case 4:
					return _civilDate;
				case 5:
					return _civilInsuranceAgentID;
				case 6:
					return _kaskoDate;
				case 7:
					return _kaskoInsuranceAgentID;
				case 8:
					return _greencardDate;
				case 9:
					return _greencardInsuranceAgentID;
				case 10:
					return _annualCheckDate;
				case 11:
					return _oilChangeDate;
				case 12:
					return _oilChangeKm;
				case 13:
					return _nextChangeDate;
				case 14:
					return _nextChangeKm;
				case 15:
					return _autoServiceID;
				case 16:
					return _isActive;
			}
			return null;
		}
		#region Properties
	
		public string CivilIns
		{
			get { return _civilIns; }
			set { _civilIns = value; }
		}
		public string KaskoIns
		{
			get { return _kaskoIns; }
			set { _kaskoIns = value; }
		}
		public string GreenIns
		{
			get { return _greenIns; }
			set { _greenIns = value; }
		}
		public string AutoService
		{
			get { return _autoService; }
			set { _autoService = value; }
		}
		
		public int AutomobileID
		{
			get { return _automobileID; }
			set { _automobileID = value; }
		}
	
		public int UserID
		{
			get { return _userID; }
			set { _userID = value; }
		}
	
		public string Name
		{
			get { return _name; }
			set { _name = value; }
		}
	
		public string RegNumber
		{
			get { return _regNumber; }
			set { _regNumber = value; }
		}
	
		public DateTime CivilDate
		{
			get { return _civilDate; }
			set { _civilDate = value; }
		}
	
		public int CivilInsuranceAgentID
		{
			get { return _civilInsuranceAgentID; }
			set { _civilInsuranceAgentID = value; }
		}
	
		public DateTime KaskoDate
		{
			get { return _kaskoDate; }
			set { _kaskoDate = value; }
		}
	
		public int KaskoInsuranceAgentID
		{
			get { return _kaskoInsuranceAgentID; }
			set { _kaskoInsuranceAgentID = value; }
		}
	
		public DateTime GreencardDate
		{
			get { return _greencardDate; }
			set { _greencardDate = value; }
		}
	
		public int GreencardInsuranceAgentID
		{
			get { return _greencardInsuranceAgentID; }
			set { _greencardInsuranceAgentID = value; }
		}
	
		public DateTime AnnualCheckDate
		{
			get { return _annualCheckDate; }
			set { _annualCheckDate = value; }
		}
	
		public DateTime OilChangeDate
		{
			get { return _oilChangeDate; }
			set { _oilChangeDate = value; }
		}
	
		public string OilChangeKm
		{
			get { return _oilChangeKm; }
			set { _oilChangeKm = value; }
		}
	
		public DateTime NextChangeDate
		{
			get { return _nextChangeDate; }
			set { _nextChangeDate = value; }
		}
	
		public string NextChangeKm
		{
			get { return _nextChangeKm; }
			set { _nextChangeKm = value; }
		}
	
		public int AutoServiceID
		{
			get { return _autoServiceID; }
			set { _autoServiceID = value; }
		}
	
		public bool IsActive
		{
			get { return _isActive; }
			set { _isActive = value; }
		}
	
		#endregion
	}
}


