using System;
using DataDynamics.ActiveReports;
using DataDynamics.ActiveReports.Document;

namespace Asa.Timesheet.Windows.Mail
{
	public class ActiveReport1 : ActiveReport
	{
		public ActiveReport1()
		{
			InitializeReport();
		}

		#region ActiveReports Designer generated code
		private DataDynamics.ActiveReports.PageHeader PageHeader = null;
		private DataDynamics.ActiveReports.Detail Detail = null;
		private DataDynamics.ActiveReports.Shape Shape1 = null;
		private DataDynamics.ActiveReports.PageFooter PageFooter = null;
		public void InitializeReport()
		{
			this.LoadLayout(this.GetType(), "Mail.ActiveReport1.rpx");
			this.PageHeader = ((DataDynamics.ActiveReports.PageHeader)(this.Sections["PageHeader"]));
			this.Detail = ((DataDynamics.ActiveReports.Detail)(this.Sections["Detail"]));
			this.PageFooter = ((DataDynamics.ActiveReports.PageFooter)(this.Sections["PageFooter"]));
			this.Shape1 = ((DataDynamics.ActiveReports.Shape)(this.Detail.Controls[0]));
		}

		#endregion
	}
}
