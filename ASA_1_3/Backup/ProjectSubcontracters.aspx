﻿<%@ Page language="c#" Codebehind="ProjectSubcontracters.aspx.cs" AutoEventWireup="false" Inherits="Asa.Timesheet.WebPages.ProjectSubcontracters" %>
<%@ Register TagPrefix="uc1" TagName="PageHeader" Src="UserControls/PageHeader.ascx" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<globalization uiCulture="bg" culture="bg-BG" />
<HTML>
	<head runat="server">
		<title>Subprojects</title>
		<meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
		<meta content="C#" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<LINK href="styles/timesheet.css" type="text/css" rel="stylesheet">
	</HEAD>
	<body bottomMargin="0" leftMargin="0" topMargin="0" rightMargin="0">
		<form id="Form1" method="post" runat="server">
			<table height="100%" cellSpacing="0" cellPadding="0" width="100%" border="0">
				<tr height="1">
					<td><uc1:pageheader id="header" runat="server"></uc1:pageheader></td>
				</tr>
				<tr>
					<td>
						<table height="100%" cellSpacing="0" cellPadding="0" width="100%" border="0">
							<tr>
								<td class="menuLeft"><asp:placeholder id="menuHolder" runat="server"></asp:placeholder></td>
								
								<td style="PADDING-RIGHT: 5px; PADDING-LEFT: 5px; PADDING-TOP: 3px" vAlign="top" noWrap><br>
									<asp:label id="lblError" runat="server" ForeColor="Red" EnableViewState="False" CssClass="ErrorLabel"></asp:label><asp:label id="lblInfo" runat="server" EnableViewState="False" CssClass="InfoLabel"></asp:label><br>
									<br>
									<asp:panel id="grid" 
										runat="server" Height="450px" Width="100%">
										<asp:datagrid id="grdProjects" runat="server" CssClass="Grid" Width="100%" AutoGenerateColumns="False"
											CellPadding="4" PageSize="2" AllowSorting="False">
											<AlternatingItemStyle CssClass="GridAltItem" BackColor="WhiteSmoke"></AlternatingItemStyle>
											<ItemStyle CssClass="GridItem"></ItemStyle>
											<HeaderStyle Wrap="False" CssClass="GridHeader"></HeaderStyle>
											<Columns>
												<asp:TemplateColumn HeaderText="#">
													<ItemStyle Width="10px" ForeColor="DimGray"></ItemStyle>
													<ItemTemplate>
														<asp:Label CssClass="menuTable" runat="server" Text='<%# (int)DataBinder.Eval(Container, "ItemIndex")+1 %>' ID="Label3" NAME="Label1">
														</asp:Label>
													</ItemTemplate>
												</asp:TemplateColumn>
												<asp:TemplateColumn HeaderText="Име на подизпълнител" SortExpression="Name">
													<ItemTemplate>
														<asp:LinkButton id="lnk" runat="server" CssClass="menuTable"  CommandName="Edit">
															<span Class="menuTable">
																<%# DataBinder.Eval(Container, "DataItem.SubcontracterName") %>
															</span>
														</asp:LinkButton>
													</ItemTemplate>
												</asp:TemplateColumn>
												<asp:BoundColumn DataField="SubcontracterType" SortExpression="SubprojectType" HeaderText="Тип">
													<HeaderStyle Width="50px"></HeaderStyle>
													<ItemStyle ForeColor="DimGray"></ItemStyle>
												</asp:BoundColumn>
												<asp:TemplateColumn HeaderText="Нач.дата">
													<ItemStyle ForeColor="DimGray"></ItemStyle>
													<ItemTemplate>
														<asp:Label runat="server" Text='<%# Asa.Timesheet.Data.Util.TimeHelper.FormatDate((DateTime)DataBinder.Eval(Container, "DataItem.StartDate")) %>' ID="Label1" NAME="Label1">
														</asp:Label>
													</ItemTemplate>
												</asp:TemplateColumn>
												<asp:TemplateColumn HeaderText="Крайна.дата">
													<ItemStyle ForeColor="DimGray"></ItemStyle>
													<ItemTemplate>
														<asp:Label runat="server" Text='<%# Asa.Timesheet.Data.Util.TimeHelper.FormatDate((DateTime)DataBinder.Eval(Container, "DataItem.EndDate")) %>' ID="Label2" NAME="Label2">
														</asp:Label>
													</ItemTemplate>
												</asp:TemplateColumn>
												<asp:BoundColumn DataField="Area" SortExpression="Area" HeaderText="Разг.площ">
													<HeaderStyle Width="50px"></HeaderStyle>
													<ItemStyle Wrap="False" HorizontalAlign="Right" ForeColor="DimGray"></ItemStyle>
												</asp:BoundColumn>
												<asp:BoundColumn DataField="Rate" SortExpression="Rate" HeaderText="Ед. цена (EUR)" DataFormatString="{0:n2}">
													<HeaderStyle HorizontalAlign="Right"></HeaderStyle>
													<ItemStyle Wrap="False" HorizontalAlign="Right" ForeColor="DimGray"></ItemStyle>
												</asp:BoundColumn>
												<asp:BoundColumn DataField="TotalAmount" SortExpression="TotalAmount" HeaderText="Обща сума (EUR)"
													DataFormatString="{0:n2}">
													<HeaderStyle HorizontalAlign="Right"></HeaderStyle>
													<ItemStyle Wrap="False" HorizontalAlign="Right" ForeColor="DimGray"></ItemStyle>
												</asp:BoundColumn>
												<asp:TemplateColumn SortExpression="Done" HeaderText="Окончателна Проектна Документация">
													<HeaderStyle HorizontalAlign="Center"></HeaderStyle>
													<ItemStyle HorizontalAlign="Center" Width="40px"></ItemStyle>
													<ItemTemplate>
														<IMG id="imgAtt" src='<%# GetImage( DataBinder.Eval(Container, "DataItem.Done")) %>' align="middle" runat="server" />
													</ItemTemplate>
												</asp:TemplateColumn>
												<asp:TemplateColumn>
													<HeaderStyle Wrap="False" Width="20px"></HeaderStyle>
													<ItemStyle Wrap="False" HorizontalAlign="Center"></ItemStyle>
													<ItemTemplate>
														<asp:ImageButton id="btnEdit" runat="server" ToolTip="Редактирай" ImageUrl="images/edit1.gif"></asp:ImageButton>
													</ItemTemplate>
												</asp:TemplateColumn>
												<asp:TemplateColumn>
													<HeaderStyle Wrap="False" Width="20px"></HeaderStyle>
													<ItemStyle Wrap="False" HorizontalAlign="Center"></ItemStyle>
													<ItemTemplate>
														<asp:ImageButton id="btnLetter" runat="server" ToolTip="Писмо" ImageUrl="images/word.gif"></asp:ImageButton>
													</ItemTemplate>
												</asp:TemplateColumn>
												<asp:TemplateColumn>
													<HeaderStyle Wrap="False" Width="20px"></HeaderStyle>
													<ItemStyle Wrap="False" HorizontalAlign="Center"></ItemStyle>
													<ItemTemplate>
														<asp:ImageButton id="btnDelete" runat="server" Width="12px" Height="12px" ToolTip="Изтрий" ImageUrl="images/delete.gif"></asp:ImageButton>
													</ItemTemplate>
												</asp:TemplateColumn>
											</Columns>
										</asp:datagrid>
									</asp:panel>
									<TABLE id="Table4"
										cellSpacing="0" cellPadding="4" width="100%" border="0">
										<TR>
											<td>
												<TABLE id="Table1" cellSpacing="0" cellPadding="0" width="100%" border="0">
													<TR>
														<td><asp:button id="btnNewProject" runat="server" CssClass="ActionButton" ></asp:button>&nbsp;<asp:button id="btnProject" runat="server" CssClass="ActionButton" Width="216px" Text="Връщане към проекта"></asp:button>&nbsp;&nbsp;
															<asp:button id="btnSubc" runat="server" CssClass="ActionButton" Width="144px" Text="Фази"></asp:button></TD>
														<td><asp:imagebutton id="ibPdfExport" runat="server" ToolTip="Справка в PDF формат" ImageUrl="images/pdf1.gif"></asp:imagebutton></TD>
													</TR>
												</TABLE>
											</TD>
										</TR>
									</TABLE>
								</td>
							</tr>
						</table>
					</td>
				</tr>
			</table>
		</form>
	</body>
</HTML>
