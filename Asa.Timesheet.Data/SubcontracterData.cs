using System;
using System.Collections;
using System.Text;

namespace Asa.Timesheet.Data
{
	public class SubcontracterData
	{
		private int _subcontracterID = -1;
		private string _subcontracterName = String.Empty;
		private string _address = String.Empty;
		private string _phone = String.Empty;
		private int _subcontracterTypeID = -1;
		private string _name1 = String.Empty;
		private string _name2 = String.Empty;
		private int _defaultPaymentSchemeID = -1;
		private decimal _defaultRate;
		private bool _isActive = true;
		private string _email = String.Empty;
		private string _fax = String.Empty;
		private string _manager = String.Empty;
		private string _delo = String.Empty;
		private string _bulstat = String.Empty;
		private string _nDR = String.Empty;
		private string _webpage = String.Empty;
		private string _phone1 = String.Empty;
		private string _email1 = String.Empty;
		private string _phone2 = String.Empty;
		private string _email2 = String.Empty;
		private int _status = -1;
		private string _subcontracterType="";
		private bool _hide=false;
		private string _accountName = String.Empty;
		private string _accountPass = String.Empty;
		private string _pMMail = string.Empty;
		private bool _isActiveGroup = false;
		private string _subcontracterNameEN="";
		private string _nameEN="";
		
		/// <summary>
		/// Initialize an new empty Login object.
		/// </summary>
		public SubcontracterData()
		{
		}
		
		/// <summary>
		/// Initialize a new Login object with the given parameters.
		/// </summary>
//		public SubcontracterData(int subcontracterID, string subcontracterName, string address, string phone, 
//			int subcontracterTypeID, string name1, string name2, int defaultPaymentSchemeID, decimal defaultRate, 
//			bool isActive, string email, string fax, string manager, string delo, string bulstat, string nDR, 
//			string webpage, string phone1, string email1, string phone2, string email2, int status, bool hide)
//		{
//			this._subcontracterID = subcontracterID;
//			this._subcontracterName = subcontracterName;
//			this._address = address;
//			this._phone = phone;
//			this._subcontracterTypeID = subcontracterTypeID;
//			this._name1 = name1;
//			this._name2 = name2;
//			this._defaultPaymentSchemeID = defaultPaymentSchemeID;
//			this._defaultRate = defaultRate;
//			this._isActive = isActive;
//			this._email = email;
//			this._fax = fax;
//			this._manager = manager;
//			this._delo = delo;
//			this._bulstat = bulstat;
//			this._nDR = nDR;
//			this._webpage = webpage;
//			this._phone1 = phone1;
//			this._email1 = email1;
//			this._phone2 = phone2;
//			this._email2 = email2;
//			this._status = status;
//			this._hide = hide;
//		}	
		
		public SubcontracterData(int subcontracterID, string subcontracterName, string address, string phone, 
			int subcontracterTypeID, string name1, string name2, int defaultPaymentSchemeID, decimal defaultRate, 
			bool isActive, string email, string fax, string manager, string delo, string bulstat, string nDR, 
			string webpage, string phone1, string email1, string phone2, string email2, int status, bool hide, 
			string accountName, string accountPass)
		{
			this._subcontracterID = subcontracterID;
			this._subcontracterName = subcontracterName;
			this._address = address;
			this._phone = phone;
			this._subcontracterTypeID = subcontracterTypeID;
			this._name1 = name1;
			this._name2 = name2;
			this._defaultPaymentSchemeID = defaultPaymentSchemeID;
			this._defaultRate = defaultRate;
			this._isActive = isActive;
			this._email = email;
			this._fax = fax;
			this._manager = manager;
			this._delo = delo;
			this._bulstat = bulstat;
			this._nDR = nDR;
			this._webpage = webpage;
			this._phone1 = phone1;
			this._email1 = email1;
			this._phone2 = phone2;
			this._email2 = email2;
			this._status = status;
			this._hide = hide;
			this._accountName = accountName;
			this._accountPass = accountPass;	
		
		}	
		
		public SubcontracterData(int subcontracterID, string subcontracterName, string address, string phone, 
			int subcontracterTypeID, string name1, string name2, int defaultPaymentSchemeID, decimal defaultRate, 
			bool isActive, string email, string fax, string manager, string delo, string bulstat, string nDR, 
			string webpage, string phone1, string email1, string phone2, string email2, int status, bool hide, 
			string accountName, string accountPass, string pMMail)
		{
			this._subcontracterID = subcontracterID;
			this._subcontracterName = subcontracterName;
			this._address = address;
			this._phone = phone;
			this._subcontracterTypeID = subcontracterTypeID;
			this._name1 = name1;
			this._name2 = name2;
			this._defaultPaymentSchemeID = defaultPaymentSchemeID;
			this._defaultRate = defaultRate;
			this._isActive = isActive;
			this._email = email;
			this._fax = fax;
			this._manager = manager;
			this._delo = delo;
			this._bulstat = bulstat;
			this._nDR = nDR;
			this._webpage = webpage;
			this._phone1 = phone1;
			this._email1 = email1;
			this._phone2 = phone2;
			this._email2 = email2;
			this._status = status;
			this._hide = hide;
			this._accountName = accountName;
			this._accountPass = accountPass;	
			this._pMMail = pMMail;
		}	
	  

		
		
		public override string ToString()
		{
			StringBuilder builder = new StringBuilder();
			builder.Append("_subcontracterID: " + _subcontracterID.ToString()).Append("\r\n");
			builder.Append("_subcontracterName: " + _subcontracterName.ToString()).Append("\r\n");
			builder.Append("_address: " + _address.ToString()).Append("\r\n");
			builder.Append("_phone: " + _phone.ToString()).Append("\r\n");
			builder.Append("_subcontracterTypeID: " + _subcontracterTypeID.ToString()).Append("\r\n");
			builder.Append("_name1: " + _name1.ToString()).Append("\r\n");
			builder.Append("_name2: " + _name2.ToString()).Append("\r\n");
			builder.Append("_defaultPaymentSchemeID: " + _defaultPaymentSchemeID.ToString()).Append("\r\n");
			builder.Append("_defaultRate: " + _defaultRate.ToString()).Append("\r\n");
			builder.Append("_isActive: " + _isActive.ToString()).Append("\r\n");
			builder.Append("_email: " + _email.ToString()).Append("\r\n");
			builder.Append("_fax: " + _fax.ToString()).Append("\r\n");
			builder.Append("_manager: " + _manager.ToString()).Append("\r\n");
			builder.Append("_delo: " + _delo.ToString()).Append("\r\n");
			builder.Append("_bulstat: " + _bulstat.ToString()).Append("\r\n");
			builder.Append("_nDR: " + _nDR.ToString()).Append("\r\n");
			builder.Append("_webpage: " + _webpage.ToString()).Append("\r\n");
			builder.Append("_phone1: " + _phone1.ToString()).Append("\r\n");
			builder.Append("_email1: " + _email1.ToString()).Append("\r\n");
			builder.Append("_phone2: " + _phone2.ToString()).Append("\r\n");
			builder.Append("_email2: " + _email2.ToString()).Append("\r\n");
			builder.Append("_status: " + _status.ToString()).Append("\r\n");
			builder.Append("_hide: " + _hide.ToString()).Append("\r\n");
			builder.Append("_accountName: " + _accountName.ToString()).Append("\r\n");
			builder.Append("_accountPass: " + _accountPass.ToString()).Append("\r\n");
			builder.Append("_pMMail: " + _pMMail.ToString()).Append("\r\n");
			builder.Append("\r\n");
			return builder.ToString();
		}	
	
		
		public object GetByIndex(int nIndex)
		{
			switch(nIndex)
			{
				case 0:
					return _subcontracterID;
				case 1:
					return _subcontracterName;
				case 2:
					return _address;
				case 3:
					return _phone;
				case 4:
					return _subcontracterTypeID;
				case 5:
					return _name1;
				case 6:
					return _name2;
				case 7:
					return _defaultPaymentSchemeID;
				case 8:
					return _defaultRate;
				case 9:
					return _isActive;
				case 10:
					return _email;
				case 11:
					return _fax;
				case 12:
					return _manager;
				case 13:
					return _delo;
				case 14:
					return _bulstat;
				case 15:
					return _nDR;
				case 16:
					return _webpage;
				case 17:
					return _phone1;
				case 18:
					return _email1;
				case 19:
					return _phone2;
				case 20:
					return _email2;
				case 21:
					return _status;
				case 22:
					return _hide;
				case 23:
					return _accountName;
				case 24:
					return _accountPass;
				case 25:
					return _pMMail;
				case 26:
					return _isActiveGroup;
			}
			return null;
		}
	
		
		#region Properties
		public string SubcontracterNameEN
		{
			get { return _subcontracterNameEN; }
			set { _subcontracterNameEN = value; }
		}
		public string NameEN
		{
			get { return _nameEN; }
			set { _nameEN = value; }
		}
		public int SubcontracterID
		{
			get { return _subcontracterID; }
			set { _subcontracterID = value; }
		}
	
		public string SubcontracterName
		{
			get { return _subcontracterName; }
			set { _subcontracterName = value; }
		}
		public string SubcontracterNameAndAll
		{
			get {
				string s = ""; 
				bool bHasAdded=false;
				if(_manager.Trim()!="" )
				{
					s+=_manager;
					bHasAdded=true;
				}
				if(_name1.Trim()!="" && _name1!=_manager )
				{
					if(bHasAdded)
                        s+=Resource.ResourceManager["or"];
//					else
//						s+=" - " ;
					s+=_name1;
					bHasAdded=true;
				}
				if(_name2.Trim()!="" && _name2!=_name1 && _name2!=_manager )
				{
					if(bHasAdded)
						s+=Resource.ResourceManager["or"];
//					else
//						s+=" - " ;
					s+=_name2;
					bHasAdded=true;
				}
				if(bHasAdded)
					s+=" - " ;
				s+=_subcontracterName;
				return s;
			}
			set {}
		}
		public string SubcontracterNameFirst
		{
			get 
			{
				string s = ""; 
				bool bHasAdded=false;
				if(_manager.Trim()!="" )
				{
					s+=_manager;
					bHasAdded=true;
				}
				if(_name1.Trim()!="" && _name1!=_manager )
				{
					if(!bHasAdded)
					{
						s+=_name1;
						bHasAdded=true;
					}
				}
				if(_name2.Trim()!="" && _name2!=_name1 && _name2!=_manager )
				{
					if(!bHasAdded)
					{
							
						s+=_name2;
						bHasAdded=true;
					}
				}
				
				return s;
			}
			set {}
		}
		public string Address
		{
			get { return _address; }
			set { _address = value; }
		}
	
		public string Phone
		{
			get { return _phone; }
			set { _phone = value; }
		}
	
		public int SubcontracterTypeID
		{
			get { return _subcontracterTypeID; }
			set { _subcontracterTypeID = value; }
		}
		public string SubcontracterType
		{
			get { return _subcontracterType; }
			set { _subcontracterType = value; }
		}
		public string Name1
		{
			get { return _name1; }
			set { _name1 = value; }
		}
	
		public string Name2
		{
			get { return _name2; }
			set { _name2 = value; }
		}
	
		public int DefaultPaymentSchemeID
		{
			get { return _defaultPaymentSchemeID; }
			set { _defaultPaymentSchemeID = value; }
		}
	
		public decimal DefaultRate
		{
			get { return _defaultRate; }
			set { _defaultRate = value; }
		}
	
		public bool IsActive
		{
			get { return _isActive; }
			set { _isActive = value; }
		}
	
		public string Email
		{
			get { return _email; }
			set { _email = value; }
		}
	
		public string Fax
		{
			get { return _fax; }
			set { _fax = value; }
		}
	
		public string Manager
		{
			get { return _manager; }
			set { _manager = value; }
		}
	
		public string Delo
		{
			get { return _delo; }
			set { _delo = value; }
		}
	
		public string Bulstat
		{
			get { return _bulstat; }
			set { _bulstat = value; }
		}
	
		public string NDR
		{
			get { return _nDR; }
			set { _nDR = value; }
		}
	
		public string Webpage
		{
			get { return _webpage; }
			set { _webpage = value; }
		}
	
		public string Phone1
		{
			get { return _phone1; }
			set { _phone1 = value; }
		}
	
		public string Email1
		{
			get { return _email1; }
			set { _email1 = value; }
		}
	
		public string Phone2
		{
			get { return _phone2; }
			set { _phone2 = value; }
		}
	
		public string Email2
		{
			get { return _email2; }
			set { _email2 = value; }
		}
	
		public int Status
		{
			get { return _status; }
			set { _status = value; }
		}
		
		public bool Hide
		{
			get { return _hide; }
			set { _hide = value; }
		}
		
		public string AccountName
		{
			get { return _accountName; }
			set { _accountName = value; }
		}
		
		public string AccountPass
		{
			get { return _accountPass; }
			set { _accountPass = value; }
		}

		public string PMMail
		{
			get { return _pMMail; }
			set { _pMMail = value; }
		}

		public bool IsActiveGroup
		{
			get {return _isActiveGroup;}
			set { _isActiveGroup = value;}
		}
		#endregion
	}
}


