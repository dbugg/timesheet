﻿<%@ Page language="c#" Codebehind="EditClient.aspx.cs" AutoEventWireup="false" Inherits="Asa.Timesheet.WebPages.EditClient" %>
<%@ Register TagPrefix="uc1" TagName="EditForm" Src="UserControls/EditForm.ascx" %>
<%@ Register TagPrefix="uc1" TagName="PageHeader" Src="UserControls/PageHeader.ascx" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
	<head runat="server">
		<title>EditClient</title>
		<meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
		<meta content="C#" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<LINK href="styles/timesheet.css" type="text/css" rel="stylesheet">
	</HEAD>
	<body bottomMargin="0" leftMargin="0" topMargin="0" rightMargin="0" onload="if(document.getElementById('txtAccountPass')!=null) document.getElementById('txtAccountPass').value=document.getElementById('hdnAccountPass').value;">
		<form id="Form1" method="post" runat="server">
			<table height="100%" cellSpacing="0" cellPadding="0" width="100%" border="0">
				<tr height="1">
					<td><uc1:pageheader id="header" runat="server"></uc1:pageheader><uc1:editform id="editCtrl" runat="server"></uc1:editform></td>
				</tr>
				<tr>
					<td>
						<table height="100%" cellSpacing="0" cellPadding="0" width="100%" border="0">
							<tr>
								<td class="menuLeft"><asp:placeholder id="menuHolder" runat="server"></asp:placeholder></td>
								
								<td style="PADDING-LEFT: 5px; PADDING-TOP: 3px" vAlign="top" noWrap>
									<TABLE id="Table3" cellSpacing="0" cellPadding="0" width="880" border="0">
										<TR>
											<td style="HEIGHT: 5px" colSpan="2"></TD>
										</TR>
										<TR>
											<td colSpan="2"><IMG height="1" src="images/dot.gif" width="100%"></TD>
										</TR>
										<TR>
											<td colSpan="2" height="3"></td>
										</TR>
										<TR>
											<td style="WIDTH: 4px"></TD>
											<td>
												<TABLE id="Table6" cellSpacing="0" cellPadding="0" border="0">
													<TR>
														<td style="WIDTH: 97px"><asp:button id="btnSaveUP" runat="server" EnableViewState="False" CssClass="ActionButton" Text="Запиши"></asp:button><asp:button id="btnEditUP" style="DISPLAY: none" runat="server" EnableViewState="False" CssClass="ActionButton"
																Text="Редактирай"></asp:button></TD>
														<td style="WIDTH: 245px"><asp:button id="btnCancelUP" runat="server" EnableViewState="False" CssClass="ActionButton"
																Text="Откажи"></asp:button></TD>
														<td></TD>
													</TR>
												</TABLE>
											</TD>
										</TR>
										<TR>
											<td height="3"></TD>
										</TR>
										<TR>
											<td colSpan="2"><IMG height="1" src="images/dot.gif" width="100%"></TD>
										</TR>
										<TR>
											<td style="WIDTH: 4px"></TD>
											<td>
												<TABLE id="tblForm" cellSpacing="0" cellPadding="3" width="100%" border="0">
													<TR>
														<td style="WIDTH: 168px"><asp:label id="lbClientType" runat="server" EnableViewState="False" Font-Bold="True" CssClass="enterDataLabel"
																Width="100%">Тип:</asp:label></TD>
														<td><asp:DropDownList id="ddlClientType" runat="server" CssClass="enterDataBox" Width="336px" AutoPostBack="True"></asp:DropDownList>&nbsp;<IMG runat="server" id="requiredClientType" height="16" alt="" src="images/required1.gif"
																width="16"></TD>
													</TR>
													<TR>
														<td style="WIDTH: 168px"><asp:label id="lblName" runat="server" EnableViewState="False" Font-Bold="True" CssClass="enterDataLabel"
																Width="100%">Име:</asp:label></TD>
														<td><asp:textbox id="txtName" runat="server" CssClass="enterDataBox" Width="336px"></asp:textbox>&nbsp;<IMG height="16" alt="" src="images/required1.gif" width="16"></TD>
													</TR>
													<TR>
														<td style="WIDTH: 168px">
															<asp:label id="Label16" runat="server" CssClass="enterDataLabel" Width="100%">Име(английски):</asp:label></TD>
														<td>
															<asp:textbox id="txtNameEN" runat="server" CssClass="enterDataBox" Width="336px"></asp:textbox></TD>
													</TR>
													<TR>
														<td style="WIDTH: 168px"><asp:label id="Label5" runat="server" CssClass="enterDataLabel" Width="100%">Фирмено дело:</asp:label></TD>
														<td><asp:textbox id="txtDelo" runat="server" CssClass="enterDataBox" Width="336px" TextMode="MultiLine"></asp:textbox></TD>
													</TR>
													<TR>
														<td style="WIDTH: 168px"><asp:label id="Label6" runat="server" CssClass="enterDataLabel" Width="100%">Булстат:</asp:label></TD>
														<td><asp:textbox id="txtBulstat" runat="server" CssClass="enterDataBox"></asp:textbox></TD>
													</TR>
													<TR>
														<td style="WIDTH: 168px"><asp:label id="Label7" runat="server" CssClass="enterDataLabel" Width="100%">Номер по НДР:</asp:label></TD>
														<td><asp:textbox id="txtNDR" runat="server" CssClass="enterDataBox"></asp:textbox></TD>
													</TR>
													<TR>
														<td style="WIDTH: 168px"><asp:label id="lblCity" runat="server" CssClass="enterDataLabel" Width="100%">Град:</asp:label></TD>
														<td><asp:textbox id="txtCity" runat="server" CssClass="enterDataBox"></asp:textbox></TD>
													</TR>
													<TR>
														<td style="WIDTH: 168px"><asp:label id="lblAddress" runat="server" CssClass="enterDataLabel">Адрес на управление:</asp:label></TD>
														<td><asp:textbox id="txtAddress" runat="server" CssClass="enterDataBox" Width="336px"></asp:textbox></TD>
													</TR>
													<TR>
														<td style="WIDTH: 168px"><asp:label id="Label1" runat="server" CssClass="enterDataLabel">Представлявано от:</asp:label></TD>
														<td><asp:textbox id="txtManager" runat="server" CssClass="enterDataBox" Width="336px"></asp:textbox></TD>
													</TR>
													<TR>
														<td style="WIDTH: 168px"><asp:label id="Label2" runat="server" CssClass="enterDataLabel">Телефон:</asp:label></TD>
														<td><asp:textbox id="txtPhone" runat="server" CssClass="enterDataBox" Width="336px"></asp:textbox></TD>
													</TR>
													<TR>
														<td style="WIDTH: 168px"><asp:label id="Label8" runat="server" CssClass="enterDataLabel">Факс:</asp:label></TD>
														<td><asp:textbox id="txtFax" runat="server" CssClass="enterDataBox" Width="336px"></asp:textbox></TD>
													</TR>
													<TR>
														<td style="WIDTH: 168px"><asp:label id="lblEmail" runat="server" CssClass="enterDataLabel">Е-мейл:</asp:label></TD>
														<td><asp:textbox id="txtEmail" runat="server" CssClass="enterDataBox" Width="336px"></asp:textbox></TD>
													</TR>
													<TR>
														<td style="WIDTH: 168px"><asp:label id="Label15" runat="server" CssClass="enterDataLabel">Уебсайт:</asp:label></TD>
														<td><asp:textbox id="txtWebsite" runat="server" CssClass="enterDataBox" Width="336px"></asp:textbox></TD>
													</TR>
													<TR>
														<td style="WIDTH: 168px"></TD>
														<td><asp:label id="Label9" runat="server" CssClass="enterDataBox"> Представител 1:</asp:label></TD>
													</TR>
													<TR>
														<td style="WIDTH: 168px"><asp:label id="Label3" runat="server" CssClass="enterDataLabel">Име:</asp:label></TD>
														<td><asp:textbox id="txtRepr1" runat="server" CssClass="enterDataBox" Width="336px"></asp:textbox></TD>
													</TR>
													<TR>
														<td style="WIDTH: 168px"><asp:label id="Label10" runat="server" CssClass="enterDataLabel">Моб. телефон:</asp:label></TD>
														<td><asp:textbox id="txtPhone1" runat="server" CssClass="enterDataBox" Width="336px"></asp:textbox></TD>
													</TR>
													<TR>
														<td style="WIDTH: 168px"><asp:label id="Label11" runat="server" CssClass="enterDataLabel">Е-мейл:</asp:label></TD>
														<td><asp:textbox id="txtEmail1" runat="server" CssClass="enterDataBox" Width="336px"></asp:textbox></TD>
													</TR>
													<TR>
														<td style="WIDTH: 168px">
															<asp:label id="Label17" runat="server" CssClass="enterDataLabel"> Име(английски):</asp:label></TD>
														<td>
															<asp:textbox id="txtReprEN" runat="server" CssClass="enterDataBox" Width="336px"></asp:textbox></TD>
													</TR>
													<TR>
														<td style="WIDTH: 168px"></TD>
														<td><asp:label id="Label12" runat="server" CssClass="enterDataBox"> Представител 2:</asp:label></TD>
													</TR>
													<TR>
														<td style="WIDTH: 168px"><asp:label id="Label4" runat="server" CssClass="enterDataLabel"> Име:</asp:label></TD>
														<td><asp:textbox id="txtRepr2" runat="server" CssClass="enterDataBox" Width="336px"></asp:textbox></TD>
													</TR>
													<TR>
														<td style="WIDTH: 168px"><asp:label id="Label13" runat="server" CssClass="enterDataLabel">Моб. телефон:</asp:label></TD>
														<td><asp:textbox id="txtPhone2" runat="server" CssClass="enterDataBox" Width="336px"></asp:textbox></TD>
													</TR>
													<TR>
														<td style="WIDTH: 168px"><asp:label id="Label14" runat="server" CssClass="enterDataLabel">Е-мейл:</asp:label></TD>
														<td><asp:textbox id="txtEmail2" runat="server" CssClass="enterDataBox" Width="336px"></asp:textbox></TD>
													</TR>
													<TR>
														<td style="WIDTH: 168px"><asp:label id="lbAccountName" runat="server" CssClass="enterDataLabel">Потребителско име:</asp:label></TD>
														<td><asp:textbox id="txtAccountName" runat="server" CssClass="enterDataBox" Width="336px"></asp:textbox></TD>
													</TR>
													<TR>
														<td style="WIDTH: 168px"><asp:label id="lbAccountPass" runat="server" CssClass="enterDataLabel">Парола:</asp:label></TD>
														<td><asp:textbox id="txtAccountPass" runat="server" CssClass="enterDataBox" Width="336px" TextMode="Password"></asp:textbox>&nbsp;
															<asp:button id="btnAutoGenerate" EnableViewState="False" runat="server" CssClass="ActionButton"
																Text="Генериране" Visible="False"></asp:button>
															<asp:textbox id="txtHdnAccountPass" runat="server" Visible="False"></asp:textbox><INPUT id="hdnAccountPass" type="hidden" runat="server" NAME="hdnPassword"></TD>
													</TR>
												</TABLE>
											</TD>
										</TR>
										<TR>
											<td style="HEIGHT: 5px" colSpan="2"></TD>
										</TR>
										<TR>
											<td colSpan="2"><IMG height="1" src="images/dot.gif" width="100%"></TD>
										</TR>
										<TR>
											<td colSpan="2" height="3"></td>
										</TR>
										<TR>
											<td style="WIDTH: 4px"></TD>
											<td>
												<TABLE id="Table5" cellSpacing="0" cellPadding="0" border="0">
													<TR>
														<td style="WIDTH: 97px"><asp:button id="btnSave" runat="server" EnableViewState="False" CssClass="ActionButton" Text="Запиши"></asp:button><asp:button id="btnEdit" style="DISPLAY: none" runat="server" EnableViewState="False" CssClass="ActionButton"
																Text="Редактирай"></asp:button></TD>
														<td style="WIDTH: 245px"><asp:button id="btnCancel" runat="server" EnableViewState="False" CssClass="ActionButton" Text="Откажи"></asp:button></TD>
														<td><asp:button id="btnDelete" runat="server" EnableViewState="False" CssClass="ActionButton" Text="Изтрий"></asp:button></TD>
													</TR>
												</TABLE>
											</TD>
										</TR>
										<TR>
											<td height="3"></TD>
										</TR>
										<TR>
											<td colSpan="2"><IMG height="1" src="images/dot.gif" width="100%"></TD>
										</TR>
									</TABLE>
									<br>
									&nbsp;
									<asp:label id="lblInfo" runat="server" EnableViewState="False" CssClass="InfoLabel"></asp:label><asp:label id="lblError" runat="server" CssClass="ErrorLabel" EnableViewState="False" ForeColor="Red"></asp:label></td>
							</tr>
						</table>
					</td>
				</tr>
			</table>
		</form>
	</body>
</HTML>
