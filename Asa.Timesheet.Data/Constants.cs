using System;

namespace Asa.Timesheet.Data
{
	public class HeaderInfo
	{
		public string ProjectName = String.Empty;
		public string ProjectCode = String.Empty;
		public string ProjectAdminName = String.Empty;
		public string ClientName = String.Empty;
		public string ClientCity = String.Empty;
		public string ClientAddress = String.Empty;
		public string Proektant = String.Empty;
		public string User = String.Empty;
		public string Activity = String.Empty;
		public string BuildingType = String.Empty;
		public DateTime StartDate;
		public DateTime EndDate;

	}
	/// <summary>
	/// Summary description for Constants.
	/// </summary>
	public class Constants
	{
		public const int ArchitectConst = 10;
		public const int ArchitectConstMax = 8;
		public const int ArchitectConstMaxPlus = 2;
		public static readonly string TextDateFormat = "dd.MM.yyyy";
		public static readonly string AnsiDateFormat = "yyyy-MM-dd";
		public const int HoursGridMaxRowsCountDefault = 10;
		public const int HoursGridInitialRowsCountDefault = 3;
		public const int HoursGridDefaultEndHourIDDefault = 35;
		public const int HoursGridDefaultStartHourIDDefault = 19;
		public const int  MaxNotes = 4000;
		public const decimal  DDS = .2M;
		public const int  NumFixedPhases = 5;
		public static readonly DateTime DateMax = new DateTime(2079,1,1);
		public static readonly DateTime DateMinMin = new DateTime(1,1,1);
		public static readonly DateTime DateMin = new DateTime(1900,1,1);
		public const int  DirectorRole = 1;
		public const int  AccRole = 4;
		public const int  InjPartnerRole = 7;
		public static readonly string _PROJECT="{project}";
		public static readonly string _SUBCONTRACTER="{subcontracter}";
		public static readonly string _INVESTOR="{investor}";
		public static readonly string _MANAGER="{manager}";
		public static readonly string _ADDINFO="{addinfo}";
		public static readonly string _ADDRESS="{address}";
		public static readonly string _USER="{user}";
		public static readonly string _CLIENT="{client}";
		public static readonly string _USERS="{users}";
		public static readonly string _CLIENTS="{clients}";
		public static readonly string _SUBCONTRACTERS="{subcontracters}";
		public static readonly string _TEXT="{text";
		public static readonly string _CODE="{code}";
		public static readonly string _FAX="{fax}";
		public static readonly string _DATE="{date}";
		public static readonly string _AREA="{area}";
		public static readonly string _LASTNAME="{lastname}";
		public static readonly string FRONT_PAGE="blanka_FRONT_PAGE.dot";
		public static readonly string FAX_COVER="blanka_fax_cover.dot";
		public static readonly string NOTES="blanka_O_ZAPISKA.dot";
		public static readonly string DOGOVOR="registration_dogovor.dot";
		public static readonly string UDO="Udostoverenie.dot";
		public static readonly string PROTOKOL1="blanka_Protokol1.dot";
		public static readonly string PROTOKOL2="blanka_Protokol2.dot";
		public static readonly string MEETING="blanka_meet.dot";
		public static readonly string BLANKAS="blanka_sub.dot";
		public static readonly string _TEXT1="{text}";
		public static readonly string _PAPKI="{papki}";
		public static readonly string LetterToClient="blanka_pismo.dot";
		public static readonly string _HOMES_BUILDING_TYPE="8";
		//add by Ivailo date:14.12.2007
		//noted: Form WorkTimes is devided to two parts - for normal time(ID = 1)
		//	and for overtime(ID = 2)
        public const int _WorktimesID_NormalTime = 1;
		public const int _WorktimesID_OverTime = 2;
		public const char _separator = ',';
		
		
		#region Application settings keys

		public const string AS_KEY_HoursGridMaxRowsCount = "HoursGrid_MaxRowsCount";
		public const string AS_KEY_HoursGridInitialRowsCount ="HoursGrid_InitialRowsCount";
		public const string AS_KEY_HoursGridDefaultStartHourID = "HoursGrid_DefaultStartHourID";
		public const string AS_KEY_HoursGridDefaultEndHourID = "HoursGrid_DefaultEndHourID";

		#endregion
	}
	public enum PaymentSchemes
	{
		Area=1,
		Fixed
	}
	public enum SalaryTypes
	{
		Main=1,
		Insurance=2,
		Bonus =3
	}
	public enum SubprojectTypes
	{
		Idea=1,
		Technical=2,
		Work,
		Revision
	}
	public enum AuthorsInvoice
	{
		None=0,
		OneMonth=1,
		TwoMonths=2,
		ThreeMonths=3
	}
	public enum ProfileItems
	{ 
		
		Notary=1,
		Sketch,
		PowerofAttorney,
		CurrentStatus,
		RegPlan,
        BuildPlan,
		PreVisa,
		VisaPlus,
		KadasterUndEnt,
		KadasterEnt,
		Visa=11,
		VIKEnt,
		ElEnt,
		TEnt,
		PathEnt,
		PhoneEnt,
		IdealProjectDone,
		TechProjectDone,
		BuildPermission,
		Other,
		OtherDoc
	}
	public enum MailingSendReseve
	{
		Send = 0,
		Reseve = 1
	}
	public enum Roles
	{
		Director = 1,
		Assistant = 2,
		Employ = 3,
		MainAccount = 4,
		Admin = 5,
		Technical = 6,
		Ingener = 7,
		Secretary = 8,
		Account = 9,
		Cleaner = 10,
		Layer = 11,
		Driver = 12,
	}

	public enum ClientTypes
	{
		Client = 1,
		Builder = 2,
		Producer = 3,
		All = 6,
		PMUser = 4,
		Supervisors = 5,
		ProjectManagers = 7,
		Brokers = 8,
	}
	public enum PMUserType
	{
		User = 1,
		Client = 2,
		Builder = 3,
		Subcontracter = 4,
		PMUser = 5,
		Supervisors = 6,
		ProjectManagers=7,
	}
	public enum Languages
	{
		EN,
		BG,
	}

	public enum HotIssueUserTypes
	{
		ASA_users = 1,
		Subcontracters =2,
		ASA_all = 3,
		Clients=4
	}
	public enum ChronologyTypeID
	{
		WorkTime=1,
		HotIssue=2,
		Mailing=3,
		Meeting=4,
		Payment=5,
		Protocolo=6,
		Profile=7,
		PMS=8
	}
}
