using System;
using System.Collections;
using System.Text;

namespace Asa.Timesheet.Data
{
	public class ProtokolData
	{
		private int _protokolID = -1;
		private DateTime _protokolDate;
		private int _projectID = -1;
		private int _createdByID = -1;
		private bool _hasScan;
		private string _notes = String.Empty;
		private string _projectName = String.Empty;

		/// <summary>
		/// Initialize an new empty Login object.
		/// </summary>
		public ProtokolData()
		{
		}
		
		/// <summary>
		/// Initialize a new Login object with the given parameters.
		/// </summary>
		public ProtokolData(int protokolID, DateTime protokolDate, int projectID, int createdByID, bool hasScan, string notes)
		{
			this._protokolID = protokolID;
			this._protokolDate = protokolDate;
			this._projectID = projectID;
			this._createdByID = createdByID;
			this._hasScan = hasScan;
			this._notes = notes;
			
		}	
		
	  
		public override string ToString()
		{
			StringBuilder builder = new StringBuilder();
			builder.Append("_protokolID: " + _protokolID.ToString()).Append("\r\n");
			builder.Append("_protokolDate: " + _protokolDate.ToString()).Append("\r\n");
			builder.Append("_projectID: " + _projectID.ToString()).Append("\r\n");
			builder.Append("_createdByID: " + _createdByID.ToString()).Append("\r\n");
			builder.Append("_hasScan: " + _hasScan.ToString()).Append("\r\n");
			builder.Append("_notes: " + _notes.ToString()).Append("\r\n");
			builder.Append("\r\n");
			return builder.ToString();
		}	
		public object GetByIndex(int nIndex)
		{
			switch(nIndex)
			{
				case 0:
					return _protokolID;
				case 1:
					return _protokolDate;
				case 2:
					return _projectID;
				case 3:
					return _createdByID;
				case 4:
					return _hasScan;
				case 5:
					return _notes;
				case 6:
					return _projectName;
			}
			return null;
		}
		#region Properties
	
		public int ProtokolID
		{
			get { return _protokolID; }
			set { _protokolID = value; }
		}
	
		public DateTime ProtokolDate
		{
			get { return _protokolDate; }
			set { _protokolDate = value; }
		}
	
		public int ProjectID
		{
			get { return _projectID; }
			set { _projectID = value; }
		}
	
		public int CreatedByID
		{
			get { return _createdByID; }
			set { _createdByID = value; }
		}
	
		public bool HasScan
		{
			get { return _hasScan; }
			set { _hasScan = value; }
		}
	
		public string Notes
		{
			get { return _notes; }
			set { _notes = value; }
		}

		public string ProjectName
		{
			get { return _projectName; }
			set { _projectName = value; }
		}
	
		#endregion
	}
}


