using System;
using System.Data;
using Asa.Timesheet.Data.Util;
using Asa.Timesheet.Data;
using GrapeCity.ActiveReports;
using GrapeCity.ActiveReports.Controls;
using GrapeCity.ActiveReports.SectionReportModel;
using GrapeCity.ActiveReports.Document.Section;
using GrapeCity.ActiveReports.Document;

namespace Asa.Timesheet.WebPages.Reports
{
    public class ScheduleVacation : GrapeCity.ActiveReports.SectionReport
	{
		private int _Month = DateTime.Now.Month;
		private int _year = DateTime.Now.Year;
		private bool _hasID = false;
		private int _mountCount = 0;
		private bool _weekly;
		private string ColorLabel1Project = "text-align: left; font-weight: bold; background-color: #FFC080; font-size: 11pt; color: Black;  ";
		//private string ColorLabel2Project = "font-weight: bold;background-color: #FFC080; font-size: 11pt; color: Black; text-align: right; ";
		private string ColorDate = "font-weight: bold;background-color: #FFC080; font-size: 11pt; color: #FFC080; text-align: right; ";
		private string ColorDateDefault = "text-align: left; font-weight: bold; background-color: White;font-size: 11pt; color: White; ";
		private string ColorLabel1Default1 = "text-align: left; font-weight: bold; background-color: White;font-size: 11pt; color: Black; ";
		//private string ColorLabel2Default1 = "text-align: left; font-weight: bold; background-color: White;font-size: 11pt; color: Red; text-align: left;";
		//private string ColorLabel2Default2 = "text-align: left; font-weight: bold; background-color: White;font-size: 10pt; color: #993366; text-align: left;";
		//private string Traine = "";
		private string Team = Resource.ResourceManager["ProjectTeam"];
		private string ProjectManager = Resource.ResourceManager["ProjectManager"];
		private DataSet _dsAbsence=null;
		private DateTime _dtStart;
		private DateTime _dtEnd;
		private const int _DAYSINMONTH=31;
        public ScheduleVacation(DataView dv, DataSet dsAbsence, DateTime dtStart, DateTime dtEnd)
        {
            _dtStart = dtStart;
            _Month = dtStart.Month;
            _year = dtStart.Year;
            _dtEnd = dtEnd;
            TimeSpan ts = (dtEnd - dtStart);
            int nMonths = 1;
            DateTime dtCur = dtStart;
            while (dtCur < dtEnd)
            {
                DateTime dtNext = dtCur.AddDays(1);
                if (dtNext.Month != dtCur.Month)
                    nMonths++;
                dtCur = dtNext;
            }
            _mountCount = nMonths; /// int.Parse(System.Configuration.ConfigurationManager.AppSettings["VacationMonths"]);
            _weekly = false;
            _dsAbsence = dsAbsence;
            this.DataSource = dv;
            InitializeComponent();
            //this.Detail.Format+=new EventHandler(Detail_Format);
        }




        private void Detail_Format(object sender, System.EventArgs eArgs)
        {


            this.Text1.Style = ColorLabel1Default1;
            //this.Text2.Style = ColorLabel2Default1;
            //this.txtStartDate.Style = ColorDateDefault;
            //this.txtEndDate.Style = ColorDateDefault;
            this.txtNotes.Style = ColorDateDefault;

            TakeBorder(this.Text1.Border);
            //TakeBorder(this.Text2.Border);
            //TakeBorder(this.txtStartDate.Border);
            //TakeBorder(this.txtEndDate.Border);
            TakeBorder(this.txtNotes.Border);
            if (_hasID)
            {
                ResetSubreports(false, false, -1);
                _hasID = false;
                //this.Text2.Style =ColorLabel2Default1;
            }
            else
            {
                if (this.lbID != null)
                    if (this.lbID.Text == "-1")
                    {
                        this.Text1.Style = ColorLabel1Project;
                        //this.Text2.Style = ColorLabel2Project;
                        //this.txtStartDate.Style = ColorDate;
                        //this.txtEndDate.Style = ColorDate;
                        this.txtNotes.Style = ColorDate;
                        ResetSubreports(true, false, -1);
                        _hasID = true;
                    }
                    else
                    {
                        int nID = int.Parse(lbID.Text);
                        ResetSubreports(true, true, nID);

                        //						if(this.Text1.Text == ProjectManager)
                        //							this.Text2.Style = ColorLabel2Default1;
                        this.Text1.Style = ColorLabel1Default1;

                    }
            }
            if (this.Text1.Text == " ")
            {
                this.Text1.Style = "font-size: 10pt;color: White; ";
                this.Text1.Text = ".";

            }

            if (this.txtNotes.Text == " ")
            {
                this.txtNotes.Text = ".";

            }
            else
                this.txtNotes.Style = ColorLabel1Default1;

        }

	
	


		#region ActiveReports Designer generated code







        public void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ScheduleVacation));
            this.Detail = new GrapeCity.ActiveReports.SectionReportModel.Detail();
            this.Text1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtNotes = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.lbID = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.SubReport = new GrapeCity.ActiveReports.SectionReportModel.SubReport();
            this.PageHeader = new GrapeCity.ActiveReports.SectionReportModel.PageHeader();
            this.PageFooter = new GrapeCity.ActiveReports.SectionReportModel.PageFooter();
            ((System.ComponentModel.ISupportInitialize)(this.Text1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtNotes)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            // 
            // Detail
            // 
            this.Detail.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
            this.Text1,
            this.txtNotes,
            this.lbID,
            this.SubReport});
            this.Detail.Height = 0.241667F;
            this.Detail.Name = "Detail";
            this.Detail.Format += new System.EventHandler(this.Detail_Format);
            // 
            // Text1
            // 
            this.Text1.CanShrink = true;
            this.Text1.DataField = "ProjectName";
            this.Text1.Height = 0.2F;
            this.Text1.Left = 0F;
            this.Text1.MultiLine = false;
            this.Text1.Name = "Text1";
            this.Text1.OutputFormat = resources.GetString("Text1.OutputFormat");
            this.Text1.Style = "text-align: right; white-space: inherit";
            this.Text1.Text = null;
            this.Text1.Top = 0F;
            this.Text1.Width = 2.27F;
            // 
            // txtNotes
            // 
            this.txtNotes.CanShrink = true;
            this.txtNotes.DataField = "Notes";
            this.txtNotes.Height = 0.2F;
            this.txtNotes.Left = 6.39F;
            this.txtNotes.MultiLine = false;
            this.txtNotes.Name = "txtNotes";
            this.txtNotes.OutputFormat = resources.GetString("txtNotes.OutputFormat");
            this.txtNotes.Style = "text-align: left; white-space: inherit";
            this.txtNotes.Text = null;
            this.txtNotes.Top = 0F;
            this.txtNotes.Width = 2F;
            // 
            // lbID
            // 
            this.lbID.DataField = "ProjectID";
            this.lbID.Height = 0.2F;
            this.lbID.HyperLink = null;
            this.lbID.Left = 0F;
            this.lbID.Name = "lbID";
            this.lbID.Style = "";
            this.lbID.Text = "";
            this.lbID.Top = 0F;
            this.lbID.Visible = false;
            this.lbID.Width = 2.27F;
            // 
            // SubReport
            // 
            this.SubReport.CloseBorder = false;
            this.SubReport.Height = 0.2F;
            this.SubReport.Left = 2.264999F;
            this.SubReport.Name = "SubReport";
            this.SubReport.Report = null;
            this.SubReport.Top = 0F;
            this.SubReport.Width = 80.59541F;
            // 
            // PageHeader
            // 
            this.PageHeader.Height = 0.01041667F;
            this.PageHeader.Name = "PageHeader";
            // 
            // PageFooter
            // 
            this.PageFooter.Height = 0F;
            this.PageFooter.Name = "PageFooter";
            // 
            // ScheduleVacation
            // 
            this.MasterReport = false;
            this.PageSettings.PaperHeight = 11F;
            this.PageSettings.PaperWidth = 8.5F;
            this.PrintWidth = 48.97917F;
            this.Sections.Add(this.PageHeader);
            this.Sections.Add(this.Detail);
            this.Sections.Add(this.PageFooter);
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule(resources.GetString("$this.StyleSheet"), "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: inherit; font-style: inherit; font-variant: inherit; font-weight: bo" +
            "ld; font-size: 16pt; font-size-adjust: inherit; font-stretch: inherit", "Heading1", "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: Times New Roman; font-style: italic; font-variant: inherit; font-wei" +
            "ght: bold; font-size: 14pt; font-size-adjust: inherit; font-stretch: inherit", "Heading2", "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: inherit; font-style: inherit; font-variant: inherit; font-weight: bo" +
            "ld; font-size: 13pt; font-size-adjust: inherit; font-stretch: inherit", "Heading3", "Normal"));
            ((System.ComponentModel.ISupportInitialize)(this.Text1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtNotes)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();

        }

		#endregion

        private void ResetSubreports(bool IDorNumb, bool empty, int nID)
        {
            float cellHighth = maxHighth(Text1.Height, Text1.Height);
            DateTime startDate = _dtStart;
            DateTime endDate = _dtEnd;
            if (!_weekly)
                this.SubReport.Report = new VacationSubreport(_Month, _year, !(empty || IDorNumb), !(empty || !IDorNumb), _mountCount, cellHighth, startDate, endDate, nID, _dsAbsence);
        }
        private float maxHighth(float f1, float f2)
        {
            if (f1 < f2)
                return f2;
            else
                return f1;
        }


        private void TakeBorder(GrapeCity.ActiveReports.Border b)
        {
            b.BottomColor = System.Drawing.Color.Black;
            b.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            b.LeftColor = System.Drawing.Color.Black;
            b.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            b.RightColor = System.Drawing.Color.Black;
            b.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            b.TopColor = System.Drawing.Color.Black;
            b.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
        }

        private PageHeader PageHeader;
        private Detail Detail;
        private TextBox Text1;
        private TextBox txtNotes;
        private Label lbID;
        private SubReport SubReport;
        private PageFooter PageFooter;

	}
}
