using System;
using System.Collections;
using System.Text;

namespace Asa.Timesheet.Data
{
	public class SalaryTypeData
	{
		private int _salaryTypeID = -1;
		private string _salaryType = String.Empty;

		/// <summary>
		/// Initialize an new empty Login object.
		/// </summary>
		public SalaryTypeData()
		{
		}
		
		/// <summary>
		/// Initialize a new Login object with the given parameters.
		/// </summary>
		public SalaryTypeData(int salaryTypeID, string salaryType)
		{
			this._salaryTypeID = salaryTypeID;
			this._salaryType = salaryType;
		}	
		
	  
		public override string ToString()
		{
			StringBuilder builder = new StringBuilder();
			builder.Append("_salaryTypeID: " + _salaryTypeID.ToString()).Append("\r\n");
			builder.Append("_salaryType: " + _salaryType.ToString()).Append("\r\n");
			builder.Append("\r\n");
			return builder.ToString();
		}	
		public object GetByIndex(int nIndex)
		{
			switch(nIndex)
			{
				case 0:
					return _salaryTypeID;
				case 1:
					return _salaryType;
			}
			return null;
		}
		#region Properties
	
		public int SalaryTypeID
		{
			get { return _salaryTypeID; }
			set { _salaryTypeID = value; }
		}
	
		public string SalaryType
		{
			get { return _salaryType; }
			set { _salaryType = value; }
		}
	
		#endregion
	}
}


