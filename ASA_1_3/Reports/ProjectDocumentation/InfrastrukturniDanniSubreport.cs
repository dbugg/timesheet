using System;
using System.Drawing;
using Asa.Timesheet.Data;
using Asa.Timesheet.Data.Reports;
using GrapeCity.ActiveReports;
using GrapeCity.ActiveReports.Controls;
using GrapeCity.ActiveReports.SectionReportModel;
using GrapeCity.ActiveReports.Document.Section;
using GrapeCity.ActiveReports.Document;

namespace Asa.Timesheet.WebPages.Reports
{
    public class InfrastrukturniDanniSubreport : GrapeCity.ActiveReports.SectionReport
	{
		bool _mainStatus  = true;
		string _projectName;
    bool _fullReport;
		int _startPageNumber;

        public InfrastrukturniDanniSubreport(string projectName, bool fullReport, int startPageNumber)
        {
            InitializeComponent();

            this._projectName = projectName;
            this._fullReport = fullReport;
            this._startPageNumber = startPageNumber;

        }

        private void Detail_Format(object sender, System.EventArgs eArgs)
        {
            if (!this._fullReport)
            {
                txtBelejkiLabel.Location =
                  new PointF(txtBelejkiLabel.Location.X, this.txtAmountLabel.Location.Y);
                txtBelejki.Location =
                  new PointF(txtBelejki.Location.X, txtBelejkiLabel.Location.Y);
            }

            if ((bool)this.Fields["PismoShow"].Value == false)
            {
                if (this._fullReport)
                {
                    txtBelejkiLabel.Location =
                      new PointF(txtBelejkiLabel.Location.X, this.txtAmountLabel.Location.Y);
                    txtBelejki.Location =
                      new PointF(txtBelejki.Location.X, txtBelejkiLabel.Location.Y);

                    this.txtAmountLabel.Location =
                      new PointF(this.txtAmountLabel.Location.X, this.txtNumberLabel.Location.Y);
                    this.txtAmount.Location =
                      new PointF(this.txtAmount.Location.X, this.txtAmountLabel.Location.Y);
                }
                else
                {
                    txtBelejkiLabel.Location =
                      new PointF(txtBelejkiLabel.Location.X, this.txtNumberLabel.Location.Y);
                    txtBelejki.Location =
                      new PointF(txtBelejki.Location.X, txtBelejkiLabel.Location.Y);
                }

                this.txtNumberLabel.Location =
                    new PointF(this.txtNumberLabel.Location.X, this.txtProjectLabel.Location.Y);
                this.txtNumber.Location =
                    new PointF(this.txtNumber.Location.X, this.txtNumberLabel.Location.Y);

                //Project up
                this.txtProjectLabel.Location =
                    new PointF(this.txtProjectLabel.Location.X, this.txtPrisDogovorLabel.Location.Y);
                this.txtProjectDate1Label.Location =
                    new PointF(this.txtProjectDate1Label.Location.X, this.txtPrisDogovorLabel.Location.Y);
                this.txtProjectDate1.Location =
                    new PointF(this.txtProjectDate1.Location.X, this.txtPrisDogovorLabel.Location.Y);
                this.txtProjectDate2Label.Location =
                    new PointF(this.txtProjectDate2Label.Location.X, this.txtPrisDogovorLabel.Location.Y);
                this.txtProjectDate2.Location =
                    new PointF(this.txtProjectDate2.Location.X, this.txtPrisDogovorLabel.Location.Y);
                this.txtProjectStatus.Location =
                    new PointF(this.txtProjectStatus.Location.X, this.txtPrisDogovorLabel.Location.Y);

                //Pris dogovor up
                this.txtPrisDogovorLabel.Location =
                    new PointF(this.txtPrisDogovorLabel.Location.X, this.txtPismoLabel.Location.Y);
                this.txtPrisDogovorDate1Label.Location =
                    new PointF(this.txtPrisDogovorDate1Label.Location.X, this.txtPismoLabel.Location.Y);
                this.txtPrisDogovorDate1.Location =
                    new PointF(this.txtPrisDogovorDate1.Location.X, this.txtPismoLabel.Location.Y);
                this.txtPrisDogovorStatus.Location =
                    new PointF(this.txtPrisDogovorStatus.Location.X, this.txtPismoLabel.Location.Y);
            }

            if ((bool)this.Fields["PrisDogovorShow"].Value == false)
            {
                if (this._fullReport)
                {
                    txtBelejkiLabel.Location =
                      new PointF(txtBelejkiLabel.Location.X, this.txtAmountLabel.Location.Y);
                    txtBelejki.Location =
                      new PointF(txtBelejki.Location.X, txtBelejkiLabel.Location.Y);

                    this.txtAmountLabel.Location =
                      new PointF(this.txtAmountLabel.Location.X, this.txtNumberLabel.Location.Y);
                    this.txtAmount.Location =
                      new PointF(this.txtAmount.Location.X, this.txtAmountLabel.Location.Y);
                }
                else
                {
                    txtBelejkiLabel.Location =
                      new PointF(txtBelejkiLabel.Location.X, this.txtNumberLabel.Location.Y);
                    txtBelejki.Location =
                      new PointF(txtBelejki.Location.X, txtBelejkiLabel.Location.Y);
                }

                this.txtNumberLabel.Location =
                    new PointF(this.txtNumberLabel.Location.X, this.txtProjectLabel.Location.Y);
                this.txtNumber.Location =
                    new PointF(this.txtNumber.Location.X, this.txtNumberLabel.Location.Y);

                //Project up
                this.txtProjectLabel.Location =
                    new PointF(this.txtProjectLabel.Location.X, this.txtPrisDogovorLabel.Location.Y);
                this.txtProjectDate1Label.Location =
                    new PointF(this.txtProjectDate1Label.Location.X, this.txtPrisDogovorLabel.Location.Y);
                this.txtProjectDate1.Location =
                    new PointF(this.txtProjectDate1.Location.X, this.txtPrisDogovorLabel.Location.Y);
                this.txtProjectDate2Label.Location =
                    new PointF(this.txtProjectDate2Label.Location.X, this.txtPrisDogovorLabel.Location.Y);
                this.txtProjectDate2.Location =
                    new PointF(this.txtProjectDate2.Location.X, this.txtPrisDogovorLabel.Location.Y);
                this.txtProjectStatus.Location =
                    new PointF(this.txtProjectStatus.Location.X, this.txtPrisDogovorLabel.Location.Y);

            }

            if ((bool)this.Fields["ProjectShow"].Value == false)
            {
                if (this._fullReport)
                {
                    txtBelejkiLabel.Location =
                      new PointF(txtBelejkiLabel.Location.X, this.txtAmountLabel.Location.Y);
                    txtBelejki.Location =
                      new PointF(txtBelejki.Location.X, txtBelejkiLabel.Location.Y);

                    this.txtAmountLabel.Location =
                      new PointF(this.txtAmountLabel.Location.X, this.txtNumberLabel.Location.Y);
                    this.txtAmount.Location =
                      new PointF(this.txtAmount.Location.X, this.txtAmountLabel.Location.Y);
                }
                else
                {
                    txtBelejkiLabel.Location =
                      new PointF(txtBelejkiLabel.Location.X, this.txtNumberLabel.Location.Y);
                    txtBelejki.Location =
                      new PointF(txtBelejki.Location.X, txtBelejkiLabel.Location.Y);
                }

                this.txtNumberLabel.Location =
                    new PointF(this.txtNumberLabel.Location.X, this.txtProjectLabel.Location.Y);
                this.txtNumber.Location =
                    new PointF(this.txtNumber.Location.X, this.txtNumberLabel.Location.Y);
            }

            System.Globalization.NumberFormatInfo nfi = new System.Globalization.NumberFormatInfo();
            nfi.NumberDecimalSeparator = ".";

            object o = this.Fields["Amount"].Value;

            if (this.Fields["Amount"].Value != System.DBNull.Value)
                this.txtAmount.Text = UIHelpers.FormatDecimal2((decimal)this.Fields["Amount"].Value);
            this.Detail.SizeToFit(true);
            this.Detail.Height = this.Detail.Height + CmToInch(0.5f);

        }

        private void ReportHeader_Format(object sender, System.EventArgs eArgs)
        {
            this.txtProjectNameHeader.Text = _projectName;
            txtReportTitle.Text = Resource.ResourceManager["Reports_strInfrastructureData"];

            this.txtMainStatus.Text = _mainStatus ? Resource.ResourceManager["Reports_Yes"] : Resource.ResourceManager["Reports_No"];
        }

        private void PageHeader_Format(object sender, System.EventArgs eArgs)
        {
            if (this.PageNumber == 1) this.PageHeader.Visible = false;
            else
            {
                this.PageHeader.Visible = true;
                txtPageHeader.Text = _projectName + " - " +
                    Resource.ResourceManager["Reports_strInfrastructureData"];
            }
        }

        private void PageFooter_Format(object sender, System.EventArgs eArgs)
        {
            lblPageFooter.Text = _projectName;
            lblPage.Text = string.Concat(Resource.ResourceManager["rpt_Page"], " ",
                (this.PageNumber + _startPageNumber - 1).ToString());
        }

        private void InfrastrukturniDanniSubreport_ReportStart(object sender, System.EventArgs eArgs)
        {
            ProjectsReportDS.InfrastructureDataItemsDataTable tbl =
                (ProjectsReportDS.InfrastructureDataItemsDataTable)this.DataSource;

            if (!this._fullReport)
            {
                this.txtAmountLabel.Visible = false;
                this.txtAmount.Visible = false;
            }

            foreach (ProjectsReportDS.InfrastructureDataItemsRow row in tbl)
            {
                if (row.MainStatus == Resource.ResourceManager["Reports_No"])
                {
                    _mainStatus = false;
                    break;
                }
            }

        }

		#region ActiveReports Designer generated code












































        public void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(InfrastrukturniDanniSubreport));
            this.Detail = new GrapeCity.ActiveReports.SectionReportModel.Detail();
            this.ReportHeader = new GrapeCity.ActiveReports.SectionReportModel.ReportHeader();
            this.ReportFooter = new GrapeCity.ActiveReports.SectionReportModel.ReportFooter();
            this.PageHeader = new GrapeCity.ActiveReports.SectionReportModel.PageHeader();
            this.PageFooter = new GrapeCity.ActiveReports.SectionReportModel.PageFooter();
            this.txtReportTitle = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtMainStatus = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtProjectNameHeader = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.Line3 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.txtPageHeader = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.Line2 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.txtTitle = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtIzhodniDanni = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.TextBox2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtIzhDanniDate1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.TextBox4 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtIzhDanniDate2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtPismoLabel = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.TextBox7 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtPismoDate1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtPrisDogovorLabel = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtPrisDogovorDate1Label = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtPrisDogovorDate1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtProjectLabel = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtProjectDate1Label = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtProjectDate1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtProjectDate2Label = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtProjectDate2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtAmountLabel = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtAmount = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtNumber = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.TextBox82 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.TextBox83 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtPrisDogovorStatus = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtProjectStatus = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtNumberLabel = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtBelejkiLabel = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtBelejki = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.Line1 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.txtStatus = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.Line11 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.lblPageFooter = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lblPage = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Line4 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            ((System.ComponentModel.ISupportInitialize)(this.txtReportTitle)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtMainStatus)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtProjectNameHeader)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPageHeader)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTitle)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtIzhodniDanni)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TextBox2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtIzhDanniDate1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TextBox4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtIzhDanniDate2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPismoLabel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TextBox7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPismoDate1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPrisDogovorLabel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPrisDogovorDate1Label)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPrisDogovorDate1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtProjectLabel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtProjectDate1Label)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtProjectDate1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtProjectDate2Label)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtProjectDate2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAmountLabel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAmount)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtNumber)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TextBox82)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TextBox83)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPrisDogovorStatus)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtProjectStatus)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtNumberLabel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtBelejkiLabel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtBelejki)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtStatus)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblPageFooter)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblPage)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            // 
            // Detail
            // 
            this.Detail.ColumnSpacing = 0F;
            this.Detail.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
						this.txtTitle,
						this.txtIzhodniDanni,
						this.TextBox2,
						this.txtIzhDanniDate1,
						this.TextBox4,
						this.txtIzhDanniDate2,
						this.txtPismoLabel,
						this.TextBox7,
						this.txtPismoDate1,
						this.txtPrisDogovorLabel,
						this.txtPrisDogovorDate1Label,
						this.txtPrisDogovorDate1,
						this.txtProjectLabel,
						this.txtProjectDate1Label,
						this.txtProjectDate1,
						this.txtProjectDate2Label,
						this.txtProjectDate2,
						this.txtAmountLabel,
						this.txtAmount,
						this.txtNumber,
						this.TextBox82,
						this.TextBox83,
						this.txtPrisDogovorStatus,
						this.txtProjectStatus,
						this.txtNumberLabel,
						this.txtBelejkiLabel,
						this.txtBelejki,
						this.Line1,
						this.txtStatus});
            this.Detail.Height = 2.1875F;
            this.Detail.KeepTogether = true;
            this.Detail.Name = "Detail";
            // 
            // ReportHeader
            // 
            this.ReportHeader.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
						this.txtReportTitle,
						this.txtMainStatus,
						this.txtProjectNameHeader,
						this.Line3});
            this.ReportHeader.Height = 0.7868056F;
            this.ReportHeader.Name = "ReportHeader";
            // 
            // ReportFooter
            // 
            this.ReportFooter.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
						this.Line4});
            this.ReportFooter.Height = 0.07222223F;
            this.ReportFooter.Name = "ReportFooter";
            // 
            // PageHeader
            // 
            this.PageHeader.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
						this.txtPageHeader,
						this.Line2});
            this.PageHeader.Height = 0.5902778F;
            this.PageHeader.Name = "PageHeader";
            // 
            // PageFooter
            // 
            this.PageFooter.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
						this.Line11,
						this.lblPageFooter,
						this.lblPage});
            this.PageFooter.Height = 0.25F;
            this.PageFooter.Name = "PageFooter";
            // 
            // txtReportTitle
            // 
            this.txtReportTitle.Height = 0.2755906F;
            this.txtReportTitle.Left = 0F;
            this.txtReportTitle.Name = "txtReportTitle";
            this.txtReportTitle.Style = "font-size: 14pt; font-weight: bold";
            this.txtReportTitle.Text = "2. ??????????????? ????? ?? ???????????";
            this.txtReportTitle.Top = 0F;
            this.txtReportTitle.Width = 6.299212F;
            // 
            // txtMainStatus
            // 
            this.txtMainStatus.Height = 0.2755906F;
            this.txtMainStatus.Left = 6.299212F;
            this.txtMainStatus.Name = "txtMainStatus";
            this.txtMainStatus.Style = "font-size: 14pt; font-weight: bold; text-align: right";
            this.txtMainStatus.Text = "??";
            this.txtMainStatus.Top = 0F;
            this.txtMainStatus.Width = 0.7086611F;
            // 
            // txtProjectNameHeader
            // 
            this.txtProjectNameHeader.Height = 0.2362205F;
            this.txtProjectNameHeader.Left = 0F;
            this.txtProjectNameHeader.Name = "txtProjectNameHeader";
            this.txtProjectNameHeader.Style = "font-size: 12pt; font-weight: bold";
            this.txtProjectNameHeader.Text = "txtProjectNameHeader";
            this.txtProjectNameHeader.Top = 0.2755906F;
            this.txtProjectNameHeader.Width = 6.299212F;
            // 
            // Line3
            // 
            this.Line3.Height = 0F;
            this.Line3.Left = 0F;
            this.Line3.LineWeight = 2F;
            this.Line3.Name = "Line3";
            this.Line3.Top = 0.551181F;
            this.Line3.Width = 7.086611F;
            this.Line3.X1 = 0F;
            this.Line3.X2 = 7.086611F;
            this.Line3.Y1 = 0.551181F;
            this.Line3.Y2 = 0.551181F;
            // 
            // txtPageHeader
            // 
            this.txtPageHeader.Height = 0.2362205F;
            this.txtPageHeader.Left = 0F;
            this.txtPageHeader.Name = "txtPageHeader";
            this.txtPageHeader.Style = "font-size: 11pt; font-weight: bold";
            this.txtPageHeader.Text = "txtPageHeader";
            this.txtPageHeader.Top = 0F;
            this.txtPageHeader.Width = 7.086611F;
            // 
            // Line2
            // 
            this.Line2.Height = 0F;
            this.Line2.Left = 0F;
            this.Line2.LineWeight = 2F;
            this.Line2.Name = "Line2";
            this.Line2.Top = 0.243493F;
            this.Line2.Width = 7.086611F;
            this.Line2.X1 = 0F;
            this.Line2.X2 = 7.086611F;
            this.Line2.Y1 = 0.243493F;
            this.Line2.Y2 = 0.243493F;
            // 
            // txtTitle
            // 
            this.txtTitle.DataField = "Title";
            this.txtTitle.Height = 0.2165354F;
            this.txtTitle.Left = 0.07874014F;
            this.txtTitle.Name = "txtTitle";
            this.txtTitle.Style = "font-size: 12pt; font-weight: bold";
            this.txtTitle.Text = "txtTitle";
            this.txtTitle.Top = 0F;
            this.txtTitle.Width = 6.299212F;
            // 
            // txtIzhodniDanni
            // 
            this.txtIzhodniDanni.DataField = "IzhDanniLabel";
            this.txtIzhodniDanni.Height = 0.2F;
            this.txtIzhodniDanni.Left = 0.07874014F;
            this.txtIzhodniDanni.Name = "txtIzhodniDanni";
            this.txtIzhodniDanni.Style = "font-weight: bold";
            this.txtIzhodniDanni.Text = "??????? ?????:";
            this.txtIzhodniDanni.Top = 0.3149606F;
            this.txtIzhodniDanni.Width = 2.165354F;
            // 
            // TextBox2
            // 
            this.TextBox2.DataField = "IzhDanniDate1Label";
            this.TextBox2.Height = 0.2F;
            this.TextBox2.Left = 2.165354F;
            this.TextBox2.Name = "TextBox2";
            this.TextBox2.Text = "??????:";
            this.TextBox2.Top = 0.3149606F;
            this.TextBox2.Width = 0.9842521F;
            // 
            // txtIzhDanniDate1
            // 
            this.txtIzhDanniDate1.DataField = "IzhDanniDate1";
            this.txtIzhDanniDate1.Height = 0.2F;
            this.txtIzhDanniDate1.Left = 3.149606F;
            this.txtIzhDanniDate1.Name = "txtIzhDanniDate1";
            this.txtIzhDanniDate1.OutputFormat = resources.GetString("txtIzhDanniDate1.OutputFormat");
            this.txtIzhDanniDate1.Style = "font-weight: bold";
            this.txtIzhDanniDate1.Text = "txtIzhDanniDate1";
            this.txtIzhDanniDate1.Top = 0.3149606F;
            this.txtIzhDanniDate1.Width = 0.9842521F;
            // 
            // TextBox4
            // 
            this.TextBox4.DataField = "IzhDanniDate2Label";
            this.TextBox4.Height = 0.2F;
            this.TextBox4.Left = 4.330709F;
            this.TextBox4.Name = "TextBox4";
            this.TextBox4.Text = "???????:";
            this.TextBox4.Top = 0.3149606F;
            this.TextBox4.Width = 0.9842521F;
            // 
            // txtIzhDanniDate2
            // 
            this.txtIzhDanniDate2.DataField = "IzhDanniDate2";
            this.txtIzhDanniDate2.Height = 0.2F;
            this.txtIzhDanniDate2.Left = 5.31496F;
            this.txtIzhDanniDate2.Name = "txtIzhDanniDate2";
            this.txtIzhDanniDate2.OutputFormat = resources.GetString("txtIzhDanniDate2.OutputFormat");
            this.txtIzhDanniDate2.Style = "font-weight: bold";
            this.txtIzhDanniDate2.Text = "txtIzhDanniDate2";
            this.txtIzhDanniDate2.Top = 0.3149606F;
            this.txtIzhDanniDate2.Width = 0.9842521F;
            // 
            // txtPismoLabel
            // 
            this.txtPismoLabel.DataField = "PismoLabel";
            this.txtPismoLabel.Height = 0.2F;
            this.txtPismoLabel.Left = 0.07874014F;
            this.txtPismoLabel.Name = "txtPismoLabel";
            this.txtPismoLabel.Style = "font-weight: bold";
            this.txtPismoLabel.Text = "????? ? ????? ?? ???????.";
            this.txtPismoLabel.Top = 0.5905511F;
            this.txtPismoLabel.Width = 2.165354F;
            // 
            // TextBox7
            // 
            this.TextBox7.DataField = "PismoDate1Label";
            this.TextBox7.Height = 0.2F;
            this.TextBox7.Left = 2.165354F;
            this.TextBox7.Name = "TextBox7";
            this.TextBox7.Text = "TextBox7";
            this.TextBox7.Top = 0.5905511F;
            this.TextBox7.Width = 0.9842521F;
            // 
            // txtPismoDate1
            // 
            this.txtPismoDate1.DataField = "PismoDate1";
            this.txtPismoDate1.Height = 0.2F;
            this.txtPismoDate1.Left = 3.149606F;
            this.txtPismoDate1.Name = "txtPismoDate1";
            this.txtPismoDate1.OutputFormat = resources.GetString("txtPismoDate1.OutputFormat");
            this.txtPismoDate1.Style = "font-weight: bold";
            this.txtPismoDate1.Text = "txtPismoDate1";
            this.txtPismoDate1.Top = 0.5905511F;
            this.txtPismoDate1.Width = 0.9842521F;
            // 
            // txtPrisDogovorLabel
            // 
            this.txtPrisDogovorLabel.DataField = "PrisDogovorLabel";
            this.txtPrisDogovorLabel.Height = 0.2F;
            this.txtPrisDogovorLabel.Left = 0.07874014F;
            this.txtPrisDogovorLabel.Name = "txtPrisDogovorLabel";
            this.txtPrisDogovorLabel.Style = "font-weight: bold";
            this.txtPrisDogovorLabel.Text = "TextBox9";
            this.txtPrisDogovorLabel.Top = 0.8661417F;
            this.txtPrisDogovorLabel.Width = 2.165354F;
            // 
            // txtPrisDogovorDate1Label
            // 
            this.txtPrisDogovorDate1Label.DataField = "PrisDogovorDate1Label";
            this.txtPrisDogovorDate1Label.Height = 0.2F;
            this.txtPrisDogovorDate1Label.Left = 2.175771F;
            this.txtPrisDogovorDate1Label.Name = "txtPrisDogovorDate1Label";
            this.txtPrisDogovorDate1Label.Text = "TextBox10";
            this.txtPrisDogovorDate1Label.Top = 0.8661417F;
            this.txtPrisDogovorDate1Label.Width = 0.9842521F;
            // 
            // txtPrisDogovorDate1
            // 
            this.txtPrisDogovorDate1.DataField = "PrisDogovorDate1";
            this.txtPrisDogovorDate1.Height = 0.2F;
            this.txtPrisDogovorDate1.Left = 3.149606F;
            this.txtPrisDogovorDate1.Name = "txtPrisDogovorDate1";
            this.txtPrisDogovorDate1.OutputFormat = resources.GetString("txtPrisDogovorDate1.OutputFormat");
            this.txtPrisDogovorDate1.Style = "font-weight: bold";
            this.txtPrisDogovorDate1.Text = "TextBox11";
            this.txtPrisDogovorDate1.Top = 0.8661417F;
            this.txtPrisDogovorDate1.Width = 0.9842521F;
            // 
            // txtProjectLabel
            // 
            this.txtProjectLabel.DataField = "ProjectLabel";
            this.txtProjectLabel.Height = 0.2F;
            this.txtProjectLabel.Left = 0.07874014F;
            this.txtProjectLabel.Name = "txtProjectLabel";
            this.txtProjectLabel.Style = "font-weight: bold";
            this.txtProjectLabel.Text = "TextBox12";
            this.txtProjectLabel.Top = 1.141733F;
            this.txtProjectLabel.Width = 2.165354F;
            // 
            // txtProjectDate1Label
            // 
            this.txtProjectDate1Label.DataField = "ProjectDate1Label";
            this.txtProjectDate1Label.Height = 0.2F;
            this.txtProjectDate1Label.Left = 2.165354F;
            this.txtProjectDate1Label.Name = "txtProjectDate1Label";
            this.txtProjectDate1Label.Text = "TextBox13";
            this.txtProjectDate1Label.Top = 1.141733F;
            this.txtProjectDate1Label.Width = 0.9842521F;
            // 
            // txtProjectDate1
            // 
            this.txtProjectDate1.DataField = "ProjectDate1";
            this.txtProjectDate1.Height = 0.2F;
            this.txtProjectDate1.Left = 3.149606F;
            this.txtProjectDate1.Name = "txtProjectDate1";
            this.txtProjectDate1.OutputFormat = resources.GetString("txtProjectDate1.OutputFormat");
            this.txtProjectDate1.Style = "font-weight: bold";
            this.txtProjectDate1.Text = "TextBox14";
            this.txtProjectDate1.Top = 1.141733F;
            this.txtProjectDate1.Width = 0.9842521F;
            // 
            // txtProjectDate2Label
            // 
            this.txtProjectDate2Label.DataField = "ProjectDate2Label";
            this.txtProjectDate2Label.Height = 0.2F;
            this.txtProjectDate2Label.Left = 4.330709F;
            this.txtProjectDate2Label.Name = "txtProjectDate2Label";
            this.txtProjectDate2Label.Text = "TextBox15";
            this.txtProjectDate2Label.Top = 1.141733F;
            this.txtProjectDate2Label.Width = 0.9842521F;
            // 
            // txtProjectDate2
            // 
            this.txtProjectDate2.DataField = "ProjectDate2";
            this.txtProjectDate2.Height = 0.2F;
            this.txtProjectDate2.Left = 5.31496F;
            this.txtProjectDate2.Name = "txtProjectDate2";
            this.txtProjectDate2.OutputFormat = resources.GetString("txtProjectDate2.OutputFormat");
            this.txtProjectDate2.Style = "font-weight: bold";
            this.txtProjectDate2.Text = "TextBox16";
            this.txtProjectDate2.Top = 1.141733F;
            this.txtProjectDate2.Width = 0.9842521F;
            // 
            // txtAmountLabel
            // 
            this.txtAmountLabel.Height = 0.2F;
            this.txtAmountLabel.Left = 0.07874014F;
            this.txtAmountLabel.Name = "txtAmountLabel";
            this.txtAmountLabel.Style = "font-weight: normal";
            this.txtAmountLabel.Text = "????:";
            this.txtAmountLabel.Top = 1.692913F;
            this.txtAmountLabel.Width = 2.165354F;
            // 
            // txtAmount
            // 
            this.txtAmount.DataField = "Amount";
            this.txtAmount.Height = 0.2F;
            this.txtAmount.Left = 2.165354F;
            this.txtAmount.Name = "txtAmount";
            this.txtAmount.OutputFormat = resources.GetString("txtAmount.OutputFormat");
            this.txtAmount.Style = "font-weight: bold";
            this.txtAmount.Text = "txtAmount";
            this.txtAmount.Top = 1.692913F;
            this.txtAmount.Width = 1F;
            // 
            // txtNumber
            // 
            this.txtNumber.DataField = "Number";
            this.txtNumber.Height = 0.2F;
            this.txtNumber.Left = 2.165354F;
            this.txtNumber.Name = "txtNumber";
            this.txtNumber.Style = "font-weight: bold";
            this.txtNumber.Text = "txtNumber";
            this.txtNumber.Top = 1.417323F;
            this.txtNumber.Width = 1F;
            // 
            // TextBox82
            // 
            this.TextBox82.DataField = "IzhDanniStatus";
            this.TextBox82.Height = 0.2F;
            this.TextBox82.Left = 6.299212F;
            this.TextBox82.Name = "TextBox82";
            this.TextBox82.Style = "font-weight: bold; text-align: right";
            this.TextBox82.Text = "TextBox82";
            this.TextBox82.Top = 0.3149606F;
            this.TextBox82.Width = 0.7086611F;
            // 
            // TextBox83
            // 
            this.TextBox83.DataField = "PismoStatus";
            this.TextBox83.Height = 0.2F;
            this.TextBox83.Left = 6.299212F;
            this.TextBox83.Name = "TextBox83";
            this.TextBox83.Style = "font-weight: bold; text-align: right";
            this.TextBox83.Text = "TextBox83";
            this.TextBox83.Top = 0.5905511F;
            this.TextBox83.Width = 0.7086611F;
            // 
            // txtPrisDogovorStatus
            // 
            this.txtPrisDogovorStatus.DataField = "PrisDogovorStatus";
            this.txtPrisDogovorStatus.Height = 0.2F;
            this.txtPrisDogovorStatus.Left = 6.299212F;
            this.txtPrisDogovorStatus.Name = "txtPrisDogovorStatus";
            this.txtPrisDogovorStatus.Style = "font-weight: bold; text-align: right";
            this.txtPrisDogovorStatus.Text = "TextBox84";
            this.txtPrisDogovorStatus.Top = 0.8661417F;
            this.txtPrisDogovorStatus.Width = 0.7086611F;
            // 
            // txtProjectStatus
            // 
            this.txtProjectStatus.DataField = "ProjectStatus";
            this.txtProjectStatus.Height = 0.2F;
            this.txtProjectStatus.Left = 6.299212F;
            this.txtProjectStatus.Name = "txtProjectStatus";
            this.txtProjectStatus.Style = "font-weight: bold; text-align: right";
            this.txtProjectStatus.Text = "TextBox85";
            this.txtProjectStatus.Top = 1.141733F;
            this.txtProjectStatus.Width = 0.7086611F;
            // 
            // txtNumberLabel
            // 
            this.txtNumberLabel.Height = 0.2F;
            this.txtNumberLabel.Left = 0.07874014F;
            this.txtNumberLabel.Name = "txtNumberLabel";
            this.txtNumberLabel.Style = "font-weight: normal";
            this.txtNumberLabel.Text = "??. ?????:";
            this.txtNumberLabel.Top = 1.417323F;
            this.txtNumberLabel.Width = 2.165354F;
            // 
            // txtBelejkiLabel
            // 
            this.txtBelejkiLabel.Height = 0.2F;
            this.txtBelejkiLabel.Left = 0.07874014F;
            this.txtBelejkiLabel.Name = "txtBelejkiLabel";
            this.txtBelejkiLabel.Text = "???????:";
            this.txtBelejkiLabel.Top = 1.968504F;
            this.txtBelejkiLabel.Width = 2.165354F;
            // 
            // txtBelejki
            // 
            this.txtBelejki.DataField = "Notes";
            this.txtBelejki.Height = 0.2F;
            this.txtBelejki.Left = 2.165354F;
            this.txtBelejki.Name = "txtBelejki";
            this.txtBelejki.Style = "font-weight: bold";
            this.txtBelejki.Text = "TextBox88";
            this.txtBelejki.Top = 1.968504F;
            this.txtBelejki.Width = 3.937008F;
            // 
            // Line1
            // 
            this.Line1.Height = 0F;
            this.Line1.Left = 0F;
            this.Line1.LineWeight = 1F;
            this.Line1.Name = "Line1";
            this.Line1.Top = 0.2637796F;
            this.Line1.Width = 7.086611F;
            this.Line1.X1 = 0F;
            this.Line1.X2 = 7.086611F;
            this.Line1.Y1 = 0.2637796F;
            this.Line1.Y2 = 0.2637796F;
            // 
            // txtStatus
            // 
            this.txtStatus.DataField = "MainStatus";
            this.txtStatus.Height = 0.2165354F;
            this.txtStatus.Left = 6.299212F;
            this.txtStatus.Name = "txtStatus";
            this.txtStatus.Style = "font-size: 12pt; font-weight: bold; text-align: right";
            this.txtStatus.Text = "??";
            this.txtStatus.Top = 0F;
            this.txtStatus.Width = 0.7086611F;
            // 
            // Line11
            // 
            this.Line11.Height = 0F;
            this.Line11.Left = 0.006944444F;
            this.Line11.LineWeight = 1F;
            this.Line11.Name = "Line11";
            this.Line11.Top = 0.006944444F;
            this.Line11.Width = 7.086611F;
            this.Line11.X1 = 0.006944444F;
            this.Line11.X2 = 7.093555F;
            this.Line11.Y1 = 0.006944444F;
            this.Line11.Y2 = 0.006944444F;
            // 
            // lblPageFooter
            // 
            this.lblPageFooter.Height = 0.1968504F;
            this.lblPageFooter.HyperLink = null;
            this.lblPageFooter.Left = 0F;
            this.lblPageFooter.Name = "lblPageFooter";
            this.lblPageFooter.Style = "font-size: 8pt; font-weight: bold; text-align: left";
            this.lblPageFooter.Text = "lblPageFooter";
            this.lblPageFooter.Top = 0.02460628F;
            this.lblPageFooter.Width = 6.299212F;
            // 
            // lblPage
            // 
            this.lblPage.Height = 0.1968504F;
            this.lblPage.HyperLink = null;
            this.lblPage.Left = 6.299212F;
            this.lblPage.Name = "lblPage";
            this.lblPage.Style = "font-size: 8pt; font-weight: bold; text-align: right";
            this.lblPage.Text = "lblPage";
            this.lblPage.Top = 0.02460628F;
            this.lblPage.Width = 0.7874014F;
            // 
            // Line4
            // 
            this.Line4.Height = 0F;
            this.Line4.Left = 0F;
            this.Line4.LineWeight = 2F;
            this.Line4.Name = "Line4";
            this.Line4.Top = 0.0384952F;
            this.Line4.Width = 7.086611F;
            this.Line4.X1 = 0F;
            this.Line4.X2 = 7.086611F;
            this.Line4.Y1 = 0.0384952F;
            this.Line4.Y2 = 0.0384952F;
            // 
            // ActiveReport1
            // 
            this.MasterReport = false;
            this.PageSettings.DefaultPaperSize = false;
            this.PageSettings.Margins.Bottom = 0.7875F;
            this.PageSettings.Margins.Left = 1.377778F;
            this.PageSettings.Margins.Right = 0.39375F;
            this.PageSettings.Margins.Top = 0.7875F;
            this.PageSettings.PaperHeight = 11.69306F;
            this.PageSettings.PaperKind = System.Drawing.Printing.PaperKind.A4;
            this.PageSettings.PaperName = "Custom paper";
            this.PageSettings.PaperWidth = 8.268056F;
            this.PrintWidth = 7.086611F;
            this.Sections.Add(this.ReportHeader);
            this.Sections.Add(this.PageHeader);
            this.Sections.Add(this.Detail);
            this.Sections.Add(this.PageFooter);
            this.Sections.Add(this.ReportFooter);
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule(resources.GetString("$this.StyleSheet"), "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: inherit; font-style: inherit; font-variant: inherit; font-weight: bo" +
                        "ld; font-size: 16pt; font-size-adjust: inherit; font-stretch: inherit", "Heading1", "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: Times New Roman; font-style: italic; font-variant: inherit; font-wei" +
                        "ght: bold; font-size: 14pt; font-size-adjust: inherit; font-stretch: inherit", "Heading2", "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: inherit; font-style: inherit; font-variant: inherit; font-weight: bo" +
                        "ld; font-size: 13pt; font-size-adjust: inherit; font-stretch: inherit", "Heading3", "Normal"));
            ((System.ComponentModel.ISupportInitialize)(this.txtReportTitle)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtMainStatus)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtProjectNameHeader)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPageHeader)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTitle)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtIzhodniDanni)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TextBox2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtIzhDanniDate1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TextBox4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtIzhDanniDate2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPismoLabel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TextBox7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPismoDate1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPrisDogovorLabel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPrisDogovorDate1Label)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPrisDogovorDate1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtProjectLabel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtProjectDate1Label)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtProjectDate1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtProjectDate2Label)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtProjectDate2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAmountLabel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAmount)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtNumber)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TextBox82)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TextBox83)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPrisDogovorStatus)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtProjectStatus)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtNumberLabel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtBelejkiLabel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtBelejki)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtStatus)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblPageFooter)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblPage)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();

            // Attach Report Events
            this.Detail.Format += new System.EventHandler(this.Detail_Format);
            this.ReportHeader.Format += new System.EventHandler(this.ReportHeader_Format);
            this.PageHeader.Format += new System.EventHandler(this.PageHeader_Format);
            this.PageFooter.Format += new System.EventHandler(this.PageFooter_Format);
            this.ReportStart += new System.EventHandler(this.InfrastrukturniDanniSubreport_ReportStart);
        }

		#endregion

        private ReportHeader ReportHeader;
        private TextBox txtReportTitle;
        private TextBox txtMainStatus;
        private TextBox txtProjectNameHeader;
        private Line Line3;
        private PageHeader PageHeader;
        private TextBox txtPageHeader;
        private Line Line2;
        private Detail Detail;
        private TextBox txtTitle;
        private TextBox txtIzhodniDanni;
        private TextBox TextBox2;
        private TextBox txtIzhDanniDate1;
        private TextBox TextBox4;
        private TextBox txtIzhDanniDate2;
        private TextBox txtPismoLabel;
        private TextBox TextBox7;
        private TextBox txtPismoDate1;
        private TextBox txtPrisDogovorLabel;
        private TextBox txtPrisDogovorDate1Label;
        private TextBox txtPrisDogovorDate1;
        private TextBox txtProjectLabel;
        private TextBox txtProjectDate1Label;
        private TextBox txtProjectDate1;
        private TextBox txtProjectDate2Label;
        private TextBox txtProjectDate2;
        private TextBox txtAmountLabel;
        private TextBox txtAmount;
        private TextBox txtNumber;
        private TextBox TextBox82;
        private TextBox TextBox83;
        private TextBox txtPrisDogovorStatus;
        private TextBox txtProjectStatus;
        private TextBox txtNumberLabel;
        private TextBox txtBelejkiLabel;
        private TextBox txtBelejki;
        private Line Line1;
        private TextBox txtStatus;
        private PageFooter PageFooter;
        private Line Line11;
        private Label lblPageFooter;
        private Label lblPage;
        private ReportFooter ReportFooter;
        private Line Line4;
	}
}
