using System;
using System.Collections;
using System.Text;

namespace Asa.Timesheet.Data
{
	/// <summary>
	/// Summary description for ProjectLogData.
	/// </summary>
	public class ProjectLogData
	{
		private int _projectLogID;
		private int _fileID;
		private DateTime _actionDate;
		private bool _createOrDownload;
		private string _actionAccount;
		private string _fileSubject = string.Empty;
		private string _fileMinutes = string.Empty;
		private string _categoryName= string.Empty;
		private string _projectName= string.Empty;
		private bool _deleted;
		private DateTime _createdDate;


		public ProjectLogData()
		{
			//
			// TODO: Add constructor logic here
			//
		}

		public ProjectLogData( int projectLogID, int fileID,DateTime actionDate, bool createOrDownload, string actionAccount)
		{
			this._projectLogID = projectLogID ;
			this._fileID = fileID ;
			this._actionDate = actionDate ;
			this._createOrDownload = createOrDownload ;
			this._actionAccount = actionAccount ;
		}	

	
		public object GetByIndex(int nIndex)
		{
			switch(nIndex)
			{
				case 0:
					return _projectLogID;
				case 1:
					return _fileID;
				case 2:
					return _actionDate;
				case 3:
					return _createOrDownload;
				case 4:
					return _actionAccount;
				case 5:
					return _fileSubject;
				case 6:
					return _fileMinutes;
				case 7:
					return _categoryName;
				case 8:
					return _projectName;
				
			}

			return null;
		}
		#region Properties
		public bool Deleted
		{
			get { return _deleted; }
			set { _deleted = value; }
		}
		
		public int ProjectLogID
		{
			get { return _projectLogID; }
			set { _projectLogID = value; }
		}
		public int FileID
		{
			get { return _fileID; }
			set { _fileID = value; }
		}
		public DateTime ActionDate
		{
			get { return _actionDate; }
			set { _actionDate = value; }
		}
		public DateTime CreatedDate
		{
			get { return _createdDate; }
			set { _createdDate = value; }
		}
		
		public bool CreateOrDownload
		{
			get { return _createOrDownload; }
			set { _createOrDownload = value; }
		}
		public string CreateOrDownloadString
		{
			get 
			{ if(_createOrDownload)
				  return Resource.ResourceManager["Log_NotDownload_BG"];
			  else
				   return Resource.ResourceManager["Log_Download_BG"];
				  }
			set
			{
			}
			
		}
		public string ActionDateString
		{
			get 
			{
				
				 return _actionDate.ToShortDateString()+" "+_actionDate.ToShortTimeString();
			}
			set
			{
			}
			
		}
		public string ActionAccount
		{
			get { return _actionAccount; }
			set { _actionAccount = value; }
		}
		public string FileSubject
		{
			get { return _fileSubject; }
			set { _fileSubject = value; }
		}
		public string FileMinutes
		{
			get { return _fileMinutes; }
			set { _fileMinutes = value; }
		}
		public string CategoryName
		{
			get { return _categoryName; }
			set { _categoryName = value; }
		}
		public string ProjectName
		{
			get { return _projectName; }
			set { _projectName = value; }
		}
		
	
		
	
		#endregion


	}
}
