﻿<%@ Page language="c#" Codebehind="EditCategory.aspx.cs" AutoEventWireup="false" Inherits="Asa.Timesheet.WebPages.EditCategory" %>
<%@ Register TagPrefix="uc1" TagName="PageHeader" Src="UserControls/PageHeader.ascx" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
	<head runat="server">
		<title>EditCategory</title>
		<meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
		<meta content="C#" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<LINK href="styles/timesheet.css" type="text/css" rel="stylesheet">
	</HEAD>
	<body bottomMargin="0" leftMargin="0" topMargin="0" rightMargin="0">
		<form id="Form1" method="post" runat="server">
			<table height="100%" cellSpacing="0" cellPadding="0" width="100%" border="0">
				<tr height="5">
					<td>
						<uc1:PageHeader id="header" runat="server"></uc1:PageHeader>
					</td>
				</tr>
				<tr>
					<td>
						<table height="100%" cellSpacing="0" cellPadding="0" width="100%" border="0">
							<tr>
								<td class="menuLeft">
									<asp:PlaceHolder id="menuHolder" runat="server"></asp:PlaceHolder>
								</td>
								
								<td style="PADDING-LEFT: 5px; PADDING-TOP: 3px" vAlign="top" noWrap>
									<TABLE id="Table3" cellSpacing="0" cellPadding="0" width="880" border="0">
										<TR>
											<td style="WIDTH: 4px"></TD>
											<td>
												<TABLE id="Table4" cellSpacing="0" cellPadding="3" width="100%" border="0">
													<TR>
														<td style="WIDTH: 131px">
															<TABLE id="Table1">
																<TR>
																	<td>
																		<asp:imagebutton id="imgBack" Runat="server" ImageUrl="images/back.gif"></asp:imagebutton></TD>
																	<td vAlign="middle">
																		<asp:linkbutton id="linkBack" Font-Bold="True" CssClass="menuTable" ForeColor="SaddleBrown" Runat="server">Обратно</asp:linkbutton></TD>
																</TR>
															</TABLE>
														</TD>
														<td></TD>
													</TR>
													<TR>
														<td style="WIDTH: 131px">
															<asp:Label id="lblName" runat="server" Width="100%" CssClass="enterDataLabel" Font-Bold="True"
																EnableViewState="False"> Тема:</asp:Label></TD>
														<td>
															<asp:TextBox id="txtName" runat="server" Width="504px" CssClass="enterDataBox"></asp:TextBox>&nbsp;<IMG height="16" alt="" src="images/required1.gif" width="16"></TD>
													</TR>
													<TR>
														<td style="WIDTH: 131px">
															<asp:Label id="Label1" runat="server" CssClass="enterDataLabel" EnableViewState="False" Width="100%"> Тема(на английски):</asp:Label></TD>
														<td>
															<asp:TextBox id="txtNameEN" runat="server" CssClass="enterDataBox" Width="504px"></asp:TextBox></TD>
													</TR>
												</TABLE>
											</TD>
										</TR>
										<TR>
											<td style="HEIGHT: 5px" colSpan="2"></TD>
										</TR>
										<TR>
											<td colspan="2"><img src="images/dot.gif" height="1" width="100%"></TD>
										</TR>
										<TR>
											<td colspan="2" height="3"></td>
										</TR>
										<TR>
											<td style="WIDTH: 4px"></TD>
											<td>
												<TABLE id="Table5" cellSpacing="0" cellPadding="0" border="0">
													<TR>
														<td style="WIDTH: 142px"></TD>
														<td style="WIDTH: 288px">
															<asp:Button id="btnSave" runat="server" Text="Запиши" CssClass="ActionButton"></asp:Button></TD>
														<td></TD>
													</TR>
												</TABLE>
											</TD>
										</TR>
										<TR>
											<td height="3"></TD>
										</TR>
										<TR>
											<td colspan="2"><img src="images/dot.gif" height="1" width="100%"></TD>
										</TR>
									</TABLE>
									<br>
									&nbsp;
									<asp:label id="lblInfo" runat="server" CssClass="InfoLabel"></asp:label>
									<asp:label id="lblError" runat="server" ForeColor="Red" CssClass="ErrorLabel"></asp:label>
								</td>
							</tr>
						</table>
					</td>
				</tr>
			</table>
		</form>
	</body>
</HTML>
