using System;
using System.Collections.Specialized;

namespace Asa.Timesheet.Data
{
	/// <summary>
	/// Summary description for MailInfo.
	/// </summary>
	
	public class MailInfo
	{
		public int UserID;
		public DateTime StartDate;
		public DateTime EndDate;
		public int ProjectID;
		public StringCollection Minutes;
		public StringCollection AdminMinutes;
		public int ActivityID;
		public int WorkTimeID;
		public bool IsFromHours=false;
		public MailInfo()
		{
			Minutes = new StringCollection();
			AdminMinutes= new StringCollection();
		}
	}
	
}
