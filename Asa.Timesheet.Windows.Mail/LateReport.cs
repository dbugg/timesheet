using System;
using System.Data;
using Asa.Timesheet.Data;
using Asa.Timesheet.Data.Util;
using GrapeCity.ActiveReports;
using GrapeCity.ActiveReports.Controls;
using GrapeCity.ActiveReports.SectionReportModel;
using GrapeCity.ActiveReports.Document.Section;
using GrapeCity.ActiveReports.Document;

namespace Asa.Timesheet.Windows.Mail
{
    public class LateReport : GrapeCity.ActiveReports.SectionReport
	{
		private bool _pdf;
		private string _createdBy= string.Empty;
        public LateReport(HeaderInfo hi, bool pdf, string createdBy, DataSet ds, WorkedHoursInfo workedHoursInfo, bool IsTraine)
        {
            _pdf = pdf;
            _createdBy = createdBy;

            InitializeComponent();
            SetFilterInfo(hi);
            this.DataSource = ds.Tables[0];
            this.ReportEnd += new EventHandler(SubAnalysisRpt1_ReportEnd);
            this.Detail.Format += new EventHandler(Detail_Format);
            this.GroupFooter1.Format += new EventHandler(GroupFooter1_Format);
            if (workedHoursInfo != null)
            {
                this.SummarySubReport.Report = new WorkTimesFooter(workedHoursInfo, false, IsTraine);
                lbUser.Visible = txtUser.Visible = true;
                txtUser.Text = hi.User;
            }
            txtCreatedBy.Text = _createdBy;
            if (IsTraine)
            {
                Label9.Visible = txtUnder.Visible = txtUnderT.Visible = false;
            }

        }

		#region ActiveReports Designer generated code








































        public void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(LateReport));
            this.Detail = new GrapeCity.ActiveReports.SectionReportModel.Detail();
            this.ReportHeader = new GrapeCity.ActiveReports.SectionReportModel.ReportHeader();
            this.ReportFooter = new GrapeCity.ActiveReports.SectionReportModel.ReportFooter();
            this.PageHeader = new GrapeCity.ActiveReports.SectionReportModel.PageHeader();
            this.PageFooter = new GrapeCity.ActiveReports.SectionReportModel.PageFooter();
            this.GroupHeader1 = new GrapeCity.ActiveReports.SectionReportModel.GroupHeader();
            this.GroupFooter1 = new GrapeCity.ActiveReports.SectionReportModel.GroupFooter();
            this.txtHeader = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtFilter4 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.lblFilter4 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lblFilter3 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.txtFilter2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.lblFilter2 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.txtFilter1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.lblFilter1 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.txtFilter3 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.Line10 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.Label4 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label5 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label6 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.TextBox2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.Label8 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label9 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label11 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.txtProjectName = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtTotalTime = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtOvertime = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtTotal = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtNormativ = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtUnder = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.TextBox3 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.Line12 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.txtUnderT = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtTotalTimeT = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.Label10 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.txtUser = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.lbUser = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.txtCreatedBy = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.Label7 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.SummarySubReport = new GrapeCity.ActiveReports.SectionReportModel.SubReport();
            ((System.ComponentModel.ISupportInitialize)(this.txtHeader)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtFilter4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblFilter4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblFilter3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtFilter2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblFilter2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtFilter1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblFilter1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtFilter3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TextBox2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label11)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtProjectName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTotalTime)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtOvertime)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTotal)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtNormativ)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtUnder)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TextBox3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtUnderT)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTotalTimeT)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtUser)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbUser)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCreatedBy)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            // 
            // Detail
            // 
            this.Detail.ColumnSpacing = 0F;
            this.Detail.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
						this.txtProjectName,
						this.txtTotalTime,
						this.txtOvertime,
						this.txtTotal,
						this.txtNormativ,
						this.txtUnder,
						this.TextBox3});
            this.Detail.Height = 0.2F;
            this.Detail.Name = "Detail";
            // 
            // ReportHeader
            // 
            this.ReportHeader.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
						this.txtHeader});
            this.ReportHeader.Height = 0.2618056F;
            this.ReportHeader.Name = "ReportHeader";
            // 
            // ReportFooter
            // 
            this.ReportFooter.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
						this.txtUser,
						this.lbUser,
						this.txtCreatedBy,
						this.Label7,
						this.SummarySubReport});
            this.ReportFooter.Height = 0.8236111F;
            this.ReportFooter.Name = "ReportFooter";
            // 
            // PageHeader
            // 
            this.PageHeader.Height = 0F;
            this.PageHeader.Name = "PageHeader";
            // 
            // PageFooter
            // 
            this.PageFooter.Height = 0F;
            this.PageFooter.Name = "PageFooter";
            // 
            // GroupHeader1
            // 
            this.GroupHeader1.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
						this.txtFilter4,
						this.lblFilter4,
						this.lblFilter3,
						this.txtFilter2,
						this.lblFilter2,
						this.txtFilter1,
						this.lblFilter1,
						this.txtFilter3,
						this.Line10,
						this.Label4,
						this.Label5,
						this.Label6,
						this.TextBox2,
						this.Label8,
						this.Label9,
						this.Label11});
            this.GroupHeader1.Height = 1.353472F;
            this.GroupHeader1.Name = "GroupHeader1";
            // 
            // GroupFooter1
            // 
            this.GroupFooter1.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
						this.Line12,
						this.txtUnderT,
						this.txtTotalTimeT,
						this.Label10});
            this.GroupFooter1.Height = 0.5819445F;
            this.GroupFooter1.Name = "GroupFooter1";
            // 
            // txtHeader
            // 
            this.txtHeader.Height = 0.2F;
            this.txtHeader.Left = 0.0625F;
            this.txtHeader.Name = "txtHeader";
            this.txtHeader.Style = "font-size: 12pt; font-weight: bold; text-align: center";
            this.txtHeader.Text = "??????? ?? ??????? ?????";
            this.txtHeader.Top = 0.0625F;
            this.txtHeader.Width = 7.3125F;
            // 
            // txtFilter4
            // 
            this.txtFilter4.Height = 0.2F;
            this.txtFilter4.Left = 2.024606F;
            this.txtFilter4.Name = "txtFilter4";
            this.txtFilter4.Style = "font-size: 9pt; vertical-align: middle";
            this.txtFilter4.Top = 0.5905511F;
            this.txtFilter4.Visible = false;
            this.txtFilter4.Width = 3.937008F;
            // 
            // lblFilter4
            // 
            this.lblFilter4.Height = 0.2F;
            this.lblFilter4.HyperLink = null;
            this.lblFilter4.Left = 0.25F;
            this.lblFilter4.Name = "lblFilter4";
            this.lblFilter4.Style = "font-size: 9pt; font-weight: bold; vertical-align: middle";
            this.lblFilter4.Text = "";
            this.lblFilter4.Top = 0.5905511F;
            this.lblFilter4.Visible = false;
            this.lblFilter4.Width = 1.75F;
            // 
            // lblFilter3
            // 
            this.lblFilter3.Height = 0.2F;
            this.lblFilter3.HyperLink = null;
            this.lblFilter3.Left = 0.25F;
            this.lblFilter3.Name = "lblFilter3";
            this.lblFilter3.Style = "font-size: 9pt; font-weight: bold; vertical-align: middle";
            this.lblFilter3.Text = "";
            this.lblFilter3.Top = 0.3937007F;
            this.lblFilter3.Visible = false;
            this.lblFilter3.Width = 1.75F;
            // 
            // txtFilter2
            // 
            this.txtFilter2.Height = 0.2F;
            this.txtFilter2.Left = 2.024606F;
            this.txtFilter2.Name = "txtFilter2";
            this.txtFilter2.Style = "font-size: 9pt; vertical-align: middle";
            this.txtFilter2.Top = 0.1968504F;
            this.txtFilter2.Visible = false;
            this.txtFilter2.Width = 3.937008F;
            // 
            // lblFilter2
            // 
            this.lblFilter2.Height = 0.2F;
            this.lblFilter2.HyperLink = null;
            this.lblFilter2.Left = 0.25F;
            this.lblFilter2.Name = "lblFilter2";
            this.lblFilter2.Style = "font-size: 9pt; font-weight: bold; vertical-align: middle";
            this.lblFilter2.Text = "";
            this.lblFilter2.Top = 0.1968504F;
            this.lblFilter2.Visible = false;
            this.lblFilter2.Width = 1.75F;
            // 
            // txtFilter1
            // 
            this.txtFilter1.Height = 0.2F;
            this.txtFilter1.Left = 2.024606F;
            this.txtFilter1.Name = "txtFilter1";
            this.txtFilter1.Style = "font-size: 9pt; vertical-align: middle";
            this.txtFilter1.Top = 0F;
            this.txtFilter1.Visible = false;
            this.txtFilter1.Width = 3.937008F;
            // 
            // lblFilter1
            // 
            this.lblFilter1.Height = 0.2F;
            this.lblFilter1.HyperLink = null;
            this.lblFilter1.Left = 0.25F;
            this.lblFilter1.Name = "lblFilter1";
            this.lblFilter1.Style = "font-size: 9pt; font-weight: bold; vertical-align: middle";
            this.lblFilter1.Text = "";
            this.lblFilter1.Top = 0F;
            this.lblFilter1.Visible = false;
            this.lblFilter1.Width = 1.75F;
            // 
            // txtFilter3
            // 
            this.txtFilter3.Height = 0.2F;
            this.txtFilter3.Left = 2.024606F;
            this.txtFilter3.Name = "txtFilter3";
            this.txtFilter3.Style = "font-size: 9pt; vertical-align: middle";
            this.txtFilter3.Top = 0.3937007F;
            this.txtFilter3.Visible = false;
            this.txtFilter3.Width = 3.937008F;
            // 
            // Line10
            // 
            this.Line10.Height = 0F;
            this.Line10.Left = 0.2638889F;
            this.Line10.LineWeight = 2F;
            this.Line10.Name = "Line10";
            this.Line10.Top = 1.326389F;
            this.Line10.Width = 7.086611F;
            this.Line10.X1 = 0.2638889F;
            this.Line10.X2 = 7.3505F;
            this.Line10.Y1 = 1.326389F;
            this.Line10.Y2 = 1.326389F;
            // 
            // Label4
            // 
            this.Label4.Height = 0.1875F;
            this.Label4.HyperLink = null;
            this.Label4.Left = 3.375F;
            this.Label4.Name = "Label4";
            this.Label4.Style = "font-size: 9pt; font-weight: bold; text-align: center; ddo-char-set: 1";
            this.Label4.Text = "????";
            this.Label4.Top = 1.125F;
            this.Label4.Width = 0.5625F;
            // 
            // Label5
            // 
            this.Label5.Height = 0.1875F;
            this.Label5.HyperLink = null;
            this.Label5.Left = 5.9375F;
            this.Label5.Name = "Label5";
            this.Label5.Style = "font-size: 9pt; font-weight: bold; text-align: right; ddo-char-set: 1";
            this.Label5.Text = "???? ???. ??????:";
            this.Label5.Top = 1.125F;
            this.Label5.Width = 1.4375F;
            // 
            // Label6
            // 
            this.Label6.Height = 0.197F;
            this.Label6.HyperLink = null;
            this.Label6.Left = 0.25F;
            this.Label6.Name = "Label6";
            this.Label6.Style = "font-size: 9pt; font-weight: bold; ddo-char-set: 1";
            this.Label6.Text = "????????";
            this.Label6.Top = 1.125F;
            this.Label6.Width = 1.75F;
            // 
            // TextBox2
            // 
            this.TextBox2.Height = 0.197F;
            this.TextBox2.Left = 2F;
            this.TextBox2.Name = "TextBox2";
            this.TextBox2.Style = "color: Black; font-size: 9pt; font-weight: bold; text-align: center; ddo-char-set" +
                ": 1";
            this.TextBox2.Text = "????";
            this.TextBox2.Top = 1.125F;
            this.TextBox2.Width = 0.8125F;
            // 
            // Label8
            // 
            this.Label8.Height = 0.1875F;
            this.Label8.HyperLink = null;
            this.Label8.Left = 2.8125F;
            this.Label8.Name = "Label8";
            this.Label8.Style = "font-size: 9pt; font-weight: bold; text-align: center; ddo-char-set: 1";
            this.Label8.Text = "??????";
            this.Label8.Top = 1.125F;
            this.Label8.Width = 0.5625F;
            // 
            // Label9
            // 
            this.Label9.Height = 0.1875F;
            this.Label9.HyperLink = null;
            this.Label9.Left = 3.9375F;
            this.Label9.Name = "Label9";
            this.Label9.Style = "font-size: 9pt; font-weight: bold; text-align: right; ddo-char-set: 1";
            this.Label9.Text = "??????????";
            this.Label9.Top = 1.125F;
            this.Label9.Width = 0.8125F;
            // 
            // Label11
            // 
            this.Label11.Height = 0.1875F;
            this.Label11.HyperLink = null;
            this.Label11.Left = 4.75F;
            this.Label11.Name = "Label11";
            this.Label11.Style = "font-size: 9pt; font-weight: bold; text-align: right; ddo-char-set: 1";
            this.Label11.Text = "???????(??-??)";
            this.Label11.Top = 1.125F;
            this.Label11.Width = 1.1875F;
            // 
            // txtProjectName
            // 
            this.txtProjectName.DataField = "FullName";
            this.txtProjectName.Height = 0.2F;
            this.txtProjectName.Left = 0.25F;
            this.txtProjectName.Name = "txtProjectName";
            this.txtProjectName.Top = 0F;
            this.txtProjectName.Width = 1.75F;
            // 
            // txtTotalTime
            // 
            this.txtTotalTime.DataField = "TotalMinutes";
            this.txtTotalTime.Height = 0.2F;
            this.txtTotalTime.Left = 6F;
            this.txtTotalTime.Name = "txtTotalTime";
            this.txtTotalTime.OutputFormat = resources.GetString("txtTotalTime.OutputFormat");
            this.txtTotalTime.Style = "text-align: right";
            this.txtTotalTime.Text = "WorkedSalary";
            this.txtTotalTime.Top = 0F;
            this.txtTotalTime.Width = 1.375F;
            // 
            // txtOvertime
            // 
            this.txtOvertime.DataField = "EndTime";
            this.txtOvertime.Height = 0.2F;
            this.txtOvertime.Left = 3.375F;
            this.txtOvertime.Name = "txtOvertime";
            this.txtOvertime.OutputFormat = resources.GetString("txtOvertime.OutputFormat");
            this.txtOvertime.Style = "text-align: center";
            this.txtOvertime.Text = "WorkedHours";
            this.txtOvertime.Top = 0F;
            this.txtOvertime.Width = 0.5625F;
            // 
            // txtTotal
            // 
            this.txtTotal.DataField = "StartTime";
            this.txtTotal.Height = 0.2F;
            this.txtTotal.Left = 2.8125F;
            this.txtTotal.Name = "txtTotal";
            this.txtTotal.OutputFormat = resources.GetString("txtTotal.OutputFormat");
            this.txtTotal.Style = "text-align: center";
            this.txtTotal.Top = 0F;
            this.txtTotal.Width = 0.5625F;
            // 
            // txtNormativ
            // 
            this.txtNormativ.DataField = "Workdate";
            this.txtNormativ.Height = 0.2F;
            this.txtNormativ.Left = 2F;
            this.txtNormativ.Name = "txtNormativ";
            this.txtNormativ.OutputFormat = resources.GetString("txtNormativ.OutputFormat");
            this.txtNormativ.Style = "text-align: center; vertical-align: top";
            this.txtNormativ.Top = 0F;
            this.txtNormativ.Width = 0.8125F;
            // 
            // txtUnder
            // 
            this.txtUnder.DataField = "MinutesLate";
            this.txtUnder.Height = 0.2F;
            this.txtUnder.Left = 3.9375F;
            this.txtUnder.Name = "txtUnder";
            this.txtUnder.OutputFormat = resources.GetString("txtUnder.OutputFormat");
            this.txtUnder.Style = "text-align: right";
            this.txtUnder.Text = "WorkedHours";
            this.txtUnder.Top = 0F;
            this.txtUnder.Width = 0.8125F;
            // 
            // TextBox3
            // 
            this.TextBox3.DataField = "Details";
            this.TextBox3.Height = 0.2F;
            this.TextBox3.Left = 4.8125F;
            this.TextBox3.Name = "TextBox3";
            this.TextBox3.OutputFormat = resources.GetString("TextBox3.OutputFormat");
            this.TextBox3.Style = "text-align: right";
            this.TextBox3.Text = "WorkedHours";
            this.TextBox3.Top = 0F;
            this.TextBox3.Width = 1.125F;
            // 
            // Line12
            // 
            this.Line12.Height = 0F;
            this.Line12.Left = 0.2569444F;
            this.Line12.LineWeight = 1F;
            this.Line12.Name = "Line12";
            this.Line12.Top = 0.006944418F;
            this.Line12.Width = 13.01944F;
            this.Line12.X1 = 0.2569444F;
            this.Line12.X2 = 13.27639F;
            this.Line12.Y1 = 0.006944418F;
            this.Line12.Y2 = 0.006944418F;
            // 
            // txtUnderT
            // 
            this.txtUnderT.DataField = "MinutesLate";
            this.txtUnderT.Height = 0.2F;
            this.txtUnderT.Left = 3.947917F;
            this.txtUnderT.Name = "txtUnderT";
            this.txtUnderT.OutputFormat = resources.GetString("txtUnderT.OutputFormat");
            this.txtUnderT.Style = "text-align: right";
            this.txtUnderT.SummaryRunning = GrapeCity.ActiveReports.SectionReportModel.SummaryRunning.Group;
            this.txtUnderT.SummaryType = GrapeCity.ActiveReports.SectionReportModel.SummaryType.GrandTotal;
            this.txtUnderT.Text = "WorkedHours";
            this.txtUnderT.Top = 0F;
            this.txtUnderT.Width = 0.8020833F;
            // 
            // txtTotalTimeT
            // 
            this.txtTotalTimeT.DataField = "TotalMinutes";
            this.txtTotalTimeT.Height = 0.2F;
            this.txtTotalTimeT.Left = 6F;
            this.txtTotalTimeT.Name = "txtTotalTimeT";
            this.txtTotalTimeT.OutputFormat = resources.GetString("txtTotalTimeT.OutputFormat");
            this.txtTotalTimeT.Style = "text-align: right";
            this.txtTotalTimeT.SummaryRunning = GrapeCity.ActiveReports.SectionReportModel.SummaryRunning.Group;
            this.txtTotalTimeT.SummaryType = GrapeCity.ActiveReports.SectionReportModel.SummaryType.GrandTotal;
            this.txtTotalTimeT.Text = "WorkedSalary";
            this.txtTotalTimeT.Top = 0F;
            this.txtTotalTimeT.Width = 1.375F;
            // 
            // Label10
            // 
            this.Label10.Height = 0.197F;
            this.Label10.HyperLink = null;
            this.Label10.Left = 0.25F;
            this.Label10.Name = "Label10";
            this.Label10.Style = "font-size: 9pt; font-weight: bold; ddo-char-set: 1";
            this.Label10.Text = "????:";
            this.Label10.Top = 0F;
            this.Label10.Width = 1.75F;
            // 
            // txtUser
            // 
            this.txtUser.Height = 0.2F;
            this.txtUser.Left = 5.625F;
            this.txtUser.Name = "txtUser";
            this.txtUser.Style = "font-size: 10pt; font-weight: normal; text-align: justify";
            this.txtUser.Text = "TextBox9";
            this.txtUser.Top = 0.625F;
            this.txtUser.Visible = false;
            this.txtUser.Width = 1.5F;
            // 
            // lbUser
            // 
            this.lbUser.Height = 0.2F;
            this.lbUser.HyperLink = null;
            this.lbUser.Left = 4.8125F;
            this.lbUser.Name = "lbUser";
            this.lbUser.Style = "font-size: 11pt; text-align: right";
            this.lbUser.Text = "????????: ......................................";
            this.lbUser.Top = 0.4375F;
            this.lbUser.Visible = false;
            this.lbUser.Width = 0.8125F;
            // 
            // txtCreatedBy
            // 
            this.txtCreatedBy.Height = 0.2F;
            this.txtCreatedBy.Left = 1.0625F;
            this.txtCreatedBy.Name = "txtCreatedBy";
            this.txtCreatedBy.Style = "font-size: 10pt; font-weight: normal; text-align: justify";
            this.txtCreatedBy.Text = "txtCreatedBy";
            this.txtCreatedBy.Top = 0.625F;
            this.txtCreatedBy.Width = 1.8F;
            // 
            // Label7
            // 
            this.Label7.Height = 0.2F;
            this.Label7.HyperLink = null;
            this.Label7.Left = 0.25F;
            this.Label7.Name = "Label7";
            this.Label7.Style = "font-size: 11pt; text-align: right";
            this.Label7.Text = "????????: ......................................";
            this.Label7.Top = 0.4375F;
            this.Label7.Width = 0.8125F;
            // 
            // SummarySubReport
            // 
            this.SummarySubReport.CloseBorder = false;
            this.SummarySubReport.Height = 0.1968504F;
            this.SummarySubReport.Left = 0.3125F;
            this.SummarySubReport.Name = "SummarySubReport";
            this.SummarySubReport.Report = null;
            this.SummarySubReport.Top = 0F;
            this.SummarySubReport.Width = 7.086611F;
            // 
            // ActiveReport1
            // 
            this.MasterReport = false;
            this.PageSettings.PaperHeight = 11F;
            this.PageSettings.PaperWidth = 8.5F;
            this.PrintWidth = 7.365F;
            this.Sections.Add(this.ReportHeader);
            this.Sections.Add(this.PageHeader);
            this.Sections.Add(this.GroupHeader1);
            this.Sections.Add(this.Detail);
            this.Sections.Add(this.GroupFooter1);
            this.Sections.Add(this.PageFooter);
            this.Sections.Add(this.ReportFooter);
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule(resources.GetString("$this.StyleSheet"), "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: inherit; font-style: inherit; font-variant: inherit; font-weight: bo" +
                        "ld; font-size: 16pt; font-size-adjust: inherit; font-stretch: inherit", "Heading1", "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: Times New Roman; font-style: italic; font-variant: inherit; font-wei" +
                        "ght: bold; font-size: 14pt; font-size-adjust: inherit; font-stretch: inherit", "Heading2", "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: inherit; font-style: inherit; font-variant: inherit; font-weight: bo" +
                        "ld; font-size: 13pt; font-size-adjust: inherit; font-stretch: inherit", "Heading3", "Normal"));
            ((System.ComponentModel.ISupportInitialize)(this.txtHeader)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtFilter4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblFilter4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblFilter3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtFilter2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblFilter2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtFilter1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblFilter1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtFilter3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TextBox2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label11)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtProjectName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTotalTime)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtOvertime)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTotal)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtNormativ)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtUnder)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TextBox3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtUnderT)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTotalTimeT)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtUser)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbUser)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCreatedBy)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();
        }

		#endregion

        private void SetFilterInfo(HeaderInfo hi)
        {
            int currentLabel = 1;

            if (hi.StartDate > Constants.DateMin)
            {
                Label lbl = (Label)this.GroupHeader1.Controls["lblFilter" + currentLabel.ToString()];
                lbl.Visible = true;
                lbl.Text = Resource.ResourceManager["reports_Period"];
                TextBox txt = (TextBox)this.GroupHeader1.Controls["txtFilter" + currentLabel.ToString()];
                txt.Visible = true;
                txt.Text = String.Concat(hi.StartDate.ToString("d MMM yyyy"), " ",
                    Resource.ResourceManager["reports_YearAbbr"], " - ",
                    hi.EndDate.ToString("d MMM yyyy"), " ",
                    Resource.ResourceManager["reports_YearAbbr"]);
                currentLabel++;
            }

            if (hi.User != String.Empty)
            {
                Label lbl = (Label)this.GroupHeader1.Controls["lblFilter" + currentLabel.ToString()];
                lbl.Visible = true;
                lbl.Text = Resource.ResourceManager["reports_User"];
                TextBox txt = (TextBox)this.GroupHeader1.Controls["txtFilter" + currentLabel.ToString()];
                txt.Visible = true;
                txt.Text = hi.User;
                currentLabel++;
            }

            if (hi.Activity != String.Empty)
            {
                Label lbl = (Label)this.GroupHeader1.Controls["lblFilter" + currentLabel.ToString()];
                lbl.Visible = true;
                lbl.Text = Resource.ResourceManager["reports_Activity"];
                TextBox txt = (TextBox)this.GroupHeader1.Controls["txtFilter" + currentLabel.ToString()];
                txt.Visible = true;
                txt.Text = hi.Activity;
                currentLabel++;
            }

            if (hi.BuildingType != String.Empty)
            {
                Label lbl = (Label)this.GroupHeader1.Controls["lblFilter" + currentLabel.ToString()];
                lbl.Visible = true;
                lbl.Text = Resource.ResourceManager["reports_BuildingType"];
                TextBox txt = (TextBox)this.GroupHeader1.Controls["txtFilter" + currentLabel.ToString()];
                txt.Visible = true;
                txt.Text = hi.BuildingType;
                currentLabel++;
            }
            if (SessionManager.LoggedUserInfo != null && !SessionManager.LoggedUserInfo.HasPaymentRights)
            {
                Label5.Visible = false;
            }

        }
        private void ReportFooter_Format(object sender, System.EventArgs eArgs)
        {

        }

        private void PageHeader_Format(object sender, System.EventArgs eArgs)
        {
            if (!_pdf)
                if (this.PageNumber > 1)
                    this.PageHeader.Visible = false;
        }

        private void Detail_Format(object sender, System.EventArgs eArgs)
        {

            if (!_pdf)
            {

                if (txtTotalTime.Text != null && txtTotalTime.Text != "")
                    txtTotalTime.Text = TimeHelper.HoursFromMinutes((int)decimal.Parse(txtTotalTime.Text)).ToString("0.00");
                if (txtUnder.Text != null && txtUnder.Text != "")
                    txtUnder.Text = TimeHelper.HoursFromMinutes((int)decimal.Parse(txtUnder.Text)).ToString("0.00");
            }
            else
            {

                if (txtTotalTime.Text != null && txtTotalTime.Text != "")
                    txtTotalTime.Text = TimeHelper.HoursStringFromMinutes((int)decimal.Parse(txtTotalTime.Text), false);
                if (txtUnder.Text != null && txtUnder.Text != "")
                    txtUnder.Text = TimeHelper.HoursStringFromMinutes((int)decimal.Parse(txtUnder.Text), false);
            }
        }
        private void SubAnalysisRpt1_ReportEnd(object sender, EventArgs e)
        {
            if (!this._pdf) return;

            Logo1 logo = new Logo1();
            logo.Run();

            for (int i = 0; i < this.Document.Pages.Count; i++)
                this.Document.Pages[i].Overlay(logo.Document.Pages[0]);
        }
        private void GroupFooter1_Format(object sender, EventArgs e)
        {
            if (!_pdf)
            {

                if (txtTotalTimeT.Text != null)
                    txtTotalTimeT.Text = TimeHelper.HoursFromMinutes((int)decimal.Parse(txtTotalTimeT.Text)).ToString("0.00");
                if (txtUnderT.Text != null)
                    txtUnderT.Text = TimeHelper.HoursFromMinutes((int)decimal.Parse(txtUnderT.Text)).ToString("0.00");
            }
            else
            {

                if (txtTotalTimeT.Text != null)
                    txtTotalTimeT.Text = TimeHelper.HoursStringFromMinutes((int)decimal.Parse(txtTotalTimeT.Text), false);
                if (txtUnderT.Text != null)
                    txtUnderT.Text = TimeHelper.HoursStringFromMinutes((int)decimal.Parse(txtUnderT.Text), false);
            }
        }

        private ReportHeader ReportHeader;
        private TextBox txtHeader;
        private PageHeader PageHeader;
        private GroupHeader GroupHeader1;
        private TextBox txtFilter4;
        private Label lblFilter4;
        private Label lblFilter3;
        private TextBox txtFilter2;
        private Label lblFilter2;
        private TextBox txtFilter1;
        private Label lblFilter1;
        private TextBox txtFilter3;
        private Line Line10;
        private Label Label4;
        private Label Label5;
        private Label Label6;
        private TextBox TextBox2;
        private Label Label8;
        private Label Label9;
        private Label Label11;
        private Detail Detail;
        private TextBox txtProjectName;
        private TextBox txtTotalTime;
        private TextBox txtOvertime;
        private TextBox txtTotal;
        private TextBox txtNormativ;
        private TextBox txtUnder;
        private TextBox TextBox3;
        private GroupFooter GroupFooter1;
        private Line Line12;
        private TextBox txtUnderT;
        private TextBox txtTotalTimeT;
        private Label Label10;
        private PageFooter PageFooter;
        private ReportFooter ReportFooter;
        private TextBox txtUser;
        private Label lbUser;
        private TextBox txtCreatedBy;
        private Label Label7;
        private SubReport SummarySubReport;
	}

}
