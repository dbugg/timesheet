using System;
using System.Resources;
using System.Reflection; 
using System.IO;

namespace Asa.Timesheet.Data
{
	/// <summary>
	/// Helper class used to manage application Resources
	/// </summary>
	public sealed class Resource
	{
		#region Static part
		private const string ResourceFileName = ".timesheet";

		static Resource InternalResource = new Resource();
		/// <summary>
		/// Gets a resource manager for the assembly resource file
		/// </summary>
		public static Resource ResourceManager
		{
			get
			{
				return InternalResource;
			}
		}
		#endregion
		
		#region Instance part 
		ResourceManager rm = null;

		/// <summary>
		/// Constructor
		/// </summary>
		public Resource()
		{
			rm = new ResourceManager(this.GetType().Namespace + ResourceFileName, Assembly.GetExecutingAssembly());
		}

		/// <summary>
		/// Gets the message with the specified key from the assembly resource file
		/// </summary>
		public string this [ string key ]
		{
			get
			{
				return rm.GetString( key, System.Globalization.CultureInfo.CurrentCulture );
			}
		}

		/// <summary>
		/// Gets a resource stream with the messages used by the UIP classes
		/// </summary>
		/// <param name="name">resource key</param>
		/// <returns>a resource stream</returns>
		public Stream GetStream( string name )
		{
			return Assembly.GetExecutingAssembly().GetManifestResourceStream(this.GetType().Namespace + "." + name); 
		}

		/// <summary>
		/// Formats a message stored in the UIP assembly resource file.
		/// </summary>
		/// <param name="key">resource key</param>
		/// <param name="format">format arguments</param>
		/// <returns>a formated string</returns>
		public string FormatMessage( string key, params object[] format )
		{
			return String.Format( System.Globalization.CultureInfo.CurrentUICulture, this[key], format );  
		}
		#endregion
	}
}