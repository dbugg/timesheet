﻿<%@ Page language="c#" Codebehind="Protokols.aspx.cs" AutoEventWireup="false" Inherits="Asa.Timesheet.WebPages.Protokols" %>
<%@ Register TagPrefix="uc1" TagName="PageHeader" Src="UserControls/PageHeader.ascx" %>
<%@ Register TagPrefix="radg" Namespace="Telerik.WebControls" Assembly="RadGrid" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
	<head runat="server">
		<title>Protokols</title>
		<meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
		<meta content="C#" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<LINK href="styles/timesheet.css" type="text/css" rel="stylesheet">
		<script language="javascript" src="PopupCalendar.js"></script>
	</HEAD>
	<body bottomMargin="0" leftMargin="0" topMargin="0" rightMargin="0">
		<form id="Form1" method="post" runat="server">
			<table height="100%" cellSpacing="0" cellPadding="0" width="100%" border="0">
				<TBODY>
					<tr height="1">
						<td><uc1:pageheader id="header" runat="server"></uc1:pageheader></td>
					</tr>
					<tr>
						<td>
							<table height="100%" cellSpacing="0" cellPadding="0" width="100%" border="0">
								<TBODY>
									<tr>
										<td class="menuLeft"><asp:placeholder id="menuHolder" runat="server"></asp:placeholder></td>
										
										<td style="PADDING-RIGHT: 5px; PADDING-LEFT: 5px; PADDING-TOP: 3px" vAlign="top" noWrap>
											<P><br>
												<asp:label id="lblError" runat="server" ForeColor="Red" EnableViewState="False" CssClass="ErrorLabel"></asp:label><asp:label id="lblInfo" runat="server" EnableViewState="False" CssClass="InfoLabel"></asp:label><BR>
												<TABLE id="Table1" style="WIDTH: 848px; HEIGHT: 26px" cellSpacing="1" cellPadding="1" width="848"
													border="0">
													<TR>
														<td style="WIDTH: 139px" vAlign="top"><asp:label id="Label4" runat="server" CssClass="enterDataLabel" Width="100%">Период:</asp:label></TD>
														<td style="WIDTH: 252px" vAlign="top" noWrap><asp:textbox id="txtStartDate" runat="server" CssClass="enterDataBox" Width="88px" MaxLength="10"></asp:textbox>&nbsp;<IMG id="lkCalendar1" alt="Calendar" src="images/calendar.ico" width="18" align="absBottom"
																runat="server"> до&nbsp;
															<asp:textbox id="txtEndDate" runat="server" CssClass="enterDataBox" Width="88px" MaxLength="10"></asp:textbox>&nbsp;<IMG id="lkCalendar2" alt="Calendar" src="images/calendar.ico" width="18" align="absBottom"
																runat="server"></TD>
														<td>
														</TD>
														<td></TD>
													</TR>
													<TR>
														<td vAlign="top">
															<asp:label id="Label2" runat="server" CssClass="enterDataLabel" Width="100%">Тип:</asp:label></TD>
														<td style="WIDTH: 252px" vAlign="top"><asp:dropdownlist id="ddlProjectsStatus" runat="server" CssClass="EnterDataBox" Width="250px" AutoPostBack="True"></asp:dropdownlist></TD>
														<td></TD>
														<td></TD>
													</TR>
													<TR>
														<td vAlign="top"><asp:label id="Label8" runat="server" CssClass="enterDataLabel" Width="100%">Сграда:</asp:label></TD>
														<td style="WIDTH: 252px" vAlign="top"><asp:dropdownlist id="ddlBuildingTypes" runat="server" CssClass="EnterDataBox" Width="250px" AutoPostBack="True"></asp:dropdownlist></TD>
														<td></TD>
														<td></TD>
													</TR>
													<TR>
														<td vAlign="top"><asp:label id="lblProject" runat="server" CssClass="enterDataLabel" Width="100%">Проект:</asp:label></TD>
														<td style="WIDTH: 252px" vAlign="top"><asp:dropdownlist id="ddlProject" runat="server" CssClass="EnterDataBox" Width="250px"></asp:dropdownlist></TD>
														<td><asp:button id="btnSearch" runat="server" CssClass="ActionButton" Text="Филтър"></asp:button></TD>
														<td></TD>
													</TR>
													<TR>
														<td><asp:button id="btnNew" runat="server" CssClass="ActionButton" Text="Нов протокол"></asp:button></TD>
														<td></TD>
														<td></TD>
														<td></TD>
													</TR>
												</TABLE>
												<BR>
											</P>
											<asp:panel id="grid" 
												runat="server" Width="98%" Height="450px">
												<asp:datagrid id="grdMeetings" runat="server" CssClass="Grid" Width="100%" AllowSorting="True"
													PageSize="2" CellPadding="4" AutoGenerateColumns="False">
													<ItemStyle CssClass="GridItem"></ItemStyle>
													<HeaderStyle Wrap="False" CssClass="GridHeader"></HeaderStyle>
													<Columns>
														<asp:TemplateColumn HeaderText="#">
															<ItemStyle ForeColor="DimGray" Width="10px"></ItemStyle>
															<ItemTemplate>
																<asp:Label CssClass="menuTable" runat="server" Text='<%# (int)DataBinder.Eval(Container, "ItemIndex")+1 %>' ID="Label1" NAME="Label1">
																</asp:Label>
															</ItemTemplate>
														</asp:TemplateColumn>
														<asp:TemplateColumn HeaderText="Проект" SortExpression="ProjectName">
															<ItemStyle Width="20%"></ItemStyle>
															<ItemTemplate>
																<asp:LinkButton id="lnk" runat="server" CssClass="menuTable"  CommandName="Edit">
																	<span Class="menuTable">
																		<%# DataBinder.Eval(Container, "DataItem.ProjectName") %>
																	</span>
																</asp:LinkButton>
															</ItemTemplate>
														</asp:TemplateColumn>
														<asp:BoundColumn DataField="ProtokolDate" HeaderText="Дата" DataFormatString="{0:dd.MM.yyyy}" SortExpression="ProtokolDate">
															<HeaderStyle Wrap="False" Width="70px"></HeaderStyle>
															<ItemStyle ForeColor="DimGray"></ItemStyle>
														</asp:BoundColumn>
														<asp:TemplateColumn HeaderText="Приет/предаден" SortExpression="Accepted">
															<ItemStyle Width="20%"></ItemStyle>
															<ItemTemplate>
																
																	<span Class="menuTable">
																		<%# GetAccepted(DataBinder.Eval(Container, "DataItem.Accepted")) %>
																	</span>
																
															</ItemTemplate>
														</asp:TemplateColumn>
														<asp:TemplateColumn>
															<HeaderStyle Wrap="False" Width="20px"></HeaderStyle>
															<ItemStyle Wrap="False" HorizontalAlign="Center"></ItemStyle>
															<ItemTemplate>
																<asp:HyperLink id="btnScan" runat="server" ToolTip="Сканиран документ" Visible='<%# GetVisible(DataBinder.Eval(Container, "DataItem.HasScan"))%>' NavigateUrl='<%# GetURL((int)DataBinder.Eval(Container, "DataItem.ProtokolID"))%>' Target=_blank>
																	<img border="0" src="images/pdf.gif" /></asp:HyperLink>
															</ItemTemplate>
														</asp:TemplateColumn>
														<asp:TemplateColumn>
															<HeaderStyle Wrap="False" Width="20px"></HeaderStyle>
															<ItemStyle Wrap="False" HorizontalAlign="Center"></ItemStyle>
															<ItemTemplate>
																<asp:ImageButton id="btnDelete" runat="server" Width="12px" Height="12px" ToolTip="Изтрий" ImageUrl="images/delete.gif"></asp:ImageButton>
															</ItemTemplate>
														</asp:TemplateColumn>
													</Columns>
												</asp:datagrid>
											</asp:panel>
											<TABLE id="Table4" 
												cellSpacing="0" cellPadding="4" width="100%" border="0">
												<TBODY>
													<TR>
														<td>
															<asp:button id="btnExport" runat="server" CssClass="ActionButton" Text="Експорт" Visible="False"></asp:button>&nbsp;</TD>
													</TR>
												</TBODY>
											</TABLE>
										</td>
									</tr>
								</TBODY>
							</table>
						</td>
					</tr>
				</TBODY>
			</table>
			<radg:radgrid id="gridCalls" runat="server" CssClass="RadGrid" Width="100%" AutoGenerateColumns="False"
				GridLines="Horizontal">
				<PagerStyle CssClass="GridHeader" Mode="NumericPages"></PagerStyle>
				<ItemStyle HorizontalAlign="Center" CssClass="GridItem"></ItemStyle>
				<GroupPanel Visible="False"></GroupPanel>
				<HeaderStyle HorizontalAlign="Center" Wrap="False" CssClass="GridHeader"></HeaderStyle>
				<AlternatingItemStyle HorizontalAlign="Center" CssClass="GridItem"></AlternatingItemStyle>
				<GroupHeaderItemStyle BorderColor="Black" BackColor="Silver"></GroupHeaderItemStyle>
				<MasterTableView DataSourcePersistenceMode="NoPersistence" AllowCustomPaging="False" AllowSorting="True"
					PageSize="15" GridLines="Horizontal" AllowPaging="False" Visible="True">
					<Columns>
						<radg:GridBoundColumn UniqueName="StartDate" HeaderButtonType="TextButton" HeaderText="Дата" DataField="MeetingDate"
							DataFormatString="{0:dd.MM.yyyy}"></radg:GridBoundColumn>
						<radg:GridBoundColumn UniqueName="StartHour" HeaderButtonType="TextButton" HeaderText="Нач. час" DataField="StartHour"></radg:GridBoundColumn>
						<radg:GridBoundColumn UniqueName="StartHour" HeaderButtonType="TextButton" HeaderText="Нач. час" DataField="StartHour"></radg:GridBoundColumn>
						<radg:GridBoundColumn UniqueName="ProjectName" HeaderButtonType="TextButton" HeaderText="Име на проект"
							DataField="ProjectName"></radg:GridBoundColumn>
					</Columns>
					<RowIndicatorColumn Visible="False" UniqueName="RowIndicator">
						<HeaderStyle Width="20px"></HeaderStyle>
					</RowIndicatorColumn>
					<EditFormSettings>
						<EditColumn UniqueName="EditCommandColumn"></EditColumn>
					</EditFormSettings>
					<ExpandCollapseColumn ButtonType="ImageButton" Visible="False" UniqueName="ExpandColumn">
						<HeaderStyle Width="19px"></HeaderStyle>
					</ExpandCollapseColumn>
				</MasterTableView>
			</radg:radgrid></form>
	</body>
</HTML>
