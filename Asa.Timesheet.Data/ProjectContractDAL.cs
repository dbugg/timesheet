using System;
using System.Data;
using System.Data.SqlClient; 
using System.Collections;
using Microsoft.ApplicationBlocks.Data;

namespace Asa.Timesheet.Data
{
	public class ProjectContractDAL
	{
		/// <summary>
		/// Loads a ProjectContractData object from the database using the given ProjectContractID
		/// </summary>
		/// <param name="projectContractID">The primary key value</param>
		/// <returns>A ProjectContractData object</returns>
		public static ProjectContractData Load(int projectContractID)
		{
			SqlParameter[] parameterValues = new SqlParameter[] { new SqlParameter("@ProjectContractID", projectContractID) };
			using (SqlDataReader reader = SqlHelper.ExecuteReader(CommonDAL.ConnectionString, CommandType.StoredProcedure, "ProjectContractsSelProc", parameterValues))
			{
				if (reader.Read())
				{
					ProjectContractData result = new ProjectContractData();
					LoadFromReader(reader,result);
					return result;
				}
				else
					return null;
			}
		}

		/// <summary>
		/// Loads a ProjectContractData object from the database using the given where clause
		/// </summary>
		/// <param name="whereClause">The filter expression for the query</param>
		/// <returns>A ProjectContractData object</returns>
		public static ProjectContractData Load(string whereClause)
		{
			SqlParameter[] parameterValues = new SqlParameter[] { new SqlParameter("@WhereClause", whereClause) };
			using (SqlDataReader reader = SqlHelper.ExecuteReader(CommonDAL.ConnectionString, CommandType.StoredProcedure, "ProjectContractsListProc", parameterValues))
			{
				if (reader.Read())
				{
					ProjectContractData result = new ProjectContractData();
					LoadFromReader(reader,result);
					return result;
				}
				else
					return null;
			}
		}
		public static int Save( ProjectContractData source)
		{
			if (source.ProjectContractID == -1)
				return Insert(source);
			else
				return Update(source);	
		}

		public static void Delete( ProjectContractData source)
		{
			Delete(source.ProjectContractID);
		}
		/// <summary>
		/// Loads a collection of ProjectContractData objects from the database.
		/// </summary>
		/// <returns>A collection containing all of the ProjectContractData objects in the database.</returns>
		public static ProjectContractsVector LoadCollection()
		{
			SqlParameter[] parms = new SqlParameter[] {};
			return LoadCollection("ProjectContractsListProc", parms);
		}
	  
		/// <summary>
		/// Saves a collection of ProjectContractData objects to the database.
		/// </summary>
		/// <returns>A collection containing all of the ProjectContractData objects in the database.</returns>
		public static  void  SaveCollection(ProjectContractsVector col,string spName, SqlParameter[] parms)
		{
			ProjectContractsVector colOld = LoadCollection(spName,parms);
			UpdateCollection(colOld,col);
		}
		public static  void  SaveCollection(ProjectContractsVector col)
		{
			ProjectContractsVector colOld = LoadCollection();
			UpdateCollection(colOld,col);
		}
		public static  void  SaveCollection(ProjectContractsVector col, string whereClause )
		{
			ProjectContractsVector colOld = LoadCollection(whereClause);
			UpdateCollection(colOld,col);
		}
		/// <summary>
		/// Loads a collection of ProjectContractData objects from the database.
		/// </summary>
		/// <returns>A collection containing all of the ProjectContractData objects in the database.</returns>
		public static ProjectContractsVector LoadCollection(string whereClause)
		{
			SqlParameter[] parms = new SqlParameter[] { new SqlParameter("@WhereClause", whereClause) };
			return LoadCollection("ProjectContractsListProc", parms);
		}

		/// <summary>
		/// Loads a collection of ProjectContractData objects from the database.
		/// </summary>
		/// <returns>A collection containing all of the ProjectContractData objects in the database.</returns>
		public static ProjectContractsVector LoadCollection(string spName, SqlParameter[] parms)
		{
			ProjectContractsVector result = new ProjectContractsVector();
			using (SqlDataReader reader = SqlHelper.ExecuteReader(CommonDAL.ConnectionString, CommandType.StoredProcedure, spName, parms))
			{
				while (reader.Read())
				{
					ProjectContractData tmp = new ProjectContractData();
					LoadFromReader(reader, tmp);
					result.Add(tmp);
				}
			}
			return result;
		}
		/// <summary>
		/// Deletes a ProjectContractData object from the database.
		/// </summary>
		/// <param name="projectContractID">The primary key value</param>
		public static void Delete(int projectContractID)
		{
			SqlParameter[] parameterValues = new SqlParameter[] { new SqlParameter("@ProjectContractID", projectContractID) };
			SqlHelper.ExecuteNonQuery(CommonDAL.ConnectionString, CommandType.StoredProcedure, "ProjectContractsDelProc", parameterValues);
		}

		public static void LoadFromReader(SqlDataReader reader, ProjectContractData target)
		{
			if (reader != null && !reader.IsClosed)
			{
				//target.Key = reader.GetInt32(0);
				if (!reader.IsDBNull(0)) target.ProjectContractID = reader.GetInt32(0);
				if (!reader.IsDBNull(1)) target.ProjectID = reader.GetInt32(1);
				if (!reader.IsDBNull(2)) target.ProjectContractName = reader.GetString(2);
			}
		}
		
	  
		public static int Insert( ProjectContractData source)
		{
			SqlParameter[] parameterValues = GetInsertParameterValues(source);
			SqlHelper.ExecuteNonQuery(CommonDAL.ConnectionString, CommandType.StoredProcedure, "ProjectContractsInsProc", parameterValues);
			source.ProjectContractID = (int) parameterValues[0].Value;
			return source.ProjectContractID;
		}

		public static int Update( ProjectContractData source)
		{
			SqlParameter[] parameterValues = GetUpdateParameterValues(source);
			SqlHelper.ExecuteNonQuery(CommonDAL.ConnectionString, CommandType.StoredProcedure,
				"ProjectContractsUpdProc", parameterValues);
			return source.ProjectContractID;
		}
		private static void UpdateCollection(ProjectContractsVector colOld,ProjectContractsVector col)
		{
			// Delete all old items
			foreach( ProjectContractData itemOld in colOld)
			{
				if(!ExistsKey(itemOld.ProjectContractID, col))
					Delete(itemOld.ProjectContractID);
			}
			foreach( ProjectContractData item in col)
			{
				if(item.ProjectContractID <= -1)
					Insert(item);
				else
					Update(item);
			}
		}
		private static bool ExistsKey(int Key, ProjectContractsVector col)
		{
			foreach( ProjectContractData item in col)
				if(item.ProjectContractID==Key)
					return true;
			return false;
		}

		private static SqlParameter GetSqlParameter(string name, ParameterDirection direction, object value)
		{
			SqlParameter p = new SqlParameter(name, value);
			p.Direction = direction;
			return p;
		}

		private static SqlParameter[] GetInsertParameterValues( ProjectContractData source)
		{
			SqlParameter[] prams = new SqlParameter[3];
			prams[0] = GetSqlParameter("@ProjectContractID", ParameterDirection.Output, source.ProjectContractID);
			prams[1] = GetSqlParameter("@ProjectID", ParameterDirection.Input, source.ProjectID);
			prams[2] = GetSqlParameter("@ProjectContractName", ParameterDirection.Input, source.ProjectContractName);
			return prams;
		}

		private static SqlParameter[] GetUpdateParameterValues( ProjectContractData source)
		{
			return new SqlParameter[] {
										  new SqlParameter("@ProjectContractID", source.ProjectContractID),
										  new SqlParameter("@ProjectID", source.ProjectID),
										  new SqlParameter("@ProjectContractName", source.ProjectContractName)
									  };
		}
	}
}   

