using System;
using System.Xml;
using System.Xml.Serialization;
using System.IO;
using System.Text;
namespace Asa.Timesheet.Data
{
	/// <summary>
	/// Serialization Helpers
	/// </summary>
	public class Serialization
	{
		/// <summary>
		/// Serializes object and returns the string
		/// </summary>
		/// <param name="obj">Object to serialize</param>
		/// <returns>String of the serialized object</returns>
		public static string SerializeObjectToXmlString(object obj)
		{
			System.IO.MemoryStream xmls = new System.IO.MemoryStream();
			XmlSerializer serializer = new XmlSerializer(obj.GetType());
			TextWriter writer = new StreamWriter(xmls);
			serializer.Serialize(writer, obj);
			xmls.Position=0;
			StreamReader sr = new StreamReader(xmls);
			string result;
			result=sr.ReadToEnd();
			sr.Close();
			writer.Close(); 
			return result; 
		}

		/// <summary>
		/// Converts xml to string
		/// </summary>
		/// <param name="xml">Xml to convert</param>
		/// <returns>The xml string</returns>
		public static string Xml2String(XmlDocument xml)
		{
			System.IO.MemoryStream xmls = new System.IO.MemoryStream();
			XmlTextWriter writer = new XmlTextWriter(xmls,System.Text.Encoding.UTF8);
			xml.WriteTo(writer);
			xmls.Position = 0;
			XmlTextReader r = new XmlTextReader(xmls);
			StringBuilder b = new StringBuilder();
			// Moves the reader to the root element.
			r.MoveToContent();

			do
			{
				string s = r.ReadOuterXml();
				b.Append(s);
			}while(r.Read());

			writer.Close();
			r.Close();
			string sRet= b.ToString();
			return sRet;

		}
	}
}
