using System;
using Asa.Timesheet.Data;
using Asa.Timesheet.Data.Util;
using GrapeCity.ActiveReports;
using GrapeCity.ActiveReports.Controls;
using GrapeCity.ActiveReports.SectionReportModel;
using GrapeCity.ActiveReports.Document.Section;
using GrapeCity.ActiveReports.Document;

namespace Asa.Timesheet.WebPages.Reports
{
    public class WorkTimesReport : GrapeCity.ActiveReports.SectionReport
	{
		#region private members

		string _projectName = String.Empty;
		string _projectCode = String.Empty;
		string _administrativeName = String.Empty;
		string _clientCity = String.Empty;
		string _clientAddress = String.Empty;
		string _clientName = String.Empty;
		string _createdBy = String.Empty;

		private string[] _groupNames, _idNames;
		private int _headerCount = 0; 
		private int _detailCount = 0;
		bool _includeTimesColumn;
        private Picture picReportHeader;
		bool _forPDF;

		#endregion

        public WorkTimesReport(HeaderInfo headerInfo, WorkTimes.WorkedHoursInfo workedHoursInfo,
            string createdBy, string groups, bool includeTimesColumn, bool forPDF, bool IsOverTime)
        {

            #region init variables

            _projectName = headerInfo.ProjectName;
            _projectCode = headerInfo.ProjectCode;
            _administrativeName = headerInfo.ProjectAdminName;
            _clientName = headerInfo.ClientName;
            _clientCity = headerInfo.ClientCity;
            _clientAddress = headerInfo.ClientAddress;

            _createdBy = createdBy;

            _includeTimesColumn = includeTimesColumn;
            _forPDF = forPDF;

            #endregion

            InitializeComponent();

            #region Page settings

            this.PageSettings.Margins.Top = SectionReport.CmToInch(1f);
            this.PageSettings.Margins.Bottom = SectionReport.CmToInch(1f);
            this.PageSettings.Gutter = 0F;
            this.PageSettings.Margins.Left = SectionReport.CmToInch(1f);
            this.PageSettings.Margins.Right = SectionReport.CmToInch(1f);


            if (forPDF)
            {
                this.PageSettings.Margins.Left = SectionReport.CmToInch(1.5f);
                this.PrintWidth = SectionReport.CmToInch(18.0f);
            }
            else
            {
                this.PageSettings.Margins.Left = SectionReport.CmToInch(1.5f);
                this.PrintWidth = SectionReport.CmToInch(17.0f);

                this.PageFooter.Visible = false;
                this.txtContacts1.Visible = true;
            }

            #endregion

            string[] tempNames = groups.Split(',');
            _groupNames = new string[tempNames.Length / 2];
            _idNames = new String[_groupNames.Length];
            for (int i = 0; i < tempNames.Length / 2; i++)
            {
                _idNames[i] = tempNames[i * 2];
                _groupNames[i] = tempNames[i * 2 + 1];
            }

            int totalFields = _groupNames.Length;

            #region subreports

            if (workedHoursInfo == null) this.SummaryFooter.Visible = false;
            else this.SummarySubReport.Report = new WorkTimesFooter(workedHoursInfo, IsOverTime, false);

            this.HeaderSubReport.Report = new WorkTimesHeader(headerInfo, forPDF, IsOverTime);

            #endregion

            this.txtCreatedBy.Text = String.Concat("                                              ", _createdBy);

            this.lblReportDate.Text = String.Concat(
                //	Resource.ResourceManager["reports_Date"], ": ", 
                DateTime.Now.ToString("dd MMM yyyy "), Resource.ResourceManager["reports_YearAbbr"]);


            switch (totalFields)
            {
                case 1: _headerCount = 0; _detailCount = 1; break;
                case 2: _headerCount = 1; _detailCount = 1; break;
                case 3: _headerCount = 2; _detailCount = 1; break;
                case 4: _headerCount = 2; _detailCount = 2; break;
            }

        }

        private void WorkTimesReport_ReportStart(object sender, System.EventArgs eArgs)
        {
            if ((this.DataSource == null) || (((System.Data.DataView)this.DataSource).Count == 0))
            {
                this.GroupHeader1.Format -= new System.EventHandler(this.GroupHeader1_Format);
                this.GroupHeader2.Format -= new System.EventHandler(this.GroupHeader2_Format);
                this.GroupFooter2.Format -= new System.EventHandler(this.GroupFooter2_Format);
                this.GroupFooter1.Format -= new System.EventHandler(this.GroupFooter1_Format);
                this.Detail.Format -= new System.EventHandler(this.Detail_Format);

                this.GrandTotalFooter.Format -= new System.EventHandler(this.GrandTotalFooter_Format);

                this.GroupHeader1.Visible = false;
                this.GroupFooter1.Visible = false;
                this.GroupHeader2.Visible = false;
                this.GroupFooter2.Visible = false;
                this.GrandTotalFooter.Visible = false;

                this.txtDetail1Item.Visible = false;
                this.txtDetail2Item.Visible = false;
                this.txtItemTotalTime.Visible = false;

                //				this.txtNoData.Visible = true;
                //				this.txtNoData.Text = Resource.ResourceManager["reports_WorkTimes_NoDataMatch"];

                ((WorkTimesHeader)this.HeaderSubReport.Report).HideStartLine();

                return;
            }

            SetGroupTitle(this.lblGroup2, 2);
            SetGroupTitle(this.lblGroup3, 3);
            SetGroupTitle(this.lblGroup4, 4);

            switch (_headerCount)
            {
                case 0: this.GroupHeader1.Visible = false;
                    this.GroupFooter1.Visible = false;
                    this.GroupHeader2.Visible = false;
                    this.GroupFooter2.Visible = false;
                    break;

                case 1: this.GroupHeader1.Visible = false;
                    this.GroupFooter1.Visible = false;
                    this.GroupHeader2.DataField = _idNames[0];
                    this.txtHdr2Item.DataField = _groupNames[0];
                    break;

                case 2: this.GroupHeader1.DataField = _idNames[0];
                    this.txtHdr1Item.DataField = _groupNames[0];//new

                    this.GroupHeader2.DataField = _idNames[1];
                    this.txtHdr2Item.DataField = _groupNames[1];
                    break;
            }

            switch (_detailCount)
            {
                case 0: this.Detail.Visible = false;
                    Line4.Visible = false;
                    Line1.LineWeight = 1; break;


                case 1: this.txtDetail1Item.DataField = _groupNames[_headerCount];
                    this.txtDetail2Item.Visible = false;
                    this.txtDetail1Item.Size = new System.Drawing.SizeF(
                        SectionReport.CmToInch(3.15f), SectionReport.CmToInch(0.197f));

                    break;
                case 2: this.txtDetail1Item.DataField = _groupNames[_headerCount];
                    this.txtDetail2Item.DataField = _groupNames[_headerCount + 1];
                    break;
            }

        }

		#region Header/Footer

        private void PageHeader_Format(object sender, System.EventArgs eArgs)
        {
            if (this.PageNumber == 1)
            {
                this.PageHeader.Visible = false;
                this.picPageHeader.Visible = false;
                this.lblGroup2.Location = new System.Drawing.PointF(this.lblGroup2.Location.X, SectionReport.CmToInch(0.5f));
                this.lblGroup3.Location = new System.Drawing.PointF(this.lblGroup3.Location.X, SectionReport.CmToInch(0.5f));
                this.lblGroup4.Location = new System.Drawing.PointF(this.lblGroup4.Location.X, SectionReport.CmToInch(0.5f));
                this.Line10.Location = new System.Drawing.PointF(this.Line10.Location.X, SectionReport.CmToInch(0.5f));
                this.Line12.Location = new System.Drawing.PointF(this.Line12.Location.X, SectionReport.CmToInch(1f));
            }
            else
            {
                this.picPageHeader.Visible = true;
                this.PageHeader.Visible = true;
            }

            if ((!_forPDF) && (this.PageNumber > 1)) this.PageHeader.Visible = false;
        }

        private void PageFooter_Format(object sender, System.EventArgs eArgs)
        {
            this.lblPage.Text = String.Concat(Resource.ResourceManager["rpt_Page"], " ",
                this.PageNumber.ToString());//, "/", this.Document.Pages.Count.ToString());
        }
		#endregion

		#region Group1

        private void GroupHeader1_Format(object sender, System.EventArgs eArgs)
        {
            //this.txtHdr1Item.Text = this.Fields[_groupNames[0]].Value.ToString()+" - "+this.Fields[_groupNames[3]].Value.ToString();
        }

        private void GroupFooter1_Format(object sender, System.EventArgs eArgs)
        {
            if (!SessionManager.LoggedUserInfo.HasPaymentRights)
            {
                txtGroupTotalSalary.Visible = false;
            }

            //txtHeader1Total.Text = Resource.ResourceManager["rpt_TotalFor"] + " "+txtHdr1Item.Text;
            txtHeader1Total.Text = Resource.ResourceManager["reports_Total"]; //

            if (_includeTimesColumn)
            {
                txtGroup1TotalTime.Text = TimeHelper.HoursFromMinutes(int.Parse(this.txtGroup1TotalTime.Text)).ToString("0.00");
            }
            else txtGroup1TotalTime.Text = TimeHelper.HoursStringFromMinutes(int.Parse(this.txtGroup1TotalTime.Text), false);
        }

		#endregion

		#region Group2

        private void GroupHeader2_Format(object sender, System.EventArgs eArgs)
        {

        }

        private void GroupFooter2_Format(object sender, System.EventArgs eArgs)
        {
            if (!SessionManager.LoggedUserInfo.HasPaymentRights)
            {
                txtGroup2TotalSal.Visible = false;
            }

            //txtGroup2Total.Text = Resource.ResourceManager["rpt_TotalFor"] + " "+txtHdr1Item.Text + " - "+ this.txtHdr2Item.Text;
            txtGroup2Total.Text = Resource.ResourceManager["reports_Total"]; //

            if (_includeTimesColumn)
            {
                txtGroup2TotalTime.Text =
                TimeHelper.HoursFromMinutes(int.Parse(this.txtGroup2TotalTime.Text)).ToString("0.00");
            }
            else txtGroup2TotalTime.Text = TimeHelper.HoursStringFromMinutes(int.Parse(this.txtGroup2TotalTime.Text), false);
        }

		#endregion

        private void Detail_Format(object sender, System.EventArgs eArgs)
        {
            if (!SessionManager.LoggedUserInfo.HasPaymentRights)
            {
                txtTotalSalary.Visible = false;
            }
            if (txtItemTotalTime.Value == null) txtItemTotalTime.Value = 0;
            if (_includeTimesColumn)
            {
                txtItemTotalTime.Text = TimeHelper.HoursFromMinutes(int.Parse(txtItemTotalTime.Text)).ToString("0.00");
            }
            else txtItemTotalTime.Text = TimeHelper.HoursStringFromMinutes(int.Parse(txtItemTotalTime.Text), false);
        }

        private void GrandTotalFooter_Format(object sender, System.EventArgs eArgs)
        {
            if (!SessionManager.LoggedUserInfo.HasPaymentRights)
            {
                txtGrandTotalSal.Visible = false;
            }
            if (_includeTimesColumn)
            {
                txtGrandTotal.Text = TimeHelper.HoursFromMinutes(int.Parse(txtGrandTotal.Text)).ToString("0.00");
            }
            else txtGrandTotal.Text = TimeHelper.HoursStringFromMinutes(int.Parse(txtGrandTotal.Text), false);
        }

		#region Util

        private void SetGroupTitle(GrapeCity.ActiveReports.SectionReportModel.Label label, int groupOrder)
        {

            if (_groupNames.Length < groupOrder)
            {
                label.Text = "";
                return;
            }

            switch (_groupNames[groupOrder - 1])
            {
                case "FullName": label.Text = Resource.ResourceManager["reports_ColumnNameUser"];
                    break;
                case "ActivityName": label.Text = Resource.ResourceManager["reports_ColumnNameActivity"];
                    break;
                case "ProjectName": label.Text = Resource.ResourceManager["reports_ColumnNameProject"];
                    break;
                case "BuildingType": label.Text = Resource.ResourceManager["reports_ColumnNameBuildingType"];
                    break;
            }
        }

		#endregion

		#region ActiveReports Designer generated code
























































        public void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(WorkTimesReport));
            this.Detail = new GrapeCity.ActiveReports.SectionReportModel.Detail();
            this.txtItemTotalTime = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtTotalSalary = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtDetail2Item = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtDetail1Item = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.TextBox2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.ReportHeader = new GrapeCity.ActiveReports.SectionReportModel.ReportHeader();
            this.picReportHeader = new GrapeCity.ActiveReports.SectionReportModel.Picture();
            this.HeaderSubReport = new GrapeCity.ActiveReports.SectionReportModel.SubReport();
            this.ReportFooter = new GrapeCity.ActiveReports.SectionReportModel.ReportFooter();
            this.txtContacts1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.lblProektant = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.txtCreatedBy = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.lblReportDate = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label4 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label5 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.PageHeader = new GrapeCity.ActiveReports.SectionReportModel.PageHeader();
            this.Label3 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lblGroup3 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lblGroup4 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lblGroup2 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Line12 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.Line10 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.picPageHeader = new GrapeCity.ActiveReports.SectionReportModel.Picture();
            this.lblPeriod = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.PageFooter = new GrapeCity.ActiveReports.SectionReportModel.PageFooter();
            this.lblPage = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Line11 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.txtContacts = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.SummaryHeader = new GrapeCity.ActiveReports.SectionReportModel.GroupHeader();
            this.SummaryFooter = new GrapeCity.ActiveReports.SectionReportModel.GroupFooter();
            this.SummarySubReport = new GrapeCity.ActiveReports.SectionReportModel.SubReport();
            this.GrandTotalHeader = new GrapeCity.ActiveReports.SectionReportModel.GroupHeader();
            this.GrandTotalFooter = new GrapeCity.ActiveReports.SectionReportModel.GroupFooter();
            this.TextBox1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtGrandTotal = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.Line13 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.Line14 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.txtGrandTotalSal = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.GroupHeader1 = new GrapeCity.ActiveReports.SectionReportModel.GroupHeader();
            this.txtHdr1Item = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.Line1 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.GroupFooter1 = new GrapeCity.ActiveReports.SectionReportModel.GroupFooter();
            this.Line4 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.txtHeader1Total = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtGroup1TotalTime = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtGroupTotalSalary = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.GroupHeader2 = new GrapeCity.ActiveReports.SectionReportModel.GroupHeader();
            this.txtHdr2Item = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.Line2 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.GroupFooter2 = new GrapeCity.ActiveReports.SectionReportModel.GroupFooter();
            this.Line3 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.txtGroup2Total = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtGroup2TotalTime = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.Line5 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.txtGroup2TotalSal = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            ((System.ComponentModel.ISupportInitialize)(this.txtItemTotalTime)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTotalSalary)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDetail2Item)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDetail1Item)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TextBox2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picReportHeader)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtContacts1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblProektant)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCreatedBy)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblReportDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblGroup3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblGroup4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblGroup2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picPageHeader)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblPeriod)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblPage)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtContacts)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TextBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtGrandTotal)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtGrandTotalSal)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtHdr1Item)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtHeader1Total)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtGroup1TotalTime)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtGroupTotalSalary)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtHdr2Item)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtGroup2Total)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtGroup2TotalTime)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtGroup2TotalSal)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            // 
            // Detail
            // 
            this.Detail.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
            this.txtItemTotalTime,
            this.txtTotalSalary,
            this.txtDetail2Item,
            this.txtDetail1Item,
            this.TextBox2});
            this.Detail.Height = 0.2F;
            this.Detail.Name = "Detail";
            this.Detail.Format += new System.EventHandler(this.Detail_Format);
            // 
            // txtItemTotalTime
            // 
            this.txtItemTotalTime.DataField = "TotalTime";
            this.txtItemTotalTime.Height = 0.1968504F;
            this.txtItemTotalTime.Left = 6.102362F;
            this.txtItemTotalTime.Name = "txtItemTotalTime";
            this.txtItemTotalTime.OutputFormat = resources.GetString("txtItemTotalTime.OutputFormat");
            this.txtItemTotalTime.Style = "color: DimGray; font-size: 9pt; font-weight: bold; text-align: right";
            this.txtItemTotalTime.Text = "txtItemTotalTime";
            this.txtItemTotalTime.Top = 0F;
            this.txtItemTotalTime.Width = 0.9842521F;
            // 
            // txtTotalSalary
            // 
            this.txtTotalSalary.DataField = "Amount";
            this.txtTotalSalary.Height = 0.1968504F;
            this.txtTotalSalary.Left = 5.125F;
            this.txtTotalSalary.Name = "txtTotalSalary";
            this.txtTotalSalary.OutputFormat = resources.GetString("txtTotalSalary.OutputFormat");
            this.txtTotalSalary.Style = "color: DimGray; font-size: 9pt; font-weight: bold; text-align: right";
            this.txtTotalSalary.Text = null;
            this.txtTotalSalary.Top = 0F;
            this.txtTotalSalary.Width = 0.9842521F;
            // 
            // txtDetail2Item
            // 
            this.txtDetail2Item.Height = 0.197F;
            this.txtDetail2Item.Left = 3.149606F;
            this.txtDetail2Item.Name = "txtDetail2Item";
            this.txtDetail2Item.Style = "color: DimGray; font-size: 9pt";
            this.txtDetail2Item.Text = "txtDetail2Item";
            this.txtDetail2Item.Top = 0F;
            this.txtDetail2Item.Width = 0.9753938F;
            // 
            // txtDetail1Item
            // 
            this.txtDetail1Item.Height = 0.1968504F;
            this.txtDetail1Item.Left = 0F;
            this.txtDetail1Item.Name = "txtDetail1Item";
            this.txtDetail1Item.Style = "color: DimGray; font-size: 9pt; text-align: left";
            this.txtDetail1Item.Text = "txtDetail1Item";
            this.txtDetail1Item.Top = 0F;
            this.txtDetail1Item.Width = 3.149606F;
            // 
            // TextBox2
            // 
            this.TextBox2.DataField = "WorkDate";
            this.TextBox2.Height = 0.1968504F;
            this.TextBox2.Left = 4.125F;
            this.TextBox2.Name = "TextBox2";
            this.TextBox2.OutputFormat = resources.GetString("TextBox2.OutputFormat");
            this.TextBox2.Style = "color: DimGray; font-size: 9pt; font-weight: bold; text-align: right";
            this.TextBox2.Text = null;
            this.TextBox2.Top = 0F;
            this.TextBox2.Width = 0.9842521F;
            // 
            // ReportHeader
            // 
            this.ReportHeader.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
            this.picReportHeader,
            this.HeaderSubReport});
            this.ReportHeader.Height = 1.364583F;
            this.ReportHeader.Name = "ReportHeader";
            // 
            // picReportHeader
            // 
            this.picReportHeader.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.picReportHeader.Height = 0.875F;
            this.picReportHeader.ImageData = ((System.IO.Stream)(resources.GetObject("picReportHeader.ImageData")));
            this.picReportHeader.Left = 3.149606F;
            this.picReportHeader.LineColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.picReportHeader.Name = "picReportHeader";
            this.picReportHeader.PictureAlignment = GrapeCity.ActiveReports.SectionReportModel.PictureAlignment.BottomRight;
            this.picReportHeader.SizeMode = GrapeCity.ActiveReports.SectionReportModel.SizeModes.Zoom;
            this.picReportHeader.Top = 0F;
            this.picReportHeader.Width = 2.952756F;
            // 
            // HeaderSubReport
            // 
            this.HeaderSubReport.CloseBorder = false;
            this.HeaderSubReport.Height = 0.1968504F;
            this.HeaderSubReport.Left = 0F;
            this.HeaderSubReport.Name = "HeaderSubReport";
            this.HeaderSubReport.Report = null;
            this.HeaderSubReport.Top = 1.181102F;
            this.HeaderSubReport.Width = 7.086611F;
            // 
            // ReportFooter
            // 
            this.ReportFooter.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
            this.txtContacts1,
            this.lblProektant,
            this.txtCreatedBy,
            this.lblReportDate,
            this.Label4,
            this.Label5});
            this.ReportFooter.Height = 1.40625F;
            this.ReportFooter.KeepTogether = true;
            this.ReportFooter.Name = "ReportFooter";
            // 
            // txtContacts1
            // 
            this.txtContacts1.Height = 0.2F;
            this.txtContacts1.Left = 0F;
            this.txtContacts1.Name = "txtContacts1";
            this.txtContacts1.Style = "font-size: 6.5pt; font-weight: bold; text-align: center";
            this.txtContacts1.Text = "Sofia 1700, 4A, Simeonovsko shose Blvd., tel.: +3592 8621978, +3592 8623348, fax." +
    ":+3592 8621614, e-mail: office@asa-bg.com, website: www.asa-bg.com";
            this.txtContacts1.Top = 0.3937007F;
            this.txtContacts1.Visible = false;
            this.txtContacts1.Width = 7.086611F;
            // 
            // lblProektant
            // 
            this.lblProektant.Height = 0.2F;
            this.lblProektant.HyperLink = null;
            this.lblProektant.Left = 0F;
            this.lblProektant.Name = "lblProektant";
            this.lblProektant.Style = "font-size: 9pt; font-weight: normal; vertical-align: bottom";
            this.lblProektant.Text = "�������� ���������:. . . . . . . . . . . . . . . . . . . . . . .";
            this.lblProektant.Top = 0.9842521F;
            this.lblProektant.Width = 7.086611F;
            // 
            // txtCreatedBy
            // 
            this.txtCreatedBy.Height = 0.2F;
            this.txtCreatedBy.Left = 0F;
            this.txtCreatedBy.Name = "txtCreatedBy";
            this.txtCreatedBy.Style = "font-size: 9pt";
            this.txtCreatedBy.Text = "txtCreatedBy";
            this.txtCreatedBy.Top = 1.181102F;
            this.txtCreatedBy.Width = 7.086611F;
            // 
            // lblReportDate
            // 
            this.lblReportDate.Height = 0.2F;
            this.lblReportDate.HyperLink = null;
            this.lblReportDate.Left = 0F;
            this.lblReportDate.Name = "lblReportDate";
            this.lblReportDate.Style = "font-size: 9pt; font-weight: normal; vertical-align: bottom";
            this.lblReportDate.Text = "lblReportDate";
            this.lblReportDate.Top = 0.7874014F;
            this.lblReportDate.Width = 7.086611F;
            // 
            // Label4
            // 
            this.Label4.Height = 0.1968504F;
            this.Label4.HyperLink = null;
            this.Label4.Left = 0F;
            this.Label4.Name = "Label4";
            this.Label4.Style = "color: White; font-size: 9pt; font-weight: normal; vertical-align: bottom";
            this.Label4.Text = " .";
            this.Label4.Top = 0.5905511F;
            this.Label4.Width = 7.086611F;
            // 
            // Label5
            // 
            this.Label5.Height = 0.1968504F;
            this.Label5.HyperLink = null;
            this.Label5.Left = 0F;
            this.Label5.Name = "Label5";
            this.Label5.Style = "color: White; font-size: 9pt; font-weight: normal; vertical-align: bottom";
            this.Label5.Text = " .";
            this.Label5.Top = 0F;
            this.Label5.Width = 7.086611F;
            // 
            // PageHeader
            // 
            this.PageHeader.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
            this.Label3,
            this.lblGroup3,
            this.lblGroup4,
            this.lblGroup2,
            this.Line12,
            this.Line10,
            this.picPageHeader,
            this.lblPeriod});
            this.PageHeader.Height = 1.1875F;
            this.PageHeader.Name = "PageHeader";
            this.PageHeader.Format += new System.EventHandler(this.PageHeader_Format);
            // 
            // Label3
            // 
            this.Label3.Height = 0.2755906F;
            this.Label3.HyperLink = null;
            this.Label3.Left = 6.102362F;
            this.Label3.Name = "Label3";
            this.Label3.Style = "font-size: 9pt; font-weight: bold; text-align: left";
            this.Label3.Text = "������";
            this.Label3.Top = 0.9911417F;
            this.Label3.Visible = false;
            this.Label3.Width = 0.9842521F;
            // 
            // lblGroup3
            // 
            this.lblGroup3.Height = 0.2755906F;
            this.lblGroup3.HyperLink = null;
            this.lblGroup3.Left = 1.771653F;
            this.lblGroup3.Name = "lblGroup3";
            this.lblGroup3.Style = "font-size: 9pt; font-weight: bold";
            this.lblGroup3.Text = "lblGroup3";
            this.lblGroup3.Top = 0.9911417F;
            this.lblGroup3.Visible = false;
            this.lblGroup3.Width = 2.165354F;
            // 
            // lblGroup4
            // 
            this.lblGroup4.Height = 0.2755906F;
            this.lblGroup4.HyperLink = null;
            this.lblGroup4.Left = 3.937008F;
            this.lblGroup4.Name = "lblGroup4";
            this.lblGroup4.Style = "font-size: 9pt; font-weight: bold";
            this.lblGroup4.Text = "lblGroup4";
            this.lblGroup4.Top = 0.9911417F;
            this.lblGroup4.Visible = false;
            this.lblGroup4.Width = 2.165354F;
            // 
            // lblGroup2
            // 
            this.lblGroup2.Height = 0.2755906F;
            this.lblGroup2.HyperLink = null;
            this.lblGroup2.Left = 0F;
            this.lblGroup2.Name = "lblGroup2";
            this.lblGroup2.Style = "font-size: 9pt; font-weight: bold";
            this.lblGroup2.Text = "lblGroup2";
            this.lblGroup2.Top = 0.9911417F;
            this.lblGroup2.Visible = false;
            this.lblGroup2.Width = 1.771653F;
            // 
            // Line12
            // 
            this.Line12.Height = 0F;
            this.Line12.Left = 0F;
            this.Line12.LineWeight = 1F;
            this.Line12.Name = "Line12";
            this.Line12.Top = 1.187992F;
            this.Line12.Visible = false;
            this.Line12.Width = 7.086611F;
            this.Line12.X1 = 0F;
            this.Line12.X2 = 7.086611F;
            this.Line12.Y1 = 1.187992F;
            this.Line12.Y2 = 1.187992F;
            // 
            // Line10
            // 
            this.Line10.Height = 0F;
            this.Line10.Left = 0F;
            this.Line10.LineWeight = 2F;
            this.Line10.Name = "Line10";
            this.Line10.Top = 0.9911417F;
            this.Line10.Width = 7.086611F;
            this.Line10.X1 = 0F;
            this.Line10.X2 = 7.086611F;
            this.Line10.Y1 = 0.9911417F;
            this.Line10.Y2 = 0.9911417F;
            // 
            // picPageHeader
            // 
            this.picPageHeader.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.picPageHeader.Height = 0.875F;
            this.picPageHeader.ImageData = ((System.IO.Stream)(resources.GetObject("picPageHeader.ImageData")));
            this.picPageHeader.Left = 3.149606F;
            this.picPageHeader.LineColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.picPageHeader.Name = "picPageHeader";
            this.picPageHeader.PictureAlignment = GrapeCity.ActiveReports.SectionReportModel.PictureAlignment.BottomLeft;
            this.picPageHeader.SizeMode = GrapeCity.ActiveReports.SectionReportModel.SizeModes.Zoom;
            this.picPageHeader.Top = 0F;
            this.picPageHeader.Width = 2.952756F;
            // 
            // lblPeriod
            // 
            this.lblPeriod.Height = 0.1968504F;
            this.lblPeriod.HyperLink = null;
            this.lblPeriod.Left = 0F;
            this.lblPeriod.Name = "lblPeriod";
            this.lblPeriod.Style = "font-size: 9pt; font-weight: bold; text-align: center";
            this.lblPeriod.Text = "lblPeriod";
            this.lblPeriod.Top = 0.2987501F;
            this.lblPeriod.Visible = false;
            this.lblPeriod.Width = 7.086611F;
            // 
            // PageFooter
            // 
            this.PageFooter.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
            this.lblPage,
            this.Line11,
            this.txtContacts});
            this.PageFooter.Height = 0.6298611F;
            this.PageFooter.Name = "PageFooter";
            this.PageFooter.Format += new System.EventHandler(this.PageFooter_Format);
            // 
            // lblPage
            // 
            this.lblPage.Height = 0.1968504F;
            this.lblPage.HyperLink = null;
            this.lblPage.Left = 0F;
            this.lblPage.Name = "lblPage";
            this.lblPage.Style = "font-size: 8pt; font-weight: bold; text-align: right";
            this.lblPage.Text = "Page";
            this.lblPage.Top = 0.1181103F;
            this.lblPage.Width = 7.086611F;
            // 
            // Line11
            // 
            this.Line11.Height = 0F;
            this.Line11.Left = 0F;
            this.Line11.LineWeight = 1F;
            this.Line11.Name = "Line11";
            this.Line11.Top = 0.1181103F;
            this.Line11.Width = 7.086611F;
            this.Line11.X1 = 0F;
            this.Line11.X2 = 7.086611F;
            this.Line11.Y1 = 0.1181103F;
            this.Line11.Y2 = 0.1181103F;
            // 
            // txtContacts
            // 
            this.txtContacts.Height = 0.2F;
            this.txtContacts.Left = 0F;
            this.txtContacts.Name = "txtContacts";
            this.txtContacts.Style = "font-size: 6.5pt; font-weight: bold; text-align: center";
            this.txtContacts.Text = "Sofia 1700, 4A, Simeonovsko shose Blvd., tel.: +3592 8621978, +3592 8623348, fax." +
    ":+3592 8621614, e-mail: office@asa-bg.com, website: www.asa-bg.com";
            this.txtContacts.Top = 0.3937007F;
            this.txtContacts.Width = 7.086611F;
            // 
            // SummaryHeader
            // 
            this.SummaryHeader.Height = 0F;
            this.SummaryHeader.Name = "SummaryHeader";
            this.SummaryHeader.Visible = false;
            // 
            // SummaryFooter
            // 
            this.SummaryFooter.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
            this.SummarySubReport});
            this.SummaryFooter.Height = 0.6756945F;
            this.SummaryFooter.KeepTogether = true;
            this.SummaryFooter.Name = "SummaryFooter";
            // 
            // SummarySubReport
            // 
            this.SummarySubReport.CloseBorder = false;
            this.SummarySubReport.Height = 0.1968504F;
            this.SummarySubReport.Left = 0F;
            this.SummarySubReport.Name = "SummarySubReport";
            this.SummarySubReport.Report = null;
            this.SummarySubReport.Top = 0.3937007F;
            this.SummarySubReport.Width = 7.086611F;
            // 
            // GrandTotalHeader
            // 
            this.GrandTotalHeader.Height = 0.08333334F;
            this.GrandTotalHeader.Name = "GrandTotalHeader";
            this.GrandTotalHeader.Visible = false;
            // 
            // GrandTotalFooter
            // 
            this.GrandTotalFooter.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
            this.TextBox1,
            this.txtGrandTotal,
            this.Line13,
            this.Line14,
            this.txtGrandTotalSal});
            this.GrandTotalFooter.Height = 0.2708333F;
            this.GrandTotalFooter.Name = "GrandTotalFooter";
            this.GrandTotalFooter.Format += new System.EventHandler(this.GrandTotalFooter_Format);
            // 
            // TextBox1
            // 
            this.TextBox1.Height = 0.197F;
            this.TextBox1.Left = 0F;
            this.TextBox1.Name = "TextBox1";
            this.TextBox1.Style = "color: Black; font-size: 10pt; font-weight: bold";
            this.TextBox1.Text = "����";
            this.TextBox1.Top = 0.03937007F;
            this.TextBox1.Width = 5.125F;
            // 
            // txtGrandTotal
            // 
            this.txtGrandTotal.DataField = "TotalTime";
            this.txtGrandTotal.Height = 0.1968504F;
            this.txtGrandTotal.Left = 6.102362F;
            this.txtGrandTotal.Name = "txtGrandTotal";
            this.txtGrandTotal.OutputFormat = resources.GetString("txtGrandTotal.OutputFormat");
            this.txtGrandTotal.Style = "font-size: 10pt; font-weight: bold; text-align: right";
            this.txtGrandTotal.SummaryRunning = GrapeCity.ActiveReports.SectionReportModel.SummaryRunning.All;
            this.txtGrandTotal.SummaryType = GrapeCity.ActiveReports.SectionReportModel.SummaryType.GrandTotal;
            this.txtGrandTotal.Text = "txtGrandTotal";
            this.txtGrandTotal.Top = 0.03937007F;
            this.txtGrandTotal.Width = 0.9842521F;
            // 
            // Line13
            // 
            this.Line13.Height = 0F;
            this.Line13.Left = 0F;
            this.Line13.LineWeight = 2F;
            this.Line13.Name = "Line13";
            this.Line13.Top = 0.2362205F;
            this.Line13.Width = 7.086611F;
            this.Line13.X1 = 0F;
            this.Line13.X2 = 7.086611F;
            this.Line13.Y1 = 0.2362205F;
            this.Line13.Y2 = 0.2362205F;
            // 
            // Line14
            // 
            this.Line14.Height = 0F;
            this.Line14.Left = 0F;
            this.Line14.LineWeight = 2F;
            this.Line14.Name = "Line14";
            this.Line14.Top = 0.03937007F;
            this.Line14.Width = 7.086611F;
            this.Line14.X1 = 0F;
            this.Line14.X2 = 7.086611F;
            this.Line14.Y1 = 0.03937007F;
            this.Line14.Y2 = 0.03937007F;
            // 
            // txtGrandTotalSal
            // 
            this.txtGrandTotalSal.DataField = "Amount";
            this.txtGrandTotalSal.Height = 0.1968504F;
            this.txtGrandTotalSal.Left = 5.1255F;
            this.txtGrandTotalSal.Name = "txtGrandTotalSal";
            this.txtGrandTotalSal.OutputFormat = resources.GetString("txtGrandTotalSal.OutputFormat");
            this.txtGrandTotalSal.Style = "font-size: 10pt; font-weight: bold; text-align: right";
            this.txtGrandTotalSal.SummaryRunning = GrapeCity.ActiveReports.SectionReportModel.SummaryRunning.All;
            this.txtGrandTotalSal.SummaryType = GrapeCity.ActiveReports.SectionReportModel.SummaryType.GrandTotal;
            this.txtGrandTotalSal.Text = "txtGrandTotalSal";
            this.txtGrandTotalSal.Top = 0.039F;
            this.txtGrandTotalSal.Width = 0.9842521F;
            // 
            // GroupHeader1
            // 
            this.GroupHeader1.CanShrink = true;
            this.GroupHeader1.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
            this.txtHdr1Item,
            this.Line1});
            this.GroupHeader1.Height = 0.2597222F;
            this.GroupHeader1.KeepTogether = true;
            this.GroupHeader1.Name = "GroupHeader1";
            this.GroupHeader1.Format += new System.EventHandler(this.GroupHeader1_Format);
            // 
            // txtHdr1Item
            // 
            this.txtHdr1Item.Height = 0.2362205F;
            this.txtHdr1Item.Left = 0F;
            this.txtHdr1Item.Name = "txtHdr1Item";
            this.txtHdr1Item.Style = "color: DimGray; font-size: 9pt; font-weight: bold; vertical-align: middle";
            this.txtHdr1Item.Text = "txtHdr1Item";
            this.txtHdr1Item.Top = 0F;
            this.txtHdr1Item.Width = 7.086611F;
            // 
            // Line1
            // 
            this.Line1.Height = 0F;
            this.Line1.Left = 0F;
            this.Line1.LineWeight = 2F;
            this.Line1.Name = "Line1";
            this.Line1.Top = 0.2362205F;
            this.Line1.Width = 7.086611F;
            this.Line1.X1 = 0F;
            this.Line1.X2 = 7.086611F;
            this.Line1.Y1 = 0.2362205F;
            this.Line1.Y2 = 0.2362205F;
            // 
            // GroupFooter1
            // 
            this.GroupFooter1.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
            this.Line4,
            this.txtHeader1Total,
            this.txtGroup1TotalTime,
            this.txtGroupTotalSalary});
            this.GroupFooter1.Height = 0.2708333F;
            this.GroupFooter1.Name = "GroupFooter1";
            this.GroupFooter1.Format += new System.EventHandler(this.GroupFooter1_Format);
            // 
            // Line4
            // 
            this.Line4.Height = 0F;
            this.Line4.Left = 0F;
            this.Line4.LineWeight = 2F;
            this.Line4.Name = "Line4";
            this.Line4.Top = 0F;
            this.Line4.Width = 7.086611F;
            this.Line4.X1 = 0F;
            this.Line4.X2 = 7.086611F;
            this.Line4.Y1 = 0F;
            this.Line4.Y2 = 0F;
            // 
            // txtHeader1Total
            // 
            this.txtHeader1Total.Height = 0.197F;
            this.txtHeader1Total.Left = 0F;
            this.txtHeader1Total.Name = "txtHeader1Total";
            this.txtHeader1Total.Style = "color: DimGray; font-size: 9pt; font-weight: bold; vertical-align: middle";
            this.txtHeader1Total.Text = "txtHeader1Total";
            this.txtHeader1Total.Top = 0F;
            this.txtHeader1Total.Width = 5.125F;
            // 
            // txtGroup1TotalTime
            // 
            this.txtGroup1TotalTime.DataField = "TotalTime";
            this.txtGroup1TotalTime.Height = 0.1968504F;
            this.txtGroup1TotalTime.Left = 6.102F;
            this.txtGroup1TotalTime.Name = "txtGroup1TotalTime";
            this.txtGroup1TotalTime.OutputFormat = resources.GetString("txtGroup1TotalTime.OutputFormat");
            this.txtGroup1TotalTime.Style = "font-size: 9pt; font-weight: bold; text-align: right; vertical-align: middle";
            this.txtGroup1TotalTime.SummaryGroup = "GroupHeader1";
            this.txtGroup1TotalTime.SummaryRunning = GrapeCity.ActiveReports.SectionReportModel.SummaryRunning.Group;
            this.txtGroup1TotalTime.SummaryType = GrapeCity.ActiveReports.SectionReportModel.SummaryType.SubTotal;
            this.txtGroup1TotalTime.Text = "txtGroup1TotalTime";
            this.txtGroup1TotalTime.Top = 0F;
            this.txtGroup1TotalTime.Width = 0.9842521F;
            // 
            // txtGroupTotalSalary
            // 
            this.txtGroupTotalSalary.DataField = "Amount";
            this.txtGroupTotalSalary.Height = 0.1968504F;
            this.txtGroupTotalSalary.Left = 5.125F;
            this.txtGroupTotalSalary.Name = "txtGroupTotalSalary";
            this.txtGroupTotalSalary.OutputFormat = resources.GetString("txtGroupTotalSalary.OutputFormat");
            this.txtGroupTotalSalary.Style = "font-size: 9pt; font-weight: bold; text-align: right; vertical-align: middle";
            this.txtGroupTotalSalary.SummaryGroup = "GroupHeader1";
            this.txtGroupTotalSalary.SummaryRunning = GrapeCity.ActiveReports.SectionReportModel.SummaryRunning.Group;
            this.txtGroupTotalSalary.SummaryType = GrapeCity.ActiveReports.SectionReportModel.SummaryType.SubTotal;
            this.txtGroupTotalSalary.Text = "txtGroupTotalSalary";
            this.txtGroupTotalSalary.Top = 0F;
            this.txtGroupTotalSalary.Width = 0.9842521F;
            // 
            // GroupHeader2
            // 
            this.GroupHeader2.CanShrink = true;
            this.GroupHeader2.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
            this.txtHdr2Item,
            this.Line2});
            this.GroupHeader2.GroupKeepTogether = GrapeCity.ActiveReports.SectionReportModel.GroupKeepTogether.FirstDetail;
            this.GroupHeader2.Name = "GroupHeader2";
            this.GroupHeader2.Format += new System.EventHandler(this.GroupHeader2_Format);
            // 
            // txtHdr2Item
            // 
            this.txtHdr2Item.Height = 0.1968504F;
            this.txtHdr2Item.Left = 0F;
            this.txtHdr2Item.Name = "txtHdr2Item";
            this.txtHdr2Item.Style = "font-size: 9pt; font-weight: bold";
            this.txtHdr2Item.Text = "txtHdr2Item";
            this.txtHdr2Item.Top = 0F;
            this.txtHdr2Item.Width = 7.086611F;
            // 
            // Line2
            // 
            this.Line2.Height = 0F;
            this.Line2.Left = 0F;
            this.Line2.LineWeight = 1F;
            this.Line2.Name = "Line2";
            this.Line2.Top = 0.1968504F;
            this.Line2.Width = 7.086611F;
            this.Line2.X1 = 0F;
            this.Line2.X2 = 7.086611F;
            this.Line2.Y1 = 0.1968504F;
            this.Line2.Y2 = 0.1968504F;
            // 
            // GroupFooter2
            // 
            this.GroupFooter2.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
            this.Line3,
            this.txtGroup2Total,
            this.txtGroup2TotalTime,
            this.Line5,
            this.txtGroup2TotalSal});
            this.GroupFooter2.Name = "GroupFooter2";
            this.GroupFooter2.Format += new System.EventHandler(this.GroupFooter2_Format);
            // 
            // Line3
            // 
            this.Line3.Height = 0F;
            this.Line3.Left = 0F;
            this.Line3.LineWeight = 1F;
            this.Line3.Name = "Line3";
            this.Line3.Top = 0F;
            this.Line3.Width = 7.086611F;
            this.Line3.X1 = 0F;
            this.Line3.X2 = 7.086611F;
            this.Line3.Y1 = 0F;
            this.Line3.Y2 = 0F;
            // 
            // txtGroup2Total
            // 
            this.txtGroup2Total.Height = 0.197F;
            this.txtGroup2Total.Left = 0F;
            this.txtGroup2Total.Name = "txtGroup2Total";
            this.txtGroup2Total.Style = "color: Black; font-size: 9pt; font-weight: bold";
            this.txtGroup2Total.Text = "txtGroup2Total";
            this.txtGroup2Total.Top = 0F;
            this.txtGroup2Total.Width = 5.125F;
            // 
            // txtGroup2TotalTime
            // 
            this.txtGroup2TotalTime.DataField = "TotalTime";
            this.txtGroup2TotalTime.Height = 0.1968504F;
            this.txtGroup2TotalTime.Left = 6.102362F;
            this.txtGroup2TotalTime.Name = "txtGroup2TotalTime";
            this.txtGroup2TotalTime.OutputFormat = resources.GetString("txtGroup2TotalTime.OutputFormat");
            this.txtGroup2TotalTime.Style = "font-size: 9pt; font-weight: bold; text-align: right";
            this.txtGroup2TotalTime.SummaryGroup = "GroupHeader2";
            this.txtGroup2TotalTime.SummaryRunning = GrapeCity.ActiveReports.SectionReportModel.SummaryRunning.Group;
            this.txtGroup2TotalTime.SummaryType = GrapeCity.ActiveReports.SectionReportModel.SummaryType.SubTotal;
            this.txtGroup2TotalTime.Text = "txtGroup2TotalTime";
            this.txtGroup2TotalTime.Top = 0F;
            this.txtGroup2TotalTime.Width = 0.9842521F;
            // 
            // Line5
            // 
            this.Line5.Height = 0F;
            this.Line5.Left = 0F;
            this.Line5.LineColor = System.Drawing.Color.FromArgb(((int)(((byte)(169)))), ((int)(((byte)(169)))), ((int)(((byte)(169)))));
            this.Line5.LineStyle = GrapeCity.ActiveReports.SectionReportModel.LineStyle.Dot;
            this.Line5.LineWeight = 1F;
            this.Line5.Name = "Line5";
            this.Line5.Top = 0.2362205F;
            this.Line5.Visible = false;
            this.Line5.Width = 7.086611F;
            this.Line5.X1 = 0F;
            this.Line5.X2 = 7.086611F;
            this.Line5.Y1 = 0.2362205F;
            this.Line5.Y2 = 0.2362205F;
            // 
            // txtGroup2TotalSal
            // 
            this.txtGroup2TotalSal.DataField = "Amount";
            this.txtGroup2TotalSal.Height = 0.1968504F;
            this.txtGroup2TotalSal.Left = 5.1255F;
            this.txtGroup2TotalSal.Name = "txtGroup2TotalSal";
            this.txtGroup2TotalSal.OutputFormat = resources.GetString("txtGroup2TotalSal.OutputFormat");
            this.txtGroup2TotalSal.Style = "font-size: 9pt; font-weight: bold; text-align: right";
            this.txtGroup2TotalSal.SummaryGroup = "GroupHeader2";
            this.txtGroup2TotalSal.SummaryRunning = GrapeCity.ActiveReports.SectionReportModel.SummaryRunning.Group;
            this.txtGroup2TotalSal.SummaryType = GrapeCity.ActiveReports.SectionReportModel.SummaryType.SubTotal;
            this.txtGroup2TotalSal.Text = "txtGroup2TotalSal";
            this.txtGroup2TotalSal.Top = 0F;
            this.txtGroup2TotalSal.Width = 0.9842521F;
            // 
            // WorkTimesReport
            // 
            this.MasterReport = false;
            this.PageSettings.Margins.Bottom = 0.4097222F;
            this.PageSettings.Margins.Left = 0.4097222F;
            this.PageSettings.Margins.Right = 0.4097222F;
            this.PageSettings.Margins.Top = 0.4097222F;
            this.PageSettings.PaperHeight = 11F;
            this.PageSettings.PaperWidth = 8.5F;
            this.PrintWidth = 7.104167F;
            this.Sections.Add(this.ReportHeader);
            this.Sections.Add(this.PageHeader);
            this.Sections.Add(this.SummaryHeader);
            this.Sections.Add(this.GrandTotalHeader);
            this.Sections.Add(this.GroupHeader1);
            this.Sections.Add(this.GroupHeader2);
            this.Sections.Add(this.Detail);
            this.Sections.Add(this.GroupFooter2);
            this.Sections.Add(this.GroupFooter1);
            this.Sections.Add(this.GrandTotalFooter);
            this.Sections.Add(this.SummaryFooter);
            this.Sections.Add(this.PageFooter);
            this.Sections.Add(this.ReportFooter);
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule(resources.GetString("$this.StyleSheet"), "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: inherit; font-style: inherit; font-variant: inherit; font-weight: bo" +
            "ld; font-size: 16pt; font-size-adjust: inherit; font-stretch: inherit", "Heading1", "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: Times New Roman; font-style: italic; font-variant: inherit; font-wei" +
            "ght: bold; font-size: 14pt; font-size-adjust: inherit; font-stretch: inherit", "Heading2", "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: inherit; font-style: inherit; font-variant: inherit; font-weight: bo" +
            "ld; font-size: 13pt; font-size-adjust: inherit; font-stretch: inherit", "Heading3", "Normal"));
            this.ReportStart += new System.EventHandler(this.WorkTimesReport_ReportStart);
            ((System.ComponentModel.ISupportInitialize)(this.txtItemTotalTime)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTotalSalary)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDetail2Item)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDetail1Item)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TextBox2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picReportHeader)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtContacts1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblProektant)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCreatedBy)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblReportDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblGroup3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblGroup4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblGroup2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picPageHeader)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblPeriod)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblPage)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtContacts)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TextBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtGrandTotal)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtGrandTotalSal)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtHdr1Item)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtHeader1Total)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtGroup1TotalTime)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtGroupTotalSalary)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtHdr2Item)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtGroup2Total)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtGroup2TotalTime)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtGroup2TotalSal)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();

        }

		#endregion

        private ReportHeader ReportHeader;
        private SubReport HeaderSubReport;
        private PageHeader PageHeader;
        private Label Label3;
        private Label lblGroup3;
        private Label lblGroup4;
        private Label lblGroup2;
        private Line Line12;
        private Line Line10;
        private Picture picPageHeader;
        private Label lblPeriod;
        private GroupHeader SummaryHeader;
        private GroupHeader GrandTotalHeader;
        private GroupHeader GroupHeader1;
        private TextBox txtHdr1Item;
        private Line Line1;
        private GroupHeader GroupHeader2;
        private TextBox txtHdr2Item;
        private Line Line2;
        private Detail Detail;
        private TextBox txtItemTotalTime;
        private TextBox txtTotalSalary;
        private TextBox txtDetail2Item;
        private TextBox txtDetail1Item;
        private TextBox TextBox2;
        private GroupFooter GroupFooter2;
        private Line Line3;
        private TextBox txtGroup2Total;
        private TextBox txtGroup2TotalTime;
        private Line Line5;
        private TextBox txtGroup2TotalSal;
        private GroupFooter GroupFooter1;
        private Line Line4;
        private TextBox txtHeader1Total;
        private TextBox txtGroup1TotalTime;
        private TextBox txtGroupTotalSalary;
        private GroupFooter GrandTotalFooter;
        private TextBox TextBox1;
        private TextBox txtGrandTotal;
        private Line Line13;
        private Line Line14;
        private TextBox txtGrandTotalSal;
        private GroupFooter SummaryFooter;
        private SubReport SummarySubReport;
        private PageFooter PageFooter;
        private Label lblPage;
        private Line Line11;
        private TextBox txtContacts;
        private ReportFooter ReportFooter;
        private TextBox txtContacts1;
        private Label lblProektant;
        private TextBox txtCreatedBy;
        private Label lblReportDate;
        private Label Label4;
        private Label Label5;

	}
}
