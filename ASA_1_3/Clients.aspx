﻿<%@ Register TagPrefix="radg" Namespace="Telerik.WebControls" Assembly="RadGrid" %>
<%@ Page language="c#" Codebehind="Clients.aspx.cs" AutoEventWireup="false" Inherits="Asa.Timesheet.WebPages.Clients" %>
<%@ Register TagPrefix="uc1" TagName="PageHeader" Src="UserControls/PageHeader.ascx" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
	<head runat="server">
		<title>Clients</title>
		<meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
		<meta content="C#" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<LINK href="styles/timesheet.css" type="text/css" rel="stylesheet">
	</HEAD>
	<body bottomMargin="0" leftMargin="0" topMargin="0" rightMargin="0">
		<form id="Form1" method="post" runat="server">
			<table height="100%" cellSpacing="0" cellPadding="0" width="100%" border="0">
				<tr height="1">
					<td>
						<uc1:PageHeader id="header" runat="server"></uc1:PageHeader>
					</td>
				</tr>
				<tr>
					<td>
						<table height="100%" cellSpacing="0" cellPadding="0" width="100%" border="0">
							<tr>
								<td class="menuLeft"><asp:placeholder id="menuHolder" runat="server"></asp:placeholder></td>
								
								<td style="PADDING-RIGHT: 5px; PADDING-LEFT: 5px; PADDING-TOP: 3px" vAlign="top" noWrap><br>
									<asp:label id="lblError" runat="server" ForeColor="Red" Visible="False" EnableViewState="False"
										CssClass="ErrorLabel"></asp:label><asp:label id="lblInfo" runat="server" Visible="False" EnableViewState="False" CssClass="InfoLabel"></asp:label><br>
									<br>
									<TABLE id="Table1" style="WIDTH: 848px; HEIGHT: 26px" cellSpacing="1" cellPadding="1" width="848"
										border="0">
										<TR>
											<td style="WIDTH: 105px">
												<asp:label id="Label4" runat="server" CssClass="enterDataLabel" EnableViewState="False" Width="100%"
													Font-Bold="True">Тип:</asp:label></TD>
											<td>
												<asp:dropdownlist id="ddlProjectsStatus" runat="server" CssClass="enterDataBox" Width="250px" AutoPostBack="True"></asp:dropdownlist>&nbsp;&nbsp;&nbsp;</TD>
										</TR>
										<TR>
											<td style="WIDTH: 105px"><asp:label id="Label2" runat="server" CssClass="enterDataLabel" EnableViewState="False" Font-Bold="True"
													Width="100%">Сграда:</asp:label></TD>
											<td><asp:dropdownlist id="ddlBuildingTypes" runat="server" CssClass="enterDataBox" Width="250px" AutoPostBack="True"></asp:dropdownlist></TD>
										</TR>
										<TR>
											<td style="WIDTH: 105px"><asp:label id="Label3" runat="server" CssClass="enterDataLabel" EnableViewState="False" Font-Bold="True"
													Width="100%">Проект:</asp:label></TD>
											<td>
												<asp:DropDownList id="ddlProject" runat="server" CssClass="enterDataBox" Width="250px"></asp:DropDownList></TD>
										</TR>
										<TR>
											<td style="WIDTH: 105px"></TD>
											<td>
												<asp:button id="btnShow" runat="server" CssClass="ActionButton" Text="Покажи"></asp:button></TD>
										</TR>
									</TABLE>
									<p></p>
									<asp:panel id="grid" 
										runat="server" Height="450px" Width="100%">
										<asp:datagrid id="grdClients" runat="server" CssClass="Grid" Width="100%" BackColor="Transparent"
											CellPadding="4" AutoGenerateColumns="False" AllowSorting="True">
											<AlternatingItemStyle CssClass="GridAltItem" BackColor="WhiteSmoke"></AlternatingItemStyle>
											<ItemStyle CssClass="GridItem"></ItemStyle>
											<HeaderStyle CssClass="GridHeader"></HeaderStyle>
											<Columns>
												<asp:TemplateColumn HeaderText="#">
													<ItemStyle Width="10px" ForeColor="DimGray"></ItemStyle>
													<ItemTemplate>
														<asp:Label CssClass="menuTable" runat="server" Text='<%# (int)DataBinder.Eval(Container, "ItemIndex")+1 %>' ID="Label1" NAME="Label1">
														</asp:Label>
													</ItemTemplate>
												</asp:TemplateColumn>
												<asp:TemplateColumn HeaderText="Име" SortExpression="Name">
													<ItemTemplate>
														<asp:LinkButton id="lnk" runat="server" CssClass="menuTable"  CommandName="Edit">
															<span Class="menuTable">
																<%# DataBinder.Eval(Container, "DataItem.ClientName") %>
															</span>
														</asp:LinkButton>
													</ItemTemplate>
												</asp:TemplateColumn>
												<asp:BoundColumn DataField="Phone" SortExpression="Phone" HeaderText="Телефон"></asp:BoundColumn>
												<asp:BoundColumn DataField="Fax" SortExpression="Fax" HeaderText="Факс"></asp:BoundColumn>
												<asp:BoundColumn DataField="Email" SortExpression="EMail" HeaderText="Е-мейл"></asp:BoundColumn>
												<asp:BoundColumn DataField="City" SortExpression="City" HeaderText="Град">
													<ItemStyle ForeColor="DimGray"></ItemStyle>
												</asp:BoundColumn>
												<asp:BoundColumn DataField="Address" SortExpression="Address" HeaderText="Адрес">
													<ItemStyle ForeColor="DimGray"></ItemStyle>
												</asp:BoundColumn>
												<asp:BoundColumn DataField="ClientTypeID" SortExpression="ClientTypeID" HeaderText="Тип"></asp:BoundColumn>
												<asp:TemplateColumn>
													<HeaderStyle Wrap="False" Width="20px"></HeaderStyle>
													<ItemStyle Wrap="False" HorizontalAlign="Center"></ItemStyle>
													<ItemTemplate>
														<asp:ImageButton id="ibEditItem" runat="server" ImageUrl="images/edit1.gif" ToolTip="Редактирай"></asp:ImageButton>
													</ItemTemplate>
												</asp:TemplateColumn>
												<asp:TemplateColumn>
													<HeaderStyle Wrap="False" Width="20px"></HeaderStyle>
													<ItemStyle Wrap="False" HorizontalAlign="Center"></ItemStyle>
													<ItemTemplate>
														<asp:ImageButton id="ibDeleteItem" runat="server" Width="12px" Height="12px" ImageUrl="images/delete.gif"
															ToolTip="Изтрий"></asp:ImageButton>
													</ItemTemplate>
												</asp:TemplateColumn>
											</Columns>
										</asp:datagrid>
										<BR>
										<BR>
										<asp:label id="lblName" runat="server" EnableViewState="False" Font-Bold="True">Подизпълнители</asp:label>
										<BR>
										<BR>
										<asp:datagrid id="grdSub" runat="server" CssClass="Grid" Width="100%" BackColor="Transparent"
											CellPadding="4" AutoGenerateColumns="False" AllowSorting="True">
											<AlternatingItemStyle CssClass="GridAltItem" BackColor="WhiteSmoke"></AlternatingItemStyle>
											<ItemStyle CssClass="GridItem"></ItemStyle>
											<HeaderStyle CssClass="GridHeader"></HeaderStyle>
											<Columns>
												<asp:TemplateColumn HeaderText="#">
													<ItemStyle ForeColor="DimGray" Width="10px"></ItemStyle>
													<ItemTemplate>
														<asp:Label CssClass="menuTable" runat="server" Text='<%# (int)DataBinder.Eval(Container, "ItemIndex")+1 %>' ID="Label1" NAME="Label1">
														</asp:Label>
													</ItemTemplate>
												</asp:TemplateColumn>
												<asp:TemplateColumn SortExpression="Name" HeaderText="Име" ItemStyle-Width="350px">
													<ItemTemplate>
														<asp:Label CssClass="menuTable" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.SubcontracterName") %>' ID="Label5" NAME="Label1">
														</asp:Label>
													</ItemTemplate>
												</asp:TemplateColumn>
												<asp:BoundColumn DataField="SubcontracterType" SortExpression="Type" HeaderText="Тип" ItemStyle-Width="300px">
													<HeaderStyle Width="50px"></HeaderStyle>
													<ItemStyle ForeColor="DimGray"></ItemStyle>
												</asp:BoundColumn>
												<asp:BoundColumn DataField="Phone" SortExpression="Phone" HeaderText="Телефон"></asp:BoundColumn>
												<asp:BoundColumn DataField="Fax" SortExpression="Fax" HeaderText="Факс"></asp:BoundColumn>
												<asp:BoundColumn DataField="Email" SortExpression="Email" HeaderText="Е-мейл"></asp:BoundColumn>
												<asp:BoundColumn DataField="Address" SortExpression="Address" HeaderText="Адрес">
													<ItemStyle ForeColor="DimGray"></ItemStyle>
												</asp:BoundColumn>
											</Columns>
										</asp:datagrid>
									</asp:panel>
									<TABLE id="Table4" 
										cellSpacing="0" cellPadding="4" width="100%" border="0">
										<TR>
											<td noWrap><asp:button id="btnNewClient" runat="server" CssClass="ActionButton" Width="200px"></asp:button>&nbsp;
												<asp:button id="btnExport" runat="server" CssClass="ActionButton" Text="Експорт"></asp:button></TD>
										</TR>
									</TABLE>
								</td>
							</tr>
						</table>
					</td>
				</tr>
			</table>
			<radg:radgrid id="gridCalls" runat="server" Width="100%" CssClass="RadGrid" GridLines="Horizontal"
				AutoGenerateColumns="False">
				<PAGERSTYLE CssClass="GridHeader" Mode="NumericPages"></PAGERSTYLE>
				<ITEMSTYLE CssClass="GridItem" HorizontalAlign="Center"></ITEMSTYLE>
				<GROUPPANEL Visible="False"></GROUPPANEL>
				<HEADERSTYLE CssClass="GridHeader" HorizontalAlign="Center" Wrap="False"></HEADERSTYLE>
				<ALTERNATINGITEMSTYLE CssClass="GridItem" HorizontalAlign="Center"></ALTERNATINGITEMSTYLE>
				<GROUPHEADERITEMSTYLE BackColor="Silver" BorderColor="Black"></GROUPHEADERITEMSTYLE>
				<MASTERTABLEVIEW Visible="True" AllowSorting="True" GridLines="Horizontal" AllowPaging="False" PageSize="15"
					AllowCustomPaging="False" DataSourcePersistenceMode="NoPersistence">
					<COLUMNS>
						<RADG:GRIDBOUNDCOLUMN DataField="ClientName" HeaderText="Име" HeaderButtonType="TextButton" UniqueName="ClientName"></RADG:GRIDBOUNDCOLUMN>
						<RADG:GRIDBOUNDCOLUMN DataField="Delo" HeaderText="Фирмено дело" HeaderButtonType="TextButton" UniqueName="Delo"></RADG:GRIDBOUNDCOLUMN>
						<RADG:GRIDBOUNDCOLUMN DataField="Bulstat" HeaderText="Булстат" HeaderButtonType="TextButton" UniqueName="Bulstat"></RADG:GRIDBOUNDCOLUMN>
						<RADG:GRIDBOUNDCOLUMN DataField="NDR" HeaderText="НДР" HeaderButtonType="TextButton" UniqueName="NDR"></RADG:GRIDBOUNDCOLUMN>
						<RADG:GRIDBOUNDCOLUMN DataField="City" HeaderText="Град" HeaderButtonType="TextButton" UniqueName="City"></RADG:GRIDBOUNDCOLUMN>
						<RADG:GRIDBOUNDCOLUMN DataField="Address" HeaderText="Адрес" HeaderButtonType="TextButton" UniqueName="Address"></RADG:GRIDBOUNDCOLUMN>
						<RADG:GRIDBOUNDCOLUMN DataField="Phone" HeaderText="Телефон" HeaderButtonType="TextButton" UniqueName="Phone"></RADG:GRIDBOUNDCOLUMN>
						<RADG:GRIDBOUNDCOLUMN DataField="Fax" HeaderText="Факс" HeaderButtonType="TextButton" UniqueName="Fax"></RADG:GRIDBOUNDCOLUMN>
						<RADG:GRIDBOUNDCOLUMN DataField="Email" HeaderText="Ел.адрес" HeaderButtonType="TextButton" UniqueName="Email"></RADG:GRIDBOUNDCOLUMN>
						<RADG:GRIDBOUNDCOLUMN DataField="Webpage" HeaderText="Уебсайт" HeaderButtonType="TextButton" UniqueName="Webpage"></RADG:GRIDBOUNDCOLUMN>
						<RADG:GRIDBOUNDCOLUMN DataField="Manager" HeaderText="Представлявано от" HeaderButtonType="TextButton"
							UniqueName="Manager"></RADG:GRIDBOUNDCOLUMN>
						<RADG:GRIDBOUNDCOLUMN DataField="Representative1" HeaderText="Представител-договор:" HeaderButtonType="TextButton"
							UniqueName="Name1"></RADG:GRIDBOUNDCOLUMN>
						<RADG:GRIDBOUNDCOLUMN DataField="Phone1" HeaderText="Телефон" HeaderButtonType="TextButton" UniqueName="Phone1"></RADG:GRIDBOUNDCOLUMN>
						<RADG:GRIDBOUNDCOLUMN DataField="Email1" HeaderText="Ел.адрес" HeaderButtonType="TextButton" UniqueName="Email1"></RADG:GRIDBOUNDCOLUMN>
						<RADG:GRIDBOUNDCOLUMN DataField="Representative2" HeaderText="Представител-техн. въпроси:" HeaderButtonType="TextButton"
							UniqueName="Name2"></RADG:GRIDBOUNDCOLUMN>
						<RADG:GRIDBOUNDCOLUMN DataField="Phone2" HeaderText="Телефон" HeaderButtonType="TextButton" UniqueName="Phone2"></RADG:GRIDBOUNDCOLUMN>
						<RADG:GRIDBOUNDCOLUMN DataField="Email2" HeaderText="Ел.адрес" HeaderButtonType="TextButton" UniqueName="Email2"></RADG:GRIDBOUNDCOLUMN>
					</COLUMNS>
					<ROWINDICATORCOLUMN Visible="False" UniqueName="RowIndicator">
						<HEADERSTYLE Width="20px"></HEADERSTYLE>
					</ROWINDICATORCOLUMN>
					<EDITFORMSETTINGS>
						<EDITCOLUMN UniqueName="EditCommandColumn"></EDITCOLUMN>
					</EDITFORMSETTINGS>
					<EXPANDCOLLAPSECOLUMN Visible="False" UniqueName="ExpandColumn" ButtonType="ImageButton">
						<HEADERSTYLE Width="19px"></HEADERSTYLE>
					</EXPANDCOLLAPSECOLUMN>
				</MASTERTABLEVIEW>
			</radg:radgrid>
		</form>
	</body>
</HTML>
