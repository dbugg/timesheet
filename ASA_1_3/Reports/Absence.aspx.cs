using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Data.SqlClient;
using log4net;
using Asa.Timesheet.Data;
using Asa.Timesheet.Data.Entities;
using Asa.Timesheet.Data.Util;
using Asa.Timesheet.Data.Reports;
using GrapeCity.ActiveReports.Export.Pdf.Section;
using GrapeCity.ActiveReports.Export.Excel.Section;
namespace Asa.Timesheet.WebPages.Reports
{
	/// <summary>
	/// Summary description for EmptyHours.
	/// </summary>
	public class AbsencePage : TimesheetPageBase
	{
		private static readonly DateTime MinReportDate = DateTime.ParseExact(System.Configuration.ConfigurationManager.AppSettings["MinReportDate"],
			"ddMMyyyy", System.Globalization.CultureInfo.InvariantCulture);
		private static readonly ILog log = LogManager.GetLogger(typeof(EmptyHours));

		#region Web controls

		protected System.Web.UI.WebControls.PlaceHolder menuHolder;
		protected System.Web.UI.WebControls.Label lblInfo;
		protected System.Web.UI.WebControls.Label lblError;
		protected System.Web.UI.WebControls.Calendar calStartDate;
		protected System.Web.UI.WebControls.Calendar calEndDate;
		protected System.Web.UI.WebControls.Label lblStartDay;
		protected System.Web.UI.HtmlControls.HtmlTable tblFooter;
		protected System.Web.UI.WebControls.CheckBox ckDates;
		protected System.Web.UI.WebControls.Label lblSelStartDate;
		protected System.Web.UI.WebControls.Label lblEndDay;
		protected System.Web.UI.WebControls.Label lblSelEndDate;
		protected System.Web.UI.WebControls.Label lblSluj;
		protected System.Web.UI.WebControls.DropDownList ddlUser;
		protected Asa.Timesheet.WebPages.UserControls.PageHeader header;
		protected System.Web.UI.WebControls.ImageButton ibXlsExport;
		protected System.Web.UI.WebControls.ImageButton ibPdfExport;
		protected System.Web.UI.WebControls.ImageButton ibGenerate;
		protected System.Web.UI.WebControls.Label Label1;
		protected System.Web.UI.WebControls.DropDownList ddlProject;
		protected System.Web.UI.WebControls.DataGrid grdAbsence;
		protected System.Web.UI.WebControls.DropDownList ddlUserStatus;
		protected System.Web.UI.WebControls.Label lbReason;
		protected System.Web.UI.WebControls.TextBox txtReason;
		protected System.Web.UI.WebControls.Button btnSendRequestAbsence;
		protected System.Web.UI.WebControls.Label Label2;
		protected System.Web.UI.WebControls.Calendar calSt;
		protected System.Web.UI.WebControls.Label Label4;
		protected System.Web.UI.WebControls.Calendar calEn;
		protected System.Web.UI.WebControls.Label lbSt;
		protected System.Web.UI.WebControls.Label lbEn;
		protected System.Web.UI.WebControls.Label lblReportTitle;
		protected System.Web.UI.WebControls.LinkButton lkSchedule;
		protected System.Web.UI.HtmlControls.HtmlTable tblRequestAbsence;
		
		
		#endregion
		
		#region Page load

        private void Page_Load(object sender, System.EventArgs e)
        {
            if ((!(LoggedUser.IsLeader || LoggedUser.IsAdmin || LoggedUser.IsAccountant || LoggedUser.IsSecretary))) ErrorRedirect(Resource.ResourceManager["noRightsAlert"]);

            if (!this.IsPostBack)
            {
                this.header.PageTitle = Resource.ResourceManager["report_Absence_PageTitle"];
                this.header.UserName = this.LoggedUser.UserName;

                if (!LoadUsers()) ErrorRedirect("<br>" + Resource.ResourceManager["error_HitRefresh"]);
                calStartDate.SelectedDate = new DateTime(DateTime.Today.Year, DateTime.Today.Month, 1);
                calStartDate.VisibleDate = calStartDate.SelectedDate;
                lblSelStartDate.Text = String.Concat("(", calStartDate.SelectedDates[0].ToString("d"), ")");
                calEndDate.SelectedDate = DateTime.Today;
                calEndDate.VisibleDate = calEndDate.SelectedDate;
                lblSelEndDate.Text = String.Concat("(", calEndDate.SelectedDates[0].ToString("d"), ")");
                LoadDDLAbsence();
            }


            UIHelpers.CreateMenuReports(menuHolder, LoggedUser);
        }


        private void LoadDDLAbsence()
        {
            ListItem li = new ListItem(Resource.ResourceManager["reports_All1"], "0");
            ddlProject.Items.Add(li);
            li = new ListItem(Resource.ResourceManager["strPlatenOtpusk"], System.Configuration.ConfigurationManager.AppSettings["Platen"]);
            ddlProject.Items.Add(li);
            li = new ListItem(Resource.ResourceManager["strNeplatenOtpusk"], System.Configuration.ConfigurationManager.AppSettings["Neplaten"]);
            ddlProject.Items.Add(li);
            li = new ListItem(Resource.ResourceManager["strBolnichni"], System.Configuration.ConfigurationManager.AppSettings["Bolen"]);
            ddlProject.Items.Add(li);
            ddlProject.SelectedValue = "0";
        }
		#endregion

		#region Web Form Designer generated code
        override protected void OnInit(EventArgs e)
        {
            //
            // CODEGEN: This call is required by the ASP.NET Web Form Designer.
            //
            InitializeComponent();
            base.OnInit(e);
        }
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
        private void InitializeComponent()
        {
            this.ckDates.CheckedChanged += new System.EventHandler(this.ckDates_CheckedChanged);
            this.calStartDate.SelectionChanged += new System.EventHandler(this.calStartDate_SelectionChanged);
            this.calEndDate.SelectionChanged += new System.EventHandler(this.calEndDate_SelectionChanged);
            this.ddlUserStatus.SelectedIndexChanged += new System.EventHandler(this.ddlUserStatus_SelectedIndexChanged);
            this.ibGenerate.Click += new System.Web.UI.ImageClickEventHandler(this.ibGenerate_Click);
            this.ibPdfExport.Click += new System.Web.UI.ImageClickEventHandler(this.ibPdfExport_Click);
            this.ibXlsExport.Click += new System.Web.UI.ImageClickEventHandler(this.ibXlsExport_Click);
            this.grdAbsence.ItemDataBound += new System.Web.UI.WebControls.DataGridItemEventHandler(this.grdAbsence_ItemDataBound);
            this.lkSchedule.Click += new System.EventHandler(this.lkSchedule_Click);
            this.Load += new System.EventHandler(this.Page_Load);

        }
		#endregion

		#region DB




        private bool LoadUsers()
        {
            //if (this.LoggedUser.IsLeader)
            //{
            ddlUser.Visible = true;

            SqlDataReader reader = null;
            try
            {
                switch (ddlUserStatus.SelectedValue)
                {
                    case "0":
                        reader = UsersData.SelectUserNames(true, true, (LoggedUser.HasPaymentRights || UIHelpers.GetIDParam() != -1)); break;
                    case "1":
                        reader = UsersData.SelectUserNames(false, true, (LoggedUser.HasPaymentRights || UIHelpers.GetIDParam() != -1)); break;
                    case "2":
                        reader = UsersData.SelectUserNames1(); break;
                }
                ddlUser.DataSource = reader;
                ddlUser.DataValueField = "UserID";
                ddlUser.DataTextField = "FullName";
                ddlUser.DataBind();
                ddlUser.Items.Insert(0, new ListItem("<" + Resource.ResourceManager["reports_ddlAllUsers"] + ">", "0"));
                ddlUser.SelectedValue = "0";

            }
            catch (Exception ex)
            {
                log.Error(ex);
                return false;
            }
            finally
            {
                if (reader != null) reader.Close();
            }
            //}

            return true;
        }

        private ArrayList SelectEnteredDays(int userID, DateTime startDate, DateTime endDate)
        {
            ArrayList days = new ArrayList();

            SqlDataReader reader = null;
            try
            {
                reader = ReportsData.SelectEnteredDays(userID, startDate, endDate);
                while (reader.Read())
                {
                    days.Add(reader.GetDateTime(1));
                }
            }
            catch (Exception ex)
            {
                log.Error(ex);
                return null;
            }
            finally
            {
                if (reader != null) reader.Close();
            }

            return days;
        }


		#endregion

		#region Event handlers

        private void btnOK_Click(object sender, System.EventArgs e)
        {



        }
        private ArrayList GetData(out string title)
        {
            title = "";
            DateTime startDay = calStartDate.SelectedDate, endDay = calEndDate.SelectedDate;

            if (ckDates.Checked)
            {
                startDay = MinReportDate;
                endDay = DateTime.Today;
            }
            else
            {
                if (startDay < MinReportDate) startDay = MinReportDate;
                if (endDay > DateTime.Today) endDay = DateTime.Today;

                if (startDay > endDay)
                {
                    lblError.Text = Resource.ResourceManager["reports_errorDate"];
                    return null;
                }
            }

            int userID = int.Parse(ddlUser.SelectedValue);
            int startIndex, endIndex;

            if (userID == 0) { startIndex = 1; endIndex = ddlUser.Items.Count - 1; }
            else { startIndex = ddlUser.SelectedIndex; endIndex = ddlUser.SelectedIndex; }

            ArrayList reportData = new ArrayList();
            ArrayList days = null;

            for (int i = startIndex; i <= endIndex; i++)
            {
                string userName = ddlUser.Items[i].Text;
                userID = int.Parse(ddlUser.Items[i].Value);

                days = SelectEnteredDays(userID, startDay, endDay);

                if (days == null) // error loading days from DB, set error message, continue to next user
                {
                    reportData.Add(new DaysListInfo(userID, userName, "Error"));
                    continue;
                }

                ArrayList notEnteredDays = TimeHelper.GetNotEnteredDaysList(startDay, endDay, days, true);

                string s = DaysListInfo.GetDaysString(notEnteredDays, false, "dd.MM.yy");
                if (s == String.Empty) s = Resource.ResourceManager["reports_EmptyDays_NoEmptyDays"];
                DaysListInfo di = new DaysListInfo(userID, userName, s);
                di.Days = TimeHelper.GetNotEnteredDaysList(startDay, endDay, days, false).Count;
                reportData.Add(di);

            }
            title = String.Concat(Resource.ResourceManager["reports_EmptyDays_GridTitle"],
                Resource.ResourceManager["rpt_periodFrom"], " ", startDay.ToShortDateString(), " ",
                Resource.ResourceManager["rpt_periodTo"], " " + endDay.ToShortDateString());
            lblReportTitle.Text = title;
            return reportData;
        }

        private void ckDates_CheckedChanged(object sender, System.EventArgs e)
        {
            lblSelStartDate.Text = ckDates.Checked ? String.Concat("(", MinReportDate.ToString("d"), ")") :
                String.Concat("(", calStartDate.SelectedDates[0].ToString("d"), ")");
            lblSelEndDate.Text = ckDates.Checked ? String.Concat("(", DateTime.Today.ToString("d"), ")") :
                String.Concat("(", calEndDate.SelectedDates[0].ToString("d"), ")");

            calStartDate.Enabled = calEndDate.Enabled = !ckDates.Checked;
        }

        private void calStartDate_SelectionChanged(object sender, System.EventArgs e)
        {
            lblSelStartDate.Text = String.Concat("(", calStartDate.SelectedDates[0].ToString("d"), ")");
            if (calStartDate.SelectedDates.Count > 1)
            {
                calEndDate.SelectedDate = calStartDate.SelectedDates[calStartDate.SelectedDates.Count - 1];
                calEndDate.VisibleDate = calEndDate.SelectedDate;

                calStartDate.SelectedDate = calStartDate.SelectedDates[0];
                lblSelEndDate.Text = String.Concat("(", calEndDate.SelectedDate.ToString("d"), ")"); ;
            }
        }

        private void calEndDate_SelectionChanged(object sender, System.EventArgs e)
        {
            lblSelEndDate.Text = String.Concat("(", calEndDate.SelectedDates[0].ToString("d"), ")");
        }

		#endregion

		#region Util

		
		#endregion

        private DataView GetDataView(out string t)
        {
            string title;
            ArrayList al = GetData(out title);
            t = title;
            DataTable dt = new DataTable();
            dt.Columns.Add(new DataColumn("UserName"));
            dt.Columns.Add(new DataColumn("DaysString"));
            dt.Columns.Add(new DataColumn("Days"));

            foreach (DaysListInfo li in al)
            {
                DataRow dr = dt.NewRow();
                dr[0] = li.UserName;
                dr[1] = li.DaysString;
                dr[2] = li.Days.ToString();
                dt.Rows.Add(dr);
            }
            dt.AcceptChanges();
            return new DataView(dt);
        }

        private void ibPdfExport_Click(object sender, System.Web.UI.ImageClickEventArgs e)
        {
            int userID = int.Parse(ddlUser.SelectedValue);
            int projectID = int.Parse(ddlProject.SelectedValue);

            DateTime startDate = ckDates.Checked ? Constants.DateMin : calStartDate.SelectedDate;
            DateTime endDate = ckDates.Checked ? Constants.DateMax : calEndDate.SelectedDate;

            PdfExport pdf = new PdfExport();

            pdf.NeverEmbedFonts = "";

            DataSet ds = null;
            switch (ddlUserStatus.SelectedValue)
            {
                case "0": ds = AbsenceReportData.SelectAbsenceReportDS(
                              userID, projectID, startDate, endDate, true); break;
                case "1": ds = AbsenceReportData.SelectAbsenceReportDS(
                              userID, projectID, startDate, endDate, false); break;
                case "2": ds = AbsenceReportData.SelectAbsenceReportDSUnactiveUsers(
                              userID, projectID, startDate, endDate); break;
            }

            GrapeCity.ActiveReports.SectionReport report = new AbsenceReport(true, startDate, endDate, ds);

            report.DataSource = ds.Tables[0];
            report.Run();

            System.IO.MemoryStream memoryFile = new System.IO.MemoryStream();
            pdf.Export(report.Document, memoryFile);

            Response.Clear();
            Response.AppendHeader("content-disposition"
                , "attachment; filename=" + "EmptyHours.pdf");
            Response.ContentType = "application/pdf";

            memoryFile.WriteTo(Response.OutputStream);
            Response.End();
        }

        private void ibXlsExport_Click(object sender, System.Web.UI.ImageClickEventArgs e)
        {
            XlsExport xls = new XlsExport();

            int userID = int.Parse(ddlUser.SelectedValue);
            int projectID = int.Parse(ddlProject.SelectedValue);

            DateTime startDate = ckDates.Checked ? Constants.DateMin : calStartDate.SelectedDate;
            DateTime endDate = ckDates.Checked ? Constants.DateMax : calEndDate.SelectedDate;
            DataSet ds = null;
            switch (ddlUserStatus.SelectedValue)
            {
                case "0": ds = AbsenceReportData.SelectAbsenceReportDS(
                              userID, projectID, startDate, endDate, true); break;
                case "1": ds = AbsenceReportData.SelectAbsenceReportDS(
                              userID, projectID, startDate, endDate, false); break;
                case "2": ds = AbsenceReportData.SelectAbsenceReportDSUnactiveUsers(
                              userID, projectID, startDate, endDate); break;
            }
            GrapeCity.ActiveReports.SectionReport report = new AbsenceReport(false, startDate, endDate, ds);

            report.DataSource = ds.Tables[0];
            report.Run();

            System.IO.MemoryStream memoryFile = new System.IO.MemoryStream();
            xls.Export(report.Document, memoryFile);

            Response.Clear();
            Response.AppendHeader("content-disposition"
                , "attachment; filename=" + "EmptyHours.xls");
            Response.ContentType = "application/pdf";

            memoryFile.WriteTo(Response.OutputStream);
            Response.End();
        }

        private void ibGenerate_Click(object sender, System.Web.UI.ImageClickEventArgs e)
        {
            GenerateClick();
            //			int userID = int.Parse(ddlUser.SelectedValue);
            //			int projectID = int.Parse(ddlProject.SelectedValue);
            //
            //			DateTime startDate = ckDates.Checked ? Constants.DateMin : calStartDate.SelectedDate;
            //			DateTime endDate = ckDates.Checked ? Constants.DateMax : calEndDate.SelectedDate;
            //			switch (ddlUserStatus.SelectedValue)
            //			{
            //				case "0":grdAbsence.DataSource = AbsenceReportData.SelectAbsenceReportDS(
            //							 userID, projectID, startDate, endDate,true);break;
            //				case "1":grdAbsence.DataSource = AbsenceReportData.SelectAbsenceReportDS(
            //							 userID, projectID, startDate, endDate,false);break;
            //				case "2":grdAbsence.DataSource = AbsenceReportData.SelectAbsenceReportDSUnactiveUsers(
            //							 userID, projectID, startDate, endDate);break;
            //			}
            //			grdAbsence.DataBind();
        }

        private void GenerateClick()
        {
            int userID = int.Parse(ddlUser.SelectedValue);
            int projectID = int.Parse(ddlProject.SelectedValue);

            DateTime startDate = ckDates.Checked ? Constants.DateMin : calStartDate.SelectedDate;
            DateTime endDate = ckDates.Checked ? Constants.DateMax : calEndDate.SelectedDate;
            switch (ddlUserStatus.SelectedValue)
            {
                case "0": grdAbsence.DataSource = AbsenceReportData.SelectAbsenceReportDS(
                              userID, projectID, startDate, endDate, true); break;
                case "1": grdAbsence.DataSource = AbsenceReportData.SelectAbsenceReportDS(
                              userID, projectID, startDate, endDate, false); break;
                case "2": grdAbsence.DataSource = AbsenceReportData.SelectAbsenceReportDSUnactiveUsers(
                              userID, projectID, startDate, endDate); break;
            }
            grdAbsence.DataBind();
        }

        private void grdAbsence_ItemDataBound(object sender, System.Web.UI.WebControls.DataGridItemEventArgs e)
        {
            if ((e.Item.ItemType == ListItemType.Item) || (e.Item.ItemType == ListItemType.AlternatingItem))
            {
                DataGrid grdReport = (DataGrid)e.Item.FindControl("grdReport");

                DataRowView drv = (DataRowView)e.Item.DataItem;
                DataTable dt = AbsenceReportData.GetDaysStringForUser(drv.Row);

                grdReport.DataSource = dt;
                grdReport.DataBind();

                ((Label)e.Item.FindControl("lblName")).Text = drv["Name"].ToString();
            }
        }

        private void ddlUserStatus_SelectedIndexChanged(object sender, System.EventArgs e)
        {
            LoadUsers();
        }
        private void AddGroup(DataTable dt1, string groupName, string nameOfProcedure)
        {
            AddRow(dt1, groupName, -1);
            AddEmptyRows(dt1, 1);

            DataTable dtUsers = CommonDAL.CreateTableFromReader(UsersData.SelectUsersSplit(Constants.DirectorRole, 1, nameOfProcedure, -1, -1));
            foreach (DataRow drr in dtUsers.Rows)
                AddRow(dt1, (string)drr["FullName"], (int)drr["UserID"]);

        }
        private DataTable MakeTable(DataTable dt)
        {

            //notes:dt1,dt2 - first and second colomns in schedule
            //	dt3 - projects id
            //	dt4,dt5 - startdate,enddate
            //	dtNotes - notes
            DataTable dt1 = new DataTable();
            dt1.Columns.Add();
            dt1.Columns.Add(new DataColumn("ID", typeof(int)));
            AddGroup(dt1, Resource.ResourceManager["Name_Arch"], "UsersListArcgitectProc");
            AddGroup(dt1, Resource.ResourceManager["Name_Eng"], "UsersListIngenerProc");
            AddGroup(dt1, Resource.ResourceManager["Name_IT"], "UsersListITProc");
            AddGroup(dt1, Resource.ResourceManager["Name_ACC"], "UsersListACCProc");
            AddGroup(dt1, Resource.ResourceManager["Name_Secr"], "UsersListSecretaryProc");



            dt.Rows.Clear();
            for (int i = 0; i < dt1.Rows.Count; i++)
            {
                DataRow dr = dt.NewRow();

                dr[(int)TableColumns.ProjectID] = dt1.Rows[i][1];
                dr[(int)TableColumns.ProjectName] = dt1.Rows[i][0];
                dt.Rows.Add(dr);
            }
            return dt;
        }
		private enum TableColumns
		{
			ProjectScheduleID = 0,
			ProjectID,
			SubcontracterID,
			IsProjectMain,
			ProjectName,
			SubcontracterName ,
			StartDate      ,
			EndDate,
			ProjectCode,
			FullName,
			Notes,
		}

        private void AddRow(DataTable dt, string Value, int nID)
        {
            DataRow dr = dt.NewRow();
            dr[0] = Value;
            dr[1] = nID;
            dt.Rows.Add(dr);
        }
        private void AddEmptyRows(DataTable dt, int RowToAdd)
        {
            for (int i = 0; i < RowToAdd; i++)
            {
                AddRow(dt, " ", -1);
            }
        }
        private void lkSchedule_Click(object sender, System.EventArgs e)
        {
            XlsExport xls = new XlsExport();

            DataSet ds = ReportsData.ExecuteReportsSchedule(-1);
            DataTable dt = MakeTable(ds.Tables[0]);
            DataView dv = new DataView(dt);


            DateTime startDate = new DateTime(DateTime.Now.Date.Year, DateTime.Now.Date.Month, 1, 0, 0, 0);

            int nMonths = int.Parse(System.Configuration.ConfigurationManager.AppSettings["VacationMonths"]);
            DateTime endDate = startDate.AddMonths(nMonths).AddDays(-1);

            if (!ckDates.Checked)
            {
                startDate = new DateTime(calStartDate.SelectedDate.Year, calStartDate.SelectedDate.Month, 1, 0, 0, 0);
                endDate = new DateTime(calEndDate.SelectedDate.Year, calEndDate.SelectedDate.Month, 1, 0, 0, 0).AddMonths(1).AddDays(-1);
            }
            DataSet dsAbsence = AbsenceReportData.SelectAbsenceReportDS(
                             0, 0, startDate, endDate, false);
            GrapeCity.ActiveReports.SectionReport report =
                new ScheduleVacation(dv, dsAbsence, startDate, endDate);

            //report.DataSource = ds.Tables[0];
            report.Run();

            System.IO.MemoryStream memoryFile = new System.IO.MemoryStream();
            xls.Export(report.Document, memoryFile);

            Response.Clear();
            Response.AppendHeader("content-disposition"
                , "attachment; filename=" + "VacationSchedule.xls");
            Response.ContentType = "application/xls";

            memoryFile.WriteTo(Response.OutputStream);
            Response.End();
        }

		
	}
}
