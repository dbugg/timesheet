using System;
using System.Data;
using System.Data.SqlClient;
using Microsoft.ApplicationBlocks.Data;

namespace Asa.Timesheet.Data
{
	/// <summary>
	/// Summary description for ClientsData.
	/// </summary>
	public class ClientsData
	{
		public static SqlDataReader SelectClients(int sortOrder,int clientTypeID,bool withPMUsers)
		{
			string spName = "ClientsListNoPMUserProc";

			if(withPMUsers)
				spName = "ClientsListProc";

			SqlParameter[] storedParams = SqlHelperParameterCache.GetSpParameterSet(DBManager.GetConnectionString(), spName);
			storedParams[0].Value = sortOrder;
			if(clientTypeID>0)
				storedParams[1].Value = clientTypeID;

			return SqlHelper.ExecuteReader(DBManager.GetConnectionString(), CommandType.StoredProcedure, spName, storedParams);
		}

		public static SqlDataReader SelectClientsFromProjects(int sortOrder,int clientTypeID,int projectID,int buildingTypeID, int ProjectByStatus)
		{
			string spName = "ClientsListForProjectsProc";

			SqlParameter[] storedParams = SqlHelperParameterCache.GetSpParameterSet(DBManager.GetConnectionString(), spName);
			storedParams[0].Value = sortOrder;
			if(clientTypeID>0)
				storedParams[1].Value = clientTypeID;
			if(projectID >0)
				storedParams[2].Value = projectID;
			if(buildingTypeID>0)
				storedParams[3].Value = buildingTypeID;
			switch(ProjectByStatus)
			{
				case (int)ProjectsData.ProjectsByStatus.Active:{storedParams[4].Value = true;storedParams[5].Value = false;break;}
				case (int)ProjectsData.ProjectsByStatus.Passive:{storedParams[4].Value = false;storedParams[5].Value = false;break;}
				case (int)ProjectsData.ProjectsByStatus.Conluded:{storedParams[5].Value = true;break;}
			}
			return SqlHelper.ExecuteReader(DBManager.GetConnectionString(), CommandType.StoredProcedure, spName, storedParams);
		}

		public static void DeleteClient(int clientID)
		{
			string spName = "ClientsDelProc";

			SqlParameter[] storedParams = SqlHelperParameterCache.GetSpParameterSet(DBManager.GetConnectionString(), spName);
			storedParams[0].Value = clientID;

			SqlHelper.ExecuteNonQuery(DBManager.GetConnectionString(), CommandType.StoredProcedure, spName, storedParams);
		}

		public static void UpdateClient(int clientID, string clientName, string city, string address, string email,string fax,string manager, string repr1, string repr2,
			string delo, string bulstat, string ndr, string phone, string webpage, string phone1, string email1,
			string phone2, string email2,int clientTypeID,string accountName,string accountPass,string sNameEN, string sReprEN)
		{
			string spName = "ClientsUpdProc";

			SqlParameter[] storedParams = SqlHelperParameterCache.GetSpParameterSet(DBManager.GetConnectionString(), spName);
			storedParams[0].Value = clientID;
			storedParams[1].Value = clientName;
			storedParams[2].Value = (city == String.Empty) ? null : city;
			storedParams[3].Value = (address == String.Empty) ? null : address;
			storedParams[4].Value = (email == String.Empty) ? null : email;
			storedParams[5].Value = (fax == String.Empty) ? null : fax;
			storedParams[6].Value = (manager == String.Empty) ? null : manager;
			storedParams[7].Value = (repr1 == String.Empty) ? null : repr1;
			storedParams[8].Value = (repr2 == String.Empty) ? null : repr2;
			storedParams[9].Value = (delo == String.Empty) ? null : delo;
			storedParams[10].Value = (bulstat == String.Empty) ? null : bulstat;
			storedParams[11].Value = (ndr == String.Empty) ? null : ndr;
			storedParams[12].Value = (phone == String.Empty) ? null : phone;
			storedParams[13].Value = (webpage == String.Empty) ? null : webpage;
			storedParams[14].Value = (phone1 == String.Empty) ? null : phone1;
			storedParams[15].Value = (email1 == String.Empty) ? null : email1;
			storedParams[16].Value = (phone2 == String.Empty) ? null : phone2;
			storedParams[17].Value = (email2 == String.Empty) ? null : email2;
			storedParams[18].Value = clientTypeID;
			storedParams[19].Value = (accountName == String.Empty) ? null : accountName;
			storedParams[20].Value = (accountPass == String.Empty) ? null : accountPass;
			storedParams[22].Value = (sNameEN == String.Empty) ? null : sNameEN;
			storedParams[23].Value = (sReprEN == String.Empty) ? null : sReprEN;

			SqlHelper.ExecuteNonQuery(DBManager.GetConnectionString(), CommandType.StoredProcedure, spName, storedParams);
		}

		public static int InsertClient(string clientName, string city, string address, string email, bool isActive,string fax,string manager, string repr1, string repr2,
			string delo, string bulstat, string ndr, string phone, string webpage, string phone1, string email1,
			string phone2, string email2,int clientTypeID,string accountName,string accountPass,string sNameEN, string sReprEN)
		{
			string spName = "ClientsInsProc";

			SqlParameter[] storedParams = SqlHelperParameterCache.GetSpParameterSet(DBManager.GetConnectionString(), spName);
			storedParams[0].Value = clientName;
			storedParams[1].Value = (city == String.Empty) ? null : city;
			storedParams[2].Value = (address == String.Empty) ? null : address;
			storedParams[3].Value = (email == String.Empty) ? null : email;
			storedParams[4].Value = isActive;
			storedParams[5].Value = (fax == String.Empty) ? null : fax;
			storedParams[6].Value = (manager == String.Empty) ? null : manager;
			storedParams[7].Value = (repr1 == String.Empty) ? null : repr1;
			storedParams[8].Value = (repr2 == String.Empty) ? null : repr2;
			storedParams[9].Value = (delo == String.Empty) ? null : delo;
			storedParams[10].Value = (bulstat == String.Empty) ? null : bulstat;
			storedParams[11].Value = (ndr == String.Empty) ? null : ndr;
			storedParams[12].Value = (phone == String.Empty) ? null : phone;
			storedParams[13].Value = (webpage == String.Empty) ? null : webpage;
			storedParams[14].Value = (phone1 == String.Empty) ? null : phone1;
			storedParams[15].Value = (email1 == String.Empty) ? null : email1;
			storedParams[16].Value = (phone2 == String.Empty) ? null : phone2;
			storedParams[17].Value = (email2 == String.Empty) ? null : email2;
			storedParams[18].Value = clientTypeID;
			storedParams[19].Value = (accountName == String.Empty) ? null : accountName;
			storedParams[20].Value = (accountPass == String.Empty) ? null : accountPass;
			storedParams[22].Value = (sNameEN == String.Empty) ? null : sNameEN;
			storedParams[23].Value = (sReprEN == String.Empty) ? null : sReprEN;

			SqlHelper.ExecuteNonQuery(DBManager.GetConnectionString(), CommandType.StoredProcedure, spName, storedParams);

			return (int)storedParams[18/*21*/].Value;
		}

		public static SqlDataReader SelectClient(int clientID)
		{
			string spName = "ClientsSelProc";

			SqlParameter[] storedParams = SqlHelperParameterCache.GetSpParameterSet(DBManager.GetConnectionString(), spName);
			storedParams[0].Value = clientID;

			return SqlHelper.ExecuteReader(DBManager.GetConnectionString(), CommandType.StoredProcedure, spName, storedParams);
		}

		public static void InactiveClient(int clientID)
		{
			string spName = "ClientsInactiveProc";

			SqlParameter[] storedParams = SqlHelperParameterCache.GetSpParameterSet(DBManager.GetConnectionString(), spName);
			storedParams[0].Value = clientID;

			SqlHelper.ExecuteNonQuery(DBManager.GetConnectionString(), CommandType.StoredProcedure, spName, storedParams);
		}

	}
}
