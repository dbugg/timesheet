using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using System.Windows.Forms;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using System.Collections.Specialized;
using Asa.Timesheet.Data;
using Asa.Timesheet.Data.Entities;
using System.Web.Mail;
using log4net;
using log4net.Config;
using Asa.Timesheet.Data.Util;
using System.IO;
using GrapeCity.ActiveReports.Export.Pdf.Section;
namespace Asa.Timesheet.Windows.Mail
{
	
	public class WorkedHoursInfo
	{
		int _totalMinutes = 0;
		decimal _totalSalary = 0;
		int _totalMinutesWT = 0;
		int _totalMinutesNWT = 0;
		int _totalMinutesWeekend = 0;
		int _totalWorkDays;
		int _totalNonWorkDays;
		int _missingHours=0;
		int _totalLates;
		int _totalMinutesLate;

		int _daysNO = 0;
		string _daysNOString = String.Empty;
		int _daysPO;
		string _daysPOString;

		int _daysB;
		string _daysBString;

        public void SetTotals(int totalMinutesWT, int totalMinutesNWT, int missingHours, int nTotalTime)
        {
            _totalMinutesWT = totalMinutesWT;
            _totalMinutesNWT = totalMinutesNWT;
            _missingHours = missingHours;
            _totalMinutes = nTotalTime;
        }
        public WorkedHoursInfo(DataSet source)
        {
            CalculateHoursValues(source.Tables[0]);

            GetDays(source.Tables[1], out _daysPO, out _daysPOString);
            GetDays(source.Tables[2], out _daysNO, out _daysNOString);
            GetDays(source.Tables[3], out _daysB, out _daysBString);
        }
        public void SetLates(DataSet source)
        {
            int nLateCount = 0;
            int nLate = 0;
            foreach (DataRow dr in source.Tables[0].Rows)
            {
                if (dr["MinutesLate"] != DBNull.Value)
                {
                    nLate += (int)dr["MinutesLate"];
                    if ((int)dr["MinutesLate"] > 0)
                        nLateCount++;
                }

            }
            _totalLates = nLateCount;
            _totalMinutesLate = nLate;
        }
        private void CalculateHoursValues(DataTable dt)
        {
            _totalMinutes = 0;
            _totalMinutesWT = 0;
            _totalMinutesNWT = 0;
            _totalMinutesWeekend = 0;
            _totalNonWorkDays = 0;
            _totalWorkDays = 0;
            _totalSalary = 0;
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                DateTime tempDate = (DateTime)dt.Rows[i]["WorkDate"];
                int tempMinutes = (int)dt.Rows[i]["WorkedMinutes"];
                int tempMinutesNormal = (int)dt.Rows[i]["WorkedMinutesNormal"];
                int tempMinutesOverTime = (int)dt.Rows[i]["WorkedMinutesOverTime"];
                int tempMinutesWeekend = (int)dt.Rows[i]["WorkedMinutesWeekend"];
                decimal dNormal = dt.Rows[i]["AmountNormal"] == DBNull.Value ? 0 : (decimal)dt.Rows[i]["AmountNormal"];
                decimal dOver = dt.Rows[i]["AmountOverTime"] == DBNull.Value ? 0 : (decimal)dt.Rows[i]["AmountOverTime"];
                _totalSalary += dNormal + dOver;
                _totalMinutes += tempMinutes;
                _totalMinutesWT += tempMinutesNormal;
                _totalMinutesNWT += tempMinutesOverTime;
                _totalMinutesWeekend += tempMinutesWeekend;
                if ((tempDate.DayOfWeek == DayOfWeek.Saturday) || (tempDate.DayOfWeek == DayOfWeek.Sunday))
                {
                    //						_totalMinutesWeekend+= tempMinutes;
                    _totalNonWorkDays++;
                }
                else
                {
                    //						if (tempMinutes>480) 
                    //						{
                    //							_totalMinutesWT+= 480;
                    //							_totalMinutesNWT += (tempMinutes-480);	
                    //						}
                    //						else _totalMinutesWT+=tempMinutes;
                    _totalWorkDays++;
                }

            }
        }

        private void GetDays(DataTable dt, out int days, out string daysString)
        {
            days = dt.Rows.Count;
            daysString = String.Empty;

            if (days == 0) return;

            DateTime startDate = (DateTime)dt.Rows[0]["WorkDate"];
            DateTime endDate = (DateTime)dt.Rows[0]["WorkDate"];

            for (int i = 1; i < dt.Rows.Count; i++)
            {
                DateTime tempDate = (DateTime)dt.Rows[i]["WorkDate"];

                if (endDate.Date.AddDays(1) == tempDate.Date) endDate = tempDate;
                else
                {

                    if (startDate.Date == endDate.Date) daysString += startDate.ToString("dd.MM") + "; ";
                    else daysString += startDate.ToString("dd.MM") + "-" + endDate.ToString("dd.MM") + "; ";
                    startDate = tempDate; endDate = tempDate;
                }

            }
            if (startDate.Date == endDate.Date) daysString += startDate.ToString("dd.MM") + "; ";
            else daysString += startDate.ToString("dd.MM") + "-" + endDate.ToString("dd.MM") + "; ";
        }

		#region properties
		public decimal MissingHours
		{
			get
			{
				return TimeHelper.HoursFromMinutes(_missingHours);
			}
		}
		public decimal TotalHours
		{
			get
			{
				return TimeHelper.HoursFromMinutes(_totalMinutes);
			}
		}
		public decimal TotalSalary
		{
			get
			{
				return _totalSalary;
			}
		}
		public decimal TotalHoursInWorkTime
		{
			get
			{
				return TimeHelper.HoursFromMinutes(_totalMinutesWT);
			}
		}
		public string TotalHoursLate
		{
			get
			{
				return TimeHelper.HoursStringFromMinutes(_totalMinutesLate,true);
			}
		}
		public int TotalLateCount
		{
			get
			{
				return _totalLates;
			}
		}
		public decimal TotalHoursInNonWorkTime
		{
			get
			{
				return TimeHelper.HoursFromMinutes(_totalMinutesNWT);
			}
		}

		public decimal TotalHoursWeekends
		{
			get
			{
				return TimeHelper.HoursFromMinutes(_totalMinutesWeekend);
			}
		}

		public int DaysNeplatenOtpusk
		{
			get
			{
				return _daysNO;
			}
		}

		public string DaysNeplatenOtpuskString
		{
			get
			{
				return _daysNOString;
			}
		}

		public int DaysPlatenOtpusk
		{
			get
			{
				return _daysPO;
			}
		}

		public string DaysPlatenOtpuskString
		{
			get
			{
				return _daysPOString;
			}
		}

		public int DaysIll
		{
			get
			{
				return _daysB;
			}
		}

		public string DaysIllString
		{
			get
			{
				return _daysBString;
			}
		}

		public int TotalWorkDays
		{
			get
			{
				return _totalWorkDays;
			}
		}

		public int TotalNonWorkDays
		{
			get
			{
				return _totalNonWorkDays;
			}
		}



		#endregion

			
	}
	/// <summary>
	/// Summary description for Form1.
	/// </summary>
	public class MailForm : System.Windows.Forms.Form
	{
		private enum FirstTableFinancePokazateli
		{
			ProjectID = 0,
			ProjectName,
			Area,
			PaymentsNotPhase,
			PaymentsPhase,
			WorkedHours,
			WorkedSalary,
			AdminSalary,
			StaffSalary,
			LeaderSalary,
			SubcontracterSalary,
		}

		private enum ReportTableFinancePokazateli
		{
			ProjectID = 0,
			ProjectName,
			Area,
			Payments,
			PaymentArea,
			WorkedHours,
			WorkedSalary,
			AdminSalary,
			StaffSalary,
			LeaderSalary,
			SubcontracterSalary,
			TotalSalary,
			TotalIncome,
			ExpencesPerHour,
			ExpencesPerArea,
			PaymentPerHour,
			ImcomePerHour,
			

		}

		

		private static readonly ILog log = LogManager.GetLogger(typeof(MailForm));
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;

        public MailForm()
        {
            //
            // Required for Windows Form Designer support
            //
            InitializeComponent();

            //
            // TODO: Add any constructor code after InitializeComponent call
            //
        }

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                if (components != null)
                {
                    components.Dispose();
                }
            }
            base.Dispose(disposing);
        }

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
        private void InitializeComponent()
        {
            // 
            // MailForm
            // 
            this.AutoScaleBaseSize = new System.Drawing.Size(5, 13);
            this.ClientSize = new System.Drawing.Size(152, 93);
            this.Name = "MailForm";
            this.Text = "Form1";

        }
		#endregion

		/// <summary>
		/// The main entry point for the application.
		/// </summary>
        [STAThread]
        static void Main(string[] args)
        {
            DOMConfigurator.Configure();

            if (args.Length == 0)
            {
                try
                {
                    CheckAuto();
                    CheckExpenses();
                    CheckLate();
                    Arhiv();

                    SendNotificationToUsers();

                }
                catch (Exception ex)
                {
                    log.Error(ex.ToString());
                }
            }
            else
            {
                #region Check arguments

                int daysPast = 6;

                try
                {
                    daysPast = int.Parse(args[0]);
                }
                catch (Exception ex)
                {
                    log.Error("Incorrect argument.", ex);
                }
                if (daysPast < 1) log.Error("Incorrect parameter value - days past.");

                int includeToday = 1;
                try
                {
                    includeToday = int.Parse(args[1]);
                }
                catch (Exception ex)
                {
                    log.Error("Incorrect argument.", ex);
                }

                #endregion

                DateTime MinReportDate = DateTime.ParseExact(System.Configuration.ConfigurationManager.AppSettings["MinReportDate"],
                    "ddMMyyyy", System.Globalization.CultureInfo.InvariantCulture);

                DateTime startDay;
                int nDays = CalendarDAL.ExecuteDaysSelProc(1);
                if (nDays == -1)
                {
                    startDay = MinReportDate;
                }
                else
                    startDay = DateTime.Today.AddDays(-nDays);
                //DateTime.Today.AddDays(-daysPast);
                DateTime endDay = (includeToday > 0) ? DateTime.Today : DateTime.Today.AddDays(-1);

                try
                {
                    SendNotEnteredDaysToLeaders(startDay, endDay);
                }
                catch (Exception ex)
                {
                    log.Error(ex.ToString());
                }
            }
        }

		#region DB
        private static void LoadLeadersOfficersMails(out StringCollection leadersNames, out StringCollection leadersEmails)
        {
            leadersEmails = new StringCollection();
            leadersNames = new StringCollection();

            SqlDataReader reader = null;

            try
            {
                reader = UsersData.SelectLeadersOfficersEmails();
                while (reader.Read())
                {
                    leadersEmails.Add(reader.GetString(1));
                    leadersNames.Add(reader.GetString(0));

                }

            }
            catch (Exception ex)
            {
                log.Error(ex);
                leadersEmails = null;
                leadersNames = null;
            }
            finally
            {
                if (reader != null) reader.Close();
            }
        }
        private static void LoadLeadersOfficersMailsOnly(out StringCollection leadersNames, out StringCollection leadersEmails)
        {
            leadersEmails = new StringCollection();
            leadersNames = new StringCollection();

            SqlDataReader reader = null;

            try
            {
                reader = UsersData.SelectLeadersOfficersEmailsOnly();
                while (reader.Read())
                {
                    leadersEmails.Add(reader.GetString(1));
                    leadersNames.Add(reader.GetString(0));

                }

            }
            catch (Exception ex)
            {
                log.Error(ex);
                leadersEmails = null;
                leadersNames = null;
            }
            finally
            {
                if (reader != null) reader.Close();
            }
        }

        private static void LoadLeadersMails(out StringCollection leadersNames, out StringCollection leadersEmails)
        {
            leadersEmails = new StringCollection();
            leadersNames = new StringCollection();

            SqlDataReader reader = null;

            try
            {
                reader = UsersData.SelectLeadersEmails();
                while (reader.Read())
                {
                    leadersEmails.Add(reader.GetString(1));
                    leadersNames.Add(reader.GetString(0));

                }

            }
            catch (Exception ex)
            {
                log.Error(ex);
                leadersEmails = null;
                leadersNames = null;
            }
            finally
            {
                if (reader != null) reader.Close();
            }
        }

        private static void GetNotEnteredUsersList(DateTime day, out StringCollection usersNames, out StringCollection usersEmails)
        {
            usersEmails = new StringCollection();
            usersNames = new StringCollection();

            SqlDataReader reader = null;

            try
            {
                reader = Asa.Timesheet.Data.WorkTimesData.SelectUsersNotEnteredDay(day);
                while (reader.Read())
                {

                    usersNames.Add(reader.GetString(1));
                    usersEmails.Add(reader.GetString(2));
                }
            }
            catch (Exception ex)
            {
                log.Error(ex);
                usersNames = null;
                usersEmails = null;
            }
            finally
            {
                if (reader != null) reader.Close();
            }
        }
        private static void GetNotEnteredUsersList(DateTime day, out StringCollection usersIDs)
        {
            usersIDs = new StringCollection();

            SqlDataReader reader = null;

            try
            {
                reader = Asa.Timesheet.Data.WorkTimesData.SelectUsersNotEnteredDay(day);
                while (reader.Read())
                {

                    usersIDs.Add(reader.GetInt32(0).ToString());
                }
            }
            catch (Exception ex)
            {
                log.Error(ex);
                usersIDs = null;
            }
            finally
            {
                if (reader != null) reader.Close();
            }
        }

        private static void GetEnteredNextDayUsersList(DateTime day, out StringCollection usersNames, out StringCollection usersEmails)
        {
            usersEmails = new StringCollection();
            usersNames = new StringCollection();

            SqlDataReader reader = null;

            try
            {
                reader = Asa.Timesheet.Data.WorkTimesData.SelectUsersEnteredOnNextDay(day);
                while (reader.Read())
                {
                    usersNames.Add(reader.GetString(1));
                    usersEmails.Add(reader.GetString(2));
                }
            }
            catch (Exception ex)
            {
                log.Error(ex);
                usersNames = null;
                usersEmails = null;
            }
            finally
            {
                if (reader != null) reader.Close();
            }
        }
        private static ArrayList LoadUsers()
        {
            ArrayList usersList = new ArrayList();

            SqlDataReader reader = null;
            try
            {
                reader = UsersData.SelectUserNames(false, false, false, false);
                while (reader.Read())
                {
                    DaysListInfo info = new DaysListInfo(reader.GetInt32(0), reader.GetString(1), "");
                    usersList.Add(info);
                }
            }
            catch (Exception ex)
            {
                log.Error(ex);
                return null;
            }
            finally
            {
                if (reader != null) reader.Close();
            }

            return usersList;
        }

        private static ArrayList SelectEnteredDays(int userID, DateTime startDate, DateTime endDate)
        {
            ArrayList days = new ArrayList();

            SqlDataReader reader = null;
            try
            {
                reader = ReportsData.SelectEnteredDays(userID, startDate, endDate);
                while (reader.Read())
                {
                    days.Add(reader.GetDateTime(1));
                }
            }
            catch (Exception ex)
            {
                log.Error(ex);
                return null;
            }
            finally
            {
                if (reader != null) reader.Close();
            }

            return days;
        }

		#endregion
        public static void CheckAuto()
        {
            AutomobilesVector av = AutomobileDAL.LoadCollectionCheckAnnual();
            foreach (AutomobileData ad in av)
            {
                string sMail = string.Format(Resource.ResourceManager["mailAnnual"], ad.Name + " " + ad.RegNumber, TimeHelper.FormatDate(ad.AnnualCheckDate));
                SendMail(System.Configuration.ConfigurationManager.AppSettings["MeetingEmail"], "Godishen pregled avtomobil", sMail);
                if (ad.UserID > 0)
                {
                    UserInfo ui = UsersData.SelectUserByID(ad.UserID);
                    if (ui != null)
                        SendMail(ui.Mail, "Godishen pregled avtomobil", sMail);
                }
                log.Info("auto: " + sMail);
            }
            av = AutomobileDAL.LoadCollectionCheckCivil();
            foreach (AutomobileData ad in av)
            {
                string sMail = string.Format(Resource.ResourceManager["mailCivil"], TimeHelper.FormatDate(ad.CivilDate), ad.Name + " " + ad.RegNumber, ad.CivilIns);
                SendMail(System.Configuration.ConfigurationManager.AppSettings["MeetingEmail"], "Grajdanska otgovornost avtomobil", sMail);
                if (ad.UserID > 0)
                {
                    UserInfo ui = UsersData.SelectUserByID(ad.UserID);
                    if (ui != null)
                        SendMail(ui.Mail, "Grajdanska otgovornost avtomobil", sMail);
                }
                log.Info("auto: " + sMail);
            }
            av = AutomobileDAL.LoadCollectionCheckKasko();
            foreach (AutomobileData ad in av)
            {
                string sMail = string.Format(Resource.ResourceManager["mailKasko"], TimeHelper.FormatDate(ad.KaskoDate), ad.Name + " " + ad.RegNumber, ad.KaskoIns);
                SendMail(System.Configuration.ConfigurationManager.AppSettings["MeetingEmail"], "Kasko avtomobil", sMail);
                if (ad.UserID > 0)
                {
                    UserInfo ui = UsersData.SelectUserByID(ad.UserID);
                    if (ui != null)
                        SendMail(ui.Mail, "Kasko avtomobil", sMail);
                }
                log.Info("auto: " + sMail);
            }
            av = AutomobileDAL.LoadCollectionCheckGreen();
            foreach (AutomobileData ad in av)
            {
                string sMail = string.Format(Resource.ResourceManager["mailGreen"], TimeHelper.FormatDate(ad.GreencardDate), ad.Name + " " + ad.RegNumber, ad.GreenIns);
                SendMail(System.Configuration.ConfigurationManager.AppSettings["MeetingEmail"], "Zelena karta avtomobil", sMail);
                if (ad.UserID > 0)
                {
                    UserInfo ui = UsersData.SelectUserByID(ad.UserID);
                    if (ui != null)
                        SendMail(ui.Mail, "Zelena karta avtomobil", sMail);
                }
                log.Info("auto: " + sMail);
            }
            av = AutomobileDAL.LoadCollectionCheckOil();
            foreach (AutomobileData ad in av)
            {
                string sMail = string.Format(Resource.ResourceManager["mailOil"], ad.Name + " " + ad.RegNumber, TimeHelper.FormatDate(ad.NextChangeDate));
                SendMail(System.Configuration.ConfigurationManager.AppSettings["MeetingEmail"], "Smiana maslo avtomobil", sMail);
                if (ad.UserID > 0)
                {
                    UserInfo ui = UsersData.SelectUserByID(ad.UserID);
                    if (ui != null)
                        SendMail(ui.Mail, "Smiana maslo avtomobil", sMail);
                }
                log.Info("auto: " + sMail);
            }
            System.Collections.Specialized.StringCollection sc = ProjectsData.SelectProjectCheckBuildPermission();
            foreach (string s in sc)
            {
                string sMail = string.Format(Resource.ResourceManager["mailBuild"], s);
                SendMail(System.Configuration.ConfigurationManager.AppSettings["MeetingEmail"], "Razreshenie za stroej", sMail);
                log.Info("build: " + sMail);
            }
        }
        public static DataTable GetBGNTable(DataTable dt1)
        {
            DataTable dt = dt1.Copy();
            decimal fix = decimal.Parse(System.Configuration.ConfigurationManager.AppSettings["fixedBGNEUR"]);
            foreach (DataRow dr in dt.Rows)
            {
                for (int i = 0; i < dt.Columns.Count; i++)
                {
                    if (dt.Columns[i].DataType == typeof(decimal) && dt.Columns[i].ColumnName != "ProjectID"
                        && dt.Columns[i].ColumnName != "Area" && dt.Columns[i].ColumnName != "WorkedHours")
                    {
                        if (dr[i] != DBNull.Value)
                            dr[i] = ((decimal)dr[i] * fix);

                    }
                }
            }
            return dt;
        }
        public static DataTable CreateTableForFinancePokazateli(DataTable dt)
        {
            DataTable dtDefault = new DataTable();
            foreach (ReportTableFinancePokazateli s in Enum.GetValues(typeof(ReportTableFinancePokazateli)))
                if (s != ReportTableFinancePokazateli.ProjectName)
                    dtDefault.Columns.Add(s.ToString(), typeof(decimal));
                else
                    dtDefault.Columns.Add(s.ToString());

            DataTable dtReturn = dtDefault.Clone();

            try
            {
                //				foreach(ReportTableFinancePokazateliFinancePokazateli s in  Enum.GetValues(typeof(ReportTableFinancePokazateli)))
                //					if(s!=ReportTableFinancePokazateli.ProjectName)
                //						dtReturn.Columns.Add(s.ToString(),typeof(decimal));
                //					else
                //						dtReturn.Columns.Add(s.ToString());

                decimal d = 0;
                decimal d1 = 0;

                DataRow drAll = dtReturn.NewRow();
                DataRow drMinus = dtReturn.NewRow();
                DataRow drPlus = dtReturn.NewRow();

                drAll[(int)ReportTableFinancePokazateli.ProjectID] = -1;
                drAll[(int)ReportTableFinancePokazateli.ProjectName] = Resource.ResourceManager["lbTotal"];

                drPlus[(int)ReportTableFinancePokazateli.ProjectID] = -1;
                drPlus[(int)ReportTableFinancePokazateli.ProjectName] = Resource.ResourceManager["lbTotalPlus"];
                drMinus[(int)ReportTableFinancePokazateli.ProjectID] = -1;
                drMinus[(int)ReportTableFinancePokazateli.ProjectName] = Resource.ResourceManager["lbTotalMinus"];

                int dtCount = dt.Rows.Count;

                decimal dArea = 0;
                decimal dPayments = 0;
                decimal dWorkedHours = 0;
                decimal dWorkedSalary = 0;
                decimal dAdminSalary = 0;
                decimal dLeaderSalary = 0;
                decimal dStaffSalary = 0;
                decimal dSubcontracterSalary = 0;

                decimal dIncomePlus = 0;
                decimal dIncomeHourPlus = 0;
                decimal dIncomeMinus = 0;
                decimal dIncomeHourMinus = 0;
                decimal dWHPlus = 0;
                decimal dWHMinus = 0;

                foreach (DataRow dr in dt.Rows)
                {
                    //					if(dr[(int)FirstTableFinancePokazateli.WorkedHours]==DBNull.Value)
                    //						continue;
                    DataRow drNew = dtReturn.NewRow();
                    drNew[(int)ReportTableFinancePokazateli.ProjectID] = (int)dr[(int)FirstTableFinancePokazateli.ProjectID];
                    drNew[(int)ReportTableFinancePokazateli.ProjectName] = (string)dr[(int)FirstTableFinancePokazateli.ProjectName];
                    //Area
                    if (dr[(int)FirstTableFinancePokazateli.Area] != DBNull.Value)
                        d = (decimal)dr[(int)FirstTableFinancePokazateli.Area];
                    else
                        d = 0;
                    drNew[(int)ReportTableFinancePokazateli.Area] = d;
                    dArea += d;

                    //WorkedHours
                    if (dr[(int)FirstTableFinancePokazateli.WorkedHours] != DBNull.Value)
                        d = ((decimal)dr[(int)FirstTableFinancePokazateli.WorkedHours]) / 60;
                    else
                        d = 0;
                    drNew[(int)ReportTableFinancePokazateli.WorkedHours] = d;
                    dWorkedHours += d;
                    //WorkedSalary
                    if (dr[(int)FirstTableFinancePokazateli.WorkedSalary] != DBNull.Value)
                        d = (decimal)dr[(int)FirstTableFinancePokazateli.WorkedSalary];
                    else
                        d = 0;
                    drNew[(int)ReportTableFinancePokazateli.WorkedSalary] = d;
                    dWorkedSalary += d;
                    //AdminSalary
                    if (dr[(int)FirstTableFinancePokazateli.AdminSalary] != DBNull.Value)
                        d = (decimal)dr[(int)FirstTableFinancePokazateli.AdminSalary];
                    else
                        d = 0;
                    drNew[(int)ReportTableFinancePokazateli.AdminSalary] = d;

                    decimal dExpenses = 0;
                    if (dr[(int)FirstTableFinancePokazateli.LeaderSalary] != DBNull.Value)
                    {
                        decimal dSum = (decimal)dr[(int)FirstTableFinancePokazateli.LeaderSalary];
                        dLeaderSalary += dSum;
                        dExpenses += dSum;
                        drNew[(int)ReportTableFinancePokazateli.LeaderSalary] = dSum;
                    }
                    if (dr[(int)FirstTableFinancePokazateli.StaffSalary] != DBNull.Value)
                    {
                        decimal dSum = (decimal)dr[(int)FirstTableFinancePokazateli.StaffSalary];
                        dStaffSalary += dSum;
                        dExpenses += dSum;
                        drNew[(int)ReportTableFinancePokazateli.StaffSalary] = dSum;
                    }


                    dAdminSalary += d;
                    //SubcontracterSalary
                    if (dr[(int)FirstTableFinancePokazateli.SubcontracterSalary] != DBNull.Value)
                        d = (decimal)dr[(int)FirstTableFinancePokazateli.SubcontracterSalary];
                    else
                        d = 0;
                    drNew[(int)ReportTableFinancePokazateli.SubcontracterSalary] = d;
                    dSubcontracterSalary += d;
                    //Payments				
                    if (dr[(int)FirstTableFinancePokazateli.PaymentsPhase] != DBNull.Value)
                        d = (decimal)dr[(int)FirstTableFinancePokazateli.PaymentsPhase];
                    else
                        d = 0;
                    if (dr[(int)FirstTableFinancePokazateli.PaymentsNotPhase] != DBNull.Value)
                        d1 = (decimal)dr[(int)FirstTableFinancePokazateli.PaymentsNotPhase];
                    else
                        d1 = 0;
                    drNew[(int)ReportTableFinancePokazateli.Payments] = d + d1;
                    dPayments += d + d1;

                    //PaymentArea		
                    if ((decimal)drNew[(int)ReportTableFinancePokazateli.Area] != 0)
                        d = (decimal)drNew[(int)ReportTableFinancePokazateli.Payments] / (decimal)drNew[(int)ReportTableFinancePokazateli.Area];
                    else
                        d = 0;
                    drNew[(int)ReportTableFinancePokazateli.PaymentArea] = d;
                    //TotalSalary		
                    drNew[(int)ReportTableFinancePokazateli.TotalSalary] = (decimal)drNew[(int)ReportTableFinancePokazateli.WorkedSalary] + (decimal)drNew[(int)ReportTableFinancePokazateli.AdminSalary] + (decimal)drNew[(int)ReportTableFinancePokazateli.SubcontracterSalary] + dExpenses;
                    //TotalIncome		
                    decimal income = (decimal)drNew[(int)ReportTableFinancePokazateli.Payments] - (decimal)drNew[(int)ReportTableFinancePokazateli.TotalSalary];
                    drNew[(int)ReportTableFinancePokazateli.TotalIncome] = income;
                    if (income > 0)
                    {
                        dIncomePlus += income;
                        if ((decimal)drNew[(int)ReportTableFinancePokazateli.WorkedHours] != 0)
                            dWHPlus += (decimal)drNew[(int)ReportTableFinancePokazateli.WorkedHours];
                    }
                    else
                    {
                        dIncomeMinus += income;
                        if ((decimal)drNew[(int)ReportTableFinancePokazateli.WorkedHours] != 0)
                            dWHMinus += (decimal)drNew[(int)ReportTableFinancePokazateli.WorkedHours];
                    }
                    //ExpencesPerHour		
                    if ((decimal)drNew[(int)ReportTableFinancePokazateli.WorkedHours] != 0)
                        d = (decimal)drNew[(int)ReportTableFinancePokazateli.TotalSalary] / (decimal)drNew[(int)ReportTableFinancePokazateli.WorkedHours];
                    else
                        d = 0;
                    drNew[(int)ReportTableFinancePokazateli.ExpencesPerHour] = d;
                    //ExpencesPerArea		
                    if ((decimal)drNew[(int)ReportTableFinancePokazateli.Area] != 0)
                        d = (decimal)drNew[(int)ReportTableFinancePokazateli.TotalSalary] / (decimal)drNew[(int)ReportTableFinancePokazateli.Area];
                    else
                        d = 0;
                    drNew[(int)ReportTableFinancePokazateli.ExpencesPerArea] = d;
                    //PaymentPerHour		
                    if ((decimal)drNew[(int)ReportTableFinancePokazateli.WorkedHours] != 0)
                        d = (decimal)drNew[(int)ReportTableFinancePokazateli.Payments] / (decimal)drNew[(int)ReportTableFinancePokazateli.WorkedHours];
                    else
                        d = 0;
                    drNew[(int)ReportTableFinancePokazateli.PaymentPerHour] = d;
                    //ImcomePerHour		
                    decimal incomehour = (decimal)drNew[(int)ReportTableFinancePokazateli.PaymentPerHour] - (decimal)drNew[(int)ReportTableFinancePokazateli.ExpencesPerHour];
                    drNew[(int)ReportTableFinancePokazateli.ImcomePerHour] = incomehour;
                    if (incomehour > 0)
                        dIncomeHourPlus += incomehour;
                    else
                        dIncomeHourMinus += incomehour;

                    dtReturn.Rows.Add(drNew);
                }

                //				//Area
                //				drAll[(int)ReportTableFinancePokazateli.Area] = dArea;
                //				//WorkedHours
                //				drAll[(int)ReportTableFinancePokazateli.WorkedHours] = dWorkedHours;
                //				//WorkedSalary
                //				drAll[(int)ReportTableFinancePokazateli.WorkedSalary] = dWorkedSalary;
                //				//AdminSalary
                //				drAll[(int)ReportTableFinancePokazateli.AdminSalary] = dAdminSalary;
                //				//AdminSalary
                //				drAll[(int)ReportTableFinancePokazateli.StaffSalary] = dStaffSalary;
                //				//AdminSalary
                //				drAll[(int)ReportTableFinancePokazateli.LeaderSalary] = dLeaderSalary;
                //				//SubcontracterSalary
                //				drAll[(int)ReportTableFinancePokazateli.SubcontracterSalary] =dSubcontracterSalary;
                //				//Payments				
                //				drAll[(int)ReportTableFinancePokazateli.Payments] = dPayments;
                //				//PaymentArea		
                //				if((decimal)drAll[(int)ReportTableFinancePokazateli.Area] != 0)
                //					d = (decimal)drAll[(int)ReportTableFinancePokazateli.Payments]/(decimal)drAll[(int)ReportTableFinancePokazateli.Area];
                //				else
                //					d = 0;
                //				drAll[(int)ReportTableFinancePokazateli.PaymentArea] = d;
                //				//TotalSalary		
                //				drAll[(int)ReportTableFinancePokazateli.TotalSalary] = (decimal)drAll[(int)ReportTableFinancePokazateli.WorkedSalary]+(decimal)drAll[(int)ReportTableFinancePokazateli.AdminSalary]+(decimal)drAll[(int)ReportTableFinancePokazateli.SubcontracterSalary];
                //				//TotalIncome		
                //				drAll[(int)ReportTableFinancePokazateli.TotalIncome] = (decimal)drAll[(int)ReportTableFinancePokazateli.Payments]-(decimal)drAll[(int)ReportTableFinancePokazateli.TotalSalary];
                //				//ExpencesPerHour		
                //				if((decimal)drAll[(int)ReportTableFinancePokazateli.WorkedHours] != 0)
                //					d = (decimal) drAll[(int)ReportTableFinancePokazateli.TotalSalary]/(decimal)drAll[(int)ReportTableFinancePokazateli.WorkedHours];
                //				else
                //					d = 0;
                //				drAll[(int)ReportTableFinancePokazateli.ExpencesPerHour] =d;
                //				//PaymentPerHour		
                //				if((decimal)drAll[(int)ReportTableFinancePokazateli.WorkedHours] != 0)
                //					d = (decimal) drAll[(int)ReportTableFinancePokazateli.Payments]/(decimal)drAll[(int)ReportTableFinancePokazateli.WorkedHours];
                //				else
                //					d = 0;
                //				drAll[(int)ReportTableFinancePokazateli.PaymentPerHour] = d;
                //				//ImcomePerHour		
                //				drAll[(int)ReportTableFinancePokazateli.ImcomePerHour] = (decimal)drAll[(int)ReportTableFinancePokazateli.PaymentPerHour] - (decimal)drAll[(int)ReportTableFinancePokazateli.ExpencesPerHour];

                for (int i = 0; i < dtReturn.Columns.Count; i++)

                    if (i != (int)ReportTableFinancePokazateli.ProjectID && i != (int)ReportTableFinancePokazateli.ProjectName)
                    {
                        decimal dVal = 0;
                        foreach (DataRow dr in dtReturn.Rows)
                        {
                            if (dr[i] != DBNull.Value)
                                dVal += (decimal)dr[i];
                        }
                        drAll[i] = dVal;
                    }
                if ((decimal)drAll[(int)ReportTableFinancePokazateli.WorkedHours] != 0)
                {
                    drAll[(int)ReportTableFinancePokazateli.PaymentPerHour] = (decimal)drAll[(int)ReportTableFinancePokazateli.Payments] / (decimal)drAll[(int)ReportTableFinancePokazateli.WorkedHours];
                    drAll[(int)ReportTableFinancePokazateli.ExpencesPerHour] = (decimal)drAll[(int)ReportTableFinancePokazateli.TotalSalary] / (decimal)drAll[(int)ReportTableFinancePokazateli.WorkedHours];
                    drAll[(int)ReportTableFinancePokazateli.ImcomePerHour] = (decimal)drAll[(int)ReportTableFinancePokazateli.TotalIncome] / (decimal)drAll[(int)ReportTableFinancePokazateli.WorkedHours];
                    drMinus[(int)ReportTableFinancePokazateli.ImcomePerHour] = dIncomeHourMinus;
                    drPlus[(int)ReportTableFinancePokazateli.ImcomePerHour] = dIncomeHourPlus;
                }
                else
                {
                    drAll[(int)ReportTableFinancePokazateli.PaymentPerHour] = 0;
                    drAll[(int)ReportTableFinancePokazateli.ExpencesPerHour] = 0;
                    drAll[(int)ReportTableFinancePokazateli.ImcomePerHour] = 0;
                }
                if ((decimal)drAll[(int)ReportTableFinancePokazateli.Area] != 0)
                    drAll[(int)ReportTableFinancePokazateli.PaymentArea] = (decimal)drAll[(int)ReportTableFinancePokazateli.Payments] / (decimal)drAll[(int)ReportTableFinancePokazateli.Area];
                else
                    drAll[(int)ReportTableFinancePokazateli.PaymentArea] = 0;
                if (dWHPlus > 0)
                    drPlus[(int)ReportTableFinancePokazateli.ImcomePerHour] = dIncomePlus / dWHPlus;
                else
                    drPlus[(int)ReportTableFinancePokazateli.ImcomePerHour] = 0;
                if (dWHMinus > 0)
                    drMinus[(int)ReportTableFinancePokazateli.ImcomePerHour] = dIncomeMinus / dWHMinus;
                else
                    drMinus[(int)ReportTableFinancePokazateli.ImcomePerHour] = 0;


                drMinus[(int)ReportTableFinancePokazateli.TotalIncome] = dIncomeMinus;
                drPlus[(int)ReportTableFinancePokazateli.TotalIncome] = dIncomePlus;
                dtReturn.Rows.Add(drMinus);
                dtReturn.Rows.Add(drPlus);
                dtReturn.Rows.Add(drAll);

                return dtReturn;
            }
            catch
            {
                return dtDefault;
            }
        }		
		//30,50, 70,90,100
		//1,2,    3, 4, 5
        private static void CheckExpenses()
        {
            ProjectExpensesVector pev = ProjectExpenseDAL.LoadCollection();
            DateTime dtStart = Constants.DateMax;
            DateTime dtEnd = Constants.DateMax;
            foreach (ProjectExpenseData ped in pev)
            {
                if (ped.Estimate > 0 && ped.PercentExpenses > 0)
                {


                    DataSet ds = ReportsData.ExecuteReportsFinancePokazateli(ped.ProjectID, dtStart, dtEnd, -1, -1, true);
                    DataTable dt1 = CreateTableForFinancePokazateli(ds.Tables[0]);
                    DataView dv = new DataView(dt1);

                    DataView dvBGN = new DataView(GetBGNTable(dt1));



                    decimal dTotal = 0;
                    if (dvBGN.Count > 0)
                    {
                        if (dvBGN[0]["WorkedSalary"] != DBNull.Value)
                            dTotal += (decimal)dvBGN[0]["WorkedSalary"];
                        if (dvBGN[0]["AdminSalary"] != DBNull.Value)
                            dTotal += (decimal)dvBGN[0]["AdminSalary"];
                        if (dvBGN[0]["LeaderSalary"] != DBNull.Value)
                            dTotal += (decimal)dvBGN[0]["LeaderSalary"];
                        if (dvBGN[0]["StaffSalary"] != DBNull.Value)
                            dTotal += (decimal)dvBGN[0]["StaffSalary"];
                    }

                    decimal dCurrent = dTotal;//calculate!
                    decimal dPrev = ped.CurrentExpenes;
                    if (decimal.Round(dCurrent, 2) != decimal.Round(dPrev, 2))//save and send emails
                    {
                        ped.CurrentExpenes = decimal.Round(dCurrent, 2);
                        ProjectExpenseDAL.Save(ped);
                        decimal dEstimateR = ped.Estimate * ped.PercentExpenses / 100;

                        if (dCurrent > dEstimateR && !ped.Email5)
                        {
                            SendEmail("100", ped.ProjectID, dCurrent);
                            ped.Email1 = true;
                            ped.Email2 = true;
                            ped.Email3 = true;
                            ped.Email4 = true;
                            ped.Email5 = true;
                            ProjectExpenseDAL.Save(ped);
                            continue;
                        }
                        if (dCurrent > dEstimateR * .9m && !ped.Email4)
                        {
                            SendEmail("90", ped.ProjectID, dCurrent);
                            ped.Email1 = true;
                            ped.Email2 = true;
                            ped.Email3 = true;
                            ped.Email4 = true;
                            ProjectExpenseDAL.Save(ped);
                            continue;
                        }
                        if (dCurrent > dEstimateR * .7m && !ped.Email3)
                        {
                            SendEmail("70", ped.ProjectID, dCurrent);
                            ped.Email1 = true;
                            ped.Email2 = true;
                            ped.Email3 = true;

                            ProjectExpenseDAL.Save(ped);
                            continue;
                        }
                        if (dCurrent > dEstimateR * .5m && !ped.Email2)
                        {
                            SendEmail("50", ped.ProjectID, dCurrent);
                            ped.Email1 = true;
                            ped.Email2 = true;
                            ProjectExpenseDAL.Save(ped);
                            continue;
                        }
                        if (dCurrent > dEstimateR * .3m && !ped.Email1)
                        {
                            SendEmail("30", ped.ProjectID, dCurrent);
                            ped.Email1 = true;
                            ProjectExpenseDAL.Save(ped);
                            continue;

                        }
                    }

                }
            }
        }
        private static void SendEmail(string percent, int ProjectID, decimal dExpense)
        {
            StringCollection leadersMails = new StringCollection();
            leadersMails.Add(ConfigurationSettings.AppSettings["LeaderEmail"]);

            int nProjectManagerID = -1;
            string sProjectName = "";
            SqlDataReader reader = null;

            try
            {
                reader = ProjectsData.SelectProject(ProjectID);
                if (reader.Read())
                {

                    sProjectName = reader.GetString(1);
                    nProjectManagerID = reader.IsDBNull(11) ? 0 : reader.GetInt32(11);

                }
            }
            catch (Exception ex)
            {
                log.Info(ex);
                return;
            }

            finally
            {
                if (reader != null) reader.Close();
            }

            reader = null;
            try
            {
                reader = UsersData.SelectUser(nProjectManagerID);
                reader.Read();


                //leadersMails.Add( reader.GetString(6));

            }
            catch (Exception ex)
            {
                log.Error(ex);
                return;
            }
            finally
            {
                if (reader != null) reader.Close();
            }


            string subject = String.Format(Resource.ResourceManager["mailExpensesSubject"],
                sProjectName, percent);

            System.Text.StringBuilder sbMailBody = new System.Text.StringBuilder();



            sbMailBody.Append(String.Format(Resource.ResourceManager["mailExpenses"], sProjectName, percent, decimal.Round(dExpense, 2)));
            sbMailBody.Append(String.Concat("<br><br><b>", DateTime.Today.ToString("d"), "</b>"));


            // send mails
            //log.Info("Leaders notification "+DateTime.Today.ToString("d"));
            for (int i = 0; i < leadersMails.Count; i++)
            {
                SendMail(leadersMails[i], subject, sbMailBody.ToString());
                log.Info(sbMailBody.ToString());

            }

        }
        private static void CheckLate()
        {
            if (DateTime.Now.Date.Day == int.Parse(ConfigurationSettings.AppSettings["DaySendLate"]))
            {

                DataTable dt = GetUsers();
                StringCollection sc = new StringCollection();
                foreach (DataRow dr in dt.Rows)
                {
                    sc.Add(SaveLateReport((int)dr["UserID"], (string)dr["FullName"]));
                }

                DateTime dtPrev = DateTime.Now.AddMonths(-1);
                SendMailLate(ConfigurationSettings.AppSettings["SendLate"], sc, dtPrev.Month.ToString() + "/" + dtPrev.Year.ToString());

            }
        }
        private static DataSet GetLates(int userID, out WorkedHoursInfo whi, DateTime startDate, DateTime endDate)
        {
            whi = null;
            DataSet dsWorkedHours = ReportsData.SelectWorkTimesHoursSummary(userID, -1, -1, -1,
                startDate, endDate, true, true, true, false,
                false);


            if (dsWorkedHours != null)
                whi = new WorkedHoursInfo(dsWorkedHours);

            DataSet dss = ReportsData.SelectReportsWorkTimesLate(-1, -1, -1,
                startDate, endDate, true, true, true, false, userID);


            DataSet dssOverTime = ReportsData.SelectWorkTimesHoursSummaryOfUsersForOverTimeByUsr(-1, -1, -1,
                startDate, endDate, true, true, true, false, 1, userID);
            if (dssOverTime.Tables.Count > 0 && dssOverTime.Tables[0].Rows.Count > 0)
            {
                int nTotalTime = (int)dssOverTime.Tables[0].Rows[0]["TotalTime"];


                int nOverTime = (int)dssOverTime.Tables[0].Rows[0]["OverTime"];

                int nUnderTime = -(int)dssOverTime.Tables[0].Rows[0]["UnderTime"];

                int nWorkHours = (nTotalTime - nOverTime);
                whi.SetTotals(nWorkHours, nOverTime, nUnderTime, nTotalTime);
            }

            if (whi != null)
            {
                whi.SetLates(dss);

            }
            return dss;

        }
        public static void ConvertMemoryStreamToFileStream(MemoryStream ms, String newFilePath)
        {
            //				using (FileStream fs = File.OpenWrite(newFilePath))            
            //					 {                ms.WriteTo(fs);            }        


            FileStream fs = File.OpenWrite(newFilePath);


            /* could replace with GetBuffer() if you don't mind the padding, or you
            could set Capacity of ms to Position to crop the padding out of the
            buffer.*/
            fs.Write(ms.GetBuffer(), 0, (int)ms.Length);


            fs.Close();
        }

//		public static void ConvertMemoryStreamToFileStream(MemoryStream ms, String newFilePath)        
//		{            
//			using (FileStream fs = File.OpenWrite(newFilePath))            
//			{                
//				const int blockSize = 1024;               
//				byte[] buffer = new byte[blockSize];                
//				int numBytes;                
//				while ((numBytes = ms.Read(buffer, 0, blockSize)) > 0)                
//				{                    
//					fs.Write(buffer, 0, numBytes);                
//				}            
//			}        
//		}
        private static string SaveLateReport(int userID, string sName)
        {

            GrapeCity.ActiveReports.SectionReport report = null;

            // Summary

            WorkedHoursInfo whi = null;
            DateTime dtPrev = DateTime.Now.AddMonths(-1);
            DateTime startDate = new DateTime(dtPrev.Year, dtPrev.Month, 1);
            DateTime endDate = startDate.AddMonths(1).AddDays(-1);


            HeaderInfo hi = new HeaderInfo();
            hi.User = sName;
            hi.StartDate = startDate;
            hi.EndDate = endDate;




            DataSet ds1 = GetLates(userID, out whi, startDate, endDate);

            UserInfo uiA;
            uiA = UsersData.SelectUserByID(int.Parse(ConfigurationSettings.AppSettings["EntererByAccountant"]));

            UserInfo ui = UsersData.SelectUserByID(userID);
            if (ui != null)
                report = new LateReport(hi, true, uiA.FullName, ds1, whi, ui.IsTraine);

            PdfExport pdf = new PdfExport();
            pdf.NeverEmbedFonts = "";

            report.Run();
            using (System.IO.MemoryStream memoryFile = new System.IO.MemoryStream())
            {
                pdf.Export(report.Document, memoryFile);



                string sPath = ConfigurationSettings.AppSettings["LateFolder"] + ui.Account + ".pdf";
                ConvertMemoryStreamToFileStream(memoryFile, sPath);
                return sPath;
            }

        }

        private static DataTable GetUsers()
        {
            SqlDataReader reader = null;
            try
            {


                reader = UsersData.SelectUserNames(false, false);
                return CommonDAL.CreateTableFromReader(reader);
            }
            catch (Exception ex)
            {
                log.Info(ex);
                return null;
            }
            finally
            {
                if (reader != null) reader.Close();
            }
        }
        private static void CheckAuthors()
        {



            SqlDataReader reader = null;

            try
            {
                reader = ProjectsData.SelectProjects(0, (int)ProjectsData.ProjectsByStatus.Active, "", -1/*,false*/, true);
                while (reader.Read())
                {
                    string name = reader.GetString(1);
                    string admname = reader.IsDBNull(4) ? String.Empty : reader.GetString(4);
                    string code = reader.IsDBNull(2) ? String.Empty : reader.GetString(2);

                    int nID = reader.GetInt32(0);
                    decimal dAuthors = 0;
                    if (!reader.IsDBNull(22))
                        dAuthors = reader.GetDecimal(22);
                    AuthorsInvoice ai = AuthorsInvoice.None;
                    if (!reader.IsDBNull(23))
                        ai = (AuthorsInvoice)reader.GetInt32(23);
                    DateTime dtToday = DateTime.Today.AddDays(int.Parse(ConfigurationSettings.AppSettings["AfterDays"]));
                    if (ai != AuthorsInvoice.None && dAuthors > 0)
                    {
                        ///Check for hours in the last n monts
                        //int nMinutes=

                        PaymentsVector pv = PaymentDAL.LoadCollection("PaymentsListByAuthorProc", SQLParms.CreatePaymentsListByAuthorsProc(nID));
                        int nMonths = (int)ai;
                        DateTime dtStart = new DateTime();

                        if (pv == null || pv.Count == 0)
                        {
                            dtStart = dtToday.AddMonths(-nMonths);
                        }
                        else
                        {
                            DateTime dateMax = new DateTime();
                            foreach (PaymentData pd in pv)
                                if (pd.AuthorsEndDate > dateMax)
                                    dateMax = pd.AuthorsEndDate;
                            if (dateMax != new DateTime())
                            {
                                dateMax = dateMax.AddDays(1);
                                if (dtToday.AddMonths(-nMonths) > dateMax)
                                {
                                    dtStart = dateMax;
                                }

                            }
                            else
                            {
                                dtStart = dtToday.AddMonths(-nMonths);
                            }

                        }
                        if (dtStart != new DateTime())
                        {
                            int minutes = WorkTimesData.ExecuteWorkTimesSelAuthorsProc(int.Parse(ConfigurationSettings.AppSettings["AN"]),
                                dtStart, dtToday, nID);
                            if (minutes > 0)
                            {
                                decimal dTotal = minutes * dAuthors / 60;
                                PaymentData pd = new PaymentData(-1, -1, -1, 0, dTotal, false, Constants.DateMax, 0, true, -1);
                                pd.AuthorsID = nID;
                                pd.AuthorsStartDate = dtStart;
                                pd.AuthorsEndDate = dtToday;
                                PaymentDAL.Save(pd);

                                StringCollection leadersMails = null, leadersNames = null;
                                LoadLeadersOfficersMails(out leadersNames, out leadersMails);

                                if (leadersMails == null) return;
                                if (leadersMails.Count == 0)
                                {
                                    log.Info("No leaders emails found?");
                                    return;
                                }



                                string subject = String.Format(Resource.ResourceManager["MailAuthorsSubject"],
                                    code + " " + name + " " + admname);

                                System.Text.StringBuilder sbMailBody = new System.Text.StringBuilder();


                                sbMailBody.Append(string.Format(Resource.ResourceManager["MailAuthorsBody"], code + " " + name + " " + admname,
dtStart.ToShortDateString() + " - " + dtToday.ToShortDateString(), decimal.Round(dAuthors, 0) + " EUR",
Asa.Timesheet.Data.Util.TimeHelper.HoursStringFromMinutes(minutes, false), decimal.Round(dTotal, 2)));


                                sbMailBody.Append(String.Concat("<br><br><b>", DateTime.Today.ToString("d"), "</b>"));

                                // send mails
                                //log.Info("Leaders notification "+DateTime.Today.ToString("d"));
                                for (int i = 0; i < leadersMails.Count; i++)
                                {
                                    SendMail(leadersMails[i], subject, sbMailBody.ToString());
                                }
                            }
                        }

                    }

                }
            }
            catch (Exception ex)
            {
                log.Info(ex);

                return;
            }
            finally
            {
                if (reader != null) reader.Close();
            }


        }
		#region Send Mails

        private static void Arhiv()
        {
            CommonDAL.Execute_Arhiv();
        }
//		private static void SendCompleteProjects()
//		{
//			DataView dv = MailDAL.ExecuteCompleteProjectsListProc();
//			foreach(DataRowView drv in dv)
//			{
//				string name=(string) drv["AdministrativeName"];
//				DateTime dt = (DateTime)drv["EndDateContract"];
//				System.Text.StringBuilder sbMailBody = new System.Text.StringBuilder();
//				string subject=Resource.ResourceManager["complete_mail_subject"];
//				string body=Resource.ResourceManager["complete_mail_body"];
//				sbMailBody.Append(string.Format(body,name,dt.ToString("dd/MM/yyyy")));
//				
//				
//				StringCollection sc = LoadEmails();
//				foreach(string s in sc)
//				{
//					SendMail(s, subject, sbMailBody.ToString());
//				}
//			}
//		}
//		private static StringCollection LoadEmails()
//		{
//			StringCollection sc = new StringCollection();
//
//			SqlDataReader reader = null;
//
//			try
//			{
//				reader = ReportsData.SelectAllEmails(false);
//				while (reader.Read())
//				{
//					string email = reader.GetString(1);
//					
//					sc.Add(email); 
//				}
//			}
//			catch (Exception ex)
//			{
//				log.Error(ex);
//			}
//			finally
//			{
//				if (reader!=null) reader.Close();
//			}
//
//			return sc;
//		}
        private static void SendNotificationToUsers()
        {
            DateTime day = DateTime.Today.AddDays(-1);
            StringCollection usersNames, usersEmails;
            if (day.DayOfWeek != DayOfWeek.Saturday &&
                day.DayOfWeek != DayOfWeek.Sunday &&
                !CalendarDAL.IsHoliday(day))
            {

                GetNotEnteredUsersList(day, out usersNames, out usersEmails);

                // error or no users without hours
                if ((usersNames == null) || (usersNames.Count == 0)) return;

                System.Text.StringBuilder mailBody = new System.Text.StringBuilder();
                mailBody.Append(String.Concat(Resource.ResourceManager["mail_UsersNotification"], day.ToString("ddd, dd MMM yyyy"), "."));

                string subject = String.Concat(Resource.ResourceManager["mail_UsersNotificationSubject"], day.ToString("ddd, dd MMM yyyy"), ".");

                //log.Info("Users notification: "+DateTime.Today.ToString("d"));
                for (int i = 0; i < usersEmails.Count; i++)
                {
                    SendMail(usersEmails[i], subject, mailBody.ToString());
                }
            }
            StringCollection leadersMails = null, leadersNames = null;
            LoadLeadersOfficersMailsOnly(out leadersNames, out leadersMails);

            if (leadersMails == null) return;
            if (leadersMails.Count == 0)
            {
                log.Info("No leaders emails found?");
                return;
            }
            GetEnteredNextDayUsersList(day, out usersNames, out usersEmails);
            if (usersNames.Count > 0)
            {
                System.Text.StringBuilder sbMailBody = new System.Text.StringBuilder();
                sbMailBody.Append(Resource.ResourceManager["mailNextDay"]);
                sbMailBody.Append(" ");
                sbMailBody.Append(DateTime.Today.AddDays(-1).ToString("d"));
                sbMailBody.Append(": ");
                for (int i = 0; i < usersNames.Count; i++)
                {



                    sbMailBody.Append(usersNames[i] + ",");


                }
                sbMailBody.Append(String.Concat("<br><br><b>", DateTime.Today.ToString("d"), "</b>"));

                for (int i = 0; i < leadersMails.Count; i++)
                {
                    SendMail(leadersMails[i], Resource.ResourceManager["mailNextDaySubject"], sbMailBody.ToString());

                }
            }
            StringCollection usersIDs = null;
            DateTime dtTwoDaysAgo = day.AddDays(-1);

            if (dtTwoDaysAgo.DayOfWeek == DayOfWeek.Saturday ||
        dtTwoDaysAgo.DayOfWeek == DayOfWeek.Sunday ||
        CalendarDAL.IsHoliday(dtTwoDaysAgo)) return;
            GetNotEnteredUsersList(dtTwoDaysAgo, out  usersIDs);
            for (int i = 0; i < usersIDs.Count; i++)
            {



                WorkTimesData.InsertWorkTime(null, dtTwoDaysAgo, int.Parse(usersIDs[i]), int.Parse(ConfigurationSettings.AppSettings["Platen"]),
                    0, int.Parse(ConfigurationSettings.AppSettings["HoursGrid_DefaultStartHourID"]), int.Parse(ConfigurationSettings.AppSettings["HoursGrid_DefaultEndHourID"]),
                    Resource.ResourceManager["enteredBySystem"], Resource.ResourceManager["enteredBySystem"], DateTime.Now.Date, int.Parse(ConfigurationSettings.AppSettings["EntererByAccountant"]), 0);




            }
        }

        private static void SendNotEnteredDaysToLeaders(DateTime startDay, DateTime endDay)
        {
            StringCollection leadersMails = null, leadersNames = null;
            LoadLeadersMails(out leadersNames, out leadersMails);

            if (leadersMails == null) return;
            if (leadersMails.Count == 0)
            {
                log.Info("No leaders emails found?");
                return;
            }

            ArrayList usersList = GetUsersInfoList(startDay, endDay);

            string subject = String.Concat(Resource.ResourceManager["mail_UsersNotEnteredPeriodSubject"], " ",
                startDay.ToString("d"), " ", Resource.ResourceManager["reports_PeriodTo"], " ", endDay.ToString("d"), ".");

            System.Text.StringBuilder sbMailBody = new System.Text.StringBuilder();

            for (int i = 0; i < usersList.Count; i++)
            {
                DaysListInfo info = (DaysListInfo)usersList[i];

                if (info.DaysString != String.Empty)
                {
                    sbMailBody.Append("<tr><td valign=\"top\" nowrap><b>" + info.UserName + "&nbsp;</b></td><td>" + info.DaysString + "</td></tr>");
                }

            }

            if (sbMailBody.Length == 0)
            {
                sbMailBody.Append(String.Concat("<B>", Resource.ResourceManager["mail_NoUsersWithEmptyDays"], "</B>"));
            }
            else
            {
                sbMailBody.Insert(0, String.Concat("<B>", Resource.ResourceManager["mail_UsersNotEnteredPeriod"], " ",
                                    startDay.ToString("d"), " ", Resource.ResourceManager["reports_PeriodTo"], " ",
                                    endDay.ToString("d"), " :", "</B>",
                                    "<br><br><table border=0 cellspacing=0>"));
                sbMailBody.Append("</table>");
            }


            sbMailBody.Append(String.Concat("<br><br><b>", DateTime.Today.ToString("d"), "</b>"));

            // send mails
            //log.Info("Leaders notification "+DateTime.Today.ToString("d"));
            for (int i = 0; i < leadersMails.Count; i++)
            {
                SendMail(leadersMails[i], subject, sbMailBody.ToString());
            }

        }
        public static void SendMailLate(string sTo, StringCollection sc, string date)
        {
            try
            {
                MailMessage Message = new MailMessage();
                Message.To = GetChangedA(sTo);
                Message.From = ConfigurationSettings.AppSettings["MailFrom"]; ;
                Message.BodyEncoding = System.Text.Encoding.UTF8;


                Message.Subject = Resource.ResourceManager["LateSubject"] + date;
                Message.Body = Resource.ResourceManager["LateSubject"] + date;
                Message.BodyFormat = System.Web.Mail.MailFormat.Html;

                foreach (string s in sc)
                {
                    Message.Attachments.Add(new MailAttachment(s));
                }
                string sAuthMail = ConfigurationSettings.AppSettings["AuthMail"];
                if (sAuthMail != "")
                {
                    Message.Fields.Add("http://schemas.microsoft.com/cdo/configuration/sendusing", "2");	//basic authentication
                    Message.Fields.Add("http://schemas.microsoft.com/cdo/configuration/smtpauthenticate", "1");	//basic authentication
                    Message.Fields.Add("http://schemas.microsoft.com/cdo/configuration/sendusername", sAuthMail); //set your username here
                    Message.Fields.Add("http://schemas.microsoft.com/cdo/configuration/sendpassword", ConfigurationSettings.AppSettings["AuthPass"]);	//set your password here
                }

                SmtpMail.SmtpServer = ConfigurationSettings.AppSettings["SMTP"];
                SmtpMail.Send(Message);
                foreach (string s in sc)
                {
                    File.Delete(s);
                }
            }
            catch (Exception ex)
            {
                log.Error(ex);
            }
        }
        private static string Replace(string toMails, string sChangeFrom, string sChangeTo)
        {
            int index = 0;
            while (index != -1)
            {
                index = toMails.IndexOf(sChangeFrom, index);
                if (index == 0)
                {
                    toMails = sChangeTo + toMails.Substring(sChangeFrom.Length + 1);
                    index++;
                }
                else if (index > 0)
                {
                    if (toMails[index - 1] != '.')
                    {
                        toMails = toMails.Substring(0, index) + sChangeTo + toMails.Substring(index + sChangeFrom.Length);
                    }
                    index++;
                }

            }
            return toMails;
        }
        public static string GetChangedA(string toMails)
        {
            string sChangeFrom = ConfigurationSettings.AppSettings["ChangeMail"];
            string sChangeTo = ConfigurationSettings.AppSettings["LowAccMail"];
            return Replace(toMails, sChangeFrom, sChangeTo);
            //return toMails.Replace(sChangeFrom,sChangeTo);
        }
        public static string GetChanged(string toMails)
        {
            string sChangeFrom = ConfigurationSettings.AppSettings["ChangeMail"];
            string sChangeTo = ConfigurationSettings.AppSettings["MeetingEmail"];
            return Replace(toMails, sChangeFrom, sChangeTo);
            //return toMails.Replace(sChangeFrom,sChangeTo);
        }
        public static void SendMail(string sTo, string sSubj, string sBody)
        {
            try
            {
                MailMessage Message = new MailMessage();
                Message.To = GetChangedA(sTo);
                Message.From = ConfigurationSettings.AppSettings["MailFrom"]; ;
                Message.BodyEncoding = System.Text.Encoding.UTF8;


                Message.Subject = sSubj;
                Message.Body = sBody;
                Message.BodyFormat = System.Web.Mail.MailFormat.Html;

                string sAuthMail = ConfigurationSettings.AppSettings["AuthMail"];
                if (sAuthMail != "")
                {
                    Message.Fields.Add("http://schemas.microsoft.com/cdo/configuration/sendusing", "2");	//basic authentication
                    Message.Fields.Add("http://schemas.microsoft.com/cdo/configuration/smtpauthenticate", "1");	//basic authentication
                    Message.Fields.Add("http://schemas.microsoft.com/cdo/configuration/sendusername", sAuthMail); //set your username here
                    Message.Fields.Add("http://schemas.microsoft.com/cdo/configuration/sendpassword", ConfigurationSettings.AppSettings["AuthPass"]);	//set your password here
                }

                SmtpMail.SmtpServer = ConfigurationSettings.AppSettings["SMTP"];
                SmtpMail.Send(Message);

            }
            catch (Exception ex)
            {
                log.Error(ex);
            }
        }

		#endregion

		#region users not entered days 

        private static ArrayList GetUsersInfoList(DateTime startDay, DateTime endDay)
        {
            ArrayList usersList = LoadUsers();
            if (usersList == null) return null;

            ArrayList enteredDays = null;

            for (int i = 0; i < usersList.Count; i++)
            {
                DaysListInfo info = (DaysListInfo)usersList[i];

                enteredDays = SelectEnteredDays(info.UserID, startDay, endDay);

                if (enteredDays == null) // error loading user days from DB
                {

                }
                else
                {
                    ArrayList notEnteredDays = TimeHelper.GetNotEnteredDaysList(startDay, endDay, enteredDays, true);
                    info.DaysString = DaysListInfo.GetDaysString(notEnteredDays, false, "dd.MM.yy");
                }


            }
            return usersList;
        }

//		private static ArrayList GetNotEnteredDaysList(DateTime startDay, DateTime endDay, 
//			ArrayList enteredDays, bool includeWeekends)
//		{
//			DateTime dt = startDay;
//			ArrayList notEnteredDays = new ArrayList();
//			
//			while (dt<=endDay)
//			{
//				if (enteredDays.BinarySearch(dt) < 0) 
//				{
//					if (includeWeekends) notEnteredDays.Add(dt);
//					else
//						if ((dt.DayOfWeek!=DayOfWeek.Saturday) && (dt.DayOfWeek!=DayOfWeek.Sunday)) notEnteredDays.Add(dt);
//				}
//
//				dt = dt.AddDays(1);
//			}
//
//			return notEnteredDays;
//		}

		#endregion

	}
	
}
