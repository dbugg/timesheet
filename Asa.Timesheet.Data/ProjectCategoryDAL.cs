using System;
using System.Data;
using System.Data.SqlClient; 
using System.Collections;
using Microsoft.ApplicationBlocks.Data;

	
namespace Asa.Timesheet.Data
{
	/// <summary>
	/// Summary description for MailingDAL.
	/// </summary>
	public class ProjectCategoryDAL
	{
		/// <summary>
		/// Loads a ProjectCategoryData object from the database using the given CategoryID
		/// </summary>
		/// <param name="CategoryID">The primary key value</param>
		/// <returns>A ProjectCategoryData object</returns>
		public static ProjectCategoryData Load(int CategoryID)
		{
			SqlParameter[] parameterValues = new SqlParameter[] { new SqlParameter("@CategoryID", CategoryID) };
			using (SqlDataReader reader = SqlHelper.ExecuteReader(CommonDAL.ConnectionString, CommandType.StoredProcedure, "ProjectCategoriesSelProc", parameterValues))
			{
				if (reader.Read())
				{
					ProjectCategoryData result = new ProjectCategoryData();
					LoadFromReader(reader,result);
					return result;
				}
				else
					return null;
			}
		}

		/// <summary>
		/// Loads a ProjectCategoryData object from the database using the given where clause
		/// </summary>
		/// <param name="whereClause">The filter expression for the query</param>
		/// <returns>A ProjectCategoryData object</returns>
		public static ProjectCategoryData Load(string whereClause)
		{
			SqlParameter[] parameterValues = new SqlParameter[] { new SqlParameter("@WhereClause", whereClause) };
			using (SqlDataReader reader = SqlHelper.ExecuteReader(CommonDAL.ConnectionString, CommandType.StoredProcedure, "ProjectCategoriesListProc", parameterValues))
			{
				if (reader.Read())
				{
					ProjectCategoryData result = new ProjectCategoryData();
					LoadFromReader(reader,result);
					return result;
				}
				else
					return null;
			}
		}
		public static int Save( ProjectCategoryData source)
		{
			if (source.CategoryID == -1)
				return Insert(source);
			else
				return Update(source);	
		}

		public static void Delete( ProjectCategoryData source)
		{
			Delete(source.CategoryID);
		}
		/// <summary>
		/// Loads a collection of ProjectCategoryData objects from the database.
		/// </summary>
		/// <returns>A collection containing all of the ProjectCategoryData objects in the database.</returns>
		public static ProjectCategoriesVector LoadCollection()
		{
			SqlParameter[] parms = new SqlParameter[] {};
			return LoadCollection("ProjectCategoriesListProc", parms);
		}

		/// <summary>
		/// Loads a collection of ProjectCategoryData objects from the database.
		/// </summary>
		/// <returns>A collection containing all of the ProjectCategoryData objects in the database.</returns>
		public static ProjectCategoriesVector LoadCollection(bool IsMain,bool clients,bool Subcontracters,int projectID)
		{
			SqlParameter[] parms = new SqlParameter[4];
			parms[0] = new SqlParameter("@ProjectID", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.Input, true, 10, 0, "ProjectID", DataRowVersion.Current, null);
			parms[0].Value = projectID;
			parms[1]= new System.Data.SqlClient.SqlParameter("@IsMain", System.Data.SqlDbType.Bit, 1, System.Data.ParameterDirection.Input, true, 0, 0, "IsMain", DataRowVersion.Current, null);
			parms[1].Value = IsMain;
			parms[2]= new System.Data.SqlClient.SqlParameter("@Clients", System.Data.SqlDbType.Bit, 1, System.Data.ParameterDirection.Input, true, 0, 0, "Clients", DataRowVersion.Current, null);
			parms[2].Value = clients;
			parms[3]= new System.Data.SqlClient.SqlParameter("@Subcontracters", System.Data.SqlDbType.Bit, 1, System.Data.ParameterDirection.Input, true, 0, 0, "Subcontracters", DataRowVersion.Current, null);
			parms[3].Value = Subcontracters;
			return LoadCollection("ProjectCategoriesListByPartsProc", parms);
		}

		/// <summary>
		/// Loads a collection of ProjectCategoryData objects from the database.
		/// </summary>
		/// <returns>A collection containing all of the ProjectCategoryData objects in the database.</returns>
		public static ProjectCategoriesVector LoadCollection(int projectID,int userType, int nClientID)
		{
			SqlParameter[] parms = new SqlParameter[3];
			parms[0] = new SqlParameter("@ProjectID", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.Input, true, 10, 0, "ProjectID", DataRowVersion.Current, null);
			parms[0].Value = projectID;
			parms[1] = new SqlParameter("@UserType", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.Input, true, 10, 0, "UserType", DataRowVersion.Current, null);
			if(userType>0)
				parms[1].Value = userType;
			parms[2] = new SqlParameter("@ClientID", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.Input, true, 10, 0, "ClientID", DataRowVersion.Current, null);
			if(nClientID>0)
				parms[2].Value = nClientID;

			return LoadCollection("ProjectCategoriesListByProjectProc", parms);
		}
		public static ProjectCategoriesVector LoadCollection(int projectID,int userType)
		{
			SqlParameter[] parms = new SqlParameter[3];
			parms[0] = new SqlParameter("@ProjectID", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.Input, true, 10, 0, "ProjectID", DataRowVersion.Current, null);
			parms[0].Value = projectID;
			parms[1] = new SqlParameter("@UserType", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.Input, true, 10, 0, "UserType", DataRowVersion.Current, null);
			if(userType>0)
				parms[1].Value = userType;
			parms[2] = new SqlParameter("@ClientID", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.Input, true, 10, 0, "ClientID", DataRowVersion.Current, null);
			

			return LoadCollection("ProjectCategoriesListByProjectProc", parms);
		}
		/// <summary>
		/// Saves a collection of ProjectCategoryData objects to the database.
		/// </summary>
		/// <returns>A collection containing all of the ProjectCategoryData objects in the database.</returns>
		public static  void  SaveCollection(ProjectCategoriesVector col,string spName, SqlParameter[] parms)
		{
			ProjectCategoriesVector colOld = LoadCollection(spName,parms);
			UpdateCollection(colOld,col);
		}
		public static  void  SaveCollection(ProjectCategoriesVector col)
		{
			ProjectCategoriesVector colOld = LoadCollection();
			UpdateCollection(colOld,col);
		}
		public static  void  SaveCollection(ProjectCategoriesVector col, string whereClause )
		{
			ProjectCategoriesVector colOld = LoadCollection(whereClause);
			UpdateCollection(colOld,col);
		}
		/// <summary>
		/// Loads a collection of ProjectCategoryData objects from the database.
		/// </summary>
		/// <returns>A collection containing all of the ProjectCategoryData objects in the database.</returns>
		public static ProjectCategoriesVector LoadCollection(string whereClause)
		{
			SqlParameter[] parms = new SqlParameter[] { new SqlParameter("@WhereClause", whereClause) };
			return LoadCollection("ProjectCategoriesListProc", parms);
		}

		/// <summary>
		/// Loads a collection of ProjectCategoryData objects from the database.
		/// </summary>
		/// <returns>A collection containing all of the ProjectCategoryData objects in the database.</returns>
		public static ProjectCategoriesVector LoadCollection(string spName, SqlParameter[] parms)
		{
			ProjectCategoriesVector result = new ProjectCategoriesVector();
			using (SqlDataReader reader = SqlHelper.ExecuteReader(CommonDAL.ConnectionString, CommandType.StoredProcedure, spName, parms))
			{
				while (reader.Read())
				{
					ProjectCategoryData tmp = new ProjectCategoryData();
					LoadFromReader(reader, tmp);
					result.Add(tmp);
				}
			}
			return result;
		}
		/// <summary>
		/// Deletes a ProjectCategoryData object from the database.
		/// </summary>
		/// <param name="CategoryID">The primary key value</param>
		public static void Delete(int CategoryID)
		{
			SqlParameter[] parameterValues = new SqlParameter[] { new SqlParameter("@CategoryID", CategoryID) };
			SqlHelper.ExecuteNonQuery(CommonDAL.ConnectionString, CommandType.StoredProcedure, "ProjectCategoriesInactiveProc", parameterValues);
		}

		public static void LoadFromReader(SqlDataReader reader, ProjectCategoryData target)
		{
			if (reader != null && !reader.IsClosed)
			{
				//target.Key = reader.GetInt32(0);
				if (!reader.IsDBNull(0)) target.CategoryID = reader.GetInt32(0);
				if (!reader.IsDBNull(1)) target.ProjectID = reader.GetInt32(1);
				if (!reader.IsDBNull(2)) target.CategoryName = reader.GetString(2);
//				if (!reader.IsDBNull(3)) target.IsClientVisible = reader.GetBoolean(3);
//				if (!reader.IsDBNull(4)) target.IsSubcontractorVisible = reader.GetBoolean(4);
//				if (!reader.IsDBNull(5)) target.IsBuilderVisible = reader.GetBoolean(5);
				if (!reader.IsDBNull(3)) target.IsActive = reader.GetBoolean(3);
				if (!reader.IsDBNull(4)) target.ClientID = reader.GetInt32(4);
				if (!reader.IsDBNull(5)) target.SubcontracterID = reader.GetInt32(5);
				if (!reader.IsDBNull(6)) target.IsProjectMain = reader.GetBoolean(6);
				if (!reader.IsDBNull(7)) target.ProjectName = reader.GetString(7);
			}
		}
		
	  
		public static int Insert( ProjectCategoryData source)
		{
			SqlParameter[] parameterValues = GetInsertParameterValues(source);
			SqlHelper.ExecuteNonQuery(CommonDAL.ConnectionString, CommandType.StoredProcedure, "ProjectCategoriesInsProc", parameterValues);
			source.CategoryID = (int) parameterValues[0].Value;
			return source.CategoryID;
		}

		public static int Update( ProjectCategoryData source)
		{
			SqlParameter[] parameterValues = GetUpdateParameterValues(source);
			SqlHelper.ExecuteNonQuery(CommonDAL.ConnectionString, CommandType.StoredProcedure,
				"ProjectCategoriesUpdProc", parameterValues);
			return source.CategoryID;
		}
		private static void UpdateCollection(ProjectCategoriesVector colOld,ProjectCategoriesVector col)
		{
			// Delete all old items
			foreach( ProjectCategoryData itemOld in colOld)
			{
				if(!ExistsKey(itemOld.CategoryID, col))
					Delete(itemOld.CategoryID);
			}
			foreach( ProjectCategoryData item in col)
			{
				if(item.CategoryID <= -1)
					Insert(item);
				else
					Update(item);
			}
		}
		private static bool ExistsKey(int Key, ProjectCategoriesVector col)
		{
			foreach( ProjectCategoryData item in col)
				if(item.CategoryID==Key)
					return true;
			return false;
		}

		private static SqlParameter GetSqlParameter(string name, ParameterDirection direction, object value)
		{
			SqlParameter p = new SqlParameter(name, value);
			p.Direction = direction;
			return p;
		}

		private static SqlParameter[] GetInsertParameterValues( ProjectCategoryData source)
		{
			SqlParameter[] prams = new SqlParameter[7];
			prams[0] = GetSqlParameter("@CategoryID", ParameterDirection.Output, source.CategoryID);
			prams[1] = GetSqlParameter("@ProjectID", ParameterDirection.Input, source.ProjectID);
			prams[2] = GetSqlParameter("@CategoryName", ParameterDirection.Input, source.CategoryName);
//			prams[3] = GetSqlParameter("@IsClientVisible", ParameterDirection.Input, source.IsClientVisible );
//			prams[4] = GetSqlParameter("@IsSubcontractorVisible", ParameterDirection.Input, source.IsSubcontractorVisible);
//			prams[5] = GetSqlParameter("@IsBuilderVisible", ParameterDirection.Input, source.IsBuilderVisible );
			prams[3] = GetSqlParameter("@IsActive", ParameterDirection.Input, source.IsActive);
			if(source.ClientID>0)
				prams[4] = GetSqlParameter("@ClientID", ParameterDirection.Input, source.ClientID);
			else
				prams[4] = GetSqlParameter("@ClientID", ParameterDirection.Input, DBNull.Value );
			if(source.SubcontracterID>0)
				prams[5] = GetSqlParameter("@SubcontracterID", ParameterDirection.Input,source.SubcontracterID);
			else
				prams[5] = GetSqlParameter("@SubcontracterID", ParameterDirection.Input,DBNull.Value);
			prams[6] = GetSqlParameter("@IsProjectMain", ParameterDirection.Input, source.IsProjectMain);
			
			return prams;
		}

		private static SqlParameter[] GetUpdateParameterValues( ProjectCategoryData source)
		{
			SqlParameter[] prm = GetInsertParameterValues(source);
			prm[0].Direction=  ParameterDirection.Input;
			return prm;
//			return new SqlParameter[] {
//										  new SqlParameter("@CategoryID", source.CategoryID),
//										  new SqlParameter("@ProjectID", source.ProjectID),
//										  new SqlParameter("@CategoryName", source.CategoryName),
//										  new SqlParameter("@IsClientVisible", source.IsClientVisible),
//										  new SqlParameter("@IsSubcontractorVisible", source.IsSubcontractorVisible),
//										  new SqlParameter("@IsBuilderVisible", source.IsBuilderVisible),
//										  new SqlParameter("@IsActive", source.IsActive),
//										  new SqlParameter("@ClientID", (source.ClientID>0)?source.ClientID:DBNull.Value),
//										  new SqlParameter("@SubcontracterID",(source.SubcontracterID>0)?source.SubcontracterID:DBNull.Value),
//										  new SqlParameter("@IsProjectMain", source.IsProjectMain),
//										
//			};
		}
	}
}

