using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Data.SqlClient;
using Asa.Timesheet.Data;
using log4net;
using log4net.Config;
using Asa.Timesheet.Data.Util;
using Asa.Timesheet.Data.Entities;


namespace PM
{
	/// <summary>
	/// Summary description for EditProject.
	/// </summary>
	public class EditProject : BasePage
	{
		#region WebControls
		
		protected System.Web.UI.WebControls.Label lblError;
		protected System.Web.UI.WebControls.Label lblInfo;
		protected System.Web.UI.WebControls.Label lblName;
		protected System.Web.UI.WebControls.Label txtProjectName;
		protected System.Web.UI.WebControls.Panel grid;
		protected System.Web.UI.WebControls.Button btnNew;
		protected System.Web.UI.WebControls.Label lblBuildingType;
		protected System.Web.UI.WebControls.DropDownList ddlBuildingTypes;
		protected System.Web.UI.WebControls.Label lblProjectCode;
		protected System.Web.UI.WebControls.TextBox txtProjectCode;
		protected System.Web.UI.WebControls.Label lblStartDate;
		protected System.Web.UI.WebControls.Label lblManager;
		protected System.Web.UI.WebControls.DropDownList ddlManager;
		protected System.Web.UI.WebControls.Label txtStartDate;
		protected System.Web.UI.WebControls.Label txtEndDateContract;
		protected System.Web.UI.WebControls.Label lblBTCode;
		protected System.Web.UI.WebControls.Label lblProjectOrder;
		protected System.Web.UI.WebControls.LinkButton lbProjectBuilding;
		private const int PreProjectBuildingTypeID = 7;
		protected System.Web.UI.WebControls.Label Label7;
		protected System.Web.UI.WebControls.Label txtManager;
		protected System.Web.UI.WebControls.Label txtProjCode;
		protected System.Web.UI.WebControls.Label txtBuildingTypes;
		protected System.Web.UI.HtmlControls.HtmlTable Table2;
		protected System.Web.UI.WebControls.Panel gridCopy;
		protected System.Web.UI.WebControls.Button btnPlusAllCopy;
		protected System.Web.UI.WebControls.Button btnMinusAllCopy;
		protected System.Web.UI.WebControls.DataList dlCategoriesCopy;
		private static readonly ILog log = LogManager.GetLogger(typeof(EditProject));
		protected System.Web.UI.WebControls.Button btnPlusAll;
		protected System.Web.UI.WebControls.Button btnMinusAll;
		protected System.Web.UI.WebControls.DataList dlCategories;
		protected System.Web.UI.WebControls.Label lbInsideUse;
		protected System.Web.UI.WebControls.Label lbOutsideUse;
		protected System.Web.UI.HtmlControls.HtmlTable tblCat;
		protected System.Web.UI.WebControls.DropDownList ddlSubCategories;
		protected System.Web.UI.WebControls.Button btnCreateCategory;
		protected System.Web.UI.WebControls.Label lbNoCategory;
		protected System.Web.UI.WebControls.DataGrid grdProfile;
		protected System.Web.UI.WebControls.ImageButton imgBack;
		protected System.Web.UI.WebControls.Label lbProfileNote;
		//DBUGG protected System.Web.UI.WebControls.Image Image1;
		protected System.Web.UI.WebControls.LinkButton linkBack;


		#endregion

		#region enum
		
		private enum gridColomns
		{
			Edit= 0,
			FileName,
			FileNameUnvisible,
			Subject,
			//Minutes,
			Version,
			Type,
			Creator,
			Date,
			Delete,
		}

		
		private enum gridProfile
		{
			ID = 0,
			Scan,
			TypeUnvisible,
			TypeVisible,
			Discription,
			IsSubVis,
			IsClVis,
			IsBuildVis,
		}

		private enum TableSub
		{
			Number = 0,
			SubType,
			FullName,
			AccountName,
			UserTypeHidden,
			Mail,
			Address,
			Phone
		}
		private enum TableNotSub
		{
			Number = 0,
			FullName,
			AccountName,
			//UserType = 3,
			UserTypeHidden,
			Mail,
			Address,
			Phone
		}
		#endregion

		#region Page_Load

        private void Page_Load(object sender, System.EventArgs e)
        {
            if ((!LoggedUser.IsAuthenticated) || UIHelpers.GetPIDParam() == -1 || (!UIHelpers.HasAccess(LoggedUser, UIHelpers.GetPIDParam())))
                ErrorRedirect(Resource.ResourceManager["noRightsAlert"]);
            LoadResourses();
            if (!this.IsPostBack)
            {



                //notes:if user is not autherized he can create or delete category
                bool b = (LoggedUser.UserRoleID > 0);//== (int)Roles.Director);
                if (!b)
                {
                    //btnNew.Visible = false;

                }

                if (!LoadProject(UIHelpers.GetPIDParam())) { lblError.Text = UIHelpers.GetText(Language, "editProject_ErrorLoadProject"); }/* lblError.Text = Resource.ResourceManager["editProject_ErrorLoadProject"]*/;
                BindGrid();


                //LoadNotCreatedCategories();
                //				if(LoggedUser.IsClient||LoggedUser.IsBuilder)
                //				{
                //					
                //					//tblCat.Rows[0].Cells[0].Visible=false;
                //					//tblCat.Rows[1].Style = "Width = 100%";
                //				}
                LoadProfile();

                //notes:here look wheather to show grdProfile or not
                //				if(LoggedUser.IsUser||LoggedUser.IsSubcontracter)
                //				{
                //					if(LoggedUser.IsSubcontracter)
                //					{
                //						PMProjectUserAccesssVector dat = PMProjectUserAccessDAL.LoadCollectionForUser((int)PMUserType.Subcontracter,LoggedUser.UserID,UIHelpers.GetPIDParam());
                //						if(dat.Count>0)
                //						{
                //							if(!dat[0].IsActiveGroup)
                //								grdProfile.Visible = false;
                //							else
                //								if(!CheckForCategory())
                //									grdProfile.Visible = false;
                //						}
                //						else
                //						{
                ////							SubcontracterData datsub = SubcontracterDAL.Load(LoggedUser.UserID);
                ////							if(!datsub.IsActiveGroup)
                //								grdProfile.Visible = false;
                //						}
                //					}
                //				}
                //				else
                //					grdProfile.Visible = false;
                if (ddlSubCategories.Visible && (!LoggedUser.IsUser))
                    ddlSubCategories.Visible = btnCreateCategory.Visible = false;
                //LoadProjectCategories();
                //LoadProjectCategoriesCopy();
                //if(!b)
                //grdCategories.Columns[(int)gridColomns.Delete].Visible = false;


            }
        }


        private bool CheckForCategory()
        {
            ProjectCategoriesVector pcv = ProjectCategoryDAL.LoadCollection(UIHelpers.GetPIDParam(), -1);
            foreach (ProjectCategoryData pcd in pcv)
            {
                if (pcd.SubcontracterID == LoggedUser.UserID)
                    return true;
            }
            return false;
        }

	
//		private bool LoadBuildingTypes(string selectedValue)
//		{
//			SqlDataReader reader = null;
//			try
//			{
//				reader = DBManager.SelectBuildingTypes();
//				ddlBuildingTypes.DataSource = reader;
//				ddlBuildingTypes.DataValueField = "BuildingTypeID";
//				ddlBuildingTypes.DataTextField = "BuildingType";
//				ddlBuildingTypes.DataBind();
//				if(ddlBuildingTypes.Items.FindByValue(System.Configuration.ConfigurationManager.AppSettings["BuildingTypeNotInPM"])!=null)
//					ddlBuildingTypes.Items.Remove(ddlBuildingTypes.Items.FindByValue(System.Configuration.ConfigurationManager.AppSettings["BuildingTypeNotInPM"]));
//				
//				ddlBuildingTypes.Items.Insert(0, new ListItem("", "0"));
//				if(ddlBuildingTypes.Items.FindByValue(selectedValue)!=null)
//					ddlBuildingTypes.SelectedValue = selectedValue;
//			}
//			catch (Exception ex)
//			{
//				log.Info(ex);
//				return false;
//			}
//			finally 
//			{
//				if (reader!=null) reader.Close();
//			}
//
//			return true;
//		}


        private void LoadResourses()
        {
            string lang = Language;

            lblBuildingType.Text = UIHelpers.GetText(lang, "Name_BuildingTypeName");
            lblProjectCode.Text = UIHelpers.GetText(lang, "Name_ProjectCodeName");
            lblStartDate.Text = UIHelpers.GetText(lang, "Name_StartDateName");
            Label7.Text = UIHelpers.GetText(lang, "Name_EndDateName");
            lblManager.Text = UIHelpers.GetText(lang, "Name_ManagerName");
            btnNew.Text = UIHelpers.GetText(lang, "Name_btnNewCategoryName");
            //btnBack.Text = UIHelpers.GetText(lang,"Name_btnBackProjectsName");
            linkBack.Text = "<i class='fas fa-chevron-left'></i> " + UIHelpers.GetText(lang, "Name_btnBackProjectsName");
            //lblName.Text = UIHelpers.GetText(lang,"Name_ProjectName");

            btnCreateCategory.Text = UIHelpers.GetText(lang, "Name_btnCreateName");
            lblBuildingType.Visible = SessionManager.Lang == Languages.BG;
            txtBuildingTypes.Visible = SessionManager.Lang == Languages.BG;
            grdProfile.Columns[(int)gridProfile.TypeVisible].HeaderText = UIHelpers.GetText(lang, "Name_colomnProfileGrid");
            if (!LoggedUser.IsUser)
            {
                //Image1.Visible = false;
                grdProfile.Columns[(int)gridProfile.IsSubVis].Visible = false;
                grdProfile.Columns[(int)gridProfile.IsClVis].Visible = false;
                grdProfile.Columns[(int)gridProfile.IsBuildVis].Visible = false;
            }
            if (SessionManager.Lang == Languages.EN)
            {
                //Image1.Visible = false;
                grdProfile.Columns[(int)gridProfile.IsSubVis].Visible = false;
                grdProfile.Columns[(int)gridProfile.IsClVis].Visible = false;
                grdProfile.Columns[(int)gridProfile.IsBuildVis].Visible = false;
            }
            //lbProfileNote.Text = UIHelpers.GetText(lang,"lbProfileNote_Text");
            if (LoggedUser.IsUser || LoggedUser.IsSubcontracter)
            {
                lbInsideUse.Text = UIHelpers.GetText(lang, "Name_lbInsideUseName");
                lbOutsideUse.Text = UIHelpers.GetText(lang, "Name_lbOutsideUseName");
            }
            else
            {
                tblCat.Rows[0].Cells[0].Style.Clear();
                tblCat.Rows[0].Cells[1].Style.Clear();

            }
            btnNew.Visible = (LoggedUser.IsUser || LoggedUser.IsSubcontracter);
            //grdCategories.Columns[(int)gridColomns.Name].HeaderText = UIHelpers.GetText(lang,"Name_CategoryName1Colomn");


        }

		
		#endregion
	
		#region HTML

        protected bool GetVisibleProfile()
        {
            return LoggedUser.HasPaymentRights;
        }


        protected string GetTextYes()
        {
            return UIHelpers.GetText(Language, "textToActiveGroup");
        }


        protected string GetTextNo()
        {
            return UIHelpers.GetText(Language, "textToUnactiveGroup");
        }


        protected string GetURL(int ID)
        {
            string s1 = string.Concat(System.Configuration.ConfigurationManager.AppSettings["AsaProjectProfile"], ID, System.Configuration.ConfigurationManager.AppSettings["ExtensionASA"]);
            string s = string.Concat(System.Configuration.ConfigurationManager.AppSettings["AsaProjectProfile"], ID, System.Configuration.ConfigurationManager.AppSettings["ExtensionASA"]);
            return s;
        }


        protected bool GetVisible(object main, object clID, object SubID)
        {
            if (main == DBNull.Value || (bool)main)
                return false;
            else return true;
        }


        protected bool CheckForActiveGroupYes(int categoryID)
        {
            ProjectCategoryData dat = ProjectCategoryDAL.Load(categoryID);
            if (dat.SubcontracterID > 0)
            {
                PMProjectUserAccesssVector vec = PMProjectUserAccessDAL.LoadCollectionForUser((int)PMUserType.Subcontracter, dat.SubcontracterID, UIHelpers.GetPIDParam());
                if (vec.Count > 0)
                    if (vec[0].IsActiveGroup) return true;
                    else return false;
                else
                {
                    SubcontracterData datsub = SubcontracterDAL.Load(dat.SubcontracterID);
                    if (datsub == null)
                        return false;
                    PMProjectUserAccessData datpmp = new PMProjectUserAccessData(-1, (int)PMUserType.Subcontracter, datsub.SubcontracterID, UIHelpers.GetPIDParam(), true, datsub.IsActiveGroup, string.Empty);
                    PMProjectUserAccessDAL.Save(datpmp);
                    return datsub.IsActiveGroup;
                }
            }
            return false;
        }


        protected bool CheckForActiveGroupNo(int categoryID)
        {
            ProjectCategoryData dat = ProjectCategoryDAL.Load(categoryID);
            if (dat.SubcontracterID > 0)
            {
                PMProjectUserAccesssVector vec = PMProjectUserAccessDAL.LoadCollectionForUser((int)PMUserType.Subcontracter, dat.SubcontracterID, UIHelpers.GetPIDParam());
                if (vec.Count > 0)
                    if (vec[0].IsActiveGroup) return false;
                    else return true;
                else
                {
                    SubcontracterData datsub = SubcontracterDAL.Load(dat.SubcontracterID);
                    if (datsub == null)
                        return false;
                    PMProjectUserAccessData datpmp = new PMProjectUserAccessData(-1, (int)PMUserType.Subcontracter, datsub.SubcontracterID, UIHelpers.GetPIDParam(), true, datsub.IsActiveGroup, string.Empty);
                    PMProjectUserAccessDAL.Save(datpmp);
                    return !(datsub.IsActiveGroup);
                }
            }
            return false;
        }

	
//		protected string getColorLink(int categoryID)
//		{
//			ProjectCategoryData dat = ProjectCategoryDAL.Load(categoryID);
//			if(dat.SubcontracterID>0)
//			{
//				PMProjectUserAccesssVector vec = PMProjectUserAccessDAL.LoadCollectionForUser((int)PMUserType.Subcontracter,dat.SubcontracterID,UIHelpers.GetPIDParam());
//				if(vec.Count>0)
//					if(vec[0].IsActiveGroup) return "#804000";//return false;
//					else return "#804000";//return true;
//				else
//				{
//					SubcontracterData datsub = SubcontracterDAL.Load(dat.SubcontracterID);
//					if(datsub == null)
//						return "#804000";//return false;
//					PMProjectUserAccessData datpmp = new PMProjectUserAccessData(-1,(int)PMUserType.Subcontracter,datsub.SubcontracterID,UIHelpers.GetPIDParam(),true,datsub.IsActiveGroup);
//					PMProjectUserAccessDAL.Save(datpmp);
//					if(!(datsub.IsActiveGroup)) return "#804000";//return true;
//					else return "#804000";//return false;
//				}
//			}
//			return "#804000";
//		}

		#endregion

		#region Web Form Designer generated code
        override protected void OnInit(EventArgs e)
        {
            //
            // CODEGEN: This call is required by the ASP.NET Web Form Designer.
            //
            InitializeComponent();
            base.OnInit(e);
        }
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
        private void InitializeComponent()
        {
            this.linkBack.Click += new System.EventHandler(this.link_Click);
            this.grdProfile.ItemCreated += new System.Web.UI.WebControls.DataGridItemEventHandler(this.grdProfile_ItemCreated);
            this.grdProfile.ItemCommand += new System.Web.UI.WebControls.DataGridCommandEventHandler(this.grdProfile_ItemCommand);
            this.grdProfile.ItemDataBound += new System.Web.UI.WebControls.DataGridItemEventHandler(this.grdProfile_ItemDataBound);
            this.btnNew.Click += new System.EventHandler(this.btnNew_Click);
            this.btnCreateCategory.Click += new System.EventHandler(this.btnCreateCategory_Click);
            this.btnPlusAll.Click += new System.EventHandler(this.btnPlusAll_Click);
            this.btnMinusAll.Click += new System.EventHandler(this.btnMinusAll_Click);
            this.dlCategories.ItemCreated += new System.Web.UI.WebControls.DataListItemEventHandler(this.dlCategories_ItemCreated);
            this.dlCategories.ItemCommand += new System.Web.UI.WebControls.DataListCommandEventHandler(this.dlCategories_ItemCommand);
            this.dlCategories.ItemDataBound += new System.Web.UI.WebControls.DataListItemEventHandler(this.dlCategories_ItemDataBound);
            this.btnPlusAllCopy.Click += new System.EventHandler(this.btnPlusAllCopy_Click);
            this.btnMinusAllCopy.Click += new System.EventHandler(this.btnMinusAllCopy_Click);
            this.dlCategoriesCopy.ItemCreated += new System.Web.UI.WebControls.DataListItemEventHandler(this.dlCategoriesCopy_ItemCreated);
            this.dlCategoriesCopy.ItemCommand += new System.Web.UI.WebControls.DataListCommandEventHandler(this.dlCategoriesCopy_ItemCommand);
            this.dlCategoriesCopy.ItemDataBound += new System.Web.UI.WebControls.DataListItemEventHandler(this.dlCategoriesCopy_ItemDataBound);
            this.Load += new System.EventHandler(this.Page_Load);

        }
		#endregion
		
		#region Database

        private void LoadProfile()
        {
            ProjectDocumentsVector pdv = new ProjectDocumentsVector();
            if (LoggedUser.IsUser || LoggedUser.IsSubcontracter)
            {
                if (LoggedUser.IsSubcontracter)
                {
                    PMProjectUserAccesssVector dat = PMProjectUserAccessDAL.LoadCollectionForUser((int)PMUserType.Subcontracter, LoggedUser.UserID, UIHelpers.GetPIDParam());
                    if (dat.Count > 0)
                    {
                        if (!dat[0].IsActiveGroup)
                            pdv = ProjectDocumentDAL.LoadCollection("ProjectDocumentsListForPMProc", SQLParms.CreateProjectDocumentsListForPMProc(UIHelpers.GetPIDParam(), true, false, false));
                        else
                            if (!CheckForCategory())
                                pdv = ProjectDocumentDAL.LoadCollection("ProjectDocumentsListForPMProc", SQLParms.CreateProjectDocumentsListForPMProc(UIHelpers.GetPIDParam(), true, false, false));
                            else
                                pdv = ProjectDocumentDAL.LoadCollection("ProjectDocumentsListForPMProc", SQLParms.CreateProjectDocumentsListForPMProc(UIHelpers.GetPIDParam(), false, false, false));
                    }
                    else
                    {
                        pdv = ProjectDocumentDAL.LoadCollection("ProjectDocumentsListForPMProc", SQLParms.CreateProjectDocumentsListForPMProc(UIHelpers.GetPIDParam(), true, false, false));
                    }
                }
                else
                    pdv = ProjectDocumentDAL.LoadCollection("ProjectDocumentsListForPMProc", SQLParms.CreateProjectDocumentsListForPMProc(UIHelpers.GetPIDParam(), false, false, false));
            }
            else
                pdv = ProjectDocumentDAL.LoadCollection("ProjectDocumentsListForPMProc", SQLParms.CreateProjectDocumentsListForPMProc(UIHelpers.GetPIDParam(), false, LoggedUser.UserType == (int)PMUserType.Client || LoggedUser.UserType == (int)PMUserType.PMUser, LoggedUser.UserType == (int)PMUserType.Builder));

            //ProjectDocumentsVector pdv = ProjectDocumentDAL.LoadCollection("ProjectDocumentsListForPMProc",SQLParms.CreateProjectDocumentsListForPMProc(UIHelpers.GetPIDParam(),false,false,false));
            grdProfile.DataSource = pdv;
            grdProfile.DataBind();
            if (grdProfile.Items.Count > 0)
            {
                lbProfileNote.Visible = LoggedUser.HasPaymentRights;
                grdProfile.Visible = true;
            }
            else
                lbProfileNote.Visible = grdProfile.Visible = false;
        }


        private bool LoadProject(int projectID)
        {
            int buildingTypeID = 0, projectManagerID = 0;

            SqlDataReader reader = null;
            //LoadBuildingTypes("0");
            try
            {
                reader = ProjectsData.SelectProject(projectID);
                if (reader.Read())
                {
                    //txtProjectName.Text = reader.GetString(1);
                    Header.PageTitle = reader.GetString(1);

                    txtProjectCode.Text = reader.IsDBNull(2) ? String.Empty : reader.GetString(2);
                    buildingTypeID = reader.IsDBNull(3) ? 0 : reader.GetInt32(3);
                    txtStartDate.Text = reader.IsDBNull(9) ? "" : TimeHelper.FormatDate(reader.GetDateTime(9));
                    projectManagerID = reader.IsDBNull(11) ? 0 : reader.GetInt32(11);

                    txtEndDateContract.Text = reader.IsDBNull(21) ? "" : TimeHelper.FormatDate(reader.GetDateTime(21));


                }
                log.Info("Building Type" + buildingTypeID.ToString());
                if ((!UIHelpers.LoadBuildingTypes(buildingTypeID.ToString(), ddlBuildingTypes)) || (!LoadUsers(projectManagerID.ToString())))
                    return false;

                LoadProjectCode(projectID);


                txtManager.Text = ddlManager.SelectedItem.Text;
                txtProjCode.Text = string.Concat(lblBTCode.Text, " ", lblProjectOrder.Text, " ", txtProjectCode.Text);
                txtBuildingTypes.Text = ddlBuildingTypes.SelectedItem.Text;

            }
            catch (Exception ex)
            {
                log.Error(UIHelpers.GetLogErrorMessage(ex));
                return false;
            }

            finally
            {
                if (reader != null) reader.Close();
            }
            ddlManager.Enabled = ddlBuildingTypes.Enabled = txtProjectCode.Enabled =
                ddlManager.Enabled = false;

            return true;
        }
        public string GetTextEN(string text)
        {
            if (SessionManager.Lang == Languages.BG)
                return text;
            return UIHelpers.ReplaceEN(text);
        }
        private void LoadProjectCategories()
        {
            CheckCategories();
            if (LoggedUser.IsUser || LoggedUser.IsSubcontracter)
            {

                ProjectCategoriesVector pcv = ProjectCategoryDAL.LoadCollection(UIHelpers.GetPIDParam(), -1);

                dlCategories.DataSource = pcv;
                dlCategories.DataKeyField = "CategoryID";
                dlCategories.DataBind();
                //			try
                //			{
                //				SetConfirmDelete(grdCategories, "btnDelete", (int)gridColomns.NameUnvisible);
                //			}
                //			catch(Exception ex)
                //			{
                //				log.Error(UIHelpers.GetLogErrorMessage(ex));
                //			}
                if (!LoggedUser.IsUser)
                {
                    foreach (DataListItem it in dlCategories.Items)
                    {
                        ImageButton btnDel = (ImageButton)it.FindControl("imgDelete");
                        btnDel.Visible = false;
                        ImageButton btn = (ImageButton)it.FindControl("imgActiveGroupYes");
                        btn.Visible = false;
                        btn = (ImageButton)it.FindControl("imgActiveGroupNo");
                        btn.Visible = false;
                    }
                }
                for (int i = 0; i < dlCategories.Items.Count; i++)
                {
                    int catID = (int)dlCategories.DataKeys[i];

                    DataGrid grdFiles = (DataGrid)dlCategories.Items[i].FindControl("grdFiles");
                    if (grdFiles != null)
                    {
                        FilesPMVector fPMv = FilePMDAL.LoadCollectionByCategory(catID, LoggedUser.UserType, SortOrder);

                        grdFiles.DataSource = fPMv;
                        grdFiles.DataKeyField = "FileID";
                        grdFiles.DataBind();

                    }
                }
            }
            else
            {
                //				lbOutsideUse.Text = "";
                //				lbInsideUse.Text = "";
                btnPlusAll.Visible = false;
                btnMinusAll.Visible = false;
                //tblCat.Rows[0].Cells[0].Visible=false;
                //grid.Visible = false;

                dlCategories.Visible = false;
                grid.Width = new Unit(0);
            }
        }

        private void LoadProjectCategoriesCopy()
        {
            //CheckCategories();
            ProjectCategoriesVector pcv = new ProjectCategoriesVector();
            if (LoggedUser.IsUser || LoggedUser.IsSubcontracter)
            {
                ProjectCategoriesVector pcvClient = ProjectCategoryDAL.LoadCollection(UIHelpers.GetPIDParam(), (int)PMUserType.Client, LoggedUser.UserID);
                pcv = ProjectCategoryDAL.LoadCollection(UIHelpers.GetPIDParam(), (int)PMUserType.Builder, LoggedUser.UserID);
                foreach (ProjectCategoryData pcd in pcvClient)
                    if (pcv.GetByID(pcd.CategoryID) == null)
                        pcv.Add(pcd);
                ProjectCategoriesVector pcvPMUser = ProjectCategoryDAL.LoadCollection(UIHelpers.GetPIDParam(), (int)PMUserType.PMUser, LoggedUser.UserID);
                foreach (ProjectCategoryData pcd in pcvPMUser)
                    if (pcv.GetByID(pcd.CategoryID) == null)
                        pcv.Add(pcd);

                ProjectCategoriesVector pcvNadzor = ProjectCategoryDAL.LoadCollection(UIHelpers.GetPIDParam(), (int)PMUserType.Supervisors, LoggedUser.UserID);
                foreach (ProjectCategoryData pcd in pcvNadzor)
                    if (pcv.GetByID(pcd.CategoryID) == null)
                        pcv.Add(pcd);
            }
            else
            {
                pcv = ProjectCategoryDAL.LoadCollection(UIHelpers.GetPIDParam(), LoggedUser.UserType, LoggedUser.UserID);
            }
            dlCategoriesCopy.DataSource = pcv;
            dlCategoriesCopy.DataKeyField = "CategoryID";
            dlCategoriesCopy.DataBind();
            //			try
            //			{
            //				SetConfirmDelete(grdCategories, "btnDelete", (int)gridColomns.NameUnvisible);
            //			}
            //			catch(Exception ex)
            //			{
            //				log.Error(UIHelpers.GetLogErrorMessage(ex));
            //			}
            if (!LoggedUser.HasPaymentRights)
            {
                foreach (DataListItem it in dlCategoriesCopy.Items)
                {
                    ImageButton btnDel = (ImageButton)it.FindControl("imgDeleteCopy");
                    btnDel.Visible = false;
                    ImageButton btn = (ImageButton)it.FindControl("imgActiveGroupYesCopy");
                    btn.Visible = false;
                    btn = (ImageButton)it.FindControl("imgActiveGroupNoCopy");
                    btn.Visible = false;
                }
            }
            for (int i = 0; i < dlCategoriesCopy.Items.Count; i++)
            {
                int catID = (int)dlCategoriesCopy.DataKeys[i];

                DataGrid grdFiles = (DataGrid)dlCategoriesCopy.Items[i].FindControl("grdFilesCopy");
                if (grdFiles != null)
                {
                    FilesPMVector fPMv = new FilesPMVector();
                    if (LoggedUser.IsSubcontracter || LoggedUser.IsUser)
                    {
                        FilesPMVector fPMv2 = FilePMDAL.LoadCollectionByCategory(catID, (int)PMUserType.Client, SortOrder);
                        FilesPMVector fPMv1 = FilePMDAL.LoadCollectionByCategory(catID, (int)PMUserType.Builder, SortOrder);
                        foreach (FilePMData d in fPMv1)
                            if (!searchByID(fPMv2, d))
                                fPMv2.Add(d);
                        if (LoggedUser.IsSubcontracter)
                        {
                            foreach (FilePMData d in fPMv2)
                                if (d.IsSubcontractorVisible)
                                    fPMv.Add(d);
                        }
                        else
                            fPMv = fPMv2;
                    }
                    else
                        fPMv = FilePMDAL.LoadCollectionByCategory(catID, LoggedUser.UserType, SortOrder);

                    grdFiles.DataSource = fPMv;
                    grdFiles.DataKeyField = "FileID";
                    grdFiles.DataBind();

                }

            }
            if ((!(LoggedUser.IsUser || LoggedUser.IsSubcontracter)) && (dlCategoriesCopy.Items.Count == 0))
            {
                tblCat.Visible = false;
                lbNoCategory.Visible = true;
                lbNoCategory.Text = UIHelpers.GetText(Language, "Name_NoCategoriesName");
            }
        }

        private bool searchByID(FilesPMVector v, FilePMData d)
        {
            foreach (FilePMData dat in v)
                if (dat.FileID == d.FileID)
                    return true;
            return false;
        }

        private bool LoadUsers(string selectedValue)
        {
            SqlDataReader reader = null;
            try
            {
                reader = UsersData.SelectUserNames(true);
                ddlManager.DataSource = reader;
                ddlManager.DataValueField = "UserID";
                ddlManager.DataTextField = "FullName";
                ddlManager.DataBind();

                ddlManager.Items.Insert(0, new ListItem("", "0"));
                if (ddlManager.Items.FindByValue(selectedValue) != null)
                    ddlManager.SelectedValue = selectedValue;
            }
            catch (Exception ex)
            {
                log.Info(ex);
                return false;
            }
            finally
            {
                if (reader != null) reader.Close();
            }
            return true;
        }

        private void BindGrid()
        {
            LoadProjectCategories();
            LoadProjectCategoriesCopy();
            LoadNotCreatedCategories();
            //			CheckCategories();
            //
            //			ProjectCategoriesVector pcv = ProjectCategoryDAL.LoadCollection(UIHelpers.GetPIDParam(),LoggedUser.UserType);
            //			
            //			grdCategories.DataSource = pcv;
            //			grdCategories.DataKeyField = "CategoryID";
            //			grdCategories.DataBind();
            //			try
            //			{
            //				SetConfirmDelete(grdCategories, "btnDelete", (int)gridColomns.NameUnvisible);
            //			}
            //			catch(Exception ex)
            //			{
            //				log.Error(UIHelpers.GetLogErrorMessage(ex));
            //			}
        }

        private bool LoadProjectCode(int projectID)
        {
            int buildingTypeID = 0;
            string buildingTypeCode;
            int number;
            string projectName;
            string projectCode;
            string folderPath;

            try
            {
                ProjectsData.SelectProjectCode(projectID, out buildingTypeID, out buildingTypeCode,
                    out number, out projectName, out projectCode, out folderPath);
            }
            catch (Exception ex)
            {
                log.Error(ex.Message);

                lblError.Text = UIHelpers.GetText(Language, "errorLoadPartOfProjectInfo");

                return false;
            }

            #region fill code labels

            if (buildingTypeID == PreProjectBuildingTypeID)
            {
                lblBTCode.Text = "XX";
                lblProjectOrder.Text = "";

            }
            else
            {
                lblBTCode.Text = buildingTypeCode;

                if (buildingTypeID == 0)
                {
                    lblProjectOrder.Text = "";

                }
                else lblProjectOrder.Text = number < 10 ? "0" + number.ToString() : number.ToString();
            }

            #endregion
            if (txtProjectCode.Text.Length >= 4 && txtProjectCode.Text.Substring(0, 2) == lblBTCode.Text
                && txtProjectCode.Text.Substring(2, 2) == lblProjectOrder.Text)
            {
                txtProjectCode.Text = txtProjectCode.Text.Substring(4);
            }
            if (txtProjectCode.Text.StartsWith("XX"))
                txtProjectCode.Text = txtProjectCode.Text.Substring(2);

            //if (Directory.Exists(folderPath)) 
            {
                //btnCreateFolder.Visible = false;

            }
            //			else
            //			{
            //				hlProjectFolder.Visible = false;
            //				btnCreateFolder.Visible = true;
            //
            //				if (buildingTypeID==0 || buildingTypeID == PreProjectBuildingTypeID)
            //				{
            //					btnCreateFolder.Visible = false;
            //				}
            //			}

            return true;
        }

        private void CheckCategories()
        {
            //			int projectID = UIHelpers.GetPIDParam();
            //			//notes:check for main category and if it not exist create it
            //			ProjectCategoriesVector pcv = ProjectCategoryDAL.LoadCollection(true,false,false,projectID);
            //			bool containMain = false;
            //			//bool containMainCl = false;
            //			foreach(ProjectCategoryData dat in pcv)
            //			{
            //				if(dat.CategoryName == Resource.ResourceManager["mainCategoryName"])
            //					containMain = true;
            ////				if(dat.CategoryName == Resource.ResourceManager["mainCategoryNameForClient"])
            ////					containMainCl = true;
            //			}
            //			if(!containMain)
            //			{
            //				ProjectCategoryData dat = new ProjectCategoryData(-1,projectID,Resource.ResourceManager["mainCategoryName"]/*,false,true,false*/,true,-1,-1,true);
            //				ProjectCategoryDAL.Save(dat);
            //			}
            //			if(!containMainCl)
            //			{
            //				ProjectCategoryData dat = new ProjectCategoryData(-1,projectID,Resource.ResourceManager["mainCategoryNameForClient"]/*,true,true,true*/,true,-1,-1,true);
            //				ProjectCategoryDAL.Save(dat);
            //			
            //			}
            //notes:add clients categories
            //			pcv = ProjectCategoryDAL.LoadCollection(false,true,false,projectID);
            //			ProjectClientsVector cvClients = ProjectClientDAL.LoadCollection("ProjectClientsSelByProjectProc",SQLParms.CreateProjectClientsSelByProjectProc(projectID,(int)ClientTypes.Client));
            //			ProjectClientsVector cv = ProjectClientDAL.LoadCollection("ProjectClientsSelByProjectProc",SQLParms.CreateProjectClientsSelByProjectProc(projectID,(int)ClientTypes.Builder));
            //			cv.AddRange(cvClients);
            //			bool[] bpcv = new bool[pcv.Count];
            //			for(int i = 0;i< bpcv.Length;i++)
            //				bpcv[i] = false;
            //			foreach(ProjectClientData cd in cv)
            //			{
            //				bool exist = false;
            //				foreach(ProjectCategoryData pcd in pcv)
            //				{
            //					if(pcd.ClientID == cd.ClientID)
            //					{
            //						int index = pcv.IndexOf(pcd);
            //						if(index >-1&&index<pcv.Count)
            //							bpcv[pcv.IndexOf(pcd)] = true;
            //						exist = true;
            //						break;
            //					}				
            //				}
            //				if(!exist)
            //				{
            //					ClientData cdat = ClientDAL.Load(cd.ClientID);
            //					ProjectCategoryData dat = new ProjectCategoryData(-1,projectID,cdat.ClientName/*,true,true,true*/,true,cd.ClientID,-1,false);
            //					ProjectCategoryDAL.Save(dat);
            //					pcv.Add(dat);
            //				}
            //			}
            //			for(int i = 0;i<bpcv.Length;i++)
            //			{
            //				bool b = bpcv[i];
            //				if(b == false)
            //				{
            //					if(i<pcv.Count)
            //					{
            //						ProjectCategoryData pcd = pcv[i];
            //						pcd.IsActive = false;
            //						ProjectCategoryDAL.Save(pcd);
            //					}
            //				}
            //			}
            //			//notes: add subcontractors categories
            //			pcv = ProjectCategoryDAL.LoadCollection(false,false,true,projectID);
            //			ProjectSubcontractersVector psv = ProjectSubcontracterDAL.LoadCollection("ProjectSubcontractersListProc1",SQLParms.CreateProjectSubcontractersListProc1(projectID,false));
            //			bpcv = new bool[pcv.Count];
            //			for(int i = 0;i< bpcv.Length;i++)
            //				bpcv[i] = false;
            //			foreach(ProjectSubcontracterData psd in psv)
            //			{
            //				bool exist = false;
            //				foreach(ProjectCategoryData pcd in pcv)
            //				{
            //					if(pcd.SubcontracterID == psd.SubcontracterID)
            //					{
            //						int index = pcv.IndexOf(pcd);
            //						if(index >-1&&index<pcv.Count)
            //							bpcv[pcv.IndexOf(pcd)] = true;
            //						exist = true;
            //						break;
            //					}				
            //				}
            //				if(!exist)
            //				{
            //					ProjectCategoryData dat = new ProjectCategoryData(-1,projectID,psd.SubcontracterType + " - " + psd.SubcontracterName/*,false,true,false*/,true,-1,psd.SubcontracterID,false);
            //					ProjectCategoryDAL.Save(dat);
            //					pcv.Add(dat);
            //				}
            //			}
            //			for(int i = 0;i<bpcv.Length;i++)
            //			{
            //				bool b = bpcv[i];
            //				if(b == false)
            //				{
            //					if(i<pcv.Count)
            //					{
            //						ProjectCategoryData pcd = pcv[i];
            //						pcd.IsActive = false;
            //						ProjectCategoryDAL.Save(pcd);
            //					}
            //				}
            //			}
        }
		
//		private bool GetAccess(ProjectCategoryData dat, int UserType)
//		{
//			switch(UserType)
//			{
//				case ((int)PMUserType.Client):return dat.IsClientVisible;
//				case ((int)PMUserType.Subcontracter):return dat.IsSubcontractorVisible;
//				case ((int)PMUserType.Builder):return dat.IsBuilderVisible;
//				default:return true;	
//			}
//			
//		}

        private void LoadNotCreatedCategories()
        {
            ddlSubCategories.Items.Clear();
            ProjectCategoriesVector pcv = ProjectCategoryDAL.LoadCollection(UIHelpers.GetPIDParam(), -1);
            DataTable dtSub = UsersData.SelectUsersPM(true, (int)PMUserType.Subcontracter, 0, UIHelpers.GetPIDParam(), -1, true, "").Table;
            //ProjectSubcontractersVector psv = ProjectSubcontracterDAL.LoadCollection("ProjectSubcontractersListProc1",SQLParms.CreateProjectSubcontractersListProc1(UIHelpers.GetPIDParam(),false));
            foreach (DataRow dr in dtSub.Rows)
            {
                bool _contain = false;
                foreach (ProjectCategoryData dat in pcv)
                {
                    if (dat.CategoryName == UIHelpers.CleanFolder(string.Concat((string)dr[(int)TableSub.SubType], " - ", (string)dr[(int)TableSub.FullName])))
                        _contain = true;
                }
                if (!_contain)
                {
                    ListItem li = new ListItem(string.Concat((string)dr[(int)TableSub.SubType], " - ", (string)dr[(int)TableSub.FullName]), string.Concat((int)PMUserType.Subcontracter, "_", dr[(int)TableSub.Number]));
                    if (SessionManager.Lang == Languages.EN)
                        li.Text = UIHelpers.ReplaceEN(li.Text);
                    ddlSubCategories.Items.Add(li);
                }
            }
            DataTable dtCl = UsersData.SelectUsersPM(true, (int)PMUserType.Client, 0, UIHelpers.GetPIDParam(), -1, true, "").Table;
            //ProjectClientsVector cvClients = ProjectClientDAL.LoadCollection("ProjectClientsSelByProjectProc",SQLParms.CreateProjectClientsSelByProjectProc(UIHelpers.GetPIDParam(),(int)ClientTypes.Client));
            foreach (DataRow dr in dtCl.Rows)
            {
                bool _contain = false;
                //ClientData cdat = ClientDAL.Load(psd.ClientID);
                foreach (ProjectCategoryData dat in pcv)
                {
                    if (dat.CategoryName == UIHelpers.CleanFolder((string)dr[(int)TableNotSub.FullName]))
                        _contain = true;
                }
                if (!_contain)
                {
                    ListItem li = new ListItem((string)dr[(int)TableNotSub.FullName], string.Concat((int)PMUserType.Client, "_", dr[(int)TableNotSub.Number]));
                    if (SessionManager.Lang == Languages.EN)
                        li.Text = UIHelpers.ReplaceEN(li.Text);
                    ddlSubCategories.Items.Add(li);
                }
            }
            DataTable dtBuild = UsersData.SelectUsersPM(true, (int)PMUserType.Builder, 0, UIHelpers.GetPIDParam(), -1, true, "").Table;
            //ProjectClientsVector cv = ProjectClientDAL.LoadCollection("ProjectClientsSelByProjectProc",SQLParms.CreateProjectClientsSelByProjectProc(UIHelpers.GetPIDParam(),(int)ClientTypes.Builder));
            foreach (DataRow dr in dtBuild.Rows)
            {
                bool _contain = false;
                foreach (ProjectCategoryData dat in pcv)
                {
                    if (dat.CategoryName == UIHelpers.CleanFolder((string)dr[(int)TableNotSub.FullName]))
                        _contain = true;
                }
                if (!_contain)
                {
                    ListItem li = new ListItem((string)dr[(int)TableNotSub.FullName], string.Concat((int)PMUserType.Client, "_", dr[(int)TableNotSub.Number]));
                    if (SessionManager.Lang == Languages.EN)
                        li.Text = UIHelpers.ReplaceEN(li.Text);
                    ddlSubCategories.Items.Add(li);
                }
            }
            ddlSubCategories.Items.Insert(0, new ListItem("", "0"));
            if (ddlSubCategories.Items.Count == 1)
                ddlSubCategories.Visible = btnCreateCategory.Visible = false;
            else
                ddlSubCategories.Visible = btnCreateCategory.Visible = true;
        }


		#endregion

		#region Event handlers

//		private void grdCategories_ItemCommand(object source, System.Web.UI.WebControls.DataGridCommandEventArgs e)
//		{
//			if (e.CommandName == "Sort")
//			{
//					return;
//			}
//			else if(e.CommandName=="Edit")
//			{
//				string sCategoryID = ((DataGrid)source).DataKeys[e.Item.ItemIndex].ToString();
//				Response.Redirect("Files.aspx"+"?cid="+sCategoryID+"&pid="+UIHelpers.GetPIDParam()); 
//				return;
//			}
//			ImageButton ib = (ImageButton)e.CommandSource;
//			string s = ib.ID;
//						switch (ib.ID)
//						{
//							case "btnDelete": 
//							{
//								int CategoryID = (int)((DataGrid)source).DataKeys[e.Item.ItemIndex];
//								ProjectCategoryDAL.Delete(CategoryID);
//								//log.Info(string.Format(Resource.ResourceManager["log_info_PM_DeleteCategory"],e.Item.Cells[(int)gridColomns.Name].Text,txtProjectName.Text,LoggedUser.Account));
//								BindGrid();
//								break;
//							}
////							case "btnEdit": 
////							{
////								string sProjectID = ((DataGrid)source).DataKeys[e.Item.ItemIndex].ToString();
////								break;
////							}
//								//Response.Redirect("EditProject.aspx"+"?pid="+sProjectID); break;
//			//				case "btnSubprojects": string sProjectID1 = ((DataGrid)source).DataKeys[e.Item.ItemIndex].ToString();
//			//					Response.Redirect("Subprojects.aspx"+"?pid="+sProjectID1); break;
//			//				case "btnProfile": string sProjectID2 = ((DataGrid)source).DataKeys[e.Item.ItemIndex].ToString();
//			//					Response.Redirect("Profile.aspx"+"?pid="+sProjectID2); break;
//			//				case "btnSubContracters": string sProjectID3 = ((DataGrid)source).DataKeys[e.Item.ItemIndex].ToString();
//			//					Response.Redirect("ProjectSubcontracters.aspx"+"?pid="+sProjectID3); break;
//							default:{return;}	
//								
//						}
//		}


//		private void grdCategories_ItemDataBound(object sender, System.Web.UI.WebControls.DataGridItemEventArgs e)
//		{
//			//			if(e.Item.ItemType==ListItemType.Item
//			//				|| e.Item.ItemType==ListItemType.AlternatingItem)
//			//			{
//			//				if(e.Item.Cells[(int)GridColumns.Active].Text=="0")
//			//				{
//			//					e.Item.CssClass="Inactive";
//			//	
//			//				}
//			//				else if(e.Item.Cells[(int)GridColumns.Concluded].Text=="1")
//			//				{
//			//					e.Item.CssClass="Concluded";
//			//	
//			//				}
//			//					
//			//				//else if(LoggedUser.HasPaymentRights && e.Item.Cells[(int)GridColumns.Black].Text=="1")
//			//				//{
//			//				//	e.Item.CssClass="Black";
//			//	
//			//				//}
//			//			}
//		}


        private void btnNew_Click(object sender, System.EventArgs e)
        {
            Response.Redirect("Files.aspx" + "?pid=" + UIHelpers.GetPIDParam());
        }

	
//		private void btnBack_Click(object sender, System.EventArgs e)
//		{
//			Response.Redirect("Projects.aspx");
//		}

        private void btnLogs_Click(object sender, System.EventArgs e)
        {
            Response.Redirect("Logs.aspx" + "?pid=" + UIHelpers.GetPIDParam());
        }


        private void back_click()
        {
            Response.Redirect("Projects.aspx");
        }


        private void dlCategories_ItemCommand(object source, System.Web.UI.WebControls.DataListCommandEventArgs e)
        {
            if (e.CommandName == "Edit")
            {
                string sCategoryID = ((DataList)source).DataKeys[e.Item.ItemIndex].ToString();
                Response.Redirect("Files.aspx" + "?cid=" + sCategoryID + "&pid=" + UIHelpers.GetPIDParam());
                return;
            }
            if (e.CommandSource is ImageButton)
            {
                ImageButton ib = (ImageButton)e.CommandSource;
                string s = ib.ID;
                switch (ib.ID)
                {
                    case "imgDelete":
                        {
                            int CategoryID = (int)((DataList)source).DataKeys[e.Item.ItemIndex];
                            ProjectCategoryDAL.Delete(CategoryID);
                            //log.Info(string.Format(Resource.ResourceManager["log_info_PM_DeleteCategory"], ((LinkButton)e.Item.FindControl("linkCategoryName")).Text, txtProjectName.Text, LoggedUser.Account));
                            BindGrid();
                            break;
                        }
                    case "imgActiveGroupNo":
                        {
                            int categoryID = (int)((DataList)source).DataKeys[e.Item.ItemIndex];
                            ProjectCategoryData datcat = ProjectCategoryDAL.Load(categoryID);
                            PMProjectUserAccesssVector vec = PMProjectUserAccessDAL.LoadCollectionForUser((int)PMUserType.Subcontracter, datcat.SubcontracterID, UIHelpers.GetPIDParam());
                            if (vec.Count > 0)
                            {
                                vec[0].IsActiveGroup = true;
                                PMProjectUserAccessDAL.Save(vec[0]);
                            }
                            //log.Info(string.Format(Resource.ResourceManager["log_info_PM_DeleteCategory"],((LinkButton)e.Item.FindControl("linkCategoryName")).Text,txtProjectName.Text,LoggedUser.Account));
                            BindGrid();
                            break;
                        }
                    case "imgActiveGroupYes":
                        {
                            int categoryID = (int)((DataList)source).DataKeys[e.Item.ItemIndex];
                            ProjectCategoryData datcat = ProjectCategoryDAL.Load(categoryID);
                            PMProjectUserAccesssVector vec = PMProjectUserAccessDAL.LoadCollectionForUser((int)PMUserType.Subcontracter, datcat.SubcontracterID, UIHelpers.GetPIDParam());
                            if (vec.Count > 0)
                            {
                                vec[0].IsActiveGroup = false;
                                PMProjectUserAccessDAL.Save(vec[0]);
                            }
                            //log.Info(string.Format(Resource.ResourceManager["log_info_PM_DeleteCategory"],((LinkButton)e.Item.FindControl("linkCategoryName")).Text,txtProjectName.Text,LoggedUser.Account));
                            BindGrid();
                            break;
                        }
                }
            }
            if (e.CommandSource is Button)
            {
                Button ib = (Button)e.CommandSource;
                string s = ib.ID;
                switch (ib.ID)
                {
                    case "btnPlus":
                        {
                            int nIndex = e.Item.ItemIndex;
                            btnPlus_Click(nIndex);
                            break;
                        }
                    case "btnMinus":
                        {
                            int nIndex = e.Item.ItemIndex;
                            btnMinus_Click(nIndex);
                            break;
                        }
                }
            }
        }

        private void dlCategoriesCopy_ItemCommand(object source, System.Web.UI.WebControls.DataListCommandEventArgs e)
        {
            if (e.CommandName == "Edit")
            {
                string sCategoryID = ((DataList)source).DataKeys[e.Item.ItemIndex].ToString();
                Response.Redirect("Files.aspx" + "?cid=" + sCategoryID + "&pid=" + UIHelpers.GetPIDParam());
                return;
            }
            if (e.CommandSource is ImageButton)
            {
                ImageButton ib = (ImageButton)e.CommandSource;
                string s = ib.ID;
                switch (ib.ID)
                {
                    case "imgDeleteCopy":
                        {
                            int CategoryID = (int)((DataList)source).DataKeys[e.Item.ItemIndex];
                            ProjectCategoryDAL.Delete(CategoryID);
                            log.Info(string.Format(Resource.ResourceManager["log_info_PM_DeleteCategory"], ((LinkButton)e.Item.FindControl("linkCategoryNameCopy")).Text, txtProjectName.Text, LoggedUser.Account));
                            BindGrid();
                            break;
                        }
                    case "imgActiveGroupNoCopy":
                        {
                            int categoryID = (int)((DataList)source).DataKeys[e.Item.ItemIndex];
                            ProjectCategoryData datcat = ProjectCategoryDAL.Load(categoryID);
                            PMProjectUserAccesssVector vec = PMProjectUserAccessDAL.LoadCollectionForUser((int)PMUserType.Subcontracter, datcat.SubcontracterID, UIHelpers.GetPIDParam());
                            if (vec.Count > 0)
                            {
                                vec[0].IsActiveGroup = true;
                                PMProjectUserAccessDAL.Save(vec[0]);
                            }
                            //log.Info(string.Format(Resource.ResourceManager["log_info_PM_DeleteCategory"],((LinkButton)e.Item.FindControl("linkCategoryName")).Text,txtProjectName.Text,LoggedUser.Account));
                            BindGrid();
                            break;
                        }
                    case "imgActiveGroupYesCopy":
                        {
                            int categoryID = (int)((DataList)source).DataKeys[e.Item.ItemIndex];
                            ProjectCategoryData datcat = ProjectCategoryDAL.Load(categoryID);
                            PMProjectUserAccesssVector vec = PMProjectUserAccessDAL.LoadCollectionForUser((int)PMUserType.Subcontracter, datcat.SubcontracterID, UIHelpers.GetPIDParam());
                            if (vec.Count > 0)
                            {
                                vec[0].IsActiveGroup = false;
                                PMProjectUserAccessDAL.Save(vec[0]);
                            }
                            //log.Info(string.Format(Resource.ResourceManager["log_info_PM_DeleteCategory"],((LinkButton)e.Item.FindControl("linkCategoryName")).Text,txtProjectName.Text,LoggedUser.Account));
                            BindGrid();
                            break;
                        }
                }
            }
            if (e.CommandSource is Button)
            {
                Button ib = (Button)e.CommandSource;
                string s = ib.ID;
                switch (ib.ID)
                {
                    case "btnPlusCopy":
                        {
                            int nIndex = e.Item.ItemIndex;
                            btnPlusCopy_Click(nIndex);
                            break;
                        }
                    case "btnMinusCopy":
                        {
                            int nIndex = e.Item.ItemIndex;
                            btnMinusCopy_Click(nIndex);
                            break;
                        }
                }
            }
        }

        private void dlCategories_ItemCreated(object sender, System.Web.UI.WebControls.DataListItemEventArgs e)
        {

            DataGrid grdFiles = (DataGrid)e.Item.FindControl("grdFiles");
            if (grdFiles != null)
                grdFiles.ItemCommand += new System.Web.UI.WebControls.DataGridCommandEventHandler(this.grdFiles_ItemCommand);

            grdFiles.Columns[(int)gridColomns.Subject].HeaderText = UIHelpers.GetText(Language, "Name_FileSubjectColomn");
            grdFiles.Columns[(int)gridColomns.Version].HeaderText = UIHelpers.GetText(Language, "Name_VersionColomn");
            grdFiles.Columns[(int)gridColomns.Creator].HeaderText = UIHelpers.GetText(Language, "Name_CreatorColomn");
            //grdFiles.Columns[(int)gridColomns.Minutes].HeaderText  = UIHelpers.GetText(Language,"Name_FileMinutesColomn");
            grdFiles.Columns[(int)gridColomns.FileName].HeaderText = UIHelpers.GetText(Language, "Name_SubjectColomn");
            grdFiles.Columns[(int)gridColomns.Date].HeaderText = UIHelpers.GetText(Language, "Name_DateColomn");

            ImageButton ib = (ImageButton)e.Item.FindControl("imgDelete");
            SetConfirmDelete(ib, Resource.ResourceManager["the_directory"]);

            ib = (ImageButton)e.Item.FindControl("imgActiveGroupYes");
            ib.ToolTip = GetTextYes();
            ib = (ImageButton)e.Item.FindControl("imgActiveGroupNo");
            ib.ToolTip = GetTextNo();
        }

        private void dlCategoriesCopy_ItemCreated(object sender, System.Web.UI.WebControls.DataListItemEventArgs e)
        {
            DataGrid grdFiles = (DataGrid)e.Item.FindControl("grdFilesCopy");
            if (grdFiles != null)
                grdFiles.ItemCommand += new System.Web.UI.WebControls.DataGridCommandEventHandler(this.grdFilesCopy_ItemCommand);
            grdFiles.Columns[(int)gridColomns.Subject].HeaderText = UIHelpers.GetText(Language, "Name_FileSubjectColomn");
            grdFiles.Columns[(int)gridColomns.Version].HeaderText = UIHelpers.GetText(Language, "Name_VersionColomn");
            grdFiles.Columns[(int)gridColomns.Creator].HeaderText = UIHelpers.GetText(Language, "Name_CreatorColomn");
            //grdFiles.Columns[(int)gridColomns.Minutes].HeaderText  = UIHelpers.GetText(Language,"Name_FileMinutesColomn");
            grdFiles.Columns[(int)gridColomns.FileName].HeaderText = UIHelpers.GetText(Language, "Name_SubjectColomn");
            grdFiles.Columns[(int)gridColomns.Date].HeaderText = UIHelpers.GetText(Language, "Name_DateColomn");
            ImageButton ib = (ImageButton)e.Item.FindControl("imgDeleteCopy");
            SetConfirmDelete(ib, Resource.ResourceManager["the_directory"]);
            if (LoggedUser.IsClient || LoggedUser.IsBuilder)
            {
                grdFiles.Columns[(int)gridColomns.Edit].Visible = false;
                grdFiles.Columns[(int)gridColomns.Creator].Visible = false;
                grdFiles.Columns[(int)gridColomns.Delete].Visible = false;
                grdFiles.Columns[(int)gridColomns.Subject].Visible = false;
                grdFiles.Columns[(int)gridColomns.Type].Visible = false;
                //grdFiles.Columns[(int)gridColomns.Minutes].Visible  = false;
                grdFiles.Columns[(int)gridColomns.Date].Visible = false;

            }

            ib = (ImageButton)e.Item.FindControl("imgActiveGroupYesCopy");
            ib.ToolTip = GetTextYes();
            ib = (ImageButton)e.Item.FindControl("imgActiveGroupNoCopy");
            ib.ToolTip = GetTextNo();
        }

        private void btnPlus_Click(int index)
        {
            Button btnPlus = (Button)dlCategories.Items[index].FindControl("btnPlus");
            if (btnPlus != null)
                btnPlus.Visible = false;
            Button btnMinus = (Button)dlCategories.Items[index].FindControl("btnMinus");
            if (btnMinus != null)
                btnMinus.Visible = true;
            DataGrid grdFiles = (DataGrid)dlCategories.Items[index].FindControl("grdFiles");
            if (grdFiles != null)
                grdFiles.Visible = true;

        }

        private void btnMinus_Click(int index)
        {
            Button btnPlus = (Button)dlCategories.Items[index].FindControl("btnPlus");
            if (btnPlus != null)
                btnPlus.Visible = true;
            Button btnMinus = (Button)dlCategories.Items[index].FindControl("btnMinus");
            if (btnMinus != null)
                btnMinus.Visible = false;
            DataGrid grdFiles = (DataGrid)dlCategories.Items[index].FindControl("grdFiles");
            if (grdFiles != null)
                grdFiles.Visible = false;
        }

        private void grdFiles_ItemCommand(object source, System.Web.UI.WebControls.DataGridCommandEventArgs e)
        {
            if (e.CommandName == "Sort")
            {
                int sortOrder = SortOrder;
                switch ((string)e.CommandArgument)
                {
                    case "FileNameSave": if (sortOrder == 1) sortOrder = -1; else sortOrder = 1;
                        SortOrder = sortOrder;
                        BindGrid(); break;
                    case "FileSubject": if (sortOrder == 2) sortOrder = -2; else sortOrder = 2;
                        SortOrder = sortOrder;
                        BindGrid(); break;
                    case "FileVersion": if (sortOrder == 3) sortOrder = -3; else sortOrder = 3;
                        SortOrder = sortOrder;
                        BindGrid(); break;
                    case "CreatedBy": if (sortOrder == 4) sortOrder = -4; else sortOrder = 4;
                        SortOrder = sortOrder;
                        BindGrid(); break;
                    case "DateCreated": if (sortOrder == 5) sortOrder = -5; else sortOrder = 5;
                        SortOrder = sortOrder;
                        BindGrid(); break;
                }
                int indexOfList = ((DataListItem)e.Item.Parent.Parent.Parent).ItemIndex;
                btnPlus_Click(indexOfList);

                return;
            }
            if (e.CommandName == "Edit")
            {
                int ID = (int)((DataGrid)source).DataKeys[e.Item.ItemIndex];
                int index = ((DataListItem)((e.Item.Parent).Parent).Parent).ItemIndex;
                Label lb = (Label)dlCategories.Items[index].FindControl("lbCategoryName");
                string sCategoryName = lb.Text;
                if (sCategoryName.Length != 0)
                {
                    string filePath = /*Server.MapPath(*/string.Concat(System.Configuration.ConfigurationManager.AppSettings["FilePath"], @"\", txtProjectName.Text, @"\", sCategoryName, @"\", e.Item.Cells[(int)gridColomns.FileNameUnvisible].Text, e.Item.Cells[(int)gridColomns.Type].Text);//);

                    Response.Cache.SetExpires(DateTime.Now.AddSeconds(-1));
                    Response.Cache.SetCacheability(HttpCacheability.Private);
                    Response.AppendHeader("content-disposition", "attachment; filename=" + e.Item.Cells[(int)gridColomns.FileNameUnvisible].Text + e.Item.Cells[(int)gridColomns.Type].Text);
                    //Response.AppendHeader("content-disposition", "attachment; filename="+ID+e.Item.Cells[(int)gridColomns.Type].Text);

                    Response.Charset = "";
                    Response.TransmitFile(filePath);
                    //notes:here send message if file is tracking
                    FilePMData fPMd = FilePMDAL.Load(ID);
                    if (fPMd.MailForDownload)
                    {
                        UserInfoPM MailToUser = UsersData.SelectUserPMByAccount(fPMd.CreatedBy);
                        MailBO.PMSendMailDownload(LoggedUser.Account, fPMd, MailToUser.Mail);
                    }

                    //notes:Save Log in DateBase
                    ProjectLogData pld = new ProjectLogData(-1, ID, DateTime.Now, false, LoggedUser.FullName);
                    ProjectLogDAL.Save(pld);
                    Response.End();
                    return;
                }
            }

        }

        private void grdFilesCopy_ItemCommand(object source, System.Web.UI.WebControls.DataGridCommandEventArgs e)
        {

            if (e.CommandName == "Sort")
            {
                int sortOrder = SortOrder;
                switch ((string)e.CommandArgument)
                {
                    case "FileNameSave": if (sortOrder == 1) sortOrder = -1; else sortOrder = 1;
                        SortOrder = sortOrder;
                        BindGrid(); break;
                    case "FileSubject": if (sortOrder == 2) sortOrder = -2; else sortOrder = 2;
                        SortOrder = sortOrder;
                        BindGrid(); break;
                    case "FileVersion": if (sortOrder == 3) sortOrder = -3; else sortOrder = 3;
                        SortOrder = sortOrder;
                        BindGrid(); break;
                    case "CreatedBy": if (sortOrder == 4) sortOrder = -4; else sortOrder = 4;
                        SortOrder = sortOrder;
                        BindGrid(); break;
                    case "DateCreated": if (sortOrder == 5) sortOrder = -5; else sortOrder = 5;
                        SortOrder = sortOrder;
                        BindGrid(); break;
                }
                int indexOfList = ((DataListItem)e.Item.Parent.Parent.Parent).ItemIndex;
                btnPlusCopy_Click(indexOfList);

                return;
            }
            if (e.CommandName == "Edit")
            {
                int ID = (int)((DataGrid)source).DataKeys[e.Item.ItemIndex];
                int index = ((DataListItem)((e.Item.Parent).Parent).Parent).ItemIndex;
                Label lb = (Label)dlCategoriesCopy.Items[index].FindControl("lbCategoryNameCopy");
                string sCategoryName = lb.Text;
                if (sCategoryName.Length != 0)
                {
                    string filePath = /*Server.MapPath(*/string.Concat(System.Configuration.ConfigurationManager.AppSettings["FilePath"], @"\", txtProjectName.Text, @"\", sCategoryName, @"\", e.Item.Cells[(int)gridColomns.FileNameUnvisible].Text, e.Item.Cells[(int)gridColomns.Type].Text);//);

                    Response.Cache.SetExpires(DateTime.Now.AddSeconds(-1));
                    Response.Cache.SetCacheability(HttpCacheability.Private);
                    Response.AppendHeader("content-disposition", "attachment; filename=" + e.Item.Cells[(int)gridColomns.FileNameUnvisible].Text + e.Item.Cells[(int)gridColomns.Type].Text);
                    //Response.AppendHeader("content-disposition", "attachment; filename="+ID+e.Item.Cells[(int)gridColomns.Type].Text);

                    Response.Charset = "";
                    Response.TransmitFile(filePath);
                    //notes:here send message if file is tracking
                    FilePMData fPMd = FilePMDAL.Load(ID);
                    if (fPMd.MailForDownload)
                    {
                        UserInfoPM MailToUser = UsersData.SelectUserPMByAccount(fPMd.CreatedBy);
                        MailBO.PMSendMailDownload(LoggedUser.Account, fPMd, MailToUser.Mail);
                    }

                    //notes:Save Log in DateBase
                    ProjectLogData pld = new ProjectLogData(-1, ID, DateTime.Now, false, LoggedUser.FullName);
                    ProjectLogDAL.Save(pld);
                    Response.End();
                    return;
                }
            }

        }

        private void btnPlusAll_Click(object sender, System.EventArgs e)
        {
            for (int i = 0; i < dlCategories.Items.Count; i++)
                btnPlus_Click(i);
        }

        private void btnMinusAll_Click(object sender, System.EventArgs e)
        {
            for (int i = 0; i < dlCategories.Items.Count; i++)
                btnMinus_Click(i);
        }

        private void btnPlusAllCopy_Click(object sender, System.EventArgs e)
        {
            for (int i = 0; i < dlCategoriesCopy.Items.Count; i++)
                btnPlusCopy_Click(i);
        }

        private void btnMinusAllCopy_Click(object sender, System.EventArgs e)
        {
            for (int i = 0; i < dlCategoriesCopy.Items.Count; i++)
                btnMinusCopy_Click(i);
        }

        private void btnPlusCopy_Click(int index)
        {
            Button btnPlus = (Button)dlCategoriesCopy.Items[index].FindControl("btnPlusCopy");
            if (btnPlus != null)
                btnPlus.Visible = false;
            Button btnMinus = (Button)dlCategoriesCopy.Items[index].FindControl("btnMinusCopy");
            if (btnMinus != null)
                btnMinus.Visible = true;
            DataGrid grdFiles = (DataGrid)dlCategoriesCopy.Items[index].FindControl("grdFilesCopy");
            if (grdFiles != null)
                grdFiles.Visible = true;

        }

        private void btnMinusCopy_Click(int index)
        {
            Button btnPlus = (Button)dlCategoriesCopy.Items[index].FindControl("btnPlusCopy");
            if (btnPlus != null)
                btnPlus.Visible = true;
            Button btnMinus = (Button)dlCategoriesCopy.Items[index].FindControl("btnMinusCopy");
            if (btnMinus != null)
                btnMinus.Visible = false;
            DataGrid grdFiles = (DataGrid)dlCategoriesCopy.Items[index].FindControl("grdFilesCopy");
            if (grdFiles != null)
                grdFiles.Visible = false;
        }

        private void btnCreateCategory_Click(object sender, System.EventArgs e)
        {
            string sValue = ddlSubCategories.SelectedValue;
            if (sValue == "0")
                return;
            int index = sValue.IndexOf("_");
            int iType = UIHelpers.ToInt(sValue.Substring(0, index));
            int iID = UIHelpers.ToInt(sValue.Substring(index + 1));
            if (iType == (int)PMUserType.Subcontracter)
            {
                SubcontracterData dat = SubcontracterDAL.Load(iID);
                if (Exists(UIHelpers.CleanFolder(dat.SubcontracterType + " - " + dat.SubcontracterName)))
                {
                    lblError.Text = Resource.ResourceManager["Category_Error_Exists"];
                    return;
                }
                ProjectCategoryData category = new ProjectCategoryData(-1, UIHelpers.GetPIDParam(), UIHelpers.CleanFolder(dat.SubcontracterType + " - " + dat.SubcontracterName), true, -1, dat.SubcontracterID, false);
                ProjectCategoryDAL.Save(category);
            }
            else
            {
                ClientData dat = ClientDAL.Load(iID);
                if (Exists(UIHelpers.CleanFolder(dat.ClientName)))
                {
                    lblError.Text = Resource.ResourceManager["Category_Error_Exists"];
                    return;
                }
                ProjectCategoryData category = new ProjectCategoryData(-1, UIHelpers.GetPIDParam(), UIHelpers.CleanFolder(dat.ClientName), true, dat.ClientID, -1, false);
                ProjectCategoryDAL.Save(category);
            }
            //LoadNotCreatedCategories();
            BindGrid();
        }
        private bool Exists(string name)
        {
            ProjectCategoriesVector pcv = ProjectCategoryDAL.LoadCollection(UIHelpers.GetPIDParam(), -1);
            foreach (ProjectCategoryData pcd in pcv)
                if (pcd.CategoryName == name)
                    return true;
            return false;
        }
        private void imgBack_Click(object sender, System.Web.UI.ImageClickEventArgs e)
        {
            back_click();
        }
        private void link_Click(object sender, System.EventArgs e)
        {
            back_click();
        }


        private void grdProfile_ItemDataBound(object sender, System.Web.UI.WebControls.DataGridItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Item
                || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                e.Item.Cells[(int)gridProfile.TypeVisible].Text = UIHelpers.GetProfileText(UIHelpers.ToInt(e.Item.Cells[(int)gridProfile.TypeUnvisible].Text), Language);
                ImageButton ib = (ImageButton)e.Item.FindControl("btnScan");
                ib.ToolTip = e.Item.Cells[(int)gridProfile.Discription].Text;

            }
        }
        private void grdProfile_ItemCommand(object source, System.Web.UI.WebControls.DataGridCommandEventArgs e)
        {
            if (e.CommandName == "pdfShow")
            {
                int ID = UIHelpers.ToInt(e.Item.Cells[(int)gridProfile.ID].Text);
                string filePath = /*Server.MapPath(*/string.Concat(System.Configuration.ConfigurationManager.AppSettings["AsaProjectProfile"], ID, System.Configuration.ConfigurationManager.AppSettings["ExtensionASA"]);

                Response.Cache.SetExpires(DateTime.Now.AddSeconds(-1));
                Response.Cache.SetCacheability(HttpCacheability.Private);
                Response.AppendHeader("content-disposition", "attachment; filename=" + ID.ToString() + System.Configuration.ConfigurationManager.AppSettings["ExtensionASA"]);
                //Response.AppendHeader("content-disposition", "attachment; filename="+ID+e.Item.Cells[(int)gridColomns.Type].Text);

                Response.Charset = "";
                Response.TransmitFile(filePath);
                Response.End();
                return;
            }

        }

        private void CheckBoxProfileAccess_CheckedChanged(object source, System.EventArgs e)
        {

            CheckBox cb = (CheckBox)source;
            int index = ((DataGridItem)cb.Parent.Parent).ItemIndex;
            int ID = UIHelpers.ToInt(grdProfile.Items[index].Cells[(int)gridProfile.ID].Text);
            ProjectDocumentData dat = ProjectDocumentDAL.Load(ID);
            switch (cb.ID)
            {
                case "imgSubcontracter": { dat.IsSubcontracterVisible = cb.Checked; ProjectDocumentDAL.Save(dat); return; }
                case "imgClient": { dat.IsClientVisible = cb.Checked; ProjectDocumentDAL.Save(dat); return; }
                case "imgBuilder": { dat.IsBuilderVisible = cb.Checked; ProjectDocumentDAL.Save(dat); return; }
            }

        }
	
		private System.Drawing.Color colAccYew = System.Drawing.Color.DarkOrange;
		private System.Drawing.Color colAccNo = System.Drawing.Color.Gray;

        private void dlCategories_ItemDataBound(object sender, System.Web.UI.WebControls.DataListItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Item
                || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                Label lbSubContracterID = (Label)e.Item.FindControl("lbSubID");
                LinkButton link = (LinkButton)e.Item.FindControl("linkCategoryName");

                Label lbID = (Label)e.Item.FindControl("lbID");

                if (UIHelpers.ToInt(lbID.Text) > 0 && link != null)
                {
                    FilesPMVector fPMv = FilePMDAL.LoadCollectionByCategory(UIHelpers.ToInt(lbID.Text), LoggedUser.UserType, SortOrder);
                    if (fPMv.Count > 0)
                        link.Font.Bold = true;
                }
                if (UIHelpers.ToInt(lbSubContracterID.Text) > 0 && link != null && lbSubContracterID != null)
                {
                    PMProjectUserAccesssVector vec = PMProjectUserAccessDAL.LoadCollectionForUser((int)PMUserType.Subcontracter, UIHelpers.ToInt(lbSubContracterID.Text), UIHelpers.GetPIDParam());
                    if (vec.Count > 0)
                        if (vec[0].IsActiveGroup) link.ForeColor = colAccYew;//return false;
                        else link.ForeColor = colAccNo;//return true;
                    else
                    {
                        SubcontracterData datsub = SubcontracterDAL.Load(UIHelpers.ToInt(lbSubContracterID.Text));
                        if (datsub == null)
                            link.ForeColor = colAccYew;//return false;
                        PMProjectUserAccessData datpmp = new PMProjectUserAccessData(-1, (int)PMUserType.Subcontracter, datsub.SubcontracterID, UIHelpers.GetPIDParam(), true, datsub.IsActiveGroup, string.Empty);
                        PMProjectUserAccessDAL.Save(datpmp);
                        if (!(datsub.IsActiveGroup)) link.ForeColor = colAccNo;//return true;
                        else link.ForeColor = colAccYew;//return false;
                    }
                }

            }
        }
        private void dlCategoriesCopy_ItemDataBound(object sender, System.Web.UI.WebControls.DataListItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Item
                || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                Label lbSubContracterID = (Label)e.Item.FindControl("lbSubIDCopy");
                Label lbID = (Label)e.Item.FindControl("lbIDCopy");
                LinkButton link = (LinkButton)e.Item.FindControl("linkCategoryNameCopy");
                if (UIHelpers.ToInt(lbID.Text) > 0 && link != null)
                {
                    FilesPMVector fPMv = FilePMDAL.LoadCollectionByCategory(UIHelpers.ToInt(lbID.Text), LoggedUser.UserType, SortOrder);
                    if (fPMv.Count > 0)
                        link.Font.Bold = true;
                }
                if (UIHelpers.ToInt(lbSubContracterID.Text) > 0 && link != null && lbSubContracterID != null)
                {
                    PMProjectUserAccesssVector vec = PMProjectUserAccessDAL.LoadCollectionForUser((int)PMUserType.Subcontracter, UIHelpers.ToInt(lbSubContracterID.Text), UIHelpers.GetPIDParam());
                    if (vec.Count > 0)
                        if (vec[0].IsActiveGroup) link.ForeColor = colAccYew;//return false;
                        else link.ForeColor = colAccNo;//return true;
                    else
                    {
                        SubcontracterData datsub = SubcontracterDAL.Load(UIHelpers.ToInt(lbSubContracterID.Text));
                        if (datsub == null)
                            link.ForeColor = colAccYew;//return false;
                        PMProjectUserAccessData datpmp = new PMProjectUserAccessData(-1, (int)PMUserType.Subcontracter, datsub.SubcontracterID, UIHelpers.GetPIDParam(), true, datsub.IsActiveGroup, string.Empty);
                        PMProjectUserAccessDAL.Save(datpmp);
                        if (!(datsub.IsActiveGroup)) link.ForeColor = colAccNo;//return true;
                        else link.ForeColor = colAccYew;//return false;
                    }
                }

            }
        }




        private void grdProfile_ItemCreated(object sender, System.Web.UI.WebControls.DataGridItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Item
                || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                CheckBox cb = (CheckBox)e.Item.FindControl("imgSubcontracter");
                cb.CheckedChanged += new System.EventHandler(this.CheckBoxProfileAccess_CheckedChanged);
                cb = (CheckBox)e.Item.FindControl("imgClient");
                cb.CheckedChanged += new System.EventHandler(this.CheckBoxProfileAccess_CheckedChanged);
                cb = (CheckBox)e.Item.FindControl("imgBuilder");
                cb.CheckedChanged += new System.EventHandler(this.CheckBoxProfileAccess_CheckedChanged);


            }
        }
		#endregion
	}
}
