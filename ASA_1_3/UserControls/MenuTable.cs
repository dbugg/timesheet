using System;
using System.Web;
using System.Web.UI.WebControls;
using Asa.Timesheet.Data.Entities;
using log4net;
using log4net.Config;

namespace Asa.Timesheet.WebPages.UserControls
{
	/// <summary>
	/// Summary description for MenuTable.
	/// </summary>
	public class MenuTable : System.Web.UI.WebControls.Table
	{
		public const string _ASPX=".aspx";
		private static readonly ILog log = LogManager.GetLogger(typeof(MenuTable));
		private static readonly bool isDebugEnabled = log.IsDebugEnabled;
		public MenuTable()
		{
			this.CssClass = "menuTable";
			this.CellSpacing = 1;
			this.BorderWidth = 0;
			this.Width = Unit.Percentage(100);
		}

		public void AddGroupHeader(string headerName)
		{
			TableRow tr = new TableRow();
			TableCell tc = new TableCell();
			tr = new TableRow();
			tc = new TableCell();
			tc.Style.Add("FONT-WEIGHT", "bold");
			tc.Style.Add("FONT-SIZE", "10pt");
			tc.Style.Add("COLOR", "black");
			tc.Style.Add("FONT-FAMILY", "Verdana");
			tc.Style.Add("HEIGHT", "8px");
			tc.Text = "&nbsp;&nbsp;"+headerName;
			tr.Cells.Add(tc);
			this.Rows.Add(tr);
		}

		public void AddMenuItem(string linkName, string navigateUrl, bool NewWindow, Pages p)
		{
			string appPath = HttpContext.Current.Request.ApplicationPath;

			TableRow tr = new TableRow();
			TableCell tc = new TableCell();
			tr.Cells.Add(tc);
			
			string sTarget="";
			if(p!=Pages.Undefined)
				navigateUrl+="#"+p.ToString();
			if(NewWindow)
				sTarget=" target= _blank ";
			tc.Text = 
			"<a" +sTarget + " href =  \""+ navigateUrl+ "\">"+ linkName +"</a>";
			this.Rows.Add(tr);
		}

		public void AddMenuItem(MenuItemInfo item)
		{
			string appPath = HttpContext.Current.Request.ApplicationPath;

			TableRow tr = new TableRow();
			TableCell tc = new TableCell();

			tr.Cells.Add(tc);
		
			string sTarget="";
			if(item!=null && item.PreviousPage!=null && item.PreviousPage!="")
				item.NavigateUrl+="#"+item.PreviousPage.ToString();
			if(item.NewWindow)
				sTarget=" target= _blank ";
			string cssClass = (item.Highlighted) ? " class = highlightedMenuItem " : " class = menuItem ";
            string licssClass = (item.Highlighted) ? " class = highlightedMenuItem " : " class = menuItem ";

			tc.Text = 
				"<li" +licssClass + "><a" +sTarget  + " href =  \""+ item.NavigateUrl+ "\">"+ item.Title +"</a></li>";
			this.Rows.Add(tr);
		}

		public void AddVerticalBlank(int pixels)
		{
			TableRow tr = new TableRow();
			TableCell tc = new TableCell();
			tc.Style.Add("HEIGHT", pixels.ToString());
			tr.Cells.Add(tc);
			this.Rows.Add(tr);
		}

		public void AddHLine()
		{
			string appPath = HttpContext.Current.Request.ApplicationPath;
			TableRow tr = new TableRow();
			TableCell tc = new TableCell();
			tc.HorizontalAlign = HorizontalAlign.Center;
			tc.Text = "<IMG style=\"WIDTH: 100px; HEIGHT: 1px\" height=\"1\" alt=\"\" src=\""+appPath+"/images/menuline.bmp\" width=\"88\">";
			tr.Cells.Add(tc);
			this.Rows.Add(tr);
		}
		
		public void AddMenuGroup(string groupName, int topBlank, System.Collections.ArrayList menuItems)
		{
			if (menuItems.Count == 0) return;

			AddVerticalBlank(topBlank);
			AddGroupHeader(groupName);
			AddHLine();

			for (int i=0; i<menuItems.Count; i++)
			{
				MenuItemInfo mi = (MenuItemInfo)menuItems[i];
				//AddMenuItem(mi.Title, mi.NavigateUrl, mi.NewWindow, mi.PreviousPage);	
				AddMenuItem(mi);
			}

			AddHLine();
		}
	}
}
