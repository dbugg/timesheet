<%@ Page language="c#" Codebehind="Subprojects.aspx.cs" AutoEventWireup="false" Inherits="Asa.Timesheet.WebPages.Subprojects" %>

<%@ Register TagPrefix="uc1" TagName="PageHeader" Src="UserControls/PageHeader.ascx" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
	<HEAD>
		<title>Subprojects</title>
		<meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
		<meta content="C#" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<LINK href="styles/timesheet.css" type="text/css" rel="stylesheet">
	</HEAD>
	<body bottomMargin="0" leftMargin="0" topMargin="0" rightMargin="0">
		<form id="Form1" method="post" runat="server">
			<table height="100%" cellSpacing="0" cellPadding="0" width="100%" border="0">
				<tr height="1">
					<td>
						<uc1:PageHeader id="header" runat="server"></uc1:PageHeader>
					</td>
				</tr>
				<TR height="1">
					<td noWrap background="images/line.gif" height="1"></td>
				</TR>
				<tr>
					<td>
						<table height="100%" cellSpacing="0" cellPadding="0" width="100%" border="0">
							<tr>
								<td style="BACKGROUND-IMAGE: url(images/left110.gif)" vAlign="top" noWrap width="110">
									<asp:PlaceHolder id="menuHolder" runat="server"></asp:PlaceHolder>
								</td>
								<td noWrap width="1" bgColor="lightgrey"></td>
								<td style="PADDING-RIGHT: 5px; PADDING-LEFT: 5px; PADDING-TOP: 3px" vAlign="top" noWrap><br>
									<asp:label id="lblError" runat="server" ForeColor="Red" EnableViewState="False" CssClass="ErrorLabel"></asp:label><asp:label id="lblInfo" runat="server" EnableViewState="False" CssClass="InfoLabel"></asp:label><br>
									<br>
									<asp:panel id="grid" style="BORDER-RIGHT: #d2b48c 2px solid; PADDING-RIGHT: 2px; BORDER-TOP: #d2b48c 2px solid; OVERFLOW-Y: scroll; PADDING-LEFT: 2px; OVERFLOW-X: auto; BORDER-LEFT: #d2b48c 2px solid; PADDING-TOP: 2px; BORDER-BOTTOM: #d2b48c 2px solid"
										runat="server" Height="450px" Width="100%">
										<asp:datagrid id="grdProjects" runat="server" CssClass="Grid" Width="100%" AllowSorting="False"
											PageSize="2" CellPadding="4" AutoGenerateColumns="False">
											<AlternatingItemStyle CssClass="GridAltItem" BackColor="WhiteSmoke"></AlternatingItemStyle>
											<ItemStyle CssClass="GridItem"></ItemStyle>
											<HeaderStyle Wrap="False" CssClass="GridHeader"></HeaderStyle>
											<Columns>
											<asp:TemplateColumn HeaderText="#">
													<ItemStyle Width="10px" ForeColor="DimGray"></ItemStyle>
													<ItemTemplate>
														<asp:Label  CssClass="menuTable" runat="server" Text='<%# (int)DataBinder.Eval(Container, "ItemIndex")+1 %>' ID="Label1" NAME="Label1">
														</asp:Label>
													</ItemTemplate>
												</asp:TemplateColumn>
												<asp:TemplateColumn HeaderText="Фаза" SortExpression="SubprojectType">
													<ItemTemplate>
														<asp:LinkButton id="lnk" runat="server" CssClass="menuTable" ForeColor="#804000" CommandName="Edit">
															<span Class="menuTable">
																<%# DataBinder.Eval(Container, "DataItem.SubprojectType") %>
															</span>
														</asp:LinkButton>
													</ItemTemplate>
												</asp:TemplateColumn>
												<asp:BoundColumn DataField="ProjectName" SortExpression="Name" HeaderText="Име на проект">
													<HeaderStyle Width="15%"></HeaderStyle>
												</asp:BoundColumn>
												<asp:BoundColumn DataField="ProjectCode" SortExpression="Code" HeaderText="Код на проект">
													<HeaderStyle Width="50px"></HeaderStyle>
													<ItemStyle ForeColor="DimGray"></ItemStyle>
												</asp:BoundColumn>
												<asp:TemplateColumn HeaderText="Нач.дата">
													<ItemTemplate>
														<asp:Label runat="server" Text='<%# Asa.Timesheet.Data.Util.TimeHelper.FormatDate((DateTime)DataBinder.Eval(Container, "DataItem.StartDate")) %>'>
														</asp:Label>
													</ItemTemplate>
												</asp:TemplateColumn>
												<asp:TemplateColumn HeaderText="Крайна.дата">
													<ItemTemplate>
														<asp:Label runat="server" Text='<%# Asa.Timesheet.Data.Util.TimeHelper.FormatDate((DateTime)DataBinder.Eval(Container, "DataItem.EndDate")) %>'>
														</asp:Label>
													</ItemTemplate>
												</asp:TemplateColumn>
												<asp:TemplateColumn>
													<HeaderStyle Wrap="False" Width="20px"></HeaderStyle>
													<ItemStyle Wrap="False" HorizontalAlign="Center"></ItemStyle>
													<ItemTemplate>
														<asp:ImageButton id="btnEdit" runat="server" ToolTip="Редактирай" ImageUrl="images/edit1.gif"></asp:ImageButton>
													</ItemTemplate>
												</asp:TemplateColumn>
												<asp:TemplateColumn>
													<HeaderStyle Wrap="False" Width="20px"></HeaderStyle>
													<ItemStyle Wrap="False" HorizontalAlign="Center"></ItemStyle>
													<ItemTemplate>
														<asp:ImageButton id="btnDelete" runat="server" Width="12px" Height="12px" ToolTip="Изтрий" ImageUrl="images/delete.gif"></asp:ImageButton>
													</ItemTemplate>
												</asp:TemplateColumn>
											</Columns>
										</asp:datagrid>
									</asp:panel>
									<TABLE id="Table4" style="BORDER-RIGHT: tan thin solid; BORDER-TOP: tan thin solid; BORDER-LEFT: tan thin solid; BORDER-BOTTOM: tan thin solid"
										cellSpacing="0" cellPadding="4" width="100%" border="0">
										<TR>
											<TD><asp:button id="btnNewProject" runat="server" CssClass="ActionButton" Width="200px"></asp:button>&nbsp;&nbsp;
												<asp:button id="btnSubc" runat="server" CssClass="ActionButton" Width="216px" Text="Подизпълнители"></asp:button>&nbsp;
												<asp:button id="btnProject" runat="server" CssClass="ActionButton" Width="216px" Text="Връщане към проекта"></asp:button></TD>
										</TR>
									</TABLE>
								</td>
							</tr>
						</table>
					</td>
				</tr>
			</table>
		</form>
	</body>
</HTML>
