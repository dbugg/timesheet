using System;
using System.Data;
using Asa.Timesheet.Data;
using GrapeCity.ActiveReports;
using GrapeCity.ActiveReports.Controls;
using GrapeCity.ActiveReports.SectionReportModel;
using GrapeCity.ActiveReports.Document.Section;
using GrapeCity.ActiveReports.Document;

namespace Asa.Timesheet.WebPages.Reports
{
    public class SalariesAllRpt : GrapeCity.ActiveReports.SectionReport
	{
		DataSet _ds;
		int numberOfTable=0;
		bool _b = false;
		private const string _styleDivision = " font-weight: bold; background-color: SandyBrown; color: Black;  font-size: 9pt; text-align: center; ";
		private const string _styleDivisionDefault = "background-color: white; color: white;  font-size: 9pt;";

        public SalariesAllRpt(DataSet ds, DataTable dt)
        {
            InitializeComponent();
            _ds = ds;
            this.DataSource = dt;
            //			foreach(DataTable dt in ds.Tables)
            //			{
            //				DataView dv = new DataView(dt);
            //				SubReport sub = new SubReport();
            //				sub.Report = new SalaryRpt(dv);
            //				Detail.Controls.Add(sub);
            //			}
        }

        private void Detail_Format(object sender, System.EventArgs eArgs)
        {
            DataView dv = new DataView(_ds.Tables[numberOfTable]);
            this.SubReport1.Report = new SalaryRpt(dv);
            if (!_b)
            {
                for (int i = numberOfTable + 1; i < _ds.Tables.Count; i++)
                {
                    if (!_ds.Tables[i].TableName.StartsWith(Resource.ResourceManager["SalaryText"]))
                    {
                        this.TextBox1.Text = _ds.Tables[i].TableName;
                        this.TextBox1.Style = _styleDivision;
                        _b = true;
                        break;
                    }
                }
            }
            else
            {
                this.TextBox1.Text = ".";
                this.TextBox1.Style = _styleDivisionDefault;
            }
            if (!_ds.Tables[numberOfTable].TableName.StartsWith(Resource.ResourceManager["SalaryText"]))
                _b = false;
            numberOfTable++;
        }

		#region ActiveReports Designer generated code







        public void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(SalariesAllRpt));
            this.Detail = new GrapeCity.ActiveReports.SectionReportModel.Detail();
            this.PageHeader = new GrapeCity.ActiveReports.SectionReportModel.PageHeader();
            this.PageFooter = new GrapeCity.ActiveReports.SectionReportModel.PageFooter();
            this.SubReport1 = new GrapeCity.ActiveReports.SectionReportModel.SubReport();
            this.Label1 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label2 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.TextBox1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            ((System.ComponentModel.ISupportInitialize)(this.Label1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TextBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            // 
            // Detail
            // 
            this.Detail.ColumnSpacing = 0F;
            this.Detail.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
						this.SubReport1,
						this.Label1,
						this.Label2,
						this.TextBox1});
            this.Detail.Height = 0.6493056F;
            this.Detail.Name = "Detail";
            // 
            // PageHeader
            // 
            this.PageHeader.Height = 0F;
            this.PageHeader.Name = "PageHeader";
            // 
            // PageFooter
            // 
            this.PageFooter.Height = 0F;
            this.PageFooter.Name = "PageFooter";
            // 
            // SubReport1
            // 
            this.SubReport1.CloseBorder = false;
            this.SubReport1.Height = 0.2F;
            this.SubReport1.Left = 0F;
            this.SubReport1.Name = "SubReport1";
            this.SubReport1.Report = null;
            this.SubReport1.Top = 0.2F;
            this.SubReport1.Width = 8F;
            // 
            // Label1
            // 
            this.Label1.DataField = "count";
            this.Label1.Height = 0.2F;
            this.Label1.HyperLink = null;
            this.Label1.Left = 0F;
            this.Label1.Name = "Label1";
            this.Label1.Style = "";
            this.Label1.Text = "Label1";
            this.Label1.Top = 0F;
            this.Label1.Visible = false;
            this.Label1.Width = 1.1F;
            // 
            // Label2
            // 
            this.Label2.Height = 0.2F;
            this.Label2.HyperLink = null;
            this.Label2.Left = 0F;
            this.Label2.Name = "Label2";
            this.Label2.Style = "color: White";
            this.Label2.Text = ".";
            this.Label2.Top = 0.4F;
            this.Label2.Width = 8F;
            // 
            // TextBox1
            // 
            this.TextBox1.Height = 0.2F;
            this.TextBox1.Left = 0F;
            this.TextBox1.Name = "TextBox1";
            this.TextBox1.Style = "text-align: center";
            this.TextBox1.Top = 0F;
            this.TextBox1.Width = 8F;
            // 
            // ActiveReport1
            // 
            this.MasterReport = false;
            this.PageSettings.PaperHeight = 11F;
            this.PageSettings.PaperWidth = 8.5F;
            this.PrintWidth = 8.01F;
            this.Sections.Add(this.PageHeader);
            this.Sections.Add(this.Detail);
            this.Sections.Add(this.PageFooter);
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule(resources.GetString("$this.StyleSheet"), "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: inherit; font-style: inherit; font-variant: inherit; font-weight: bo" +
                        "ld; font-size: 16pt; font-size-adjust: inherit; font-stretch: inherit", "Heading1", "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: Times New Roman; font-style: italic; font-variant: inherit; font-wei" +
                        "ght: bold; font-size: 14pt; font-size-adjust: inherit; font-stretch: inherit", "Heading2", "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: inherit; font-style: inherit; font-variant: inherit; font-weight: bo" +
                        "ld; font-size: 13pt; font-size-adjust: inherit; font-stretch: inherit", "Heading3", "Normal"));
            ((System.ComponentModel.ISupportInitialize)(this.Label1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TextBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();

            // Attach Report Events
            this.Detail.Format += new System.EventHandler(this.Detail_Format);
        }

		#endregion

        private PageHeader PageHeader;
        private Detail Detail;
        private SubReport SubReport1;
        private Label Label1;
        private Label Label2;
        private TextBox TextBox1;
        private PageFooter PageFooter;
	}
}
