using System;
using System.Data;
using System.Data.SqlClient;
namespace Asa.Timesheet.Data
{
	/// <summary>
	/// Summary description for HotIssueUDL.
	/// </summary>
	public class HotIssueUDL
	{
		
		private static SqlCommand CreateHotIssueUpdateClientProc (int HotIssueIDMain, bool ToClient)
		{
			SqlCommand cmd = new SqlCommand();
			cmd.CommandType = System.Data.CommandType.StoredProcedure;
			cmd.CommandText = "HotIssueUpdateClientProc";
			cmd.Parameters.Add(new System.Data.SqlClient.SqlParameter("@HotIssueIDMain", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.Input, true, 10, 0, "HotIssueIDMain", DataRowVersion.Current, null));
			cmd.Parameters["@HotIssueIDMain"].Value = HotIssueIDMain;
			cmd.Parameters.Add(new System.Data.SqlClient.SqlParameter("@ToClient", System.Data.SqlDbType.Bit, 1, System.Data.ParameterDirection.Input, true, 0, 0, "ToClient", DataRowVersion.Current, null));
			cmd.Parameters["@ToClient"].Value = ToClient;

			return cmd;
		}

		public static int ExecuteHotIssueUpdateClientProc(int HotIssueIDMain, bool ToClient)
		{
			int RetObj;
			System.Data.SqlClient.SqlCommand cmd;
			cmd = CreateHotIssueUpdateClientProc(HotIssueIDMain, ToClient);
			SqlConnection cnDAL = new SqlConnection();

			try
			{
				cnDAL =  CommonDAL.OpenConnection();
				cmd.Connection = cnDAL;
				RetObj = cmd.ExecuteNonQuery();
				return RetObj;
		
			}
			catch(SqlException ex)
			{

				throw new ApplicationException("HotIssueUpdateClientProc",ex);
			}

			finally
			{
				CommonDAL.CloseConnection(cnDAL);
			}
		}

	}
}
