using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Data.SqlClient;
using Asa.Timesheet.Data;
using Asa.Timesheet.Data.Util;
using Asa.Timesheet.Data.Entities;
using log4net;
using Asa.Timesheet.WebPages.UserControls;
using DataDynamics.ActiveReports.Export.Pdf;
using DataDynamics.ActiveReports.Export.Xls;
using Asa.Timesheet.WebPages.Reports;
using DataDynamics.ActiveReports.Export.Rtf;
namespace Asa.Timesheet.WebPages
{
	/// <summary>
	/// Summary description for Projects.
	/// </summary>
	public class Profile : TimesheetPageBase 
	{
		#region Web controls

		#region Labels
		
		protected System.Web.UI.WebControls.Label Label66;
		protected System.Web.UI.WebControls.Label lblSketchTitle;
		protected System.Web.UI.WebControls.Label lblInfo;
		protected System.Web.UI.WebControls.Label lblError;
		protected System.Web.UI.WebControls.Label Label1;
		protected System.Web.UI.WebControls.Label Label2;
		protected System.Web.UI.WebControls.Label Label3;
		protected System.Web.UI.WebControls.Label Label5;
		protected System.Web.UI.WebControls.Label Label6;
		protected System.Web.UI.WebControls.Label Label7;
		protected System.Web.UI.WebControls.Label Label8;
		protected System.Web.UI.WebControls.Label Label9;
		protected System.Web.UI.WebControls.Label Label10;
		protected System.Web.UI.WebControls.Label Label11;
		protected System.Web.UI.WebControls.Label Label17;
		protected System.Web.UI.WebControls.Label Label18;
		protected System.Web.UI.WebControls.Label Label19;
		protected System.Web.UI.WebControls.Label Label20;
		protected System.Web.UI.WebControls.Label Label21;
		protected System.Web.UI.WebControls.Label Label22;
		protected System.Web.UI.WebControls.Label Label23;
		protected System.Web.UI.WebControls.Label Label24;
		protected System.Web.UI.WebControls.Label Label25;
		protected System.Web.UI.WebControls.Label Label26;
		protected System.Web.UI.WebControls.Label Label27;
		protected System.Web.UI.WebControls.Label Label28;
		protected System.Web.UI.WebControls.Label Label29;
		protected System.Web.UI.WebControls.Label Label31;
		protected System.Web.UI.WebControls.Label Label32;
		protected System.Web.UI.WebControls.Label Label33;
		protected System.Web.UI.WebControls.Label Label34;
		protected System.Web.UI.WebControls.Label Label35;
		protected System.Web.UI.WebControls.Label Label36;
		protected System.Web.UI.WebControls.Label Label37;
		protected System.Web.UI.WebControls.Label Label38;
		protected System.Web.UI.WebControls.Label Label39;
		protected System.Web.UI.WebControls.Label Label40;
		protected System.Web.UI.WebControls.Label Label41;
		protected System.Web.UI.WebControls.Label Label42;
		protected System.Web.UI.WebControls.Label Label43;
		protected System.Web.UI.WebControls.Label Label44;
		protected System.Web.UI.WebControls.Label Label45;
		protected System.Web.UI.WebControls.Label Label46;
		protected System.Web.UI.WebControls.Label Label47;
		protected System.Web.UI.WebControls.Label Label49;
		protected System.Web.UI.WebControls.Label Label50;
		protected System.Web.UI.WebControls.Label Label51;
		protected System.Web.UI.WebControls.Label Label48;
		protected System.Web.UI.WebControls.Label Label52;
		protected System.Web.UI.WebControls.Label Label53;
		protected System.Web.UI.WebControls.Label Label54;
		protected System.Web.UI.WebControls.Label Label55;
		protected System.Web.UI.WebControls.Label Label56;
		protected System.Web.UI.WebControls.Label Label57;
		protected System.Web.UI.WebControls.Label Label58;
		protected System.Web.UI.WebControls.Label Label59;
		protected System.Web.UI.WebControls.Label Label60;
		protected System.Web.UI.WebControls.Label Label61;
		protected System.Web.UI.WebControls.Label Label62;
		protected System.Web.UI.WebControls.Label Label63;
		protected System.Web.UI.WebControls.Label Label64;
		protected System.Web.UI.WebControls.Label Label65;
		protected System.Web.UI.WebControls.Label Label68;
		protected System.Web.UI.WebControls.Label Label69;
		protected System.Web.UI.WebControls.Label Label70;
		protected System.Web.UI.WebControls.Label Label71;
		protected System.Web.UI.WebControls.Label Label72;
		protected System.Web.UI.WebControls.Label Label12;
		protected System.Web.UI.WebControls.Label Label13;
		protected System.Web.UI.WebControls.Label Label14;
		protected System.Web.UI.WebControls.Label Label15;
		protected System.Web.UI.WebControls.Label Label67;
		protected System.Web.UI.WebControls.Label Label73;
		protected System.Web.UI.WebControls.Label Label74;
		protected System.Web.UI.WebControls.Label Label75;
		protected System.Web.UI.WebControls.Label Label76;
		protected System.Web.UI.WebControls.Label Label77;
		protected System.Web.UI.WebControls.Label Label78;
		protected System.Web.UI.WebControls.Label Label79;
		protected System.Web.UI.WebControls.Label Label80;
		protected System.Web.UI.WebControls.Label Label82;
		protected System.Web.UI.WebControls.Label Label83;
		protected System.Web.UI.WebControls.Label Label84;
		protected System.Web.UI.WebControls.Label Label86;
		protected System.Web.UI.WebControls.Label Label87;
		protected System.Web.UI.WebControls.Label Label88;
		protected System.Web.UI.WebControls.Label Label89;
		protected System.Web.UI.WebControls.Label Label90;
		protected System.Web.UI.WebControls.Label Label91;
		protected System.Web.UI.WebControls.Label Label92;
		protected System.Web.UI.WebControls.Label Label93;
		protected System.Web.UI.WebControls.Label Label96;
		protected System.Web.UI.WebControls.Label Label97;
		protected System.Web.UI.WebControls.Label Label98;
		protected System.Web.UI.WebControls.Label Label99;
		protected System.Web.UI.WebControls.Label Label101;
		protected System.Web.UI.WebControls.Label Label103;
		protected System.Web.UI.WebControls.Label Label104;
		protected System.Web.UI.WebControls.Label Label30;
		protected System.Web.UI.WebControls.Label Label94;
		protected System.Web.UI.WebControls.Label Label95;
		protected System.Web.UI.WebControls.Label Label16;
		protected System.Web.UI.WebControls.Label Label100;
		protected System.Web.UI.WebControls.Label Label102;
		protected System.Web.UI.WebControls.Label Label105;
		protected System.Web.UI.WebControls.Label txtTNumber;
		protected System.Web.UI.WebControls.Label txtELNum;
		protected System.Web.UI.WebControls.Label Label85;
		protected System.Web.UI.WebControls.Label Label106;
		protected System.Web.UI.WebControls.Label Label107;
		protected System.Web.UI.WebControls.Label Label108;
		protected System.Web.UI.WebControls.Label Label109;
		protected System.Web.UI.WebControls.Label Label116;
		protected System.Web.UI.WebControls.Label Label117;
		protected System.Web.UI.WebControls.Label Label118;
		protected System.Web.UI.WebControls.Label Label119;
		protected System.Web.UI.WebControls.Label Label120;
		protected System.Web.UI.WebControls.Label Label121;
		protected System.Web.UI.WebControls.Label Label122;
		protected System.Web.UI.WebControls.Label Label123;
		protected System.Web.UI.WebControls.Label Label124;
		protected System.Web.UI.WebControls.Label Label113;
		protected System.Web.UI.WebControls.Label Label125;
		protected System.Web.UI.WebControls.Label Label111;
		protected System.Web.UI.WebControls.Label Label110;
		protected System.Web.UI.WebControls.Label lbOtherTitle;
		protected System.Web.UI.WebControls.Label Label128;
		protected System.Web.UI.WebControls.Label Label129;
		protected System.Web.UI.WebControls.Label lbNotaryTitle;
		protected System.Web.UI.WebControls.Label Label112;
		protected System.Web.UI.WebControls.Label Label114;
		protected System.Web.UI.WebControls.Label Label115;
		protected System.Web.UI.WebControls.Label Label126;
		protected System.Web.UI.WebControls.Label lbSK;
		protected System.Web.UI.WebControls.Label Label127;
		protected System.Web.UI.WebControls.Label Label4;
		
		#endregion
		
		#region TextBox
		protected System.Web.UI.WebControls.TextBox txtOther;
		protected System.Web.UI.WebControls.TextBox txtPhoneAmount;
		protected System.Web.UI.WebControls.TextBox txtBuildPermissionNotes;
		protected System.Web.UI.WebControls.TextBox txtOtherDeeds;
		protected System.Web.UI.WebControls.TextBox txtOtherDeedsDone;
		protected System.Web.UI.WebControls.TextBox txtOtherDone;
		protected System.Web.UI.WebControls.TextBox txtOtherNumber;
		protected System.Web.UI.WebControls.TextBox txtOtherExpenses;
		protected System.Web.UI.WebControls.TextBox txtIdeaProjectNumber;
		protected System.Web.UI.WebControls.TextBox txtIdeaProjectMun;
		protected System.Web.UI.WebControls.TextBox txtIdeaProjectNotes;
		protected System.Web.UI.WebControls.TextBox txtTechProjectNumber;
		protected System.Web.UI.WebControls.TextBox txtTechProjectMun;
		protected System.Web.UI.WebControls.TextBox txtTechProjectNotes;
		protected System.Web.UI.WebControls.TextBox txtElNumber;
		protected System.Web.UI.WebControls.TextBox txtTNum;
		protected System.Web.UI.WebControls.TextBox txtPowerOfAttorney;
		protected System.Web.UI.WebControls.TextBox txtPowerOfAttorneyDone;
		protected System.Web.UI.WebControls.TextBox txtPOAED;
		protected System.Web.UI.WebControls.TextBox txtVIKEnt;
		protected System.Web.UI.WebControls.TextBox txtVIKRec;
		protected System.Web.UI.WebControls.TextBox txtVIKProjAss;
		protected System.Web.UI.WebControls.TextBox txtVIKProjAgg;
		protected System.Web.UI.WebControls.TextBox txtVIKDate;
		protected System.Web.UI.WebControls.TextBox txtVIKNum;
		protected System.Web.UI.WebControls.TextBox txtVIKLetter;
		protected System.Web.UI.WebControls.TextBox txtVIKContract;
		protected System.Web.UI.WebControls.TextBox txtVIKAmount;
		protected System.Web.UI.WebControls.TextBox txtELEnt;
		protected System.Web.UI.WebControls.TextBox txtELRec;
		protected System.Web.UI.WebControls.TextBox txtELProjAss;
		protected System.Web.UI.WebControls.TextBox txtELProjAgg;
		protected System.Web.UI.WebControls.TextBox txtELDate;
		protected System.Web.UI.WebControls.TextBox txtELLetter;
		protected System.Web.UI.WebControls.TextBox txtELContract;
		protected System.Web.UI.WebControls.TextBox txtELAmount;
		protected System.Web.UI.WebControls.TextBox txtTEnt;
		protected System.Web.UI.WebControls.TextBox txtTRec;
		protected System.Web.UI.WebControls.TextBox txtTProjAss;
		protected System.Web.UI.WebControls.TextBox txtTProjAgg;
		protected System.Web.UI.WebControls.TextBox txtTDate;
		protected System.Web.UI.WebControls.TextBox txtTLetter;
		protected System.Web.UI.WebControls.TextBox txtTContract;
		protected System.Web.UI.WebControls.TextBox txtTAmount;
		protected System.Web.UI.WebControls.TextBox txtPathEnt;
		protected System.Web.UI.WebControls.TextBox txtPathRec;
		protected System.Web.UI.WebControls.TextBox txtPathProjAss;
		protected System.Web.UI.WebControls.TextBox txtPathProjAgg;
		protected System.Web.UI.WebControls.TextBox txtPathDate;
		protected System.Web.UI.WebControls.TextBox txtPathNumber;
		protected System.Web.UI.WebControls.TextBox txtPathAmount;
		protected System.Web.UI.WebControls.TextBox txtPhoneEnt;
		protected System.Web.UI.WebControls.TextBox txtPhoneRec;
		protected System.Web.UI.WebControls.TextBox txtPhoneDate;
		protected System.Web.UI.WebControls.TextBox txtPhoneNumber;
		protected System.Web.UI.WebControls.TextBox txtPhoneNotes;
		protected System.Web.UI.WebControls.TextBox txtVIKNotes;
		protected System.Web.UI.WebControls.TextBox txtELNotes;
		protected System.Web.UI.WebControls.TextBox txtTNotes;
		protected System.Web.UI.WebControls.TextBox txtPathNotes;
		protected System.Web.UI.WebControls.TextBox txtSketch;
		protected System.Web.UI.WebControls.TextBox txtSketchDone;
		protected System.Web.UI.WebControls.TextBox txtPowerOfAttorneyDate;
		protected System.Web.UI.WebControls.TextBox txtPowerOfAttorneyDoneDate;
		protected System.Web.UI.WebControls.TextBox txtCurrentStatus;
		protected System.Web.UI.WebControls.TextBox txtCurrentStatusDone;
		protected System.Web.UI.WebControls.TextBox txtRegPlan;
		protected System.Web.UI.WebControls.TextBox txtRegPlanDone;
		protected System.Web.UI.WebControls.TextBox txtRegPlanNumber;
		protected System.Web.UI.WebControls.TextBox txtBuildPlan;
		protected System.Web.UI.WebControls.TextBox txtBuildPlanDone;
		protected System.Web.UI.WebControls.TextBox txtBuildPlanNumber;
		protected System.Web.UI.WebControls.TextBox txtPreVisa;
		protected System.Web.UI.WebControls.TextBox txtPreVisaDone;
		protected System.Web.UI.WebControls.TextBox txtPreVisaNumber;
		protected System.Web.UI.WebControls.TextBox txtVisa;
		protected System.Web.UI.WebControls.TextBox txtVisaDone;
		protected System.Web.UI.WebControls.TextBox txtVisaNumber;
		protected System.Web.UI.WebControls.TextBox txtVisaPlus;
		protected System.Web.UI.WebControls.TextBox txtVisaPlusDone;
		protected System.Web.UI.WebControls.TextBox txtVisaPlusNumber;
		protected System.Web.UI.WebControls.TextBox txtKadasterUndEnt;
		protected System.Web.UI.WebControls.TextBox txtKadasterUndRec;
		protected System.Web.UI.WebControls.TextBox txtKadasterUndNumber;
		protected System.Web.UI.WebControls.TextBox txtKadasterEnt;
		protected System.Web.UI.WebControls.TextBox txtKadasterRec;
		protected System.Web.UI.WebControls.TextBox txtKadasterNumber;
		protected System.Web.UI.WebControls.TextBox txtIdeaProjectAgreed;
		protected System.Web.UI.WebControls.TextBox txtTechProjectDone;
		protected System.Web.UI.WebControls.TextBox txtTechProjectAgreed;
		protected System.Web.UI.WebControls.TextBox txtIdeaProjectDone;
		protected System.Web.UI.WebControls.TextBox txtBuildPermission;
		protected System.Web.UI.WebControls.TextBox txtBuildPermissionDone;
		protected System.Web.UI.WebControls.TextBox txtBuildPermissionNumber;
		

		#endregion

		#region	CheckBox

		protected System.Web.UI.WebControls.CheckBox CheckBox10;
		protected System.Web.UI.WebControls.CheckBox CheckBox11;
		protected System.Web.UI.WebControls.CheckBox CheckBox12;
		protected System.Web.UI.WebControls.CheckBox CheckBox13;
		protected System.Web.UI.WebControls.CheckBox CheckBox14;
		protected System.Web.UI.WebControls.CheckBox CheckBox15;
		protected System.Web.UI.WebControls.CheckBox CheckBox16;
		protected System.Web.UI.WebControls.CheckBox CheckBox17;
		protected System.Web.UI.WebControls.CheckBox CheckBox18;
		protected System.Web.UI.WebControls.CheckBox CheckBox19;
		protected System.Web.UI.WebControls.CheckBox CheckBox20;
		protected System.Web.UI.WebControls.CheckBox CheckBox21;
		protected System.Web.UI.WebControls.CheckBox cbNotaryDeeds;
		protected System.Web.UI.WebControls.CheckBox cbSketch;
		protected System.Web.UI.WebControls.CheckBox cbRegPlan;
		protected System.Web.UI.WebControls.CheckBox cbBuildPlan;
		protected System.Web.UI.WebControls.CheckBox cbPreVisa;
		protected System.Web.UI.WebControls.CheckBox cbVisa;
		protected System.Web.UI.WebControls.CheckBox cbVisaPlus;
		protected System.Web.UI.WebControls.CheckBox cbKadasterUnd;
		protected System.Web.UI.WebControls.CheckBox cbKadaster;
		protected System.Web.UI.WebControls.CheckBox Checkbox2;
		protected System.Web.UI.WebControls.CheckBox Checkbox3;
		protected System.Web.UI.WebControls.CheckBox Checkbox4;
		protected System.Web.UI.WebControls.CheckBox Checkbox5;
		protected System.Web.UI.WebControls.CheckBox Checkbox6;
		protected System.Web.UI.WebControls.CheckBox Checkbox7;
		protected System.Web.UI.WebControls.CheckBox Checkbox8;
		protected System.Web.UI.WebControls.CheckBox Checkbox22;
		protected System.Web.UI.WebControls.CheckBox Checkbox23;
		protected System.Web.UI.WebControls.CheckBox Checkbox1;
		protected System.Web.UI.WebControls.CheckBox Checkbox24;
		
		#endregion

		#region	LinkButtons and Buttons

		protected System.Web.UI.WebControls.LinkButton LinkButton1;
		protected System.Web.UI.WebControls.LinkButton Linkbutton2;
		protected System.Web.UI.WebControls.LinkButton Linkbutton3;
		protected System.Web.UI.WebControls.Button btnSave;
		protected System.Web.UI.WebControls.Button btnCancel;
		
		#endregion

		#region	Documents

		protected Documents docSketch;
		protected Documents docPowerOfAttorney;
		protected Documents docCurrentStatus;  
		protected Documents docRegPlan;
		protected Documents docBuildPlan;
		protected Documents docPreVisa;
		protected Documents docVisaPlus;
		//protected Documents docVisa;
		protected Documents docVIKEnt;
		protected Documents docElEnt;
		protected Documents docTEnt;
		protected Documents docPathEnt;
		protected Documents docPhoneEnt;
		protected Documents docIdealProjectDone;
		protected Documents docTechProjectDone;
		protected Documents docBuildPermission;
		protected Documents docOther;
		protected Documents docKadasterEnt;
		protected Documents docKadasterUndEnt;
		protected Documents docNotary;
		protected Documents docOtherDoc;
		#endregion

		#region	HtmlImages

		protected System.Web.UI.HtmlControls.HtmlImage imgMain;
		protected System.Web.UI.HtmlControls.HtmlImage imgOther;
		protected System.Web.UI.HtmlControls.HtmlImage lkOtherDeedsE;
		protected System.Web.UI.HtmlControls.HtmlImage lkOtherDeedsD;
		protected System.Web.UI.HtmlControls.HtmlImage imgNotary;
		protected System.Web.UI.HtmlControls.HtmlImage lkNotaryDeedsE;
		protected System.Web.UI.HtmlControls.HtmlImage lkNotaryDeedsD;
		protected System.Web.UI.HtmlControls.HtmlImage imgSketch;
		protected System.Web.UI.HtmlControls.HtmlImage imgAtt;
		protected System.Web.UI.HtmlControls.HtmlImage Img3;
		protected System.Web.UI.HtmlControls.HtmlImage imgActual;
		protected System.Web.UI.HtmlControls.HtmlImage imgPlanReg;
		protected System.Web.UI.HtmlControls.HtmlImage imgPlanBuild;
		protected System.Web.UI.HtmlControls.HtmlImage imgPreVisa;
		protected System.Web.UI.HtmlControls.HtmlImage imgVisa;
		protected System.Web.UI.HtmlControls.HtmlImage imgVisaPlus;
		protected System.Web.UI.HtmlControls.HtmlImage imgKadUnd;
		protected System.Web.UI.HtmlControls.HtmlImage imgKad;
		protected System.Web.UI.HtmlControls.HtmlImage lkOther;
		protected System.Web.UI.HtmlControls.HtmlImage lkBuildPermissionDone;
		protected System.Web.UI.HtmlControls.HtmlImage lkOtherDone;
		protected System.Web.UI.HtmlControls.HtmlImage imgInfra;
		protected System.Web.UI.HtmlControls.HtmlImage iVIKEnt;
		protected System.Web.UI.HtmlControls.HtmlImage iVIKLetter;
		protected System.Web.UI.HtmlControls.HtmlImage iVIKContract;
		protected System.Web.UI.HtmlControls.HtmlImage iVIKProj;
		protected System.Web.UI.HtmlControls.HtmlImage imgVIK;
		protected System.Web.UI.HtmlControls.HtmlImage imgEL;
		protected System.Web.UI.HtmlControls.HtmlImage imgT;
		protected System.Web.UI.HtmlControls.HtmlImage iELEnt;
		protected System.Web.UI.HtmlControls.HtmlImage iELLetter;
		protected System.Web.UI.HtmlControls.HtmlImage iELContract;
		protected System.Web.UI.HtmlControls.HtmlImage iELProject;
		protected System.Web.UI.HtmlControls.HtmlImage iTEnt;
		protected System.Web.UI.HtmlControls.HtmlImage iTLetter;
		protected System.Web.UI.HtmlControls.HtmlImage iTContract;
		protected System.Web.UI.HtmlControls.HtmlImage iTProject;
		protected System.Web.UI.HtmlControls.HtmlImage iPathEnt;
		protected System.Web.UI.HtmlControls.HtmlImage iPathLetter;
		protected System.Web.UI.HtmlControls.HtmlImage iPathProj;
		protected System.Web.UI.HtmlControls.HtmlImage iPhoneEnt;
		protected System.Web.UI.HtmlControls.HtmlImage imgRazr;
		protected System.Web.UI.HtmlControls.HtmlImage iIdea;
		protected System.Web.UI.HtmlControls.HtmlImage iTech;
		protected System.Web.UI.HtmlControls.HtmlImage iRazr;
		protected System.Web.UI.HtmlControls.HtmlImage iOther;
		protected System.Web.UI.HtmlControls.HtmlImage imgPath;
		protected System.Web.UI.HtmlControls.HtmlImage imgPhone;
		protected System.Web.UI.HtmlControls.HtmlImage lkIdeaProjectAgreed;
		protected System.Web.UI.HtmlControls.HtmlImage lkTechProjectDone;
		protected System.Web.UI.HtmlControls.HtmlImage lkTechProjectAgreed;
		protected System.Web.UI.HtmlControls.HtmlImage lkIdeaProjectDone;
		protected System.Web.UI.HtmlControls.HtmlImage lkBuildPermission;
		protected System.Web.UI.HtmlControls.HtmlImage lkSketchE;
		protected System.Web.UI.HtmlControls.HtmlImage lkSketchD;
		protected System.Web.UI.HtmlControls.HtmlImage lkPOAE;
		protected System.Web.UI.HtmlControls.HtmlImage lkPOAD;
		protected System.Web.UI.HtmlControls.HtmlImage lkCurrentStatusE;
		protected System.Web.UI.HtmlControls.HtmlImage lkCurrentStatusD;
		protected System.Web.UI.HtmlControls.HtmlImage lkRegPlanE;
		protected System.Web.UI.HtmlControls.HtmlImage lkRegPlanD;
		protected System.Web.UI.HtmlControls.HtmlImage lkBuildPlanE;
		protected System.Web.UI.HtmlControls.HtmlImage lkBuildPlanD;
		protected System.Web.UI.HtmlControls.HtmlImage lkPreVisaE;
		protected System.Web.UI.HtmlControls.HtmlImage lkPreVisaD;
		protected System.Web.UI.HtmlControls.HtmlImage lkVisaE;
		protected System.Web.UI.HtmlControls.HtmlImage lkVisaD;
		protected System.Web.UI.HtmlControls.HtmlImage lkVisaPlusE;
		protected System.Web.UI.HtmlControls.HtmlImage lkVisaPlusD;
		protected System.Web.UI.HtmlControls.HtmlImage lkKadUndE;
		protected System.Web.UI.HtmlControls.HtmlImage lkKadUndD;
		protected System.Web.UI.HtmlControls.HtmlImage lkKadasterE;
		protected System.Web.UI.HtmlControls.HtmlImage lkKadasterD;
		protected System.Web.UI.HtmlControls.HtmlImage lkVIKEnt;
		protected System.Web.UI.HtmlControls.HtmlImage lkVIKRec;
		protected System.Web.UI.HtmlControls.HtmlImage lkVIKProjAss;
		protected System.Web.UI.HtmlControls.HtmlImage lkVIKProjAgg;
		protected System.Web.UI.HtmlControls.HtmlImage lkVIKDate;
		protected System.Web.UI.HtmlControls.HtmlImage lkVIKLetter;
		protected System.Web.UI.HtmlControls.HtmlImage lkVIKContract;
		protected System.Web.UI.HtmlControls.HtmlImage lkELEnt;
		protected System.Web.UI.HtmlControls.HtmlImage lkELRec;
		protected System.Web.UI.HtmlControls.HtmlImage lkELProjAss;
		protected System.Web.UI.HtmlControls.HtmlImage lkELProjAgg;
		protected System.Web.UI.HtmlControls.HtmlImage lkELDate;
		protected System.Web.UI.HtmlControls.HtmlImage lkELLetter;
		protected System.Web.UI.HtmlControls.HtmlImage lkELContract;
		protected System.Web.UI.HtmlControls.HtmlImage lkTEnt;
		protected System.Web.UI.HtmlControls.HtmlImage lkTRec;
		protected System.Web.UI.HtmlControls.HtmlImage lkTProjAss;
		protected System.Web.UI.HtmlControls.HtmlImage lkTProjAgg;
		protected System.Web.UI.HtmlControls.HtmlImage lkTDate;
		protected System.Web.UI.HtmlControls.HtmlImage lkTLetter;
		protected System.Web.UI.HtmlControls.HtmlImage lkTContract;
		protected System.Web.UI.HtmlControls.HtmlImage lkPathEnt;
		protected System.Web.UI.HtmlControls.HtmlImage lkPathRec;
		protected System.Web.UI.HtmlControls.HtmlImage lkPathProjAss;
		protected System.Web.UI.HtmlControls.HtmlImage lkPathProjAgg;
		protected System.Web.UI.HtmlControls.HtmlImage lkPathDate;
		protected System.Web.UI.HtmlControls.HtmlImage lkPhoneEnt;
		protected System.Web.UI.HtmlControls.HtmlImage lkPhoneRec;
		protected System.Web.UI.HtmlControls.HtmlImage lkPhoneDate;
		protected System.Web.UI.WebControls.TextBox txtNotaryDeeds;
		protected System.Web.UI.WebControls.TextBox txtNotaryDeedsDone;
		
		#endregion
		
		protected System.Web.UI.WebControls.PlaceHolder menuHolder;
		protected Asa.Timesheet.WebPages.UserControls.PageHeader header;
		protected System.Web.UI.HtmlControls.HtmlGenericControl div1;
		protected System.Web.UI.HtmlControls.HtmlGenericControl Div2;
		protected System.Web.UI.HtmlControls.HtmlGenericControl Div3;
		protected System.Web.UI.HtmlControls.HtmlInputHidden hdn;
		private const string MAIL_STRING="MAIL";
		protected System.Web.UI.WebControls.Button btnPrint;
		protected System.Web.UI.WebControls.TextBox txtOtherDocNotes;
		protected System.Web.UI.WebControls.Label Label81;
		private static readonly ILog log = LogManager.GetLogger(typeof(Projects));
		#endregion
		

		#region Grid columns

		private enum GridColumns
		{
			ProjectName = 0,
			ProjectCode,
			StartDate,
			BuildingType,
			Area,
			Address,
			Client,
			ProjectManager,
			Subprojects,
			EditItem,
			DeleteItem
		}

		#endregion

		private void Page_Load(object sender, System.EventArgs e)
		{
			if (!LoggedUser.HasRole) ErrorRedirect(Resource.ResourceManager["noRightsAlert"]);
		
			UIHelpers.CreateMenu(menuHolder,LoggedUser);
			
			if (!this.IsPostBack)
			{
				
				header.UserName = LoggedUser.UserName;
				//	btnNewProject.Text = Resource.ResourceManager["projects_btnNewProject"];
				int PID = UIHelpers.GetPIDParam();
				string projectName, projectCode, administrativeName,add;
				decimal area=0;
				int clientID=0;
				bool phases;
				if (!ProjectsData.SelectProject(PID, out projectName, out  projectCode, out administrativeName, out area, out add, 
					out clientID,out phases))
				{
					log.Error("Project not found " + PID);
				
				}
				int div=UIHelpers.ToInt(Request.Params["div"]);
				div1.Style["display"]=Div2.Style["display"]=Div3.Style["display"]="none";
				if(div>0)
				{
					if(div==1)
					{
						div1.Style["display"]="block";
						hdn.Value="1";
					}
					if(div==2)
					{
						Div2.Style["display"]="block";
						hdn.Value="2";
					}
					if(div==3)
					{
						Div3.Style["display"]="block";
						hdn.Value="3";
					}
				}
				else
				{
					div1.Style["display"]="block";
					hdn.Value="1";
				}
				header.PageTitle = Resource.ResourceManager["Profile_PageTitle"]+" - " +projectName;//+projectCode+ " " +administrativeName;
				LinkButton1.Attributes["onclick"] = "ShowDiv(1); return false;";
				Linkbutton2.Attributes["onclick"] = "ShowDiv(2); return false;";
				Linkbutton3.Attributes["onclick"] = "ShowDiv(3); return false;";

				LoadProfile();
				
			}
			LoadDocuments();
			#region Div1
			
			lkNotaryDeedsE.Attributes["onclick"] = TimeHelper.InvokePopupCal(txtNotaryDeeds);
			lkNotaryDeedsD.Attributes["onclick"] = TimeHelper.InvokePopupCal(txtNotaryDeedsDone);
			lkSketchE.Attributes["onclick"] = TimeHelper.InvokePopupCal(txtSketch);
			lkSketchD.Attributes["onclick"] = TimeHelper.InvokePopupCal(txtSketchDone);
			lkPOAE.Attributes["onclick"] = TimeHelper.InvokePopupCal(txtPowerOfAttorneyDate);
			lkPOAD.Attributes["onclick"] = TimeHelper.InvokePopupCal(txtPowerOfAttorneyDoneDate);
			lkCurrentStatusE.Attributes["onclick"] = TimeHelper.InvokePopupCal(txtCurrentStatus);
			lkCurrentStatusD.Attributes["onclick"] = TimeHelper.InvokePopupCal(txtCurrentStatusDone);
			lkRegPlanE.Attributes["onclick"] = TimeHelper.InvokePopupCal(txtRegPlan);
			lkRegPlanD.Attributes["onclick"] = TimeHelper.InvokePopupCal(txtRegPlanDone);
			lkBuildPlanE.Attributes["onclick"] = TimeHelper.InvokePopupCal(txtBuildPlan);
			lkBuildPlanD.Attributes["onclick"] = TimeHelper.InvokePopupCal(txtBuildPlanDone);
			lkPreVisaE.Attributes["onclick"] = TimeHelper.InvokePopupCal(txtPreVisa);
			lkPreVisaD.Attributes["onclick"] = TimeHelper.InvokePopupCal(txtPreVisaDone);
			lkVisaE.Attributes["onclick"] = TimeHelper.InvokePopupCal(txtVisa);
			lkVisaD.Attributes["onclick"] = TimeHelper.InvokePopupCal(txtVisaDone);
			lkVisaPlusE.Attributes["onclick"] = TimeHelper.InvokePopupCal(txtVisaPlus);
			lkVisaPlusD.Attributes["onclick"] = TimeHelper.InvokePopupCal(txtVisaPlusDone);
			lkKadUndE.Attributes["onclick"] = TimeHelper.InvokePopupCal(txtKadasterUndEnt);
			lkKadUndD.Attributes["onclick"] = TimeHelper.InvokePopupCal(txtKadasterUndRec);
			lkKadasterE.Attributes["onclick"] = TimeHelper.InvokePopupCal(txtKadasterEnt);
			lkKadasterD.Attributes["onclick"] = TimeHelper.InvokePopupCal(txtKadasterRec);

			#endregion

			#region Div2
			// VIK
			lkVIKEnt.Attributes["onclick"] = TimeHelper.InvokePopupCal(txtVIKEnt);
			lkVIKRec.Attributes["onclick"] = TimeHelper.InvokePopupCal(txtVIKRec);
			lkVIKProjAss.Attributes["onclick"] = TimeHelper.InvokePopupCal(txtVIKProjAss);
			lkVIKProjAgg.Attributes["onclick"] = TimeHelper.InvokePopupCal(txtVIKProjAgg);
			// lkVIKDate.Attributes["onclick"] = TimeHelper.InvokePopupCal(txtVIKDate);
			lkVIKLetter.Attributes["onclick"] = TimeHelper.InvokePopupCal(txtVIKLetter);
			lkVIKContract.Attributes["onclick"] = TimeHelper.InvokePopupCal(txtVIKContract);

			// Elektro
			lkELEnt.Attributes["onclick"] = TimeHelper.InvokePopupCal(txtELEnt);
			lkELRec.Attributes["onclick"] = TimeHelper.InvokePopupCal(txtELRec);
			lkELProjAss.Attributes["onclick"] = TimeHelper.InvokePopupCal(txtELProjAss);
			lkELProjAgg.Attributes["onclick"] = TimeHelper.InvokePopupCal(txtELProjAgg);
			//lkELDate.Attributes["onclick"] = TimeHelper.InvokePopupCal(txtELDate);
			lkELLetter.Attributes["onclick"] = TimeHelper.InvokePopupCal(txtELLetter);
			lkELContract.Attributes["onclick"] = TimeHelper.InvokePopupCal(txtELContract);

			// Toplo
			lkTEnt.Attributes["onclick"] = TimeHelper.InvokePopupCal(txtTEnt);
			lkTRec.Attributes["onclick"] = TimeHelper.InvokePopupCal(txtTRec);
			lkTProjAss.Attributes["onclick"] = TimeHelper.InvokePopupCal(txtTProjAss);
			lkTProjAgg.Attributes["onclick"] = TimeHelper.InvokePopupCal(txtTProjAgg);
			//lkTDate.Attributes["onclick"] = TimeHelper.InvokePopupCal(txtTDate);
			lkTLetter.Attributes["onclick"] = TimeHelper.InvokePopupCal(txtTLetter);
			lkTContract.Attributes["onclick"] = TimeHelper.InvokePopupCal(txtTContract);
     
			// Path
			lkPathEnt.Attributes["onclick"] = TimeHelper.InvokePopupCal(txtPathEnt);
			lkPathRec.Attributes["onclick"] = TimeHelper.InvokePopupCal(txtPathRec);
			lkPathProjAss.Attributes["onclick"] = TimeHelper.InvokePopupCal(txtPathProjAss);
			lkPathProjAgg.Attributes["onclick"] = TimeHelper.InvokePopupCal(txtPathProjAgg);
			lkPathDate.Attributes["onclick"] = TimeHelper.InvokePopupCal(txtPathDate);

			// Telephone
			lkPhoneEnt.Attributes["onclick"] = TimeHelper.InvokePopupCal(txtPhoneEnt);
			lkPhoneRec.Attributes["onclick"] = TimeHelper.InvokePopupCal(txtPhoneRec);
			// lkPhoneDate.Attributes["onclick"] = TimeHelper.InvokePopupCal(txtPhoneDate);

			#endregion

			#region Div3

			lkIdeaProjectDone.Attributes["onclick"] = TimeHelper.InvokePopupCal(txtIdeaProjectDone);
			//lkIdeaProjectDAG.Attributes["onclick"] = TimeHelper.InvokePopupCal(txtIdeaProjectDAG);
			//lkIdeaProjectMun.Attributes["onclick"] = TimeHelper.InvokePopupCal(txtIdeaProjectMun);
			lkIdeaProjectAgreed.Attributes["onclick"] = TimeHelper.InvokePopupCal(txtIdeaProjectAgreed);

			lkTechProjectDone.Attributes["onclick"] = TimeHelper.InvokePopupCal(txtTechProjectDone);
			//lkTechProjectDAG.Attributes["onclick"] = TimeHelper.InvokePopupCal(txtTechProjectDAG);
			//lkTechProjectMun.Attributes["onclick"] = TimeHelper.InvokePopupCal(txtTechProjectMun);
			lkTechProjectAgreed.Attributes["onclick"] = TimeHelper.InvokePopupCal(txtTechProjectAgreed);

			lkBuildPermission.Attributes["onclick"] = TimeHelper.InvokePopupCal(txtBuildPermission);
			lkBuildPermissionDone.Attributes["onclick"] = TimeHelper.InvokePopupCal(txtBuildPermissionDone);
			lkOtherDone.Attributes["onclick"] = TimeHelper.InvokePopupCal(txtOtherDone);
			lkOther.Attributes["onclick"] = TimeHelper.InvokePopupCal(txtOther);

			#endregion
     
		}
		private void LoadDocuments()
		{
			//OtherDoc
			docOtherDoc.ItemType=ProfileItems.OtherDoc;
			docOtherDoc.ProjectID=this.ProjectID;
			//Notary
			docNotary.ItemType=ProfileItems.Notary;
			docNotary.ProjectID=this.ProjectID;
			//Sketch
			docSketch.ItemType=ProfileItems.Sketch;
			docSketch.ProjectID=this.ProjectID;
			//PowerOfAttorney
			docPowerOfAttorney.ItemType=ProfileItems.PowerofAttorney;
			docPowerOfAttorney.ProjectID=this.ProjectID;
			//CurrentStatus
			docCurrentStatus.ItemType=ProfileItems.CurrentStatus;
			docCurrentStatus.ProjectID=this.ProjectID;
			//RegPlan
			docRegPlan.ItemType=ProfileItems.RegPlan;
			docRegPlan.ProjectID=this.ProjectID;   
			//BuildPlan
			docBuildPlan.ItemType=ProfileItems.BuildPlan;
			docBuildPlan.ProjectID=this.ProjectID;
			//PreVisa
			docPreVisa.ItemType=ProfileItems.PreVisa;
			docPreVisa.ProjectID=this.ProjectID;
			//VisaPlus
			docVisaPlus.ItemType=ProfileItems.VisaPlus;
			docVisaPlus.ProjectID=this.ProjectID;
			//KadasterUntEnt
			docKadasterUndEnt.ItemType=ProfileItems.KadasterUndEnt;
			docKadasterUndEnt.ProjectID=this.ProjectID;
			//KadasterEnt
			docKadasterEnt.ItemType=ProfileItems.KadasterEnt;
			docKadasterEnt.ProjectID=this.ProjectID;
			//Visa
//			docVisa.ItemType=ProfileItems.Visa;
//			docVisa.ProjectID=this.ProjectID;
			//VIKEnt
			docVIKEnt.ItemType=ProfileItems.VIKEnt;
			docVIKEnt.ProjectID=this.ProjectID;
			//ElEnt
			docElEnt.ItemType=ProfileItems.ElEnt;
			docElEnt.ProjectID=this.ProjectID;
			//TEnt
			docTEnt.ItemType=ProfileItems.TEnt;
			docTEnt.ProjectID=this.ProjectID;
			//PathEnt
			docPathEnt.ItemType=ProfileItems.PathEnt;
			docPathEnt.ProjectID=this.ProjectID;
			//PhoneEnt
			docPhoneEnt.ItemType=ProfileItems.PhoneEnt;
			docPhoneEnt.ProjectID=this.ProjectID;
			//IdealProjectDone
			docIdealProjectDone.ItemType=ProfileItems.IdealProjectDone;
			docIdealProjectDone.ProjectID=this.ProjectID;

			//TechProjectDone
			docTechProjectDone.ItemType=ProfileItems.TechProjectDone;
			docTechProjectDone.ProjectID=this.ProjectID;
			//BuildPermission
			docBuildPermission.ItemType=ProfileItems.BuildPermission;
			docBuildPermission.ProjectID=this.ProjectID;
			//Other
			docOther.ItemType=ProfileItems.Other;
			docOther.ProjectID=this.ProjectID;
		}
		private void SaveDocuments()
		{
			//OtherDoc
			docOtherDoc.Save();
			//Notary
			docNotary.Save();
			//Sketch
            docSketch.Save();
			//PowerOfAttorney
            docPowerOfAttorney.Save();
			//CurrentStatus
             docCurrentStatus.Save();
			//RegPlan
            docRegPlan.Save();
			//BuildPlan
            docBuildPlan.Save();
			//Previsa
            docPreVisa.Save();
			//VisaPlus
            docVisaPlus.Save();
			//KadasterUntEnt
            docKadasterUndEnt.Save();
			//KadasterEnt
            docKadasterEnt.Save();
			//Visa
            //docVisa.Save();
			//VIKEnt
			docVIKEnt.Save();
			//ElEnt
			docElEnt.Save();

			//TEnt
			docTEnt.Save();
			//PathEnt
			docPathEnt.Save();
			//PhoneEnt
			docPhoneEnt.Save();
			//IdealProjectDone
			docIdealProjectDone.Save();
			//TechProjectDone
			docTechProjectDone.Save();
			//BuildPermission
			docBuildPermission.Save();
			//Other
			docOther.Save();


		} 
		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
			this.btnPrint.Click += new System.EventHandler(this.btnPrint_Click);
			this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
			this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
			this.Load += new System.EventHandler(this.Page_Load);

		}
		#endregion

		#region Database

		#region LoadProfile

		#region Profile1

		private void SetStatus(HtmlImage hi)
		{
			
			hi.Src="images/done.gif";
			hi.Alt=Resource.ResourceManager["imgReceived"];
			
		}
		private void SetStatus(TextBox tb1, HtmlImage hi)
		{
			if(tb1.Text!="" )
			{
				hi.Src="images/done.gif";
				hi.Alt=Resource.ResourceManager["imgReceived"];
			}
		}
		private void SetStatus(TextBox tb1, TextBox tb2, HtmlImage hi)
		{
			if(tb1.Text!="" && tb2.Text!="")
			{
				hi.Src="images/done.gif";
				hi.Alt=Resource.ResourceManager["imgReceived"];
			}
		}
		private bool IsSet( HtmlImage hi)
		{
			return hi.Src=="images/done.gif";
		}
		private void LoadProjectProfile2()
		{
			DataRow row = null;
    
			try
			{
				row = ProjectsData.SelectProjectProfile2(this.ProjectID);
			}
			catch (Exception ex)
			{
				log.Error(ex.ToString());
			}
			if (row == null) Response.Redirect("Projects.aspx");

			//VIK
			txtVIKEnt.Text = row.IsNull("VIKEnt") ? 
				"" : ((DateTime)row["VIKEnt"]).ToString(Constants.TextDateFormat);
			txtVIKRec.Text = row.IsNull("VIKRec") ? 
				"" : ((DateTime)row["VIKRec"]).ToString(Constants.TextDateFormat);
			SetStatus(txtVIKEnt,txtVIKRec,iVIKEnt);


			txtVIKLetter.Text = row.IsNull("VIKLetter") ? 
				"" : ((DateTime)row["VIKLetter"]).ToString(Constants.TextDateFormat);
			SetStatus(txtVIKLetter,iVIKLetter);

			txtVIKContract.Text = row.IsNull("VIKContract") ? 
				"" : ((DateTime)row["VIKContract"]).ToString(Constants.TextDateFormat);
			SetStatus(txtVIKContract,iVIKContract);

			txtVIKProjAss.Text = row.IsNull("VIKProjectAssinged") ? 
				"" : ((DateTime)row["VIKProjectAssinged"]).ToString(Constants.TextDateFormat);
			txtVIKProjAgg.Text = row.IsNull("VIKProjectAgreed") ? 
				"" : ((DateTime)row["VIKProjectAgreed"]).ToString(Constants.TextDateFormat);
			SetStatus(txtVIKProjAss,txtVIKProjAgg,iVIKProj);

			txtVIKNum.Text= row.IsNull("VIKNumber") ? 
				"" : (string)row["VIKNumber"];
			txtVIKAmount.Text= row.IsNull("VIKAmount") ? 
				"" : UIHelpers.FormatDecimal2((decimal)row["VIKAmount"]);
			txtVIKNotes.Text= row.IsNull("VIKNotes") ? 
				"" : (string)row["VIKNotes"];
			if(IsSet(iVIKEnt) &&  IsSet(iVIKContract) && IsSet(iVIKProj) && IsSet(iVIKLetter))
				SetStatus(imgVIK);

			if(!IsSet(iVIKProj))
				ViewState.Add("iVIKProj",MAIL_STRING);
			//EL
			txtELEnt.Text = row.IsNull("ELEnt") ? 
				"" : ((DateTime)row["ELEnt"]).ToString(Constants.TextDateFormat);
			txtELRec.Text = row.IsNull("ELRec") ? 
				"" : ((DateTime)row["ELRec"]).ToString(Constants.TextDateFormat);
			SetStatus(txtELEnt,txtELRec,iELEnt);


			txtELLetter.Text = row.IsNull("ELLetter") ? 
				"" : ((DateTime)row["ELLetter"]).ToString(Constants.TextDateFormat);
			SetStatus(txtELLetter,iELLetter);

			txtELContract.Text = row.IsNull("ELContract") ? 
				"" : ((DateTime)row["ELContract"]).ToString(Constants.TextDateFormat);
			SetStatus(txtELContract,iELContract);

			txtELProjAss.Text = row.IsNull("ELProjectAssinged") ? 
				"" : ((DateTime)row["ELProjectAssinged"]).ToString(Constants.TextDateFormat);
			txtELProjAgg.Text = row.IsNull("ELProjectAgreed") ? 
				"" : ((DateTime)row["ELProjectAgreed"]).ToString(Constants.TextDateFormat);
			SetStatus(txtELProjAss,txtELProjAgg,iELProject);

			txtElNumber.Text= row.IsNull("ELNumber") ? 
				"" : (string)row["ELNumber"];
			txtELAmount.Text= row.IsNull("ELAmount") ? 
				"" : UIHelpers.FormatDecimal2((decimal)row["ELAmount"]);
			txtELNotes.Text= row.IsNull("ELNotes") ? 
				"" : (string)row["ELNotes"];

			if(IsSet(iELEnt) &&  IsSet(iELContract) && IsSet(iELProject) && IsSet(iELLetter))
				SetStatus(imgEL);

			if(!IsSet(iELProject))
				ViewState.Add("iELProject",MAIL_STRING);
			//T
			txtTEnt.Text = row.IsNull("TEnt") ? 
				"" : ((DateTime)row["TEnt"]).ToString(Constants.TextDateFormat);
			txtTRec.Text = row.IsNull("TRec") ? 
				"" : ((DateTime)row["TRec"]).ToString(Constants.TextDateFormat);
			SetStatus(txtTEnt,txtTRec,iTEnt);


			txtTLetter.Text = row.IsNull("TLetter") ? 
				"" : ((DateTime)row["TLetter"]).ToString(Constants.TextDateFormat);
			SetStatus(txtTLetter,iTLetter);

			txtTContract.Text = row.IsNull("TContract") ? 
				"" : ((DateTime)row["TContract"]).ToString(Constants.TextDateFormat);
			SetStatus(txtTContract,iTContract);

			txtTProjAss.Text = row.IsNull("TProjectAssinged") ? 
				"" : ((DateTime)row["TProjectAssinged"]).ToString(Constants.TextDateFormat);
			txtTProjAgg.Text = row.IsNull("TProjectAgreed") ? 
				"" : ((DateTime)row["TProjectAgreed"]).ToString(Constants.TextDateFormat);
			SetStatus(txtTProjAss,txtTProjAgg,iTProject);

			txtTNum.Text= row.IsNull("TNumber") ? 
				"" : (string)row["TNumber"];
			txtTAmount.Text= row.IsNull("TAmount") ? 
				"" : UIHelpers.FormatDecimal2((decimal)row["TAmount"]);
			txtTNotes.Text= row.IsNull("TNotes") ? 
				"" : (string)row["TNotes"];

			if(IsSet(iTEnt) &&  IsSet(iTContract) && IsSet(iTProject) && IsSet(iTLetter))
				SetStatus(imgT);

			if(!IsSet(iTProject))
				ViewState.Add("iTProject",MAIL_STRING);
			//Path
			txtPathEnt.Text = row.IsNull("PathEnt") ? 
				"" : ((DateTime)row["PathEnt"]).ToString(Constants.TextDateFormat);
			txtPathRec.Text = row.IsNull("PathRec") ? 
				"" : ((DateTime)row["PathRec"]).ToString(Constants.TextDateFormat);
			SetStatus(txtPathEnt,txtPathRec,iPathEnt);


			txtPathDate.Text = row.IsNull("PathDate") ? 
				"" : ((DateTime)row["PathDate"]).ToString(Constants.TextDateFormat);
			SetStatus(txtPathDate,iPathLetter);

			txtPathProjAss.Text = row.IsNull("PathProjectAssinged") ? 
				"" : ((DateTime)row["PathProjectAssinged"]).ToString(Constants.TextDateFormat);
			txtPathProjAgg.Text = row.IsNull("PathProjectAgreed") ? 
				"" : ((DateTime)row["PathProjectAgreed"]).ToString(Constants.TextDateFormat);
			SetStatus(txtPathProjAss,txtPathProjAgg,iPathProj);

			txtPathNumber.Text= row.IsNull("PathNumber") ? 
				"" : (string)row["PathNumber"];
			txtPathAmount.Text= row.IsNull("PathAmount") ? 
				"" : UIHelpers.FormatDecimal2((decimal)row["PathAmount"]);
			txtPathNotes.Text= row.IsNull("PathNotes") ? 
				"" : (string)row["PathNotes"];

			if(IsSet(iPathEnt)  && IsSet(iPathProj) && IsSet(iPathLetter))
				SetStatus(imgPath);

			if(!IsSet(iPathProj))
				ViewState.Add("iPathProj",MAIL_STRING);

			//Phone
			txtPhoneEnt.Text = row.IsNull("PhoneEnt") ? 
				"" : ((DateTime)row["PhoneEnt"]).ToString(Constants.TextDateFormat);
			txtPhoneRec.Text = row.IsNull("PhoneRec") ? 
				"" : ((DateTime)row["PhoneRec"]).ToString(Constants.TextDateFormat);
			SetStatus(txtPhoneEnt,txtPhoneRec,iPhoneEnt);
		
		

			//		txtPhoneDate.Text = row.IsNull("PhoneDate") ? 
			//			"" : ((DateTime)row["PhoneLetter"]).ToString(Constants.TextDateFormat);
			//		SetStatus(txtPhoneDate,iPhoneEnt);

		

			txtPhoneNumber.Text= row.IsNull("PhoneNumber") ? 
				"" : (string)row["PhoneNumber"];
		
			txtPhoneNotes.Text= row.IsNull("PhoneNotes") ? 
				"" : (string)row["PhoneNotes"];
			txtPhoneAmount.Text= row.IsNull("PhoneAmount") ? 
				"" : UIHelpers.FormatDecimal2((decimal)row["PhoneAmount"]);
			if(IsSet(iPhoneEnt)  )
				SetStatus(imgPhone);
			else
				ViewState.Add("iPhoneEnt",MAIL_STRING);
			//Idea
			txtIdeaProjectDone.Text = row.IsNull("IdeaProjectDone") ? 
				"" : ((DateTime)row["IdeaProjectDone"]).ToString(Constants.TextDateFormat);
			txtIdeaProjectAgreed.Text = row.IsNull("IdeaProjectAgreed") ? 
				"" : ((DateTime)row["IdeaProjectAgreed"]).ToString(Constants.TextDateFormat);
			SetStatus(txtIdeaProjectDone,txtIdeaProjectAgreed,iIdea);


			txtIdeaProjectMun.Text= row.IsNull("IdeaProjectMun") ? 
				"" : (string)row["IdeaProjectMun"];
			txtIdeaProjectNumber.Text= row.IsNull("IdeaProjectMunWhere") ? 
				"" : (string)row["IdeaProjectMunWhere"];
		
			txtIdeaProjectNotes.Text= row.IsNull("IdeaProjectNotes") ? 
				"" : (string)row["IdeaProjectNotes"];

			//Tech
			txtTechProjectDone.Text = row.IsNull("TechProjectDone") ? 
				"" : ((DateTime)row["TechProjectDone"]).ToString(Constants.TextDateFormat);
			txtTechProjectAgreed.Text = row.IsNull("TechProjectAgreed") ? 
				"" : ((DateTime)row["TechProjectAgreed"]).ToString(Constants.TextDateFormat);
			SetStatus(txtTechProjectDone,txtTechProjectAgreed,iTech);


			txtTechProjectMun.Text= row.IsNull("TechProjectMun") ? 
				"" : (string)row["TechProjectMun"];
			txtTechProjectNumber.Text= row.IsNull("TechProjectMunWhere") ? 
				"" : (string)row["TechProjectMunWhere"];
		
			txtTechProjectNotes.Text= row.IsNull("TechProjectNotes") ? 
				"" : (string)row["TechProjectNotes"];

			//BuildPermission
			txtBuildPermission.Text = row.IsNull("BuildPermission") ? 
				"" : ((DateTime)row["BuildPermission"]).ToString(Constants.TextDateFormat);
			txtBuildPermissionDone.Text = row.IsNull("BuildPermissionDone") ? 
				"" : ((DateTime)row["BuildPermissionDone"]).ToString(Constants.TextDateFormat);
			SetStatus(txtBuildPermissionDone,iRazr);


			txtBuildPermissionNumber.Text= row.IsNull("BuildPermissionNumber") ? 
				"" : (string)row["BuildPermissionNumber"];
			txtBuildPermissionNotes.Text= row.IsNull("BuildNotes") ? 
				"" : (string)row["BuildNotes"];
			//Other
			txtOther.Text = row.IsNull("Other") ? 
				"" : ((DateTime)row["Other"]).ToString(Constants.TextDateFormat);
			txtOtherDone.Text = row.IsNull("OtherDone") ? 
				"" : ((DateTime)row["OtherDone"]).ToString(Constants.TextDateFormat);
			SetStatus(txtOther,txtOtherDone,iOther);


			txtOtherNumber.Text= row.IsNull("OtherNumber") ? 
				"" : (string)row["OtherNumber"];
			txtOtherExpenses.Text= row.IsNull("OtherExpenses") ? 
				"" : UIHelpers.FormatDecimal2((decimal)row["OtherExpenses"]);
		
			if(IsSet(imgEL) &&  IsSet(imgVIK) && IsSet(imgT) && IsSet(imgPath)&& IsSet(imgPhone))
				SetStatus(imgInfra);
			if(!IsSet(iRazr))
				ViewState.Add("iRazr",MAIL_STRING);
			if(IsSet(iIdea) &&  IsSet(iTech) && IsSet(iRazr) && IsSet(iOther))
				SetStatus(imgRazr);
		}
		private void LoadProjectProfile1()
		{
			DataRow row = null;
      
			try
			{
				row = ProjectsData.SelectProjectProfile1(this.ProjectID);
			}
			catch (Exception ex)
			{
				log.Error(ex.ToString());
			}

			if (row == null) Response.Redirect("Projects.aspx");

			#region Fill form fields

			txtNotaryDeeds.Text = row.IsNull("NotaryDeeds") ? 
				"" : ((DateTime)row["NotaryDeeds"]).ToString(Constants.TextDateFormat);
			txtNotaryDeedsDone.Text = row.IsNull("NotaryDeedsDone") ? 
				"" : ((DateTime)row["NotaryDeedsDone"]).ToString(Constants.TextDateFormat);
			SetStatus(txtNotaryDeeds,txtNotaryDeedsDone,imgNotary);
			//sketch
			txtSketch.Text = row.IsNull("Sketch") ? 
				"" : ((DateTime)row["Sketch"]).ToString(Constants.TextDateFormat);
			if(!row.IsNull("SketchDone"))
			{
				DateTime dtSK= (DateTime)row["SketchDone"];
				if(dtSK.AddMonths(6)<=DateTime.Now)
					lbSK.Text=Resource.ResourceManager["warnSketch"];		
				txtSketchDone.Text =dtSK.ToString(Constants.TextDateFormat);
			}
			SetStatus(txtSketch,txtSketchDone,imgSketch);
			//power of attorney
			txtPowerOfAttorneyDate.Text = row.IsNull("PowerOfAttorneyDate") ? 
				"" : ((DateTime)row["PowerOfAttorneyDate"]).ToString(Constants.TextDateFormat);
			txtPowerOfAttorneyDoneDate.Text = row.IsNull("PowerOfAttorneyDoneDate") ? 
				"" : ((DateTime)row["PowerOfAttorneyDoneDate"]).ToString(Constants.TextDateFormat);
			SetStatus(txtPowerOfAttorneyDate,txtPowerOfAttorneyDoneDate,imgAtt);
			txtPowerOfAttorney.Text = row.IsNull("PowerOfAttorney") ? 
				"" : ((int)row["PowerOfAttorney"]).ToString();
			txtPowerOfAttorneyDone.Text = row.IsNull("PowerOfAttorneyDone") ? 
				"" : ((int)row["PowerOfAttorneyDone"]).ToString();

			// current status
			txtCurrentStatus.Text = row.IsNull("CurrentStatus") ? 
				"" : ((DateTime)row["CurrentStatus"]).ToString(Constants.TextDateFormat);
			txtCurrentStatusDone.Text = row.IsNull("CurrentStatusDone") ? 
				"" : ((DateTime)row["CurrentStatusDone"]).ToString(Constants.TextDateFormat);
			SetStatus(txtCurrentStatus,txtCurrentStatusDone,imgActual);
			// reg plan
			txtRegPlan.Text = row.IsNull("RegPlan") ? 
				"" : ((DateTime)row["RegPlan"]).ToString(Constants.TextDateFormat);
			txtRegPlanDone.Text = row.IsNull("RegPlanDone") ? 
				"" : ((DateTime)row["RegPlanDone"]).ToString(Constants.TextDateFormat);
			txtRegPlanNumber.Text = row.IsNull("RegPlanNumber") ? 
				"" : (string)row["RegPlanNumber"];
			SetStatus(txtRegPlan,txtRegPlanDone,imgPlanReg);
			// build plan
			txtBuildPlan.Text = row.IsNull("BuildPlan") ? 
				"" : ((DateTime)row["BuildPlan"]).ToString(Constants.TextDateFormat);
			txtBuildPlanDone.Text = row.IsNull("BuildPlanDone") ? 
				"" : ((DateTime)row["BuildPlanDone"]).ToString(Constants.TextDateFormat);
			txtBuildPlanNumber.Text = row.IsNull("BuildPlanNumber") ? 
				"" : (string)row["BuildPlanNumber"];
			SetStatus(txtBuildPlan,txtBuildPlanDone,imgPlanBuild);
			// pre visa
			txtPreVisa.Text = row.IsNull("PreVisa") ? 
				"" : ((DateTime)row["PreVisa"]).ToString(Constants.TextDateFormat);
			txtPreVisaDone.Text = row.IsNull("PreVisaDone") ? 
				"" : ((DateTime)row["PreVisaDone"]).ToString(Constants.TextDateFormat);
			txtPreVisaNumber.Text = row.IsNull("PreVisaNumber") ? 
				"" : (string)row["PreVisaNumber"];
			SetStatus(txtPreVisa,txtPreVisaDone,imgPreVisa);
			// visa
			txtVisa.Text = row.IsNull("Visa") ? 
				"" : ((DateTime)row["Visa"]).ToString(Constants.TextDateFormat);
			txtVisaDone.Text = row.IsNull("VisaDone") ? 
				"" : ((DateTime)row["VisaDone"]).ToString(Constants.TextDateFormat);
			txtVisaNumber.Text = row.IsNull("VisaNumber") ? 
				"" : (string)row["VisaNumber"];
			SetStatus(txtVisa,txtVisaDone,imgVisa);
			// visa plus
			txtVisaPlus.Text = row.IsNull("VisaPlus") ? 
				"" : ((DateTime)row["VisaPlus"]).ToString(Constants.TextDateFormat);
			txtVisaPlusDone.Text = row.IsNull("VisaPlusDone") ? 
				"" : ((DateTime)row["VisaPlusDone"]).ToString(Constants.TextDateFormat);
			txtVisaPlusNumber.Text = row.IsNull("VisaPlusNumber") ? 
				"" : (string)row["VisaPlusNumber"];
			SetStatus(txtVisaPlus,txtVisaPlusDone,imgVisaPlus);
			// kadaster und
			txtKadasterUndEnt.Text = row.IsNull("KadasterUndEnt") ? 
				"" : ((DateTime)row["KadasterUndEnt"]).ToString(Constants.TextDateFormat);
			txtKadasterUndRec.Text = row.IsNull("KadasterUndRec") ? 
				"" : ((DateTime)row["KadasterUndRec"]).ToString(Constants.TextDateFormat);
			txtKadasterUndNumber.Text = row.IsNull("KadasterUndNumber") ? 
				"" : (string)row["KadasterUndNumber"];
			SetStatus(txtKadasterUndEnt,txtKadasterUndRec,imgKadUnd);
			// kadaster
			txtKadasterEnt.Text = row.IsNull("KadasterEnt") ? 
				"" : ((DateTime)row["KadasterEnt"]).ToString(Constants.TextDateFormat);
			txtKadasterRec.Text = row.IsNull("KadasterRec") ? 
				"" : ((DateTime)row["KadasterRec"]).ToString(Constants.TextDateFormat);
			txtKadasterNumber.Text = row.IsNull("KadasterNumber") ? 
				"" : (string)row["KadasterNumber"];
			SetStatus(txtKadasterEnt,txtKadasterRec,imgKad);

			if(IsSet(imgKad) &&  IsSet(imgKadUnd) && IsSet(imgVisaPlus) && IsSet(imgVisa)&& IsSet(imgPreVisa)
				&& IsSet(imgPlanBuild) &&  IsSet(imgPlanReg) && IsSet(imgActual) && IsSet(imgAtt)&& IsSet(imgSketch)
				&& IsSet(imgNotary)&& IsSet(imgOther))
				SetStatus(imgMain);
			#endregion
    
		}

		#endregion

		private void LoadProfile()
		{
			if (this.ProjectID == -1) Response.Redirect("Projects.aspx");

			LoadProjectProfile1();
			LoadProjectProfile2();
		}

		#endregion
		public  DateTime GetDate(TextBox t)
		{
			DateTime dt = Constants.DateMax;
			string s= t.Text;
			try
			{
				if(s!="")
					dt=DateTime.ParseExact(s,Constants.TextDateFormat,System.Globalization.NumberFormatInfo.InvariantInfo);
				return dt;
			}
			catch
			{
				log.Error("Invalid Date");
				return dt;
			}
		}
		private void SaveProjectProfile2()
		{
			ProjectsData.ExecuteProjectsUpdateProfile2(this.ProjectID,GetDate(txtVIKEnt),GetDate(txtVIKRec),Constants.DateMax,
				txtVIKNum.Text,GetDate(txtVIKProjAss),GetDate(txtVIKProjAgg),GetDate(txtVIKLetter),GetDate(txtVIKContract),UIHelpers.ParseDecimal(txtVIKAmount.Text),
				GetDate(txtELEnt),GetDate(txtELRec),Constants.DateMax,
				txtElNumber.Text,GetDate(txtELProjAss),GetDate(txtELProjAgg),GetDate(txtELLetter),GetDate(txtELContract),UIHelpers.ParseDecimal(txtELAmount.Text),
				GetDate(txtTEnt),GetDate(txtTRec),Constants.DateMax,
				txtTNum.Text,GetDate(txtTProjAss),GetDate(txtTProjAgg),GetDate(txtTLetter),GetDate(txtTContract),UIHelpers.ParseDecimal(txtTAmount.Text),
				GetDate(txtPathEnt),GetDate(txtPathRec),GetDate(txtPathDate),
				txtPathNumber.Text,GetDate(txtPathProjAss),GetDate(txtPathProjAgg),UIHelpers.ParseDecimal(txtPathAmount.Text),
				GetDate(txtPhoneEnt),GetDate(txtPhoneRec),Constants.DateMax,
				txtPhoneNumber.Text,
				GetDate(txtIdeaProjectDone),txtIdeaProjectNotes.Text, txtIdeaProjectMun.Text, txtIdeaProjectNumber.Text,GetDate(txtIdeaProjectAgreed),
				GetDate(txtTechProjectDone),txtTechProjectNotes.Text, txtTechProjectMun.Text, txtTechProjectNumber.Text,GetDate(txtTechProjectAgreed),
				GetDate(txtBuildPermission),GetDate(txtBuildPermissionDone),txtBuildPermissionNumber.Text,
				GetDate(txtOther),GetDate(txtOtherDone),txtOtherNumber.Text,UIHelpers.ParseDecimal(txtOtherExpenses.Text),txtVIKNotes.Text,
				txtELNotes.Text, txtTNotes.Text, txtPathNotes.Text,txtPhoneNotes.Text,UIHelpers.ParseDecimal(txtPhoneAmount.Text),txtBuildPermissionNotes.Text);

			
		}
		#region Save profile1

		private void SaveProjectProfile1()
		{

			#region Collect values

			object otherDeeds = null, otherDeedsDone = null, notaryDeeds = null, notaryDeedsDone = null, currentStatus = null, currentStatusDone = null,
				sketch = null, sketchDone = null;

			//other deeds
			try
			{
				otherDeeds = DateTime.ParseExact(txtOtherDeeds.Text, Constants.TextDateFormat, System.Globalization.CultureInfo.InvariantCulture);
			}
			catch
			{}
			try
			{
				otherDeedsDone = DateTime.ParseExact(txtOtherDeedsDone.Text, Constants.TextDateFormat, System.Globalization.CultureInfo.InvariantCulture);
			}
			catch
			{}
			//notary deeds
			try
			{
				notaryDeeds = DateTime.ParseExact(txtNotaryDeeds.Text, Constants.TextDateFormat, System.Globalization.CultureInfo.InvariantCulture);
			}
			catch
			{}
			try
			{
				notaryDeedsDone = DateTime.ParseExact(txtNotaryDeedsDone.Text, Constants.TextDateFormat, System.Globalization.CultureInfo.InvariantCulture);
			}
			catch
			{}

			//current status
			try
			{
				currentStatus = DateTime.ParseExact(txtCurrentStatus.Text, Constants.TextDateFormat, System.Globalization.CultureInfo.InvariantCulture);
			}
			catch
			{}
			try
			{
				currentStatusDone = DateTime.ParseExact(txtCurrentStatusDone.Text, Constants.TextDateFormat, System.Globalization.CultureInfo.InvariantCulture);
			}
			catch
			{}

			//sketch
			try
			{
				sketch = DateTime.ParseExact(txtSketch.Text, Constants.TextDateFormat, System.Globalization.CultureInfo.InvariantCulture);
			}
			catch
			{}
			try
			{
				sketchDone = DateTime.ParseExact(txtSketchDone.Text, Constants.TextDateFormat, System.Globalization.CultureInfo.InvariantCulture);
			}
			catch
			{}

			// power of attorney
			object powerOfAttorney = null, powerOfAttorneyDate = null, powerOfAttorneyDone = null, powerOfAttorneyDoneDate = null;
      
			try
			{
				powerOfAttorney = int.Parse(txtPowerOfAttorney.Text);
			}
			catch
			{}
			try
			{
				powerOfAttorneyDate = DateTime.ParseExact(txtPowerOfAttorneyDate.Text, Constants.TextDateFormat, System.Globalization.CultureInfo.InvariantCulture);
			}
			catch
			{}
			try
			{
				powerOfAttorneyDone = int.Parse(txtPowerOfAttorneyDone.Text);
			}
			catch
			{}
			try
			{
				powerOfAttorneyDoneDate = DateTime.ParseExact(txtPowerOfAttorneyDoneDate.Text, Constants.TextDateFormat, System.Globalization.CultureInfo.InvariantCulture);
			}
			catch
			{}

			//reg plan
			object regPlan = null, regPlanDone = null; 
			try
			{
				regPlan = DateTime.ParseExact(txtRegPlan.Text, Constants.TextDateFormat, System.Globalization.CultureInfo.InvariantCulture);
			}
			catch
			{}
			try
			{
				regPlanDone = DateTime.ParseExact(txtRegPlanDone.Text, Constants.TextDateFormat, System.Globalization.CultureInfo.InvariantCulture);
			}
			catch
			{}
			string regPlanNumber = txtRegPlanNumber.Text.Trim();
			if (regPlanNumber.Length == 0) regPlanNumber = null;

			//build plan
			object buildPlan = null, buildPlanDone = null; 
			try
			{
				buildPlan = DateTime.ParseExact(txtBuildPlan.Text, Constants.TextDateFormat, System.Globalization.CultureInfo.InvariantCulture);
			}
			catch
			{}
			try
			{
				buildPlanDone = DateTime.ParseExact(txtBuildPlanDone.Text, Constants.TextDateFormat, System.Globalization.CultureInfo.InvariantCulture);
			}
			catch
			{}
			string buildPlanNumber = txtBuildPlanNumber.Text.Trim();
			if (buildPlanNumber.Length == 0) buildPlanNumber = null;

			//pre visa
			object preVisa = null, preVisaDone = null; 
			try
			{
				preVisa = DateTime.ParseExact(txtPreVisa.Text, Constants.TextDateFormat, System.Globalization.CultureInfo.InvariantCulture);
			}
			catch
			{}
			try
			{
				preVisaDone = DateTime.ParseExact(txtPreVisaDone.Text, Constants.TextDateFormat, System.Globalization.CultureInfo.InvariantCulture);
			}
			catch
			{}
			string preVisaNumber = txtPreVisaNumber.Text.Trim();
			if (preVisaNumber.Length == 0) preVisaNumber = null;

			//visa
			object visa = null, visaDone = null; 
			try
			{
				visa = DateTime.ParseExact(txtVisa.Text, Constants.TextDateFormat, System.Globalization.CultureInfo.InvariantCulture);
			}
			catch
			{}
			try
			{
				visaDone = DateTime.ParseExact(txtVisaDone.Text, Constants.TextDateFormat, System.Globalization.CultureInfo.InvariantCulture);
			}
			catch
			{}
			string visaNumber = txtVisaNumber.Text.Trim();
			if (visaNumber.Length == 0) visaNumber = null;

			//visa plus
			object visaPlus = null, visaPlusDone = null; 
			try
			{
				visaPlus = DateTime.ParseExact(txtVisaPlus.Text, Constants.TextDateFormat, System.Globalization.CultureInfo.InvariantCulture);
			}
			catch
			{}
			try
			{
				visaPlusDone = DateTime.ParseExact(txtVisaPlusDone.Text, Constants.TextDateFormat, System.Globalization.CultureInfo.InvariantCulture);
			}
			catch
			{}
			string visaPlusNumber = txtVisaPlusNumber.Text.Trim();
			if (visaPlusNumber.Length == 0) visaPlusNumber = null;

			//kadaster und
			object kadasterUndEnt = null, kadasterUndRec = null; 
			try
			{
				kadasterUndEnt = DateTime.ParseExact(txtKadasterUndEnt.Text, Constants.TextDateFormat, System.Globalization.CultureInfo.InvariantCulture);
			}
			catch
			{}
			try
			{
				kadasterUndRec = DateTime.ParseExact(txtKadasterUndRec.Text, Constants.TextDateFormat, System.Globalization.CultureInfo.InvariantCulture);
			}
			catch
			{}
			string kadasterUndNumber = txtKadasterUndNumber.Text.Trim();
			if (kadasterUndNumber.Length == 0) kadasterUndNumber = null;

			//kadaster
			object kadasterEnt = null, kadasterRec = null; 
			try
			{
				kadasterEnt = DateTime.ParseExact(txtKadasterEnt.Text, Constants.TextDateFormat, System.Globalization.CultureInfo.InvariantCulture);
			}
			catch
			{}
			try
			{
				kadasterRec = DateTime.ParseExact(txtKadasterRec.Text, Constants.TextDateFormat, System.Globalization.CultureInfo.InvariantCulture);
			}
			catch
			{}
			string kadasterNumber = txtKadasterNumber.Text.Trim();
			if (kadasterNumber.Length == 0) kadasterNumber = null;
      
			#endregion

			try
			{
				ProjectsData.UpdateProjectProfile1(this.ProjectID, 
					
					notaryDeeds, notaryDeedsDone, 
					currentStatus, currentStatusDone,
					sketch, sketchDone, 
					regPlan, regPlanDone, regPlanNumber, 
					buildPlan, buildPlanDone, buildPlanNumber,
					powerOfAttorney, powerOfAttorneyDate, powerOfAttorneyDone, powerOfAttorneyDoneDate, 
					preVisa, preVisaDone, preVisaNumber,
					visa, visaDone, visaNumber, 
					visaPlus, visaPlusDone, visaPlusNumber, 
					kadasterUndEnt, kadasterUndRec, kadasterUndNumber,
					kadasterEnt, kadasterRec, kadasterNumber);
			}
			catch (Exception ex)
			{
				log.Error(ex.ToString());
			}

		}

		private void SaveProfile()
		{
			SaveProjectProfile1();
			SaveProjectProfile2();


			int PID = UIHelpers.GetPIDParam();
			string projectName, projectCode, administrativeName,add;
			decimal area=0;
			int clientID=0;
			bool phases;
			if (!ProjectsData.SelectProject(PID, out projectName, out  projectCode, out administrativeName, out area, out add, 
				out clientID,out phases))
			{
				log.Error("Project not found " + PID);
				
			}
			//Mail
			if(ViewState["iVIKProj"]!=null && (string)ViewState["iVIKProj"]==MAIL_STRING)
			{
				if(GetDate(txtVIKProjAgg)!=Constants.DateMax
					&& GetDate(txtVIKProjAss)!=Constants.DateMax)
				{
					MailBO.SendProjectMail(administrativeName,Label9.Text,txtVIKAmount.Text, txtVIKProjAgg.Text,projectName, projectCode);
				}
			}
			if(ViewState["iELProject"]!=null && (string)ViewState["iELProject"]==MAIL_STRING)
			{
				if(GetDate(txtELProjAgg)!=Constants.DateMax
					&& GetDate(txtELProjAss)!=Constants.DateMax)
				{
					MailBO.SendProjectMail(administrativeName,Label4.Text,txtELAmount.Text,txtELProjAgg.Text,projectName, projectCode);
				}
			}
			if(ViewState["iTProject"]!=null && (string)ViewState["iTProject"]==MAIL_STRING)
			{
				if(GetDate(txtTProjAgg)!=Constants.DateMax
					&& GetDate(txtTProjAss)!=Constants.DateMax)
				{
					MailBO.SendProjectMail(administrativeName,Label17.Text,txtTAmount.Text,txtTProjAss.Text,projectName, projectCode);
				}
			}
		
			if(ViewState["iPathProj"]!=null && (string)ViewState["iPathProj"]==MAIL_STRING)
			{
				if(GetDate(txtPathProjAgg)!=Constants.DateMax
					&& GetDate(txtPathProjAss)!=Constants.DateMax)
				{
					MailBO.SendProjectMail(administrativeName,Label22.Text,txtPathAmount.Text,txtPathProjAgg.Text,projectName, projectCode);
				}
			}
			if(ViewState["iPhoneEnt"]!=null && (string)ViewState["iPhoneEnt"]==MAIL_STRING)
			{
				if(GetDate(txtPhoneEnt)!=Constants.DateMax
					&& GetDate(txtPhoneRec)!=Constants.DateMax)
				{
					MailBO.SendProjectMail(administrativeName,Label27.Text,txtPhoneAmount.Text,txtPhoneRec.Text,projectName, projectCode);
				}
			}
			if(ViewState["iRazr"]!=null && (string)ViewState["iRazr"]==MAIL_STRING)
			{
				if(GetDate(txtBuildPermissionDone)!=Constants.DateMax
					)
				{
					MailBO.SendProjectMail1(administrativeName,txtBuildPermissionDone.Text,projectName, projectCode);
				}
			}
		}

		#endregion

		#endregion
		
		#region Properties

		private int ProjectID
		{
			get 
			{
				try
				{
					return int.Parse(Request.Params["pid"]);
				}
				catch
				{
					return -1;
				}
			}		
		}

		#endregion

		#region Event handlers

		private void btnSave_Click(object sender, System.EventArgs e)
		{
			SaveProfile();
			SaveDocuments();
			int div =UIHelpers.ToInt(hdn.Value);
		
			Response.Redirect("Profile.aspx?PID="+UIHelpers.GetPIDParam().ToString()+"&div="+div.ToString());
		}

		private void btnCancel_Click(object sender, System.EventArgs e)
		{
			Response.Redirect("EditProject.aspx?PID="+UIHelpers.GetPIDParam().ToString());
		}

		#endregion

		#region Menu
		//		private void CreateMenu()
		//		{
		//			UserControls.MenuTable menu = new UserControls.MenuTable();
		//			menu.ID = "MenuTable";
		//
		//			ArrayList menuItems = new ArrayList();
		//
		//			menuItems.Add(new MenuItemInfo(Resource.ResourceManager["menuItem_Hours"], "Hours.aspx", true));
		//			menuItems.Add(new MenuItemInfo(Resource.ResourceManager["menuItem_Projects"], "Projects.aspx"));
		//			if ((LoggedUser.IsLeader) || (LoggedUser.IsAssistant)) 
		//			{
		//				menuItems.Add(new MenuItemInfo(Resource.ResourceManager["menuItem_Clients"], "Clients.aspx"));
		//				menuItems.Add(new MenuItemInfo(Resource.ResourceManager["menuItem_Subcontracters"], "Subcontracters.aspx"));
		//				if (LoggedUser.IsLeader)
		//				menuItems.Add(new MenuItemInfo(Resource.ResourceManager["menuItem_Users"], "Users.aspx"));	
		//			}
		//			menuItems.Add(new MenuItemInfo(Resource.ResourceManager["menuItem_Emails"], "Emails.aspx"));
		//
		//			menu.AddMenuGroup(Resource.ResourceManager["menuHeader_Links"], 20, menuItems);
		//			
		//			//'New' group
		//			menuItems = new ArrayList();
		//
		//			if ((LoggedUser.IsLeader) || (LoggedUser.IsAssistant)) 
		//			{
		//				menuItems.Add(new MenuItemInfo(Resource.ResourceManager["menuItem_NewClient"], "EditClient.aspx"));
		//				if (LoggedUser.IsLeader)
		//				menuItems.Add(new MenuItemInfo(Resource.ResourceManager["menuItem_NewUser"], "EditUser.aspx"));
		//			}
		//			menuItems.Add(new MenuItemInfo(Resource.ResourceManager["menuItem_NewEmail"], "EditEmail.aspx"));
		//			menuItems.Add(new MenuItemInfo(Resource.ResourceManager["newcontracter_PageTitle"], "EditSubContracter.aspx"));
		//
		//			menu.AddMenuGroup(Resource.ResourceManager["menuHeader_New"], 30, menuItems);
		//
		//			// REports
		//			menuItems = new ArrayList();
		//			menuItems.Add(new MenuItemInfo(Resource.ResourceManager["menuItem_WorkTimeReport"], "reports/worktimes.aspx"));
		//			menuItems.Add(new MenuItemInfo(Resource.ResourceManager["menuItem_MinutesReport"], "Reports/Minutes.aspx"));
		//			if (LoggedUser.IsLeader || LoggedUser.IsAssistant)
		//				menuItems.Add(new MenuItemInfo(Resource.ResourceManager["menuItem_MailReport"], "MailForm.aspx"));
		//			if (LoggedUser.IsLeader)
		//			{
		//				menuItems.Add(new MenuItemInfo(Resource.ResourceManager["menuItem_NotEnteredWorkTime"], "EmptyHours.aspx"));
		//				menuItems.Add(new MenuItemInfo(Resource.ResourceManager["menuItem_Calendar"], "reports/Calendar.aspx"));
		//			}
		//
		//			menu.AddMenuGroup(Resource.ResourceManager["menuHeader_Reports"], 30, menuItems);
		//
		//			//Help
		//			menuItems = new ArrayList();
		//			menuItems.Add(new MenuItemInfo(Resource.ResourceManager["menuItem_Help"], SessionManager.GetHelpLink(),true,Pages.Profile));
		//			menu.AddMenuGroup("", 10, menuItems);
		//
		//			menuHolder.Controls.Add(menu);
		//		}

	
		#endregion

		private void btnPrint_Click(object sender, System.EventArgs e)
		{
			PdfExport pdf = new PdfExport();
			pdf.NeverEmbedFonts = "";


			DataDynamics.ActiveReports.ActiveReport report = new Reports.ProjectDocumentation(UIHelpers.GetPIDParam(),true,1);
			System.IO.MemoryStream memoryFile = new System.IO.MemoryStream();
			report.Run();
			pdf.Export(report.Document, memoryFile);
			Response.Clear();

			Response.AppendHeader( "content-disposition","attachment; filename="+"Profile.pdf");
			Response.ContentType = "application/pdf";


			memoryFile.WriteTo(Response.OutputStream); 
			Response.End(); 
		}

    

	}
}
