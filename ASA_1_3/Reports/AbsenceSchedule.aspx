﻿<%@ Page Title="" Language="C#" MasterPageFile="~/timesheet.Master" AutoEventWireup="true" CodeBehind="AbsenceSchedule.aspx.cs" Inherits="Asa.Timesheet.WebPages.Reports.AbsenceSchedule" %>
<%@ Register TagPrefix="uc1" TagName="PageHeader" Src="~/header.ascx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style>
        .table-sm{font-size: x-small;}
        .table-sm td{ border: none; white-space:pre}
        .sticky {position: sticky; left: 0px; background: white; font-weight: 900; font-size: small}
        .table-sm th{ border: none; white-space:pre}
        .monthName{font-size: xx-large; border-top: thin solid gray; }
        .monthName td{border: none; background: #fafafa}
        .fa-suitcase{color: #00487c}
    </style>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="header" runat="server">
    <uc1:pageheader id="header" runat="server"></uc1:pageheader></td>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
<div class="container-fluid">
    <asp:Button runat="server" id="btnLogin" Text="SeeVacations" OnClick="btnSeeVacations_Click" Visible="False"/>
    <asp:GridView autogeneratecolumns="False" ID="GridView1" runat="server" onrowdatabound="VacationsGridView_RowDataBound" 
        OnRowCreated="VacationsGridView_RowCreated" 
        border="0" CssClass="table table-sm table-responsive">
        <Columns>
        <asp:BoundField DataField="UserID" HeaderText="№" Visible="false" />
        <asp:BoundField DataField="FullName" HeaderText="Име" />
    </Columns>
    </asp:GridView>
</div>
</asp:Content>
