using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Data.SqlClient;
using log4net;
using Asa.Timesheet.Data;
using Asa.Timesheet.Data.Entities;
using Asa.Timesheet.Data.Util;
using GrapeCity.ActiveReports.Export.Pdf.Section;
using GrapeCity.ActiveReports.Export.Excel.Section;

namespace Asa.Timesheet.WebPages.Reports
{
	/// <summary>
	/// Summary description for WorkTimes.
	/// </summary>
	public class SubcontracterAnalisis : TimesheetPageBase
	{
		private static readonly ILog log = LogManager.GetLogger(typeof(WorkTimes));
		protected System.Web.UI.WebControls.DropDownList ddlBuildingTypes;
		#region Web controls

		protected System.Web.UI.WebControls.Label lblTitle;
		protected System.Web.UI.WebControls.Label lblUser;
		protected System.Web.UI.WebControls.PlaceHolder menuHolder;
		protected System.Web.UI.WebControls.DropDownList ddlPeriodType;
		protected System.Web.UI.WebControls.Label lblStartDay;
		protected System.Web.UI.WebControls.Label lblEndDay;
		protected System.Web.UI.WebControls.Calendar calStartDate;
		protected System.Web.UI.WebControls.Calendar calEndDate;
		protected System.Web.UI.WebControls.Label lblSelStartDate;
		protected System.Web.UI.WebControls.Label lblSelEndDate;
		protected System.Web.UI.WebControls.Label lblError;
		protected System.Web.UI.WebControls.Label lblInfo;
		protected System.Web.UI.WebControls.ImageButton ibGenerate;
		protected System.Web.UI.WebControls.ImageButton ibPdfExport;
		protected System.Web.UI.WebControls.ImageButton ibXlsExport;
		protected Asa.Timesheet.WebPages.UserControls.PageHeader header;
		protected System.Web.UI.WebControls.CheckBox ckDates;
		protected System.Web.UI.WebControls.DataGrid grdReport;
		protected System.Web.UI.WebControls.Label lblReportTitle;
		protected System.Web.UI.WebControls.Label lblCreatedBy;
		protected System.Web.UI.HtmlControls.HtmlTable tblFooter;
		protected System.Web.UI.WebControls.Label lblCreatedByLbl;
		protected System.Web.UI.WebControls.Label Label2;
		protected System.Web.UI.WebControls.DropDownList ddlClients;
		protected System.Web.UI.WebControls.DropDownList ddlType;
		protected System.Web.UI.WebControls.Label Label4;
		protected System.Web.UI.WebControls.Label lblObekt;
		protected System.Web.UI.WebControls.Label Label1;
		protected System.Web.UI.HtmlControls.HtmlTable tblHeader;
		protected System.Web.UI.WebControls.Label Label5;
		protected System.Web.UI.WebControls.Label lblNoDataFound;
		protected System.Web.UI.WebControls.Label lblProject;
		protected System.Web.UI.WebControls.Label Label3;
		protected System.Web.UI.WebControls.Label Label6;
		protected System.Web.UI.WebControls.Label lbZad;
		protected System.Web.UI.WebControls.Label lbProject;
    protected System.Web.UI.WebControls.DropDownList ddlProjectsStatus;
		protected System.Web.UI.WebControls.Label Label8;
		protected System.Web.UI.WebControls.Label Label7;
		protected System.Web.UI.WebControls.DropDownList ddlProject;
		#endregion

		#region Page load

        private void Page_Load(object sender, System.EventArgs e)
        {
            if (!(this.LoggedUser.HasPaymentRights || this.LoggedUser.IsASI)) ErrorRedirect(Resource.ResourceManager["noRightsAlert"]);
            if (!this.IsPostBack)
            {
                header.PageTitle = Resource.ResourceManager["SubcontracterAnalysis_PageTitle"];
                header.UserName = this.LoggedUser.UserName;






                calStartDate.SelectedDate = calStartDate.VisibleDate = new DateTime(DateTime.Today.Year, DateTime.Today.Month, 1);
                lblSelStartDate.Text = "(" + Resource.ResourceManager["reports_SelectDate"] + ")";
                calEndDate.SelectedDate = calEndDate.VisibleDate = DateTime.Today;
                lblSelEndDate.Text = "(" + Resource.ResourceManager["reports_SelectDate"] + ")";
                UIHelpers.LoadProjectStatus(ddlProjectsStatus, (int)ProjectsData.ProjectsByStatus.Active);
                UIHelpers.LoadBuildingTypes("", ddlBuildingTypes);
                LoadClients("0");
                LoadProjects();

            }
            else
                UIHelpers.LoadBuildingTypes(ddlBuildingTypes.SelectedValue, ddlBuildingTypes, (ProjectsData.ProjectsByStatus)int.Parse(ddlProjectsStatus.SelectedValue));



            if (Request.Params["type"] == "ASI")
            {
                ddlClients.SelectedValue = System.Configuration.ConfigurationManager.AppSettings["ASI"];
                ddlClients.Visible = Label2.Visible = false;
                lblReportTitle.Text = header.PageTitle = Resource.ResourceManager["SubcontracterAnalysis_ASI"];
            }
            UIHelpers.CreateMenuReports(menuHolder, LoggedUser);

        }
        private bool LoadClients(string selectedValue)
        {
            if (Request.Params["type"] == "ASI")
                ddlClients.DataSource = SubprojectsUDL.SelectSubcontracters(-1, SortOrder, true, -1, false, false, -1, (int)ProjectsData.ProjectsByStatus.AllProjects);
            else
                ddlClients.DataSource = SubprojectsUDL.SelectSubcontracters(-1, SortOrder, true, UIHelpers.ToInt(ddlProject.SelectedValue), false, false, UIHelpers.ToInt(ddlBuildingTypes.SelectedValue), UIHelpers.ToInt(ddlProjectsStatus.SelectedValue));
            ddlClients.DataValueField = "SubcontracterID";
            ddlClients.DataTextField = "SubcontracterName";
            ddlClients.DataBind();

            ddlClients.Items.Insert(0, new ListItem(Resource.ResourceManager["reports_ddlAllSub"], "0"));
            if (ddlClients.Items.FindByValue(selectedValue) != null)
                ddlClients.SelectedValue = selectedValue;

            return true;
        }
		#endregion

		#region Web Form Designer generated code
        override protected void OnInit(EventArgs e)
        {
            //
            // CODEGEN: This call is required by the ASP.NET Web Form Designer.
            //
            InitializeComponent();
            base.OnInit(e);
        }
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
        private void InitializeComponent()
        {
            this.ckDates.CheckedChanged += new System.EventHandler(this.ckDates_CheckedChanged);
            this.calStartDate.SelectionChanged += new System.EventHandler(this.calStartDate_SelectionChanged);
            this.calEndDate.SelectionChanged += new System.EventHandler(this.calEndDate_SelectionChanged);
            this.ddlProjectsStatus.SelectedIndexChanged += new System.EventHandler(this.ddlProjectsStatus_SelectedIndexChanged);
            this.ddlBuildingTypes.SelectedIndexChanged += new System.EventHandler(this.ddlBuildingTypes_SelectedIndexChanged);
            this.ddlProject.SelectedIndexChanged += new System.EventHandler(this.ddlProject_SelectedIndexChanged);
            this.ibGenerate.Click += new System.Web.UI.ImageClickEventHandler(this.ibGenerate_Click);
            this.ibPdfExport.Click += new System.Web.UI.ImageClickEventHandler(this.ibPdfExport_Click);
            this.ibXlsExport.Click += new System.Web.UI.ImageClickEventHandler(this.ibXlsExport_Click);
            this.grdReport.ItemDataBound += new System.Web.UI.WebControls.DataGridItemEventHandler(this.grdReport_ItemDataBound);
            this.Load += new System.EventHandler(this.Page_Load);

        }
		#endregion

		


	

		#region Menu

//		private void CreateMenu()
//		{
//			UserControls.MenuTable menu = new UserControls.MenuTable();
//			menu.ID = "MenuTable";
//
//			// 'Links' group
//			ArrayList menuItems = new ArrayList();
//
//			menuItems.Add(new MenuItemInfo(Resource.ResourceManager["menuItem_Hours"], "../Hours.aspx", true));
//			menuItems.Add(new MenuItemInfo(Resource.ResourceManager["menuItem_Projects"], "../Projects.aspx"));
//			if (LoggedUser.IsLeader || LoggedUser.IsAssistant) 
//			{
//				menuItems.Add(new MenuItemInfo(Resource.ResourceManager["menuItem_Clients"], "../Clients.aspx"));
//								menuItems.Add(new MenuItemInfo(Resource.ResourceManager["menuItem_Subcontracters"], "../Subcontracters.aspx"));
//				if (LoggedUser.IsLeader)
//				menuItems.Add(new MenuItemInfo(Resource.ResourceManager["menuItem_Users"], "../Users.aspx"));	
//			}
//			menuItems.Add(new MenuItemInfo(Resource.ResourceManager["menuItem_Emails"], "../Emails.aspx"));
//
//			menu.AddMenuGroup(Resource.ResourceManager["menuHeader_Links"], 20, menuItems);
//			
//			// 'New' group
//			menuItems = new ArrayList();
//			menuItems.Add(new MenuItemInfo(Resource.ResourceManager["menuItem_NewProject"], "../EditProject.aspx"));
//			if (LoggedUser.IsLeader || LoggedUser.IsAssistant)
//			{
//				menuItems.Add(new MenuItemInfo(Resource.ResourceManager["menuItem_NewClient"], "../EditClient.aspx"));
//				if (LoggedUser.IsLeader)
//				menuItems.Add(new MenuItemInfo(Resource.ResourceManager["menuItem_NewUser"], "../EditUser.aspx"));
//			}
//			menuItems.Add(new MenuItemInfo(Resource.ResourceManager["menuItem_NewEmail"], "../EditEmail.aspx"));
//			menuItems.Add(new MenuItemInfo(Resource.ResourceManager["newcontracter_PageTitle"], "../EditSubContracter.aspx"));
//
//
//			menu.AddMenuGroup(Resource.ResourceManager["menuHeader_New"], 30, menuItems);
//
//			// "Reports' group
//			menuItems = new ArrayList();
//			//menuItems.Add(new MenuItemInfo(Resource.ResourceManager["menuItem_WorkTimeReport"], "#"));
//			menuItems.Add(new MenuItemInfo(Resource.ResourceManager["menuItem_MinutesReport"], "Minutes.aspx"));
//			if (LoggedUser.IsLeader || LoggedUser.IsAssistant)
//				menuItems.Add(new MenuItemInfo(Resource.ResourceManager["menuItem_MailReport"], "../MailForm.aspx"));
//			
//			if (LoggedUser.IsLeader)
//			{
//				menuItems.Add(new MenuItemInfo(Resource.ResourceManager["menuItem_NotEnteredWorkTime"], "EmptyHours.aspx"));
//				menuItems.Add(new MenuItemInfo(Resource.ResourceManager["menuItem_Calendar"], "Calendar.aspx"));
//			}
//			menu.AddMenuGroup(Resource.ResourceManager["menuHeader_Reports"], 30, menuItems);
//
//			
//
//			menuItems = new ArrayList();
//			menuItems.Add(new MenuItemInfo(Resource.ResourceManager["menuItem_Help"], SessionManager.GetReportsHelpLink(),true,Pages.WorkTimes));
//			menu.AddMenuGroup("", 10, menuItems);
//
//
//			menuHolder.Controls.Add(menu);
//		}

		#endregion


        protected string GetImage(object Done)
        {
            if (Done == null || Done == System.DBNull.Value)
                return "~/images/delete.png";
            int nDone = (int)Done;
            if (nDone == 1)
                return "~/images/done.gif";
            return "~/images/delete.png";
        }
        private void SetVis()
        {
            Label5.Visible = true;
            Label3.Visible = true;
            if (Request.Params["type"] != "ASI")
            {
                Label6.Visible = true;
                lblObekt.Text = ddlClients.SelectedItem.Text;
            }
            lblReportTitle.Visible = true;
            lblObekt.Visible = true;
            lblCreatedBy.Visible = true;
            lblCreatedByLbl.Visible = true;

            lbProject.Text = ddlProject.SelectedItem.Text;
            lbProject.Visible = true;
            lbZad.Visible = true;

            lbZad.Text = ddlType.SelectedItem.Text;
        }
        private void ibGenerate_Click(object sender, System.Web.UI.ImageClickEventArgs e)
        {
            SetVis();
            int nID = int.Parse(ddlClients.SelectedValue);
            lblCreatedBy.Text = this.LoggedUser.UserName;

            int nType = int.Parse(ddlType.SelectedValue);
            DateTime dtStart = Constants.DateMax;
            DateTime dtEnd = Constants.DateMax;
            if (!ckDates.Checked)
            {
                dtStart = calStartDate.SelectedDate;
                dtEnd = calEndDate.SelectedDate;
            }
            grdReport.DataSource = ReportsData.ExecuteReportsSubcontracterAnalysis(nID, dtStart, dtEnd, nType, int.Parse(ddlProject.SelectedValue), this.ProjectsStatus, LoggedUser.HasPaymentRights, UIHelpers.ToInt(ddlBuildingTypes.SelectedValue));
            grdReport.DataBind();
            if (grdReport.Items.Count == 0)
            {
                grdReport.Visible = false;
                lblNoDataFound.Visible = true;
                lblNoDataFound.Text = Resource.ResourceManager["reports_WorkTimes_NoDataMatch"];
            }
            else
                grdReport.Visible = true;
            grdReport.Columns[2].Visible = nID == 0;
        }
        private bool LoadProjects()
        {
            return UIHelpers.LoadProjects(ddlProject, ddlProjectsStatus, ddlBuildingTypes);
            //			SqlDataReader reader = null;
            //			
            //			try
            //			{
            //				reader = ProjectsData.SelectProjectNamesClear(this.ProjectsStatus, int.Parse(ddlBuildingTypes.SelectedValue));
            //			
            //				ddlProject.DataSource = reader;
            //				ddlProject.DataValueField = "ProjectID";
            //				ddlProject.DataTextField = "ProjectName";
            //				ddlProject.DataBind();
            //
            //				ddlProject.Items.Insert(0, new ListItem("<"+Resource.ResourceManager["reports_ddlAllProjects"]+">", "-1"));
            //				ddlProject.SelectedValue = "-1";
            //			}
            //			catch (Exception ex)
            //			{
            //				log.Error(ex);
            //				return false;
            //			}
            //
            //			finally
            //			{
            //				if (reader!=null) reader.Close();
            //			}
            //			return true;
        }
        private void grdReport_ItemDataBound(object sender, DataGridItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Item
                || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                if (e.Item.Cells[1].Text == "&nbsp;")
                {
                    e.Item.CssClass = "ReportsTotal";
                    //e.Item.Cells[5].Text="";
                    e.Item.Cells[6].Text = "";
                    e.Item.Cells[2].Text = "";
                    e.Item.Cells[6].Text = "";
                    e.Item.Cells[7].Text = "";
                    e.Item.Cells[8].Text = "";

                }
                else if (e.Item.Cells[0].Text == "&nbsp;")
                {
                    e.Item.CssClass = "ReportsSubTotal";
                    //e.Item.Cells[5].Text="";
                    e.Item.Cells[2].Text = "";
                    e.Item.Cells[6].Text = "";
                    e.Item.Cells[7].Text = "";
                    e.Item.Cells[8].Text = "";
                }

            }
        }

        private void ibPdfExport_Click(object sender, System.Web.UI.ImageClickEventArgs e)
        {
            PdfExport pdf = new PdfExport();

            pdf.NeverEmbedFonts = "";


            DateTime dtStart = Constants.DateMax;
            DateTime dtEnd = Constants.DateMax;
            if (!ckDates.Checked)
            {
                dtStart = calStartDate.SelectedDate;
                dtEnd = calEndDate.SelectedDate;
            }

            GrapeCity.ActiveReports.SectionReport report = new SubAnalysisRpt(true, lblReportTitle.Text, ddlType.SelectedItem.Text, ddlProject.SelectedItem.Text, ddlClients.SelectedItem.Text);
            report.PageSettings.Orientation = GrapeCity.ActiveReports.Document.Section.PageOrientation.Landscape;
            DataView dv = ReportsData.ExecuteReportsSubcontracterAnalysis(int.Parse(ddlClients.SelectedValue), dtStart, dtEnd, int.Parse(ddlType.SelectedValue), int.Parse(ddlProject.SelectedValue), this.ProjectsStatus, LoggedUser.HasPaymentRights, UIHelpers.ToInt(ddlBuildingTypes.SelectedValue));
            dv.RowFilter = "PaymentID is not null";
            report.DataSource = dv;
            report.Run();

            System.IO.MemoryStream memoryFile = new System.IO.MemoryStream();

            pdf.Export(report.Document, memoryFile);

            Response.Clear();
            Response.AppendHeader("content-disposition"
                , "attachment; filename=" + "SubcontracterAnalysis.pdf");
            Response.ContentType = "application/pdf";

            memoryFile.WriteTo(Response.OutputStream);
            Response.End();
        }

        private void ibXlsExport_Click(object sender, System.Web.UI.ImageClickEventArgs e)
        {
            XlsExport pdf = new XlsExport();


            DateTime dtStart = Constants.DateMax;
            DateTime dtEnd = Constants.DateMax;
            if (!ckDates.Checked)
            {
                dtStart = calStartDate.SelectedDate;
                dtEnd = calEndDate.SelectedDate;
            }
            GrapeCity.ActiveReports.SectionReport report = new SubAnalysisRpt(false, lblReportTitle.Text, ddlType.SelectedItem.Text, ddlProject.SelectedItem.Text, ddlClients.SelectedItem.Text);
            report.PageSettings.Orientation = GrapeCity.ActiveReports.Document.Section.PageOrientation.Landscape;
            DataView dv = ReportsData.ExecuteReportsSubcontracterAnalysis(int.Parse(ddlClients.SelectedValue), dtStart, dtEnd, int.Parse(ddlType.SelectedValue), int.Parse(ddlProject.SelectedValue), this.ProjectsStatus, LoggedUser.HasPaymentRights, UIHelpers.ToInt(ddlBuildingTypes.SelectedValue));
            dv.RowFilter = "PaymentID is not null";
            report.DataSource = dv;
            report.Run();


            System.IO.MemoryStream memoryFile = new System.IO.MemoryStream();
            pdf.Export(report.Document, memoryFile);

            Response.Clear();
            Response.AppendHeader("content-disposition"
                , "attachment; filename=" + "SubcontracterAnalysis.xls");
            Response.ContentType = "application/pdf";

            memoryFile.WriteTo(Response.OutputStream);
            Response.End();
        }

        private void ddlProjectsStatus_SelectedIndexChanged(object sender, System.EventArgs e)
        {
            LoadClients("0");
            LoadProjects();
        }

        private void ddlBuildingTypes_SelectedIndexChanged(object sender, System.EventArgs e)
        {
            LoadClients("0");
            LoadProjects();
        }
//tzveti
        private void ckDates_CheckedChanged(object sender, System.EventArgs e)
        {
            bool bEnable = !ckDates.Checked;
            calStartDate.Enabled = calEndDate.Enabled = bEnable;

            if (bEnable)
            {
                lblSelStartDate.Text = String.Concat("(" + calStartDate.SelectedDate.ToString("d"), ")");

                lblSelEndDate.Text = String.Concat("(" + calEndDate.SelectedDate.ToString("d"), ")");
            }
            else
            {
                lblSelStartDate.Text = lblSelEndDate.Text = String.Concat("(", Resource.ResourceManager["reports_SelectDate"], ")");
            }

        }

        private void calStartDate_SelectionChanged(object sender, System.EventArgs e)
        {
            lblSelStartDate.Text = String.Concat("(", calStartDate.SelectedDates[0].ToString("dd.MM.yyyy"), ")");
            if (calStartDate.SelectedDates.Count > 1)
            {
                calEndDate.SelectedDate = calStartDate.SelectedDates[calStartDate.SelectedDates.Count - 1];
                calEndDate.VisibleDate = calEndDate.SelectedDate;

                calStartDate.SelectedDate = calStartDate.SelectedDates[0];
                lblSelEndDate.Text = String.Concat("(", calEndDate.SelectedDate.ToString("dd.MM.yyyy"), ")"); ;

            }
        }

        private void calEndDate_SelectionChanged(object sender, System.EventArgs e)
        {
            lblSelEndDate.Text = String.Concat("(", calEndDate.SelectedDates[0].ToString("dd.MM.yyyy"), ")");

        }

        private void ddlProject_SelectedIndexChanged(object sender, System.EventArgs e)
        {
            LoadClients("0");
        }


    #region Properties

    public ProjectsData.ProjectsByStatus ProjectsStatus
    {
      get
      {
        try
        {
          return  (ProjectsData.ProjectsByStatus)int.Parse(ddlProjectsStatus.SelectedValue);
        }
        catch { return ProjectsData.ProjectsByStatus.AllProjects; }
      }
    }

    #endregion
	
	}
}
