using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Data.SqlClient;
using log4net;
using Asa.Timesheet.Data;
using Asa.Timesheet.Data.Entities;
using Asa.Timesheet.Data.Util;
using Asa.Timesheet.Data.Reports;
using GrapeCity.ActiveReports.Export.Pdf.Section;
using GrapeCity.ActiveReports.Export.Excel.Section;

namespace Asa.Timesheet.WebPages.Reports
{
	/// <summary>
	/// Summary description for WorkTimes.
	/// </summary>
	public class FoldersReportPage : TimesheetPageBase
	{
		private static readonly ILog log = LogManager.GetLogger(typeof(FoldersReportPage));

		#region Web controls

		protected System.Web.UI.WebControls.PlaceHolder menuHolder;
		protected System.Web.UI.WebControls.DropDownList ddlPeriodType;
		protected System.Web.UI.WebControls.Label lblError;
		protected System.Web.UI.WebControls.Label lblInfo;
		protected System.Web.UI.WebControls.ImageButton ibGenerate;
		protected System.Web.UI.WebControls.ImageButton ibPdfExport;
		protected System.Web.UI.WebControls.ImageButton ibXlsExport;
		protected Asa.Timesheet.WebPages.UserControls.PageHeader header;
		protected System.Web.UI.WebControls.DataGrid grdReport;
		protected System.Web.UI.WebControls.Label lblReportTitle;
		protected System.Web.UI.WebControls.Label lblCreatedBy;
		protected System.Web.UI.HtmlControls.HtmlTable tblFooter;
		protected System.Web.UI.WebControls.Label lblCreatedByLbl;
		protected System.Web.UI.WebControls.Label Label2;
		protected System.Web.UI.WebControls.Label lbgrdReport;
    protected System.Web.UI.WebControls.DropDownList ddlProjects;
    protected System.Web.UI.WebControls.DataGrid grdProjects;
    protected System.Web.UI.WebControls.Label Label5;
    protected System.Web.UI.WebControls.Label Label7;
    protected System.Web.UI.HtmlControls.HtmlTable tblTotals;
    protected System.Web.UI.WebControls.Label lblTotalASAGiven;
    protected System.Web.UI.WebControls.Label lblTotalASAArchive;
    protected System.Web.UI.WebControls.Label lblTotalSubcGiven;
    protected System.Web.UI.WebControls.Label lblTotalSubcArchive;
    protected System.Web.UI.WebControls.DropDownList ddlProjectStatus;
		protected System.Web.UI.WebControls.Label Label1;
		protected System.Web.UI.WebControls.Label Label4;
		protected System.Web.UI.WebControls.Label lblNoDataFound;

		#endregion

		#region Page load

        private void Page_Load(object sender, System.EventArgs e)
        {
            if (!this.IsPostBack)
            {
                header.PageTitle = Resource.ResourceManager["reports_Folders_Title"];
                header.UserName = this.LoggedUser.UserName;
                UIHelpers.LoadProjectStatusWithoutConcluded(ddlProjectStatus, (int)ProjectsData.ProjectsByStatus.Active);
                LoadProjects("0");
            }

            UIHelpers.CreateMenuReports(menuHolder, LoggedUser);
        }

        private bool LoadProjects(string selectedValue)
        {
            SqlDataReader reader = null;
            try
            {
                reader = FoldersReportData.SelectProjectNames(this.ProjectStatus);

                ddlProjects.DataSource = reader;
                ddlProjects.DataValueField = "ProjectID";
                ddlProjects.DataTextField = "ProjectName";
                ddlProjects.DataBind();

                ddlProjects.Items.Insert(0, new ListItem("<" + Resource.ResourceManager["reports_ddlAllProjects"] + ">", "0"));
                if (ddlProjects.Items.FindByValue(selectedValue) != null)
                    ddlProjects.SelectedValue = selectedValue;
            }
            catch (Exception ex)
            {
                log.Info(ex);
                return false;
            }
            finally
            {
                if (reader != null) reader.Close();
            }
            return true;
        }
		#endregion

		#region Web Form Designer generated code
        override protected void OnInit(EventArgs e)
        {
            //
            // CODEGEN: This call is required by the ASP.NET Web Form Designer.
            //
            InitializeComponent();
            base.OnInit(e);
        }
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
        private void InitializeComponent()
        {
            this.ddlProjectStatus.SelectedIndexChanged += new System.EventHandler(this.ddlProjectStatus_SelectedIndexChanged);
            this.ibGenerate.Click += new System.Web.UI.ImageClickEventHandler(this.ibGenerate_Click);
            this.ibPdfExport.Click += new System.Web.UI.ImageClickEventHandler(this.ibPdfExport_Click);
            this.ibXlsExport.Click += new System.Web.UI.ImageClickEventHandler(this.ibXlsExport_Click);
            this.grdProjects.ItemDataBound += new System.Web.UI.WebControls.DataGridItemEventHandler(this.grdProjects_ItemDataBound);
            this.Load += new System.EventHandler(this.Page_Load);

        }
		#endregion

    #region Event handlers

    #region Command buttons

        private void ibGenerate_Click(object sender, System.Web.UI.ImageClickEventArgs e)
        {
            int nID = int.Parse(ddlProjects.SelectedValue);

            DataSet dsReport = null;
            try
            {
                dsReport = FoldersReportData.SelectFoldersReportData(nID, this.ProjectStatus, LoggedUser.HasPaymentRights);
            }
            catch (Exception ex)
            {
                log.Error(ex.Message);
                return;
            }

            lblCreatedBy.Text = this.LoggedUser.UserName;

            lblReportTitle.Visible = true;

            lblCreatedBy.Visible = true;
            lblCreatedByLbl.Visible = true;


            grdProjects.DataSource = dsReport.Tables[0];
            grdProjects.DataBind();

            //Totals
            int foldersGiven = 0, foldersArchive = 0;

            FoldersReportData.GetTotalASAFolders(dsReport.Tables[0], out foldersGiven, out foldersArchive);

            tblTotals.Visible = true;
            lblTotalASAGiven.Text = foldersGiven.ToString();
            lblTotalASAArchive.Text = foldersArchive.ToString();

            FoldersReportData.GetTotalSubcontractersFolders(dsReport.Tables[1], out foldersGiven, out foldersArchive);

            lblTotalSubcGiven.Text = foldersGiven.ToString();
            lblTotalSubcArchive.Text = foldersArchive.ToString();
        }

        private void ibPdfExport_Click(object sender, System.Web.UI.ImageClickEventArgs e)
        {
            PdfExport pdf = new PdfExport();

            pdf.NeverEmbedFonts = "";

            int projectID = int.Parse(ddlProjects.SelectedValue);

            GrapeCity.ActiveReports.SectionReport report = null;

            try
            {
                report = new Folders(projectID, this.ProjectStatus, this.LoggedUser.UserName, true, LoggedUser.HasPaymentRights);
                report.Run();
            }
            catch (Exception ex)
            {
                log.Error(ex);
                return;
            }


            System.IO.MemoryStream memoryFile = new System.IO.MemoryStream();
            pdf.Export(report.Document, memoryFile);

            Response.Clear();
            Response.AppendHeader("content-disposition"
              , "attachment; filename=" + "Folders.pdf");
            Response.ContentType = "application/pdf";

            memoryFile.WriteTo(Response.OutputStream);
            Response.End();
        }

        private void ibXlsExport_Click(object sender, System.Web.UI.ImageClickEventArgs e)
        {
            XlsExport pdf = new XlsExport();

            int projectID = int.Parse(ddlProjects.SelectedValue);

            GrapeCity.ActiveReports.SectionReport report = null;

            try
            {
                report = new Folders(projectID, this.ProjectStatus, this.LoggedUser.UserName, false, LoggedUser.HasPaymentRights);
                report.Run();
            }
            catch (Exception ex)
            {
                log.Error(ex.Message);
                return;
            }

            System.IO.MemoryStream memoryFile = new System.IO.MemoryStream();
            pdf.Export(report.Document, memoryFile);

            Response.Clear();
            Response.AppendHeader("content-disposition"
              , "attachment; filename=" + "Folders.xls");
            //Response.ContentType = "application/pdf";

            memoryFile.WriteTo(Response.OutputStream);
            Response.End();
        }

    #endregion

    #region Web report grids

    int _projectFoldersGiven, _projectFoldersArchive;

    private void grdProjects_ItemDataBound(object sender, System.Web.UI.WebControls.DataGridItemEventArgs e)
    {
        if ((e.Item.ItemType == ListItemType.Item) || (e.Item.ItemType == ListItemType.AlternatingItem))
        {
            DataRowView drv = (DataRowView)e.Item.DataItem;

            ((Label)e.Item.FindControl("lblProjectName")).Text = drv["ProjectName"].ToString();
            ((Label)e.Item.FindControl("lblAddress")).Text = drv["Address"].ToString();

            _projectFoldersGiven = drv["FoldersGiven"] == System.DBNull.Value ? 0 : int.Parse(drv["FoldersGiven"].ToString());
            _projectFoldersArchive = drv["FoldersArchive"] == System.DBNull.Value ? 0 : int.Parse(drv["FoldersArchive"].ToString());

            ((Label)e.Item.FindControl("lblFoldersGiven")).Text =
              String.Concat(Resource.ResourceManager["Reports_strFoldersGiven"], " - ", _projectFoldersGiven.ToString());
            ((Label)e.Item.FindControl("lblFoldersArchive")).Text =
              String.Concat(Resource.ResourceManager["Reports_strFoldersArchive"], " - ", _projectFoldersArchive.ToString());

            DataGrid grdFolders = (DataGrid)e.Item.FindControl("grdFolders");
            grdFolders.ItemDataBound += new DataGridItemEventHandler(grdFolders_ItemDataBound);

            grdFolders.DataSource = FoldersReportData.GetProjectFoldersView(drv.Row);
            grdFolders.DataBind();
        }
        else
        {
            if (e.Item.ItemType == ListItemType.Header)
            {
                _projectFoldersGiven = 0;
                _projectFoldersArchive = 0;

            }
        }
    }

    int _totalSubcFoldersGiven, _totalSubcFoldersArchive;

    private void grdFolders_ItemDataBound(object sender, DataGridItemEventArgs e)
    {
        if (e.Item.ItemType == ListItemType.Header)
        {

            _totalSubcFoldersGiven = 0; _totalSubcFoldersArchive = 0;

        }
        else
        {
            if (e.Item.ItemType == ListItemType.Footer)
            {
                e.Item.Cells[2].HorizontalAlign = HorizontalAlign.Center;
                e.Item.Cells[2].Text = _totalSubcFoldersGiven.ToString();

                e.Item.Cells[3].HorizontalAlign = HorizontalAlign.Center;
                e.Item.Cells[3].Text = _totalSubcFoldersArchive.ToString();
            }
            else
            {
                DataRowView drv = (DataRowView)e.Item.DataItem;
                _totalSubcFoldersGiven += drv["FoldersGiven"] == DBNull.Value ? 0 : (int)drv["FoldersGiven"];
                _totalSubcFoldersArchive += drv["FoldersArchive"] == DBNull.Value ? 0 : (int)drv["FoldersArchive"];
            }
        }

    }

    #endregion

    #endregion

		#region Menu

//		private void CreateMenu()
//		{
//			UserControls.MenuTable menu = new UserControls.MenuTable();
//			menu.ID = "MenuTable";
//
//			// 'Links' group
//			ArrayList menuItems = new ArrayList();
//
//			menuItems.Add(new MenuItemInfo(Resource.ResourceManager["menuItem_Hours"], "../Hours.aspx", true));
//			menuItems.Add(new MenuItemInfo(Resource.ResourceManager["menuItem_Projects"], "../Projects.aspx"));
//			if (LoggedUser.IsLeader || LoggedUser.IsAssistant) 
//			{
//				menuItems.Add(new MenuItemInfo(Resource.ResourceManager["menuItem_Clients"], "../Clients.aspx"));
//								menuItems.Add(new MenuItemInfo(Resource.ResourceManager["menuItem_Subcontracters"], "../Subcontracters.aspx"));
//				if (LoggedUser.IsLeader)
//				menuItems.Add(new MenuItemInfo(Resource.ResourceManager["menuItem_Users"], "../Users.aspx"));	
//			}
//			menuItems.Add(new MenuItemInfo(Resource.ResourceManager["menuItem_Emails"], "../Emails.aspx"));
//
//			menu.AddMenuGroup(Resource.ResourceManager["menuHeader_Links"], 20, menuItems);
//			
//			// 'New' group
//			menuItems = new ArrayList();
//			menuItems.Add(new MenuItemInfo(Resource.ResourceManager["menuItem_NewProject"], "../EditProject.aspx"));
//			if (LoggedUser.IsLeader || LoggedUser.IsAssistant)
//			{
//				menuItems.Add(new MenuItemInfo(Resource.ResourceManager["menuItem_NewClient"], "../EditClient.aspx"));
//				if (LoggedUser.IsLeader)
//				menuItems.Add(new MenuItemInfo(Resource.ResourceManager["menuItem_NewUser"], "../EditUser.aspx"));
//			}
//			menuItems.Add(new MenuItemInfo(Resource.ResourceManager["menuItem_NewEmail"], "../EditEmail.aspx"));
//			menuItems.Add(new MenuItemInfo(Resource.ResourceManager["newcontracter_PageTitle"], "../EditSubContracter.aspx"));
//
//
//			menu.AddMenuGroup(Resource.ResourceManager["menuHeader_New"], 30, menuItems);
//
//			// "Reports' group
//			menuItems = new ArrayList();
//			//menuItems.Add(new MenuItemInfo(Resource.ResourceManager["menuItem_WorkTimeReport"], "#"));
//			menuItems.Add(new MenuItemInfo(Resource.ResourceManager["menuItem_MinutesReport"], "Minutes.aspx"));
//			if (LoggedUser.IsLeader || LoggedUser.IsAssistant)
//				menuItems.Add(new MenuItemInfo(Resource.ResourceManager["menuItem_MailReport"], "../MailForm.aspx"));
//			
//			if (LoggedUser.IsLeader)
//			{
//				menuItems.Add(new MenuItemInfo(Resource.ResourceManager["menuItem_NotEnteredWorkTime"], "EmptyHours.aspx"));
//				menuItems.Add(new MenuItemInfo(Resource.ResourceManager["menuItem_Calendar"], "Calendar.aspx"));
//			}
//			menu.AddMenuGroup(Resource.ResourceManager["menuHeader_Reports"], 30, menuItems);
//
//			
//
//			menuItems = new ArrayList();
//			menuItems.Add(new MenuItemInfo(Resource.ResourceManager["menuItem_Help"], SessionManager.GetReportsHelpLink(),true,Pages.WorkTimes));
//			menu.AddMenuGroup("", 10, menuItems);
//
//
//			menuHolder.Controls.Add(menu);
//		}

    #endregion

    private void ddlProjectStatus_SelectedIndexChanged(object sender, System.EventArgs e)
    {
        LoadProjects("0");
    }

    #region Properties

    public int ProjectStatus
    {
      get
      {
        try
        {
          return int.Parse(ddlProjectStatus.SelectedValue);
        }
        catch { return 0; }
      }
    }

    #endregion

    
  }
}
