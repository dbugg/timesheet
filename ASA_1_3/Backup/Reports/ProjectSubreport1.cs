using System;
using System.Data;
using Asa.Timesheet.Data;
using GrapeCity.ActiveReports;
using GrapeCity.ActiveReports.Controls;
using GrapeCity.ActiveReports.SectionReportModel;
using GrapeCity.ActiveReports.Document.Section;
using GrapeCity.ActiveReports.Document;

namespace Asa.Timesheet.WebPages.Reports
{
    public class ProjectSubreport1 : GrapeCity.ActiveReports.SectionReport
	{
		decimal _payd = 0;
		decimal _notpayd = 0;
		bool _isPUP = false;
        public ProjectSubreport1(DataView dv, bool isPUP)
        {
            _isPUP = isPUP;

            InitializeComponent();
            this.DataSource = dv;


        }

        private void GroupHeader1_Format(object sender, System.EventArgs eArgs)
        {
            if (_isPUP)
            {
                this.Label13.Text = Resource.ResourceManager["ProjectAnalysisgrdReportClientsHeaderText"];
            }
        }

        private void GroupFooter1_Format(object sender, System.EventArgs eArgs)
        {
            decimal fix = decimal.Parse(System.Configuration.ConfigurationManager.AppSettings["fixedBGNEUR"]);
            this.txtPaydEUR.Text = _payd.ToString("#,##0");
            decimal bgn = _payd * fix;
            this.txtPaydBGN.Text = bgn.ToString("#,##0");
            this.TxtNotPaydEUR.Text = _notpayd.ToString("#,##0");
            bgn = _notpayd * fix;
            this.txtNotPaydBGN.Text = bgn.ToString("#,##0");

        }

        private void Detail_Format(object sender, System.EventArgs eArgs)
        {
            decimal eur = UIHelpers.ParseDecimal(this.TextBox3.Text);
            if (this.TextBox5.Text == Resource.ResourceManager["Reports_Yes"])
                _payd += eur;
            else
                _notpayd += eur;
        }

		#region ActiveReports Designer generated code

































        public void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ProjectSubreport1));
            this.Detail = new GrapeCity.ActiveReports.SectionReportModel.Detail();
            this.ReportHeader = new GrapeCity.ActiveReports.SectionReportModel.ReportHeader();
            this.ReportFooter = new GrapeCity.ActiveReports.SectionReportModel.ReportFooter();
            this.PageHeader = new GrapeCity.ActiveReports.SectionReportModel.PageHeader();
            this.PageFooter = new GrapeCity.ActiveReports.SectionReportModel.PageFooter();
            this.GroupHeader1 = new GrapeCity.ActiveReports.SectionReportModel.GroupHeader();
            this.GroupFooter1 = new GrapeCity.ActiveReports.SectionReportModel.GroupFooter();
            this.Label12 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label13 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label14 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label15 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label16 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label17 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label18 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.TextBox1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.TextBox2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.TextBox3 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.TextBox4 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.TextBox5 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.TextBox9 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.TextBox10 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.Line4 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.TextBox6 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.Label8 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Line2 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.TextBox11 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtPaydEUR = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.Label19 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.txtPaydBGN = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.TxtNotPaydEUR = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.Label20 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.txtNotPaydBGN = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.TextBox8 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            ((System.ComponentModel.ISupportInitialize)(this.Label12)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label13)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label14)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label15)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label16)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label17)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label18)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TextBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TextBox2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TextBox3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TextBox4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TextBox5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TextBox9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TextBox10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TextBox6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TextBox11)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPaydEUR)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label19)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPaydBGN)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtNotPaydEUR)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label20)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtNotPaydBGN)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TextBox8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            // 
            // Detail
            // 
            this.Detail.ColumnSpacing = 0F;
            this.Detail.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
						this.TextBox1,
						this.TextBox2,
						this.TextBox3,
						this.TextBox4,
						this.TextBox5,
						this.TextBox9,
						this.TextBox10,
						this.Line4});
            this.Detail.Height = 0.21875F;
            this.Detail.Name = "Detail";
            // 
            // ReportHeader
            // 
            this.ReportHeader.Height = 0F;
            this.ReportHeader.Name = "ReportHeader";
            // 
            // ReportFooter
            // 
            this.ReportFooter.Height = 0F;
            this.ReportFooter.Name = "ReportFooter";
            // 
            // PageHeader
            // 
            this.PageHeader.Height = 0F;
            this.PageHeader.Name = "PageHeader";
            // 
            // PageFooter
            // 
            this.PageFooter.Height = 0F;
            this.PageFooter.Name = "PageFooter";
            // 
            // GroupHeader1
            // 
            this.GroupHeader1.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
						this.Label12,
						this.Label13,
						this.Label14,
						this.Label15,
						this.Label16,
						this.Label17,
						this.Label18});
            this.GroupHeader1.DataField = "ProjectID";
            this.GroupHeader1.Height = 0.28125F;
            this.GroupHeader1.Name = "GroupHeader1";
            // 
            // GroupFooter1
            // 
            this.GroupFooter1.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
						this.TextBox6,
						this.Label8,
						this.Line2,
						this.TextBox11,
						this.txtPaydEUR,
						this.Label19,
						this.txtPaydBGN,
						this.TxtNotPaydEUR,
						this.Label20,
						this.txtNotPaydBGN,
						this.TextBox8});
            this.GroupFooter1.Height = 0.65625F;
            this.GroupFooter1.Name = "GroupFooter1";
            // 
            // Label12
            // 
            this.Label12.Height = 0.2F;
            this.Label12.HyperLink = null;
            this.Label12.Left = 0F;
            this.Label12.Name = "Label12";
            this.Label12.Style = "font-weight: bold";
            this.Label12.Text = "??????";
            this.Label12.Top = 0.0625F;
            this.Label12.Width = 2.18F;
            // 
            // Label13
            // 
            this.Label13.Height = 0.2F;
            this.Label13.HyperLink = null;
            this.Label13.Left = 2.1875F;
            this.Label13.Name = "Label13";
            this.Label13.Style = "font-weight: bold";
            this.Label13.Text = "????";
            this.Label13.Top = 0.0625F;
            this.Label13.Width = 1.875F;
            // 
            // Label14
            // 
            this.Label14.Height = 0.2F;
            this.Label14.HyperLink = null;
            this.Label14.Left = 4.069445F;
            this.Label14.Name = "Label14";
            this.Label14.Style = "font-weight: bold; text-align: right";
            this.Label14.Text = "???? ???? (EUR)";
            this.Label14.Top = 0.0625F;
            this.Label14.Width = 1.243056F;
            // 
            // Label15
            // 
            this.Label15.Height = 0.2F;
            this.Label15.HyperLink = null;
            this.Label15.Left = 6.5F;
            this.Label15.Name = "Label15";
            this.Label15.Style = "font-weight: bold; text-align: right";
            this.Label15.Text = "???? ???????";
            this.Label15.Top = 0.0625F;
            this.Label15.Width = 1.0625F;
            // 
            // Label16
            // 
            this.Label16.Height = 0.2F;
            this.Label16.HyperLink = null;
            this.Label16.Left = 8.313F;
            this.Label16.Name = "Label16";
            this.Label16.Style = "font-weight: bold; text-align: right";
            this.Label16.Text = "???????";
            this.Label16.Top = 0.0625F;
            this.Label16.Width = 0.813F;
            // 
            // Label17
            // 
            this.Label17.Height = 0.2F;
            this.Label17.HyperLink = null;
            this.Label17.Left = 7.5625F;
            this.Label17.Name = "Label17";
            this.Label17.Style = "font-weight: bold; text-align: right";
            this.Label17.Text = "%";
            this.Label17.Top = 0.0625F;
            this.Label17.Width = 0.75F;
            // 
            // Label18
            // 
            this.Label18.Height = 0.2F;
            this.Label18.HyperLink = null;
            this.Label18.Left = 5.3125F;
            this.Label18.Name = "Label18";
            this.Label18.Style = "font-weight: bold; text-align: right";
            this.Label18.Text = "???? ???? (??.)";
            this.Label18.Top = 0.0625F;
            this.Label18.Width = 1.1875F;
            // 
            // TextBox1
            // 
            this.TextBox1.DataField = "projectname";
            this.TextBox1.Height = 0.2F;
            this.TextBox1.Left = 0F;
            this.TextBox1.Name = "TextBox1";
            this.TextBox1.Text = "TextBox1";
            this.TextBox1.Top = 0F;
            this.TextBox1.Width = 2.1875F;
            // 
            // TextBox2
            // 
            this.TextBox2.DataField = "SubprojectType";
            this.TextBox2.Height = 0.2F;
            this.TextBox2.Left = 2.1875F;
            this.TextBox2.Name = "TextBox2";
            this.TextBox2.Text = "TextBox2";
            this.TextBox2.Top = 0F;
            this.TextBox2.Width = 1.875F;
            // 
            // TextBox3
            // 
            this.TextBox3.DataField = "Amount";
            this.TextBox3.Height = 0.2F;
            this.TextBox3.Left = 4.069F;
            this.TextBox3.Name = "TextBox3";
            this.TextBox3.OutputFormat = resources.GetString("TextBox3.OutputFormat");
            this.TextBox3.Style = "text-align: right";
            this.TextBox3.Text = "TextBox3";
            this.TextBox3.Top = 0F;
            this.TextBox3.Width = 1.243F;
            // 
            // TextBox4
            // 
            this.TextBox4.DataField = "PaymentDate";
            this.TextBox4.Height = 0.2F;
            this.TextBox4.Left = 6.5F;
            this.TextBox4.Name = "TextBox4";
            this.TextBox4.OutputFormat = resources.GetString("TextBox4.OutputFormat");
            this.TextBox4.Style = "text-align: right";
            this.TextBox4.Text = "TextBox4";
            this.TextBox4.Top = 0F;
            this.TextBox4.Width = 1.0625F;
            // 
            // TextBox5
            // 
            this.TextBox5.DataField = "DoneString";
            this.TextBox5.Height = 0.2F;
            this.TextBox5.Left = 8.31F;
            this.TextBox5.Name = "TextBox5";
            this.TextBox5.Style = "text-align: right; ddo-char-set: 204";
            this.TextBox5.Text = "TextBox5";
            this.TextBox5.Top = 0F;
            this.TextBox5.Width = 0.8125F;
            // 
            // TextBox9
            // 
            this.TextBox9.DataField = "PaymentPercent";
            this.TextBox9.Height = 0.2F;
            this.TextBox9.Left = 7.5625F;
            this.TextBox9.Name = "TextBox9";
            this.TextBox9.OutputFormat = resources.GetString("TextBox9.OutputFormat");
            this.TextBox9.Style = "text-align: right";
            this.TextBox9.Text = "TextBox9";
            this.TextBox9.Top = 0F;
            this.TextBox9.Width = 0.75F;
            // 
            // TextBox10
            // 
            this.TextBox10.DataField = "AmountBGN";
            this.TextBox10.Height = 0.2F;
            this.TextBox10.Left = 5.313F;
            this.TextBox10.Name = "TextBox10";
            this.TextBox10.OutputFormat = resources.GetString("TextBox10.OutputFormat");
            this.TextBox10.Style = "text-align: right";
            this.TextBox10.Text = "TextBox10";
            this.TextBox10.Top = 0F;
            this.TextBox10.Width = 1.188F;
            // 
            // Line4
            // 
            this.Line4.Height = 0F;
            this.Line4.Left = 0.006944418F;
            this.Line4.LineWeight = 1F;
            this.Line4.Name = "Line4";
            this.Line4.Top = 0F;
            this.Line4.Width = 9.1875F;
            this.Line4.X1 = 0.006944418F;
            this.Line4.X2 = 9.194445F;
            this.Line4.Y1 = 0F;
            this.Line4.Y2 = 0F;
            // 
            // TextBox6
            // 
            this.TextBox6.DataField = "Amount";
            this.TextBox6.Height = 0.2F;
            this.TextBox6.Left = 4.069F;
            this.TextBox6.Name = "TextBox6";
            this.TextBox6.OutputFormat = resources.GetString("TextBox6.OutputFormat");
            this.TextBox6.Style = "text-align: right";
            this.TextBox6.SummaryGroup = "GroupHeader1";
            this.TextBox6.SummaryRunning = GrapeCity.ActiveReports.SectionReportModel.SummaryRunning.Group;
            this.TextBox6.SummaryType = GrapeCity.ActiveReports.SectionReportModel.SummaryType.SubTotal;
            this.TextBox6.Top = 0.4375F;
            this.TextBox6.Width = 1.243F;
            // 
            // Label8
            // 
            this.Label8.Height = 0.2F;
            this.Label8.HyperLink = null;
            this.Label8.Left = 0.006944418F;
            this.Label8.Name = "Label8";
            this.Label8.Style = "";
            this.Label8.Text = "????";
            this.Label8.Top = 0.4444444F;
            this.Label8.Width = 4.055555F;
            // 
            // Line2
            // 
            this.Line2.Height = 0F;
            this.Line2.Left = 0F;
            this.Line2.LineWeight = 1F;
            this.Line2.Name = "Line2";
            this.Line2.Top = 0.0625F;
            this.Line2.Width = 9.1875F;
            this.Line2.X1 = 0F;
            this.Line2.X2 = 9.1875F;
            this.Line2.Y1 = 0.0625F;
            this.Line2.Y2 = 0.0625F;
            // 
            // TextBox11
            // 
            this.TextBox11.DataField = "AmountBGN";
            this.TextBox11.Height = 0.2F;
            this.TextBox11.Left = 5.3125F;
            this.TextBox11.Name = "TextBox11";
            this.TextBox11.OutputFormat = resources.GetString("TextBox11.OutputFormat");
            this.TextBox11.Style = "text-align: right";
            this.TextBox11.SummaryGroup = "GroupHeader1";
            this.TextBox11.SummaryRunning = GrapeCity.ActiveReports.SectionReportModel.SummaryRunning.Group;
            this.TextBox11.SummaryType = GrapeCity.ActiveReports.SectionReportModel.SummaryType.SubTotal;
            this.TextBox11.Top = 0.4375F;
            this.TextBox11.Width = 1.1875F;
            // 
            // txtPaydEUR
            // 
            this.txtPaydEUR.DataField = "Amount";
            this.txtPaydEUR.Height = 0.2F;
            this.txtPaydEUR.Left = 4.069F;
            this.txtPaydEUR.Name = "txtPaydEUR";
            this.txtPaydEUR.OutputFormat = resources.GetString("txtPaydEUR.OutputFormat");
            this.txtPaydEUR.Style = "text-align: right";
            this.txtPaydEUR.SummaryGroup = "GroupHeader1";
            this.txtPaydEUR.SummaryRunning = GrapeCity.ActiveReports.SectionReportModel.SummaryRunning.Group;
            this.txtPaydEUR.SummaryType = GrapeCity.ActiveReports.SectionReportModel.SummaryType.SubTotal;
            this.txtPaydEUR.Top = 0.0625F;
            this.txtPaydEUR.Width = 1.243F;
            // 
            // Label19
            // 
            this.Label19.Height = 0.2F;
            this.Label19.HyperLink = null;
            this.Label19.Left = 0F;
            this.Label19.Name = "Label19";
            this.Label19.Style = "";
            this.Label19.Text = "???? ???????";
            this.Label19.Top = 0.06944442F;
            this.Label19.Width = 4.0625F;
            // 
            // txtPaydBGN
            // 
            this.txtPaydBGN.DataField = "AmountBGN";
            this.txtPaydBGN.Height = 0.2F;
            this.txtPaydBGN.Left = 5.3125F;
            this.txtPaydBGN.Name = "txtPaydBGN";
            this.txtPaydBGN.OutputFormat = resources.GetString("txtPaydBGN.OutputFormat");
            this.txtPaydBGN.Style = "text-align: right";
            this.txtPaydBGN.SummaryGroup = "GroupHeader1";
            this.txtPaydBGN.SummaryRunning = GrapeCity.ActiveReports.SectionReportModel.SummaryRunning.Group;
            this.txtPaydBGN.SummaryType = GrapeCity.ActiveReports.SectionReportModel.SummaryType.SubTotal;
            this.txtPaydBGN.Top = 0.0625F;
            this.txtPaydBGN.Width = 1.1875F;
            // 
            // TxtNotPaydEUR
            // 
            this.TxtNotPaydEUR.DataField = "Amount";
            this.TxtNotPaydEUR.Height = 0.2F;
            this.TxtNotPaydEUR.Left = 4.069F;
            this.TxtNotPaydEUR.Name = "TxtNotPaydEUR";
            this.TxtNotPaydEUR.OutputFormat = resources.GetString("TxtNotPaydEUR.OutputFormat");
            this.TxtNotPaydEUR.Style = "text-align: right";
            this.TxtNotPaydEUR.SummaryGroup = "GroupHeader1";
            this.TxtNotPaydEUR.SummaryRunning = GrapeCity.ActiveReports.SectionReportModel.SummaryRunning.Group;
            this.TxtNotPaydEUR.SummaryType = GrapeCity.ActiveReports.SectionReportModel.SummaryType.SubTotal;
            this.TxtNotPaydEUR.Top = 0.25F;
            this.TxtNotPaydEUR.Width = 1.243F;
            // 
            // Label20
            // 
            this.Label20.Height = 0.2F;
            this.Label20.HyperLink = null;
            this.Label20.Left = 0F;
            this.Label20.Name = "Label20";
            this.Label20.Style = "";
            this.Label20.Text = "???? ?????????";
            this.Label20.Top = 0.2569444F;
            this.Label20.Width = 4.0625F;
            // 
            // txtNotPaydBGN
            // 
            this.txtNotPaydBGN.DataField = "AmountBGN";
            this.txtNotPaydBGN.Height = 0.2F;
            this.txtNotPaydBGN.Left = 5.313F;
            this.txtNotPaydBGN.Name = "txtNotPaydBGN";
            this.txtNotPaydBGN.OutputFormat = resources.GetString("txtNotPaydBGN.OutputFormat");
            this.txtNotPaydBGN.Style = "text-align: right";
            this.txtNotPaydBGN.SummaryGroup = "GroupHeader1";
            this.txtNotPaydBGN.SummaryRunning = GrapeCity.ActiveReports.SectionReportModel.SummaryRunning.Group;
            this.txtNotPaydBGN.SummaryType = GrapeCity.ActiveReports.SectionReportModel.SummaryType.SubTotal;
            this.txtNotPaydBGN.Top = 0.25F;
            this.txtNotPaydBGN.Width = 1.1875F;
            // 
            // TextBox8
            // 
            this.TextBox8.DataField = "projectname";
            this.TextBox8.Height = 0.2F;
            this.TextBox8.Left = 0.6319444F;
            this.TextBox8.Name = "TextBox8";
            this.TextBox8.Top = 0.4444444F;
            this.TextBox8.Visible = false;
            this.TextBox8.Width = 3.430556F;
            // 
            // ActiveReport1
            // 
            this.MasterReport = false;
            this.PageSettings.PaperHeight = 11F;
            this.PageSettings.PaperWidth = 8.5F;
            this.PrintWidth = 9.15F;
            this.Sections.Add(this.ReportHeader);
            this.Sections.Add(this.PageHeader);
            this.Sections.Add(this.GroupHeader1);
            this.Sections.Add(this.Detail);
            this.Sections.Add(this.GroupFooter1);
            this.Sections.Add(this.PageFooter);
            this.Sections.Add(this.ReportFooter);
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule(resources.GetString("$this.StyleSheet"), "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: inherit; font-style: inherit; font-variant: inherit; font-weight: bo" +
                        "ld; font-size: 16pt; font-size-adjust: inherit; font-stretch: inherit", "Heading1", "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: Times New Roman; font-style: italic; font-variant: inherit; font-wei" +
                        "ght: bold; font-size: 14pt; font-size-adjust: inherit; font-stretch: inherit", "Heading2", "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: inherit; font-style: inherit; font-variant: inherit; font-weight: bo" +
                        "ld; font-size: 13pt; font-size-adjust: inherit; font-stretch: inherit", "Heading3", "Normal"));
            ((System.ComponentModel.ISupportInitialize)(this.Label12)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label13)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label14)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label15)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label16)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label17)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label18)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TextBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TextBox2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TextBox3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TextBox4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TextBox5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TextBox9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TextBox10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TextBox6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TextBox11)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPaydEUR)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label19)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPaydBGN)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtNotPaydEUR)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label20)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtNotPaydBGN)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TextBox8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();

            // Attach Report Events
            this.GroupHeader1.Format += new System.EventHandler(this.GroupHeader1_Format);
            this.GroupFooter1.Format += new System.EventHandler(this.GroupFooter1_Format);
            this.Detail.Format += new System.EventHandler(this.Detail_Format);
        }

		#endregion

        private ReportHeader ReportHeader;
        private PageHeader PageHeader;
        private GroupHeader GroupHeader1;
        private Label Label12;
        private Label Label13;
        private Label Label14;
        private Label Label15;
        private Label Label16;
        private Label Label17;
        private Label Label18;
        private Detail Detail;
        private TextBox TextBox1;
        private TextBox TextBox2;
        private TextBox TextBox3;
        private TextBox TextBox4;
        private TextBox TextBox5;
        private TextBox TextBox9;
        private TextBox TextBox10;
        private Line Line4;
        private GroupFooter GroupFooter1;
        private TextBox TextBox6;
        private Label Label8;
        private Line Line2;
        private TextBox TextBox11;
        private TextBox txtPaydEUR;
        private Label Label19;
        private TextBox txtPaydBGN;
        private TextBox TxtNotPaydEUR;
        private Label Label20;
        private TextBox txtNotPaydBGN;
        private TextBox TextBox8;
        private PageFooter PageFooter;
        private ReportFooter ReportFooter;

		
	}
}
