using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Data.SqlClient;
using log4net;
using Asa.Timesheet.Data;
using Asa.Timesheet.Data.Entities;
using Asa.Timesheet.Data.Util;
using DataDynamics.ActiveReports.Export.Pdf;
using DataDynamics.ActiveReports.Export.Xls;
using Asa.Timesheet.WebPages.Reports;
namespace Asa.Timesheet.WebPages
{
	/// <summary>
	/// Summary description for EditSubproject.
	/// </summary>
	public class EditSubproject : TimesheetPageBase
	{
		protected System.Web.UI.WebControls.PlaceHolder menuHolder;
		protected System.Web.UI.WebControls.Label lblName;
		protected System.Web.UI.WebControls.Label lblProjectCode;
		protected System.Web.UI.WebControls.Label lblAdministrativeName;
		protected System.Web.UI.WebControls.Label lblStartDate;
		protected System.Web.UI.WebControls.Label lblArea;
		protected System.Web.UI.WebControls.TextBox txtArea;
		protected System.Web.UI.WebControls.Label lblAddress;
		protected System.Web.UI.WebControls.Label lblClient;
		protected System.Web.UI.WebControls.Label lblManager;
		protected System.Web.UI.WebControls.Button btnSave;
		protected System.Web.UI.WebControls.Button btnCancel;
		protected System.Web.UI.WebControls.Button btnDelete;
		protected System.Web.UI.WebControls.Label lblInfo;
		protected System.Web.UI.WebControls.Label lbProject;
		protected Telerik.WebControls.RadEditor ftb;
		protected System.Web.UI.WebControls.DropDownList ddlTypes;
		protected System.Web.UI.WebControls.Label Label1;
		protected System.Web.UI.WebControls.DropDownList ddlScheme;
		protected System.Web.UI.WebControls.Label Label2;
		protected System.Web.UI.WebControls.DataList dlPayments;
		protected System.Web.UI.WebControls.Label Label3;
		protected System.Web.UI.WebControls.Label lbBGN;
		protected System.Web.UI.WebControls.Button btnNewPayment;
		protected System.Web.UI.WebControls.Label lblError;
		
		protected System.Web.UI.WebControls.TextBox txtStartDate;
		protected System.Web.UI.WebControls.TextBox txtEndDate;
		protected System.Web.UI.HtmlControls.HtmlImage lkCalendar1;
		protected System.Web.UI.HtmlControls.HtmlImage lkCalendar2;
		protected System.Web.UI.WebControls.Label lbEUR;
		protected System.Web.UI.WebControls.Label Label4;
		protected System.Web.UI.WebControls.Label Label5;
		protected System.Web.UI.WebControls.Label Label6;
		protected System.Web.UI.WebControls.Label Label7;
		protected System.Web.UI.WebControls.TextBox txtRate;
		protected System.Web.UI.HtmlControls.HtmlTable tbl;
		protected System.Web.UI.WebControls.Button btn;
		protected Asa.Timesheet.WebPages.UserControls.PageHeader header;
		protected System.Web.UI.HtmlControls.HtmlInputHidden hdnInfo;
		protected System.Web.UI.HtmlControls.HtmlInputHidden hdnClientID;
		protected System.Web.UI.WebControls.TextBox txtNotes;
		protected System.Web.UI.WebControls.Button btnNote;
		protected System.Web.UI.HtmlControls.HtmlTable tblPayments;
		protected System.Web.UI.WebControls.CheckBox ckPhases;
    protected System.Web.UI.WebControls.Button btnEdit;
    protected Asa.Timesheet.WebPages.UserControls.EditForm editCtrl;
		private static readonly ILog log = LogManager.GetLogger(typeof(EditSubproject));
		private void Page_Load(object sender, System.EventArgs e)
		{
			if(Page.IsPostBack == false)
			{
				// Set up form for data change checking when
				// first loaded.
				this.CheckForDataChanges= true;
				this.BypassPromptIds =
					new string[] { "btnSave",  "btnEdit", "btnDelete","btnNote","ddlPhase","btnNewPayment","btnNew","btn"};
			}
			int PID = UIHelpers.GetPIDParam();
			string projectName, projectCode, administrativeName,add;
			decimal area=0;int clientID=0;
			bool phases;
			if (!ProjectsData.SelectProject(PID, out projectName, out  projectCode, out administrativeName, out area, out add, 
				out clientID,out phases))
			{
				log.Error("Project not found " + PID);
				
			}
			else
			{
				lbProject.Text=projectCode+ " "+administrativeName;
				hdnInfo.Value=add;
				hdnClientID.Value=clientID.ToString();
			}
			int nSID=UIHelpers.GetSIDParam();
			tblPayments.Visible=phases;
			ckPhases.Checked=phases;
			if(!LoggedUser.HasPaymentRights)
			{
				tblPayments.Visible=false;
				tbl.Visible=false;
			}
			if (!LoggedUser.HasRole) ErrorRedirect(Resource.ResourceManager["noRightsAlert"]);
			lkCalendar1.Attributes["onclick"] = TimeHelper.InvokePopupCal(txtStartDate);
			lkCalendar2.Attributes["onclick"] = TimeHelper.InvokePopupCal(txtEndDate);
			
			UIHelpers.CreateMenu(menuHolder,LoggedUser);
			if(!IsPostBack)
			{
				ddlScheme.DataSource=SubprojectsUDL.SelectSchemes();
				ddlScheme.DataValueField="PaymentSchemeID";
				ddlScheme.DataTextField="PaymentScheme";
				ddlScheme.DataBind();

				ddlTypes.DataSource=SubprojectsUDL.SelectSubprojectTypes();
				ddlTypes.DataValueField="SubprojectTypeID";
				ddlTypes.DataTextField="SubprojectType";
				ddlTypes.DataBind();
				
				header.PageTitle = Resource.ResourceManager["subprojects_PageTitle"]+" "+projectName;
				header.UserName = LoggedUser.UserName;
				btnNewPayment.Text = Resource.ResourceManager["editsubprojects_btnNewPayment"];
				if(nSID<=0 && area>0)
					txtArea.Text=area.ToString();

			
				
				if ((!this.LoggedUser.HasPaymentRights) || nSID<=0 ) btnDelete.Visible = false;
				else SetConfirmDelete(btnDelete, Resource.ResourceManager["editsubprojects_phase"]);
			
			
				if(nSID<=0)
				{
					
					SessionTable =UIHelpers.GetNewPaymentsTable(0);
					dlPayments.DataSource=SessionTable;
					dlPayments.DataBind();
				}
				else
				{
					SetFromID(nSID,phases);
					
				}
			}

      InitEdit();
			
		}
		private void SetFromID(int nSID, bool phases)
		{
			SubprojectData sd = SubprojectDAL.Load(nSID);
			if(sd!=null)
			{
				txtArea.Text=UIHelpers.FormatDecimal2(sd.Area);
				txtEndDate.Text=TimeHelper.FormatDate(sd.EndDate);
				txtStartDate.Text=TimeHelper.FormatDate(sd.StartDate);
				//ftb.Html=sd.Notes;
				txtNotes.Text=sd.Notes;
				ddlScheme.SelectedValue=sd.PaymentSchemeID.ToString();
				txtRate.Text=UIHelpers.FormatDecimal2( sd.Rate);
				ddlTypes.SelectedValue=sd.SubprojectTypeID.ToString();
				
				if(phases)
				{
					PaymentsVector pv = PaymentDAL.LoadCollection("PaymentsListBySubprojectProc", SQLParms.CreatePaymentsListBySubprojectProc(nSID));
					SessionTable=pv;
					dlPayments.DataSource=pv;
					dlPayments.DataBind();
				}
				Recalculate();
			}
		}
		//private DataTable Get
		#region Menu
//		private void CreateMenu()
//		{
//			UserControls.MenuTable menu = new UserControls.MenuTable();
//			menu.ID = "MenuTable";
//
//			ArrayList menuItems = new ArrayList();
//			menuItems.Add(new MenuItemInfo(Resource.ResourceManager["menuItem_Hours"], "Hours.aspx", true));
//			menuItems.Add(new MenuItemInfo(Resource.ResourceManager["menuItem_Projects"], "Projects.aspx"));
//			if ((LoggedUser.IsLeader) || (LoggedUser.IsAssistant)) 
//			{
//				menuItems.Add(new MenuItemInfo(Resource.ResourceManager["menuItem_Clients"], "Clients.aspx"));
//				menuItems.Add(new MenuItemInfo(Resource.ResourceManager["menuItem_Subcontracters"], "Subcontracters.aspx"));
//				if (LoggedUser.IsLeader)
//				menuItems.Add(new MenuItemInfo(Resource.ResourceManager["menuItem_Users"], "Users.aspx"));			
//			}
//			menuItems.Add(new MenuItemInfo(Resource.ResourceManager["menuItem_Emails"], "Emails.aspx"));
//			menu.AddMenuGroup(Resource.ResourceManager["menuHeader_Links"], 20, menuItems);
//			
//			//2 group
//			menuItems = new ArrayList();
//			menuItems.Add(new MenuItemInfo(Resource.ResourceManager["menuItem_NewProject"], "EditProject.aspx"));
//			if ((LoggedUser.IsLeader) || (LoggedUser.IsAssistant)) 
//			{
//				menuItems.Add(new MenuItemInfo(Resource.ResourceManager["menuItem_NewClient"], "EditClient.aspx"));
//				if (LoggedUser.IsLeader)
//				menuItems.Add(new MenuItemInfo(Resource.ResourceManager["menuItem_NewUser"], "EditUser.aspx"));		
//			}
//			menuItems.Add(new MenuItemInfo(Resource.ResourceManager["menuItem_NewEmail"], "EditEmail.aspx"));
//			menuItems.Add(new MenuItemInfo(Resource.ResourceManager["newcontracter_PageTitle"], "EditSubContracter.aspx"));
//
//			menu.AddMenuGroup(Resource.ResourceManager["menuHeader_New"], 30, menuItems);
//
//			menuItems = new ArrayList();
//			menuItems.Add(new MenuItemInfo(Resource.ResourceManager["menuItem_WorkTimeReport"], "reports/worktimes.aspx"));
//			menuItems.Add(new MenuItemInfo(Resource.ResourceManager["menuItem_MinutesReport"], "Reports/Minutes.aspx"));
//			if (LoggedUser.IsLeader || LoggedUser.IsAssistant)
//				menuItems.Add(new MenuItemInfo(Resource.ResourceManager["menuItem_MailReport"], "MailForm.aspx"));
//			if (LoggedUser.IsLeader)
//			{
//				menuItems.Add(new MenuItemInfo(Resource.ResourceManager["menuItem_NotEnteredWorkTime"], "reports/EmptyHours.aspx"));
//				menuItems.Add(new MenuItemInfo(Resource.ResourceManager["menuItem_Calendar"], "reports/Calendar.aspx"));
//			}
//
//			menu.AddMenuGroup(Resource.ResourceManager["menuHeader_Reports"], 30, menuItems);
//			
//			menuItems = new ArrayList();
//			menuItems.Add(new MenuItemInfo(Resource.ResourceManager["menuItem_Help"], SessionManager.GetHelpLink(),true,Pages.Subprojects));
//			menu.AddMenuGroup("", 10, menuItems);
//
//			menuHolder.Controls.Add(menu);
//		}
		#endregion
		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
      this.btn.Click += new System.EventHandler(this.btn_Click);
      this.dlPayments.ItemDataBound += new System.Web.UI.WebControls.DataListItemEventHandler(this.dlPayments_ItemDataBound);
      this.btnNewPayment.Click += new System.EventHandler(this.btnNewPayment_Click);
      this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
      this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
      this.btnNote.Click += new System.EventHandler(this.btnNote_Click);
      this.btnDelete.Click += new System.EventHandler(this.btnDelete_Click);
      this.Load += new System.EventHandler(this.Page_Load);

    }
		#endregion

		private void btnSave_Click(object sender, System.EventArgs e)
		{
//			if(txtNotes.Text.Length>=Constants.MaxNotes)
//			{
//				lblError.Text=Resource.ResourceManager["errorTextTooLong"];
//				return;
//			}
			SetRec();
			int TypeID=int.Parse(ddlTypes.SelectedValue);
			int ID = UIHelpers.GetSIDParam();
			int PID = UIHelpers.GetPIDParam();
//			if(TypeID<=Constants.NumFixedPhases)
//			{
//				SubprojectsVector sv = SubprojectDAL.LoadCollection("SubprojectsListProc",SQLParms.CreateSubprojectsListProc(PID));
//				SubprojectData sdd = sv.GetByTypeID(TypeID);
//				if(sdd!=null && ID<=0)
//				{
//					lblError.Text=Resource.ResourceManager["errorPhaseExists"];
//					Dirty=true;
//					return;
//				}
//				if(sdd!=null && sdd.SubprojectID!=ID)
//				{
//					lblError.Text=Resource.ResourceManager["errorPhaseExists"];
//					Dirty=true;
//					return;
//				}
//			}
			
			SubprojectData sd = new SubprojectData();
			sd.Area= UIHelpers.ParseDecimal(txtArea.Text);
			sd.EndDate= TimeHelper.GetDate(txtEndDate.Text);
			sd.StartDate=TimeHelper.GetDate(txtStartDate.Text);
			sd.Notes= txtNotes.Text;//ftb.Html;
			sd.PaymentSchemeID= int.Parse(ddlScheme.SelectedValue);
			sd.ProjectID= PID;
			if(txtRate.Text!="")
				sd.Rate=UIHelpers.ParseDecimal(txtRate.Text);
			sd.SubprojectID=ID;
			sd.SubprojectTypeID=int.Parse(ddlTypes.SelectedValue);
			PaymentSchemes ps =(PaymentSchemes)sd.PaymentSchemeID;
			if(ps==PaymentSchemes.Fixed)
				sd.TotalAmount=sd.Rate;
			else if(ps==PaymentSchemes.Area)
				sd.TotalAmount=sd.Rate*(decimal)sd.Area;
			decimal totall=0;
			decimal dEUR=0;
			if(ckPhases.Checked && LoggedUser.HasPaymentRights)
			{
				for(int i=0;i<dlPayments.Items.Count;i++)
				{
					decimal per=0;
					decimal am=0;

					TextBox txtPer=(TextBox) dlPayments.Items[i].FindControl("txtPer");
					if(txtPer!=null && txtPer.Text!="")
					{
						per = UIHelpers.ParseDecimal(txtPer.Text);
						totall+=per;
					}
					TextBox txtAmount=(TextBox) dlPayments.Items[i].FindControl("txtAmount");
					if(txtAmount!=null && txtAmount.Text!="")
					{
						am = UIHelpers.ParseDecimal(txtAmount.Text);
						dEUR+=am;
				
					}
				
				}
				if(dEUR!=sd.TotalAmount )
				{
					lblError.Text=Resource.ResourceManager["editsubprojects_errorTotal"];
					Dirty=true;
					return;
				}
			}
			Dirty=false;
			ID=SubprojectDAL.Save(sd);
			PaymentsVector pv = new PaymentsVector();
			if(ckPhases.Checked &&  LoggedUser.HasPaymentRights)
			{
				
				for(int i=0;i<dlPayments.Items.Count;i++)
				{
					decimal per=0;
					decimal am=0;

					TextBox txtPer=(TextBox) dlPayments.Items[i].FindControl("txtPer");
					if(txtPer!=null && txtPer.Text!="")
					{
						per =UIHelpers.ParseDecimal(txtPer.Text);
						//totall+=per;
					}
					TextBox txtAmount=(TextBox) dlPayments.Items[i].FindControl("txtAmount");
					if(txtAmount!=null && txtAmount.Text!="")
					{
						am = UIHelpers.ParseDecimal(txtAmount.Text);
						//dEUR+=am;

						if(am>0 && dEUR>0)
							per=am*100/dEUR;
					}
					CheckBox ckPaid=(CheckBox) dlPayments.Items[i].FindControl("ckPaid");
					TextBox txt=(TextBox) dlPayments.Items[i].FindControl("txtDate");
					TextBox txtNotes1=(TextBox) dlPayments.Items[i].FindControl("txtNotes1");
					if(per>0)
					{
						PaymentData pd= new PaymentData(-1, ID,-1,per,am,ckPaid.Checked,TimeHelper.GetDate(txt.Text),am,false);
						pd.IsSubproject=true;
						pd.Notes=txtNotes1.Text;
						pv.Add(pd);
					}
				}
		
				
				
				SubprojectsUDL.ExecutePaymentsDelBySubprojectProc(ID);
				foreach(PaymentData pd in pv)
					PaymentDAL.Save(pd);
			}
			Response.Redirect("Subprojects.aspx?pid="+UIHelpers.GetPIDParam());
		}

		private void btnCancel_Click(object sender, System.EventArgs e)
		{
			Response.Redirect("Subprojects.aspx?pid="+UIHelpers.GetPIDParam());
		}

		private void btnDelete_Click(object sender, System.EventArgs e)
		{
			SubprojectDAL.Delete(UIHelpers.GetSIDParam());
			Response.Redirect("Subprojects.aspx?pid="+UIHelpers.GetPIDParam());
		}

		private void dlPayments_ItemDataBound(object sender, System.Web.UI.WebControls.DataListItemEventArgs e)
		{
			
			HtmlImage hi=(HtmlImage) e.Item.FindControl("lkCal");
			TextBox txt=(TextBox) e.Item.FindControl("txtDate");
			if(txt!=null&& hi!=null)
				hi.Attributes["onclick"] = TimeHelper.InvokePopupCal(txt);

		}
		private decimal Recalculate()
		{
			decimal rate=0;
			if(txtRate.Text!="")
				rate=UIHelpers.ParseDecimal(txtRate.Text);
			decimal EUR=0;
			PaymentSchemes ps =(PaymentSchemes) int.Parse(ddlScheme.SelectedValue);
			if(ps==PaymentSchemes.Fixed)
				EUR=rate;
			else if(ps==PaymentSchemes.Area)
				EUR=rate*UIHelpers.ParseDecimal(txtArea.Text);
			decimal fix=decimal.Parse( System.Configuration.ConfigurationSettings.AppSettings["fixedBGNEUR"]);
			lbEUR.Text=UIHelpers.FormatDecimal2(EUR);
			lbBGN.Text=UIHelpers.FormatDecimal2(EUR*fix);
			return EUR;
		}
		private void SetRec()
		{
			decimal EUR=Recalculate();
			decimal dTotal=0;
			for(int i=0;i<dlPayments.Items.Count;i++)
			{
				decimal per=0;
				

				TextBox txtPer=(TextBox) dlPayments.Items[i].FindControl("txtPer");
				if(txtPer!=null && txtPer.Text!="")
				{
					per = UIHelpers.ParseDecimal(txtPer.Text);
					
				}
				
				TextBox txtAmount=(TextBox) dlPayments.Items[i].FindControl("txtAmount");
				CheckBox ckPaid=(CheckBox) dlPayments.Items[i].FindControl("ckPaid");
				if(txtAmount!=null )
				{

					if(txtAmount.Text=="")
					{
						if(i==dlPayments.Items.Count-1 && txtPer.Text=="")
						{
							if(EUR>0)
							{
								if(!ckPaid.Checked)
									txtAmount.Text=UIHelpers.FormatDecimal2(EUR-dTotal);
								decimal d = UIHelpers.ParseDecimal(txtAmount.Text);
								per= d/EUR;
								txtPer.Text=UIHelpers.FormatDecimal4(per*100);
							}
						}
						else
						{
							if(!ckPaid.Checked)
								txtAmount.Text=UIHelpers.FormatDecimal2((decimal)per * EUR/100);
						}
					}
					else if(txtPer.Text=="")
					{
						decimal d = UIHelpers.ParseDecimal(txtAmount.Text);
						per= d/EUR;
						txtPer.Text=UIHelpers.FormatDecimal4(per*100);
					}
					else 
					{
						if(!ckPaid.Checked)
						txtAmount.Text=UIHelpers.FormatDecimal2((decimal)per * EUR/100);
					}
					dTotal+=UIHelpers.ParseDecimal(txtAmount.Text);
		
				}

				
			}
		}
		private void btn_Click(object sender, System.EventArgs e)
		{
			SetRec();
			
		}

		private void btnNewPayment_Click(object sender, System.EventArgs e)
		{
			PaymentData pd = new PaymentData(-1,-1,-1,0,0,false,new DateTime(),0,false);
			SessionTable.Add(pd);
			dlPayments.DataSource=SessionTable;
			dlPayments.DataBind();
		}

		private void lkFax_Click(object sender, System.EventArgs e)
		{
//			int ClientID=0;
//			if(hdnClientID.Value!="")
//				ClientID=int.Parse(hdnClientID.Value);
			

			//hdnInfo.Value
		}

		private void btnNote_Click(object sender, System.EventArgs e)
		{
			PdfExport pdf = new PdfExport();
			XlsExport xls = new XlsExport();
		
			pdf.NeverEmbedFonts = "";

			DataDynamics.ActiveReports.ActiveReport report = new cs(lbProject.Text,hdnInfo.Value,  txtNotes.Text,LoggedUser.FullName);
			report.Run();
			System.IO.MemoryStream memoryFile = new System.IO.MemoryStream();

			pdf.Export(report.Document, memoryFile);
			Response.Clear();

			Response.AppendHeader( "content-disposition","attachment; filename="+"Notes.pdf");
			Response.ContentType = "application/pdf";


			memoryFile.WriteTo(Response.OutputStream); 
			Response.End(); 
			//UIHelpers.Note(this.Page,lbProject.Text,hdnInfo.Value,  txtNotes.Text,LoggedUser.FullName);
		}

    private void InitEdit()
    {
      int nSID=UIHelpers.GetSIDParam();

      if (nSID<=0) return;

      string vmButtons = this.btnEdit.ClientID;
      string emButtons = this.btnSave.ClientID+";"+this.btnDelete.ClientID;

      editCtrl.InitEdit("tblForm;tblPayments;tbl", vmButtons, emButtons, "");
    }
	}
}
