using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Data.SqlClient;
using Asa.Timesheet.Data;
using Asa.Timesheet.Data.Entities;
using log4net;
using Asa.Timesheet.WebPages.UserControls;
using DataDynamics.ActiveReports.Export.Pdf;
using DataDynamics.ActiveReports.Export.Xls;
using Asa.Timesheet.WebPages.Reports;
using DataDynamics.ActiveReports.Export.Rtf;
namespace Asa.Timesheet.WebPages
{
	/// <summary>
	/// Summary description for Projects.
	/// </summary>
	public class Projects : TimesheetPageBase 
	{
		#region Web controls

		protected System.Web.UI.WebControls.Panel grid;
		protected System.Web.UI.WebControls.Label lblInfo;
		protected System.Web.UI.WebControls.Button btnNewProject;
		protected System.Web.UI.WebControls.DataGrid grdProjects;
		protected System.Web.UI.WebControls.Label lblError;
		protected System.Web.UI.WebControls.PlaceHolder menuHolder;
		protected Asa.Timesheet.WebPages.UserControls.PageHeader header;

		#endregion
		protected System.Web.UI.WebControls.Button btnExport;
		protected Telerik.WebControls.RadGrid gridCalls;
		protected System.Web.UI.WebControls.Label lblName;
		protected System.Web.UI.WebControls.Button btnSearch;
		protected System.Web.UI.WebControls.DropDownList ddlSearch;
		protected System.Web.UI.WebControls.TextBox txtProject;
		protected System.Web.UI.WebControls.Button btnClear;
		protected System.Web.UI.WebControls.DropDownList ddlBuildingTypes;
		protected System.Web.UI.WebControls.Button btnMissing;


		private static readonly ILog log = LogManager.GetLogger(typeof(Projects));

		#region Grid columns

		private enum GridColumns
		{
			Number=0,
			ProjectName,
			ProjectCode,
			StartDate,
			BuildingType,
			Area,
			Address,
			Client,
			ProjectManager,
			Profile,
			Subprojects,
			SubContracters,
			EditItem,
			DeleteItem,
			Active,
			Black,
			Concluded
		}

		#endregion

		private void Page_Load(object sender, System.EventArgs e)
		{
			if (!LoggedUser.HasRole) ErrorRedirect(Resource.ResourceManager["noRightsAlert"]);
		
			UIHelpers.CreateMenu(menuHolder,LoggedUser);
			Page.RegisterHiddenField("__EVENTTARGET", "btnSearch");
			if (!this.IsPostBack)
			{
				
				header.PageTitle = Resource.ResourceManager["projects_PageTitle"];
				header.UserName = LoggedUser.UserName;
				btnNewProject.Text = Resource.ResourceManager["projects_btnNewProject"];

				if (!LoggedUser.IsLeader)
						grdProjects.Columns[(int)GridColumns.DeleteItem].Visible = false;
				SortOrder = 1;
				ProjectsInfo pi = SessionManager.CurrentProjectsInfo;
				string sBT=System.Configuration.ConfigurationSettings.AppSettings["HousesID"];
				if(pi!=null)
				{
					txtProject.Text=pi.SearchText;
					ddlSearch.SelectedValue=pi.TypeID.ToString();
					sBT=pi.BuildingType;
				}
				LoadBuildingTypes(sBT);
				BindGrid();
			}
			if(!(LoggedUser.IsAssistantOnly || LoggedUser.HasPaymentRights ||LoggedUser.IsSecretary))
			{
				btnNewProject.Visible=false;
			}
		}

		private bool LoadBuildingTypes(string selectedValue)
		{
			SqlDataReader reader = null;
			try
			{
				reader = DBManager.SelectBuildingTypes();
				ddlBuildingTypes.DataSource = reader;
				ddlBuildingTypes.DataValueField = "BuildingTypeID";
				ddlBuildingTypes.DataTextField = "BuildingType";
				ddlBuildingTypes.DataBind();

				ddlBuildingTypes.Items.Insert(0, new ListItem("", "0"));
				ddlBuildingTypes.SelectedValue = selectedValue;
			}
			catch (Exception ex)
			{
				log.Info(ex);
				return false;
			}
			finally 
			{
				if (reader!=null) reader.Close();
			}

			return true;
		}
		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
			this.ddlSearch.SelectedIndexChanged += new System.EventHandler(this.ddlSearch_SelectedIndexChanged);
			this.ddlBuildingTypes.SelectedIndexChanged += new System.EventHandler(this.ddlBuildingTypes_SelectedIndexChanged);
			this.btnSearch.Click += new System.EventHandler(this.btnSearch_Click);
			this.btnClear.Click += new System.EventHandler(this.btnClear_Click);
			this.grdProjects.ItemCreated += new System.Web.UI.WebControls.DataGridItemEventHandler(this.grdProjects_ItemCreated);
			this.grdProjects.ItemCommand += new System.Web.UI.WebControls.DataGridCommandEventHandler(this.grdProjects_ItemCommand);
			this.grdProjects.EditCommand += new System.Web.UI.WebControls.DataGridCommandEventHandler(this.grdProjects_EditCommand);
			this.grdProjects.ItemDataBound += new System.Web.UI.WebControls.DataGridItemEventHandler(this.grdProjects_ItemDataBound);
			this.btnNewProject.Click += new System.EventHandler(this.btnNewProject_Click);
			this.btnExport.Click += new System.EventHandler(this.btnExport_Click);
			this.btnMissing.Click += new System.EventHandler(this.btnMissing_Click);
			this.Load += new System.EventHandler(this.Page_Load);

		}
		#endregion

		#region Database

		protected Color GetColor(object o)
		{
			if(DBNull.Value==o)
                return Color.White;
			string sColor= (string)o;
			return ColorTranslator.FromHtml(sColor);
		}
		protected Color GetForeColor(object o)
		{
			if(DBNull.Value==o)
				return Color.Black;
			else
				return Color.White;
		}
		private void BindGrid()
		{
			SqlDataReader reader = null;
			int Active = int.Parse(ddlSearch.SelectedValue);
			try
			{
				reader = ProjectsData.SelectProjects(SortOrder,  Active,  txtProject.Text.Trim(), int.Parse(ddlBuildingTypes.SelectedValue), ddlSearch.SelectedValue=="2");
				if (reader!=null)
				{
					grdProjects.DataSource = reader;
					grdProjects.DataKeyField = "ProjectID";
					grdProjects.DataBind();
				}
			}
			catch (Exception ex)
			{
				log.Info(ex);
				lblError.Text = Resource.ResourceManager["projects_ErrorLoadProjects"];
				return;
			}
			finally
			{
				if (reader!=null) reader.Close();
			}

			if (grdProjects.Items.Count == 0) 
			{
				grid.Visible = false;
				lblInfo.Text = Resource.ResourceManager["projects_NoActiveProjects"];
			}
			else grid.Visible = true;

			try
			{
				SetConfirmDelete(grdProjects, "btnDelete", 1);
			}
			catch (Exception ex)
			{
				log.Error(ex);
			}
			if(IsPostBack)
			{
				ProjectsInfo pi = new ProjectsInfo();
				pi.SearchText=txtProject.Text;
				pi.TypeID= Active;
				pi.BuildingType=ddlBuildingTypes.SelectedValue;
				SessionManager.CurrentProjectsInfo=pi;
				
			}
		}

		private bool DeleteProject(int projectID)
		{
			try
			{
				ProjectsData.InactiveProject(projectID);
			}
			catch (Exception ex)
			{
				log.Info(ex);
				return false;
			}

			return true;
		}

		#endregion
		

		#region Event handlers

		private void grdProjects_ItemCommand(object source, System.Web.UI.WebControls.DataGridCommandEventArgs e)
		{
			if (e.CommandName == "Sort")
			{
				int sortOrder = SortOrder;
				switch ((string)e.CommandArgument)
				{
					case "Name": if (sortOrder==1) sortOrder = -1; else sortOrder = 1;
						SortOrder = sortOrder;
						BindGrid(); break;
					case "Code": if (sortOrder==2) sortOrder = -2; else sortOrder = 2;
						SortOrder = sortOrder;
						BindGrid(); break;
					case "StartDate": if (sortOrder==3) sortOrder = -3; else sortOrder = 3;
						SortOrder = sortOrder;
						BindGrid(); break;
					case "BuildingType": if (sortOrder==4) sortOrder = -4; else sortOrder = 4;
						SortOrder = sortOrder;
						BindGrid(); break;
					case "Area": if (sortOrder==5) sortOrder = -5; else sortOrder = 5;
						SortOrder = sortOrder;
						BindGrid(); break;
					case "Address": if (sortOrder==6) sortOrder = -6; else sortOrder = 6;
						SortOrder = sortOrder;
						BindGrid(); break;
					case "Client": if (sortOrder==7) sortOrder = -7; else sortOrder = 7;
						SortOrder = sortOrder;
						BindGrid(); break;
					case "Leader": if (sortOrder==8) sortOrder = -8; else sortOrder = 8;
						SortOrder = sortOrder;
						BindGrid(); break;

				}
				return;
			}
			else if(e.CommandName=="Edit")
			{
				string sProjectID = ((DataGrid)source).DataKeys[e.Item.ItemIndex].ToString();
				Response.Redirect("EditProject.aspx"+"?pid="+sProjectID); 
				return;
			}
			ImageButton ib = (ImageButton)e.CommandSource;
			string s = ib.ID;
			switch (ib.ID)
			{
				case "btnDelete": int projectID = (int)((DataGrid)source).DataKeys[e.Item.ItemIndex];
								  if (!DeleteProject(projectID))
								  {
									  lblError.Text = Resource.ResourceManager["editProject_ErrorDelete"];
									  return;
							      };
								  BindGrid();
							      break;
				case "btnEdit": string sProjectID = ((DataGrid)source).DataKeys[e.Item.ItemIndex].ToString();
									Response.Redirect("EditProject.aspx"+"?pid="+sProjectID); break;
				case "btnSubprojects": string sProjectID1 = ((DataGrid)source).DataKeys[e.Item.ItemIndex].ToString();
					Response.Redirect("Subprojects.aspx"+"?pid="+sProjectID1); break;
				case "btnProfile": string sProjectID2 = ((DataGrid)source).DataKeys[e.Item.ItemIndex].ToString();
					Response.Redirect("Profile.aspx"+"?pid="+sProjectID2); break;
				case "btnSubContracters": string sProjectID3 = ((DataGrid)source).DataKeys[e.Item.ItemIndex].ToString();
					Response.Redirect("ProjectSubcontracters.aspx"+"?pid="+sProjectID3); break;
					
					
			}
		}

		private void btnNewProject_Click(object sender, System.EventArgs e)
		{
			Response.Redirect("EditProject.aspx");
		}

		#endregion

		#region Menu
//		private void CreateMenu()
//		{
//			UserControls.MenuTable menu = new UserControls.MenuTable();
//			menu.ID = "MenuTable";
//
//			ArrayList menuItems = new ArrayList();
//			menuItems.Add(new MenuItemInfo(Resource.ResourceManager["menuItem_Hours"], "Hours.aspx", true));
//			if ((LoggedUser.IsLeader) || (LoggedUser.IsAssistant)) 
//			{
//				menuItems.Add(new MenuItemInfo(Resource.ResourceManager["menuItem_Clients"], "Clients.aspx"));
//								
//				menuItems.Add(new MenuItemInfo(Resource.ResourceManager["menuItem_Subcontracters"], "Subcontracters.aspx"));
//			}
//			if (LoggedUser.IsLeader || LoggedUser.IsAdmin)
//				menuItems.Add(new MenuItemInfo(Resource.ResourceManager["menuItem_Users"], "Users.aspx"));			
//		
//			menuItems.Add(new MenuItemInfo(Resource.ResourceManager["menuItem_Emails"], "Emails.aspx"));
//			menu.AddMenuGroup(Resource.ResourceManager["menuHeader_Links"], 20, menuItems);
//			
//			//2 group
//			menuItems = new ArrayList();
//			menuItems.Add(new MenuItemInfo(Resource.ResourceManager["menuItem_NewProject"], "EditProject.aspx"));
//			if ((LoggedUser.IsLeader) || (LoggedUser.IsAssistant)) 
//			{
//				menuItems.Add(new MenuItemInfo(Resource.ResourceManager["menuItem_NewClient"], "EditClient.aspx"));
//			}
//			if (LoggedUser.IsLeader || LoggedUser.IsAdmin)
//				menuItems.Add(new MenuItemInfo(Resource.ResourceManager["menuItem_NewUser"], "EditUser.aspx"));		
//	
//			menuItems.Add(new MenuItemInfo(Resource.ResourceManager["menuItem_NewEmail"], "EditEmail.aspx"));
//			menuItems.Add(new MenuItemInfo(Resource.ResourceManager["newcontracter_PageTitle"], "EditSubContracter.aspx"));
//
//			menu.AddMenuGroup(Resource.ResourceManager["menuHeader_New"], 30, menuItems);
//
//			menuItems = new ArrayList();
//			menuItems.Add(new MenuItemInfo(Resource.ResourceManager["menuItem_WorkTimeReport"], "reports/worktimes.aspx"));
//			menuItems.Add(new MenuItemInfo(Resource.ResourceManager["menuItem_MinutesReport"], "Reports/Minutes.aspx"));
//			if (LoggedUser.IsLeader || LoggedUser.IsAssistant)
//				menuItems.Add(new MenuItemInfo(Resource.ResourceManager["menuItem_MailReport"], "MailForm.aspx"));
//			if (LoggedUser.IsLeader || LoggedUser.IsAdmin)
//			{
//				menuItems.Add(new MenuItemInfo(Resource.ResourceManager["menuItem_NotEnteredWorkTime"], "reports/EmptyHours.aspx"));
//				menuItems.Add(new MenuItemInfo(Resource.ResourceManager["menuItem_Calendar"], "reports/Calendar.aspx"));
//			}
//
//			menu.AddMenuGroup(Resource.ResourceManager["menuHeader_Reports"], 30, menuItems);
//			
//			menuItems = new ArrayList();
//			menuItems.Add(new MenuItemInfo(Resource.ResourceManager["menuItem_Help"], SessionManager.GetHelpLink(),true,Pages.Projects));
//			menu.AddMenuGroup("", 10, menuItems);
//
//			menuHolder.Controls.Add(menu);
//		}
		#endregion


		private void grdProjects_ItemCreated(object sender, System.Web.UI.WebControls.DataGridItemEventArgs e)
		{
			if (e.Item.ItemType == ListItemType.Header)
			{
				string imgUrl = "";
				int sortOrder = SortOrder;

				if (sortOrder!=0)
				{
					imgUrl = (sortOrder>0)?"images/sup.gif":"images/sdown.gif";
					Label sep = new Label(); 
					sep.Width = 2; 
					e.Item.Cells[Math.Abs(sortOrder)].Controls.Add(sep);
					ImageButton ib = new ImageButton();
					ib.Height = 11; ib.Width = 11; ib.ImageUrl = imgUrl;
					e.Item.Cells[Math.Abs(sortOrder)].Controls.Add(ib);
				}
			}
		}

		private void grdProjects_EditCommand(object source, System.Web.UI.WebControls.DataGridCommandEventArgs e)
		{
			
		}

		private void btnExport_Click(object sender, System.EventArgs e)
		{
			
			this.EnableViewState = true;

			System.IO.StringWriter tw = new System.IO.StringWriter();
			System.Web.UI.HtmlTextWriter hw = new System.Web.UI.HtmlTextWriter(tw);
			SqlDataReader reader = null;

			int Active = int.Parse(ddlSearch.SelectedValue);
			try
			{
				reader = ProjectsData.SelectProjects(SortOrder,  Active,  txtProject.Text.Trim(),int.Parse(ddlBuildingTypes.SelectedValue), ddlSearch.SelectedValue=="2");
				if (reader!=null)
				{
					gridCalls.DataSource = reader;
					gridCalls.DataKeyField = "ProjectID";
					
				gridCalls.AllowSorting=false;
					
					gridCalls.DataBind();
					for(int i=0;i<gridCalls.Columns.Count;i++)
					{
						
						gridCalls.Columns[i].SortExpression=null;
					}
				}
			}
			
			finally
			{
				if (reader!=null) reader.Close();
			}

			gridCalls.MasterTableView.ExportToExcel("Projects");
  
			
		}

		private void btnSearch_Click(object sender, System.EventArgs e)
		{
		BindGrid();
		}

		private void grdProjects_ItemDataBound(object sender, System.Web.UI.WebControls.DataGridItemEventArgs e)
		{
			if(e.Item.ItemType==ListItemType.Item
				|| e.Item.ItemType==ListItemType.AlternatingItem)
			{
				if(e.Item.Cells[(int)GridColumns.Active].Text=="0")
				{
					e.Item.CssClass="Inactive";
	
				}
				else if(e.Item.Cells[(int)GridColumns.Concluded].Text=="1")
				{
					e.Item.CssClass="Concluded";
	
				}
					
				else if(LoggedUser.HasPaymentRights && e.Item.Cells[(int)GridColumns.Black].Text=="1")
				{
					e.Item.CssClass="Black";
	
				}
			}
		}

		private void btnClear_Click(object sender, System.EventArgs e)
		{
			txtProject.Text="";
			ddlSearch.SelectedIndex=0;
			ddlBuildingTypes.SelectedIndex=0;
			BindGrid();
		}

		private void ddlBuildingTypes_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			BindGrid();
		}

		private void ddlSearch_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			BindGrid();
		}

		private void btnMissing_Click(object sender, System.EventArgs e)
		{
			SqlDataReader reader = null;
			int Active = int.Parse(ddlSearch.SelectedValue);
			try
			{
				reader = ProjectsData.SelectProjects(SortOrder,  Active,  txtProject.Text.Trim(), int.Parse(ddlBuildingTypes.SelectedValue), ddlSearch.SelectedValue=="2");
				ArrayList al = new ArrayList();
				while(reader.Read())
				{
					int nID= reader.GetInt32(0);
					al.Add(nID);
				}
				if(al.Count>0)
				{
					PdfExport pdf = new PdfExport();
					pdf.NeverEmbedFonts = "";

					DataDynamics.ActiveReports.ActiveReport report = Reports.MissingData.AllProjectMissings.CreateProjectMissings(al);
					System.IO.MemoryStream memoryFile = new System.IO.MemoryStream();

					pdf.Export(report.Document, memoryFile);
					Response.Clear();

					Response.AppendHeader( "content-disposition","attachment; filename="+"AllMissingInfo.pdf");
					Response.ContentType = "application/pdf";

					memoryFile.WriteTo(Response.OutputStream); 
					Response.End(); 
				}
			}
			catch (Exception ex)
			{
				log.Info(ex);
				lblError.Text = Resource.ResourceManager["projects_ErrorLoadProjects"];
				return;
			}
			finally
			{
				if (reader!=null) reader.Close();
			}
		}
	}
}
