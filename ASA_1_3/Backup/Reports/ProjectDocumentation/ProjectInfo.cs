using System;
using System.Data;
using Asa.Timesheet.Data.Reports;
using Asa.Timesheet.Data;
using Asa.Timesheet.Data.Entities;
using GrapeCity.ActiveReports;
using GrapeCity.ActiveReports.Controls;
using GrapeCity.ActiveReports.SectionReportModel;
using GrapeCity.ActiveReports.Document.Section;
using GrapeCity.ActiveReports.Document;

namespace Asa.Timesheet.WebPages.Reports
{
    public class ProjectInfo : GrapeCity.ActiveReports.SectionReport
	{
		int _projectID;
		int _startPageNumber;
		bool _fullReport;

        public ProjectInfo(int projectID, bool fullReport, int startPageNumber)
        {
            InitializeComponent();

            _projectID = projectID;
            _fullReport = fullReport;
            _startPageNumber = startPageNumber;
        }

        private void BasicInfoSubreport_ReportStart(object sender, System.EventArgs eArgs)
        {
            if (!this._fullReport)
            {

            }

            this.DataSource = ProjectsReportData.GetProjectBasicInfo(_projectID);
        }

        private void PageHeader_Format(object sender, System.EventArgs eArgs)
        {
            if (this.PageNumber == 1) this.PageHeader.Visible = false;
            else
            {
                this.PageHeader.Visible = true;
                txtPageHeader.Text = String.Concat(this.Fields["ProjectName"].Value.ToString(), " - ",
                    Resource.ResourceManager["Reports_strBasicInfo"]);
            }
        }

        private void ReportHeader_Format(object sender, System.EventArgs eArgs)
        {
            this.txtProjectNameHeader.Text = this.Fields["ProjectName"].Value.ToString();
            txtReportTitle.Text = Resource.ResourceManager["Reports_strBasicInfo"];
        }

        private void Detail_Format(object sender, System.EventArgs eArgs)
        {

            decimal area = 0;
            try
            {
                area = (decimal)this.Fields["Area"].Value;
            }
            catch
            { }
            ProjectUsersVector puv = ProjectUserDAL.LoadCollection("ProjectUsersSelByProjectProc", SQLParms.CreateProjectUsersSelByProjectProc(_projectID));
            if (puv.Count > 0)
            {
                string s = "";
                foreach (ProjectUserData pud in puv)
                {
                    s += ", ";
                    UserInfo ui = UsersData.SelectUserByID(pud.UserID);
                    s += ui.FullName.ToString();
                }
                string s1 = s.Substring(2);
                TextBox30.Text = s1;
            }
            else
                TextBox29.Visible = false;
            ProjectClientsVector pcv = ProjectClientDAL.LoadCollection("ProjectClientsSelByProjectProc", SQLParms.CreateProjectClientsSelByProjectProc(_projectID, (int)ClientTypes.Client));
            if (pcv.Count > 0)
            {
                string s = txtClient.Text;
                foreach (ProjectClientData pud in pcv)
                {
                    s += ", ";
                    ClientData ui = ClientDAL.Load(pud.ClientID);
                    s += ui.ClientName.ToString();
                }
                //				string s1=s.Substring(2);
                txtClient.Text = s;
            }
            if (!this._fullReport) return;

            decimal totalAmountEUR = 0;

            decimal fix = decimal.Parse(System.Configuration.ConfigurationManager.AppSettings["fixedBGNEUR"]);

            decimal rate = 0;
            try
            {
                rate = (decimal)this.Fields["Rate"].Value;
            }
            catch
            { }

            int paymentSchemeID = (int)PaymentSchemes.Area;
            try
            {
                paymentSchemeID = (int)this.Fields["PaymentSchemeID"].Value;
            }
            catch
            { }

            if (paymentSchemeID == (int)PaymentSchemes.Fixed) totalAmountEUR = rate;
            else totalAmountEUR = area * rate;


        }

        private void PageFooter_Format(object sender, System.EventArgs eArgs)
        {

        }

		#region ActiveReports Designer generated code





























        public void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ProjectInfo));
            this.Detail = new GrapeCity.ActiveReports.SectionReportModel.Detail();
            this.ReportHeader = new GrapeCity.ActiveReports.SectionReportModel.ReportHeader();
            this.ReportFooter = new GrapeCity.ActiveReports.SectionReportModel.ReportFooter();
            this.PageHeader = new GrapeCity.ActiveReports.SectionReportModel.PageHeader();
            this.PageFooter = new GrapeCity.ActiveReports.SectionReportModel.PageFooter();
            this.txtReportTitle = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtProjectNameHeader = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.Line3 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.txtAdminName = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtPageHeader = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.Line1 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.txtInvestor = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtAdmAddress = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCode = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtClient = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtArea = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.TextBox8 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.TextBox19 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.TextBox20 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.TextBox22 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.TextBox23 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.TextBox24 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtBuildingType = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.TextBox25 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.TextBox26 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.TextBox27 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.TextBox28 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.TextBox29 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.TextBox30 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            ((System.ComponentModel.ISupportInitialize)(this.txtReportTitle)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtProjectNameHeader)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAdminName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPageHeader)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtInvestor)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAdmAddress)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCode)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtClient)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtArea)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TextBox8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TextBox19)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TextBox20)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TextBox22)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TextBox23)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TextBox24)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtBuildingType)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TextBox25)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TextBox26)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TextBox27)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TextBox28)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TextBox29)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TextBox30)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            // 
            // Detail
            // 
            this.Detail.CanShrink = true;
            this.Detail.ColumnSpacing = 0F;
            this.Detail.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
						this.txtInvestor,
						this.txtAdmAddress,
						this.txtCode,
						this.txtClient,
						this.txtArea,
						this.TextBox8,
						this.TextBox19,
						this.TextBox20,
						this.TextBox22,
						this.TextBox23,
						this.TextBox24,
						this.txtBuildingType,
						this.TextBox25,
						this.TextBox26,
						this.TextBox27,
						this.TextBox28,
						this.TextBox29,
						this.TextBox30});
            this.Detail.Height = 2.488889F;
            this.Detail.Name = "Detail";
            // 
            // ReportHeader
            // 
            this.ReportHeader.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
						this.txtReportTitle,
						this.txtProjectNameHeader,
						this.Line3,
						this.txtAdminName});
            this.ReportHeader.Height = 0.7965278F;
            this.ReportHeader.Name = "ReportHeader";
            // 
            // ReportFooter
            // 
            this.ReportFooter.Height = 0.02083333F;
            this.ReportFooter.Name = "ReportFooter";
            // 
            // PageHeader
            // 
            this.PageHeader.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
						this.txtPageHeader,
						this.Line1});
            this.PageHeader.Height = 0.5902778F;
            this.PageHeader.Name = "PageHeader";
            // 
            // PageFooter
            // 
            this.PageFooter.Height = 0.2201389F;
            this.PageFooter.Name = "PageFooter";
            // 
            // txtReportTitle
            // 
            this.txtReportTitle.Height = 0.2755906F;
            this.txtReportTitle.Left = 0F;
            this.txtReportTitle.Name = "txtReportTitle";
            this.txtReportTitle.Style = "font-size: 14pt; font-weight: bold; text-align: left; ddo-char-set: 1";
            this.txtReportTitle.Text = "???? ??????????";
            this.txtReportTitle.Top = 0F;
            this.txtReportTitle.Width = 6.3125F;
            // 
            // txtProjectNameHeader
            // 
            this.txtProjectNameHeader.Height = 0.2362205F;
            this.txtProjectNameHeader.Left = 1.875F;
            this.txtProjectNameHeader.Name = "txtProjectNameHeader";
            this.txtProjectNameHeader.Style = "font-size: 11pt; font-weight: bold; text-align: right; ddo-char-set: 1";
            this.txtProjectNameHeader.Text = "txtProjectNameHeader";
            this.txtProjectNameHeader.Top = 0.5625F;
            this.txtProjectNameHeader.Width = 4.4375F;
            // 
            // Line3
            // 
            this.Line3.Height = 0.0001810193F;
            this.Line3.Left = 0F;
            this.Line3.LineWeight = 2F;
            this.Line3.Name = "Line3";
            this.Line3.Top = 0.551F;
            this.Line3.Width = 6.3125F;
            this.Line3.X1 = 0F;
            this.Line3.X2 = 6.3125F;
            this.Line3.Y1 = 0.551181F;
            this.Line3.Y2 = 0.551F;
            // 
            // txtAdminName
            // 
            this.txtAdminName.DataField = "AdministrativeName";
            this.txtAdminName.Height = 0.2F;
            this.txtAdminName.Left = 0F;
            this.txtAdminName.Name = "txtAdminName";
            this.txtAdminName.Style = "font-size: 13pt; font-weight: bold; ddo-char-set: 1";
            this.txtAdminName.Text = "txtAdminName";
            this.txtAdminName.Top = 0.3125F;
            this.txtAdminName.Width = 5.31496F;
            // 
            // txtPageHeader
            // 
            this.txtPageHeader.Height = 0.2362205F;
            this.txtPageHeader.Left = 0F;
            this.txtPageHeader.Name = "txtPageHeader";
            this.txtPageHeader.Style = "font-size: 12pt; font-weight: bold";
            this.txtPageHeader.Text = "???? ??????????";
            this.txtPageHeader.Top = 0F;
            this.txtPageHeader.Width = 6.3125F;
            // 
            // Line1
            // 
            this.Line1.Height = 0.0002204925F;
            this.Line1.Left = 0F;
            this.Line1.LineWeight = 2F;
            this.Line1.Name = "Line1";
            this.Line1.Top = 0.236F;
            this.Line1.Width = 6.3125F;
            this.Line1.X1 = 0F;
            this.Line1.X2 = 6.3125F;
            this.Line1.Y1 = 0.2362205F;
            this.Line1.Y2 = 0.236F;
            // 
            // txtInvestor
            // 
            this.txtInvestor.DataField = "Investor";
            this.txtInvestor.Height = 0.2F;
            this.txtInvestor.Left = 1.771653F;
            this.txtInvestor.Name = "txtInvestor";
            this.txtInvestor.Style = "font-weight: bold";
            this.txtInvestor.Text = "txtInvestor";
            this.txtInvestor.Top = 0.375F;
            this.txtInvestor.Width = 4.540846F;
            // 
            // txtAdmAddress
            // 
            this.txtAdmAddress.DataField = "AddInfo";
            this.txtAdmAddress.Height = 0.2F;
            this.txtAdmAddress.Left = 1.771653F;
            this.txtAdmAddress.Name = "txtAdmAddress";
            this.txtAdmAddress.Style = "font-weight: bold";
            this.txtAdmAddress.Text = "txtAdmAddress";
            this.txtAdmAddress.Top = 0.8892716F;
            this.txtAdmAddress.Width = 4.540846F;
            // 
            // txtCode
            // 
            this.txtCode.DataField = "ProjectCode";
            this.txtCode.Height = 0.2F;
            this.txtCode.Left = 1.771653F;
            this.txtCode.Name = "txtCode";
            this.txtCode.Style = "font-weight: bold";
            this.txtCode.Text = "txtCode";
            this.txtCode.Top = 0.6505905F;
            this.txtCode.Width = 4.540846F;
            // 
            // txtClient
            // 
            this.txtClient.DataField = "ClientName";
            this.txtClient.Height = 0.2F;
            this.txtClient.Left = 1.771653F;
            this.txtClient.Name = "txtClient";
            this.txtClient.Style = "font-weight: bold";
            this.txtClient.Text = "txtClient";
            this.txtClient.Top = 1.667815F;
            this.txtClient.Width = 4.540846F;
            // 
            // txtArea
            // 
            this.txtArea.DataField = "Area";
            this.txtArea.Height = 0.2F;
            this.txtArea.Left = 1.771653F;
            this.txtArea.Name = "txtArea";
            this.txtArea.Style = "font-weight: bold";
            this.txtArea.Text = "txtArea";
            this.txtArea.Top = 1.164862F;
            this.txtArea.Width = 1.181102F;
            // 
            // TextBox8
            // 
            this.TextBox8.Height = 0.2F;
            this.TextBox8.Left = 0F;
            this.TextBox8.Name = "TextBox8";
            this.TextBox8.Text = "???????? ???:";
            this.TextBox8.Top = 1.164862F;
            this.TextBox8.Width = 1.771653F;
            // 
            // TextBox19
            // 
            this.TextBox19.Height = 0.2F;
            this.TextBox19.Left = 0F;
            this.TextBox19.Name = "TextBox19";
            this.TextBox19.Text = "??????:";
            this.TextBox19.Top = 1.667815F;
            this.TextBox19.Width = 1.771653F;
            // 
            // TextBox20
            // 
            this.TextBox20.Height = 0.2F;
            this.TextBox20.Left = 0F;
            this.TextBox20.Name = "TextBox20";
            this.TextBox20.Text = "????? - ??. ?:";
            this.TextBox20.Top = 0.8892716F;
            this.TextBox20.Width = 1.771653F;
            // 
            // TextBox22
            // 
            this.TextBox22.Height = 0.2F;
            this.TextBox22.Left = 0F;
            this.TextBox22.Name = "TextBox22";
            this.TextBox22.Text = "??? ?? ???????:";
            this.TextBox22.Top = 0.6505905F;
            this.TextBox22.Width = 1.771653F;
            // 
            // TextBox23
            // 
            this.TextBox23.Height = 0.2F;
            this.TextBox23.Left = 0F;
            this.TextBox23.Name = "TextBox23";
            this.TextBox23.Text = "??????????:";
            this.TextBox23.Top = 0.386975F;
            this.TextBox23.Width = 1.771653F;
            // 
            // TextBox24
            // 
            this.TextBox24.Height = 0.2F;
            this.TextBox24.Left = 0F;
            this.TextBox24.Name = "TextBox24";
            this.TextBox24.Text = "??? ??????:";
            this.TextBox24.Top = 1.429134F;
            this.TextBox24.Width = 1.771653F;
            // 
            // txtBuildingType
            // 
            this.txtBuildingType.DataField = "BuildingType";
            this.txtBuildingType.Height = 0.2F;
            this.txtBuildingType.Left = 1.771653F;
            this.txtBuildingType.Name = "txtBuildingType";
            this.txtBuildingType.Style = "font-weight: bold";
            this.txtBuildingType.Text = "txtBuildingType";
            this.txtBuildingType.Top = 1.429134F;
            this.txtBuildingType.Width = 4.540846F;
            // 
            // TextBox25
            // 
            this.TextBox25.Height = 0.2F;
            this.TextBox25.Left = 0F;
            this.TextBox25.Name = "TextBox25";
            this.TextBox25.Text = "???????????:";
            this.TextBox25.Top = 1.943406F;
            this.TextBox25.Width = 1.771653F;
            // 
            // TextBox26
            // 
            this.TextBox26.DataField = "ProjectManager";
            this.TextBox26.Height = 0.2F;
            this.TextBox26.Left = 1.771653F;
            this.TextBox26.Name = "TextBox26";
            this.TextBox26.Style = "font-weight: bold";
            this.TextBox26.Text = "TextBox26";
            this.TextBox26.Top = 1.943406F;
            this.TextBox26.Width = 4.540846F;
            // 
            // TextBox27
            // 
            this.TextBox27.Height = 0.2F;
            this.TextBox27.Left = 0F;
            this.TextBox27.Name = "TextBox27";
            this.TextBox27.Text = "?????? ??????????:";
            this.TextBox27.Top = 0.1161417F;
            this.TextBox27.Width = 1.771653F;
            // 
            // TextBox28
            // 
            this.TextBox28.Height = 0.2F;
            this.TextBox28.Left = 1.771653F;
            this.TextBox28.Name = "TextBox28";
            this.TextBox28.Style = "font-weight: bold";
            this.TextBox28.Text = "?????? ????????? ?????????";
            this.TextBox28.Top = 0.125F;
            this.TextBox28.Width = 4.5F;
            // 
            // TextBox29
            // 
            this.TextBox29.Height = 0.2F;
            this.TextBox29.Left = 0F;
            this.TextBox29.Name = "TextBox29";
            this.TextBox29.Text = "????:";
            this.TextBox29.Top = 2.25F;
            this.TextBox29.Width = 1.771653F;
            // 
            // TextBox30
            // 
            this.TextBox30.Height = 0.2F;
            this.TextBox30.Left = 1.8125F;
            this.TextBox30.Name = "TextBox30";
            this.TextBox30.Style = "font-weight: bold";
            this.TextBox30.Top = 2.25F;
            this.TextBox30.Width = 4.540846F;
            // 
            // ActiveReport1
            // 
            this.MasterReport = false;
            this.PageSettings.DefaultPaperSize = false;
            this.PageSettings.Margins.Bottom = 0.7875F;
            this.PageSettings.Margins.Left = 1.377778F;
            this.PageSettings.Margins.Right = 0.39375F;
            this.PageSettings.Margins.Top = 0.7875F;
            this.PageSettings.PaperHeight = 11.69306F;
            this.PageSettings.PaperKind = System.Drawing.Printing.PaperKind.A4;
            this.PageSettings.PaperWidth = 8.268056F;
            this.PrintWidth = 6.693F;
            this.Sections.Add(this.ReportHeader);
            this.Sections.Add(this.PageHeader);
            this.Sections.Add(this.Detail);
            this.Sections.Add(this.PageFooter);
            this.Sections.Add(this.ReportFooter);
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule(resources.GetString("$this.StyleSheet"), "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: inherit; font-style: inherit; font-variant: inherit; font-weight: bo" +
                        "ld; font-size: 16pt; font-size-adjust: inherit; font-stretch: inherit", "Heading1", "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: Times New Roman; font-style: italic; font-variant: inherit; font-wei" +
                        "ght: bold; font-size: 14pt; font-size-adjust: inherit; font-stretch: inherit", "Heading2", "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: inherit; font-style: inherit; font-variant: inherit; font-weight: bo" +
                        "ld; font-size: 13pt; font-size-adjust: inherit; font-stretch: inherit", "Heading3", "Normal"));
            ((System.ComponentModel.ISupportInitialize)(this.txtReportTitle)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtProjectNameHeader)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAdminName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPageHeader)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtInvestor)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAdmAddress)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCode)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtClient)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtArea)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TextBox8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TextBox19)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TextBox20)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TextBox22)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TextBox23)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TextBox24)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtBuildingType)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TextBox25)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TextBox26)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TextBox27)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TextBox28)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TextBox29)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TextBox30)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();

            // Attach Report Events
            this.ReportStart += new System.EventHandler(this.BasicInfoSubreport_ReportStart);
            this.PageHeader.Format += new System.EventHandler(this.PageHeader_Format);
            this.ReportHeader.Format += new System.EventHandler(this.ReportHeader_Format);
            this.Detail.Format += new System.EventHandler(this.Detail_Format);
            this.PageFooter.Format += new System.EventHandler(this.PageFooter_Format);
        }

		#endregion

        private ReportHeader ReportHeader;
        private TextBox txtReportTitle;
        private TextBox txtProjectNameHeader;
        private Line Line3;
        private TextBox txtAdminName;
        private PageHeader PageHeader;
        private TextBox txtPageHeader;
        private Line Line1;
        private Detail Detail;
        private TextBox txtInvestor;
        private TextBox txtAdmAddress;
        private TextBox txtCode;
        private TextBox txtClient;
        private TextBox txtArea;
        private TextBox TextBox8;
        private TextBox TextBox19;
        private TextBox TextBox20;
        private TextBox TextBox22;
        private TextBox TextBox23;
        private TextBox TextBox24;
        private TextBox txtBuildingType;
        private TextBox TextBox25;
        private TextBox TextBox26;
        private TextBox TextBox27;
        private TextBox TextBox28;
        private TextBox TextBox29;
        private TextBox TextBox30;
        private PageFooter PageFooter;
        private ReportFooter ReportFooter;
	}
}
