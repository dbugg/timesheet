using System;
using System.Data;
using System.Data.SqlClient;
using System.Collections.Specialized;
using Microsoft.ApplicationBlocks.Data;
using log4net;
using log4net.Config;

namespace Asa.Timesheet.Data
{
	/// <summary>
	/// Summary description for MailDAL.
	/// </summary>
	public class MailDAL
	{
		private static readonly ILog log = LogManager.GetLogger(typeof(MailDAL));
		public static void LoadLeadersAndOfficersMails(out StringCollection names , out StringCollection emails )
		{
			emails = new StringCollection();
			names = new StringCollection();

			SqlDataReader reader = null;

			try
			{
				reader = UsersData.SelectLeadersOfficersEmails();
				while (reader.Read())
				{
					emails.Add(reader.GetString(1));
					names.Add(reader.GetString(0));

				}

			}
			catch (Exception ex)
			{
				log.Error(ex);
				emails = null;
				names = null;
			}
			finally
			{
				if (reader!=null) reader.Close();
			}
		}
		public static void LoadSecrAndOfficersMails(out StringCollection names , out StringCollection emails )
		{
			emails = new StringCollection();
			names = new StringCollection();

			SqlDataReader reader = null;

			try
			{
				reader = UsersData.SelectSecretariesOfficersEmails();
				while (reader.Read())
				{
					emails.Add(reader.GetString(1));
					names.Add(reader.GetString(0));

				}

			}
			catch (Exception ex)
			{
				log.Error(ex);
				emails = null;
				names = null;
			}
			finally
			{
				if (reader!=null) reader.Close();
			}
		}
		private static SqlCommand CreateCompleteProjectsListProc ()
		{
			SqlCommand cmd = new SqlCommand();
			cmd.CommandType = System.Data.CommandType.StoredProcedure;
			cmd.CommandText = "CompleteProjectsListProc";

			return cmd;
		}
		public static System.Data.DataView ExecuteCompleteProjectsListProc()  
		{
			System.Data.SqlClient.SqlDataReader RetObj;
			System.Data.SqlClient.SqlCommand cmd;
			cmd = CreateCompleteProjectsListProc();
			SqlConnection cnDAL = new SqlConnection();

			try
			{
				cnDAL =  CommonDAL.OpenConnection();
				cmd.Connection = cnDAL;
				RetObj = cmd.ExecuteReader();
				return new DataView(CommonDAL.CreateTableFromReader(RetObj));
		
			}
			catch(SqlException ex)
			{

				throw new ApplicationException("CompleteProjectsListProc",ex);
			}

			finally
			{
				CommonDAL.CloseConnection(cnDAL);
			}
		}


		//notes:here select mails for selected Role or all emails
		public static StringCollection LoadEmailsByRole(int RoleID)
		{
			StringCollection emails = new StringCollection();
			//names = new StringCollection();

			SqlDataReader reader = null;

			try
			{
				string spName = "UsersEmailsListByRoleProc";
				SqlParameter[] parms = new SqlParameter[1];
				parms[0]= new System.Data.SqlClient.SqlParameter("@RoleID", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.Input, true, 10, 0, "RoleID", DataRowVersion.Current, null);
				if(RoleID>0)
					parms[0].Value = RoleID;
				reader = SqlHelper.ExecuteReader(DBManager.GetConnectionString(), CommandType.StoredProcedure, spName,parms);
				while (reader.Read())
				{
					emails.Add(reader.GetString(0));
				}
				return emails;
			}
			catch (Exception ex)
			{
				log.Error(ex);
				return new StringCollection();
			}
			finally
			{
				if (reader!=null) reader.Close();
			}
		}
		
	}
}
