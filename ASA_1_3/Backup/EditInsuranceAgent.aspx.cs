using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Data.SqlClient;
using log4net;
using Asa.Timesheet.Data;
using Asa.Timesheet.Data.Entities;
using System.Text;

namespace Asa.Timesheet.WebPages
{
	/// <summary>
	/// Summary description for EditInsuranceAgent.
	/// </summary>
	public class EditInsuranceAgent : TimesheetPageBase
	{
		protected System.Web.UI.WebControls.PlaceHolder menuHolder;
		protected System.Web.UI.WebControls.Button btnSaveUP;
		protected System.Web.UI.WebControls.Button btnEditUP;
		protected System.Web.UI.WebControls.Button btnCancelUP;
		protected System.Web.UI.WebControls.Label lblName;
		protected System.Web.UI.WebControls.TextBox txtName;
		protected System.Web.UI.WebControls.Label Label5;
		protected System.Web.UI.WebControls.TextBox txtDelo;
		protected System.Web.UI.WebControls.Button btnSave;
		protected System.Web.UI.WebControls.Button btnEdit;
		protected System.Web.UI.WebControls.Button btnCancel;
		protected System.Web.UI.WebControls.Label lblInfo;
		protected System.Web.UI.WebControls.Label lblError;
		protected Asa.Timesheet.WebPages.UserControls.PageHeader header;

        private void Page_Load(object sender, System.EventArgs e)
        {
            UIHelpers.CreateMenu(menuHolder, LoggedUser);

            if (!this.IsPostBack)
            {
                header.UserName = LoggedUser.FullName;
                header.PageTitle = Resource.ResourceManager["editAgent_EditLabel"];
                int nID = UIHelpers.GetIDParam();
                if (nID > 0)
                {
                    InsuranceAgentData asd = InsuranceAgentDAL.Load(nID);
                    if (asd != null)
                    {
                        txtName.Text = asd.InsuranceAgentName;
                        txtDelo.Text = asd.InsurancePhones;
                    }

                }
            }
            if (!( LoggedUser.HasPaymentRights || LoggedUser.IsSecretary))
            {
                ErrorRedirect(Resource.ResourceManager["noRightsAlert"]);
            }
        }

		#region Web Form Designer generated code
        override protected void OnInit(EventArgs e)
        {
            //
            // CODEGEN: This call is required by the ASP.NET Web Form Designer.
            //
            InitializeComponent();
            base.OnInit(e);
        }
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
        private void InitializeComponent()
        {
            this.btnSaveUP.Click += new System.EventHandler(this.btnSave_Click);
            this.btnCancelUP.Click += new System.EventHandler(this.btnCancel_Click);
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            this.Load += new System.EventHandler(this.Page_Load);

        }
		#endregion

        private void btnSave_Click(object sender, System.EventArgs e)
        {
            int nID = UIHelpers.GetIDParam();
            InsuranceAgentData asd = new InsuranceAgentData(nID, txtName.Text, txtDelo.Text, true);
            InsuranceAgentDAL.Save(asd);
            Response.Redirect("Automobiles.aspx");
        }

        private void btnCancel_Click(object sender, System.EventArgs e)
        {
            Response.Redirect("Automobiles.aspx");
        }
	}
}
