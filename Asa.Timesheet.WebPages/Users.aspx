<%@ Page language="c#" Codebehind="Users.aspx.cs" AutoEventWireup="false" Inherits="Asa.Timesheet.WebPages.Users" %>
<%@ Register TagPrefix="radg" Namespace="Telerik.WebControls" Assembly="RadGrid" %>
<%@ Register TagPrefix="uc1" TagName="PageHeader" Src="UserControls/PageHeader.ascx" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
	<HEAD>
		<title>Users</title>
		<meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
		<meta content="C#" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<LINK href="styles/timesheet.css" type="text/css" rel="stylesheet">
	</HEAD>
	<body bottomMargin="0" leftMargin="0" topMargin="0" rightMargin="0">
		<form id="Form1" method="post" runat="server">
			<table height="100%" cellSpacing="0" cellPadding="0" width="100%" border="0">
				<tr height="1">
					<td><uc1:pageheader id="header" runat="server"></uc1:pageheader></td>
				</tr>
				<TR height="1">
					<td noWrap background="images/line.gif" height="1"></td>
				</TR>
				<tr>
					<td>
						<table height="100%" cellSpacing="0" cellPadding="0" width="100%" border="0">
							<tr>
								<td style="BACKGROUND-IMAGE: url(images/left110.gif)" vAlign="top" noWrap width="110"><asp:placeholder id="menuHolder" runat="server"></asp:placeholder></td>
								<td noWrap width="1" bgColor="lightgrey"></td>
								<td style="PADDING-RIGHT: 5px; PADDING-LEFT: 5px; PADDING-TOP: 3px" vAlign="top" noWrap><br>
									<asp:label id="lblError" runat="server" CssClass="ErrorLabel" EnableViewState="False" ForeColor="Red"></asp:label><asp:label id="lblInfo" runat="server" CssClass="InfoLabel" EnableViewState="False"></asp:label><br>
									<asp:panel id="grid" style="BORDER-RIGHT: #d2b48c 2px solid; PADDING-RIGHT: 2px; BORDER-TOP: #d2b48c 2px solid; PADDING-LEFT: 2px; OVERFLOW-X: auto; BORDER-LEFT: #d2b48c 2px solid; PADDING-TOP: 2px; BORDER-BOTTOM: #d2b48c 2px solid"
										runat="server" Height="450px" Width="100%">
										<TABLE>
											<TR>
												<TD>
													<asp:Label id="lbSectionArchitects" EnableViewState="False" CssClass="enterDataLabel" Width="160px"
														Runat="server" Font-Bold="True">АРХИТЕКТУРЕН ОТДЕЛ</asp:Label>
													<P></P>
													<asp:datagrid id="grdUsersArchitect" runat="server" CssClass="Grid" Width="100%" AllowSorting="True"
														AutoGenerateColumns="False" CellPadding="4" PageSize="2">
														<AlternatingItemStyle CssClass="GridAltItem"></AlternatingItemStyle>
														<ItemStyle CssClass="GridItem"></ItemStyle>
														<HeaderStyle CssClass="GridHeader"></HeaderStyle>
														<Columns>
															<asp:TemplateColumn HeaderText="#">
																<ItemStyle ForeColor="DimGray" Width="10px"></ItemStyle>
																<ItemTemplate>
																	<asp:Label CssClass="menuTable" runat="server" Text='<%# (int)DataBinder.Eval(Container, "ItemIndex")+1 %>' ID="Label2" NAME="Label1">
																	</asp:Label>
																</ItemTemplate>
															</asp:TemplateColumn>
															<asp:TemplateColumn SortExpression="FirstName" HeaderText="Име">
																<ItemTemplate>
																	<asp:LinkButton id="lnk" runat="server" CssClass="menuTable" ForeColor="#804000" CommandName="Edit">
																		<span Class="menuTable">
																			<%# DataBinder.Eval(Container, "DataItem.FirstName") %>
																		</span>
																	</asp:LinkButton>
																</ItemTemplate>
															</asp:TemplateColumn>
															<asp:BoundColumn DataField="LastName" SortExpression="LastName" HeaderText="Фамилия">
																<HeaderStyle Width="100px"></HeaderStyle>
																<ItemStyle Wrap="False"></ItemStyle>
															</asp:BoundColumn>
															<asp:BoundColumn DataField="Ext" HeaderText="Вътр. телефон">
																<HeaderStyle Width="120px"></HeaderStyle>
															</asp:BoundColumn>
															<asp:BoundColumn DataField="Mobile" HeaderText="Моб. телефон">
																<HeaderStyle Width="120px"></HeaderStyle>
															</asp:BoundColumn>
															<asp:BoundColumn DataField="MobilePersonal" HeaderText="Личен моб.">
																<HeaderStyle Width="120px"></HeaderStyle>
															</asp:BoundColumn>
															<asp:BoundColumn DataField="HomePhone" HeaderText="Дом. телефон">
																<HeaderStyle Width="120px"></HeaderStyle>
															</asp:BoundColumn>
															<asp:BoundColumn DataField="Mail" HeaderText="Ел.поща">
																<HeaderStyle Width="120px"></HeaderStyle>
																<ItemStyle Wrap="False"></ItemStyle>
															</asp:BoundColumn>
															<asp:BoundColumn DataField="RoleName" HeaderText="Роля в системата">
																<HeaderStyle Width="120px"></HeaderStyle>
																<ItemStyle ForeColor="DimGray"></ItemStyle>
															</asp:BoundColumn>
															<asp:TemplateColumn>
																<HeaderStyle Wrap="False" Width="20px"></HeaderStyle>
																<ItemStyle Wrap="False" HorizontalAlign="Center"></ItemStyle>
																<ItemTemplate>
																	<asp:ImageButton id="btnEdit" runat="server" ImageUrl="images/edit1.gif" ToolTip="Редактирай"></asp:ImageButton>
																</ItemTemplate>
															</asp:TemplateColumn>
															<asp:TemplateColumn>
																<HeaderStyle Wrap="False" Width="20px"></HeaderStyle>
																<ItemStyle Wrap="False" HorizontalAlign="Center"></ItemStyle>
																<ItemTemplate>
																	<asp:ImageButton id="btnDelete" runat="server" Width="12px" Height="12px" ToolTip="Изтрий" ImageUrl="images/delete.gif"></asp:ImageButton>
																</ItemTemplate>
															</asp:TemplateColumn>
														</Columns>
													</asp:datagrid></TD>
											</TR>
											<TR>
												<TD>
													<asp:Label id="lbSectionIngener" EnableViewState="False" CssClass="enterDataLabel" Width="140px"
														Runat="server" Font-Bold="True">ИНЖЕНЕРЕН ОТДЕЛ</asp:Label><P></P>
													<asp:datagrid id="grdUsersIngener" runat="server" CssClass="Grid" Width="100%" AllowSorting="True"
														AutoGenerateColumns="False" CellPadding="4" PageSize="2">
														<AlternatingItemStyle CssClass="GridAltItem"></AlternatingItemStyle>
														<ItemStyle CssClass="GridItem"></ItemStyle>
														<HeaderStyle CssClass="GridHeader"></HeaderStyle>
														<Columns>
															<asp:TemplateColumn HeaderText="#">
																<ItemStyle ForeColor="DimGray" Width="10px"></ItemStyle>
																<ItemTemplate>
																	<asp:Label CssClass="menuTable" runat="server" Text='<%# (int)DataBinder.Eval(Container, "ItemIndex")+1 %>' ID="Label3" NAME="Label1">
																	</asp:Label>
																</ItemTemplate>
															</asp:TemplateColumn>
															<asp:TemplateColumn SortExpression="FirstName" HeaderText="Име">
																<ItemTemplate>
																	<asp:LinkButton id="lnk" runat="server" CssClass="menuTable" ForeColor="#804000" CommandName="Edit">
																		<span Class="menuTable">
																			<%# DataBinder.Eval(Container, "DataItem.FirstName") %>
																		</span>
																	</asp:LinkButton>
																</ItemTemplate>
															</asp:TemplateColumn>
															<asp:BoundColumn DataField="LastName" SortExpression="LastName" HeaderText="Фамилия">
																<HeaderStyle Width="100px"></HeaderStyle>
																<ItemStyle Wrap="False"></ItemStyle>
															</asp:BoundColumn>
															<asp:BoundColumn DataField="Ext" HeaderText="Вътр. телефон">
																<HeaderStyle Width="120px"></HeaderStyle>
															</asp:BoundColumn>
															<asp:BoundColumn DataField="Mobile" HeaderText="Моб. телефон">
																<HeaderStyle Width="120px"></HeaderStyle>
															</asp:BoundColumn>
															<asp:BoundColumn DataField="MobilePersonal" HeaderText="Личен моб.">
																<HeaderStyle Width="120px"></HeaderStyle>
															</asp:BoundColumn>
															<asp:BoundColumn DataField="HomePhone" HeaderText="Дом. телефон">
																<HeaderStyle Width="120px"></HeaderStyle>
															</asp:BoundColumn>
															<asp:BoundColumn DataField="Mail" HeaderText="Ел.поща">
																<HeaderStyle Width="120px"></HeaderStyle>
																<ItemStyle Wrap="False"></ItemStyle>
															</asp:BoundColumn>
															<asp:BoundColumn DataField="RoleName" HeaderText="Роля в системата">
																<HeaderStyle Width="120px"></HeaderStyle>
																<ItemStyle ForeColor="DimGray"></ItemStyle>
															</asp:BoundColumn>
															<asp:TemplateColumn>
																<HeaderStyle Wrap="False" Width="20px"></HeaderStyle>
																<ItemStyle Wrap="False" HorizontalAlign="Center"></ItemStyle>
																<ItemTemplate>
																	<asp:ImageButton id="btnEdit" runat="server" ImageUrl="images/edit1.gif" ToolTip="Редактирай"></asp:ImageButton>
																</ItemTemplate>
															</asp:TemplateColumn>
															<asp:TemplateColumn>
																<HeaderStyle Wrap="False" Width="20px"></HeaderStyle>
																<ItemStyle Wrap="False" HorizontalAlign="Center"></ItemStyle>
																<ItemTemplate>
																	<asp:ImageButton id="btnDelete" runat="server" Width="12px" Height="12px" ToolTip="Изтрий" ImageUrl="images/delete.gif"></asp:ImageButton>
																</ItemTemplate>
															</asp:TemplateColumn>
														</Columns>
													</asp:datagrid></TD>
											</TR>
											<TR>
												<TD>
													<asp:Label id="lbSectionIT" EnableViewState="False" CssClass="enterDataLabel" Width="70px"
														Runat="server" Font-Bold="True">IT ОТДЕЛ</asp:Label><P></P>
													<asp:datagrid id="grdUsersIT" runat="server" CssClass="Grid" Width="100%" AllowSorting="True"
														AutoGenerateColumns="False" CellPadding="4" PageSize="2">
														<AlternatingItemStyle CssClass="GridAltItem"></AlternatingItemStyle>
														<ItemStyle CssClass="GridItem"></ItemStyle>
														<HeaderStyle CssClass="GridHeader"></HeaderStyle>
														<Columns>
															<asp:TemplateColumn HeaderText="#">
																<ItemStyle ForeColor="DimGray" Width="10px"></ItemStyle>
																<ItemTemplate>
																	<asp:Label CssClass="menuTable" runat="server" Text='<%# (int)DataBinder.Eval(Container, "ItemIndex")+1 %>' ID="Label4" NAME="Label1">
																	</asp:Label>
																</ItemTemplate>
															</asp:TemplateColumn>
															<asp:TemplateColumn SortExpression="FirstName" HeaderText="Име">
																<ItemTemplate>
																	<asp:LinkButton id="lnk" runat="server" CssClass="menuTable" ForeColor="#804000" CommandName="Edit">
																		<span Class="menuTable">
																			<%# DataBinder.Eval(Container, "DataItem.FirstName") %>
																		</span>
																	</asp:LinkButton>
																</ItemTemplate>
															</asp:TemplateColumn>
															<asp:BoundColumn DataField="LastName" SortExpression="LastName" HeaderText="Фамилия">
																<HeaderStyle Width="100px"></HeaderStyle>
																<ItemStyle Wrap="False"></ItemStyle>
															</asp:BoundColumn>
															<asp:BoundColumn DataField="Ext" HeaderText="Вътр. телефон">
																<HeaderStyle Width="120px"></HeaderStyle>
															</asp:BoundColumn>
															<asp:BoundColumn DataField="Mobile" HeaderText="Моб. телефон">
																<HeaderStyle Width="120px"></HeaderStyle>
															</asp:BoundColumn>
															<asp:BoundColumn DataField="MobilePersonal" HeaderText="Личен моб.">
																<HeaderStyle Width="120px"></HeaderStyle>
															</asp:BoundColumn>
															<asp:BoundColumn DataField="HomePhone" HeaderText="Дом. телефон">
																<HeaderStyle Width="120px"></HeaderStyle>
															</asp:BoundColumn>
															<asp:BoundColumn DataField="Mail" HeaderText="Ел.поща">
																<HeaderStyle Width="120px"></HeaderStyle>
																<ItemStyle Wrap="False"></ItemStyle>
															</asp:BoundColumn>
															<asp:BoundColumn DataField="RoleName" HeaderText="Роля в системата">
																<HeaderStyle Width="120px"></HeaderStyle>
																<ItemStyle ForeColor="DimGray"></ItemStyle>
															</asp:BoundColumn>
															<asp:TemplateColumn>
																<HeaderStyle Wrap="False" Width="20px"></HeaderStyle>
																<ItemStyle Wrap="False" HorizontalAlign="Center"></ItemStyle>
																<ItemTemplate>
																	<asp:ImageButton id="btnEdit" runat="server" ImageUrl="images/edit1.gif" ToolTip="Редактирай"></asp:ImageButton>
																</ItemTemplate>
															</asp:TemplateColumn>
															<asp:TemplateColumn>
																<HeaderStyle Wrap="False" Width="20px"></HeaderStyle>
																<ItemStyle Wrap="False" HorizontalAlign="Center"></ItemStyle>
																<ItemTemplate>
																	<asp:ImageButton id="btnDelete" runat="server" Width="12px" Height="12px" ToolTip="Изтрий" ImageUrl="images/delete.gif"></asp:ImageButton>
																</ItemTemplate>
															</asp:TemplateColumn>
														</Columns>
													</asp:datagrid></TD>
											</TR>
											<TR>
												<TD>
													<asp:Label id="lbSectionACC" EnableViewState="False" CssClass="enterDataLabel" Width="112px"
														Runat="server" Font-Bold="True">СЧЕТОВОДСТВО</asp:Label><P></P>
													<asp:datagrid id="grdUsersACC" runat="server" CssClass="Grid" Width="100%" AllowSorting="True"
														AutoGenerateColumns="False" CellPadding="4" PageSize="2">
														<AlternatingItemStyle CssClass="GridAltItem"></AlternatingItemStyle>
														<ItemStyle CssClass="GridItem"></ItemStyle>
														<HeaderStyle CssClass="GridHeader"></HeaderStyle>
														<Columns>
															<asp:TemplateColumn HeaderText="#">
																<ItemStyle ForeColor="DimGray" Width="10px"></ItemStyle>
																<ItemTemplate>
																	<asp:Label CssClass="menuTable" runat="server" Text='<%# (int)DataBinder.Eval(Container, "ItemIndex")+1 %>' ID="Label5" NAME="Label1">
																	</asp:Label>
																</ItemTemplate>
															</asp:TemplateColumn>
															<asp:TemplateColumn SortExpression="FirstName" HeaderText="Име">
																<ItemTemplate>
																	<asp:LinkButton id="lnk" runat="server" CssClass="menuTable" ForeColor="#804000" CommandName="Edit">
																		<span Class="menuTable">
																			<%# DataBinder.Eval(Container, "DataItem.FirstName") %>
																		</span>
																	</asp:LinkButton>
																</ItemTemplate>
															</asp:TemplateColumn>
															<asp:BoundColumn DataField="LastName" SortExpression="LastName" HeaderText="Фамилия">
																<HeaderStyle Width="100px"></HeaderStyle>
																<ItemStyle Wrap="False"></ItemStyle>
															</asp:BoundColumn>
															<asp:BoundColumn DataField="Ext" HeaderText="Вътр. телефон">
																<HeaderStyle Width="120px"></HeaderStyle>
															</asp:BoundColumn>
															<asp:BoundColumn DataField="Mobile" HeaderText="Моб. телефон">
																<HeaderStyle Width="120px"></HeaderStyle>
															</asp:BoundColumn>
															<asp:BoundColumn DataField="MobilePersonal" HeaderText="Личен моб.">
																<HeaderStyle Width="120px"></HeaderStyle>
															</asp:BoundColumn>
															<asp:BoundColumn DataField="HomePhone" HeaderText="Дом. телефон">
																<HeaderStyle Width="120px"></HeaderStyle>
															</asp:BoundColumn>
															<asp:BoundColumn DataField="Mail" HeaderText="Ел.поща">
																<HeaderStyle Width="120px"></HeaderStyle>
																<ItemStyle Wrap="False"></ItemStyle>
															</asp:BoundColumn>
															<asp:BoundColumn DataField="RoleName" HeaderText="Роля в системата">
																<HeaderStyle Width="120px"></HeaderStyle>
																<ItemStyle ForeColor="DimGray"></ItemStyle>
															</asp:BoundColumn>
															<asp:TemplateColumn>
																<HeaderStyle Wrap="False" Width="20px"></HeaderStyle>
																<ItemStyle Wrap="False" HorizontalAlign="Center"></ItemStyle>
																<ItemTemplate>
																	<asp:ImageButton id="btnEdit" runat="server" ImageUrl="images/edit1.gif" ToolTip="Редактирай"></asp:ImageButton>
																</ItemTemplate>
															</asp:TemplateColumn>
															<asp:TemplateColumn>
																<HeaderStyle Wrap="False" Width="20px"></HeaderStyle>
																<ItemStyle Wrap="False" HorizontalAlign="Center"></ItemStyle>
																<ItemTemplate>
																	<asp:ImageButton id="btnDelete" runat="server" Width="12px" Height="12px" ToolTip="Изтрий" ImageUrl="images/delete.gif"></asp:ImageButton>
																</ItemTemplate>
															</asp:TemplateColumn>
														</Columns>
													</asp:datagrid></TD>
											</TR>
											<TR>
												<TD>
													<asp:Label id="lbSectionSecretary" EnableViewState="False" CssClass="enterDataLabel" Width="112px"
														Runat="server" Font-Bold="True">АДМИНИСТРАЦИЯ</asp:Label><P></P>
													<asp:datagrid id="grdUsersSecretary" runat="server" CssClass="Grid" Width="100%" AllowSorting="True"
														AutoGenerateColumns="False" CellPadding="4" PageSize="2">
														<AlternatingItemStyle CssClass="GridAltItem"></AlternatingItemStyle>
														<ItemStyle CssClass="GridItem"></ItemStyle>
														<HeaderStyle CssClass="GridHeader"></HeaderStyle>
														<Columns>
															<asp:TemplateColumn HeaderText="#">
																<ItemStyle ForeColor="DimGray" Width="10px"></ItemStyle>
																<ItemTemplate>
																	<asp:Label CssClass="menuTable" runat="server" Text='<%# (int)DataBinder.Eval(Container, "ItemIndex")+1 %>' ID="Label6" NAME="Label1">
																	</asp:Label>
																</ItemTemplate>
															</asp:TemplateColumn>
															<asp:TemplateColumn SortExpression="FirstName" HeaderText="Име">
																<ItemTemplate>
																	<asp:LinkButton id="lnk" runat="server" CssClass="menuTable" ForeColor="#804000" CommandName="Edit">
																		<span Class="menuTable">
																			<%# DataBinder.Eval(Container, "DataItem.FirstName") %>
																		</span>
																	</asp:LinkButton>
																</ItemTemplate>
															</asp:TemplateColumn>
															<asp:BoundColumn DataField="LastName" SortExpression="LastName" HeaderText="Фамилия">
																<HeaderStyle Width="100px"></HeaderStyle>
																<ItemStyle Wrap="False"></ItemStyle>
															</asp:BoundColumn>
															<asp:BoundColumn DataField="Ext" HeaderText="Вътр. телефон">
																<HeaderStyle Width="120px"></HeaderStyle>
															</asp:BoundColumn>
															<asp:BoundColumn DataField="Mobile" HeaderText="Моб. телефон">
																<HeaderStyle Width="120px"></HeaderStyle>
															</asp:BoundColumn>
															<asp:BoundColumn DataField="MobilePersonal" HeaderText="Личен моб.">
																<HeaderStyle Width="120px"></HeaderStyle>
															</asp:BoundColumn>
															<asp:BoundColumn DataField="HomePhone" HeaderText="Дом. телефон">
																<HeaderStyle Width="120px"></HeaderStyle>
															</asp:BoundColumn>
															<asp:BoundColumn DataField="Mail" HeaderText="Ел.поща">
																<HeaderStyle Width="120px"></HeaderStyle>
																<ItemStyle Wrap="False"></ItemStyle>
															</asp:BoundColumn>
															<asp:BoundColumn DataField="RoleName" HeaderText="Роля в системата">
																<HeaderStyle Width="120px"></HeaderStyle>
																<ItemStyle ForeColor="DimGray"></ItemStyle>
															</asp:BoundColumn>
															<asp:TemplateColumn>
																<HeaderStyle Wrap="False" Width="20px"></HeaderStyle>
																<ItemStyle Wrap="False" HorizontalAlign="Center"></ItemStyle>
																<ItemTemplate>
																	<asp:ImageButton id="btnEdit" runat="server" ImageUrl="images/edit1.gif" ToolTip="Редактирай"></asp:ImageButton>
																</ItemTemplate>
															</asp:TemplateColumn>
															<asp:TemplateColumn>
																<HeaderStyle Wrap="False" Width="20px"></HeaderStyle>
																<ItemStyle Wrap="False" HorizontalAlign="Center"></ItemStyle>
																<ItemTemplate>
																	<asp:ImageButton id="btnDelete" runat="server" Width="12px" Height="12px" ToolTip="Изтрий" ImageUrl="images/delete.gif"></asp:ImageButton>
																</ItemTemplate>
															</asp:TemplateColumn>
														</Columns>
													</asp:datagrid></TD>
											</TR>
											<TR>
												<TD>
													<asp:Label id="lbSectionCleaner" EnableViewState="False" CssClass="enterDataLabel" Width="100px"
														Runat="server" Font-Bold="True">ХИГИЕНИСКА</asp:Label><P></P>
													<asp:datagrid id="grdUsersCleaner" runat="server" CssClass="Grid" Width="100%" AllowSorting="True"
														AutoGenerateColumns="False" CellPadding="4" PageSize="2">
														<AlternatingItemStyle CssClass="GridAltItem"></AlternatingItemStyle>
														<ItemStyle CssClass="GridItem"></ItemStyle>
														<HeaderStyle CssClass="GridHeader"></HeaderStyle>
														<Columns>
															<asp:TemplateColumn HeaderText="#">
																<ItemStyle ForeColor="DimGray" Width="10px"></ItemStyle>
																<ItemTemplate>
																	<asp:Label CssClass="menuTable" runat="server" Text='<%# (int)DataBinder.Eval(Container, "ItemIndex")+1 %>' ID="Label7" NAME="Label1">
																	</asp:Label>
																</ItemTemplate>
															</asp:TemplateColumn>
															<asp:TemplateColumn SortExpression="FirstName" HeaderText="Име">
																<ItemTemplate>
																	<asp:LinkButton id="lnk" runat="server" CssClass="menuTable" ForeColor="#804000" CommandName="Edit">
																		<span Class="menuTable">
																			<%# DataBinder.Eval(Container, "DataItem.FirstName") %>
																		</span>
																	</asp:LinkButton>
																</ItemTemplate>
															</asp:TemplateColumn>
															<asp:BoundColumn DataField="LastName" SortExpression="LastName" HeaderText="Фамилия">
																<HeaderStyle Width="100px"></HeaderStyle>
																<ItemStyle Wrap="False"></ItemStyle>
															</asp:BoundColumn>
															<asp:BoundColumn DataField="Ext" HeaderText="Вътр. телефон">
																<HeaderStyle Width="120px"></HeaderStyle>
															</asp:BoundColumn>
															<asp:BoundColumn DataField="Mobile" HeaderText="Моб. телефон">
																<HeaderStyle Width="120px"></HeaderStyle>
															</asp:BoundColumn>
															<asp:BoundColumn DataField="MobilePersonal" HeaderText="Личен моб.">
																<HeaderStyle Width="120px"></HeaderStyle>
															</asp:BoundColumn>
															<asp:BoundColumn DataField="HomePhone" HeaderText="Дом. телефон">
																<HeaderStyle Width="120px"></HeaderStyle>
															</asp:BoundColumn>
															<asp:BoundColumn DataField="Mail" HeaderText="Ел.поща">
																<HeaderStyle Width="120px"></HeaderStyle>
																<ItemStyle Wrap="False"></ItemStyle>
															</asp:BoundColumn>
															<asp:BoundColumn DataField="RoleName" HeaderText="Роля в системата">
																<HeaderStyle Width="120px"></HeaderStyle>
																<ItemStyle ForeColor="DimGray"></ItemStyle>
															</asp:BoundColumn>
															<asp:TemplateColumn>
																<HeaderStyle Wrap="False" Width="20px"></HeaderStyle>
																<ItemStyle Wrap="False" HorizontalAlign="Center"></ItemStyle>
																<ItemTemplate>
																	<asp:ImageButton id="btnEdit" runat="server" ImageUrl="images/edit1.gif" ToolTip="Редактирай"></asp:ImageButton>
																</ItemTemplate>
															</asp:TemplateColumn>
															<asp:TemplateColumn>
																<HeaderStyle Wrap="False" Width="20px"></HeaderStyle>
																<ItemStyle Wrap="False" HorizontalAlign="Center"></ItemStyle>
																<ItemTemplate>
																	<asp:ImageButton id="btnDelete" runat="server" Width="12px" Height="12px" ToolTip="Изтрий" ImageUrl="images/delete.gif"></asp:ImageButton>
																</ItemTemplate>
															</asp:TemplateColumn>
														</Columns>
													</asp:datagrid></TD>
											</TR>
											<TR>
												<TD>
													<asp:Label id="lbSectionLowers" EnableViewState="False" CssClass="enterDataLabel" Width="150px"
														Runat="server" Font-Bold="True">АСОЦИИРАН ЮРИСТ</asp:Label><P></P>
													<asp:datagrid id="grdUsersLowers" runat="server" CssClass="Grid" Width="100%" AllowSorting="True"
														AutoGenerateColumns="False" CellPadding="4" PageSize="2">
														<AlternatingItemStyle CssClass="GridAltItem"></AlternatingItemStyle>
														<ItemStyle CssClass="GridItem"></ItemStyle>
														<HeaderStyle CssClass="GridHeader"></HeaderStyle>
														<Columns>
															<asp:TemplateColumn HeaderText="#">
																<ItemStyle ForeColor="DimGray" Width="10px"></ItemStyle>
																<ItemTemplate>
																	<asp:Label CssClass="menuTable" runat="server" Text='<%# (int)DataBinder.Eval(Container, "ItemIndex")+1 %>' ID="Label8" NAME="Label1">
																	</asp:Label>
																</ItemTemplate>
															</asp:TemplateColumn>
															<asp:TemplateColumn SortExpression="FirstName" HeaderText="Име">
																<ItemTemplate>
																	<asp:LinkButton id="lnk" runat="server" CssClass="menuTable" ForeColor="#804000" CommandName="Edit">
																		<span Class="menuTable">
																			<%# DataBinder.Eval(Container, "DataItem.FirstName") %>
																		</span>
																	</asp:LinkButton>
																</ItemTemplate>
															</asp:TemplateColumn>
															<asp:BoundColumn DataField="LastName" SortExpression="LastName" HeaderText="Фамилия">
																<HeaderStyle Width="100px"></HeaderStyle>
																<ItemStyle Wrap="False"></ItemStyle>
															</asp:BoundColumn>
															<asp:BoundColumn DataField="Ext" HeaderText="Вътр. телефон">
																<HeaderStyle Width="120px"></HeaderStyle>
															</asp:BoundColumn>
															<asp:BoundColumn DataField="Mobile" HeaderText="Моб. телефон">
																<HeaderStyle Width="120px"></HeaderStyle>
															</asp:BoundColumn>
															<asp:BoundColumn DataField="MobilePersonal" HeaderText="Личен моб.">
																<HeaderStyle Width="120px"></HeaderStyle>
															</asp:BoundColumn>
															<asp:BoundColumn DataField="HomePhone" HeaderText="Дом. телефон">
																<HeaderStyle Width="120px"></HeaderStyle>
															</asp:BoundColumn>
															<asp:BoundColumn DataField="Mail" HeaderText="Ел.поща">
																<HeaderStyle Width="120px"></HeaderStyle>
																<ItemStyle Wrap="False"></ItemStyle>
															</asp:BoundColumn>
															<asp:BoundColumn DataField="RoleName" HeaderText="Роля в системата">
																<HeaderStyle Width="120px"></HeaderStyle>
																<ItemStyle ForeColor="DimGray"></ItemStyle>
															</asp:BoundColumn>
															<asp:TemplateColumn>
																<HeaderStyle Wrap="False" Width="20px"></HeaderStyle>
																<ItemStyle Wrap="False" HorizontalAlign="Center"></ItemStyle>
																<ItemTemplate>
																	<asp:ImageButton id="btnEdit" runat="server" ImageUrl="images/edit1.gif" ToolTip="Редактирай"></asp:ImageButton>
																</ItemTemplate>
															</asp:TemplateColumn>
															<asp:TemplateColumn>
																<HeaderStyle Wrap="False" Width="20px"></HeaderStyle>
																<ItemStyle Wrap="False" HorizontalAlign="Center"></ItemStyle>
																<ItemTemplate>
																	<asp:ImageButton id="btnDelete" runat="server" Width="12px" Height="12px" ToolTip="Изтрий" ImageUrl="images/delete.gif"></asp:ImageButton>
																</ItemTemplate>
															</asp:TemplateColumn>
														</Columns>
													</asp:datagrid></TD>
											</TR>
										</TABLE>
									</asp:panel>
									<TABLE id="Table4" style="BORDER-RIGHT: tan thin solid; BORDER-TOP: tan thin solid; BORDER-LEFT: tan thin solid; BORDER-BOTTOM: tan thin solid"
										cellSpacing="0" cellPadding="4" width="100%" border="0">
										<TR>
											<TD><asp:button id="btnNewUser" tabIndex="1" runat="server" CssClass="ActionButton"></asp:button>&nbsp;
												<asp:button id="btnExport" runat="server" CssClass="ActionButton" Text="Експорт"></asp:button>&nbsp;</TD>
										</TR>
									</TABLE>
								</td>
							</tr>
						</table>
					</td>
				</tr>
			</table>
			<radg:radgrid id="gridCalls" runat="server" CssClass="RadGrid" Width="100%" AutoGenerateColumns="False"
				GridLines="Horizontal">
				<PAGERSTYLE CssClass="GridHeader" Mode="NumericPages"></PAGERSTYLE>
				<ITEMSTYLE CssClass="GridItem" HorizontalAlign="Center"></ITEMSTYLE>
				<GROUPPANEL Visible="False"></GROUPPANEL>
				<HEADERSTYLE CssClass="GridHeader" HorizontalAlign="Center" Wrap="False"></HEADERSTYLE>
				<ALTERNATINGITEMSTYLE CssClass="GridItem" HorizontalAlign="Center"></ALTERNATINGITEMSTYLE>
				<GROUPHEADERITEMSTYLE BackColor="Silver" BorderColor="Black"></GROUPHEADERITEMSTYLE>
				<MASTERTABLEVIEW Visible="True" AllowSorting="True" GridLines="Horizontal" AllowPaging="False" PageSize="15"
					AllowCustomPaging="False" DataSourcePersistenceMode="NoPersistence">
					<COLUMNS>
						<RADG:GRIDBOUNDCOLUMN DataField="FirstName" HeaderText="Име" HeaderButtonType="TextButton" UniqueName="FirstName"></RADG:GRIDBOUNDCOLUMN>
						<RADG:GRIDBOUNDCOLUMN DataField="LastName" HeaderText="Фамилия" HeaderButtonType="TextButton" UniqueName="LastName"></RADG:GRIDBOUNDCOLUMN>
						<RADG:GRIDBOUNDCOLUMN DataField="Account" HeaderText="Акаунт" HeaderButtonType="TextButton" UniqueName="Account"></RADG:GRIDBOUNDCOLUMN>
						<RADG:GRIDBOUNDCOLUMN DataField="Mail" HeaderText="Ел.поща" HeaderButtonType="TextButton" UniqueName="Mail"></RADG:GRIDBOUNDCOLUMN>
						<RADG:GRIDBOUNDCOLUMN DataField="RoleName" HeaderText="Роля в системата" HeaderButtonType="TextButton"
							UniqueName="RoleName"></RADG:GRIDBOUNDCOLUMN>
						<RADG:GRIDBOUNDCOLUMN DataField="Occupation" HeaderText="Длъжност" HeaderButtonType="TextButton" UniqueName="Occupation"></RADG:GRIDBOUNDCOLUMN>
						<RADG:GRIDBOUNDCOLUMN DataField="UserRole" HeaderText="Част" HeaderButtonType="TextButton" UniqueName="UserRole"></RADG:GRIDBOUNDCOLUMN>
						<RADG:GRIDBOUNDCOLUMN DataField="Ext" HeaderText="Вътр. телефон" HeaderButtonType="TextButton" UniqueName="Ext"></RADG:GRIDBOUNDCOLUMN>
						<RADG:GRIDBOUNDCOLUMN DataField="Mobile" HeaderText="Моб. телефон" HeaderButtonType="TextButton" UniqueName="Mobile"></RADG:GRIDBOUNDCOLUMN>
						<RADG:GRIDBOUNDCOLUMN DataField="HomePhone" HeaderText="Дом. телефон" HeaderButtonType="TextButton" UniqueName="HomePhone"></RADG:GRIDBOUNDCOLUMN>
						<RADG:GRIDBOUNDCOLUMN DataField="CV" HeaderText="Кратко CV" HeaderButtonType="TextButton" UniqueName="CV"></RADG:GRIDBOUNDCOLUMN>
					</COLUMNS>
					<ROWINDICATORCOLUMN Visible="False" UniqueName="RowIndicator">
						<HEADERSTYLE Width="20px"></HEADERSTYLE>
					</ROWINDICATORCOLUMN>
					<EDITFORMSETTINGS>
						<EDITCOLUMN UniqueName="EditCommandColumn"></EDITCOLUMN>
					</EDITFORMSETTINGS>
					<EXPANDCOLLAPSECOLUMN Visible="False" UniqueName="ExpandColumn" ButtonType="ImageButton">
						<HEADERSTYLE Width="19px"></HEADERSTYLE>
					</EXPANDCOLLAPSECOLUMN>
				</MASTERTABLEVIEW>
			</radg:radgrid></form>
	</body>
</HTML>
