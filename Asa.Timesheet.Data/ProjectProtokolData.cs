using System;
using System.Collections;
using System.Text;

namespace Asa.Timesheet.Data
{
	public class ProjectProtokolData
	{
		private int _projectProtokolID = -1;
		private int _protokolID = -1;
		private bool _isDeleted;

		/// <summary>
		/// Initialize an new empty Login object.
		/// </summary>
		public ProjectProtokolData()
		{
		}
		
		/// <summary>
		/// Initialize a new Login object with the given parameters.
		/// </summary>
		public ProjectProtokolData(int projectProtokolID, int protokolID, bool isDeleted)
		{
			this._projectProtokolID = projectProtokolID;
			this._protokolID = protokolID;
			this._isDeleted = isDeleted;
		}	
		
	  
		public override string ToString()
		{
			StringBuilder builder = new StringBuilder();
			builder.Append("_projectProtokolID: " + _projectProtokolID.ToString()).Append("\r\n");
			builder.Append("_protokolID: " + _protokolID.ToString()).Append("\r\n");
			builder.Append("_isDeleted: " + _isDeleted.ToString()).Append("\r\n");
			builder.Append("\r\n");
			return builder.ToString();
		}	
		public object GetByIndex(int nIndex)
		{
			switch(nIndex)
			{
				case 0:
					return _projectProtokolID;
				case 1:
					return _protokolID;
				case 2:
					return _isDeleted;
			}
			return null;
		}
		#region Properties
	
		public int ProjectProtokolID
		{
			get { return _projectProtokolID; }
			set { _projectProtokolID = value; }
		}
	
		public int ProtokolID
		{
			get { return _protokolID; }
			set { _protokolID = value; }
		}
	
		public bool IsDeleted
		{
			get { return _isDeleted; }
			set { _isDeleted = value; }
		}
	
		#endregion
	}
}


