using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Data.SqlClient;
using Asa.Timesheet.Data;
using log4net;
using System.Text;
using Asa.Timesheet.Data.Entities;
namespace Asa.Timesheet.WebPages
{
	/// <summary>
	/// Summary description for CopyHours.
	/// </summary>
	public class Meeting : TimesheetPageBase
	{
		protected System.Web.UI.WebControls.ListBox lbSelectedUsers;
		protected System.Web.UI.WebControls.ListBox lbUsers;
		protected System.Web.UI.HtmlControls.HtmlInputText hdnlbUsers;
		protected System.Web.UI.HtmlControls.HtmlInputText hdnlbSelectedUsers;
		protected System.Web.UI.WebControls.Button btnSave;
		protected System.Web.UI.WebControls.Button btnCancel;
		protected System.Web.UI.WebControls.Label lblTitle;
		protected System.Web.UI.WebControls.Label lblSaveInfo;
		protected System.Web.UI.WebControls.Label Label1;
		protected System.Web.UI.WebControls.Label Label2;
		protected System.Web.UI.WebControls.ListBox lbSelectedClients;
		protected System.Web.UI.WebControls.ListBox lbClients;
		protected System.Web.UI.HtmlControls.HtmlInputText hdnlbClients;
		protected System.Web.UI.HtmlControls.HtmlInputText hdnlbSelectedClients;
		protected System.Web.UI.HtmlControls.HtmlInputText hdn1lbSelectedUsers;
		protected System.Web.UI.HtmlControls.HtmlInputText hdn1lbSelectedClients;
		protected System.Web.UI.WebControls.Label lbError;
        private static readonly ILog log = LogManager.GetLogger(typeof(Meeting));


        private void LoadData()
        {
            LoadSubcontracters();
            BindGrid();


            btnCancel.Attributes["onclick"] = "CancelClick(); return false;";
            //if(!IsPostBack)
            {

                if (Session["Meeting"] is WorkTimeInfo)
                {
                    WorkTimeInfo wti = (WorkTimeInfo)Session["Meeting"];
                    MeetingData md = MeetingDAL.Load(wti.WorkDate, wti.ProjectID, wti.ActivityID, wti.StartTimeID, wti.EndTimeID);
                    if (md != null)
                    {
                        lbSelectedClients.Items.Clear();
                        lbSelectedUsers.Items.Clear();
                        UIHelpers.SetSelected(md.Clients, lbClients, lbSelectedClients);
                        UIHelpers.SetSelected(md.Subcontracters, lbUsers, lbSelectedUsers);
                    }
                }
                else
                    lbError.Visible = true;
            }
            lbUsers.Attributes["ondblclick"] = "UserDblClick();";
            lbSelectedUsers.Attributes["ondblclick"] = "UserDblClick();";
            lbSelectedClients.Attributes["ondblclick"] = "UserDblClick(1);";
            lbClients.Attributes["ondblclick"] = "UserDblClick1();";
        }
        private void Page_Load(object sender, System.EventArgs e)
        {
            LoadData();
        }

		#region Web Form Designer generated code
        override protected void OnInit(EventArgs e)
        {
            //
            // CODEGEN: This call is required by the ASP.NET Web Form Designer.
            //
            InitializeComponent();
            base.OnInit(e);
        }
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
        private void InitializeComponent()
        {
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            this.Load += new System.EventHandler(this.Page_Load);

        }
		#endregion

        private void LoadSubcontracters()
        {



            lbUsers.DataSource = SubcontracterDAL.LoadCollection();
            lbUsers.DataValueField = "SubcontracterID";
            lbUsers.DataTextField = "SubcontracterName";
            lbUsers.DataBind();


            lbClients.DataSource = SubcontracterDAL.LoadCollection();
            lbClients.DataValueField = "SubcontracterID";
            lbClients.DataTextField = "SubcontracterName";
            lbClients.DataBind();

        }
        private void BindGrid()
        {
            SqlDataReader reader = null;

            try
            {
                reader = ClientsData.SelectClients(SortOrder, (int)ClientTypes.Client, false);
                if (reader != null)
                {
                    lbClients.DataSource = reader;
                    lbClients.DataValueField = "ClientID";
                    lbClients.DataTextField = "ClientName";
                    lbClients.DataBind();

                }
            }
            catch (Exception ex)
            {
                log.Error(ex);

                return;
            }

            finally
            {
                if (reader != null) reader.Close();
            }



        }

        private void btnSave_Click(object sender, System.EventArgs e)
        {
            if (Session["Meeting"] != null && Session["Meeting"] is WorkTimeInfo)
            {
                WorkTimeInfo wi = (WorkTimeInfo)Session["Meeting"];
                StringBuilder sbSub = new StringBuilder();
                int n = -1;
                MeetingData mdOld = MeetingDAL.Load(wi.WorkDate, wi.ProjectID, wi.ActivityID, wi.StartTimeID, wi.EndTimeID);
                if (mdOld != null)
                    n = mdOld.MeetingID;
                MeetingData md = new MeetingData(n, wi.WorkDate, wi.ProjectID,
                    wi.ActivityID, wi.StartTimeID, wi.EndTimeID, hdn1lbSelectedClients.Value, hdn1lbSelectedUsers.Value, LoggedUser.UserID.ToString(), LoggedUser.UserID, false, -1);
                MeetingDAL.Save(md);
                LoadData();
            }
        }
   
	}
}
