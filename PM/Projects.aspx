﻿<%@ Page Title="Проекти" Language="C#" MasterPageFile="~/PM.Master" AutoEventWireup="True" CodeBehind="Projects.aspx.cs" Inherits="PM.Projects" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
  
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">


<table height="100%" cellSpacing="0" cellPadding="0" width="100%" border="0">
	<tr>
		<td >

				<asp:label id="lblError" runat="server" ForeColor="Red" EnableViewState="False" CssClass="ErrorLabel"></asp:label><asp:label id="lblInfo" runat="server" EnableViewState="False" CssClass="InfoLabel"></asp:label><BR>
				<TABLE id="Table1"  width="864" border="0">
					<TR>

						<td><asp:textbox id="txtProject" onkeydown="search()" runat="server" CssClass="enterDataBox" Width="220px"
								MaxLength="30"></asp:textbox>
                        <td>    
                            <asp:dropdownlist id="ddlSearch" runat="server" CssClass="enterDataBox" Width="220px" AutoPostBack="True"></asp:dropdownlist></td>
                        <td>    
                            <asp:dropdownlist id="ddlBuildingTypes" runat="server" CssClass="enterDataBox" Width="220px" AutoPostBack="True"></asp:dropdownlist></TD>
                        <td><asp:button id="btnSearch" runat="server" CssClass="ActionButton" Width="80px"></asp:button>&nbsp;<asp:button id="btnClear" runat="server" CssClass="ActionButton" Width="80px"></asp:button></td>
					</TR>
				</TABLE>

			<asp:panel id="grid" 
				runat="server" Width="98%" Height="450px">
				<asp:datagrid id="grdProjects" runat="server" CssClass="Grid" Width="100%" AllowSorting="True"
					PageSize="2" CellPadding="4" AutoGenerateColumns="False">
					<ItemStyle CssClass="GridItem"></ItemStyle>
					<HeaderStyle Wrap="False" CssClass="GridHeader"></HeaderStyle>
					<Columns>
						<asp:TemplateColumn HeaderText="#">
							<ItemStyle Width="10px" ForeColor="DimGray"></ItemStyle>
							<ItemTemplate>
								<asp:Label CssClass="menuTable" runat="server" Text='<%# (int)DataBinder.Eval(Container, "ItemIndex")+1 %>' ID="Label1" NAME="Label1">
								</asp:Label>
							</ItemTemplate>
						</asp:TemplateColumn>
						<asp:TemplateColumn SortExpression="Name" ItemStyle-Width="20%">
							<ItemTemplate>
								<asp:LinkButton id="lnk" runat="server" CssClass="menuTable" ForeColor="#804000" CommandName="Edit">
									<span Class="menuTable">
										<%# DataBinder.Eval(Container, "DataItem.ProjectName") %>
									</span>
								</asp:LinkButton>
							</ItemTemplate>
						</asp:TemplateColumn>
						<asp:TemplateColumn SortExpression="Code">
							<ItemStyle Width="10px"></ItemStyle>
							<ItemTemplate>
								<asp:Label ForeColor='<%# GetForeColor(DataBinder.Eval(Container, "DataItem.ProjectColor") )%>' BackColor='<%# GetColor(DataBinder.Eval(Container, "DataItem.ProjectColor") )%>' CssClass="menuTable" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.ProjectCode") %>' ID="Label2" >
								</asp:Label>
							</ItemTemplate>
						</asp:TemplateColumn>
						<asp:BoundColumn DataField="FullName" SortExpression="Leader">
							<HeaderStyle></HeaderStyle>
						</asp:BoundColumn>
						<asp:BoundColumn DataField="BuildingType" SortExpression="BuildingType">
							<HeaderStyle Width="20%"></HeaderStyle>
							<ItemStyle ForeColor="DimGray"></ItemStyle>
						</asp:BoundColumn>
						<asp:BoundColumn DataField="IsActive" Visible="False"></asp:BoundColumn>
						<asp:BoundColumn DataField="Black" Visible="False"></asp:BoundColumn>
						<asp:BoundColumn DataField="Concluded" Visible="False"></asp:BoundColumn>
					</Columns>
				</asp:datagrid>
			</asp:panel>
 </asp:Content>
