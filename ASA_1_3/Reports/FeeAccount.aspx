﻿<%@ Register TagPrefix="uc1" TagName="PageHeader" Src="../UserControls/PageHeader.ascx" %>
<%@ Register TagPrefix="activereportsweb" Namespace="GrapeCity.ActiveReports.Web" Assembly="GrapeCity.ActiveReports.Web.v7, Version=7.1.7470.0, Culture=neutral, PublicKeyToken=cc4967777c49a3ff" %>
<%@ Page language="c#" Codebehind="FeeAccount.aspx.cs" AutoEventWireup="false" Inherits="Asa.Timesheet.WebPages.Reports.FeeAccount"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
	<head runat="server">
		<title>Fee Account</title>
		<meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
		<meta content="C#" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<LINK href="../styles/timesheet.css" type="text/css" rel="stylesheet">
	</HEAD>
	<body bottomMargin="0" leftMargin="0" topMargin="0" rightMargin="0">
		<form id="Form1" method="post" runat="server">
			<table height="100%" cellSpacing="0" cellPadding="0" width="100%" border="0">
				<tr height="1">
					<td><uc1:pageheader id="header" runat="server"></uc1:pageheader></td>
				</tr>
				<tr style="DISPLAY: none" height="80">
					<td background="../images/copy1.gif">
						<TABLE id="Table2" height="100%" width="100%">
							<TR>
								<td style="WIDTH: 106px">&nbsp;&nbsp;&nbsp;&nbsp; <IMG height="80" alt="" src="../images/logo.jpg" width="64"></TD>
								<td noWrap><asp:label id="lblTitle" runat="server" Font-Size="X-Small" Font-Bold="True"></asp:label><br>
									<asp:label id="lblUser" runat="server" Font-Size="Medium" Font-Bold="True" ForeColor="SaddleBrown"></asp:label></TD>
							</TR>
						</TABLE>
					</td>
				</tr>
				<tr>
					<td>
						<table height="100%" cellSpacing="0" cellPadding="0" width="100%" border="0">
							<tr>
								<td class="menuLeft"><asp:placeholder id="menuHolder" runat="server"></asp:placeholder></td>
								
								<td style="PADDING-LEFT: 5px; PADDING-RIGHT: 5px; PADDING-TOP: 3px" vAlign="top" noWrap>
									<TABLE id="tblInfo" height="30" cellSpacing="0" cellPadding="3" width="880" border="0">
										<TR>
											<td vAlign="middle" noWrap><asp:label id="lblError" runat="server" ForeColor="Red" EnableViewState="False" CssClass="ErrorLabel"></asp:label><asp:label id="lblInfo" runat="server" EnableViewState="False" CssClass="InfoLabel"></asp:label></TD>
										</TR>
									</TABLE>
									<TABLE id="Table3" cellSpacing="0" cellPadding="0" width="880" border="0">
										<TR>
											<td style="HEIGHT: 5px" colSpan="2"></TD>
										</TR>
										<TR style="DISPLAY: none">
											<td colSpan="2"><IMG height="1" src="../images/dot.gif" width="100%"></TD>
										</TR>
										<TR>
											<td colSpan="2" height="3"><asp:checkbox id="ckDates" runat="server" CssClass="EnterDataLabel" Width="144px" Text="За целия период"
													Checked="True" AutoPostBack="True"></asp:checkbox></td>
										</TR>
										<TR>
											<td>
												<TABLE id="Table4" cellSpacing="0" cellPadding="3" border="0">
													<TR id="SelectDateRow">
														<td style="WIDTH: 139px; HEIGHT: 4px" vAlign="top" align="right"><asp:label id="lblStartDay" runat="server" CssClass="EnterDataLabel">Ден, седмица, нач.дата:</asp:label><asp:label id="lblSelStartDate" runat="server" Font-Bold="True" ForeColor="SaddleBrown" Width="100px"></asp:label></TD>
														<td style="WIDTH: 72px" vAlign="top">
															<TABLE id="Table1" cellSpacing="0" cellPadding="0" border="0">
																<TR>
																	<td><asp:calendar id="calStartDate" runat="server" ForeColor="#222222" Width="240px" SelectionMode="DayWeek"
																			BorderColor="#999999" CellPadding="0" BorderWidth="1px" BackColor="White" Height="240px" NextMonthText=""
																			PrevMonthText="" DayNameFormat="FirstLetter" SelectWeekText="+"
																			FirstDayOfWeek="Monday" BorderStyle="Solid" CssClass="aspCalendar" NextPrevFormat="ShortMonth" Font-Bold="True" Font-Size="11pt">
																			<TodayDayStyle ForeColor="#C73B2E" BackColor="White" BorderColor="#C73B2E" 
                                                                                BorderStyle="Solid" BorderWidth="2px" Height="32px" Width="32px"></TodayDayStyle>
																			<SelectorStyle Font-Size="10pt" Font-Names="Verdana" ForeColor="SaddleBrown" 
                                                                                BorderStyle="None"></SelectorStyle>
																			<DayStyle Font-Size="10pt" Font-Bold="False" BorderStyle="Dotted" 
                                                                                BorderColor="#CCCCCC" BorderWidth="1px" Height="30px" Width="32px"></DayStyle>
																			<NextPrevStyle Font-Bold="True" Wrap="False" HorizontalAlign="Center" 
                                                                                Height="16px" BorderStyle="None" VerticalAlign="Middle" Font-Size="9pt" 
                                                                                Font-Underline="True"></NextPrevStyle>
																			<DayHeaderStyle Font-Size="9pt" Height="1px" ForeColor="SaddleBrown"></DayHeaderStyle>
																			<SelectedDayStyle Font-Bold="True" ForeColor="#C73B2E" BorderColor="#C73B2E" 
                                                                                BackColor="White" BorderStyle="Solid" BorderWidth="2px"
                                                                                Font-Size="11pt" Height="32px" Width="32px"></SelectedDayStyle>
																			<TitleStyle Font-Size="13pt" Font-Bold="True" ForeColor="#C73B2E" 
                                                                                BackColor="White" HorizontalAlign="Center"></TitleStyle>
																			<OtherMonthDayStyle ForeColor="Gray" BackColor="#FDFDFD" Font-Size="6pt"></OtherMonthDayStyle>
																		    <WeekendDayStyle Font-Bold="True" Font-Italic="False" 
                                                                                ForeColor="#C73B2E" />
																		</asp:calendar></TD>
																	<td vAlign="top" noWrap></TD>
																</TR>
															</TABLE>
														</TD>
														<td style="WIDTH: 117px" vAlign="top" align="right"><asp:label id="lblEndDay" runat="server" CssClass="EnterDataLabel">Крайна дата:</asp:label><asp:label id="lblSelEndDate" runat="server" Font-Bold="True" ForeColor="SaddleBrown" Width="100px"></asp:label></TD>
														<td style="WIDTH: 278px; HEIGHT: 4px" vAlign="top">
															<TABLE id="Table6" cellSpacing="0" cellPadding="0" border="0">
																<TR>
																	<td><asp:calendar id="calEndDate" runat="server" ForeColor="#222222" Width="240px" SelectionMode="DayWeek"
																			BorderColor="#999999" CellPadding="0" BorderWidth="1px" BackColor="White" Height="240px" NextMonthText=""
																			PrevMonthText="" DayNameFormat="FirstLetter" SelectWeekText="+"
																			FirstDayOfWeek="Monday" BorderStyle="Solid" CssClass="aspCalendar" NextPrevFormat="ShortMonth" Font-Bold="True" Font-Size="11pt">
																			<TodayDayStyle ForeColor="#C73B2E" BackColor="White" BorderColor="#C73B2E" 
                                                                                BorderStyle="Solid" BorderWidth="2px" Height="32px" Width="32px"></TodayDayStyle>
																			<SelectorStyle Font-Size="10pt" Font-Names="Verdana" ForeColor="SaddleBrown" 
                                                                                BorderStyle="None"></SelectorStyle>
																			<DayStyle Font-Size="10pt" Font-Bold="False" BorderStyle="Dotted" 
                                                                                BorderColor="#CCCCCC" BorderWidth="1px" Height="30px" Width="32px"></DayStyle>
																			<NextPrevStyle Font-Bold="True" Wrap="False" HorizontalAlign="Center" 
                                                                                Height="16px" BorderStyle="None" VerticalAlign="Middle" Font-Size="9pt" 
                                                                                Font-Underline="True"></NextPrevStyle>
																			<DayHeaderStyle Font-Size="9pt" Height="1px" ForeColor="SaddleBrown"></DayHeaderStyle>
																			<SelectedDayStyle Font-Bold="True" ForeColor="#C73B2E" BorderColor="#C73B2E" 
                                                                                BackColor="White" BorderStyle="Solid" BorderWidth="2px"
                                                                                Font-Size="11pt" Height="32px" Width="32px"></SelectedDayStyle>
																			<TitleStyle Font-Size="13pt" Font-Bold="True" ForeColor="#C73B2E" 
                                                                                BackColor="White" HorizontalAlign="Center"></TitleStyle>
																			<OtherMonthDayStyle ForeColor="Gray" BackColor="#FDFDFD" Font-Size="6pt"></OtherMonthDayStyle>
																		    <WeekendDayStyle Font-Bold="True" Font-Italic="False" 
                                                                                ForeColor="#C73B2E" />
																		</asp:calendar></TD>
																	<td vAlign="top" noWrap></TD>
																</TR>
															</TABLE>
														</TD>
													</TR>
												</TABLE>
											</TD>
										</TR>
										<TR>
											<td style="HEIGHT: 3px" colSpan="2"></TD>
										</TR>
										<TR>
											<td colSpan="2"><IMG height="1" src="../images/dot.gif" width="100%"></TD>
										</TR>
										<TR>
											<td colSpan="2" height="3"></td>
										</TR>
										<tr>
											<td>
												<table cellSpacing="0" cellPadding="3" border="0">
													<TR>
														<td style="WIDTH: 139px" vAlign="top"><asp:label id="Label1" runat="server" CssClass="enterDataLabel" Width="100%">Тип:</asp:label></TD>
														<td style="WIDTH: 243px" vAlign="top"><asp:dropdownlist id="ddlProjectsStatus" runat="server" CssClass="EnterDataBox" Width="250px" AutoPostBack="True"></asp:dropdownlist></TD>
														<td vAlign="top"></TD>
														<td></TD>
														<td></TD>
														<td style="WIDTH: 8px"></TD>
														<td style="WIDTH: 8px"></TD>
													</TR>
													<TR>
														<td style="WIDTH: 139px" vAlign="top"><asp:label id="Label7" runat="server" CssClass="enterDataLabel" Width="100%">Сграда:</asp:label></TD>
														<td style="WIDTH: 243px" vAlign="top"><asp:dropdownlist id="ddlBuildingTypes" runat="server" CssClass="enterDataBox" Width="250px" AutoPostBack="True"></asp:dropdownlist></TD>
														<td vAlign="top"></TD>
														<td></TD>
														<td></TD>
														<td style="WIDTH: 8px"></TD>
														<td style="WIDTH: 8px"></TD>
													</TR>
													<TR>
														<td style="WIDTH: 139px" vAlign="top"><asp:label id="Label2" runat="server" CssClass="enterDataLabel" Width="100%">Проект:</asp:label></TD>
														<td style="WIDTH: 243px" vAlign="top"><asp:dropdownlist id="ddlProjects" runat="server" CssClass="EnterDataBox" Width="250px"></asp:dropdownlist></TD>
														<td vAlign="top"></TD>
														<td></TD>
														<td><asp:imagebutton id="ibGenerate" runat="server" ImageUrl="../images/ie.gif"></asp:imagebutton></TD>
														<td style="WIDTH: 8px"><asp:imagebutton id="ibPdfExport" runat="server" ImageUrl="../images/pdf1.gif" ToolTip="Справка в PDF формат"></asp:imagebutton></TD>
														<td style="WIDTH: 8px"><asp:imagebutton id="ibXlsExport" runat="server" ImageUrl="../images/xls.gif" ToolTip="Справка в Excel формат"></asp:imagebutton></TD>
													</TR>
													<TR>
														<td style="WIDTH: 139px" vAlign="top" height="10"></TD>
														<td style="WIDTH: 243px" vAlign="top" height="10"></TD>
														<td vAlign="top" height="10"></TD>
														<td height="10"></TD>
														<td height="10"></TD>
														<td style="WIDTH: 8px" height="10"></TD>
														<td style="WIDTH: 8px" height="10"></TD>
													</TR>
												</table>
											</td>
										</tr>
										<TR>
											<td style="HEIGHT: 5px" colSpan="2"></TD>
										</TR>
										<TR>
											<td colSpan="2"><IMG height="1" src="../images/dot.gif" width="100%"></TD>
										</TR>
										<TR>
											<td colSpan="2" height="3"></TD>
										</TR>
										<TR>
											<td colSpan="2"><IMG height="1" src="images/dot.gif" width="100%"></TD>
										</TR>
									</TABLE>
									<P>&nbsp;</P>
									<TABLE id="Table5" cellSpacing="0" cellPadding="0" width="100%" border="0">
										<TR>
											<td align="center"><asp:label id="lblReportTitle" runat="server" Font-Size="11pt" Font-Bold="True" EnableViewState="False"
													Visible="False">ХОНОРАР СМЕТКА</asp:label></TD>
										</TR>
									</TABLE>
									<br>
									<TABLE id="tblHeader" cellSpacing="0" cellPadding="2" border="0" runat="server">
										<TR>
											<td vAlign="top"><asp:label id="Label5" runat="server" Font-Size="9pt" Font-Bold="True" Visible="false">ПРОЕКТ:</asp:label></TD>
											<td vAlign="top"><asp:label id="lblObekt" runat="server" Font-Size="9pt" Visible="false"></asp:label></TD>
										</TR>
									</TABLE>
									<br>
									<asp:datagrid id="grdReport" runat="server" ForeColor="DimGray" EnableViewState="False" CssClass="ReportGrid"
										Width="100%" CellPadding="4" AutoGenerateColumns="False" ShowHeader="True">
										<HeaderStyle CssClass="reportGridHeader"></HeaderStyle>
										<Columns>
											<asp:BoundColumn DataField="SubprojectID" Visible="false"></asp:BoundColumn>
											<asp:BoundColumn DataField="projectname" HeaderText="Описание"></asp:BoundColumn>
											<asp:BoundColumn DataField="ClientName" HeaderText="Клиент"></asp:BoundColumn>
											<asp:BoundColumn DataField="Area" HeaderText="Кв.метра" DataFormatString="{0:n2}" ItemStyle-HorizontalAlign="Right"></asp:BoundColumn>
											<asp:BoundColumn DataField="EURForArea" HeaderText="EUR/Кв.метър" DataFormatString="{0:n2}" ItemStyle-HorizontalAlign="Right"></asp:BoundColumn>
											<asp:BoundColumn DataField="EUR" HeaderText="Общо ЕUR" DataFormatString="{0:n2}" ItemStyle-HorizontalAlign="Right"></asp:BoundColumn>
											<asp:BoundColumn DataField="Amount" HeaderText="Платени" DataFormatString="{0:n2}" ItemStyle-HorizontalAlign="Right"></asp:BoundColumn>
											<asp:BoundColumn HeaderText="Платени %" ItemStyle-HorizontalAlign="Right"></asp:BoundColumn>
											<asp:BoundColumn DataField="AmountLeft" HeaderText="Дължими към" DataFormatString="{0:n2}" ItemStyle-HorizontalAlign="Right"></asp:BoundColumn>
											<asp:BoundColumn HeaderText="Дължими %" ItemStyle-HorizontalAlign="Right"></asp:BoundColumn>
											<asp:BoundColumn DataField="ORDERCLAUSE" Visible="false"></asp:BoundColumn>
										</Columns>
									</asp:datagrid><br>
									<br>
									<br>
									<br>
									<asp:label id="lblNoDataFound" runat="server" EnableViewState="False" CssClass="InfoLabel"
										Visible="False">Не са намерени данни съответстващи на избрания филтър.</asp:label><br>
									<br>
									<br>
									<asp:label id="lblCreatedByLbl" runat="server" Font-Size="9pt" Font-Bold="True" EnableViewState="False"
										Visible="False">ИЗГОТВИЛ СПРАВКАТА:</asp:label><asp:label id="lblCreatedBy" runat="server" Font-Size="9pt" EnableViewState="False" Visible="False"></asp:label><BR>
									<br>
									<br>
								</td>
							</tr>
						</table>
					</td>
				</tr>
			</table>
		</form>
	</body>
</HTML>
