using System;
using System.Collections;
using System.Text;

namespace Asa.Timesheet.Data
{
	
	public class MailingTypeData
	{
		private int _mailingTypeID = -1;
		private string _mailingTypeName = string.Empty;
	
		/// <summary>
		/// Initialize an new empty Login object.
		/// </summary>
		public MailingTypeData()
		{
		}
		
		/// <summary>
		/// Initialize a new Login object with the given parameters.
		/// </summary>
		public MailingTypeData(int mailingTypeID, string mailingTypeName)
		{
			this._mailingTypeID = mailingTypeID;
			this._mailingTypeName = mailingTypeName;
		}	
		
	  
		public override string ToString()
		{
			StringBuilder builder = new StringBuilder();
			builder.Append("_mailingTypeID: " + _mailingTypeID.ToString()).Append("\r\n");
			builder.Append("_mailingTypeName: " + _mailingTypeName.ToString()).Append("\r\n");
			builder.Append("\r\n");
			return builder.ToString();
		}	
		public object GetByIndex(int nIndex)
		{
			switch(nIndex)
			{
				case 0:
					return _mailingTypeID;
				case 1:
					return _mailingTypeName;
			}
			return null;
		}
		#region Properties
	
		public int MailingTypeID
		{
			get { return _mailingTypeID; }
			set { _mailingTypeID = value; }
		}
	
		public string MailingTypeName
		{
			get { return _mailingTypeName; }
			set { _mailingTypeName = value; }
		}

		#endregion
	}
}
