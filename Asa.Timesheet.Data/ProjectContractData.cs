using System;
using System.Collections;
using System.Text;

namespace Asa.Timesheet.Data
{
	public class ProjectContractData
	{
		private int _projectContractID = -1;
		private int _projectID = -1;
		private string _projectContractName = string.Empty;
		/// <summary>
		/// Initialize an new empty Login object.
		/// </summary>
		public ProjectContractData()
		{
		}
		
		/// <summary>
		/// Initialize a new Login object with the given parameters.
		/// </summary>
		public ProjectContractData(int projectContractID, int projectID,string projectContractName)
		{
			this._projectContractID = projectContractID;
			this._projectID = projectID;
			this._projectContractName = projectContractName;
		}	
		
	  
		public override string ToString()
		{
			StringBuilder builder = new StringBuilder();
			builder.Append("_projectContractID: " + _projectContractID.ToString()).Append("\r\n");
			builder.Append("_projectID: " + _projectID.ToString()).Append("\r\n");
			builder.Append("\r\n");
			return builder.ToString();
		}	
		public object GetByIndex(int nIndex)
		{
			switch(nIndex)
			{
				case 0:
					return _projectContractID;
				case 1:
					return _projectID;
				case 2:
					return _projectContractName;
			}
			return null;
		}

		#region Properties
	
		public int ProjectContractID
		{
			get { return _projectContractID; }
			set { _projectContractID = value; }
		}
	
		public int ProjectID
		{
			get { return _projectID; }
			set { _projectID = value; }
		}

		public string ProjectContractName
		{
			get { return _projectContractName; }
			set { _projectContractName = value; }
		}
	
		#endregion
	}
}


