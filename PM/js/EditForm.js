

function InitEdit(mel, els, vmBtns, emBtns, excls)
{
  mode = document.getElementById(mel);
  elsAr = document.getElementById(els).value.split(';');
  
  vmButtons = document.getElementById(vmBtns).value.split(";");
  emButtons = document.getElementById(emBtns).value.split(";");
  
  exclEls = document.getElementById(excls).value.split(";");
  
  SetViewMode();
  for(var i=0;i<vmButtons.length;i++)
  {
	if(document.getElementById(vmButtons[i])!=null)
	{
		document.getElementById(vmButtons[i]).onclick = 
		function()
		{ 
			mode.value=0; 
			SetViewMode(); 
			return false; 
		}
	}
  }
}

function SetViewMode()
{
  var disable = (mode.value == 1) ? true : false;  
  for (var i=0; i<elsAr.length; i++) SetContainer(elsAr[i], disable);
   
 
  for (var i=0; i<vmButtons.length; i++)
  {
    var btn = document.getElementById(vmButtons[i]);
    if (btn!=null)  btn.style.display = disable ? "" : "none";
      
  }
  
  for (var i=0; i<emButtons.length; i++)
  {
    var btn = document.getElementById(emButtons[i]);
    if (btn!=null)  btn.style.display = disable ? "none" : "";
      
  }
}

function FindExcl(id)
{
  for (var i=0; i<exclEls.length; i++)
  if (exclEls[i] == id) return true;
  
  return false;
}

function SetContainer(el, disable)
{
  var cont;
  
  cont = document.getElementById(el);
 
  if (cont == null)return;
  
 var cls = cont.getElementsByTagName("input");
  
  for (var i=0;i<cls.length;i++)
  {
    if (FindExcl(cls[i].id) == true) continue;
    
    if ( (cls[i].type=="text") || (cls[i].type=="password") )
    {
      cls[i].disabled = disable;
    //  cls[i].className = disable ? "enterDataBoxReadOnly" : "enterDataBox";
    }
    else if ( (cls[i].type=="file"))
    {
     cls[i].disabled = disable;
    }
    else if ( (cls[i].type=="image"))
    {
    cls[i].style.display = disable ? "none" : "";
    }
    else
    {
       if (cls[i].type=="checkbox" ||cls[i].type=="radio" ) cls[i].disabled = disable;
       else 
        if (cls[i].type == "submit") cls[i].style.display = disable ? "none" : "";//cls[i].disabled = disable;
    }
  }
      
  cls = cont.getElementsByTagName("textarea");
  
  for (var i=0;i<cls.length;i++)
  {
    cls[i].disabled = disable;
  }
      
  cls = cont.getElementsByTagName("select");
  
  for (var i=0;i<cls.length;i++)
  {
    cls[i].disabled = disable;
   // cls[i].className = readOnly? "enterDataBoxReadOnly" : "enterDataBox";
  }
  
  cls = cont.getElementsByTagName("a");
  for (var i=0;i<cls.length;i++)
  {
   if (FindExcl(cls[i].id) == false) 
    cls[i].style.display = disable ? "none" : "";
  }
  
  cls = cont.getElementsByTagName("img");
  for (var i=0;i<cls.length;i++)
  {
    cls[i].style.display = disable ? "none" : "";
  }     
}
