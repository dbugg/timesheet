﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace PM.UserControls
{
    public partial class PageFooter : System.Web.UI.UserControl
    {
        protected System.Web.UI.WebControls.Label lblTitle;
        protected void Page_Load(object sender, EventArgs e)
        {

        }
        public string PageTitle
        {
            get { return lblTitle.Text; }
            set { lblTitle.Text = value; }
        }
    }
}