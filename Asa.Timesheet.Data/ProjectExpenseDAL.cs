using System;
using System.Data;
using System.Data.SqlClient; 
using System.Collections;
using Microsoft.ApplicationBlocks.Data;

namespace Asa.Timesheet.Data
{
	public class ProjectExpenseDAL
	{
		/// <summary>
		/// Loads a ProjectExpenseData object from the database using the given ProjectID
		/// </summary>
		/// <param name="projectID">The primary key value</param>
		/// <returns>A ProjectExpenseData object</returns>
		public static ProjectExpenseData Load(int projectID)
		{
			SqlParameter[] parameterValues = new SqlParameter[] { new SqlParameter("@ProjectID", projectID) };
			using (SqlDataReader reader = SqlHelper.ExecuteReader(CommonDAL.ConnectionString, CommandType.StoredProcedure, "ProjectExpensesSelProc", parameterValues))
			{
				if (reader.Read())
				{
					ProjectExpenseData result = new ProjectExpenseData();
					LoadFromReader(reader,result);
					return result;
				}
				else
					return null;
			}
		}

		/// <summary>
		/// Loads a ProjectExpenseData object from the database using the given where clause
		/// </summary>
		/// <param name="whereClause">The filter expression for the query</param>
		/// <returns>A ProjectExpenseData object</returns>
		public static ProjectExpenseData Load(string whereClause)
		{
			SqlParameter[] parameterValues = new SqlParameter[] { new SqlParameter("@WhereClause", whereClause) };
			using (SqlDataReader reader = SqlHelper.ExecuteReader(CommonDAL.ConnectionString, CommandType.StoredProcedure, "ProjectExpensesListProc", parameterValues))
			{
				if (reader.Read())
				{
					ProjectExpenseData result = new ProjectExpenseData();
					LoadFromReader(reader,result);
					return result;
				}
				else
					return null;
			}
		}
		public static int Save( ProjectExpenseData source)
		{
			ProjectExpenseData ped=Load(source.ProjectID);
			if (ped==null)
				return Insert(source);
			else
				return Update(source);	
		}

		public static void Delete( ProjectExpenseData source)
		{
			Delete(source.ProjectID);
		}
		/// <summary>
		/// Loads a collection of ProjectExpenseData objects from the database.
		/// </summary>
		/// <returns>A collection containing all of the ProjectExpenseData objects in the database.</returns>
		public static ProjectExpensesVector LoadCollection()
		{
			SqlParameter[] parms = new SqlParameter[] {};
			return LoadCollection("ProjectExpensesListProc", parms);
		}
	  
		/// <summary>
		/// Saves a collection of ProjectExpenseData objects to the database.
		/// </summary>
		/// <returns>A collection containing all of the ProjectExpenseData objects in the database.</returns>
		public static  void  SaveCollection(ProjectExpensesVector col,string spName, SqlParameter[] parms)
		{
			ProjectExpensesVector colOld = LoadCollection(spName,parms);
			UpdateCollection(colOld,col);
		}
		public static  void  SaveCollection(ProjectExpensesVector col)
		{
			ProjectExpensesVector colOld = LoadCollection();
			UpdateCollection(colOld,col);
		}
		public static  void  SaveCollection(ProjectExpensesVector col, string whereClause )
		{
			ProjectExpensesVector colOld = LoadCollection(whereClause);
			UpdateCollection(colOld,col);
		}
		/// <summary>
		/// Loads a collection of ProjectExpenseData objects from the database.
		/// </summary>
		/// <returns>A collection containing all of the ProjectExpenseData objects in the database.</returns>
		public static ProjectExpensesVector LoadCollection(string whereClause)
		{
			SqlParameter[] parms = new SqlParameter[] { new SqlParameter("@WhereClause", whereClause) };
			return LoadCollection("ProjectExpensesListProc", parms);
		}

		/// <summary>
		/// Loads a collection of ProjectExpenseData objects from the database.
		/// </summary>
		/// <returns>A collection containing all of the ProjectExpenseData objects in the database.</returns>
		public static ProjectExpensesVector LoadCollection(string spName, SqlParameter[] parms)
		{
			ProjectExpensesVector result = new ProjectExpensesVector();
			using (SqlDataReader reader = SqlHelper.ExecuteReader(CommonDAL.ConnectionString, CommandType.StoredProcedure, spName, parms))
			{
				while (reader.Read())
				{
					ProjectExpenseData tmp = new ProjectExpenseData();
					LoadFromReader(reader, tmp);
					result.Add(tmp);
				}
			}
			return result;
		}
		/// <summary>
		/// Deletes a ProjectExpenseData object from the database.
		/// </summary>
		/// <param name="projectID">The primary key value</param>
		public static void Delete(int projectID)
		{
			SqlParameter[] parameterValues = new SqlParameter[] { new SqlParameter("@ProjectID", projectID) };
			SqlHelper.ExecuteNonQuery(CommonDAL.ConnectionString, CommandType.StoredProcedure, "ProjectExpensesDelProc", parameterValues);
		}

		public static void LoadFromReader(SqlDataReader reader, ProjectExpenseData target)
		{
			if (reader != null && !reader.IsClosed)
			{
				//target.Key = reader.GetInt32(0);
				if (!reader.IsDBNull(0)) target.ProjectID = reader.GetInt32(0);
				if (!reader.IsDBNull(1)) target.PercentExpenses = reader.GetDecimal(1);
				if (!reader.IsDBNull(2)) target.Estimate = reader.GetDecimal(2);
				if (!reader.IsDBNull(3)) target.CurrentExpenes = reader.GetDecimal(3);
				if (!reader.IsDBNull(4)) target.Email1 = reader.GetBoolean(4);
				if (!reader.IsDBNull(5)) target.Email2 = reader.GetBoolean(5);
				if (!reader.IsDBNull(6)) target.Email3 = reader.GetBoolean(6);
				if (!reader.IsDBNull(7)) target.Email4 = reader.GetBoolean(7);
				if (!reader.IsDBNull(8)) target.Email5 = reader.GetBoolean(8);
				if (!reader.IsDBNull(9)) target.Email6 = reader.GetBoolean(9);
				if (!reader.IsDBNull(10)) target.Email7 = reader.GetBoolean(10);
				if (!reader.IsDBNull(11)) target.Email8 = reader.GetBoolean(11);
			}
		}
		
	  
		public static int Insert( ProjectExpenseData source)
		{
			SqlParameter[] parameterValues = GetInsertParameterValues(source);
			SqlHelper.ExecuteNonQuery(CommonDAL.ConnectionString, CommandType.StoredProcedure, "ProjectExpensesInsProc", parameterValues);
			
			return source.ProjectID;
		}

		public static int Update( ProjectExpenseData source)
		{
			SqlParameter[] parameterValues = GetUpdateParameterValues(source);
			SqlHelper.ExecuteNonQuery(CommonDAL.ConnectionString, CommandType.StoredProcedure,
				"ProjectExpensesUpdProc", parameterValues);
			return source.ProjectID;
		}
		private static void UpdateCollection(ProjectExpensesVector colOld,ProjectExpensesVector col)
		{
			// Delete all old items
			foreach( ProjectExpenseData itemOld in colOld)
			{
				if(!ExistsKey(itemOld.ProjectID, col))
					Delete(itemOld.ProjectID);
			}
			foreach( ProjectExpenseData item in col)
			{
				if(item.ProjectID <= -1)
					Insert(item);
				else
					Update(item);
			}
		}
		private static bool ExistsKey(int Key, ProjectExpensesVector col)
		{
			foreach( ProjectExpenseData item in col)
				if(item.ProjectID==Key)
					return true;
			return false;
		}

		private static SqlParameter GetSqlParameter(string name, ParameterDirection direction, object value)
		{
			SqlParameter p = new SqlParameter(name, value);
			p.Direction = direction;
			return p;
		}

		private static SqlParameter[] GetInsertParameterValues( ProjectExpenseData source)
		{
			SqlParameter[] prams = new SqlParameter[12];
			prams[0] = GetSqlParameter("@ProjectID", ParameterDirection.Input, source.ProjectID);
			prams[1] = GetSqlParameter("@PercentExpenses", ParameterDirection.Input, source.PercentExpenses);
			prams[2] = GetSqlParameter("@Estimate", ParameterDirection.Input, source.Estimate);
			prams[3] = GetSqlParameter("@CurrentExpenes", ParameterDirection.Input, source.CurrentExpenes);
			prams[4] = GetSqlParameter("@Email1", ParameterDirection.Input, source.Email1);
			prams[5] = GetSqlParameter("@Email2", ParameterDirection.Input, source.Email2);
			prams[6] = GetSqlParameter("@Email3", ParameterDirection.Input, source.Email3);
			prams[7] = GetSqlParameter("@Email4", ParameterDirection.Input, source.Email4);
			prams[8] = GetSqlParameter("@Email5", ParameterDirection.Input, source.Email5);
			prams[9] = GetSqlParameter("@Email6", ParameterDirection.Input, source.Email6);
			prams[10] = GetSqlParameter("@Email7", ParameterDirection.Input, source.Email7);
			prams[11] = GetSqlParameter("@Email8", ParameterDirection.Input, source.Email8);
			return prams;
		}

		private static SqlParameter[] GetUpdateParameterValues( ProjectExpenseData source)
		{
			return new SqlParameter[] {
										  new SqlParameter("@ProjectID", source.ProjectID),
										  new SqlParameter("@PercentExpenses", source.PercentExpenses),
										  new SqlParameter("@Estimate", source.Estimate),
										  new SqlParameter("@CurrentExpenes", source.CurrentExpenes),
										  new SqlParameter("@Email1", source.Email1),
										  new SqlParameter("@Email2", source.Email2),
										  new SqlParameter("@Email3", source.Email3),
										  new SqlParameter("@Email4", source.Email4),
										  new SqlParameter("@Email5", source.Email5),
										  new SqlParameter("@Email6", source.Email6),
										  new SqlParameter("@Email7", source.Email7),
										  new SqlParameter("@Email8", source.Email8)
									  };
		}
	}
}   

