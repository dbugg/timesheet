<%@ Page language="c#" Codebehind="Error.aspx.cs" AutoEventWireup="false" Inherits="Asa.Timesheet.WebPages.Error" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
	<HEAD>
		<title>Timesheet Error</title>
		<meta name="GENERATOR" Content="Microsoft Visual Studio .NET 7.1">
		<meta name="CODE_LANGUAGE" Content="C#">
		<meta name="vs_defaultClientScript" content="JavaScript">
		<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5">
		<LINK href="styles/timesheet.css" type="text/css" rel="stylesheet">
		<Link href="<%=Request.ApplicationPath%>/styles/timesheet.css" type="text/css" rel="stylesheet">
	</HEAD>
	<body bottomMargin="0" leftMargin="0" topMargin="0" rightMargin="0">
		<form id="Form1" method="post" runat="server">
			<table height="100%" cellSpacing="0" cellPadding="0" width="100%" border="0">
				<tr height="80">
					<td background="<%=Request.ApplicationPath%>/images/copy1.gif">
						<TABLE id="Table2" height="100%" width="100%">
							<TR>
								<TD style="WIDTH: 106px">&nbsp;&nbsp;&nbsp;&nbsp; <IMG height="72" alt="" src="<%=Request.ApplicationPath%>/images/logo.jpg" width="58"></TD>
								<TD noWrap>
									<asp:label id="lblUser" runat="server" Font-Bold="True" Font-Size="Medium" ForeColor="Red">Грешка!</asp:label>
									<br>
									<asp:label id="lblInfo" runat="server"></asp:label>
								</TD>
							</TR>
						</TABLE>
					</td>
				</tr>
				<TR height="1">
					<td noWrap background="<%=Request.ApplicationPath%>/images/line.gif" height="1"></td>
				</TR>
				<tr>
					<td>
						<table height="100%" cellSpacing="0" cellPadding="0" width="100%" border="0">
							<tr>
								<td noWrap width="1" bgColor="lightgrey"></td>
								<TD style="PADDING-LEFT: 5px; WIDTH: 105px; PADDING-TOP: 3px" vAlign="top" noWrap></TD>
								<td style="PADDING-LEFT: 5px; PADDING-TOP: 3px" vAlign="top" noWrap></td>
							</tr>
						</table>
					</td>
				</tr>
			</table>
		</form>
	</body>
</HTML>
