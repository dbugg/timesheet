using System;
using System.Data;
using System.Data.SqlClient; 
using System.Collections;
using System.Collections.Specialized;
using Microsoft.ApplicationBlocks.Data;

namespace Asa.Timesheet.Data
{
	/// <summary>
	/// Summary description for EffUDL.
	/// </summary>
	public class EffUDL
	{
		
		private static SqlCommand CreateReportsWorkTimesHoursByUsersSummary (int UserID, DateTime StartDate, DateTime EndDate)
		{
			SqlCommand cmd = new SqlCommand();
			cmd.CommandType = System.Data.CommandType.StoredProcedure;
			cmd.CommandText = "ReportsWorkTimesHoursByUsersSummary1";
			cmd.Parameters.Add(new System.Data.SqlClient.SqlParameter("@UserID", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.Input, true, 10, 0, "UserID", DataRowVersion.Current, null));
			if(UserID>0)
				cmd.Parameters["@UserID"].Value = UserID;
			cmd.Parameters.Add(new System.Data.SqlClient.SqlParameter("@StartDate", System.Data.SqlDbType.SmallDateTime, 8, System.Data.ParameterDirection.Input, true, 0, 0, "StartDate", DataRowVersion.Current, null));
			if(StartDate!=Constants.DateMax)
				cmd.Parameters["@StartDate"].Value = StartDate;
			cmd.Parameters.Add(new System.Data.SqlClient.SqlParameter("@EndDate", System.Data.SqlDbType.SmallDateTime, 8, System.Data.ParameterDirection.Input, true, 0, 0, "EndDate", DataRowVersion.Current, null));
			if(EndDate!=Constants.DateMax)
				cmd.Parameters["@EndDate"].Value = EndDate;

			return cmd;
		}
		
		public static System.Data.DataView ExecuteReportsWorkTimesHoursByUsersSummary(int UserID, DateTime StartDate, DateTime EndDate)  
		{
			System.Data.SqlClient.SqlDataReader RetObj;
			System.Data.SqlClient.SqlCommand cmd;
			cmd = CreateReportsWorkTimesHoursByUsersSummary(UserID, StartDate, EndDate);
			SqlConnection cnDAL = new SqlConnection();

			try
			{
				cnDAL =  CommonDAL.OpenConnection();
				cmd.Connection = cnDAL;
				RetObj = cmd.ExecuteReader();
				return new DataView(CommonDAL.CreateTableFromReader(RetObj));
		
			}
			catch(SqlException ex)
			{

				throw new ApplicationException("ReportsWorkTimesHoursByUsersSummary",ex);
			}

			finally
			{
				CommonDAL.CloseConnection(cnDAL);
			}
		}
		
		private static SqlCommand CreateReportsWorkTimesHoursSummary (DateTime StartDate, DateTime EndDate, bool ASI)
		{
			SqlCommand cmd = new SqlCommand();
			cmd.CommandType = System.Data.CommandType.StoredProcedure;
			cmd.CommandText = "ReportsWorkTimesHoursSummary1";
			cmd.Parameters.Add(new System.Data.SqlClient.SqlParameter("@StartDate", System.Data.SqlDbType.SmallDateTime, 8, System.Data.ParameterDirection.Input, true, 0, 0, "StartDate", DataRowVersion.Current, null));
			if(StartDate!=Constants.DateMax)
				cmd.Parameters["@StartDate"].Value = StartDate;
			cmd.Parameters.Add(new System.Data.SqlClient.SqlParameter("@EndDate", System.Data.SqlDbType.SmallDateTime, 8, System.Data.ParameterDirection.Input, true, 0, 0, "EndDate", DataRowVersion.Current, null));
			if(EndDate!=Constants.DateMax)
				cmd.Parameters["@EndDate"].Value = EndDate;
			cmd.Parameters.Add(new System.Data.SqlClient.SqlParameter("@ASI", System.Data.SqlDbType.Bit, 1, System.Data.ParameterDirection.Input, true, 0, 0, "ASI", DataRowVersion.Current, null));
			cmd.Parameters["@ASI"].Value = ASI;
			return cmd;
		}

		public static System.Data.DataView ExecuteReportsWorkTimesHoursSummary(DateTime StartDate, DateTime EndDate, bool ASI)  
		{
			System.Data.SqlClient.SqlDataReader RetObj;
			System.Data.SqlClient.SqlCommand cmd;
			cmd = CreateReportsWorkTimesHoursSummary(StartDate, EndDate, ASI);
			SqlConnection cnDAL = new SqlConnection();

			try
			{
				cnDAL =  CommonDAL.OpenConnection();
				cmd.Connection = cnDAL;
				RetObj = cmd.ExecuteReader();
				return new DataView(CommonDAL.CreateTableFromReader(RetObj));
		
			}
			catch(SqlException ex)
			{

				throw new ApplicationException("ReportsWorkTimesHoursSummary",ex);
			}

			finally
			{
				CommonDAL.CloseConnection(cnDAL);
			}
		}


		private static SqlCommand CreateReportsPayments (DateTime StartDate, DateTime EndDate)
		{
			SqlCommand cmd = new SqlCommand();
			cmd.CommandType = System.Data.CommandType.StoredProcedure;
			cmd.CommandText = "ReportsPayments";
			cmd.Parameters.Add(new System.Data.SqlClient.SqlParameter("@StartDate", System.Data.SqlDbType.SmallDateTime, 8, System.Data.ParameterDirection.Input, true, 0, 0, "StartDate", DataRowVersion.Current, null));
			if(StartDate!=Constants.DateMax)
				cmd.Parameters["@StartDate"].Value = StartDate;
			cmd.Parameters.Add(new System.Data.SqlClient.SqlParameter("@EndDate", System.Data.SqlDbType.SmallDateTime, 8, System.Data.ParameterDirection.Input, true, 0, 0, "EndDate", DataRowVersion.Current, null));
			if(EndDate!=Constants.DateMax)
				cmd.Parameters["@EndDate"].Value = EndDate;

			return cmd;
		}

		public static System.Data.DataView ExecuteReportsPayments(DateTime StartDate, DateTime EndDate)  
		{
			System.Data.SqlClient.SqlDataReader RetObj;
			System.Data.SqlClient.SqlCommand cmd;
			cmd = CreateReportsPayments(StartDate, EndDate);
			SqlConnection cnDAL = new SqlConnection();

			try
			{
				cnDAL =  CommonDAL.OpenConnection();
				cmd.Connection = cnDAL;
				RetObj = cmd.ExecuteReader();
				return new DataView(CommonDAL.CreateTableFromReader(RetObj));
		
			}
			catch(SqlException ex)
			{

				throw new ApplicationException("ReportsPayments",ex);
			}

			finally
			{
				CommonDAL.CloseConnection(cnDAL);
			}
		}


	}
}
