using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using Asa.Timesheet.Data;
using System.Data.SqlClient;
using System.Configuration;
using System.IO;
using log4net;
using log4net.Config;
using System.Collections.Generic;
using System.Linq;


namespace PM
{
	/// <summary>
	/// Summary description for EditFile.
	/// </summary>
	public partial class EditFile : BasePage
	{
		#region WebControls
	
		protected System.Web.UI.WebControls.CheckBox cbMail;
		
		
		private static readonly ILog log = LogManager.GetLogger(typeof(EditFile));
	
		#endregion

		#region enum

		private enum sendMailcbl
		{
			subcontracters = 0,
			asa = 1,
			clients = 2,
			builders = 3,
		}
	
		private enum gridColomns
		{
			link = 0,
			FileName,
		}

		#endregion

		#region Page_Load

        protected void Page_Load(object sender, System.EventArgs e)
        {
            if ((!LoggedUser.IsAuthenticated) && UIHelpers.HasAccess(LoggedUser, UIHelpers.GetPIDParam()) && UIHelpers.HasAccessToCategory(LoggedUser, UIHelpers.GetCIDParam())) ErrorRedirect(UIHelpers.GetText(Language, "noRightsAlert"));
            if (UIHelpers.GetPIDParam() == -1 || UIHelpers.GetCIDParam() == -1) ErrorRedirect(UIHelpers.GetText(Language, "noRightsAlert"));

            LoadResourses();
            //string s = (UIHelpers.GetIDParam()>0)? Resource.ResourceManager["pmPageTitle_EditFile"]:Resource.ResourceManager["pmPageTitle_NewFile"];

            if (!this.IsPostBack)
            {
                if (!LoggedUser.IsUser)
                {
                    lbLaterMail.Visible = cbLaterMail.Visible = false;
                    lbVisibles.Visible = cblVisibles.Visible = false;
                }
                else
                {
                    lbVisibles.Visible = cblVisibles.Visible = true;
                }

                int FileID = UIHelpers.GetIDParam();
                LoadCheckBoxListSend();

                if (FileID > 0)
                {
                    if (!LoadFileInfo(FileID)) { lblError.Text = UIHelpers.GetText(Language, "ErrorLoadFile"); return; }
                    //lbMail.Visible = cblMail.Visible = false;
                    //					int iVersion = UIHelpers.ToInt(txtFileVersion.Text)+1;
                    //					txtFileVersion.Text = iVersion.ToString();
                }
                else
                {
                    int CategoryID = UIHelpers.GetCIDParam();
                    if (!LoadCategory(CategoryID)) { lblError.Text = UIHelpers.GetText(Language, "noRightsAlert"); return; }
                    //notes:setFileVersion to 0
                    txtFileNameSave.Text = "";
                    txtFileVersion.Text = "0";
                    lbFileVersion.Visible = txtFileVersion.Visible = false;
                    LoadVisibles(false, false, false, false, false);


                }
                //add subcontractors
                List<ProjectPerson> list = new List<ProjectPerson>();
                var col = ProjectSubcontracterDAL.LoadCollection2("ProjectSubcontractersListProcDistinct", SQLParms.CreateProjectSubcontractersListProc1(UIHelpers.GetPIDParam(), LoggedUser.HasPaymentRights));
                foreach (ProjectSubcontracterData2 el in col)
                {
                    if (!String.IsNullOrEmpty(el.Email))
                    {
                        list.Add(new ProjectPerson() { Name = el.SubcontracterName, Id = el.SubcontracterID, Type = "�������������", InternalType = 1, Email = el.Email });
                    }
                }
                int ProjectID = UIHelpers.GetPIDParam();
                ProjectUsersVector puv = ProjectUserDAL.LoadCollection2("ProjectUsersSelByProjectProc2", SQLParms.CreateProjectUsersSelByProjectProc(ProjectID));
                foreach (ProjectUserData2 el in puv)
                {
                    if (!String.IsNullOrEmpty(el.Email))
                    {
                        list.Add(new ProjectPerson() { Name = el.Name, Id = el.UserID, Type = "����", InternalType = 2, Email = el.Email });
                    }
                }
                ProjectClientsVector pcv = ProjectClientDAL.LoadCollection2("ProjectClientsSelByProjectProc2", SQLParms.CreateProjectClientsSelByProjectProc(ProjectID, (int)ClientTypes.Client));
                ProjectClientsVector pcv3 = ProjectClientDAL.LoadCollection2("ProjectClientsSelByProjectProc2", SQLParms.CreateProjectClientsSelByProjectProc(ProjectID, (int)ClientTypes.Builder));
                ProjectClientsVector pcv1 = ProjectClientDAL.LoadCollection2("ProjectClientsSelByProjectProc2", SQLParms.CreateProjectClientsSelByProjectProc(ProjectID, (int)ClientTypes.Brokers));
                ProjectClientsVector pcv2 = ProjectClientDAL.LoadCollection2("ProjectClientsSelByProjectProc2", SQLParms.CreateProjectClientsSelByProjectProc(ProjectID, (int)ClientTypes.ProjectManagers));
                ProjectClientsVector pcv4 = ProjectClientDAL.LoadCollection2("ProjectClientsSelByProjectProc2", SQLParms.CreateProjectClientsSelByProjectProc(ProjectID, (int)ClientTypes.Supervisors));
                this.FillClients(pcv, list, "������");
                this.FillClients(pcv1, list, "��������");
                this.FillClients(pcv2, list, "������");
                this.FillClients(pcv3, list, "��������");
                this.FillClients(pcv4, list, "�����������");
                grdProjects.DataSource = list;
                grdProjects.DataBind();

                lblError.Text = "";
                ShowArchive();
                ShowArchive_Click();
            }
            if (UIHelpers.GetIDParam() > 0)
                Header.PageTitle = string.Format(UIHelpers.GetText(Language, "pmPageTitle_EditFile"), txtCategoryName.Text, ProjectsData.SelectProjectNameForPMmails(UIHelpers.GetPIDParam()));
            else
                Header.PageTitle = string.Format(UIHelpers.GetText(Language, "pmPageTitle_NewFile"), txtCategoryName.Text, ProjectsData.SelectProjectNameForPMmails(UIHelpers.GetPIDParam()));

        }

        private void FillClients(ProjectClientsVector pcv, List<ProjectPerson> list, string type)
        {
            foreach (ProjectClientData2 el in pcv)
            {
                if (!String.IsNullOrEmpty(el.Email))
                {
                    list.Add(new ProjectPerson() { Name = el.Name, Id = el.ClientID, Type = type, InternalType = 3, Email = el.Email });
                }
            }
        }




        private bool LoadFileInfo(int FileID)
        {
            try
            {
                FilePMData dat = FilePMDAL.Load(FileID);
                if (dat == null)
                    return false;
                txtCategoryName.Text = dat.CategoryName;
                txtProjectName.Text = dat.ProjectName;
                txtFileSubject.Text = dat.FileSubject;
                txtFileMinutes.Text = dat.FileMinutes;
                txtFileVersion.Text = dat.FileVersion.ToString();
                //FileVersionSession = dat.FileVersion;
                txtType.Text = dat.FileType;
                cbLaterMail.Checked = dat.MailForDownload;
                txtFileNameSave.Text = dat.FileNameSave;
                LoadVisibles(dat.IsClientVisible, dat.IsBuilderVisible, dat.IsSubcontractorVisible, dat.IsNadzorVisible, dat.IsOtherVisible);

            }
            catch (Exception ex)
            {
                log.Error(UIHelpers.GetLogErrorMessage(ex));
                return false;
            }
            finally
            {
                //if (reader!=null) reader.Close(); 
            }
            return true;
        }

        private void LoadVisibles(bool IsClientVisible, bool IsBuilderVisible, bool IsSubcontracterVisible,
            bool IsNadzorVisible, bool IsOtherVisible)
        {
            string lang = Language;
            ListItem li = new ListItem(UIHelpers.GetText(lang, "CategoryVisibleSubcontracters"));
            cblVisibles.Items.Add(li);

            cblVisibles.Items[0].Selected = IsSubcontracterVisible;
            li = new ListItem(UIHelpers.GetText(lang, "CategoryVisibleClients"));
            cblVisibles.Items.Add(li);
            cblVisibles.Items[1].Selected = IsClientVisible;
            li = new ListItem(UIHelpers.GetText(lang, "CategoryVisibleBuilders"));
            cblVisibles.Items.Add(li);
            cblVisibles.Items[2].Selected = IsBuilderVisible;

            li = new ListItem(UIHelpers.GetText(lang, "CategoryVisibleNadzor"));
            cblVisibles.Items.Add(li);
            cblVisibles.Items[3].Selected = IsNadzorVisible;

            li = new ListItem(UIHelpers.GetText(lang, "CategoryVisibleOther"));
            cblVisibles.Items.Add(li);
            cblVisibles.Items[4].Selected = IsOtherVisible;
        }
        private bool LoadCategory(int CategoryID)
        {
            try
            {
                ProjectCategoryData dat = ProjectCategoryDAL.Load(CategoryID);
                if (dat == null)
                    return false;
                txtProjectName.Text = dat.ProjectName;
                txtCategoryName.Text = dat.CategoryName;
                //LoadVisibles(dat.IsClientVisible,dat.IsBuilderVisible,dat.IsSubcontractorVisible);
            }
            catch (Exception ex)
            {
                log.Error(UIHelpers.GetLogErrorMessage(ex));
                return false;
            }
            finally
            {
                //if (reader!=null) reader.Close(); 
            }
            return true;
        }


        private void LoadCheckBoxListSend()
        {
            string lang = Language;
            ListItem li = new ListItem(UIHelpers.GetText(lang, "cblSendMailSubcontracters"));
            cblMail.Items.Add(li);
            li.Selected = false;
            li = new ListItem(UIHelpers.GetText(lang, "cblSendMailAsa"));
            li.Selected = false;
            cblMail.Items.Add(li);
            if (LoggedUser.IsUser)
            {
                li = new ListItem(UIHelpers.GetText(lang, "cblSendMailClients"));
                cblMail.Items.Add(li);
                li.Selected = false;
                li = new ListItem(UIHelpers.GetText(lang, "cblSendMailBuilders"));
                cblMail.Items.Add(li);
                li.Selected = false;
            }
            else
            {
                cblMail.Width = new Unit(cblMail.Width.Value / 2);
            }
            if (!LoggedUser.IsUser && !LoggedUser.IsSubcontracter)
            {
                cblMail.Visible = lbMail.Visible = false;
            }
            //			cblMail.Items[(int)sendMailcbl.asa].Selected = false;
            //			cblMail.Items[(int)sendMailcbl.clients].Selected = false;
            //			cblMail.Items[(int)sendMailcbl.builders].Selected = false;
            //			cblMail.Items[(int)sendMailcbl.subcontracters].Selected = false;

        }


        private void LoadResourses()
        {
            string lang = Language;
            lbProjectName.Text = UIHelpers.GetText(lang, "Name_ProjectName");
            lblCategoryName.Text = UIHelpers.GetText(lang, "Name_CategoryName");
            lbFile.Text = UIHelpers.GetText(lang, "Name_AttachFileName");
            lbFileSubject.Text = UIHelpers.GetText(lang, "Name_SubjectName");
            lbFileMinutes.Text = UIHelpers.GetText(lang, "Name_MinutesName");
            lbFileVersion.Text = UIHelpers.GetText(lang, "Name_VersionName");
            lbType.Text = UIHelpers.GetText(lang, "Name_TypeName");
            lbMail.Text = UIHelpers.GetText(lang, "Name_SendMailName");
            lbLaterMail.Text = UIHelpers.GetText(lang, "Name_DownloadName");
            btnSave.Text = UIHelpers.GetText(lang, "Name_btnSaveName");
            //btnBack.Text = UIHelpers.GetText(lang,"Name_btnBackFileName");
            linkBack.Text = "<i class='fas fa-chevron-left'></i> " + UIHelpers.GetText(lang, "Name_btnBackFileName");
            lbAskedToAchive.Text = UIHelpers.GetText(lang, "Name_AskedToAchiveName");
            lbVisibles.Text = UIHelpers.GetText(lang, "Name_VisiblesName");

            lbType.Visible = txtType.Visible = false;
            lbArchive.Text = UIHelpers.GetText(lang, "EditFile_Archive_Label");
            btnShowArchive.Text = UIHelpers.GetText(lang, "btnShowArchive_Text");
        }
	

		#endregion

		#region Web Form Designer generated code
        override protected void OnInit(EventArgs e)
        {
            //
            // CODEGEN: This call is required by the ASP.NET Web Form Designer.
            //
            InitializeComponent();
            base.OnInit(e);
        }
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
        private void InitializeComponent()
        {
            this.grdFiles.ItemCommand += new System.Web.UI.WebControls.DataGridCommandEventHandler(this.grdFiles_ItemCommand);
            this.grdFiles.ItemDataBound += new System.Web.UI.WebControls.DataGridItemEventHandler(this.grdFiles_ItemDataBound);

        }
		#endregion

		#region EventHandlers

        private void back_click()
        {
            Response.Redirect("Files.aspx" + "?cid=" + UIHelpers.GetCIDParam() + "&pid=" + UIHelpers.GetPIDParam());
        }
		
//		private void btnBack_Click(object sender, System.EventArgs e)
//		{
//			Response.Redirect("Files.aspx"+"?cid="+UIHelpers.GetCIDParam()+"&pid="+UIHelpers.GetPIDParam());
//		}


        protected void btnSave_Click(object sender, System.EventArgs e)
        {
            SaveFile();
        }


        private bool AddFile(string fileName, int version)
        {
            try
            {
                HttpPostedFile file = FileCtrl.PostedFile;
                //bool b = File.Exists(file.FileName);
                //if(!b)
                //	return;
                string name = file.FileName;
                int n = name.LastIndexOf(".");
                if (n != -1)
                {
                    DirectoryInfo directory = new DirectoryInfo(string.Concat(System.Configuration.ConfigurationManager.AppSettings["FilePath"], @"\", txtProjectName.Text, @"\", txtCategoryName.Text));
                    if (!directory.Exists)
                        Directory.CreateDirectory(string.Concat(System.Configuration.ConfigurationManager.AppSettings["FilePath"], @"\", txtProjectName.Text, @"\", txtCategoryName.Text));
                    string ext = name.Substring(n);
                    string path = string.Concat(System.Configuration.ConfigurationManager.AppSettings["FilePath"], @"\", txtProjectName.Text, @"\", txtCategoryName.Text, @"\", fileName, ext);
                    string mapped = path;//Request.MapPath(path);
                    if (File.Exists(mapped))
                    {
                        File.Delete(mapped);
                    }
                    FileCtrl.PostedFile.SaveAs(mapped);
                    log.Info(string.Format(Resource.ResourceManager["log_info_PM_SaveFile"], txtFileSubject.Text, txtCategoryName.Text, txtProjectName.Text, LoggedUser.Account));
                }
                return true;
            }
            catch (Exception ex)
            {
                log.Error(UIHelpers.GetLogErrorMessage(ex));
                return false;
            }
        }


        private void SaveFile()
        {

            if (UIHelpers.GetIDParam() == -1 && FileCtrl.PostedFile.FileName.Length == 0)
            {
                AlertFieldNotEntered(lbFile);
                Dirty = true;
                return;
            }
            if (txtFileSubject.Text.Length == 0)
            {
                AlertFieldNotEntered(lbFileSubject);
                Dirty = true;
                return;
            }



            HttpPostedFile file = FileCtrl.PostedFile;
            //string fileType = txtType.Text;

            int iFileVersion = UIHelpers.ToInt(txtFileVersion.Text);
            string ext = txtType.Text;
            string sFileNameSave = txtFileNameSave.Text;
            if (FileCtrl.PostedFile.FileName.Length > 0)
            {
                if (file.ContentLength == 0)
                {
                    lblError.Text = UIHelpers.GetText(Language, "ErrorSaveFileTooBig");
                    return;
                }
                if (file.ContentLength > Convert.ToInt32(System.Configuration.ConfigurationManager.AppSettings["MaxSizeOfFile"]))
                {
                    lblError.Text = UIHelpers.GetText(Language, "ErrorSaveFileTooBig");
                    return;
                }
                else
                {
                    string name = file.FileName;
                    int n = name.LastIndexOf(@"\");
                    if (n != -1)
                    {
                        sFileNameSave = name.Substring(n + 1);
                        n = sFileNameSave.LastIndexOf(".");
                        if (n != -1)
                        {
                            ext = sFileNameSave.Substring(n);
                            sFileNameSave = sFileNameSave.Substring(0, n);
                        }
                    }
                    else
                    {
                        sFileNameSave = name;
                        n = sFileNameSave.LastIndexOf(".");
                        if (n != -1)
                        {
                            ext = sFileNameSave.Substring(n);
                            sFileNameSave = sFileNameSave.Substring(0, n);
                        }
                    }
                }
                iFileVersion += 1;
            }
            string path = string.Concat(System.Configuration.ConfigurationManager.AppSettings["FilePath"], @"\", txtProjectName.Text, @"\", txtCategoryName.Text, @"\", sFileNameSave, ext);
            //			if(File.Exists(path)&&(sFileNameSave != txtFileNameSave.Text||ext!=txtType.Text.Trim())&&
            //				FileCtrl.PostedFile.FileName.Length>0)
            //			{
            //				lblError.Text= string.Format(UIHelpers.GetText(Language,"ErrorFileSaveExist"),path);
            //				return;
            //			}

            //notes:here set visibility
            bool isClientVisible = false;
            bool isBuilderVisible = false;
            bool isSubcontracterVisible = false;
            bool isNadzorVisible = false;
            bool isOtherVisible = false;
            ListItem li = cblVisibles.Items.FindByText(UIHelpers.GetText(Language, "CategoryVisibleSubcontracters"));
            if (li != null)
                isSubcontracterVisible = li.Selected;
            if (LoggedUser.IsSubcontracter) isSubcontracterVisible = true;
            li = cblVisibles.Items.FindByText(UIHelpers.GetText(Language, "CategoryVisibleClients"));
            if (li != null)
                isClientVisible = li.Selected;
            li = cblVisibles.Items.FindByText(UIHelpers.GetText(Language, "CategoryVisibleBuilders"));
            if (li != null)
                isBuilderVisible = li.Selected;

            li = cblVisibles.Items.FindByText(UIHelpers.GetText(Language, "CategoryVisibleNadzor"));
            if (li != null)
                isNadzorVisible = li.Selected;

            li = cblVisibles.Items.FindByText(UIHelpers.GetText(Language, "CategoryVisibleOther"));
            if (li != null)
                isOtherVisible = li.Selected;

            if ((!isSubcontracterVisible) && cblMail.Items[(int)sendMailcbl.subcontracters].Selected)
            {
                lblError.Text = string.Format(UIHelpers.GetText(Language, "Name_errorEditFileSendMail"), UIHelpers.GetText(Language, "CategoryVisibleSubcontracters"));
                AlertFieldNotEntered(lbMail);
                Dirty = true;
                return;
            }
            if (LoggedUser.IsUser)
            {
                if ((!isClientVisible) && cblMail.Items[(int)sendMailcbl.clients].Selected)
                {
                    lblError.Text = string.Format(UIHelpers.GetText(Language, "Name_errorEditFileSendMail"), UIHelpers.GetText(Language, "CategoryVisibleClients"));
                    AlertFieldNotEntered(lbMail);
                    Dirty = true;
                    return;
                }
                if ((!isBuilderVisible) && cblMail.Items[(int)sendMailcbl.builders].Selected)
                {
                    lblError.Text = string.Format(UIHelpers.GetText(Language, "Name_errorEditFileSendMail"), UIHelpers.GetText(Language, "CategoryVisibleBuilders"));
                    AlertFieldNotEntered(lbMail);
                    Dirty = true;
                    return;
                }
            }
            if (LoggedUser.IsClient || LoggedUser.IsBuilder)
                isClientVisible = true;
            FilePMData fPMd = new FilePMData(UIHelpers.GetIDParam(), txtFileSubject.Text, txtFileMinutes.Text, iFileVersion, LoggedUser.FullName, ext, DateTime.Now, cbLaterMail.Checked, UIHelpers.GetCIDParam(), sFileNameSave, isClientVisible, isSubcontracterVisible, isBuilderVisible);
            fPMd.IsNadzorVisible = isNadzorVisible;
            fPMd.IsOtherVisible = isOtherVisible;

            fPMd.Link = RandomString(20);//no need to be cryptographically safe
            FilePMDAL.Save(fPMd);
            log.Info(string.Format(Resource.ResourceManager["log_info_PM_SaveFileInfo"], txtFileSubject.Text, txtCategoryName.Text, txtProjectName.Text, LoggedUser.Account));


            if (iFileVersion.ToString() != txtFileVersion.Text && txtFileVersion.Text != "0")
                if (!ArchiveFile(txtFileNameSave.Text, txtType.Text, UIHelpers.ToInt(txtFileVersion.Text), fPMd.FileID)) { lblError.Text = UIHelpers.GetText(Language, "ErrorArchiveFileNotSuccessfull"); }
            if (FileCtrl.PostedFile.FileName.Length > 0)
            {

                if (!AddFile(sFileNameSave, iFileVersion)) { lblError.Text = UIHelpers.GetText(Language, "ErrorSaveNotSuccessfull"); return; }
                this.SendMails(sFileNameSave + ext, fPMd.Link);
                //if(LoggedUser.IsSubcontracter)
                //    MailBO.PMSendMailAccessGiven(cblMail.Items[(int)sendMailcbl.asa].Selected,false,false,cblMail.Items[(int)sendMailcbl.subcontracters].Selected,sFileNameSave+ext,UIHelpers.GetPIDParam(),txtCategoryName.Text,LoggedUser,txtFileSubject.Text,txtFileMinutes.Text,UIHelpers.GetCIDParam(), UIHelpers.GetAppPathString());
                //    //MailBO.PMSendMailFileCreated(cblMail.Items[(int)sendMailcbl.asa].Selected,false,false,cblMail.Items[(int)sendMailcbl.subcontracters].Selected,sFileNameSave+ext,UIHelpers.GetPIDParam(),txtCategoryName.Text,LoggedUser);
                //else if(LoggedUser.IsUser)
                //    MailBO.PMSendMailAccessGiven(cblMail.Items[(int)sendMailcbl.asa].Selected,cblMail.Items[(int)sendMailcbl.clients].Selected,cblMail.Items[(int)sendMailcbl.builders].Selected,cblMail.Items[(int)sendMailcbl.subcontracters].Selected,sFileNameSave+ext,UIHelpers.GetPIDParam(),txtCategoryName.Text,LoggedUser,txtFileSubject.Text,txtFileMinutes.Text,UIHelpers.GetCIDParam(),UIHelpers.GetAppPathString());
                //    //MailBO.PMSendMailFileCreated(cblMail.Items[(int)sendMailcbl.asa].Selected,cblMail.Items[(int)sendMailcbl.clients].Selected,cblMail.Items[(int)sendMailcbl.builders].Selected,cblMail.Items[(int)sendMailcbl.subcontracters].Selected,txtFileSubject.Text,UIHelpers.GetPIDParam(),txtCategoryName.Text,LoggedUser);	
                //notes:Save Log in DateBase
                ProjectLogData pld = new ProjectLogData(-1, fPMd.FileID, DateTime.Now, true, LoggedUser.FullName);
                ProjectLogDAL.Save(pld);
            }
            //			if(cblMail.Items.Count>0)
            //			{
            //				
            //
            //			}

            Response.Redirect("Files.aspx" + "?cid=" + UIHelpers.GetCIDParam() + "&pid=" + UIHelpers.GetPIDParam());
        }

        private void SendMails(string filename, string link)
        {
            List<ProjectPerson> mails = new List<ProjectPerson>();
            foreach (DataGridItem row in grdProjects.Items)
            {
                // Access the CheckBox
                CheckBox cb = (CheckBox)row.FindControl("selector");
                if (cb != null && cb.Checked)
                {
                    ProjectPerson pp = new ProjectPerson();
                    pp.Email = row.Cells[1].Text;
                    pp.Id = int.Parse(row.Cells[4].Text);
                    pp.InternalType = int.Parse(row.Cells[5].Text);
                    mails.Add(pp);
                }
            }
            if (mails.Count > 0)
            {
                MailBO.PMSendMailAccessGiven(mails, filename, UIHelpers.GetPIDParam(), txtCategoryName.Text, LoggedUser, txtFileSubject.Text, txtFileMinutes.Text, UIHelpers.GetCIDParam(), UIHelpers.GetAppPathString(), link);
            }
        }


        private bool ArchiveFile(string fileName, string ext, int version, int fileID)
        {
            try
            {
                string filePathOld = /*this.MapPath(*/string.Concat(System.Configuration.ConfigurationManager.AppSettings["FilePath"], @"\", txtProjectName.Text, @"\", txtCategoryName.Text, @"\", fileName, ext);//);
                //notes:archived files are fileName + configSeparator + fileID + configSeparator + fileVersion
                string filePathNew = /*this.MapPath(*/string.Concat(System.Configuration.ConfigurationManager.AppSettings["FilePath"], @"\", txtProjectName.Text, @"\", txtCategoryName.Text, @"\", fileName, System.Configuration.ConfigurationManager.AppSettings["FileVersionSeparator"], fileID, System.Configuration.ConfigurationManager.AppSettings["FileVersionSeparator"], version, ext);//);
                //if(File.Exists( filePathOld.Trim())&&(!File.Exists(filePathNew)))
                File.Copy(filePathOld, filePathNew, true);
                if (File.Exists(filePathOld))
                    File.Delete(filePathOld);
                log.Info(string.Format(Resource.ResourceManager["log_info_PM_ArchiveFile"], txtFileSubject.Text, txtCategoryName.Text, txtProjectName.Text, LoggedUser.Account));
                return true;
            }
            catch (Exception ex)
            {
                log.Error(UIHelpers.GetLogErrorMessage(ex));
                return false;
            }
        }


        private void btnLogs_Click(object sender, System.EventArgs e)
        {
            Response.Redirect("Logs.aspx" + "?id=" + UIHelpers.GetIDParam());
        }

        protected void imgBack_Click(object sender, System.Web.UI.ImageClickEventArgs e)
        {
            back_click();
        }
        protected void link_Click(object sender, System.EventArgs e)
        {
            back_click();
        }
		
		#endregion

        private void ShowArchive()
        {
            if (UIHelpers.GetIDParam() == -1)
            {
                grid.Visible = false;
                return;
            }
            int iVersion = UIHelpers.ToInt(txtFileVersion.Text);
            if (iVersion <= 1)
            {
                grid.Visible = false;
                return;
            }
            string dirPath = string.Concat(string.Concat(System.Configuration.ConfigurationManager.AppSettings["FilePath"], @"\", txtProjectName.Text, @"\", txtCategoryName.Text));
            string[] files = Directory.GetFiles(dirPath);
            DataTable dtFiles = new DataTable();
            dtFiles.Columns.Add("FileName");

            for (int vers = 1; vers < iVersion; vers++)
            {
                string endWith = string.Concat("_", UIHelpers.GetIDParam(), "_", vers);
                foreach (string file in files)
                {
                    int index = file.LastIndexOf(".");
                    if (index <= 0) continue;
                    string fileName = file.Substring(0, index);
                    if (fileName.EndsWith(endWith))
                    {
                        DataRow dr = dtFiles.NewRow();
                        dr["FileName"] = file;
                        dtFiles.Rows.Add(dr);
                        break;
                    }
                }
            }
            grdFiles.DataSource = new DataView(dtFiles);
            grdFiles.DataBind();
        }

        private void grdFiles_ItemDataBound(object sender, System.Web.UI.WebControls.DataGridItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Item
                || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                int index = e.Item.Cells[(int)gridColomns.FileName].Text.LastIndexOf(@"\");
                if (index > 0)
                {
                    LinkButton lb = (LinkButton)e.Item.FindControl("lnk");
                    lb.Text = e.Item.Cells[(int)gridColomns.FileName].Text.Substring(index + 1);
                }
            }
        }

        private void grdFiles_ItemCommand(object source, System.Web.UI.WebControls.DataGridCommandEventArgs e)
        {
            if (e.CommandName == "Edit")
            {
                string filePath = /*Server.MapPath(*/e.Item.Cells[(int)gridColomns.FileName].Text;
                LinkButton lb = (LinkButton)e.Item.FindControl("lnk");

                Response.Cache.SetExpires(DateTime.Now.AddSeconds(-1));
                Response.Cache.SetCacheability(HttpCacheability.Private);
                Response.AppendHeader("content-disposition", "attachment; filename=" + lb.Text);
                //Response.AppendHeader("content-disposition", "attachment; filename="+ID+e.Item.Cells[(int)gridColomns.Type].Text);

                Response.Charset = "";
                Response.TransmitFile(filePath);
                //notes:here send message if file is tracking
                //				FilePMData fPMd = FilePMDAL.Load(ID);
                //				if(fPMd.MailForDownload&&LoggedUser.FullName!=fPMd.CreatedBy)
                //				{
                //					UserInfoPM MailToUser = UsersData.SelectUserPMDownLoadByName(fPMd.CreatedBy);
                //					MailBO.PMSendMailDownload(LoggedUser.Account,fPMd,MailToUser.Mail);
                //				}
                //			
                //				//notes:Save Log in DateBase
                //				ProjectLogData pld = new ProjectLogData(-1,ID,DateTime.Now,false,LoggedUser.FullName);
                //				ProjectLogDAL.Save(pld);
                Response.End();
                return;
            }
        }

        protected void btnShowArchive_Click(object sender, System.EventArgs e)
        {
            ShowArchive_Click();
        }

        private void ShowArchive_Click()
        {
            if (grdFiles.Items.Count == 0)
            {
                grid.Visible = false;
                lbArchive.Visible = false;
                btnShowArchive.Visible = false;
                return;
            }
            if (grid.Visible)
            {
                grid.Visible = false;
                lbArchive.Visible = false;
                btnShowArchive.Text = UIHelpers.GetText(Language, "btnShowArchive_Text");
            }
            else
            {
                grid.Visible = true;
                lbArchive.Visible = true;
                btnShowArchive.Text = UIHelpers.GetText(Language, "btnShowArchiveHide_Text");
            }
        }

        private static Random random = new Random();
        public static string RandomString(int length)
        {
            const string chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
            return new string(Enumerable.Repeat(chars, length)
              .Select(s => s[random.Next(s.Length)]).ToArray());
        }


		#region Session
//		protected int FileVersionSession
//		{
//			get 
//			{
//				if (this.Session["fileVersion"] != null&&this.Session["fileVersion"] is int)
//					return (int)this.Session["fileVersion"];
//				else
//					return -1;
//			}
//			set 
//			{
//				this.Session["fileVersion"] = value;
//			}
//		}
		#endregion
	}

 
}
