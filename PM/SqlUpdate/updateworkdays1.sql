USE [asatimesheet]
GO
/****** Object:  StoredProcedure [dbo].[WorkTimesSelectProjectsForDay]    Script Date: 10/3/2017 1:19:04 PM ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER ON
GO






ALTER    PROCEDURE [dbo].[WorkTimesSelectProjectsForDay]

@Date smalldatetime,
@IsBuildingTypeGradoustrShow bit = null

AS

SELECT    WorkTimes.ProjectID, 
	    RTRIM([ProjectName]+ ' '+isnull([ProjectCode],'')) as ProjectName, 
	    HasActivity,
	    HasDefaultWorkTimes
FROM       WorkTimes INNER JOIN Projects 
	    ON WorkTimes.ProjectID = Projects.ProjectID
WHERE WorkDate = @Date --and concluded=0
 and (@IsBuildingTypeGradoustrShow is null or @IsBuildingTypeGradoustrShow = 1 or BuildingTypeID<>1)
--and IsVisible=1
UNION
(
SELECT
	[ProjectID],
	RTRIM([ProjectName]+ ' '+isnull([ProjectCode],'')) as ProjectName,
	[HasActivity],
	HasDefaultWorkTimes
FROM
	Projects
	WHERE IsActive = 1
	and IsVisible=1 --and concluded=0
 and (@IsBuildingTypeGradoustrShow is null or @IsBuildingTypeGradoustrShow = 1 or BuildingTypeID<>1)

)

ORDER BY ProjectName


