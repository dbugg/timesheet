using System;
using System.Data;
using System.Data.SqlClient; 
using System.Collections;
using Microsoft.ApplicationBlocks.Data;

namespace Asa.Timesheet.Data
{
	public class ContentDAL
	{
		/// <summary>
		/// Loads a ContentData object from the database using the given ContentID
		/// </summary>
		/// <param name="contentID">The primary key value</param>
		/// <returns>A ContentData object</returns>
		public static ContentData Load(int contentID)
		{
			SqlParameter[] parameterValues = new SqlParameter[] { new SqlParameter("@ContentID", contentID) };
			using (SqlDataReader reader = SqlHelper.ExecuteReader(CommonDAL.ConnectionString, CommandType.StoredProcedure, "ContentsSelProc", parameterValues))
			{
				if (reader.Read())
				{
					ContentData result = new ContentData();
					LoadFromReader(reader,result);
					return result;
				}
				else
					return null;
			}
		}

		/// <summary>
		/// Loads a ContentData object from the database using the given where clause
		/// </summary>
		/// <param name="whereClause">The filter expression for the query</param>
		/// <returns>A ContentData object</returns>
		public static ContentData Load(string whereClause)
		{
			SqlParameter[] parameterValues = new SqlParameter[] { new SqlParameter("@WhereClause", whereClause) };
			using (SqlDataReader reader = SqlHelper.ExecuteReader(CommonDAL.ConnectionString, CommandType.StoredProcedure, "ContentsListProc", parameterValues))
			{
				if (reader.Read())
				{
					ContentData result = new ContentData();
					LoadFromReader(reader,result);
					return result;
				}
				else
					return null;
			}
		}
		public static int Save( ContentData source)
		{
			if (source.ContentID == -1)
				return Insert(source);
			else
				return Update(source);	
		}

		public static void Delete( ContentData source)
		{
			Delete(source.ContentID);
		}
		/// <summary>
		/// Loads a collection of ContentData objects from the database.
		/// </summary>
		/// <returns>A collection containing all of the ContentData objects in the database.</returns>
		public static ContentsVector LoadCollection()
		{
			SqlParameter[] parms = new SqlParameter[] {};
			return LoadCollection("ContentsListProc", parms);
		}
	  
		/// <summary>
		/// Saves a collection of ContentData objects to the database.
		/// </summary>
		/// <returns>A collection containing all of the ContentData objects in the database.</returns>
		public static  void  SaveCollection(ContentsVector col,string spName, SqlParameter[] parms)
		{
			ContentsVector colOld = LoadCollection(spName,parms);
			UpdateCollection(colOld,col);
		}
		public static  void  SaveCollection(ContentsVector col)
		{
			ContentsVector colOld = LoadCollection();
			UpdateCollection(colOld,col);
		}
		public static  void  SaveCollection(ContentsVector col, string whereClause )
		{
			ContentsVector colOld = LoadCollection(whereClause);
			UpdateCollection(colOld,col);
		}
		/// <summary>
		/// Loads a collection of ContentData objects from the database.
		/// </summary>
		/// <returns>A collection containing all of the ContentData objects in the database.</returns>
		public static ContentsVector LoadCollection(string whereClause)
		{
			SqlParameter[] parms = new SqlParameter[] { new SqlParameter("@WhereClause", whereClause) };
			return LoadCollection("ContentsListProc", parms);
		}

		/// <summary>
		/// Loads a collection of ContentData objects from the database.
		/// </summary>
		/// <returns>A collection containing all of the ContentData objects in the database.</returns>
		public static ContentsVector LoadCollection(string spName, SqlParameter[] parms)
		{
			ContentsVector result = new ContentsVector();
			using (SqlDataReader reader = SqlHelper.ExecuteReader(CommonDAL.ConnectionString, CommandType.StoredProcedure, spName, parms))
			{
				while (reader.Read())
				{
					ContentData tmp = new ContentData();
					LoadFromReader(reader, tmp);
					result.Add(tmp);
				}
			}
			return result;
		}
		/// <summary>
		/// Deletes a ContentData object from the database.
		/// </summary>
		/// <param name="contentID">The primary key value</param>
		public static void Delete(int contentID)
		{
			SqlParameter[] parameterValues = new SqlParameter[] { new SqlParameter("@ContentID", contentID) };
			SqlHelper.ExecuteNonQuery(CommonDAL.ConnectionString, CommandType.StoredProcedure, "ContentsDelProc", parameterValues);
		}

		public static void LoadFromReader(SqlDataReader reader, ContentData target)
		{
			if (reader != null && !reader.IsClosed)
			{
				//target.Key = reader.GetInt32(0);
				if (!reader.IsDBNull(0)) target.ContentID = reader.GetInt32(0);
				if (!reader.IsDBNull(1)) target.ProjectID = reader.GetInt32(1);
				if (!reader.IsDBNull(2)) target.ContentTypeID = reader.GetInt32(2);
				if (!reader.IsDBNull(3)) target.Kota = reader.GetDecimal(3);
				if (!reader.IsDBNull(4)) target.Area = reader.GetDecimal(4);
				if (!reader.IsDBNull(5)) target.ContentText = reader.GetString(5);
				if (!reader.IsDBNull(6)) target.ContentType = reader.GetString(6);
				 if (!reader.IsDBNull(7)) target.Scale = reader.GetString(7);
				 if (!reader.IsDBNull(8)) target.BuildingNumber = reader.GetInt32(8);
				if (!reader.IsDBNull(9)) target.Signature = reader.GetString(9);
				if (!reader.IsDBNull(10)) target.ContentCode = reader.GetString(10);
				if (!reader.IsDBNull(11)) target.ContentRevision = reader.GetString(11);
				if (!reader.IsDBNull(12)) target.ContentOrder = reader.GetInt32(12);
				if(!reader.IsDBNull(13)) target.ContentFinishedPhase1 = reader.GetInt32(13);
				if(!reader.IsDBNull(14)) target.ContentFinishedPhase2 = reader.GetInt32(14);
				if(!reader.IsDBNull(15)) target.ContentFinishedPhase3 = reader.GetInt32(15);
				if(!reader.IsDBNull(16)) target.ContentFinishedPhase4 = reader.GetInt32(16);

			}
		}
		
	  
		public static int Insert( ContentData source)
		{
			SqlParameter[] parameterValues = GetInsertParameterValues(source);
			SqlHelper.ExecuteNonQuery(CommonDAL.ConnectionString, CommandType.StoredProcedure, "ContentsInsProc", parameterValues);
			source.ContentID = (int) parameterValues[0].Value;
			return source.ContentID;
		}

		public static int Update( ContentData source)
		{
			SqlParameter[] parameterValues = GetUpdateParameterValues(source);
			SqlHelper.ExecuteNonQuery(CommonDAL.ConnectionString, CommandType.StoredProcedure,
				"ContentsUpdProc", parameterValues);
			return source.ContentID;
		}
		private static void UpdateCollection(ContentsVector colOld,ContentsVector col)
		{
			// Delete all old items
			foreach( ContentData itemOld in colOld)
			{
				if(!ExistsKey(itemOld.ContentID, col))
					Delete(itemOld.ContentID);
			}
			foreach( ContentData item in col)
			{
				if(item.ContentID <= -1)
					Insert(item);
				else
					Update(item);
			}
		}
		private static bool ExistsKey(int Key, ContentsVector col)
		{
			foreach( ContentData item in col)
				if(item.ContentID==Key)
					return true;
			return false;
		}

		private static SqlParameter GetSqlParameter(string name, ParameterDirection direction, object value)
		{
			SqlParameter p = new SqlParameter(name, value);
			p.Direction = direction;
			return p;
		}

		private static SqlParameter[] GetInsertParameterValues( ContentData source)
		{
			SqlParameter[] prams = new SqlParameter[16];
			prams[0] = GetSqlParameter("@ContentID", ParameterDirection.Output, source.ContentID);
			prams[1] = GetSqlParameter("@ProjectID", ParameterDirection.Input, source.ProjectID);
			prams[2] = GetSqlParameter("@ContentTypeID", ParameterDirection.Input, source.ContentTypeID);
			prams[3] = GetSqlParameter("@Kota", ParameterDirection.Input, source.Kota);
			prams[4] = GetSqlParameter("@Area", ParameterDirection.Input, source.Area);
			prams[5] = GetSqlParameter("@ContentText", ParameterDirection.Input, source.ContentText);
			prams[6] = GetSqlParameter("@Scale", ParameterDirection.Input, source.Scale);
			prams[7] = GetSqlParameter("@BuildingNumber", ParameterDirection.Input, source.BuildingNumber);
			prams[8] = GetSqlParameter("@Signature", ParameterDirection.Input, source.Signature);
			prams[9] = GetSqlParameter("@ContentCode", ParameterDirection.Input, source.ContentCode);
			prams[10] = GetSqlParameter("@ContentRevision", ParameterDirection.Input, source.ContentRevision);
			prams[11] = GetSqlParameter("@ContentOrder", ParameterDirection.Input, source.ContentOrder);
			prams[12] = GetSqlParameter("@ContentFinishedPhase1", ParameterDirection.Input, source.ContentFinishedPhase1);
			prams[13] = GetSqlParameter("@ContentFinishedPhase2", ParameterDirection.Input, source.ContentFinishedPhase2);
			prams[14] = GetSqlParameter("@ContentFinishedPhase3", ParameterDirection.Input, source.ContentFinishedPhase3);
			prams[15] = GetSqlParameter("@ContentFinishedPhase4", ParameterDirection.Input, source.ContentFinishedPhase4);
			return prams;
		}

		private static SqlParameter[] GetUpdateParameterValues( ContentData source)
		{
			return new SqlParameter[] {
										  new SqlParameter("@ContentID", source.ContentID),
										  new SqlParameter("@ProjectID", source.ProjectID),
										  new SqlParameter("@ContentTypeID", source.ContentTypeID),
										  new SqlParameter("@Kota", source.Kota),
										  new SqlParameter("@Area", source.Area),
										  new SqlParameter("@ContentText", source.ContentText),
                      new SqlParameter("@Scale", source.Scale),
				new SqlParameter("@BuildingNumber", source.BuildingNumber),
				new SqlParameter("@Signature", source.Signature),
				new SqlParameter("@ContentCode", source.ContentCode),
				new SqlParameter("@ContentRevision", source.ContentRevision),
				new SqlParameter("@ContentOrder", source.ContentOrder),
				new SqlParameter("@ContentFinishedPhase1", source.ContentFinishedPhase1),
				new SqlParameter("@ContentFinishedPhase2", source.ContentFinishedPhase2),
				new SqlParameter("@ContentFinishedPhase3", source.ContentFinishedPhase3),
				new SqlParameter("@ContentFinishedPhase4", source.ContentFinishedPhase4),
									  };
		}
	}
}   

