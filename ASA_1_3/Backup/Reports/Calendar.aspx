﻿<%@ Register TagPrefix="uc1" TagName="PageHeader" Src="../UserControls/PageHeader.ascx" %>
<%@ Page language="c#" Codebehind="Calendar.aspx.cs" AutoEventWireup="false" Inherits="Asa.Timesheet.WebPages.Reports.Calendar" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
	<head runat="server">
		<title>EmptyHours</title>
		<meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
		<meta content="C#" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<LINK href="../styles/timesheet.css" type="text/css" rel="stylesheet">
	</HEAD>
	<body bottomMargin="0" leftMargin="0" topMargin="0" rightMargin="0">
		<form id="Form1" method="post" runat="server">
			<table height="100%" cellSpacing="0" cellPadding="0" width="100%" border="0">
				<tr height="1">
					<td><uc1:pageheader id="header" runat="server"></uc1:pageheader></td>
				</tr>
				<tr>
					<td>
						<table height="100%" cellSpacing="0" cellPadding="0" width="100%" border="0">
							<tr>
								<td class="menuLeft"><asp:placeholder id="menuHolder" runat="server"></asp:placeholder></td>
								
								<td style="PADDING-RIGHT: 5px; PADDING-LEFT: 5px; PADDING-TOP: 3px" vAlign="top" noWrap>
									<TABLE id="tblInfo" height="30" cellSpacing="0" cellPadding="3" width="880" border="0">
										<TR>
											<td vAlign="middle" noWrap><asp:label id="lblInfo" runat="server" CssClass="InfoLabel" EnableViewState="False"></asp:label></TD>
										</TR>
									</TABLE>
									<TABLE id="Table3" cellSpacing="0" cellPadding="0" width="880" border="0">
										<TR>
											<td style="HEIGHT: 5px" colSpan="2"></TD>
										</TR>
										<TR>
											<td colSpan="2"><IMG height="1" src="../images/dot.gif" width="100%"></TD>
										</TR>
										<TR>
											<td colSpan="2" height="23" class="HoursGridStatusLabel" style="HEIGHT: 23px" align="left">Почивни/работни 
												дни</td>
										</TR>
										<TR>
											<td style="HEIGHT: 135px">
												<TABLE id="Table4" cellSpacing="0" cellPadding="3" border="0">
													<TR id="SelectDateRow">
														<td style="WIDTH: 117px; HEIGHT: 4px" vAlign="top" align="right"><asp:label id="lblStartDay" runat="server" CssClass="EnterDataLabel">Ден:</asp:label><asp:label id="lblSelStartDate" runat="server" ForeColor="SaddleBrown" Width="100px" Font-Bold="True"></asp:label></TD>
														<td style="WIDTH: 72px" vAlign="top">
															<TABLE id="Table1" cellSpacing="0" cellPadding="0" border="0">
																<TR>
																	<td><asp:calendar id="calStartDate" runat="server" ForeColor="#222222" Width="240px" SelectionMode="DayWeek"
																			BorderColor="#999999" CellPadding="0" BorderWidth="1px" BackColor="White" Height="240px" NextMonthText=""
																			PrevMonthText="" DayNameFormat="FirstLetter" SelectWeekText="+"
																			FirstDayOfWeek="Monday" BorderStyle="Solid" CssClass="aspCalendar" NextPrevFormat="ShortMonth" Font-Bold="True" Font-Size="11pt">
																			<TodayDayStyle ForeColor="#C73B2E" BackColor="White" BorderColor="#C73B2E" 
                                                                                BorderStyle="Solid" BorderWidth="2px" Height="32px" Width="32px"></TodayDayStyle>
																			<SelectorStyle Font-Size="10pt" Font-Names="Verdana" ForeColor="SaddleBrown" 
                                                                                BorderStyle="None"></SelectorStyle>
																			<DayStyle Font-Size="10pt" Font-Bold="False" BorderStyle="Dotted" 
                                                                                BorderColor="#CCCCCC" BorderWidth="1px" Height="30px" Width="32px"></DayStyle>
																			<NextPrevStyle Font-Bold="True" Wrap="False" HorizontalAlign="Center" 
                                                                                Height="16px" BorderStyle="None" VerticalAlign="Middle" Font-Size="9pt" 
                                                                                Font-Underline="True"></NextPrevStyle>
																			<DayHeaderStyle Font-Size="9pt" Height="1px" ForeColor="SaddleBrown"></DayHeaderStyle>
																			<SelectedDayStyle Font-Bold="True" ForeColor="#C73B2E" BorderColor="#C73B2E" 
                                                                                BackColor="White" BorderStyle="Solid" BorderWidth="2px"
                                                                                Font-Size="11pt" Height="32px" Width="32px"></SelectedDayStyle>
																			<TitleStyle Font-Size="13pt" Font-Bold="True" ForeColor="#C73B2E" 
                                                                                BackColor="White" HorizontalAlign="Center"></TitleStyle>
																			<OtherMonthDayStyle ForeColor="Gray" BackColor="#FDFDFD" Font-Size="6pt"></OtherMonthDayStyle>
																		    <WeekendDayStyle Font-Bold="True" Font-Italic="False" 
                                                                                ForeColor="#C73B2E" />
																		</asp:calendar></TD>
																	<td vAlign="top" noWrap colSpan="1" rowSpan="1"></TD>
																</TR>
															</TABLE>
														</TD>
														<td style="WIDTH: 145px" vAlign="top" align="center">
															<asp:RadioButtonList id="rbb" runat="server" CssClass="EnterDataLabel" Width="135px">
																<asp:ListItem Value="0">Денят е работен</asp:ListItem>
																<asp:ListItem Value="1">Денят е почивен</asp:ListItem>
															</asp:RadioButtonList><BR>
															<asp:button id="btnOK" tabIndex="1" runat="server" CssClass="actionButton" Text="Запази"></asp:button></TD>
														<td style="WIDTH: 278px; HEIGHT: 4px" vAlign="top"><BR>
														</TD>
													</TR>
												</TABLE>
											</TD>
										</TR>
										<TR>
											<td style="HEIGHT: 31px" colSpan="2"></TD>
										</TR>
										<TR>
											<td colSpan="2" style="HEIGHT: 28px"><IMG height="1" src="../images/dot.gif" width="100%"></TD>
										</TR>
										<TR>
											<td colSpan="2" height="24" class="HoursGridStatusLabel" style="HEIGHT: 24px">Брой 
												дни за справката по Е-мейл &nbsp;за непопълнено работно време</td>
										</TR>
										<tr>
											<td>
												<table cellSpacing="0" cellPadding="3" border="0" style="WIDTH: 490px">
													<TR>
														<td style="WIDTH: 97px" vAlign="top">
															<asp:TextBox id="txtDates" runat="server" Width="96px"></asp:TextBox></TD>
														<td style="WIDTH: 230px" vAlign="top">
															<asp:checkbox id="ckDates" runat="server" CssClass="EnterDataLabel" Width="144px" Text="За целия период"
																AutoPostBack="True"></asp:checkbox></TD>
														<td style="WIDTH: 148px" vAlign="top" align="center">
															<asp:button id="Button1" tabIndex="1" runat="server" CssClass="actionButton" Text="Запази"></asp:button></TD>
													</TR>
												</table>
											</td>
										</tr>
										<TR>
											<td style="HEIGHT: 5px" colSpan="2"></TD>
										</TR>
										<TR>
											<td colSpan="2"><IMG height="1" src="../images/dot.gif" width="100%"></TD>
										</TR>
										<TR>
											<td colSpan="2" height="3"></TD>
										</TR>
										<TR>
											<td colSpan="2"><IMG height="1" src="images/dot.gif" width="100%"></TD>
										</TR>
									</TABLE>
									<br>
									<br>
									<TABLE id="Table2" cellSpacing="0" cellPadding="0" width="100%" border="0">
										<TR>
											<td align="center"></TD>
										</TR>
									</TABLE>
									<br>
									<br>
								</td>
							</tr>
						</table>
					</td>
				</tr>
			</table>
		</form>
	</body>
</HTML>
