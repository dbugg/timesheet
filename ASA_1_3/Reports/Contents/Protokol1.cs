using System;
using System.Data;
using Asa.Timesheet.Data.Reports;
using Asa.Timesheet.Data;
using GrapeCity.ActiveReports;
using GrapeCity.ActiveReports.Controls;
using GrapeCity.ActiveReports.SectionReportModel;
using GrapeCity.ActiveReports.Document.Section;
using GrapeCity.ActiveReports.Document;

namespace Asa.Timesheet.WebPages.Reports
{
    public class Protokol : GrapeCity.ActiveReports.SectionReport
	{
		//const int Constants.ArchitectConst = 8;
		//public const int Constants.ArchitectConstMaxPlus = 2;
		int _projectID = 0;
		bool _isRtf = false;
		string _userName = String.Empty;
		string _txtClient = String.Empty;
		string _txtProtokol = String.Empty;
		string _txtPapki = String.Empty;
		ChecksVector _checks = null;
		string _phase = "";
        public Protokol(int projectID, string userName, bool IsRtf, string s1, string s2, string s3, ChecksVector checks, string phase)
        {
            _isRtf = IsRtf;
            _projectID = projectID;
            _userName = userName;
            _txtClient = s1;
            _txtProtokol = s2;
            _txtPapki = s3;
            _checks = checks;
            _phase = phase;
            InitializeComponent();
            this.ReportEnd += new EventHandler(Contents_ReportEnd);
        }

        private void Contents_ReportStart(object sender, System.EventArgs eArgs)
        {
            DataSet dsReport = ContentsReportData.SelectContentsReportData(_projectID, -1);
            if (_phase != "")
            {
                for (int i = 0; i < dsReport.Tables[1].Rows.Count; i++)
                {
                    string code = dsReport.Tables[1].Rows[i][10].ToString();
                    if (code.Length != 0)
                    {
                        code = string.Concat(_phase, ".", code);
                        dsReport.Tables[1].Rows[i][10] = code;
                    }
                }
                for (int i = 0; i < dsReport.Tables[2].Rows.Count; i++)
                {
                    string code = dsReport.Tables[2].Rows[i][10].ToString();
                    if (code.Length != 0)
                    {
                        code = string.Concat(_phase, ".", code);
                        dsReport.Tables[2].Rows[i][10] = code;
                    }
                }
            }

            this.DataSource = dsReport.Tables[0];
            bool IsCode = false;
            bool IsOneBuilding = IsMaxBuildingOne(_projectID);
            int CountofContents = dsReport.Tables[1].Rows.Count + dsReport.Tables[2].Rows.Count;
            foreach (DataRow dr in dsReport.Tables[1].Rows)
            {
                int _cid = int.Parse(dr[0].ToString());
                int index = _checks.IndexOf(_cid, true);
                if (index == -1)
                { IsCode = true; break; }
                bool ischecked = _checks[index].IsChecked;
                if (ischecked)
                { IsCode = true; break; }
            }

            if (dsReport.Tables[1].Rows.Count > 0)
            {
                this.SubReport1.Report = new ContentsType1Subreport(IsCode, _checks, IsOneBuilding, CountofContents);
                this.SubReport1.Report.DataSource = dsReport.Tables[1];
            }
            else this.SubReport1.Visible = false;
            foreach (DataRow dr in dsReport.Tables[2].Rows)
            {
                int _cid = int.Parse(dr[0].ToString());
                int index = _checks.IndexOf(_cid, true);
                if (index == -1)
                { IsCode = true; break; }
                bool ischecked = _checks[index].IsChecked;
                if (ischecked)
                { IsCode = true; break; }
            }
            if (dsReport.Tables[2].Rows.Count > 0)
            {
                this.SubReport2.Report = new ContentsType2Subreport(IsCode, _checks, IsOneBuilding);
                this.SubReport2.Report.DataSource = dsReport.Tables[2];
            }
            else this.SubReport2.Visible = false;
            txtClient.Text = _txtClient;
            txtProtokol.Text = _txtProtokol;
            txtPapki.Text = _txtPapki;
        }

        private void Contents_FetchData(object sender, FetchEventArgs eArgs)
        {
            if (eArgs.EOF) return;


        }

        private void ReportFooter_Format(object sender, System.EventArgs eArgs)
        {

        }

		#region ActiveReports Designer generated code



















        public void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Protokol));
            this.Detail = new GrapeCity.ActiveReports.SectionReportModel.Detail();
            this.ReportHeader = new GrapeCity.ActiveReports.SectionReportModel.ReportHeader();
            this.ReportFooter = new GrapeCity.ActiveReports.SectionReportModel.ReportFooter();
            this.PageHeader = new GrapeCity.ActiveReports.SectionReportModel.PageHeader();
            this.PageFooter = new GrapeCity.ActiveReports.SectionReportModel.PageFooter();
            this.txtReportTitle = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.lblObekt = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.TextBox1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.SubReport1 = new GrapeCity.ActiveReports.SectionReportModel.SubReport();
            this.SubReport2 = new GrapeCity.ActiveReports.SectionReportModel.SubReport();
            this.TextBox2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.TextBox3 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtClient = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.TextBox5 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.TextBox6 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtProtokol = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtPapki = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.Label1 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label2 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            ((System.ComponentModel.ISupportInitialize)(this.txtReportTitle)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblObekt)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TextBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TextBox2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TextBox3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtClient)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TextBox5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TextBox6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtProtokol)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPapki)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            // 
            // Detail
            // 
            this.Detail.ColumnSpacing = 0F;
            this.Detail.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
						this.lblObekt,
						this.TextBox1,
						this.SubReport1,
						this.SubReport2,
						this.TextBox2,
						this.TextBox3,
						this.txtClient,
						this.TextBox5,
						this.TextBox6,
						this.txtProtokol,
						this.txtPapki});
            this.Detail.Height = 4.614583F;
            this.Detail.Name = "Detail";
            // 
            // ReportHeader
            // 
            this.ReportHeader.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
						this.txtReportTitle});
            this.ReportHeader.Height = 0.7868056F;
            this.ReportHeader.Name = "ReportHeader";
            // 
            // ReportFooter
            // 
            this.ReportFooter.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
						this.Label1,
						this.Label2});
            this.ReportFooter.Height = 1.725F;
            this.ReportFooter.KeepTogether = true;
            this.ReportFooter.Name = "ReportFooter";
            // 
            // PageHeader
            // 
            this.PageHeader.Height = 0.25F;
            this.PageHeader.Name = "PageHeader";
            // 
            // PageFooter
            // 
            this.PageFooter.Height = 0F;
            this.PageFooter.Name = "PageFooter";
            // 
            // txtReportTitle
            // 
            this.txtReportTitle.Height = 0.2755906F;
            this.txtReportTitle.Left = 0F;
            this.txtReportTitle.Name = "txtReportTitle";
            this.txtReportTitle.Style = "font-size: 15pt; font-weight: bold; text-align: center";
            this.txtReportTitle.Text = "??????-???????????? ????????";
            this.txtReportTitle.Top = 0F;
            this.txtReportTitle.Width = 7.086611F;
            // 
            // lblObekt
            // 
            this.lblObekt.Height = 0.4375F;
            this.lblObekt.Left = 0.6875F;
            this.lblObekt.Name = "lblObekt";
            this.lblObekt.Style = "font-size: 12pt";
            this.lblObekt.Text = "????, ? ??. ?????, ? ????? ?? �?????? ????????? ????????? ???�, ???. ??????????? " +
                "????, ?4? ?????:";
            this.lblObekt.Top = 0F;
            this.lblObekt.Width = 5.982366F;
            // 
            // TextBox1
            // 
            this.TextBox1.Height = 0.4375F;
            this.TextBox1.Left = 0.6875F;
            this.TextBox1.Name = "TextBox1";
            this.TextBox1.Style = "font-size: 12pt";
            this.TextBox1.Text = "1.\t�?????? ????????? ????????? ???� ??? ???????? ? ??. ?????, ?????????????? ?? ?" +
                "????????? ?. ?????????, ????";
            this.TextBox1.Top = 0.5F;
            this.TextBox1.Width = 5.982366F;
            // 
            // SubReport1
            // 
            this.SubReport1.CloseBorder = false;
            this.SubReport1.Height = 0.1968504F;
            this.SubReport1.Left = 0.5905511F;
            this.SubReport1.Name = "SubReport1";
            this.SubReport1.Report = null;
            this.SubReport1.Top = 4F;
            this.SubReport1.Width = 6.299212F;
            // 
            // SubReport2
            // 
            this.SubReport2.CloseBorder = false;
            this.SubReport2.Height = 0.1968504F;
            this.SubReport2.Left = 0.5905511F;
            this.SubReport2.Name = "SubReport2";
            this.SubReport2.Report = null;
            this.SubReport2.Top = 4.375F;
            this.SubReport2.Width = 6.299212F;
            // 
            // TextBox2
            // 
            this.TextBox2.Height = 0.25F;
            this.TextBox2.Left = 0.6875F;
            this.TextBox2.Name = "TextBox2";
            this.TextBox2.Style = "font-size: 12pt";
            this.TextBox2.Text = "�������.";
            this.TextBox2.Top = 0.9375F;
            this.TextBox2.Width = 5.982366F;
            // 
            // TextBox3
            // 
            this.TextBox3.Height = 0.25F;
            this.TextBox3.Left = 0.6875F;
            this.TextBox3.Name = "TextBox3";
            this.TextBox3.Style = "font-size: 12pt";
            this.TextBox3.Text = "?? ???? ?????? - ??????????";
            this.TextBox3.Top = 1.1875F;
            this.TextBox3.Width = 5.982366F;
            // 
            // txtClient
            // 
            this.txtClient.Height = 0.5625F;
            this.txtClient.Left = 0.6875F;
            this.txtClient.Name = "txtClient";
            this.txtClient.Style = "font-size: 12pt";
            this.txtClient.Top = 1.5F;
            this.txtClient.Width = 6F;
            // 
            // TextBox5
            // 
            this.TextBox5.Height = 0.25F;
            this.TextBox5.Left = 0.6875F;
            this.TextBox5.Name = "TextBox5";
            this.TextBox5.Style = "font-size: 12pt";
            this.TextBox5.Text = "�������.";
            this.TextBox5.Top = 2.0625F;
            this.TextBox5.Width = 5.982366F;
            // 
            // TextBox6
            // 
            this.TextBox6.Height = 0.25F;
            this.TextBox6.Left = 0.6875F;
            this.TextBox6.Name = "TextBox6";
            this.TextBox6.Style = "font-size: 12pt";
            this.TextBox6.Text = "?? ????? ?????? � ??????????";
            this.TextBox6.Top = 2.3125F;
            this.TextBox6.Width = 5.982366F;
            // 
            // txtProtokol
            // 
            this.txtProtokol.Height = 0.5625F;
            this.txtProtokol.Left = 0.6875F;
            this.txtProtokol.Name = "txtProtokol";
            this.txtProtokol.Style = "font-size: 12pt";
            this.txtProtokol.Top = 2.6875F;
            this.txtProtokol.Width = 6F;
            // 
            // txtPapki
            // 
            this.txtPapki.Height = 0.25F;
            this.txtPapki.Left = 0.6875F;
            this.txtPapki.Name = "txtPapki";
            this.txtPapki.Style = "font-size: 12pt; vertical-align: top";
            this.txtPapki.Top = 3.375F;
            this.txtPapki.Width = 6F;
            // 
            // Label1
            // 
            this.Label1.Height = 0.2738189F;
            this.Label1.HyperLink = null;
            this.Label1.Left = 0.5905511F;
            this.Label1.Name = "Label1";
            this.Label1.Style = "font-size: 11pt";
            this.Label1.Text = "??????:";
            this.Label1.Top = 1.254922F;
            this.Label1.Width = 1.346949F;
            // 
            // Label2
            // 
            this.Label2.Height = 0.2738189F;
            this.Label2.HyperLink = null;
            this.Label2.Left = 5.125F;
            this.Label2.Name = "Label2";
            this.Label2.Style = "font-size: 11pt";
            this.Label2.Text = "?????:";
            this.Label2.Top = 1.25F;
            this.Label2.Width = 1.346949F;
            // 
            // ActiveReport1
            // 
            this.MasterReport = false;
            this.PageSettings.PaperHeight = 11F;
            this.PageSettings.PaperWidth = 8.5F;
            this.PrintWidth = 7.086611F;
            this.Sections.Add(this.ReportHeader);
            this.Sections.Add(this.PageHeader);
            this.Sections.Add(this.Detail);
            this.Sections.Add(this.PageFooter);
            this.Sections.Add(this.ReportFooter);
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule(resources.GetString("$this.StyleSheet"), "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: inherit; font-style: inherit; font-variant: inherit; font-weight: bo" +
                        "ld; font-size: 16pt; font-size-adjust: inherit; font-stretch: inherit", "Heading1", "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: Times New Roman; font-style: italic; font-variant: inherit; font-wei" +
                        "ght: bold; font-size: 14pt; font-size-adjust: inherit; font-stretch: inherit", "Heading2", "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: inherit; font-style: inherit; font-variant: inherit; font-weight: bo" +
                        "ld; font-size: 13pt; font-size-adjust: inherit; font-stretch: inherit", "Heading3", "Normal"));
            ((System.ComponentModel.ISupportInitialize)(this.txtReportTitle)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblObekt)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TextBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TextBox2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TextBox3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtClient)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TextBox5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TextBox6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtProtokol)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPapki)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();

            // Attach Report Events
            this.ReportStart += new System.EventHandler(this.Contents_ReportStart);
            this.FetchData += new GrapeCity.ActiveReports.SectionReport.FetchEventHandler(this.Contents_FetchData);
            this.ReportFooter.Format += new System.EventHandler(this.ReportFooter_Format);
        }

		#endregion

        private void Contents_ReportEnd(object sender, EventArgs e)
        {
            if (_isRtf)
                return;
            Logo logo = new Logo();
            logo.Run();

            for (int i = 0; i < this.Document.Pages.Count; i++)
                this.Document.Pages[i].Overlay(logo.Document.Pages[0]);
        }
        private bool IsMaxBuildingOne(int projectID)
        {
            ContentsVector cv1 = ContentDAL.LoadCollection("ContentsList0Proc", SQLParms.CreateContentsList0Proc(projectID));
            ContentsVector[] cv2 = new ContentsVector[Constants.ArchitectConst + Constants.ArchitectConstMaxPlus + 1];
            for (int j = 1; j <= Constants.ArchitectConst; j++)
            {
                ContentsVector cvFilter = ContentDAL.LoadCollection("ContentsListArchitectProc", SQLParms.CreateContentsListFilterProc(projectID, j));
                cv2[j - 1] = cvFilter;
            }
            ContentsVector cv3 = ContentDAL.LoadCollection("ContentsList8Proc", SQLParms.CreateContentsList0Proc(projectID));
            int nMax = cv1.GetMaxBuilding();
            for (int j = 1; j <= Constants.ArchitectConst; j++)
            {
                if (cv2[j - 1].GetMaxBuilding() > nMax)
                    nMax = cv2[j - 1].GetMaxBuilding();
            }
            if (cv3.GetMaxBuilding() > nMax)
                nMax = cv3.GetMaxBuilding();
            if (nMax < 2) return true;
            else return false;
        }

        private ReportHeader ReportHeader;
        private TextBox txtReportTitle;
        private PageHeader PageHeader;
        private Detail Detail;
        private TextBox lblObekt;
        private TextBox TextBox1;
        private SubReport SubReport1;
        private SubReport SubReport2;
        private TextBox TextBox2;
        private TextBox TextBox3;
        private TextBox txtClient;
        private TextBox TextBox5;
        private TextBox TextBox6;
        private TextBox txtProtokol;
        private TextBox txtPapki;
        private PageFooter PageFooter;
        private ReportFooter ReportFooter;
        private Label Label1;
        private Label Label2;
	}
}
