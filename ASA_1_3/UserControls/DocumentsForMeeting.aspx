﻿<%@ Page language="c#" Codebehind="DocumentsForMeeting.aspx.cs" AutoEventWireup="false" Inherits="Asa.Timesheet.WebPages.UserControls.DocumentsForMeeting" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
	<head runat="server">
		<title>DocumentsForMeeting</title>
		<meta name="GENERATOR" Content="Microsoft Visual Studio .NET 7.1">
		<meta name="CODE_LANGUAGE" Content="C#">
		<meta name="vs_defaultClientScript" content="JavaScript">
		<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5">
	</HEAD>
	<body>
		<form id="Form1" method="post" runat="server">
			<TABLE id="Table1" style="Z-INDEX: 101; LEFT: 8px; POSITION: absolute; TOP: 8px" cellSpacing="0"
				cellPadding="0" border="0">
				<TR>
					<td>
						<asp:label id="Label58" runat="server" CssClass="lblDiv1">Документи&nbsp;</asp:label></TD>
					<td>
						<asp:DataList id="dlDocs" runat="server" RepeatDirection="Horizontal">
							<ItemTemplate>
								<asp:HyperLink id="btnScan" runat="server" ToolTip="Сканиран документ" NavigateUrl='<%# GetURL((int)DataBinder.Eval(Container, "DataItem.ProjectDocumentID"))%>' Target=_blank>
									<img border="0" src="images/pdf.gif" /></asp:HyperLink>
								<br>
								</br>
								<asp:ImageButton id="btnDel" runat="server" Width="24px" Height="24px" ToolTip="Изтрий" ImageUrl="images/delete.gif"
									CommandName="Delete"></asp:ImageButton>
							</ItemTemplate>
						</asp:DataList></TD>
					<td><INPUT id="FileCtrl" type="file" name="File1" runat="server">
						<asp:Button id="btnScanDocAdd" runat="server" Text="Добави"></asp:Button></TD>
				</TR>
			</TABLE>
		</form>
	</body>
</HTML>
