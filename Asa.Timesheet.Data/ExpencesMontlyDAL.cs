using System;
using System.Data;
using System.Data.SqlClient; 
using System.Collections;
using Microsoft.ApplicationBlocks.Data;

namespace Asa.Timesheet.Data
{
	/// <summary>
	/// Summary description for ExpencesMontlyDAL.
	/// </summary>
	public class ExpencesMontlyDAL
	{
		/// <summary>
		/// Loads a ExpencesMontlyData object from the database using the given ExpencesMontlyID
		/// </summary>
		/// <param name="ExpencesMontlyID">The primary key value</param>
		/// <returns>A ExpencesMontlyData object</returns>
		public static ExpencesMontlyData Load(int ExpenceID)
		{
			SqlParameter[] parameterValues = new SqlParameter[] { new SqlParameter("@ExpenceID", ExpenceID) };
			using (SqlDataReader reader = SqlHelper.ExecuteReader(CommonDAL.ConnectionString, CommandType.StoredProcedure, "ExpencesMontlySelProc", parameterValues))
			{
				if (reader.Read())
				{
					ExpencesMontlyData result = new ExpencesMontlyData();
					LoadFromReader(reader,result);
					return result;
				}
				else
					return null;
			}
		}
		/// <summary>
		/// Loads a ExpencesMontlyData object from the database using the given ExpencesMontlyID
		/// </summary>
		/// <param name="ExpencesMontlyID">The primary key value</param>
		/// <returns>A ExpencesMontlyData object</returns>
		public static ExpencesMontlyData Load(DateTime dt)
		{
			SqlParameter[] parameterValues = new SqlParameter[] { new SqlParameter("@MonthOfYear", dt) };
			using (SqlDataReader reader = SqlHelper.ExecuteReader(CommonDAL.ConnectionString, CommandType.StoredProcedure, "ExpencesMontlyListProc", parameterValues))
			{
				if (reader.Read())
				{
					ExpencesMontlyData result = new ExpencesMontlyData();
					LoadFromReader(reader,result);
					return result;
				}
				else
					return null;
			}
		}
		/// <summary>
		/// Loads a ExpencesMontlyData object from the database using the given where clause
		/// </summary>
		/// <param name="whereClause">The filter expression for the query</param>
		/// <returns>A ExpencesMontlyData object</returns>
		public static ExpencesMontlyData Load(string whereClause)
		{
			SqlParameter[] parameterValues = new SqlParameter[] { new SqlParameter("@WhereClause", whereClause) };
			using (SqlDataReader reader = SqlHelper.ExecuteReader(CommonDAL.ConnectionString, CommandType.StoredProcedure, "ExpencesMontlyListProc", parameterValues))
			{
				if (reader.Read())
				{
					ExpencesMontlyData result = new ExpencesMontlyData();
					LoadFromReader(reader,result);
					return result;
				}
				else
					return null;
			}
		}
		public static int Save( ExpencesMontlyData source)
		{
			if (source.ExpenceID == -1)
				return Insert(source);
			else
				return Update(source);	
		}

		public static void Delete( ExpencesMontlyData source)
		{
			Delete(source.ExpenceID);
		}
		/// <summary>
		/// Loads a collection of ExpencesMontlyData objects from the database.
		/// </summary>
		/// <returns>A collection containing all of the ExpencesMontlyData objects in the database.</returns>
		public static ExpencesMontliesVector LoadCollection()
		{
			SqlParameter[] parms = new SqlParameter[] {};
			return LoadCollection("ExpencesMontlyListProc", parms);
		}
		public static ExpencesMontliesVector LoadCollectionAll()
		{
			SqlParameter[] parms = new SqlParameter[] {};
			return LoadCollection("ExpencesMontlyListAllProc", parms);
		}
		/// <summary>
		/// Saves a collection of ExpencesMontlyData objects to the database.
		/// </summary>
		/// <returns>A collection containing all of the ExpencesMontlyData objects in the database.</returns>
		public static  void  SaveCollection(ExpencesMontliesVector col,string spName, SqlParameter[] parms)
		{
			ExpencesMontliesVector colOld = LoadCollection(spName,parms);
			UpdateCollection(colOld,col);
		}
		public static  void  SaveCollection(ExpencesMontliesVector col)
		{
			ExpencesMontliesVector colOld = LoadCollection();
			UpdateCollection(colOld,col);
		}
		public static  void  SaveCollection(ExpencesMontliesVector col, string whereClause )
		{
			ExpencesMontliesVector colOld = LoadCollection(whereClause);
			UpdateCollection(colOld,col);
		}
		/// <summary>
		/// Loads a collection of ExpencesMontlyData objects from the database.
		/// </summary>
		/// <returns>A collection containing all of the ExpencesMontlyData objects in the database.</returns>
		public static ExpencesMontliesVector LoadCollection(string whereClause)
		{
			SqlParameter[] parms = new SqlParameter[] { new SqlParameter("@WhereClause", whereClause) };
			return LoadCollection("ExpencesMontlyListProc", parms);
		}

		/// <summary>
		/// Loads a collection of ExpencesMontlyData objects from the database.
		/// </summary>
		/// <returns>A collection containing all of the ExpencesMontlyData objects in the database.</returns>
		public static ExpencesMontliesVector LoadCollection(string spName, SqlParameter[] parms)
		{
			ExpencesMontliesVector result = new ExpencesMontliesVector();
			using (SqlDataReader reader = SqlHelper.ExecuteReader(CommonDAL.ConnectionString, CommandType.StoredProcedure, spName, parms))
			{
				while (reader.Read())
				{
					ExpencesMontlyData tmp = new ExpencesMontlyData();
					LoadFromReader(reader, tmp);
					result.Add(tmp);
				}
			}
			return result;
		}
		/// <summary>
		/// Deletes a ExpencesMontlyData object from the database.
		/// </summary>
		/// <param name="ExpencesMontlyID">The primary key value</param>
		public static void Delete(int ExpencesMontlyID)
		{
			SqlParameter[] parameterValues = new SqlParameter[] { new SqlParameter("@ExpenceID", ExpencesMontlyID) };
			SqlHelper.ExecuteNonQuery(CommonDAL.ConnectionString, CommandType.StoredProcedure, "ExpencesMontlyDelProc", parameterValues);
		}

		public static void LoadFromReader(SqlDataReader reader, ExpencesMontlyData target)
		{
			if (reader != null && !reader.IsClosed)
			{
				//target.Key = reader.GetInt32(0);
				if (!reader.IsDBNull(0)) target.ExpenceID  = reader.GetInt32(0);
				if (!reader.IsDBNull(1)) target.MonthOfYear = reader.GetDateTime(1);
				if (!reader.IsDBNull(2)) target.Expences = reader.GetInt32(2);
				if (!reader.IsDBNull(3)) target.ExpencesASI = reader.GetInt32(3);
				}
			
		}
		
	  
		public static int Insert( ExpencesMontlyData source)
		{
			SqlParameter[] parameterValues = GetInsertParameterValues(source);
			SqlHelper.ExecuteNonQuery(CommonDAL.ConnectionString, CommandType.StoredProcedure, "ExpencesMontlyInsProc", parameterValues);
			source.ExpenceID = (int) parameterValues[0].Value;
			return source.ExpenceID;
		}

		public static int Update( ExpencesMontlyData source)
		{
			SqlParameter[] parameterValues = GetUpdateParameterValues(source);
			SqlHelper.ExecuteNonQuery(CommonDAL.ConnectionString, CommandType.StoredProcedure,
				"ExpencesMontlyUpdProc", parameterValues);
			return source.ExpenceID;
		}
		private static void UpdateCollection(ExpencesMontliesVector colOld,ExpencesMontliesVector col)
		{
			// Delete all old items
			foreach( ExpencesMontlyData itemOld in colOld)
			{
				if(!ExistsKey(itemOld.ExpenceID, col))
					Delete(itemOld.ExpenceID);
			}
			foreach( ExpencesMontlyData item in col)
			{
				if(item.ExpenceID <= -1)
					Insert(item);
				else
					Update(item);
			}
		}
		private static bool ExistsKey(int Key, ExpencesMontliesVector col)
		{
			foreach( ExpencesMontlyData item in col)
				if(item.ExpenceID==Key)
					return true;
			return false;
		}

		private static SqlParameter GetSqlParameter(string name, ParameterDirection direction, object value)
		{
			SqlParameter p = new SqlParameter(name, value);
			p.Direction = direction;
			return p;
		}

		private static SqlParameter[] GetInsertParameterValues( ExpencesMontlyData source)
		{
			SqlParameter[] prams = new SqlParameter[4];
			prams[0] = GetSqlParameter("@ExpenceID", ParameterDirection.Output, source.ExpenceID);
			prams[1] = GetSqlParameter("@MonthOfYear", ParameterDirection.Input, source.MonthOfYear);
			prams[2] = GetSqlParameter("@Expences", ParameterDirection.Input, source.Expences);
			prams[3] = GetSqlParameter("@ExpencesASI", ParameterDirection.Input, source.ExpencesASI);
			return prams;
		}

		private static SqlParameter[] GetUpdateParameterValues( ExpencesMontlyData source)
		{
			return new SqlParameter[] {
										  new SqlParameter("@ExpenceID", source.ExpenceID),
										  new SqlParameter("@MonthOfYear", source.MonthOfYear),
										  new SqlParameter("@Expences", source.Expences),
				new SqlParameter("@ExpencesASI", source.ExpencesASI),
			};
		}
	}
}
