using System;
using System.Data;
using System.Data.SqlClient;
using Microsoft.ApplicationBlocks.Data;

namespace Asa.Timesheet.Data
{
	/// <summary>
	/// Summary description for ClientsData.
	/// </summary>
	public class ClientsData
	{
		public static SqlDataReader SelectClients(int sortOrder)
		{
			string spName = "ClientsListProc";

			SqlParameter[] storedParams = SqlHelperParameterCache.GetSpParameterSet(DBManager.GetConnectionString(), spName);
			storedParams[0].Value = sortOrder;

			return SqlHelper.ExecuteReader(DBManager.GetConnectionString(), CommandType.StoredProcedure, spName, storedParams);
		}

		public static void DeleteClient(int clientID)
		{
			string spName = "ClientsDelProc";

			SqlParameter[] storedParams = SqlHelperParameterCache.GetSpParameterSet(DBManager.GetConnectionString(), spName);
			storedParams[0].Value = clientID;

			SqlHelper.ExecuteNonQuery(DBManager.GetConnectionString(), CommandType.StoredProcedure, spName, storedParams);
		}

		public static void UpdateClient(int clientID, string clientName, string city, string address, string email,string fax,string manager, string repr1, string repr2,
			string delo, string bulstat, string ndr, string phone, string webpage, string phone1, string email1,
			string phone2, string email2)
		{
			string spName = "ClientsUpdProc";

			SqlParameter[] storedParams = SqlHelperParameterCache.GetSpParameterSet(DBManager.GetConnectionString(), spName);
			storedParams[0].Value = clientID;
			storedParams[1].Value = clientName;
			storedParams[2].Value = (city == String.Empty) ? null : city;
			storedParams[3].Value = (address == String.Empty) ? null : address;
			storedParams[4].Value = (email == String.Empty) ? null : email;
			storedParams[5].Value = (fax == String.Empty) ? null : fax;
			storedParams[6].Value = (manager == String.Empty) ? null : manager;
			storedParams[7].Value = (repr1 == String.Empty) ? null : repr1;
			storedParams[8].Value = (repr2 == String.Empty) ? null : repr2;
			storedParams[9].Value = (delo == String.Empty) ? null : delo;
			storedParams[10].Value = (bulstat == String.Empty) ? null : bulstat;
			storedParams[11].Value = (ndr == String.Empty) ? null : ndr;
			storedParams[12].Value = (phone == String.Empty) ? null : phone;
			storedParams[13].Value = (webpage == String.Empty) ? null : webpage;
			storedParams[14].Value = (phone1 == String.Empty) ? null : phone1;
			storedParams[15].Value = (email1 == String.Empty) ? null : email1;
			storedParams[16].Value = (phone2 == String.Empty) ? null : phone2;
			storedParams[17].Value = (email2 == String.Empty) ? null : email2;
			SqlHelper.ExecuteNonQuery(DBManager.GetConnectionString(), CommandType.StoredProcedure, spName, storedParams);
		}

		public static int InsertClient(string clientName, string city, string address, string email, bool isActive,string fax,string manager, string repr1, string repr2,
			string delo, string bulstat, string ndr, string phone, string webpage, string phone1, string email1,
			string phone2, string email2)
		{
			string spName = "ClientsInsProc";

			SqlParameter[] storedParams = SqlHelperParameterCache.GetSpParameterSet(DBManager.GetConnectionString(), spName);
			storedParams[0].Value = clientName;
			storedParams[1].Value = (city == String.Empty) ? null : city;
			storedParams[2].Value = (address == String.Empty) ? null : address;
			storedParams[3].Value = (email == String.Empty) ? null : email;
			storedParams[4].Value = isActive;
			storedParams[5].Value = (fax == String.Empty) ? null : fax;
			storedParams[6].Value = (manager == String.Empty) ? null : manager;
			storedParams[7].Value = (repr1 == String.Empty) ? null : repr1;
			storedParams[8].Value = (repr2 == String.Empty) ? null : repr2;
			storedParams[9].Value = (delo == String.Empty) ? null : delo;
			storedParams[10].Value = (bulstat == String.Empty) ? null : bulstat;
			storedParams[11].Value = (ndr == String.Empty) ? null : ndr;
			storedParams[12].Value = (phone == String.Empty) ? null : phone;
			storedParams[13].Value = (webpage == String.Empty) ? null : webpage;
			storedParams[14].Value = (phone1 == String.Empty) ? null : phone1;
			storedParams[15].Value = (email1 == String.Empty) ? null : email1;
			storedParams[16].Value = (phone2 == String.Empty) ? null : phone2;
			storedParams[17].Value = (email2 == String.Empty) ? null : email2;
			SqlHelper.ExecuteNonQuery(DBManager.GetConnectionString(), CommandType.StoredProcedure, spName, storedParams);

			return (int)storedParams[18].Value;
		}

		public static SqlDataReader SelectClient(int clientID)
		{
			string spName = "ClientsSelProc";

			SqlParameter[] storedParams = SqlHelperParameterCache.GetSpParameterSet(DBManager.GetConnectionString(), spName);
			storedParams[0].Value = clientID;

			return SqlHelper.ExecuteReader(DBManager.GetConnectionString(), CommandType.StoredProcedure, spName, storedParams);
		}

		public static void InactiveClient(int clientID)
		{
			string spName = "ClientsInactiveProc";

			SqlParameter[] storedParams = SqlHelperParameterCache.GetSpParameterSet(DBManager.GetConnectionString(), spName);
			storedParams[0].Value = clientID;

			SqlHelper.ExecuteNonQuery(DBManager.GetConnectionString(), CommandType.StoredProcedure, spName, storedParams);
		}

	}
}
