﻿<%@ Page language="c#" Codebehind="Emails.aspx.cs" AutoEventWireup="false" Inherits="Asa.Timesheet.WebPages.Emails" %>
<%@ Register TagPrefix="uc1" TagName="PageHeader" Src="UserControls/PageHeader.ascx" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
	<head runat="server">
		<title>Emails</title>
		<meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
		<meta content="C#" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<LINK href="styles/timesheet.css" type="text/css" rel="stylesheet">
	</HEAD>
	<body bottomMargin="0" leftMargin="0" topMargin="0" rightMargin="0">
		<form id="Form1" method="post" runat="server">
			<table height="100%" cellSpacing="0" cellPadding="0" width="100%" border="0">
				<tr height="1">
					<td><uc1:pageheader id="header" runat="server"></uc1:pageheader></td>
				</tr>
				<tr>
					<td>
						<table height="100%" cellSpacing="0" cellPadding="0" width="100%" border="0">
							<tr>
								<td class="menuLeft"><asp:placeholder id="menuHolder" runat="server"></asp:placeholder></td>
								
								<td style="PADDING-RIGHT: 5px; PADDING-LEFT: 5px; PADDING-TOP: 3px" vAlign="top" noWrap><br>
									<asp:label id="lblError" runat="server" EnableViewState="False" ForeColor="Red" CssClass="ErrorLabel"></asp:label><asp:label id="lblInfo" runat="server" EnableViewState="False" CssClass="InfoLabel"></asp:label><br>
									<br>
									<asp:panel id="grid" 
										runat="server" Width="100%" Height="450px">
										<asp:datagrid id="grdEmails" runat="server" CssClass="Grid" Width="100%" AutoGenerateColumns="False"
											CellPadding="4" BackColor="Transparent" AllowSorting="True">
											<AlternatingItemStyle CssClass="GridAltItem"></AlternatingItemStyle>
											<ItemStyle CssClass="GridItem"></ItemStyle>
											<HeaderStyle CssClass="GridHeader"></HeaderStyle>
											<Columns>
											<asp:TemplateColumn HeaderText="#">
													<ItemStyle Width="10px" ForeColor="DimGray"></ItemStyle>
													<ItemTemplate>
														<asp:Label  CssClass="menuTable" runat="server" Text='<%# (int)DataBinder.Eval(Container, "ItemIndex")+1 %>' ID="Label1" NAME="Label1">
														</asp:Label>
													</ItemTemplate>
												</asp:TemplateColumn>
												<asp:BoundColumn DataField="EmailName" SortExpression="Name" HeaderText="Име"></asp:BoundColumn>
												<asp:BoundColumn DataField="Email" SortExpression="EMail" HeaderText="Електронен адрес"></asp:BoundColumn>
												<asp:TemplateColumn>
													<HeaderStyle Wrap="False" Width="20px"></HeaderStyle>
													<ItemStyle Wrap="False" HorizontalAlign="Center"></ItemStyle>
													<ItemTemplate>
														<asp:ImageButton id="ibEditItem" runat="server" ImageUrl="images/edit1.gif" ToolTip="Редактирай"></asp:ImageButton>
													</ItemTemplate>
												</asp:TemplateColumn>
												<asp:TemplateColumn>
													<HeaderStyle Wrap="False" Width="20px"></HeaderStyle>
													<ItemStyle Wrap="False" HorizontalAlign="Center"></ItemStyle>
													<ItemTemplate>
														<asp:ImageButton id="ibDeleteItem" runat="server" Width="12px" Height="12px" ImageUrl="images/delete.gif"
															ToolTip="Изтрий"></asp:ImageButton>
													</ItemTemplate>
												</asp:TemplateColumn>
											</Columns>
										</asp:datagrid>
									</asp:panel>
									<TABLE id="Table4"
										cellSpacing="0" cellPadding="4" width="100%" border="0">
										<TR>
											<td noWrap><asp:button id="btnNewEmail" runat="server" CssClass="ActionButton"></asp:button></TD>
										</TR>
									</TABLE>
								</td>
							</tr>
						</table>
					</td>
				</tr>
			</table>
		</form>
	</body>
</HTML>
