using System;
using System.Web;
using System.Collections;
using System.Web.UI.WebControls;
using System.Web.Caching;
using System.Configuration;
using System.Data;
//using System.Web.Mail;
using System.Net.Mail;
using System.Text;
using System.Collections.Specialized;
using Asa.Timesheet.Data;
using log4net;
using Asa.Timesheet.Data.Entities;
using System.IO;


namespace Asa.Timesheet.WebPages
{
	
	/// <summary>
	/// Summary description for MailBO.
	/// </summary>
	public class MailBO
	{
		private static readonly ILog log = LogManager.GetLogger(typeof(MailBO));
		private const string _BR="<BR>";
		private const string _SP=" ";
		private const string _ITEM=" - ";
		
		#region getmail text old

		public static string GetMailText1(MailInfo mi, out string subject)
		{
			decimal area = 0;
			subject="";

			string projectName, projectCode, administrativeName,add;
			int clientID=0;
			bool phases;bool isPUP;
			if (!ProjectsData.SelectProject(mi.ProjectID, out projectName, out  projectCode, out administrativeName, out area, out add, 
				out clientID,out phases,out isPUP))
			{
				log.Error("Project not found" + mi.ProjectID);
				return null;
			}

			UserInfo ui = UsersData.SelectUserByID(mi.UserID);
			if(ui==null)
			{
				log.Error("User not found" + mi.UserID);
				return null;
			}
//			if(mi.IsFromHours)
//				subject = ui.UserName;
//			else
			subject = string.Concat(projectCode,_SP,projectName);

			StringBuilder sb = new StringBuilder();
			//project
			sb.Append("<H4>");
			sb.Append(Resource.ResourceManager["mail_Project"]);
			sb.Append(projectCode);
			sb.Append(_SP);
			sb.Append(projectName);
			sb.Append("</H4>");
			//minutes
			if(mi.Minutes.Count>0 && mi.Minutes[0].Length>0)
			{
				sb.Append("<H5>");
				sb.Append(Resource.ResourceManager["reports_Minutes_Title"]);
				sb.Append("</H5>");
				sb.Append("<UL>");
				/*
				foreach(string s in mi.Minutes)
				{
					sb.Append("<LI>");
					sb.Append(s);
					sb.Append("</LI>");
				}*/

				foreach(string s in mi.Minutes)
				{
					//s = s.Replace("\r\n","
					string[] rows = s.Split('\r');
					sb.Append("<LI>");

					if (rows.Length==1) sb.Append(s);
					else
					{
						sb.Append("<UL>");
						for (int i=0; i<rows.Length; i++)
						{
							sb.Append("<LI>");
							sb.Append(rows[i]);
							sb.Append("</LI>");
						}
						sb.Append("</UL>");
					}
					
					sb.Append("</LI>");
				}

				sb.Append("</UL>");
			}
			//admin minutes
			if(mi.AdminMinutes.Count>0 && mi.AdminMinutes[0].Length>0)
			{
				sb.Append("<H5>");
				sb.Append(Resource.ResourceManager["mail_Admin"]);
				sb.Append("</H5>");
				sb.Append("<UL>");
				/*
				foreach(string s in mi.AdminMinutes)
				{
					sb.Append("<LI>");
					sb.Append(s);
					sb.Append("</LI>");
				}*/

				foreach(string s in mi.AdminMinutes)
				{
					//s = s.Replace("\r\n","
					string[] rows = s.Split('\r');
					sb.Append("<LI>");

					if (rows.Length==1) sb.Append(s);
					else
					{
						sb.Append("<UL>");
						for (int i=0; i<rows.Length; i++)
						{
							sb.Append("<LI>");
							sb.Append(rows[i]);
							sb.Append("</LI>");
						}
						sb.Append("</UL>");
					}
					
					sb.Append("</LI>");
				}

				sb.Append("</UL>");
			}
			return sb.ToString();
		}

		public static string GetMailText2(MailInfo mi, out string subject)
		{
			decimal area = 0;
			subject="";

			string projectName, projectCode, administrativeName,add;
			int clientID=0;
			bool phases;bool isPUP;
			if (!ProjectsData.SelectProject(mi.ProjectID, out projectName, out  projectCode, out administrativeName, out area, out add, 
				out clientID,out phases,out isPUP))
			{
				log.Error("Project not found" + mi.ProjectID);
				return null;
			}

			string activity = String.Empty;
			try
			{
				activity = DBManager.SelectActivityName(mi.ActivityID);
			}
			catch (Exception ex)
			{
				log.Error(ex);
			}

			UserInfo ui = UsersData.SelectUserByID(mi.UserID);
			if(ui==null)
			{
				log.Error("User not found" + mi.UserID);
				return null;
			}
	
			subject = string.Concat(projectCode,_SP,projectName);

			return CreateLayoutHtml(projectName, projectCode, projectName, ui.FullName, mi.Minutes[0], mi.AdminMinutes[0]);	
		}


		#endregion
		
		#region Get mail text

		public static string GetMailText(MailInfo mi, string reqUserName, out string subject)
		{
			decimal area = 0;
			subject="";

			string projectName = String.Empty, 
				projectCode = String.Empty, administrativeName = String.Empty, 
				userName = String.Empty, activityName = String.Empty, clientName = String.Empty, 
				minutes = String.Empty, adminMinutes = String.Empty;
	
			System.Data.SqlClient.SqlDataReader reader = null;
			try
			{
				reader = ReportsData.SelectMailReportHours(mi.WorkTimeID);
				if (reader.Read())
				{
					projectName = reader.GetString(4);
					projectCode = reader.IsDBNull(5) ? String.Empty : reader.GetString(5);
					area = reader.IsDBNull(6) ? 0 : reader.GetInt32(6);
					administrativeName = reader.IsDBNull(7) ? String.Empty : reader.GetString(7);
					userName = reader.GetString(2);
					activityName = reader.IsDBNull(10) ? String.Empty : reader.GetString(10);
					clientName = reader.IsDBNull(11) ? String.Empty : reader.GetString(11);
					minutes = reader.IsDBNull(8) ? String.Empty : reader.GetString(8);
					adminMinutes = reader.IsDBNull(9) ? String.Empty : reader.GetString(9);

				}
			}
			catch (Exception ex)
			{
				log.Error(ex);
				return null;
			}
			finally
			{
				if (reader!=null) reader.Close();
			}

			subject = string.Concat(projectCode,_SP,projectName);
			
			if (administrativeName == String.Empty) administrativeName = projectName;
			
			return String.Concat(CreateInfoHeaderHtml(administrativeName, projectCode, clientName, activityName, DateTime.Today),
									CreateLayoutHtml(projectName, projectCode, administrativeName, userName, minutes, adminMinutes),
									CreateFooterHtml(reqUserName));
		}

		public static void GetMailText(int projectID, int userID, DateTime startDate, DateTime endDate, string reqUserName, 
			out string headerHtml, out string html, out String footerHtml, out string subject)
		{

			DataSet ds = ReportsData.SelectMailReport(projectID, userID, startDate, endDate);
			
			DataTable dtProjectInfo = ds.Tables[0];
			string projectCode = dtProjectInfo.Rows[0]["ProjectCode"].ToString();
			string projectName = dtProjectInfo.Rows[0]["ProjectName"].ToString();
			string administrativeName = dtProjectInfo.Rows[0]["AdministrativeName"].ToString();
			string clientName = dtProjectInfo.Rows[0]["ClientName"].ToString();

			subject = string.Concat(projectCode,_SP,projectName);
			
			DataTable dt = ds.Tables[1];
			
			string userName = String.Empty;
			if (userID>0 && ds.Tables[1].Rows.Count>0)
			{
				userName = ds.Tables[1].Rows[0]["FullName"].ToString();
			}

			headerHtml = CreateInfoHeaderHtml(administrativeName, projectCode, clientName,"", DateTime.Today); 
			html = CreateLayoutHtml(ds, userName, startDate, endDate);
			footerHtml = CreateFooterHtml(reqUserName);
		}

		public static string GetMailText(int projectID, int userID, DateTime startDate, DateTime endDate, string reqUserName, out string subject)
		{
			string s = String.Empty;

			DataSet ds = ReportsData.SelectMailReport(projectID, userID, startDate, endDate);
			
			DataTable dtProjectInfo = ds.Tables[0];
			string projectCode = dtProjectInfo.Rows[0]["ProjectCode"].ToString();
			string projectName = dtProjectInfo.Rows[0]["ProjectName"].ToString();
			string administrativeName = dtProjectInfo.Rows[0]["AdministrativeName"].ToString();
			string clientName = dtProjectInfo.Rows[0]["ClientName"].ToString();

			if (administrativeName == String.Empty) administrativeName = projectName;

			subject = string.Concat(projectCode,_SP,projectName);
			
			DataTable dt = ds.Tables[1];
			
			string userName = String.Empty;
			if (userID>0 && ds.Tables[1].Rows.Count>0)
			{
				userName = ds.Tables[1].Rows[0]["FullName"].ToString();
			}

			s = String.Concat(CreateInfoHeaderHtml(administrativeName, projectCode, clientName,Resource.ResourceManager["reports_All"], DateTime.Today), 
						CreateLayoutHtml(ds, userName, startDate, endDate), CreateFooterHtml(reqUserName));
			
			return s;
		}

		#endregion

		#region Send mail

		public static void SendMail(string sTo, string sSubj, string sBody)
		{
			try
			{
				MailMessage Message = new MailMessage();
				Message.To.Add(GetChangedA(sTo));
				//Message.From = System.Configuration.ConfigurationManager.AppSettings["MailFrom"];;
				Message.BodyEncoding= Encoding.UTF8;
			
				
				Message.Subject = sSubj;
				Message.Body = sBody;
				Message.IsBodyHtml = true;

				string sAuthMail = System.Configuration.ConfigurationManager.AppSettings["AuthMail"];
				if(sAuthMail!="")
				{
					//Message.Fields.Add("http://schemas.microsoft.com/cdo/configuration/sendusing", "2");	//basic authentication
					//Message.Fields.Add("http://schemas.microsoft.com/cdo/configuration/smtpauthenticate", "1");	//basic authentication
					//Message.Fields.Add("http://schemas.microsoft.com/cdo/configuration/sendusername", sAuthMail); //set your username here
					//Message.Fields.Add("http://schemas.microsoft.com/cdo/configuration/sendpassword", System.Configuration.ConfigurationManager.AppSettings["AuthPass"]);	//set your password here
				}
                SmtpClient smtp = new SmtpClient();

                smtp.Send(Message);
				
			}
			catch(Exception ex)
			{
				log.Error(ex);
			}
		}
		public static void SendMailFromMeetings(string sTo, string sSubj, string sBody)
		{
			try
			{
				MailMessage Message = new MailMessage();
				Message.To.Add(GetChanged(sTo));
				//Message.From = System.Configuration.ConfigurationManager.AppSettings["MailFromMeetings"];;
				Message.BodyEncoding= Encoding.UTF8;
			
				
				Message.Subject = sSubj;
				Message.Body = sBody;
				Message.IsBodyHtml = true;

				string sAuthMail = System.Configuration.ConfigurationManager.AppSettings["AuthMailMeetings"];
				if(sAuthMail!="")
				{
					//Message.Fields.Add("http://schemas.microsoft.com/cdo/configuration/sendusing", "2");	//basic authentication
					//Message.Fields.Add("http://schemas.microsoft.com/cdo/configuration/smtpauthenticate", "1");	//basic authentication
					//Message.Fields.Add("http://schemas.microsoft.com/cdo/configuration/sendusername", sAuthMail); //set your username here
					//Message.Fields.Add("http://schemas.microsoft.com/cdo/configuration/sendpassword", System.Configuration.ConfigurationManager.AppSettings["AuthPassMeetings"]);	//set your password here
				}

                SmtpClient smtp = new SmtpClient();

                smtp.Send(Message);
				
			}
			catch(Exception ex)
			{
				log.Error(ex);
			}
		}
		public static void SendMailFromProtocol(string sTo, string sSubj, string sBody,string path)
		{
			try
			{
				MailMessage Message = new MailMessage();
				Message.To.Add(sTo);
				//Message.From = System.Configuration.ConfigurationManager.AppSettings["MailFromMeetings"];;
				Message.BodyEncoding= Encoding.UTF8;
			
				
				Message.Subject = sSubj;
				Message.Body = sBody;
				Message.IsBodyHtml = true;
				Message.Attachments.Add(new Attachment(path));
				string sAuthMail = System.Configuration.ConfigurationManager.AppSettings["AuthMailMeetings"];
				if(sAuthMail!="")
				{
					//Message.Fields.Add("http://schemas.microsoft.com/cdo/configuration/sendusing", "2");	//basic authentication
					//Message.Fields.Add("http://schemas.microsoft.com/cdo/configuration/smtpauthenticate", "1");	//basic authentication
					//Message.Fields.Add("http://schemas.microsoft.com/cdo/configuration/sendusername", sAuthMail); //set your username here
					//Message.Fields.Add("http://schemas.microsoft.com/cdo/configuration/sendpassword", System.Configuration.ConfigurationManager.AppSettings["AuthPassMeetings"]);	//set your password here
				}

                SmtpClient smtp = new SmtpClient();

                smtp.Send(Message);
				
			}
			catch(Exception ex)
			{
				log.Error(ex);
			}
		}

		#endregion
		
		//notes:here sent mail with PDFHotIssues
		public static string GetMails(int meetingID, out string body)
		{
			MeetingData md = MeetingDAL.Load(meetingID);
//			string hourOfMeeting = "";
//			if(md.StartHour==md.EndHour)
//				hourOfMeeting = md.StartHour;
//			else
//				hourOfMeeting = string.Concat(md.StartHour," - ",md.EndHour);
//			string subjectOfMeeting = md.Notes;
//			string meetingDate = md.MeetingDate.ToString("dd.MM.yyyy");
//			subject=string.Format(Resource.ResourceManager["MeetingHotIssue_subject"],meetingDate,hourOfMeeting/*,UserName,RestType,dates*/);
			body = "Dear collegues,%0A%0AEnclosed you will find the latest PCM Minutes.";

			return GetUserMails(md.Users,md.Subcontracters,md.Clients,md.OtherMails);
		}
		public static void SendMailASAHotIssue(string Project,string Category, string Name,string Desc,string Comment,string sAssignedBy,string sAssignedByMail,string sAssignedTo,string sAssignedToMail,string sStatus, string sID)
		{
			try
			{
				string mails=sAssignedByMail+";"+sAssignedToMail;
				MailMessage Message = new MailMessage();
                Message.To.Add(sAssignedByMail);
                Message.To.Add(sAssignedToMail);
				//Message.From = System.Configuration.ConfigurationManager.AppSettings["MailFromMeetings"];;
				Message.BodyEncoding= Encoding.UTF8;
		
				string systemPath = HttpContext.Current.Request.Url.AbsoluteUri;
				int index = systemPath.LastIndexOf(@"/");
				systemPath = systemPath.Substring(0,index+1);
				string sURL=	string.Concat( systemPath,"EditHotIssue.aspx?id="+sID+ "&ASA=1");

			
				Message.Subject = Resource.ResourceManager["SubjectMailTasks"];
				Message.Body = string.Format( Resource.ResourceManager["BodyMailTasks"],Project,Category,Name,Desc,Comment,sAssignedBy,sAssignedTo, DateTime.Now.ToShortDateString(),sStatus,sURL);
				Message.IsBodyHtml = true;
				
					
				string sAuthMail = System.Configuration.ConfigurationManager.AppSettings["AuthMailMeetings"];
				if(sAuthMail!="")
				{
					//Message.Fields.Add("http://schemas.microsoft.com/cdo/configuration/sendusing", "2");	//basic authentication
					//Message.Fields.Add("http://schemas.microsoft.com/cdo/configuration/smtpauthenticate", "1");	//basic authentication
					//Message.Fields.Add("http://schemas.microsoft.com/cdo/configuration/sendusername", sAuthMail); //set your username here
					//Message.Fields.Add("http://schemas.microsoft.com/cdo/configuration/sendpassword", System.Configuration.ConfigurationManager.AppSettings["AuthPassMeetings"]);	//set your password here
				}

                SmtpClient smtp = new SmtpClient();

                smtp.Send(Message);
				log.Info(string.Format("Sent mail new task {0} to {1}",Message.Body ,mails));
			}
			catch(Exception ex)
			{
				log.Error(ex);
			}
			
		}

		public static void SendMailWithPDFHotIssues(int meetingID,string path)
		{
			MeetingData md = MeetingDAL.Load(meetingID);
			string meetingDate = md.MeetingDate.ToString("dd.MM.yyyy");
			string hourOfMeeting = "";
			if(md.StartHour==md.EndHour)
				hourOfMeeting = md.StartHour;
			else
				hourOfMeeting = string.Concat(md.StartHour," - ",md.EndHour);
			string subjectOfMeeting = md.Notes;

			string mails = GetUserMails(md.Users,md.Subcontracters,md.Clients,md.OtherMails);
			//return;
			
			try
			{

				MailMessage Message = new MailMessage();
				Message.To.Add(GetChanged(mails));
				//Message.From = System.Configuration.ConfigurationManager.AppSettings["MailFromMeetings"];;
				Message.BodyEncoding= Encoding.UTF8;
		
			
				Message.Subject = string.Format(Resource.ResourceManager["MeetingHotIssue_subject"],meetingDate,hourOfMeeting/*,UserName,RestType,dates*/);
				Message.Body = string.Format( Resource.ResourceManager["MeetingHotIssue_body"],subjectOfMeeting,meetingDate,hourOfMeeting/*,UserName,RestType,dates*/);
				Message.IsBodyHtml = true;
				Message.Attachments.Add(new Attachment(path));
					
				string sAuthMail = System.Configuration.ConfigurationManager.AppSettings["AuthMailMeetings"];
				if(sAuthMail!="")
				{
					//Message.Fields.Add("http://schemas.microsoft.com/cdo/configuration/sendusing", "2");	//basic authentication
					//Message.Fields.Add("http://schemas.microsoft.com/cdo/configuration/smtpauthenticate", "1");	//basic authentication
					//Message.Fields.Add("http://schemas.microsoft.com/cdo/configuration/sendusername", sAuthMail); //set your username here
					//Message.Fields.Add("http://schemas.microsoft.com/cdo/configuration/sendpassword", System.Configuration.ConfigurationManager.AppSettings["AuthPassMeetings"]);	//set your password here
				}

                SmtpClient smtp = new SmtpClient();

                smtp.Send(Message);
				log.Info(string.Format("Sent mail PDFHotIssues of meeting {0} to {1}",meetingID ,mails));
			}
			catch(Exception ex)
			{
				log.Error(ex);
			}
			
		}
		private static void GetEmails(string othermails, StringCollection scMails)
		{
			string[] arrUsers1 = othermails.Split(';');
			System.Text.RegularExpressions.Regex mailRegex = new System.Text.RegularExpressions.Regex(@"\w+([-+.]\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*");
			
				
			//notes:method for Users
			for(int i=0;i<arrUsers1.Length;i++)
			{
				string[] arrUsersNames = arrUsers1[i].Trim().Split(',');
				string name="";
				string email="";
				if(arrUsersNames.Length==0)
					continue;
				else if(arrUsersNames.Length==1)
				{
					email=arrUsersNames[0];
				}
				else if(arrUsersNames.Length==2)
				{
					name=arrUsersNames[0];
					email=arrUsersNames[1];
				}
				if(mailRegex.IsMatch(email))
					scMails.Add(email);
					
			
			}
			
		}
		private static string GetUserMails(string users,string subs,string clients,string othermails)
		{
			try
			{
				string returnString = "";
				System.Text.RegularExpressions.Regex mailRegex = new System.Text.RegularExpressions.Regex(@"\w+([-+.]\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*");
				SubcontractersVector ssv = SubcontracterDAL.LoadCollection();
				ClientsVector cv = ClientDAL.LoadCollection((int)ClientTypes.Client);
				string[] arrUsers = users.Split(';');
				string[] arrSubs = subs.Split(';');
				string[] arrClients = clients.Split(';');
				foreach(string userID in arrUsers)
				{
					if(UIHelpers.ToInt(userID)==-1)
						continue;
					UserInfo ui = UsersData.SelectUserByID(UIHelpers.ToInt(userID));
					if(ui == null)
						continue;
					if(mailRegex.IsMatch(ui.Mail))
						returnString = string.Concat(returnString,ui.Mail,";");
				}
				//notes:method for Subcontracters
				foreach(string subID in arrSubs)
				{
					if(UIHelpers.ToInt(subID)==-1)
						continue;
					SubcontracterData datsub = ssv.GetByID(UIHelpers.ToInt(subID));
					if(datsub == null||datsub.Hide)
						continue;
					if(mailRegex.IsMatch(datsub.Email))
						returnString = string.Concat(returnString,datsub.Email,";");
				}
				//notes:method for Clients
				foreach(string clID in arrClients)
				{
					if(UIHelpers.ToInt(clID)==-1)
						continue;
					ClientData datcl = cv.GetByID(UIHelpers.ToInt(clID));
					if(datcl == null)
						continue;
					if(mailRegex.IsMatch(datcl.Email))
						returnString = string.Concat(returnString,datcl.Email,";");
				}
				//other mails
				string[] arrUsers1 = othermails.Split(';');
				
				
				//notes:method for Users
				for(int i=0;i<arrUsers1.Length;i++)
				{
					string[] arrUsersNames = arrUsers1[i].Trim().Split(',');
					string name="";
					string email="";
					if(arrUsersNames.Length==0)
						continue;
					else if(arrUsersNames.Length==1)
					{
						email=arrUsersNames[0];
					}
					else if(arrUsersNames.Length==2)
					{
						name=arrUsersNames[0];
						email=arrUsersNames[1];
					}
					if(mailRegex.IsMatch(email))
						returnString = string.Concat(returnString,email,";");
					
			
				}
				returnString = returnString.Substring(0,returnString.Length-1);
				return returnString;
			}
			catch(Exception ex)
			{
				log.Error(ex.Message);
				return "";
			}
		}
	
		


		public static void SendMailConfirmAbsence(string UserName,string startDate,string endDate,string RestType,bool isConfirm, UserInfo loggedUser,string sFile,bool hasChanges, string UserNameSub)
		{
			string UserMail = loggedUser.Mail;
			
			StringCollection AccNotMainMail = MailDAL.LoadEmailsByRole((int)Roles.MainAccount);

            MailMessage Message = new MailMessage();
            Message.To.Add(UserMail);
            Message.CC.Add("yosifov@asa-bg.com");

			foreach(string s in AccNotMainMail)
			{
                    Message.CC.Add(s);
			}
			
			if(loggedUser.Manager >0)
			{
				UserInfo uiManager = UsersData.SelectUserByID(loggedUser.Manager);
				if(uiManager!=null)
				{
                    Message.To.Add(uiManager.Mail);
				}
			}
			string dates;
				if(startDate == endDate)
					dates = startDate;
				else
					dates = string.Concat(startDate," - ",endDate);
			
				try
				{


					Message.BodyEncoding= Encoding.UTF8;
					if(sFile!=null && isConfirm)
						Message.Attachments.Add(new Attachment(sFile));
			
					Message.Subject = string.Format(Resource.ResourceManager["ConfirmAbsence_subject"],UserName,RestType,dates);
					if(isConfirm)
						Message.Body = string.Format( Resource.ResourceManager["ConfirmAbsence_bodyConfirm"],UserName,RestType,dates);
					else
						Message.Body = string.Format( Resource.ResourceManager["ConfirmAbsence_bodyDecline"],UserName,RestType,dates);

                    Message.Body += UserNameSub;
					if(hasChanges)
					Message.Body +=Resource.ResourceManager["ConfirmAbsence_dateChanged"];
                    Message.IsBodyHtml = true;
				
					SmtpClient smtp = new SmtpClient();
					smtp.Send(Message);
					log.Info("Mail Sent Absense Confirm to " + Message.To.ToString());
			
				}
				catch(Exception ex)
				{
					log.Error(ex);
				}
			
		}

		private static StringCollection GetMailRequestAbsence()
		{
			StringCollection sc = new StringCollection();
			
			StringCollection directorMail = MailDAL.LoadEmailsByRole((int)Roles.Director);
			StringCollection AccNotMainMail = MailDAL.LoadEmailsByRole((int)Roles.Account);
			StringCollection AccNotMainMail1 = MailDAL.LoadEmailsByRole((int)Roles.MainAccount);
			foreach(string s in directorMail)
			{
				sc.Add(s);
			}
			foreach(string s in AccNotMainMail)
			{
				if(s.IndexOf("plamen")==-1)
					sc.Add(s);
			}
			foreach(string s in AccNotMainMail1)
			{
				if(s.IndexOf("plamen")==-1)
					sc.Add(s);
			}
			return sc;
		}
		public static bool SendMailRequestAbsence(UserInfo loggedUser,string startDate,string endDate,string reason,int requestID,string RestType,string SystemPath, string sUsed, string sRest,string sTotal, string sTotalAll)
		{
			//bool returnValue = false;
			bool IsSend = false;
			StringCollection emails = GetMailRequestAbsence();
			StringBuilder sMails=new StringBuilder();
			string managerMail = string.Empty;
			if(loggedUser.Manager >0)
			{
				UserInfo uiManager = UsersData.SelectUserByID(loggedUser.Manager);
				if(uiManager!=null && uiManager.Mail.ToLower()!="t.serafimov@asa-bg.com")
				{
					managerMail = uiManager.Mail;
					emails.Add(uiManager.Mail);
				}
			}
			foreach(string s in emails)
			{
//				System.Text.RegularExpressions.Regex mailRegex = new System.Text.RegularExpressions.Regex(@"\w+([-+.]\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*");
//				if(!mailRegex.IsMatch(s))
//				{
//					log.Info("send to invalid mail address - " +s);
//					continue;
//				}

				string link = string.Concat( SystemPath,"AbsenceConfirms.aspx"+"?id="+requestID.ToString());
				string dates;
				if(startDate == endDate)
					dates = startDate;
				else
					dates = string.Concat(startDate," - ",endDate);
				try
				{

					MailMessage Message = new MailMessage();
					Message.To.Add(s);
					//Message.From = System.Configuration.ConfigurationManager.AppSettings["MailFromAbs"];;
					Message.BodyEncoding= Encoding.UTF8;
			
					if(reason.Trim()!="")
						reason=Resource.ResourceManager["za"]+reason;
					Message.Subject = string.Format(Resource.ResourceManager["RequestAbsence_subject"],loggedUser.FullName,RestType,dates);
                    if (s == managerMail)
                    {
                        Message.Body = string.Format(Resource.ResourceManager["RequestAbsenceWithoutConfurm_body"], loggedUser.FullName, RestType, dates, reason, DateTime.Now.ToShortDateString(), sUsed, sRest, sTotal, sTotalAll);//
                    }
                    else
                    {
                        Message.Body = "<span style='font-size: 14pt;'>" + string.Format(Resource.ResourceManager["RequestAbsence_body"], loggedUser.FullName, RestType, dates, reason, sUsed, sRest, sTotal, sTotalAll) + "</span><br><br>";
                        Message.Body += "<br><span style='font-size: 18pt;'>"
                          + "<a href='" + link + "' style='color:#00487c; '>Потвърждение...</a></span>";
                    }
                        Message.IsBodyHtml = true;
					
					string sAuthMail = System.Configuration.ConfigurationManager.AppSettings["AuthMailAbs"];
					if(sAuthMail!="")
					{
						//notes:not in use for asa,but for test
						////Message.Fields.Add("http://schemas.microsoft.com/cdo/configuration/smtpaccountname", System.Configuration.ConfigurationManager.AppSettings["NameMail"]);

						//Message.Fields.Add("http://schemas.microsoft.com/cdo/configuration/sendusing", "2");	//basic authentication
						//Message.Fields.Add("http://schemas.microsoft.com/cdo/configuration/smtpauthenticate", "1");	//basic authentication
						//Message.Fields.Add("http://schemas.microsoft.com/cdo/configuration/sendusername", sAuthMail); //set your username here
						//Message.Fields.Add("http://schemas.microsoft.com/cdo/configuration/sendpassword", System.Configuration.ConfigurationManager.AppSettings["AuthPassAbs"]);	//set your password here
					}

					SmtpClient smtp = new SmtpClient();
					smtp.Send(Message);
					log.Info("Mail sent Absense Req  to "+s);
					IsSend = true;
				}
				catch(Exception ex)
				{
					log.Error(ex);
					//return false;
				}
			}
			return IsSend;
		}

		public static void SendMailNewAccount(string email,string accName,string accPass)
		{
			try
			{
				MailMessage Message = new MailMessage();
				Message.To.Add(email);
				//Message.From = System.Configuration.ConfigurationManager.AppSettings["MailFromMeetings"];;
				Message.BodyEncoding= Encoding.UTF8;
		
			
				Message.Subject = string.Format(Resource.ResourceManager["newAccount_subject"]);
				Message.Body = string.Format( Resource.ResourceManager["newAccount_body"],accName,accPass,System.Configuration.ConfigurationManager.AppSettings["SystemURL"]);
				Message.IsBodyHtml = true;
				
					
				string sAuthMail = System.Configuration.ConfigurationManager.AppSettings["AuthMailMeetings"];
				if(sAuthMail!="")
				{
					//Message.Fields.Add("http://schemas.microsoft.com/cdo/configuration/sendusing", "2");	//basic authentication
					//Message.Fields.Add("http://schemas.microsoft.com/cdo/configuration/smtpauthenticate", "1");	//basic authentication
					//Message.Fields.Add("http://schemas.microsoft.com/cdo/configuration/sendusername", sAuthMail); //set your username here
					//Message.Fields.Add("http://schemas.microsoft.com/cdo/configuration/sendpassword", System.Configuration.ConfigurationManager.AppSettings["AuthPassMeetings"]);	//set your password here
				}

				SmtpClient smtp = new SmtpClient();
				smtp.Send(Message);
			
			}
			catch(Exception ex)
			{
				log.Error(ex);
			}
		}

		private static StringCollection GetMailForCorespondecy()
		{
			StringCollection sc = new StringCollection();
			
			StringCollection directorMail = MailDAL.LoadEmailsByRole((int)Roles.Director);
			StringCollection AccMainMail = MailDAL.LoadEmailsByRole((int)Roles.MainAccount);
			StringCollection LayerMail = MailDAL.LoadEmailsByRole((int)Roles.Layer);
			foreach(string s in directorMail)
				sc.Add(s);
			foreach(string s in AccMainMail)
				sc.Add(s);
			foreach(string s in LayerMail)
				sc.Add(s);
			return sc;
		}

		public static void SendMailCorespondency(string SendReseve,int ID, string typeOfCorespondency, string date,string enterNumber,
			string project, string client, string fromto, string notes)
		{
			StringCollection scMail;
			
			scMail = GetMailForCorespondecy();

			StringBuilder sMails=new StringBuilder();
			foreach(string s in scMail)
			{
				sMails.Append(s);
				sMails.Append(";");
			}
			try
			{
				MailMessage Message = new MailMessage();
				Message.To.Add(sMails.ToString());
				//Message.From = System.Configuration.ConfigurationManager.AppSettings["MailFromCorr"];;
				Message.BodyEncoding= Encoding.UTF8;
		
			
				Message.Subject = string.Format(Resource.ResourceManager["corespondecy_subject"]);
				string systemPath = System.Configuration.ConfigurationManager.AppSettings["LocalPath"];
//				int index = systemPath.LastIndexOf(@"/");
//				systemPath = systemPath.Substring(0,index+1);
				string sURL=	string.Concat( systemPath,"EditCorespondency.aspx?id="+ID.ToString());
				Message.Body = string.Format( Resource.ResourceManager["corespondecy_body"],SendReseve,enterNumber.ToString(),typeOfCorespondency,date,sURL,project, client,fromto, notes);
				Message.IsBodyHtml = true;
				
					
				string sAuthMail = System.Configuration.ConfigurationManager.AppSettings["AuthMailCorr"];
				if(sAuthMail!="")
				{
					//Message.Fields.Add("http://schemas.microsoft.com/cdo/configuration/sendusing", "2");	//basic authentication
					//Message.Fields.Add("http://schemas.microsoft.com/cdo/configuration/smtpauthenticate", "1");	//basic authentication
					//Message.Fields.Add("http://schemas.microsoft.com/cdo/configuration/sendusername", sAuthMail); //set your username here
					//Message.Fields.Add("http://schemas.microsoft.com/cdo/configuration/sendpassword", System.Configuration.ConfigurationManager.AppSettings["AuthPassCorr"]);	//set your password here
				}

				SmtpClient smtp = new SmtpClient();
				smtp.Send(Message);
				log.Info("Corresp sent to "+sMails.ToString());
			
			}
			catch(Exception ex)
			{
				log.Error(ex);
			}

		}

		public static void SendMailOfficer (string sc, string project, string date,string path,bool Protocol, string type)
		{
			SendMailFromProtocol(sc,string.Format(Resource.ResourceManager["Protocol_subject"],date,project,type)
				, string.Format( Resource.ResourceManager["Protocol_body1"],project,date,type),path);

		}
		public static void SendMailOfficer (string project, string date, string start, string end)
		{
			SendMailFromMeetings(System.Configuration.ConfigurationManager.AppSettings["MeetingEmail"],string.Format(Resource.ResourceManager["meeting_subject"],date,start,project)
				, string.Format( Resource.ResourceManager["meeting_body1"],project,date,start,end));

		}
		public static void SendMailFirst(string users, string project, string date, string start, string end,string subs, string clients,string place, string other, int nClient,string notes,string date1)
		{
			
			StringCollection scMail,scNames;
			string sPeople = GetAllPeople(users,clients,subs,other,out scMail,out scNames);

			string sClient = "";
			if(nClient>0)
			{
				ClientData cd= ClientDAL.Load(nClient);
				if(cd!=null)
					sClient= cd.ClientName;
			}
			StringBuilder sMails=new StringBuilder();
			foreach(string s in scMail)
			{
				sMails.Append(s);
				sMails.Append(";");
			}
			try
			{
				MailMessage Message = new MailMessage();
				Message.To.Add(GetChanged(sMails.ToString()));
				//Message.From = System.Configuration.ConfigurationManager.AppSettings["MailFromMeetings"];;
				Message.BodyEncoding= Encoding.UTF8;
		
			
				Message.Subject = string.Format(Resource.ResourceManager["meeting_subject"],date1,start,project);
				Message.Body = string.Format( Resource.ResourceManager["meeting_body"],project,sClient,"",date,start,place, sPeople,notes);
				Message.IsBodyHtml = true;
				
					
				string sAuthMail = System.Configuration.ConfigurationManager.AppSettings["AuthMailMeetings"];
				if(sAuthMail!="")
				{
					//Message.Fields.Add("http://schemas.microsoft.com/cdo/configuration/sendusing", "2");	//basic authentication
					//Message.Fields.Add("http://schemas.microsoft.com/cdo/configuration/smtpauthenticate", "1");	//basic authentication
					//Message.Fields.Add("http://schemas.microsoft.com/cdo/configuration/sendusername", sAuthMail); //set your username here
					//Message.Fields.Add("http://schemas.microsoft.com/cdo/configuration/sendpassword", System.Configuration.ConfigurationManager.AppSettings["AuthPassMeetings"]);	//set your password here
				}

				SmtpClient smtp = new SmtpClient();
				smtp.Send(Message);
			
			}
			catch(Exception ex)
			{
				log.Error(ex);
			}
				
			
		}
		private static string GetName(string sFull)
		{
			int n= sFull.LastIndexOf(" ");
			if(n!=-1)
				return sFull.Substring(n+1);
			return sFull;
		}
		public static string GetAllPeople(string users, string clients, string subs,string other, out StringCollection scMails,out StringCollection scNames)
		{
			StringBuilder sb = new StringBuilder();
			int n=1;

			scMails = new StringCollection();
			scNames= new StringCollection();
			string [] sclients=clients.Split(';');
			foreach(string s in sclients)
			{
				if(s!=null && s.Length>0)
				{
					int i = int.Parse(s);
					ClientData cd = ClientDAL.Load(i);
					sb.Append(n);
					sb.Append(". ");
					sb.Append(cd.ClientNameAndAll);
					sb.Append("<BR>");
					if(cd.Email!="")
					{
						scMails.Add(cd.Email);
						scNames.Add(GetName(cd.Manager));
					}
					if(cd.ClientID==int.Parse(System.Configuration.ConfigurationManager.AppSettings["ClientRepr21"])
						||cd.ClientID==int.Parse(System.Configuration.ConfigurationManager.AppSettings["ClientRepr22"])
						|| cd.ClientID==int.Parse(System.Configuration.ConfigurationManager.AppSettings["ClientRepr23"]))
					{
						if(cd.Email2!="")
						{
							scMails.Add(cd.Email2);
							scNames.Add(GetName(cd.Representative2));
						}
					}
					n++;
				}
			}
			string [] susers=users.Split(';');
			foreach(string s in susers)
			{
				if(s!=null && s.Length>0)
				{
					int i = int.Parse(s);
					UserInfo ui= UsersData.SelectUserByID(i);
					sb.Append(n);
					sb.Append(". ");
					sb.Append(ui.FullName);
					sb.Append("<BR>");
					//if(cd.Email!="")
					{
						scMails.Add(ui.Mail);
						scNames.Add(ui.LastName);
					}
					n++;
				}
			}
			string [] ssubs=subs.Split(';');
			foreach(string s in ssubs)
			{
				if(s!=null && s.Length>0)
				{
					int i = int.Parse(s);
					SubcontracterData cd = SubcontracterDAL.Load(i);
					sb.Append(n);
					sb.Append(". ");
					sb.Append(cd.SubcontracterNameAndAll);
					sb.Append("<BR>");
					if(cd.Email!="")
					{
						scMails.Add(cd.Email);
						scNames.Add(GetName(cd.Manager));
					}
					n++;
				}
			}
			if(other!="")
			{
				sb.Append(n);
				sb.Append(". ");
				sb.Append(other);
				sb.Append("<BR>");
			}
			return sb.ToString();
		}
		public static void SendMailMinutes(string users, string path,string project, string date, string start, string end,string subs, string clients,string place, string other, int nClient,string notes,string date1, string othermails)
		{
			
			

			StringCollection scMail,scNames;
			string sPeople = GetAllPeople(users,clients,subs,other,out scMail,out scNames);
			GetEmails(othermails,scMail);
			
			string sClient = "";
			if(nClient>0)
			{
				ClientData cd= ClientDAL.Load(nClient);
				if(cd!=null)
					sClient= cd.ClientName;
			}
			StringBuilder sMails=new StringBuilder();
			foreach(string s in scMail)
			{
				sMails.Append(s);
				sMails.Append(";");
			}
			try
			{
				MailMessage Message = new MailMessage();
				Message.To.Add(sMails.ToString());
				//Message.From = System.Configuration.ConfigurationManager.AppSettings["MailFromMeetings"];
				Message.BodyEncoding= Encoding.UTF8;
		
			
				Message.Subject = string.Format(Resource.ResourceManager["meeting_subject2"],date1,start,project);
				Message.Body = string.Format( Resource.ResourceManager["meeting_body2"],project,sClient,"",date,start,place, sPeople,notes);
				Message.IsBodyHtml = true;
				Message.Attachments.Add(new Attachment(path));
					
				string sAuthMail = System.Configuration.ConfigurationManager.AppSettings["AuthMailMeetings"];
				if(sAuthMail!="")
				{
					//Message.Fields.Add("http://schemas.microsoft.com/cdo/configuration/sendusing", "2");	//basic authentication
					//Message.Fields.Add("http://schemas.microsoft.com/cdo/configuration/smtpauthenticate", "1");	//basic authentication
					//Message.Fields.Add("http://schemas.microsoft.com/cdo/configuration/sendusername", sAuthMail); //set your username here
					//Message.Fields.Add("http://schemas.microsoft.com/cdo/configuration/sendpassword", System.Configuration.ConfigurationManager.AppSettings["AuthPassMeetings"]);	//set your password here
				}

				SmtpClient smtp = new SmtpClient();
				smtp.Send(Message);
				log.Info(string.Format("Sent mail minutes {0} to {1}",Message.Body ,sMails));
				string path1=HttpContext.Current.Request.MapPath("meetingmails");
				StringBuilder sb1 = new StringBuilder();
				sb1.Append("to:"+sMails.ToString());
				sb1.Append("\r\n");
				sb1.Append("subject:"+Message.Subject);
				sb1.Append("\r\n");
				sb1.Append("body:"+string.Format( Resource.ResourceManager["meeting_body2"],project,sClient,"",date,start,place, sPeople,notes));
				sb1.Append("\r\n");
				using (StreamWriter outfile = 
						   new StreamWriter(path1 + @"/Meeting"+DateTime.Now.ToString("ddMMyyyyHHmm")+".txt",true,System.Text.Encoding.UTF8))
				{
					outfile.Write(sb1.ToString());
				}
			}
			catch(Exception ex)
			{
				log.Error(ex);
			}
				
			
		}
		public static void SendMailMinutes(string Mails, string path,string project, string date,string type)
		{
			
			try
			{
				MailMessage Message = new MailMessage();
				Message.To.Add(GetChanged(Mails));
				//Message.From = System.Configuration.ConfigurationManager.AppSettings["MailFromMeetings"];
				Message.BodyEncoding= Encoding.UTF8;
		
			
				Message.Subject = string.Format(Resource.ResourceManager["Protocol_subject"],project,date,type);
				Message.Body = string.Format( Resource.ResourceManager["Protocol_body1"],project,date,type);
				Message.IsBodyHtml = true;
				Message.Attachments.Add(new Attachment(path));
					
				string sAuthMail = System.Configuration.ConfigurationManager.AppSettings["AuthMailMeetings"];
				if(sAuthMail!="")
				{
					//Message.Fields.Add("http://schemas.microsoft.com/cdo/configuration/sendusing", "2");	//basic authentication
					//Message.Fields.Add("http://schemas.microsoft.com/cdo/configuration/smtpauthenticate", "1");	//basic authentication
					//Message.Fields.Add("http://schemas.microsoft.com/cdo/configuration/sendusername", sAuthMail); //set your username here
					//Message.Fields.Add("http://schemas.microsoft.com/cdo/configuration/sendpassword", System.Configuration.ConfigurationManager.AppSettings["AuthPassMeetings"]);	//set your password here
				}

				SmtpClient smtp = new SmtpClient();
				smtp.Send(Message);
			
			}
			catch(Exception ex)
			{
				log.Error(ex);
			}
				
			
		}
		public static string GetChangedA(string toMails)
		{
			
			string sChangeFrom = System.Configuration.ConfigurationManager.AppSettings["ChangeMail"];
			string sChangeTo = System.Configuration.ConfigurationManager.AppSettings["LowAccMail"];
			return Replace(toMails,sChangeFrom,sChangeTo);
			//return toMails.Replace(sChangeFrom,sChangeTo);
		}
		public static string GetChanged(string toMails)
		{
			return toMails;
			//string sChangeFrom = System.Configuration.ConfigurationManager.AppSettings["ChangeMail"];
			//string sChangeTo = System.Configuration.ConfigurationManager.AppSettings["MeetingEmail"];
			//return Replace(toMails,sChangeFrom,sChangeTo);
			//return toMails.Replace(sChangeFrom,sChangeTo);
		}
		private static string Replace(string toMails,string sChangeFrom,string sChangeTo)
		{
			int index=0;
			while(index!=-1)
			{
				index=toMails.IndexOf(sChangeFrom,index);
				if(index==0)
				{
					toMails=sChangeTo+toMails.Substring(sChangeFrom.Length+1);
					index++;
				}
				else if(index>0)
				{
					if(toMails[index-1]!='.')
					{
						toMails=toMails.Substring(0,index)+ sChangeTo+toMails.Substring(index+sChangeFrom.Length);
					}
					index++;
				}
				
			}
			return toMails;
		}
		public static void SendMailApproved(string users, string path,string project, string date, string start, string end,string subs, string clients,string place, string other, int nClient,string notes,string date1)
		{
			
			StringCollection scMail,scNames;
			string sPeople = GetAllPeople(users,clients,subs,other,out scMail,out scNames);

			string sClient = "";
			if(nClient>0)
			{
				ClientData cd= ClientDAL.Load(nClient);
				if(cd!=null)
					sClient= cd.ClientName;
			}
			StringBuilder sMails=new StringBuilder();
			foreach(string s in scMail)
			{
				sMails.Append(s);
				sMails.Append(";");
			}
			try
			{
				MailMessage Message = new MailMessage();
				Message.To.Add(GetChanged(sMails.ToString()));
					//Message.From = System.Configuration.ConfigurationManager.AppSettings["MailFromMeetings"];;
					Message.BodyEncoding= Encoding.UTF8;
		
			
					Message.Subject = string.Format(Resource.ResourceManager["meeting_subject3"],date1,start,project);
					Message.Body = string.Format( Resource.ResourceManager["meeting_body3"],project,sClient,"",date,start,place, sPeople,notes);
					Message.IsBodyHtml = true;
					Message.Attachments.Add(new Attachment(path));
					
					string sAuthMail = System.Configuration.ConfigurationManager.AppSettings["AuthMailMeetings"];
					if(sAuthMail!="")
					{
						//Message.Fields.Add("http://schemas.microsoft.com/cdo/configuration/sendusing", "2");	//basic authentication
						//Message.Fields.Add("http://schemas.microsoft.com/cdo/configuration/smtpauthenticate", "1");	//basic authentication
						//Message.Fields.Add("http://schemas.microsoft.com/cdo/configuration/sendusername", sAuthMail); //set your username here
						//Message.Fields.Add("http://schemas.microsoft.com/cdo/configuration/sendpassword", System.Configuration.ConfigurationManager.AppSettings["AuthPassMeetings"]);	//set your password here
					}

					SmtpClient smtp = new SmtpClient();
					smtp.Send(Message);
			
				}
				catch(Exception ex)
				{
					log.Error(ex);
				}
				
			
		}

		public static void SendProjectMail(string project, string type, string sum,string sdate,string projectNameShort,string projectCode)
		{
			project = projectCode+" "+ projectNameShort+", "+ project;
			StringCollection mails = null, names = null;
			MailDAL.LoadLeadersAndOfficersMails(out names, out mails);
			System.Text.StringBuilder sbMailBody = new System.Text.StringBuilder();

			string subject=Resource.ResourceManager["project_mail_subject"]+ ": "+projectCode+" "+ projectNameShort;
			string body=Resource.ResourceManager["project_mail_body"];
			sbMailBody.Append(string.Format(body,type,project,sum,sdate));
			sbMailBody.Append(String.Concat("<br><br><b>", DateTime.Today.ToString("d"), "</b>"));

			// send mails
			
			for (int i=0; i<mails.Count; i++)
			{
				SendMail(mails[i], subject, sbMailBody.ToString());
			}
		}
		public static void SendNewProjectMail(string project)
		{
			StringCollection mails = null, names = null;
			MailDAL.LoadSecrAndOfficersMails(out names, out mails);
			System.Text.StringBuilder sbMailBody = new System.Text.StringBuilder();

			string subject=Resource.ResourceManager["newproject_mail_subject"];
			string body=Resource.ResourceManager["newproject_mail_body"];
			
			sbMailBody.Append(string.Format(body,project));
			sbMailBody.Append(String.Concat("<br><br><b>", DateTime.Today.ToString("d"), "</b>"));

			// send mails
			
			for (int i=0; i<mails.Count; i++)
			{
				SendMail(mails[i], subject, sbMailBody.ToString());
			}
		}
		public static void SendProjectMail1(string project, string sdate,string projectNameShort,string projectCode)
		{
			project = string.Concat(projectCode," ", projectNameShort,", ", project);
			StringCollection mails = null, names = null;
			MailDAL.LoadLeadersAndOfficersMails(out names, out mails);
			System.Text.StringBuilder sbMailBody = new System.Text.StringBuilder();

			string subject=Resource.ResourceManager["project_mail_subject1"]+": "+projectCode+" "+ projectNameShort;
			string body=Resource.ResourceManager["project_mail_body1"];
			sbMailBody.Append(string.Format(body,sdate,project));
			sbMailBody.Append(String.Concat("<br><br><b>", DateTime.Today.ToString("d"), "</b>"));

			// send mails
			
			for (int i=0; i<mails.Count; i++)
			{
				SendMail(mails[i], subject, sbMailBody.ToString());
			}
		}

//notes:Add by Ivailo on 23.01.2008
		public static void SendMailWorktime(string DirectorMail,string loggedUserName, string DateEntered, string UsersEntered,string account)
		{
//			StringCollection scMail,scNames;
//			string sPeople = GetAllPeople(users,clients,subs,other,out scMail,out scNames);

//			string sClient = "";
//			if(nClient>0)
//			{
//				ClientData cd= ClientDAL.Load(nClient);
//				if(cd!=null)
//					sClient= cd.ClientName;
//			}
			StringBuilder sMails=new StringBuilder();
//			foreach(string s in scMail)
//			{
				sMails.Append(DirectorMail);
//				sMails.Append(";");
//			}
			try
			{
				MailMessage Message = new MailMessage();
				Message.To.Add( GetChanged(sMails.ToString()));
				//set from config mail for Worktime
				//Message.From = System.Configuration.ConfigurationManager.AppSettings["MailFromMeetings"];
				Message.BodyEncoding= Encoding.UTF8;
		
			//set from resources subject for worktime
				Message.Subject = string.Format(Resource.ResourceManager["MailForEnteredWorkTimeTitle"]);
				//set from resources subject for body worktime
				Message.Body = string.Format( Resource.ResourceManager["MailForEnteredWorkTimeBody"],loggedUserName,UsersEntered,DateEntered,account);

				Message.IsBodyHtml = true;
				//Message.Attachments.Add(new Attachment(path));
					
				string sAuthMail = System.Configuration.ConfigurationManager.AppSettings["AuthMailMeetings"];
				if(sAuthMail!="")
				{
					//Message.Fields.Add("http://schemas.microsoft.com/cdo/configuration/sendusing", "2");	//basic authentication
					//Message.Fields.Add("http://schemas.microsoft.com/cdo/configuration/smtpauthenticate", "1");	//basic authentication
					//Message.Fields.Add("http://schemas.microsoft.com/cdo/configuration/sendusername", sAuthMail); //set your username here
					//Message.Fields.Add("http://schemas.microsoft.com/cdo/configuration/sendpassword", System.Configuration.ConfigurationManager.AppSettings["AuthPassMeetings"]);	//set your password here
				}

				SmtpClient smtp = new SmtpClient();
				smtp.Send(Message);
			
			}
			catch(Exception ex)
			{
				log.Error(ex);
			}
				
			
		}



		#region CreateHtml

		#region Recipients header

		public static string CreateRecipientsHeaderHtml(string mailTo, string mailCopyTo)
		{
			int fontSize = 8;

			System.Web.UI.WebControls.Table tblLayout = new System.Web.UI.WebControls.Table();
			tblLayout.BorderWidth=0;

			TableRow tr = new TableRow();
			TableCell tc = new TableCell();

			tr = new TableRow(); tr.Height = Unit.Pixel(10); tc = new TableCell(); tr.Cells.Add(tc); tblLayout.Rows.Add(tr);

			tr = new TableRow(); tc = new TableCell(); tc.Wrap = false; tc.Width = 65;
			tc.Font.Size = FontUnit.Point(fontSize); tc.VerticalAlign = VerticalAlign.Top;	
			tc.Text = String.Concat("<B>", Resource.ResourceManager["reports_MailReport_MailTo"],": ", "</B>");
			tr.Cells.Add(tc);

			tc = new TableCell();
			tc.Font.Size = FontUnit.Point(fontSize); tc.VerticalAlign = VerticalAlign.Top;	
			tc.Text = mailTo;
			tr.Cells.Add(tc);
			tblLayout.Rows.Add(tr);

			tr = new TableRow();
			tc = new TableCell();
			tc.Font.Size = FontUnit.Point(fontSize);tc.VerticalAlign = VerticalAlign.Top;	
			tc.Text = String.Concat("<B>", Resource.ResourceManager["reports_MailReport_MailCopyTo"],": ", "</B>");
			tr.Cells.Add(tc);
			tc = new TableCell();
			tc.Font.Size = FontUnit.Point(fontSize);tc.VerticalAlign = VerticalAlign.Top;	
			tc.Text = mailCopyTo;
			tr.Cells.Add(tc);
			tblLayout.Rows.Add(tr);

			System.Text.StringBuilder sb = new System.Text.StringBuilder();
			System.Web.UI.HtmlTextWriter tw = new System.Web.UI.HtmlTextWriter(new System.IO.StringWriter(sb));

			tblLayout.RenderControl(tw);
			return sb.ToString();
		}

		#endregion

		#region header html

		public static string CreateInfoHeaderHtml(string administrativeName, string projectCode,
			string clientName, string activity, DateTime date)
		{
			int fontSize = 8;

			System.Web.UI.WebControls.Table tblLayout = new System.Web.UI.WebControls.Table();
			tblLayout.BorderWidth=0;
			//tblLayout.CellPadding = 0;
			//tblLayout.CellSpacing = 0;

			TableRow tr = new TableRow();
			TableCell tc = new TableCell();

			tr = new TableRow(); tr.Height = Unit.Pixel(10); tc = new TableCell(); tr.Cells.Add(tc);
			tblLayout.Rows.Add(tr);

			tr = new TableRow();
			tc = new TableCell(); tc.VerticalAlign = VerticalAlign.Top;
			tc.Font.Size = FontUnit.Point(fontSize);
			tc.Text = String.Concat("<B>", Resource.ResourceManager["reports_MailReport_Obekt"],": ", "</B>");
			tr.Cells.Add(tc);
			tc = new TableCell();
			tc.Font.Size = FontUnit.Point(fontSize);tc.VerticalAlign = VerticalAlign.Top;
			tc.Text = administrativeName;
			tr.Cells.Add(tc);
			tblLayout.Rows.Add(tr);

			tr = new TableRow();
			tc = new TableCell();
			tc.Font.Size = FontUnit.Point(fontSize);
			tc.Text = String.Concat("<B>", Resource.ResourceManager["reports_MailReport_Code"],": ", "</B>");tc.VerticalAlign = VerticalAlign.Top;
			tr.Cells.Add(tc);
			tc = new TableCell();
			tc.Font.Size = FontUnit.Point(fontSize);tc.VerticalAlign = VerticalAlign.Top;
			tc.Text = projectCode;
			tr.Cells.Add(tc);
			tblLayout.Rows.Add(tr);
			
			
			tr = new TableRow();
			tc = new TableCell();
			tc.Font.Size = FontUnit.Point(fontSize);
			tc.Text = String.Concat("<B>", Resource.ResourceManager["reports_MailReport_Client"],": ", "</B>"); tc.VerticalAlign = VerticalAlign.Top;
			tr.Cells.Add(tc);
			tc = new TableCell();
			tc.Font.Size = FontUnit.Point(fontSize);tc.VerticalAlign = VerticalAlign.Top;
			tc.Text = clientName;
			tr.Cells.Add(tc);
			tblLayout.Rows.Add(tr);

			
			tr = new TableRow(); tr.Height = Unit.Pixel(10); tc = new TableCell(); tr.Cells.Add(tc);
			tblLayout.Rows.Add(tr);

			tr = new TableRow();
			tc = new TableCell();tc.Font.Size = FontUnit.Point(fontSize);tc.VerticalAlign = VerticalAlign.Top;
			tc.Text = String.Concat("<B>", Resource.ResourceManager["reports_MailReport_Activity"],": ", "</B>");
			tr.Cells.Add(tc);
			tc = new TableCell();
			tc.Font.Size = FontUnit.Point(fontSize);tc.VerticalAlign = VerticalAlign.Top;
			tc.Text = activity;
			tr.Cells.Add(tc);
			tblLayout.Rows.Add(tr);
			
			tr = new TableRow();
			tc = new TableCell();
			tc.Font.Size = FontUnit.Point(fontSize);tc.VerticalAlign = VerticalAlign.Top;
			tc.Text = String.Concat("<B>", Resource.ResourceManager["reports_MailReport_Date"],": ", "</B>"); 
			tr.Cells.Add(tc);
			tc = new TableCell();
			tc.Font.Size = FontUnit.Point(fontSize);tc.VerticalAlign = VerticalAlign.Top;
			tc.Text = date.ToShortDateString();
			tr.Cells.Add(tc);
			tblLayout.Rows.Add(tr);

			
			tr = new TableRow(); tr.Height = Unit.Pixel(20); tc = new TableCell(); tr.Cells.Add(tc);
			tblLayout.Rows.Add(tr);

			System.Text.StringBuilder sb = new System.Text.StringBuilder();
			System.Web.UI.HtmlTextWriter tw = new System.Web.UI.HtmlTextWriter(new System.IO.StringWriter(sb));

			tblLayout.RenderControl(tw);
			return sb.ToString();
		}


		public static string CreateFooterHtml(string userName)
		{
			string html = String.Concat("<br><br><br><B>", Resource.ResourceManager["reports_MailReport_CreatedBy"], ": </B>", userName, "<br>");
			return html;
		}

		#endregion

		public static string CreateLayoutHtml(DataSet ds, string userName, DateTime startDate, DateTime endDate)
		{
			System.Web.UI.WebControls.Table tblLayout = new System.Web.UI.WebControls.Table();
			
			TableRow tr = new TableRow();
			TableCell tc = new TableCell();

			#region Add header

			//Header
			DataTable dtProjectInfo = ds.Tables[0];
			string projectCode = dtProjectInfo.Rows[0]["ProjectCode"].ToString();
			string projectName = 
				(dtProjectInfo.Rows[0]["AdministrativeName"].ToString() == String.Empty) ? dtProjectInfo.Rows[0]["ProjectName"].ToString() : dtProjectInfo.Rows[0]["AdministrativeName"].ToString();

			System.Text.StringBuilder userHeaderHtml = new System.Text.StringBuilder();
			
			userHeaderHtml.Append("<H5>");

			if (userName!=String.Empty) userHeaderHtml.Append(
											String.Concat(Resource.ResourceManager["reports_MailReport_ForUser"]," ", userName));
			else if( ds.Tables[1].Rows.Count>0) userHeaderHtml.Append(Resource.ResourceManager["reports_MailReport_ForAllUsers"]);

			if(startDate!=Constants.DateMax && endDate!=Constants.DateMax)
				if (startDate == endDate) userHeaderHtml.Append(String.Concat(" ", Resource.ResourceManager["reports_For"], " ", startDate.ToString("d")));
				else userHeaderHtml.Append(String.Concat(Resource.ResourceManager["reports_ForPeriodFrom"],
						 startDate.ToString("d"), " ", Resource.ResourceManager["reports_PeriodTo"], " ", endDate.ToString("d")));
			
			userHeaderHtml.Append("<H5>");

			tr = new System.Web.UI.WebControls.TableRow();
			tblLayout.Rows.Add(tr);
			tc = new System.Web.UI.WebControls.TableCell();
			tc.ColumnSpan = 2; tc.HorizontalAlign = HorizontalAlign.Center;
			tc.Text = String.Concat("<H4>", Resource.ResourceManager["mail_Project"], 
				projectCode, " ", projectName, "</H4>", userHeaderHtml.ToString());
			tr.Cells.Add(tc);

			#endregion
			
			DataTable dt = ds.Tables[1];
			DateTime tempDate = new DateTime(1900, 1,1);

			for (int i = 0; i<dt.Rows.Count; i++)
			{
				DataRow dr = dt.Rows[i];

				#region Add Date
				
				DateTime currentDate = (DateTime)dr["WorkDate"];
				if (currentDate!=tempDate)
				{
					tr = new TableRow();
					tc = new TableCell(); tc.ColumnSpan = 2; tc.Font.Bold = true;
					tc.Text = ((DateTime)dr["WorkDate"]).ToString("d");
					tr.Cells.Add(tc);
					tblLayout.Rows.Add(tr);
					tempDate = currentDate;
					
					tr = new TableRow(); tr.Height = Unit.Pixel(5);
					tc = new TableCell(); tc.ColumnSpan = 2;
					tr.Cells.Add(tc);
					tblLayout.Rows.Add(tr);
				
				}

				#endregion

				#region Add minutes

				//minutes row
				string minutes = dr["Minutes"].ToString();
				string adminMunutes = dr["AdminMinutes"].ToString();

//				if (minutes ==String.Empty) minutes = Resource.ResourceManager["reports_MailReport_NoMinutes"];
//				if (adminMunutes == String.Empty) adminMunutes = Resource.ResourceManager["reports_MailReport_NoMinutes"];

				tr = new TableRow();
				TableCell td = new TableCell(); td.Width = Unit.Percentage(50); td.VerticalAlign = VerticalAlign.Top;
				if (minutes !=String.Empty)
					td.Text = GetMinuteHtml(minutes);
				tr.Cells.Add(td);

				td = new TableCell();
				td.ForeColor = System.Drawing.Color.Red; td.VerticalAlign = VerticalAlign.Top;
				if (adminMunutes !=String.Empty)
					td.Text = GetMinuteHtml(adminMunutes);
				tr.Cells.Add(td);
				tblLayout.Rows.Add(tr);
			
				// space
				tr = new TableRow(); tr.Height = Unit.Pixel(5);
				tc = new TableCell(); tc.ColumnSpan = 2;
				tr.Cells.Add(tc);
				tblLayout.Rows.Add(tr);
				#endregion

			}

			System.Text.StringBuilder sb = new System.Text.StringBuilder();
			System.Web.UI.HtmlTextWriter tw = new System.Web.UI.HtmlTextWriter(new System.IO.StringWriter(sb));

			tblLayout.RenderControl(tw);
			return sb.ToString();
		}

		public static string CreateLayoutHtml(string projectName, string projectCode , string administrativeName, string userName, string minutes, string adminMinutes)
		{
			System.Web.UI.WebControls.Table tblLayout = new System.Web.UI.WebControls.Table();
			
			TableRow tr = new System.Web.UI.WebControls.TableRow();
			tblLayout.Rows.Add(tr);
			System.Web.UI.WebControls.TableCell tc = new System.Web.UI.WebControls.TableCell();
			tc.ColumnSpan = 2; tc.HorizontalAlign = HorizontalAlign.Center;
			tc.Text = String.Concat("<H4>", Resource.ResourceManager["mail_Project"], 
				projectCode, " ", administrativeName, "</H4>");
			tr.Cells.Add(tc);

//			if (minutes == String.Empty) minutes = Resource.ResourceManager["reports_MailReport_NoMinutes"];
//			if (adminMinutes == String.Empty) adminMinutes = Resource.ResourceManager["reports_MailReport_NoMinutes"];

			tr = new TableRow();
			TableCell td = new TableCell(); td.Width = Unit.Percentage(50); td.VerticalAlign = VerticalAlign.Top;
			if (minutes != String.Empty)
				td.Text = GetMinuteHtml(minutes);
			tr.Cells.Add(td);

			td = new TableCell();
			td.ForeColor = System.Drawing.Color.Red; td.VerticalAlign = VerticalAlign.Top;
			if (adminMinutes != String.Empty)	
				td.Text = GetMinuteHtml(adminMinutes);
			tr.Cells.Add(td);
			tblLayout.Rows.Add(tr);
			
			System.Text.StringBuilder sb = new System.Text.StringBuilder();
			System.Web.UI.HtmlTextWriter tw = new System.Web.UI.HtmlTextWriter(new System.IO.StringWriter(sb));

			tblLayout.RenderControl(tw);
			return sb.ToString();
		}

		private static string GetMinuteHtml(string minute)
		{
			System.Text.StringBuilder sb = new System.Text.StringBuilder();

			minute = minute.Replace("\r\n", "\r");

			string[] rows = minute.Split('\r');
			
			for (int i=0; i<rows.Length; i++)
			{
				sb.Append("<LI>");
				sb.Append(rows[i]);
				sb.Append("</LI>");
			}
			return sb.ToString();

		}

		#endregion
	}
	
}
