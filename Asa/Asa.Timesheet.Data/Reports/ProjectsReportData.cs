using System;
using System.Data;
using System.Data.SqlClient;
using Microsoft.ApplicationBlocks.Data;

namespace Asa.Timesheet.Data.Reports
{
	/// <summary>
	/// Summary description for ProjectsReportData.
	/// </summary>
	public class ProjectsReportData
	{
    public static ProjectsReportDS CreateReportDataSource(int projectID)
    {
      ProjectsReportDS ds = new ProjectsReportDS();

   //   ds.Project.AddProjectRow(projectID,"","");

      SelectProjectHeader(projectID, ds);
      FillProjectDocumentationData(projectID, ds);

      return ds;

    }

    public static ProjectsReportDS CreateMissingDataReportDataSource(int projectID)
    {
      ProjectsReportDS ds = new ProjectsReportDS();


      SelectProjectHeader(projectID, ds);
      FillMissingProjectDocumentationData(projectID, ds);

      return ds;

    }

    #region Izhodna Proektna dokumentacija report helpers

    public static ProjectsReportDS.ProjectDataTable SelectProjectHeader(int projectID, ProjectsReportDS ds)
    {
      string spName = "ProjectsReport_HeaderInfo";

      SqlCommand cmd = new SqlCommand();
      cmd.CommandType = CommandType.StoredProcedure;
      cmd.CommandText = spName;

      cmd.Parameters.Add("@ProjectID", SqlDbType.Int).Value = projectID;
      
      using (SqlConnection conn = new SqlConnection( DBManager.GetConnectionString() ) )
      {
        cmd.Connection = conn;

        using (SqlDataAdapter da = new SqlDataAdapter(cmd))
        {
          da.Fill(ds.Project);
        }
      }

      return ds.Project;
    }


    public static void FillProjectDocumentationData(int projectID, ProjectsReportDS reportDS)
    {
      #region Izhodni danni za proektirane 
 
      FillInitialData(projectID, reportDS);
    
      #endregion

      #region  Infrastructurni danni za proektirane

      SetInfrastructureDataReportTable(projectID, reportDS);

      #endregion     

      #region Razreshitelno za stroej

      FillBuildPermission(projectID, reportDS);

      #endregion
    }

    public static void FillMissingProjectDocumentationData(int projectID, ProjectsReportDS reportDS)
    {
      #region Izhodni danni za proektirane 
 
      FillInitialData(projectID, reportDS);
    
      #endregion

      #region  Infrastructurni danni za proektirane

      SetInfrastructureDataReportTable(projectID, reportDS);

      #endregion     

      #region Razreshitelno za stroej

      FillBuildPermission(projectID, reportDS);

      #endregion
    }


    #region Load & init Build permission

    public static ProjectsReportDS.BuildPermissionDataDataTable 
      FillBuildPermission(int projectID, ProjectsReportDS ds)
    {
      if (ds == null) ds = new ProjectsReportDS();

      string spName = "ProjectsSelectProfile3";

      SqlCommand cmd = new SqlCommand();
      cmd.CommandType = CommandType.StoredProcedure;
      cmd.CommandText = spName;

      cmd.Parameters.Add("@ProjectID", SqlDbType.Int).Value = projectID;

      
      using (SqlConnection conn = new SqlConnection( DBManager.GetConnectionString() ) )
      {
        cmd.Connection = conn;

        using (SqlDataAdapter da = new SqlDataAdapter(cmd))
        {
          da.Fill(ds.BuildPermissionData);
        }
      }

      if (ds.BuildPermissionData.Rows.Count == 0) return ds.BuildPermissionData;

      ProjectsReportDS.BuildPermissionDataRow row = 
        (ProjectsReportDS.BuildPermissionDataRow)ds.BuildPermissionData.Rows[0];

      bool ideaProjectStatus = !( row.IsIdeaProjectDoneNull() || row.IsIdeaProjectAgreedNull());
      row.IdeaStatus = ideaProjectStatus ? 
        Resource.ResourceManager["Reports_Yes"] : Resource.ResourceManager["Reports_No"];

      bool techProjectStatus = !(row.IsTechProjectDoneNull() || row.IsTechProjectAgreedNull());
      row.TechStatus = techProjectStatus ? 
        Resource.ResourceManager["Reports_Yes"] : Resource.ResourceManager["Reports_No"];
      
      bool buildPermissionStatus = !row.IsBuildPermissionDoneNull();
      row.BuildPermissionStatus = 
        buildPermissionStatus ? Resource.ResourceManager["Reports_Yes"] : Resource.ResourceManager["Reports_No"];
      
      bool otherStatus = ! (row.IsOtherNull() || row.IsOtherDoneNull());
      row.OtherStatus = 
        otherStatus ? Resource.ResourceManager["Reports_Yes"] : Resource.ResourceManager["Reports_No"];

      row.MainStatus = ideaProjectStatus && techProjectStatus && buildPermissionStatus && otherStatus
        ? Resource.ResourceManager["Reports_No"] : Resource.ResourceManager["Reports_Yes"];

      return ds.BuildPermissionData;
    }

    #endregion

    #region Load & init izhodni danni za proektirane

    public static ProjectsReportDS.InitialDataItemsDataTable FillInitialData(int projectID, ProjectsReportDS reportDS)
    {
      if (reportDS == null) reportDS = new ProjectsReportDS();

      #region Load raw data

      string spName = "ProjectsSelectProfile1";

      SqlCommand cmd = new SqlCommand();
      cmd.CommandType = CommandType.StoredProcedure;
      cmd.CommandText = spName;

      cmd.Parameters.Add("@ProjectID", SqlDbType.Int).Value = projectID;

      using (SqlConnection conn = new SqlConnection( DBManager.GetConnectionString() ) )
      {
        cmd.Connection = conn;

        using (SqlDataAdapter da = new SqlDataAdapter(cmd))
        {
          da.Fill(reportDS.InitialData);
        }
      }

      #endregion

      if (reportDS.InitialData.Rows.Count == 0)
      {
        reportDS.InitialDataItems.Rows.Clear();
        return reportDS.InitialDataItems;
      }
       

      ProjectsReportDS.InitialDataRow rowOrig = 
        (ProjectsReportDS.InitialDataRow)reportDS.InitialData.Rows[0];

      ProjectsReportDS.InitialDataItemsDataTable tblReport = reportDS.InitialDataItems;

      ProjectsReportDS.InitialDataItemsRow reportRow = null;

      #region Notarialen akt

      reportRow  = tblReport.NewInitialDataItemsRow();
      
      reportRow.Title = Resource.ResourceManager["ProjectsReport_lblNotarialenAkt"];

      reportRow.Date1Label = Resource.ResourceManager["ProjectsReport_lblZajaven"];
      reportRow.Date2Label = Resource.ResourceManager["ProjectsReport_lblPoluchen"];

      if (rowOrig.IsNotaryDeedsNull()) reportRow.SetDate1Null();
      else reportRow.Date1 = rowOrig.NotaryDeeds;
      
      if (rowOrig.IsNotaryDeedsDoneNull()) reportRow.SetDate2Null();
      else reportRow.Date2 = rowOrig.NotaryDeedsDone;

      reportRow.Status = (reportRow.IsDate1Null() || reportRow.IsDate2Null()) ? 
        Resource.ResourceManager["Reports_No"] : Resource.ResourceManager["Reports_Yes"];
      
      tblReport.AddInitialDataItemsRow(reportRow);

      #endregion

      #region Skica

      reportRow = tblReport.NewInitialDataItemsRow();
 
      reportRow.Title = Resource.ResourceManager["ProjectsReport_lblSkica"];

      reportRow.Date1Label = Resource.ResourceManager["ProjectsReport_lblZajavena"];
      reportRow.Date2Label = Resource.ResourceManager["ProjectsReport_lblPoluchena"];

      if (rowOrig.IsSketchNull()) reportRow.SetDate1Null();
      else reportRow.Date1 = rowOrig.Sketch;

      if (rowOrig.IsSketchDoneNull()) reportRow.SetDate2Null();
      else reportRow.Date2 = rowOrig.SketchDone;

      reportRow.Status = reportRow.IsDate1Null() || reportRow.IsDate2Null() ? 
        Resource.ResourceManager["Reports_No"] : Resource.ResourceManager["Reports_Yes"];

      tblReport.AddInitialDataItemsRow(reportRow);

      #endregion

      #region Pulnomoshtno

      reportRow = tblReport.NewInitialDataItemsRow();
 
      reportRow.Title = Resource.ResourceManager["ProjectsReport_lblPulnomoshtno"];

      reportRow.Date1Label = Resource.ResourceManager["ProjectsReport_lblZajaveno"];
      reportRow.Date2Label = Resource.ResourceManager["ProjectsReport_lblPolucheno"];

      if (rowOrig.IsPowerOfAttorneyDateNull()) reportRow.SetDate1Null();
      else reportRow.Date1 = rowOrig.PowerOfAttorneyDate;
      
      if (rowOrig.IsPowerOfAttorneyDoneDateNull()) reportRow.SetDate2Null();
      else reportRow.Date2 = rowOrig.PowerOfAttorneyDoneDate;

      reportRow.Status = reportRow.IsDate1Null() || reportRow.IsDate2Null() ? 
        Resource.ResourceManager["Reports_No"] : Resource.ResourceManager["Reports_Yes"];
      
      tblReport.AddInitialDataItemsRow(reportRow);

      #endregion

      #region Aktualno sustojanie

      reportRow = tblReport.NewInitialDataItemsRow();
 
      reportRow.Title = Resource.ResourceManager["ProjectsReport_lblAktualnoSustojanie"];

      reportRow.Date1Label = Resource.ResourceManager["ProjectsReport_lblZajaveno"];
      reportRow.Date2Label = Resource.ResourceManager["ProjectsReport_lblPolucheno"];

      if (rowOrig.IsCurrentStatusNull()) reportRow.SetDate1Null();
      else reportRow.Date1 = rowOrig.CurrentStatus;

      if (rowOrig.IsCurrentStatusDoneNull()) reportRow.SetDate2Null();
      else reportRow.Date2 = rowOrig.CurrentStatusDone;

      reportRow.Status = reportRow.IsDate1Null() || reportRow.IsDate2Null() ? 
        Resource.ResourceManager["Reports_No"] : Resource.ResourceManager["Reports_Yes"];

      tblReport.AddInitialDataItemsRow(reportRow);

      #endregion

      #region Regulacionen plan

      reportRow = tblReport.NewInitialDataItemsRow();
 
      reportRow.Title = Resource.ResourceManager["ProjectsReport_lblRegulacionenPlan"];

      reportRow.Date1Label = Resource.ResourceManager["ProjectsReport_lblZajaven"];
      reportRow.Date2Label = Resource.ResourceManager["ProjectsReport_lblPoluchen"];

      if (rowOrig.IsRegPlanNull()) reportRow.SetDate1Null();
      else reportRow.Date1 = rowOrig.RegPlan;

      if (rowOrig.IsRegPlanDoneNull()) reportRow.SetDate2Null();
      else reportRow.Date2 = rowOrig.RegPlanDone;

      reportRow.NumberLabel = Resource.ResourceManager["ProjectsReport_lblVhNomer"];
      if (rowOrig.IsRegPlanNumberNull()) reportRow.SetNumberNull();
      else reportRow.Number = rowOrig.RegPlanNumber;

      reportRow.Status = reportRow.IsDate1Null() || reportRow.IsDate2Null() ? 
        Resource.ResourceManager["Reports_No"] : Resource.ResourceManager["Reports_Yes"];

      tblReport.AddInitialDataItemsRow(reportRow);

      #endregion

      #region Zastroitelen plan

      reportRow = tblReport.NewInitialDataItemsRow();
 
      reportRow.Title = Resource.ResourceManager["ProjectsReport_lblZastroitelenPlan"];

      reportRow.Date1Label = Resource.ResourceManager["ProjectsReport_lblZajaven"];
      reportRow.Date2Label = Resource.ResourceManager["ProjectsReport_lblPoluchen"];

      if (rowOrig.IsBuildPlanNull()) reportRow.SetDate1Null();
      else reportRow.Date1 = rowOrig.BuildPlan;

      if (rowOrig.IsBuildPlanDoneNull()) reportRow.SetDate2Null();
      else reportRow.Date2 = rowOrig.BuildPlanDone;

      reportRow.NumberLabel = Resource.ResourceManager["ProjectsReport_lblVhNomer"];
      if (rowOrig.IsBuildPlanNumberNull()) reportRow.SetNumberNull();
      else reportRow.Number = rowOrig.BuildPlanNumber;

      reportRow.Status = reportRow.IsDate1Null() || reportRow.IsDate2Null() ? 
        Resource.ResourceManager["Reports_No"] : Resource.ResourceManager["Reports_Yes"];

      tblReport.AddInitialDataItemsRow(reportRow);

      #endregion

      #region PreVisa
      
      reportRow = tblReport.NewInitialDataItemsRow();
 
      reportRow.Title = Resource.ResourceManager["ProjectsReport_lblPreVisa"];

      reportRow.Date1Label = Resource.ResourceManager["ProjectsReport_lblZajavena"];
      reportRow.Date2Label = Resource.ResourceManager["ProjectsReport_lblPoluchena"];

      if (rowOrig.IsPreVisaNull()) reportRow.SetDate1Null();
      else reportRow.Date1 = rowOrig.PreVisa;

      if (rowOrig.IsPreVisaDoneNull()) reportRow.SetDate2Null();
      else reportRow.Date2 = rowOrig.PreVisaDone;

      reportRow.NumberLabel = Resource.ResourceManager["ProjectsReport_lblVhNomer"];
      if (rowOrig.IsPreVisaNumberNull()) reportRow.SetNumberNull();
      else reportRow.Number = rowOrig.PreVisaNumber;

      reportRow.Status = reportRow.IsDate1Null() || reportRow.IsDate2Null() ? 
        Resource.ResourceManager["Reports_No"] : Resource.ResourceManager["Reports_Yes"];

      tblReport.AddInitialDataItemsRow(reportRow);

      #endregion

      #region Visa
      
//      reportRow = tblReport.NewInitialDataItemsRow();
// 
//      reportRow.Title = Resource.ResourceManager["ProjectsReport_lblVisa"];
//
//      reportRow.Date1Label = Resource.ResourceManager["ProjectsReport_lblZajavena"];
//      reportRow.Date2Label = Resource.ResourceManager["ProjectsReport_lblPoluchena"];
//
//      if (rowOrig.IsVisaNull()) reportRow.SetDate1Null();
//      else reportRow.Date1 = rowOrig.Visa;
//
//      if (rowOrig.IsVisaDoneNull()) reportRow.SetDate2Null();
//      else reportRow.Date2 = rowOrig.VisaDone;
//
//      reportRow.NumberLabel = Resource.ResourceManager["ProjectsReport_lblVhNomer"];
//      if (rowOrig.IsVisaNumberNull()) reportRow.SetNumberNull();
//      else reportRow.Number = rowOrig.VisaNumber;
//
//      reportRow.Status = reportRow.IsDate2Null() ? 
//        Resource.ResourceManager["Reports_No"] : Resource.ResourceManager["Reports_Yes"];
//
//      tblReport.AddInitialDataItemsRow(reportRow);

      #endregion

      #region Visa plus

      reportRow = tblReport.NewInitialDataItemsRow();
 
      reportRow.Title = Resource.ResourceManager["ProjectsReport_lblVisaPlus"];

      reportRow.Date1Label = Resource.ResourceManager["ProjectsReport_lblZajavena"];
      reportRow.Date2Label = Resource.ResourceManager["ProjectsReport_lblPoluchena"];

      if (rowOrig.IsVisaPlusNull()) reportRow.SetDate1Null();
      else reportRow.Date1 = rowOrig.VisaPlus;

      if (rowOrig.IsVisaPlusDoneNull()) reportRow.SetDate2Null();
      else reportRow.Date2 = rowOrig.VisaPlusDone;

      reportRow.NumberLabel = Resource.ResourceManager["ProjectsReport_lblVhNomer"];
      if (rowOrig.IsVisaPlusNumberNull()) reportRow.SetNumberNull();
      else reportRow.Number = rowOrig.VisaPlusNumber;

      reportRow.Status = reportRow.IsDate1Null() || reportRow.IsDate2Null() ? 
        Resource.ResourceManager["Reports_No"] : Resource.ResourceManager["Reports_Yes"];

      tblReport.AddInitialDataItemsRow(reportRow);

      #endregion

      #region Podzemen kadaster

      reportRow = tblReport.NewInitialDataItemsRow();
 
      reportRow.Title = Resource.ResourceManager["ProjectsReport_lblPodzemenKadaster"];

      reportRow.Date1Label = Resource.ResourceManager["ProjectsReport_lblZajaven"];
      reportRow.Date2Label = Resource.ResourceManager["ProjectsReport_lblPoluchen"];

      if (rowOrig.IsKadasterUndEntNull()) reportRow.SetDate1Null();
      else reportRow.Date1 = rowOrig.KadasterUndEnt;

      if (rowOrig.IsKadasterUndRecNull()) reportRow.SetDate2Null();
      else reportRow.Date2 = rowOrig.KadasterUndRec;

      reportRow.NumberLabel = Resource.ResourceManager["ProjectsReport_lblVhNomer"];
      if (rowOrig.IsKadasterUndNumberNull()) reportRow.SetNumberNull();
      else reportRow.Number = rowOrig.KadasterUndNumber;

      reportRow.Status = reportRow.IsDate1Null() || reportRow.IsDate2Null() ? 
        Resource.ResourceManager["Reports_No"] : Resource.ResourceManager["Reports_Yes"];

      tblReport.AddInitialDataItemsRow(reportRow);

      #endregion

      #region Nadzemen kadaster

      reportRow = tblReport.NewInitialDataItemsRow();
 
      reportRow.Title = Resource.ResourceManager["ProjectsReport_lblNadzemenKadaster"];

      reportRow.Date1Label = Resource.ResourceManager["ProjectsReport_lblZajaven"];
      reportRow.Date2Label = Resource.ResourceManager["ProjectsReport_lblPoluchen"];

      if (rowOrig.IsKadasterEntNull()) reportRow.SetDate1Null();
      else reportRow.Date1 = rowOrig.KadasterEnt;

      if (rowOrig.IsKadasterRecNull()) reportRow.SetDate2Null();
      else reportRow.Date2 = rowOrig.KadasterRec;

      reportRow.NumberLabel = Resource.ResourceManager["ProjectsReport_lblVhNomer"];
      if (rowOrig.IsKadasterNumberNull()) reportRow.SetNumberNull();
      else reportRow.Number = rowOrig.KadasterNumber;

      reportRow.Status = reportRow.IsDate1Null() || reportRow.IsDate2Null() ? 
        Resource.ResourceManager["Reports_No"] : Resource.ResourceManager["Reports_Yes"];

      tblReport.AddInitialDataItemsRow(reportRow);

      #endregion

      return tblReport;

    }

    #endregion

    #region Load & Init infrastrukturni danni za proektirane

    public static ProjectsReportDS.InfrastructureDataItemsDataTable 
      SetInfrastructureDataReportTable(int projectID, ProjectsReportDS reportDS)
    {
      if (reportDS == null) reportDS = new ProjectsReportDS();

      #region Load raw data

      string spName = "ProjectsSelectProfile2";

      SqlCommand cmd = new SqlCommand();
      cmd.CommandType = CommandType.StoredProcedure;
      cmd.CommandText = spName;

      cmd.Parameters.Add("@ProjectID", SqlDbType.Int).Value = projectID;
      
      using (SqlConnection conn = new SqlConnection( DBManager.GetConnectionString() ) )
      {
        cmd.Connection = conn;

        using (SqlDataAdapter da = new SqlDataAdapter(cmd))
        {
          da.Fill(reportDS.InfrastructureData);
        }
      }

      #endregion

      if (reportDS.InfrastructureData.Rows.Count == 0)
      {
        reportDS.InfrastructureDataItems.Rows.Clear();
        return reportDS.InfrastructureDataItems;
      }

      ProjectsReportDS.InfrastructureDataRow rowOrig =
        (ProjectsReportDS.InfrastructureDataRow)reportDS.InfrastructureData.Rows[0];

      ProjectsReportDS.InfrastructureDataItemsDataTable tblReport = reportDS.InfrastructureDataItems;

      ProjectsReportDS.InfrastructureDataItemsRow reportRow = null;
      

      #region VIK

      // Main title
      reportRow = tblReport.NewInfrastructureDataItemsRow();
      reportRow.Title = Resource.ResourceManager["Reports_VIK"];

      // Izhodni danni
      reportRow.IzhDanniLabel = Resource.ResourceManager["Reports_strIzhodniDanni"];

      reportRow.IzhDanniDate1Label = Resource.ResourceManager["ProjectsReport_Vnesen"];
      reportRow.IzhDanniDate2Label = Resource.ResourceManager["ProjectsReport_Poluchen"];

      if (rowOrig.IsVIKEntNull()) reportRow.SetIzhDanniDate1Null();
      else reportRow.IzhDanniDate1 = rowOrig.VIKEnt;
      if (rowOrig.IsVIKRecNull()) reportRow.SetIzhDanniDate2Null();
      else reportRow.IzhDanniDate2 = rowOrig.VIKRec;
 
      bool izhDanniStatus = ! (rowOrig.IsVIKRecNull() || rowOrig.IsVIKEntNull() );
      reportRow.IzhDanniStatus = izhDanniStatus ? 
        Resource.ResourceManager["Reports_Yes"] : Resource.ResourceManager["Reports_No"];
     

      //Pismo i skica
      reportRow.PismoLabel = Resource.ResourceManager["Reports_strPismoISkica"];
      
      reportRow.PismoDate1Label = Resource.ResourceManager["ProjectsReport_Vnesen"];

      if (rowOrig.IsVIKLetterNull()) reportRow.SetPismoDate1Null();
      else reportRow.PismoDate1 = rowOrig.VIKLetter;

      reportRow.PismoStatus = rowOrig.IsVIKLetterNull() ? 
        Resource.ResourceManager["Reports_No"] : Resource.ResourceManager["Reports_Yes"];
      bool pismoStatus = !rowOrig.IsVIKLetterNull();

      reportRow.PismoShow = true;

      // Prisuedinitelen dogovor 
      reportRow.PrisDogovorLabel = Resource.ResourceManager["Reports_strPrisDogovor"];
      reportRow.PrisDogovorDate1Label = Resource.ResourceManager["ProjectsReport_Vnesen"];
     
      if (rowOrig.IsVIKContractNull()) reportRow.SetPrisDogovorDate1Null();
      else reportRow.PrisDogovorDate1 = rowOrig.VIKContract;

      reportRow.PrisDogovorStatus = rowOrig.IsVIKContractNull() ? 
        Resource.ResourceManager["Reports_No"] : Resource.ResourceManager["Reports_Yes"];
      bool prisDogovorStatus = !rowOrig.IsVIKContractNull();

      reportRow.PrisDogovorShow = true;

      // Proekt za vun6ni vruzki
      reportRow.ProjectLabel = Resource.ResourceManager["Reports_strProektVunVruzki"];

      reportRow.ProjectDate1Label = Resource.ResourceManager["ProjectsReport_Vuzlojen"];
      reportRow.ProjectDate2Label = Resource.ResourceManager["ProjectsReport_Suglasuvan"];

      if (rowOrig.IsVIKProjectAssingedNull()) reportRow.SetProjectDate1Null();
      else reportRow.ProjectDate1 = rowOrig.VIKProjectAssinged;
      if (rowOrig.IsVIKProjectAgreedNull()) reportRow.SetProjectDate2Null();
      else reportRow.ProjectDate2 = rowOrig.VIKProjectAgreed;
    
      bool projectStatus = ! (rowOrig.IsVIKProjectAssingedNull() || rowOrig.IsVIKProjectAgreedNull());
      reportRow.ProjectStatus = projectStatus ? Resource.ResourceManager["Reports_Yes"] : Resource.ResourceManager["Reports_No"];
      
      reportRow.ProjectShow = true;

      // Vh nomer
      if (rowOrig.IsVIKNumberNull()) reportRow.SetNumberNull();
      else reportRow.Number = rowOrig.VIKNumber;

      // Amount
      if (rowOrig.IsVIKAmountNull()) reportRow.SetAmountNull();
      else reportRow.Amount = rowOrig.VIKAmount;

      //Notes
      if (rowOrig.IsVIKNotesNull()) reportRow.SetNotesNull();
      else reportRow.Notes = rowOrig.VIKNotes;

      reportRow.MainStatus = 
        (izhDanniStatus && pismoStatus && prisDogovorStatus && projectStatus) ?
        Resource.ResourceManager["Reports_Yes"] : Resource.ResourceManager["Reports_No"];

      tblReport.AddInfrastructureDataItemsRow(reportRow);

      #endregion

      #region EL

      reportRow = tblReport.NewInfrastructureDataItemsRow();
      reportRow.Title = Resource.ResourceManager["Reports_El"];

      //Izhodni danni
      reportRow.IzhDanniLabel = Resource.ResourceManager["Reports_strIzhodniDanni"];

      reportRow.IzhDanniDate1Label = Resource.ResourceManager["ProjectsReport_Vnesen"];
      reportRow.IzhDanniDate2Label = Resource.ResourceManager["ProjectsReport_Poluchen"];

      if (rowOrig.IsELEntNull()) reportRow.SetIzhDanniDate1Null();
      else reportRow.IzhDanniDate1 = rowOrig.ELEnt;
      if (rowOrig.IsELRecNull()) reportRow.SetIzhDanniDate2Null();
      else reportRow.IzhDanniDate2 = rowOrig.ELRec;

      reportRow.IzhDanniStatus = rowOrig.IsELRecNull() ? 
        Resource.ResourceManager["Reports_No"] : Resource.ResourceManager["Reports_Yes"];
      izhDanniStatus = !rowOrig.IsELRecNull();

      //Pismo i skica
      reportRow.PismoLabel = Resource.ResourceManager["Reports_strPismoISkica"];

      reportRow.PismoDate1Label = Resource.ResourceManager["ProjectsReport_Vnesen"];
      if (rowOrig.IsELLetterNull()) reportRow.SetPismoDate1Null();
      else reportRow.PismoDate1 = rowOrig.ELLetter;

      reportRow.PismoStatus = rowOrig.IsELLetterNull() ? 
        Resource.ResourceManager["Reports_No"] : Resource.ResourceManager["Reports_Yes"];

      pismoStatus = !rowOrig.IsELLetterNull();
      reportRow.PismoShow = true;

      // Prisued. dogovor
      reportRow.PrisDogovorLabel = Resource.ResourceManager["Reports_strPrisDogovor"];

      reportRow.PrisDogovorDate1Label = Resource.ResourceManager["ProjectsReport_Vnesen"];
      if (rowOrig.IsELContractNull()) reportRow.SetPrisDogovorDate1Null();
      else reportRow.PrisDogovorDate1 = rowOrig.ELContract;

      reportRow.PrisDogovorStatus = rowOrig.IsELContractNull() ? 
        Resource.ResourceManager["Reports_No"] : Resource.ResourceManager["Reports_Yes"];

      prisDogovorStatus = !rowOrig.IsELContractNull();
      reportRow.PrisDogovorShow = true;

      //Proekt za vun6ni vruzki
      reportRow.ProjectLabel = Resource.ResourceManager["Reports_strProektVunVruzki"];

      reportRow.ProjectDate1Label = Resource.ResourceManager["ProjectsReport_Vuzlojen"];
      reportRow.ProjectDate2Label = Resource.ResourceManager["ProjectsReport_Suglasuvan"];

      if (rowOrig.IsELProjectAssingedNull()) reportRow.SetProjectDate1Null();
      else reportRow.ProjectDate1 = rowOrig.ELProjectAssinged;
      if (rowOrig.IsELProjectAgreedNull()) reportRow.SetProjectDate2Null();
      else reportRow.ProjectDate2 = rowOrig.ELProjectAgreed;

      reportRow.ProjectStatus = rowOrig.IsELProjectAgreedNull() ? 
        Resource.ResourceManager["Reports_No"] : Resource.ResourceManager["Reports_Yes"];
      projectStatus = !rowOrig.IsELProjectAgreedNull();

      reportRow.ProjectShow = true;

      // Vh nomer
      if (rowOrig.IsELNumberNull()) reportRow.SetNumberNull();
      else reportRow.Number = rowOrig.ELNumber;

      //Amount
      if (rowOrig.IsELAmountNull()) reportRow.SetAmountNull();
      else reportRow.Amount = rowOrig.ELAmount;

      //Notes
      if (rowOrig.IsElNotesNull()) reportRow.SetNotesNull();
      else reportRow.Notes = rowOrig.ElNotes;

      reportRow.MainStatus = 
        (izhDanniStatus && pismoStatus && prisDogovorStatus && projectStatus) ?
        Resource.ResourceManager["Reports_Yes"] : Resource.ResourceManager["Reports_No"];

      tblReport.AddInfrastructureDataItemsRow(reportRow);

      #endregion

      #region Toplofikacija

      reportRow = tblReport.NewInfrastructureDataItemsRow();
      reportRow.Title = Resource.ResourceManager["Reports_Toplofikacija"];

      //Izhodni danni
      reportRow.IzhDanniLabel = Resource.ResourceManager["Reports_strIzhodniDanni"];

      reportRow.IzhDanniDate1Label = Resource.ResourceManager["ProjectsReport_Vnesen"];
      reportRow.IzhDanniDate2Label = Resource.ResourceManager["ProjectsReport_Poluchen"];

      if (rowOrig.IsTEntNull()) reportRow.SetIzhDanniDate1Null();
      else reportRow.IzhDanniDate1 = rowOrig.TEnt;
      if (rowOrig.IsTRecNull()) reportRow.SetIzhDanniDate2Null();
      else reportRow.IzhDanniDate2 = rowOrig.TRec;

      reportRow.IzhDanniStatus = rowOrig.IsTRecNull() ? 
        Resource.ResourceManager["Reports_No"] : Resource.ResourceManager["Reports_Yes"];
      izhDanniStatus = !rowOrig.IsTRecNull();

      //Pismo i skica
      reportRow.PismoLabel = Resource.ResourceManager["Reports_strPismoISkica"];

      reportRow.PismoDate1Label = Resource.ResourceManager["ProjectsReport_Vnesen"];
      if (rowOrig.IsTLetterNull()) reportRow.SetPismoDate1Null();
      else reportRow.PismoDate1 = rowOrig.TLetter;

      reportRow.PismoStatus = rowOrig.IsTLetterNull() ? 
        Resource.ResourceManager["Reports_No"] : Resource.ResourceManager["Reports_Yes"];

      pismoStatus = !rowOrig.IsTLetterNull();
      reportRow.PismoShow = true;

      // Prisued. dogovor
      reportRow.PrisDogovorLabel = Resource.ResourceManager["Reports_strPrisDogovor"];

      reportRow.PrisDogovorDate1Label = Resource.ResourceManager["ProjectsReport_Vnesen"];
      if (rowOrig.IsTContractNull()) reportRow.SetPrisDogovorDate1Null();
      else reportRow.PrisDogovorDate1 = rowOrig.TContract;

      reportRow.PrisDogovorStatus = rowOrig.IsTContractNull() ? 
        Resource.ResourceManager["Reports_No"] : Resource.ResourceManager["Reports_Yes"];
      prisDogovorStatus = !rowOrig.IsTContractNull();

      reportRow.PrisDogovorShow = true;

      //Proekt za vun6ni vruzki
      reportRow.ProjectLabel = Resource.ResourceManager["Reports_strProektVunVruzki"];

      reportRow.ProjectDate1Label = Resource.ResourceManager["ProjectsReport_Vuzlojen"];
      reportRow.ProjectDate2Label = Resource.ResourceManager["ProjectsReport_Suglasuvan"];

      if (rowOrig.IsTProjectAssingedNull()) reportRow.SetProjectDate1Null();
      else reportRow.ProjectDate1 = rowOrig.TProjectAssinged;
      if (rowOrig.IsTProjectAgreedNull()) reportRow.SetProjectDate2Null();
      else reportRow.ProjectDate2 = rowOrig.TProjectAgreed;

      reportRow.ProjectStatus = rowOrig.IsTProjectAgreedNull() ? 
        Resource.ResourceManager["Reports_No"] : Resource.ResourceManager["Reports_Yes"];

      projectStatus = !rowOrig.IsTProjectAgreedNull();
      reportRow.ProjectShow = true;

      // Vh nomer
      if (rowOrig.IsTNumberNull()) reportRow.SetNumberNull();
      else reportRow.Number = rowOrig.TNumber;

      //Amount
      if (rowOrig.IsTAmountNull()) reportRow.SetAmountNull();
      else reportRow.Amount = rowOrig.TAmount;

      //Notes
      if (rowOrig.IsTNotesNull()) reportRow.SetNotesNull();
      else reportRow.Notes = rowOrig.TNotes;

      reportRow.MainStatus = 
        (izhDanniStatus && pismoStatus && prisDogovorStatus && projectStatus) ?
        Resource.ResourceManager["Reports_Yes"] : Resource.ResourceManager["Reports_No"];

      tblReport.AddInfrastructureDataItemsRow(reportRow);

      #endregion

      #region Path

      reportRow = tblReport.NewInfrastructureDataItemsRow();
      reportRow.Title = Resource.ResourceManager["Reports_Path"];

      //Izhodni danni
      reportRow.IzhDanniLabel = Resource.ResourceManager["Reports_strIzhodniDanni"];

      reportRow.IzhDanniDate1Label = Resource.ResourceManager["ProjectsReport_Vnesen"];
      reportRow.IzhDanniDate2Label = Resource.ResourceManager["ProjectsReport_Poluchen"];

      if (rowOrig.IsPathEntNull()) reportRow.SetIzhDanniDate1Null();
      else reportRow.IzhDanniDate1 = rowOrig.PathEnt;
      if (rowOrig.IsPathRecNull()) reportRow.SetIzhDanniDate2Null();
      else reportRow.IzhDanniDate2 = rowOrig.PathRec;

      reportRow.IzhDanniStatus = rowOrig.IsPathRecNull() ? 
        Resource.ResourceManager["Reports_No"] : Resource.ResourceManager["Reports_Yes"];

      izhDanniStatus = !rowOrig.IsPathRecNull();

      //Pismo i skica
      reportRow.PismoLabel = Resource.ResourceManager["Reports_strPismoISkica"];

      reportRow.PismoDate1Label = Resource.ResourceManager["ProjectsReport_Vnesen"];
      if (rowOrig.IsPathDateNull()) reportRow.SetPismoDate1Null();
      else reportRow.PismoDate1 = rowOrig.PathDate;

      reportRow.PismoStatus = rowOrig.IsPathDateNull() ? 
        Resource.ResourceManager["Reports_No"] : Resource.ResourceManager["Reports_Yes"];

      pismoStatus = !rowOrig.IsPathDateNull();
      reportRow.PismoShow = true;

      // Prisued. dogovor
      reportRow.PrisDogovorShow = false;
      prisDogovorStatus = true;

      //Proekt za vun6ni vruzki
      reportRow.ProjectLabel = Resource.ResourceManager["Reports_strPathProject"];

      reportRow.ProjectDate1Label = Resource.ResourceManager["ProjectsReport_Vuzlojen"];
      reportRow.ProjectDate2Label = Resource.ResourceManager["ProjectsReport_Suglasuvan"];

      if (rowOrig.IsPathProjectAssingedNull()) reportRow.SetProjectDate1Null();
      else reportRow.ProjectDate1 = rowOrig.PathProjectAssinged;
      if (rowOrig.IsPathProjectAgreedNull()) reportRow.SetProjectDate2Null();
      else reportRow.ProjectDate2 = rowOrig.PathProjectAgreed;

      reportRow.ProjectStatus = rowOrig.IsPathProjectAgreedNull() ? 
        Resource.ResourceManager["Reports_No"] : Resource.ResourceManager["Reports_Yes"];
      
      projectStatus = !rowOrig.IsPathProjectAgreedNull();

      reportRow.ProjectShow = true;

      // Vh nomer
      if (rowOrig.IsPathNumberNull()) reportRow.SetNumberNull();
      else reportRow.Number = rowOrig.PathNumber;

      //Amount
      if (rowOrig.IsPathAmountNull()) reportRow.SetAmountNull();
      else reportRow.Amount = rowOrig.PathAmount;

      //Notes
      if (rowOrig.IsPathNotesNull()) reportRow.SetNotesNull();
      else reportRow.Notes = rowOrig.PathNotes;

      reportRow.MainStatus = 
        (izhDanniStatus && pismoStatus && prisDogovorStatus && projectStatus) ?
        Resource.ResourceManager["Reports_Yes"] : Resource.ResourceManager["Reports_No"];


      tblReport.AddInfrastructureDataItemsRow(reportRow);

      #endregion

      #region Telephone

      reportRow = tblReport.NewInfrastructureDataItemsRow();
      reportRow.Title = Resource.ResourceManager["Reports_Phone"];

      //Izhodni danni
      reportRow.IzhDanniLabel = Resource.ResourceManager["Reports_strIzhodniDanni"];

      reportRow.IzhDanniDate1Label = Resource.ResourceManager["ProjectsReport_Vnesen"];
      reportRow.IzhDanniDate2Label = Resource.ResourceManager["ProjectsReport_Poluchen"];

      if (rowOrig.IsPhoneEntNull()) reportRow.SetIzhDanniDate1Null();
      else reportRow.IzhDanniDate1 = rowOrig.PhoneEnt;
      if (rowOrig.IsPhoneRecNull()) reportRow.SetIzhDanniDate2Null();
      else reportRow.IzhDanniDate2 = rowOrig.PhoneRec;

      reportRow.IzhDanniStatus = rowOrig.IsPhoneRecNull() ? 
        Resource.ResourceManager["Reports_No"] : Resource.ResourceManager["Reports_Yes"];

      izhDanniStatus = ! (rowOrig.IsPhoneEntNull() || rowOrig.IsPhoneRecNull());

      //Pismo i skica
      reportRow.PismoShow = false;
      pismoStatus = true;
      
      //Pris dogovor
      reportRow.PrisDogovorShow = false;
      prisDogovorStatus = true;
      
      //Project
      reportRow.ProjectShow = false;
      projectStatus = true;
      
      //Vh. nomer
      if (rowOrig.IsPhoneNumberNull()) reportRow.SetNumberNull();
      else reportRow.Number = rowOrig.PhoneNumber;

      //Amount
      if (rowOrig.IsPhoneAmountNull()) reportRow.SetAmountNull();
      else reportRow.Amount = rowOrig.PhoneAmount;

      //Notes
      if (rowOrig.IsPhoneNotesNull()) reportRow.SetNotesNull();
      else reportRow.Notes = rowOrig.PhoneNotes;

      reportRow.MainStatus = 
        (izhDanniStatus && pismoStatus && prisDogovorStatus && projectStatus) ?
        Resource.ResourceManager["Reports_Yes"] : Resource.ResourceManager["Reports_No"];

      tblReport.AddInfrastructureDataItemsRow(reportRow);

      #endregion

      return reportDS.InfrastructureDataItems;
    }

    #endregion

    #endregion

    #region Subprojects

    public static DataTable GetSubprojects(int projectID, bool hpr)
    {
      string spName = "ProjectsReport_Subprojects";

      SqlParameter[] parms = SqlHelperParameterCache.GetSpParameterSet(DBManager.GetConnectionString(), spName);
      parms[0].Value = projectID;
	parms[1].Value= hpr;

      DataSet dsSubprojects = SqlHelper.ExecuteDataset(DBManager.GetConnectionString (), CommandType.StoredProcedure, spName, parms);

      return dsSubprojects.Tables[0];
    }

    #endregion

    #region Subcontracters

    public static DataSet GetSubcontracters(int projectID,bool hpr)
    {
      string spName = "ProjectsReport_Subcontracters";

      SqlParameter[] parms = SqlHelperParameterCache.GetSpParameterSet(DBManager.GetConnectionString(), spName);
      parms[0].Value = projectID;
		parms[1].Value = hpr;

      DataSet dsSubcontracters = SqlHelper.ExecuteDataset(DBManager.GetConnectionString (), CommandType.StoredProcedure, spName, parms);

      return dsSubcontracters;
    }

    #endregion

    #region Project Basic info report

    public static DataTable GetProjectBasicInfo(int projectID)
    {
      string spName = "ProjectsReport_BasicInfo";

      SqlParameter[] parms = SqlHelperParameterCache.GetSpParameterSet(DBManager.GetConnectionString(), spName);
      parms[0].Value = projectID;

      DataSet ds = SqlHelper.ExecuteDataset(DBManager.GetConnectionString (), CommandType.StoredProcedure, spName, parms);

      return ds.Tables[0];
    }

    #endregion

    #region Header info

    public static DataTable GetProjectHeaderInfo(int projectID)
    {
      string spName = "ProjectsReport_HeaderInfo";

      SqlParameter[] parms = SqlHelperParameterCache.GetSpParameterSet(DBManager.GetConnectionString(), spName);
      parms[0].Value = projectID;

      DataSet ds = SqlHelper.ExecuteDataset(DBManager.GetConnectionString (), CommandType.StoredProcedure, spName, parms);

      return ds.Tables[0];
    }

    #endregion

	}
}
