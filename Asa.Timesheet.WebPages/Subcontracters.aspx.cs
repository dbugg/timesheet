using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Data.SqlClient;
using Asa.Timesheet.Data;
using Asa.Timesheet.Data.Entities;
using log4net;
using Asa.Timesheet.WebPages.UserControls;
using Asa.Timesheet.Data.Util;

namespace Asa.Timesheet.WebPages
{
	/// <summary>
	/// Summary description for Subcontracters.
	/// </summary>
	public class Subcontracters : TimesheetPageBase
	{
		protected System.Web.UI.WebControls.PlaceHolder menuHolder;
		protected System.Web.UI.WebControls.Label lblError;
		protected System.Web.UI.WebControls.Label lblInfo;
		protected System.Web.UI.WebControls.DataGrid grdClients;
		protected System.Web.UI.WebControls.Panel grid;
		protected System.Web.UI.WebControls.Button btnNewClient;
		private static readonly ILog log = LogManager.GetLogger(typeof(Subcontracters));
		protected Telerik.WebControls.RadGrid gridCalls;
		protected System.Web.UI.WebControls.Button btnExport;
		protected Asa.Timesheet.WebPages.UserControls.PageHeader header;
		private enum GridColumns
		{
			Number=0,
			Name ,
			Type,
			
			Address,
			Phone,
			EditItem,
			DeleteItem
		}
		private void Page_Load(object sender, System.EventArgs e)
		{
//			if (!(LoggedUser.IsLeader || LoggedUser.IsAssistant))
//			{
//				ErrorRedirect(Resource.ResourceManager["noRightsAlert"]);
//			}
			
			UIHelpers.CreateMenu(menuHolder,LoggedUser);
			
			if (!this.IsPostBack)
			{
				header.PageTitle = Resource.ResourceManager["subcontracter_PageTitle"];
				header.UserName = this.LoggedUser.UserName;
				btnNewClient.Text = Resource.ResourceManager["newcontracter_PageTitle1"];

				SortOrder = 1;
				BindGrid();
			}
			if(!(LoggedUser.IsAssistantOnly || LoggedUser.HasPaymentRights ||LoggedUser.IsSecretary))
			{
				btnNewClient.Visible=false;
			}
		}

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
			this.grdClients.ItemCreated += new System.Web.UI.WebControls.DataGridItemEventHandler(this.grdClients_ItemCreated);
			this.grdClients.ItemCommand += new System.Web.UI.WebControls.DataGridCommandEventHandler(this.grdClients_ItemCommand);
			this.btnNewClient.Click += new System.EventHandler(this.btnNewClient_Click);
			this.btnExport.Click += new System.EventHandler(this.btnExport_Click);
			this.Load += new System.EventHandler(this.Page_Load);

		}
		#endregion
		private void BindGrid()
		{
			SqlDataReader reader = null;

			try
			{
				bool hpr = LoggedUser.HasPaymentRights; 
				reader = SubprojectsUDL.SelectSubcontracters(-1,SortOrder,hpr);
				if (reader!=null)
				{
					grdClients.DataSource = reader;
					grdClients.DataKeyField = "SubcontracterID";
					grdClients.DataBind();
					if (!LoggedUser.HasPaymentRights)
						grdClients.Columns[(int)GridColumns.DeleteItem].Visible = false;
				}
			}
			catch (Exception ex)
			{
				log.Error(ex);
				lblError.Text = Resource.ResourceManager["prsubcontracters_ErrorLoadProjects"];
				return;
			}

			finally
			{
				if (reader!=null) reader.Close();
			}

			try
			{
				SetConfirmDelete(grdClients, "ibDeleteItem", 1);
			}
			catch (Exception ex)
			{
				log.Error(ex);
			}
		}
		#region Menu
//		private void CreateMenu()
//		{
//			UserControls.MenuTable menu = new UserControls.MenuTable();
//			menu.ID = "MenuTable";
//
//			ArrayList menuItems = new ArrayList();
//			menuItems.Add(new MenuItemInfo(Resource.ResourceManager["menuItem_Hours"], "Hours.aspx", true));
//			menuItems.Add(new MenuItemInfo(Resource.ResourceManager["menuItem_Projects"], "Projects.aspx"));
//			if ((LoggedUser.IsLeader) || (LoggedUser.IsAssistant)) 
//			{
//				menuItems.Add(new MenuItemInfo(Resource.ResourceManager["menuItem_Clients"], "Clients.aspx"));
//				if (LoggedUser.IsLeader)
//				menuItems.Add(new MenuItemInfo(Resource.ResourceManager["menuItem_Users"], "Users.aspx"));			
//			}
//			menuItems.Add(new MenuItemInfo(Resource.ResourceManager["menuItem_Emails"], "Emails.aspx"));
//			menu.AddMenuGroup(Resource.ResourceManager["menuHeader_Links"], 20, menuItems);
//			
//			//2 group
//			menuItems = new ArrayList();
//			menuItems.Add(new MenuItemInfo(Resource.ResourceManager["menuItem_NewProject"], "EditProject.aspx"));
//			if ((LoggedUser.IsLeader) || (LoggedUser.IsAssistant)) 
//			{
//				menuItems.Add(new MenuItemInfo(Resource.ResourceManager["menuItem_NewClient"], "EditClient.aspx"));
//				if (LoggedUser.IsLeader)
//				menuItems.Add(new MenuItemInfo(Resource.ResourceManager["menuItem_NewUser"], "EditUser.aspx"));		
//			}
//			menuItems.Add(new MenuItemInfo(Resource.ResourceManager["menuItem_NewEmail"], "EditEmail.aspx"));
//			menuItems.Add(new MenuItemInfo(Resource.ResourceManager["newcontracter_PageTitle"], "EditSubContracter.aspx"));
//
//			menu.AddMenuGroup(Resource.ResourceManager["menuHeader_New"], 30, menuItems);
//
//			menuItems = new ArrayList();
//			menuItems.Add(new MenuItemInfo(Resource.ResourceManager["menuItem_WorkTimeReport"], "reports/worktimes.aspx"));
//			menuItems.Add(new MenuItemInfo(Resource.ResourceManager["menuItem_MinutesReport"], "Reports/Minutes.aspx"));
//			if (LoggedUser.IsLeader || LoggedUser.IsAssistant)
//				menuItems.Add(new MenuItemInfo(Resource.ResourceManager["menuItem_MailReport"], "MailForm.aspx"));
//			if (LoggedUser.IsLeader)
//			{
//				menuItems.Add(new MenuItemInfo(Resource.ResourceManager["menuItem_NotEnteredWorkTime"], "reports/EmptyHours.aspx"));
//				menuItems.Add(new MenuItemInfo(Resource.ResourceManager["menuItem_Calendar"], "reports/Calendar.aspx"));
//			}
//
//			menu.AddMenuGroup(Resource.ResourceManager["menuHeader_Reports"], 30, menuItems);
//			
//			menuItems = new ArrayList();
//			menuItems.Add(new MenuItemInfo(Resource.ResourceManager["menuItem_Help"], SessionManager.GetHelpLink(),true,Pages.Subcontracters));
//			menu.AddMenuGroup("", 10, menuItems);
//
//			menuHolder.Controls.Add(menu);
//		}
		#endregion

		private void btnNewClient_Click(object sender, System.EventArgs e)
		{
			Response.Redirect("EditSubContracter.aspx");
		}

		private void grdClients_ItemCommand(object source, System.Web.UI.WebControls.DataGridCommandEventArgs e)
		{
			if (e.CommandName == "Sort")
			{
				int sortOrder = SortOrder;
				switch ((string)e.CommandArgument)
				{
					case "Name": if (sortOrder==1) sortOrder = -1; else sortOrder = 1;
						SortOrder = sortOrder;
						BindGrid(); break;
					case "Type": if (sortOrder==2) sortOrder = -2; else sortOrder = 2;
						SortOrder = sortOrder;
						BindGrid(); break;
					case "Address": if (sortOrder==3) sortOrder = -3; else sortOrder = 3;
						SortOrder = sortOrder;
						BindGrid(); break;
					case "Phone": if (sortOrder==4) sortOrder = -4; else sortOrder = 4;
						SortOrder = sortOrder;
						BindGrid(); break;
				}

				return;
			}
			else if(e.CommandName=="Edit")
			{
				string SID = ((DataGrid)source).DataKeys[e.Item.ItemIndex].ToString();
				Response.Redirect("EditSubcontracter.aspx"+"?sid="+SID); 
				return;
			}
			ImageButton ib = (ImageButton)e.CommandSource;
			string s = ib.ID;
			switch (ib.ID)
			{
				case "ibDeleteItem": int ID = (int)((DataGrid)source).DataKeys[e.Item.ItemIndex];
					SubcontracterDAL.Delete(ID);
					BindGrid();
					break;
				case "ibEditItem":  string SID = ((DataGrid)source).DataKeys[e.Item.ItemIndex].ToString();
					Response.Redirect("EditSubcontracter.aspx"+"?sid="+SID); 
					break;
			}
		}

	
		private void grdClients_ItemCreated(object sender, System.Web.UI.WebControls.DataGridItemEventArgs e)
		{
			if (e.Item.ItemType == ListItemType.Header)
			{
				string imgUrl = "";
				int sortOrder = SortOrder;

				if (sortOrder!=0)
				{
					imgUrl = (sortOrder>0)?"images/sup.gif":"images/sdown.gif";
					Label sep = new Label(); 
					sep.Width = 2; 
					e.Item.Cells[Math.Abs(sortOrder)].Controls.Add(sep);
					ImageButton ib = new ImageButton();
					ib.Height = 11; ib.Width = 11; ib.ImageUrl = imgUrl;
					e.Item.Cells[Math.Abs(sortOrder)].Controls.Add(ib);
				}
			}
		}

		private void btnExport_Click(object sender, System.EventArgs e)
		{
			this.EnableViewState = true;

			System.IO.StringWriter tw = new System.IO.StringWriter();
			System.Web.UI.HtmlTextWriter hw = new System.Web.UI.HtmlTextWriter(tw);
			SqlDataReader reader = null;

			try
			{
				bool hpr = LoggedUser.HasPaymentRights;
				reader = SubprojectsUDL.SelectSubcontracters(-1,SortOrder,hpr);
				if (reader!=null)
				{
					gridCalls.DataSource = reader;
					gridCalls.DataKeyField = "SubcontracterID";
					
					
					gridCalls.AllowSorting=false;
					
					gridCalls.DataBind();
					for(int i=0;i<gridCalls.Columns.Count;i++)
					{
						
						gridCalls.Columns[i].SortExpression=null;
					}
				}
			}
			
			finally
			{
				if (reader!=null) reader.Close();
			}

			gridCalls.MasterTableView.ExportToExcel("Subcontracters");
		}
	}
}
