using System;
using System.Data;
using GrapeCity.ActiveReports;
using GrapeCity.ActiveReports.Controls;
using GrapeCity.ActiveReports.SectionReportModel;
using GrapeCity.ActiveReports.Document.Section;
using GrapeCity.ActiveReports.Document;

namespace Asa.Timesheet.WebPages.Reports
{
    public class FinancePokazateliSub : GrapeCity.ActiveReports.SectionReport
	{
        public FinancePokazateliSub(DataView dv)
        {
            this.DataSource = dv;
            InitializeComponent();
            this.Detail.Format += new EventHandler(Detail_Format);
        }

		#region ActiveReports Designer generated code







































        public void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FinancePokazateliSub));
            this.Detail = new GrapeCity.ActiveReports.SectionReportModel.Detail();
            this.ReportHeader = new GrapeCity.ActiveReports.SectionReportModel.ReportHeader();
            this.ReportFooter = new GrapeCity.ActiveReports.SectionReportModel.ReportFooter();
            this.PageHeader = new GrapeCity.ActiveReports.SectionReportModel.PageHeader();
            this.PageFooter = new GrapeCity.ActiveReports.SectionReportModel.PageFooter();
            this.txtHeaderSubcontracterSalary = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtHeaderImcomePerHour = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtHeaderPaymentPerHour = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtHeaderExpencesPerHour = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtHeaderTotalIncome = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtHeaderTotalSalary = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtHeaderProjectName = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtHeaderAdminSalary = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtHeaderWorkedSalary = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtHeaderWorkedHours = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtHeaderPaymentArea = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtHeaderPayments = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtHeaderArea = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.TextBox3 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.TextBox4 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.TextBox5 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtTotalIncome = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.Line2 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.txtExpencesPerHour = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtTotalSalary = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtSubcontracterSalary = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtImcomePerHour = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtPaymentPerHour = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtProjectName = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtWorkedSalary = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtAdminSalary = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtWorkedHours = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtPaymentArea = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtPayments = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtArea = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtProjectID = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.TextBox1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.TextBox2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.TextBox6 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            ((System.ComponentModel.ISupportInitialize)(this.txtHeaderSubcontracterSalary)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtHeaderImcomePerHour)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtHeaderPaymentPerHour)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtHeaderExpencesPerHour)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtHeaderTotalIncome)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtHeaderTotalSalary)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtHeaderProjectName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtHeaderAdminSalary)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtHeaderWorkedSalary)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtHeaderWorkedHours)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtHeaderPaymentArea)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtHeaderPayments)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtHeaderArea)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TextBox3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TextBox4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TextBox5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTotalIncome)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtExpencesPerHour)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTotalSalary)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSubcontracterSalary)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtImcomePerHour)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPaymentPerHour)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtProjectName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtWorkedSalary)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAdminSalary)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtWorkedHours)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPaymentArea)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPayments)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtArea)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtProjectID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TextBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TextBox2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TextBox6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            // 
            // Detail
            // 
            this.Detail.ColumnSpacing = 0F;
            this.Detail.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
						this.txtTotalIncome,
						this.Line2,
						this.txtExpencesPerHour,
						this.txtTotalSalary,
						this.txtSubcontracterSalary,
						this.txtImcomePerHour,
						this.txtPaymentPerHour,
						this.txtProjectName,
						this.txtWorkedSalary,
						this.txtAdminSalary,
						this.txtWorkedHours,
						this.txtPaymentArea,
						this.txtPayments,
						this.txtArea,
						this.txtProjectID,
						this.TextBox1,
						this.TextBox2,
						this.TextBox6});
            this.Detail.Height = 0.2F;
            this.Detail.Name = "Detail";
            // 
            // ReportHeader
            // 
            this.ReportHeader.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
						this.txtHeaderSubcontracterSalary,
						this.txtHeaderImcomePerHour,
						this.txtHeaderPaymentPerHour,
						this.txtHeaderExpencesPerHour,
						this.txtHeaderTotalIncome,
						this.txtHeaderTotalSalary,
						this.txtHeaderProjectName,
						this.txtHeaderAdminSalary,
						this.txtHeaderWorkedSalary,
						this.txtHeaderWorkedHours,
						this.txtHeaderPaymentArea,
						this.txtHeaderPayments,
						this.txtHeaderArea,
						this.TextBox3,
						this.TextBox4,
						this.TextBox5});
            this.ReportHeader.Height = 0.3847222F;
            this.ReportHeader.Name = "ReportHeader";
            // 
            // ReportFooter
            // 
            this.ReportFooter.Height = 0F;
            this.ReportFooter.Name = "ReportFooter";
            // 
            // PageHeader
            // 
            this.PageHeader.Height = 0F;
            this.PageHeader.Name = "PageHeader";
            // 
            // PageFooter
            // 
            this.PageFooter.Height = 0F;
            this.PageFooter.Name = "PageFooter";
            // 
            // txtHeaderSubcontracterSalary
            // 
            this.txtHeaderSubcontracterSalary.Height = 0.3F;
            this.txtHeaderSubcontracterSalary.Left = 8.895F;
            this.txtHeaderSubcontracterSalary.Name = "txtHeaderSubcontracterSalary";
            this.txtHeaderSubcontracterSalary.Style = "font-size: 8pt; font-weight: bold; text-align: right; ddo-char-set: 1";
            this.txtHeaderSubcontracterSalary.Text = "??????. ??????? EUR";
            this.txtHeaderSubcontracterSalary.Top = 0.0625F;
            this.txtHeaderSubcontracterSalary.Width = 1F;
            // 
            // txtHeaderImcomePerHour
            // 
            this.txtHeaderImcomePerHour.Height = 0.3F;
            this.txtHeaderImcomePerHour.Left = 13.41F;
            this.txtHeaderImcomePerHour.Name = "txtHeaderImcomePerHour";
            this.txtHeaderImcomePerHour.Style = "font-size: 8pt; font-weight: bold; text-align: right; ddo-char-set: 1";
            this.txtHeaderImcomePerHour.Text = "??????? ?? ??? EUR";
            this.txtHeaderImcomePerHour.Top = 0.0625F;
            this.txtHeaderImcomePerHour.Width = 0.8F;
            // 
            // txtHeaderPaymentPerHour
            // 
            this.txtHeaderPaymentPerHour.Height = 0.3F;
            this.txtHeaderPaymentPerHour.Left = 12.6F;
            this.txtHeaderPaymentPerHour.Name = "txtHeaderPaymentPerHour";
            this.txtHeaderPaymentPerHour.Style = "font-size: 8pt; font-weight: bold; text-align: right; ddo-char-set: 1";
            this.txtHeaderPaymentPerHour.Text = "??????? ?? ??? EUR";
            this.txtHeaderPaymentPerHour.Top = 0.0625F;
            this.txtHeaderPaymentPerHour.Width = 0.8F;
            // 
            // txtHeaderExpencesPerHour
            // 
            this.txtHeaderExpencesPerHour.Height = 0.3F;
            this.txtHeaderExpencesPerHour.Left = 10.915F;
            this.txtHeaderExpencesPerHour.Name = "txtHeaderExpencesPerHour";
            this.txtHeaderExpencesPerHour.Style = "font-size: 8pt; font-weight: bold; text-align: right; ddo-char-set: 1";
            this.txtHeaderExpencesPerHour.Text = "??????? ?? ??? EUR";
            this.txtHeaderExpencesPerHour.Top = 0.0625F;
            this.txtHeaderExpencesPerHour.Width = 0.8F;
            // 
            // txtHeaderTotalIncome
            // 
            this.txtHeaderTotalIncome.Height = 0.3F;
            this.txtHeaderTotalIncome.Left = 14.22F;
            this.txtHeaderTotalIncome.Name = "txtHeaderTotalIncome";
            this.txtHeaderTotalIncome.Style = "font-size: 8pt; font-weight: bold; text-align: right; ddo-char-set: 1";
            this.txtHeaderTotalIncome.Text = "?????? ??????? EUR";
            this.txtHeaderTotalIncome.Top = 0.0625F;
            this.txtHeaderTotalIncome.Width = 1F;
            // 
            // txtHeaderTotalSalary
            // 
            this.txtHeaderTotalSalary.Height = 0.3F;
            this.txtHeaderTotalSalary.Left = 9.905F;
            this.txtHeaderTotalSalary.Name = "txtHeaderTotalSalary";
            this.txtHeaderTotalSalary.Style = "font-size: 8pt; font-weight: bold; text-align: right; ddo-char-set: 1";
            this.txtHeaderTotalSalary.SummaryRunning = GrapeCity.ActiveReports.SectionReportModel.SummaryRunning.Group;
            this.txtHeaderTotalSalary.Text = "?????? ??????? EUR";
            this.txtHeaderTotalSalary.Top = 0.0625F;
            this.txtHeaderTotalSalary.Width = 1F;
            // 
            // txtHeaderProjectName
            // 
            this.txtHeaderProjectName.Height = 0.3F;
            this.txtHeaderProjectName.Left = 0.5625F;
            this.txtHeaderProjectName.Name = "txtHeaderProjectName";
            this.txtHeaderProjectName.Style = "font-size: 8pt; font-weight: bold; text-align: left; ddo-char-set: 1";
            this.txtHeaderProjectName.Text = "??????";
            this.txtHeaderProjectName.Top = 0.0625F;
            this.txtHeaderProjectName.Width = 1.875F;
            // 
            // txtHeaderAdminSalary
            // 
            this.txtHeaderAdminSalary.Height = 0.3F;
            this.txtHeaderAdminSalary.Left = 6.46F;
            this.txtHeaderAdminSalary.Name = "txtHeaderAdminSalary";
            this.txtHeaderAdminSalary.Style = "font-size: 8pt; font-weight: bold; text-align: right; ddo-char-set: 1";
            this.txtHeaderAdminSalary.Text = "?????. ????. EUR";
            this.txtHeaderAdminSalary.Top = 0.0625F;
            this.txtHeaderAdminSalary.Width = 0.8F;
            // 
            // txtHeaderWorkedSalary
            // 
            this.txtHeaderWorkedSalary.Height = 0.3F;
            this.txtHeaderWorkedSalary.Left = 5.45F;
            this.txtHeaderWorkedSalary.Name = "txtHeaderWorkedSalary";
            this.txtHeaderWorkedSalary.Style = "font-size: 8pt; font-weight: bold; text-align: right; ddo-char-set: 1";
            this.txtHeaderWorkedSalary.Text = "??????? EUR";
            this.txtHeaderWorkedSalary.Top = 0.0625F;
            this.txtHeaderWorkedSalary.Width = 1F;
            // 
            // txtHeaderWorkedHours
            // 
            this.txtHeaderWorkedHours.Height = 0.3F;
            this.txtHeaderWorkedHours.Left = 4.75F;
            this.txtHeaderWorkedHours.Name = "txtHeaderWorkedHours";
            this.txtHeaderWorkedHours.Style = "font-size: 8pt; font-weight: bold; text-align: right; ddo-char-set: 1";
            this.txtHeaderWorkedHours.Text = "???. ??????";
            this.txtHeaderWorkedHours.Top = 0.0625F;
            this.txtHeaderWorkedHours.Width = 0.6900001F;
            // 
            // txtHeaderPaymentArea
            // 
            this.txtHeaderPaymentArea.Height = 0.3F;
            this.txtHeaderPaymentArea.Left = 2.9375F;
            this.txtHeaderPaymentArea.Name = "txtHeaderPaymentArea";
            this.txtHeaderPaymentArea.Style = "font-size: 8pt; font-weight: bold; text-align: right; ddo-char-set: 1";
            this.txtHeaderPaymentArea.Text = "??????? EUR";
            this.txtHeaderPaymentArea.Top = 0.0625F;
            this.txtHeaderPaymentArea.Width = 0.8825001F;
            // 
            // txtHeaderPayments
            // 
            this.txtHeaderPayments.Height = 0.3F;
            this.txtHeaderPayments.Left = 3.83F;
            this.txtHeaderPayments.Name = "txtHeaderPayments";
            this.txtHeaderPayments.Style = "font-size: 8pt; font-weight: bold; text-align: right; ddo-char-set: 1";
            this.txtHeaderPayments.Text = "???????  EUR/M2";
            this.txtHeaderPayments.Top = 0.0625F;
            this.txtHeaderPayments.Width = 0.8575F;
            // 
            // txtHeaderArea
            // 
            this.txtHeaderArea.Height = 0.3F;
            this.txtHeaderArea.Left = 2.4375F;
            this.txtHeaderArea.Name = "txtHeaderArea";
            this.txtHeaderArea.Style = "font-size: 8pt; font-weight: bold; text-align: right; ddo-char-set: 1";
            this.txtHeaderArea.Text = "M2";
            this.txtHeaderArea.Top = 0.0625F;
            this.txtHeaderArea.Width = 0.49F;
            // 
            // TextBox3
            // 
            this.TextBox3.Height = 0.3F;
            this.TextBox3.Left = 7.25F;
            this.TextBox3.Name = "TextBox3";
            this.TextBox3.Style = "font-size: 8pt; font-weight: bold; text-align: right; ddo-char-set: 1";
            this.TextBox3.Text = "?????. ????. EUR";
            this.TextBox3.Top = 0.0625F;
            this.TextBox3.Width = 0.8F;
            // 
            // TextBox4
            // 
            this.TextBox4.Height = 0.3F;
            this.TextBox4.Left = 8.0625F;
            this.TextBox4.Name = "TextBox4";
            this.TextBox4.Style = "font-size: 8pt; font-weight: bold; text-align: right; ddo-char-set: 1";
            this.TextBox4.Text = "??????- ???????";
            this.TextBox4.Top = 0.0625F;
            this.TextBox4.Width = 0.8F;
            // 
            // TextBox5
            // 
            this.TextBox5.Height = 0.3F;
            this.TextBox5.Left = 11.75F;
            this.TextBox5.Name = "TextBox5";
            this.TextBox5.Style = "font-size: 8pt; font-weight: bold; text-align: right; ddo-char-set: 1";
            this.TextBox5.Text = "??????? EUR/M2";
            this.TextBox5.Top = 0.0625F;
            this.TextBox5.Width = 0.8F;
            // 
            // txtTotalIncome
            // 
            this.txtTotalIncome.DataField = "TotalIncome";
            this.txtTotalIncome.Height = 0.2F;
            this.txtTotalIncome.Left = 14.22F;
            this.txtTotalIncome.Name = "txtTotalIncome";
            this.txtTotalIncome.OutputFormat = resources.GetString("txtTotalIncome.OutputFormat");
            this.txtTotalIncome.Style = "text-align: right";
            this.txtTotalIncome.Text = "TotalIncome";
            this.txtTotalIncome.Top = 0F;
            this.txtTotalIncome.Width = 1F;
            // 
            // Line2
            // 
            this.Line2.Height = 0F;
            this.Line2.Left = 0.5625F;
            this.Line2.LineWeight = 1F;
            this.Line2.Name = "Line2";
            this.Line2.Top = 0F;
            this.Line2.Width = 14.6875F;
            this.Line2.X1 = 0.5625F;
            this.Line2.X2 = 15.25F;
            this.Line2.Y1 = 0F;
            this.Line2.Y2 = 0F;
            // 
            // txtExpencesPerHour
            // 
            this.txtExpencesPerHour.DataField = "ExpencesPerHour";
            this.txtExpencesPerHour.Height = 0.2F;
            this.txtExpencesPerHour.Left = 10.915F;
            this.txtExpencesPerHour.Name = "txtExpencesPerHour";
            this.txtExpencesPerHour.OutputFormat = resources.GetString("txtExpencesPerHour.OutputFormat");
            this.txtExpencesPerHour.Style = "text-align: right";
            this.txtExpencesPerHour.Text = "ExpencesPerHour";
            this.txtExpencesPerHour.Top = 0F;
            this.txtExpencesPerHour.Width = 0.8F;
            // 
            // txtTotalSalary
            // 
            this.txtTotalSalary.DataField = "TotalSalary";
            this.txtTotalSalary.Height = 0.2F;
            this.txtTotalSalary.Left = 9.905F;
            this.txtTotalSalary.Name = "txtTotalSalary";
            this.txtTotalSalary.OutputFormat = resources.GetString("txtTotalSalary.OutputFormat");
            this.txtTotalSalary.Style = "text-align: right";
            this.txtTotalSalary.Text = "TotalSalary";
            this.txtTotalSalary.Top = 0F;
            this.txtTotalSalary.Width = 1F;
            // 
            // txtSubcontracterSalary
            // 
            this.txtSubcontracterSalary.DataField = "SubcontracterSalary";
            this.txtSubcontracterSalary.Height = 0.2F;
            this.txtSubcontracterSalary.Left = 8.895F;
            this.txtSubcontracterSalary.Name = "txtSubcontracterSalary";
            this.txtSubcontracterSalary.OutputFormat = resources.GetString("txtSubcontracterSalary.OutputFormat");
            this.txtSubcontracterSalary.Style = "text-align: right";
            this.txtSubcontracterSalary.Text = "SubcontracterSalary";
            this.txtSubcontracterSalary.Top = 0F;
            this.txtSubcontracterSalary.Width = 1F;
            // 
            // txtImcomePerHour
            // 
            this.txtImcomePerHour.DataField = "ImcomePerHour";
            this.txtImcomePerHour.Height = 0.2F;
            this.txtImcomePerHour.Left = 13.41F;
            this.txtImcomePerHour.Name = "txtImcomePerHour";
            this.txtImcomePerHour.OutputFormat = resources.GetString("txtImcomePerHour.OutputFormat");
            this.txtImcomePerHour.Style = "text-align: right";
            this.txtImcomePerHour.Text = "ImcomePerHour";
            this.txtImcomePerHour.Top = 0F;
            this.txtImcomePerHour.Width = 0.8F;
            // 
            // txtPaymentPerHour
            // 
            this.txtPaymentPerHour.DataField = "PaymentPerHour";
            this.txtPaymentPerHour.Height = 0.2F;
            this.txtPaymentPerHour.Left = 12.6F;
            this.txtPaymentPerHour.Name = "txtPaymentPerHour";
            this.txtPaymentPerHour.OutputFormat = resources.GetString("txtPaymentPerHour.OutputFormat");
            this.txtPaymentPerHour.Style = "text-align: right";
            this.txtPaymentPerHour.Text = "PaymentPerHour";
            this.txtPaymentPerHour.Top = 0F;
            this.txtPaymentPerHour.Width = 0.8F;
            // 
            // txtProjectName
            // 
            this.txtProjectName.DataField = "ProjectName";
            this.txtProjectName.Height = 0.2F;
            this.txtProjectName.Left = 0.5625F;
            this.txtProjectName.Name = "txtProjectName";
            this.txtProjectName.Text = "ProjectName";
            this.txtProjectName.Top = 0F;
            this.txtProjectName.Width = 1.875F;
            // 
            // txtWorkedSalary
            // 
            this.txtWorkedSalary.DataField = "WorkedSalary";
            this.txtWorkedSalary.Height = 0.2F;
            this.txtWorkedSalary.Left = 5.5F;
            this.txtWorkedSalary.Name = "txtWorkedSalary";
            this.txtWorkedSalary.OutputFormat = resources.GetString("txtWorkedSalary.OutputFormat");
            this.txtWorkedSalary.Style = "text-align: right";
            this.txtWorkedSalary.Text = "WorkedSalary";
            this.txtWorkedSalary.Top = 0F;
            this.txtWorkedSalary.Width = 1.0125F;
            // 
            // txtAdminSalary
            // 
            this.txtAdminSalary.DataField = "AdminSalary";
            this.txtAdminSalary.Height = 0.2F;
            this.txtAdminSalary.Left = 6.5225F;
            this.txtAdminSalary.Name = "txtAdminSalary";
            this.txtAdminSalary.OutputFormat = resources.GetString("txtAdminSalary.OutputFormat");
            this.txtAdminSalary.Style = "text-align: right";
            this.txtAdminSalary.Text = "AdminSalary";
            this.txtAdminSalary.Top = 0F;
            this.txtAdminSalary.Width = 0.8F;
            // 
            // txtWorkedHours
            // 
            this.txtWorkedHours.DataField = "WorkedHours";
            this.txtWorkedHours.Height = 0.2F;
            this.txtWorkedHours.Left = 4.7025F;
            this.txtWorkedHours.Name = "txtWorkedHours";
            this.txtWorkedHours.OutputFormat = resources.GetString("txtWorkedHours.OutputFormat");
            this.txtWorkedHours.Style = "text-align: right";
            this.txtWorkedHours.Text = "WorkedHours";
            this.txtWorkedHours.Top = 0F;
            this.txtWorkedHours.Width = 0.735F;
            // 
            // txtPaymentArea
            // 
            this.txtPaymentArea.DataField = "PaymentArea";
            this.txtPaymentArea.Height = 0.2F;
            this.txtPaymentArea.Left = 3.8925F;
            this.txtPaymentArea.Name = "txtPaymentArea";
            this.txtPaymentArea.OutputFormat = resources.GetString("txtPaymentArea.OutputFormat");
            this.txtPaymentArea.Style = "text-align: right";
            this.txtPaymentArea.Text = "PaymentArea";
            this.txtPaymentArea.Top = 0F;
            this.txtPaymentArea.Width = 0.8F;
            // 
            // txtPayments
            // 
            this.txtPayments.DataField = "Payments";
            this.txtPayments.Height = 0.2F;
            this.txtPayments.Left = 3F;
            this.txtPayments.Name = "txtPayments";
            this.txtPayments.OutputFormat = resources.GetString("txtPayments.OutputFormat");
            this.txtPayments.Style = "text-align: right";
            this.txtPayments.Text = "Payments";
            this.txtPayments.Top = 0F;
            this.txtPayments.Width = 0.8825001F;
            // 
            // txtArea
            // 
            this.txtArea.DataField = "Area";
            this.txtArea.Height = 0.2F;
            this.txtArea.Left = 2.4375F;
            this.txtArea.Name = "txtArea";
            this.txtArea.OutputFormat = resources.GetString("txtArea.OutputFormat");
            this.txtArea.Style = "text-align: right; vertical-align: top";
            this.txtArea.Text = "Area";
            this.txtArea.Top = 0F;
            this.txtArea.Width = 0.49F;
            // 
            // txtProjectID
            // 
            this.txtProjectID.DataField = "ProjectID";
            this.txtProjectID.Height = 0.2F;
            this.txtProjectID.Left = 0.5625F;
            this.txtProjectID.Name = "txtProjectID";
            this.txtProjectID.Text = "ProjectID ";
            this.txtProjectID.Top = 0F;
            this.txtProjectID.Visible = false;
            this.txtProjectID.Width = 0.4375F;
            // 
            // TextBox1
            // 
            this.TextBox1.DataField = "LeaderSalary";
            this.TextBox1.Height = 0.2F;
            this.TextBox1.Left = 7.322917F;
            this.TextBox1.Name = "TextBox1";
            this.TextBox1.OutputFormat = resources.GetString("TextBox1.OutputFormat");
            this.TextBox1.Style = "text-align: right";
            this.TextBox1.Text = "AdminSalary";
            this.TextBox1.Top = 0F;
            this.TextBox1.Width = 0.8F;
            // 
            // TextBox2
            // 
            this.TextBox2.DataField = "StaffSalary";
            this.TextBox2.Height = 0.2F;
            this.TextBox2.Left = 8.125F;
            this.TextBox2.Name = "TextBox2";
            this.TextBox2.OutputFormat = resources.GetString("TextBox2.OutputFormat");
            this.TextBox2.Style = "text-align: right";
            this.TextBox2.Text = "AdminSalary";
            this.TextBox2.Top = 0F;
            this.TextBox2.Width = 0.6875F;
            // 
            // TextBox6
            // 
            this.TextBox6.DataField = "ExpencesPerArea";
            this.TextBox6.Height = 0.2F;
            this.TextBox6.Left = 11.74833F;
            this.TextBox6.Name = "TextBox6";
            this.TextBox6.OutputFormat = resources.GetString("TextBox6.OutputFormat");
            this.TextBox6.Style = "text-align: right";
            this.TextBox6.Text = "ExpencesPerArea";
            this.TextBox6.Top = 0F;
            this.TextBox6.Width = 0.8F;
            // 
            // ActiveReport1
            // 
            this.MasterReport = false;
            this.PageSettings.PaperHeight = 11F;
            this.PageSettings.PaperWidth = 8.5F;
            this.PrintWidth = 17.02083F;
            this.Sections.Add(this.ReportHeader);
            this.Sections.Add(this.PageHeader);
            this.Sections.Add(this.Detail);
            this.Sections.Add(this.PageFooter);
            this.Sections.Add(this.ReportFooter);
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule(resources.GetString("$this.StyleSheet"), "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: inherit; font-style: inherit; font-variant: inherit; font-weight: bo" +
                        "ld; font-size: 16pt; font-size-adjust: inherit; font-stretch: inherit", "Heading1", "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: Times New Roman; font-style: italic; font-variant: inherit; font-wei" +
                        "ght: bold; font-size: 14pt; font-size-adjust: inherit; font-stretch: inherit", "Heading2", "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: inherit; font-style: inherit; font-variant: inherit; font-weight: bo" +
                        "ld; font-size: 13pt; font-size-adjust: inherit; font-stretch: inherit", "Heading3", "Normal"));
            ((System.ComponentModel.ISupportInitialize)(this.txtHeaderSubcontracterSalary)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtHeaderImcomePerHour)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtHeaderPaymentPerHour)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtHeaderExpencesPerHour)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtHeaderTotalIncome)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtHeaderTotalSalary)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtHeaderProjectName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtHeaderAdminSalary)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtHeaderWorkedSalary)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtHeaderWorkedHours)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtHeaderPaymentArea)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtHeaderPayments)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtHeaderArea)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TextBox3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TextBox4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TextBox5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTotalIncome)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtExpencesPerHour)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTotalSalary)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSubcontracterSalary)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtImcomePerHour)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPaymentPerHour)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtProjectName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtWorkedSalary)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAdminSalary)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtWorkedHours)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPaymentArea)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPayments)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtArea)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtProjectID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TextBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TextBox2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TextBox6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();
        }

		#endregion

        private void Detail_Format(object sender, EventArgs e)
        {
            if (txtTotalIncome.Text.StartsWith("-"))
                txtPaymentArea.ForeColor = txtProjectName.ForeColor = txtArea.ForeColor = txtPayments.ForeColor =
                    txtWorkedHours.ForeColor = txtWorkedSalary.ForeColor = txtAdminSalary.ForeColor =
                    TextBox2.ForeColor = TextBox1.ForeColor = txtSubcontracterSalary.ForeColor =
                    txtTotalSalary.ForeColor = txtExpencesPerHour.ForeColor =
                    txtPaymentPerHour.ForeColor = txtImcomePerHour.ForeColor = txtTotalIncome.ForeColor = System.Drawing.Color.Red;
            else
                txtPaymentArea.ForeColor = txtProjectName.ForeColor = txtArea.ForeColor = txtPayments.ForeColor =
                    txtWorkedHours.ForeColor = txtWorkedSalary.ForeColor = txtAdminSalary.ForeColor =
                    TextBox2.ForeColor = TextBox1.ForeColor = txtSubcontracterSalary.ForeColor =
                    txtTotalSalary.ForeColor = txtExpencesPerHour.ForeColor =
                    txtPaymentPerHour.ForeColor = txtImcomePerHour.ForeColor = txtTotalIncome.ForeColor = System.Drawing.Color.Black;
        }

        private ReportHeader ReportHeader;
        private TextBox txtHeaderSubcontracterSalary;
        private TextBox txtHeaderImcomePerHour;
        private TextBox txtHeaderPaymentPerHour;
        private TextBox txtHeaderExpencesPerHour;
        private TextBox txtHeaderTotalIncome;
        private TextBox txtHeaderTotalSalary;
        private TextBox txtHeaderProjectName;
        private TextBox txtHeaderAdminSalary;
        private TextBox txtHeaderWorkedSalary;
        private TextBox txtHeaderWorkedHours;
        private TextBox txtHeaderPaymentArea;
        private TextBox txtHeaderPayments;
        private TextBox txtHeaderArea;
        private TextBox TextBox3;
        private TextBox TextBox4;
        private TextBox TextBox5;
        private PageHeader PageHeader;
        private Detail Detail;
        private TextBox txtTotalIncome;
        private Line Line2;
        private TextBox txtExpencesPerHour;
        private TextBox txtTotalSalary;
        private TextBox txtSubcontracterSalary;
        private TextBox txtImcomePerHour;
        private TextBox txtPaymentPerHour;
        private TextBox txtProjectName;
        private TextBox txtWorkedSalary;
        private TextBox txtAdminSalary;
        private TextBox txtWorkedHours;
        private TextBox txtPaymentArea;
        private TextBox txtPayments;
        private TextBox txtArea;
        private TextBox txtProjectID;
        private TextBox TextBox1;
        private TextBox TextBox2;
        private TextBox TextBox6;
        private PageFooter PageFooter;
        private ReportFooter ReportFooter;
	}
}
