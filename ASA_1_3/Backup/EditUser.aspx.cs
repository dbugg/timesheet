using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Data.SqlClient;
using log4net;
using Asa.Timesheet.Data;
using Asa.Timesheet.Data.Entities;
using System.Configuration;
using Asa.Timesheet.Data.Util;
using GrapeCity.ActiveReports.Export.Excel.Section;
using Asa.Timesheet.WebPages.Reports;

using System.IO;
using System.Security.Cryptography;

namespace Asa.Timesheet.WebPages
{
	/// <summary>
	/// Summary description for EditUser.
	/// </summary>
	public partial class EditUser : TimesheetPageBase
	{
		#region WebControls

		protected Asa.Timesheet.WebPages.UserControls.PageHeader header;
		protected System.Web.UI.WebControls.DropDownList ddlPart;

		protected SalariesVector _salaries=null;
		protected SalaryTypesVector _salaryTypes=null;
//Ivailo
		private static readonly ILog log = LogManager.GetLogger(typeof(EditUser));
		private const int salariesColomn = 14;
		
		#endregion

		private enum Months
		{
			Jan=1,
			Feb=2,
			March=3,
			April=4,
			May=5,
			June=6,
			July=7,
			Aug=8,
			Sept=9,
			Oct=10,
			Nov=11,
			Dec=12

		}
        protected string GetTotal(int Year, int Type)
        {
            decimal dTotal = 0;
            for (int i = 1; i <= Enum.GetNames(typeof(Months)).Length; i++)
            {
                dTotal += SalaryUDL.GetSalaryByMonthYearType(_salaries, Year, i, Type);
            }
            if ((SalaryTypes)Type != SalaryTypes.Bonus)
                dTotal = dTotal / Enum.GetNames(typeof(Months)).Length;
            return UIHelpers.FormatDecimal2(dTotal);
        }

        protected string GetSalary(int Year, int Type, int Month)
        {
            return UIHelpers.FormatDecimal2(SalaryUDL.GetSalaryByMonthYearType(_salaries, Year, Month, Type));
        }
        protected void Page_Load(object sender, System.EventArgs e)
        {

            if (! (LoggedUser.IsLeader  || LoggedUser.IsAdmin)) ErrorRedirect(Resource.ResourceManager["noRightsAlert"]);
            //if (! (LoggedUser.IsLeader || LoggedUser.IsAssistant)) ErrorRedirect(Resource.ResourceManager["noRightsAlert"]);
            lkCalendar1.Attributes["onclick"] = TimeHelper.InvokePopupCal(txtWorkDateStart);
            if (Page.IsPostBack == false)
            {
                // Set up form for data change checking when
                // first loaded.
                this.CheckForDataChanges = true;
                this.BypassPromptIds =
                    new string[] { "btnSave", "btnSaveUP", "btnDelete" };
            }
            UIHelpers.CreateMenu(menuHolder, LoggedUser);
            tbl.Visible = LoggedUser.HasPaymentRights;
            if (UserID > 0)
                _salaries = SalaryUDL.GetSalariesByUser(UserID);
            bool bHasRights = (LoggedUser.HasPaymentRights && (!LoggedUser.IsAccountantLow));
            lbSalary.Visible = grdSalaries.Visible = btnExport.Visible = bHasRights;
            lblVacation.Visible = txtVacation.Visible = bHasRights;
            if (!this.IsPostBack)
            {
                header.UserName = this.LoggedUser.UserName;
                UIHelpers.SetUserOccupationDDL(ddlOccupation);
                int userID = UserID;
                if (userID > 0) // Edit
                {
                    header.PageTitle = Resource.ResourceManager["editUser_EditLabel"];
                    if (!LoadUser(userID)) lblError.Text = Resource.ResourceManager["editUser_ErrorLoadUser"];

                    if (!(LoggedUser.HasPaymentRights || LoggedUser.IsAdmin || LoggedUser.IsSecretary ))
                    {
                        txtPassword.Enabled = ddlUserRole.Enabled =
                            txtCV.Enabled = txtExt.Enabled = txtHome.Enabled = txtMobile.Enabled = ddlOccupation.Enabled =
                            txtAccount.Enabled = txtEmail.Enabled = txtFirstName.Enabled = txtSecondName.Enabled =
                            txtLastName.Enabled = ddlRole.Enabled = cbIsASI.Enabled = cbIsTraine.Enabled = cbHasWorkTime.Enabled =
                            txtWorkDateStart.Enabled = txtMobilePersonal.Enabled = cbHasWorkTimeOver.Enabled = txtCoeff.Enabled  = false;
                        lkCalendar1.Visible = btnDelete.Visible = btnSave.Visible = btnSaveUP.Visible = false;

                    }

                    //if (!LoggedUser.IsLeader) ddlRole.Enabled = false; //can change roles
                    
                }
                else // New
                {
                    if (!(LoggedUser.HasPaymentRights || LoggedUser.IsAdmin || LoggedUser.IsSecretary ))
                    {
                        ErrorRedirect(Resource.ResourceManager["noRightsAlert"]);
                        return;
                    }
                    header.PageTitle = Resource.ResourceManager["editUser_NewLabel"];

                    // if non admin can change roles
                    /*	if (!LoggedUser.IsLeader)
                    {
                        LoadRoles("3");
                        ddlRole.Enabled = false;
                    }
                    else */

                    LoadRoles(0);
                    lbSalary.Visible = grdSalaries.Visible = btnExport.Visible = false;
                }

                if (userID == -1) btnDelete.Visible = false;
                else SetConfirmDelete(btnDelete, Resource.ResourceManager["users_ConfirmDeleteDefaultName"]);
                lblError.Text = "";
            }
        }

	
		#region Web Form Designer generated code
        override protected void OnInit(EventArgs e)
        {
            //
            // CODEGEN: This call is required by the ASP.NET Web Form Designer.
            //
            InitializeComponent();
            base.OnInit(e);
        }
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
        private void InitializeComponent()
        {

        }
		#endregion

		#region Database

        private bool LoadRoles(int selectedIndex)
        {
            SqlDataReader reader = null;

            try
            {
                reader = DBManager.SelectRoles();
                ddlRole.DataSource = reader;
                ddlRole.DataValueField = "RoleID";
                ddlRole.DataTextField = "Role";
                ddlRole.DataBind();

                ddlRole.Items.Insert(0, new ListItem("", "0"));
                ddlRole.SelectedIndex = selectedIndex;

                IfLoggedUserIsNotLeader(selectedIndex);


            }
            catch (Exception ex)
            {
                log.Error(ex);
                return false;

            }
            finally
            {
                if (reader != null) reader.Close();
            }

            return true;

        }
        private bool LoadManager(string selectValue)
        {
            SqlDataReader reader = null;

            try
            {
                reader = UsersData.SelectUserNames();
                ddlManager.DataSource = reader;
                ddlManager.DataValueField = "UserID";
                ddlManager.DataTextField = "FullName";
                ddlManager.DataBind();

                ddlManager.Items.Insert(0, new ListItem("", "0"));
                if (ddlManager.Items.FindByValue(selectValue) != null)
                    ddlManager.SelectedValue = selectValue;

                //IfLoggedUserIsNotLeader(selectValue);


            }
            catch (Exception ex)
            {
                log.Error(ex);
                return false;

            }
            finally
            {
                if (reader != null) reader.Close();
            }

            return true;
        }
        protected void IfLoggedUserIsNotLeader(int selectValue)
        {
            if (LoggedUser.HasPaymentRights)
            {
                if (LoggedUser.IsAccountant)
                {
                    //					if(LoggedUser.IsAccountantLow)
                    //					{
                    //						if(selectValue == "1" || selectValue=="4" || selectValue=="5")
                    //						{
                    //							ddlRole.Enabled = false;
                    //							txtEmail.Enabled = false;
                    //							txtPassword.Enabled = false;
                    //						}
                    //						else
                    //						{
                    //							ListItem li = ddlRole.Items[5];
                    //							ddlRole.Items.Remove(li);
                    //							li = ddlRole.Items[4];
                    //							ddlRole.Items.Remove(li);
                    //							li = ddlRole.Items[1];
                    //							ddlRole.Items.Remove(li);
                    //						}
                    //					}
                    //					else
                    //					{
                    //						if(selectValue == "1" || selectValue=="5")
                    //						{
                    //							ddlRole.Enabled = false;
                    //							txtEmail.Enabled = false;
                    //							txtPassword.Enabled = false;
                    //						}	
                    //						else
                    //						{
                    //							ListItem li = ddlRole.Items[5];
                    //							ddlRole.Items.Remove(li);
                    //							li = ddlRole.Items[1];
                    //							ddlRole.Items.Remove(li);
                    //						}
                    //					}
                }
            }
            if (LoggedUser.IsAdmin)
            {
                if (selectValue == 1 || selectValue == 4 || selectValue == 9)
                {
                    ddlRole.Enabled = false;
                    txtEmail.Enabled = false;
                    txtPassword.Enabled = false;
                }
                else
                {
                    ListItem li = ddlRole.Items[9];
                    ddlRole.Items.Remove(li);
                    li = ddlRole.Items[4];
                    ddlRole.Items.Remove(li);
                    li = ddlRole.Items[1];
                    ddlRole.Items.Remove(li);
                }
            }
            if (LoggedUser.IsSecretary)
            {
                if (selectValue == 1 || selectValue == 4 || selectValue == 9)
                {
                    ddlRole.Enabled = false;
                    txtEmail.Enabled = false;
                    txtPassword.Enabled = false;
                }
                else
                {
                    ListItem li = ddlRole.Items[9];
                    ddlRole.Items.Remove(li);
                    li = ddlRole.Items[4];
                    ddlRole.Items.Remove(li);
                    li = ddlRole.Items[1];
                    ddlRole.Items.Remove(li);
                }
            }
        }
        protected string GetSalaryType(int Type)
        {

            SalaryTypeData std = _salaryTypes.GetByID(Type);
            if (std != null)
                return std.SalaryType;
            return "";
        }
        private ArrayList GetSalaryArray()
        {
            ArrayList al = new ArrayList();
            int CurrentYear = DateTime.Now.Year;
            int StartYear = int.Parse(System.Configuration.ConfigurationManager.AppSettings["MinSalaryYear"]);
            for (int j = CurrentYear; j >= StartYear; j--)
            {
                for (int i = 1; i <= Enum.GetNames(typeof(SalaryTypes)).Length; i++)
                {
                    SalaryYearData syd = new SalaryYearData(j, i);
                    al.Add(syd);
                }
            }
            return al;
        }
        private void SetSalaryGrid()
        {

            grdSalaries.DataSource = GetSalaryArray();
            grdSalaries.DataBind();
        }

        private bool LoadUser(int userID)
        {
            int roleID = 0;
            int manager = 0;

            _salaryTypes = SalaryTypeDAL.LoadCollection();
            SetSalaryGrid();
            SqlDataReader reader = null;
            try
            {
                reader = UsersData.SelectUser(userID);
                reader.Read();

                roleID = reader.IsDBNull(1) ? 0 : reader.GetInt32(1);
                txtFirstName.Text = reader.IsDBNull(2) ? String.Empty : reader.GetString(2);
                txtSecondName.Text = reader.IsDBNull(3) ? String.Empty : reader.GetString(3);
                txtLastName.Text = reader.GetString(4);
                txtAccount.Text = reader.GetString(5);
                txtEmail.Text = reader.GetString(6);

                
                txtHdnPassword.Text = reader.IsDBNull(9) ? String.Empty : reader.GetString(9);
                //for (int i = 0; i < txtPassword.Text.Length; i++) hdnPassword.Value += "*";
                string occ = reader.IsDBNull(11) ? String.Empty : reader.GetString(11);
                if (ddlOccupation.Items.FindByValue(occ) != null)
                    ddlOccupation.SelectedValue = occ;
                string r = reader.IsDBNull(12) ? String.Empty : reader.GetString(12);
                if (ddlUserRole.Items.FindByValue(r) != null)
                    ddlUserRole.SelectedValue = r;
                int r1 = reader.GetInt32(1);
                LoadRoles(r1);

                txtExt.Text = reader.IsDBNull(13) ? String.Empty : reader.GetString(13);
                txtMobile.Text = reader.IsDBNull(14) ? String.Empty : reader.GetString(14);
                txtHome.Text = reader.IsDBNull(15) ? String.Empty : reader.GetString(15);
                txtCV.Text = reader.IsDBNull(16) ? String.Empty : reader.GetString(16);
                
                txtSalary.Text = reader.IsDBNull(17) ? String.Empty : reader.GetDecimal(17).ToString();
                txtBonus.Text = reader.IsDBNull(18) ? String.Empty : reader.GetDecimal(18).ToString();

                if (!reader.IsDBNull(19))
                { cbIsTraine.Checked = reader.GetBoolean(19); }
                else
                { cbIsTraine.Checked = false; }
                txtMobilePersonal.Text = reader.IsDBNull(20) ? String.Empty : reader.GetString(20);
                if (!reader.IsDBNull(21))
                { cbIsASI.Checked = reader.GetBoolean(21); }
                else
                { cbIsASI.Checked = false; }
                if (!reader.IsDBNull(22))
                { cbHasWorkTime.Checked = reader.GetBoolean(22); }
                else
                { cbHasWorkTime.Checked = true; }
                if (reader.IsDBNull(23))
                    txtWorkDateStart.Text = "";
                else
                {
                    DateTime dt = reader.GetDateTime(23);
                    txtWorkDateStart.Text = dt.ToString(Constants.TextDateFormat);
                }
                gridSalaryHide(cbHasWorkTime.Checked);
                //Add by Ivailo date: 04.12.2007
                //notes:new parameter for user who add work time overtime
                if (!reader.IsDBNull(24))
                { cbHasWorkTimeOver.Checked = reader.GetBoolean(24); }
                else
                { cbHasWorkTimeOver.Checked = false; }
                OldAccName = reader.GetString(6);
                OldAccPass = reader.IsDBNull(9) ? String.Empty : reader.GetString(9);
                if (!reader.IsDBNull(25))
                    manager = reader.GetInt32(25);

                if (!reader.IsDBNull(26))
                    txtVacation.Text = reader.GetInt32(26).ToString();
                if (!reader.IsDBNull(27))
                    txtNameEN.Text = reader.GetString(27);
                if (!reader.IsDBNull(28))
                    txtPrivate.Text = reader.GetString(28);
                txtCoeff.Text = reader.IsDBNull(29) ? "1" : reader.GetDecimal(29).ToString("n2");
            }
            catch (Exception ex)
            {
                log.Error(ex);
                return false;
            }
            finally
            {
                if (reader != null) reader.Close();
            }

            //can not edit this user - no rights
            //if (LoggedUser.UserRoleID>roleID) ErrorRedirect(Resource.ResourceManager["noRightsAlert"]);

            if (!LoadRoles(roleID)) return false;
            if (!LoadManager(manager.ToString())) return false;
            return true;
        }

        private void gridSalaryHide(bool HasWorkTime)
        {
            //			if(HasWorkTime)
            //			{
            //				if(LoggedUser.HasPaymentRights)
            //					lbSalary.Visible = grdSalaries.Visible = true;
            //			}
            //			else
            //			{
            //				lbSalary.Visible = grdSalaries.Visible = false;
            //			}
        }
        private bool DeleteUser(int userID)
        {
            try
            {
                UsersData.InactiveUser(userID);
            }
            catch (Exception ex)
            {
                log.Error(ex);
                return false;
            }

            return true;
        }

        private bool UpdateUser(int userID, int roleID, string firstName, string secondName,
            string lastName, string account, string email, string password,
            string occupation, string userrole, string ext, string mobile, string home, string cv, decimal salary, decimal bonus, bool istraine, string mobilePersonal,
            bool isASI, bool HasWorkTime, DateTime WorkDateStart, bool HasWorkTimeOver, int manager, int vacation, float coefficient)
        {
            try
            {
                UsersData.UpdateUser(userID, roleID, firstName, secondName, lastName, account, email, password,
                    occupation, userrole, ext, mobile, home, cv, salary, bonus, istraine, mobilePersonal,
                    isASI, HasWorkTime, WorkDateStart, HasWorkTimeOver, manager, vacation, txtNameEN.Text, txtPrivate.Text, coefficient);
            }
            catch (Exception ex)
            {
                log.Error(ex);
                return false;
            }

            return true;
        }

        private bool InsertUser(int roleID, string firstName, string secondName,
            string lastName, string account, string email, string password,
            string occupation, string userrole, string ext, string mobile, string home, string cv, decimal salary, decimal bonus, bool istraine,
            string mobilePersonal, bool isASI, bool HasWorkTime, DateTime WorkDateStart, bool HasWorkTimeOver, int manager, int vacation, float coefficient)
        {
            try
            {
                UsersData.InsertUser(roleID, firstName, secondName, lastName, account, email, true, password,
                    occupation, userrole, ext, mobile, home, cv, salary, bonus, istraine, mobilePersonal,
                    isASI, HasWorkTime, WorkDateStart, HasWorkTimeOver, manager, vacation, txtNameEN.Text, txtPrivate.Text, coefficient);
            }
            catch (Exception ex)
            {
                log.Error(ex);
                return false;
            }

            return true;
        }

		#endregion

		#region Event handlers


        protected void btnSave_Click(object sender, System.EventArgs e)
        {
            #region validate input

            string firstName = txtFirstName.Text.Trim();
            string secondName = txtSecondName.Text.Trim();
            string lastName = txtLastName.Text.Trim();
            if (lastName == String.Empty)
            {
                AlertFieldNotEntered(lblLastName);
                return;
            }
            string account = txtAccount.Text.Trim();
            //			if (account == String.Empty)
            //			{
            //				AlertFieldNotEntered(lblAccount);
            //				return;
            //			}
            string email = txtEmail.Text.Trim();
            if (email == String.Empty)
            {
                AlertFieldNotEntered(lblEmail);
                return;
            }
            int roleID = int.Parse(ddlRole.SelectedValue);
            if (roleID == 0)
            {
                AlertFieldNotEntered(lblRole);
                hdnPassword.Value = "";
                return;
            }
            int Role = ddlOccupation.SelectedIndex;
            if (roleID != (int)Roles.MainAccount && roleID != (int)Roles.Admin && roleID != (int)Roles.Technical && roleID != (int)Roles.Secretary && roleID != (int)Roles.Account && roleID != (int)Roles.Cleaner && roleID != (int)Roles.Layer && roleID != (int)Roles.Driver && Role == 0)
            {
                AlertFieldNotEntered(Label2);
                hdnPassword.Value = "";
                return;
            }

            string password = txtPassword.Text.Trim();

            if (UserID == -1 && password == String.Empty)
            {
                AlertFieldNotEntered(lblPassword);
                hdnPassword.Value = "";
                return;
            }

            //if (password == hdnPassword.Value) password = txtHdnPassword.Text;

            if (!(UIHelpers.CheckForUniqueNameAccount(txtEmail.Text, OldAccName/*_accName*/)))
            {
                lblError.Text = string.Format(Resource.ResourceManager["ErrorNotUniqueName"], txtEmail.Text);
                return;
            }
            float coef = 1;
            if (!float.TryParse(this.txtCoeff.Text, out coef))
            {
                 lblError.Text = "Коефициент трбява да е число";
                return;
            }
                
            #endregion

            //			if(!LoggedUser.HasPaymentRights && 
            //				(roleID==Constants.DirectorRole ||roleID==Constants.AccRole ||roleID==Constants.InjPartnerRole ))
            //				ErrorRedirect(Resource.ResourceManager["noRightsAlert"]);


            decimal dSalary, dBonus;
            dSalary = dBonus = 0;
            int nVacation = 0;
            int userID = UserID;
            if (txtSalary.Text != "")
                dSalary = decimal.Parse(txtSalary.Text);
            if (txtBonus.Text != "")
                dBonus = decimal.Parse(txtBonus.Text);
            if (txtVacation.Text != "")
                nVacation = int.Parse(txtVacation.Text);
         
            DateTime dt = Constants.DateMax;
            if (txtWorkDateStart.Text.Length > 0)
                dt = TimeHelper.GetDate(txtWorkDateStart.Text);
            bool bWorkTime = cbHasWorkTime.Checked;
            if (cbHasWorkTimeOver.Checked)
                bWorkTime = true;
            if (!String.IsNullOrEmpty(password))
            {
                password = HashPassword(password);
            }
            if (userID == -1)
            {

                if (!InsertUser(roleID, firstName, secondName, lastName, account, email, password, ddlOccupation.SelectedValue, ddlUserRole.SelectedValue, txtExt.Text, txtMobile.Text, txtHome.Text, txtCV.Text, dSalary, dBonus, cbIsTraine.Checked, txtMobilePersonal.Text, cbIsASI.Checked, bWorkTime, dt, cbHasWorkTimeOver.Checked, UIHelpers.ToInt(ddlManager.SelectedValue), nVacation, coef))
                {
                    lblError.Text = Resource.ResourceManager["editUser_ErrorSave"];
                    return;
                }
                else
                    log.Info(string.Format("User {0} has been INSERTED by {1}", userID, string.Concat(LoggedUser.FullName, " ( userID ", LoggedUser.UserID, " )")));
            }
            else
            {
                if (String.IsNullOrEmpty(password))
                {
                    password = txtHdnPassword.Text.Trim();
                }
                if (!UpdateUser(userID, roleID, firstName, secondName, lastName, account, email, password, ddlOccupation.SelectedValue, ddlUserRole.SelectedValue, txtExt.Text, txtMobile.Text, txtHome.Text, txtCV.Text, dSalary, dBonus, cbIsTraine.Checked, txtMobilePersonal.Text, cbIsASI.Checked, bWorkTime, dt, cbHasWorkTimeOver.Checked, UIHelpers.ToInt(ddlManager.SelectedValue), nVacation, coef))
                {
                    lblError.Text = Resource.ResourceManager["editUser_ErrorSave"];
                    return;
                }
                else
                    log.Info(string.Format("User {0} has been UPDATED by {1}", userID, string.Concat(LoggedUser.FullName, " ( userID ", LoggedUser.UserID, " )")));
            }
            //new
            if (LoggedUser.HasPaymentRights)
            {
                ArrayList al = GetSalaryArray();
                SalariesVector sv = new SalariesVector();
                for (int i = 0; i < grdSalaries.Items.Count; i++)
                {
                    //DataGrid grdSalary=(DataGrid) grdSalaries.Items[i].FindControl("grdSalary");
                    SalaryYearData syd = (SalaryYearData)al[i];
                    TextBox txtJan = (TextBox)grdSalaries.Items[i].FindControl("txtJan");
                    sv.Add(GetSalaryData(txtJan, Months.Jan, syd));
                    TextBox txtFeb = (TextBox)grdSalaries.Items[i].FindControl("txtFeb");
                    sv.Add(GetSalaryData(txtFeb, Months.Feb, syd));
                    TextBox txtMarch = (TextBox)grdSalaries.Items[i].FindControl("txtMarch");
                    sv.Add(GetSalaryData(txtMarch, Months.March, syd));
                    TextBox txtApril = (TextBox)grdSalaries.Items[i].FindControl("txtApril");
                    sv.Add(GetSalaryData(txtApril, Months.April, syd));
                    TextBox txtMay = (TextBox)grdSalaries.Items[i].FindControl("txtMay");
                    sv.Add(GetSalaryData(txtMay, Months.May, syd));
                    TextBox txtJune = (TextBox)grdSalaries.Items[i].FindControl("txtJune");
                    sv.Add(GetSalaryData(txtJune, Months.June, syd));
                    TextBox txtJuly = (TextBox)grdSalaries.Items[i].FindControl("txtJuly");
                    sv.Add(GetSalaryData(txtJuly, Months.July, syd));
                    TextBox txtAug = (TextBox)grdSalaries.Items[i].FindControl("txtAug");
                    sv.Add(GetSalaryData(txtAug, Months.Aug, syd));
                    TextBox txtSept = (TextBox)grdSalaries.Items[i].FindControl("txtSept");
                    sv.Add(GetSalaryData(txtSept, Months.Sept, syd));
                    TextBox txtOct = (TextBox)grdSalaries.Items[i].FindControl("txtOct");
                    sv.Add(GetSalaryData(txtOct, Months.Oct, syd));
                    TextBox txtNov = (TextBox)grdSalaries.Items[i].FindControl("txtNov");
                    sv.Add(GetSalaryData(txtNov, Months.Nov, syd));
                    TextBox txtDec = (TextBox)grdSalaries.Items[i].FindControl("txtDec");
                    sv.Add(GetSalaryData(txtDec, Months.Dec, syd));
                }
                foreach (SalarieData sd in sv)
                    SalarieDAL.Save(sd);
            }
            Response.Redirect("EditUser.aspx?uid=" + UserID.ToString());

        }
        private SalarieData GetSalaryData(TextBox tb, Months m, SalaryYearData syd)
        {
            SalarieData sd = new SalarieData();
            sd.Salary = UIHelpers.ParseDecimal(tb.Text);
            sd.SalaryMonth = (int)m;
            sd.SalaryTypeID = syd.SalaryType;
            sd.SalaryYear = syd.SalaryYear;
            sd.UserID = UserID;
            sd.SalaryID = SalaryUDL.GetSalaryIDByMonthYearType(_salaries, sd.SalaryYear,
                sd.SalaryMonth, syd.SalaryType);
            return sd;
        }
        protected void btnCancel_Click(object sender, System.EventArgs e)
        {
            Response.Redirect("Users.aspx");
        }

        protected void btnDelete_Click(object sender, System.EventArgs e)
        {
            if (!DeleteUser(UserID))
            {
                lblError.Text = Resource.ResourceManager["editUser_ErrorDelete"];
                return;
            }
            else
                log.Info(string.Format("User {0} has been DELETED by {1}", UserID, string.Concat(LoggedUser.FullName, " ( userID ", LoggedUser.UserID, " )")));

            Response.Redirect("Users.aspx");
        }
        protected void cbIsTraine_CheckedChanged(object sender, System.EventArgs e)
        {

        }

        protected void cbHasWorkTime_CheckedChanged(object sender, System.EventArgs e)
        {
            gridSalaryHide(cbHasWorkTime.Checked);
        }
	
		#endregion
	
		#region Export	

        protected void btnExport_Click(object sender, System.EventArgs e)
        {
            if (UserID < 1)
            {
                lblError.Text = Resource.ResourceManager["ErrorNoSave"];
                return;
            }
            DataTable dt = new DataTable();
            _salaryTypes = SalaryTypeDAL.LoadCollection();
            BindSalaries(out dt);
            dt.TableName = string.Concat(txtFirstName.Text, " ", txtLastName.Text);
            DataView dv = new DataView(dt);

            XlsExport pdf = new XlsExport();

            GrapeCity.ActiveReports.SectionReport report = new SalaryRpt(dv);
            report.PageSettings.Orientation = GrapeCity.ActiveReports.Document.Section.PageOrientation.Landscape;
            report.Run();
            System.IO.MemoryStream memoryFile = new System.IO.MemoryStream();
            pdf.Export(report.Document, memoryFile);

            Response.Clear();
            Response.AppendHeader("content-disposition"
                , "attachment; filename=" + "Salary.xls");
            Response.ContentType = "application/pdf";

            memoryFile.WriteTo(Response.OutputStream);
            Response.End();
        }
        private DataTable AddColomns()
        {
            DataTable dt = new DataTable();
            for (int i = 0; i < salariesColomn; i++)
            {
                dt.Columns.Add(i.ToString());
            }
            return dt;
        }
        private bool BindSalaries(out DataTable dtSalaries)
        {
            dtSalaries = AddColomns();
            int CurrentYear = DateTime.Now.Year;
            int StartYear = int.Parse(System.Configuration.ConfigurationManager.AppSettings["MinSalaryYear"]);
            for (int j = CurrentYear; j >= StartYear; j--)
            {
                for (int i = 1; i <= Enum.GetNames(typeof(SalaryTypes)).Length; i++)
                {
                    DataRow dr = dtSalaries.NewRow();
                    string Type = GetSalaryType(i);
                    dr[0] = string.Concat(j, " - ", Type);
                    for (int month = 1; month <= 12; month++)
                    {
                        dr[month] = GetSalary(j, i, month);
                    }
                    dr[13] = GetTotal(j, i);
                    dtSalaries.Rows.Add(dr);
                }
            }
            return true;
        }



		#endregion
		
		#region private properties

		private int UserID
		{
			get 
			{
				try
				{
					return int.Parse(Request.Params["uid"]);
				}
				catch
				{
					return -1;
				}
			}		
		}

		#endregion
        private string HashPassword(string pass)
        {
            byte[] salt;
            new RNGCryptoServiceProvider().GetBytes(salt = new byte[16]);
            var pbkdf2 = new Rfc2898DeriveBytes(pass, salt, 1000);
            byte[] hash = pbkdf2.GetBytes(20);
            byte[] hashBytes = new byte[36];
            Array.Copy(salt, 0, hashBytes, 0, 16);
            Array.Copy(hash, 0, hashBytes, 16, 20);
            return Convert.ToBase64String(hashBytes);
        }
		
	}
}
