using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Data.SqlClient;
using Asa.Timesheet.Data;
using Asa.Timesheet.Data.Entities;
using log4net;

namespace Asa.Timesheet.WebPages
{
	/// <summary>
	/// Summary description for Automobiles.
	/// </summary>
	public class Automobiles : TimesheetPageBase
	{
		protected System.Web.UI.WebControls.PlaceHolder menuHolder;
		protected System.Web.UI.WebControls.DataGrid grdAuto;
		protected System.Web.UI.WebControls.Label lblName;
		protected System.Web.UI.WebControls.Button btnNew;
		protected System.Web.UI.WebControls.Label Label2;
		protected System.Web.UI.WebControls.DataGrid grdInsuranceAgents;
		protected System.Web.UI.WebControls.DataGrid grdAutoServices;
		protected System.Web.UI.WebControls.Panel grid;
		protected Asa.Timesheet.WebPages.UserControls.PageHeader header;
		protected System.Web.UI.WebControls.Label Label10;
		protected System.Web.UI.WebControls.Button Button1;
		protected System.Web.UI.WebControls.Button Button2;
		private static readonly ILog log = LogManager.GetLogger(typeof(Automobiles));

        private void Page_Load(object sender, System.EventArgs e)
        {
            UIHelpers.CreateMenu(menuHolder, LoggedUser);
            header.UserName = LoggedUser.FullName;
            header.PageTitle = Resource.ResourceManager["auto_PageTitle"];
            if (!IsPostBack)
                BindGrid();
            if (!(LoggedUser.IsSecretary || LoggedUser.HasPaymentRights || LoggedUser.IsAssistant ))
            {
                ErrorRedirect(Resource.ResourceManager["noRightsAlert"]);
            }
        }

		#region Web Form Designer generated code
        override protected void OnInit(EventArgs e)
        {
            //
            // CODEGEN: This call is required by the ASP.NET Web Form Designer.
            //
            InitializeComponent();
            base.OnInit(e);
        }
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
        private void InitializeComponent()
        {
            this.grdAuto.ItemCommand += new System.Web.UI.WebControls.DataGridCommandEventHandler(this.grdAuto_ItemCommand);
            this.grdInsuranceAgents.ItemCommand += new System.Web.UI.WebControls.DataGridCommandEventHandler(this.grdInsuranceAgents_ItemCommand);
            this.grdAutoServices.ItemCommand += new System.Web.UI.WebControls.DataGridCommandEventHandler(this.grdAutoServices_ItemCommand);
            this.btnNew.Click += new System.EventHandler(this.btnNew_Click);
            this.Button1.Click += new System.EventHandler(this.Button1_Click);
            this.Button2.Click += new System.EventHandler(this.Button2_Click);
            this.Load += new System.EventHandler(this.Page_Load);

        }
		#endregion

        private void BindGrid()
        {
            grdAuto.DataSource = AutomobileDAL.LoadCollection();
            grdAuto.DataKeyField = "AutomobileID";
            grdAuto.DataBind();
            SetConfirmDelete(grdAuto, "ibDeleteItem", 1);

            grdInsuranceAgents.DataSource = InsuranceAgentDAL.LoadCollection();
            grdInsuranceAgents.DataKeyField = "InsuranceAgentID";
            grdInsuranceAgents.DataBind();
            SetConfirmDelete(grdInsuranceAgents, "ibDeleteItemI", 1);

            grdAutoServices.DataSource = AutoServiceDAL.LoadCollection();
            grdAutoServices.DataKeyField = "AutoServiceID";
            grdAutoServices.DataBind();
            SetConfirmDelete(grdAutoServices, "ibDeleteItemS", 1);
        }
        private void grdAuto_ItemCommand(object source, System.Web.UI.WebControls.DataGridCommandEventArgs e)
        {

            if (e.CommandName == "Edit")
            {
                string sClientID = ((DataGrid)source).DataKeys[e.Item.ItemIndex].ToString();
                Response.Redirect("EditAutomobile.aspx" + "?id=" + sClientID);
                return;
            }
            ImageButton ib = (ImageButton)e.CommandSource;
            string s = ib.ID;
            switch (ib.ID)
            {
                case "ibDeleteItem": int clientID = (int)((DataGrid)source).DataKeys[e.Item.ItemIndex];
                    AutomobileDAL.Delete(clientID);
                    log.Info(string.Format("Auto {0} has been DELETED by {1}", clientID, string.Concat(LoggedUser.FullName, " ( userID ", LoggedUser.UserID, " )")));
                    BindGrid();
                    break;
                case "ibEditItem": string sClientID = ((DataGrid)source).DataKeys[e.Item.ItemIndex].ToString();
                    Response.Redirect("EditAutomobile.aspx" + "?id=" + sClientID);
                    break;
            }

        }

        private void Button1_Click(object sender, System.EventArgs e)
        {
            Response.Redirect("EditAutoService.aspx");
        }

        private void Button2_Click(object sender, System.EventArgs e)
        {
            Response.Redirect("EditInsuranceAgent.aspx");
        }

        private void grdAutoServices_ItemCommand(object source, System.Web.UI.WebControls.DataGridCommandEventArgs e)
        {
            if (e.CommandName == "Edit")
            {
                string sClientID = ((DataGrid)source).DataKeys[e.Item.ItemIndex].ToString();
                Response.Redirect("EditAutoService.aspx" + "?id=" + sClientID);
                return;
            }
            ImageButton ib = (ImageButton)e.CommandSource;
            string s = ib.ID;
            switch (ib.ID)
            {
                case "ibDeleteItemS": int clientID = (int)((DataGrid)source).DataKeys[e.Item.ItemIndex];
                    AutoServiceDAL.Delete(clientID);
                    log.Info(string.Format("Auto {0} has been DELETED by {1}", clientID, string.Concat(LoggedUser.FullName, " ( userID ", LoggedUser.UserID, " )")));
                    BindGrid();
                    break;
                case "ibEditItemS": string sClientID = ((DataGrid)source).DataKeys[e.Item.ItemIndex].ToString();
                    Response.Redirect("EditAutoService.aspx" + "?id=" + sClientID);
                    break;
            }
        }

        private void grdInsuranceAgents_ItemCommand(object source, System.Web.UI.WebControls.DataGridCommandEventArgs e)
        {
            if (e.CommandName == "Edit")
            {
                string sClientID = ((DataGrid)source).DataKeys[e.Item.ItemIndex].ToString();
                Response.Redirect("EditInsuranceAgent.aspx" + "?id=" + sClientID);
                return;
            }
            ImageButton ib = (ImageButton)e.CommandSource;
            string s = ib.ID;
            switch (ib.ID)
            {
                case "ibDeleteItemI": int clientID = (int)((DataGrid)source).DataKeys[e.Item.ItemIndex];
                    InsuranceAgentDAL.Delete(clientID);
                    log.Info(string.Format("Auto {0} has been DELETED by {1}", clientID, string.Concat(LoggedUser.FullName, " ( userID ", LoggedUser.UserID, " )")));
                    BindGrid();
                    break;
                case "ibEditItemI": string sClientID = ((DataGrid)source).DataKeys[e.Item.ItemIndex].ToString();
                    Response.Redirect("EditInsuranceAgent.aspx" + "?id=" + sClientID);
                    break;
            }
        }

        private void btnNew_Click(object sender, System.EventArgs e)
        {
            Response.Redirect("EditAutomobile.aspx");
        }

		
	}
}
