using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Configuration;
using log4net;
using log4net.Config;
using Asa.Timesheet.Data;

namespace Asa.Timesheet.WebPages.Reports
{
	/// <summary>
	/// Summary description for MontlyExpence.
	/// </summary>
	public class MontlyExpence : TimesheetPageBase
	{
		protected System.Web.UI.WebControls.PlaceHolder menuHolder;
		protected Asa.Timesheet.WebPages.UserControls.PageHeader header;
		protected System.Web.UI.WebControls.Label lblTitle;
		protected System.Web.UI.WebControls.Label lblUser;
		protected System.Web.UI.WebControls.Label lblError;
		protected System.Web.UI.WebControls.Label lblInfo;
		protected System.Web.UI.WebControls.DropDownList ddlMonth;
		protected System.Web.UI.WebControls.TextBox txtMonthExpence;
		protected System.Web.UI.WebControls.Button btnSave;
		protected System.Web.UI.WebControls.Label Label9;
		protected System.Web.UI.WebControls.Label lbID;
		protected System.Web.UI.WebControls.Label lbExpences;
		protected System.Web.UI.WebControls.DataGrid grdMain;
		protected System.Web.UI.WebControls.Panel grid;
		protected System.Web.UI.WebControls.Label Label1;
		protected System.Web.UI.WebControls.TextBox txtASI;
		private static readonly ILog log = LogManager.GetLogger(typeof(MontlyExpence));

        private void Page_Load(object sender, System.EventArgs e)
        {
            if (!this.IsPostBack)
            {
                LoadMonths();

                header.PageTitle = Resource.ResourceManager["MontlyExpence_PageTitle"];
                header.UserName = this.LoggedUser.UserName;
            }
            UIHelpers.CreateMenuReports(menuHolder, LoggedUser);
            BindGrid();

        }
        private void BindGrid()
        {
            grdMain.DataSource = ExpencesMontlyDAL.LoadCollectionAll();
            grdMain.DataKeyField = "ExpenceID";
            grdMain.DataBind();
        }

        public string GetMonth(DateTime dt)
        {
            return dt.ToString("MM/yyyy");
        }

		#region Web Form Designer generated code
        override protected void OnInit(EventArgs e)
        {
            //
            // CODEGEN: This call is required by the ASP.NET Web Form Designer.
            //
            InitializeComponent();
            base.OnInit(e);
        }
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
        private void InitializeComponent()
        {
            this.ddlMonth.SelectedIndexChanged += new System.EventHandler(this.ddlMonth_SelectedIndexChanged);
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            this.Load += new System.EventHandler(this.Page_Load);

        }
		#endregion

        private void LoadMonths()
        {
            int selectedIndex = 0;
            int startYear = UIHelpers.ToInt(System.Configuration.ConfigurationManager.AppSettings["MinSalaryYear"]);
            for (int iyear = startYear; iyear <= DateTime.Now.Year; iyear++)
            {
                int _monts = 13;
                if (iyear == DateTime.Now.Year)
                    _monts = DateTime.Now.Month + 1;
                for (int iMonth = 1; iMonth < _monts; iMonth++)
                {
                    int ItemValue = (iyear - startYear) * 12 + iMonth;
                    ListItem li = new ListItem(GetMonthName(iMonth, iyear), ItemValue.ToString());
                    ddlMonth.Items.Add(li);
                    if (iyear == DateTime.Now.Year && iMonth == DateTime.Now.Month)
                        selectedIndex = ddlMonth.Items.IndexOf(li);
                }

            }
            ddlMonth.SelectedIndex = selectedIndex;
            SetSelectedIndex(ddlMonth.SelectedIndex);
        }

        private string GetMonthName(int month, int year)
        {
            string smonth = "";
            switch (month)
            {
                case (1): { smonth = Resource.ResourceManager["MonthYanuary"]; break; }
                case (2): { smonth = Resource.ResourceManager["MonthFebruary"]; break; }
                case (3): { smonth = Resource.ResourceManager["MonthMarch"]; break; }
                case (4): { smonth = Resource.ResourceManager["MonthApril"]; break; }
                case (5): { smonth = Resource.ResourceManager["MonthMay"]; break; }
                case (6): { smonth = Resource.ResourceManager["MonthYuny"]; break; }
                case (7): { smonth = Resource.ResourceManager["MonthYuly"]; break; }
                case (8): { smonth = Resource.ResourceManager["MonthAugust"]; break; }
                case (9): { smonth = Resource.ResourceManager["MonthSeptember"]; break; }
                case (10): { smonth = Resource.ResourceManager["MonthOctober"]; break; }
                case (11): { smonth = Resource.ResourceManager["MonthNovember"]; break; }
                case (12): { smonth = Resource.ResourceManager["MonthDecember"]; break; }

            }
            return string.Format(Resource.ResourceManager["DateMonthAndYear"], smonth, year.ToString());
        }

        private void SetSelectedIndex(int index)
        {
            string val = ddlMonth.SelectedValue;
            int ival = UIHelpers.ToInt(val);

            int iYear = UIHelpers.ToInt(System.Configuration.ConfigurationManager.AppSettings["MinSalaryYear"]) + ival / 12;
            int iMonth = ival % 12;
            if (iMonth == 0)
            {
                iMonth = 12;
                iYear--;
            }
            DateTime dtSeek = new DateTime(iYear, iMonth, 1);
            ExpencesMontlyData dat = ExpencesMontlyDAL.Load(dtSeek);
            if (dat != null)
            {
                txtMonthExpence.Text = dat.Expences.ToString();
                txtASI.Text = dat.ExpencesASI.ToString();
                lbID.Text = dat.ExpenceID.ToString();
            }
            else
            {
                txtMonthExpence.Text = "";
                lbID.Text = "-1";
            }
        }

        private void btnSave_Click(object sender, System.EventArgs e)
        {
            if (UIHelpers.ToInt(txtMonthExpence.Text) > -1)
            {
                string val = ddlMonth.SelectedValue;
                int ival = UIHelpers.ToInt(val);

                int iYear = UIHelpers.ToInt(System.Configuration.ConfigurationManager.AppSettings["MinSalaryYear"]) + ival / 12;
                int iMonth = ival % 12;
                if (iMonth == 0)
                {
                    iMonth = 12;
                    iYear--;
                }
                int nExpensesASI = 0;
                if (UIHelpers.ToInt(txtASI.Text) > -1)
                {
                    nExpensesASI = UIHelpers.ToInt(txtASI.Text);
                }
                DateTime dtSeek = new DateTime(iYear, iMonth, 1);
                ExpencesMontlyData dat = new ExpencesMontlyData((UIHelpers.ToInt(lbID.Text) > 0) ? UIHelpers.ToInt(lbID.Text) : -1, dtSeek, UIHelpers.ToInt(txtMonthExpence.Text));
                dat.ExpencesASI = nExpensesASI;
                ExpencesMontlyDAL.Save(dat);
                BindGrid();
            }
            else
            {
                AlertFieldNotEntered(lbExpences);
                return;
            }
        }

        private void ddlMonth_SelectedIndexChanged(object sender, System.EventArgs e)
        {
            SetSelectedIndex(ddlMonth.SelectedIndex);
        }



	}
}
