using System;
using System.Collections;
using System.Text;

namespace Asa.Timesheet.Data
{
	public class MeetingProjectData
	{
		private int _meetingProjectID = -1;
		private int _meetingID = -1;
		private int _projectID = -1;

		/// <summary>
		/// Initialize an new empty Login object.
		/// </summary>
		public MeetingProjectData()
		{
		}
		
		/// <summary>
		/// Initialize a new Login object with the given parameters.
		/// </summary>
		public MeetingProjectData(int meetingProjectID, int meetingID, int projectID)
		{
			this._meetingProjectID = meetingProjectID;
			this._meetingID = meetingID;
			this._projectID = projectID;
		}	
		
	  
		public override string ToString()
		{
			StringBuilder builder = new StringBuilder();
			builder.Append("_meetingProjectID: " + _meetingProjectID.ToString()).Append("\r\n");
			builder.Append("_meetingID: " + _meetingID.ToString()).Append("\r\n");
			builder.Append("_projectID: " + _projectID.ToString()).Append("\r\n");
			builder.Append("\r\n");
			return builder.ToString();
		}	
		public object GetByIndex(int nIndex)
		{
			switch(nIndex)
			{
				case 0:
					return _meetingProjectID;
				case 1:
					return _meetingID;
				case 2:
					return _projectID;
			}
			return null;
		}
		#region Properties
	
		public int MeetingProjectID
		{
			get { return _meetingProjectID; }
			set { _meetingProjectID = value; }
		}
	
		public int MeetingID
		{
			get { return _meetingID; }
			set { _meetingID = value; }
		}
	
		public int ProjectID
		{
			get { return _projectID; }
			set { _projectID = value; }
		}
	
		#endregion
	}
}


