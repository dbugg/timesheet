using System;
using System.Data;
using System.Data.SqlClient; 
using System.Collections;
using Microsoft.ApplicationBlocks.Data;

namespace Asa.Timesheet.Data
{
	public class SalaryTypeDAL
	{
		/// <summary>
		/// Loads a SalaryTypeData object from the database using the given SalaryTypeID
		/// </summary>
		/// <param name="salaryTypeID">The primary key value</param>
		/// <returns>A SalaryTypeData object</returns>
		public static SalaryTypeData Load(int salaryTypeID)
		{
			SqlParameter[] parameterValues = new SqlParameter[] { new SqlParameter("@SalaryTypeID", salaryTypeID) };
			using (SqlDataReader reader = SqlHelper.ExecuteReader(CommonDAL.ConnectionString, CommandType.StoredProcedure, "SalaryTypesSelProc", parameterValues))
			{
				if (reader.Read())
				{
					SalaryTypeData result = new SalaryTypeData();
					LoadFromReader(reader,result);
					return result;
				}
				else
					return null;
			}
		}

		/// <summary>
		/// Loads a SalaryTypeData object from the database using the given where clause
		/// </summary>
		/// <param name="whereClause">The filter expression for the query</param>
		/// <returns>A SalaryTypeData object</returns>
		public static SalaryTypeData Load(string whereClause)
		{
			SqlParameter[] parameterValues = new SqlParameter[] { new SqlParameter("@WhereClause", whereClause) };
			using (SqlDataReader reader = SqlHelper.ExecuteReader(CommonDAL.ConnectionString, CommandType.StoredProcedure, "SalaryTypesListProc", parameterValues))
			{
				if (reader.Read())
				{
					SalaryTypeData result = new SalaryTypeData();
					LoadFromReader(reader,result);
					return result;
				}
				else
					return null;
			}
		}
		public static int Save( SalaryTypeData source)
		{
			if (source.SalaryTypeID == -1)
				return Insert(source);
			else
				return Update(source);	
		}

		public static void Delete( SalaryTypeData source)
		{
			Delete(source.SalaryTypeID);
		}
		/// <summary>
		/// Loads a collection of SalaryTypeData objects from the database.
		/// </summary>
		/// <returns>A collection containing all of the SalaryTypeData objects in the database.</returns>
		public static SalaryTypesVector LoadCollection()
		{
			SqlParameter[] parms = new SqlParameter[] {};
			return LoadCollection("SalaryTypesListProc", parms);
		}
	  
		/// <summary>
		/// Saves a collection of SalaryTypeData objects to the database.
		/// </summary>
		/// <returns>A collection containing all of the SalaryTypeData objects in the database.</returns>
		public static  void  SaveCollection(SalaryTypesVector col,string spName, SqlParameter[] parms)
		{
			SalaryTypesVector colOld = LoadCollection(spName,parms);
			UpdateCollection(colOld,col);
		}
		public static  void  SaveCollection(SalaryTypesVector col)
		{
			SalaryTypesVector colOld = LoadCollection();
			UpdateCollection(colOld,col);
		}
		public static  void  SaveCollection(SalaryTypesVector col, string whereClause )
		{
			SalaryTypesVector colOld = LoadCollection(whereClause);
			UpdateCollection(colOld,col);
		}
		/// <summary>
		/// Loads a collection of SalaryTypeData objects from the database.
		/// </summary>
		/// <returns>A collection containing all of the SalaryTypeData objects in the database.</returns>
		public static SalaryTypesVector LoadCollection(string whereClause)
		{
			SqlParameter[] parms = new SqlParameter[] { new SqlParameter("@WhereClause", whereClause) };
			return LoadCollection("SalaryTypesListProc", parms);
		}

		/// <summary>
		/// Loads a collection of SalaryTypeData objects from the database.
		/// </summary>
		/// <returns>A collection containing all of the SalaryTypeData objects in the database.</returns>
		public static SalaryTypesVector LoadCollection(string spName, SqlParameter[] parms)
		{
			SalaryTypesVector result = new SalaryTypesVector();
			using (SqlDataReader reader = SqlHelper.ExecuteReader(CommonDAL.ConnectionString, CommandType.StoredProcedure, spName, parms))
			{
				while (reader.Read())
				{
					SalaryTypeData tmp = new SalaryTypeData();
					LoadFromReader(reader, tmp);
					result.Add(tmp);
				}
			}
			return result;
		}
		/// <summary>
		/// Deletes a SalaryTypeData object from the database.
		/// </summary>
		/// <param name="salaryTypeID">The primary key value</param>
		public static void Delete(int salaryTypeID)
		{
			SqlParameter[] parameterValues = new SqlParameter[] { new SqlParameter("@SalaryTypeID", salaryTypeID) };
			SqlHelper.ExecuteNonQuery(CommonDAL.ConnectionString, CommandType.StoredProcedure, "SalaryTypesDelProc", parameterValues);
		}

		public static void LoadFromReader(SqlDataReader reader, SalaryTypeData target)
		{
			if (reader != null && !reader.IsClosed)
			{
				//target.Key = reader.GetInt32(0);
				if (!reader.IsDBNull(0)) target.SalaryTypeID = reader.GetInt32(0);
				if (!reader.IsDBNull(1)) target.SalaryType = reader.GetString(1);
			}
		}
		
	  
		public static int Insert( SalaryTypeData source)
		{
			SqlParameter[] parameterValues = GetInsertParameterValues(source);
			SqlHelper.ExecuteNonQuery(CommonDAL.ConnectionString, CommandType.StoredProcedure, "SalaryTypesInsProc", parameterValues);
			source.SalaryTypeID = (int) parameterValues[0].Value;
			return source.SalaryTypeID;
		}

		public static int Update( SalaryTypeData source)
		{
			SqlParameter[] parameterValues = GetUpdateParameterValues(source);
			SqlHelper.ExecuteNonQuery(CommonDAL.ConnectionString, CommandType.StoredProcedure,
				"SalaryTypesUpdProc", parameterValues);
			return source.SalaryTypeID;
		}
		private static void UpdateCollection(SalaryTypesVector colOld,SalaryTypesVector col)
		{
			// Delete all old items
			foreach( SalaryTypeData itemOld in colOld)
			{
				if(!ExistsKey(itemOld.SalaryTypeID, col))
					Delete(itemOld.SalaryTypeID);
			}
			foreach( SalaryTypeData item in col)
			{
				if(item.SalaryTypeID <= -1)
					Insert(item);
				else
					Update(item);
			}
		}
		private static bool ExistsKey(int Key, SalaryTypesVector col)
		{
			foreach( SalaryTypeData item in col)
				if(item.SalaryTypeID==Key)
					return true;
			return false;
		}

		private static SqlParameter GetSqlParameter(string name, ParameterDirection direction, object value)
		{
			SqlParameter p = new SqlParameter(name, value);
			p.Direction = direction;
			return p;
		}

		private static SqlParameter[] GetInsertParameterValues( SalaryTypeData source)
		{
			SqlParameter[] prams = new SqlParameter[2];
			prams[0] = GetSqlParameter("@SalaryTypeID", ParameterDirection.Output, source.SalaryTypeID);
			prams[1] = GetSqlParameter("@SalaryType", ParameterDirection.Input, source.SalaryType);
			return prams;
		}

		private static SqlParameter[] GetUpdateParameterValues( SalaryTypeData source)
		{
			return new SqlParameter[] {
										  new SqlParameter("@SalaryTypeID", source.SalaryTypeID),
										  new SqlParameter("@SalaryType", source.SalaryType)
									  };
		}
	}
}   

