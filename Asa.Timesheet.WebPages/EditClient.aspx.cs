using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Data.SqlClient;
using log4net;
using Asa.Timesheet.Data;
using Asa.Timesheet.Data.Entities;

namespace Asa.Timesheet.WebPages
{
	/// <summary>
	/// Summary description for EditClient.
	/// </summary>
	public class EditClient : TimesheetPageBase
	{
		#region WebControls

		protected System.Web.UI.WebControls.Label lblInfo;
		protected System.Web.UI.WebControls.Label lblName;
		protected System.Web.UI.WebControls.Label lblCity;
		protected System.Web.UI.WebControls.TextBox txtName;
		protected System.Web.UI.WebControls.TextBox txtCity;
		protected System.Web.UI.WebControls.TextBox txtAddress;
		protected System.Web.UI.WebControls.Button btnDelete;
		protected System.Web.UI.WebControls.Button btnSave;
		protected System.Web.UI.WebControls.PlaceHolder menuHolder;
		protected System.Web.UI.WebControls.Label lblAddress;
		protected System.Web.UI.WebControls.Button btnCancel;
		protected System.Web.UI.WebControls.Label lblError;
		protected Asa.Timesheet.WebPages.UserControls.PageHeader header;
		protected System.Web.UI.WebControls.Label lblEmail;
		protected System.Web.UI.WebControls.TextBox txtEmail;
		protected System.Web.UI.WebControls.Label Label1;
		protected System.Web.UI.WebControls.Label Label2;
		protected System.Web.UI.WebControls.Label Label3;
		protected System.Web.UI.WebControls.Label Label4;
		protected System.Web.UI.WebControls.TextBox txtFax;
		protected System.Web.UI.WebControls.TextBox txtManager;
		protected System.Web.UI.WebControls.TextBox txtRepr1;
		protected System.Web.UI.WebControls.TextBox txtRepr2;
		protected System.Web.UI.WebControls.Label Label5;
		protected System.Web.UI.WebControls.TextBox TextBox1;
		protected System.Web.UI.WebControls.Label Label6;
		protected System.Web.UI.WebControls.TextBox TextBox2;
		protected System.Web.UI.WebControls.Label Label7;
		protected System.Web.UI.WebControls.TextBox TextBox3;
		protected System.Web.UI.WebControls.Label Label8;
		protected System.Web.UI.WebControls.TextBox Textbox4;
		protected System.Web.UI.WebControls.Label Label9;
		protected System.Web.UI.WebControls.Label Label10;
		protected System.Web.UI.WebControls.TextBox Textbox5;
		protected System.Web.UI.WebControls.Label Label11;
		protected System.Web.UI.WebControls.TextBox Textbox6;
		protected System.Web.UI.WebControls.TextBox txtDelo;
		protected System.Web.UI.WebControls.TextBox txtBulstat;
		protected System.Web.UI.WebControls.TextBox txtNDR;
		protected System.Web.UI.WebControls.TextBox txtPhone;
		protected System.Web.UI.WebControls.TextBox txtPhone1;
		protected System.Web.UI.WebControls.TextBox txtEmail1;
		protected System.Web.UI.WebControls.Label Label12;
		protected System.Web.UI.WebControls.Label Label13;
		protected System.Web.UI.WebControls.TextBox txtPhone2;
		protected System.Web.UI.WebControls.Label Label14;
		protected System.Web.UI.WebControls.TextBox txtEmail2;
		protected System.Web.UI.WebControls.Label Label15;
		protected System.Web.UI.WebControls.TextBox txtWebsite;
    protected System.Web.UI.WebControls.Button btnEdit;
    protected Asa.Timesheet.WebPages.UserControls.EditForm editCtrl;

    #endregion
		
		private static readonly ILog log = LogManager.GetLogger(typeof(EditClient));

		private void Page_Load(object sender, System.EventArgs e)
		{
			//if (! (LoggedUser.IsLeader || LoggedUser.IsAssistant)) ErrorRedirect(Resource.ResourceManager["noRightsAlert"]);
			if(Page.IsPostBack == false)
			{
				// Set up form for data change checking when
				// first loaded.
				this.CheckForDataChanges= true;
			  this.BypassPromptIds =
				 new string[] { "btnSave", "btnDelete",
                           "btnEdit" };
			}
			UIHelpers.CreateMenu(menuHolder,LoggedUser);

			if (!this.IsPostBack)
			{
				header.UserName = LoggedUser.FullName;

				int clientID = this.ClientID;
				if (clientID>0)
				{
					header.PageTitle = Resource.ResourceManager["editClient_EditLabel"];
					if (!LoadClient(clientID)) ErrorRedirect(Resource.ResourceManager["editClient_ErrorLoadClient"]);
					
					SetConfirmDelete(btnDelete, txtName.Text);
				}
				else
				{
					header.PageTitle = Resource.ResourceManager["editClient_NewLabel"];
					btnDelete.Visible = false;
				}
			}

      InitEdit();
			if(!(LoggedUser.IsAssistantOnly || LoggedUser.HasPaymentRights ||LoggedUser.IsSecretary))
			{
				btnEdit.Visible=false;
				if(ClientID<=0)
					ErrorRedirect(Resource.ResourceManager["noRightsAlert"]);
			}

		}

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
			this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
			this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
			this.btnDelete.Click += new System.EventHandler(this.btnDelete_Click);
			this.Load += new System.EventHandler(this.Page_Load);

		}
		#endregion

		#region Event handlers 

		private void btnCancel_Click(object sender, System.EventArgs e)
		{
			Response.Redirect("Clients.aspx");
		}

		private void btnSave_Click(object sender, System.EventArgs e)
		{
			string clientName = txtName.Text.Trim();
			if (clientName == String.Empty) 
			{
				AlertFieldNotEntered(lblName);
				return;	
			}

			string city = txtCity.Text.Trim();
			string address = txtAddress.Text.Trim();
			string email = txtEmail.Text.Trim();

			int clientID = this.ClientID;
			if (clientID>0) 
			{
				if (!UpdateClient(clientID, clientName, city, address, email, txtFax.Text, txtManager.Text, txtRepr1.Text, txtRepr2.Text
					,txtDelo.Text,txtBulstat.Text,txtNDR.Text, txtPhone.Text, txtWebsite.Text, txtPhone1.Text, txtEmail1.Text, txtPhone2.Text, txtEmail2.Text))
				{
					lblError.Text = Resource.ResourceManager["editClient_ErrorSave"];
					return;
				}
			}
			else
			{
				if (!InsertClient(clientName, city, address, email, txtFax.Text, txtManager.Text, txtRepr1.Text, txtRepr2.Text
					,txtDelo.Text,txtBulstat.Text,txtNDR.Text, txtPhone.Text, txtWebsite.Text, txtPhone1.Text, txtEmail1.Text, txtPhone2.Text, txtEmail2.Text))
				{
					lblError.Text = Resource.ResourceManager["editClient_ErrorSave"];
					return;
				}
			}
			Response.Redirect("Clients.aspx");
		}

		private void btnDelete_Click(object sender, System.EventArgs e)
		{
			int clientID = this.ClientID;

			if (!DeleteClient(clientID))
			{
				lblError.Text = Resource.ResourceManager["editClient_ErrorDelete"];
				return;
			}

			Response.Redirect("Clients.aspx");
		}

		#endregion

		#region Database

		private bool LoadClient(int clientID)
		{
			SqlDataReader reader = null;
			try
			{
				reader = ClientsData.SelectClient(clientID);
				if (reader.Read())
				{
					txtName.Text = reader.GetString(1);
					txtCity.Text = reader.IsDBNull(2) ? String.Empty : reader.GetString(2);
					txtAddress.Text = reader.IsDBNull(3) ? String.Empty : reader.GetString(3);
					txtEmail.Text = reader.IsDBNull(4) ? String.Empty : reader.GetString(4);
					txtFax.Text = reader.IsDBNull(6) ? String.Empty : reader.GetString(6);
					txtManager.Text = reader.IsDBNull(7) ? String.Empty : reader.GetString(7);
					txtRepr1.Text = reader.IsDBNull(8) ? String.Empty : reader.GetString(8);
					txtRepr2.Text = reader.IsDBNull(9) ? String.Empty : reader.GetString(9);
					txtDelo.Text = reader.IsDBNull(10) ? String.Empty : reader.GetString(10);
					txtBulstat.Text = reader.IsDBNull(11) ? String.Empty : reader.GetString(11);
					txtNDR.Text = reader.IsDBNull(12) ? String.Empty : reader.GetString(12);
					txtPhone.Text = reader.IsDBNull(13) ? String.Empty : reader.GetString(13);
					txtWebsite.Text = reader.IsDBNull(14) ? String.Empty : reader.GetString(14);
					txtPhone1.Text = reader.IsDBNull(15) ? String.Empty : reader.GetString(15);
					txtEmail1.Text = reader.IsDBNull(16) ? String.Empty : reader.GetString(16);
					txtPhone2.Text = reader.IsDBNull(17) ? String.Empty : reader.GetString(17);
					txtEmail2.Text = reader.IsDBNull(18) ? String.Empty : reader.GetString(18);
				}
				else return false;
			}
			catch (Exception ex)
			{
				log.Error(ex);
				return false;
			}
			finally
			{
				if (reader!=null) reader.Close();
			}

			return true;
		}

		private bool DeleteClient(int clientID)
		{
			try
			{
				ClientsData.InactiveClient(clientID);
			}
			catch (Exception ex)
			{
				log.Error(ex);
				return false;
			}
			
			return true;
		}

		private bool UpdateClient(int clientID, string clientName, string city, string address, string email,string fax,string manager, string repr1, string repr2,string delo, string bulstat, string ndr, string phone, string webpage, string phone1, string email1,
			string phone2, string email2)
		{
			try
			{
				ClientsData.UpdateClient(clientID, clientName, city, address, email, fax, manager,  repr1,  repr2, delo,  bulstat,  ndr,  phone,  webpage,  phone1,  email1,
			 phone2,  email2);
			}
			catch (Exception ex)
			{
				log.Error(ex);
				return false;
			}

			return true;
		}

		private bool InsertClient(string clientName, string city, string address, string email,string fax,string manager, string repr1, string repr2,string delo, string bulstat, string ndr, string phone, string webpage, string phone1, string email1,
			string phone2, string email2)
		{
			try
			{
				ClientsData.InsertClient(clientName, city, address, email, true, fax, manager,  repr1,  repr2, delo,  bulstat,  ndr,  phone,  webpage,  phone1,  email1,
			 phone2,  email2);
			}
			catch (Exception ex)
			{
				log.Error(ex);
				return false;
			}

			return true;
		}

		#endregion

		#region private properties 
		new private int ClientID
		{
			get 
			{
				try
				{
					return int.Parse(Request.Params["cid"]);
				}
				catch
				{
					return -1;
				}
			}		
		}
		#endregion

    #region UI

    private void InitEdit()
    {
      if (this.ClientID <=0) return;

      string cont = "tblForm";
      string vmButtons = this.btnEdit.ClientID;
      string emButtons = this.btnSave.ClientID+";"+this.btnDelete.ClientID;

      string exl = String.Empty;

      editCtrl.InitEdit(cont, vmButtons, emButtons, exl);
    }

    #endregion

		#region Menu

//		private void CreateMenu()
//		{
//			UserControls.MenuTable menu = new UserControls.MenuTable();
//			menu.ID = "MenuTable";
//
//			ArrayList menuItems = new ArrayList();
//
//			menuItems.Add(new MenuItemInfo(Resource.ResourceManager["menuItem_Hours"], "Hours.aspx", true));
//			menuItems.Add(new MenuItemInfo(Resource.ResourceManager["menuItem_Projects"], "Projects.aspx"));
//			menuItems.Add(new MenuItemInfo(Resource.ResourceManager["menuItem_Clients"], "Clients.aspx"));
//			menuItems.Add(new MenuItemInfo(Resource.ResourceManager["menuItem_Subcontracters"], "Subcontracters.aspx"));
//
//			if (LoggedUser.IsLeader)
//			menuItems.Add(new MenuItemInfo(Resource.ResourceManager["menuItem_Users"], "Users.aspx"));
//			menuItems.Add(new MenuItemInfo(Resource.ResourceManager["menuItem_Emails"], "Emails.aspx"));
//			
//			menu.AddMenuGroup(Resource.ResourceManager["menuHeader_Links"], 20, menuItems);
//			
//			menuItems = new ArrayList();
//			menuItems.Add(new MenuItemInfo(Resource.ResourceManager["menuItem_NewProject"], "EditProject.aspx"));
//			if (LoggedUser.IsLeader)
//			menuItems.Add(new MenuItemInfo(Resource.ResourceManager["menuItem_NewUser"], "EditUser.aspx"));
//			menuItems.Add(new MenuItemInfo(Resource.ResourceManager["menuItem_NewEmail"], "EditEmail.aspx"));
//			menuItems.Add(new MenuItemInfo(Resource.ResourceManager["newcontracter_PageTitle"], "EditSubContracter.aspx"));
//
//			menu.AddMenuGroup(Resource.ResourceManager["menuHeader_New"], 30, menuItems);
//
//			menuItems = new ArrayList();
//			menuItems.Add(new MenuItemInfo(Resource.ResourceManager["menuItem_WorkTimeReport"], "reports/worktimes.aspx"));
//			menuItems.Add(new MenuItemInfo(Resource.ResourceManager["menuItem_MinutesReport"], "Reports/Minutes.aspx"));
//			if (LoggedUser.IsLeader || LoggedUser.IsAssistant)
//				menuItems.Add(new MenuItemInfo(Resource.ResourceManager["menuItem_MailReport"], "MailForm.aspx"));
//			if (LoggedUser.IsLeader)
//			{
//				menuItems.Add(new MenuItemInfo(Resource.ResourceManager["menuItem_NotEnteredWorkTime"], "reports/EmptyHours.aspx"));
//				menuItems.Add(new MenuItemInfo(Resource.ResourceManager["menuItem_Calendar"], "reports/Calendar.aspx"));
//			}
//
//			menu.AddMenuGroup(Resource.ResourceManager["menuHeader_Reports"], 30, menuItems);
//
//			menuItems = new ArrayList();
//			menuItems.Add(new MenuItemInfo(Resource.ResourceManager["menuItem_Help"], SessionManager.GetHelpLink(),true,Pages.Clients));
//			menu.AddMenuGroup("", 10, menuItems);
//
//			menuHolder.Controls.Add(menu);
//		}

		#endregion
	}
}
