using System;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using Asa.Timesheet.Data.Entities;

using Microsoft.ApplicationBlocks.Data;

namespace Asa.Timesheet.Data
{
	/// <summary>
	/// Summary description for DBManager.
	/// </summary>
	public class DBManager
	{

		internal static string GetConnectionString()
		{
			return ConfigurationSettings.AppSettings["ConnectionString"];
		}


		public static SqlDataReader SelectActivities()
		{
			string spName = "ActivitiesListProc";
			return SqlHelper.ExecuteReader(GetConnectionString(), CommandType.StoredProcedure, spName);
		}

		public static SqlDataReader SelectActivity(int activityID)
		{
			string spName = "ActivitiesSelProc";

			SqlParameter[] storedParams = SqlHelperParameterCache.GetSpParameterSet(GetConnectionString(), spName);
			storedParams[0].Value = activityID;

			return SqlHelper.ExecuteReader(GetConnectionString(), CommandType.StoredProcedure, spName, storedParams);
		}

		public static string SelectActivityName(int activityID)
		{
			string spName = "ActivitiesSelectName";

			SqlParameter[] storedParams = SqlHelperParameterCache.GetSpParameterSet(GetConnectionString(), spName);
			storedParams[0].Value = activityID;

			object o = SqlHelper.ExecuteScalar(GetConnectionString(), CommandType.StoredProcedure, spName, storedParams);
			
			return (o==null) ? String.Empty : (string)o;
		}

		public static SqlDataReader SelectTimes(DateTime dt)
		{
			string spName = "TimesListWithEndProc";
			int TimeNow =0;
			if(dt==DateTime.Today)
				TimeNow = DateTime.Now.Hour*2 + 3;
			if(dt<DateTime.Today || SessionManager.LoggedUserInfo.IsAccountant|| SessionManager.LoggedUserInfo.IsLeader)
				TimeNow = 50;
			SqlParameter[] storedParams = SqlHelperParameterCache.GetSpParameterSet(GetConnectionString(), spName);
			storedParams[0].Value = TimeNow;

			return SqlHelper.ExecuteReader(GetConnectionString(), CommandType.StoredProcedure, spName, storedParams);
		}
		public static SqlDataReader SelectTimes()
		{
			string spName = "TimesListProc";
			
			
			return SqlHelper.ExecuteReader(GetConnectionString(), CommandType.StoredProcedure, spName);
		}
	
		public static SqlDataReader SelectBuildingTypes()
		{
			string spName = "BuildingTypesListProc";
			

			return SqlHelper.ExecuteReader(GetConnectionString(), CommandType.StoredProcedure, spName);

		}
		public static SqlDataReader SelectActiveBuildingTypes()
		{
			string spName = "BuildingTypesListActiveProc";
			

			return SqlHelper.ExecuteReader(GetConnectionString(), CommandType.StoredProcedure, spName);

		}
		public static SqlDataReader SelectHotIssueCategories(int projectID)
		{
			string spName = "HotIssueCategoryListProc";
			SqlParameter[] storedParams = SqlHelperParameterCache.GetSpParameterSet(GetConnectionString(), spName);
			if(projectID !=-1)
				storedParams[0].Value = projectID;

			return SqlHelper.ExecuteReader(GetConnectionString(), CommandType.StoredProcedure, spName,storedParams);

		}

		public static SqlDataReader SelectHotIssueCategories(int projectID,int buildingType,int projectStatus)
		{
			string spName = "HotIssueCategoryListProc";
			SqlParameter[] storedParams = SqlHelperParameterCache.GetSpParameterSet(GetConnectionString(), spName);
			if(projectID >0)
				storedParams[0].Value = projectID;
			if(buildingType >0)
				storedParams[1].Value = buildingType;
			switch(projectStatus)
			{
				case (int)ProjectsData.ProjectsByStatus.Active:{storedParams[2].Value=true;storedParams[3].Value=false;break;}
				case (int)ProjectsData.ProjectsByStatus.Passive:{storedParams[2].Value=false;storedParams[3].Value=false;break;}
				case (int)ProjectsData.ProjectsByStatus.Conluded:{storedParams[3].Value=true;break;}
			}

			return SqlHelper.ExecuteReader(GetConnectionString(), CommandType.StoredProcedure, spName,storedParams);

		}

		public static SqlDataReader SelectHotIssuePriorities()
		{
			string spName = "HotIssuePriorityListProc";
			

			return SqlHelper.ExecuteReader(GetConnectionString(), CommandType.StoredProcedure, spName);

		}

		public static SqlDataReader SelectHotIssueStatuses()
		{
			string spName = "HotIssueStatusListProc";
			

			return SqlHelper.ExecuteReader(GetConnectionString(), CommandType.StoredProcedure, spName);

		}
		
		public static SqlDataReader SelectRoles()
		{
			string spName = "RolesListProc";

			return SqlHelper.ExecuteReader(GetConnectionString(), CommandType.StoredProcedure, spName);
		}

		public static SqlDataReader SelectRolesByRights(int userID)
		{
			string spName = "RolesListByRightsProc";

			SqlParameter[] storedParams = SqlHelperParameterCache.GetSpParameterSet(GetConnectionString(), spName);
			storedParams[0].Value = userID;

			return SqlHelper.ExecuteReader(GetConnectionString(), CommandType.StoredProcedure, spName, storedParams);
		}

		public static SqlDataReader SelectMinutesReport()
		{
			string spName = "selproc";

			return SqlHelper.ExecuteReader(DBManager.GetConnectionString(), CommandType.StoredProcedure,
				spName);
		}

		public static DataSet SelectMinutesReportDS()
		{
			string spName = "selproc";

			return SqlHelper.ExecuteDataset(DBManager.GetConnectionString(), CommandType.StoredProcedure,
				spName);
		}

		public static string SelectUserAccount(string loginName, string password) 
		{
			string spName = "UsersAccountSelProc";

			SqlParameter[] storedParams = SqlHelperParameterCache.GetSpParameterSet(GetConnectionString(), spName);
			storedParams[0].Value = loginName;
			storedParams[1].Value = password;

			return (string)SqlHelper.ExecuteScalar(DBManager.GetConnectionString(), CommandType.StoredProcedure, spName, storedParams);
		}

		//notes:here selectAccount for PM system
		public static string SelectUserPMAccount(string loginName, string password) 
		{
			string returnValue;
			//notes: first check in users table
			string spName = "UsersAccountSelProc";

			SqlParameter[] storedParams = SqlHelperParameterCache.GetSpParameterSet(GetConnectionString(), spName);
			storedParams[0].Value = loginName;
			storedParams[1].Value = password;

			returnValue = (string)SqlHelper.ExecuteScalar(DBManager.GetConnectionString(), CommandType.StoredProcedure, spName, storedParams);
			if(returnValue!= null)
				return returnValue;
			//notes: second check in Subcontractors table
			spName = "UsersAccountSelInSubcontractorsProc";
			storedParams = SqlHelperParameterCache.GetSpParameterSet(GetConnectionString(), spName);
			storedParams[0].Value = loginName;
			storedParams[1].Value = password;
			returnValue = (string)SqlHelper.ExecuteScalar(DBManager.GetConnectionString(), CommandType.StoredProcedure, spName, storedParams);
			if(returnValue!= null)
				return returnValue;
			//notes: finnaly check in Clients table
			spName = "UsersAccountSelInClientsProc";
			storedParams = SqlHelperParameterCache.GetSpParameterSet(GetConnectionString(), spName);
			storedParams[0].Value = loginName;
			storedParams[1].Value = password;
			returnValue = (string)SqlHelper.ExecuteScalar(DBManager.GetConnectionString(), CommandType.StoredProcedure, spName, storedParams);
			//if(returnValue!= null)
			//	return returnValue;

			return returnValue;
		}

	}
}
