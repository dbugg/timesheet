using System;
using System.Data;
using Asa.Timesheet.Data.Reports;
using Asa.Timesheet.Data;
using GrapeCity.ActiveReports;
using GrapeCity.ActiveReports.Controls;
using GrapeCity.ActiveReports.SectionReportModel;
using GrapeCity.ActiveReports.Document.Section;
using GrapeCity.ActiveReports.Document;

namespace Asa.Timesheet.WebPages.Reports
{
    public class SubcontractersSubreport : GrapeCity.ActiveReports.SectionReport
	{
		int _projectID;
		string _projectName = String.Empty;
		int _startPageNumber;
    bool _fullReport;
bool _hpr;


public SubcontractersSubreport(int projectID, bool fullReport, int startPageNumber, bool hpr)
{
    InitializeComponent();
    _projectID = projectID;
    _startPageNumber = startPageNumber;
    _hpr = hpr;
    _fullReport = fullReport;
}

private void Detail_Format(object sender, System.EventArgs eArgs)
{
    if (((DataTable)this.DataSource).Rows.Count == 0) return;

    this.txtTitle.Text =
        String.Concat(this.Fields["SubcontracterName"].Value.ToString(),
        " - ",
        this.Fields["SubcontracterType"].Value.ToString());

    DataTable tblActivities = ((DataTable)this.DataSource).DataSet.Tables[1];
    DataView dv = new DataView(tblActivities);

    string s = this.Fields["ProjectSubcontracterID"].Value.ToString();
    dv.RowFilter = "ProjectSubcontracterID = " + this.Fields["ProjectSubcontracterID"].Value.ToString();

    this.SubReport1.Report = new SubactivitiesSubreport();
    this.SubReport1.Report.DataSource = dv;

    #region format Area

    //      try
    //      {
    //        txtArea.Text = UIHelpers.FormatDecimal2((decimal)this.Fields["Area"].Value);
    //      }
    //      catch
    //      {
    //        txtArea.Text = String.Empty;
    //      }

    #endregion

    if (!this._fullReport) return;

    #region format Rate

    try
    {
        txtRate.Text = UIHelpers.FormatDecimal2((decimal)this.Fields["Rate"].Value);
    }
    catch
    {
        txtRate.Text = String.Empty;
    }


    #endregion

    #region Calculate Amount

    decimal totalAmountEUR = 0;

    decimal fix = decimal.Parse(System.Configuration.ConfigurationManager.AppSettings["fixedBGNEUR"]);

    decimal rate = 0;
    try
    {
        rate = (decimal)this.Fields["Rate"].Value;
    }
    catch
    { }

    decimal area = 0;
    try
    {
        area = (decimal)this.Fields["Area"].Value;
    }
    catch
    { }

    int paymentSchemeID = (int)PaymentSchemes.Area;
    try
    {
        paymentSchemeID = (int)this.Fields["PaymentSchemeID"].Value;
    }
    catch
    { }

    if (paymentSchemeID == (int)PaymentSchemes.Fixed) totalAmountEUR = rate;
    else totalAmountEUR = area * rate;

    this.txtAmountEUR.Text = UIHelpers.FormatDecimal2(totalAmountEUR);
    this.txtAmountBGN.Text = UIHelpers.FormatDecimal2(totalAmountEUR * fix);

    #endregion
}

private void SubcontractersSubreport_ReportStart(object sender, System.EventArgs eArgs)
{
    DataTable tblHeader = ProjectsReportData.GetProjectHeaderInfo(this._projectID);
    _projectName = tblHeader.Rows[0]["ProjectName"].ToString();

    if (!this._fullReport)
    {
        this.txtPSLbl.Visible = false;
        this.txtPaymentScheme.Visible = false;
        this.txtPriceLbl.Visible = false;
        this.txtRate.Visible = false;
        this.txtEuroLbl.Visible = false;
        this.txtAmountEUR.Visible = false;
        this.txtBGNLbl.Visible = false;
        this.txtAmountBGN.Visible = false;

        this.txtFGLbl.Location = new System.Drawing.PointF(txtFGLbl.Location.X, txtPaymentScheme.Location.Y);
        this.txtFALbl.Location = new System.Drawing.PointF(txtFALbl.Location.X, txtPaymentScheme.Location.Y);
        this.txtFoldersGiven.Location = new System.Drawing.PointF(txtFoldersGiven.Location.X, txtPaymentScheme.Location.Y);
        this.txtFoldersArchive.Location = new System.Drawing.PointF(txtFoldersArchive.Location.X, txtPaymentScheme.Location.Y);

        this.txtSep.Location = new System.Drawing.PointF(0, txtFoldersGiven.Location.Y + CmToInch(0.7f));
    }

    this.DataSource = ProjectsReportData.GetSubcontracters(_projectID, _hpr).Tables[0];
}

private void ReportHeader_Format(object sender, System.EventArgs eArgs)
{
    this.txtProjectNameHeader.Text = _projectName;
    txtReportTitle.Text = Resource.ResourceManager["Reports_strProjectSubcontractors"];
}

private void PageHeader_Format(object sender, System.EventArgs eArgs)
{
    if (this.PageNumber == 1) this.PageHeader.Visible = false;
    else
    {
        this.PageHeader.Visible = true;
        txtPageHeader.Text = String.Concat(_projectName, " - ",
            Resource.ResourceManager["Reports_strProjectSubcontractors"]);
    }
}

		public bool HasData
		{
			get { return ((DataTable)this.DataSource).Rows.Count > 0; }
		}

        private void PageFooter_Format(object sender, System.EventArgs eArgs)
        {
            lblPageFooter.Text = _projectName;
            lblPage.Text = string.Concat(Resource.ResourceManager["rpt_Page"], " ",
                (this.PageNumber + _startPageNumber - 1).ToString());
        }

		#region ActiveReports Designer generated code







































        public void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(SubcontractersSubreport));
            this.Detail = new GrapeCity.ActiveReports.SectionReportModel.Detail();
            this.ReportHeader = new GrapeCity.ActiveReports.SectionReportModel.ReportHeader();
            this.ReportFooter = new GrapeCity.ActiveReports.SectionReportModel.ReportFooter();
            this.PageHeader = new GrapeCity.ActiveReports.SectionReportModel.PageHeader();
            this.PageFooter = new GrapeCity.ActiveReports.SectionReportModel.PageFooter();
            this.txtReportTitle = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.Line3 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.txtProjectNameHeader = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.Line2 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.txtPageHeader = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.TextBox1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtSubprojectType = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtPaymentScheme = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtFoldersGiven = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtFALbl = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtFoldersArchive = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtFGLbl = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtPSLbl = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.Line1 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.txtTitle = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.TextBox11 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtStartDate = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.TextBox12 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtEndDate = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtPriceLbl = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtRate = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtEuroLbl = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtAmountEUR = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtBGNLbl = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtAmountBGN = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.SubReport1 = new GrapeCity.ActiveReports.SectionReportModel.SubReport();
            this.TextBox19 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.TextBox20 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.TextBox21 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtSep = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.Line11 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.lblPageFooter = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lblPage = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Line4 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            ((System.ComponentModel.ISupportInitialize)(this.txtReportTitle)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtProjectNameHeader)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPageHeader)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TextBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSubprojectType)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPaymentScheme)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtFoldersGiven)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtFALbl)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtFoldersArchive)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtFGLbl)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPSLbl)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTitle)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TextBox11)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtStartDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TextBox12)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtEndDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPriceLbl)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtRate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtEuroLbl)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAmountEUR)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtBGNLbl)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAmountBGN)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TextBox19)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TextBox20)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TextBox21)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSep)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblPageFooter)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblPage)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            // 
            // Detail
            // 
            this.Detail.CanShrink = true;
            this.Detail.ColumnSpacing = 0F;
            this.Detail.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
						this.TextBox1,
						this.txtSubprojectType,
						this.txtPaymentScheme,
						this.txtFoldersGiven,
						this.txtFALbl,
						this.txtFoldersArchive,
						this.txtFGLbl,
						this.txtPSLbl,
						this.Line1,
						this.txtTitle,
						this.TextBox11,
						this.txtStartDate,
						this.TextBox12,
						this.txtEndDate,
						this.txtPriceLbl,
						this.txtRate,
						this.txtEuroLbl,
						this.txtAmountEUR,
						this.txtBGNLbl,
						this.txtAmountBGN,
						this.SubReport1,
						this.TextBox19,
						this.TextBox20,
						this.TextBox21,
						this.txtSep});
            this.Detail.Height = 2.395139F;
            this.Detail.KeepTogether = true;
            this.Detail.Name = "Detail";
            // 
            // ReportHeader
            // 
            this.ReportHeader.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
						this.txtReportTitle,
						this.Line3,
						this.txtProjectNameHeader});
            this.ReportHeader.Height = 0.7868056F;
            this.ReportHeader.Name = "ReportHeader";
            // 
            // ReportFooter
            // 
            this.ReportFooter.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
						this.Line4});
            this.ReportFooter.Height = 0.09375F;
            this.ReportFooter.Name = "ReportFooter";
            // 
            // PageHeader
            // 
            this.PageHeader.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
						this.Line2,
						this.txtPageHeader});
            this.PageHeader.Height = 0.5902778F;
            this.PageHeader.Name = "PageHeader";
            // 
            // PageFooter
            // 
            this.PageFooter.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
						this.Line11,
						this.lblPageFooter,
						this.lblPage});
            this.PageFooter.Height = 0.25F;
            this.PageFooter.Name = "PageFooter";
            // 
            // txtReportTitle
            // 
            this.txtReportTitle.Height = 0.2755906F;
            this.txtReportTitle.Left = 0F;
            this.txtReportTitle.Name = "txtReportTitle";
            this.txtReportTitle.Style = "font-size: 14pt; font-weight: bold";
            this.txtReportTitle.Text = "??????????????";
            this.txtReportTitle.Top = 0F;
            this.txtReportTitle.Width = 7.086611F;
            // 
            // Line3
            // 
            this.Line3.Height = 0F;
            this.Line3.Left = 0F;
            this.Line3.LineWeight = 2F;
            this.Line3.Name = "Line3";
            this.Line3.Top = 0.551181F;
            this.Line3.Width = 7.086611F;
            this.Line3.X1 = 0F;
            this.Line3.X2 = 7.086611F;
            this.Line3.Y1 = 0.551181F;
            this.Line3.Y2 = 0.551181F;
            // 
            // txtProjectNameHeader
            // 
            this.txtProjectNameHeader.Height = 0.2362205F;
            this.txtProjectNameHeader.Left = 0F;
            this.txtProjectNameHeader.Name = "txtProjectNameHeader";
            this.txtProjectNameHeader.Style = "font-size: 12pt; font-weight: bold";
            this.txtProjectNameHeader.Text = "txtProjectNameHeader";
            this.txtProjectNameHeader.Top = 0.2755906F;
            this.txtProjectNameHeader.Width = 7.086611F;
            // 
            // Line2
            // 
            this.Line2.Height = 0F;
            this.Line2.Left = 0F;
            this.Line2.LineWeight = 2F;
            this.Line2.Name = "Line2";
            this.Line2.Top = 0.243493F;
            this.Line2.Width = 7.086611F;
            this.Line2.X1 = 0F;
            this.Line2.X2 = 7.086611F;
            this.Line2.Y1 = 0.243493F;
            this.Line2.Y2 = 0.243493F;
            // 
            // txtPageHeader
            // 
            this.txtPageHeader.Height = 0.2362205F;
            this.txtPageHeader.Left = 0F;
            this.txtPageHeader.Name = "txtPageHeader";
            this.txtPageHeader.Style = "font-size: 11pt; font-weight: bold";
            this.txtPageHeader.Text = "txtPageHeader";
            this.txtPageHeader.Top = 0F;
            this.txtPageHeader.Width = 7.086611F;
            // 
            // TextBox1
            // 
            this.TextBox1.Height = 0.2F;
            this.TextBox1.Left = 0F;
            this.TextBox1.Name = "TextBox1";
            this.TextBox1.Text = "???? ?? ??????:";
            this.TextBox1.Top = 0.3149606F;
            this.TextBox1.Width = 1.377953F;
            // 
            // txtSubprojectType
            // 
            this.txtSubprojectType.DataField = "SubprojectType";
            this.txtSubprojectType.Height = 0.2F;
            this.txtSubprojectType.Left = 1.377953F;
            this.txtSubprojectType.Name = "txtSubprojectType";
            this.txtSubprojectType.Style = "font-weight: bold";
            this.txtSubprojectType.Text = "txtSubprojectType";
            this.txtSubprojectType.Top = 0.3149606F;
            this.txtSubprojectType.Width = 5.708662F;
            // 
            // txtPaymentScheme
            // 
            this.txtPaymentScheme.DataField = "PaymentScheme";
            this.txtPaymentScheme.Height = 0.2F;
            this.txtPaymentScheme.Left = 1.377953F;
            this.txtPaymentScheme.Name = "txtPaymentScheme";
            this.txtPaymentScheme.Style = "font-weight: bold";
            this.txtPaymentScheme.Text = "txtPaymentScheme";
            this.txtPaymentScheme.Top = 1.380413F;
            this.txtPaymentScheme.Width = 2.952756F;
            // 
            // txtFoldersGiven
            // 
            this.txtFoldersGiven.DataField = "FoldersGiven";
            this.txtFoldersGiven.Height = 0.2F;
            this.txtFoldersGiven.Left = 1.377953F;
            this.txtFoldersGiven.Name = "txtFoldersGiven";
            this.txtFoldersGiven.Style = "font-weight: bold";
            this.txtFoldersGiven.Text = "TextBox4";
            this.txtFoldersGiven.Top = 1.931594F;
            this.txtFoldersGiven.Width = 1.181102F;
            // 
            // txtFALbl
            // 
            this.txtFALbl.Height = 0.2F;
            this.txtFALbl.Left = 2.559055F;
            this.txtFALbl.Name = "txtFALbl";
            this.txtFALbl.Text = "????? ? ??????:";
            this.txtFALbl.Top = 1.931594F;
            this.txtFALbl.Width = 1.377953F;
            // 
            // txtFoldersArchive
            // 
            this.txtFoldersArchive.DataField = "FoldersArchive";
            this.txtFoldersArchive.Height = 0.2F;
            this.txtFoldersArchive.Left = 3.937008F;
            this.txtFoldersArchive.Name = "txtFoldersArchive";
            this.txtFoldersArchive.Style = "font-weight: bold";
            this.txtFoldersArchive.Text = "TextBox6";
            this.txtFoldersArchive.Top = 1.931594F;
            this.txtFoldersArchive.Width = 1.181102F;
            // 
            // txtFGLbl
            // 
            this.txtFGLbl.Height = 0.2F;
            this.txtFGLbl.Left = 0F;
            this.txtFGLbl.Name = "txtFGLbl";
            this.txtFGLbl.Text = "?????? ?????:";
            this.txtFGLbl.Top = 1.931594F;
            this.txtFGLbl.Width = 1.377953F;
            // 
            // txtPSLbl
            // 
            this.txtPSLbl.Height = 0.2F;
            this.txtPSLbl.Left = 0F;
            this.txtPSLbl.Name = "txtPSLbl";
            this.txtPSLbl.Text = "????? ?? ???????:";
            this.txtPSLbl.Top = 1.380413F;
            this.txtPSLbl.Width = 1.377953F;
            // 
            // Line1
            // 
            this.Line1.Height = 0F;
            this.Line1.Left = 0F;
            this.Line1.LineWeight = 1F;
            this.Line1.Name = "Line1";
            this.Line1.Top = 0.2637796F;
            this.Line1.Width = 7.086611F;
            this.Line1.X1 = 0F;
            this.Line1.X2 = 7.086611F;
            this.Line1.Y1 = 0.2637796F;
            this.Line1.Y2 = 0.2637796F;
            // 
            // txtTitle
            // 
            this.txtTitle.DataField = "SubprojectType";
            this.txtTitle.Height = 0.2165354F;
            this.txtTitle.Left = 0F;
            this.txtTitle.Name = "txtTitle";
            this.txtTitle.Style = "font-size: 12pt; font-weight: bold";
            this.txtTitle.Text = "txtTitle";
            this.txtTitle.Top = 0F;
            this.txtTitle.Width = 7.086611F;
            // 
            // TextBox11
            // 
            this.TextBox11.Height = 0.2F;
            this.TextBox11.Left = 0F;
            this.TextBox11.Name = "TextBox11";
            this.TextBox11.Text = "??????? ????:";
            this.TextBox11.Top = 0.5905511F;
            this.TextBox11.Width = 1.377953F;
            // 
            // txtStartDate
            // 
            this.txtStartDate.DataField = "StartDate";
            this.txtStartDate.Height = 0.2F;
            this.txtStartDate.Left = 1.377953F;
            this.txtStartDate.Name = "txtStartDate";
            this.txtStartDate.OutputFormat = resources.GetString("txtStartDate.OutputFormat");
            this.txtStartDate.Style = "font-weight: bold";
            this.txtStartDate.Text = "txtStartDate";
            this.txtStartDate.Top = 0.5905511F;
            this.txtStartDate.Width = 1.181102F;
            // 
            // TextBox12
            // 
            this.TextBox12.Height = 0.2F;
            this.TextBox12.Left = 2.559055F;
            this.TextBox12.Name = "TextBox12";
            this.TextBox12.Text = "?????? ????:";
            this.TextBox12.Top = 0.5905511F;
            this.TextBox12.Width = 1.377953F;
            // 
            // txtEndDate
            // 
            this.txtEndDate.DataField = "EndDate";
            this.txtEndDate.Height = 0.2F;
            this.txtEndDate.Left = 3.937008F;
            this.txtEndDate.Name = "txtEndDate";
            this.txtEndDate.OutputFormat = resources.GetString("txtEndDate.OutputFormat");
            this.txtEndDate.Style = "font-weight: bold";
            this.txtEndDate.Text = "txtEndDate";
            this.txtEndDate.Top = 0.5905511F;
            this.txtEndDate.Width = 1.181102F;
            // 
            // txtPriceLbl
            // 
            this.txtPriceLbl.Height = 0.2F;
            this.txtPriceLbl.Left = 4.330709F;
            this.txtPriceLbl.Name = "txtPriceLbl";
            this.txtPriceLbl.Text = "?????????? ????:";
            this.txtPriceLbl.Top = 1.380413F;
            this.txtPriceLbl.Width = 1.181102F;
            // 
            // txtRate
            // 
            this.txtRate.DataField = "Rate";
            this.txtRate.Height = 0.2F;
            this.txtRate.Left = 5.536417F;
            this.txtRate.Name = "txtRate";
            this.txtRate.Style = "font-weight: bold";
            this.txtRate.Text = "txtRate";
            this.txtRate.Top = 1.380413F;
            this.txtRate.Width = 0.9842521F;
            // 
            // txtEuroLbl
            // 
            this.txtEuroLbl.Height = 0.2F;
            this.txtEuroLbl.Left = 0F;
            this.txtEuroLbl.Name = "txtEuroLbl";
            this.txtEuroLbl.Text = "???? ????(EUR):";
            this.txtEuroLbl.Top = 1.656004F;
            this.txtEuroLbl.Width = 1.377953F;
            // 
            // txtAmountEUR
            // 
            this.txtAmountEUR.Height = 0.2F;
            this.txtAmountEUR.Left = 1.377953F;
            this.txtAmountEUR.Name = "txtAmountEUR";
            this.txtAmountEUR.Style = "font-weight: bold";
            this.txtAmountEUR.Text = "txtAmountEUR";
            this.txtAmountEUR.Top = 1.656004F;
            this.txtAmountEUR.Width = 1.181102F;
            // 
            // txtBGNLbl
            // 
            this.txtBGNLbl.Height = 0.2F;
            this.txtBGNLbl.Left = 2.559055F;
            this.txtBGNLbl.Name = "txtBGNLbl";
            this.txtBGNLbl.Text = "???? ????(BGN):";
            this.txtBGNLbl.Top = 1.656004F;
            this.txtBGNLbl.Width = 1.377953F;
            // 
            // txtAmountBGN
            // 
            this.txtAmountBGN.Height = 0.2F;
            this.txtAmountBGN.Left = 3.937008F;
            this.txtAmountBGN.Name = "txtAmountBGN";
            this.txtAmountBGN.Style = "font-weight: bold";
            this.txtAmountBGN.Text = "txtAmountBGN";
            this.txtAmountBGN.Top = 1.656004F;
            this.txtAmountBGN.Width = 1.181102F;
            // 
            // SubReport1
            // 
            this.SubReport1.CloseBorder = false;
            this.SubReport1.Height = 0.1968504F;
            this.SubReport1.Left = 1.377953F;
            this.SubReport1.Name = "SubReport1";
            this.SubReport1.Report = null;
            this.SubReport1.Top = 1.104823F;
            this.SubReport1.Width = 5.708662F;
            // 
            // TextBox19
            // 
            this.TextBox19.Height = 0.2F;
            this.TextBox19.Left = 0F;
            this.TextBox19.Name = "TextBox19";
            this.TextBox19.Text = "???????????:";
            this.TextBox19.Top = 1.104823F;
            this.TextBox19.Width = 1.377953F;
            // 
            // TextBox20
            // 
            this.TextBox20.Height = 0.2F;
            this.TextBox20.Left = 0F;
            this.TextBox20.Name = "TextBox20";
            this.TextBox20.Text = "?????????:";
            this.TextBox20.Top = 0.8292326F;
            this.TextBox20.Width = 1.377953F;
            // 
            // TextBox21
            // 
            this.TextBox21.DataField = "Note";
            this.TextBox21.Height = 0.2F;
            this.TextBox21.Left = 1.377953F;
            this.TextBox21.Name = "TextBox21";
            this.TextBox21.OutputFormat = resources.GetString("TextBox21.OutputFormat");
            this.TextBox21.Style = "font-weight: bold";
            this.TextBox21.Text = "TextBox21";
            this.TextBox21.Top = 0.8292326F;
            this.TextBox21.Width = 5.708662F;
            // 
            // txtSep
            // 
            this.txtSep.Height = 0.1574803F;
            this.txtSep.Left = 0F;
            this.txtSep.Name = "txtSep";
            this.txtSep.Top = 2.221949F;
            this.txtSep.Width = 1.377953F;
            // 
            // Line11
            // 
            this.Line11.Height = 0F;
            this.Line11.Left = 0.006944444F;
            this.Line11.LineWeight = 1F;
            this.Line11.Name = "Line11";
            this.Line11.Top = 0.006944444F;
            this.Line11.Width = 7.086611F;
            this.Line11.X1 = 0.006944444F;
            this.Line11.X2 = 7.093555F;
            this.Line11.Y1 = 0.006944444F;
            this.Line11.Y2 = 0.006944444F;
            // 
            // lblPageFooter
            // 
            this.lblPageFooter.Height = 0.1968504F;
            this.lblPageFooter.HyperLink = null;
            this.lblPageFooter.Left = 0F;
            this.lblPageFooter.Name = "lblPageFooter";
            this.lblPageFooter.Style = "font-size: 8pt; font-weight: bold; text-align: left";
            this.lblPageFooter.Text = "???????????? ???????";
            this.lblPageFooter.Top = 0.02460628F;
            this.lblPageFooter.Width = 6.299212F;
            // 
            // lblPage
            // 
            this.lblPage.Height = 0.1968504F;
            this.lblPage.HyperLink = null;
            this.lblPage.Left = 6.299212F;
            this.lblPage.Name = "lblPage";
            this.lblPage.Style = "font-size: 8pt; font-weight: bold; text-align: right";
            this.lblPage.Text = "Page 25";
            this.lblPage.Top = 0.02460628F;
            this.lblPage.Width = 0.7874014F;
            // 
            // Line4
            // 
            this.Line4.Height = 0F;
            this.Line4.Left = 0F;
            this.Line4.LineWeight = 2F;
            this.Line4.Name = "Line4";
            this.Line4.Top = 0.0384952F;
            this.Line4.Width = 7.086611F;
            this.Line4.X1 = 0F;
            this.Line4.X2 = 7.086611F;
            this.Line4.Y1 = 0.0384952F;
            this.Line4.Y2 = 0.0384952F;
            // 
            // ActiveReport1
            // 
            this.MasterReport = false;
            this.PageSettings.DefaultPaperSize = false;
            this.PageSettings.Margins.Bottom = 0.7875F;
            this.PageSettings.Margins.Left = 1.377778F;
            this.PageSettings.Margins.Right = 0.39375F;
            this.PageSettings.Margins.Top = 0.7875F;
            this.PageSettings.PaperHeight = 11.69306F;
            this.PageSettings.PaperKind = System.Drawing.Printing.PaperKind.A4;
            this.PageSettings.PaperWidth = 8.268056F;
            this.PrintWidth = 7.0625F;
            this.Sections.Add(this.ReportHeader);
            this.Sections.Add(this.PageHeader);
            this.Sections.Add(this.Detail);
            this.Sections.Add(this.PageFooter);
            this.Sections.Add(this.ReportFooter);
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule(resources.GetString("$this.StyleSheet"), "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: inherit; font-style: inherit; font-variant: inherit; font-weight: bo" +
                        "ld; font-size: 16pt; font-size-adjust: inherit; font-stretch: inherit", "Heading1", "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: Times New Roman; font-style: italic; font-variant: inherit; font-wei" +
                        "ght: bold; font-size: 14pt; font-size-adjust: inherit; font-stretch: inherit", "Heading2", "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: inherit; font-style: inherit; font-variant: inherit; font-weight: bo" +
                        "ld; font-size: 13pt; font-size-adjust: inherit; font-stretch: inherit", "Heading3", "Normal"));
            ((System.ComponentModel.ISupportInitialize)(this.txtReportTitle)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtProjectNameHeader)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPageHeader)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TextBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSubprojectType)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPaymentScheme)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtFoldersGiven)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtFALbl)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtFoldersArchive)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtFGLbl)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPSLbl)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTitle)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TextBox11)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtStartDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TextBox12)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtEndDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPriceLbl)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtRate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtEuroLbl)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAmountEUR)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtBGNLbl)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAmountBGN)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TextBox19)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TextBox20)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TextBox21)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSep)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblPageFooter)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblPage)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();

            // Attach Report Events
            this.Detail.Format += new System.EventHandler(this.Detail_Format);
            this.ReportStart += new System.EventHandler(this.SubcontractersSubreport_ReportStart);
            this.ReportHeader.Format += new System.EventHandler(this.ReportHeader_Format);
            this.PageHeader.Format += new System.EventHandler(this.PageHeader_Format);
            this.PageFooter.Format += new System.EventHandler(this.PageFooter_Format);
        }

		#endregion

        private ReportHeader ReportHeader;
        private TextBox txtReportTitle;
        private Line Line3;
        private TextBox txtProjectNameHeader;
        private PageHeader PageHeader;
        private Line Line2;
        private TextBox txtPageHeader;
        private Detail Detail;
        private TextBox TextBox1;
        private TextBox txtSubprojectType;
        private TextBox txtPaymentScheme;
        private TextBox txtFoldersGiven;
        private TextBox txtFALbl;
        private TextBox txtFoldersArchive;
        private TextBox txtFGLbl;
        private TextBox txtPSLbl;
        private Line Line1;
        private TextBox txtTitle;
        private TextBox TextBox11;
        private TextBox txtStartDate;
        private TextBox TextBox12;
        private TextBox txtEndDate;
        private TextBox txtPriceLbl;
        private TextBox txtRate;
        private TextBox txtEuroLbl;
        private TextBox txtAmountEUR;
        private TextBox txtBGNLbl;
        private TextBox txtAmountBGN;
        private SubReport SubReport1;
        private TextBox TextBox19;
        private TextBox TextBox20;
        private TextBox TextBox21;
        private TextBox txtSep;
        private PageFooter PageFooter;
        private Line Line11;
        private Label lblPageFooter;
        private Label lblPage;
        private ReportFooter ReportFooter;
        private Line Line4;
	}
}
