using System;
using Asa.Timesheet.Data;
using Asa.Timesheet.Data.Reports;
using GrapeCity.ActiveReports;
using GrapeCity.ActiveReports.Controls;
using GrapeCity.ActiveReports.SectionReportModel;
using GrapeCity.ActiveReports.Document.Section;
using GrapeCity.ActiveReports.Document;

namespace Asa.Timesheet.WebPages.Reports
{
    public class PokazateliSubreport : GrapeCity.ActiveReports.SectionReport
	{
		decimal _rzp = 0;
		decimal _area = 0;
		decimal _parterArea=0;
		decimal _rzpAbove = 0;
		decimal _kvadraturaNaChertej = 0;
		int _buildings=0;
		decimal _rzpAbove1=0;
		ChecksVector _checks = null;
		bool _IsCode = false;
        public PokazateliSubreport(bool IsCode, ChecksVector checks)
        {
            _IsCode = IsCode;
            _checks = checks;
            InitializeComponent();
        }

        internal void Reset(decimal area, decimal kv, decimal parterArea, decimal rzpAbove, int Buildings)
        {
            _rzp = 0;
            _area = area;
            _parterArea = parterArea;
            _rzpAbove = rzpAbove;
            _kvadraturaNaChertej = kv;
            _buildings = Buildings;
            _rzpAbove1 = 0;
        }

        private void GroupHeader1_Format(object sender, System.EventArgs eArgs)
        {
            if (_buildings == 1)
            {
                lbSubLabel.Visible = lbSub.Visible = false;
            }
            if (!_IsCode)
            {
                this.Label13.Text = "";
                this.Line17.Visible = false;
            }
            this.GroupHeader1.SizeToFit(true);
        }

        private void Detail_Format(object sender, System.EventArgs eArgs)
        {
            if (this.Fields.Count > 0)
            {
                decimal nivo = (decimal)this.Fields["Kota"].Value;
                int contentType = (int)this.Fields["ContentTypeID"].Value;
                decimal area = (decimal)this.Fields["Area"].Value;


                //				if (contentType == (int)PokazateliReportData.ContentTypes.Parter) _parterArea+=area;
                //        
                if ((contentType != (int)PokazateliReportData.ContentTypes.Suteren) &&
                    (contentType != (int)PokazateliReportData.ContentTypes.KvadraturaNaChertej)) this._rzpAbove1 += area;


                if (contentType == (int)PokazateliReportData.ContentTypes.KvadraturaNaChertej)
                {
                    //this._kvadraturaNaChertej+=area;
                    this.Detail.Visible = false;
                }
                else
                {
                    this.Detail.Visible = true;
                    this._rzp += (decimal)this.Fields["Area"].Value;
                }
            }
            //this.Detail.Visible = (_kvadraturaNaChertej>0);
            if (!_IsCode)
            {
                this.Line18.Visible = false;
                this.txtOzn.Text = "";
            }
            else
            {
                int _cid = int.Parse(this.txtcontentid.Text);
                int index = _checks.IndexOf(_cid, true);
                if (index != -1)
                    if (!_checks[index].IsChecked)
                    {
                        this.txtOzn.Text = "";
                        this.txtREV.Text = "";
                    }
                if (this.txtREV.Text != "" && this.txtREV.Text != null)
                    this.txtOzn.Text = string.Concat(this.txtOzn.Text, ".", this.txtREV.Text);
            }
            this.Detail.SizeToFit(true);
        }

        private void GroupFooter1_Format(object sender, System.EventArgs eArgs)
        {
            // RZP
            this.txtRZP.Text = UIHelpers.FormatDecimal2(_rzp);
            //this.txtRZPTable.Text =
            //String.Concat( UIHelpers.FormatDecimal2(_rzp), " ", Resource.ResourceManager["Reports_strSquareMeters"] );

            //RZP nad terena
            this.txtRZPAbove.Text = UIHelpers.FormatDecimal2(this._rzpAbove1);
            //	String.Concat( UIHelpers.FormatDecimal2(this._rzpAbove1), " ", 
            //	Resource.ResourceManager["Reports_strSquareMeters"]);

            //edited 29.01.2008 by ivailo
            //notes:RZP pod terena
            this.txtRZPUnder.Text = UIHelpers.FormatDecimal2(_rzp - this._rzpAbove1);


            // Plosht
            this.txtArea.Text = UIHelpers.FormatDecimal2(this._kvadraturaNaChertej);
            //	String.Concat( UIHelpers.FormatDecimal2(this._kvadraturaNaChertej), " ",
            //	Resource.ResourceManager["Reports_strSquareMeters"]);

            //Plutnost
            decimal plutnost = this._kvadraturaNaChertej == 0 ? 0 : this._parterArea / this._kvadraturaNaChertej * 100;
            this.txtPlutnost.Text = String.Concat(UIHelpers.FormatDecimal2(plutnost), " %");

            //KINT
            decimal KINT = 0;
            if (this._kvadraturaNaChertej > 0)
                KINT = (_rzpAbove) == 0 ? 0 : this._rzpAbove / this._kvadraturaNaChertej;

            this.txtKINT.Text = KINT == 0 ? "-" : UIHelpers.FormatDecimal2(KINT);
        }

		internal decimal Area
		{
			get { return _area; }
			set { _area = value; }
		}

		#region ActiveReports Designer generated code














































        public void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(PokazateliSubreport));
            this.Detail = new GrapeCity.ActiveReports.SectionReportModel.Detail();
            this.GroupHeader1 = new GrapeCity.ActiveReports.SectionReportModel.GroupHeader();
            this.GroupFooter1 = new GrapeCity.ActiveReports.SectionReportModel.GroupFooter();
            this.Label13 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label5 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label6 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label7 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Line3 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.Line4 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.Line5 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.Line6 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.Line7 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.Line8 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.lbSub = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.lbSubLabel = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.Line17 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.txtNivo = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtRazpredelenie = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtZastrPl = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.Line1 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.Line9 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.Line10 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.Line11 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.Line12 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.txtOzn = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.Line18 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.txtREV = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtcontentid = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.Label8 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.txtRZP = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.Label9 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.txtRZPAbove = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.Label10 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.txtArea = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.Label11 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.txtPlutnost = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.Label12 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.txtKINT = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.TextBox2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtRZPTable = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.Line13 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.Line14 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.Line15 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.Line16 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.Label14 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.txtRZPUnder = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            ((System.ComponentModel.ISupportInitialize)(this.Label13)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbSub)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbSubLabel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtNivo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtRazpredelenie)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtZastrPl)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtOzn)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtREV)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtcontentid)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtRZP)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtRZPAbove)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtArea)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label11)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPlutnost)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label12)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtKINT)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TextBox2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtRZPTable)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label14)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtRZPUnder)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            // 
            // Detail
            // 
            this.Detail.ColumnSpacing = 0F;
            this.Detail.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
						this.txtNivo,
						this.txtRazpredelenie,
						this.txtZastrPl,
						this.Line1,
						this.Line9,
						this.Line10,
						this.Line11,
						this.Line12,
						this.txtOzn,
						this.Line18,
						this.txtREV,
						this.txtcontentid});
            this.Detail.Height = 0.4375F;
            this.Detail.Name = "Detail";
            // 
            // GroupHeader1
            // 
            this.GroupHeader1.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
						this.Label13,
						this.Label5,
						this.Label6,
						this.Label7,
						this.Line3,
						this.Line4,
						this.Line5,
						this.Line6,
						this.Line7,
						this.Line8,
						this.lbSub,
						this.lbSubLabel,
						this.Line17});
            this.GroupHeader1.GroupKeepTogether = GrapeCity.ActiveReports.SectionReportModel.GroupKeepTogether.All;
            this.GroupHeader1.Height = 0.6666667F;
            this.GroupHeader1.Name = "GroupHeader1";
            this.GroupHeader1.RepeatStyle = GrapeCity.ActiveReports.SectionReportModel.RepeatStyle.OnPage;
            // 
            // GroupFooter1
            // 
            this.GroupFooter1.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
						this.Label8,
						this.txtRZP,
						this.Label9,
						this.txtRZPAbove,
						this.Label10,
						this.txtArea,
						this.Label11,
						this.txtPlutnost,
						this.Label12,
						this.txtKINT,
						this.TextBox2,
						this.txtRZPTable,
						this.Line13,
						this.Line14,
						this.Line15,
						this.Line16,
						this.Label14,
						this.txtRZPUnder});
            this.GroupFooter1.Height = 2.113889F;
            this.GroupFooter1.KeepTogether = true;
            this.GroupFooter1.Name = "GroupFooter1";
            // 
            // Label13
            // 
            this.Label13.Height = 0.236F;
            this.Label13.HyperLink = null;
            this.Label13.Left = 3.493F;
            this.Label13.Name = "Label13";
            this.Label13.Style = "font-size: 12pt; font-weight: bold; text-align: left; vertical-align: middle";
            this.Label13.Text = "? ?? ??????";
            this.Label13.Top = 0.375F;
            this.Label13.Width = 1.12F;
            // 
            // Label5
            // 
            this.Label5.Height = 0.2362205F;
            this.Label5.HyperLink = null;
            this.Label5.Left = 0.625F;
            this.Label5.Name = "Label5";
            this.Label5.Style = "font-size: 12pt; font-weight: bold; text-align: center; vertical-align: middle";
            this.Label5.Text = "????";
            this.Label5.Top = 0.375F;
            this.Label5.Width = 0.9F;
            // 
            // Label6
            // 
            this.Label6.Height = 0.236F;
            this.Label6.HyperLink = null;
            this.Label6.Left = 1.5F;
            this.Label6.Name = "Label6";
            this.Label6.Style = "font-size: 12pt; font-weight: bold; text-align: center; vertical-align: middle";
            this.Label6.Text = "?????????????";
            this.Label6.Top = 0.375F;
            this.Label6.Width = 1.98F;
            // 
            // Label7
            // 
            this.Label7.Height = 0.236F;
            this.Label7.HyperLink = null;
            this.Label7.Left = 4.613F;
            this.Label7.Name = "Label7";
            this.Label7.Style = "font-size: 12pt; font-weight: bold; text-align: center; vertical-align: middle";
            this.Label7.Text = "????????? ????(?�)";
            this.Label7.Top = 0.375F;
            this.Label7.Width = 1.7F;
            // 
            // Line3
            // 
            this.Line3.Height = 0F;
            this.Line3.Left = 0.569F;
            this.Line3.LineWeight = 1F;
            this.Line3.Name = "Line3";
            this.Line3.Top = 0.375F;
            this.Line3.Width = 5.758F;
            this.Line3.X1 = 0.569F;
            this.Line3.X2 = 6.327F;
            this.Line3.Y1 = 0.375F;
            this.Line3.Y2 = 0.375F;
            // 
            // Line4
            // 
            this.Line4.Height = 0.2760001F;
            this.Line4.Left = 0.5694444F;
            this.Line4.LineWeight = 1F;
            this.Line4.Name = "Line4";
            this.Line4.Top = 0.375F;
            this.Line4.Width = 0F;
            this.Line4.X1 = 0.5694444F;
            this.Line4.X2 = 0.5694444F;
            this.Line4.Y1 = 0.375F;
            this.Line4.Y2 = 0.6510001F;
            // 
            // Line5
            // 
            this.Line5.Height = 0.2755905F;
            this.Line5.Left = 6.327F;
            this.Line5.LineWeight = 1F;
            this.Line5.Name = "Line5";
            this.Line5.Top = 0.375F;
            this.Line5.Width = 0F;
            this.Line5.X1 = 6.327F;
            this.Line5.X2 = 6.327F;
            this.Line5.Y1 = 0.375F;
            this.Line5.Y2 = 0.6505905F;
            // 
            // Line6
            // 
            this.Line6.Height = 0.2760001F;
            this.Line6.Left = 1.465F;
            this.Line6.LineWeight = 1F;
            this.Line6.Name = "Line6";
            this.Line6.Top = 0.375F;
            this.Line6.Width = 0F;
            this.Line6.X1 = 1.465F;
            this.Line6.X2 = 1.465F;
            this.Line6.Y1 = 0.375F;
            this.Line6.Y2 = 0.6510001F;
            // 
            // Line7
            // 
            this.Line7.Height = 0.2760001F;
            this.Line7.Left = 4.595F;
            this.Line7.LineWeight = 1F;
            this.Line7.Name = "Line7";
            this.Line7.Top = 0.375F;
            this.Line7.Width = 0F;
            this.Line7.X1 = 4.595F;
            this.Line7.X2 = 4.595F;
            this.Line7.Y1 = 0.375F;
            this.Line7.Y2 = 0.6510001F;
            // 
            // Line8
            // 
            this.Line8.Height = 0F;
            this.Line8.Left = 0.569F;
            this.Line8.LineWeight = 1F;
            this.Line8.Name = "Line8";
            this.Line8.Top = 0.6505905F;
            this.Line8.Width = 5.758F;
            this.Line8.X1 = 0.569F;
            this.Line8.X2 = 6.327F;
            this.Line8.Y1 = 0.6505905F;
            this.Line8.Y2 = 0.6505905F;
            // 
            // lbSub
            // 
            this.lbSub.DataField = "Signature";
            this.lbSub.Height = 0.2F;
            this.lbSub.Left = 1.625F;
            this.lbSub.Name = "lbSub";
            this.lbSub.Style = "font-size: 11pt; vertical-align: bottom";
            this.lbSub.Top = 0F;
            this.lbSub.Width = 4.688F;
            // 
            // lbSubLabel
            // 
            this.lbSubLabel.Height = 0.1875F;
            this.lbSubLabel.Left = 0.625F;
            this.lbSubLabel.Name = "lbSubLabel";
            this.lbSubLabel.Style = "font-size: 11pt";
            this.lbSubLabel.Text = "????????:";
            this.lbSubLabel.Top = 0.01249999F;
            this.lbSubLabel.Width = 1F;
            // 
            // Line17
            // 
            this.Line17.Height = 0.2760001F;
            this.Line17.Left = 3.47F;
            this.Line17.LineWeight = 1F;
            this.Line17.Name = "Line17";
            this.Line17.Top = 0.375F;
            this.Line17.Width = 0F;
            this.Line17.X1 = 3.47F;
            this.Line17.X2 = 3.47F;
            this.Line17.Y1 = 0.375F;
            this.Line17.Y2 = 0.6510001F;
            // 
            // txtNivo
            // 
            this.txtNivo.DataField = "Kota";
            this.txtNivo.Height = 0.2F;
            this.txtNivo.Left = 0.609F;
            this.txtNivo.Name = "txtNivo";
            this.txtNivo.OutputFormat = resources.GetString("txtNivo.OutputFormat");
            this.txtNivo.Style = "font-size: 11pt";
            this.txtNivo.Text = "txtNivo";
            this.txtNivo.Top = 0F;
            this.txtNivo.Width = 0.9F;
            // 
            // txtRazpredelenie
            // 
            this.txtRazpredelenie.DataField = "ContentType";
            this.txtRazpredelenie.Height = 0.2F;
            this.txtRazpredelenie.Left = 1.5F;
            this.txtRazpredelenie.Name = "txtRazpredelenie";
            this.txtRazpredelenie.Style = resources.GetString("txtRazpredelenie.Style");
            this.txtRazpredelenie.Text = "txtRazpredelenie";
            this.txtRazpredelenie.Top = 0F;
            this.txtRazpredelenie.Width = 1.98F;
            // 
            // txtZastrPl
            // 
            this.txtZastrPl.DataField = "Area";
            this.txtZastrPl.Height = 0.2F;
            this.txtZastrPl.Left = 4.613F;
            this.txtZastrPl.Name = "txtZastrPl";
            this.txtZastrPl.OutputFormat = resources.GetString("txtZastrPl.OutputFormat");
            this.txtZastrPl.Style = "font-size: 11pt; text-align: right";
            this.txtZastrPl.Text = "txtZastrPl";
            this.txtZastrPl.Top = 0F;
            this.txtZastrPl.Width = 1.7F;
            // 
            // Line1
            // 
            this.Line1.Height = 0F;
            this.Line1.Left = 0.569F;
            this.Line1.LineWeight = 1F;
            this.Line1.Name = "Line1";
            this.Line1.Top = 0.2755906F;
            this.Line1.Width = 5.758F;
            this.Line1.X1 = 0.569F;
            this.Line1.X2 = 6.327F;
            this.Line1.Y1 = 0.2755906F;
            this.Line1.Y2 = 0.2755906F;
            // 
            // Line9
            // 
            this.Line9.Height = 0.2755906F;
            this.Line9.Left = 0.569F;
            this.Line9.LineWeight = 1F;
            this.Line9.Name = "Line9";
            this.Line9.Top = 0F;
            this.Line9.Width = 0F;
            this.Line9.X1 = 0.569F;
            this.Line9.X2 = 0.569F;
            this.Line9.Y1 = 0F;
            this.Line9.Y2 = 0.2755906F;
            // 
            // Line10
            // 
            this.Line10.Height = 0.2755906F;
            this.Line10.Left = 6.327F;
            this.Line10.LineWeight = 1F;
            this.Line10.Name = "Line10";
            this.Line10.Top = 0F;
            this.Line10.Width = 0F;
            this.Line10.X1 = 6.327F;
            this.Line10.X2 = 6.327F;
            this.Line10.Y1 = 0F;
            this.Line10.Y2 = 0.2755906F;
            // 
            // Line11
            // 
            this.Line11.Height = 0.2755906F;
            this.Line11.Left = 4.595F;
            this.Line11.LineWeight = 1F;
            this.Line11.Name = "Line11";
            this.Line11.Top = 0F;
            this.Line11.Width = 0F;
            this.Line11.X1 = 4.595F;
            this.Line11.X2 = 4.595F;
            this.Line11.Y1 = 0F;
            this.Line11.Y2 = 0.2755906F;
            // 
            // Line12
            // 
            this.Line12.Height = 0.2755906F;
            this.Line12.Left = 1.465F;
            this.Line12.LineWeight = 1F;
            this.Line12.Name = "Line12";
            this.Line12.Top = 0F;
            this.Line12.Width = 0F;
            this.Line12.X1 = 1.465F;
            this.Line12.X2 = 1.465F;
            this.Line12.Y1 = 0F;
            this.Line12.Y2 = 0.2755906F;
            // 
            // txtOzn
            // 
            this.txtOzn.DataField = "ContentCode";
            this.txtOzn.Height = 0.2F;
            this.txtOzn.Left = 3.493F;
            this.txtOzn.Name = "txtOzn";
            this.txtOzn.Style = resources.GetString("txtOzn.Style");
            this.txtOzn.Text = "txtOzn";
            this.txtOzn.Top = 0F;
            this.txtOzn.Width = 1.12F;
            // 
            // Line18
            // 
            this.Line18.Height = 0.276F;
            this.Line18.Left = 3.47F;
            this.Line18.LineWeight = 1F;
            this.Line18.Name = "Line18";
            this.Line18.Top = 0F;
            this.Line18.Width = 0F;
            this.Line18.X1 = 3.47F;
            this.Line18.X2 = 3.47F;
            this.Line18.Y1 = 0F;
            this.Line18.Y2 = 0.276F;
            // 
            // txtREV
            // 
            this.txtREV.DataField = "ContentRevision";
            this.txtREV.Height = 0.1F;
            this.txtREV.Left = 0.375F;
            this.txtREV.Name = "txtREV";
            this.txtREV.Top = 0.125F;
            this.txtREV.Visible = false;
            this.txtREV.Width = 0.1F;
            // 
            // txtcontentid
            // 
            this.txtcontentid.DataField = "ContentID";
            this.txtcontentid.Height = 0.1F;
            this.txtcontentid.Left = 0.4375F;
            this.txtcontentid.Name = "txtcontentid";
            this.txtcontentid.Top = 0.0625F;
            this.txtcontentid.Visible = false;
            this.txtcontentid.Width = 0.1F;
            // 
            // Label8
            // 
            this.Label8.Height = 0.2F;
            this.Label8.HyperLink = null;
            this.Label8.Left = 0.609F;
            this.Label8.Name = "Label8";
            this.Label8.Style = "font-size: 12pt";
            this.Label8.Text = "?????? ????????? ????:";
            this.Label8.Top = 1.35F;
            this.Label8.Width = 4.004F;
            // 
            // txtRZP
            // 
            this.txtRZP.Height = 0.2F;
            this.txtRZP.Left = 4.613F;
            this.txtRZP.Name = "txtRZP";
            this.txtRZP.OutputFormat = resources.GetString("txtRZP.OutputFormat");
            this.txtRZP.Style = "font-size: 12pt; font-weight: bold; text-align: right";
            this.txtRZP.Text = "txtRZP";
            this.txtRZP.Top = 1.35F;
            this.txtRZP.Width = 1.7F;
            // 
            // Label9
            // 
            this.Label9.Height = 0.2F;
            this.Label9.HyperLink = null;
            this.Label9.Left = 0.609F;
            this.Label9.Name = "Label9";
            this.Label9.Style = "font-size: 12pt";
            this.Label9.Text = "????????? ????????? ????:";
            this.Label9.Top = 1.85F;
            this.Label9.Width = 4.004F;
            // 
            // txtRZPAbove
            // 
            this.txtRZPAbove.Height = 0.2F;
            this.txtRZPAbove.Left = 4.613F;
            this.txtRZPAbove.Name = "txtRZPAbove";
            this.txtRZPAbove.OutputFormat = resources.GetString("txtRZPAbove.OutputFormat");
            this.txtRZPAbove.Style = "font-size: 12pt; font-weight: bold; text-align: right; ddo-char-set: 0";
            this.txtRZPAbove.Text = "txtRZPAbove";
            this.txtRZPAbove.Top = 1.85F;
            this.txtRZPAbove.Width = 1.7F;
            // 
            // Label10
            // 
            this.Label10.Height = 0.2F;
            this.Label10.HyperLink = null;
            this.Label10.Left = 0.609F;
            this.Label10.Name = "Label10";
            this.Label10.Style = "font-size: 12pt";
            this.Label10.Text = "???? ?? ?????:";
            this.Label10.Top = 0.6000001F;
            this.Label10.Width = 4.004F;
            // 
            // txtArea
            // 
            this.txtArea.Height = 0.2F;
            this.txtArea.Left = 4.613F;
            this.txtArea.Name = "txtArea";
            this.txtArea.OutputFormat = resources.GetString("txtArea.OutputFormat");
            this.txtArea.Style = "font-size: 12pt; font-weight: bold; text-align: right";
            this.txtArea.Text = "txtArea";
            this.txtArea.Top = 0.6000001F;
            this.txtArea.Width = 1.7F;
            // 
            // Label11
            // 
            this.Label11.Height = 0.2F;
            this.Label11.HyperLink = null;
            this.Label11.Left = 0.609F;
            this.Label11.Name = "Label11";
            this.Label11.Style = "font-size: 12pt";
            this.Label11.Text = "????????:";
            this.Label11.Top = 0.85F;
            this.Label11.Width = 4.004F;
            // 
            // txtPlutnost
            // 
            this.txtPlutnost.Height = 0.2F;
            this.txtPlutnost.Left = 4.613F;
            this.txtPlutnost.Name = "txtPlutnost";
            this.txtPlutnost.Style = "font-size: 12pt; font-weight: bold; text-align: right";
            this.txtPlutnost.Text = "txtPlutnost";
            this.txtPlutnost.Top = 0.85F;
            this.txtPlutnost.Width = 1.7F;
            // 
            // Label12
            // 
            this.Label12.Height = 0.2F;
            this.Label12.HyperLink = null;
            this.Label12.Left = 0.609F;
            this.Label12.Name = "Label12";
            this.Label12.Style = "font-size: 12pt";
            this.Label12.Text = "????:";
            this.Label12.Top = 1.1F;
            this.Label12.Width = 4.004F;
            // 
            // txtKINT
            // 
            this.txtKINT.Height = 0.2F;
            this.txtKINT.Left = 4.613F;
            this.txtKINT.Name = "txtKINT";
            this.txtKINT.Style = "font-size: 12pt; font-weight: bold; text-align: right";
            this.txtKINT.Text = "txtKINT";
            this.txtKINT.Top = 1.1F;
            this.txtKINT.Width = 1.7F;
            // 
            // TextBox2
            // 
            this.TextBox2.Height = 0.2F;
            this.TextBox2.Left = 3.493F;
            this.TextBox2.Name = "TextBox2";
            this.TextBox2.Style = resources.GetString("TextBox2.Style");
            this.TextBox2.Text = "???? ???:";
            this.TextBox2.Top = 0F;
            this.TextBox2.Width = 1.12F;
            // 
            // txtRZPTable
            // 
            this.txtRZPTable.DataField = "Area";
            this.txtRZPTable.Height = 0.2F;
            this.txtRZPTable.Left = 4.613F;
            this.txtRZPTable.Name = "txtRZPTable";
            this.txtRZPTable.OutputFormat = resources.GetString("txtRZPTable.OutputFormat");
            this.txtRZPTable.Style = "font-size: 11pt; font-weight: bold; text-align: right";
            this.txtRZPTable.SummaryGroup = "GroupHeader1";
            this.txtRZPTable.SummaryRunning = GrapeCity.ActiveReports.SectionReportModel.SummaryRunning.All;
            this.txtRZPTable.SummaryType = GrapeCity.ActiveReports.SectionReportModel.SummaryType.SubTotal;
            this.txtRZPTable.Text = "TextBox3";
            this.txtRZPTable.Top = 0F;
            this.txtRZPTable.Width = 1.7F;
            // 
            // Line13
            // 
            this.Line13.Height = 0F;
            this.Line13.Left = 0.569F;
            this.Line13.LineWeight = 1F;
            this.Line13.Name = "Line13";
            this.Line13.Top = 0.2755906F;
            this.Line13.Width = 5.758F;
            this.Line13.X1 = 0.569F;
            this.Line13.X2 = 6.327F;
            this.Line13.Y1 = 0.2755906F;
            this.Line13.Y2 = 0.2755906F;
            // 
            // Line14
            // 
            this.Line14.Height = 0.2755906F;
            this.Line14.Left = 0.569F;
            this.Line14.LineWeight = 1F;
            this.Line14.Name = "Line14";
            this.Line14.Top = 0F;
            this.Line14.Width = 0F;
            this.Line14.X1 = 0.569F;
            this.Line14.X2 = 0.569F;
            this.Line14.Y1 = 0F;
            this.Line14.Y2 = 0.2755906F;
            // 
            // Line15
            // 
            this.Line15.Height = 0.2755906F;
            this.Line15.Left = 6.327F;
            this.Line15.LineWeight = 1F;
            this.Line15.Name = "Line15";
            this.Line15.Top = 0F;
            this.Line15.Width = 0F;
            this.Line15.X1 = 6.327F;
            this.Line15.X2 = 6.327F;
            this.Line15.Y1 = 0F;
            this.Line15.Y2 = 0.2755906F;
            // 
            // Line16
            // 
            this.Line16.Height = 0.2755906F;
            this.Line16.Left = 4.595F;
            this.Line16.LineWeight = 1F;
            this.Line16.Name = "Line16";
            this.Line16.Top = 0F;
            this.Line16.Width = 0F;
            this.Line16.X1 = 4.595F;
            this.Line16.X2 = 4.595F;
            this.Line16.Y1 = 0F;
            this.Line16.Y2 = 0.2755906F;
            // 
            // Label14
            // 
            this.Label14.Height = 0.2F;
            this.Label14.HyperLink = null;
            this.Label14.Left = 0.609F;
            this.Label14.Name = "Label14";
            this.Label14.Style = "font-size: 12pt";
            this.Label14.Text = "????????? ????????? ?????:";
            this.Label14.Top = 1.6F;
            this.Label14.Width = 4.004F;
            // 
            // txtRZPUnder
            // 
            this.txtRZPUnder.Height = 0.2F;
            this.txtRZPUnder.Left = 4.613F;
            this.txtRZPUnder.Name = "txtRZPUnder";
            this.txtRZPUnder.OutputFormat = resources.GetString("txtRZPUnder.OutputFormat");
            this.txtRZPUnder.Style = "font-size: 12pt; font-weight: bold; text-align: right; ddo-char-set: 0";
            this.txtRZPUnder.Text = "txtRZPUnder";
            this.txtRZPUnder.Top = 1.6F;
            this.txtRZPUnder.Width = 1.7F;
            // 
            // ActiveReport1
            // 
            this.MasterReport = false;
            this.PageSettings.PaperHeight = 11F;
            this.PageSettings.PaperWidth = 8.5F;
            this.PrintWidth = 6.47F;
            this.Sections.Add(this.GroupHeader1);
            this.Sections.Add(this.Detail);
            this.Sections.Add(this.GroupFooter1);
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule(resources.GetString("$this.StyleSheet"), "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: inherit; font-style: inherit; font-variant: inherit; font-weight: bo" +
                        "ld; font-size: 16pt; font-size-adjust: inherit; font-stretch: inherit", "Heading1", "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: Times New Roman; font-style: italic; font-variant: inherit; font-wei" +
                        "ght: bold; font-size: 14pt; font-size-adjust: inherit; font-stretch: inherit", "Heading2", "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: inherit; font-style: inherit; font-variant: inherit; font-weight: bo" +
                        "ld; font-size: 13pt; font-size-adjust: inherit; font-stretch: inherit", "Heading3", "Normal"));
            ((System.ComponentModel.ISupportInitialize)(this.Label13)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbSub)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbSubLabel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtNivo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtRazpredelenie)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtZastrPl)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtOzn)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtREV)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtcontentid)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtRZP)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtRZPAbove)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtArea)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label11)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPlutnost)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label12)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtKINT)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TextBox2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtRZPTable)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label14)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtRZPUnder)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();

            // Attach Report Events
            this.GroupHeader1.Format += new System.EventHandler(this.GroupHeader1_Format);
            this.Detail.Format += new System.EventHandler(this.Detail_Format);
            this.GroupFooter1.Format += new System.EventHandler(this.GroupFooter1_Format);
        }

		#endregion

        private GroupHeader GroupHeader1;
        private Label Label13;
        private Label Label5;
        private Label Label6;
        private Label Label7;
        private Line Line3;
        private Line Line4;
        private Line Line5;
        private Line Line6;
        private Line Line7;
        private Line Line8;
        private TextBox lbSub;
        private TextBox lbSubLabel;
        private Line Line17;
        private Detail Detail;
        private TextBox txtNivo;
        private TextBox txtRazpredelenie;
        private TextBox txtZastrPl;
        private Line Line1;
        private Line Line9;
        private Line Line10;
        private Line Line11;
        private Line Line12;
        private TextBox txtOzn;
        private Line Line18;
        private TextBox txtREV;
        private TextBox txtcontentid;
        private GroupFooter GroupFooter1;
        private Label Label8;
        private TextBox txtRZP;
        private Label Label9;
        private TextBox txtRZPAbove;
        private Label Label10;
        private TextBox txtArea;
        private Label Label11;
        private TextBox txtPlutnost;
        private Label Label12;
        private TextBox txtKINT;
        private TextBox TextBox2;
        private TextBox txtRZPTable;
        private Line Line13;
        private Line Line14;
        private Line Line15;
        private Line Line16;
        private Label Label14;
        private TextBox txtRZPUnder;
	}
}
