﻿<%@ Page language="c#" Codebehind="EditMeeting.aspx.cs" AutoEventWireup="false" Inherits="Asa.Timesheet.WebPages.EditMeeting" %>
<%@ Register TagPrefix="uc1" TagName="EditForm" Src="UserControls/EditForm.ascx" %>
<%@ Register TagPrefix="uc1" TagName="PageHeader" Src="UserControls/PageHeader.ascx" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
	<head runat="server">
		<title>Meeting</title>
		<meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
		<meta content="C#" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<LINK href="styles/timesheet.css" type="text/css" rel="stylesheet">
		<script language="javascript" src="PopupCalendar.js"></script>
		<script language="javascript">
		
		function MailDblClick()
		{
			var src, dest;
			
			src = event.srcElement;
			switch (src.id)
			{
				case "lbUsers" : dest = document.getElementById("lbSelectedUsers");
								 break;
				case "lbSelectedUsers" : dest = document.getElementById("lbUsers");
								break;
				case "lbClients" : dest = document.getElementById("lbSelectedClients");
								 break;
				case "lbSelectedClients" : dest = document.getElementById("lbClients");
								break;
				case "lbSelectedUsersMails" : dest = document.getElementById("lbUsersMails");
								break;
				case "lbUsersMails" : dest = document.getElementById("lbSelectedUsersMails");
								break;
			}
			
			index = src.selectedIndex;
			if (index==-1) return;
			
			var email = src.options[index].innerText;
			var value = src.options[index].value;
			
			src.options.remove(index);
			var opt = document.createElement("OPTION");
			dest.options.add(opt); 
			opt.innerText = email;
			opt.value = value;
			
			UpdateContainer(src);
			UpdateContainer(dest);
		} 
		
		function ArrowButtonClick()
		{
			var btn = event.srcElement;
			var src, dest;
			
			switch (btn.id)
			{
				case "lbMeetingProjects":
				case "inputProjectToRight" : src = document.getElementById("lbMeetingProjects");
									  dest = document.getElementById("lbSelectedMeetingProjects");
									  break;
									  
				case "lbSelectedMeetingProjects":
				case "inputProjectToLeft" :  src = document.getElementById("lbSelectedMeetingProjects");
									  dest = document.getElementById("lbMeetingProjects");
									  break;
				case "lbUsers":
				case "inputToRight" : src = document.getElementById("lbUsers");
									  dest = document.getElementById("lbSelectedUsers");
									  break;
									  
				case "lbSelectedUsers":
				case "inputToLeft" :  src = document.getElementById("lbSelectedUsers");
									  dest = document.getElementById("lbUsers");
									  break;
				case "lbSelectedUsersMails" :					  
				case "inputUsersToLeft" :   dest = document.getElementById("lbUsersMails");
											src = document.getElementById("lbSelectedUsersMails");
											break;
				case "lbUsersMails":
				case "inputUsersToRight" :  dest = document.getElementById("lbSelectedUsersMails");
											src = document.getElementById("lbUsersMails");
											break;
				case "lbSelectedClients" :
				case "inputUsersToLeft1" :   dest = document.getElementById("lbClients");
											src = document.getElementById("lbSelectedClients");
											break;
				case "lbClients" :
				case "inputUsersToRight1" :  dest = document.getElementById("lbSelectedClients");
											src = document.getElementById("lbClients");
											break;
			}
			
			//for (i=src.options.length-1;i>=0 ;i--)
			var email, value, opt;
			
			for (i=0; i<src.options.length; i++)
			if (src.options[i].selected)
			{ 
				email = src.options[i].innerText;
				value = src.options[i].value;
				opt = document.createElement("OPTION");
				dest.options.add(opt); 
				opt.innerText = email;
				opt.value = value;
			}
			
			for (i=src.options.length-1;i>=0 ;i--)
			if (src.options[i].selected) src.options.remove(i);
			
			UpdateContainer(dest);
			UpdateContainer(src);
			
		}
		
		function UpdateContainer(listBox)
		{
			var container = document.getElementById("hdn"+listBox.id);
			
			var arr = new Array();
			for (i=0; i<listBox.options.length; i++) arr.push(listBox.options[i].value);
			container.value = arr.join(";");
			
		}
		
		
		
		</script>
	</HEAD>
	<body bottomMargin="0" leftMargin="0" topMargin="0" rightMargin="0">
		<form id="Form1" method="post" runat="server">
			<table height="100%" cellSpacing="0" cellPadding="0" width="100%" border="0">
				<tr height="1">
					<td><uc1:pageheader id="header" runat="server"></uc1:pageheader><uc1:editform id="editCtrl" runat="server"></uc1:editform></td>
				</tr>
				<tr>
					<td>
						<table height="100%" cellSpacing="0" cellPadding="0" width="100%" border="0">
							<tr>
								<td class="menuLeft"><asp:placeholder id="menuHolder" runat="server"></asp:placeholder></td>
								
								<td style="PADDING-LEFT: 5px; PADDING-TOP: 3px" vAlign="top" noWrap>
									<TABLE id="Table3" cellSpacing="0" cellPadding="0" width="880" border="0">
										<TR>
											<td style="HEIGHT: 5px" colSpan="2"></TD>
										</TR>
										<TR>
											<td colSpan="2"><IMG height="1" src="images/dot.gif" width="100%"></TD>
										</TR>
										<TR>
											<td colSpan="2" height="3"></td>
										</TR>
										<TR>
											<td style="WIDTH: 4px"></TD>
											<td>
												<TABLE id="Table6" cellSpacing="0" cellPadding="0" border="0" width="950">
													<TR>
														<td style="WIDTH: 184px" vAlign="bottom">
															<asp:button id="btnCancelUP" runat="server" CssClass="ActionButton" EnableViewState="False"
																Text="Обратно"></asp:button></TD>
														<td vAlign="top" noWrap></TD>
														<td noWrap colSpan="3" vAlign="top">
															<asp:button id="btnSaveUP" runat="server" CssClass="ActionButton" EnableViewState="False" Text="Запиши"></asp:button><asp:button id="btnEditUP" style="DISPLAY: none" runat="server" Text="Редактирай срещата" EnableViewState="False"
																CssClass="ActionButton" Width="146px"></asp:button>&nbsp;
															<asp:button id="btnEditIssues" runat="server" CssClass="ActionButton" EnableViewState="False"
																Text="Редактирай задачите " ></asp:button></TD>
													</TR>
													<TR>
														<td style="WIDTH: 184px" vAlign="bottom" height="8"></TD>
														<td vAlign="top" noWrap height="8"></TD>
														<td vAlign="top" noWrap colSpan="3" height="8"></TD>
													</TR>
													<TR>
														<td style="WIDTH: 184px" vAlign="bottom">
															<asp:label id="Label21" runat="server" CssClass="enterDataLabel" EnableViewState="False" Width="100%"
																Font-Bold="True">Проект за печат на бележки:</asp:label></TD>
														<td vAlign="top" noWrap width="4"></TD>
														<td vAlign="top" noWrap colSpan="3">
															<asp:dropdownlist id="ddlProject1" runat="server" CssClass="EnterDataBox" Width="250px"></asp:dropdownlist></TD>
													</TR>
													<TR>
														<td style="WIDTH: 184px" vAlign="bottom">
															<asp:label id="Label18" runat="server" CssClass="enterDataLabel" EnableViewState="False" Width="100%"
																Font-Bold="True">Бележки:</asp:label></TD>
														<td noWrap></TD>
														<td noWrap colSpan="3">
															<asp:button id="btnMinutes" runat="server" Text="Бележки PDF" EnableViewState="False" CssClass="ActionButton"
																></asp:button>&nbsp;
															<asp:button id="btnPDFEN" runat="server" CssClass="ActionButton" EnableViewState="False" Text="Бележки PDF на английски"
																Width="192px"></asp:button>&nbsp;
															<asp:button id="btnWord1" runat="server" CssClass="ActionButton" EnableViewState="False" Text="Бележки PDF в MS WORD"
																></asp:button>&nbsp;
															<asp:hyperlink id="lkMails" style="VERTICAL-ALIGN: bottom; TEXT-ALIGN: center; TEXT-DECORATION: none"
																runat="server" CssClass="ActionButton">Прати мейл</asp:hyperlink>&nbsp;
															<asp:imagebutton id="btnExcel" runat="server" ImageUrl="images/xls.gif" Height="20px" Visible="False"></asp:imagebutton><asp:imagebutton id="btnWord" runat="server" ImageUrl="images/word1.gif" Height="20px" Visible="False"></asp:imagebutton></TD>
													</TR>
													<TR>
														<td style="WIDTH: 184px" vAlign="bottom" height="5"></TD>
														<td noWrap height="5"></TD>
														<td noWrap colSpan="3" height="5"></TD>
													</TR>
													<TR>
														<td style="WIDTH: 184px" vAlign="bottom">
															<asp:label id="Label19" runat="server" CssClass="enterDataLabel" EnableViewState="False" Width="100%"
																Font-Bold="True">Бележки САМО от срещата:</asp:label></TD>
														<td noWrap></TD>
														<td noWrap colSpan="3">
															<asp:button id="btnOnlyMeeting" runat="server" CssClass="ActionButton" EnableViewState="False"
																Text="Бележки САМО от срещата" Width="184px"></asp:button>&nbsp;
															<asp:button id="btnOnlyMeetingEN" runat="server" CssClass="ActionButton" EnableViewState="False"
																Text="Бележки САМО от срещата на англ." Width="240px"></asp:button>&nbsp;</TD>
													</TR>
													<TR>
														<td style="WIDTH: 184px" vAlign="bottom" height="5"></TD>
														<td noWrap height="5"></TD>
														<td noWrap colSpan="3" height="5"></TD>
													</TR>
													<TR>
														<td style="WIDTH: 184px" vAlign="bottom">
															<asp:label id="Label20" runat="server" CssClass="enterDataLabel" EnableViewState="False" Width="100%"
																Font-Bold="True">Бележки към възложителя:</asp:label></TD>
														<td noWrap></TD>
														<td noWrap colSpan="3">
															<asp:button id="btnMinutesInv" runat="server" CssClass="ActionButton" EnableViewState="False"
																Text="Бележки PDF към възложителя" Width="225px"></asp:button>
															<asp:button id="btnMinutesInvEN" runat="server" CssClass="ActionButton" EnableViewState="False"
																Text="Бележки към възложителя на англ." Width="244px"></asp:button></TD>
													</TR>
												</TABLE>
												<asp:label id="lbError" runat="server" CssClass="ErrorLabel" EnableViewState="False" ForeColor="Red"></asp:label></TD>
										</TR>
										<TR>
											<td height="3"></TD>
										</TR>
										<TR>
											<td colSpan="2"><IMG height="1" src="images/dot.gif" width="100%"></TD>
										</TR>
										<TR>
											<td style="WIDTH: 4px"></TD>
											<td>
												<TABLE id="tblForm" cellSpacing="0" cellPadding="3" width="100%" border="0">
													<TBODY>
														<TR>
															<td style="WIDTH: 177px"><asp:label id="Label4" runat="server" EnableViewState="False" CssClass="enterDataLabel" Width="100%">Тип:</asp:label></TD>
															<td><asp:dropdownlist id="ddlProjectsStatus" runat="server" CssClass="EnterDataBox" Width="250px" AutoPostBack="True"></asp:dropdownlist></TD>
														</TR>
														<TR>
															<td style="WIDTH: 177px"><asp:label id="Label8" runat="server" CssClass="enterDataLabel" Width="100%">Сграда:</asp:label></TD>
															<td><asp:dropdownlist id="ddlBuildingTypes" runat="server" CssClass="enterDataBox" Width="250px" AutoPostBack="True"></asp:dropdownlist></TD>
														</TR>
														<TR>
															<td style="WIDTH: 177px"><asp:label id="lblName" runat="server" EnableViewState="False" CssClass="enterDataLabel" Width="100%"
																	Font-Bold="True">Главен Проект:</asp:label></TD>
															<td><asp:dropdownlist id="ddlProject" runat="server" CssClass="EnterDataBox" Width="250px" AutoPostBack="True"></asp:dropdownlist><IMG height="16" alt="" src="images/required1.gif" width="16">
															</TD>
														</TR>
														<TR>
															<td style="WIDTH: 177px"><asp:label id="lbProjects" runat="server" EnableViewState="False" CssClass="enterDataLabel"
																	Width="100%">Проекти:</asp:label></TD>
															<td>
																<P>
																	<TABLE id="Table1" cellSpacing="0" cellPadding="0" width="100%" border="0">
																		<TR>
																			<td style="WIDTH: 228px">
																				<P><asp:listbox id="lbMeetingProjects1" runat="server" CssClass="enterDataBox" Width="300px" Visible="False"
																						SelectionMode="Multiple" Height="106px"></asp:listbox></P>
																				<P><asp:listbox id="lbMeetingProjects" ondblclick="ArrowButtonClick();" runat="server" CssClass="enterDataBox"
																						Width="300px" SelectionMode="Multiple" Height="106px"></asp:listbox></P>
																			</TD>
																			<td style="WIDTH: 34px">
																				<TABLE id="Table2" border="0">
																					<TR>
																						<td><INPUT class="ActionButton" id="inputProjectToRight" style="WIDTH: 26px; HEIGHT: 18px"
																								onclick="ArrowButtonClick();" type="button" size="20" value=">" name="Button2"></TD>
																					</TR>
																					<TR>
																						<td><INPUT class="ActionButton" id="inputProjectToLeft" style="WIDTH: 26px; HEIGHT: 18px" onclick="ArrowButtonClick();"
																								type="button" size="20" value="<" name="Button1"></TD>
																					</TR>
																				</TABLE>
																			</TD>
																			<td>
																				<P><asp:listbox id="lbSelectedMeetingProjects" ondblclick="ArrowButtonClick();" runat="server" CssClass="enterDataBox"
																						Width="300px" SelectionMode="Multiple" Height="106px"></asp:listbox><INPUT id="hdnlbMeetingProjects" style="DISPLAY: none; WIDTH: 42px; HEIGHT: 19px" type="text"
																						size="1" name="Text1" runat="server"><INPUT id="hdnlbSelectedMeetingProjects" style="DISPLAY: none; WIDTH: 34px; HEIGHT: 19px"
																						type="text" size="1" name="Text2" runat="server"><INPUT id="hdnlbMeetingProjects1" style="DISPLAY: none; WIDTH: 42px; HEIGHT: 19px" type="text"
																						size="1" name="Text1" runat="server"></P>
																			</TD>
																		</TR>
																	</TABLE>
																</P>
															</TD>
														</TR>
														<TR>
															<td style="WIDTH: 177px"><asp:label id="lbDate" runat="server" CssClass="enterDataLabel" Width="100%" Font-Bold="True">Дата:</asp:label></TD>
															<td><asp:textbox id="txtStartDate" runat="server" CssClass="enterDataBox" Width="88px" MaxLength="10"></asp:textbox>&nbsp;<IMG id="lkCalendar1" alt="Calendar" src="images/calendar.ico" width="18" align="absBottom"
																	runat="server">&nbsp;<IMG height="16" alt="" src="images/required1.gif" width="16"></TD>
														</TR>
														<TR>
															<td style="WIDTH: 177px; HEIGHT: 22px"><asp:label id="lbStart" runat="server" CssClass="enterDataLabel" Width="100%" Font-Bold="True"> Начален час:</asp:label></TD>
															<td style="HEIGHT: 22px"><asp:dropdownlist id="ddlStartTime" Width="88px" Font-Size="10pt" Runat="server">
																	<asp:ListItem></asp:ListItem>
																</asp:dropdownlist>&nbsp;<IMG height="16" alt="" src="images/required1.gif" width="16"></TD>
														</TR>
														<TR>
															<td style="WIDTH: 177px"><asp:label id="lbEnd" runat="server" CssClass="enterDataLabel" Width="100%" Font-Bold="True">Краен час:</asp:label></TD>
															<td><asp:dropdownlist id="ddlEndTime" Width="88px" Font-Size="10pt" Runat="server">
																	<asp:ListItem></asp:ListItem>
																</asp:dropdownlist>&nbsp;<IMG height="16" alt="" src="images/required1.gif" width="16"></TD>
														</TR>
														<TR>
															<td style="WIDTH: 177px"><asp:label id="lbPlace" runat="server" CssClass="enterDataLabel" Width="100%" Font-Bold="True">Място на провеждане:</asp:label></TD>
															<td><asp:textbox id="txtPlace" runat="server" CssClass="enterDataBox" Width="250px" MaxLength="1000"></asp:textbox>&nbsp;<IMG height="16" alt="" src="images/required1.gif" width="16">
															</TD>
														</TR>
														<TR>
															<td style="WIDTH: 177px">
																<asp:label id="Label17" runat="server" CssClass="enterDataLabel" Width="100%" Font-Bold="True">Място на провеждане (англ. език):</asp:label></TD>
															<td>
																<asp:textbox id="txtPlaceEN" runat="server" CssClass="enterDataBox" Width="250px" MaxLength="1000"></asp:textbox></TD>
														</TR>
														<TR>
															<td style="WIDTH: 177px"></TD>
															<td><asp:button id="btnReload" runat="server" Text="Покажи Подизпълнители и Клиенти" CssClass="ActionButton"
																	Width="235px" DESIGNTIMEDRAGDROP="2337" BackColor="White"></asp:button></TD>
											</TD>
										</TR>
										<TR>
											<td style="WIDTH: 177px"><asp:label id="Label3" runat="server" CssClass="enterDataLabel" Width="100%">Служители:</asp:label></TD>
											<td>
												<TABLE id="Table8" cellSpacing="0" cellPadding="0" width="100%" border="0">
													<TR>
														<td style="WIDTH: 228px"><asp:listbox id="lbUsersMails" ondblclick="ArrowButtonClick();" runat="server" CssClass="enterDataBox"
																Width="300px" SelectionMode="Multiple" Height="106px"></asp:listbox></TD>
														<td style="WIDTH: 34px">
															<TABLE id="Table9" border="0">
																<TR>
																	<td><INPUT class="ActionButton" id="inputUsersToRight" style="WIDTH: 26px; HEIGHT: 18px" onclick="ArrowButtonClick();"
																			type="button" size="20" value=">" name="Button2"></TD>
																</TR>
																<TR>
																	<td><INPUT class="ActionButton" id="inputUsersToLeft" style="WIDTH: 26px; HEIGHT: 18px" onclick="ArrowButtonClick();"
																			type="button" size="20" value="<" name="Button1"></TD>
																</TR>
															</TABLE>
														</TD>
														<td><asp:listbox id="lbSelectedUsersMails" ondblclick="ArrowButtonClick();" runat="server" CssClass="enterDataBox"
																Width="300px" SelectionMode="Multiple" Height="106px"></asp:listbox><INPUT id="hdnlbUsersMails" style="DISPLAY: none; WIDTH: 42px; HEIGHT: 19px" type="text"
																size="1" name="Text1" runat="server"><INPUT id="hdnlbSelectedUsersMails" style="DISPLAY: none; WIDTH: 34px; HEIGHT: 19px" type="text"
																size="1" name="Text2" runat="server"></TD>
													</TR>
												</TABLE>
											</TD>
										</TR>
										<TR>
											<td style="WIDTH: 177px"><asp:label id="Label1" runat="server" CssClass="enterDataLabel" Width="100%">Подизпълнители:</asp:label></TD>
											<td>
												<TABLE id="Table8" cellSpacing="0" cellPadding="0" border="0">
													<TR>
														<td style="WIDTH: 179px"><asp:listbox id="lbUsers" ondblclick="ArrowButtonClick();" runat="server" CssClass="enterDataBox"
																Width="300px" SelectionMode="Multiple" Height="106px"></asp:listbox></TD>
														<td style="WIDTH: 34px">
															<TABLE id="Table9" border="0">
																<TR>
																	<td><INPUT class="ActionButton" id="inputToRight" style="WIDTH: 26px; HEIGHT: 18px" onclick="ArrowButtonClick();"
																			type="button" size="20" value=">" name="Button2"></TD>
																</TR>
																<TR>
																	<td><INPUT class="ActionButton" id="inputToLeft" style="WIDTH: 26px; HEIGHT: 18px" onclick="ArrowButtonClick();"
																			type="button" size="20" value="<" name="Button1"></TD>
																</TR>
															</TABLE>
														</TD>
														<td><asp:listbox id="lbSelectedUsers" ondblclick="ArrowButtonClick();" runat="server" CssClass="enterDataBox"
																Width="300px" SelectionMode="Multiple" Height="106px"></asp:listbox><INPUT id="hdnlbUsers" style="DISPLAY: none; WIDTH: 42px; HEIGHT: 19px" type="text" size="1"
																name="Text1" runat="server"><INPUT id="hdnlbSelectedUsers" style="DISPLAY: none; WIDTH: 34px; HEIGHT: 19px" type="text"
																size="1" name="Text2" runat="server"><INPUT id="hdn1lbSelectedUsers" style="DISPLAY: none; WIDTH: 34px; HEIGHT: 19px" type="text"
																size="1" name="Text2" runat="server"></TD>
													</TR>
												</TABLE>
											</TD>
										</TR>
										<TR>
											<td style="WIDTH: 177px"><asp:label id="Label2" runat="server" CssClass="enterDataLabel" Width="100%">Клиенти/ Строители /Проектни мениджъри/ Консултанти/Брокери:</asp:label></TD>
											<td>
												<TABLE id="Table8" cellSpacing="0" cellPadding="0" border="0">
													<TR>
														<td style="WIDTH: 179px"><asp:listbox id="lbClients" ondblclick="ArrowButtonClick();" runat="server" CssClass="enterDataBox"
																Width="300px" SelectionMode="Multiple" Height="106"></asp:listbox></TD>
														<td style="WIDTH: 34px">
															<TABLE id="Table9" border="0">
																<TR>
																	<td><INPUT class="ActionButton" id="inputUsersToRight1" style="WIDTH: 26px; HEIGHT: 18px" onclick="ArrowButtonClick();"
																			type="button" size="20" value=">" name="Button2"></TD>
																</TR>
																<TR>
																	<td><INPUT class="ActionButton" id="inputUsersToLeft1" style="WIDTH: 26px; HEIGHT: 18px" onclick="ArrowButtonClick();"
																			type="button" size="20" value="<" name="Button1"></TD>
																</TR>
															</TABLE>
														</TD>
														<td><asp:listbox id="lbSelectedClients" ondblclick="ArrowButtonClick();" runat="server" CssClass="enterDataBox"
																Width="300px" SelectionMode="Multiple" Height="106px"></asp:listbox><INPUT id="hdnlbClients" style="DISPLAY: none; WIDTH: 42px; HEIGHT: 19px" type="text" size="1"
																name="Text1" runat="server"><INPUT id="hdnlbSelectedClients" style="DISPLAY: none; WIDTH: 34px; HEIGHT: 19px" type="text"
																size="1" name="Text2" runat="server"><INPUT id="hdn1lbSelectedClients" style="DISPLAY: none; WIDTH: 34px; HEIGHT: 19px" type="text"
																size="1" name="Text2" runat="server"></TD>
													</TR>
												</TABLE>
											</TD>
										</TR>
										<TR>
											<td style="WIDTH: 177px" vAlign="top"><asp:label id="Label10" runat="server" CssClass="enterDataLabel" Width="100%">Протокола се изпраща още до:</asp:label></TD>
											<td><asp:textbox id="txtEmails" runat="server" CssClass="enterDataBox" Width="712px" MaxLength="1000"
													TextMode="MultiLine"></asp:textbox><br>
												<asp:label id="lb" ForeColor="Gray" Runat="server">Въведете имената и мейловете с разделител ';' между получателите и ',' между име и мейл: Пример: 'Иван Петров,mail@asa-bg.com;Иван Петров1,mail@asa-bg.com' или 'mail@asa.bg.com;mail@gmail.com' ако не знаете имената</asp:label></TD>
										</TR>
										<TR>
											<td style="WIDTH: 177px"><asp:label id="Label7" runat="server" CssClass="enterDataLabel" Width="100%">Други поканени:</asp:label></TD>
											<td><asp:textbox id="txtOtherPeople" runat="server" CssClass="enterDataBox" Width="712px" MaxLength="1000"></asp:textbox></TD>
										</TR>
										<TR>
											<td style="WIDTH: 177px"><asp:label id="Label9" runat="server" CssClass="enterDataLabel" Width="100%">Обект</asp:label></TD>
											<td><asp:textbox id="txtNotes" runat="server" CssClass="enterDataBox" Width="368px" Height="50px"
													MaxLength="10" TextMode="MultiLine"></asp:textbox></TD>
										</TR>
										<TR>
											<td style="WIDTH: 177px"><asp:label id="Label6" runat="server" CssClass="enterDataLabel" Width="100%">Сканирани бележки:</asp:label></TD>
											<td><asp:datalist id="dlDocs" runat="server" RepeatDirection="Horizontal">
													<ItemTemplate>
														<asp:HyperLink id="btnScan" runat="server" ToolTip="Сканиран документ" NavigateUrl='<%# GetURL((int)DataBinder.Eval(Container, "DataItem.MeetingDocument"))%>' Target=_blank>
															<img border="0" src="images/pdf.gif" /></asp:HyperLink>
														<br />
														<asp:ImageButton id="btnDel" runat="server" ToolTip="Изтрий" ImageUrl="images/delete.gif" Visible='<%# GetVis()%>' >
														</asp:ImageButton>
													</ItemTemplate>
												</asp:datalist><INPUT id="FileCtrl" type="file" name="File1" runat="server">
												<asp:button id="btnScanDocAdd" runat="server" Text="Добави" CssClass="ActionButton"></asp:button><asp:hyperlink id="btnScan" runat="server" ToolTip="Сканиран документ" Visible="False" Target="_blank">
													<img border="0" src="images/pdf.gif" /></asp:hyperlink></TD>
										</TR>
										<TR>
											<td style="WIDTH: 177px"><asp:label id="Label5" runat="server" CssClass="enterDataLabel" Width="100%">Потвърдена:</asp:label></TD>
											<td><asp:checkbox id="ckApproved" runat="server"></asp:checkbox></TD>
										</TR>
										<TR>
											<td style="WIDTH: 177px" valign="top"></TD>
											<td>
												<asp:checkbox id="ckAll" runat="server" Text="Всички теми" AutoPostBack="True" Visible="False"></asp:checkbox><BR>
												<asp:checkboxlist id="lst" CssClass="enterDataBox" Runat="server" RepeatDirection="Vertical" CellPadding="0"
													CellSpacing="0" Visible="False"></asp:checkboxlist></TD>
										</TR>
										<TR>
											<td colSpan="2"><IMG height="1" src="images/dot.gif" width="100%"></TD>
										</TR>
									</TABLE>
								</td>
							</tr>
							<TR>
								<td style="WIDTH: 4px"></TD>
								<td id="HotIssueDatas" runat="server">&nbsp;
									<asp:checkbox id="ckInclude" runat="server" Text="Включи задачи АСА към АСА" Visible="False" AutoPostBack="True"></asp:checkbox><br>
									<asp:datalist id="dlHotIssues" runat="server" Width="100%" RepeatDirection="Horizontal" RepeatColumns="1">
										<EditItemStyle VerticalAlign="Top"></EditItemStyle>
										<ItemStyle VerticalAlign="Top"></ItemStyle>
										<ItemTemplate>
											<TABLE cellSpacing="0" cellPadding="3" width="100%" border="0">
												<tr>
													<td><hr>
														<TABLE width="100%">
															<TR>
																<td style="WIDTH: 152px" valign="top">
																	<asp:label id="lbHotIssueCategory" runat="server" Width="100%" CssClass="enterDataLabel" Font-Bold="True">Тема:</asp:label></TD>
																<td valign="top">
																	<asp:label id="txtCategoryText" runat="server" text='<%# DataBinder.Eval(Container, "DataItem.HotIssueCategoryName") %>' CssClass="EnterDataBox">
																	</asp:label>
																	<asp:label id="lbCategoryText" runat="server" Visible="False" text='<%# DataBinder.Eval(Container, "DataItem.HotIssueCategoryID") %>'>
																	</asp:label>
																</TD>
																<td valign="top">
																	<asp:label id="lbAssigned" runat="server" Width="100%" CssClass="enterDataLabel" Font-Bold="True">Възложено на:</asp:label></TD>
																<td valign="top">
																	<asp:label id="txtAssignedText" runat="server" Visible="False" CssClass="EnterDataBox"></asp:label>
																	<asp:dropdownlist id="ddlAssigned" runat="server" CssClass="EnterDataBox" Width="250px"></asp:dropdownlist>&nbsp;<IMG id="Img1" alt="" src="images/required1.gif" width="16" runat="server">
																	<asp:label id=lbAssignedText runat="server" Visible="False" text='<%# DataBinder.Eval(Container, "DataItem.AssignedTo") %>'>
																	</asp:label>
																	<asp:label id=lbAssignedTypeText runat="server" Visible="False" text='<%# DataBinder.Eval(Container, "DataItem.AssignedToType") %>'>
																	</asp:label></TD>
															</TR>
															<TR>
																<td style="WIDTH: 152px" valign="top">
																	<asp:label id="lbHotIssueIme" runat="server" Width="100%" CssClass="enterDataLabel" Font-Bold="True">Задача:</asp:label></TD>
																<td valign="top">
																	<asp:Label id="txtHotIssueIme" runat="server" Width="250px" CssClass="enterDataBox" Text='<%# DataBinder.Eval(Container, "DataItem.HotIssueName") %>'>
																	</asp:Label></TD>
																<td valign="top">
																	<asp:label id="Label13" runat="server" Width="100%" CssClass="enterDataLabel">Възложено на(още):</asp:label></td>
																<td valign="top">
																	<asp:Label id="Label14" runat="server" Width="250px" CssClass="enterDataBox" Text='<%# DataBinder.Eval(Container, "DataItem.AssignedToMore") %>'>
																	</asp:Label></td>
															</TR>
															<TR>
																<td style="WIDTH: 152px" valign="top">
																	<asp:label id="lbDiscription" runat="server" Width="100%" CssClass="enterDataLabel">Описание:</asp:label></TD>
																<td valign="top">
																	<asp:label id="txtDiscription" runat="server" Width="250px" CssClass="enterDataBox" text='<%# DataBinder.Eval(Container, "DataItem.HotIssueDiscription") %>'>
																	</asp:label></TD>
																<td valign="top">
																	<asp:label id="lbDeadline" runat="server" Width="100%" CssClass="enterDataLabel" Font-Bold="True">Краен срок:</asp:label></TD>
																<td valign="top">
																	<asp:label id="txtDeadlineText" runat="server" Visible="False" CssClass="EnterDataBox"></asp:label>
																	<asp:textbox id=txtDeadline runat="server" CssClass="enterDataBox" Width="100px" text='<%# Asa.Timesheet.Data.Util.TimeHelper.FormatDateO(DataBinder.Eval(Container, "DataItem.HotIssueDeadline")) %>' MaxLength="10">
																	</asp:textbox>&nbsp; <IMG id="Img5" alt="Calendar" src="images/calendar.ico" width="18" align="absBottom"
																		runat="server"> <IMG id="imgRequired" alt="" src="images/required1.gif" width="16" runat="server">
																	<asp:label id=lbMainID runat="server" Visible="False" text='<%# DataBinder.Eval(Container, "DataItem.HotIssueIDMain") %>'>
																	</asp:label></TD>
															</TR>
															<TR>
																<td style="WIDTH: 152px" valign="top">
																	<asp:label id="lbProjectName" runat="server" Width="100%" CssClass="enterDataLabel" EnableViewState="False"
																		Visible="False" Font-Bold="True">Проект:</asp:label></TD>
																<td valign="top">
																	<asp:Label id="lbProjectNameStatic" runat="server" CssClass="EnterDataBox" Text='<%# DataBinder.Eval(Container, "DataItem.ProjectName") %>' Visible=False>
																	</asp:Label>
																	<asp:Label id="lbProjectNameID" runat="server" CssClass="EnterDataBox" Text='<%# DataBinder.Eval(Container, "DataItem.ProjectID") %>' Visible=False>
																	</asp:Label></TD>
																<td valign="top">
																	<asp:label id="lbStatus" runat="server" Width="100%" CssClass="enterDataLabel" Font-Bold="True">Статус:</asp:label></TD>
																<td>
																	<asp:label id="txtStatusText" runat="server" Visible="False" CssClass="EnterDataBox"></asp:label>
																	<asp:dropdownlist id="ddlStatus" runat="server" CssClass="EnterDataBox" Width="250px"></asp:dropdownlist>&nbsp;<IMG id="Img7" alt="" src="images/required1.gif" width="16" runat="server">
																	<asp:label id=lbStatusText runat="server" Visible="False" text='<%# DataBinder.Eval(Container, "DataItem.HotIssueStatusID") %>'>
																	</asp:label></TD>
															</TR>
															<TR>
																<td style="WIDTH: 152px" valign="top">
																	<asp:label id="lbComment" runat="server" Width="100%" CssClass="enterDataLabel">Коментар:</asp:label></TD>
																<td valign="top">
																	<asp:textbox id="txtComment" runat="server" Width="250px" CssClass="enterDataBox" MaxLength="10"
																		Rows="5" Height="96px" TextMode="MultiLine"></asp:textbox>
																</TD>
																<td valign="top">
																	<asp:label id="lbToClient" runat="server" Width="100%" CssClass="enterDataLabel">Към възложителя:</asp:label><br>
																	<br>
																	<asp:label id="Label12" runat="server" Width="100%" CssClass="enterDataLabel">Спрян е срока на изпълнение:</asp:label></TD>
																<td valign="top">
																	<asp:CheckBox id="cbToClient" enabled=false runat="server" CssClass="EnterDataBox" Checked='<%# (bool)DataBinder.Eval(Container, "DataItem.ToClient") %>'>
																	</asp:CheckBox>
																	<asp:Label id="lbToClientText" runat="server" CssClass="EnterDataBox" Text='<%# GetTextToClient((bool)DataBinder.Eval(Container, "DataItem.ToClient")) %>'>
																	</asp:Label><br>
																	<br>
																	<asp:CheckBox AutoPostBack=True id="ckStopped" runat="server" CssClass="EnterDataBox" enabled=false Checked='<%# GetExecution(DataBinder.Eval(Container, "DataItem.ExecutionStopped")) %>'>
																	</asp:CheckBox>
																	<asp:Label id="Label11" runat="server" CssClass="EnterDataBox" Text='<%# GetExecutionDate(DataBinder.Eval(Container, "DataItem.ExecutionStopped")) %>'>
																	</asp:Label></TD>
															</TR>
														</TABLE>
													</td>
												</tr>
												<TR>
													<td colSpan="2"><IMG height="1" src="images/dot.gif" width="100%"></TD>
												</TR>
											</TABLE>
										</ItemTemplate>
									</asp:datalist></td>
							</TR>
							<TR>
								<td style="HEIGHT: 5px" colSpan="2"></TD>
							</TR>
							<TR>
								<td colSpan="2"><IMG height="1" src="images/dot.gif" width="100%"></TD>
							</TR>
							<TR>
								<td colSpan="2" height="3"></td>
							</TR>
							<TR>
								<td style="WIDTH: 4px"></TD>
								<td>
									<TABLE id="Table5" style="WIDTH: 784px; HEIGHT: 36px" cellSpacing="0" cellPadding="0" border="0">
										<TR>
											<td style="WIDTH: 115px"><asp:button id="btnSave" runat="server" Text="Запиши" EnableViewState="False" CssClass="ActionButton"></asp:button><asp:button id="btnEdit" style="DISPLAY: none" runat="server" Text="Редактирай" EnableViewState="False"
													CssClass="ActionButton"></asp:button></TD>
											<td style="WIDTH: 438px"><asp:button id="btnCancel" runat="server" Text="Откажи" EnableViewState="False" CssClass="ActionButton"></asp:button>&nbsp;
												<asp:button id="btnNew1" runat="server" Text="Новa Задача" CssClass="ActionButton" Width="165px"
													DESIGNTIMEDRAGDROP="511"></asp:button></TD>
											<td><asp:button id="btnDelete" runat="server" Text="Изтрий" EnableViewState="False" CssClass="ActionButton"></asp:button></TD>
										</TR>
									</TABLE>
								</TD>
							</TR>
							<TR>
								<td height="3"></TD>
							</TR>
							<TR>
								<td colSpan="2"><IMG height="1" src="images/dot.gif" width="100%"></TD>
							</TR>
						</table>
						<asp:label id="lblError" runat="server" EnableViewState="False" CssClass="ErrorLabel" ForeColor="Red"></asp:label><BR>
						<asp:label id="lblInfo" runat="server" EnableViewState="False" CssClass="InfoLabel"></asp:label><BR>
					</td>
				</tr>
			</table>
			</TD></TR></TBODY></TABLE></form>
	</body>
</HTML>
