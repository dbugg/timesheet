using System;
using GrapeCity.ActiveReports;
using GrapeCity.ActiveReports.Controls;
using GrapeCity.ActiveReports.SectionReportModel;
using GrapeCity.ActiveReports.Document.Section;
using GrapeCity.ActiveReports.Document;

namespace Asa.Timesheet.WebPages.Reports
{
    public class ScheduleSubreportRpt : GrapeCity.ActiveReports.SectionReport
	{
		private float _StartValue = 0f;
		private float _CellWidth = 0.2f;
		private float _cellHighth = 0.2f;
		private int _mount;
		private int _year;
		private bool _numbers = false;
		private bool _mountShow = false;
		private int _monthCount;
		private DateTime _startDate;
		private DateTime _endDate;
		private string StyleMonth1 = "text-align: center; font-weight: bold; background-color:  LightGreen; font-size: 12pt; color: Black; ";
		private string StyleMonth2 = "text-align: center; font-weight: bold; background-color:  SkyBlue; font-size: 12pt; color: Black; ";
		private string StyleDefaultNumb = "background-color: white;  color: black;";
		private string StyleDefault = "background-color: white;  color: white;";
		private string StyleWeekendNumb = "background-color: #CCFFFF;  color: black;";
		private string StyleWeekend =" background-color: #CCFFFF; color: #CCFFFF;";
		private string StyleWorked = "background-color: red;  color: red;";
		private string StyleWorkedWeekend =" background-color: red; color: red;";
		
//		private string StyleDefault1 = " background-color: Green;font-size: 10pt;";
        public ScheduleSubreportRpt(int month, int year, bool numbers, bool mountShow, int monthCount, float cellHighth, DateTime startDate, DateTime endDate)
        {
            _mount = month;
            _year = year;
            _numbers = numbers;
            _mountShow = mountShow;
            _monthCount = monthCount;
            _cellHighth = cellHighth;
            _startDate = startDate;
            _endDate = endDate;
            InitializeComponent();
        }

        private void ScheduleSubreportRpt_ReportStart(object sender, System.EventArgs eArgs)
        {
            for (int j = 0; j < _monthCount; j++)
            {
                int DaysOfMount = DateTime.DaysInMonth(_year, _mount);
                if (_mountShow)
                {
                    Label lb = new Label();
                    lb.Location = new System.Drawing.PointF(_StartValue, 0);
                    lb.Size = new System.Drawing.SizeF(_CellWidth * DaysOfMount, _cellHighth);
                    lb.Text = MountShow(_mount);
                    for (int m = 1; m < 4; m++)
                        lb.Text = string.Concat("                      ", lb.Text);
                    if (j % 2 == 0)
                        lb.Style = StyleMonth1;
                    else
                        lb.Style = StyleMonth2;
                    TakeBorder(lb.Border);

                    Detail.Controls.Add(lb);

                }
                else
                {
                    for (int i = 1; i <= DaysOfMount; i++)
                    {

                        TextBox lb = new TextBox();

                        lb.Location = new System.Drawing.PointF(_StartValue + _CellWidth * (i - 1), 0);
                        lb.Size = new System.Drawing.SizeF(_CellWidth, _cellHighth);
                        if (_numbers)
                            lb.Text = i.ToString();

                        else lb.Text = ".";//string.Empty;
                        DateTime dtCur = new DateTime(_year, _mount, i);
                        if (dtCur.DayOfWeek == DayOfWeek.Saturday || dtCur.DayOfWeek == DayOfWeek.Sunday)
                        {
                            if (_numbers)
                                lb.Style = StyleWeekendNumb;
                            else
                            {
                                if (_startDate != DateTime.MinValue && _endDate != DateTime.MinValue && _startDate <= dtCur && _endDate >= dtCur)
                                    lb.Style = StyleWorkedWeekend;
                                else
                                    lb.Style = StyleWeekend;
                            }
                        }
                        else
                        {
                            if (_numbers)
                                lb.Style = StyleDefaultNumb;
                            else
                            {
                                if (_startDate != DateTime.MinValue && _endDate != DateTime.MinValue && _startDate <= dtCur && _endDate >= dtCur)
                                    lb.Style = StyleWorked;
                                else
                                    lb.Style = StyleDefault;
                            }
                        }
                        TakeBorder(lb.Border);
                        lb.MultiLine = false;

                        Detail.Controls.Add(lb);

                    }
                }
                if (_mount == 12)
                {
                    _mount = 1; _year++;
                }
                else
                    _mount++;
                _StartValue += _CellWidth * DaysOfMount;
            }
            {
                Label lb = new Label();
                lb.Location = new System.Drawing.PointF(_StartValue, 0);
                lb.Size = new System.Drawing.SizeF(0.2f, _cellHighth);
                lb.Text = "      1";
                lb.Style = StyleDefault;

                Detail.Controls.Add(lb);
            }

        }

		#region ActiveReports Designer generated code



        public void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ScheduleSubreportRpt));
            this.Detail = new GrapeCity.ActiveReports.SectionReportModel.Detail();
            this.PageHeader = new GrapeCity.ActiveReports.SectionReportModel.PageHeader();
            this.PageFooter = new GrapeCity.ActiveReports.SectionReportModel.PageFooter();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            // 
            // Detail
            // 
            this.Detail.ColumnSpacing = 0F;
            this.Detail.Height = 0.2F;
            this.Detail.Name = "Detail";
            // 
            // PageHeader
            // 
            this.PageHeader.Height = 0F;
            this.PageHeader.Name = "PageHeader";
            // 
            // PageFooter
            // 
            this.PageFooter.Height = 0F;
            this.PageFooter.Name = "PageFooter";
            // 
            // ActiveReport1
            // 
            this.MasterReport = false;
            this.PageSettings.PaperHeight = 11F;
            this.PageSettings.PaperWidth = 8.5F;
            this.PrintWidth = 81.5F;
            this.Sections.Add(this.PageHeader);
            this.Sections.Add(this.Detail);
            this.Sections.Add(this.PageFooter);
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule(resources.GetString("$this.StyleSheet"), "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: inherit; font-style: inherit; font-variant: inherit; font-weight: bo" +
                        "ld; font-size: 16pt; font-size-adjust: inherit; font-stretch: inherit", "Heading1", "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: Times New Roman; font-style: italic; font-variant: inherit; font-wei" +
                        "ght: bold; font-size: 14pt; font-size-adjust: inherit; font-stretch: inherit", "Heading2", "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: inherit; font-style: inherit; font-variant: inherit; font-weight: bo" +
                        "ld; font-size: 13pt; font-size-adjust: inherit; font-stretch: inherit", "Heading3", "Normal"));
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();

            // Attach Report Events
            this.ReportStart += new System.EventHandler(this.ScheduleSubreportRpt_ReportStart);
        }

		#endregion

        private string MountShow(int month)
        {
            switch (month)
            {
                case 1: return "January";
                case 2: return "February";
                case 3: return "March";
                case 4: return "April";
                case 5: return "May";
                case 6: return "June";
                case 7: return "July";
                case 8: return "August";
                case 9: return "September";
                case 10: return "October";
                case 11: return "November";
                case 12: return "December";
                default: return "";
            }
        }
        private void TakeBorder(GrapeCity.ActiveReports.Border b)
        {
            b.BottomColor = System.Drawing.Color.Black;
            b.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            b.LeftColor = System.Drawing.Color.Black;
            b.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            b.RightColor = System.Drawing.Color.Black;
            b.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            b.TopColor = System.Drawing.Color.Black;
            b.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
        }

        private PageHeader PageHeader;
        private Detail Detail;
        private PageFooter PageFooter;
	}
}
