using System;
using System.Data;
using System.Data.SqlClient; 
using System.Collections;
using Microsoft.ApplicationBlocks.Data;

namespace Asa.Timesheet.Data
{
	/// <summary>
	/// Summary description for MailingDAL.
	/// </summary>
	public class MailingDAL
	{
		//notes:this function return the highter mailing number
		public static int HighterMailingNumber()
		{
			string spName = "MailingHighterNumberProc";

			int hightest = (int)SqlHelper.ExecuteScalar(DBManager.GetConnectionString(), CommandType.StoredProcedure, spName);
			return hightest+1;
		}
		/// <summary>
		/// Loads a MailingData object from the database using the given MailingID
		/// </summary>
		/// <param name="MailingID">The primary key value</param>
		/// <returns>A MailingData object</returns>
		public static MailingData Load(int MailingID)
		{
			SqlParameter[] parameterValues = new SqlParameter[] { new SqlParameter("@MailingID", MailingID) };
			using (SqlDataReader reader = SqlHelper.ExecuteReader(CommonDAL.ConnectionString, CommandType.StoredProcedure, "MailingsSelProc", parameterValues))
			{
				if (reader.Read())
				{
					MailingData result = new MailingData();
					LoadFromReader(reader,result);
					return result;
				}
				else
					return null;
			}
		}

		/// <summary>
		/// Loads a MailingData object from the database using the given where clause
		/// </summary>
		/// <param name="whereClause">The filter expression for the query</param>
		/// <returns>A MailingData object</returns>
		public static MailingData Load(string whereClause)
		{
			SqlParameter[] parameterValues = new SqlParameter[] { new SqlParameter("@WhereClause", whereClause) };
			using (SqlDataReader reader = SqlHelper.ExecuteReader(CommonDAL.ConnectionString, CommandType.StoredProcedure, "MailingsListProc", parameterValues))
			{
				if (reader.Read())
				{
					MailingData result = new MailingData();
					LoadFromReader(reader,result);
					return result;
				}
				else
					return null;
			}
		}
		public static int Save( MailingData source)
		{
			if (source.MailingID == -1)
				return Insert(source);
			else
				return Update(source);	
		}

		public static void Delete( MailingData source)
		{
			Delete(source.MailingID);
		}
		/// <summary>
		/// Loads a collection of MailingData objects from the database.
		/// </summary>
		/// <returns>A collection containing all of the MailingData objects in the database.</returns>
		public static MailingsVector LoadCollection()
		{
			SqlParameter[] parms = new SqlParameter[] {};
			return LoadCollection("MailingsListProc", parms);
		}
	  
		/// <summary>
		/// Saves a collection of MailingData objects to the database.
		/// </summary>
		/// <returns>A collection containing all of the MailingData objects in the database.</returns>
		public static  void  SaveCollection(MailingsVector col,string spName, SqlParameter[] parms)
		{
			MailingsVector colOld = LoadCollection(spName,parms);
			UpdateCollection(colOld,col);
		}
		public static  void  SaveCollection(MailingsVector col)
		{
			MailingsVector colOld = LoadCollection();
			UpdateCollection(colOld,col);
		}
		public static  void  SaveCollection(MailingsVector col, string whereClause )
		{
			MailingsVector colOld = LoadCollection(whereClause);
			UpdateCollection(colOld,col);
		}
		/// <summary>
		/// Loads a collection of MailingData objects from the database.
		/// </summary>
		/// <returns>A collection containing all of the MailingData objects in the database.</returns>
		public static MailingsVector LoadCollection(string whereClause)
		{
			SqlParameter[] parms = new SqlParameter[] { new SqlParameter("@WhereClause", whereClause) };
			return LoadCollection("MailingsListProc", parms);
		}

		/// <summary>
		/// Loads a collection of MailingData objects from the database.
		/// </summary>
		/// <returns>A collection containing all of the MailingData objects in the database.</returns>
		public static MailingsVector LoadCollection(string spName, SqlParameter[] parms)
		{
			MailingsVector result = new MailingsVector();
			using (SqlDataReader reader = SqlHelper.ExecuteReader(CommonDAL.ConnectionString, CommandType.StoredProcedure, spName, parms))
			{
				while (reader.Read())
				{
					MailingData tmp = new MailingData();
					LoadFromReader(reader, tmp);
					result.Add(tmp);
				}
			}
			return result;
		}
		/// <summary>
		/// Deletes a MailingData object from the database.
		/// </summary>
		/// <param name="MailingID">The primary key value</param>
		public static void Delete(int MailingID)
		{
			SqlParameter[] parameterValues = new SqlParameter[] { new SqlParameter("@MailingID", MailingID) };
			SqlHelper.ExecuteNonQuery(CommonDAL.ConnectionString, CommandType.StoredProcedure, "MailingsDelProc", parameterValues);
		}

		public static void LoadFromReader(SqlDataReader reader, MailingData target)
		{
			if (reader != null && !reader.IsClosed)
			{
				//target.Key = reader.GetInt32(0);
				if (!reader.IsDBNull(0)) target.MailingID = reader.GetInt32(0);
				if (!reader.IsDBNull(1)) target.ProjectID = reader.GetInt32(1);
				if (!reader.IsDBNull(2)) target.MailingDate = reader.GetDateTime(2);
				if (!reader.IsDBNull(3)) target.Send = reader.GetBoolean(3);
				if (!reader.IsDBNull(4)) target.MailingTypes = reader.GetString(4);
				if (!reader.IsDBNull(5)) target.HasScan = reader.GetBoolean(5);
				if (!reader.IsDBNull(6)) target.MailingNumber = reader.GetInt32(6);
				if (!reader.IsDBNull(7)) target.Notes = reader.GetString(7);
				if (!reader.IsDBNull(8)) target.ClientID = reader.GetInt32(8);
				if (!reader.IsDBNull(9)) target.Ext = reader.GetString(9);
				if(reader.FieldCount>10)
				{
					if (!reader.IsDBNull(10)) target.ProjectName = reader.GetString(10);
					//if (!reader.IsDBNull(10)) target.MailingTypeName = reader.GetString(10);
						if (!reader.IsDBNull(11)) target.ClientName = reader.GetString(11);
					if (!reader.IsDBNull(12)) target.Person = reader.GetString(12);
				}
			}
		}
		
	  
		public static int Insert( MailingData source)
		{
			SqlParameter[] parameterValues = GetInsertParameterValues(source);
			SqlHelper.ExecuteNonQuery(CommonDAL.ConnectionString, CommandType.StoredProcedure, "MailingsInsProc", parameterValues);
			source.MailingID = (int) parameterValues[0].Value;
			return source.MailingID;
		}

		public static int Update( MailingData source)
		{
			SqlParameter[] parameterValues = GetUpdateParameterValues(source);
			SqlHelper.ExecuteNonQuery(CommonDAL.ConnectionString, CommandType.StoredProcedure,
				"MailingsUpdProc", parameterValues);
			return source.MailingID;
		}
		private static void UpdateCollection(MailingsVector colOld,MailingsVector col)
		{
			// Delete all old items
			foreach( MailingData itemOld in colOld)
			{
				if(!ExistsKey(itemOld.MailingID, col))
					Delete(itemOld.MailingID);
			}
			foreach( MailingData item in col)
			{
				if(item.MailingID <= -1)
					Insert(item);
				else
					Update(item);
			}
		}
		private static bool ExistsKey(int Key, MailingsVector col)
		{
			foreach( MailingData item in col)
				if(item.MailingID==Key)
					return true;
			return false;
		}

		private static SqlParameter GetSqlParameter(string name, ParameterDirection direction, object value)
		{
			SqlParameter p = new SqlParameter(name, value);
			p.Direction = direction;
			return p;
		}

		private static SqlParameter[] GetInsertParameterValues( MailingData source)
		{
			SqlParameter[] prams = new SqlParameter[11];
			prams[0] = GetSqlParameter("@MailingID", ParameterDirection.Output, source.MailingID);
			if(source.ProjectID>0)
				prams[1] = GetSqlParameter("@ProjectID", ParameterDirection.Input, source.ProjectID);
			else
				prams[1] = GetSqlParameter("@ProjectID", ParameterDirection.Input, DBNull.Value);
			prams[2] = GetSqlParameter("@MailingDate", ParameterDirection.Input, source.MailingDate);
			prams[3] = GetSqlParameter("@Send", ParameterDirection.Input, source.Send );
			prams[4] = GetSqlParameter("@MailingTypes", ParameterDirection.Input, source.MailingTypes);
			prams[5] = GetSqlParameter("@HasScan", ParameterDirection.Input, source.HasScan );
			if(source.MailingNumber>0)
				prams[6] = GetSqlParameter("@MailingNumber", ParameterDirection.Input, source.MailingNumber);
			else
				prams[6] = GetSqlParameter("@MailingNumber", ParameterDirection.Input, DBNull.Value);
			prams[7] = GetSqlParameter("@Notes", ParameterDirection.Input, source.Notes );
			if(source.ClientID>0)
				prams[8] = GetSqlParameter("@ClientID", ParameterDirection.Input, source.ClientID);
			else
				prams[8] = GetSqlParameter("@ClientID", ParameterDirection.Input, DBNull.Value);
			if(source.Ext.Length>0)
				prams[9] = GetSqlParameter("@ext", ParameterDirection.Input, source.Ext);
			else
				prams[9] = GetSqlParameter("@ext", ParameterDirection.Input, DBNull.Value);
			prams[10] = GetSqlParameter("@Person", ParameterDirection.Input, source.Person );
			return prams;
		}

		private static SqlParameter[] GetUpdateParameterValues( MailingData source)
		{
			SqlParameter sqlParProjectID;
			SqlParameter sqlParClientID;
			if(source.ProjectID>0)
				sqlParProjectID = new SqlParameter("@ProjectID", source.ProjectID);
			else
				sqlParProjectID = new SqlParameter("@ProjectID", DBNull.Value);
			if(source.ClientID>0)
				sqlParClientID = new SqlParameter("@ClientID", source.ClientID);
			else
				sqlParClientID = new SqlParameter("@ClientID", DBNull.Value);
			return new SqlParameter[] {
										  new SqlParameter("@MailingID", source.MailingID),
										  
										  sqlParProjectID,
										  new SqlParameter("@MailingDate", source.MailingDate),
										  new SqlParameter("@Send", source.Send),
										  new SqlParameter("@MailingTypes", source.MailingTypes),
										  new SqlParameter("@HasScan", source.HasScan),
										  new SqlParameter("@MailingNumber", source.MailingNumber),
										  new SqlParameter("@Notes", source.Notes),
										  sqlParClientID,
										new SqlParameter("@ext",source.Ext),
				new SqlParameter("@Person",source.Person),
			};
		}
	}
}
