using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using log4net;
using Asa.Timesheet.Data;
using Asa.Timesheet.Data.Entities;

namespace Asa.Timesheet.WebPages
{
	/// <summary>
	/// Summary description for EditCategory.
	/// </summary>
	public class EditCategory : TimesheetPageBase
	{
		protected System.Web.UI.WebControls.PlaceHolder menuHolder;
		protected System.Web.UI.WebControls.Label lblName;
		protected System.Web.UI.WebControls.TextBox txtName;
		protected System.Web.UI.WebControls.Button btnSave;
		protected System.Web.UI.WebControls.Label lblInfo;
		protected System.Web.UI.WebControls.LinkButton linkBack;
		protected System.Web.UI.WebControls.ImageButton imgBack;
		protected System.Web.UI.WebControls.Label Label1;
		protected System.Web.UI.WebControls.TextBox txtNameEN;
		protected System.Web.UI.WebControls.Label lblError;

        private void Page_Load(object sender, System.EventArgs e)
        {
            if (!(LoggedUser.HasRole)) ErrorRedirect(Resource.ResourceManager["noRightsAlert"]);
            UIHelpers.CreateMenu(menuHolder, LoggedUser);
            if (!IsPostBack)
            {
                int nID = UIHelpers.GetIDParam();

                HotIssueCategoryData cd = HotIssueCategoryDAL.Load(nID);
                if (cd != null)
                {
                    txtName.Text = cd.HotIssueCategoryName;
                    txtNameEN.Text = cd.HotIssueCategoryNameEN;
                }
            }
        }

		#region Web Form Designer generated code
        override protected void OnInit(EventArgs e)
        {
            //
            // CODEGEN: This call is required by the ASP.NET Web Form Designer.
            //
            InitializeComponent();
            base.OnInit(e);
        }
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
        private void InitializeComponent()
        {
            this.linkBack.Click += new System.EventHandler(this.linkBack_Click);
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            this.Load += new System.EventHandler(this.Page_Load);

        }
		#endregion

        private void linkBack_Click(object sender, System.EventArgs e)
        {
            Response.Redirect("HotIssues.aspx");
        }

        private void btnSave_Click(object sender, System.EventArgs e)
        {
            int nID = UIHelpers.GetIDParam();
            HotIssueCategoryData cd = HotIssueCategoryDAL.Load(nID);
            if (cd != null)
            {
                cd.HotIssueCategoryName = txtName.Text;
                cd.HotIssueCategoryNameEN = txtNameEN.Text;
                HotIssueCategoryDAL.Update(cd);
                Response.Redirect("HotIssues.aspx");


            }
        }
	}
}
