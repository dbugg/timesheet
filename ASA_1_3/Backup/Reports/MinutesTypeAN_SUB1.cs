using System;
using System.Data;
using Asa.Timesheet.Data;
using GrapeCity.ActiveReports;
using GrapeCity.ActiveReports.Controls;
using GrapeCity.ActiveReports.SectionReportModel;
using GrapeCity.ActiveReports.Document.Section;
using GrapeCity.ActiveReports.Document;

namespace Asa.Timesheet.WebPages.Reports
{
    public class MinutesTypeAN_SUB1 : GrapeCity.ActiveReports.SectionReport
	{
		//created by Ivailo date: 05.12.2007
		//notes:Subreport of MinutesTypeAN_main

		//notes:in _sum is counting partial sum
		// in _sumAll is counting whole sum
		double _sum = 0;
		double _sumAll = 0;
        public MinutesTypeAN_SUB1(DataView dv)
        {
            this.DataSource = dv;
            InitializeComponent();
        }

        private void Detail_Format(object sender, System.EventArgs eArgs)
        {
            //notes:set Detail(row) visible true, so hide only empty rows
            this.Detail.Visible = true;
            if (this.txtDate.Text.Length == 0)
            {
                //notes: check if the row is empty
                // if it's empty set visible to false
                if (this.txtUser.Text.Length != 0)
                {
                    this.txtUser.Text = string.Concat(Resource.ResourceManager["reports_TotalFor"], this.txtUser.Text);
                    this.txtStartHour.Text = "";
                    this.txtEndHour.Text = "";
                    this.txtAdminNotes.Text = "";
                    //notes:set _sum and make it zero
                    this.txtProtocol.Text = string.Concat(_sum, " ", Resource.ResourceManager["exportHour"]);
                    _sum = 0;
                }
                else
                {
                    //notes: hide empty row
                    this.Detail.Visible = false;
                }
            }
            else
            {
                //notes:counting hours
                double stHour = UIHelpers.ToDouble(this.txtStartHour.Text);
                double eHour = UIHelpers.ToDouble(this.txtEndHour.Text);
                _sum += eHour - stHour;
                _sumAll += eHour - stHour;
            }
        }

        private void ReportFooter_Format(object sender, System.EventArgs eArgs)
        {
            this.txtAllText.Text = Resource.ResourceManager["reports_Total"];
            this.txtAllHours.Text = string.Concat(_sumAll, " ", Resource.ResourceManager["exportHour"]);
        }

	
		#region ActiveReports Designer generated code







































        public void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MinutesTypeAN_SUB1));
            this.Detail = new GrapeCity.ActiveReports.SectionReportModel.Detail();
            this.ReportHeader = new GrapeCity.ActiveReports.SectionReportModel.ReportHeader();
            this.ReportFooter = new GrapeCity.ActiveReports.SectionReportModel.ReportFooter();
            this.Label1 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label2 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label3 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label4 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label5 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label6 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Line1 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.Line2 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.Line3 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.Line4 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.Line5 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.Line6 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.Line7 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.Line8 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.Line9 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.txtUser = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtDate = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtStartHour = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtEndHour = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtProtocol = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtAdminNotes = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.Line10 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.Line11 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.Line12 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.Line13 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.Line14 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.Line15 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.Line16 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.Line17 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.txtAllText = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtAllHours = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.Line18 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.Line19 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.Line20 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.Line21 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.Line22 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            ((System.ComponentModel.ISupportInitialize)(this.Label1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtUser)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtStartHour)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtEndHour)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtProtocol)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAdminNotes)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAllText)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAllHours)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            // 
            // Detail
            // 
            this.Detail.ColumnSpacing = 0F;
            this.Detail.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
						this.txtUser,
						this.txtDate,
						this.txtStartHour,
						this.txtEndHour,
						this.txtProtocol,
						this.txtAdminNotes,
						this.Line10,
						this.Line11,
						this.Line12,
						this.Line13,
						this.Line14,
						this.Line15,
						this.Line16,
						this.Line17});
            this.Detail.Height = 0.2F;
            this.Detail.Name = "Detail";
            // 
            // ReportHeader
            // 
            this.ReportHeader.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
						this.Label1,
						this.Label2,
						this.Label3,
						this.Label4,
						this.Label5,
						this.Label6,
						this.Line1,
						this.Line2,
						this.Line3,
						this.Line4,
						this.Line5,
						this.Line6,
						this.Line7,
						this.Line8,
						this.Line9});
            this.ReportHeader.Height = 0.2F;
            this.ReportHeader.Name = "ReportHeader";
            // 
            // ReportFooter
            // 
            this.ReportFooter.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
						this.txtAllText,
						this.txtAllHours,
						this.Line18,
						this.Line19,
						this.Line20,
						this.Line21,
						this.Line22});
            this.ReportFooter.Height = 0.2F;
            this.ReportFooter.Name = "ReportFooter";
            // 
            // Label1
            // 
            this.Label1.Height = 0.2F;
            this.Label1.HyperLink = null;
            this.Label1.Left = 0F;
            this.Label1.Name = "Label1";
            this.Label1.Style = "font-weight: bold";
            this.Label1.Text = "????????";
            this.Label1.Top = 0F;
            this.Label1.Width = 2F;
            // 
            // Label2
            // 
            this.Label2.Height = 0.2F;
            this.Label2.HyperLink = null;
            this.Label2.Left = 2F;
            this.Label2.Name = "Label2";
            this.Label2.Style = "font-weight: bold";
            this.Label2.Text = "????";
            this.Label2.Top = 0F;
            this.Label2.Width = 1F;
            // 
            // Label3
            // 
            this.Label3.Height = 0.2F;
            this.Label3.HyperLink = null;
            this.Label3.Left = 3F;
            this.Label3.Name = "Label3";
            this.Label3.Style = "font-weight: bold";
            this.Label3.Text = "???.???";
            this.Label3.Top = 0F;
            this.Label3.Width = 1F;
            // 
            // Label4
            // 
            this.Label4.Height = 0.2F;
            this.Label4.HyperLink = null;
            this.Label4.Left = 4F;
            this.Label4.Name = "Label4";
            this.Label4.Style = "font-weight: bold";
            this.Label4.Text = "??.???";
            this.Label4.Top = 0F;
            this.Label4.Width = 1F;
            // 
            // Label5
            // 
            this.Label5.Height = 0.2F;
            this.Label5.HyperLink = null;
            this.Label5.Left = 5F;
            this.Label5.Name = "Label5";
            this.Label5.Style = "font-weight: bold";
            this.Label5.Text = "????????";
            this.Label5.Top = 0F;
            this.Label5.Width = 2F;
            // 
            // Label6
            // 
            this.Label6.Height = 0.2F;
            this.Label6.HyperLink = null;
            this.Label6.Left = 7F;
            this.Label6.Name = "Label6";
            this.Label6.Style = "font-weight: bold";
            this.Label6.Text = "?????.???????";
            this.Label6.Top = 0F;
            this.Label6.Width = 2F;
            // 
            // Line1
            // 
            this.Line1.Height = 0F;
            this.Line1.Left = 0F;
            this.Line1.LineWeight = 1F;
            this.Line1.Name = "Line1";
            this.Line1.Top = 0F;
            this.Line1.Width = 9F;
            this.Line1.X1 = 0F;
            this.Line1.X2 = 9F;
            this.Line1.Y1 = 0F;
            this.Line1.Y2 = 0F;
            // 
            // Line2
            // 
            this.Line2.Height = 0F;
            this.Line2.Left = 0F;
            this.Line2.LineWeight = 1F;
            this.Line2.Name = "Line2";
            this.Line2.Top = 0.2F;
            this.Line2.Width = 9F;
            this.Line2.X1 = 0F;
            this.Line2.X2 = 9F;
            this.Line2.Y1 = 0.2F;
            this.Line2.Y2 = 0.2F;
            // 
            // Line3
            // 
            this.Line3.Height = 0.2F;
            this.Line3.Left = 0F;
            this.Line3.LineWeight = 1F;
            this.Line3.Name = "Line3";
            this.Line3.Top = 0F;
            this.Line3.Width = 0F;
            this.Line3.X1 = 0F;
            this.Line3.X2 = 0F;
            this.Line3.Y1 = 0F;
            this.Line3.Y2 = 0.2F;
            // 
            // Line4
            // 
            this.Line4.Height = 0.2F;
            this.Line4.Left = 2F;
            this.Line4.LineWeight = 1F;
            this.Line4.Name = "Line4";
            this.Line4.Top = 0F;
            this.Line4.Width = 0F;
            this.Line4.X1 = 2F;
            this.Line4.X2 = 2F;
            this.Line4.Y1 = 0F;
            this.Line4.Y2 = 0.2F;
            // 
            // Line5
            // 
            this.Line5.Height = 0.2F;
            this.Line5.Left = 3F;
            this.Line5.LineWeight = 1F;
            this.Line5.Name = "Line5";
            this.Line5.Top = 0F;
            this.Line5.Width = 0F;
            this.Line5.X1 = 3F;
            this.Line5.X2 = 3F;
            this.Line5.Y1 = 0F;
            this.Line5.Y2 = 0.2F;
            // 
            // Line6
            // 
            this.Line6.Height = 0.2F;
            this.Line6.Left = 4F;
            this.Line6.LineWeight = 1F;
            this.Line6.Name = "Line6";
            this.Line6.Top = 0F;
            this.Line6.Width = 0F;
            this.Line6.X1 = 4F;
            this.Line6.X2 = 4F;
            this.Line6.Y1 = 0F;
            this.Line6.Y2 = 0.2F;
            // 
            // Line7
            // 
            this.Line7.Height = 0.2F;
            this.Line7.Left = 5F;
            this.Line7.LineWeight = 1F;
            this.Line7.Name = "Line7";
            this.Line7.Top = 0F;
            this.Line7.Width = 0F;
            this.Line7.X1 = 5F;
            this.Line7.X2 = 5F;
            this.Line7.Y1 = 0F;
            this.Line7.Y2 = 0.2F;
            // 
            // Line8
            // 
            this.Line8.Height = 0.2F;
            this.Line8.Left = 7F;
            this.Line8.LineWeight = 1F;
            this.Line8.Name = "Line8";
            this.Line8.Top = 0F;
            this.Line8.Width = 0F;
            this.Line8.X1 = 7F;
            this.Line8.X2 = 7F;
            this.Line8.Y1 = 0F;
            this.Line8.Y2 = 0.2F;
            // 
            // Line9
            // 
            this.Line9.Height = 0.2F;
            this.Line9.Left = 9F;
            this.Line9.LineWeight = 1F;
            this.Line9.Name = "Line9";
            this.Line9.Top = 0F;
            this.Line9.Width = 0F;
            this.Line9.X1 = 9F;
            this.Line9.X2 = 9F;
            this.Line9.Y1 = 0F;
            this.Line9.Y2 = 0.2F;
            // 
            // txtUser
            // 
            this.txtUser.DataField = "UserName";
            this.txtUser.Height = 0.2F;
            this.txtUser.Left = 0F;
            this.txtUser.Name = "txtUser";
            this.txtUser.Top = 0F;
            this.txtUser.Width = 2F;
            // 
            // txtDate
            // 
            this.txtDate.DataField = "WorkDate";
            this.txtDate.Height = 0.2F;
            this.txtDate.Left = 2F;
            this.txtDate.Name = "txtDate";
            this.txtDate.OutputFormat = resources.GetString("txtDate.OutputFormat");
            this.txtDate.Style = "text-align: center";
            this.txtDate.Top = 0F;
            this.txtDate.Width = 1F;
            // 
            // txtStartHour
            // 
            this.txtStartHour.DataField = "StartHour";
            this.txtStartHour.Height = 0.2F;
            this.txtStartHour.Left = 3F;
            this.txtStartHour.Name = "txtStartHour";
            this.txtStartHour.Style = "text-align: center";
            this.txtStartHour.Top = 0F;
            this.txtStartHour.Width = 1F;
            // 
            // txtEndHour
            // 
            this.txtEndHour.DataField = "EndHour";
            this.txtEndHour.Height = 0.2F;
            this.txtEndHour.Left = 4F;
            this.txtEndHour.Name = "txtEndHour";
            this.txtEndHour.Style = "text-align: center";
            this.txtEndHour.Top = 0F;
            this.txtEndHour.Width = 1F;
            // 
            // txtProtocol
            // 
            this.txtProtocol.DataField = "Minutes";
            this.txtProtocol.Height = 0.2F;
            this.txtProtocol.Left = 5F;
            this.txtProtocol.Name = "txtProtocol";
            this.txtProtocol.Style = "text-align: center";
            this.txtProtocol.Top = 0F;
            this.txtProtocol.Width = 2F;
            // 
            // txtAdminNotes
            // 
            this.txtAdminNotes.DataField = "AdminMinutes";
            this.txtAdminNotes.Height = 0.2F;
            this.txtAdminNotes.Left = 7F;
            this.txtAdminNotes.Name = "txtAdminNotes";
            this.txtAdminNotes.Style = "text-align: center";
            this.txtAdminNotes.Top = 0F;
            this.txtAdminNotes.Width = 2F;
            // 
            // Line10
            // 
            this.Line10.Height = 0F;
            this.Line10.Left = 0F;
            this.Line10.LineWeight = 1F;
            this.Line10.Name = "Line10";
            this.Line10.Top = 0.2F;
            this.Line10.Width = 9F;
            this.Line10.X1 = 0F;
            this.Line10.X2 = 9F;
            this.Line10.Y1 = 0.2F;
            this.Line10.Y2 = 0.2F;
            // 
            // Line11
            // 
            this.Line11.AnchorBottom = true;
            this.Line11.Height = 0.2F;
            this.Line11.Left = 0F;
            this.Line11.LineWeight = 1F;
            this.Line11.Name = "Line11";
            this.Line11.Top = 0F;
            this.Line11.Width = 0F;
            this.Line11.X1 = 0F;
            this.Line11.X2 = 0F;
            this.Line11.Y1 = 0F;
            this.Line11.Y2 = 0.2F;
            // 
            // Line12
            // 
            this.Line12.AnchorBottom = true;
            this.Line12.Height = 0.2F;
            this.Line12.Left = 2F;
            this.Line12.LineWeight = 1F;
            this.Line12.Name = "Line12";
            this.Line12.Top = 0F;
            this.Line12.Width = 0F;
            this.Line12.X1 = 2F;
            this.Line12.X2 = 2F;
            this.Line12.Y1 = 0F;
            this.Line12.Y2 = 0.2F;
            // 
            // Line13
            // 
            this.Line13.AnchorBottom = true;
            this.Line13.Height = 0.2F;
            this.Line13.Left = 3F;
            this.Line13.LineWeight = 1F;
            this.Line13.Name = "Line13";
            this.Line13.Top = 0F;
            this.Line13.Width = 0F;
            this.Line13.X1 = 3F;
            this.Line13.X2 = 3F;
            this.Line13.Y1 = 0F;
            this.Line13.Y2 = 0.2F;
            // 
            // Line14
            // 
            this.Line14.AnchorBottom = true;
            this.Line14.Height = 0.2F;
            this.Line14.Left = 4F;
            this.Line14.LineWeight = 1F;
            this.Line14.Name = "Line14";
            this.Line14.Top = 0F;
            this.Line14.Width = 0F;
            this.Line14.X1 = 4F;
            this.Line14.X2 = 4F;
            this.Line14.Y1 = 0F;
            this.Line14.Y2 = 0.2F;
            // 
            // Line15
            // 
            this.Line15.AnchorBottom = true;
            this.Line15.Height = 0.2F;
            this.Line15.Left = 5F;
            this.Line15.LineWeight = 1F;
            this.Line15.Name = "Line15";
            this.Line15.Top = 0F;
            this.Line15.Width = 0F;
            this.Line15.X1 = 5F;
            this.Line15.X2 = 5F;
            this.Line15.Y1 = 0F;
            this.Line15.Y2 = 0.2F;
            // 
            // Line16
            // 
            this.Line16.AnchorBottom = true;
            this.Line16.Height = 0.2F;
            this.Line16.Left = 7F;
            this.Line16.LineWeight = 1F;
            this.Line16.Name = "Line16";
            this.Line16.Top = 0F;
            this.Line16.Width = 0F;
            this.Line16.X1 = 7F;
            this.Line16.X2 = 7F;
            this.Line16.Y1 = 0F;
            this.Line16.Y2 = 0.2F;
            // 
            // Line17
            // 
            this.Line17.AnchorBottom = true;
            this.Line17.Height = 0.2F;
            this.Line17.Left = 9F;
            this.Line17.LineWeight = 1F;
            this.Line17.Name = "Line17";
            this.Line17.Top = 0F;
            this.Line17.Width = 0F;
            this.Line17.X1 = 9F;
            this.Line17.X2 = 9F;
            this.Line17.Y1 = 0F;
            this.Line17.Y2 = 0.2F;
            // 
            // txtAllText
            // 
            this.txtAllText.DataField = "UserName";
            this.txtAllText.Height = 0.2F;
            this.txtAllText.Left = 0F;
            this.txtAllText.Name = "txtAllText";
            this.txtAllText.Style = "font-weight: bold";
            this.txtAllText.Top = 0F;
            this.txtAllText.Width = 2F;
            // 
            // txtAllHours
            // 
            this.txtAllHours.DataField = "Minutes";
            this.txtAllHours.Height = 0.2F;
            this.txtAllHours.Left = 5F;
            this.txtAllHours.Name = "txtAllHours";
            this.txtAllHours.Style = "font-weight: bold; text-align: center";
            this.txtAllHours.Top = 0F;
            this.txtAllHours.Width = 2F;
            // 
            // Line18
            // 
            this.Line18.Height = 0F;
            this.Line18.Left = 0F;
            this.Line18.LineWeight = 1F;
            this.Line18.Name = "Line18";
            this.Line18.Top = 0.2F;
            this.Line18.Width = 7F;
            this.Line18.X1 = 0F;
            this.Line18.X2 = 7F;
            this.Line18.Y1 = 0.2F;
            this.Line18.Y2 = 0.2F;
            // 
            // Line19
            // 
            this.Line19.Height = 0.2F;
            this.Line19.Left = 0F;
            this.Line19.LineWeight = 1F;
            this.Line19.Name = "Line19";
            this.Line19.Top = 0F;
            this.Line19.Width = 0F;
            this.Line19.X1 = 0F;
            this.Line19.X2 = 0F;
            this.Line19.Y1 = 0F;
            this.Line19.Y2 = 0.2F;
            // 
            // Line20
            // 
            this.Line20.Height = 0.2F;
            this.Line20.Left = 2F;
            this.Line20.LineWeight = 1F;
            this.Line20.Name = "Line20";
            this.Line20.Top = 0F;
            this.Line20.Width = 0F;
            this.Line20.X1 = 2F;
            this.Line20.X2 = 2F;
            this.Line20.Y1 = 0F;
            this.Line20.Y2 = 0.2F;
            // 
            // Line21
            // 
            this.Line21.Height = 0.2F;
            this.Line21.Left = 5F;
            this.Line21.LineWeight = 1F;
            this.Line21.Name = "Line21";
            this.Line21.Top = 0F;
            this.Line21.Width = 0F;
            this.Line21.X1 = 5F;
            this.Line21.X2 = 5F;
            this.Line21.Y1 = 0F;
            this.Line21.Y2 = 0.2F;
            // 
            // Line22
            // 
            this.Line22.Height = 0.2F;
            this.Line22.Left = 7F;
            this.Line22.LineWeight = 1F;
            this.Line22.Name = "Line22";
            this.Line22.Top = 0F;
            this.Line22.Width = 0F;
            this.Line22.X1 = 7F;
            this.Line22.X2 = 7F;
            this.Line22.Y1 = 0F;
            this.Line22.Y2 = 0.2F;
            // 
            // ActiveReport1
            // 
            this.MasterReport = false;
            this.PageSettings.PaperHeight = 11F;
            this.PageSettings.PaperWidth = 8.5F;
            this.PrintWidth = 9F;
            this.Sections.Add(this.ReportHeader);
            this.Sections.Add(this.Detail);
            this.Sections.Add(this.ReportFooter);
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule(resources.GetString("$this.StyleSheet"), "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: inherit; font-style: inherit; font-variant: inherit; font-weight: bo" +
                        "ld; font-size: 16pt; font-size-adjust: inherit; font-stretch: inherit", "Heading1", "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: Times New Roman; font-style: italic; font-variant: inherit; font-wei" +
                        "ght: bold; font-size: 14pt; font-size-adjust: inherit; font-stretch: inherit", "Heading2", "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: inherit; font-style: inherit; font-variant: inherit; font-weight: bo" +
                        "ld; font-size: 13pt; font-size-adjust: inherit; font-stretch: inherit", "Heading3", "Normal"));
            ((System.ComponentModel.ISupportInitialize)(this.Label1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtUser)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtStartHour)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtEndHour)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtProtocol)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAdminNotes)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAllText)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAllHours)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();

            // Attach Report Events
            this.Detail.Format += new System.EventHandler(this.Detail_Format);
            this.ReportFooter.Format += new System.EventHandler(this.ReportFooter_Format);
        }

		#endregion

        private ReportHeader ReportHeader;
        private Label Label1;
        private Label Label2;
        private Label Label3;
        private Label Label4;
        private Label Label5;
        private Label Label6;
        private Line Line1;
        private Line Line2;
        private Line Line3;
        private Line Line4;
        private Line Line5;
        private Line Line6;
        private Line Line7;
        private Line Line8;
        private Line Line9;
        private Detail Detail;
        private TextBox txtUser;
        private TextBox txtDate;
        private TextBox txtStartHour;
        private TextBox txtEndHour;
        private TextBox txtProtocol;
        private TextBox txtAdminNotes;
        private Line Line10;
        private Line Line11;
        private Line Line12;
        private Line Line13;
        private Line Line14;
        private Line Line15;
        private Line Line16;
        private Line Line17;
        private ReportFooter ReportFooter;
        private TextBox txtAllText;
        private TextBox txtAllHours;
        private Line Line18;
        private Line Line19;
        private Line Line20;
        private Line Line21;
        private Line Line22;
	}
}
