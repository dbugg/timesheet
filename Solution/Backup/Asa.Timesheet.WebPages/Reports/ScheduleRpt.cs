using System;
using System.Data;
using Asa.Timesheet.Data.Util;
using Asa.Timesheet.Data;
using GrapeCity.ActiveReports;
using GrapeCity.ActiveReports.Controls;
using GrapeCity.ActiveReports.SectionReportModel;
using GrapeCity.ActiveReports.Document.Section;
using GrapeCity.ActiveReports.Document;

namespace Asa.Timesheet.WebPages.Reports
{
    public class ScheduleRpt : GrapeCity.ActiveReports.SectionReport
	{
		private int _Month = DateTime.Now.Month;
		private int _year = DateTime.Now.Year;
		private bool _hasID = false;
		private int _mountCount = 0;
		private bool _weekly;
		private string ColorLabel1Project = "text-align: left; font-weight: bold; background-color: #FFC080; font-size: 11pt; color: Black;  ";
		private string ColorLabel2Project = "font-weight: bold;background-color: #FFC080; font-size: 11pt; color: Black; text-align: right; ";
		private string ColorDate = "font-weight: bold;background-color: #FFC080; font-size: 11pt; color: #FFC080; text-align: right; ";
		private string ColorDateDefault = "text-align: left; font-weight: bold; background-color: White;font-size: 11pt; color: White; ";
		private string ColorLabel1Default1 = "text-align: left; font-weight: bold; background-color: White;font-size: 11pt; color: Black; ";
		private string ColorLabel2Default1 = "text-align: left; font-weight: bold; background-color: White;font-size: 11pt; color: Red; text-align: left;";
		private string ColorLabel2Default2 = "text-align: left; font-weight: bold; background-color: White;font-size: 10pt; color: #993366; text-align: left;";
		//private string Traine = "";
		private string Team = Resource.ResourceManager["ProjectTeam"];
		private string ProjectManager = Resource.ResourceManager["ProjectManager"];

        public ScheduleRpt(DataView dv, int MonthCount, bool Weekly)
        {
            _mountCount = MonthCount;
            _weekly = Weekly;
            this.DataSource = dv;
            InitializeComponent();
        }




        private void Detail_Format(object sender, System.EventArgs eArgs)
        {


            this.Text1.Style = ColorLabel1Default1;
            this.Text2.Style = ColorLabel2Default1;
            this.txtStartDate.Style = ColorDateDefault;
            this.txtEndDate.Style = ColorDateDefault;
            this.txtNotes.Style = ColorDateDefault;

            TakeBorder(this.Text1.Border);
            TakeBorder(this.Text2.Border);
            TakeBorder(this.txtStartDate.Border);
            TakeBorder(this.txtEndDate.Border);
            TakeBorder(this.txtNotes.Border);
            if (_hasID)
            {
                ResetSubreports(false, false, this.txtStartDate.Text, this.txtEndDate.Text);
                _hasID = false;
                this.Text2.Style = ColorLabel2Default1;
            }
            else
            {
                if (this.lbID != null)
                    if (this.lbID.Text != "-1")
                    {
                        this.Text1.Style = ColorLabel1Project;
                        this.Text2.Style = ColorLabel2Project;
                        this.txtStartDate.Style = ColorDate;
                        this.txtEndDate.Style = ColorDate;
                        this.txtNotes.Style = ColorDate;
                        ResetSubreports(true, false, this.txtStartDate.Text, this.txtEndDate.Text);
                        _hasID = true;
                    }
                    else
                    {
                        ResetSubreports(true, true, this.txtStartDate.Text, this.txtEndDate.Text);
                        if (this.Text1.Text == " " || this.Text1.Text == Team)
                            this.Text2.Style = ColorLabel1Default1;
                        else
                            this.Text2.Style = ColorLabel2Default2;

                        //						if(this.Text1.Text == ProjectManager)
                        //							this.Text2.Style = ColorLabel2Default1;
                        this.Text1.Style = ColorLabel1Default1;

                    }
            }
            if (this.Text1.Text == " ")
            {
                this.Text1.Style = "font-size: 10pt;color: White; ";
                this.Text1.Text = ".";

            }
            if (this.Text2.Text == " ")
            {
                this.Text2.Style = "font-size: 10pt;color: White; ";
                this.Text2.Text = ".";

            }
            if (this.txtNotes.Text == " ")
            {
                this.txtNotes.Text = ".";

            }
            else
                this.txtNotes.Style = ColorLabel1Default1;
            if (this.txtStartDate.Text == " ")
            {
                //this.txtStartDate.Style = "font-size: 10pt;color: White; ";
                this.txtStartDate.Text = ".";
            }
            else
                this.txtStartDate.Style = ColorLabel1Default1;
            if (this.txtEndDate.Text == " ")
            {
                //this.txtEndDate.Style = "font-size: 10pt;color: White; ";
                this.txtEndDate.Text = ".";
            }
            else
                this.txtEndDate.Style = ColorLabel1Default1;
        }

	
	


		#region ActiveReports Designer generated code










        public void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ScheduleRpt));
            this.Detail = new GrapeCity.ActiveReports.SectionReportModel.Detail();
            this.PageHeader = new GrapeCity.ActiveReports.SectionReportModel.PageHeader();
            this.PageFooter = new GrapeCity.ActiveReports.SectionReportModel.PageFooter();
            this.lbID = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.SubReport = new GrapeCity.ActiveReports.SectionReportModel.SubReport();
            this.Text1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.Text2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtStartDate = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtEndDate = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtNotes = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            ((System.ComponentModel.ISupportInitialize)(this.lbID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Text1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Text2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtStartDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtEndDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtNotes)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            // 
            // Detail
            // 
            this.Detail.CanShrink = true;
            this.Detail.ColumnSpacing = 0F;
            this.Detail.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
						this.lbID,
						this.SubReport,
						this.Text1,
						this.Text2,
						this.txtStartDate,
						this.txtEndDate,
						this.txtNotes});
            this.Detail.Height = 0.1979167F;
            this.Detail.Name = "Detail";
            // 
            // PageHeader
            // 
            this.PageHeader.Height = 0F;
            this.PageHeader.Name = "PageHeader";
            // 
            // PageFooter
            // 
            this.PageFooter.Height = 0F;
            this.PageFooter.Name = "PageFooter";
            // 
            // lbID
            // 
            this.lbID.DataField = "ProjectID";
            this.lbID.Height = 0.2F;
            this.lbID.HyperLink = null;
            this.lbID.Left = 0F;
            this.lbID.Name = "lbID";
            this.lbID.Style = "";
            this.lbID.Text = "";
            this.lbID.Top = 0F;
            this.lbID.Visible = false;
            this.lbID.Width = 2.27F;
            // 
            // SubReport
            // 
            this.SubReport.CloseBorder = false;
            this.SubReport.Height = 0.2F;
            this.SubReport.Left = 8.389999F;
            this.SubReport.Name = "SubReport";
            this.SubReport.Report = null;
            this.SubReport.Top = 0F;
            this.SubReport.Width = 80.59541F;
            // 
            // Text1
            // 
            this.Text1.CanShrink = true;
            this.Text1.DataField = "ProjectName";
            this.Text1.Height = 0.2F;
            this.Text1.Left = 0F;
            this.Text1.MultiLine = false;
            this.Text1.Name = "Text1";
            this.Text1.OutputFormat = resources.GetString("Text1.OutputFormat");
            this.Text1.Style = "text-align: right; white-space: inherit";
            this.Text1.Top = 0F;
            this.Text1.Width = 2.27F;
            // 
            // Text2
            // 
            this.Text2.CanShrink = true;
            this.Text2.DataField = "SubcontracterName";
            this.Text2.Height = 0.2F;
            this.Text2.Left = 2.27F;
            this.Text2.MultiLine = false;
            this.Text2.Name = "Text2";
            this.Text2.Style = "color: #000000; text-align: right; white-space: inherit";
            this.Text2.Top = 0F;
            this.Text2.Width = 2.52F;
            // 
            // txtStartDate
            // 
            this.txtStartDate.CanShrink = true;
            this.txtStartDate.DataField = "StartDate";
            this.txtStartDate.Height = 0.2F;
            this.txtStartDate.Left = 4.79F;
            this.txtStartDate.MultiLine = false;
            this.txtStartDate.Name = "txtStartDate";
            this.txtStartDate.OutputFormat = resources.GetString("txtStartDate.OutputFormat");
            this.txtStartDate.Style = "text-align: right; white-space: inherit";
            this.txtStartDate.Top = 0F;
            this.txtStartDate.Width = 0.8F;
            // 
            // txtEndDate
            // 
            this.txtEndDate.CanShrink = true;
            this.txtEndDate.DataField = "EndDate";
            this.txtEndDate.Height = 0.2F;
            this.txtEndDate.Left = 5.59F;
            this.txtEndDate.MultiLine = false;
            this.txtEndDate.Name = "txtEndDate";
            this.txtEndDate.OutputFormat = resources.GetString("txtEndDate.OutputFormat");
            this.txtEndDate.Style = "text-align: right; white-space: inherit";
            this.txtEndDate.Top = 0F;
            this.txtEndDate.Width = 0.8F;
            // 
            // txtNotes
            // 
            this.txtNotes.CanShrink = true;
            this.txtNotes.DataField = "Notes";
            this.txtNotes.Height = 0.2F;
            this.txtNotes.Left = 6.39F;
            this.txtNotes.MultiLine = false;
            this.txtNotes.Name = "txtNotes";
            this.txtNotes.OutputFormat = resources.GetString("txtNotes.OutputFormat");
            this.txtNotes.Style = "text-align: left; white-space: inherit";
            this.txtNotes.Top = 0F;
            this.txtNotes.Width = 2F;
            // 
            // ActiveReport1
            // 
            this.MasterReport = false;
            this.PageSettings.PaperHeight = 11F;
            this.PageSettings.PaperWidth = 8.5F;
            this.PrintWidth = 89.01041F;
            this.Sections.Add(this.PageHeader);
            this.Sections.Add(this.Detail);
            this.Sections.Add(this.PageFooter);
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule(resources.GetString("$this.StyleSheet"), "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: inherit; font-style: inherit; font-variant: inherit; font-weight: bo" +
                        "ld; font-size: 16pt; font-size-adjust: inherit; font-stretch: inherit", "Heading1", "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: Times New Roman; font-style: italic; font-variant: inherit; font-wei" +
                        "ght: bold; font-size: 14pt; font-size-adjust: inherit; font-stretch: inherit", "Heading2", "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: inherit; font-style: inherit; font-variant: inherit; font-weight: bo" +
                        "ld; font-size: 13pt; font-size-adjust: inherit; font-stretch: inherit", "Heading3", "Normal"));
            ((System.ComponentModel.ISupportInitialize)(this.lbID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Text1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Text2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtStartDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtEndDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtNotes)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();

            // Attach Report Events
            this.Detail.Format += new System.EventHandler(this.Detail_Format);
        }

		#endregion

        private void ResetSubreports(bool IDorNumb, bool empty, string _startDate, string _endDate)
        {
            float cellHighth = maxHighth(Text1.Height, Text2.Height);
            DateTime startDate = DateTime.MinValue;
            if (_startDate != " ")
                startDate = TimeHelper.GetDate(_startDate);
            DateTime endDate = DateTime.MinValue;
            if (_endDate != " ")
                endDate = TimeHelper.GetDate(_endDate);
            if (startDate == Constants.DateMax)
                txtStartDate.Text = " ";
            if (endDate == Constants.DateMax)
                txtEndDate.Text = " ";
            if (!_weekly)
                this.SubReport.Report = new ScheduleSubreportRpt(_Month, _year, !(empty || IDorNumb), !(empty || !IDorNumb), _mountCount, cellHighth, startDate, endDate);
            else
                this.SubReport.Report = new ScheduleSubreportWeeklyRpt(_Month, _year, !(empty || IDorNumb), !(empty || !IDorNumb), _mountCount, cellHighth, startDate, endDate);
        }
        private float maxHighth(float f1, float f2)
        {
            if (f1 < f2)
                return f2;
            else
                return f1;
        }


        private void TakeBorder(GrapeCity.ActiveReports.Border b)
        {
            b.BottomColor = System.Drawing.Color.Black;
            b.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            b.LeftColor = System.Drawing.Color.Black;
            b.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            b.RightColor = System.Drawing.Color.Black;
            b.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            b.TopColor = System.Drawing.Color.Black;
            b.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
        }

        private PageHeader PageHeader;
        private Detail Detail;
        private Label lbID;
        private SubReport SubReport;
        private TextBox Text1;
        private TextBox Text2;
        private TextBox txtStartDate;
        private TextBox txtEndDate;
        private TextBox txtNotes;
        private PageFooter PageFooter;

	}
}
