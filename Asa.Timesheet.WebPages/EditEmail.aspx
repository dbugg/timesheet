﻿<%@ Register TagPrefix="uc1" TagName="PageHeader" Src="UserControls/PageHeader.ascx" %>
<%@ Page language="c#" Codebehind="EditEmail.aspx.cs" AutoEventWireup="false" Inherits="Asa.Timesheet.WebPages.EditEmail" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
	<HEAD>
		<title>EditEmail</title>
		<meta name="GENERATOR" Content="Microsoft Visual Studio .NET 7.1">
		<meta name="CODE_LANGUAGE" Content="C#">
		<meta name="vs_defaultClientScript" content="JavaScript">
		<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5">
		<LINK href="styles/timesheet.css" type="text/css" rel="stylesheet">
	</HEAD>
	<body bottomMargin="0" leftMargin="0" topMargin="0" rightMargin="0">
		<form id="Form1" method="post" runat="server">
			<table height="100%" cellSpacing="0" cellPadding="0" width="100%" border="0">
				<tr height="5">
					<td>
						<uc1:PageHeader id="header" runat="server"></uc1:PageHeader>
					</td>
				</tr>
				<TR height="1">
					<td noWrap background="images/line.gif" height="1"></td>
				</TR>
				<tr>
					<td>
						<table height="100%" cellSpacing="0" cellPadding="0" width="100%" border="0">
							<tr>
								<td style="BACKGROUND-IMAGE: url(images/left110.gif)" vAlign="top" noWrap width="110">
									<asp:PlaceHolder id="menuHolder" runat="server"></asp:PlaceHolder>
								</td>
								<td noWrap width="1" bgColor="lightgrey"></td>
								<td style="PADDING-LEFT: 5px; PADDING-TOP: 3px" vAlign="top" noWrap>
									<TABLE id="Table3" cellSpacing="0" cellPadding="0" width="880" border="0">
										<TR>
											<TD style="WIDTH: 4px"></TD>
											<TD>
												<TABLE id="Table4" cellSpacing="0" cellPadding="3" width="100%" border="0">
													<TR>
														<TD style="WIDTH: 131px">
															<asp:Label id="lblName" runat="server" Width="100%" CssClass="enterDataLabel" Font-Bold="True"
																EnableViewState="False">Име:</asp:Label></TD>
														<TD>
															<asp:TextBox id="txtName" runat="server" Width="336px" CssClass="enterDataBox"></asp:TextBox>&nbsp;<IMG height="16" alt="" src="images/required1.gif" width="16"></TD>
													</TR>
													<TR>
														<TD style="WIDTH: 131px">
															<asp:Label id="lblEmail" runat="server" Width="100%" CssClass="enterDataLabel" Font-Bold="True"
																EnableViewState="False">Електронен адрес:</asp:Label></TD>
														<TD>
															<asp:TextBox id="txtEmail" runat="server" CssClass="enterDataBox" Width="336px"></asp:TextBox>&nbsp;<IMG height="16" alt="" src="images/required1.gif" width="16"></TD>
													</TR>
													<TR>
														<TD style="WIDTH: 131px">
															<asp:Label id="lblClient" runat="server" CssClass="enterDataLabel" Visible="False">Клиент:</asp:Label></TD>
														<TD>
															<asp:DropDownList id="ddlClients" runat="server" CssClass="enterDataBox" Width="336px" Visible="False"></asp:DropDownList></TD>
													</TR>
												</TABLE>
											</TD>
										</TR>
										<TR>
											<TD style="HEIGHT: 5px" colSpan="2"></TD>
										</TR>
										<TR>
											<TD colspan="2"><img src="images/dot.gif" height="1" width="100%"></TD>
										</TR>
										<TR>
											<td colspan="2" height="3"></td>
										</TR>
										<TR>
											<TD style="WIDTH: 4px"></TD>
											<TD>
												<TABLE id="Table5" cellSpacing="0" cellPadding="0" border="0">
													<TR>
														<TD style="WIDTH: 97px">
															<asp:Button id="btnSave" runat="server" Text="Запиши" CssClass="ActionButton"></asp:Button></TD>
														<TD style="WIDTH: 288px">
															<asp:Button id="btnCancel" runat="server" Text="Откажи" CssClass="ActionButton"></asp:Button></TD>
														<TD>
															<asp:Button id="btnDelete" runat="server" Text="Изтрий" CssClass="ActionButton"></asp:Button></TD>
													</TR>
												</TABLE>
											</TD>
										</TR>
										<TR>
											<TD height="3"></TD>
										</TR>
										<TR>
											<TD colspan="2"><img src="images/dot.gif" height="1" width="100%"></TD>
										</TR>
									</TABLE>
									<br>
									&nbsp;
									<asp:label id="lblInfo" runat="server" CssClass="InfoLabel"></asp:label>
									<asp:label id="lblError" runat="server" ForeColor="Red" CssClass="ErrorLabel"></asp:label>
								</td>
							</tr>
						</table>
					</td>
				</tr>
			</table>
		</form>
	</body>
</HTML>
