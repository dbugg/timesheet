using System;
using GrapeCity.ActiveReports;
using GrapeCity.ActiveReports.Controls;
using GrapeCity.ActiveReports.SectionReportModel;
using GrapeCity.ActiveReports.Document.Section;
using GrapeCity.ActiveReports.Document;

namespace Asa.Timesheet.WebPages.Reports
{
    public class ScheduleSubreportWeeklyRpt : GrapeCity.ActiveReports.SectionReport
	{
		private float _StartValue = 0f;
		private float _CellWidth = 0.2f;
		private float _cellHighth = 0.2f;
		private int _mount;
		private int _year;
		private bool _numbers = false;
		private bool _mountShow = false;
		private int _monthCount;
		private DateTime _startDate;
		private DateTime _endDate;
		private string StyleMonth1 = "text-align: center; font-weight: bold; background-color:  LightGreen; font-size: 12pt; color: Black; ";
		private string StyleMonth2 = "text-align: center; font-weight: bold; background-color:  SkyBlue; font-size: 12pt; color: Black; ";
		private string StyleDefault = "background-color: white;  color: white;";
		private string StyleText = "background-color: white;  color: Black;";
		
		private string StyleWorkedWeekend =" background-color: red; color: red;";


        public ScheduleSubreportWeeklyRpt(int month, int year, bool numbers, bool mountShow, int monthCount, float cellHighth, DateTime startDate, DateTime endDate)
        {
            _mount = month;
            _year = year;
            _numbers = numbers;
            _mountShow = mountShow;
            _monthCount = monthCount;
            _cellHighth = cellHighth;
            _startDate = startDate;
            _endDate = endDate;
            InitializeComponent();
        }


        private void ScheduleSubreportWeeklyRpt_ReportStart(object sender, System.EventArgs eArgs)
        {
            DateTime dtCurr = new DateTime(_year, _mount, 1);

            for (int j = 0; j < _monthCount; j++)
            {

                int WeeksOfMount = WeeksInMount(_year, _mount);
                if (_mountShow)
                {
                    Label lb = new Label();
                    lb.Location = new System.Drawing.PointF(_StartValue, 0);
                    lb.Size = new System.Drawing.SizeF(_CellWidth * WeeksOfMount, _cellHighth);
                    lb.Text = MountShow(_mount);

                    if (j % 2 == 0)
                        lb.Style = StyleMonth1;
                    else
                        lb.Style = StyleMonth2;
                    TakeBorder(lb.Border);
                    Detail.Controls.Add(lb);
                }
                else
                {
                    for (int i = 1; i <= WeeksOfMount; i++)
                    {
                        dtCurr = dtCurr.AddDays(7);

                        Label lb = new Label();

                        lb.Location = new System.Drawing.PointF(_StartValue + _CellWidth * (i - 1), 0);
                        lb.Size = new System.Drawing.SizeF(_CellWidth, _cellHighth);
                        if (_numbers)
                        {
                            lb.Text = i.ToString();
                            lb.Style = StyleText;
                        }
                        else
                        {
                            lb.Text = ".";
                            if (_startDate != DateTime.MinValue && _endDate != DateTime.MinValue && _startDate <= dtCurr && _endDate >= dtCurr)
                                lb.Style = StyleWorkedWeekend;
                            else
                                lb.Style = StyleDefault;
                        }
                        TakeBorder(lb.Border);
                        Detail.Controls.Add(lb);
                    }
                }
                if (_mount == 12)
                {
                    _mount = 1; _year++;
                }
                else
                    _mount++;
                _StartValue += _CellWidth * WeeksOfMount;
            }
            {
                Label lb = new Label();
                lb.Location = new System.Drawing.PointF(_StartValue, 0);
                lb.Size = new System.Drawing.SizeF(0.2f, _cellHighth);
                lb.Text = "      1";

                Detail.Controls.Add(lb);
            }
        }

		

		#region ActiveReports Designer generated code



        public void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ScheduleSubreportWeeklyRpt));
            this.Detail = new GrapeCity.ActiveReports.SectionReportModel.Detail();
            this.PageHeader = new GrapeCity.ActiveReports.SectionReportModel.PageHeader();
            this.PageFooter = new GrapeCity.ActiveReports.SectionReportModel.PageFooter();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            // 
            // Detail
            // 
            this.Detail.ColumnSpacing = 0F;
            this.Detail.Height = 0.2F;
            this.Detail.Name = "Detail";
            // 
            // PageHeader
            // 
            this.PageHeader.Height = 0F;
            this.PageHeader.Name = "PageHeader";
            // 
            // PageFooter
            // 
            this.PageFooter.Height = 0F;
            this.PageFooter.Name = "PageFooter";
            // 
            // ActiveReport1
            // 
            this.MasterReport = false;
            this.PageSettings.PaperHeight = 11F;
            this.PageSettings.PaperWidth = 8.5F;
            this.PrintWidth = 40.5F;
            this.Sections.Add(this.PageHeader);
            this.Sections.Add(this.Detail);
            this.Sections.Add(this.PageFooter);
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule(resources.GetString("$this.StyleSheet"), "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: inherit; font-style: inherit; font-variant: inherit; font-weight: bo" +
                        "ld; font-size: 16pt; font-size-adjust: inherit; font-stretch: inherit", "Heading1", "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: Times New Roman; font-style: italic; font-variant: inherit; font-wei" +
                        "ght: bold; font-size: 14pt; font-size-adjust: inherit; font-stretch: inherit", "Heading2", "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: inherit; font-style: inherit; font-variant: inherit; font-weight: bo" +
                        "ld; font-size: 13pt; font-size-adjust: inherit; font-stretch: inherit", "Heading3", "Normal"));
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();

            // Attach Report Events
            this.ReportStart += new System.EventHandler(this.ScheduleSubreportWeeklyRpt_ReportStart);
        }

		#endregion

        private string MountShow(int month)
        {
            switch (month)
            {
                case 1: return "I";
                case 2: return "II";
                case 3: return "III";
                case 4: return "IV";
                case 5: return "V";
                case 6: return "VI";
                case 7: return "VII";
                case 8: return "VIII";
                case 9: return "IX";
                case 10: return "X";
                case 11: return "XI";
                case 12: return "XII";
                default: return "";
            }
        }
        private int WeeksInMount(int y, int m)
        {
            DateTime dt = new DateTime(y, m, 1);
            DayOfWeek dw = dt.DayOfWeek;
            int Mond = GetMond(dw);
            int DaysOfMonth = DateTime.DaysInMonth(y, m);

            int returnValue = (DaysOfMonth - Mond) / 7 + 1;
            return returnValue;
        }
        private int GetMond(DayOfWeek dw)
        {
            switch (dw)
            {
                case DayOfWeek.Monday: return 1;
                case DayOfWeek.Tuesday: return 7;
                case DayOfWeek.Thursday: return 5;
                case DayOfWeek.Wednesday: return 6;
                case DayOfWeek.Friday: return 4;
                case DayOfWeek.Saturday: return 3;
                case DayOfWeek.Sunday: return 2;
                default: return 1;
            }
        }
        private void TakeBorder(GrapeCity.ActiveReports.Border b)
        {
            b.BottomColor = System.Drawing.Color.Black;
            b.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            b.LeftColor = System.Drawing.Color.Black;
            b.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            b.RightColor = System.Drawing.Color.Black;
            b.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            b.TopColor = System.Drawing.Color.Black;
            b.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
        }

        private PageHeader PageHeader;
        private Detail Detail;
        private PageFooter PageFooter;
	}
}
