
using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using Asa.Timesheet.Data;
using Asa.Timesheet.Data.Entities;
using Asa.Timesheet.Data.Util;
using System.Data.SqlClient;
using System.Xml;
using log4net;
using System.Threading;
using System.Security.Principal;

namespace Asa.Timesheet.WebPages
{
	/// <summary>
	/// Summary description for Hours.
	/// </summary>
	public class HoursPeriod : TimesheetPageBase
	{
		#region Web controls

		protected System.Web.UI.WebControls.DataGrid GridHours;
		protected System.Web.UI.HtmlControls.HtmlInputText Total;
		protected System.Web.UI.WebControls.Button btnNewProject;
		protected System.Web.UI.WebControls.Label lblInfo;
		protected System.Web.UI.HtmlControls.HtmlInputText txtVisibleRows;
		protected System.Web.UI.HtmlControls.HtmlInputText txtNoActivity;
		protected System.Web.UI.WebControls.Button btnSave;
		protected System.Web.UI.HtmlControls.HtmlInputText txtIncorrectDataMsg;
		protected System.Web.UI.WebControls.DropDownList ddlUsers;
		protected System.Web.UI.WebControls.Panel grid;
		protected System.Web.UI.WebControls.Panel divMain;
		protected System.Web.UI.HtmlControls.HtmlTable tblGridBottom;
		protected Asa.Timesheet.WebPages.UserControls.PageHeader header;
		protected System.Web.UI.WebControls.Label lblTop;
		protected System.Web.UI.WebControls.PlaceHolder menuHolder;
		protected System.Web.UI.WebControls.Label lblError;
		protected System.Web.UI.HtmlControls.HtmlInputText txtTimeAbbr;
		protected System.Web.UI.HtmlControls.HtmlInputText txtDefaultWorkDayTimes;
		protected System.Web.UI.HtmlControls.HtmlInputText txtDefaultHoursProjects;
		protected System.Web.UI.HtmlControls.HtmlInputText txtMinGridRows;

		#endregion

		private static readonly ILog log = LogManager.GetLogger(typeof(Hours));

		#region private members

		private ArrayList _activities;
		private ArrayList _projects;
		private ArrayList _times;
		private int _totalMinutes = 0;
		//private bool _isEditMode;
		protected System.Web.UI.WebControls.Label lblStatus;
    protected System.Web.UI.WebControls.Label lblPeriodLbl;
    protected System.Web.UI.WebControls.TextBox txtSD;
    protected System.Web.UI.WebControls.TextBox txtED;
    protected System.Web.UI.HtmlControls.HtmlImage lkSD;
    protected System.Web.UI.HtmlControls.HtmlImage lkED;
    protected System.Web.UI.WebControls.HyperLink HyperLink1;
    protected System.Web.UI.WebControls.Label Label1;
		//private bool _hasGridDefaultProjects = false;
		

		#endregion
	
		#region Grid columns

		private enum GridColumns
		{
			Delete = 0,
			ProjectAndActivity,
			StartTime,
			EndTime,
			Minutes,
			AdminMinutes,
			Mail,
			WorkTimeID,
			
		}

		#endregion

    
		//private const bool ClearGridAfterSave = false;
    protected System.Web.UI.WebControls.Label lblSaveInfo;
    protected System.Web.UI.WebControls.Button btnCopySave;
    protected System.Web.UI.WebControls.TextBox txtCopiedUsers;
    protected System.Web.UI.WebControls.Button btnCopy;
    private const int MaxPeriodDays = 60;

		private enum GridItemStatus
		{
			Normal = 0,
			HasDefaultTimes,
			Disabled
		}


        private void Page_Load(object sender, System.EventArgs e)
        {
            lkSD.Attributes["onclick"] = TimeHelper.InvokePopupCal(txtSD);
            lkED.Attributes["onclick"] = TimeHelper.InvokePopupCal(txtED);

            if (!this.LoggedUser.IsAuthenticated) ErrorRedirect(Resource.ResourceManager["noRightsAlert"]);

            UIHelpers.CreateMenu(menuHolder, LoggedUser);

            if (!this.IsPostBack)
            {
                header.PageTitle = Resource.ResourceManager["hours_PageTitle"];
                header.UserName = this.LoggedUser.FullName;

                btnNewProject.Text = Resource.ResourceManager["hours_btnNewProject"];
                btnNewProject.Attributes["onclick"] = "javascript:ShowNewProject();return false;";

                //		CurrentMonth = new DateTime(DateTime.Now.Year, DateTime.Now.Month, 1).ToString("dd.MM.yyyy");

                txtDefaultWorkDayTimes.Value =
                    String.Concat(SettingsManager.HoursGridDefaultStartHourID.ToString(), ",",
                                                        SettingsManager.HoursGridDefaultEndHourID.ToString());

                LoadUsers(true);
                BindGrid(new ArrayList());
                if (this.LoggedUser.IsAccountant)
                {
                    this.GridHours.Columns[2].Visible = false;
                    this.GridHours.Columns[3].Visible = false;
                }

                btnSave.Text = Resource.ResourceManager["hours_btnSave"];
                btnSave.Attributes["onclick"] = "if (!ValidateInput()) return false; else DisableAllRows(false, null);";

                btnCopy.Attributes["onclick"] = "if (ValidateInput()) { OpenCopyWindow(); } return false;";

                btnCopySave.Attributes["onclick"] = "if (!ValidateInput()) return false; else DisableAllRows(false, null);";


                txtIncorrectDataMsg.Value = Resource.ResourceManager["hoursIncorrectDataMsg"];
                txtTimeAbbr.Value = String.Concat(Resource.ResourceManager["common_HourAbbr"], ";",
                                                            Resource.ResourceManager["common_MinuteAbbr"]);


                txtMinGridRows.Value = "2";//SettingsManager.HoursGridInitialRowsCount.ToString(); 
                //Constants.TimesGridMinVisibleRows.ToString();
                //lblTop.Text = "";

                if (!(this.LoggedUser.IsLeader || this.LoggedUser.IsAccountant))
                {

                    // ddlUsers.Visible = false;	
                    ddlUsers.Style["display"] = "none";
                    // lblTop.Visible = true;
                    lblTop.Text = String.Concat(Resource.ResourceManager["hoursWorkTimeForLabel"], ddlUsers.SelectedItem.Text);

                }
                else
                {
                    ddlUsers.Visible = true;
                    lblTop.Text = String.Concat(Resource.ResourceManager["hoursWorkTimeForLabel"]);
                    // lblTop.Visible = false;

                }

                GridHours.Columns[5].Visible = false;
                GridHours.Columns[4].ItemStyle.Width = Unit.Percentage(40);// = false;
                SetMessageStrings();

                lblSaveInfo.Text = Resource.ResourceManager["hoursPeriod_WillNotSaveAlreadyEnteredMsg"] + "<br>" +
                  String.Format(Resource.ResourceManager["hoursPeriod_PeriodLimitationMsg"], MaxPeriodDays);
            }

            //	CreateCalendar(DateTime.ParseExact(CurrentMonth, "dd.MM.yyyy", System.Globalization.CultureInfo.InvariantCulture));
        }

		#region Hours grid functions

        private void BindGrid(ArrayList workTimesList)
        {
            if (ddlUsers.SelectedValue == "0")
            {
                GridHours.DataSource = null;
                GridHours.DataBind();

                lblTop.Text = String.Empty;
                lblStatus.Text = String.Empty;
                btnSave.Visible = false;
                btnNewProject.Visible = false;
                Total.Value = String.Empty;
                divMain.Visible = false;
                return;
            }

            _projects = LoadProjects();
            _activities = LoadActivities();
            _times = LoadTimes();

            //	ArrayList workTimesList = new ArrayList();

            int firstHiddenRowIndex;
            int prevCount = workTimesList.Count;

            firstHiddenRowIndex = workTimesList.Count > 2 ? workTimesList.Count : 2;

            divMain.Visible = true;


            for (int i = prevCount; i < SettingsManager.HoursGridMaxRowsCount; i++)
            {
                WorkTimeInfo wti = new WorkTimeInfo();
                wti.ProjectID = 0;
                wti.ActivityID = 0;
                wti.StartTimeID = 0;
                wti.EndTimeID = 0;
                wti.Minutes = String.Empty;
                wti.AdminMinutes = String.Empty;
                wti.WorkTimeID = 0;
                wti.Status = 0;
                workTimesList.Add(wti);
            }

            btnNewProject.Visible = true;
            btnSave.Visible = true;

            SetGridItemsStatuses(workTimesList);

            GridHours.DataSource = workTimesList;
            GridHours.DataBind();

            txtVisibleRows.Value = firstHiddenRowIndex.ToString();

            Total.Value = TimeHelper.HoursStringFromMinutes(_totalMinutes, true);
        }

        private void SetGridItemsStatuses(ArrayList workTimes)
        {
            WorkTimeInfo wti = (WorkTimeInfo)workTimes[0];
            if (txtDefaultHoursProjects.Value.IndexOf("." + wti.ProjectID.ToString() + ".") >= 0)
            {
                wti.Status = 1;
                for (int i = 1; i < workTimes.Count; i++) ((WorkTimeInfo)workTimes[i]).Status = 2;
            }
        }

		#endregion

		#region Web Form Designer generated code
        override protected void OnInit(EventArgs e)
        {
            //
            // CODEGEN: This call is required by the ASP.NET Web Form Designer.
            //
            InitializeComponent();
            base.OnInit(e);
        }
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
        private void InitializeComponent()
        {
            this.ddlUsers.SelectedIndexChanged += new System.EventHandler(this.ddlUser_SelectedIndexChanged);
            this.GridHours.ItemCommand += new System.Web.UI.WebControls.DataGridCommandEventHandler(this.GridHours_ItemCommand);
            this.GridHours.ItemDataBound += new System.Web.UI.WebControls.DataGridItemEventHandler(this.GridHours_ItemDataBound);
            this.btnCopySave.Click += new System.EventHandler(this.btnCopySave_Click);
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            this.Load += new System.EventHandler(this.Page_Load);

        }
		#endregion

		#region Database

        private bool LoadUsers(bool loggedUserDefault)
        {
            if (this.LoggedUser.IsLeader)
            {
                ddlUsers.Visible = true;

                SqlDataReader reader = null;
                try
                {
                    reader = UsersData.SelectUserNamesOnlyActive(false/*LoggedUser.HasPaymentRights*/);
                    ddlUsers.DataSource = reader;
                    ddlUsers.DataValueField = "UserID";
                    ddlUsers.DataTextField = "FullName";
                    ddlUsers.DataBind();
                    ddlUsers.Items.Insert(0, new ListItem("<" + Resource.ResourceManager["hours_SelectUser"] + ">", "0"));
                    ddlUsers.SelectedValue = this.LoggedUser.UserID.ToString(); //"0";
                }
                catch (Exception ex)
                {
                    log.Error(ex);
                    return false;
                }
                finally
                {
                    if (reader != null) reader.Close();
                }
            }
            if (this.LoggedUser.IsAccountant)
            {
                ddlUsers.Visible = true;

                SqlDataReader reader = null;
                try
                {
                    reader = UsersData.SelectUserNames(LoggedUser.HasPaymentRights);
                    ddlUsers.DataSource = reader;
                    ddlUsers.DataValueField = "UserID";
                    ddlUsers.DataTextField = "FullName";
                    ddlUsers.DataBind();
                    ddlUsers.Items.Insert(0, new ListItem("<" + Resource.ResourceManager["hours_SelectUser"] + ">", "0"));
                    ddlUsers.SelectedValue = this.LoggedUser.UserID.ToString(); //"0";
                }
                catch (Exception ex)
                {
                    log.Error(ex);
                    return false;
                }
                finally
                {
                    if (reader != null) reader.Close();
                }
            }
            else
            {
                ddlUsers.Items.Insert(0, new ListItem(this.LoggedUser.FullName, this.LoggedUser.UserID.ToString()));
                ddlUsers.SelectedIndex = 0;
            }

            return true;
        }

        private ArrayList LoadProjects()
        {
            System.Text.StringBuilder activityStr = new System.Text.StringBuilder(".");
            System.Text.StringBuilder defaultTimesProjectsStr = new System.Text.StringBuilder(".");

            ArrayList projects = new ArrayList();

            SqlDataReader reader = null;

            try
            {
                if (LoggedUser.IsAccountant) reader = WorkTimesData.SelectProjectsForPeriod(2);
                else reader = WorkTimesData.SelectProjectsForPeriod(1);

                while (reader.Read())
                {
                    ShortProjectInfo pi = new ShortProjectInfo(reader.GetInt32(0), reader.GetString(1));
                    projects.Add(pi);
                    bool hasActivity = reader.IsDBNull(2) ? false : reader.GetBoolean(2);
                    if (!hasActivity) activityStr.Append(pi.ProjectID.ToString() + ".");
                    bool hasDefaultWorkTimes = reader.IsDBNull(3) ? false : reader.GetBoolean(3);
                    if (hasDefaultWorkTimes) defaultTimesProjectsStr.Append(pi.ProjectID.ToString() + ".");
                }
            }
            catch (Exception ex)
            {
                log.Error(ex);
                return null;
            }
            finally
            {
                if (reader != null) reader.Close();
            }

            projects.Insert(0, new ShortProjectInfo(0, ""));

            txtDefaultHoursProjects.Value = defaultTimesProjectsStr.ToString();
            txtNoActivity.Value = activityStr.ToString();
            return projects;
        }

        private ArrayList LoadActivities()
        {
            ArrayList activities = new ArrayList();
            SqlDataReader reader = null;

            try
            {
                reader = DBManager.SelectActivities();

                while (reader.Read())
                {
                    ShortActivityInfo ai = new ShortActivityInfo(reader.GetInt32(0), reader.GetString(1));
                    activities.Add(ai);
                }
            }
            catch (Exception ex)
            {
                log.Error(ex);
                ErrorRedirect("Error load activities.");
            }
            finally
            {
                if (reader != null) reader.Close();
            }

            activities.Insert(0, new ShortActivityInfo(0, ""));
            return activities;
        }

        private ArrayList LoadTimes()
        {
            ArrayList times = new ArrayList();

            SqlDataReader reader = null;

            try
            {
                reader = DBManager.SelectTimes();

                while (reader.Read())
                {
                    TimeInfo ti = new TimeInfo(reader.GetInt32(0), reader.GetString(1));
                    times.Add(ti);
                }
            }
            catch (Exception ex)
            {
                log.Error(ex);
                ErrorRedirect("Error load times.");
            }
            finally
            {
                if (reader != null) reader.Close();
            }

            times.Insert(0, new TimeInfo(0, ""));
            return times;
        }

        private bool SavePeriod()
        {
            DateTime startDate, endDate;
            ArrayList workTimes = null;

            try
            {
                startDate = DateTime.ParseExact(txtSD.Text, "dd.MM.yyyy", System.Globalization.CultureInfo.InvariantCulture);
                endDate = DateTime.ParseExact(txtED.Text, "dd.MM.yyyy", System.Globalization.CultureInfo.InvariantCulture);
            }
            catch
            {
                lblError.Text = Resource.ResourceManager["hoursPeriod_IncorrectDateMsg"];
                workTimes = CreateWorkTimesList(true);
                BindGrid(workTimes);

                return false;
            }

            if (startDate > endDate)
            {
                lblError.Text = Resource.ResourceManager["hoursPeriod_IncorrectPeriodMsg"];
                workTimes = CreateWorkTimesList(true);
                BindGrid(workTimes);

                return false;
            }


            if (startDate.Date.AddDays(MaxPeriodDays) < endDate.Date)
            {
                lblError.Text = String.Format(Resource.ResourceManager["hoursPeriod_TooLongPeriodMsg"], MaxPeriodDays);
                workTimes = CreateWorkTimesList(true);
                BindGrid(workTimes);

                return false;
            }

            int userID = int.Parse(ddlUsers.SelectedValue);

            workTimes = CreateWorkTimesList(true);

            try
            {
                WorkTimesData.InsertWorkTimesForPeriod(userID, startDate, endDate, workTimes,
                    DateTime.Now, this.LoggedUser.UserID, false);
                log.Info(string.Format("Worktime ADD PERIOD for user {0} on dates from {1} to {2} by {3}", userID, startDate, endDate, string.Concat(LoggedUser.FullName, " ( userID ", LoggedUser.UserID, " )")));
                string s = WindowsIdentity.GetCurrent().Name;
                int n = s.IndexOf(@"\");
                if (n >= 0)
                    s = s.Substring(n + 1);
                log.Info(WindowsIdentity.GetCurrent().Name);
                if (LoggedUser.Account.Length != 0 && LoggedUser.Account != s && (!LoggedUser.IsLeader) && (!LoggedUser.IsAccountant))
                {
                    string Dates = string.Concat(startDate.ToShortDateString(), "-", endDate.ToShortDateString());
                    int sUserID = int.Parse(ddlUsers.SelectedValue);
                    UserInfo ui = UsersData.SelectUserByID(sUserID);
                    if (LoggedUser.FullName != ui.FullName)
                        SendDirectorMail(Dates, ui.FullName, s);
                }
            }
            catch (Exception ex)
            {
                log.Error(ex.Message);
                return false;
            }

            //			if (ClearGridAfterSave) BindGrid(new ArrayList());
            //			else 
            BindGrid(workTimes);

            return true;
        }

	
		//notes:Add by Ivailo on 23.01.2008 
		//if you enter worktime for other user than yourself send mail to director
        private void SendDirectorMail(string WorkDates, string UsersName, string account)
        {
            UserInfo Director = UsersData.UsersListByRoleProc(1);
            string DMail = Director.Mail;
            MailBO.SendMailWorktime(DMail, LoggedUser.FullName, WorkDates, UsersName, account);
        }


        private bool SavePeriod(ArrayList userIDs)
        {
            DateTime startDate, endDate;
            ArrayList workTimes = null;

            try
            {
                startDate = DateTime.ParseExact(txtSD.Text, "dd.MM.yyyy", System.Globalization.CultureInfo.InvariantCulture);
                endDate = DateTime.ParseExact(txtED.Text, "dd.MM.yyyy", System.Globalization.CultureInfo.InvariantCulture);
            }
            catch
            {
                lblError.Text = Resource.ResourceManager["hoursPeriod_IncorrectDateMsg"];
                workTimes = CreateWorkTimesList(true);
                BindGrid(workTimes);

                return false;
            }

            if (startDate > endDate)
            {
                lblError.Text = Resource.ResourceManager["hoursPeriod_IncorrectPeriodMsg"];
                workTimes = CreateWorkTimesList(true);
                BindGrid(workTimes);

                return false;
            }


            if (startDate.Date.AddDays(MaxPeriodDays) < endDate.Date)
            {
                lblError.Text = String.Format(Resource.ResourceManager["hoursPeriod_TooLongPeriodMsg"], MaxPeriodDays);
                workTimes = CreateWorkTimesList(true);
                BindGrid(workTimes);

                return false;
            }

            int userID = int.Parse(ddlUsers.SelectedValue);

            userIDs.Insert(0, userID);

            workTimes = CreateWorkTimesList(true);

            try
            {
                string UserIDsNames = string.Empty;
                for (int i = 0; i < userIDs.Count; i++)
                {
                    WorkTimesData.InsertWorkTimesForPeriod((int)userIDs[i], startDate, endDate, workTimes,
                        DateTime.Now, this.LoggedUser.UserID, false);
                    log.Info(string.Format("Worktime ADD PERIOD for user {0} on dates from {1} to {2} by {3}", (int)userIDs[i], startDate, endDate, string.Concat(LoggedUser.FullName, " ( userID ", LoggedUser.UserID, " )")));
                    UserInfo ui = UsersData.SelectUserByID((int)userIDs[i]);
                    UserIDsNames = string.Concat(UserIDsNames, ", ", ui.FullName);
                }
                //				string s=	Request.UserHostAddress;
                //				if(LoggedUser.Account.Length!=0 && LoggedUser.Account!=s&& (!LoggedUser.IsLeader))
                //				{
                //					string sDates = string.Concat(startDate.ToShortDateString(), " - ",endDate.ToShortDateString());
                //					SendDirectorMail(sDates,UserIDsNames,s);
                //				}
            }
            catch (Exception ex)
            {
                log.Error(ex.Message);
                return false;
            }

            //      if (ClearGridAfterSave) BindGrid(new ArrayList());
            //      else 
            BindGrid(workTimes);

            return true;
        }


		

		#endregion

		#region Event handlers

        private void GridHours_ItemDataBound(object sender, System.Web.UI.WebControls.DataGridItemEventArgs e)
        {
            if ((e.Item.ItemType == ListItemType.Item) || (e.Item.ItemType == ListItemType.AlternatingItem))
            {
                WorkTimeInfo currentItemInfo = (WorkTimeInfo)e.Item.DataItem;

                #region Bind controls

                #region Projects

                DropDownList ddlProjects = (DropDownList)e.Item.FindControl("ddlProject");
                ddlProjects.DataSource = _projects;
                ddlProjects.DataValueField = "ProjectID";
                ddlProjects.DataTextField = "ProjectName";
                ddlProjects.DataBind();
                //if(!LoggedUser.IsAccountant)
                {
                    if (ddlProjects.Items.FindByValue(((WorkTimeInfo)e.Item.DataItem).ProjectID.ToString()) != null)
                        ddlProjects.SelectedValue = ((WorkTimeInfo)e.Item.DataItem).ProjectID.ToString();

                    HtmlInputText hdnPrevProjectID = (HtmlInputText)e.Item.FindControl("hdnPrevProjectID");
                    hdnPrevProjectID.Value = ddlProjects.SelectedValue;
                }
                #endregion

                #region Activities

                DropDownList ddlActivities = (DropDownList)e.Item.FindControl("ddlActivity");
                ddlActivities.DataSource = _activities;
                ddlActivities.DataValueField = "ActivityID";
                ddlActivities.DataTextField = "ActivityName";
                ddlActivities.DataBind();
                //if(!LoggedUser.IsAccountant)
                ddlActivities.SelectedValue = currentItemInfo.ActivityID.ToString();

                #endregion

                #region Start time

                DropDownList ddlStartTime = (DropDownList)e.Item.FindControl("ddlStartTime");
                ddlStartTime.DataSource = _times;
                ddlStartTime.DataValueField = "TimeID";
                ddlStartTime.DataTextField = "TimeString";
                ddlStartTime.DataBind();
                ddlStartTime.SelectedValue = currentItemInfo.StartTimeID.ToString();


                #endregion

                #region End time

                DropDownList ddlEndTime = (DropDownList)e.Item.FindControl("ddlEndTime");
                ddlEndTime.DataSource = _times;
                ddlEndTime.DataValueField = "TimeID";
                ddlEndTime.DataTextField = "TimeString";
                ddlEndTime.DataBind();
                ddlEndTime.SelectedValue = currentItemInfo.EndTimeID.ToString();

                #endregion

                #region Minutes

                TextBox minutes = (TextBox)e.Item.FindControl("txtMinutes");
                minutes.Text = currentItemInfo.Minutes == null ? String.Empty : currentItemInfo.Minutes;

                //admin minutes
                TextBox adminMinutes = (TextBox)e.Item.FindControl("txtAdminMinutes");
                adminMinutes.Text = currentItemInfo.AdminMinutes == null ? String.Empty : currentItemInfo.AdminMinutes;

                #endregion

                //worktimeID hidden field - not used now
                HtmlInputHidden hdnWorkTimeID = (HtmlInputHidden)e.Item.FindControl("hdnWorkTimeID");
                hdnWorkTimeID.Value = ((WorkTimeInfo)e.Item.DataItem).WorkTimeID.ToString();

                #region Image buttons

                ImageButton btnDelete = (ImageButton)e.Item.FindControl("btnDeleteItem");

                ImageButton btnMail = (ImageButton)e.Item.FindControl("btnMail");
                if (currentItemInfo.WorkTimeID == 0) btnMail.Style["display"] = "none";
                else
                    if (currentItemInfo.Status != 3) btnMail.Attributes["onclick"] =
                                String.Concat("return confirm('", Resource.ResourceManager["hours_WarningSaveBeforeMail"], "');");
                ImageButton btnMeeting = (ImageButton)e.Item.FindControl("btnMeeting");
                //				btnMeeting.Style["display"] = "none";
                //				else
                //					if (currentItemInfo.Status != 3) btnMeeting.Attributes["onclick"] = 
                //														 String.Concat("return confirm('",Resource.ResourceManager["hours_WarningSaveBeforeMail"],"');");

                #endregion

                #endregion

                #region Set controls' properties

                switch (currentItemInfo.Status)
                {
                    case 0: //Editable item
                        ddlProjects.Attributes["onchange"] = "DisableActivity()";
                        if (txtNoActivity.Value.IndexOf("." + currentItemInfo.ProjectID.ToString() + ".") >= 0) ddlActivities.Enabled = false;

                        ddlStartTime.Attributes["onchange"] = "ProcessTimes();";
                        ddlEndTime.Attributes["onchange"] = "ProcessTimes();";

                        btnDelete.Visible = true;
                        btnDelete.Attributes["onclick"] = "ClearItem(); ProcessTimes(); return false;";

                        break;

                    case 1: // item with default work times (work times - disabled)
                        ddlProjects.Attributes["onchange"] = "DisableActivity()";
                        ddlActivities.Enabled = false;

                        ddlStartTime.Enabled = false;
                        if (ddlStartTime.SelectedValue == "0")
                            ddlStartTime.SelectedValue = SettingsManager.HoursGridDefaultStartHourID.ToString();
                        ddlStartTime.Attributes["onchange"] = "ProcessTimes();";

                        ddlEndTime.Enabled = false;
                        if (ddlEndTime.SelectedValue == "0")
                            ddlEndTime.SelectedValue = SettingsManager.HoursGridDefaultEndHourID.ToString();
                        ddlEndTime.Attributes["onchange"] = "ProcessTimes();";

                        btnDelete.Visible = true;
                        btnDelete.Attributes["onclick"] = "ClearItem(); ProcessTimes(); return false;";

                        break;

                    case 2: // disabled item where grid has a default hours item (remaining empty items rows are with this status)
                        // all event properties are set, in case user changes the default item
                        ddlProjects.Enabled = false;
                        ddlProjects.Attributes["onchange"] = "DisableActivity()";

                        ddlActivities.Enabled = false;

                        ddlStartTime.Enabled = false;
                        ddlStartTime.Attributes["onchange"] = "ProcessTimes();";
                        ddlEndTime.Enabled = false;
                        ddlEndTime.Attributes["onchange"] = "ProcessTimes();";

                        minutes.Enabled = false;
                        adminMinutes.Enabled = false;

                        btnDelete.Visible = true;
                        btnDelete.Attributes["onclick"] = "ClearItem(); ProcessTimes(); return false;";

                        break;

                    case 3: // readonly item - all controls disabled , user can only view hours
                        ddlProjects.Enabled = false;
                        ddlActivities.Enabled = false;

                        ddlStartTime.Enabled = false;
                        ddlEndTime.Enabled = false;

                        minutes.ReadOnly = true;
                        adminMinutes.ReadOnly = true;

                        btnDelete.Visible = false;

                        break;
                }

                #endregion

                int et = TimeHelper.TimeInMinutes(ddlEndTime.SelectedItem.Text);
                int st = TimeHelper.TimeInMinutes(ddlStartTime.SelectedItem.Text);

                _totalMinutes += (et - st);
            }

        }

		#region Create work times list

        private ArrayList CreateWorkTimesList(bool validateInput)
        {
            ArrayList workTimesList = new ArrayList();
            ArrayList timeIntervals = new ArrayList();

            for (int i = 0; i < GridHours.Items.Count; i++)
            {
                WorkTimeInfo wti = new WorkTimeInfo();
                DataGridItem item = (DataGridItem)GridHours.Items[i];

                #region process controls

                int workTimeID = int.Parse(((HtmlInputHidden)item.FindControl("hdnWorkTimeID")).Value);
                wti.WorkTimeID = workTimeID;

                DropDownList ddl = (DropDownList)item.FindControl("ddlProject");
                int projectID = int.Parse(ddl.SelectedValue);
                wti.ProjectID = projectID;

                ddl = (DropDownList)item.FindControl("ddlActivity");
                int activityID = int.Parse(ddl.SelectedValue);
                wti.ActivityID = activityID;

                ddl = (DropDownList)item.FindControl("ddlStartTime");

                int startTimeID = int.Parse(ddl.SelectedValue);
                wti.StartTimeID = startTimeID;
                string sStartTime = ddl.SelectedItem.Text;


                ddl = (DropDownList)item.FindControl("ddlEndTime");

                int endTimeID = int.Parse(ddl.SelectedValue);
                wti.EndTimeID = endTimeID;
                string sEndTime = ddl.SelectedItem.Text;


                TextBox txt = (TextBox)item.FindControl("txtMinutes");
                string minutes = txt.Text.Trim();
                wti.Minutes = (minutes == String.Empty) ? null : minutes;

                txt = (TextBox)item.FindControl("txtAdminMinutes");
                string adminMinutes = txt.Text.Trim();
                wti.AdminMinutes = (adminMinutes == String.Empty) ? null : adminMinutes;

                #endregion

                System.Web.UI.WebControls.Image imgWarning = (System.Web.UI.WebControls.Image)item.FindControl("imgWarning");

                #region validation


                if (wti.ProjectID == 0)
                {
                    if ((wti.ActivityID != 0) || (wti.StartTimeID != 0) || (wti.EndTimeID != 0) || (wti.Minutes != null) || (wti.AdminMinutes != null))
                    {
                        imgWarning.Style["display"] = "";
                        lblError.Text = Resource.ResourceManager["hours_ErrorEmptyProjectField"];
                        return null;
                    }
                    else continue;
                }
                else
                {
                    string s = txtNoActivity.Value;
                    if (txtNoActivity.Value.IndexOf("." + wti.ProjectID.ToString() + ".") == -1)
                    {
                        if (wti.ActivityID == 0)
                        {
                            lblError.Text = Resource.ResourceManager["hours_ErrorMustHaveActivity"];
                            imgWarning.Style["display"] = "";
                            return null;
                        }
                    }
                    else
                    {
                        if (wti.ActivityID != 0)
                        {
                            lblError.Text = Resource.ResourceManager["hours_ErrorMustNotHaveActivity"];
                            imgWarning.Style["display"] = "";
                            return null;
                        }
                    }

                    if ((wti.StartTimeID == 0) || (wti.EndTimeID == 0))
                    {
                        //if columns are hidden set default times
                        if (!GridHours.Columns[2].Visible && !GridHours.Columns[3].Visible)
                        {
                            wti.StartTimeID = 1;
                            wti.EndTimeID = 20;
                        }
                        else
                        {
                            imgWarning.Style["display"] = "";
                            lblError.Text = Resource.ResourceManager["hours_ErrorTimeEmpty"];
                            return null;
                        }
                    }
                }

                if (validateInput)
                {
                    TimeInterval ti = new TimeInterval(TimeHelper.TimeInMinutes(sStartTime), TimeHelper.TimeInMinutes(sEndTime));
                    //new
                    if (CheckTimeOverlaps(ti, timeIntervals))
                    {
                        lblError.Text = Resource.ResourceManager["hours_ErrorTimesOverlap"];
                        imgWarning.Style["display"] = "";
                        return null;
                    }
                    //endnew

                    timeIntervals.Add(ti);

                }

                #endregion

                workTimesList.Add(wti);

            }


            return workTimesList;
        }

        private bool CheckTimeOverlaps(TimeInterval current, ArrayList timeIntervals)
        {
            for (int j = 0; j < timeIntervals.Count; j++)
            {
                TimeInterval temp = (TimeInterval)timeIntervals[j];
                if (temp.StartTime < current.StartTime)
                {
                    if (temp.EndTime > current.StartTime) { return true; }
                }
                else
                {
                    if (temp.StartTime < current.EndTime) { return true; }
                }
            }

            return false;
        }

        private bool CheckTimesOverlap(ArrayList timeIntervals)
        {
            for (int i = 0; i < timeIntervals.Count; i++)
            {
                TimeInterval current = (TimeInterval)timeIntervals[i];
                for (int j = i + 1; j < timeIntervals.Count; j++)
                {
                    TimeInterval temp = (TimeInterval)timeIntervals[j];
                    if (temp.StartTime < current.StartTime)
                    {
                        if (temp.EndTime > current.StartTime) { return true; }
                    }
                    else
                    {
                        if (temp.StartTime < current.EndTime) { return true; }
                    }
                }
            }

            return false;
        }

		#endregion

        private void btnSave_Click(object sender, System.EventArgs e)
        {
            SavePeriod();
            //  BindGrid();
        }

        private void btnCopySave_Click(object sender, System.EventArgs e)
        {
            ArrayList userIDs = Hours.ParseSelectedUsers(this.txtCopiedUsers.Text);

            SavePeriod(userIDs);
        }

        private bool CheckData()
        {
            return true;
        }

        private void ddlUser_SelectedIndexChanged(object sender, System.EventArgs e)
        {
            BindGrid(new ArrayList());
        }

        private void GridHours_ItemCommand(object source, System.Web.UI.WebControls.DataGridCommandEventArgs e)
        {
            if (e.CommandSource is ImageButton)
            {
                ImageButton ib = (ImageButton)e.CommandSource;
                if (ib.ID == "btnMail" || ib.ID == "btnMeeting")
                {
                    //SaveWorkTimes();

                    int WorkTimeID = int.Parse(GridHours.Items[e.Item.ItemIndex].Cells[(int)GridColumns.WorkTimeID].Text);

                    WorkTimeInfo wti = null;
                    try
                    {
                        wti = WorkTimesData.SelectWorkTimesForID(WorkTimeID);
                    }
                    catch (Exception ex)
                    {
                        log.Error(ex);
                        lblError.Text = Resource.ResourceManager["hours_ErrorLoadEmailData"];
                        return;
                    }

                    if (wti != null)
                    {
                        MailInfo mi = new MailInfo();
                        mi.StartDate = mi.EndDate = wti.WorkDate;
                        mi.ProjectID = wti.ProjectID;
                        mi.UserID = wti.UserID;
                        mi.Minutes.Add(wti.Minutes);
                        mi.AdminMinutes.Add(wti.AdminMinutes);
                        mi.ActivityID = wti.ActivityID;
                        mi.WorkTimeID = WorkTimeID;

                        mi.IsFromHours = true;
                        SessionManager.CurrentMailInfo = mi;
                        if (ib.ID == "btnMail")
                            Response.Redirect("MailForm.aspx?view=hours");
                        else
                        {
                            //							string projectName, projectCode, administrativeName,add;
                            //							decimal area=0;
                            //							int clientID=0;
                            //							bool phases;
                            //							if (!ProjectsData.SelectProject(wti.ProjectID, out projectName, out  projectCode, out administrativeName, out area, out add, 
                            //								out clientID,out phases))
                            //							{
                            //								log.Error("Project not found " + wti.ProjectID);
                            //								return;
                            //				
                            //							}
                            //							string sManager="";
                            //							string sClient="";
                            //							SqlDataReader reader = null;
                            //							try
                            //							{
                            //								reader = ClientsData.SelectClient(clientID);
                            //								if (reader.Read())
                            //								{
                            //									
                            //									sManager = reader["Manager"]==DBNull.Value ? String.Empty :(string) reader["Manager"];
                            //									sClient =reader["ClientName"]==DBNull.Value ? String.Empty :(string) reader["ClientName"];
                            //								}
                            //					
                            //							}
                            //							catch (Exception ex)
                            //							{
                            //								log.Error(ex);
                            //					
                            //							}
                            //							finally
                            //							{
                            //								if (reader!=null) reader.Close();
                            //							}
                            //							UIHelpers.Meeting(this,administrativeName,sClient,wti.WorkDate.ToShortDateString(),sManager,LoggedUser.FullName,wti.Minutes);
                        }
                    }
                    else lblError.Text = Resource.ResourceManager["hours_ErrorEmailDataNotFound"];

                }
            }
        }

		#endregion

		#region Menu

//		private void CreateMenu()
//		{
//			UserControls.MenuTable menu = new UserControls.MenuTable();
//			menu.ID = "MenuTable";
//
//			// 'Links' group
//			ArrayList menuItems = new ArrayList();
//
//			menuItems.Add(new MenuItemInfo(Resource.ResourceManager["menuItem_Projects"], "Projects.aspx"));
//			if (LoggedUser.IsLeader || LoggedUser.IsAssistant) 
//			{
//				menuItems.Add(new MenuItemInfo(Resource.ResourceManager["menuItem_Clients"], "Clients.aspx"));
//								
//				menuItems.Add(new MenuItemInfo(Resource.ResourceManager["menuItem_Subcontracters"], "Subcontracters.aspx"));
//					
//			}
//			if (LoggedUser.IsLeader || LoggedUser.IsAdmin)
//				menuItems.Add(new MenuItemInfo(Resource.ResourceManager["menuItem_Users"], "Users.aspx"));
//		
//			menuItems.Add(new MenuItemInfo(Resource.ResourceManager["menuItem_Emails"], "Emails.aspx"));
//
//			menu.AddMenuGroup(Resource.ResourceManager["menuHeader_Links"], 20, menuItems);
//			
//			// 'New' group
//			menuItems = new ArrayList();
//			menuItems.Add(new MenuItemInfo(Resource.ResourceManager["menuItem_NewProject"], "EditProject.aspx"));
//			if (LoggedUser.IsLeader || LoggedUser.IsAssistant)
//			{
//				menuItems.Add(new MenuItemInfo(Resource.ResourceManager["menuItem_NewClient"], "EditClient.aspx"));
//			}
//			if (LoggedUser.IsLeader || LoggedUser.IsAdmin)
//				menuItems.Add(new MenuItemInfo(Resource.ResourceManager["menuItem_NewUser"], "EditUser.aspx"));				
//	
//			menuItems.Add(new MenuItemInfo(Resource.ResourceManager["menuItem_NewEmail"], "EditEmail.aspx"));
//			menuItems.Add(new MenuItemInfo(Resource.ResourceManager["newcontracter_PageTitle"], "EditSubContracter.aspx"));
//
//	
//			menu.AddMenuGroup(Resource.ResourceManager["menuHeader_New"], 30, menuItems);
//
//			
//			// "Reports' group
//			menuItems = new ArrayList();
//			menuItems.Add(new MenuItemInfo(Resource.ResourceManager["menuItem_WorkTimeReport"], "reports/worktimes.aspx"));
//			menuItems.Add(new MenuItemInfo(Resource.ResourceManager["menuItem_MinutesReport"], "Reports/Minutes.aspx"));
//		
//			
//			if (LoggedUser.IsLeader || LoggedUser.IsAssistant)
//				menuItems.Add(new MenuItemInfo(Resource.ResourceManager["menuItem_MailReport"], "MailForm.aspx"));
//			if (LoggedUser.IsLeader  || LoggedUser.IsAdmin)
//			{
//				menuItems.Add(new MenuItemInfo(Resource.ResourceManager["menuItem_NotEnteredWorkTime"], "reports/EmptyHours.aspx"));
//				menuItems.Add(new MenuItemInfo(Resource.ResourceManager["menuItem_Calendar"], "reports/Calendar.aspx"));
//			}
//
//			menu.AddMenuGroup(Resource.ResourceManager["menuHeader_Reports"], 30, menuItems);
//
//			if(LoggedUser.HasPaymentRights)
//				UIHelpers.AddMenuAnalysis(menu);
//			
//			menuItems = new ArrayList();
//			menuItems.Add(new MenuItemInfo(Resource.ResourceManager["menuItem_Help"], SessionManager.GetHelpLink(),true,Pages.Hours));
//			menu.AddMenuGroup("", 10, menuItems);
//
//			menuHolder.Controls.Add(menu);
//		}

		#endregion

		#region Util

        private void SetMessageStrings()
        {
            txtIncorrectDataMsg.Value = String.Concat(Resource.ResourceManager["hours_ErrorEmptyProjectField"], ";",
                Resource.ResourceManager["hours_ErrorMustHaveActivity"], ";",
                Resource.ResourceManager["hours_ErrorMustNotHaveActivity"], ";",
                Resource.ResourceManager["hours_ErrorTimesOverlap"], ";",
                Resource.ResourceManager["hours_ErrorTimeEmpty"], ";",
                Resource.ResourceManager["hours_WarningAllOtherDeleted"]);
        }

		#endregion		

   
	}
}
