using System;
using Asa.Timesheet.Data;
using GrapeCity.ActiveReports;
using GrapeCity.ActiveReports.Controls;
using GrapeCity.ActiveReports.SectionReportModel;
using GrapeCity.ActiveReports.Document.Section;
using GrapeCity.ActiveReports.Document;

namespace Asa.Timesheet.WebPages.Reports
{
    public class ContentsType1Subreport : GrapeCity.ActiveReports.SectionReport
	{
		bool _IsCode=false;
		ChecksVector _checks = null;
		bool _IsOneBuilding = false;
		int _CountofContents = 0;
		bool IsFirst = true;
        public ContentsType1Subreport(bool IsCode, ChecksVector checks, bool IsOneBuilding, int CountofContents)
        {
            _IsCode = IsCode;
            _checks = checks;
            _IsOneBuilding = IsOneBuilding;
            _CountofContents = CountofContents;
            InitializeComponent();
        }

        private void Detail_Format(object sender, System.EventArgs eArgs)
        {
            if (this.txtScale.Text.Length == 0) this.txtScale.Text = " ";
            // this.txtArea.Text = UIHelpers.FormatDecimal2((decimal)this.txtArea.Value);
            this.txtKota.Text = UIHelpers.FormatDecimal2((decimal)this.txtKota.Value);
            if (!_IsCode)
            {
                this.txtFloor.Text = "";
                this.Line20.Visible = false;
            }
            else
            {
                int _cid = int.Parse(this.txtContentid.Text);
                int index = _checks.IndexOf(_cid, true);
                if (index != -1)
                    if (!_checks[index].IsChecked)
                    {
                        this.txtFloor.Text = "";
                        this.txtREV.Text = "";
                    }
                if (this.txtREV.Text.Length != 0)
                    this.txtFloor.Text = string.Concat(this.txtFloor.Text, ".", this.txtREV.Text);
            }


            this.Detail.SizeToFit(false);

        }

        private void GroupHeader1_Format(object sender, System.EventArgs eArgs)
        {
            if (!_IsCode)
            {
                this.Label9.Text = "";
                this.Line6.Visible = false;
            }
            if (_IsOneBuilding) lbSub.Visible = lbSubLabel.Visible = false;
            if (IsFirst)
            {
                txtCountofContents.Text = _CountofContents.ToString();
                IsFirst = false;
            }
            else
            {
                txtCountofContents.Visible = false;
                lbCountofContent.Visible = false;
            }
            this.GroupHeader1.SizeToFit(true);
        }

		#region ActiveReports Designer generated code






























        public void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ContentsType1Subreport));
            this.Detail = new GrapeCity.ActiveReports.SectionReportModel.Detail();
            this.GroupHeader1 = new GrapeCity.ActiveReports.SectionReportModel.GroupHeader();
            this.GroupFooter1 = new GrapeCity.ActiveReports.SectionReportModel.GroupFooter();
            this.Label9 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label5 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label6 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label8 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Line6 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.Line7 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.Line8 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.Line3 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.Line13 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.Line15 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.lbSub = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.lbSubLabel = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.Line19 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.lbCountofContent = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.txtCountofContents = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtFloor = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtContentType = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtKota = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.Line1 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.Line11 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.Line12 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.txtScale = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.Line17 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.Line18 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.Line20 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.txtREV = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtContentid = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            ((System.ComponentModel.ISupportInitialize)(this.Label9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbSub)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbSubLabel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbCountofContent)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCountofContents)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtFloor)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtContentType)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtKota)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtScale)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtREV)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtContentid)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            // 
            // Detail
            // 
            this.Detail.ColumnSpacing = 0F;
            this.Detail.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
						this.txtFloor,
						this.txtContentType,
						this.txtKota,
						this.Line1,
						this.Line11,
						this.Line12,
						this.txtScale,
						this.Line17,
						this.Line18,
						this.Line20,
						this.txtREV,
						this.txtContentid});
            this.Detail.Height = 0.3F;
            this.Detail.Name = "Detail";
            // 
            // GroupHeader1
            // 
            this.GroupHeader1.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
						this.Label9,
						this.Label5,
						this.Label6,
						this.Label8,
						this.Line6,
						this.Line7,
						this.Line8,
						this.Line3,
						this.Line13,
						this.Line15,
						this.lbSub,
						this.lbSubLabel,
						this.Line19,
						this.lbCountofContent,
						this.txtCountofContents});
            this.GroupHeader1.DataField = "BuildingNumber";
            this.GroupHeader1.GroupKeepTogether = GrapeCity.ActiveReports.SectionReportModel.GroupKeepTogether.All;
            this.GroupHeader1.Height = 0.71875F;
            this.GroupHeader1.Name = "GroupHeader1";
            this.GroupHeader1.RepeatStyle = GrapeCity.ActiveReports.SectionReportModel.RepeatStyle.OnPageIncludeNoDetail;
            // 
            // GroupFooter1
            // 
            this.GroupFooter1.Height = 0.09375F;
            this.GroupFooter1.Name = "GroupFooter1";
            // 
            // Label9
            // 
            this.Label9.Height = 0.236F;
            this.Label9.HyperLink = null;
            this.Label9.Left = 3.6875F;
            this.Label9.Name = "Label9";
            this.Label9.Style = "font-size: 10pt; font-weight: bold; text-align: center; vertical-align: middle; d" +
                "do-char-set: 1";
            this.Label9.Text = "???:";
            this.Label9.Top = 0.257F;
            this.Label9.Width = 2.9375F;
            // 
            // Label5
            // 
            this.Label5.Height = 0.236F;
            this.Label5.HyperLink = null;
            this.Label5.Left = 0.05F;
            this.Label5.Name = "Label5";
            this.Label5.Style = "font-size: 10pt; font-weight: bold; text-align: center; vertical-align: middle; d" +
                "do-char-set: 1";
            this.Label5.Text = "?????????????";
            this.Label5.Top = 0.257F;
            this.Label5.Width = 1.825F;
            // 
            // Label6
            // 
            this.Label6.Height = 0.236F;
            this.Label6.HyperLink = null;
            this.Label6.Left = 2.0625F;
            this.Label6.Name = "Label6";
            this.Label6.Style = "font-size: 10pt; font-weight: bold; text-align: center; vertical-align: middle; d" +
                "do-char-set: 1";
            this.Label6.Text = "????";
            this.Label6.Top = 0.257F;
            this.Label6.Width = 0.75F;
            // 
            // Label8
            // 
            this.Label8.Height = 0.2362205F;
            this.Label8.HyperLink = null;
            this.Label8.Left = 2.8625F;
            this.Label8.Name = "Label8";
            this.Label8.Style = "font-size: 10pt; font-weight: bold; text-align: center; vertical-align: middle; d" +
                "do-char-set: 1";
            this.Label8.Text = "?????";
            this.Label8.Top = 0.257F;
            this.Label8.Width = 0.75F;
            // 
            // Line6
            // 
            this.Line6.Height = 0.2760001F;
            this.Line6.Left = 3.632F;
            this.Line6.LineWeight = 1F;
            this.Line6.Name = "Line6";
            this.Line6.Top = 0.25F;
            this.Line6.Width = 0F;
            this.Line6.X1 = 3.632F;
            this.Line6.X2 = 3.632F;
            this.Line6.Y1 = 0.25F;
            this.Line6.Y2 = 0.5260001F;
            // 
            // Line7
            // 
            this.Line7.Height = 0.2690556F;
            this.Line7.Left = 2.819444F;
            this.Line7.LineWeight = 1F;
            this.Line7.Name = "Line7";
            this.Line7.Top = 0.2569444F;
            this.Line7.Width = 0F;
            this.Line7.X1 = 2.819444F;
            this.Line7.X2 = 2.819444F;
            this.Line7.Y1 = 0.2569444F;
            this.Line7.Y2 = 0.5260001F;
            // 
            // Line8
            // 
            this.Line8.Height = 0F;
            this.Line8.Left = 0F;
            this.Line8.LineWeight = 1F;
            this.Line8.Name = "Line8";
            this.Line8.Top = 0.5F;
            this.Line8.Width = 6.625F;
            this.Line8.X1 = 0F;
            this.Line8.X2 = 6.625F;
            this.Line8.Y1 = 0.5F;
            this.Line8.Y2 = 0.5F;
            // 
            // Line3
            // 
            this.Line3.Height = 0F;
            this.Line3.Left = 0F;
            this.Line3.LineWeight = 1F;
            this.Line3.Name = "Line3";
            this.Line3.Top = 0.25F;
            this.Line3.Width = 6.625F;
            this.Line3.X1 = 0F;
            this.Line3.X2 = 6.625F;
            this.Line3.Y1 = 0.25F;
            this.Line3.Y2 = 0.25F;
            // 
            // Line13
            // 
            this.Line13.Height = 0.2760001F;
            this.Line13.Left = 6.6315F;
            this.Line13.LineWeight = 1F;
            this.Line13.Name = "Line13";
            this.Line13.Top = 0.25F;
            this.Line13.Width = 0F;
            this.Line13.X1 = 6.6315F;
            this.Line13.X2 = 6.6315F;
            this.Line13.Y1 = 0.25F;
            this.Line13.Y2 = 0.5260001F;
            // 
            // Line15
            // 
            this.Line15.Height = 0.2755905F;
            this.Line15.Left = 0F;
            this.Line15.LineWeight = 1F;
            this.Line15.Name = "Line15";
            this.Line15.Top = 0.25F;
            this.Line15.Width = 0F;
            this.Line15.X1 = 0F;
            this.Line15.X2 = 0F;
            this.Line15.Y1 = 0.25F;
            this.Line15.Y2 = 0.5255905F;
            // 
            // lbSub
            // 
            this.lbSub.DataField = "Signature";
            this.lbSub.Height = 0.2F;
            this.lbSub.Left = 1.0075F;
            this.lbSub.Name = "lbSub";
            this.lbSub.Style = "font-size: 11pt; vertical-align: bottom";
            this.lbSub.Top = 0F;
            this.lbSub.Width = 0.8674999F;
            // 
            // lbSubLabel
            // 
            this.lbSubLabel.Height = 0.1875F;
            this.lbSubLabel.Left = 0F;
            this.lbSubLabel.Name = "lbSubLabel";
            this.lbSubLabel.Style = "font-size: 11pt";
            this.lbSubLabel.Text = "????????:";
            this.lbSubLabel.Top = 0.01249999F;
            this.lbSubLabel.Width = 0.9842521F;
            // 
            // Line19
            // 
            this.Line19.Height = 0.2690001F;
            this.Line19.Left = 2.007F;
            this.Line19.LineWeight = 1F;
            this.Line19.Name = "Line19";
            this.Line19.Top = 0.257F;
            this.Line19.Width = 0F;
            this.Line19.X1 = 2.007F;
            this.Line19.X2 = 2.007F;
            this.Line19.Y1 = 0.257F;
            this.Line19.Y2 = 0.5260001F;
            // 
            // lbCountofContent
            // 
            this.lbCountofContent.Height = 0.188F;
            this.lbCountofContent.HyperLink = null;
            this.lbCountofContent.Left = 5.0875F;
            this.lbCountofContent.Name = "lbCountofContent";
            this.lbCountofContent.Style = "font-size: 11pt; text-align: right";
            this.lbCountofContent.Text = "???? ???????:";
            this.lbCountofContent.Top = 0.012F;
            this.lbCountofContent.Width = 1.1F;
            // 
            // txtCountofContents
            // 
            this.txtCountofContents.Height = 0.188F;
            this.txtCountofContents.Left = 6.25F;
            this.txtCountofContents.Name = "txtCountofContents";
            this.txtCountofContents.Top = 0.012F;
            this.txtCountofContents.Width = 0.3F;
            // 
            // txtFloor
            // 
            this.txtFloor.DataField = "ContentCode";
            this.txtFloor.Height = 0.2F;
            this.txtFloor.Left = 3.6875F;
            this.txtFloor.Name = "txtFloor";
            this.txtFloor.OutputFormat = resources.GetString("txtFloor.OutputFormat");
            this.txtFloor.Style = "font-size: 9pt; text-align: center; ddo-char-set: 1";
            this.txtFloor.Top = 0.007F;
            this.txtFloor.Width = 2.9375F;
            // 
            // txtContentType
            // 
            this.txtContentType.DataField = "ContentType";
            this.txtContentType.Height = 0.2F;
            this.txtContentType.Left = 0.05F;
            this.txtContentType.Name = "txtContentType";
            this.txtContentType.OutputFormat = resources.GetString("txtContentType.OutputFormat");
            this.txtContentType.Style = "font-size: 9pt; ddo-char-set: 1";
            this.txtContentType.Top = 0.007F;
            this.txtContentType.Width = 1.7F;
            // 
            // txtKota
            // 
            this.txtKota.DataField = "Kota";
            this.txtKota.Height = 0.2F;
            this.txtKota.Left = 2.0625F;
            this.txtKota.Name = "txtKota";
            this.txtKota.Style = "font-size: 9pt; text-align: center; ddo-char-set: 1";
            this.txtKota.Top = 0.007F;
            this.txtKota.Width = 0.75F;
            // 
            // Line1
            // 
            this.Line1.Height = 0F;
            this.Line1.Left = 0F;
            this.Line1.LineWeight = 1F;
            this.Line1.Name = "Line1";
            this.Line1.Top = 0.2755906F;
            this.Line1.Width = 6.625F;
            this.Line1.X1 = 0F;
            this.Line1.X2 = 6.625F;
            this.Line1.Y1 = 0.2755906F;
            this.Line1.Y2 = 0.2755906F;
            // 
            // Line11
            // 
            this.Line11.Height = 0.2690555F;
            this.Line11.Left = 2.8195F;
            this.Line11.LineWeight = 1F;
            this.Line11.Name = "Line11";
            this.Line11.Top = 0F;
            this.Line11.Width = 0F;
            this.Line11.X1 = 2.8195F;
            this.Line11.X2 = 2.8195F;
            this.Line11.Y1 = 0F;
            this.Line11.Y2 = 0.2690555F;
            // 
            // Line12
            // 
            this.Line12.Height = 0.276F;
            this.Line12.Left = 2.007F;
            this.Line12.LineWeight = 1F;
            this.Line12.Name = "Line12";
            this.Line12.Top = 0F;
            this.Line12.Width = 0F;
            this.Line12.X1 = 2.007F;
            this.Line12.X2 = 2.007F;
            this.Line12.Y1 = 0F;
            this.Line12.Y2 = 0.276F;
            // 
            // txtScale
            // 
            this.txtScale.DataField = "Scale";
            this.txtScale.Height = 0.2F;
            this.txtScale.Left = 2.8625F;
            this.txtScale.Name = "txtScale";
            this.txtScale.OutputFormat = resources.GetString("txtScale.OutputFormat");
            this.txtScale.Style = "font-size: 9pt; text-align: center; ddo-char-set: 1";
            this.txtScale.Top = 0.007F;
            this.txtScale.Width = 0.75F;
            // 
            // Line17
            // 
            this.Line17.Height = 0.2755906F;
            this.Line17.Left = 0F;
            this.Line17.LineWeight = 1F;
            this.Line17.Name = "Line17";
            this.Line17.Top = 0F;
            this.Line17.Width = 0F;
            this.Line17.X1 = 0F;
            this.Line17.X2 = 0F;
            this.Line17.Y1 = 0F;
            this.Line17.Y2 = 0.2755906F;
            // 
            // Line18
            // 
            this.Line18.Height = 0.276F;
            this.Line18.Left = 6.6315F;
            this.Line18.LineWeight = 1F;
            this.Line18.Name = "Line18";
            this.Line18.Top = 0F;
            this.Line18.Width = 0F;
            this.Line18.X1 = 6.6315F;
            this.Line18.X2 = 6.6315F;
            this.Line18.Y1 = 0F;
            this.Line18.Y2 = 0.276F;
            // 
            // Line20
            // 
            this.Line20.Height = 0.2690555F;
            this.Line20.Left = 3.632F;
            this.Line20.LineWeight = 1F;
            this.Line20.Name = "Line20";
            this.Line20.Top = 0F;
            this.Line20.Width = 0F;
            this.Line20.X1 = 3.632F;
            this.Line20.X2 = 3.632F;
            this.Line20.Y1 = 0F;
            this.Line20.Y2 = 0.2690555F;
            // 
            // txtREV
            // 
            this.txtREV.DataField = "ContentRevision";
            this.txtREV.Height = 0.1F;
            this.txtREV.Left = 4.8125F;
            this.txtREV.Name = "txtREV";
            this.txtREV.Top = 0.0625F;
            this.txtREV.Visible = false;
            this.txtREV.Width = 0.1F;
            // 
            // txtContentid
            // 
            this.txtContentid.DataField = "ContentID";
            this.txtContentid.Height = 0.1F;
            this.txtContentid.Left = 4.625F;
            this.txtContentid.Name = "txtContentid";
            this.txtContentid.Top = 0.0625F;
            this.txtContentid.Visible = false;
            this.txtContentid.Width = 0.1F;
            // 
            // ActiveReport1
            // 
            this.MasterReport = false;
            this.PageSettings.PaperHeight = 11F;
            this.PageSettings.PaperWidth = 8.5F;
            this.PrintWidth = 6.65625F;
            this.Sections.Add(this.GroupHeader1);
            this.Sections.Add(this.Detail);
            this.Sections.Add(this.GroupFooter1);
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule(resources.GetString("$this.StyleSheet"), "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: inherit; font-style: inherit; font-variant: inherit; font-weight: bo" +
                        "ld; font-size: 16pt; font-size-adjust: inherit; font-stretch: inherit", "Heading1", "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: Times New Roman; font-style: italic; font-variant: inherit; font-wei" +
                        "ght: bold; font-size: 14pt; font-size-adjust: inherit; font-stretch: inherit", "Heading2", "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: inherit; font-style: inherit; font-variant: inherit; font-weight: bo" +
                        "ld; font-size: 13pt; font-size-adjust: inherit; font-stretch: inherit", "Heading3", "Normal"));
            ((System.ComponentModel.ISupportInitialize)(this.Label9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbSub)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbSubLabel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbCountofContent)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCountofContents)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtFloor)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtContentType)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtKota)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtScale)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtREV)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtContentid)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();

            // Attach Report Events
            this.Detail.Format += new System.EventHandler(this.Detail_Format);
            this.GroupHeader1.Format += new System.EventHandler(this.GroupHeader1_Format);
        }

		#endregion

        private GroupHeader GroupHeader1;
        private Label Label9;
        private Label Label5;
        private Label Label6;
        private Label Label8;
        private Line Line6;
        private Line Line7;
        private Line Line8;
        private Line Line3;
        private Line Line13;
        private Line Line15;
        private TextBox lbSub;
        private TextBox lbSubLabel;
        private Line Line19;
        private Label lbCountofContent;
        private TextBox txtCountofContents;
        private Detail Detail;
        private TextBox txtFloor;
        private TextBox txtContentType;
        private TextBox txtKota;
        private Line Line1;
        private Line Line11;
        private Line Line12;
        private TextBox txtScale;
        private Line Line17;
        private Line Line18;
        private Line Line20;
        private TextBox txtREV;
        private TextBox txtContentid;
        private GroupFooter GroupFooter1;
		
		
	}
}
