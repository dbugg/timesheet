using System;
using Asa.Timesheet.Data.Reports;
using Asa.Timesheet.Data;
using GrapeCity.ActiveReports;
using GrapeCity.ActiveReports.Controls;
using GrapeCity.ActiveReports.SectionReportModel;
using GrapeCity.ActiveReports.Document.Section;
using GrapeCity.ActiveReports.Document;

namespace Asa.Timesheet.WebPages.Reports
{
    public class IzhodniDanniSubreport : GrapeCity.ActiveReports.SectionReport
	{
		bool _mainStatus  = true;
		string _projectName = String.Empty;
		int _startPageNumber;

        public IzhodniDanniSubreport(string projectName, int startPageNumber)
        {
            InitializeComponent();
            this._projectName = projectName;
            _startPageNumber = startPageNumber;
        }

        private void IzhodniDanniSubreport_DataInitialize(object sender, System.EventArgs eArgs)
        {

            ProjectsReportDS.InitialDataItemsDataTable tbl = (ProjectsReportDS.InitialDataItemsDataTable)this.DataSource;

            foreach (ProjectsReportDS.InitialDataItemsRow row in tbl)
            {
                if (row.Status == Resource.ResourceManager["Reports_No"])
                {
                    _mainStatus = false;
                    break;
                }
            }

        }


        private void PageHeader_Format(object sender, System.EventArgs eArgs)
        {
            if (this.PageNumber == 1) this.PageHeader.Visible = false;
            else
            {
                this.PageHeader.Visible = true;
                txtPageHeader.Text = _projectName + " - " +
                    Resource.ResourceManager["Reports_strInitialData"];
            }
        }

        private void ReportHeader_Format(object sender, System.EventArgs eArgs)
        {
            this.txtProjectNameHeader.Text = _projectName;
            txtReportTitle.Text = Resource.ResourceManager["Reports_strInitialData"];

            txtMainStatus.Text = this._mainStatus ?
                Resource.ResourceManager["Reports_Yes"] : Resource.ResourceManager["Reports_No"];

        }

        private void PageFooter_Format(object sender, System.EventArgs eArgs)
        {
            lblPageFooter.Text = _projectName;
            lblPage.Text = string.Concat(Resource.ResourceManager["rpt_Page"], " ",
                (this.PageNumber + _startPageNumber - 1).ToString());
        }

		#region ActiveReports Designer generated code
























        public void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(IzhodniDanniSubreport));
            this.Detail = new GrapeCity.ActiveReports.SectionReportModel.Detail();
            this.ReportHeader = new GrapeCity.ActiveReports.SectionReportModel.ReportHeader();
            this.ReportFooter = new GrapeCity.ActiveReports.SectionReportModel.ReportFooter();
            this.PageHeader = new GrapeCity.ActiveReports.SectionReportModel.PageHeader();
            this.PageFooter = new GrapeCity.ActiveReports.SectionReportModel.PageFooter();
            this.txtReportTitle = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtMainStatus = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtProjectNameHeader = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.Line3 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.txtPageHeader = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.Line2 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.txtTitle = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtDate1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtDate2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtNumber = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtStatus = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.TextBox2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.TextBox3 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.TextBox4 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.Line1 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.Line11 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.lblPageFooter = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lblPage = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Line4 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            ((System.ComponentModel.ISupportInitialize)(this.txtReportTitle)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtMainStatus)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtProjectNameHeader)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPageHeader)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTitle)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDate1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDate2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtNumber)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtStatus)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TextBox2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TextBox3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TextBox4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblPageFooter)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblPage)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            // 
            // Detail
            // 
            this.Detail.ColumnSpacing = 0F;
            this.Detail.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
						this.txtTitle,
						this.txtDate1,
						this.txtDate2,
						this.txtNumber,
						this.txtStatus,
						this.TextBox2,
						this.TextBox3,
						this.TextBox4,
						this.Line1});
            this.Detail.Height = 0.6756945F;
            this.Detail.Name = "Detail";
            // 
            // ReportHeader
            // 
            this.ReportHeader.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
						this.txtReportTitle,
						this.txtMainStatus,
						this.txtProjectNameHeader,
						this.Line3});
            this.ReportHeader.Height = 0.7868056F;
            this.ReportHeader.Name = "ReportHeader";
            // 
            // ReportFooter
            // 
            this.ReportFooter.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
						this.Line4});
            this.ReportFooter.Height = 0.09375F;
            this.ReportFooter.Name = "ReportFooter";
            // 
            // PageHeader
            // 
            this.PageHeader.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
						this.txtPageHeader,
						this.Line2});
            this.PageHeader.Height = 0.5902778F;
            this.PageHeader.Name = "PageHeader";
            // 
            // PageFooter
            // 
            this.PageFooter.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
						this.Line11,
						this.lblPageFooter,
						this.lblPage});
            this.PageFooter.Height = 0.2208333F;
            this.PageFooter.Name = "PageFooter";
            // 
            // txtReportTitle
            // 
            this.txtReportTitle.Height = 0.2755906F;
            this.txtReportTitle.Left = 0F;
            this.txtReportTitle.Name = "txtReportTitle";
            this.txtReportTitle.Style = "font-size: 14pt; font-weight: bold";
            this.txtReportTitle.Text = "1. ??????? ????? ?? ???????????";
            this.txtReportTitle.Top = 0F;
            this.txtReportTitle.Width = 6.299212F;
            // 
            // txtMainStatus
            // 
            this.txtMainStatus.Height = 0.2755906F;
            this.txtMainStatus.Left = 6.299212F;
            this.txtMainStatus.Name = "txtMainStatus";
            this.txtMainStatus.Style = "font-size: 14pt; font-weight: bold; text-align: right";
            this.txtMainStatus.Text = "txtMainStatus";
            this.txtMainStatus.Top = 0F;
            this.txtMainStatus.Width = 0.7874014F;
            // 
            // txtProjectNameHeader
            // 
            this.txtProjectNameHeader.Height = 0.2362205F;
            this.txtProjectNameHeader.Left = 0F;
            this.txtProjectNameHeader.Name = "txtProjectNameHeader";
            this.txtProjectNameHeader.Style = "font-size: 12pt; font-weight: bold";
            this.txtProjectNameHeader.Text = "txtProjectNameHeader";
            this.txtProjectNameHeader.Top = 0.2755906F;
            this.txtProjectNameHeader.Width = 7.086611F;
            // 
            // Line3
            // 
            this.Line3.Height = 0F;
            this.Line3.Left = 0F;
            this.Line3.LineWeight = 2F;
            this.Line3.Name = "Line3";
            this.Line3.Top = 0.551181F;
            this.Line3.Width = 7.086611F;
            this.Line3.X1 = 0F;
            this.Line3.X2 = 7.086611F;
            this.Line3.Y1 = 0.551181F;
            this.Line3.Y2 = 0.551181F;
            // 
            // txtPageHeader
            // 
            this.txtPageHeader.Height = 0.2362205F;
            this.txtPageHeader.Left = 0F;
            this.txtPageHeader.Name = "txtPageHeader";
            this.txtPageHeader.Style = "font-size: 11pt; font-weight: bold";
            this.txtPageHeader.Text = "txtPageHeader";
            this.txtPageHeader.Top = 0F;
            this.txtPageHeader.Width = 7.086611F;
            // 
            // Line2
            // 
            this.Line2.Height = 0F;
            this.Line2.Left = 0F;
            this.Line2.LineWeight = 2F;
            this.Line2.Name = "Line2";
            this.Line2.Top = 0.243493F;
            this.Line2.Width = 7.086611F;
            this.Line2.X1 = 0F;
            this.Line2.X2 = 7.086611F;
            this.Line2.Y1 = 0.243493F;
            this.Line2.Y2 = 0.243493F;
            // 
            // txtTitle
            // 
            this.txtTitle.DataField = "Title";
            this.txtTitle.DistinctField = "Title";
            this.txtTitle.Height = 0.2165354F;
            this.txtTitle.Left = 0.07874014F;
            this.txtTitle.Name = "txtTitle";
            this.txtTitle.Style = "font-size: 12pt; font-weight: bold";
            this.txtTitle.Text = "txtTitle";
            this.txtTitle.Top = 0F;
            this.txtTitle.Width = 6.220472F;
            // 
            // txtDate1
            // 
            this.txtDate1.DataField = "Date1";
            this.txtDate1.Height = 0.2F;
            this.txtDate1.Left = 1.377953F;
            this.txtDate1.Name = "txtDate1";
            this.txtDate1.OutputFormat = resources.GetString("txtDate1.OutputFormat");
            this.txtDate1.Style = "font-weight: bold; text-align: left";
            this.txtDate1.Text = "txtDate1";
            this.txtDate1.Top = 0.3267717F;
            this.txtDate1.Width = 1.181102F;
            // 
            // txtDate2
            // 
            this.txtDate2.DataField = "Date2";
            this.txtDate2.Height = 0.2F;
            this.txtDate2.Left = 3.543307F;
            this.txtDate2.Name = "txtDate2";
            this.txtDate2.OutputFormat = resources.GetString("txtDate2.OutputFormat");
            this.txtDate2.Style = "font-weight: bold; text-align: left";
            this.txtDate2.Text = "txtDate2";
            this.txtDate2.Top = 0.3267717F;
            this.txtDate2.Width = 1.181102F;
            // 
            // txtNumber
            // 
            this.txtNumber.DataField = "Number";
            this.txtNumber.Height = 0.2F;
            this.txtNumber.Left = 5.708662F;
            this.txtNumber.Name = "txtNumber";
            this.txtNumber.Style = "font-weight: bold; text-align: left";
            this.txtNumber.Text = "txtNumber";
            this.txtNumber.Top = 0.3267717F;
            this.txtNumber.Width = 1.181102F;
            // 
            // txtStatus
            // 
            this.txtStatus.DataField = "Status";
            this.txtStatus.Height = 0.2165354F;
            this.txtStatus.Left = 6.299212F;
            this.txtStatus.Name = "txtStatus";
            this.txtStatus.Style = "font-size: 12pt; font-weight: bold; text-align: right";
            this.txtStatus.Text = "??";
            this.txtStatus.Top = 0F;
            this.txtStatus.Width = 0.7086611F;
            // 
            // TextBox2
            // 
            this.TextBox2.DataField = "Date2Label";
            this.TextBox2.Height = 0.2F;
            this.TextBox2.Left = 2.559055F;
            this.TextBox2.Name = "TextBox2";
            this.TextBox2.Text = "TextBox2";
            this.TextBox2.Top = 0.3267717F;
            this.TextBox2.Width = 0.9842521F;
            // 
            // TextBox3
            // 
            this.TextBox3.DataField = "NumberLabel";
            this.TextBox3.Height = 0.2F;
            this.TextBox3.Left = 4.72441F;
            this.TextBox3.Name = "TextBox3";
            this.TextBox3.Text = "TextBox3";
            this.TextBox3.Top = 0.3267717F;
            this.TextBox3.Width = 0.9842521F;
            // 
            // TextBox4
            // 
            this.TextBox4.DataField = "Date1Label";
            this.TextBox4.Height = 0.2F;
            this.TextBox4.Left = 0.07874014F;
            this.TextBox4.Name = "TextBox4";
            this.TextBox4.Text = "TextBox4";
            this.TextBox4.Top = 0.3267717F;
            this.TextBox4.Width = 0.9842521F;
            // 
            // Line1
            // 
            this.Line1.Height = 0F;
            this.Line1.Left = 0F;
            this.Line1.LineWeight = 1F;
            this.Line1.Name = "Line1";
            this.Line1.Top = 0.2637796F;
            this.Line1.Width = 7.086611F;
            this.Line1.X1 = 0F;
            this.Line1.X2 = 7.086611F;
            this.Line1.Y1 = 0.2637796F;
            this.Line1.Y2 = 0.2637796F;
            // 
            // Line11
            // 
            this.Line11.Height = 0F;
            this.Line11.Left = 0.006944444F;
            this.Line11.LineWeight = 1F;
            this.Line11.Name = "Line11";
            this.Line11.Top = 0.006944444F;
            this.Line11.Width = 7.086611F;
            this.Line11.X1 = 0.006944444F;
            this.Line11.X2 = 7.093555F;
            this.Line11.Y1 = 0.006944444F;
            this.Line11.Y2 = 0.006944444F;
            // 
            // lblPageFooter
            // 
            this.lblPageFooter.Height = 0.1968504F;
            this.lblPageFooter.HyperLink = null;
            this.lblPageFooter.Left = 0F;
            this.lblPageFooter.Name = "lblPageFooter";
            this.lblPageFooter.Style = "font-size: 8pt; font-weight: bold; text-align: left";
            this.lblPageFooter.Text = "lblPageFooter";
            this.lblPageFooter.Top = 0.02460628F;
            this.lblPageFooter.Width = 6.299212F;
            // 
            // lblPage
            // 
            this.lblPage.Height = 0.1968504F;
            this.lblPage.HyperLink = null;
            this.lblPage.Left = 6.299212F;
            this.lblPage.Name = "lblPage";
            this.lblPage.Style = "font-size: 8pt; font-weight: bold; text-align: right";
            this.lblPage.Text = "lblPage";
            this.lblPage.Top = 0.02460628F;
            this.lblPage.Width = 0.7874014F;
            // 
            // Line4
            // 
            this.Line4.Height = 0F;
            this.Line4.Left = 0F;
            this.Line4.LineWeight = 2F;
            this.Line4.Name = "Line4";
            this.Line4.Top = 0.0384952F;
            this.Line4.Width = 7.086611F;
            this.Line4.X1 = 0F;
            this.Line4.X2 = 7.086611F;
            this.Line4.Y1 = 0.0384952F;
            this.Line4.Y2 = 0.0384952F;
            // 
            // ActiveReport1
            // 
            this.MasterReport = false;
            this.PageSettings.DefaultPaperSize = false;
            this.PageSettings.Margins.Bottom = 0.7875F;
            this.PageSettings.Margins.Left = 1.377778F;
            this.PageSettings.Margins.Right = 0.39375F;
            this.PageSettings.Margins.Top = 0.7875F;
            this.PageSettings.PaperHeight = 11.69306F;
            this.PageSettings.PaperKind = System.Drawing.Printing.PaperKind.A4;
            this.PageSettings.PaperWidth = 8.268056F;
            this.PrintWidth = 7.086611F;
            this.Sections.Add(this.ReportHeader);
            this.Sections.Add(this.PageHeader);
            this.Sections.Add(this.Detail);
            this.Sections.Add(this.PageFooter);
            this.Sections.Add(this.ReportFooter);
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule(resources.GetString("$this.StyleSheet"), "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: inherit; font-style: inherit; font-variant: inherit; font-weight: bo" +
                        "ld; font-size: 16pt; font-size-adjust: inherit; font-stretch: inherit", "Heading1", "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: Times New Roman; font-style: italic; font-variant: inherit; font-wei" +
                        "ght: bold; font-size: 14pt; font-size-adjust: inherit; font-stretch: inherit", "Heading2", "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: inherit; font-style: inherit; font-variant: inherit; font-weight: bo" +
                        "ld; font-size: 13pt; font-size-adjust: inherit; font-stretch: inherit", "Heading3", "Normal"));
            ((System.ComponentModel.ISupportInitialize)(this.txtReportTitle)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtMainStatus)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtProjectNameHeader)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPageHeader)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTitle)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDate1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDate2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtNumber)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtStatus)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TextBox2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TextBox3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TextBox4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblPageFooter)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblPage)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();

            // Attach Report Events
            this.DataInitialize += new System.EventHandler(this.IzhodniDanniSubreport_DataInitialize);
            this.PageHeader.Format += new System.EventHandler(this.PageHeader_Format);
            this.ReportHeader.Format += new System.EventHandler(this.ReportHeader_Format);
            this.PageFooter.Format += new System.EventHandler(this.PageFooter_Format);
        }

		#endregion

        private ReportHeader ReportHeader;
        private TextBox txtReportTitle;
        private TextBox txtMainStatus;
        private TextBox txtProjectNameHeader;
        private Line Line3;
        private PageHeader PageHeader;
        private TextBox txtPageHeader;
        private Line Line2;
        private Detail Detail;
        private TextBox txtTitle;
        private TextBox txtDate1;
        private TextBox txtDate2;
        private TextBox txtNumber;
        private TextBox txtStatus;
        private TextBox TextBox2;
        private TextBox TextBox3;
        private TextBox TextBox4;
        private Line Line1;
        private PageFooter PageFooter;
        private Line Line11;
        private Label lblPageFooter;
        private Label lblPage;
        private ReportFooter ReportFooter;
        private Line Line4;
	}
}
