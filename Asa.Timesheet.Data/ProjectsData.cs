
using System;
using System.Data;
using System.Collections;
using System.Data.SqlClient;
using Microsoft.ApplicationBlocks.Data;

namespace Asa.Timesheet.Data
{
	/// <summary>
	/// Summary description for ProjectsData.
	/// </summary>
	public class ProjectsData
	{
		public enum ProjectsByStatus
		{
			AllProjects = 0,
			Active = 1,
			Passive = 2,
			Conluded = 3
		}

		public static SqlDataReader SelectProjectNames()
		{
			string spName = "ProjectsNamesListProc";

			return SqlHelper.ExecuteReader(DBManager.GetConnectionString(), CommandType.StoredProcedure, spName);
		}

		public static SqlDataReader SelectProjectNames(bool includeInactive, int BuildingTypeID)
		{
			string spName = "ProjectsNamesListProc";

			SqlParameter[] storedParams = SqlHelperParameterCache.GetSpParameterSet(DBManager.GetConnectionString(), spName);
			storedParams[0].Value = includeInactive;

			return SqlHelper.ExecuteReader(DBManager.GetConnectionString(), CommandType.StoredProcedure, spName, storedParams);
		}

		public static SqlDataReader SelectProjectNamesClear(ProjectsByStatus projectsStatus, int BuildingTypeID)
		{
			return SelectProjectNamesClear(projectsStatus,  BuildingTypeID, true);
		}

		public static SqlDataReader SelectProjectNamesClear(ProjectsByStatus projectsStatus, int BuildingTypeID,bool isBuildingTypeGradoustrShow)
		{
			string spName = "ProjectsNamesListProc1";

			SqlParameter[] storedParams = SqlHelperParameterCache.GetSpParameterSet(DBManager.GetConnectionString(), spName);
			
			switch (projectsStatus)
			{
				case ProjectsByStatus.Active : storedParams[0].Value = true; storedParams[1].Value = false;break;
				case ProjectsByStatus.Passive : storedParams[0].Value = false;storedParams[1].Value = false; break;
				case ProjectsByStatus.Conluded : storedParams[1].Value = true; break;
				default : storedParams[0].Value = null; break;
			}
			if(BuildingTypeID>0)
				storedParams[2].Value =BuildingTypeID;
			storedParams[3].Value =isBuildingTypeGradoustrShow;

			return SqlHelper.ExecuteReader(DBManager.GetConnectionString(), CommandType.StoredProcedure, spName, storedParams);
		}

		public static SqlDataReader SelectProjectNames(int projectStatus)
		{
			string spName = "ProjectsNamesListProc2";

			SqlParameter[] storedParams = SqlHelperParameterCache.GetSpParameterSet(DBManager.GetConnectionString(), spName);
			storedParams[0].Value = projectStatus;

			return SqlHelper.ExecuteReader(DBManager.GetConnectionString(), CommandType.StoredProcedure, spName, storedParams);
		}

        public static SqlDataReader SelectProjectNamesRecent()
        {
            string spName = "ProjectsNamesListRecent";



            return SqlHelper.ExecuteReader(DBManager.GetConnectionString(), CommandType.StoredProcedure, spName);
        }

		public static System.Collections.Specialized.StringCollection SelectProjectCheckBuildPermission()
		{
			string spName = "ProjectsListCheckProc";
			System.Collections.Specialized.StringCollection s= new  System.Collections.Specialized.StringCollection();
			

			SqlDataReader reader=null;
			try
			{
				reader = SqlHelper.ExecuteReader(DBManager.GetConnectionString(), CommandType.StoredProcedure, spName, null);
				while(reader.Read())
				{

					s.Add( reader.GetString(1));
					
				}
				
			}
			catch(Exception ex)
			{
				throw ex;
			}
			
			finally
			{
				if (reader!=null) reader.Close();
			}
			return s;

		}

		public static SqlDataReader SelectProjects(int sortOrder, int Active, string Search, int type,/* bool concluded, */bool IsVisible)
		{
			string spName = "ProjectsListProc";

			SqlParameter[] storedParams = SqlHelperParameterCache.GetSpParameterSet(DBManager.GetConnectionString(), spName);
			storedParams[0].Value = sortOrder;
//			if(Active>=0 && !concluded)
//				storedParams[1].Value = Active;
			if(Search!="")
				storedParams[2].Value =Search+'%';
			if(type>0)
				storedParams[3].Value = type;
//			if(concluded)
//				storedParams[4].Value = true;
			switch(Active)
			{
				case (int)ProjectsData.ProjectsByStatus.Active:{storedParams[1].Value = true;storedParams[4].Value = false;break;}
				case (int)ProjectsData.ProjectsByStatus.Passive:{storedParams[1].Value = false;storedParams[4].Value = false;break;}
				case (int)ProjectsData.ProjectsByStatus.Conluded:{storedParams[4].Value = true;break;}
			}
			if(!IsVisible)
				storedParams[5].Value = true;
			//			if(Active==1)
			//				storedParams[4].Value = false;
			return SqlHelper.ExecuteReader(DBManager.GetConnectionString(), CommandType.StoredProcedure, spName, storedParams);
		}
		//notes:this is for PM
		public static SqlDataReader SelectProjects(int sortOrder, int Active, string Search, int type/*, bool concluded*/, bool IsVisible,int ClientID,int SubcontractorID)
		{
			return  SelectProjects( sortOrder,  Active,  Search,  type,/*  concluded, */ IsVisible, ClientID, SubcontractorID,true);
		}
		//notes:this is for PM
		public static SqlDataReader SelectProjects(int sortOrder, int Active, string Search, int type/*, bool concluded*/, bool IsVisible,int ClientID,int SubcontractorID,bool IsBuildingTypeGradoustrShow)
		{
			string spName = "ProjectsListProc";

			SqlParameter[] storedParams = SqlHelperParameterCache.GetSpParameterSet(DBManager.GetConnectionString(), spName);
			storedParams[0].Value = sortOrder;
//			if(Active>=0 && !concluded)
//				storedParams[1].Value = Active;
			if(Search!="")
				storedParams[2].Value =Search+'%';
			if(type>0)
				storedParams[3].Value = type;
//			if(concluded)
//				storedParams[4].Value = true;
			switch(Active)
			{
				case (int)ProjectsData.ProjectsByStatus.Active:{storedParams[1].Value = true;storedParams[4].Value = false;break;}
				case (int)ProjectsData.ProjectsByStatus.Passive:{storedParams[1].Value = false;storedParams[4].Value = false;break;}
				case (int)ProjectsData.ProjectsByStatus.Conluded:{storedParams[4].Value = true;break;}
			}
			if(!IsVisible)
				storedParams[5].Value = true;
			if(ClientID>0)
				storedParams[6].Value = ClientID;
			if(SubcontractorID>0)
				storedParams[7].Value = SubcontractorID;
			storedParams[8].Value = IsBuildingTypeGradoustrShow;

			return SqlHelper.ExecuteReader(DBManager.GetConnectionString(), CommandType.StoredProcedure, spName, storedParams);
		}
		public static string SelectProjectName(int projectID)
		{
			string projectName;
			string projectCode;
			string administrativeName;
			decimal area;
			string AddInfo;
			int ClientID;
			bool phases;
			bool isPUP;
			SelectProject(projectID,out projectName, out projectCode, out administrativeName,
				out area, out AddInfo, out ClientID, out phases,out isPUP);
			string projectNameAndCode = projectName + " " + projectCode;
			return projectNameAndCode;
		}
		//notes:here is for PM mail function
		public static string SelectProjectNameForPMmails(int projectID)
		{
			string projectName;
			string projectCode;
			string administrativeName;
			decimal area;
			string AddInfo;
			int ClientID;
			bool phases;
			bool isPUP;
			SelectProject(projectID,out projectName, out projectCode, out administrativeName,
				out area, out AddInfo, out ClientID, out phases,out isPUP);
			string projectNameAndCode = projectCode + " " + projectName;
			return projectNameAndCode;
		}
		public static bool SelectProject(int projectID, out string projectName, out string projectCode, out string administrativeName, 
			out decimal area, 
			out string AddInfo, out int ClientID, out bool phases,out bool isPUP)
		{
			SqlDataReader reader = null;
			projectName=projectCode=administrativeName=AddInfo= "";
			area=ClientID=0;
			phases=false;isPUP=false;
			try
			{
				reader = ProjectsData.SelectProject(projectID);
				if(reader.Read())
				{

					projectName = reader.GetString(1);
					projectCode = reader.IsDBNull(2) ? String.Empty : reader.GetString(2);
					area = reader.IsDBNull(4) ? 0 : reader.GetDecimal(4);
					administrativeName = reader.IsDBNull(12) ? String.Empty : reader.GetString(12);
					AddInfo=reader.IsDBNull(14) ? String.Empty : reader.GetString(14);
					ClientID=reader.IsDBNull(5) ? 0 : reader.GetInt32(5);
					phases=reader.IsDBNull(19) ? false : reader.GetBoolean(19);
					isPUP = reader.IsDBNull(32) ? false : reader.GetBoolean(32);
				}
				else
					return false;
			}
			catch(Exception ex)
			{
				throw ex;
			}
			
			finally
			{
				if (reader!=null) reader.Close();
			}

			return true;

		}
		public static int SelectProjectManagerID(int projectID)
		{
			int projectManagerID = 0;
			
			SqlDataReader reader = null;
			try
			{
				reader = SqlHelper.ExecuteReader(DBManager.GetConnectionString(), CommandType.StoredProcedure, "ProjectsSelProc", SQLParms.CreateProjectsSelProc(projectID));			
				if(reader.Read())
				{
					projectManagerID = reader.IsDBNull(11) ? 0 : reader.GetInt32(11);
				}
				
			}
			catch(Exception ex)
			{
				throw ex;
			}
			
			finally
			{
				if (reader!=null) reader.Close();
			}

			return projectManagerID;

		}
		public static int SelectProjectClientID(int projectID)
		{
			int projectClientID = 0;
			SqlDataReader reader = null;
			try
			{
				reader = SqlHelper.ExecuteReader(DBManager.GetConnectionString(), CommandType.StoredProcedure, "ProjectsSelProc", SQLParms.CreateProjectsSelProc(projectID));
				if(reader.Read())
				{
					projectClientID = reader.IsDBNull(5) ? 0 : reader.GetInt32(5);
				}
				
			}
			catch(Exception ex)
			{
				throw ex;
			}
			
			finally
			{
				if (reader!=null) reader.Close();
			}

			return projectClientID;

		}
		public static SqlDataReader SelectProject(int projectID)
		{
			string spName = "ProjectsSelProc";

			SqlParameter[] storedParams = SqlHelperParameterCache.GetSpParameterSet(DBManager.GetConnectionString(), spName);
			storedParams[0].Value = projectID;

			return SqlHelper.ExecuteReader(DBManager.GetConnectionString(), CommandType.StoredProcedure, spName, storedParams);
		}

		public static decimal SelectSubprojectsAmount(int projectID)
		{
			string spName = "Project_SelectSubprojectsAmount";

			SqlParameter[] storedParams = SqlHelperParameterCache.GetSpParameterSet(DBManager.GetConnectionString(), spName);
			storedParams[0].Value = projectID;


			object o = null;
			o = SqlHelper.ExecuteScalar(DBManager.GetConnectionString(), CommandType.StoredProcedure, spName, storedParams);
      
			return o == null ? 0 : (decimal)o;
		}

		
		public static int InsertProject(string projectName, string projectCode, string administrativeName, int buildingTypeID, decimal area, 
			int clientID, bool isVisible, bool isActive, bool hasActivity, DateTime startDate, string address, int projectManagerID,
			string companyOwner, string investor, string add, int SchemeID, decimal rate, decimal total, decimal area1, bool phases, DateTime enddate,DateTime enddatecont,bool black,
			int FoldersGiven, int FoldersArchive, bool concluded, decimal dAuth, int nInv,string color,bool isPUP,
			string ProjectTechnicalName,string ProjectTechnicalPhone,string ProjectTechnicalEmail,bool registerCameraArchitects, string nameEN)
		{
			string spName = "ProjectsInsProc";

			SqlParameter[] storedParams = SqlHelperParameterCache.GetSpParameterSet(
				DBManager.GetConnectionString(), spName);		

			storedParams[0].Value = projectName;
			storedParams[1].Value = (projectCode==String.Empty) ? null : projectCode;
			storedParams[2].Value = (administrativeName==String.Empty) ? null : administrativeName;
			storedParams[3].Value = (buildingTypeID==0) ? null : (object)buildingTypeID;
			storedParams[4].Value = (area == 0) ?  null : (object)area;
			storedParams[5].Value = (clientID == 0) ? null : (object)clientID;
			storedParams[6].Value = isVisible;
			storedParams[7].Value = isActive;
			storedParams[8].Value = hasActivity;
			if(startDate!=Constants.DateMax)
				storedParams[9].Value = startDate;
			storedParams[10].Value = (address == String.Empty) ? null : address;
			storedParams[11].Value = (projectManagerID == 0) ? null : (object)projectManagerID;
			storedParams[12].Value = companyOwner;
			storedParams[13].Value = investor;
			storedParams[14].Value = add;
			storedParams[15].Value = SchemeID;
			storedParams[16].Value = rate;
			storedParams[17].Value = total;
			storedParams[18].Value = area1;
			storedParams[19].Value = phases;
			if(enddate!=Constants.DateMax)
				storedParams[20].Value = enddate;
			if(enddatecont!=Constants.DateMax)
				storedParams[21].Value = enddatecont;
			storedParams[22].Value = black;
			if(FoldersGiven>0)
				storedParams[23].Value =FoldersGiven;
			if(FoldersArchive>0)
				storedParams[24].Value =FoldersArchive;
			storedParams[25].Value = concluded;
			storedParams[26].Value = dAuth;
			if(nInv>0)
				storedParams[27].Value = nInv;
			if(color!="")
				storedParams[28].Value = color;
			storedParams[29].Value = isPUP;
			storedParams[30].Value = ProjectTechnicalName;
			storedParams[31].Value = ProjectTechnicalPhone;
			storedParams[32].Value = ProjectTechnicalEmail;
			storedParams[33].Value = registerCameraArchitects;
			storedParams[34].Value =nameEN;
			SqlHelper.ExecuteNonQuery(DBManager.GetConnectionString(), CommandType.StoredProcedure, spName, storedParams);

			return (int)storedParams[34].Value;
		}

		public static void UpdateProject(int projectID, string projectName, string projectCode, string administrativeName, int buildingTypeID, decimal area, 
			int clientID, bool hasActivity, DateTime startDate, string address, int projectManagerID, string companyOwner,
			string investor, string add, int SchemeID, decimal rate, decimal total, decimal area1, bool phases, DateTime enddate,DateTime enddatecont,bool black, bool isactive,
			int FoldersGiven, int FoldersArchive, bool concluded, decimal dAuth, int nInv,string color, bool isPUP,
			string ProjectTechnicalName,string ProjectTechnicalPhone,string ProjectTechnicalEmail,bool registerCameraArchitects, string nameEN)
		{
			string spName = "ProjectsUpdProc";

			SqlParameter[] storedParams = SqlHelperParameterCache.GetSpParameterSet(
				DBManager.GetConnectionString(), spName);		
			storedParams[0].Value = projectID;
			storedParams[1].Value = projectName;
			storedParams[2].Value = (projectCode==String.Empty) ? null : projectCode;
			storedParams[3].Value = (administrativeName==String.Empty) ? null : administrativeName;
			storedParams[4].Value = (buildingTypeID==0) ? null : (object)buildingTypeID;
			storedParams[5].Value = (area == 0) ?  null : (object)area;
			storedParams[6].Value = (clientID == 0) ? null : (object)clientID;
			storedParams[7].Value = hasActivity;
			if(startDate!=Constants.DateMax)
				storedParams[8].Value = startDate;
			storedParams[9].Value = (address == String.Empty) ? null : address;
			storedParams[10].Value = (projectManagerID == 0) ? null : (object)projectManagerID;
			storedParams[11].Value = companyOwner;
			storedParams[12].Value = investor;
			storedParams[13].Value = add;
			storedParams[14].Value = SchemeID;
			storedParams[15].Value = rate;
			storedParams[16].Value = total;
			storedParams[17].Value = area1;
			storedParams[18].Value = phases;
			if(enddate!=Constants.DateMax)
				storedParams[19].Value = enddate;
			if(enddatecont!=Constants.DateMax)
				storedParams[20].Value = enddatecont;
			storedParams[21].Value = black;
			storedParams[22].Value =isactive;
			if(FoldersGiven>0)
				storedParams[23].Value =FoldersGiven;
			if(FoldersArchive>0)
				storedParams[24].Value =FoldersArchive;

			storedParams[25].Value =concluded;
			storedParams[26].Value = dAuth;
			if(nInv>0)
				storedParams[27].Value = nInv;
			if(color!="")
				storedParams[28].Value = color;
			storedParams[30].Value = isPUP;
			storedParams[31].Value = ProjectTechnicalName;
			storedParams[32].Value = ProjectTechnicalPhone;
			storedParams[33].Value = ProjectTechnicalEmail;
			storedParams[34].Value = registerCameraArchitects;
			storedParams[35].Value =nameEN;
			SqlHelper.ExecuteNonQuery(DBManager.GetConnectionString(), CommandType.StoredProcedure, spName, storedParams);
		}

		public static void UpdateProjectPhase(int projectID, string projectPhase)
		{
			string spName = "ProjectsUpdPhaseProc";

			SqlParameter[] storedParams = SqlHelperParameterCache.GetSpParameterSet(
				DBManager.GetConnectionString(), spName);		
			storedParams[0].Value = projectID;
			if(projectPhase.Length!=0)
				storedParams[1].Value = projectPhase;
			SqlHelper.ExecuteNonQuery(DBManager.GetConnectionString(), CommandType.StoredProcedure, spName, storedParams);
		}

		public static void DeleteProject(int projectID)
		{
			string spName = "ProjectsDelProc";

			SqlParameter[] storedParams = SqlHelperParameterCache.GetSpParameterSet(DBManager.GetConnectionString(), spName);
			storedParams[0].Value = projectID;

			SqlHelper.ExecuteNonQuery(DBManager.GetConnectionString(), CommandType.StoredProcedure, spName, storedParams);
		}

		public static void InactiveProject(int projectID)
		{
			string spName = "ProjectsInactiveProc";

			SqlParameter[] storedParams = SqlHelperParameterCache.GetSpParameterSet(DBManager.GetConnectionString(), spName);
			storedParams[0].Value = projectID;

			SqlHelper.ExecuteNonQuery(DBManager.GetConnectionString(), CommandType.StoredProcedure, spName, storedParams);
		}
		/*
				public static bool InactiveProjectWithCheck(int projectID)
				{
					string spName = "ProjectsInactiveWithCheckProc";

					SqlParameter[] storedParams = SqlHelperParameterCache.GetSpParameterSet(DBManager.GetConnectionString(), spName);
					storedParams[0].Value = projectID;

					SqlHelper.ExecuteNonQuery(DBManager.GetConnectionString(), CommandType.StoredProcedure, spName, storedParams);

					return (bool)storedParams[1].Value;
				}
		*/
		
		public static DataRow SelectProjectProfile1(int projectID)
		{
			string spName = "ProjectsSelectProfile1";

			#region Parameters

			SqlParameter[] storedParams = SqlHelperParameterCache.GetSpParameterSet(DBManager.GetConnectionString(), spName);
			storedParams[0].Value = projectID;

			#endregion
    
			DataSet ds = SqlHelper.ExecuteDataset(DBManager.GetConnectionString(), CommandType.StoredProcedure, spName, storedParams);
			if (ds.Tables[0].Rows.Count > 0) return ds.Tables[0].Rows[0];

			return null;
		}
		public static DataRow SelectProjectProfile2(int projectID)
		{
			string spName = "ProjectsSelectProfile2";

			#region Parameters

			SqlParameter[] storedParams = SqlHelperParameterCache.GetSpParameterSet(DBManager.GetConnectionString(), spName);
			storedParams[0].Value = projectID;

			#endregion
    
			DataSet ds = SqlHelper.ExecuteDataset(DBManager.GetConnectionString(), CommandType.StoredProcedure, spName, storedParams);
			if (ds.Tables[0].Rows.Count > 0) return ds.Tables[0].Rows[0];

			return null;
		}
		public static int ExecuteProjectsUpdateProfile2(int ProjectID, DateTime VIKEnt, DateTime VIKRec, DateTime VIKDate, string VIKNumber, DateTime VIKProjectAssinged, DateTime VIKProjectAgreed, DateTime VIKLetter, DateTime VIKContract, decimal VIKAmount, DateTime ELEnt, DateTime ELRec, DateTime ELDate, string ELNumber, DateTime ELProjectAssinged, DateTime ELProjectAgreed, DateTime ELLetter, DateTime ELContract, decimal ELAmount, DateTime TEnt, DateTime TRec, DateTime TDate, string TNumber, DateTime TProjectAssinged, DateTime TProjectAgreed, DateTime TLetter, DateTime TContract, decimal TAmount, DateTime PathEnt, DateTime PathRec, DateTime PathDate, string PathNumber, DateTime PathProjectAssinged, DateTime PathProjectAgreed, decimal PathAmount, DateTime PhoneEnt, DateTime PhoneRec, DateTime PhoneDate, string PhoneNumber, DateTime IdeaProjectDone, string IdeaProjectNotes, string IdeaProjectMun, string IdeaProjectMunWhere, DateTime IdeaProjectAgreed, DateTime TechProjectDone, string TechProjectNotes, string TechProjectMun, string TechProjectMunWhere, DateTime TechProjectAgreed, DateTime BuildPermission, DateTime BuildPermissionDone, string BuildPermissionNumber, DateTime Other, DateTime OtherDone, string OtherNumber, decimal OtherExpenses, string VIKNotes, string ElNotes, string TNotes, string PathNotes, string PhoneNotes, decimal phoneamount,string buildnotes)
		{
			int RetObj;
			System.Data.SqlClient.SqlCommand cmd;
			cmd = CreateProjectsUpdateProfile2(ProjectID, VIKEnt, VIKRec, VIKDate, VIKNumber, VIKProjectAssinged, VIKProjectAgreed, VIKLetter, VIKContract, VIKAmount, ELEnt, ELRec, ELDate, ELNumber, ELProjectAssinged, ELProjectAgreed, ELLetter, ELContract, ELAmount, TEnt, TRec, TDate, TNumber, TProjectAssinged, TProjectAgreed, TLetter, TContract, TAmount, PathEnt, PathRec, PathDate, PathNumber, PathProjectAssinged, PathProjectAgreed, PathAmount, PhoneEnt, PhoneRec, PhoneDate, PhoneNumber, IdeaProjectDone, IdeaProjectNotes, IdeaProjectMun, IdeaProjectMunWhere, IdeaProjectAgreed, TechProjectDone, TechProjectNotes, TechProjectMun, TechProjectMunWhere, TechProjectAgreed, BuildPermission, BuildPermissionDone, BuildPermissionNumber, Other, OtherDone, OtherNumber, OtherExpenses, VIKNotes, ElNotes, TNotes, PathNotes, PhoneNotes, phoneamount,buildnotes);
			SqlConnection cnDAL = new SqlConnection();

			try
			{
				cnDAL =  CommonDAL.OpenConnection();
				cmd.Connection = cnDAL;
				RetObj = cmd.ExecuteNonQuery();
				return RetObj;
		
			}
			catch(SqlException ex)
			{

				throw new ApplicationException("ProjectsUpdateProfile2",ex);
			}

			finally
			{
				CommonDAL.CloseConnection(cnDAL);
			}
		}
		private static SqlCommand CreateProjectsUpdateProfile2 (int ProjectID, DateTime VIKEnt, DateTime VIKRec, DateTime VIKDate, string VIKNumber, DateTime VIKProjectAssinged, DateTime VIKProjectAgreed, DateTime VIKLetter, DateTime VIKContract, decimal VIKAmount, DateTime ELEnt, DateTime ELRec, DateTime ELDate, string ELNumber, DateTime ELProjectAssinged, DateTime ELProjectAgreed, DateTime ELLetter, DateTime ELContract, decimal ELAmount, DateTime TEnt, DateTime TRec, DateTime TDate, string TNumber, DateTime TProjectAssinged, DateTime TProjectAgreed, DateTime TLetter, DateTime TContract, decimal TAmount, DateTime PathEnt, DateTime PathRec, DateTime PathDate, string PathNumber, DateTime PathProjectAssinged, DateTime PathProjectAgreed, decimal PathAmount, DateTime PhoneEnt, DateTime PhoneRec, DateTime PhoneDate, string PhoneNumber, DateTime IdeaProjectDone, string IdeaProjectNotes, string IdeaProjectMun, string IdeaProjectMunWhere, DateTime IdeaProjectAgreed, DateTime TechProjectDone, string TechProjectNotes, string TechProjectMun, string TechProjectMunWhere, DateTime TechProjectAgreed, DateTime BuildPermission, DateTime BuildPermissionDone, string BuildPermissionNumber, DateTime Other, DateTime OtherDone, string OtherNumber, decimal OtherExpenses, string VIKNotes, string ElNotes, string TNotes, string PathNotes, string PhoneNotes, decimal phoneamount,string buildnotes)
		{
			SqlCommand cmd = new SqlCommand();
			cmd.CommandType = System.Data.CommandType.StoredProcedure;
			cmd.CommandText = "ProjectsUpdateProfile2";
			cmd.Parameters.Add(new System.Data.SqlClient.SqlParameter("@ProjectID", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.Input, true, 10, 0, "ProjectID", DataRowVersion.Current, null));
			cmd.Parameters["@ProjectID"].Value = ProjectID;
			cmd.Parameters.Add(new System.Data.SqlClient.SqlParameter("@VIKEnt", System.Data.SqlDbType.SmallDateTime, 8, System.Data.ParameterDirection.Input, true, 0, 0, "VIKEnt", DataRowVersion.Current, null));
			if(VIKEnt!=Constants.DateMax)
				cmd.Parameters["@VIKEnt"].Value = VIKEnt;
			cmd.Parameters.Add(new System.Data.SqlClient.SqlParameter("@VIKRec", System.Data.SqlDbType.SmallDateTime, 8, System.Data.ParameterDirection.Input, true, 0, 0, "VIKRec", DataRowVersion.Current, null));
			if(VIKRec!=Constants.DateMax)
				cmd.Parameters["@VIKRec"].Value = VIKRec;
			cmd.Parameters.Add(new System.Data.SqlClient.SqlParameter("@VIKDate", System.Data.SqlDbType.SmallDateTime, 8, System.Data.ParameterDirection.Input, true, 0, 0, "VIKDate", DataRowVersion.Current, null));
			if(VIKDate!=Constants.DateMax)
				cmd.Parameters["@VIKDate"].Value = VIKDate;
			cmd.Parameters.Add(new System.Data.SqlClient.SqlParameter("@VIKNumber", System.Data.SqlDbType.NVarChar,100 , System.Data.ParameterDirection.Input, true, 0, 0, "VIKNumber", DataRowVersion.Current, null));
			if(VIKNumber!="")
				cmd.Parameters["@VIKNumber"].Value = VIKNumber;
			cmd.Parameters.Add(new System.Data.SqlClient.SqlParameter("@VIKProjectAssinged", System.Data.SqlDbType.SmallDateTime, 8, System.Data.ParameterDirection.Input, true, 0, 0, "VIKProjectAssinged", DataRowVersion.Current, null));
			if(VIKProjectAssinged!=Constants.DateMax)
				cmd.Parameters["@VIKProjectAssinged"].Value = VIKProjectAssinged;
			cmd.Parameters.Add(new System.Data.SqlClient.SqlParameter("@VIKProjectAgreed", System.Data.SqlDbType.SmallDateTime, 8, System.Data.ParameterDirection.Input, true, 0, 0, "VIKProjectAgreed", DataRowVersion.Current, null));
			if(VIKProjectAgreed!=Constants.DateMax)
				cmd.Parameters["@VIKProjectAgreed"].Value = VIKProjectAgreed;
			cmd.Parameters.Add(new System.Data.SqlClient.SqlParameter("@VIKLetter", System.Data.SqlDbType.SmallDateTime, 8, System.Data.ParameterDirection.Input, true, 0, 0, "VIKLetter", DataRowVersion.Current, null));
			if(VIKLetter!=Constants.DateMax)
				cmd.Parameters["@VIKLetter"].Value = VIKLetter;
			cmd.Parameters.Add(new System.Data.SqlClient.SqlParameter("@VIKContract", System.Data.SqlDbType.SmallDateTime, 8, System.Data.ParameterDirection.Input, true, 0, 0, "VIKContract", DataRowVersion.Current, null));
			if(VIKContract!=Constants.DateMax)
				cmd.Parameters["@VIKContract"].Value = VIKContract;
			cmd.Parameters.Add(new System.Data.SqlClient.SqlParameter("@VIKAmount", System.Data.SqlDbType.Money, 4, System.Data.ParameterDirection.Input, true, 19, 0, "VIKAmount", DataRowVersion.Current, null));
			if(VIKAmount>0)
				cmd.Parameters["@VIKAmount"].Value = VIKAmount;
			cmd.Parameters.Add(new System.Data.SqlClient.SqlParameter("@ELEnt", System.Data.SqlDbType.SmallDateTime, 8, System.Data.ParameterDirection.Input, true, 0, 0, "ELEnt", DataRowVersion.Current, null));
			if(ELEnt!=Constants.DateMax)
				cmd.Parameters["@ELEnt"].Value = ELEnt;
			cmd.Parameters.Add(new System.Data.SqlClient.SqlParameter("@ELRec", System.Data.SqlDbType.SmallDateTime, 8, System.Data.ParameterDirection.Input, true, 0, 0, "ELRec", DataRowVersion.Current, null));
			if(ELRec!=Constants.DateMax)
				cmd.Parameters["@ELRec"].Value = ELRec;
			cmd.Parameters.Add(new System.Data.SqlClient.SqlParameter("@ELDate", System.Data.SqlDbType.SmallDateTime, 8, System.Data.ParameterDirection.Input, true, 0, 0, "ELDate", DataRowVersion.Current, null));
			if(ELDate!=Constants.DateMax)
				cmd.Parameters["@ELDate"].Value = ELDate;
			cmd.Parameters.Add(new System.Data.SqlClient.SqlParameter("@ELNumber", System.Data.SqlDbType.NVarChar,100 , System.Data.ParameterDirection.Input, true, 0, 0, "ELNumber", DataRowVersion.Current, null));
			if(ELNumber!="")
				cmd.Parameters["@ELNumber"].Value = ELNumber;
			cmd.Parameters.Add(new System.Data.SqlClient.SqlParameter("@ELProjectAssinged", System.Data.SqlDbType.SmallDateTime, 8, System.Data.ParameterDirection.Input, true, 0, 0, "ELProjectAssinged", DataRowVersion.Current, null));
			if(ELProjectAssinged!=Constants.DateMax)
				cmd.Parameters["@ELProjectAssinged"].Value = ELProjectAssinged;
			cmd.Parameters.Add(new System.Data.SqlClient.SqlParameter("@ELProjectAgreed", System.Data.SqlDbType.SmallDateTime, 8, System.Data.ParameterDirection.Input, true, 0, 0, "ELProjectAgreed", DataRowVersion.Current, null));
			if(ELProjectAgreed!=Constants.DateMax)
				cmd.Parameters["@ELProjectAgreed"].Value = ELProjectAgreed;
			cmd.Parameters.Add(new System.Data.SqlClient.SqlParameter("@ELLetter", System.Data.SqlDbType.SmallDateTime, 8, System.Data.ParameterDirection.Input, true, 0, 0, "ELLetter", DataRowVersion.Current, null));
			if(ELLetter!=Constants.DateMax)
				cmd.Parameters["@ELLetter"].Value = ELLetter;
			cmd.Parameters.Add(new System.Data.SqlClient.SqlParameter("@ELContract", System.Data.SqlDbType.SmallDateTime, 8, System.Data.ParameterDirection.Input, true, 0, 0, "ELContract", DataRowVersion.Current, null));
			if(ELContract!=Constants.DateMax)
				cmd.Parameters["@ELContract"].Value = ELContract;
			cmd.Parameters.Add(new System.Data.SqlClient.SqlParameter("@ELAmount", System.Data.SqlDbType.Money, 4, System.Data.ParameterDirection.Input, true, 19, 0, "ELAmount", DataRowVersion.Current, null));
			if(ELAmount>0)
				cmd.Parameters["@ELAmount"].Value = ELAmount;
			cmd.Parameters.Add(new System.Data.SqlClient.SqlParameter("@TEnt", System.Data.SqlDbType.SmallDateTime, 8, System.Data.ParameterDirection.Input, true, 0, 0, "TEnt", DataRowVersion.Current, null));
			if(TEnt!=Constants.DateMax)
				cmd.Parameters["@TEnt"].Value = TEnt;
			cmd.Parameters.Add(new System.Data.SqlClient.SqlParameter("@TRec", System.Data.SqlDbType.SmallDateTime, 8, System.Data.ParameterDirection.Input, true, 0, 0, "TRec", DataRowVersion.Current, null));
			if(TRec!=Constants.DateMax)
				cmd.Parameters["@TRec"].Value = TRec;
			cmd.Parameters.Add(new System.Data.SqlClient.SqlParameter("@TDate", System.Data.SqlDbType.SmallDateTime, 8, System.Data.ParameterDirection.Input, true, 0, 0, "TDate", DataRowVersion.Current, null));
			if(TDate!=Constants.DateMax)
				cmd.Parameters["@TDate"].Value = TDate;
			cmd.Parameters.Add(new System.Data.SqlClient.SqlParameter("@TNumber", System.Data.SqlDbType.NVarChar,100 , System.Data.ParameterDirection.Input, true, 0, 0, "TNumber", DataRowVersion.Current, null));
			if(TNumber!="")
				cmd.Parameters["@TNumber"].Value = TNumber;
			cmd.Parameters.Add(new System.Data.SqlClient.SqlParameter("@TProjectAssinged", System.Data.SqlDbType.SmallDateTime, 8, System.Data.ParameterDirection.Input, true, 0, 0, "TProjectAssinged", DataRowVersion.Current, null));
			if(TProjectAssinged!=Constants.DateMax)
				cmd.Parameters["@TProjectAssinged"].Value = TProjectAssinged;
			cmd.Parameters.Add(new System.Data.SqlClient.SqlParameter("@TProjectAgreed", System.Data.SqlDbType.SmallDateTime, 8, System.Data.ParameterDirection.Input, true, 0, 0, "TProjectAgreed", DataRowVersion.Current, null));
			if(TProjectAgreed!=Constants.DateMax)
				cmd.Parameters["@TProjectAgreed"].Value = TProjectAgreed;
			cmd.Parameters.Add(new System.Data.SqlClient.SqlParameter("@TLetter", System.Data.SqlDbType.SmallDateTime, 8, System.Data.ParameterDirection.Input, true, 0, 0, "TLetter", DataRowVersion.Current, null));
			if(TLetter!=Constants.DateMax)
				cmd.Parameters["@TLetter"].Value = TLetter;
			cmd.Parameters.Add(new System.Data.SqlClient.SqlParameter("@TContract", System.Data.SqlDbType.SmallDateTime, 8, System.Data.ParameterDirection.Input, true, 0, 0, "TContract", DataRowVersion.Current, null));
			if(TContract!=Constants.DateMax)
				cmd.Parameters["@TContract"].Value = TContract;
			cmd.Parameters.Add(new System.Data.SqlClient.SqlParameter("@TAmount", System.Data.SqlDbType.Money, 4, System.Data.ParameterDirection.Input, true, 19, 0, "TAmount", DataRowVersion.Current, null));
			if(TAmount>0)
				cmd.Parameters["@TAmount"].Value = TAmount;
			cmd.Parameters.Add(new System.Data.SqlClient.SqlParameter("@PathEnt", System.Data.SqlDbType.SmallDateTime, 8, System.Data.ParameterDirection.Input, true, 0, 0, "PathEnt", DataRowVersion.Current, null));
			if(PathEnt!=Constants.DateMax)
				cmd.Parameters["@PathEnt"].Value = PathEnt;
			cmd.Parameters.Add(new System.Data.SqlClient.SqlParameter("@PathRec", System.Data.SqlDbType.SmallDateTime, 8, System.Data.ParameterDirection.Input, true, 0, 0, "PathRec", DataRowVersion.Current, null));
			if(PathRec!=Constants.DateMax)
				cmd.Parameters["@PathRec"].Value = PathRec;
			cmd.Parameters.Add(new System.Data.SqlClient.SqlParameter("@PathDate", System.Data.SqlDbType.SmallDateTime, 8, System.Data.ParameterDirection.Input, true, 0, 0, "PathDate", DataRowVersion.Current, null));
			if(PathDate!=Constants.DateMax)
				cmd.Parameters["@PathDate"].Value = PathDate;
			cmd.Parameters.Add(new System.Data.SqlClient.SqlParameter("@PathNumber", System.Data.SqlDbType.NVarChar,100 , System.Data.ParameterDirection.Input, true, 0, 0, "PathNumber", DataRowVersion.Current, null));
			if(PathNumber!="")
				cmd.Parameters["@PathNumber"].Value = PathNumber;
			cmd.Parameters.Add(new System.Data.SqlClient.SqlParameter("@PathProjectAssinged", System.Data.SqlDbType.SmallDateTime, 8, System.Data.ParameterDirection.Input, true, 0, 0, "PathProjectAssinged", DataRowVersion.Current, null));
			if(PathProjectAssinged!=Constants.DateMax)
				cmd.Parameters["@PathProjectAssinged"].Value = PathProjectAssinged;
			cmd.Parameters.Add(new System.Data.SqlClient.SqlParameter("@PathProjectAgreed", System.Data.SqlDbType.SmallDateTime, 8, System.Data.ParameterDirection.Input, true, 0, 0, "PathProjectAgreed", DataRowVersion.Current, null));
			if(PathProjectAgreed!=Constants.DateMax)
				cmd.Parameters["@PathProjectAgreed"].Value = PathProjectAgreed;
			cmd.Parameters.Add(new System.Data.SqlClient.SqlParameter("@PathAmount", System.Data.SqlDbType.Money, 4, System.Data.ParameterDirection.Input, true, 19, 0, "PathAmount", DataRowVersion.Current, null));
			if(PathAmount>0)
				cmd.Parameters["@PathAmount"].Value = PathAmount;
			cmd.Parameters.Add(new System.Data.SqlClient.SqlParameter("@PhoneEnt", System.Data.SqlDbType.SmallDateTime, 8, System.Data.ParameterDirection.Input, true, 0, 0, "PhoneEnt", DataRowVersion.Current, null));
			if(PhoneEnt!=Constants.DateMax)
				cmd.Parameters["@PhoneEnt"].Value = PhoneEnt;
			cmd.Parameters.Add(new System.Data.SqlClient.SqlParameter("@PhoneRec", System.Data.SqlDbType.SmallDateTime, 8, System.Data.ParameterDirection.Input, true, 0, 0, "PhoneRec", DataRowVersion.Current, null));
			if(PhoneRec!=Constants.DateMax)
				cmd.Parameters["@PhoneRec"].Value = PhoneRec;
			cmd.Parameters.Add(new System.Data.SqlClient.SqlParameter("@PhoneDate", System.Data.SqlDbType.SmallDateTime, 8, System.Data.ParameterDirection.Input, true, 0, 0, "PhoneDate", DataRowVersion.Current, null));
			if(PhoneDate!=Constants.DateMax)
				cmd.Parameters["@PhoneDate"].Value = PhoneDate;
			cmd.Parameters.Add(new System.Data.SqlClient.SqlParameter("@PhoneNumber", System.Data.SqlDbType.NVarChar,100 , System.Data.ParameterDirection.Input, true, 0, 0, "PhoneNumber", DataRowVersion.Current, null));
			if(PhoneNumber!="")
				cmd.Parameters["@PhoneNumber"].Value = PhoneNumber;
			cmd.Parameters.Add(new System.Data.SqlClient.SqlParameter("@IdeaProjectDone", System.Data.SqlDbType.SmallDateTime, 8, System.Data.ParameterDirection.Input, true, 0, 0, "IdeaProjectDone", DataRowVersion.Current, null));
			if(IdeaProjectDone!=Constants.DateMax)
				cmd.Parameters["@IdeaProjectDone"].Value = IdeaProjectDone;
			cmd.Parameters.Add(new System.Data.SqlClient.SqlParameter("@IdeaProjectNotes", System.Data.SqlDbType.NVarChar,2000 , System.Data.ParameterDirection.Input, true, 0, 0, "IdeaProjectNotes", DataRowVersion.Current, null));
			if(IdeaProjectNotes!="")
				cmd.Parameters["@IdeaProjectNotes"].Value = IdeaProjectNotes;
			cmd.Parameters.Add(new System.Data.SqlClient.SqlParameter("@IdeaProjectMun", System.Data.SqlDbType.NVarChar, 100, System.Data.ParameterDirection.Input, true, 0, 0, "IdeaProjectMun", DataRowVersion.Current, null));
			if(IdeaProjectMun!="")
				cmd.Parameters["@IdeaProjectMun"].Value = IdeaProjectMun;
			cmd.Parameters.Add(new System.Data.SqlClient.SqlParameter("@IdeaProjectMunWhere", System.Data.SqlDbType.NVarChar,100 , System.Data.ParameterDirection.Input, true, 0, 0, "IdeaProjectMunWhere", DataRowVersion.Current, null));
			if(IdeaProjectMunWhere!="")
				cmd.Parameters["@IdeaProjectMunWhere"].Value = IdeaProjectMunWhere;
			cmd.Parameters.Add(new System.Data.SqlClient.SqlParameter("@IdeaProjectAgreed", System.Data.SqlDbType.SmallDateTime, 8, System.Data.ParameterDirection.Input, true, 0, 0, "IdeaProjectAgreed", DataRowVersion.Current, null));
			if(IdeaProjectAgreed!=Constants.DateMax)
				cmd.Parameters["@IdeaProjectAgreed"].Value = IdeaProjectAgreed;
			cmd.Parameters.Add(new System.Data.SqlClient.SqlParameter("@TechProjectDone", System.Data.SqlDbType.SmallDateTime, 8, System.Data.ParameterDirection.Input, true, 0, 0, "TechProjectDone", DataRowVersion.Current, null));
			if(TechProjectDone!=Constants.DateMax)
				cmd.Parameters["@TechProjectDone"].Value = TechProjectDone;
			cmd.Parameters.Add(new System.Data.SqlClient.SqlParameter("@TechProjectNotes", System.Data.SqlDbType.NVarChar,2000 , System.Data.ParameterDirection.Input, true, 0, 0, "TechProjectNotes", DataRowVersion.Current, null));
			if(TechProjectNotes!="")
				cmd.Parameters["@TechProjectNotes"].Value = TechProjectNotes;
			cmd.Parameters.Add(new System.Data.SqlClient.SqlParameter("@TechProjectMun", System.Data.SqlDbType.NVarChar, 100, System.Data.ParameterDirection.Input, true, 0, 0, "TechProjectMun", DataRowVersion.Current, null));
			if(TechProjectMun!="")
				cmd.Parameters["@TechProjectMun"].Value = TechProjectMun;
			cmd.Parameters.Add(new System.Data.SqlClient.SqlParameter("@TechProjectMunWhere", System.Data.SqlDbType.NVarChar,100 , System.Data.ParameterDirection.Input, true, 0, 0, "TechProjectMunWhere", DataRowVersion.Current, null));
			if(TechProjectMunWhere!="")
				cmd.Parameters["@TechProjectMunWhere"].Value = TechProjectMunWhere;
			cmd.Parameters.Add(new System.Data.SqlClient.SqlParameter("@TechProjectAgreed", System.Data.SqlDbType.SmallDateTime, 8, System.Data.ParameterDirection.Input, true, 0, 0, "TechProjectAgreed", DataRowVersion.Current, null));
			if(TechProjectAgreed!=Constants.DateMax)
				cmd.Parameters["@TechProjectAgreed"].Value = TechProjectAgreed;
			cmd.Parameters.Add(new System.Data.SqlClient.SqlParameter("@BuildPermission", System.Data.SqlDbType.SmallDateTime, 8, System.Data.ParameterDirection.Input, true, 0, 0, "BuildPermission", DataRowVersion.Current, null));
			if(BuildPermission!=Constants.DateMax)
				cmd.Parameters["@BuildPermission"].Value = BuildPermission;
			cmd.Parameters.Add(new System.Data.SqlClient.SqlParameter("@BuildPermissionDone", System.Data.SqlDbType.SmallDateTime, 8, System.Data.ParameterDirection.Input, true, 0, 0, "BuildPermissionDone", DataRowVersion.Current, null));
			if(BuildPermissionDone!=Constants.DateMax)
				cmd.Parameters["@BuildPermissionDone"].Value = BuildPermissionDone;
			cmd.Parameters.Add(new System.Data.SqlClient.SqlParameter("@BuildPermissionNumber", System.Data.SqlDbType.NVarChar,100 , System.Data.ParameterDirection.Input, true, 0, 0, "BuildPermissionNumber", DataRowVersion.Current, null));
			if(BuildPermissionNumber!="")
				cmd.Parameters["@BuildPermissionNumber"].Value = BuildPermissionNumber;
			cmd.Parameters.Add(new System.Data.SqlClient.SqlParameter("@Other", System.Data.SqlDbType.SmallDateTime, 8, System.Data.ParameterDirection.Input, true, 0, 0, "Other", DataRowVersion.Current, null));
			if(Other!=Constants.DateMax)
				cmd.Parameters["@Other"].Value = Other;
			cmd.Parameters.Add(new System.Data.SqlClient.SqlParameter("@OtherDone", System.Data.SqlDbType.SmallDateTime, 8, System.Data.ParameterDirection.Input, true, 0, 0, "OtherDone", DataRowVersion.Current, null));
			if(OtherDone!=Constants.DateMax)
				cmd.Parameters["@OtherDone"].Value = OtherDone;
			cmd.Parameters.Add(new System.Data.SqlClient.SqlParameter("@OtherNumber", System.Data.SqlDbType.NVarChar,100 , System.Data.ParameterDirection.Input, true, 0, 0, "OtherNumber", DataRowVersion.Current, null));
			if(OtherNumber!="")
				cmd.Parameters["@OtherNumber"].Value = OtherNumber;
			cmd.Parameters.Add(new System.Data.SqlClient.SqlParameter("@OtherExpenses", System.Data.SqlDbType.Money, 4, System.Data.ParameterDirection.Input, true, 19, 0, "OtherExpenses", DataRowVersion.Current, null));
			if(OtherExpenses>0)
				cmd.Parameters["@OtherExpenses"].Value = OtherExpenses;
			cmd.Parameters.Add(new System.Data.SqlClient.SqlParameter("@VIKNotes", System.Data.SqlDbType.NVarChar,2000 , System.Data.ParameterDirection.Input, true, 0, 0, "VIKNotes", DataRowVersion.Current, null));
			if(VIKNotes!="")
				cmd.Parameters["@VIKNotes"].Value = VIKNotes;
			cmd.Parameters.Add(new System.Data.SqlClient.SqlParameter("@ElNotes", System.Data.SqlDbType.NVarChar,2000 , System.Data.ParameterDirection.Input, true, 0, 0, "ElNotes", DataRowVersion.Current, null));
			if(ElNotes!="")
				cmd.Parameters["@ElNotes"].Value = ElNotes;
			cmd.Parameters.Add(new System.Data.SqlClient.SqlParameter("@TNotes", System.Data.SqlDbType.NVarChar,2000 , System.Data.ParameterDirection.Input, true, 0, 0, "TNotes", DataRowVersion.Current, null));
			if(TNotes!="")
				cmd.Parameters["@TNotes"].Value = TNotes;
			cmd.Parameters.Add(new System.Data.SqlClient.SqlParameter("@PathNotes", System.Data.SqlDbType.NVarChar,2000 , System.Data.ParameterDirection.Input, true, 0, 0, "PathNotes", DataRowVersion.Current, null));
			if(PathNotes!="")
				cmd.Parameters["@PathNotes"].Value = PathNotes;
			cmd.Parameters.Add(new System.Data.SqlClient.SqlParameter("@PhoneNotes", System.Data.SqlDbType.NVarChar,2000 , System.Data.ParameterDirection.Input, true, 0, 0, "PhoneNotes", DataRowVersion.Current, null));
			if(PhoneNotes!="")
				cmd.Parameters["@PhoneNotes"].Value = PhoneNotes;
			cmd.Parameters.Add(new System.Data.SqlClient.SqlParameter("@PhoneAmount", System.Data.SqlDbType.Money, 4, System.Data.ParameterDirection.Input, true, 19, 0, "PhoneAmount", DataRowVersion.Current, null));
			if(phoneamount>0)
				cmd.Parameters["@PhoneAmount"].Value = phoneamount;
			cmd.Parameters.Add(new System.Data.SqlClient.SqlParameter("@BuildNotes", System.Data.SqlDbType.NVarChar,2000 , System.Data.ParameterDirection.Input, true, 0, 0, "PathNotes", DataRowVersion.Current, null));
			if(buildnotes!="")
				cmd.Parameters["@BuildNotes"].Value = buildnotes;
			
			return cmd;
		}


		public static void UpdateProjectProfile1(
			int projectID,
			
			object NotaryDeeds, object NotaryDeedsDone, 
			object CurrentStatus, object CurrentStatusDone, 
			object Sketch, object SketchDone, 
			object RegPlan, object RegPlanDone, string RegPlanNumber, 
			object BuildPlan, object BuildPlanDone, string BuildPlanNumber, 
			object PowerOfAttorney, object PowerOfAttorneyDate, object PowerOfAttorneyDone, object PowerOfAttorneyDoneDate, 
			object PreVisa, object PreVisaDone, string PreVisaNumber, 
			object Visa, object VisaDone, string VisaNumber, 
			object VisaPlus, object VisaPlusDone, string VisaPlusNumber, 
			object KadasterUndEnt, object KadasterUndRec, string KadasterUndNumber, 
			object KadasterEnt, object KadasterRec, string KadasterNumber)
		{
			string spName = "ProjectsUpdateProfile1";

			#region Parameters

			SqlParameter[] storedParams = SqlHelperParameterCache.GetSpParameterSet(DBManager.GetConnectionString(), spName);
			storedParams[0].Value = projectID;
			storedParams[1].Value = NotaryDeeds;
			storedParams[2].Value = NotaryDeedsDone; 
			storedParams[3].Value = CurrentStatus;
			storedParams[4].Value = CurrentStatusDone;
			storedParams[5].Value = Sketch; 
			storedParams[6].Value = SketchDone;
			storedParams[7].Value = RegPlan; 
			storedParams[8].Value = RegPlanDone;
			storedParams[9].Value = RegPlanNumber;
			storedParams[10].Value = BuildPlan;
			storedParams[11].Value = BuildPlanDone;
			storedParams[12].Value = BuildPlanNumber;
			storedParams[13].Value = PowerOfAttorney;
			storedParams[14].Value = PowerOfAttorneyDate;
			storedParams[15].Value = PowerOfAttorneyDone;
			storedParams[16].Value = PowerOfAttorneyDoneDate;
			storedParams[17].Value = PreVisa ;
			storedParams[18].Value = PreVisaDone;
			storedParams[19].Value = PreVisaNumber;
			storedParams[20].Value = Visa;
			storedParams[21].Value = VisaDone; 
			storedParams[22].Value = VisaNumber;
			storedParams[23].Value = VisaPlus;
			storedParams[24].Value = VisaPlusDone;
			storedParams[25].Value = VisaPlusNumber;
			storedParams[26].Value = KadasterUndEnt;
			storedParams[27].Value = KadasterUndRec;
			storedParams[28].Value = KadasterUndNumber;
			storedParams[29].Value = KadasterEnt;
			storedParams[30].Value = KadasterRec;
			storedParams[31].Value = KadasterNumber;
			

			#endregion
    
			SqlHelper.ExecuteNonQuery(DBManager.GetConnectionString(), CommandType.StoredProcedure, spName, storedParams);
		}

		public static int SelectProjectNumberByBuildingType(int projectID, int buildingTypeID)
		{
			string spName = "ProjectsSelectProjectNumberByBuildingType";

			SqlParameter[] storedParams = SqlHelperParameterCache.GetSpParameterSet(DBManager.GetConnectionString(), spName, true);
			storedParams[1].Value = projectID <= 0 ? null : (object)projectID;
			storedParams[2].Value = buildingTypeID == 0 ? null : (object)buildingTypeID;

			SqlHelper.ExecuteNonQuery(DBManager.GetConnectionString(), CommandType.StoredProcedure, spName, storedParams);
			return (int)storedParams[0].Value;
		}

		public static int SelectNextNumberForBuildingType(int buildingTypeID)
		{
			string spName = "ProjectsSelectNextNumberForBuildingType";

			SqlParameter[] storedParams = SqlHelperParameterCache.GetSpParameterSet(DBManager.GetConnectionString(), spName, true);
			storedParams[1].Value = buildingTypeID == 0 ? null : (object)buildingTypeID;

			SqlHelper.ExecuteNonQuery(DBManager.GetConnectionString(), CommandType.StoredProcedure, spName, storedParams);
			return (int)storedParams[0].Value;
		}

		public static void SelectProjectCode(int projectID, out int buildingTypeID, out string buildingTypeCode, 
			out int number, out string projectName, out string projectCode, out string folderPath)
		{
			string spName = "ProjectsSelectProjectCode";

			SqlParameter[] storedParams = SqlHelperParameterCache.GetSpParameterSet(DBManager.GetConnectionString(), spName);
			storedParams[0].Value = projectID;

			DataSet ds = SqlHelper.ExecuteDataset(DBManager.GetConnectionString(), CommandType.StoredProcedure, spName, storedParams);
			if (ds.Tables[0].Rows.Count == 0) 
			{
				buildingTypeID = 0;
				buildingTypeCode = "";
				number = -1;
				projectName = "";
				projectCode = "";
				folderPath= "";
				return;
			}
      
			DataRow row = ds.Tables[0].Rows[0];

			buildingTypeID = row.IsNull("BuildingTypeID") ? 0 : (int)row["BuildingTypeID"];
			buildingTypeCode = row.IsNull("BuildingTypeCode") ? "" : ((string)row["BuildingTypeCode"]).Trim();
			number = row.IsNull("Number") ? 0 : (int)row["Number"];
			projectName = (string)row["ProjectName"];
			projectCode = row.IsNull("ProjectCode") ? String.Empty : ((string)row["ProjectCode"]).Trim();
			folderPath = "";
		}

		public static string SelectBuildingTypeCode(int buildingTypeID)
		{
			string spName = "ProjectsSelectBuildingTypeCode";

			SqlParameter[] storedParams = SqlHelperParameterCache.GetSpParameterSet(DBManager.GetConnectionString(), spName);
			storedParams[0].Value = buildingTypeID;

			object o = SqlHelper.ExecuteScalar(DBManager.GetConnectionString(), CommandType.StoredProcedure, spName, storedParams);
			return o == null ? String.Empty : (string)o;
		}

		//notes:this is used in report schedule
		public static void SetOrderNumber(int ProjectID,int OrderNumber)
		{
			string spName = "ProjectSetOrderNumber";

			SqlParameter[] storedParams = SqlHelperParameterCache.GetSpParameterSet(DBManager.GetConnectionString(), spName);
			storedParams[0].Value = ProjectID;
			storedParams[1].Value = (OrderNumber==-1)?null:(object)OrderNumber;

			SqlHelper.ExecuteNonQuery(DBManager.GetConnectionString(),CommandType.StoredProcedure, spName, storedParams);
		}

	
	}
}
