using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Data.SqlClient;
using log4net;
using Asa.Timesheet.Data;
using Asa.Timesheet.Data.Entities;
using Asa.Timesheet.Data.Util;
using GrapeCity.ActiveReports.Export.Pdf.Section;
using GrapeCity.ActiveReports.Export.Excel.Section;

namespace Asa.Timesheet.WebPages.Reports
{
	/// <summary>
	/// Summary description for Chronology.
	/// </summary>
	public class Chronology : TimesheetPageBase
	{
		private static readonly ILog log = LogManager.GetLogger(typeof(Chronology));
		protected System.Web.UI.WebControls.Label lblTitle;
		protected System.Web.UI.WebControls.Label lblUser;
		protected System.Web.UI.WebControls.PlaceHolder menuHolder;
		protected System.Web.UI.WebControls.Label lblError;
		protected System.Web.UI.WebControls.Label lblInfo;
		protected System.Web.UI.WebControls.CheckBox ckDates;
		protected System.Web.UI.WebControls.Label lblStartDay;
		protected System.Web.UI.WebControls.Label lblSelStartDate;
		protected System.Web.UI.WebControls.Calendar calStartDate;
		protected System.Web.UI.WebControls.Label lblEndDay;
		protected System.Web.UI.WebControls.Label lblSelEndDate;
		protected System.Web.UI.WebControls.Calendar calEndDate;
		protected System.Web.UI.WebControls.Label Label9;
		protected System.Web.UI.WebControls.DropDownList ddlProjectsStatus;
		protected System.Web.UI.WebControls.Label Label8;
		protected System.Web.UI.WebControls.DropDownList ddlBuildingTypes;
		protected System.Web.UI.WebControls.Label lblProject;
		protected System.Web.UI.WebControls.DropDownList ddlProject;
		protected System.Web.UI.WebControls.ImageButton ibGenerate;
		protected System.Web.UI.WebControls.ImageButton ibPdfExport;
		protected System.Web.UI.WebControls.ImageButton ibXlsExport;
		protected System.Web.UI.WebControls.Label lblReportTitle;
		protected System.Web.UI.WebControls.Label lbNo;
		protected System.Web.UI.WebControls.DataGrid grdReport;
		protected System.Web.UI.WebControls.Label lblNoDataFound;
		protected System.Web.UI.WebControls.Label lblCreatedByLbl;
		protected System.Web.UI.WebControls.Label lblCreatedBy;
		protected System.Web.UI.WebControls.Label lbProject;
		protected System.Web.UI.WebControls.Label Label7;
		protected System.Web.UI.HtmlControls.HtmlTable tblHeader;
		protected System.Web.UI.WebControls.Label Label2;
		protected System.Web.UI.WebControls.CheckBox ckWorktime;
		protected System.Web.UI.WebControls.CheckBox ckhotIssues;
		protected System.Web.UI.WebControls.CheckBox ckMailing;
		protected System.Web.UI.WebControls.CheckBox ckMeetings;
		protected System.Web.UI.WebControls.CheckBox ckPayments;
		protected System.Web.UI.WebControls.CheckBox ckProtocols;
		protected System.Web.UI.WebControls.CheckBox ckProjectDocuments;
		protected System.Web.UI.WebControls.Label lblSluj;
		protected System.Web.UI.WebControls.DropDownList ddlUser;
		protected System.Web.UI.WebControls.DropDownList ddlUserStatus;
		protected System.Web.UI.WebControls.Label Label10;
		protected System.Web.UI.WebControls.DropDownList ddlCompany;
		protected System.Web.UI.WebControls.CheckBox ckPMS;
		protected Asa.Timesheet.WebPages.UserControls.PageHeader header;
		private enum Columns
		{
			ID=0,
			TypeID,
			Date,
			Item,
			Notes,
			Details,
			PDF
		}
        private void Page_Load(object sender, System.EventArgs e)
        {
            if (!this.LoggedUser.HasPaymentRights) ErrorRedirect(Resource.ResourceManager["noRightsAlert"]);
            if (!this.IsPostBack)
            {
                LoadUsers();
                header.PageTitle = lblReportTitle.Text;
                header.UserName = this.LoggedUser.UserName;






                calStartDate.SelectedDate = calStartDate.VisibleDate = new DateTime(DateTime.Today.Year, DateTime.Today.Month, 1);
                lblSelStartDate.Text = "(" + Resource.ResourceManager["reports_SelectDate"] + ")";
                calEndDate.SelectedDate = calEndDate.VisibleDate = DateTime.Today;
                lblSelEndDate.Text = "(" + Resource.ResourceManager["reports_SelectDate"] + ")";
                UIHelpers.LoadProjectStatus(ddlProjectsStatus, (int)ProjectsData.ProjectsByStatus.Active);
                UIHelpers.LoadBuildingTypes("", ddlBuildingTypes);
                LoadProjects();
            }
            else
                UIHelpers.LoadBuildingTypes(ddlBuildingTypes.SelectedValue, ddlBuildingTypes, (ProjectsData.ProjectsByStatus)int.Parse(ddlProjectsStatus.SelectedValue));



            UIHelpers.CreateMenuReports(menuHolder, LoggedUser);
        }
		public ProjectsData.ProjectsByStatus ProjectsStatus
		{
			get
			{
				try
				{
					return (ProjectsData.ProjectsByStatus)int.Parse(ddlProjectsStatus.SelectedValue);
				}
				catch { return ProjectsData.ProjectsByStatus.AllProjects; }
			}
		}
        private bool LoadProjects()
        {
            SqlDataReader reader = null;

            try
            {
                reader = ProjectsData.SelectProjectNamesClear(this.ProjectsStatus, int.Parse(ddlBuildingTypes.SelectedValue));

                ddlProject.DataSource = reader;
                ddlProject.DataValueField = "ProjectID";
                ddlProject.DataTextField = "ProjectName";
                ddlProject.DataBind();

                ddlProject.Items.Insert(0, new ListItem("<" + Resource.ResourceManager["reports_ddlAllProjects"] + ">", "-1"));
                ddlProject.SelectedValue = "-1";
            }
            catch (Exception ex)
            {
                log.Error(ex);
                return false;
            }

            finally
            {
                if (reader != null) reader.Close();
            }
            return true;
        }
		#region Web Form Designer generated code
        override protected void OnInit(EventArgs e)
        {
            //
            // CODEGEN: This call is required by the ASP.NET Web Form Designer.
            //
            InitializeComponent();
            base.OnInit(e);
        }
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
        private void InitializeComponent()
        {
            this.ckDates.CheckedChanged += new System.EventHandler(this.ckDates_CheckedChanged);
            this.calStartDate.SelectionChanged += new System.EventHandler(this.calStartDate_SelectionChanged);
            this.calEndDate.SelectionChanged += new System.EventHandler(this.calEndDate_SelectionChanged);
            this.ddlProjectsStatus.SelectedIndexChanged += new System.EventHandler(this.ddlProjectsStatus_SelectedIndexChanged);
            this.ddlBuildingTypes.SelectedIndexChanged += new System.EventHandler(this.ddlBuildingTypes_SelectedIndexChanged);
            this.ddlUserStatus.SelectedIndexChanged += new System.EventHandler(this.ddlUserStatus_SelectedIndexChanged);
            this.ibGenerate.Click += new System.Web.UI.ImageClickEventHandler(this.ibGenerate_Click);
            this.ibPdfExport.Click += new System.Web.UI.ImageClickEventHandler(this.ibPdfExport_Click);
            this.ibXlsExport.Click += new System.Web.UI.ImageClickEventHandler(this.ibXlsExport_Click);
            this.grdReport.ItemCommand += new System.Web.UI.WebControls.DataGridCommandEventHandler(this.grdReport_ItemCommand);
            this.Load += new System.EventHandler(this.Page_Load);

        }
		#endregion


        private void ddlProjectsStatus_SelectedIndexChanged(object sender, System.EventArgs e)
        {
            LoadProjects();
        }

        private void ddlBuildingTypes_SelectedIndexChanged(object sender, System.EventArgs e)
        {
            LoadProjects();
        }

        private void ckDates_CheckedChanged(object sender, System.EventArgs e)
        {
            bool bEnable = !ckDates.Checked;
            calStartDate.Enabled = calEndDate.Enabled = bEnable;

            if (bEnable)
            {
                lblSelStartDate.Text = String.Concat("(" + calStartDate.SelectedDate.ToString("d"), ")");

                lblSelEndDate.Text = String.Concat("(" + calEndDate.SelectedDate.ToString("d"), ")");
            }
            else
            {
                lblSelStartDate.Text = lblSelEndDate.Text = String.Concat("(", Resource.ResourceManager["reports_SelectDate"], ")");
            }
        }

        private void calEndDate_SelectionChanged(object sender, System.EventArgs e)
        {
            lblSelEndDate.Text = String.Concat("(", calEndDate.SelectedDates[0].ToString("dd.MM.yyyy"), ")");
        }

        private void calStartDate_SelectionChanged(object sender, System.EventArgs e)
        {
            lblSelStartDate.Text = String.Concat("(", calStartDate.SelectedDates[0].ToString("dd.MM.yyyy"), ")");
            if (calStartDate.SelectedDates.Count > 1)
            {
                calEndDate.SelectedDate = calStartDate.SelectedDates[calStartDate.SelectedDates.Count - 1];
                calEndDate.VisibleDate = calEndDate.SelectedDate;

                calStartDate.SelectedDate = calStartDate.SelectedDates[0];
                lblSelEndDate.Text = String.Concat("(", calEndDate.SelectedDate.ToString("dd.MM.yyyy"), ")"); ;

            }
        }
        private bool LoadUsers()
        {
            //if (this.LoggedUser.IsLeader)
            //{
            ddlUser.Visible = true;

            SqlDataReader reader = null;
            try
            {
                switch (ddlUserStatus.SelectedValue)
                {
                    case "0":
                        reader = UsersData.SelectUserNames(true, true, (LoggedUser.HasPaymentRights || UIHelpers.GetIDParam() != -1)); break;
                    case "1":
                        reader = UsersData.SelectUserNames(false, true, (LoggedUser.HasPaymentRights || UIHelpers.GetIDParam() != -1)); break;
                    case "2":
                        reader = UsersData.SelectUserNames1(); break;
                }
                ddlUser.DataSource = reader;
                ddlUser.DataValueField = "UserID";
                ddlUser.DataTextField = "FullName";
                ddlUser.DataBind();
                ddlUser.Items.Insert(0, new ListItem("<" + Resource.ResourceManager["reports_ddlAllUsers"] + ">", "0"));
                ddlUser.SelectedValue = "0";

            }
            catch (Exception ex)
            {
                log.Error(ex);
                return false;
            }
            finally
            {
                if (reader != null) reader.Close();
            }
            //}

            return true;
        }
        private DataView BindGrid()
        {
            lblCreatedBy.Text = this.LoggedUser.UserName;
            lblReportTitle.Visible = true;
            lblCreatedBy.Visible = true;
            lblCreatedByLbl.Visible = true;

            Label7.Visible = true;
            lbProject.Text = ddlProject.SelectedItem.Text;
            lbProject.Visible = true;

            DateTime dtStart = Constants.DateMin;
            DateTime dtEnd = Constants.DateMax;
            if (!ckDates.Checked)
            {
                dtStart = calStartDate.SelectedDate;
                dtEnd = calEndDate.SelectedDate;
            }
            int nID = int.Parse(ddlProject.SelectedValue);
            if (nID <= 0)
            {
                lblError.Text = Resource.ResourceManager["reports_Minutes_ErrorSelectProject"];
                return null;
            }
            DataView dv = ReportsData.Execute_ChronologyReport(dtStart, dtEnd, nID, ckWorktime.Checked, ckhotIssues.Checked,
                ckMailing.Checked, ckMeetings.Checked, ckPayments.Checked, ckProtocols.Checked, ckProjectDocuments.Checked, int.Parse(ddlUser.SelectedValue), int.Parse(ddlCompany.SelectedValue), ckPMS.Checked);
            grdReport.DataSource = dv;
            grdReport.DataKeyField = "IDD";
            grdReport.DataBind();
            for (int i = 0; i < grdReport.Items.Count; i++)
            {
                int nTypeID = int.Parse(grdReport.Items[i].Cells[(int)Columns.TypeID].Text);
                if (nTypeID == (int)ChronologyTypeID.WorkTime ||
                    nTypeID == (int)ChronologyTypeID.Payment)
                {
                    grdReport.Items[i].Cells[(int)Columns.Details].Controls[1].Visible = false;
                    grdReport.Items[i].Cells[(int)Columns.PDF].Controls[1].Visible = false;
                }
                if (nTypeID == (int)ChronologyTypeID.HotIssue ||
                    nTypeID == (int)ChronologyTypeID.PMS)
                {
                    grdReport.Items[i].Cells[(int)Columns.PDF].Controls[1].Visible = false;
                }
                int nDID = int.Parse(grdReport.Items[i].Cells[(int)Columns.ID].Text);
                if (nTypeID == (int)ChronologyTypeID.Meeting)
                {
                    string sURL = GetURL(nDID);
                    if (sURL == null)
                        grdReport.Items[i].Cells[(int)Columns.PDF].Controls[1].Visible = false;
                }
                else if (nTypeID == (int)ChronologyTypeID.Protocolo)
                {
                    ProtokolData pd = ProtokolDAL.Load(nDID);
                    if (pd != null && pd.HasScan)
                    { }
                    else
                    {
                        grdReport.Items[i].Cells[(int)Columns.PDF].Controls[1].Visible = false;
                    }
                }
                else if (nTypeID == (int)ChronologyTypeID.Mailing)
                {
                    MailingData md = MailingDAL.Load(nDID);
                    if (md != null && md.HasScan)
                    { }
                    else
                    {
                        grdReport.Items[i].Cells[(int)Columns.PDF].Controls[1].Visible = false;
                    }
                }
            }
            return dv;
        }
        private void ibGenerate_Click(object sender, System.Web.UI.ImageClickEventArgs e)
        {
            BindGrid();
        }

        private void grdReport_ItemCommand(object source, System.Web.UI.WebControls.DataGridCommandEventArgs e)
        {
            ImageButton ib = (ImageButton)e.CommandSource;
            string s = ib.ID;
            if (ib.ID == "ibEditItem")
            {
                int nID = int.Parse(e.Item.Cells[(int)Columns.ID].Text);
                int nTypeID = int.Parse(e.Item.Cells[(int)Columns.TypeID].Text);
                ChronologyTypeID type = (ChronologyTypeID)nTypeID;
                switch (type)
                {
                    case ChronologyTypeID.HotIssue:
                        {
                            HotIssueData hid = HotIssueDAL.Load(nID);
                            if (hid != null)
                            {
                                HotIssuesVector hi = HotIssueDAL.LoadCollectionOldIssues(hid.HotIssueIDMain, true);
                                if (hi.Count > 0 && hi[0].IsActive)
                                    Response.Redirect("../EditHotIssue.aspx" + "?id=" + hi[0].HotIssueID.ToString());
                            }
                        }
                        break;
                    case ChronologyTypeID.Mailing:
                        Response.Redirect("../EditCorespondency.aspx?id=" + nID.ToString());
                        break;
                    case ChronologyTypeID.Meeting:
                        Response.Redirect("../EditMeeting.aspx?id=" + nID.ToString());
                        break;
                    case ChronologyTypeID.Profile:

                        Response.Redirect("../Profile.aspx?id=" + ddlProject.SelectedValue);
                        break;
                    case ChronologyTypeID.Protocolo:
                        Response.Redirect("../EditProtokols.aspx?id=" + nID.ToString());
                        break;
                    case ChronologyTypeID.PMS:

                        FilePMData dat = FilePMDAL.Load(nID);
                        if (dat != null)
                        {
                            ProjectCategoryData pcd = ProjectCategoryDAL.Load(dat.CatetegoryID);
                            string sURL = System.Configuration.ConfigurationManager.AppSettings["InternalURL"];
                            if (pcd != null)
                                Response.Redirect(sURL + "Files.aspx?cid=" + dat.CatetegoryID.ToString() + "&pid=" + pcd.ProjectID.ToString());
                        }
                        break;
                }


            }
            else if (ib.ID == "btnPDF")
            {
                int nID = int.Parse(e.Item.Cells[(int)Columns.ID].Text);
                int nTypeID = int.Parse(e.Item.Cells[(int)Columns.TypeID].Text);
                ChronologyTypeID type = (ChronologyTypeID)nTypeID;
                switch (type)
                {

                    case ChronologyTypeID.Mailing:
                        MailingData md = MailingDAL.Load(nID);
                        if (md != null)
                        {
                            string filePath = /*Server.MapPath(*/string.Concat("../", System.Configuration.ConfigurationManager.AppSettings["ScanPathMailing"], nID, md.Ext);//);

                            Response.Cache.SetExpires(DateTime.Now.AddSeconds(-1));
                            Response.Cache.SetCacheability(HttpCacheability.Private);
                            Response.AppendHeader("content-disposition", "attachment; filename=" + nID.ToString() + md.Ext);
                            //Response.AppendHeader("content-disposition", "attachment; filename="+ID+e.Item.Cells[(int)gridColomns.Type].Text);

                            Response.Charset = "";
                            Response.TransmitFile(filePath);
                            Response.End();
                        }
                        break;
                    case ChronologyTypeID.Meeting:
                        Response.Redirect(GetURL(nID));
                        break;
                    case ChronologyTypeID.Protocolo:
                        Response.Redirect(GetPrURL(nID));
                        break;
                    case ChronologyTypeID.Profile:
                        Response.Redirect(GetProfileURL(nID));
                        break;
                }


            }
        }
        protected string GetPrURL(int ID)
        {
            ProjectProtocolsVector ppv = ProjectProtokolDAL.LoadCollection("ProjectProtokolsSelByProtocolProc", SQLParms.CreateProjectProtokolsSelByProtocolProc(ID));
            int mdID = -1;
            foreach (ProjectProtokolData mdd in ppv)
            {
                mdID = mdd.ProjectProtokolID;
                break;
            }
            return string.Concat("../", System.Configuration.ConfigurationManager.AppSettings["ScanPathProtocol"], mdID, System.Configuration.ConfigurationManager.AppSettings["Extension"]);
            //return Request.MapPath(path);
        }
        protected string GetProfileURL(int ID)
        {

            return string.Concat("../", System.Configuration.ConfigurationManager.AppSettings["ProjectDocs"], ID, System.Configuration.ConfigurationManager.AppSettings["Extension"]);
            //return Request.MapPath(path);
        }
        protected string GetURL(int ID)
        {
            int mdID = -1;
            MeetingDocumentsVector mdv = MeetingDocumentDAL.LoadCollection("MeetingDocumentsSelByMeetingProc", SQLParms.CreateMeetingDocumentsSelByMeetingProc(ID));
            foreach (MeetingDocumentData mdd in mdv)
            {
                mdID = mdd.MeetingDocument;
                break;
            }
            if (mdID == -1)
                return null;
            return string.Concat("../", System.Configuration.ConfigurationManager.AppSettings["ScanPath"], mdID, System.Configuration.ConfigurationManager.AppSettings["Extension"]);
            //return Request.MapPath(path);
        }

        private void ibPdfExport_Click(object sender, System.Web.UI.ImageClickEventArgs e)
        {
            PdfExport pdf = new PdfExport();

            pdf.NeverEmbedFonts = "";


            GrapeCity.ActiveReports.SectionReport report = null;

            try
            {
                DataView dv = BindGrid();
                if (null == dv)
                    return;
                report = new ChronologyReport(dv, GetPeriod(), ddlProject.SelectedItem.Text, true);
                report.Run();
            }
            catch (Exception ex)
            {
                log.Error(ex);
                return;
            }


            System.IO.MemoryStream memoryFile = new System.IO.MemoryStream();
            pdf.Export(report.Document, memoryFile);

            Response.Clear();
            Response.AppendHeader("content-disposition"
                , "attachment; filename=" + "Chronology.pdf");
            Response.ContentType = "application/pdf";

            memoryFile.WriteTo(Response.OutputStream);
            Response.End();
        }
        private string GetPeriod()
        {
            string s = ckDates.Text;
            if (!ckDates.Checked)
            {
                DateTime dtStart = calStartDate.SelectedDate;
                DateTime dtEnd = calEndDate.SelectedDate;
                s = calStartDate.SelectedDate.ToLongDateString() + " - " + calEndDate.SelectedDate.ToLongDateString();
            }
            return s;
        }
        private void ibXlsExport_Click(object sender, System.Web.UI.ImageClickEventArgs e)
        {
            XlsExport pdf = new XlsExport();



            GrapeCity.ActiveReports.SectionReport report = null;

            try
            {
                DataView dv = BindGrid();
                if (null == dv)
                    return;
                report = new ChronologyReport(dv, GetPeriod(), ddlProject.SelectedItem.Text, false);
                report.Run();
            }
            catch (Exception ex)
            {
                log.Error(ex.Message);
                return;
            }

            System.IO.MemoryStream memoryFile = new System.IO.MemoryStream();
            pdf.Export(report.Document, memoryFile);

            Response.Clear();
            Response.AppendHeader("content-disposition"
                , "attachment; filename=" + "Chronology.xls");
            //Response.ContentType = "application/pdf";

            memoryFile.WriteTo(Response.OutputStream);
            Response.End();
        }

        private void ddlUserStatus_SelectedIndexChanged(object sender, System.EventArgs e)
        {
            LoadUsers();
        }

	}
}
