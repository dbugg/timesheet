using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Data.SqlClient;
using log4net;
using Asa.Timesheet.Data;
using Asa.Timesheet.Data.Entities;
using Asa.Timesheet.Data.Util;
using Asa.Timesheet.Data.Reports;
using GrapeCity.ActiveReports.Export.Pdf.Section;
using GrapeCity.ActiveReports.Export.Excel.Section;

namespace Asa.Timesheet.WebPages.Reports
{
	/// <summary>
	/// Summary description for BuildingPercentFinished.
	/// </summary>
	public class BuildingPercentFinished :TimesheetPageBase
	{
		
		
		#region Web controls
		protected System.Web.UI.WebControls.PlaceHolder menuHolder;
		protected System.Web.UI.WebControls.Label lblInfo;
		protected System.Web.UI.WebControls.Label lblError;
		protected Asa.Timesheet.WebPages.UserControls.PageHeader header;
		protected System.Web.UI.WebControls.Label Label2;
		protected System.Web.UI.WebControls.DropDownList ddlProjects;
		protected System.Web.UI.WebControls.DropDownList ddlProjectStatus;
		protected System.Web.UI.WebControls.ImageButton ibGenerate;
		protected System.Web.UI.WebControls.ImageButton ibPdfExport;
		protected System.Web.UI.WebControls.ImageButton ibXlsExport;
		protected System.Web.UI.WebControls.Label lbPhase;
		protected System.Web.UI.WebControls.Label lblReportTitle;
		protected System.Web.UI.WebControls.DataGrid grdReport;
		protected System.Web.UI.WebControls.Label lblCreatedByLbl;
		protected System.Web.UI.WebControls.Label lblCreatedBy;
		protected System.Web.UI.WebControls.Label lblNoDataFound;
		protected System.Web.UI.WebControls.Label Label7;
		protected System.Web.UI.WebControls.DropDownList ddlBuildingTypes;
		protected System.Web.UI.WebControls.DropDownList ddlPhases;
		protected System.Web.UI.WebControls.Label Label1;
		protected System.Web.UI.WebControls.Label lbSubs;
		protected System.Web.UI.WebControls.DataGrid grdSub;
		protected System.Web.UI.WebControls.Label lblNoDataFound1;
		
		private static readonly ILog log = LogManager.GetLogger(typeof(Minutes));
		
		#endregion
	
		#region PageLoad
        private void Page_Load(object sender, System.EventArgs e)
        {
            if (!this.LoggedUser.HasRole) ErrorRedirect(Resource.ResourceManager["noRightsAlert"]);
            UIHelpers.CreateMenuReports(menuHolder, LoggedUser);
            if (Request.Params["subs"] == "1")
            {
                ibPdfExport.Visible = ibXlsExport.Visible = false;
            }
            if (!this.IsPostBack)
            {
                header.UserName = LoggedUser.FullName;
                if (Request.Params["subs"] == "1")
                    header.PageTitle = Resource.ResourceManager["reports_SubsFinished_Title"];
                else
                    header.PageTitle = Resource.ResourceManager["reports_ContentFinished_Title"];
                UIHelpers.LoadProjectStatus(ddlProjectStatus, (int)ProjectsData.ProjectsByStatus.Active);
                UIHelpers.LoadBuildingTypes("", ddlBuildingTypes);
                LoadProjects("0");
                LoadPhases();
            }
            else
                UIHelpers.LoadBuildingTypes(ddlBuildingTypes.SelectedValue, ddlBuildingTypes, (ProjectsData.ProjectsByStatus)int.Parse(ddlProjectStatus.SelectedValue));

        }
		#endregion
		
		#region Bind
        private bool LoadProjects(string selectedValue)
        {
            SqlDataReader reader = null;
            try
            {
                reader = ProjectsData.SelectProjectNamesClear(this.ProjectsStatus, UIHelpers.ToInt(ddlBuildingTypes.SelectedValue));

                ddlProjects.DataSource = reader;
                ddlProjects.DataValueField = "ProjectID";
                ddlProjects.DataTextField = "ProjectName";
                ddlProjects.DataBind();

                ddlProjects.Items.Insert(0, new ListItem("<" + Resource.ResourceManager["reports_ddlAllProjects"] + ">", "0"));
                if (ddlProjects.Items.FindByValue(selectedValue) != null)
                    ddlProjects.SelectedValue = selectedValue;
            }
            catch (Exception ex)
            {
                log.Info(ex);
                return false;
            }
            finally
            {
                if (reader != null) reader.Close();
            }
            return true;
        }
        private void LoadPhases()
        {
            ddlPhases.Items.Add(Resource.ResourceManager["Reports_IdeenProekt"]);
            ddlPhases.Items.Add(Resource.ResourceManager["Reports_TehnicheskiProekt"]);
            ddlPhases.Items.Add(Resource.ResourceManager["ItemWorkProject"]);
            ddlPhases.DataBind();
            ddlPhases.SelectedIndex = 0;
            ddlPhases.Items.Insert(0, new ListItem("<" + Resource.ResourceManager["AllPhases"] + ">", "0"));
            ddlPhases.SelectedIndex = 0;
        }

        private void ddlProjectStatus_SelectedIndexChanged(object sender, System.EventArgs e)
        {
            LoadProjects("0");

        }
		#endregion

		#region Web Form Designer generated code
        override protected void OnInit(EventArgs e)
        {
            //
            // CODEGEN: This call is required by the ASP.NET Web Form Designer.
            //
            InitializeComponent();
            base.OnInit(e);
        }
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
        private void InitializeComponent()
        {
            this.ddlProjectStatus.SelectedIndexChanged += new System.EventHandler(this.ddlProjectStatus_SelectedIndexChanged);
            this.ddlBuildingTypes.SelectedIndexChanged += new System.EventHandler(this.ddlBuildingTypes_SelectedIndexChanged);
            this.ibGenerate.Click += new System.Web.UI.ImageClickEventHandler(this.ibGenerate_Click);
            this.ibPdfExport.Click += new System.Web.UI.ImageClickEventHandler(this.ibPdfExport_Click);
            this.ibXlsExport.Click += new System.Web.UI.ImageClickEventHandler(this.ibXlsExport_Click);
            this.grdSub.ItemDataBound += new System.Web.UI.WebControls.DataGridItemEventHandler(this.grdSub_ItemDataBound);
            this.grdReport.ItemDataBound += new System.Web.UI.WebControls.DataGridItemEventHandler(this.grdReport_ItemDataBound);
            this.Load += new System.EventHandler(this.Page_Load);

        }
		#endregion

		#region EventHandlers
        private void ibGenerate_Click(object sender, System.Web.UI.ImageClickEventArgs e)
        {
            int projectID = UIHelpers.ToInt(ddlProjects.SelectedValue);
            int ActiveStatus = ddlProjectStatus.SelectedIndex;
            int Phase = ddlPhases.SelectedIndex;
            int nType = UIHelpers.ToInt(ddlBuildingTypes.SelectedValue);


            SqlDataReader reader = ReportsData.ReportContentFinished(projectID, (ActiveStatus == 1 || ActiveStatus == 2), ActiveStatus == 1, ActiveStatus == 0, nType);




            grdReport.DataSource = reader;
            grdReport.DataBind();
            if (grdReport.Items.Count == 0)
            {
                lblNoDataFound.Visible = true;
                lblNoDataFound.Text = Resource.ResourceManager["reports_WorkTimes_NoDataMatch"];
                grdReport.Visible = false;
            }
            else
            {
                grdReport.Visible = true;
                lblCreatedBy.Visible = true;
                lblCreatedByLbl.Visible = true;
                lblCreatedBy.Text = LoggedUser.FullName;
            }


            if (ddlPhases.SelectedIndex != 0)
                switch (ddlPhases.SelectedIndex)
                {
                    case 1:
                        {
                            grdReport.Columns[4].Visible = false;
                            grdReport.Columns[5].Visible = false;
                            break;
                        }
                    case 2:
                        {
                            grdReport.Columns[3].Visible = false;
                            grdReport.Columns[5].Visible = false;
                            break;
                        }
                    case 3:
                        {
                            grdReport.Columns[3].Visible = false;
                            grdReport.Columns[4].Visible = false;
                            break;
                        }
                }
            lblReportTitle.Visible = true;
            if (Request.Params["subs"] == "1")
                BindSubs();
            else
            {
                lbSubs.Visible = grdSub.Visible = false;

            }
        }
        protected string GetImage(object Done)
        {
            if (Done == null || Done == System.DBNull.Value)
                return "~/images/delete.png";
            int nDone = (int)Done;
            if (nDone == 1)
                return "~/images/done.gif";
            return "~/images/delete.png";
        }
        private void BindSubs()
        {
            grdSub.DataSource = ReportsData.ExecuteProjectSubcontracterActivitiesListReportProc(int.Parse(ddlProjects.SelectedValue), LoggedUser.HasPaymentRights, this.ProjectsStatus, UIHelpers.ToInt(ddlBuildingTypes.SelectedValue));
            grdSub.DataBind();
            lbSubs.Visible = true;

            if (grdSub.Items.Count == 0)
            {
                grdSub.Visible = false;
                lblNoDataFound1.Visible = true;
                lblNoDataFound1.Text = Resource.ResourceManager["reports_WorkTimes_NoDataMatch"];
            }
            else
                grdSub.Visible = true;
        }
        private void ibPdfExport_Click(object sender, System.Web.UI.ImageClickEventArgs e)
        {
            PdfExport pdf = new PdfExport();

            pdf.NeverEmbedFonts = "";

            int projectID = UIHelpers.ToInt(ddlProjects.SelectedValue);
            int ActiveStatus = ddlProjectStatus.SelectedIndex;
            int Phase = ddlPhases.SelectedIndex;
            int nType = UIHelpers.ToInt(ddlBuildingTypes.SelectedValue);




            DataView dv = ReportsData.ExecuteReportContentFinished(projectID, (ActiveStatus == 1 || ActiveStatus == 2), ActiveStatus == 1, ActiveStatus == 0, nType);


            string projectCode, administrativeName, add;
            decimal area = 0; int clientID = 0;
            bool phases; bool isPUP;
            string clientName = "";
            string projectName = "";
            if (projectID != 0)
            {

                if (ProjectsData.SelectProject(projectID, out projectName, out  projectCode, out administrativeName, out area, out add, out clientID, out phases, out isPUP))
                {
                    ClientData cd = ClientDAL.Load(clientID);
                    clientName = cd.ClientNameAndAll;
                }

            }
            GrapeCity.ActiveReports.SectionReport report = new BuildingPercentFinishedRpt(dv, true, LoggedUser.FullName, ddlPhases.SelectedIndex, projectName, clientName);
            report.PageSettings.Orientation = GrapeCity.ActiveReports.Document.Section.PageOrientation.Portrait;




            report.Run();

            System.IO.MemoryStream memoryFile = new System.IO.MemoryStream();

            pdf.Export(report.Document, memoryFile);

            Response.Clear();
            Response.AppendHeader("content-disposition"
                , "attachment; filename=" + "BuildingPercentFinished.pdf");
            Response.ContentType = "application/pdf";

            memoryFile.WriteTo(Response.OutputStream);
            Response.End();
        }

        private void ibXlsExport_Click(object sender, System.Web.UI.ImageClickEventArgs e)
        {
            XlsExport pdf = new XlsExport();


            int projectID = UIHelpers.ToInt(ddlProjects.SelectedValue);
            int ActiveStatus = ddlProjectStatus.SelectedIndex;
            int Phase = ddlPhases.SelectedIndex;
            int nType = UIHelpers.ToInt(ddlBuildingTypes.SelectedValue);




            DataView dv = ReportsData.ExecuteReportContentFinished(projectID, (ActiveStatus == 1 || ActiveStatus == 2), ActiveStatus == 1, ActiveStatus == 0, nType);
            string projectCode, administrativeName, add;
            decimal area = 0; int clientID = 0;
            bool phases; bool isPUP;
            string clientName = "";
            string projectName = "";
            if (projectID != 0)
            {

                if (ProjectsData.SelectProject(projectID, out projectName, out  projectCode, out administrativeName, out area, out add, out clientID, out phases, out isPUP))
                {
                    ClientData cd = ClientDAL.Load(clientID);
                    clientName = cd.ClientNameAndAll;
                }

            }
            GrapeCity.ActiveReports.SectionReport report = new BuildingPercentFinishedRpt(dv, false, LoggedUser.FullName, ddlPhases.SelectedIndex, projectName, clientName);

            report.Run();
            System.IO.MemoryStream memoryFile = new System.IO.MemoryStream();

            pdf.Export(report.Document, memoryFile);

            Response.Clear();
            Response.AppendHeader("content-disposition"
                , "attachment; filename=" + "BuildingPercentFinished.xls");
            Response.ContentType = "application/pdf";

            memoryFile.WriteTo(Response.OutputStream);
            Response.End();
        }

        private void grdReport_ItemDataBound(object sender, System.Web.UI.WebControls.DataGridItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Item
                || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                if (e.Item.Cells[0].Text == "&nbsp;")
                {
                    string pod = e.Item.Cells[1].Text;
                    int indexCheck = pod.IndexOf(Resource.ResourceManager["dlBuildings"].Substring(0, Resource.ResourceManager["dlBuildings"].Length - 1));
                    if (indexCheck == -1)
                    {
                        e.Item.CssClass = "ReportsTotal";
                    }
                    else
                    {
                        e.Item.CssClass = "ReportsSubTotal";
                    }
                    e.Item.Cells[2].Text = "";
                }
                e.Item.Cells[3].Text += "%";
                e.Item.Cells[4].Text += "%";
                e.Item.Cells[5].Text += "%";
                //				if(e.Item.Cells[2].Text.StartsWith("."))
                //					e.Item.Cells[2].Text = e.Item.Cells[2].Text.Substring(1);
                if (e.Item.Cells[2].Text.Length != 0)
                    if (e.Item.Cells[2].Text.LastIndexOf(".") == e.Item.Cells[2].Text.Length - 1)
                        e.Item.Cells[2].Text = e.Item.Cells[2].Text.Substring(0, e.Item.Cells[2].Text.Length - 1);
            }
        }

        private void ddlBuildingTypes_SelectedIndexChanged(object sender, System.EventArgs e)
        {
            LoadProjects("0");
        }
		#endregion

        private void grdSub_ItemDataBound(object sender, System.Web.UI.WebControls.DataGridItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Item
                || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                if (e.Item.Cells[1].Text == "&nbsp;")
                {
                    string pod = e.Item.Cells[0].Text;
                    if (pod == "&nbsp;")
                    {
                        e.Item.CssClass = "ReportsTotal";
                        e.Item.Cells[3].Text = "";
                        e.Item.Cells[4].Text = "";
                    }
                    else
                    {
                        e.Item.CssClass = "ReportsSubTotal";
                        e.Item.Cells[3].Text = "";
                        e.Item.Cells[4].Text = "";
                    }

                }

                //				if(e.Item.Cells[2].Text.StartsWith("."))
                //					e.Item.Cells[2].Text = e.Item.Cells[2].Text.Substring(1);

            }
        }


		#region Properties

		public ProjectsData.ProjectsByStatus ProjectsStatus
		{
			get
			{
				try
				{
					return  (ProjectsData.ProjectsByStatus)int.Parse(ddlProjectStatus.SelectedValue);
				}
				catch { return ProjectsData.ProjectsByStatus.AllProjects; }
			}
		}

		#endregion
	}
}
