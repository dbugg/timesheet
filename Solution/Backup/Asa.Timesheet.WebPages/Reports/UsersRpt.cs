using System;
using System.Data;
using Asa.Timesheet.Data;
using GrapeCity.ActiveReports;
using GrapeCity.ActiveReports.Controls;
using GrapeCity.ActiveReports.SectionReportModel;
using GrapeCity.ActiveReports.Document.Section;
using GrapeCity.ActiveReports.Document;

namespace Asa.Timesheet.WebPages.Reports
{
    public class UsersRpt : GrapeCity.ActiveReports.SectionReport
	{
		private const string _styleDivision = " font-weight: bold; background-color: SandyBrown; font-size: 9pt;";
		private const string _styleDivisionDefault = "background-color: white; font-size: 9pt;";

        public UsersRpt(DataView dv)
        {
            InitializeComponent();
            this.DataSource = dv;
            TakeBorder(Label1.Border);
            TakeBorder(Label2.Border);
            TakeBorder(Label3.Border);
            TakeBorder(Label4.Border);
            TakeBorder(Label5.Border);
            TakeBorder(Label6.Border);
            TakeBorder(Label7.Border);
            TakeBorder(Label8.Border);
            TakeBorder(Label9.Border);
            TakeBorder(Label10.Border);
            TakeBorder(Label11.Border);
        }

        private void Detail_Format(object sender, System.EventArgs eArgs)
        {
            if (division(this.TextBox1.Text))
            {
                this.TextBox1.Style = _styleDivision;
                this.TextBox1.Size = new System.Drawing.SizeF(15.7f, 0.2f);
                TakeBorder(this.TextBox1.Border);
            }
            else
            {
                this.TextBox1.Style = _styleDivisionDefault;
            }
            TakeBorder(this.TextBox1.Border);
            TakeBorder(this.TextBox2.Border);
            TakeBorder(this.TextBox3.Border);
            TakeBorder(this.TextBox4.Border);
            TakeBorder(this.TextBox5.Border);
            TakeBorder(this.TextBox6.Border);
            TakeBorder(this.TextBox7.Border);
            TakeBorder(this.TextBox8.Border);
            TakeBorder(this.TextBox9.Border);
            TakeBorder(this.TextBox10.Border);
            TakeBorder(this.TextBox11.Border);
        }

        private bool division(string text)
        {
            if (text == Resource.ResourceManager["exportArchitectureDivision"])
                return true;
            if (text == Resource.ResourceManager["exportIngenerDivision"])
                return true;
            if (text == Resource.ResourceManager["exportITDivision"])
                return true;
            if (text == Resource.ResourceManager["exportLayerDivision"])
                return true;
            if (text == Resource.ResourceManager["exportCleanerDivision"])
                return true;
            if (text == Resource.ResourceManager["exportAccDivision"])
                return true;
            if (text == Resource.ResourceManager["exportSecretaryDivision"])
                return true;
            return false;
        }

        private void TakeBorder(GrapeCity.ActiveReports.Border b)
        {
            b.BottomColor = System.Drawing.Color.Black;
            b.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            b.LeftColor = System.Drawing.Color.Black;
            b.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            b.RightColor = System.Drawing.Color.Black;
            b.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            b.TopColor = System.Drawing.Color.Black;
            b.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
        }


		#region ActiveReports Designer generated code



























        public void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(UsersRpt));
            this.Detail = new GrapeCity.ActiveReports.SectionReportModel.Detail();
            this.ReportHeader = new GrapeCity.ActiveReports.SectionReportModel.ReportHeader();
            this.ReportFooter = new GrapeCity.ActiveReports.SectionReportModel.ReportFooter();
            this.Label1 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label2 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label3 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label4 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label5 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label6 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label7 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label8 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label9 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label10 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label11 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label12 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.TextBox1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.TextBox2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.TextBox3 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.TextBox4 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.TextBox5 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.TextBox6 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.TextBox7 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.TextBox8 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.TextBox9 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.TextBox10 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.TextBox11 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.TextBox12 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            ((System.ComponentModel.ISupportInitialize)(this.Label1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label11)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label12)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TextBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TextBox2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TextBox3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TextBox4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TextBox5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TextBox6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TextBox7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TextBox8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TextBox9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TextBox10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TextBox11)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TextBox12)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            // 
            // Detail
            // 
            this.Detail.CanShrink = true;
            this.Detail.ColumnSpacing = 0F;
            this.Detail.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
						this.TextBox1,
						this.TextBox2,
						this.TextBox3,
						this.TextBox4,
						this.TextBox5,
						this.TextBox6,
						this.TextBox7,
						this.TextBox8,
						this.TextBox9,
						this.TextBox10,
						this.TextBox11,
						this.TextBox12});
            this.Detail.Height = 0.2076389F;
            this.Detail.Name = "Detail";
            // 
            // ReportHeader
            // 
            this.ReportHeader.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
						this.Label1,
						this.Label2,
						this.Label3,
						this.Label4,
						this.Label5,
						this.Label6,
						this.Label7,
						this.Label8,
						this.Label9,
						this.Label10,
						this.Label11,
						this.Label12});
            this.ReportHeader.Height = 0.25F;
            this.ReportHeader.Name = "ReportHeader";
            // 
            // ReportFooter
            // 
            this.ReportFooter.Height = 0.25F;
            this.ReportFooter.Name = "ReportFooter";
            // 
            // Label1
            // 
            this.Label1.Height = 0.2F;
            this.Label1.HyperLink = null;
            this.Label1.Left = 0F;
            this.Label1.Name = "Label1";
            this.Label1.Style = "font-size: 9pt; font-weight: bold; text-align: center";
            this.Label1.Text = "???";
            this.Label1.Top = 0F;
            this.Label1.Width = 1.9F;
            // 
            // Label2
            // 
            this.Label2.Height = 0.2F;
            this.Label2.HyperLink = null;
            this.Label2.Left = 1.9F;
            this.Label2.Name = "Label2";
            this.Label2.Style = "font-size: 9pt; font-weight: bold; text-align: center";
            this.Label2.Text = "???????";
            this.Label2.Top = 0F;
            this.Label2.Width = 1.6F;
            // 
            // Label3
            // 
            this.Label3.Height = 0.2F;
            this.Label3.HyperLink = null;
            this.Label3.Left = 3.5F;
            this.Label3.Name = "Label3";
            this.Label3.Style = "font-size: 9pt; font-weight: bold; text-align: center";
            this.Label3.Text = "??????";
            this.Label3.Top = 0F;
            this.Label3.Width = 1.7F;
            // 
            // Label4
            // 
            this.Label4.Height = 0.2F;
            this.Label4.HyperLink = null;
            this.Label4.Left = 5.2F;
            this.Label4.Name = "Label4";
            this.Label4.Style = "font-size: 9pt; font-weight: bold; text-align: center";
            this.Label4.Text = "??.????";
            this.Label4.Top = 0F;
            this.Label4.Width = 1.8F;
            // 
            // Label5
            // 
            this.Label5.Height = 0.2F;
            this.Label5.HyperLink = null;
            this.Label5.Left = 7F;
            this.Label5.Name = "Label5";
            this.Label5.Style = "font-size: 9pt; font-weight: bold; text-align: center";
            this.Label5.Text = "???? ? ?????????";
            this.Label5.Top = 0F;
            this.Label5.Width = 1.8F;
            // 
            // Label6
            // 
            this.Label6.Height = 0.2F;
            this.Label6.HyperLink = null;
            this.Label6.Left = 8.8F;
            this.Label6.Name = "Label6";
            this.Label6.Style = "font-size: 9pt; font-weight: bold; text-align: center";
            this.Label6.Text = "????????";
            this.Label6.Top = 0F;
            this.Label6.Width = 0.8F;
            // 
            // Label7
            // 
            this.Label7.Height = 0.2F;
            this.Label7.HyperLink = null;
            this.Label7.Left = 9.6F;
            this.Label7.Name = "Label7";
            this.Label7.Style = "font-size: 9pt; font-weight: bold; text-align: center";
            this.Label7.Text = "????";
            this.Label7.Top = 0F;
            this.Label7.Width = 0.8F;
            // 
            // Label8
            // 
            this.Label8.Height = 0.2F;
            this.Label8.HyperLink = null;
            this.Label8.Left = 10.4F;
            this.Label8.Name = "Label8";
            this.Label8.Style = "font-size: 9pt; font-weight: bold; text-align: center";
            this.Label8.Text = "????. ???????";
            this.Label8.Top = 0F;
            this.Label8.Width = 1.1F;
            // 
            // Label9
            // 
            this.Label9.Height = 0.2F;
            this.Label9.HyperLink = null;
            this.Label9.Left = 11.5F;
            this.Label9.Name = "Label9";
            this.Label9.Style = "font-size: 9pt; font-weight: bold; text-align: center";
            this.Label9.Text = "???. ???????";
            this.Label9.Top = 0F;
            this.Label9.Width = 1.1F;
            // 
            // Label10
            // 
            this.Label10.Height = 0.2F;
            this.Label10.HyperLink = null;
            this.Label10.Left = 12.6F;
            this.Label10.Name = "Label10";
            this.Label10.Style = "font-size: 9pt; font-weight: bold; text-align: center";
            this.Label10.Text = "???. ???????";
            this.Label10.Top = 0F;
            this.Label10.Width = 1.1F;
            // 
            // Label11
            // 
            this.Label11.Height = 0.2F;
            this.Label11.HyperLink = null;
            this.Label11.Left = 13.7F;
            this.Label11.Name = "Label11";
            this.Label11.Style = "font-size: 9pt; font-weight: bold; text-align: center";
            this.Label11.Text = "?????? CV";
            this.Label11.Top = 0F;
            this.Label11.Width = 2F;
            // 
            // Label12
            // 
            this.Label12.Height = 0.2F;
            this.Label12.HyperLink = null;
            this.Label12.Left = 15.7F;
            this.Label12.Name = "Label12";
            this.Label12.Style = "color: White";
            this.Label12.Text = ".";
            this.Label12.Top = 0F;
            this.Label12.Width = 0.1F;
            // 
            // TextBox1
            // 
            this.TextBox1.CanGrow = false;
            this.TextBox1.DataField = "FirstName";
            this.TextBox1.Height = 0.2F;
            this.TextBox1.Left = 0F;
            this.TextBox1.Name = "TextBox1";
            this.TextBox1.Style = "font-size: 9pt; ddo-char-set: 1";
            this.TextBox1.Text = "TextBox1";
            this.TextBox1.Top = 0F;
            this.TextBox1.Width = 1.9F;
            // 
            // TextBox2
            // 
            this.TextBox2.CanGrow = false;
            this.TextBox2.DataField = "LastName";
            this.TextBox2.Height = 0.2F;
            this.TextBox2.Left = 1.9F;
            this.TextBox2.Name = "TextBox2";
            this.TextBox2.Style = "font-size: 9pt; ddo-char-set: 1";
            this.TextBox2.Text = "TextBox2";
            this.TextBox2.Top = 0F;
            this.TextBox2.Width = 1.6F;
            // 
            // TextBox3
            // 
            this.TextBox3.CanGrow = false;
            this.TextBox3.DataField = "Account";
            this.TextBox3.Height = 0.2F;
            this.TextBox3.Left = 3.5F;
            this.TextBox3.Name = "TextBox3";
            this.TextBox3.Style = "font-size: 9pt; ddo-char-set: 1";
            this.TextBox3.Text = "TextBox3";
            this.TextBox3.Top = 0F;
            this.TextBox3.Width = 1.7F;
            // 
            // TextBox4
            // 
            this.TextBox4.CanGrow = false;
            this.TextBox4.DataField = "Mail";
            this.TextBox4.Height = 0.2F;
            this.TextBox4.Left = 5.2F;
            this.TextBox4.Name = "TextBox4";
            this.TextBox4.Style = "font-size: 9pt; ddo-char-set: 1";
            this.TextBox4.Text = "TextBox4";
            this.TextBox4.Top = 0F;
            this.TextBox4.Width = 1.8F;
            // 
            // TextBox5
            // 
            this.TextBox5.CanGrow = false;
            this.TextBox5.DataField = "RoleName";
            this.TextBox5.Height = 0.2F;
            this.TextBox5.Left = 7F;
            this.TextBox5.Name = "TextBox5";
            this.TextBox5.Style = "font-size: 9pt; ddo-char-set: 1";
            this.TextBox5.Text = "TextBox5";
            this.TextBox5.Top = 0F;
            this.TextBox5.Width = 1.8F;
            // 
            // TextBox6
            // 
            this.TextBox6.CanGrow = false;
            this.TextBox6.DataField = "Occupation";
            this.TextBox6.Height = 0.2F;
            this.TextBox6.Left = 8.8F;
            this.TextBox6.Name = "TextBox6";
            this.TextBox6.Style = "font-size: 9pt; ddo-char-set: 1";
            this.TextBox6.Text = "TextBox6";
            this.TextBox6.Top = 0F;
            this.TextBox6.Width = 0.8F;
            // 
            // TextBox7
            // 
            this.TextBox7.CanGrow = false;
            this.TextBox7.DataField = "UserRole";
            this.TextBox7.Height = 0.2F;
            this.TextBox7.Left = 9.6F;
            this.TextBox7.Name = "TextBox7";
            this.TextBox7.Style = "font-size: 9pt; ddo-char-set: 1";
            this.TextBox7.Text = "TextBox7";
            this.TextBox7.Top = 0F;
            this.TextBox7.Width = 0.8F;
            // 
            // TextBox8
            // 
            this.TextBox8.CanGrow = false;
            this.TextBox8.DataField = "Ext";
            this.TextBox8.Height = 0.2F;
            this.TextBox8.Left = 10.4F;
            this.TextBox8.Name = "TextBox8";
            this.TextBox8.Style = "font-size: 9pt; ddo-char-set: 1";
            this.TextBox8.Text = "TextBox8";
            this.TextBox8.Top = 0F;
            this.TextBox8.Width = 1.1F;
            // 
            // TextBox9
            // 
            this.TextBox9.CanGrow = false;
            this.TextBox9.DataField = "Mobile";
            this.TextBox9.Height = 0.2F;
            this.TextBox9.Left = 11.5F;
            this.TextBox9.Name = "TextBox9";
            this.TextBox9.Style = "font-size: 9pt; ddo-char-set: 1";
            this.TextBox9.Text = "TextBox9";
            this.TextBox9.Top = 0F;
            this.TextBox9.Width = 1.1F;
            // 
            // TextBox10
            // 
            this.TextBox10.CanGrow = false;
            this.TextBox10.DataField = "HomePhone";
            this.TextBox10.Height = 0.2F;
            this.TextBox10.Left = 12.6F;
            this.TextBox10.Name = "TextBox10";
            this.TextBox10.Style = "font-size: 9pt; ddo-char-set: 1";
            this.TextBox10.Text = "TextBox10";
            this.TextBox10.Top = 0F;
            this.TextBox10.Width = 1.1F;
            // 
            // TextBox11
            // 
            this.TextBox11.CanGrow = false;
            this.TextBox11.DataField = "CV";
            this.TextBox11.Height = 0.2F;
            this.TextBox11.Left = 13.7F;
            this.TextBox11.Name = "TextBox11";
            this.TextBox11.Style = "font-size: 9pt; ddo-char-set: 1";
            this.TextBox11.Text = "TextBox11";
            this.TextBox11.Top = 0F;
            this.TextBox11.Width = 2F;
            // 
            // TextBox12
            // 
            this.TextBox12.Height = 0.2F;
            this.TextBox12.Left = 15.7F;
            this.TextBox12.Name = "TextBox12";
            this.TextBox12.Style = "color: White; font-size: 10pt";
            this.TextBox12.Text = ".";
            this.TextBox12.Top = 0F;
            this.TextBox12.Width = 0.1F;
            // 
            // ActiveReport1
            // 
            this.MasterReport = false;
            this.PageSettings.PaperHeight = 11F;
            this.PageSettings.PaperWidth = 8.5F;
            this.PrintWidth = 16.5F;
            this.Sections.Add(this.ReportHeader);
            this.Sections.Add(this.Detail);
            this.Sections.Add(this.ReportFooter);
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule(resources.GetString("$this.StyleSheet"), "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: inherit; font-style: inherit; font-variant: inherit; font-weight: bo" +
                        "ld; font-size: 16pt; font-size-adjust: inherit; font-stretch: inherit", "Heading1", "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: Times New Roman; font-style: italic; font-variant: inherit; font-wei" +
                        "ght: bold; font-size: 14pt; font-size-adjust: inherit; font-stretch: inherit", "Heading2", "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: inherit; font-style: inherit; font-variant: inherit; font-weight: bo" +
                        "ld; font-size: 13pt; font-size-adjust: inherit; font-stretch: inherit", "Heading3", "Normal"));
            ((System.ComponentModel.ISupportInitialize)(this.Label1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label11)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label12)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TextBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TextBox2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TextBox3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TextBox4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TextBox5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TextBox6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TextBox7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TextBox8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TextBox9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TextBox10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TextBox11)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TextBox12)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();

            // Attach Report Events
            this.Detail.Format += new System.EventHandler(this.Detail_Format);
        }

		#endregion

        private ReportHeader ReportHeader;
        private Label Label1;
        private Label Label2;
        private Label Label3;
        private Label Label4;
        private Label Label5;
        private Label Label6;
        private Label Label7;
        private Label Label8;
        private Label Label9;
        private Label Label10;
        private Label Label11;
        private Label Label12;
        private Detail Detail;
        private TextBox TextBox1;
        private TextBox TextBox2;
        private TextBox TextBox3;
        private TextBox TextBox4;
        private TextBox TextBox5;
        private TextBox TextBox6;
        private TextBox TextBox7;
        private TextBox TextBox8;
        private TextBox TextBox9;
        private TextBox TextBox10;
        private TextBox TextBox11;
        private TextBox TextBox12;
        private ReportFooter ReportFooter;
	}
}
