using System;
using System.Configuration;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Data.SqlClient;
using log4net;
using Asa.Timesheet.Data;
using Asa.Timesheet.Data.Entities;
using Asa.Timesheet.WebPages.Reports;

using System.IO;
using GrapeCity.ActiveReports.Export.Excel.Section;

namespace Asa.Timesheet.WebPages
{
	/// <summary>
	/// Summary description for Users.
	/// </summary>
	public class Users : TimesheetPageBase
	{

		#region Web controls

		protected System.Web.UI.WebControls.PlaceHolder menuHolder;
		protected System.Web.UI.WebControls.Label lblInfo;
		protected System.Web.UI.WebControls.Panel grid;
		protected System.Web.UI.WebControls.Button btnNewUser;
		protected Asa.Timesheet.WebPages.UserControls.PageHeader header;
		protected System.Web.UI.WebControls.Label lblError;
		protected Telerik.WebControls.RadGrid gridCalls;
		protected System.Web.UI.WebControls.DataGrid grdUsersArchitect;
		protected System.Web.UI.WebControls.DataGrid grdUsersIngener;
		protected System.Web.UI.WebControls.DataGrid grdUsersIT;
		protected System.Web.UI.WebControls.DataGrid grdUsers;
		protected System.Web.UI.WebControls.DataGrid grdUsersACC;
		protected System.Web.UI.WebControls.DataGrid grdUsersSecretary;
		protected System.Web.UI.WebControls.DataGrid grdUsersCleaner;
		protected System.Web.UI.WebControls.DataGrid grdUsersLowers;
		protected System.Web.UI.WebControls.Label lbSectionArchitects;
		protected System.Web.UI.WebControls.Label lbSectionIngener;
		protected System.Web.UI.WebControls.Label lbSectionIT;
		protected System.Web.UI.WebControls.Label lbSectionACC;
		protected System.Web.UI.WebControls.Label lbSectionSecretary;
		protected System.Web.UI.WebControls.Label lbSectionCleaner;
		protected System.Web.UI.WebControls.Label lbSectionLowers;
		protected System.Web.UI.WebControls.Label lbExEmploy;
		protected System.Web.UI.WebControls.DataGrid grdExEmploy;
		private static readonly ILog log = LogManager.GetLogger(typeof(Users));
		private const int salariesColomn = 14;
		protected SalaryTypesVector _salaryTypes=null;
		protected System.Web.UI.WebControls.Button btnShowOldUsers;
		protected System.Web.UI.WebControls.Button btnExport;
		protected System.Web.UI.WebControls.Button btnExportSalaries;
		protected System.Web.UI.WebControls.DropDownList ddlExport;
		protected System.Web.UI.WebControls.Label lbSectionDrivers;
		protected System.Web.UI.WebControls.DataGrid grdUsersDrivers;
		protected System.Web.UI.WebControls.DropDownList ddlRoles;
		protected SalariesVector _salaries=null;

		#endregion
		
		#region Grid columns

		private enum GridColumns
		{
			RestoreItem=0,
			Number,
			FirstName,
			LastName,
			Ext,
			Mobile,
			MobilePersonal,
			Phone,
			Email,
			Private,
			RoleName,
			EditItem,
			DeleteItem
		}

		private enum Months
		{
			Jan=1,
			Feb=2,
			March=3,
			April=4,
			May=5,
			June=6,
			July=7,
			Aug=8,
			Sept=9,
			Oct=10,
			Nov=11,
			Dec=12

		}  
		
		#endregion
	
		#region PageLoad
        private void Page_Load(object sender, System.EventArgs e)
        {
            //if (! (LoggedUser.IsLeader ||  LoggedUser.IsAdmin)) ErrorRedirect(Resource.ResourceManager["noRightsAlert"]);

            UIHelpers.CreateMenu(menuHolder, LoggedUser);
            if (!(LoggedUser.HasPaymentRights || LoggedUser.IsAdmin || LoggedUser.IsSecretary ))
                btnNewUser.Visible = false;
            if (!(LoggedUser.HasPaymentRights && !LoggedUser.IsAccountantLow))
                btnExportSalaries.Visible = false;
            //Add by Ivailo
            //date 03.12.2007
            //notes:label(titles of section) after some event are visible=true, here set them the true value
            HideLabels();
            if (!this.IsPostBack)
            {
                header.PageTitle = Resource.ResourceManager["users_TitleLabel"];
                header.UserName = LoggedUser.UserName;
                btnNewUser.Text = Resource.ResourceManager["users_btnNewUser"];
                SortOrder = 1;
                LoadDDLRole();
                BindGrid();
                LoadDDL();
            }

        }
		//created by Ivailo
		//date 03.12.2007
		//notes:set section titles to be visible equals to their grids
        private void HideLabels()
        {
            lbSectionDrivers.Visible = grdUsersDrivers.Visible;
        }

        private bool LoadDDLRole()
        {
            SqlDataReader reader = null;

            try
            {
                reader = DBManager.SelectRoles();
                ddlRoles.DataSource = reader;
                ddlRoles.DataValueField = "RoleID";
                ddlRoles.DataTextField = "Role";
                ddlRoles.DataBind();

                ddlRoles.Items.Insert(0, new ListItem(Resource.ResourceManager["ddlRole_allRoles"], "0"));
                ddlRoles.SelectedValue = "0";

                //IfLoggedUserIsNotLeader(selectValue);


            }
            catch (Exception ex)
            {
                log.Error(ex);
                return false;

            }
            finally
            {
                if (reader != null) reader.Close();
            }

            return true;
        }
		#endregion

		#region Web Form Designer generated code
        override protected void OnInit(EventArgs e)
        {
            //
            // CODEGEN: This call is required by the ASP.NET Web Form Designer.
            //
            InitializeComponent();
            base.OnInit(e);
        }
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
        private void InitializeComponent()
        {
            //this.ddlRoles.SelectedIndexChanged += new System.EventHandler(this.ddlRoles_SelectedIndexChanged);
            this.grdUsersArchitect.ItemCreated += new System.Web.UI.WebControls.DataGridItemEventHandler(this.grdUsers_ItemCreated);
            this.grdUsersArchitect.ItemCommand += new System.Web.UI.WebControls.DataGridCommandEventHandler(this.grdUsers_ItemCommand);
            this.grdUsersIngener.ItemCreated += new System.Web.UI.WebControls.DataGridItemEventHandler(this.grdUsers_ItemCreated);
            this.grdUsersIngener.ItemCommand += new System.Web.UI.WebControls.DataGridCommandEventHandler(this.grdUsers_ItemCommand);
            this.grdUsersIT.ItemCreated += new System.Web.UI.WebControls.DataGridItemEventHandler(this.grdUsers_ItemCreated);
            this.grdUsersIT.ItemCommand += new System.Web.UI.WebControls.DataGridCommandEventHandler(this.grdUsers_ItemCommand);
            this.grdUsersACC.ItemCreated += new System.Web.UI.WebControls.DataGridItemEventHandler(this.grdUsers_ItemCreated);
            this.grdUsersACC.ItemCommand += new System.Web.UI.WebControls.DataGridCommandEventHandler(this.grdUsers_ItemCommand);
            this.grdUsersSecretary.ItemCreated += new System.Web.UI.WebControls.DataGridItemEventHandler(this.grdUsers_ItemCreated);
            this.grdUsersSecretary.ItemCommand += new System.Web.UI.WebControls.DataGridCommandEventHandler(this.grdUsers_ItemCommand);
            this.grdUsersCleaner.ItemCreated += new System.Web.UI.WebControls.DataGridItemEventHandler(this.grdUsers_ItemCreated);
            this.grdUsersCleaner.ItemCommand += new System.Web.UI.WebControls.DataGridCommandEventHandler(this.grdUsers_ItemCommand);
            this.grdUsersLowers.ItemCreated += new System.Web.UI.WebControls.DataGridItemEventHandler(this.grdUsers_ItemCreated);
            this.grdUsersLowers.ItemCommand += new System.Web.UI.WebControls.DataGridCommandEventHandler(this.grdUsers_ItemCommand);
            this.grdUsersDrivers.ItemCreated += new System.Web.UI.WebControls.DataGridItemEventHandler(this.grdUsers_ItemCreated);
            this.grdUsersDrivers.ItemCommand += new System.Web.UI.WebControls.DataGridCommandEventHandler(this.grdUsers_ItemCommand);
            this.grdExEmploy.ItemCommand += new System.Web.UI.WebControls.DataGridCommandEventHandler(this.grdExEmploy_ItemCommand);
            this.btnNewUser.Click += new System.EventHandler(this.btnNewUser_Click);
            this.btnShowOldUsers.Click += new System.EventHandler(this.btnShowOldUsers_Click);
            this.btnExport.Click += new System.EventHandler(this.btnExport_Click);
            this.btnExportSalaries.Click += new System.EventHandler(this.btnExportSalaries_Click);
            this.Load += new System.EventHandler(this.Page_Load);

        }
		#endregion

		#region database

        private void LoadDDL()
        {
            int OrderCount = 11;
            #region Users
            string resourceName = "UserOrder";
            for (int i = 0; i < OrderCount; i++)
            {
                string UNamesOrder = Resource.ResourceManager[string.Concat(resourceName, i)];
                int UnamesOrderDB = ((i % 2) - ((i + 1) % 2)) * ((i + 1) / 2);
                if (i == 0)
                    UnamesOrderDB = 1;
                ListItem li = new ListItem(UNamesOrder, UnamesOrderDB.ToString());
                ddlExport.Items.Add(li);
            }
            #endregion
            //			#region Salaries
            //			resourceName = "UserOrder";
            //			for(int i=0;i<OrderCount;i++)
            //			{
            //				string UNamesOrder = Resource.ResourceManager[string.Concat(resourceName,i)];
            //				int UnamesOrderDB = ((i%2)-((i+1)%2))*((i+1)/2);
            //				if(i==0)
            //					UnamesOrderDB = 1;
            //				ListItem li = new ListItem(UNamesOrder,UnamesOrderDB.ToString());
            //				ddlExportSalaries.Items.Add(li);
            //			}
            //			#endregion
        }
        private void BindGrid()
        {
            BindArchitects();
            BindIngeners();
            BindIT();
            BindACC();
            BindCleaner();
            BindLowers();
            BindSecretary();
            //Add by Ivailo
            //date 03.12.2007
            //notes: add new section 
            BindDrivers();
            //end add
            BindExEmploys();
        }
        private void BindExEmploys()
        {
            SqlDataReader reader = null;
            try
            {
                string nameOfProcedure = "UsersListExEmploysProc";
                reader = UsersData.SelectUsersExEmploys(Constants.DirectorRole, SortOrder, nameOfProcedure);

                if (reader != null)
                {
                    grdExEmploy.DataSource = reader;
                    grdExEmploy.DataKeyField = "UserID";
                    grdExEmploy.DataBind();
                    if (!LoggedUser.IsLeader)
                        grdUsersArchitect.Columns[(int)GridColumns.DeleteItem].Visible = false;
                }
                else
                {
                    btnShowOldUsers.Visible = grdExEmploy.Visible = lbExEmploy.Visible = false;
                }
                if (!LoggedUser.HasPaymentRights)
                    btnShowOldUsers.Visible = false;
                grdExEmploy.Visible = lbExEmploy.Visible = false;
                btnShowOldUsers.Text = Resource.ResourceManager["ShowExEmp"];
            }
            catch (Exception ex)
            {
                log.Error(ex);
                lblError.Text = Resource.ResourceManager["users_ErrorLoadUsers"];
                return;
            }
            finally
            {
                if (reader != null) reader.Close();
            }

            try
            {
                SetConfirmRestore(grdExEmploy, "btnRestore", Resource.ResourceManager["users_ConfirmDeleteDefaultName"]);
            }
            catch (Exception ex)
            {
                log.Error(ex);
            }
            //			if(grdExEmploy.Items.Count==0)
            //				grdExEmploy.Visible = lbExEmploy.Visible = false;
            //			else 
            //				grdExEmploy.Visible = lbExEmploy.Visible = true;
        }
        private void BindArchitects()
        {
            SqlDataReader reader = null;
            try
            {
                string nameOfProcedure = "UsersListArcgitectProc";
                reader = UsersData.SelectUsersSplit(Constants.DirectorRole, SortOrder, nameOfProcedure, UIHelpers.ToInt(ddlRoles.SelectedValue));

                if (reader != null)
                {
                    grdUsersArchitect.DataSource = reader;
                    grdUsersArchitect.DataKeyField = "UserID";
                    grdUsersArchitect.DataBind();
                    if (!LoggedUser.IsLeader)
                        grdUsersArchitect.Columns[(int)GridColumns.DeleteItem].Visible = false;
                }
                else
                {
                    grdUsersArchitect.Visible = lbSectionArchitects.Visible = false;
                }
            }
            catch (Exception ex)
            {
                log.Error(ex);
                lblError.Text = Resource.ResourceManager["users_ErrorLoadUsers"];
                return;
            }
            finally
            {
                if (reader != null) reader.Close();
            }

            try
            {
                SetConfirmDelete(grdUsersArchitect, "btnDelete", Resource.ResourceManager["users_ConfirmDeleteDefaultName"]);
            }
            catch (Exception ex)
            {
                log.Error(ex);
            }
            if (grdUsersArchitect.Items.Count == 0)
                grdUsersArchitect.Visible = lbSectionArchitects.Visible = false;
            else
                grdUsersArchitect.Visible = lbSectionArchitects.Visible = true;
        }
        private void BindSecretary()
        {
            SqlDataReader reader = null;
            try
            {
                string nameOfProcedure = "UsersListSecretaryProc";
                reader = UsersData.SelectUsersSplit(Constants.DirectorRole, SortOrder, nameOfProcedure, UIHelpers.ToInt(ddlRoles.SelectedValue));

                if (reader != null)
                {
                    grdUsersSecretary.DataSource = reader;
                    grdUsersSecretary.DataKeyField = "UserID";
                    grdUsersSecretary.DataBind();
                    if (!LoggedUser.IsLeader)
                        grdUsersSecretary.Columns[(int)GridColumns.DeleteItem].Visible = false;
                }
                else
                {
                    grdUsersSecretary.Visible = lbSectionSecretary.Visible = false;
                }
            }
            catch (Exception ex)
            {
                log.Error(ex);
                lblError.Text = Resource.ResourceManager["users_ErrorLoadUsers"];
                return;
            }
            finally
            {
                if (reader != null) reader.Close();
            }

            try
            {
                SetConfirmDelete(grdUsersSecretary, "btnDelete", Resource.ResourceManager["users_ConfirmDeleteDefaultName"]);
            }
            catch (Exception ex)
            {
                log.Error(ex);
            }
            if (grdUsersSecretary.Items.Count == 0)
                grdUsersSecretary.Visible = lbSectionSecretary.Visible = false;
            else
                grdUsersSecretary.Visible = lbSectionSecretary.Visible = true;
        }
        private void BindACC()
        {
            SqlDataReader reader = null;
            try
            {
                string nameOfProcedure = "UsersListACCProc";
                reader = UsersData.SelectUsersSplit(Constants.DirectorRole, SortOrder, nameOfProcedure, UIHelpers.ToInt(ddlRoles.SelectedValue));

                if (reader != null)
                {
                    grdUsersACC.DataSource = reader;
                    grdUsersACC.DataKeyField = "UserID";
                    grdUsersACC.DataBind();
                    if (!LoggedUser.IsLeader)
                        grdUsersACC.Columns[(int)GridColumns.DeleteItem].Visible = false;
                }
                else
                {
                    grdUsersACC.Visible = lbSectionACC.Visible = false;
                }
            }
            catch (Exception ex)
            {
                log.Error(ex);
                lblError.Text = Resource.ResourceManager["users_ErrorLoadUsers"];
                return;
            }
            finally
            {
                if (reader != null) reader.Close();
            }

            try
            {
                SetConfirmDelete(grdUsersACC, "btnDelete", Resource.ResourceManager["users_ConfirmDeleteDefaultName"]);
            }
            catch (Exception ex)
            {
                log.Error(ex);
            }
            if (grdUsersACC.Items.Count == 0)
                grdUsersACC.Visible = lbSectionACC.Visible = false;
            else
                grdUsersACC.Visible = lbSectionACC.Visible = true;
        }
        private void BindCleaner()
        {
            SqlDataReader reader = null;
            try
            {
                string nameOfProcedure = "UsersListCleanerProc";
                reader = UsersData.SelectUsersSplit(Constants.DirectorRole, SortOrder, nameOfProcedure, UIHelpers.ToInt(ddlRoles.SelectedValue));

                if (reader != null)
                {
                    grdUsersCleaner.DataSource = reader;
                    grdUsersCleaner.DataKeyField = "UserID";
                    grdUsersCleaner.DataBind();
                    if (!LoggedUser.IsLeader)
                        grdUsersCleaner.Columns[(int)GridColumns.DeleteItem].Visible = false;
                }
                else
                {
                    grdUsersCleaner.Visible = lbSectionCleaner.Visible = false;
                }
            }
            catch (Exception ex)
            {
                log.Error(ex);
                lblError.Text = Resource.ResourceManager["users_ErrorLoadUsers"];
                return;
            }
            finally
            {
                if (reader != null) reader.Close();
            }

            try
            {
                SetConfirmDelete(grdUsersCleaner, "btnDelete", Resource.ResourceManager["users_ConfirmDeleteDefaultName"]);
            }
            catch (Exception ex)
            {
                log.Error(ex);
            }
            if (grdUsersCleaner.Items.Count == 0)
                grdUsersCleaner.Visible = lbSectionCleaner.Visible = false;
            else
                grdUsersCleaner.Visible = lbSectionCleaner.Visible = true;
        }
        private void BindLowers()
        {
            SqlDataReader reader = null;
            try
            {
                string nameOfProcedure = "UsersListLawyerProc";
                reader = UsersData.SelectUsersSplit(Constants.DirectorRole, SortOrder, nameOfProcedure, UIHelpers.ToInt(ddlRoles.SelectedValue));

                if (reader != null)
                {
                    grdUsersLowers.DataSource = reader;
                    grdUsersLowers.DataKeyField = "UserID";
                    grdUsersLowers.DataBind();
                    if (!LoggedUser.IsLeader)
                        grdUsersLowers.Columns[(int)GridColumns.DeleteItem].Visible = false;
                }
                else
                {
                    grdUsersLowers.Visible = lbSectionLowers.Visible = false;
                }
            }
            catch (Exception ex)
            {
                log.Error(ex);
                lblError.Text = Resource.ResourceManager["users_ErrorLoadUsers"];
                return;
            }
            finally
            {
                if (reader != null) reader.Close();
            }

            try
            {
                SetConfirmDelete(grdUsersLowers, "btnDelete", Resource.ResourceManager["users_ConfirmDeleteDefaultName"]);
            }
            catch (Exception ex)
            {
                log.Error(ex);
            }
            if (grdUsersLowers.Items.Count == 0)
                grdUsersLowers.Visible = lbSectionLowers.Visible = false;
            else
                grdUsersLowers.Visible = lbSectionLowers.Visible = true;
        }
		//created by Ivailo
		//date 03.12.2007
		//notes: new section drivers, the code is similar to others section's code
        private void BindDrivers()
        {
            SqlDataReader reader = null;
            try
            {
                string nameOfProcedure = "UsersListDriverProc";
                reader = UsersData.SelectUsersSplit(Constants.DirectorRole, SortOrder, nameOfProcedure, UIHelpers.ToInt(ddlRoles.SelectedValue));

                if (reader != null)
                {
                    grdUsersDrivers.DataSource = reader;
                    grdUsersDrivers.DataKeyField = "UserID";
                    grdUsersDrivers.DataBind();
                    if (!LoggedUser.IsLeader)
                        grdUsersDrivers.Columns[(int)GridColumns.DeleteItem].Visible = false;
                }
                else
                {
                    grdUsersDrivers.Visible = lbSectionDrivers.Visible = false;
                }
                //add by Ivailo
                //date 03.12.2007
                //notes: this is new, it is if reader != null, but there is no users which are drivers
                if (!(grdUsersDrivers.Items.Count > 0))
                {
                    grdUsersDrivers.Visible = lbSectionDrivers.Visible = false;
                }
                else
                {
                    grdUsersDrivers.Visible = lbSectionDrivers.Visible = true;
                }
                //end add
            }
            catch (Exception ex)
            {
                log.Error(ex);
                lblError.Text = Resource.ResourceManager["users_ErrorLoadUsers"];
                return;
            }
            finally
            {
                if (reader != null) reader.Close();
            }

            try
            {
                SetConfirmDelete(grdUsersDrivers, "btnDelete", Resource.ResourceManager["users_ConfirmDeleteDefaultName"]);
            }
            catch (Exception ex)
            {
                log.Error(ex);
            }
            if (grdUsersDrivers.Items.Count == 0)
                grdUsersDrivers.Visible = lbSectionDrivers.Visible = false;
            else
                grdUsersDrivers.Visible = lbSectionDrivers.Visible = true;
        }

        private void BindIngeners()
        {
            SqlDataReader reader = null;
            try
            {
                string nameOfProcedure = "UsersListIngenerProc";
                reader = UsersData.SelectUsersSplit(Constants.DirectorRole, SortOrder, nameOfProcedure, UIHelpers.ToInt(ddlRoles.SelectedValue));

                if (reader != null)
                {
                    grdUsersIngener.DataSource = reader;
                    grdUsersIngener.DataKeyField = "UserID";
                    grdUsersIngener.DataBind();
                    if (!LoggedUser.IsLeader)
                        grdUsersIngener.Columns[(int)GridColumns.DeleteItem].Visible = false;
                }
                else
                {
                    grdUsersIngener.Visible = lbSectionIngener.Visible = false;
                }
            }
            catch (Exception ex)
            {
                log.Error(ex);
                lblError.Text = Resource.ResourceManager["users_ErrorLoadUsers"];
                return;
            }
            finally
            {
                if (reader != null) reader.Close();
            }

            try
            {
                SetConfirmDelete(grdUsersIngener, "btnDelete", Resource.ResourceManager["users_ConfirmDeleteDefaultName"]);
            }
            catch (Exception ex)
            {
                log.Error(ex);
            }
            if (grdUsersIngener.Items.Count == 0)
                grdUsersIngener.Visible = lbSectionIngener.Visible = false;
            else
                grdUsersIngener.Visible = lbSectionIngener.Visible = true;
        }
        private void BindIT()
        {
            SqlDataReader reader = null;
            try
            {
                string nameOfProcedure = "UsersListITProc";
                reader = UsersData.SelectUsersSplit(Constants.DirectorRole, SortOrder, nameOfProcedure, UIHelpers.ToInt(ddlRoles.SelectedValue));

                if (reader != null)
                {
                    grdUsersIT.DataSource = reader;
                    grdUsersIT.DataKeyField = "UserID";
                    grdUsersIT.DataBind();
                    if (!LoggedUser.IsLeader)
                        grdUsersIT.Columns[(int)GridColumns.DeleteItem].Visible = false;
                }
                else
                {
                    grdUsersIT.Visible = lbSectionIT.Visible = false;
                }
            }
            catch (Exception ex)
            {
                log.Error(ex);
                lblError.Text = Resource.ResourceManager["users_ErrorLoadUsers"];
                return;
            }
            finally
            {
                if (reader != null) reader.Close();
            }

            try
            {
                SetConfirmDelete(grdUsersIT, "btnDelete", Resource.ResourceManager["users_ConfirmDeleteDefaultName"]);
            }
            catch (Exception ex)
            {
                log.Error(ex);
            }

            if (grdUsersIT.Items.Count == 0)
                grdUsersIT.Visible = lbSectionIT.Visible = false;
            else
                grdUsersIT.Visible = lbSectionIT.Visible = true;
        }
        private bool DeleteUser(int userID)
        {
            try
            {
                UsersData.InactiveUser(userID);
                log.Info(string.Format("User {0} has been DELETED by {1}", userID, string.Concat(LoggedUser.FullName, " ( userID ", LoggedUser.UserID, " )")));
            }
            catch (Exception ex)
            {
                log.Error(ex);
                return false;
            }

            return true;
        }


		#endregion

		#region Event handlers

        private void btnShowOldUsers_Click(object sender, System.EventArgs e)
        {
            ShowOldUsers();
        }
        private void ShowOldUsers()
        {
            if (grdExEmploy.Visible)
            {
                lbExEmploy.Visible = grdExEmploy.Visible = false;
                btnShowOldUsers.Text = Resource.ResourceManager["ShowExEmp"];
            }
            else
            {
                lbExEmploy.Visible = grdExEmploy.Visible = true;
                btnShowOldUsers.Text = Resource.ResourceManager["HideExEmp"];
            }
        }
        private void grdExEmploy_ItemCommand(object source, System.Web.UI.WebControls.DataGridCommandEventArgs e)
        {
            if (e.CommandName == "Sort")
            {
                int sortOrder = SortOrder;
                switch ((string)e.CommandArgument)
                {
                    case "FirstName": if (sortOrder == 1) sortOrder = -1; else sortOrder = 1;
                        SortOrder = sortOrder;
                        BindGrid();
                        break;
                    case "LastName": if (sortOrder == 2) sortOrder = -2; else sortOrder = 2;
                        SortOrder = sortOrder;
                        BindGrid(); break;
                    case "Account": if (sortOrder == 3) sortOrder = -3; else sortOrder = 3;
                        SortOrder = sortOrder;
                        BindGrid(); break;
                    case "EMail": if (sortOrder == 4) sortOrder = -4; else sortOrder = 4;
                        SortOrder = sortOrder;
                        BindGrid();
                        break;
                    case "Role": if (sortOrder == 5) sortOrder = -5; else sortOrder = 5;
                        SortOrder = sortOrder;
                        BindGrid();
                        break;
                }
                return;
            }
            else if (e.CommandName == "Edit")
            {
                string sUserID = ((DataGrid)source).DataKeys[e.Item.ItemIndex].ToString();
                Response.Redirect("EditUser.aspx" + "?uid=" + sUserID);
                return;
            }
            if (e.CommandSource is ImageButton)
            {
                ImageButton ib = (ImageButton)e.CommandSource;
                string s = ib.ID;
                switch (ib.ID)
                {
                    case "btnRestore": int userID = (int)((DataGrid)source).DataKeys[e.Item.ItemIndex];
                        UsersData.RestoreUser(userID);
                        log.Info(string.Format("User {0} has been RESTORED by {1}", userID, string.Concat(LoggedUser.FullName, " ( userID ", LoggedUser.UserID, " )")));
                        BindGrid();
                        break;
                    case "btnEdit": string sUserID = ((DataGrid)source).DataKeys[e.Item.ItemIndex].ToString();
                        Response.Redirect("EditUser.aspx" + "?uid=" + sUserID); break;
                }

            }
        }
        private void grdUsers_ItemCommand(object source, System.Web.UI.WebControls.DataGridCommandEventArgs e)
        {
            if (e.CommandName == "Sort")
            {
                int sortOrder = SortOrder;
                switch ((string)e.CommandArgument)
                {
                    case "FirstName": if (sortOrder == 1) sortOrder = -1; else sortOrder = 1;
                        SortOrder = sortOrder;
                        BindGrid();
                        break;
                    case "LastName": if (sortOrder == 2) sortOrder = -2; else sortOrder = 2;
                        SortOrder = sortOrder;
                        BindGrid(); break;
                    case "Account": if (sortOrder == 3) sortOrder = -3; else sortOrder = 3;
                        SortOrder = sortOrder;
                        BindGrid(); break;
                    case "EMail": if (sortOrder == 4) sortOrder = -4; else sortOrder = 4;
                        SortOrder = sortOrder;
                        BindGrid();
                        break;
                    case "Role": if (sortOrder == 5) sortOrder = -5; else sortOrder = 5;
                        SortOrder = sortOrder;
                        BindGrid();
                        break;
                }
                return;
            }
            else if (e.CommandName == "Edit")
            {
                string sUserID = ((DataGrid)source).DataKeys[e.Item.ItemIndex].ToString();
                Response.Redirect("EditUser.aspx" + "?uid=" + sUserID);
                return;
            }
            ImageButton ib = (ImageButton)e.CommandSource;
            string s = ib.ID;
            switch (ib.ID)
            {
                case "btnDelete": int userID = (int)((DataGrid)source).DataKeys[e.Item.ItemIndex];
                    if (!DeleteUser(userID))
                    {
                        lblError.Text = Resource.ResourceManager["editUser_ErrorDelete"];
                        return;
                    };
                    BindGrid();
                    break;
                case "btnEdit": string sUserID = ((DataGrid)source).DataKeys[e.Item.ItemIndex].ToString();
                    Response.Redirect("EditUser.aspx" + "?uid=" + sUserID); break;
            }
        }

        private void grdUsers_ItemCreated(object sender, System.Web.UI.WebControls.DataGridItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Header)
            {
                string imgUrl = "";
                int sortOrder = SortOrder;

                if (sortOrder != 0)
                {
                    imgUrl = (sortOrder > 0) ? "images/sup.gif" : "images/sdown.gif";
                    Label sep = new Label();
                    sep.Width = 2;
                    e.Item.Cells[Math.Abs(sortOrder) + 1].Controls.Add(sep);
                    ImageButton ib = new ImageButton();
                    ib.Height = 11; ib.Width = 11; ib.ImageUrl = imgUrl;
                    e.Item.Cells[Math.Abs(sortOrder) + 1].Controls.Add(ib);
                }
            }
        }

        private void btnNewUser_Click(object sender, System.EventArgs e)
        {
            Response.Redirect("EditUser.aspx");
        }

        private void ddlRoles_SelectedIndexChanged(object sender, System.EventArgs e)
        {
            ChangeRoleFilter();
        }

        private void ChangeRoleFilter()
        {
            BindGrid();
        }

		#endregion

		#region Export

        private void btnExport_Click(object sender, System.EventArgs e)
        {
            //			this.EnableViewState = true;
            //
            //			System.IO.StringWriter tw = new System.IO.StringWriter();
            //			System.Web.UI.HtmlTextWriter hw = new System.Web.UI.HtmlTextWriter(tw);

            DataTable dtAll = new DataTable();

            AllTableExport(out dtAll);



            DataView dv = new DataView(dtAll);
            XlsExport pdf = new XlsExport();

            GrapeCity.ActiveReports.SectionReport report = new UsersRpt(dv);
            report.PageSettings.Orientation = GrapeCity.ActiveReports.Document.Section.PageOrientation.Landscape;
            report.Run();
            System.IO.MemoryStream memoryFile = new System.IO.MemoryStream();
            pdf.Export(report.Document, memoryFile);

            Response.Clear();
            Response.AppendHeader("content-disposition"
                , "attachment; filename=" + "Users.xls");
            Response.ContentType = "application/pdf";

            memoryFile.WriteTo(Response.OutputStream);
            Response.End();

            //			gridCalls.DataSource = dv;
            //			gridCalls.DataKeyField = "UserID";
            //					
            //			gridCalls.AllowSorting=false;
            //					
            //			gridCalls.DataBind();
            //			for(int i=0;i<gridCalls.Columns.Count;i++)
            //			{
            //						
            //				gridCalls.Columns[i].SortExpression=null;
            //			}
            //			
            //
            //		gridCalls.MasterTableView.ExportToExcel("Users");
        }
        private void CreateColoums(out DataTable dtAll)
        {
            TableExport(out dtAll, "UsersListProc");
            for (int i = dtAll.Rows.Count - 1; i > -1; i--)
            {
                dtAll.Rows.RemoveAt(i);
            }
        }
        private bool TableExport(out DataTable dt, string nameOfProcedure)
        {
            dt = new DataTable();
            try
            {
                SortOrder = UIHelpers.ToInt(ddlExport.SelectedValue);
                DataView dv = UsersData.ExecuteUsersDataViewComandProc(Constants.DirectorRole, SortOrder, nameOfProcedure);
                if (dv.Table.Rows.Count > 0)
                {
                    dt = dv.Table;
                    DataRow dr = dt.NewRow();
                    dr["FirstName"] = GetTableTitle(nameOfProcedure);
                    dt.Rows.InsertAt(dr, 0);
                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch (Exception ex)
            {
                log.Error(ex);
                lblError.Text = Resource.ResourceManager["users_ErrorLoadUsers"];
                return false;
            }
        }
        private string GetTableTitle(string nameOfProcedure)
        {
            //created by Ivailo
            //last edit 03.12.2007
            //notes: get procedure name and return the name of the sections
            switch (nameOfProcedure)
            {
                case "UsersListArcgitectProc": return Resource.ResourceManager["exportArchitectureDivision"];
                case "UsersListSecretaryProc": return Resource.ResourceManager["exportSecretaryDivision"];
                case "UsersListACCProc": return Resource.ResourceManager["exportAccDivision"];
                case "UsersListITProc": return Resource.ResourceManager["exportITDivision"];
                case "UsersListIngenerProc": return Resource.ResourceManager["exportIngenerDivision"];
                case "UsersListCleanerProc": return Resource.ResourceManager["exportCleanerDivision"];
                case "UsersListLawyerProc": return Resource.ResourceManager["exportLayerDivision"];
                case "UsersListDriverProc": return Resource.ResourceManager["exportDriverDivision"];
                default: return "";
            }
        }

        private void AllTableExport(out DataTable dtAll)
        {
            dtAll = new DataTable();
            CreateColoums(out dtAll);
            DataTable dt = new DataTable();
            if (TableExport(out dt, "UsersListArcgitectProc"))
            {
                foreach (DataRow dr in dt.Rows)
                {
                    DataRow drAll = dtAll.NewRow();
                    drAll.ItemArray = dr.ItemArray;
                    dtAll.Rows.Add(drAll);
                }
            }
            if (TableExport(out dt, "UsersListIngenerProc"))
            {
                foreach (DataRow dr in dt.Rows)
                {
                    DataRow drAll = dtAll.NewRow();
                    drAll.ItemArray = dr.ItemArray;
                    dtAll.Rows.Add(drAll);
                }
            }
            if (TableExport(out dt, "UsersListITProc"))
            {
                foreach (DataRow dr in dt.Rows)
                {
                    DataRow drAll = dtAll.NewRow();
                    drAll.ItemArray = dr.ItemArray;
                    dtAll.Rows.Add(drAll);
                }
            }
            if (TableExport(out dt, "UsersListACCProc"))
            {
                foreach (DataRow dr in dt.Rows)
                {
                    DataRow drAll = dtAll.NewRow();
                    drAll.ItemArray = dr.ItemArray;
                    dtAll.Rows.Add(drAll);
                }
            }
            if (TableExport(out dt, "UsersListSecretaryProc"))
            {
                foreach (DataRow dr in dt.Rows)
                {
                    DataRow drAll = dtAll.NewRow();
                    drAll.ItemArray = dr.ItemArray;
                    dtAll.Rows.Add(drAll);
                }
            }
            if (TableExport(out dt, "UsersListCleanerProc"))
            {
                foreach (DataRow dr in dt.Rows)
                {
                    DataRow drAll = dtAll.NewRow();
                    drAll.ItemArray = dr.ItemArray;
                    dtAll.Rows.Add(drAll);
                }
            }
            if (TableExport(out dt, "UsersListLawyerProc"))
            {
                foreach (DataRow dr in dt.Rows)
                {
                    DataRow drAll = dtAll.NewRow();
                    drAll.ItemArray = dr.ItemArray;
                    dtAll.Rows.Add(drAll);
                }
            }
            //Add by Ivailo
            //date 03.12.2007
            //notes: new section add
            if (TableExport(out dt, "[UsersListDriverProc]"))
            {
                foreach (DataRow dr in dt.Rows)
                {
                    DataRow drAll = dtAll.NewRow();
                    drAll.ItemArray = dr.ItemArray;
                    dtAll.Rows.Add(drAll);
                }
            }
        }


		#endregion

		#region ExportSalaries

        private void btnExportSalaries_Click(object sender, System.EventArgs e)
        {
            DataSet ds = MakeDataSet();
            DataTable dtCount = new DataTable();
            dtCount.Columns.Add("count");
            for (int i = 0; i < ds.Tables.Count; i++)
            {
                DataRow dr = dtCount.NewRow();
                dr[0] = i.ToString();
                dtCount.Rows.Add(dr);
            }

            XlsExport pdf = new XlsExport();

            GrapeCity.ActiveReports.SectionReport report = new SalariesAllRpt(ds, dtCount);
            report.PageSettings.Orientation = GrapeCity.ActiveReports.Document.Section.PageOrientation.Landscape;
            report.Run();
            System.IO.MemoryStream memoryFile = new System.IO.MemoryStream();
            pdf.Export(report.Document, memoryFile);

            Response.Clear();
            Response.AppendHeader("content-disposition"
                , "attachment; filename=" + "Salary.xls");
            Response.ContentType = "application/pdf";

            memoryFile.WriteTo(Response.OutputStream);
            Response.End();

        }
        private DataSet MakeDataSet()
        {
            string[] names;
            int[] UsersIDs = ActiveUsersIDs(out names);
            int countTableIndex = -1;
            DataTable dtSalariesCountAll = new DataTable();
            DataTable dtSalariesCount = new DataTable();
            _salaryTypes = SalaryTypeDAL.LoadCollection();
            DataSet ds = new DataSet();
            for (int i = 0; i < UsersIDs.Length - 1; i++)
            {
                int UserID = UsersIDs[i];
                DataTable dtSalaries = new DataTable();
                if (UserID < 1)
                {
                    if (countTableIndex != -1)
                    {
                        dtSalariesCountAll = AddSalaries(dtSalariesCount, dtSalariesCountAll);
                        dtSalariesCount.TableName = names[countTableIndex];
                        ds.Tables.Add(dtSalariesCount);
                        dtSalariesCount = new DataTable();
                    }
                    countTableIndex = i;
                }
                else
                    if (BindSalaries(UserID, out dtSalaries))
                    {
                        dtSalariesCount = AddSalaries(dtSalaries, dtSalariesCount);
                        UserInfo ui = UsersData.SelectUserByID(UserID);

                        dtSalaries.TableName = string.Concat(Resource.ResourceManager["SalaryText"], ui.FullName);
                        ds.Tables.Add(dtSalaries);
                    }
            }
            dtSalariesCountAll = AddSalaries(dtSalariesCount, dtSalariesCountAll);
            dtSalariesCount.TableName = names[countTableIndex];
            ds.Tables.Add(dtSalariesCount);
            dtSalariesCountAll.TableName = names[UsersIDs.Length - 1];
            ds.Tables.Add(dtSalariesCountAll);
            return ds;
        }
        private DataTable AddSalaries(DataTable newTable, DataTable AllTable)
        {
            if (AllTable.Columns.Count == 0)
            {
                for (int col = 0; col < newTable.Columns.Count; col++)
                {
                    AllTable.Columns.Add(newTable.Columns[col].ColumnName);
                }
                for (int rows = 0; rows < newTable.Rows.Count; rows++)
                {
                    DataRow dr = AllTable.NewRow();
                    dr.ItemArray = newTable.Rows[rows].ItemArray;
                    AllTable.Rows.Add(dr);
                }
                return AllTable;
            }
            for (int rows = 0; rows < newTable.Rows.Count; rows++)
            {
                for (int col = 1; col < newTable.Columns.Count; col++)
                {
                    decimal all = decimal.Parse(AllTable.Rows[rows][col].ToString());
                    decimal addValue = decimal.Parse(newTable.Rows[rows][col].ToString());
                    decimal newall = all + addValue;
                    AllTable.Rows[rows][col] = newall.ToString();
                }
            }
            return AllTable;
        }
        private int[] ActiveUsersIDs(out string[] names)
        {
            DataTable dtUsers = new DataTable();
            AllTableExport(out dtUsers);
            int[] ActiveUsers = new int[dtUsers.Rows.Count + 1];
            names = new string[dtUsers.Rows.Count + 1];
            for (int i = 0; i < dtUsers.Rows.Count; i++)
            {
                DataRow dr = dtUsers.Rows[i];
                ActiveUsers[i] = UIHelpers.ToInt(dr["UserID"].ToString());
                names[i] = dr["FirstName"].ToString();
            }
            ActiveUsers[dtUsers.Rows.Count] = -1;
            names[dtUsers.Rows.Count] = Resource.ResourceManager["reports_Total"];
            return ActiveUsers;
        }
        private DataTable AddColomns()
        {
            DataTable dt = new DataTable();
            for (int i = 0; i < salariesColomn; i++)
            {
                dt.Columns.Add(i.ToString());
            }
            return dt;
        }
        private bool BindSalaries(int UserID, out DataTable dtSalaries)
        {
            dtSalaries = AddColomns();
            if (UserID > 0)
                _salaries = SalaryUDL.GetSalariesByUser(UserID);
            else
                return false;
            int CurrentYear = DateTime.Now.Year;
            int StartYear = int.Parse(System.Configuration.ConfigurationManager.AppSettings["MinSalaryYear"]);
            for (int j = CurrentYear; j >= StartYear; j--)
            {
                for (int i = 1; i <= Enum.GetNames(typeof(SalaryTypes)).Length; i++)
                {
                    DataRow dr = dtSalaries.NewRow();
                    string Type = GetSalaryType(i);
                    dr[0] = string.Concat(j, " - ", Type);
                    for (int month = 1; month <= 12; month++)
                    {
                        dr[month] = GetSalary(j, i, month);
                    }
                    dr[13] = GetTotal(j, i);
                    dtSalaries.Rows.Add(dr);
                }
            }
            return true;
        }
        protected string GetSalaryType(int Type)
        {

            SalaryTypeData std = _salaryTypes.GetByID(Type);
            if (std != null)
                return std.SalaryType;
            return "";
        }
        protected string GetSalary(int Year, int Type, int Month)
        {
            return UIHelpers.FormatDecimal2(SalaryUDL.GetSalaryByMonthYearType(_salaries, Year, Month, Type));
        }
        protected string GetTotal(int Year, int Type)
        {
            decimal dTotal = 0;
            for (int i = 1; i <= Enum.GetNames(typeof(Months)).Length; i++)
            {
                dTotal += SalaryUDL.GetSalaryByMonthYearType(_salaries, Year, i, Type);
            }
            if ((SalaryTypes)Type != SalaryTypes.Bonus)
                dTotal = dTotal / Enum.GetNames(typeof(Months)).Length;
            return UIHelpers.FormatDecimal2(dTotal);
        }


		#endregion

		

		

		
		
	}
}
