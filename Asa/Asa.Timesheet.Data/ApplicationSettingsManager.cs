using System;
using System.Data;
using System.Configuration;

namespace Asa.Timesheet.Data
{
	/// <summary>
	/// Summary description for ApplicationSettingsManager.
	/// </summary>
	public class SettingsManager
	{

		#region Hours grid settings

		public static int HoursGridInitialRowsCount
		{
			get 
			{   
				string sInitialCount = ConfigurationSettings.AppSettings[Constants.AS_KEY_HoursGridInitialRowsCount];
				int initialCount = Constants.HoursGridInitialRowsCountDefault;

				try
				{
					initialCount = int.Parse(sInitialCount);
				}
				catch (Exception ex)
				{
				}

				if (initialCount > HoursGridMaxRowsCount) initialCount = HoursGridMaxRowsCount;

				return initialCount;

			}
		}

		public static int HoursGridMaxRowsCount
		{
			get 
			{   
				string sMaxCount = ConfigurationSettings.AppSettings[Constants.AS_KEY_HoursGridMaxRowsCount];
				int maxCount = Constants.HoursGridMaxRowsCountDefault;
				
				try
				{
					maxCount = int.Parse(sMaxCount);
				}
				catch (Exception ex)
				{
				}
				
				return maxCount;
			}
		}
		#endregion

		public static int HoursGridDefaultStartHourID
		{
			get 
			{   
				string sStartHourID = ConfigurationSettings.AppSettings[Constants.AS_KEY_HoursGridDefaultStartHourID];
				int startHourID = Constants.HoursGridDefaultStartHourIDDefault;

				try
				{
					startHourID = int.Parse(sStartHourID);
				}
				catch (Exception ex)
				{
				}
				return startHourID;
			}		 
		}

		public static int HoursGridDefaultEndHourID
		{
			get 
			{   
				string sEndHourID = ConfigurationSettings.AppSettings[Constants.AS_KEY_HoursGridDefaultEndHourID];
				int endHourID = Constants.HoursGridDefaultEndHourIDDefault;

				try
				{
					endHourID = int.Parse(sEndHourID);
				}
				catch (Exception ex)
				{
				}
				return endHourID;
			}		 
		}

    public static string ProjectRootFolderPath
    {
      get
      {
        return ConfigurationSettings.AppSettings["ProjectsRootFolderPath"];
      }
    }
	}
}
