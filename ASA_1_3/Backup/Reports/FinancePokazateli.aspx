﻿<%@ Page language="c#" Codebehind="FinancePokazateli.aspx.cs" AutoEventWireup="false" Inherits="Asa.Timesheet.WebPages.Reports.FinancePokazateli"%>
<%@ Register TagPrefix="activereportsweb" Namespace="GrapeCity.ActiveReports.Web" Assembly="GrapeCity.ActiveReports.Web.v7, Version=7.1.7470.0, Culture=neutral, PublicKeyToken=cc4967777c49a3ff" %>
<%@ Register TagPrefix="uc1" TagName="PageHeader" Src="../UserControls/PageHeader.ascx" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
	<head runat="server">
		<title>Finance Pokazateli</title>
		<meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
		<meta content="C#" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<LINK href="../styles/timesheet.css" type="text/css" rel="stylesheet">
		<script language="javascript" src="../PopupCalendar.js"></script>
	</HEAD>
	<body bottomMargin="0" leftMargin="0" topMargin="0" rightMargin="0">
		<form id="Form1" method="post" runat="server">
			<table height="100%" cellSpacing="0" cellPadding="0" width="100%" border="0">
				<tr height="1">
					<td><uc1:pageheader id="header" runat="server"></uc1:pageheader></td>
				</tr>
				<tr style="DISPLAY: none" height="80">
					<td background="../images/copy1.gif">
						<TABLE id="Table2" height="100%" width="100%">
							<TR>
								<td style="WIDTH: 106px">&nbsp;&nbsp;&nbsp;&nbsp; <IMG height="80" alt="" src="../images/logo.jpg" width="64"></TD>
								<td noWrap><asp:label id="lblTitle" runat="server" Font-Bold="True" Font-Size="X-Small"></asp:label><br>
									<asp:label id="lblUser" runat="server" Font-Bold="True" Font-Size="Medium" ForeColor="SaddleBrown"></asp:label></TD>
							</TR>
						</TABLE>
					</td>
				</tr>
				<tr>
					<td>
						<table height="100%" cellSpacing="0" cellPadding="0" width="100%" border="0">
							<tr>
								<td class="menuLeft"><asp:placeholder id="menuHolder" runat="server"></asp:placeholder></td>
								
								<td style="PADDING-LEFT: 5px; PADDING-RIGHT: 5px; PADDING-TOP: 3px" vAlign="top" noWrap>
									<TABLE id="tblInfo" height="30" cellSpacing="0" cellPadding="3" width="880" border="0">
										<TR>
											<td vAlign="middle" noWrap><asp:label id="lblError" runat="server" ForeColor="Red" CssClass="ErrorLabel" EnableViewState="False"></asp:label><asp:label id="lblInfo" runat="server" CssClass="InfoLabel" EnableViewState="False"></asp:label></TD>
										</TR>
									</TABLE>
									<TABLE id="Table3" cellSpacing="0" cellPadding="0" width="880" border="0">
										<TR>
											<td style="HEIGHT: 5px" colSpan="2"></TD>
										</TR>
										<TR style="DISPLAY: none">
											<td colSpan="2"><IMG height="1" src="../images/dot.gif" width="100%"></TD>
										</TR>
										<TR>
											<td colSpan="2" height="3"><asp:checkbox id="ckDates" runat="server" CssClass="EnterDataLabel" AutoPostBack="True" Checked="True"
													Text="За целия период" Width="144px"></asp:checkbox></td>
										</TR>
										<TR>
											<td>
												<TABLE id="Table4" cellSpacing="0" cellPadding="3" border="0">
													<TR id="SelectDateRow">
														<td style="WIDTH: 139px; HEIGHT: 4px" vAlign="top" align="right"><asp:label id="lblStartDay" runat="server" CssClass="EnterDataLabel">Ден, седмица, нач.дата:</asp:label><asp:label id="lblSelStartDate" runat="server" Font-Bold="True" ForeColor="SaddleBrown" Width="100px"></asp:label></TD>
														<td style="WIDTH: 72px" vAlign="top">
															<TABLE id="Table1" cellSpacing="0" cellPadding="0" border="0">
																<TR>
																	<td><asp:calendar id="calStartDate" runat="server" ForeColor="#222222" Width="240px" SelectionMode="DayWeek"
																			BorderColor="#999999" CellPadding="0" BorderWidth="1px" BackColor="White" Height="240px" NextMonthText=""
																			PrevMonthText="" DayNameFormat="FirstLetter" SelectWeekText="+"
																			FirstDayOfWeek="Monday" BorderStyle="Solid" CssClass="aspCalendar" NextPrevFormat="ShortMonth" Font-Bold="True" Font-Size="11pt">
																			<TodayDayStyle ForeColor="#C73B2E" BackColor="White" BorderColor="#C73B2E" 
                                                                                BorderStyle="Solid" BorderWidth="2px" Height="32px" Width="32px"></TodayDayStyle>
																			<SelectorStyle Font-Size="10pt" Font-Names="Verdana" ForeColor="SaddleBrown" 
                                                                                BorderStyle="None"></SelectorStyle>
																			<DayStyle Font-Size="10pt" Font-Bold="False" BorderStyle="Dotted" 
                                                                                BorderColor="#CCCCCC" BorderWidth="1px" Height="30px" Width="32px"></DayStyle>
																			<NextPrevStyle Font-Bold="True" Wrap="False" HorizontalAlign="Center" 
                                                                                Height="16px" BorderStyle="None" VerticalAlign="Middle" Font-Size="9pt" 
                                                                                Font-Underline="True"></NextPrevStyle>
																			<DayHeaderStyle Font-Size="9pt" Height="1px" ForeColor="SaddleBrown"></DayHeaderStyle>
																			<SelectedDayStyle Font-Bold="True" ForeColor="#C73B2E" BorderColor="#C73B2E" 
                                                                                BackColor="White" BorderStyle="Solid" BorderWidth="2px"
                                                                                Font-Size="11pt" Height="32px" Width="32px"></SelectedDayStyle>
																			<TitleStyle Font-Size="13pt" Font-Bold="True" ForeColor="#C73B2E" 
                                                                                BackColor="White" HorizontalAlign="Center"></TitleStyle>
																			<OtherMonthDayStyle ForeColor="Gray" BackColor="#FDFDFD" Font-Size="6pt"></OtherMonthDayStyle>
																		    <WeekendDayStyle Font-Bold="True" Font-Italic="False" 
                                                                                ForeColor="#C73B2E" />
																		</asp:calendar>
																		<asp:textbox style="Z-INDEX: 0" id="txtStartDate" runat="server" CssClass="enterDataBox" Width="88px"
																			MaxLength="10"></asp:textbox><IMG style="Z-INDEX: 0" id="lkCalendar1" alt="Calendar" align="absBottom" src="../images/calendar.ico"
																			width="18" runat="server"></TD>
																	<td vAlign="top" noWrap></TD>
																</TR>
															</TABLE>
														</TD>
														<td style="WIDTH: 117px" vAlign="top" align="right"><asp:label id="lblEndDay" runat="server" CssClass="EnterDataLabel">Крайна дата:</asp:label><asp:label id="lblSelEndDate" runat="server" Font-Bold="True" ForeColor="SaddleBrown" Width="100px"></asp:label></TD>
														<td style="WIDTH: 278px; HEIGHT: 4px" vAlign="top">
															<TABLE id="Table6" cellSpacing="0" cellPadding="0" border="0">
																<TR>
																	<td><asp:calendar id="calEndDate" runat="server" ForeColor="#222222" Width="240px" SelectionMode="DayWeek"
																			BorderColor="#999999" CellPadding="0" BorderWidth="1px" BackColor="White" Height="240px" NextMonthText=""
																			PrevMonthText="" DayNameFormat="FirstLetter" SelectWeekText="+"
																			FirstDayOfWeek="Monday" BorderStyle="Solid" CssClass="aspCalendar" NextPrevFormat="ShortMonth" Font-Bold="True" Font-Size="11pt">
																			<TodayDayStyle ForeColor="#C73B2E" BackColor="White" BorderColor="#C73B2E" 
                                                                                BorderStyle="Solid" BorderWidth="2px" Height="32px" Width="32px"></TodayDayStyle>
																			<SelectorStyle Font-Size="10pt" Font-Names="Verdana" ForeColor="SaddleBrown" 
                                                                                BorderStyle="None"></SelectorStyle>
																			<DayStyle Font-Size="10pt" Font-Bold="False" BorderStyle="Dotted" 
                                                                                BorderColor="#CCCCCC" BorderWidth="1px" Height="30px" Width="32px"></DayStyle>
																			<NextPrevStyle Font-Bold="True" Wrap="False" HorizontalAlign="Center" 
                                                                                Height="16px" BorderStyle="None" VerticalAlign="Middle" Font-Size="9pt" 
                                                                                Font-Underline="True"></NextPrevStyle>
																			<DayHeaderStyle Font-Size="9pt" Height="1px" ForeColor="SaddleBrown"></DayHeaderStyle>
																			<SelectedDayStyle Font-Bold="True" ForeColor="#C73B2E" BorderColor="#C73B2E" 
                                                                                BackColor="White" BorderStyle="Solid" BorderWidth="2px"
                                                                                Font-Size="11pt" Height="32px" Width="32px"></SelectedDayStyle>
																			<TitleStyle Font-Size="13pt" Font-Bold="True" ForeColor="#C73B2E" 
                                                                                BackColor="White" HorizontalAlign="Center"></TitleStyle>
																			<OtherMonthDayStyle ForeColor="Gray" BackColor="#FDFDFD" Font-Size="6pt"></OtherMonthDayStyle>
																		    <WeekendDayStyle Font-Bold="True" Font-Italic="False" 
                                                                                ForeColor="#C73B2E" />
																		</asp:calendar>
																		<asp:textbox style="Z-INDEX: 0" id="txtEndDate" runat="server" CssClass="enterDataBox" Width="88px"
																			MaxLength="10"></asp:textbox><IMG style="Z-INDEX: 0" id="lkCalendar2" alt="Calendar" align="absBottom" src="../images/calendar.ico"
																			width="18" runat="server"></TD>
																	<td vAlign="top" noWrap></TD>
																</TR>
															</TABLE>
														</TD>
													</TR>
												</TABLE>
											</TD>
										</TR>
										<TR>
											<td style="HEIGHT: 3px" colSpan="2"></TD>
										</TR>
										<TR>
											<td colSpan="2"><IMG height="1" src="../images/dot.gif" width="100%"></TD>
										</TR>
										<TR>
											<td colSpan="2" height="3"></td>
										</TR>
										<tr>
											<td>
												<table cellSpacing="0" cellPadding="3" border="0">
													<TR>
														<td style="WIDTH: 139px" vAlign="top"><asp:label id="Label1" runat="server" CssClass="enterDataLabel" Width="100%">Тип:</asp:label></TD>
														<td style="WIDTH: 243px" vAlign="top"><asp:dropdownlist id="ddlProjectsStatus" runat="server" CssClass="EnterDataBox" AutoPostBack="True"
																Width="250px"></asp:dropdownlist></TD>
														<td vAlign="top"></TD>
														<td></TD>
														<td></TD>
														<td style="WIDTH: 8px"></TD>
														<td style="WIDTH: 8px"></TD>
													</TR>
													<TR>
														<td style="WIDTH: 139px" vAlign="top"><asp:label id="Label7" runat="server" CssClass="enterDataLabel" Width="100%">Сграда:</asp:label></TD>
														<td style="WIDTH: 243px" vAlign="top"><asp:dropdownlist id="ddlBuildingTypes" runat="server" CssClass="enterDataBox" AutoPostBack="True"
																Width="250px"></asp:dropdownlist></TD>
														<td vAlign="top"></TD>
														<td></TD>
														<td></TD>
														<td style="WIDTH: 8px"></TD>
														<td style="WIDTH: 8px"></TD>
													</TR>
													<TR>
														<td style="WIDTH: 139px" vAlign="top"><asp:label id="Label2" runat="server" CssClass="enterDataLabel" Width="100%">Проект:</asp:label></TD>
														<td style="WIDTH: 243px" vAlign="top"><asp:dropdownlist id="ddlProjects" runat="server" CssClass="EnterDataBox" Width="250px"></asp:dropdownlist></TD>
														<td vAlign="top"></TD>
														<td></TD>
														<td></TD>
														<td style="WIDTH: 8px"></TD>
														<td style="WIDTH: 8px"></TD>
													</TR>
													<TR>
														<td style="WIDTH: 139px" vAlign="top">
															<asp:label id="Label10" runat="server" CssClass="enterDataLabel" Width="100%">Фирма за финансови показатели:</asp:label></TD>
														<td style="WIDTH: 243px" vAlign="top">
															<asp:dropdownlist id="ddlCompany" runat="server" CssClass="EnterDataBox" Width="250px">
																<asp:ListItem Value="0">АСА</asp:ListItem>
																<asp:ListItem Value="1">АСИ</asp:ListItem>
															</asp:dropdownlist></TD>
														<td vAlign="top"></TD>
														<td></TD>
														<td>
															<asp:imagebutton id="ibGenerate" runat="server" ImageUrl="../images/ie.gif"></asp:imagebutton></TD>
														<td style="WIDTH: 8px">
															<asp:imagebutton id="ibPdfExport" runat="server" ImageUrl="../images/pdf1.gif" ToolTip="Справка в PDF формат"></asp:imagebutton></TD>
														<td style="WIDTH: 8px">
															<asp:imagebutton id="ibXlsExport" runat="server" ImageUrl="../images/xls.gif" ToolTip="Справка в Excel формат"></asp:imagebutton></TD>
													</TR>
													<TR>
														<td style="WIDTH: 139px" vAlign="top" height="10"></TD>
														<td style="WIDTH: 243px" vAlign="top" height="10"></TD>
														<td vAlign="top" height="10"></TD>
														<td height="10"></TD>
														<td height="10"></TD>
														<td style="WIDTH: 8px" height="10"></TD>
														<td style="WIDTH: 8px" height="10"></TD>
													</TR>
												</table>
											</td>
										</tr>
										<TR>
											<td style="HEIGHT: 5px" colSpan="2"></TD>
										</TR>
										<TR>
											<td colSpan="2"><IMG height="1" src="../images/dot.gif" width="100%"></TD>
										</TR>
										<TR>
											<td colSpan="2" height="3"></TD>
										</TR>
										<TR>
											<td colSpan="2"><IMG height="1" src="images/dot.gif" width="100%"></TD>
										</TR>
									</TABLE>
									<P>&nbsp;</P>
									<TABLE id="Table5" cellSpacing="0" cellPadding="0" width="100%" border="0">
										<TR>
											<td align="center"><asp:label id="lblReportTitle" runat="server" Font-Bold="True" Font-Size="11pt" EnableViewState="False"
													Visible="False">ФИНАНСОВИ ПОКАЗАТЕЛИ</asp:label></TD>
										</TR>
									</TABLE>
									<br>
									<TABLE id="tblHeader" cellSpacing="0" cellPadding="2" border="0" runat="server">
										<TR>
											<td vAlign="top"><asp:label id="Label5" runat="server" Font-Bold="True" Font-Size="9pt" Visible="false">ПРОЕКТ:</asp:label></TD>
											<td vAlign="top"><asp:label id="lblObekt" runat="server" Font-Size="9pt" Visible="false"></asp:label></TD>
										</TR>
									</TABLE>
									<BR>
									<asp:label id="lbEUR" runat="server" Font-Bold="True" Font-Size="11pt" EnableViewState="False"
										Visible="False">в EUR</asp:label><br>
									<asp:datagrid id="grdReport" runat="server" ForeColor="DimGray" CssClass="ReportGrid" EnableViewState="False"
										Width="100%" CellPadding="4" ShowHeader="True" AutoGenerateColumns="False">
										<HeaderStyle CssClass="reportGridHeader"></HeaderStyle>
										<Columns>
											<asp:BoundColumn HeaderStyle-Font-Size="8" DataField="ProjectID" Visible="False"></asp:BoundColumn>
											<asp:BoundColumn HeaderStyle-Font-Size="8" DataField="ProjectName" HeaderText="ПРОЕКТ"></asp:BoundColumn>
											<asp:BoundColumn HeaderStyle-Font-Size="8" DataField="Area" HeaderText="M2" DataFormatString="{0:n0}"
												ItemStyle-HorizontalAlign="Right"></asp:BoundColumn>
											<asp:BoundColumn HeaderStyle-Font-Size="8" DataField="Payments" HeaderText="ПРИХОДИ EUR" DataFormatString="{0:n2}"
												ItemStyle-HorizontalAlign="Right"></asp:BoundColumn>
											<asp:BoundColumn HeaderStyle-Font-Size="8" DataField="PaymentArea" HeaderText="ПРИХОДИ EUR/M2" DataFormatString="{0:n2}"
												ItemStyle-HorizontalAlign="Right"></asp:BoundColumn>
											<asp:BoundColumn HeaderStyle-Font-Size="8" DataField="WorkedHours" HeaderText="РАБ. ЧАСОВЕ" DataFormatString="{0:n1}"
												ItemStyle-HorizontalAlign="Right"></asp:BoundColumn>
											<asp:BoundColumn HeaderStyle-Font-Size="8" DataField="WorkedSalary" HeaderText="ЗАПЛАТИ EUR" DataFormatString="{0:n2}"
												ItemStyle-HorizontalAlign="Right"></asp:BoundColumn>
											<asp:BoundColumn HeaderStyle-Font-Size="8" DataField="AdminSalary" HeaderText="ОБЩИ АДМИН. РАЗХОДИ EUR"
												DataFormatString="{0:n2}" ItemStyle-HorizontalAlign="Right"></asp:BoundColumn>
											<asp:BoundColumn HeaderStyle-Font-Size="8" DataField="LeaderSalary" HeaderText="РАЗХОДИ РЪКОВОДИТЕЛ EUR"
												DataFormatString="{0:n2}" ItemStyle-HorizontalAlign="Right"></asp:BoundColumn>
											<asp:BoundColumn HeaderStyle-Font-Size="8" DataField="StaffSalary" HeaderText="РАЗХОДИ ЗАПЛАТИ АДМИНИСТРАЦИЯ EUR"
												DataFormatString="{0:n2}" ItemStyle-HorizontalAlign="Right"></asp:BoundColumn>
											<asp:BoundColumn HeaderStyle-Font-Size="8" DataField="SubcontracterSalary" HeaderText="ПОДИЗП. РАЗХОДИ EUR"
												DataFormatString="{0:n2}" ItemStyle-HorizontalAlign="Right"></asp:BoundColumn>
											<asp:BoundColumn HeaderStyle-Font-Size="8" DataField="TotalSalary" HeaderText="ВСИЧКО РАЗХОДИ EUR"
												DataFormatString="{0:n2}" ItemStyle-HorizontalAlign="Right"></asp:BoundColumn>
											<asp:BoundColumn HeaderStyle-Font-Size="8" DataField="ExpencesPerHour" HeaderText="РАЗХОДИ НА ЧАС EUR"
												DataFormatString="{0:n2}" ItemStyle-HorizontalAlign="Right"></asp:BoundColumn>
											<asp:BoundColumn DataField="ExpencesPerArea" HeaderText="РАЗХОДИ EUR /М2" DataFormatString="{0:n2}">
												<HeaderStyle Font-Size="8pt"></HeaderStyle>
												<ItemStyle HorizontalAlign="Right"></ItemStyle>
											</asp:BoundColumn>
											<asp:BoundColumn HeaderStyle-Font-Size="8" DataField="PaymentPerHour" HeaderText="ПРИХОДИ НА ЧАС EUR"
												DataFormatString="{0:n2}" ItemStyle-HorizontalAlign="Right"></asp:BoundColumn>
											<asp:BoundColumn HeaderStyle-Font-Size="8" DataField="ImcomePerHour" HeaderText="ПЕЧАЛБА НА ЧАС EUR"
												DataFormatString="{0:n2}" ItemStyle-HorizontalAlign="Right"></asp:BoundColumn>
											<asp:BoundColumn HeaderStyle-Font-Size="8" DataField="TotalIncome" HeaderText="ВСИЧКО ПЕЧАЛБА EUR"
												ItemStyle-Wrap="False" DataFormatString="{0:n2}" ItemStyle-HorizontalAlign="Right"></asp:BoundColumn>
										</Columns>
									</asp:datagrid><BR>
									<asp:label id="lbBGN" runat="server" Font-Bold="True" Font-Size="11pt" EnableViewState="False"
										Visible="False">в лева</asp:label><br>
									<asp:datagrid id="grdReport1" runat="server" ForeColor="DimGray" CssClass="ReportGrid" EnableViewState="False"
										Width="100%" CellPadding="4" ShowHeader="True" AutoGenerateColumns="False">
										<HeaderStyle CssClass="reportGridHeader"></HeaderStyle>
										<Columns>
											<asp:BoundColumn Visible="False" DataField="ProjectID">
												<HeaderStyle Font-Size="8pt"></HeaderStyle>
											</asp:BoundColumn>
											<asp:BoundColumn DataField="ProjectName" HeaderText="ПРОЕКТ">
												<HeaderStyle Font-Size="8pt"></HeaderStyle>
											</asp:BoundColumn>
											<asp:BoundColumn DataField="Area" HeaderText="M2" DataFormatString="{0:n0}">
												<HeaderStyle Font-Size="8pt"></HeaderStyle>
												<ItemStyle HorizontalAlign="Right"></ItemStyle>
											</asp:BoundColumn>
											<asp:BoundColumn DataField="Payments" HeaderText="ПРИХОДИ BGN" DataFormatString="{0:n2}">
												<HeaderStyle Font-Size="8pt"></HeaderStyle>
												<ItemStyle HorizontalAlign="Right"></ItemStyle>
											</asp:BoundColumn>
											<asp:BoundColumn DataField="PaymentArea" HeaderText="ПРИХОДИ BGN/M2" DataFormatString="{0:n2}">
												<HeaderStyle Font-Size="8pt"></HeaderStyle>
												<ItemStyle HorizontalAlign="Right"></ItemStyle>
											</asp:BoundColumn>
											<asp:BoundColumn DataField="WorkedHours" HeaderText="РАБ. ЧАСОВЕ" DataFormatString="{0:n1}">
												<HeaderStyle Font-Size="8pt"></HeaderStyle>
												<ItemStyle HorizontalAlign="Right"></ItemStyle>
											</asp:BoundColumn>
											<asp:BoundColumn DataField="WorkedSalary" HeaderText="ЗАПЛАТИ BGN" DataFormatString="{0:n2}">
												<HeaderStyle Font-Size="8pt"></HeaderStyle>
												<ItemStyle HorizontalAlign="Right"></ItemStyle>
											</asp:BoundColumn>
											<asp:BoundColumn HeaderStyle-Font-Size="8" DataField="AdminSalary" HeaderText="ОБЩИ АДМИН. РАЗХОДИ BGN"
												DataFormatString="{0:n2}" ItemStyle-HorizontalAlign="Right"></asp:BoundColumn>
											<asp:BoundColumn HeaderStyle-Font-Size="8" DataField="LeaderSalary" HeaderText="РАЗХОДИ РЪКОВОДИТЕЛ BGN"
												DataFormatString="{0:n2}" ItemStyle-HorizontalAlign="Right"></asp:BoundColumn>
											<asp:BoundColumn HeaderStyle-Font-Size="8" DataField="StaffSalary" HeaderText="РАЗХОДИ ЗАПЛАТИ АДМИНИСТРАЦИЯ BGN"
												DataFormatString="{0:n2}" ItemStyle-HorizontalAlign="Right"></asp:BoundColumn>
											<asp:BoundColumn DataField="SubcontracterSalary" HeaderText="ПОДИЗП. РАЗХОДИ BGN" DataFormatString="{0:n2}">
												<HeaderStyle Font-Size="8pt"></HeaderStyle>
												<ItemStyle HorizontalAlign="Right"></ItemStyle>
											</asp:BoundColumn>
											<asp:BoundColumn DataField="TotalSalary" HeaderText="ВСИЧКО РАЗХОДИ BGN" DataFormatString="{0:n2}">
												<HeaderStyle Font-Size="8pt"></HeaderStyle>
												<ItemStyle HorizontalAlign="Right"></ItemStyle>
											</asp:BoundColumn>
											<asp:BoundColumn DataField="ExpencesPerHour" HeaderText="РАЗХОДИ НА ЧАС BGN" DataFormatString="{0:n2}">
												<HeaderStyle Font-Size="8pt"></HeaderStyle>
												<ItemStyle HorizontalAlign="Right"></ItemStyle>
											</asp:BoundColumn>
											<asp:BoundColumn DataField="ExpencesPerArea" HeaderText="РАЗХОДИ BGN /М2" DataFormatString="{0:n2}">
												<HeaderStyle Font-Size="8pt"></HeaderStyle>
												<ItemStyle HorizontalAlign="Right"></ItemStyle>
											</asp:BoundColumn>
											<asp:BoundColumn DataField="PaymentPerHour" HeaderText="ПРИХОДИ НА ЧАС BGN" DataFormatString="{0:n2}">
												<HeaderStyle Font-Size="8pt"></HeaderStyle>
												<ItemStyle HorizontalAlign="Right"></ItemStyle>
											</asp:BoundColumn>
											<asp:BoundColumn DataField="ImcomePerHour" HeaderText="ПЕЧАЛБА НА ЧАС BGN" DataFormatString="{0:n2}">
												<HeaderStyle Font-Size="8pt"></HeaderStyle>
												<ItemStyle HorizontalAlign="Right"></ItemStyle>
											</asp:BoundColumn>
											<asp:BoundColumn DataField="TotalIncome" HeaderText="ВСИЧКО ПЕЧАЛБА BGN" DataFormatString="{0:n2}"
												ItemStyle-Wrap="False">
												<HeaderStyle Font-Size="8pt"></HeaderStyle>
												<ItemStyle HorizontalAlign="Right"></ItemStyle>
											</asp:BoundColumn>
										</Columns>
									</asp:datagrid><br>
									<br>
									<br>
									<asp:label id="lblNoDataFound" runat="server" CssClass="InfoLabel" EnableViewState="False"
										Visible="False">Не са намерени данни съответстващи на избрания филтър.</asp:label><br>
									<br>
									<br>
									<asp:label id="lblCreatedByLbl" runat="server" Font-Bold="True" Font-Size="9pt" EnableViewState="False"
										Visible="False">ИЗГОТВИЛ СПРАВКАТА:</asp:label><asp:label id="lblCreatedBy" runat="server" Font-Size="9pt" EnableViewState="False" Visible="False"></asp:label><BR>
									<br>
									<br>
								</td>
							</tr>
						</table>
					</td>
				</tr>
			</table>
		</form>
	</body>
</HTML>
