using System;
using DataDynamics.ActiveReports;
using DataDynamics.ActiveReports.Document;

namespace Asa.Timesheet.Windows.Mail
{
	public class WorkTimesFooter : ActiveReport
	{
		public WorkTimesFooter(WorkedHoursInfo whi,bool IsOverTime, bool IsTraine)
		{
			InitializeReport();

			decimal desTotal = 0;
			if(IsOverTime)
				desTotal = whi.TotalHours - whi.TotalHoursInWorkTime;
			else
				desTotal = whi.TotalHours;
			this.txtTotalHours.Text = desTotal.ToString("0.00");
			

			this.txtWorkDays.Text = whi.TotalHoursInWorkTime.ToString("0.00");
			this.txtHoursNWT.Text = whi.TotalHoursInNonWorkTime.ToString("0.00");
			txtMissingHours.Text = whi.MissingHours.ToString("0.00");
			//this.txtHoursWND.Text = whi.TotalHoursWeekends.ToString("0.00");

			//this.txtWorkDays.Text = whi.TotalWorkDays.ToString();
			this.txtNonWorkDays.Text = whi.TotalHoursInNonWorkTime.ToString("0.00");//whi.TotalNonWorkDays.ToString();


			this.txtDaysIll.Text = whi.DaysIll.ToString();
			if (whi.DaysIll>0) this.txtDaysIll.Text+=": "+whi.DaysIllString;
			this.txtDaysPO.Text = whi.DaysPlatenOtpusk.ToString();
			if (whi.DaysPlatenOtpusk>0) this.txtDaysPO.Text+=": "+whi.DaysPlatenOtpuskString;
			this.txtDaysNO.Text = whi.DaysNeplatenOtpusk.ToString();
			if(whi.DaysNeplatenOtpusk>0) this.txtDaysNO.Text+=": "+whi.DaysNeplatenOtpuskString;
			if(whi.TotalLateCount>0)
			{
				txtTotalLate.Text=whi.TotalLateCount.ToString();
				txtTotalLateHours.Text=whi.TotalHoursLate;
				txtTotalLate.Visible=txtTotalLateHours.Visible=Label11.Visible=Label12.Visible=true;
			}
			if(IsOverTime)
			{
				txtWorkDays.Visible = txtNonWorkDays.Visible = false;
				Label9.Visible = Label10.Visible = false;
				GroupHeader2.Visible = false;
				GroupHeader3.Visible = false;
				GroupHeader4.Visible = false;
			}
			if(IsTraine)
			{
				GroupHeader4.Visible=GroupHeader3.Visible=false;
				txtTotalLate.Visible=txtTotalLateHours.Visible=Label11.Visible=Label12.Visible=false;
			}
		}

		#region ActiveReports Designer generated code
		private DataDynamics.ActiveReports.GroupHeader GroupHeader1 = null;
		private DataDynamics.ActiveReports.TextBox txtHoursNWT = null;
		private DataDynamics.ActiveReports.Label lblProektant = null;
		private DataDynamics.ActiveReports.Label Label1 = null;
		private DataDynamics.ActiveReports.TextBox txtTotalHours = null;
		private DataDynamics.ActiveReports.Label Label8 = null;
		private DataDynamics.ActiveReports.Line Line1 = null;
		private DataDynamics.ActiveReports.GroupHeader GroupHeader5 = null;
		private DataDynamics.ActiveReports.Label Label11 = null;
		private DataDynamics.ActiveReports.Label Label12 = null;
		private DataDynamics.ActiveReports.TextBox txtTotalLate = null;
		private DataDynamics.ActiveReports.TextBox txtTotalLateHours = null;
		private DataDynamics.ActiveReports.TextBox txtNonWorkDays = null;
		private DataDynamics.ActiveReports.Label Label10 = null;
		private DataDynamics.ActiveReports.TextBox txtWorkDays = null;
		private DataDynamics.ActiveReports.Label Label9 = null;
		private DataDynamics.ActiveReports.Label Label13 = null;
		private DataDynamics.ActiveReports.TextBox txtMissingHours = null;
		private DataDynamics.ActiveReports.GroupHeader GroupHeader2 = null;
		private DataDynamics.ActiveReports.Label Label4 = null;
		private DataDynamics.ActiveReports.TextBox txtDaysPO = null;
		private DataDynamics.ActiveReports.Label Label7 = null;
		private DataDynamics.ActiveReports.Line Line3 = null;
		private DataDynamics.ActiveReports.GroupHeader GroupHeader3 = null;
		private DataDynamics.ActiveReports.Label Label5 = null;
		private DataDynamics.ActiveReports.TextBox txtDaysNO = null;
		private DataDynamics.ActiveReports.GroupHeader GroupHeader4 = null;
		private DataDynamics.ActiveReports.Label Label6 = null;
		private DataDynamics.ActiveReports.TextBox txtDaysIll = null;
		private DataDynamics.ActiveReports.Line Line2 = null;
		private DataDynamics.ActiveReports.Detail Detail = null;
		private DataDynamics.ActiveReports.GroupFooter GroupFooter4 = null;
		private DataDynamics.ActiveReports.GroupFooter GroupFooter3 = null;
		private DataDynamics.ActiveReports.GroupFooter GroupFooter2 = null;
		private DataDynamics.ActiveReports.GroupFooter GroupFooter5 = null;
		private DataDynamics.ActiveReports.GroupFooter GroupFooter1 = null;
		public void InitializeReport()
		{
			this.LoadLayout(this.GetType(), "Mail.WorkTimesFooter.rpx");
			this.GroupHeader1 = ((DataDynamics.ActiveReports.GroupHeader)(this.Sections["GroupHeader1"]));
			this.GroupHeader5 = ((DataDynamics.ActiveReports.GroupHeader)(this.Sections["GroupHeader5"]));
			this.GroupHeader2 = ((DataDynamics.ActiveReports.GroupHeader)(this.Sections["GroupHeader2"]));
			this.GroupHeader3 = ((DataDynamics.ActiveReports.GroupHeader)(this.Sections["GroupHeader3"]));
			this.GroupHeader4 = ((DataDynamics.ActiveReports.GroupHeader)(this.Sections["GroupHeader4"]));
			this.Detail = ((DataDynamics.ActiveReports.Detail)(this.Sections["Detail"]));
			this.GroupFooter4 = ((DataDynamics.ActiveReports.GroupFooter)(this.Sections["GroupFooter4"]));
			this.GroupFooter3 = ((DataDynamics.ActiveReports.GroupFooter)(this.Sections["GroupFooter3"]));
			this.GroupFooter2 = ((DataDynamics.ActiveReports.GroupFooter)(this.Sections["GroupFooter2"]));
			this.GroupFooter5 = ((DataDynamics.ActiveReports.GroupFooter)(this.Sections["GroupFooter5"]));
			this.GroupFooter1 = ((DataDynamics.ActiveReports.GroupFooter)(this.Sections["GroupFooter1"]));
			this.txtHoursNWT = ((DataDynamics.ActiveReports.TextBox)(this.GroupHeader1.Controls[0]));
			this.lblProektant = ((DataDynamics.ActiveReports.Label)(this.GroupHeader1.Controls[1]));
			this.Label1 = ((DataDynamics.ActiveReports.Label)(this.GroupHeader1.Controls[2]));
			this.txtTotalHours = ((DataDynamics.ActiveReports.TextBox)(this.GroupHeader1.Controls[3]));
			this.Label8 = ((DataDynamics.ActiveReports.Label)(this.GroupHeader1.Controls[4]));
			this.Line1 = ((DataDynamics.ActiveReports.Line)(this.GroupHeader1.Controls[5]));
			this.Label11 = ((DataDynamics.ActiveReports.Label)(this.GroupHeader5.Controls[0]));
			this.Label12 = ((DataDynamics.ActiveReports.Label)(this.GroupHeader5.Controls[1]));
			this.txtTotalLate = ((DataDynamics.ActiveReports.TextBox)(this.GroupHeader5.Controls[2]));
			this.txtTotalLateHours = ((DataDynamics.ActiveReports.TextBox)(this.GroupHeader5.Controls[3]));
			this.txtNonWorkDays = ((DataDynamics.ActiveReports.TextBox)(this.GroupHeader5.Controls[4]));
			this.Label10 = ((DataDynamics.ActiveReports.Label)(this.GroupHeader5.Controls[5]));
			this.txtWorkDays = ((DataDynamics.ActiveReports.TextBox)(this.GroupHeader5.Controls[6]));
			this.Label9 = ((DataDynamics.ActiveReports.Label)(this.GroupHeader5.Controls[7]));
			this.Label13 = ((DataDynamics.ActiveReports.Label)(this.GroupHeader5.Controls[8]));
			this.txtMissingHours = ((DataDynamics.ActiveReports.TextBox)(this.GroupHeader5.Controls[9]));
			this.Label4 = ((DataDynamics.ActiveReports.Label)(this.GroupHeader2.Controls[0]));
			this.txtDaysPO = ((DataDynamics.ActiveReports.TextBox)(this.GroupHeader2.Controls[1]));
			this.Label7 = ((DataDynamics.ActiveReports.Label)(this.GroupHeader2.Controls[2]));
			this.Line3 = ((DataDynamics.ActiveReports.Line)(this.GroupHeader2.Controls[3]));
			this.Label5 = ((DataDynamics.ActiveReports.Label)(this.GroupHeader3.Controls[0]));
			this.txtDaysNO = ((DataDynamics.ActiveReports.TextBox)(this.GroupHeader3.Controls[1]));
			this.Label6 = ((DataDynamics.ActiveReports.Label)(this.GroupHeader4.Controls[0]));
			this.txtDaysIll = ((DataDynamics.ActiveReports.TextBox)(this.GroupHeader4.Controls[1]));
			this.Line2 = ((DataDynamics.ActiveReports.Line)(this.GroupHeader4.Controls[2]));
		}

		#endregion
	}
}
