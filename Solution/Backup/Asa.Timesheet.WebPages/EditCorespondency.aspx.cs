using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using log4net;
using Asa.Timesheet.Data;
using Asa.Timesheet.Data.Entities;
using Asa.Timesheet.Data.Util;
using Asa.Timesheet.WebPages.Reports;
using System.Data.SqlClient;

namespace Asa.Timesheet.WebPages
{
	/// <summary>
	/// Summary description for EditCorespondancy.
	/// </summary>
	public class EditCorespondency : TimesheetPageBase
	{
		#region WebControls
			private static readonly ILog log = LogManager.GetLogger(typeof(EditProtokols));
		protected System.Web.UI.WebControls.PlaceHolder menuHolder;
		protected System.Web.UI.WebControls.Label Label8;
		protected System.Web.UI.WebControls.DropDownList ddlBuildingTypes;
		protected System.Web.UI.WebControls.DropDownList ddlProjectsStatus;
		protected System.Web.UI.WebControls.Label lblName;
		protected System.Web.UI.WebControls.DropDownList ddlProject;
		protected System.Web.UI.WebControls.Label lbProjects;
		protected System.Web.UI.WebControls.Label lbDate;
		protected System.Web.UI.WebControls.TextBox txtStartDate;
		protected System.Web.UI.WebControls.Label Label9;
		protected System.Web.UI.WebControls.TextBox txtNotes;
		protected System.Web.UI.WebControls.Label Label6;
		protected System.Web.UI.WebControls.Button btnScanDocAdd;
		protected System.Web.UI.WebControls.Button btnSave;
		protected System.Web.UI.WebControls.Button btnEdit;
		protected System.Web.UI.WebControls.Button btnCancel;
		protected System.Web.UI.WebControls.Button btnDelete;
		protected System.Web.UI.WebControls.Label lblInfo;
		protected System.Web.UI.WebControls.Label lblError;
		protected System.Web.UI.HtmlControls.HtmlImage lkCalendar1;
		protected System.Web.UI.HtmlControls.HtmlInputFile FileCtrl;
		protected Asa.Timesheet.WebPages.UserControls.PageHeader header;
		protected System.Web.UI.WebControls.Label lbScanDocNew;
		protected System.Web.UI.WebControls.DataList dlDocs;
		protected System.Web.UI.WebControls.Label lbSendRes;
		protected System.Web.UI.WebControls.RadioButtonList rbSendRes;
		protected System.Web.UI.WebControls.Label lbMailingType;
		protected Asa.Timesheet.WebPages.UserControls.EditForm editCtrl;
		protected System.Web.UI.WebControls.TextBox txtEnterNumber;
		protected System.Web.UI.WebControls.Label lbSD;
		protected System.Web.UI.WebControls.Label lbNotes;
		protected System.Web.UI.WebControls.DropDownList ddlClients;
		protected System.Web.UI.WebControls.Label lbClient;
		protected System.Web.UI.HtmlControls.HtmlImage imgRequired;
		protected System.Web.UI.WebControls.CheckBox cbSendLetter;
		protected System.Web.UI.WebControls.Label lbSendLetter;
		protected System.Web.UI.WebControls.CheckBoxList cblMailingType;
		protected System.Web.UI.WebControls.Label lbExt;
		protected System.Web.UI.WebControls.Label Label1;
		protected System.Web.UI.WebControls.Button btnSaveUP;
		protected System.Web.UI.WebControls.Button btnEditUP;
		protected System.Web.UI.WebControls.Button btnCancelUP;
		
		
		
		
		#endregion

		
		
		MailingTypesVector _mtv = new MailingTypesVector();
		protected System.Web.UI.WebControls.ImageButton btnFile;
		protected System.Web.UI.WebControls.TextBox txtPerson;
		protected System.Web.UI.WebControls.Label lbEnterPerson;
		protected System.Web.UI.WebControls.Label lbEnterNumber;
		
		
		private const string _hasScan = "true";

		#region Page_Load

        private void Page_Load(object sender, System.EventArgs e)
        {
            //lbSD.Visible = true;
            lkCalendar1.Attributes["onclick"] = TimeHelper.InvokePopupCal(txtStartDate);
            _mtv = MailingTypeDAL.LoadCollection();
            if (!this.IsPostBack)
            {
                if (Page.IsPostBack == false)
                {
                    // Set up form for data change checking when
                    // first loaded.
                    this.CheckForDataChanges = true;
                    this.BypassPromptIds =
                        new string[] { "ibPdfExport", "btnFile", "btnDel", "btnScanDocAdd", "btnSave", "btnSaveUP", "btnEdit", "btnEditUP", "btnDelete", "ddlProject", "ddlBuildingTypes", "ddlProjectsStatus" };
                }
                //				if(UIHelpers.GetIDParam()<=0)
                //				{
                //					}
                //				else

                header.UserName = this.LoggedUser.UserName;
                UIHelpers.LoadProjectStatus(ddlProjectsStatus, (int)ProjectsData.ProjectsByStatus.AllProjects);
                UIHelpers.LoadBuildingTypes("", ddlBuildingTypes);
                LoadProjects();
                //LoadMailingTypes();
                LoadCheckBoxTypes();
                LoadRadioButtons();
                LoadClients();
                InitEdit();
                if (UIHelpers.GetIDParam() > 0)
                {
                    LoadFromID(UIHelpers.GetIDParam());
                    //					BindGrid();				
                    header.PageTitle = Resource.ResourceManager["editCorespondency_PageTitle"];
                }
                else
                {
                    btnSave.Text = Resource.ResourceManager["btnNewMeeting"];
                    btnSaveUP.Text = Resource.ResourceManager["btnNewMeeting"];
                    //					btnScanDocAdd.Visible=false;				
                    ddlProjectsStatus.SelectedIndex = 1;
                    header.PageTitle = Resource.ResourceManager["newCorespondency_PageTitle"];
                    //lbEnterNumber.Visible = txtEnterNumber.Visible = false;
                    //lkCalendar1.Visible = imgRequired.Visible = false;
                    //txtStartDate.Enabled=false;
                    txtEnterNumber.Text = MailingDAL.HighterMailingNumber().ToString();
                    txtStartDate.Text = DateTime.Now.ToString(Constants.TextDateFormat);
                    btnFile.Visible = false;

                }
                if (UIHelpers.GetPIDParam() != -1)
                {
                    lblName.Visible = ddlProject.Visible =
                    Label8.Visible = ddlBuildingTypes.Visible =
                    ddlProjectsStatus.Visible = Label1.Visible = false;
                    ListItem li = ddlProject.Items.FindByValue(UIHelpers.GetPIDParam().ToString());
                    if (li != null)
                    {
                        ddlProject.SelectedValue = UIHelpers.GetPIDParam().ToString();
                    }
                    else
                        ErrorRedirect(Resource.ResourceManager["ErrorLoadProject"]);
                    ddlProject.Enabled = false;

                    if (UIHelpers.GetIDParam() <= 0)
                        LoadClients();
                    header.PageTitle = Resource.ResourceManager["editCorespondency_PageTitle"] + " - " + ddlProject.SelectedItem.Text;

                }
                if (!(LoggedUser.HasPaymentRights || LoggedUser.IsSecretary || LoggedUser.IsLayer))
                {
                    ErrorRedirect(Resource.ResourceManager["noRightsAlert"]);
                }
            }
            else

                UIHelpers.LoadBuildingTypes(ddlBuildingTypes.SelectedValue, ddlBuildingTypes, (ProjectsData.ProjectsByStatus)int.Parse(ddlProjectsStatus.SelectedValue));

            UIHelpers.CreateMenu(menuHolder, LoggedUser);

        }
        private void LoadFromID(int ID)
        {
            MailingData pd = MailingDAL.Load(ID);
            if (pd == null)
                ErrorRedirect("Error load meeting.");
            ddlProject.SelectedValue = pd.ProjectID.ToString();
            txtStartDate.Text = TimeHelper.FormatDate(pd.MailingDate);
            SetMailingTypes(pd.MailingTypes);
            //			if(!pd.Send)
            //				rbSendRes.SelectedIndex = (int)MailingSendReseve.Reseve;
            //			if(ddlMailingType.Items.FindByValue(pd.MailingTypeID.ToString())!= null)
            //				ddlMailingType.SelectedValue = pd.MailingTypeID.ToString();
            if (pd.HasScan)
                lbSD.Text = _hasScan;
            else
            {
                lbSD.Text = "false";
                btnFile.Visible = false;
            }
            txtPerson.Text = pd.Person;
            txtEnterNumber.Text = pd.MailingNumber.ToString();
            txtNotes.Text = pd.Notes;
            if (ddlClients.Items.FindByValue(pd.ClientID.ToString()) != null)
                ddlClients.SelectedValue = pd.ClientID.ToString();
            lbSendLetter.Visible = cbSendLetter.Visible = false;
            cbSendLetter.Checked = false;
            rbSendRes.SelectedIndex = pd.Send ? 0 : 1;
            lbExt.Text = pd.Ext;
        }
        protected string GetURL(int ID)
        {
            return string.Concat(System.Configuration.ConfigurationManager.AppSettings["ScanPathMailing"], ID, System.Configuration.ConfigurationManager.AppSettings["Extension"]);
        }	
		#endregion

		public ProjectsData.ProjectsByStatus ProjectsStatus
		{
			get
			{
				try
				{
					return  (ProjectsData.ProjectsByStatus)int.Parse(ddlProjectsStatus.SelectedValue);
				}
				catch { return ProjectsData.ProjectsByStatus.AllProjects; }
			}
		}


        private void InitEdit()
        {
            if (UIHelpers.GetIDParam() <= 0) return;

            string vmButtons = this.btnEdit.ClientID + ";" + this.btnEditUP.ClientID;
            string emButtons = this.btnSave.ClientID + ";" + this.btnSaveUP.ClientID + ";" + this.btnDelete.ClientID;

            string exl = string.Empty;

            editCtrl.InitEdit("tblForm", vmButtons, emButtons, exl);
        }
	
	
		#region BindGrid
//		private void BindGrid()
//		{
//			int ID = UIHelpers.GetIDParam();
//			ProjectProtocolsVector ppv = ProjectProtokolDAL.LoadCollection("ProjectProtokolsSelByProtocolProc",SQLParms.CreateProjectProtokolsSelByProtocolProc(ID));
//			if (!(ppv.Count>0))
//			{
//				ProtokolData pd = ProtokolDAL.Load(ID);
//				pd.HasScan = false;
//				ProtokolDAL.Save(pd);
//			}
//			dlDocs.DataSource = ppv;
//			dlDocs.DataKeyField="ProjectProtokolID";
//			dlDocs.DataBind();
//			
//			//			if (!LoggedUser.HasPaymentRights)
//			////				grdMeetings.Columns[(int)GridColumns.DeleteItem].Visible = false;
//			//				dlDocs.GridLines[GridLines.Horizontal].Visible = false;
//			try
//			{
//				for (int i=0; i<dlDocs.Items.Count; i++)
//				{
//					string js = string.Concat("return confirm('",Resource.ResourceManager["grid_ConfirmDeleteForProtokols"]," ?');");
//					System.Web.UI.WebControls.WebControl btn = (System.Web.UI.WebControls.WebControl)dlDocs.Items[i].FindControl("btnDel");
//					btn.Attributes["onclick"] = js;
//				}
//			}
//			catch 
//			{
//				
//			}
//			
//		}

        private bool LoadProjects()
        {
            SqlDataReader reader = null;

            try
            {
                reader = ProjectsData.SelectProjectNamesClear(this.ProjectsStatus, int.Parse(ddlBuildingTypes.SelectedValue));

                ddlProject.DataSource = reader;
                ddlProject.DataValueField = "ProjectID";
                ddlProject.DataTextField = "ProjectName";
                ddlProject.DataBind();

                ddlProject.Items.Insert(0, new ListItem(Resource.ResourceManager["reports_ddlNoProjects"], "-1"));
                ddlProject.SelectedValue = "-1";
            }
            catch (Exception ex)
            {
                log.Error(ex);
                return false;
            }

            finally
            {
                if (reader != null) reader.Close();
            }
            return true;
        }

//		private bool LoadMailingTypes()
//		{
//			//SqlDataReader reader = null;
//			
////			try
////			{
////				MailingTypesVector mtv = MailingTypeDAL.LoadCollection();//.SelectProjectNamesClear(this.ProjectsStatus, int.Parse(ddlBuildingTypes.SelectedValue));
////			
////				ddlMailingType.DataSource = mtv;
////				ddlMailingType.DataValueField = "MailingTypeID";
////				ddlMailingType.DataTextField = "MailingTypeName";
////				ddlMailingType.DataBind();
////
////				ddlMailingType.Items.Insert(0, new ListItem("", "-1"));
////				ddlMailingType.SelectedValue = "-1";
////			}
////			catch (Exception ex)
////			{
////				log.Error(ex);
////				return false;
////			}
////
////			finally
////			{
////				if (reader!=null) reader.Close();
////			}
//			return true;
//		}

        private void LoadRadioButtons()
        {
            ListItem li = new ListItem(Resource.ResourceManager["Corespondency_Send"], "True");
            rbSendRes.Items.Add(li);
            li = new ListItem(Resource.ResourceManager["Corespondency_Reseve"], "False");
            rbSendRes.Items.Add(li);
            rbSendRes.SelectedIndex = 0;
        }
        private bool LoadClients()
        {
            SqlDataReader reader = null;
            try
            {

                //reader = ClientsData.SelectClients(0,(int)ClientTypes.Client,false);
                reader = ClientsData.SelectClientsFromProjects(0, (int)ClientTypes.Client, UIHelpers.ToInt(ddlProject.SelectedValue), UIHelpers.ToInt(ddlBuildingTypes.SelectedValue), UIHelpers.ToInt(ddlProjectsStatus.SelectedValue));
                ddlClients.DataSource = reader;
                ddlClients.DataValueField = "ClientID";
                ddlClients.DataTextField = "ClientName";
                ddlClients.DataBind();

                ddlClients.Items.Insert(0, new ListItem("", "0"));
                ddlClients.SelectedIndex = 0;
            }
            catch (Exception ex)
            {
                log.Info(ex);
                return false;
            }
            finally
            {
                if (reader != null) reader.Close();
            }
            return true;
        }
        private void LoadCheckBoxTypes()
        {
            for (int i = 0; i < _mtv.Count; i++)
            {
                MailingTypeData mtd = _mtv[i];
                ListItem li = new ListItem(mtd.MailingTypeName, "False");
                cblMailingType.Items.Add(li);
            }
            //			ListItem li = new ListItem(Resource.ResourceManager["Corespondency_Send"],"True");
            //			rbSendRes.Items.Add(li);
            //			li = new ListItem(Resource.ResourceManager["Corespondency_Reseve"],"False");
            //			rbSendRes.Items.Add(li);
            //			rbSendRes.SelectedIndex = 0;
        }
		#endregion
	
		#region Web Form Designer generated code
        override protected void OnInit(EventArgs e)
        {
            //
            // CODEGEN: This call is required by the ASP.NET Web Form Designer.
            //
            InitializeComponent();
            base.OnInit(e);
        }
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
        private void InitializeComponent()
        {
            this.btnSaveUP.Click += new System.EventHandler(this.btnSave_Click);
            this.btnCancelUP.Click += new System.EventHandler(this.btnCancel_Click);
            this.ddlProjectsStatus.SelectedIndexChanged += new System.EventHandler(this.ddlProjectsStatus_SelectedIndexChanged);
            this.ddlBuildingTypes.SelectedIndexChanged += new System.EventHandler(this.ddlBuildingTypes_SelectedIndexChanged);
            this.ddlProject.SelectedIndexChanged += new System.EventHandler(this.ddlProject_SelectedIndexChanged);
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            this.btnFile.Click += new System.Web.UI.ImageClickEventHandler(this.btnFile_Click);
            this.Load += new System.EventHandler(this.Page_Load);

        }
		#endregion
	
		#region Event handlers
        private void ddlProjectsStatus_SelectedIndexChanged(object sender, System.EventArgs e)
        {
            LoadProjects();
        }

        private void ddlBuildingTypes_SelectedIndexChanged(object sender, System.EventArgs e)
        {
            LoadProjects();
        }

        private void btnSave_Click(object sender, System.EventArgs e)
        {
            //			if (ddlProject.SelectedIndex==0) 
            //			{
            //				AlertFieldNotEntered(lblName);
            //				Dirty=true;
            //				return;
            //			}		
            if (txtStartDate.Text.Trim() == "" || UIHelpers.GetDate(txtStartDate) == Constants.DateMax)
            {
                AlertFieldNotEntered(lbDate);
                Dirty = true;
                return;
            }
            bool IsMailingTypeSelected = false;
            foreach (ListItem li in cblMailingType.Items)
            {
                if (li.Selected)
                {
                    IsMailingTypeSelected = true;
                    break;
                }
            }
            if (!IsMailingTypeSelected)
            {
                AlertFieldNotEntered(lbMailingType);
                Dirty = true;
                return;
            }
            if (txtEnterNumber.Text.Length == 0)
            {
                AlertFieldNotEntered(lbEnterNumber);
                Dirty = true;
                return;
            }
            int ID = UIHelpers.GetIDParam();
            //			bool bHasScanBefore = false;
            //			
            DateTime dtMeet = UIHelpers.GetDate(txtStartDate);
            //			if(ID>0)
            //			{
            //				ProtokolData mdd = ProtokolDAL.Load(ID);
            //				bHasScanBefore=mdd.HasScan;
            //				
            //			}
            //			else
            //			{
            //				
            //				
            //			}
            bool InsertOrUpdate = (ID < 1);
            //notes:MailingNumber is ID if there is not set from user
            //int iEnterNumber = -1;
            //			if(UIHelpers.ToInt(txtEnterNumber.Text)== -1)
            //				 iEnterNumber = ID;
            //			else 
            //				iEnterNumber = UIHelpers.ToInt(txtEnterNumber.Text);
            string extToDB = lbExt.Text;
            if (FileCtrl.PostedFile.FileName.Length != 0)
            {
                string name = FileCtrl.PostedFile.FileName;
                int n = name.LastIndexOf(".");
                if (n != -1)
                {
                    extToDB = name.Substring(n);
                }
            }
            MailingData md = new MailingData(ID, UIHelpers.ToInt(ddlProject.SelectedValue), dtMeet, rbSendRes.SelectedIndex == (int)MailingSendReseve.Send, (FileCtrl.PostedFile.FileName.Length != 0 || lbSD.Text == _hasScan), UIHelpers.ToInt(txtEnterNumber.Text), txtNotes.Text, UIHelpers.ToInt(ddlClients.SelectedValue), GetMailingTypesText(), extToDB);
            md.Person = txtPerson.Text;
            MailingDAL.Save(md);
            if (InsertOrUpdate)
                log.Info(string.Format("Mailing {0} has been INSERTED by {1}", md.MailingID, string.Concat(LoggedUser.FullName, " ( userID ", LoggedUser.UserID, " )")));
            else
                log.Info(string.Format("Mailing {0} has been UPDATED by {1}", md.MailingID, string.Concat(LoggedUser.FullName, " ( userID ", LoggedUser.UserID, " )")));
            if (FileCtrl.PostedFile.FileName.Length != 0)
            {
                string name = FileCtrl.PostedFile.FileName;
                int n = name.LastIndexOf(".");
                if (n != -1)
                {
                    string ext = name.Substring(n);
                    if (ext == System.Configuration.ConfigurationManager.AppSettings["Extension"])
                    {
                        //						ProjectProtokolData mdd = new ProjectProtokolData(-1,md.ProtokolID,false);
                        //						ProjectProtokolDAL.Save(mdd);
                        //						log.Info(string.Format("ScanDoc {0} of Protocol {2} has been INSERTED by {1}",mdd.ProjectProtokolID,string.Concat(LoggedUser.FullName," ( userID ",LoggedUser.UserID," )"),md.ProtokolID));
                        string path = string.Concat(System.Configuration.ConfigurationManager.AppSettings["ScanPathMailing"], md.MailingID, ext);
                        string mapped = Request.MapPath(path);
                        FileCtrl.PostedFile.SaveAs(mapped);
                        //						md.HasScan=true;
                        //						ProtokolDAL.Save(md);		
                        //						string sc = string.Empty;//new StringCollection();
                        //						SqlDataReader reader = UsersData.SelectUsersMainAccMailListProc();
                        //						while (reader.Read())
                        //						{
                        //							string email = reader.GetString(1);
                        //							string emailName = reader.GetString(0);
                        //							//					sc.Add(String.Concat(email, ":", emailName)); 
                        //							sc = string.Concat(sc,";",email);
                        //						}
                        //						sc = sc.Substring(1);
                        //						MailBO.SendMailOfficer(sc,ddlProject.SelectedItem.Text,txtStartDate.Text,mapped,true);
                        //						UserInfo Director = UsersData.UsersListByRoleProc(1);
                        //						
                        //						MailBO.SendMailMinutes(Director.Mail, mapped,ddlProject.SelectedItem.Text,txtStartDate.Text);
                    }
                    //					else
                    //					{
                    //						md.HasScan = false;
                    //						MailingDAL.Save(md);
                    //					}
                }

            }
            //notes:if selected send letters to direktor, iurist i glaven shcetovoditel
            if (cbSendLetter.Checked)
            {
                MailBO.SendMailCorespondency(rbSendRes.SelectedItem.Text, md.MailingID, GetMailingTypesName()/*ddlMailingType.SelectedItem.Text*/, dtMeet.ToShortDateString(), txtEnterNumber.Text, ddlProject.SelectedItem.Text, ddlClients.SelectedItem.Value, txtPerson.Text, txtNotes.Text);
            }
            Response.Redirect("Corespondency.aspx" + "?pid=" + UIHelpers.GetPIDParam());
        }

        private void btnCancel_Click(object sender, System.EventArgs e)
        {
            Response.Redirect("Corespondency.aspx" + "?pid=" + UIHelpers.GetPIDParam());
        }
		
		
		#endregion

        private string GetMailingTypesText()
        {
            string returnText = "";
            for (int i = 0; i < cblMailingType.Items.Count; i++)
            {
                ListItem li = cblMailingType.Items[i];
                if (li.Selected)
                    returnText = string.Concat(returnText, _mtv[i].MailingTypeID, Constants._separator);
            }
            returnText.TrimEnd(Constants._separator);
            return returnText;
        }

        private void SetMailingTypes(string sText)
        {
            string[] ArrayNumbers = sText.Split(Constants._separator);
            foreach (string sMailingTypeID in ArrayNumbers)
            {
                int iMailingTypeID = UIHelpers.ToInt(sMailingTypeID);
                if (iMailingTypeID == -1)
                    continue;
                for (int i = 0; i < _mtv.Count; i++)
                {
                    if (_mtv[i].MailingTypeID == iMailingTypeID)
                        cblMailingType.Items[i].Selected = true;
                }
            }
        }
        private string GetMailingTypesName()
        {
            string returnText = "";
            for (int i = 0; i < cblMailingType.Items.Count; i++)
            {
                ListItem li = cblMailingType.Items[i];
                if (li.Selected)
                    returnText = string.Concat(returnText, _mtv[i].MailingTypeName, Constants._separator);
            }
            returnText = returnText.TrimEnd(Constants._separator);
            return returnText;
        }

        private void btnFile_Click(object sender, System.Web.UI.ImageClickEventArgs e)
        {

            int id = UIHelpers.GetIDParam();
            string filePath = /*Server.MapPath(*/string.Concat(System.Configuration.ConfigurationManager.AppSettings["ScanPathMailing"], id, lbExt.Text);//);

            Response.Cache.SetExpires(DateTime.Now.AddSeconds(-1));
            Response.Cache.SetCacheability(HttpCacheability.Private);
            Response.AppendHeader("content-disposition", "attachment; filename=" + id.ToString() + lbExt.Text);
            //Response.AppendHeader("content-disposition", "attachment; filename="+ID+e.Item.Cells[(int)gridColomns.Type].Text);

            Response.Charset = "";
            Response.TransmitFile(filePath);
            Response.End();
        }

        private void ddlProject_SelectedIndexChanged(object sender, System.EventArgs e)
        {
            LoadClients();
        }
	}
}
