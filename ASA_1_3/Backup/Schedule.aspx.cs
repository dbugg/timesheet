using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Data.SqlClient;
using log4net;
using Asa.Timesheet.Data;
using Asa.Timesheet.Data.Entities;
using Asa.Timesheet.Data.Util;
using GrapeCity.ActiveReports.Export.Pdf.Section;
using GrapeCity.ActiveReports.Export.Excel.Section;
using Asa.Timesheet.WebPages.Reports;

using System.IO;

namespace Asa.Timesheet.WebPages
{
	/// <summary>
	/// Summary description for Schedule.
	/// </summary>
	public class Schedule : TimesheetPageBase
	{
		#region WebControls
			
		protected System.Web.UI.WebControls.PlaceHolder menuHolder;
		protected System.Web.UI.WebControls.Label lblError;
		protected System.Web.UI.WebControls.Label lblInfo;
		protected System.Web.UI.WebControls.Label lblBuildingType;
		protected System.Web.UI.WebControls.DropDownList ddlBuildingType;
		protected System.Web.UI.WebControls.ImageButton ibXlsExport;
		protected System.Web.UI.WebControls.ImageButton ibGenerate;
		protected System.Web.UI.WebControls.DataGrid grdReport;
		protected System.Web.UI.WebControls.Label lblReportTitle;
		protected Asa.Timesheet.WebPages.UserControls.PageHeader header;
		protected System.Web.UI.WebControls.Label lblNoDataFound;
		protected System.Web.UI.WebControls.DropDownList ddlMonthCount;
		protected System.Web.UI.WebControls.Label lbMountCount;
		protected System.Web.UI.WebControls.DropDownList ddlDaysOrWeek;
		private static readonly ILog log = LogManager.GetLogger(typeof(Schedule));
		private const int MonthMaxDays = 9;
		protected System.Web.UI.WebControls.DataGrid grdReport1;
		protected System.Web.UI.WebControls.Button btnSave;
		protected System.Web.UI.WebControls.Button btnUncheck;
		private const int MonthMaxWeekly = 13;

		
		#endregion
		

		private enum gridColomn
		{
			ProjectScheduleID = 0,
			ProjectID,
			SubcontracterID,
			IsProjectMain,
			Checks,
			ProjectName,
			SubcontracterName,
			StartDate,
			EndDate,
			Notes,
//			ArrowUp,
//			ArrowDown,
		}

	
		private enum TableColomns
		{
			ProjectScheduleID = 0,
			ProjectID,
			SubcontracterID,
			IsProjectMain,
			ProjectName,
			SubcontracterName ,
			StartDate      ,
			EndDate,
			ProjectCode,
			FullName,
			Notes,
		}
	
	
		#region PageLoad
        private void Page_Load(object sender, System.EventArgs e)
        {
            if (!LoggedUser.IsLeader)
            {
                ErrorRedirect(Resource.ResourceManager["noRightsAlert"]);
            }

            UIHelpers.CreateMenu(menuHolder, LoggedUser);

            if (!this.IsPostBack)
            {
                header.PageTitle = Resource.ResourceManager["Schedule_PageTitle"];
                header.UserName = this.LoggedUser.UserName;
                CheckProjectSchedule();
                UIHelpers.LoadBuildingTypes(ddlBuildingType);
                BindDaysOrWeek();
                BindMonth();
                LoadProjects();
            }

            SetCalendars();
        }

        protected bool GetVisible(object ProjectScheduleID)
        {
            if (ProjectScheduleID is string && (string)ProjectScheduleID == "-1")
                return true;
            else
                return false;
        }
        protected bool GetVisibleForTextBox(object ProjectScheduleID)
        {
            if (ProjectScheduleID is string && (string)ProjectScheduleID == "-1")
                return false;
            else
                return true;
        }

        protected string GetDateFromDB(object Date)
        {
            if (Date != DBNull.Value)
            {
                DateTime date = DateTime.Parse((string)Date);
                if (date != Constants.DateMax)
                    return date.ToString("dd.MM.yyyy");
                else
                    return "";
            }
            else
                return "";
        }
		#endregion

		#region DataBase
        private void CheckProjectSchedule()
        {
            string projectToAct = "";
            DataSet ds = ReportsData.CheckProjectScheduleTable();
            DataTable dtChecks = ds.Tables[0];
            //notes:this data table contain 2 colomns
            //	one with projectID and second 
            //if everything is all right in the DB with current project
            foreach (DataRow dr in dtChecks.Rows)
                if (dr[1] is int && (int)dr[1] == 0)
                    projectToAct = string.Concat(projectToAct, dr[0].ToString(), ";");
            string[] projectsIDs = projectToAct.Split(';');
            foreach (string sProjectID in projectsIDs)
            {
                if (UIHelpers.ToInt(sProjectID) > 0)
                    CheckProject(UIHelpers.ToInt(sProjectID));
            }
        }
        private void CheckProject(int projectID)
        {
            ProjectSchedulesVector psv = ProjectScheduleDAL.LoadCollectionByProject(projectID);

            ProjectSubcontractersVector pSubV = ProjectSubcontracterDAL.LoadCollection("ProjectSubcontractersListProc1", SQLParms.CreateProjectSubcontractersListProc1(projectID, false));
            bool hasMain = false;
            foreach (ProjectScheduleData psd in psv)
            {
                if (psd.IsProjectMain)
                    hasMain = true;
                if (pSubV.GetByID(psd.SubcontracterID) != null)
                    pSubV.Remove(pSubV.GetByID(psd.SubcontracterID));
            }
            if (!hasMain)
            {
                ProjectScheduleData dat = new ProjectScheduleData(-1, DateTime.MinValue, DateTime.MinValue, -1, true, projectID, "");
                ProjectScheduleDAL.Save(dat);
            }
            foreach (ProjectSubcontracterData pSubD in pSubV)
            {
                ProjectScheduleData dat = new ProjectScheduleData(-1, DateTime.MinValue, DateTime.MinValue, pSubD.SubcontracterID, false, projectID, "");
                ProjectScheduleDAL.Save(dat);
            }
        }
        private DataTable CreateDataTableForSchedule(DataTable dt, DataTable dtSchedule)
        {
            DataTable dtReturn = new DataTable();
            foreach (DataRow dr in dtSchedule.Rows)
            {
                string type = dr[(int)TableColomns.IsProjectMain].GetType().Name;
                if ((dr[(int)TableColomns.IsProjectMain] is bool) && (bool)dr[(int)TableColomns.IsProjectMain])
                    dr[(int)TableColomns.SubcontracterName] = Resource.ResourceManager["schedule_architecture"];
            }
            foreach (DataColumn dc in dt.Columns)
                dtReturn.Columns.Add(dc.ColumnName);
            foreach (DataRow dr in dt.Rows)
            {
                if (dr[(int)TableColomns.ProjectID] is int)
                {
                    DataRow drNew = dtReturn.NewRow();
                    drNew.ItemArray = dr.ItemArray;
                    dtReturn.Rows.Add(drNew);
                    int projectID = (int)dr[(int)TableColomns.ProjectID];
                    foreach (DataRow drSchedule in dtSchedule.Rows)
                    {
                        if (drSchedule[(int)TableColomns.ProjectID] is int && (int)drSchedule[(int)TableColomns.ProjectID] == projectID)
                        {
                            drNew = dtReturn.NewRow();
                            drNew.ItemArray = drSchedule.ItemArray;
                            dtReturn.Rows.Add(drNew);
                        }
                    }
                }
            }
            return dtReturn;
        }
        private void LoadProjects()
        {
            DataSet ds = ReportsData.ExecuteReportsSchedule(UIHelpers.ToInt(ddlBuildingType.SelectedValue));
            //			DataTable dt = MakeTable(ds.Tables[0]);
            DataTable dt = CreateDataTableForSchedule(ds.Tables[0], ds.Tables[1]);

            //			DataView dv = new DataView(ds.Tables[0]);
            //			grdReport.DataSource = dv;
            //			grdReport.DataKeyField = "ProjectID";
            //			grdReport.DataBind();

            DataView dv1 = new DataView(dt);
            grdReport1.DataSource = dv1;
            grdReport1.DataBind();
            //			if(grdReport.Items.Count==0)
            //			{
            //				lblNoDataFound.Visible=true;
            //				grdReport.Visible=false;
            //			}
            //			else
            //			{
            //				grdReport.Visible=true;
            //				
            //				lblNoDataFound.Visible=false;
            //			}
            if (grdReport.Items.Count > 1)
            {
                ImageButton imgArrowUp = (ImageButton)grdReport.Items[0].FindControl("imgArrowUp");
                imgArrowUp.Visible = false;
                ImageButton imgArrowDown = (ImageButton)grdReport.Items[grdReport.Items.Count - 1].FindControl("imgArrowDown");
                imgArrowDown.Visible = false;
            }
        }
//		private bool LoadBuildingTypes()
//		{
//			SqlDataReader reader = null;
//			try
//			{
//				reader = DBManager.SelectBuildingTypes();
//				ddlBuildingType.DataSource = reader;
//				ddlBuildingType.DataValueField = "BuildingTypeID";
//				ddlBuildingType.DataTextField = "BuildingType";
//				ddlBuildingType.DataBind();
//
//				ddlBuildingType.Items.Insert(0, new ListItem("<"+Resource.ResourceManager["reports_ddlAllBuildingTypes"]+">", "0"));
//				ddlBuildingType.SelectedValue = "0";
//			}
//			catch (Exception ex)
//			{
//				log.Info(ex);
//				return false;
//			}
//			finally 
//			{
//				if (reader!=null) reader.Close();
//			}
//
//			return true;
//		}

        private void BindMonth()
        {
            ddlMonthCount.Items.Clear();
            int MonthMax = MonthMaxDays;
            if (ddlDaysOrWeek.SelectedValue != "1")
                MonthMax = MonthMaxWeekly;
            ListItem li = new ListItem();
            for (int i = 1; i < MonthMax; i++)
            {
                //				if(ddlDaysOrWeek.SelectedValue=="1")
                li = new ListItem(i.ToString(), i.ToString());
                //				else
                //				{
                //					int i1 = i;
                //					li = new ListItem(i1.ToString(),i1.ToString());
                //				}
                ddlMonthCount.Items.Add(li);
            }

            ddlMonthCount.SelectedValue = "3";
        }
        private void BindDaysOrWeek()
        {
            ListItem li = new ListItem(Resource.ResourceManager["ddlDays"], "1");
            ddlDaysOrWeek.Items.Add(li);
            li = new ListItem(Resource.ResourceManager["ddlWeeks"], "2");
            ddlDaysOrWeek.Items.Add(li);
            ddlDaysOrWeek.SelectedValue = "1";
        }
		#endregion
        
		#region EventHandlers
        private void ddlDaysOrWeek_SelectedIndexChanged(object sender, System.EventArgs e)
        {
            BindMonth();
        }
        private void ibGenerate_Click(object sender, System.Web.UI.ImageClickEventArgs e)
        {

        }

        private void ibXlsExport_Click(object sender, System.Web.UI.ImageClickEventArgs e)
        {
            DataSet ds = ReportsData.ExecuteReportsSchedule(UIHelpers.ToInt(ddlBuildingType.SelectedValue));
            DataTable dt = MakeTable(ds.Tables[0]);
            DataView dv = new DataView(dt);

            XlsExport pdf = new XlsExport();


            GrapeCity.ActiveReports.SectionReport report = new ScheduleRpt(dv, UIHelpers.ToInt(ddlMonthCount.SelectedValue), ddlDaysOrWeek.SelectedValue == "2");
            report.PageSettings.Orientation = GrapeCity.ActiveReports.Document.Section.PageOrientation.Landscape;

            report.Run();

            System.IO.MemoryStream memoryFile = new System.IO.MemoryStream();
            pdf.Export(report.Document, memoryFile);

            Response.Clear();
            Response.AppendHeader("content-disposition"
                , "attachment; filename=" + "Schedule.xls");
            Response.ContentType = "application/pdf";

            memoryFile.WriteTo(Response.OutputStream);
            Response.End();
        }
		 
		//notes:here create Table for schedule
        private DataTable MakeTable(DataTable dt)
        {
            //notes:dt1,dt2 - first and second colomns in schedule
            //	dt3 - projects id
            //	dt4,dt5 - startdate,enddate
            //	dtNotes - notes
            DataTable dt1 = new DataTable();
            DataTable dt2 = new DataTable();
            DataTable dt3 = new DataTable();
            DataTable dt4 = new DataTable();
            DataTable dt5 = new DataTable();
            DataTable dtNotes = new DataTable();
            dt1.Columns.Add();
            dt2.Columns.Add();
            dt3.Columns.Add();
            dt4.Columns.Add();
            dt5.Columns.Add();
            dtNotes.Columns.Add();
            foreach (DataRow dr in dt.Rows)
            {
                int projectID = UIHelpers.ToInt(dr[1].ToString());
                bool vis = true;
                if (grdReport1.Visible)
                    vis = FindCheckBox(projectID);
                if (vis)
                {
                    bool hasEmptyRow = false;
                    AddRow(dt1, dr[(int)TableColomns.ProjectName].ToString());
                    AddRow(dt2, dr[(int)TableColomns.ProjectCode].ToString());
                    //AddEmptyRows(dt4,1);
                    //AddEmptyRows(dt5,1);


                    AddRow(dt1, Resource.ResourceManager["ProjectManager"]);
                    AddRow(dt2, dr[(int)TableColomns.FullName].ToString());
                    //AddEmptyRows(dt4,1);
                    //AddEmptyRows(dt5,1);

                    ProjectUsersVector puv = ProjectUserDAL.LoadCollection("ProjectUsersSelByProjectProc", SQLParms.CreateProjectUsersSelByProjectProc(projectID));
                    if (puv.Count != 0)
                    {
                        AddRow(dt1, Resource.ResourceManager["ProjectTeam"]);
                        hasEmptyRow = true;
                    }
                    bool TeamOfTraine = true;
                    ArrayList traines = new ArrayList();
                    foreach (ProjectUserData pud in puv)
                    {

                        string UserName;
                        string FirstName = string.Empty;
                        string LastName = string.Empty;
                        //bool Traine=false;
                        SqlDataReader reader = null;
                        try
                        {
                            reader = UsersData.SelectUser(pud.UserID);
                            reader.Read();
                            FirstName = reader.IsDBNull(2) ? String.Empty : reader.GetString(2);
                            LastName = reader.GetString(4);
                            //							if (!reader.IsDBNull(19))
                            //								Traine=reader.GetBoolean(19);
                            //							else
                            //Traine=false;
                        }
                        catch
                        {
                            UserName = "";
                            //Traine = false;
                        }
                        UserName = string.Concat(FirstName, " ", LastName);
                        //						if(Traine)
                        //						{
                        //							traines.Add(UserName);
                        //							//							AddRow(dt1,Resource.ResourceManager["UserTraine"]);
                        //						}
                        //						else
                        {
                            AddRow(dt2, UserName);
                            TeamOfTraine = false;
                        }
                    }
                    if (TeamOfTraine && puv.Count != 0)
                        dt1.Rows.RemoveAt(dt1.Rows.Count - 1);




                    if (dt1.Rows.Count != dt2.Rows.Count)
                    {
                        if (dt1.Rows.Count > dt2.Rows.Count)
                            AddEmptyRows(dt2, dt1.Rows.Count - dt2.Rows.Count);
                        else
                            AddEmptyRows(dt1, dt2.Rows.Count - dt1.Rows.Count);
                    }

                    //					if(traines.Count>0)
                    //						AddRow(dt1,Resource.ResourceManager["UserTraine"]);
                    //					for(int l=0;l<traines.Count;l++)
                    //					{
                    //						AddRow(dt2,traines[l].ToString());
                    //					}
                    if (dt1.Rows.Count != dt2.Rows.Count)
                    {
                        if (dt1.Rows.Count > dt2.Rows.Count)
                            AddEmptyRows(dt2, dt1.Rows.Count - dt2.Rows.Count);
                        else
                            AddEmptyRows(dt1, dt2.Rows.Count - dt1.Rows.Count);
                    }

                    AddEmptyRows(dt4, dt1.Rows.Count - dt4.Rows.Count);
                    AddEmptyRows(dt5, dt1.Rows.Count - dt5.Rows.Count);
                    AddEmptyRows(dtNotes, dt1.Rows.Count - dtNotes.Rows.Count);
                    //					AddRow(dt1,Resource.ResourceManager["schedule_architecture"]);
                    //					AddEmptyRows(dt2,1);

                    //					AddRow(dt2," ");
                    ProjectSchedulesVector psv = ProjectScheduleDAL.LoadCollectionByProject(projectID);
                    foreach (ProjectScheduleData psd in psv)
                    {
                        if (psd.SubcontracterID > 0)
                        {
                            SubcontracterData dat = SubcontracterDAL.Load(psd.SubcontracterID);
                            AddRow(dt1, dat.SubcontracterType);
                            //						string ps = string.Concat(psd.SubcontracterType," ","-"," ", psd.SubcontracterName);
                            AddRow(dt2, dat.SubcontracterName);
                            hasEmptyRow = true;
                        }
                        else
                        {

                            AddRow(dt1, Resource.ResourceManager["schedule_architecture"]);
                            AddEmptyRows(dt2, 1);
                            hasEmptyRow = true;
                        }
                        if (psd.StartDate != DateTime.MinValue)
                            AddRow(dt4, psd.StartDate.ToString("dd.MM.yyyy"));
                        else
                            AddEmptyRows(dt4, 1);
                        if (psd.EndDate != DateTime.MinValue)
                            AddRow(dt5, psd.EndDate.ToString("dd.MM.yyyy"));
                        else
                            AddEmptyRows(dt5, 1);
                        AddRow(dtNotes, psd.Notes);
                    }

                    if (!hasEmptyRow)
                    {
                        AddEmptyRows(dt1, 1);
                        AddEmptyRows(dt2, 1);
                        AddEmptyRows(dt3, 1);
                    }


                    AddRow(dt3, dr[1].ToString());
                    AddEmptyRows(dt3, dt1.Rows.Count - dt3.Rows.Count);


                }

            }
            dt.Rows.Clear();
            for (int i = 0; i < dt1.Rows.Count; i++)
            {
                DataRow dr = dt.NewRow();
                dr[(int)TableColomns.ProjectID] = UIHelpers.ToInt(dt3.Rows[i][0].ToString());
                dr[(int)TableColomns.ProjectName] = dt1.Rows[i][0];
                dr[(int)TableColomns.SubcontracterName] = dt2.Rows[i][0];
                dr[(int)TableColomns.StartDate] = dt4.Rows[i][0];
                dr[(int)TableColomns.EndDate] = dt5.Rows[i][0];
                dr[(int)TableColomns.Notes] = dtNotes.Rows[i][0];
                dt.Rows.Add(dr);
            }
            return dt;
        }

        private bool FindCheckBox(int ID)
        {

            for (int i = 0; i < grdReport1.Items.Count; i++)
            {
                int CurrID = UIHelpers.ToInt(grdReport1.Items[i].Cells[1].Text);
                if (UIHelpers.ToInt(grdReport1.Items[i].Cells[0].ToString()) != -1)
                    continue;
                if (CurrID == ID)
                {
                    CheckBox cb = (CheckBox)grdReport1.Items[i].FindControl("cbShow1");
                    if (cb.Checked)
                        return true;
                    else return false;
                }

            }
            return true;
        }
        private void AddRow(DataTable dt, string Value)
        {
            DataRow dr = dt.NewRow();
            dr[0] = Value;
            dt.Rows.Add(dr);
        }
        private void AddEmptyRows(DataTable dt, int RowToAdd)
        {
            for (int i = 0; i < RowToAdd; i++)
            {
                AddRow(dt, " ");
            }
        }
//		private void grdReport_ItemDataBound(object sender, System.Web.UI.WebControls.DataGridItemEventArgs e)
//		{
//			if ((e.Item.ItemType == ListItemType.Item) || (e.Item.ItemType == ListItemType.AlternatingItem))
//			{
//				int i = UIHelpers.ToInt(e.Item.Cells[(int)gridColomn.OrderNumber].Text);
//				if(i==0)
//				{
//					int ID = (int)grdReport.DataKeys[e.Item.ItemIndex];
//					ProjectsData.SetOrderNumber(ID,-1);
//
//
//					e.Item.Cells[(int)gridColomn.OrderNumber].Text =  ID.ToString();
//				}
//			}
//		}
        private void ddlBuildingType_SelectedIndexChanged(object sender, System.EventArgs e)
        {
            LoadProjects();
        }


        private void grdReport_ItemCommand(object source, System.Web.UI.WebControls.DataGridCommandEventArgs e)
        {
            //			if(e.CommandSource is ImageButton)
            //			{
            //				if(e.CommandName == "Up")
            //				{
            //					
            ////					
            ////					int iCurrentOrderNumber = UIHelpers.ToInt(e.Item.Cells[(int)gridColomn.OrderNumber].Text);
            ////					int iNewOrderNumber = UIHelpers.ToInt(grdReport.Items[itemIndex-1].Cells[(int)gridColomn.OrderNumber].Text);
            ////					int iCurrentID = (int)grdReport.DataKeys[e.Item.ItemIndex];
            ////					int iNewID = (int)grdReport.DataKeys[e.Item.ItemIndex-1];
            ////					ProjectsData.SetOrderNumber(iCurrentID,iNewOrderNumber);
            ////					ProjectsData.SetOrderNumber(iNewID,iCurrentOrderNumber);
            ////					LoadProjects();
            //						
            //								
            //								
            //					//				DataGrid dg =(DataGrid) dlBuildings.Items[nNum-1].FindControl("grdDistr");
            //					//			int ii = e.Item.ItemIndex;	
            //					//				TextBox txtS=(TextBox) dlBuildings.Items[dg.TabIndex-1].FindControl("txtS");
            //					//				string sign="";
            //					//				if(txtS!=null)
            //					//					sign=txtS.Text;
            //					//				ContentsVector DistrTable=GetDistr(projectID,dg,sign);
            //					//				ContentData cd = DistrTable[ii];
            //					//				ContentData cdUp = DistrTable[ii-1];
            //					//				DistrTable.RemoveAt(ii);
            //					//				DistrTable.Insert(ii,cdUp);
            //					//				DistrTable.RemoveAt(ii-1);
            //					//				DistrTable.Insert(ii-1,cd);
            //					//				SetDistrTable(nNum,DistrTable);
            //					//				
            //					//				BindGrid();
            //					//				
            //					//				return;
            //				}
            ////				if(e.CommandName ==	"Down")
            ////				{
            ////					int itemIndex = e.Item.ItemIndex;
            ////					int iCurrentOrderNumber = UIHelpers.ToInt(e.Item.Cells[(int)gridColomn.OrderNumber].Text);
            ////					int iNewOrderNumber = UIHelpers.ToInt(grdReport.Items[itemIndex+1].Cells[(int)gridColomn.OrderNumber].Text);
            ////					int iCurrentID = (int)grdReport.DataKeys[e.Item.ItemIndex];
            ////					int iNewID = (int)grdReport.DataKeys[e.Item.ItemIndex+1];
            ////					ProjectsData.SetOrderNumber(iCurrentID,iNewOrderNumber);
            ////					ProjectsData.SetOrderNumber(iNewID,iCurrentOrderNumber);
            ////					LoadProjects();
            ////								
            ////
            ////								
            ////					//				//				DataGrid dg =(DataGrid) dlBuildings.Items[nNum-1].FindControl("grdDistr");
            ////					//				int ii = e.Item.ItemIndex;				
            ////					//				TextBox txtS=(TextBox) dlBuildings.Items[dg.TabIndex-1].FindControl("txtS");
            ////					//				string sign="";
            ////					//				if(txtS!=null)
            ////					//					sign=txtS.Text;
            ////					//				ContentsVector DistrTable=GetDistr(projectID,dg,sign);
            ////					//				ContentData cd = DistrTable[ii];
            ////					//				ContentData cdDown = DistrTable[ii+1];
            ////					//				DistrTable.RemoveAt(ii);
            ////					//				DistrTable.Insert(ii,cdDown);
            ////					//				DistrTable.RemoveAt(ii+1);
            ////					//				DistrTable.Insert(ii+1,cd);
            ////					//				SetDistrTable(nNum,DistrTable);
            ////					//				BindGrid();
            ////					//				return;
            ////				}
            //			}
        }


        private void btnSave_Click(object sender, System.EventArgs e)
        {
            SaveSchedule();
        }


        private void SaveSchedule()
        {
            foreach (DataGridItem dgi in grdReport1.Items)
            {
                if (dgi.Cells[0].Text != "-1")
                {
                    int id = UIHelpers.ToInt(dgi.Cells[0].Text);
                    TextBox txtstartDate = (TextBox)dgi.FindControl("txtStartDate");
                    DateTime dtSd = TimeHelper.GetDate(txtstartDate.Text);
                    TextBox txtendDate = (TextBox)dgi.FindControl("txtEndDate");
                    DateTime dtEd = TimeHelper.GetDate(txtendDate.Text);
                    TextBox txtNotes = (TextBox)dgi.FindControl("txtNotes");
                    //if(txtstartDate.Text.Length != 0||txtendDate.Text.Length != 0||txtNotes.Text.Length>0)
                    {
                        ProjectScheduleData psd = ProjectScheduleDAL.Load(id);
                        if (psd == null)
                        {
                            psd = new ProjectScheduleData();
                            psd.ProjectID = UIHelpers.ToInt(dgi.Cells[(int)gridColomn.ProjectID].Text);
                            psd.SubcontracterID = UIHelpers.ToInt(dgi.Cells[(int)gridColomn.SubcontracterID].Text);

                        }
                        //if(txtstartDate.Text.Length != 0)
                        psd.StartDate = dtSd;
                        //if(txtendDate.Text.Length != 0)
                        psd.EndDate = dtEd;
                        psd.Notes = txtNotes.Text;
                        ProjectScheduleDAL.Save(psd);
                    }
                }

            }
        }


        private void SetCalendars()
        {
            foreach (DataGridItem dgi in grdReport1.Items)
            {
                HtmlImage img1 = (HtmlImage)dgi.FindControl("lkCalendar1");
                if (img1.Visible)
                {
                    TextBox txtstartDate = (TextBox)dgi.FindControl("txtStartDate");
                    img1.Attributes["onclick"] = TimeHelper.InvokePopupCal(txtstartDate);
                }
                HtmlImage img2 = (HtmlImage)dgi.FindControl("lkCalendar2");
                if (img2.Visible)
                {
                    TextBox txtendDate = (TextBox)dgi.FindControl("txtEndDate");
                    img2.Attributes["onclick"] = TimeHelper.InvokePopupCal(txtendDate);
                }

            }
        }
		
	
		#endregion

		#region Web Form Designer generated code
        override protected void OnInit(EventArgs e)
        {
            //
            // CODEGEN: This call is required by the ASP.NET Web Form Designer.
            //
            InitializeComponent();
            base.OnInit(e);
        }
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
        private void InitializeComponent()
        {
            this.ddlDaysOrWeek.SelectedIndexChanged += new System.EventHandler(this.ddlDaysOrWeek_SelectedIndexChanged);
            this.ddlBuildingType.SelectedIndexChanged += new System.EventHandler(this.ddlBuildingType_SelectedIndexChanged);
            this.ibGenerate.Click += new System.Web.UI.ImageClickEventHandler(this.ibGenerate_Click);
            this.ibXlsExport.Click += new System.Web.UI.ImageClickEventHandler(this.ibXlsExport_Click);
            this.grdReport1.ItemCommand += new System.Web.UI.WebControls.DataGridCommandEventHandler(this.grdReport_ItemCommand);
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            this.btnUncheck.Click += new System.EventHandler(this.btnUncheck_Click);
            this.Load += new System.EventHandler(this.Page_Load);

        }
		#endregion

        private void btnUncheck_Click(object sender, System.EventArgs e)
        {

            for (int i = 0; i < grdReport1.Items.Count; i++)
            {
                CheckBox cb = (CheckBox)grdReport1.Items[i].FindControl("cbShow1");
                cb.Checked = false;
            }

        }

	
	}
}
