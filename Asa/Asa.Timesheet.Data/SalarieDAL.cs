using System;
using System.Data;
using System.Data.SqlClient; 
using System.Collections;
using Microsoft.ApplicationBlocks.Data;

namespace Asa.Timesheet.Data
{
	public class SalarieDAL
	{
		/// <summary>
		/// Loads a SalarieData object from the database using the given SalaryID
		/// </summary>
		/// <param name="salaryID">The primary key value</param>
		/// <returns>A SalarieData object</returns>
		public static SalarieData Load(int salaryID)
		{
			SqlParameter[] parameterValues = new SqlParameter[] { new SqlParameter("@SalaryID", salaryID) };
			using (SqlDataReader reader = SqlHelper.ExecuteReader(CommonDAL.ConnectionString, CommandType.StoredProcedure, "SalariesSelProc", parameterValues))
			{
				if (reader.Read())
				{
					SalarieData result = new SalarieData();
					LoadFromReader(reader,result);
					return result;
				}
				else
					return null;
			}
		}

		/// <summary>
		/// Loads a SalarieData object from the database using the given where clause
		/// </summary>
		/// <param name="whereClause">The filter expression for the query</param>
		/// <returns>A SalarieData object</returns>
		public static SalarieData Load(string whereClause)
		{
			SqlParameter[] parameterValues = new SqlParameter[] { new SqlParameter("@WhereClause", whereClause) };
			using (SqlDataReader reader = SqlHelper.ExecuteReader(CommonDAL.ConnectionString, CommandType.StoredProcedure, "SalariesListProc", parameterValues))
			{
				if (reader.Read())
				{
					SalarieData result = new SalarieData();
					LoadFromReader(reader,result);
					return result;
				}
				else
					return null;
			}
		}
		public static int Save( SalarieData source)
		{
			if (source.SalaryID == -1)
				return Insert(source);
			else
				return Update(source);	
		}

		public static void Delete( SalarieData source)
		{
			Delete(source.SalaryID);
		}
		/// <summary>
		/// Loads a collection of SalarieData objects from the database.
		/// </summary>
		/// <returns>A collection containing all of the SalarieData objects in the database.</returns>
		public static SalariesVector LoadCollection()
		{
			SqlParameter[] parms = new SqlParameter[] {};
			return LoadCollection("SalariesListProc", parms);
		}
	  
		/// <summary>
		/// Saves a collection of SalarieData objects to the database.
		/// </summary>
		/// <returns>A collection containing all of the SalarieData objects in the database.</returns>
		public static  void  SaveCollection(SalariesVector col,string spName, SqlParameter[] parms)
		{
			SalariesVector colOld = LoadCollection(spName,parms);
			UpdateCollection(colOld,col);
		}
		public static  void  SaveCollection(SalariesVector col)
		{
			SalariesVector colOld = LoadCollection();
			UpdateCollection(colOld,col);
		}
		public static  void  SaveCollection(SalariesVector col, string whereClause )
		{
			SalariesVector colOld = LoadCollection(whereClause);
			UpdateCollection(colOld,col);
		}
		/// <summary>
		/// Loads a collection of SalarieData objects from the database.
		/// </summary>
		/// <returns>A collection containing all of the SalarieData objects in the database.</returns>
		public static SalariesVector LoadCollection(string whereClause)
		{
			SqlParameter[] parms = new SqlParameter[] { new SqlParameter("@WhereClause", whereClause) };
			return LoadCollection("SalariesListProc", parms);
		}

		/// <summary>
		/// Loads a collection of SalarieData objects from the database.
		/// </summary>
		/// <returns>A collection containing all of the SalarieData objects in the database.</returns>
		public static SalariesVector LoadCollection(string spName, SqlParameter[] parms)
		{
			SalariesVector result = new SalariesVector();
			using (SqlDataReader reader = SqlHelper.ExecuteReader(CommonDAL.ConnectionString, CommandType.StoredProcedure, spName, parms))
			{
				while (reader.Read())
				{
					SalarieData tmp = new SalarieData();
					LoadFromReader(reader, tmp);
					result.Add(tmp);
				}
			}
			return result;
		}
		/// <summary>
		/// Deletes a SalarieData object from the database.
		/// </summary>
		/// <param name="salaryID">The primary key value</param>
		public static void Delete(int salaryID)
		{
			SqlParameter[] parameterValues = new SqlParameter[] { new SqlParameter("@SalaryID", salaryID) };
			SqlHelper.ExecuteNonQuery(CommonDAL.ConnectionString, CommandType.StoredProcedure, "SalariesDelProc", parameterValues);
		}

		public static void LoadFromReader(SqlDataReader reader, SalarieData target)
		{
			if (reader != null && !reader.IsClosed)
			{
				//target.Key = reader.GetInt32(0);
				if (!reader.IsDBNull(0)) target.SalaryID = reader.GetInt32(0);
				if (!reader.IsDBNull(1)) target.UserID = reader.GetInt32(1);
				if (!reader.IsDBNull(2)) target.SalaryYear = reader.GetInt32(2);
				if (!reader.IsDBNull(3)) target.SalaryMonth = reader.GetInt32(3);
				if (!reader.IsDBNull(4)) target.SalaryTypeID = reader.GetInt32(4);
				if (!reader.IsDBNull(5)) target.Salary = reader.GetDecimal(5);
			}
		}
		
	  
		public static int Insert( SalarieData source)
		{
			SqlParameter[] parameterValues = GetInsertParameterValues(source);
			SqlHelper.ExecuteNonQuery(CommonDAL.ConnectionString, CommandType.StoredProcedure, "SalariesInsProc", parameterValues);
			source.SalaryID = (int) parameterValues[0].Value;
			return source.SalaryID;
		}

		public static int Update( SalarieData source)
		{
			SqlParameter[] parameterValues = GetUpdateParameterValues(source);
			SqlHelper.ExecuteNonQuery(CommonDAL.ConnectionString, CommandType.StoredProcedure,
				"SalariesUpdProc", parameterValues);
			return source.SalaryID;
		}
		private static void UpdateCollection(SalariesVector colOld,SalariesVector col)
		{
			// Delete all old items
			foreach( SalarieData itemOld in colOld)
			{
				if(!ExistsKey(itemOld.SalaryID, col))
					Delete(itemOld.SalaryID);
			}
			foreach( SalarieData item in col)
			{
				if(item.SalaryID <= -1)
					Insert(item);
				else
					Update(item);
			}
		}
		private static bool ExistsKey(int Key, SalariesVector col)
		{
			foreach( SalarieData item in col)
				if(item.SalaryID==Key)
					return true;
			return false;
		}

		private static SqlParameter GetSqlParameter(string name, ParameterDirection direction, object value)
		{
			SqlParameter p = new SqlParameter(name, value);
			p.Direction = direction;
			return p;
		}

		private static SqlParameter[] GetInsertParameterValues( SalarieData source)
		{
			SqlParameter[] prams = new SqlParameter[6];
			prams[0] = GetSqlParameter("@SalaryID", ParameterDirection.Output, source.SalaryID);
			prams[1] = GetSqlParameter("@UserID", ParameterDirection.Input, source.UserID);
			prams[2] = GetSqlParameter("@SalaryYear", ParameterDirection.Input, source.SalaryYear);
			prams[3] = GetSqlParameter("@SalaryMonth", ParameterDirection.Input, source.SalaryMonth);
			prams[4] = GetSqlParameter("@SalaryTypeID", ParameterDirection.Input, source.SalaryTypeID);
			prams[5] = GetSqlParameter("@Salary", ParameterDirection.Input, source.Salary);
			return prams;
		}

		private static SqlParameter[] GetUpdateParameterValues( SalarieData source)
		{
			return new SqlParameter[] {
										  new SqlParameter("@SalaryID", source.SalaryID),
										  new SqlParameter("@UserID", source.UserID),
										  new SqlParameter("@SalaryYear", source.SalaryYear),
										  new SqlParameter("@SalaryMonth", source.SalaryMonth),
										  new SqlParameter("@SalaryTypeID", source.SalaryTypeID),
										  new SqlParameter("@Salary", source.Salary)
									  };
		}
	}
}   

