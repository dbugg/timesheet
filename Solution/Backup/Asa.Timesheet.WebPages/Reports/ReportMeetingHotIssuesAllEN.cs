using System;
using System.Data;
using Asa.Timesheet.Data;
using System.Data.SqlClient;
using GrapeCity.ActiveReports.Data;
using GrapeCity.ActiveReports;
using GrapeCity.ActiveReports.Controls;
using GrapeCity.ActiveReports.SectionReportModel;
using GrapeCity.ActiveReports.Document.Section;
using GrapeCity.ActiveReports.Document;

namespace Asa.Timesheet.WebPages.Reports
{
    public class ReportMeetingHotIssuesAllEN : GrapeCity.ActiveReports.SectionReport
	{
		private DateTime _dateOfMeeting = DateTime.MinValue;
		private string _startHour = string.Empty;
		private string _endHour = string.Empty;
		private string _placeOfMeeting = string.Empty;
		private string _tema = string.Empty;
		private int _count = 1;
		private string catName = "";
		private string _meetingNumber = "";
		private bool _all=true;
		private string _user="";
		private int _countAll=0;
		//private int _MAX_CHARS=100;
		private bool _ASA=false;
		private DataView _dvProjects;
        public ReportMeetingHotIssuesAllEN(DateTime dateOfMeeting, string startHour, string endHour, string placeOfMeeting, string tema, DataView dvProjects, DataView dvUsers, HotIssuesVector hiv, string meetingNumber, bool bAll, string other, string mails, int nProjectID, string user, int count, bool ASA)
        {
            _dvProjects = dvProjects;
            _all = bAll;
            _ASA = ASA;
            _user = user;
            _countAll = count;
            InitializeComponent();
            if (tema != "")
                txtPlace.Text = txtPlace1.Text = tema;
            else
                txtPlace.Visible = txtPlace1.Visible = txtP1.Visible = txtP2.Visible = false;
            this._placeOfMeeting = placeOfMeeting;
            this._dateOfMeeting = dateOfMeeting;
            this._startHour = startHour;
            this._endHour = endHour;

            if (startHour == "" && endHour == "")
            {
                TextBox29.Visible = TextBox34.Visible = txtDate1.Visible = txtHour1.Visible = false;
                TextBox3.Visible = TextBox28.Visible = txtDate.Visible = txtHour.Visible = false;

            }
            this._tema = tema;
            this._meetingNumber = meetingNumber;
            //txtDateNow.Text=DateTime.Now.Date.ToShortDateString();
            txtCreatedBy.Text = user + txtCreatedBy.Text;


            srProjects1.Report = new ReportMeetingHotIssuesProjects(dvProjects, true);
            srUsers.Report = new ReportMeetingHotIssuesUsers(dvUsers);
            srUsers1.Report = new ReportMeetingHotIssuesUsers(UIHelpers.HotIssueCreateDVusers(mails));
            //this.ReportEnd+=new EventHandler(SubAnalysisRpt_ReportEnd);
            //			DataView dvHIV = UIHelpers.CreateDataViewFromHotIssuesVector(hiv, bAll);
            //			dvHIV.Sort = "HotIssueCategoryID";
            //			this.DataSource= dvHIV;


            string ds = Serialization.SerializeObjectToXmlString(hiv);

            XMLDataSource source;
            source = new XMLDataSource();
            source.LoadXML((string)ds);
            source.RecordsetPattern = "//HotIssueData";
            this.DataSource = source;


            //			if(dvHIV.Table.Rows.Count==0)
            //				this.txtNoIssueFound.Visible = true;
            //			else 
            //				this.txtNoIssueFound.Visible = false;

            this.PageSettings.Margins.Top = 0.4f;
            this.PageSettings.Margins.Bottom = 0.2f;
            this.PageSettings.Gutter = 0F;
            this.PageSettings.Margins.Left = 0.2f;
            this.PageSettings.Margins.Right = 0.2f;




            this.txtDate.Text = _placeOfMeeting + " / " + _dateOfMeeting.ToString("dd.MM.yyyy");
            if (_startHour == _endHour)
                this.txtHour.Text = string.Concat(_startHour, " h");
            else
                this.txtHour.Text = string.Concat(_startHour, " - ", _endHour, " h");
            this.txtHour1.Text = txtHour.Text;
            this.txtDate1.Text = txtDate.Text;

        }





        private void ReportHeader_Format(object sender, System.EventArgs eArgs)
        {
            if (_meetingNumber.Trim() == "")
            {
                //txtHeaderText.Text=txtHeaderText.Text.Substring(0,txtHeaderText.Text.Length-2)+ Resource.ResourceManager["Name_All"];
                txtHeaderTextEN.Text = string.Concat(this.txtHeaderTextEN.Text, " - All Tasks");
            }
            else
            {
                //txtHeaderText.Text= string.Concat(this.txtHeaderText.Text,_meetingNumber);
                txtHeaderTextEN.Text = string.Concat(this.txtHeaderTextEN.Text, _meetingNumber);
            }
        }
        private void GroupHeader1_Format(object sender, System.EventArgs eArgs)
        {
            this.txtCategoryName.Text = "Item - " + this.txtCategoryName.Text;
            if (this.txtCategoryName.Text == catName)
            {
                //				this.txtCategoryName.Visible = lineCat.Visible= false;
                //				txtName.Location= new System.Drawing.PointF(txtName.Location.X,0f);
                //				txtNumber.Location= new System.Drawing.PointF(txtNumber.Location.X,0f);
                //				txtDiscription.Location= new System.Drawing.PointF(txtDiscription.Location.X,0.36f);
                //				txtComment.Location= new System.Drawing.PointF(txtComment.Location.X,0.625f);
                //				txtAccToName.Location= new System.Drawing.PointF(txtAccToName.Location.X,0.313f);
                //				txtStatusDate.Location= new System.Drawing.PointF(txtAccToName.Location.X,0.063f);
                //				srTask.Location= new System.Drawing.PointF(srTask.Location.X,0f);
                //				srHotIssue.Location= new System.Drawing.PointF(srHotIssue.Location.X,0.063f);
                //				picDone.Location= new System.Drawing.PointF(picDone.Location.X,0.063f);
                //				picUndone.Location= new System.Drawing.PointF(picUndone.Location.X,0.063f);
                //_count++;
                GroupHeader1.Visible = false;
            }
            else
            {
                this.txtCategoryName.Visible = lineCat.Visible = true;
                GroupHeader1.Visible = true;

                //catName = this.txtCategoryName.Text;
                //_count =1;
            }

        }

        private void Detail_Format(object sender, System.EventArgs eArgs)
        {
            if (this.txtCategoryName.Text == catName)
            {
                //				this.txtCategoryName.Visible = lineCat.Visible= false;
                //				txtName.Location= new System.Drawing.PointF(txtName.Location.X,0f);
                //				txtNumber.Location= new System.Drawing.PointF(txtNumber.Location.X,0f);
                //				txtDiscription.Location= new System.Drawing.PointF(txtDiscription.Location.X,0.36f);
                //				txtComment.Location= new System.Drawing.PointF(txtComment.Location.X,0.625f);
                //				txtAccToName.Location= new System.Drawing.PointF(txtAccToName.Location.X,0.313f);
                //				txtStatusDate.Location= new System.Drawing.PointF(txtAccToName.Location.X,0.063f);
                //				srTask.Location= new System.Drawing.PointF(srTask.Location.X,0f);
                //				srHotIssue.Location= new System.Drawing.PointF(srHotIssue.Location.X,0.063f);
                //				picDone.Location= new System.Drawing.PointF(picDone.Location.X,0.063f);
                //				picUndone.Location= new System.Drawing.PointF(picUndone.Location.X,0.063f);
                _count++;
            }
            else
            {
                catName = this.txtCategoryName.Text;
                _count = 1;
            }
            System.Drawing.Font f = new System.Drawing.Font(txtName.Font.FontFamily.Name, txtName.Font.SizeInPoints, System.Drawing.FontStyle.Regular);

            bool bNormal = true;
            try
            {
                txtStop.Visible = txtStopped.Text.Trim() != "";
                this.txtNumber.Text = string.Concat(_count.ToString(), ".");
                int mainID = UIHelpers.ToInt(this.txtHotIssueMainID.Text);


                int AccTo = UIHelpers.ToInt(this.txtAssignedTo.Text);
                int accToType = UIHelpers.ToInt(this.txtAssignedToType.Text);
                //				if(txtName!= null && txtName.Text.Length>_MAX_CHARS)
                //					txtDiscription.Visible=false;
                //				else
                //					txtDiscription.Visible=true;
                //				if(txtDiscription!= null && txtDiscription.Text.Length>_MAX_CHARS)
                //					txtComment.Visible=false;
                //				else
                //					txtComment.Visible=true;
                //txtComment.Location= new System.Drawing.PointF(txtDiscription.Location.X,txtDiscription.Location.Y);
                //txtComment.Visible=true;
                txtDiscription.Visible = false;
                string sText = txt.Text;
                txtDiscription.Visible = false;

                if (sText != null && sText.Trim().Length > 0 && AccTo > 0 && accToType > 0)
                    sText = ", " + sText;

                HotIssuesVector hv = HotIssueDAL.LoadCollectionOldIssues(mainID, _ASA);
                DateTime dt = new DateTime();
                int nStatusID = UIHelpers.ToInt(txtStatusID.Text);
                foreach (HotIssueData hid in hv)
                {
                    if (hid.HotIssueStatusID == nStatusID)
                        dt = hid.CreatedDate;
                }
                if (dt != new DateTime())
                    txtStatusDate.Text = txtStatus.Text + " - " + UIHelpers.FormatDate(dt);
                else
                    txtStatusDate.Text = txtStatus.Text;

                this.txtAccToName.Text = UIHelpers.GetHotIssueUserNameAssignedEN(AccTo, accToType) + sText;
                bool bDone = (txtStatusID.Text == System.Configuration.ConfigurationManager.AppSettings["HotIssueStatusFinished"]);
                picDone.Visible = bDone;

                int nDays = int.Parse(System.Configuration.ConfigurationManager.AppSettings["EditDays"]);
                HotIssuesVector hivAll = HotIssueDAL.LoadCollection(-1, -1, -1,/*-1,*/mainID, true, -1, -1, Constants.DateMax, _dateOfMeeting.AddDays(nDays), _ASA, true, null);
                picUndone.Visible = false;
                if (hivAll.Count > 0)
                {
                    HotIssueData hidd = hivAll[0];
                    if (_dateOfMeeting > hidd.HotIssueDeadline)
                        picUndone.Visible = true;

                    //System.Drawing.Font f = null;

                    if (hidd.HotIssueStatusID == int.Parse(System.Configuration.ConfigurationManager.AppSettings["HotIssueStatusFinished"]))
                    {
                        f = new System.Drawing.Font(txtName.Font.FontFamily.Name, txtName.Font.SizeInPoints, System.Drawing.FontStyle.Italic);
                        bNormal = false;

                    }
                    else if (hidd.HotIssueStatusID == int.Parse(System.Configuration.ConfigurationManager.AppSettings["HotIssueStatusDeletedTask"]))
                    {
                        f = new System.Drawing.Font(txtName.Font.FontFamily.Name, txtName.Font.SizeInPoints, System.Drawing.FontStyle.Strikeout);
                        txtComment.Font = f;
                        bNormal = false;
                    }
                    else
                    {
                        f = new System.Drawing.Font(txtName.Font.FontFamily.Name, txtName.Font.SizeInPoints, System.Drawing.FontStyle.Regular);
                        bNormal = true;

                    }
                    txtStop.Visible = hidd.ExecutionStopped != new System.DateTime();
                    txtName.Font = txtDiscription.Font = txtStatusDate.Font = txtAccToName.Font = f;
                    /*if(bNormal)
                    {
                        f =new System.Drawing.Font(txtName.Font.FontFamily.Name,txtName.Font.SizeInPoints,System.Drawing.FontStyle.Italic);
                        txtComment.Font=f;
                    }*/
                }
                //this.srHotIssue.Report  =  new ReportMeetingHotIssuesHotIssue(mainID,_dateOfMeeting,_all,_ASA);
            }
            catch { }
            this.srTask.Report = new ReportMeetingHotIssuesTask(txtDiscription.Text, txtComment.Text, txtName.Text, f, bNormal, txtPosition.Text, txtAccToName.Text);
        }

        private void PageHeader_Format(object sender, System.EventArgs eArgs)
        {
            srProjects.Report = new ReportMeetingHotIssuesProjects(_dvProjects, true);
            //this.txtTema.Text = _tema;

            if (this.PageNumber == 1)
                this.PageHeader.Visible = false;
            else
            {
                this.PageHeader.Visible = true;
                if (_meetingNumber.Trim() == "")
                    txtHeader.Text = string.Format(Resource.ResourceManager["HotIssue_PDF_PageHeaderEN"].Substring(0, 12), this.PageNumber, _meetingNumber, _dateOfMeeting.ToString("dd.MM.yyyy"));
                else
                    txtHeader.Text = string.Format(Resource.ResourceManager["HotIssue_PDF_PageHeaderEN"], this.PageNumber, _meetingNumber, _dateOfMeeting.ToString("dd.MM.yyyy"));
            }
        }


		#region ActiveReports Designer generated code





















































































        public void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ReportMeetingHotIssuesAllEN));
            this.Detail = new GrapeCity.ActiveReports.SectionReportModel.Detail();
            this.picUndone = new GrapeCity.ActiveReports.SectionReportModel.Picture();
            this.picDone = new GrapeCity.ActiveReports.SectionReportModel.Picture();
            this.srTask = new GrapeCity.ActiveReports.SectionReportModel.SubReport();
            this.txtStop = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.srHotIssue = new GrapeCity.ActiveReports.SectionReportModel.SubReport();
            this.txtName = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtDiscription = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtComment = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtAccToName = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtAssignedToType = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtAssignedTo = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtHotIssueMainID = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtNumber = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtStatus = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtStatusID = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txt = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtStatusDate = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtPosition = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtDeadline = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtStopped = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.ReportHeader = new GrapeCity.ActiveReports.SectionReportModel.ReportHeader();
            this.Shape1 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
            this.lbTema1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.TextBox20 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.Line2 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.srUsers = new GrapeCity.ActiveReports.SectionReportModel.SubReport();
            this.txtHeaderTextEN = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.Line6 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.TextBox21 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.srUsers1 = new GrapeCity.ActiveReports.SectionReportModel.SubReport();
            this.TextBox23 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.TextBox24 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.Line14 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.Line15 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.Picture3 = new GrapeCity.ActiveReports.SectionReportModel.Picture();
            this.TextBox29 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtP2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtPlace1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtDate1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtHour1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.TextBox34 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.srProjects1 = new GrapeCity.ActiveReports.SectionReportModel.SubReport();
            this.ReportFooter = new GrapeCity.ActiveReports.SectionReportModel.ReportFooter();
            this.PageHeader = new GrapeCity.ActiveReports.SectionReportModel.PageHeader();
            this.Shape2 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
            this.lbTema = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtHeader = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.Picture2 = new GrapeCity.ActiveReports.SectionReportModel.Picture();
            this.TextBox25 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.TextBox26 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.Line11 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.Line12 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.TextBox3 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtP1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtPlace = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtDate = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.TextBox28 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtHour = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.Line19 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.srProjects = new GrapeCity.ActiveReports.SectionReportModel.SubReport();
            this.PageFooter = new GrapeCity.ActiveReports.SectionReportModel.PageFooter();
            this.txtContacts = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.Line5 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.page3 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.page1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.page2 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lblCreated = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.txtCreatedBy = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.TextBox36 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.TextBox37 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.TextBox38 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.TextBox39 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.TextBox40 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.TextBox41 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.TextBox42 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.TextBox43 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.GroupHeader1 = new GrapeCity.ActiveReports.SectionReportModel.GroupHeader();
            this.Line23 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.txtCategoryName = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.lineCat = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.Line22 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.Line7 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.Line21 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.GroupFooter1 = new GrapeCity.ActiveReports.SectionReportModel.GroupFooter();
            ((System.ComponentModel.ISupportInitialize)(this.picUndone)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picDone)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtStop)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDiscription)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtComment)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAccToName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAssignedToType)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAssignedTo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtHotIssueMainID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtNumber)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtStatus)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtStatusID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtStatusDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPosition)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDeadline)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtStopped)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbTema1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TextBox20)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtHeaderTextEN)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TextBox21)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TextBox23)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TextBox24)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Picture3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TextBox29)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtP2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPlace1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDate1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtHour1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TextBox34)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbTema)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtHeader)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Picture2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TextBox25)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TextBox26)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TextBox3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtP1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPlace)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TextBox28)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtHour)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtContacts)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.page3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.page1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.page2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblCreated)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCreatedBy)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TextBox36)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TextBox37)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TextBox38)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TextBox39)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TextBox40)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TextBox41)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TextBox42)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TextBox43)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCategoryName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            // 
            // Detail
            // 
            this.Detail.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
            this.picUndone,
            this.picDone,
            this.srTask,
            this.txtStop,
            this.srHotIssue,
            this.txtName,
            this.txtDiscription,
            this.txtComment,
            this.txtAccToName,
            this.txtAssignedToType,
            this.txtAssignedTo,
            this.txtHotIssueMainID,
            this.txtNumber,
            this.txtStatus,
            this.txtStatusID,
            this.txt,
            this.txtStatusDate,
            this.txtPosition,
            this.txtDeadline,
            this.txtStopped});
            this.Detail.Height = 0.6354166F;
            this.Detail.KeepTogether = true;
            this.Detail.Name = "Detail";
            this.Detail.Format += new System.EventHandler(this.Detail_Format);
            // 
            // picUndone
            // 
            this.picUndone.Height = 0.2F;
            this.picUndone.ImageData = ((System.IO.Stream)(resources.GetObject("picUndone.ImageData")));
            this.picUndone.Left = 6.8125F;
            this.picUndone.LineColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.picUndone.Name = "picUndone";
            this.picUndone.SizeMode = GrapeCity.ActiveReports.SectionReportModel.SizeModes.Zoom;
            this.picUndone.Top = 0.01F;
            this.picUndone.Width = 0.2F;
            // 
            // picDone
            // 
            this.picDone.Height = 0.2F;
            this.picDone.ImageData = ((System.IO.Stream)(resources.GetObject("picDone.ImageData")));
            this.picDone.Left = 6.8125F;
            this.picDone.LineColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.picDone.Name = "picDone";
            this.picDone.SizeMode = GrapeCity.ActiveReports.SectionReportModel.SizeModes.Zoom;
            this.picDone.Top = 0.01F;
            this.picDone.Width = 0.2F;
            // 
            // srTask
            // 
            this.srTask.CloseBorder = false;
            this.srTask.Height = 1F;
            this.srTask.Left = 0F;
            this.srTask.Name = "srTask";
            this.srTask.Report = null;
            this.srTask.Top = 0F;
            this.srTask.Width = 8F;
            // 
            // txtStop
            // 
            this.txtStop.CanGrow = false;
            this.txtStop.Height = 0.5F;
            this.txtStop.Left = 4.75F;
            this.txtStop.Name = "txtStop";
            this.txtStop.Style = "color: #C00000; font-family: Verdana; font-size: 9pt; font-style: normal; font-we" +
    "ight: normal; text-align: center; ddo-char-set: 0";
            this.txtStop.Text = "Due to an absense of decision, the execution terms are stopped.";
            this.txtStop.Top = 0.5F;
            this.txtStop.Visible = false;
            this.txtStop.Width = 2.25F;
            // 
            // srHotIssue
            // 
            this.srHotIssue.CanGrow = false;
            this.srHotIssue.CanShrink = false;
            this.srHotIssue.CloseBorder = false;
            this.srHotIssue.Height = 1F;
            this.srHotIssue.Left = 6.96F;
            this.srHotIssue.Name = "srHotIssue";
            this.srHotIssue.Report = null;
            this.srHotIssue.Top = 0F;
            this.srHotIssue.Visible = false;
            this.srHotIssue.Width = 1F;
            // 
            // txtName
            // 
            this.txtName.CanGrow = false;
            this.txtName.DataField = "HotIssueName";
            this.txtName.Height = 0.2F;
            this.txtName.Left = 0.3125F;
            this.txtName.Name = "txtName";
            this.txtName.Style = "font-family: Verdana; font-size: 9pt; font-weight: bold; text-decoration: none; d" +
    "do-char-set: 1";
            this.txtName.Text = null;
            this.txtName.Top = 0.0004999935F;
            this.txtName.Visible = false;
            this.txtName.Width = 4.3125F;
            // 
            // txtDiscription
            // 
            this.txtDiscription.CanGrow = false;
            this.txtDiscription.DataField = "HotIssueDiscription";
            this.txtDiscription.Height = 0.25F;
            this.txtDiscription.Left = 0.0625F;
            this.txtDiscription.Name = "txtDiscription";
            this.txtDiscription.Style = "font-family: Verdana; font-size: 9pt; font-weight: normal; ddo-char-set: 1";
            this.txtDiscription.Text = null;
            this.txtDiscription.Top = 0.375F;
            this.txtDiscription.Visible = false;
            this.txtDiscription.Width = 4.5625F;
            // 
            // txtComment
            // 
            this.txtComment.CanGrow = false;
            this.txtComment.DataField = "HotIssueComment";
            this.txtComment.Height = 0.3125F;
            this.txtComment.Left = 0.0625F;
            this.txtComment.Name = "txtComment";
            this.txtComment.Style = "font-family: Verdana; font-size: 9pt; font-style: italic; font-weight: normal; dd" +
    "o-char-set: 1";
            this.txtComment.Text = null;
            this.txtComment.Top = 0.625F;
            this.txtComment.Visible = false;
            this.txtComment.Width = 4.5625F;
            // 
            // txtAccToName
            // 
            this.txtAccToName.CanGrow = false;
            this.txtAccToName.Height = 0.4375F;
            this.txtAccToName.Left = 4.6875F;
            this.txtAccToName.Name = "txtAccToName";
            this.txtAccToName.Style = "font-family: Verdana; font-size: 9pt; font-weight: bold; text-align: center; vert" +
    "ical-align: top; ddo-char-set: 1";
            this.txtAccToName.Text = null;
            this.txtAccToName.Top = 0.1875F;
            this.txtAccToName.Width = 2.0625F;
            // 
            // txtAssignedToType
            // 
            this.txtAssignedToType.CanGrow = false;
            this.txtAssignedToType.DataField = "AssignedToType";
            this.txtAssignedToType.Height = 0.2F;
            this.txtAssignedToType.Left = 1.6875F;
            this.txtAssignedToType.Name = "txtAssignedToType";
            this.txtAssignedToType.Text = null;
            this.txtAssignedToType.Top = 0.3125F;
            this.txtAssignedToType.Visible = false;
            this.txtAssignedToType.Width = 0.25F;
            // 
            // txtAssignedTo
            // 
            this.txtAssignedTo.CanGrow = false;
            this.txtAssignedTo.DataField = "AssignedTo";
            this.txtAssignedTo.Height = 0.2F;
            this.txtAssignedTo.Left = 1.375F;
            this.txtAssignedTo.Name = "txtAssignedTo";
            this.txtAssignedTo.Text = null;
            this.txtAssignedTo.Top = 0.625F;
            this.txtAssignedTo.Visible = false;
            this.txtAssignedTo.Width = 0.25F;
            // 
            // txtHotIssueMainID
            // 
            this.txtHotIssueMainID.CanGrow = false;
            this.txtHotIssueMainID.DataField = "HotIssueIDMain";
            this.txtHotIssueMainID.Height = 0.2F;
            this.txtHotIssueMainID.Left = 1.0625F;
            this.txtHotIssueMainID.Name = "txtHotIssueMainID";
            this.txtHotIssueMainID.Text = null;
            this.txtHotIssueMainID.Top = 0.625F;
            this.txtHotIssueMainID.Visible = false;
            this.txtHotIssueMainID.Width = 0.25F;
            // 
            // txtNumber
            // 
            this.txtNumber.Height = 0.2F;
            this.txtNumber.Left = 0.0625F;
            this.txtNumber.Name = "txtNumber";
            this.txtNumber.Style = "font-family: Verdana; font-size: 9pt; font-weight: bold; ddo-char-set: 1";
            this.txtNumber.Text = null;
            this.txtNumber.Top = 0F;
            this.txtNumber.Width = 0.3125F;
            // 
            // txtStatus
            // 
            this.txtStatus.DataField = "HotIssueStatusName";
            this.txtStatus.Height = 0.2F;
            this.txtStatus.Left = 2.125F;
            this.txtStatus.Name = "txtStatus";
            this.txtStatus.Style = "font-family: Verdana; font-size: 10pt; font-weight: bold; text-decoration: none; " +
    "ddo-char-set: 1";
            this.txtStatus.Text = null;
            this.txtStatus.Top = 0.375F;
            this.txtStatus.Visible = false;
            this.txtStatus.Width = 0.1875F;
            // 
            // txtStatusID
            // 
            this.txtStatusID.DataField = "HotIssueStatusID";
            this.txtStatusID.Height = 0.2F;
            this.txtStatusID.Left = 2.5F;
            this.txtStatusID.Name = "txtStatusID";
            this.txtStatusID.Style = "font-family: Verdana; font-size: 10pt; font-weight: bold; text-decoration: none; " +
    "ddo-char-set: 1";
            this.txtStatusID.Text = null;
            this.txtStatusID.Top = 0.375F;
            this.txtStatusID.Visible = false;
            this.txtStatusID.Width = 0.1875F;
            // 
            // txt
            // 
            this.txt.CanGrow = false;
            this.txt.DataField = "AssignedToMore";
            this.txt.Height = 0.2F;
            this.txt.Left = 2.9375F;
            this.txt.Name = "txt";
            this.txt.Text = null;
            this.txt.Top = 0.375F;
            this.txt.Visible = false;
            this.txt.Width = 0.25F;
            // 
            // txtStatusDate
            // 
            this.txtStatusDate.CanGrow = false;
            this.txtStatusDate.Height = 0.1875F;
            this.txtStatusDate.Left = 4.6875F;
            this.txtStatusDate.Name = "txtStatusDate";
            this.txtStatusDate.Style = "font-family: Verdana; font-size: 9pt; font-weight: bold; text-align: center; vert" +
    "ical-align: top; ddo-char-set: 1";
            this.txtStatusDate.Text = "TextBox35";
            this.txtStatusDate.Top = 0F;
            this.txtStatusDate.Width = 2.0625F;
            // 
            // txtPosition
            // 
            this.txtPosition.CanGrow = false;
            this.txtPosition.DataField = "HotIssuePosition";
            this.txtPosition.Height = 0.2F;
            this.txtPosition.Left = 2.9375F;
            this.txtPosition.Name = "txtPosition";
            this.txtPosition.Text = null;
            this.txtPosition.Top = 0.375F;
            this.txtPosition.Visible = false;
            this.txtPosition.Width = 0.25F;
            // 
            // txtDeadline
            // 
            this.txtDeadline.DataField = "HotIssueDeadlineString";
            this.txtDeadline.Height = 0.2F;
            this.txtDeadline.Left = 7.125F;
            this.txtDeadline.Name = "txtDeadline";
            this.txtDeadline.OutputFormat = resources.GetString("txtDeadline.OutputFormat");
            this.txtDeadline.Style = "font-family: Verdana; font-size: 9pt; text-align: right; ddo-char-set: 1";
            this.txtDeadline.Text = null;
            this.txtDeadline.Top = 0F;
            this.txtDeadline.Width = 0.8125F;
            // 
            // txtStopped
            // 
            this.txtStopped.CanGrow = false;
            this.txtStopped.DataField = "ExecutionStoppedString";
            this.txtStopped.Height = 0.2F;
            this.txtStopped.Left = 7.3125F;
            this.txtStopped.Name = "txtStopped";
            this.txtStopped.Text = null;
            this.txtStopped.Top = 0.5F;
            this.txtStopped.Visible = false;
            this.txtStopped.Width = 0.25F;
            // 
            // ReportHeader
            // 
            this.ReportHeader.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
            this.Shape1,
            this.lbTema1,
            this.TextBox20,
            this.Line2,
            this.srUsers,
            this.txtHeaderTextEN,
            this.Line6,
            this.TextBox21,
            this.srUsers1,
            this.TextBox23,
            this.TextBox24,
            this.Line14,
            this.Line15,
            this.Picture3,
            this.TextBox29,
            this.txtP2,
            this.txtPlace1,
            this.txtDate1,
            this.txtHour1,
            this.TextBox34,
            this.srProjects1});
            this.ReportHeader.Height = 3.135417F;
            this.ReportHeader.Name = "ReportHeader";
            this.ReportHeader.Format += new System.EventHandler(this.ReportHeader_Format);
            // 
            // Shape1
            // 
            this.Shape1.Height = 0.2395835F;
            this.Shape1.Left = 0F;
            this.Shape1.Name = "Shape1";
            this.Shape1.RoundingRadius = 9.999999F;
            this.Shape1.Top = 2.885417F;
            this.Shape1.Width = 8F;
            // 
            // lbTema1
            // 
            this.lbTema1.Height = 0.2F;
            this.lbTema1.Left = 0.0625F;
            this.lbTema1.Name = "lbTema1";
            this.lbTema1.Style = "font-family: Verdana; font-size: 8pt; font-weight: bold; text-align: left; ddo-ch" +
    "ar-set: 1";
            this.lbTema1.Text = "Item";
            this.lbTema1.Top = 2.905F;
            this.lbTema1.Visible = false;
            this.lbTema1.Width = 1.0625F;
            // 
            // TextBox20
            // 
            this.TextBox20.Height = 0.2F;
            this.TextBox20.Left = 0F;
            this.TextBox20.Name = "TextBox20";
            this.TextBox20.Style = "font-family: Verdana; font-size: 9pt; font-style: normal; font-weight: bold; text" +
    "-align: left; text-decoration: none; ddo-char-set: 1";
            this.TextBox20.Text = "Attendees :";
            this.TextBox20.Top = 2.375F;
            this.TextBox20.Width = 2.5625F;
            // 
            // Line2
            // 
            this.Line2.Height = 0F;
            this.Line2.Left = 0.006944444F;
            this.Line2.LineWeight = 1F;
            this.Line2.Name = "Line2";
            this.Line2.Top = 2.319444F;
            this.Line2.Width = 8F;
            this.Line2.X1 = 0.006944444F;
            this.Line2.X2 = 8.006945F;
            this.Line2.Y1 = 2.319444F;
            this.Line2.Y2 = 2.319444F;
            // 
            // srUsers
            // 
            this.srUsers.CloseBorder = false;
            this.srUsers.Height = 0.1875F;
            this.srUsers.Left = 0F;
            this.srUsers.Name = "srUsers";
            this.srUsers.Report = null;
            this.srUsers.Top = 2.625F;
            this.srUsers.Width = 8F;
            // 
            // txtHeaderTextEN
            // 
            this.txtHeaderTextEN.Height = 0.2F;
            this.txtHeaderTextEN.Left = 0F;
            this.txtHeaderTextEN.Name = "txtHeaderTextEN";
            this.txtHeaderTextEN.Style = "font-family: Verdana; font-size: 11pt; font-weight: bold; text-align: left; text-" +
    "decoration: none; ddo-char-set: 1";
            this.txtHeaderTextEN.Text = "MINUTES - PCM Planning  Coordination Meeting ";
            this.txtHeaderTextEN.Top = 1.4375F;
            this.txtHeaderTextEN.Width = 7.9375F;
            // 
            // Line6
            // 
            this.Line6.Height = 0F;
            this.Line6.Left = 0.875F;
            this.Line6.LineWeight = 1F;
            this.Line6.Name = "Line6";
            this.Line6.Top = 1.1875F;
            this.Line6.Width = 7.125F;
            this.Line6.X1 = 0.875F;
            this.Line6.X2 = 8F;
            this.Line6.Y1 = 1.1875F;
            this.Line6.Y2 = 1.1875F;
            // 
            // TextBox21
            // 
            this.TextBox21.Height = 0.2F;
            this.TextBox21.Left = 0F;
            this.TextBox21.Name = "TextBox21";
            this.TextBox21.Style = "font-family: Verdana; font-size: 9pt; font-style: normal; font-weight: bold; text" +
    "-align: left; text-decoration: none; ddo-char-set: 1";
            this.TextBox21.Text = "Distribution :";
            this.TextBox21.Top = 1.8125F;
            this.TextBox21.Width = 2.5625F;
            // 
            // srUsers1
            // 
            this.srUsers1.CloseBorder = false;
            this.srUsers1.Height = 0.1875F;
            this.srUsers1.Left = 0F;
            this.srUsers1.Name = "srUsers1";
            this.srUsers1.Report = null;
            this.srUsers1.Top = 2.0625F;
            this.srUsers1.Width = 8F;
            // 
            // TextBox23
            // 
            this.TextBox23.Height = 0.2F;
            this.TextBox23.Left = 4.6875F;
            this.TextBox23.Name = "TextBox23";
            this.TextBox23.Style = "font-family: Verdana; font-size: 8pt; font-weight: bold; text-align: center; ddo-" +
    "char-set: 1";
            this.TextBox23.Text = "Status / Responsible";
            this.TextBox23.Top = 2.905F;
            this.TextBox23.Width = 2.0005F;
            // 
            // TextBox24
            // 
            this.TextBox24.Height = 0.2F;
            this.TextBox24.Left = 7.125F;
            this.TextBox24.Name = "TextBox24";
            this.TextBox24.Style = "font-family: Verdana; font-size: 8pt; font-weight: bold; text-align: center; ddo-" +
    "char-set: 1";
            this.TextBox24.Text = "Deadline";
            this.TextBox24.Top = 2.905F;
            this.TextBox24.Width = 0.8755007F;
            // 
            // Line14
            // 
            this.Line14.Height = 0.24F;
            this.Line14.Left = 7.1255F;
            this.Line14.LineWeight = 1F;
            this.Line14.Name = "Line14";
            this.Line14.Top = 2.885F;
            this.Line14.Width = 0F;
            this.Line14.X1 = 7.1255F;
            this.Line14.X2 = 7.1255F;
            this.Line14.Y1 = 2.885F;
            this.Line14.Y2 = 3.125F;
            // 
            // Line15
            // 
            this.Line15.Height = 0.24F;
            this.Line15.Left = 4.6255F;
            this.Line15.LineWeight = 1F;
            this.Line15.Name = "Line15";
            this.Line15.Top = 2.885F;
            this.Line15.Width = 0F;
            this.Line15.X1 = 4.6255F;
            this.Line15.X2 = 4.6255F;
            this.Line15.Y1 = 2.885F;
            this.Line15.Y2 = 3.125F;
            // 
            // Picture3
            // 
            this.Picture3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.Picture3.Height = 1.181102F;
            this.Picture3.ImageData = ((System.IO.Stream)(resources.GetObject("Picture3.ImageData")));
            this.Picture3.Left = 0.04166674F;
            this.Picture3.LineColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.Picture3.Name = "Picture3";
            this.Picture3.PictureAlignment = GrapeCity.ActiveReports.SectionReportModel.PictureAlignment.TopRight;
            this.Picture3.SizeMode = GrapeCity.ActiveReports.SectionReportModel.SizeModes.Zoom;
            this.Picture3.Top = 0F;
            this.Picture3.Width = 0.5833333F;
            // 
            // TextBox29
            // 
            this.TextBox29.Height = 0.2F;
            this.TextBox29.Left = 0.875F;
            this.TextBox29.Name = "TextBox29";
            this.TextBox29.Style = "font-family: Verdana; font-size: 9pt; font-weight: normal; text-align: left; text" +
    "-decoration: none; ddo-char-set: 1";
            this.TextBox29.Text = "Date:";
            this.TextBox29.Top = 0.9375F;
            this.TextBox29.Width = 1.1875F;
            // 
            // txtP2
            // 
            this.txtP2.Height = 0.2F;
            this.txtP2.Left = 0.875F;
            this.txtP2.Name = "txtP2";
            this.txtP2.Style = "font-family: Verdana; font-size: 9pt; font-weight: normal; text-align: left; ddo-" +
    "char-set: 1";
            this.txtP2.Text = "Project:";
            this.txtP2.Top = 0.5005F;
            this.txtP2.Visible = false;
            this.txtP2.Width = 1.1875F;
            // 
            // txtPlace1
            // 
            this.txtPlace1.Height = 0.2F;
            this.txtPlace1.Left = 2.0625F;
            this.txtPlace1.Name = "txtPlace1";
            this.txtPlace1.Style = "font-family: Verdana; font-size: 9pt; font-weight: normal; ddo-char-set: 1";
            this.txtPlace1.Text = "TextBox31";
            this.txtPlace1.Top = 0.5F;
            this.txtPlace1.Visible = false;
            this.txtPlace1.Width = 5.8125F;
            // 
            // txtDate1
            // 
            this.txtDate1.Height = 0.2F;
            this.txtDate1.Left = 2.0625F;
            this.txtDate1.Name = "txtDate1";
            this.txtDate1.Style = "font-family: Verdana; font-size: 9pt; font-weight: bold; ddo-char-set: 1";
            this.txtDate1.Text = "TextBox32";
            this.txtDate1.Top = 0.9375F;
            this.txtDate1.Width = 1.875F;
            // 
            // txtHour1
            // 
            this.txtHour1.Height = 0.2F;
            this.txtHour1.Left = 4.6875F;
            this.txtHour1.Name = "txtHour1";
            this.txtHour1.Style = "font-family: Verdana; font-size: 9pt; font-weight: bold; ddo-char-set: 1";
            this.txtHour1.Text = null;
            this.txtHour1.Top = 0.9375F;
            this.txtHour1.Width = 2.0625F;
            // 
            // TextBox34
            // 
            this.TextBox34.Height = 0.2F;
            this.TextBox34.Left = 4F;
            this.TextBox34.Name = "TextBox34";
            this.TextBox34.Style = "font-family: Verdana; font-size: 9pt; font-weight: normal; text-align: left; text" +
    "-decoration: none; ddo-char-set: 1";
            this.TextBox34.Text = "Hour:";
            this.TextBox34.Top = 0.9375F;
            this.TextBox34.Width = 0.6875F;
            // 
            // srProjects1
            // 
            this.srProjects1.CanGrow = false;
            this.srProjects1.CanShrink = false;
            this.srProjects1.CloseBorder = false;
            this.srProjects1.Height = 0.875F;
            this.srProjects1.Left = 0.875F;
            this.srProjects1.Name = "srProjects1";
            this.srProjects1.Report = null;
            this.srProjects1.Top = 0F;
            this.srProjects1.Width = 7.0625F;
            // 
            // ReportFooter
            // 
            this.ReportFooter.Height = 0F;
            this.ReportFooter.Name = "ReportFooter";
            // 
            // PageHeader
            // 
            this.PageHeader.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
            this.Shape2,
            this.lbTema,
            this.txtHeader,
            this.Picture2,
            this.TextBox25,
            this.TextBox26,
            this.Line11,
            this.Line12,
            this.TextBox3,
            this.txtP1,
            this.txtPlace,
            this.txtDate,
            this.TextBox28,
            this.txtHour,
            this.Line19,
            this.srProjects});
            this.PageHeader.Height = 2.192361F;
            this.PageHeader.Name = "PageHeader";
            this.PageHeader.Format += new System.EventHandler(this.PageHeader_Format);
            // 
            // Shape2
            // 
            this.Shape2.Height = 0.1875F;
            this.Shape2.Left = 0F;
            this.Shape2.Name = "Shape2";
            this.Shape2.RoundingRadius = 9.999999F;
            this.Shape2.Top = 2F;
            this.Shape2.Width = 8F;
            // 
            // lbTema
            // 
            this.lbTema.Height = 0.2F;
            this.lbTema.Left = 0.0625F;
            this.lbTema.Name = "lbTema";
            this.lbTema.Style = "font-family: Verdana; font-size: 8pt; font-weight: bold; text-align: left; ddo-ch" +
    "ar-set: 1";
            this.lbTema.Text = "Item";
            this.lbTema.Top = 1.9925F;
            this.lbTema.Visible = false;
            this.lbTema.Width = 1.0625F;
            // 
            // txtHeader
            // 
            this.txtHeader.CanGrow = false;
            this.txtHeader.Height = 0.2F;
            this.txtHeader.Left = 0F;
            this.txtHeader.Name = "txtHeader";
            this.txtHeader.Style = "font-family: Verdana; font-size: 9pt; font-weight: bold; text-align: left; ddo-ch" +
    "ar-set: 1";
            this.txtHeader.Text = "TextBox21";
            this.txtHeader.Top = 1.479167F;
            this.txtHeader.Width = 8F;
            // 
            // Picture2
            // 
            this.Picture2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.Picture2.Height = 1.181102F;
            this.Picture2.ImageData = ((System.IO.Stream)(resources.GetObject("Picture2.ImageData")));
            this.Picture2.Left = 0.04166674F;
            this.Picture2.LineColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.Picture2.Name = "Picture2";
            this.Picture2.PictureAlignment = GrapeCity.ActiveReports.SectionReportModel.PictureAlignment.TopRight;
            this.Picture2.SizeMode = GrapeCity.ActiveReports.SectionReportModel.SizeModes.Zoom;
            this.Picture2.Top = 0F;
            this.Picture2.Width = 0.5833333F;
            // 
            // TextBox25
            // 
            this.TextBox25.Height = 0.2F;
            this.TextBox25.Left = 4.6875F;
            this.TextBox25.Name = "TextBox25";
            this.TextBox25.Style = "font-family: Verdana; font-size: 8pt; font-weight: bold; text-align: center; ddo-" +
    "char-set: 1";
            this.TextBox25.Text = "Status / Responsible";
            this.TextBox25.Top = 1.9925F;
            this.TextBox25.Width = 2.0625F;
            // 
            // TextBox26
            // 
            this.TextBox26.Height = 0.2F;
            this.TextBox26.Left = 7.125F;
            this.TextBox26.Name = "TextBox26";
            this.TextBox26.Style = "font-family: Verdana; font-size: 8pt; font-weight: bold; text-align: center; ddo-" +
    "char-set: 1";
            this.TextBox26.Text = "Deadline";
            this.TextBox26.Top = 1.9925F;
            this.TextBox26.Width = 0.8755007F;
            // 
            // Line11
            // 
            this.Line11.Height = 0.1875F;
            this.Line11.Left = 7.125F;
            this.Line11.LineWeight = 1F;
            this.Line11.Name = "Line11";
            this.Line11.Top = 2F;
            this.Line11.Width = 0F;
            this.Line11.X1 = 7.125F;
            this.Line11.X2 = 7.125F;
            this.Line11.Y1 = 2F;
            this.Line11.Y2 = 2.1875F;
            // 
            // Line12
            // 
            this.Line12.Height = 0.1875F;
            this.Line12.Left = 4.625F;
            this.Line12.LineWeight = 1F;
            this.Line12.Name = "Line12";
            this.Line12.Top = 2F;
            this.Line12.Width = 0F;
            this.Line12.X1 = 4.625F;
            this.Line12.X2 = 4.625F;
            this.Line12.Y1 = 2F;
            this.Line12.Y2 = 2.1875F;
            // 
            // TextBox3
            // 
            this.TextBox3.Height = 0.2F;
            this.TextBox3.Left = 0.875F;
            this.TextBox3.Name = "TextBox3";
            this.TextBox3.Style = "font-family: Verdana; font-size: 9pt; font-weight: normal; text-align: left; text" +
    "-decoration: none; ddo-char-set: 1";
            this.TextBox3.Text = "Date:";
            this.TextBox3.Top = 0.938F;
            this.TextBox3.Width = 1.1875F;
            // 
            // txtP1
            // 
            this.txtP1.Height = 0.2F;
            this.txtP1.Left = 0.875F;
            this.txtP1.Name = "txtP1";
            this.txtP1.Style = "font-family: Verdana; font-size: 9pt; font-weight: normal; text-align: left; ddo-" +
    "char-set: 1";
            this.txtP1.Text = "Project:";
            this.txtP1.Top = 0.688F;
            this.txtP1.Visible = false;
            this.txtP1.Width = 1.1875F;
            // 
            // txtPlace
            // 
            this.txtPlace.Height = 0.2F;
            this.txtPlace.Left = 2.0625F;
            this.txtPlace.Name = "txtPlace";
            this.txtPlace.Style = "font-family: Verdana; font-size: 9pt; font-weight: normal; ddo-char-set: 1";
            this.txtPlace.Text = null;
            this.txtPlace.Top = 0.6875F;
            this.txtPlace.Visible = false;
            this.txtPlace.Width = 5.9375F;
            // 
            // txtDate
            // 
            this.txtDate.Height = 0.2F;
            this.txtDate.Left = 2.063F;
            this.txtDate.Name = "txtDate";
            this.txtDate.Style = "font-family: Verdana; font-size: 9pt; font-weight: bold; ddo-char-set: 1";
            this.txtDate.Text = null;
            this.txtDate.Top = 0.938F;
            this.txtDate.Width = 1.875F;
            // 
            // TextBox28
            // 
            this.TextBox28.Height = 0.2F;
            this.TextBox28.Left = 4F;
            this.TextBox28.Name = "TextBox28";
            this.TextBox28.Style = "font-family: Verdana; font-size: 9pt; font-weight: normal; text-align: left; text" +
    "-decoration: none; ddo-char-set: 1";
            this.TextBox28.Text = "Hour:";
            this.TextBox28.Top = 0.938F;
            this.TextBox28.Width = 0.6875F;
            // 
            // txtHour
            // 
            this.txtHour.Height = 0.2F;
            this.txtHour.Left = 4.688F;
            this.txtHour.Name = "txtHour";
            this.txtHour.Style = "font-family: Verdana; font-size: 9pt; font-weight: bold; ddo-char-set: 1";
            this.txtHour.Text = null;
            this.txtHour.Top = 0.938F;
            this.txtHour.Width = 2.0625F;
            // 
            // Line19
            // 
            this.Line19.Height = 0F;
            this.Line19.Left = 0.875F;
            this.Line19.LineWeight = 1F;
            this.Line19.Name = "Line19";
            this.Line19.Top = 1.1875F;
            this.Line19.Width = 7.125F;
            this.Line19.X1 = 0.875F;
            this.Line19.X2 = 8F;
            this.Line19.Y1 = 1.1875F;
            this.Line19.Y2 = 1.1875F;
            // 
            // srProjects
            // 
            this.srProjects.CanGrow = false;
            this.srProjects.CanShrink = false;
            this.srProjects.CloseBorder = false;
            this.srProjects.Height = 0.875F;
            this.srProjects.Left = 0.875F;
            this.srProjects.Name = "srProjects";
            this.srProjects.Report = null;
            this.srProjects.Top = 0F;
            this.srProjects.Width = 7.063F;
            // 
            // PageFooter
            // 
            this.PageFooter.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
            this.txtContacts,
            this.Line5,
            this.page3,
            this.page1,
            this.page2,
            this.lblCreated,
            this.txtCreatedBy,
            this.TextBox36,
            this.TextBox37,
            this.TextBox38,
            this.TextBox39,
            this.TextBox40,
            this.TextBox41,
            this.TextBox42,
            this.TextBox43});
            this.PageFooter.Height = 1F;
            this.PageFooter.Name = "PageFooter";
            this.PageFooter.Format += new System.EventHandler(this.PageFooter_Format);
            // 
            // txtContacts
            // 
            this.txtContacts.Height = 0.25F;
            this.txtContacts.Left = 0F;
            this.txtContacts.Name = "txtContacts";
            this.txtContacts.Style = "font-family: Verdana; font-size: 6pt; font-weight: bold; text-align: center; ddo-" +
    "char-set: 1";
            this.txtContacts.Text = "Sofia 1700, 4A, Simeonovsko shose Blvd., tel.: +3592 8621978, +3592 8623348, fax." +
    ":+3592 8621614, e-mail: office@asa-bg.com, website: www.asa-bg.com";
            this.txtContacts.Top = 0.75F;
            this.txtContacts.Width = 8.0625F;
            // 
            // Line5
            // 
            this.Line5.Height = 0F;
            this.Line5.Left = 0F;
            this.Line5.LineWeight = 1F;
            this.Line5.Name = "Line5";
            this.Line5.Top = 0.6944444F;
            this.Line5.Width = 8F;
            this.Line5.X1 = 0F;
            this.Line5.X2 = 8F;
            this.Line5.Y1 = 0.6944444F;
            this.Line5.Y2 = 0.6944444F;
            // 
            // page3
            // 
            this.page3.CanGrow = false;
            this.page3.Height = 0.2F;
            this.page3.Left = 0.3125F;
            this.page3.Name = "page3";
            this.page3.Style = "font-family: Verdana; font-size: 8pt; font-weight: normal; text-align: left; ddo-" +
    "char-set: 1";
            this.page3.SummaryType = GrapeCity.ActiveReports.SectionReportModel.SummaryType.PageCount;
            this.page3.Text = null;
            this.page3.Top = 0.25F;
            this.page3.Width = 0.25F;
            // 
            // page1
            // 
            this.page1.CanGrow = false;
            this.page1.Height = 0.2F;
            this.page1.Left = 0F;
            this.page1.Name = "page1";
            this.page1.Style = "font-family: Verdana; font-size: 8pt; font-weight: normal; text-align: left; ddo-" +
    "char-set: 1";
            this.page1.SummaryFunc = GrapeCity.ActiveReports.SectionReportModel.SummaryFunc.Count;
            this.page1.SummaryRunning = GrapeCity.ActiveReports.SectionReportModel.SummaryRunning.All;
            this.page1.SummaryType = GrapeCity.ActiveReports.SectionReportModel.SummaryType.PageCount;
            this.page1.Text = null;
            this.page1.Top = 0.25F;
            this.page1.Width = 0.25F;
            // 
            // page2
            // 
            this.page2.Height = 0.2F;
            this.page2.HyperLink = null;
            this.page2.Left = 0.25F;
            this.page2.Name = "page2";
            this.page2.Style = "font-family: Verdana; font-size: 8pt; ddo-char-set: 1";
            this.page2.Text = "/";
            this.page2.Top = 0.25F;
            this.page2.Width = 0.0625F;
            // 
            // lblCreated
            // 
            this.lblCreated.Height = 0.2F;
            this.lblCreated.HyperLink = null;
            this.lblCreated.Left = 0F;
            this.lblCreated.Name = "lblCreated";
            this.lblCreated.Style = "font-family: Verdana; font-size: 9pt; ddo-char-set: 1";
            this.lblCreated.Text = "Created by:";
            this.lblCreated.Top = 0F;
            this.lblCreated.Width = 0.8125F;
            // 
            // txtCreatedBy
            // 
            this.txtCreatedBy.Height = 0.2F;
            this.txtCreatedBy.Left = 0.875F;
            this.txtCreatedBy.Name = "txtCreatedBy";
            this.txtCreatedBy.Style = "font-family: Verdana; font-size: 9pt; font-weight: normal; text-align: center; dd" +
    "o-char-set: 1";
            this.txtCreatedBy.Text = " - ASA";
            this.txtCreatedBy.Top = 0F;
            this.txtCreatedBy.Width = 1.9375F;
            // 
            // TextBox36
            // 
            this.TextBox36.Height = 0.1875F;
            this.TextBox36.Left = 0F;
            this.TextBox36.Name = "TextBox36";
            this.TextBox36.Style = "font-family: Verdana; font-size: 6.5pt; font-weight: normal; text-align: justify;" +
    " text-decoration: line-through; ddo-char-set: 1";
            this.TextBox36.Text = "strikeout task";
            this.TextBox36.Top = 0.5F;
            this.TextBox36.Width = 0.75F;
            // 
            // TextBox37
            // 
            this.TextBox37.Height = 0.1875F;
            this.TextBox37.Left = 0.6875F;
            this.TextBox37.Name = "TextBox37";
            this.TextBox37.Style = "font-family: Verdana; font-size: 6.5pt; font-weight: normal; text-align: justify;" +
    " text-decoration: none; ddo-char-set: 1";
            this.TextBox37.Text = "- status removed;";
            this.TextBox37.Top = 0.5F;
            this.TextBox37.Width = 0.875F;
            // 
            // TextBox38
            // 
            this.TextBox38.Height = 0.1875F;
            this.TextBox38.Left = 1.5625F;
            this.TextBox38.Name = "TextBox38";
            this.TextBox38.Style = "font-family: Verdana; font-size: 6.5pt; font-style: italic; font-weight: normal; " +
    "text-align: justify; text-decoration: none; ddo-char-set: 1";
            this.TextBox38.Text = "italic font";
            this.TextBox38.Top = 0.5F;
            this.TextBox38.Width = 0.5F;
            // 
            // TextBox39
            // 
            this.TextBox39.Height = 0.1875F;
            this.TextBox39.Left = 2.0625F;
            this.TextBox39.Name = "TextBox39";
            this.TextBox39.Style = "font-family: Verdana; font-size: 6.5pt; font-weight: normal; text-align: justify;" +
    " text-decoration: none; ddo-char-set: 1";
            this.TextBox39.Text = "- concluded;";
            this.TextBox39.Top = 0.5F;
            this.TextBox39.Width = 0.75F;
            // 
            // TextBox40
            // 
            this.TextBox40.Height = 0.1875F;
            this.TextBox40.Left = 2.6875F;
            this.TextBox40.Name = "TextBox40";
            this.TextBox40.Style = "font-family: Verdana; font-size: 6.5pt; font-style: normal; font-weight: normal; " +
    "text-align: justify; text-decoration: underline; ddo-char-set: 1";
            this.TextBox40.Text = "underlined date";
            this.TextBox40.Top = 0.5F;
            this.TextBox40.Width = 0.8125F;
            // 
            // TextBox41
            // 
            this.TextBox41.Height = 0.1875F;
            this.TextBox41.Left = 3.4375F;
            this.TextBox41.Name = "TextBox41";
            this.TextBox41.Style = "font-family: Verdana; font-size: 6.5pt; font-weight: normal; text-align: justify;" +
    " text-decoration: none; ddo-char-set: 1";
            this.TextBox41.Text = "- status changed for task;";
            this.TextBox41.Top = 0.5F;
            this.TextBox41.Width = 1.25F;
            // 
            // TextBox42
            // 
            this.TextBox42.Height = 0.1875F;
            this.TextBox42.Left = 4.6875F;
            this.TextBox42.Name = "TextBox42";
            this.TextBox42.Style = "font-family: Verdana; font-size: 6.5pt; font-style: normal; font-weight: bold; te" +
    "xt-align: justify; text-decoration: none; ddo-char-set: 1";
            this.TextBox42.Text = "bold date";
            this.TextBox42.Top = 0.5F;
            this.TextBox42.Width = 0.5625F;
            // 
            // TextBox43
            // 
            this.TextBox43.Height = 0.1875F;
            this.TextBox43.Left = 5.1875F;
            this.TextBox43.Name = "TextBox43";
            this.TextBox43.Style = "font-family: Verdana; font-size: 6.5pt; font-weight: normal; text-align: justify;" +
    " text-decoration: none; ddo-char-set: 1";
            this.TextBox43.Text = "- postponed task.";
            this.TextBox43.Top = 0.5F;
            this.TextBox43.Width = 0.9375F;
            // 
            // GroupHeader1
            // 
            this.GroupHeader1.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
            this.Line23,
            this.txtCategoryName,
            this.lineCat,
            this.Line22,
            this.Line7,
            this.Line21});
            this.GroupHeader1.DataField = "HotIssueCategoryName";
            this.GroupHeader1.Height = 0.2777778F;
            this.GroupHeader1.Name = "GroupHeader1";
            this.GroupHeader1.Format += new System.EventHandler(this.GroupHeader1_Format);
            // 
            // Line23
            // 
            this.Line23.AnchorBottom = true;
            this.Line23.Height = 0.271F;
            this.Line23.Left = 0F;
            this.Line23.LineWeight = 1F;
            this.Line23.Name = "Line23";
            this.Line23.Top = 0F;
            this.Line23.Width = 0F;
            this.Line23.X1 = 0F;
            this.Line23.X2 = 0F;
            this.Line23.Y1 = 0F;
            this.Line23.Y2 = 0.271F;
            // 
            // txtCategoryName
            // 
            this.txtCategoryName.CanGrow = false;
            this.txtCategoryName.DataField = "HotIssueCategoryName";
            this.txtCategoryName.Height = 0.2F;
            this.txtCategoryName.Left = 0.0625F;
            this.txtCategoryName.Name = "txtCategoryName";
            this.txtCategoryName.Style = "font-family: Verdana; font-size: 10pt; font-weight: bold; text-decoration: underl" +
    "ine; ddo-char-set: 1";
            this.txtCategoryName.Text = null;
            this.txtCategoryName.Top = 0.0625F;
            this.txtCategoryName.Width = 4.5F;
            // 
            // lineCat
            // 
            this.lineCat.Height = 0F;
            this.lineCat.Left = 0F;
            this.lineCat.LineWeight = 1F;
            this.lineCat.Name = "lineCat";
            this.lineCat.Top = 0.2708334F;
            this.lineCat.Width = 8F;
            this.lineCat.X1 = 0F;
            this.lineCat.X2 = 8F;
            this.lineCat.Y1 = 0.2708334F;
            this.lineCat.Y2 = 0.2708334F;
            // 
            // Line22
            // 
            this.Line22.Height = 0.271F;
            this.Line22.Left = 7.125F;
            this.Line22.LineWeight = 1F;
            this.Line22.Name = "Line22";
            this.Line22.Top = 0F;
            this.Line22.Width = 0F;
            this.Line22.X1 = 7.125F;
            this.Line22.X2 = 7.125F;
            this.Line22.Y1 = 0F;
            this.Line22.Y2 = 0.271F;
            // 
            // Line7
            // 
            this.Line7.AnchorBottom = true;
            this.Line7.Height = 0.271F;
            this.Line7.Left = 8F;
            this.Line7.LineWeight = 1F;
            this.Line7.Name = "Line7";
            this.Line7.Top = 0F;
            this.Line7.Width = 0F;
            this.Line7.X1 = 8F;
            this.Line7.X2 = 8F;
            this.Line7.Y1 = 0F;
            this.Line7.Y2 = 0.271F;
            // 
            // Line21
            // 
            this.Line21.AnchorBottom = true;
            this.Line21.Height = 0.271F;
            this.Line21.Left = 4.625F;
            this.Line21.LineWeight = 1F;
            this.Line21.Name = "Line21";
            this.Line21.Top = 0F;
            this.Line21.Width = 0F;
            this.Line21.X1 = 4.625F;
            this.Line21.X2 = 4.625F;
            this.Line21.Y1 = 0F;
            this.Line21.Y2 = 0.271F;
            // 
            // GroupFooter1
            // 
            this.GroupFooter1.Height = 0F;
            this.GroupFooter1.Name = "GroupFooter1";
            // 
            // ReportMeetingHotIssuesAllEN
            // 
            this.MasterReport = false;
            this.PageSettings.PaperHeight = 11F;
            this.PageSettings.PaperWidth = 8.5F;
            this.PrintWidth = 8.031F;
            this.Sections.Add(this.ReportHeader);
            this.Sections.Add(this.PageHeader);
            this.Sections.Add(this.GroupHeader1);
            this.Sections.Add(this.Detail);
            this.Sections.Add(this.GroupFooter1);
            this.Sections.Add(this.PageFooter);
            this.Sections.Add(this.ReportFooter);
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule(resources.GetString("$this.StyleSheet"), "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: inherit; font-style: inherit; font-variant: inherit; font-weight: bo" +
            "ld; font-size: 16pt; font-size-adjust: inherit; font-stretch: inherit", "Heading1", "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: Times New Roman; font-style: italic; font-variant: inherit; font-wei" +
            "ght: bold; font-size: 14pt; font-size-adjust: inherit; font-stretch: inherit", "Heading2", "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: inherit; font-style: inherit; font-variant: inherit; font-weight: bo" +
            "ld; font-size: 13pt; font-size-adjust: inherit; font-stretch: inherit", "Heading3", "Normal"));
            ((System.ComponentModel.ISupportInitialize)(this.picUndone)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picDone)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtStop)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDiscription)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtComment)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAccToName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAssignedToType)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAssignedTo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtHotIssueMainID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtNumber)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtStatus)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtStatusID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtStatusDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPosition)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDeadline)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtStopped)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbTema1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TextBox20)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtHeaderTextEN)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TextBox21)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TextBox23)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TextBox24)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Picture3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TextBox29)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtP2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPlace1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDate1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtHour1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TextBox34)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbTema)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtHeader)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Picture2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TextBox25)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TextBox26)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TextBox3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtP1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPlace)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TextBox28)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtHour)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtContacts)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.page3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.page1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.page2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblCreated)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCreatedBy)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TextBox36)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TextBox37)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TextBox38)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TextBox39)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TextBox40)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TextBox41)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TextBox42)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TextBox43)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCategoryName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();

        }

		#endregion


        private void PageFooter_Format(object sender, EventArgs e)
        {
            //			if(this.PageNumber==1)
            //			{
            //				page1.Visible= page2.Visible=page3.Visible=false;
            //			}
            //			else
            //			{
            //				page1.Visible= page2.Visible=page3.Visible=true;
            //			}
            bool bLastpage = PageNumber == _countAll;


            lblCreated.Visible = bLastpage;
            txtCreatedBy.Visible = bLastpage;
            //txtDateNow.Visible=bLastpage;
            //lbDate.Visible=bLastpage;

        }

        private ReportHeader ReportHeader;
        private Shape Shape1;
        private TextBox lbTema1;
        private TextBox TextBox20;
        private Line Line2;
        private SubReport srUsers;
        private TextBox txtHeaderTextEN;
        private Line Line6;
        private TextBox TextBox21;
        private SubReport srUsers1;
        private TextBox TextBox23;
        private TextBox TextBox24;
        private Line Line14;
        private Line Line15;
        private Picture Picture3;
        private TextBox TextBox29;
        private TextBox txtP2;
        private TextBox txtPlace1;
        private TextBox txtDate1;
        private TextBox txtHour1;
        private TextBox TextBox34;
        private SubReport srProjects1;
        private PageHeader PageHeader;
        private Shape Shape2;
        private TextBox lbTema;
        private TextBox txtHeader;
        private Picture Picture2;
        private TextBox TextBox25;
        private TextBox TextBox26;
        private Line Line11;
        private Line Line12;
        private TextBox TextBox3;
        private TextBox txtP1;
        private TextBox txtPlace;
        private TextBox txtDate;
        private TextBox TextBox28;
        private TextBox txtHour;
        private Line Line19;
        private SubReport srProjects;
        private GroupHeader GroupHeader1;
        private Line Line23;
        private TextBox txtCategoryName;
        private Line lineCat;
        private Line Line22;
        private Line Line7;
        private Line Line21;
        private Detail Detail;
        private Picture picUndone;
        private Picture picDone;
        private SubReport srTask;
        private TextBox txtStop;
        private SubReport srHotIssue;
        private TextBox txtName;
        private TextBox txtDiscription;
        private TextBox txtComment;
        private TextBox txtAccToName;
        private TextBox txtAssignedToType;
        private TextBox txtAssignedTo;
        private TextBox txtHotIssueMainID;
        private TextBox txtNumber;
        private TextBox txtStatus;
        private TextBox txtStatusID;
        private TextBox txt;
        private TextBox txtStatusDate;
        private TextBox txtPosition;
        private TextBox txtDeadline;
        private TextBox txtStopped;
        private GroupFooter GroupFooter1;
        private PageFooter PageFooter;
        private TextBox txtContacts;
        private Line Line5;
        private TextBox page3;
        private TextBox page1;
        private Label page2;
        private Label lblCreated;
        private TextBox txtCreatedBy;
        private TextBox TextBox36;
        private TextBox TextBox37;
        private TextBox TextBox38;
        private TextBox TextBox39;
        private TextBox TextBox40;
        private TextBox TextBox41;
        private TextBox TextBox42;
        private TextBox TextBox43;
        private ReportFooter ReportFooter;

		
	


	}

}
