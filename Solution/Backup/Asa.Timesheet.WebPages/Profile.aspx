﻿<%@ Register TagPrefix="uc1" TagName="PageHeader" Src="UserControls/PageHeader.ascx"  %>
<%@ Register TagPrefix="uc1" TagName="Documents" Src="UserControls/Documents.ascx" %>
<%@ Page language="c#" Codebehind="Profile.aspx.cs" AutoEventWireup="false" Inherits="Asa.Timesheet.WebPages.Profile" smartNavigation="True"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
	<head runat="server">
		<title>Profile</title>
		<script language="javascript" src="PopupCalendar.js"></script>
		<meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
		<meta content="C#" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<LINK href="styles/timesheet.css" type="text/css" rel="stylesheet">
		<script >
		function showDiv(divid)
		{
			for (i=1; i<=3; i++)
			{
			    document.getElementById("div" + i).style.display = "none";
			    document.getElementById("btn" + i).setAttribute("class", "tabNotSelected");
				
			}
			document.getElementById("div" + divid).style.display = "block";
			document.getElementById("btn" + divid).setAttribute("class", "tabSelected");

			document.getElementById("hdn").value=divid;
		}
		
		</script>
	</HEAD>
	<body bottomMargin="0" leftMargin="0" topMargin="0" rightMargin="0">
		<form id="Form1" method="post" runat="server">
			<table id="Table4" height="100%" cellSpacing="0" cellPadding="0" width="100%" border="0">
				<tr height="1">
					<td><uc1:pageheader id="header" runat="server"></uc1:pageheader></td>
				</tr>
				<tr>
					<td>
						<table id="Table5" height="100%" cellSpacing="0" cellPadding="0" width="100%" border="0">
							<tr>
								<td class="menuLeft"><asp:placeholder id="menuHolder" runat="server"></asp:placeholder></td>
								
								<td style="PADDING-RIGHT: 5px; PADDING-LEFT: 5px; PADDING-TOP: 3px" vAlign="top" noWrap><br>
									<asp:label id="lblError" runat="server" ForeColor="Red" EnableViewState="False" CssClass="ErrorLabel"></asp:label><asp:label id="lblInfo" runat="server" EnableViewState="False" CssClass="InfoLabel"></asp:label><br>
									<br>
									<TABLE id="Table1" cellSpacing="0" cellPadding="0" width="100%" border="0">
										<TR>
											<td style="WIDTH: 8px"></TD>
											<td style="WIDTH: 134px" vAlign="top" align="center"></TD>
											<td style="PADDING-RIGHT: 10px" vAlign="top"></TD>
										</TR>
										<TR>
											<td style="WIDTH: 8px"></TD>
											<td style="WIDTH: 134px" vAlign="top" align="center"><br>
												<br>
												<br>
                                                <a href="#" id="btn1" onclick="showDiv(1);" class="tabSelected">Изходни данни за проектиране</a>
                                                <a href="#" id="btn2" onclick="showDiv(2);" class="tabNotSelected">Инфраструктурни данни за проектиране</a>
                                                <a href="#" id="btn3" onclick="showDiv(3);" class="tabNotSelected">Предаване/одобряване на проектната документация</a>

                                                <br>
												<br>
												<asp:button id="btnPrint" CssClass="ActionButton" Text="Печат" Runat="server"></asp:button><br>
												<p align="center"><asp:button id="btnSave" CssClass="ActionButton" Text="Запиши" Runat="server"></asp:button></p>
												<p align="center"><asp:button id="btnCancel" CssClass="ActionButton" Text="Връщане" Runat="server" Width="92px"></asp:button></p>
											</TD>
											<td style="PADDING-RIGHT: 10px" vAlign="top">
												<!-- Div1 -->
												<DIV id="div1" style="PADDING-RIGHT: 10px; PADDING-LEFT: 10px; PADDING-BOTTOM: 10px; PADDING-TOP: 10px"
													runat="server"><asp:label id="lbOtherTitle" runat="server" CssClass="Div2Header">Изходни данни за проектиране</asp:label>
													<IMG id="imgMain" alt="Няма" src="images/delete.png" runat="server">
                                                    <TABLE id="Table2" cellSpacing="0" cellPadding="3" border="0">
														<TR>
															<td style="BORDER-RIGHT: lightgrey 1px solid; BORDER-TOP: lightgrey 1px solid; BORDER-LEFT: lightgrey 1px solid; BORDER-BOTTOM: lightgrey 1px solid"
																colSpan="7"><asp:label id="Label104" runat="server" CssClass="lblSubheader">Други документи</asp:label>&nbsp;</TD>
															<td style="BORDER-RIGHT: lightgrey 1px solid; BORDER-TOP: lightgrey 1px solid; BORDER-LEFT: lightgrey 1px solid; BORDER-BOTTOM: lightgrey 1px solid">
															</TD>
														</TR>
														
														<TR>
															<td colSpan="7"><uc1:documents id="docOtherDoc" runat="server"></uc1:documents></TD>
															<td></TD>
														</TR>
														<TR>
															<td style="BORDER-RIGHT: lightgrey 1px solid; BORDER-TOP: lightgrey 1px solid; BORDER-LEFT: lightgrey 1px solid; BORDER-BOTTOM: lightgrey 1px solid"
																colSpan="7"><asp:label id="lbNotaryTitle" runat="server" CssClass="lblSubheader">Нотариален акт</asp:label>&nbsp;</TD>
															<td style="BORDER-RIGHT: lightgrey 1px solid; BORDER-TOP: lightgrey 1px solid; BORDER-LEFT: lightgrey 1px solid; BORDER-BOTTOM: lightgrey 1px solid"><IMG id="imgNotary" alt="Няма" src="images/delete.png" align="middle" runat="server"></TD>
														</TR>
														<TR>
															<td style="WIDTH: 60px"><asp:label id="Label38" runat="server" CssClass="lblDiv1">Заявен</asp:label></TD>
															<td style="WIDTH: 120px" vAlign="bottom"><asp:textbox id="txtNotaryDeeds" runat="server" CssClass="enterDataBox" Width="88px" MaxLength="10"></asp:textbox>&nbsp;<IMG id="lkNotaryDeedsE" alt="Calendar" src="images/calendar.ico" width="18" align="middle"
																	runat="server">
															</TD>
															<td style="WIDTH: 52px"><asp:label id="Label39" runat="server" CssClass="lblDiv1">Получен</asp:label></TD>
															<td style="WIDTH: 117px"><asp:textbox id="txtNotaryDeedsDone" runat="server" CssClass="enterDataBox" Width="88px" MaxLength="10"></asp:textbox>&nbsp;<IMG id="lkNotaryDeedsD" alt="Calendar" src="images/calendar.ico" width="18" align="middle"
																	runat="server"></TD>
															<td style="WIDTH: 40px"></TD>
															<td></TD>
															<td></TD>
															<td></TD>
														</TR>
														<TR>
															<td colSpan="7"><uc1:documents id="docNotary" runat="server"></uc1:documents></TD>
															<td></TD>
														</TR>
														<TR>
															<td style="BORDER-RIGHT: lightgrey 1px solid; BORDER-TOP: lightgrey 1px solid; BORDER-LEFT: lightgrey 1px solid; BORDER-BOTTOM: lightgrey 1px solid"
																colSpan="7"><asp:label id="lblSketchTitle" runat="server" CssClass="lblSubheader">Скица</asp:label>&nbsp;
																<asp:label id="lbSK" runat="server" ForeColor="Red" EnableViewState="False" CssClass="ErrorLabel"></asp:label></TD>
															<td style="BORDER-RIGHT: lightgrey 1px solid; BORDER-TOP: lightgrey 1px solid; BORDER-LEFT: lightgrey 1px solid; BORDER-BOTTOM: lightgrey 1px solid"><IMG id="imgSketch" alt="Няма" src="images/delete.png" align="middle" runat="server"></TD>
														</TR>
														<TR>
															<td style="WIDTH: 60px"><asp:label id="Label40" runat="server" CssClass="lblDiv1">Заявена</asp:label></TD>
															<td style="WIDTH: 120px"><asp:textbox id="txtSketch" runat="server" CssClass="enterDataBox" Width="88px" MaxLength="10"></asp:textbox>&nbsp;<IMG id="lkSketchE" alt="Calendar" src="images/calendar.ico" width="18" align="middle"
																	runat="server"></TD>
															<td style="WIDTH: 52px"><asp:label id="Label43" runat="server" CssClass="lblDiv1">Получена</asp:label></TD>
															<td style="WIDTH: 117px"><asp:textbox id="txtSketchDone" runat="server" CssClass="enterDataBox" Width="88px" MaxLength="10"></asp:textbox>&nbsp;<IMG id="lkSketchD" alt="Calendar" src="images/calendar.ico" width="18" align="middle"
																	runat="server"></TD>
															<td style="WIDTH: 40px"></TD>
															<td></TD>
															<td></TD>
															<td></TD>
														</TR>
														<TR>
															<td colSpan="7"><uc1:documents id="docSketch" runat="server"></uc1:documents></TD>
															<td></TD>
														</TR>
														<TR>
															<td style="BORDER-RIGHT: lightgrey 1px solid; BORDER-TOP: lightgrey 1px solid; BORDER-LEFT: lightgrey 1px solid; BORDER-BOTTOM: lightgrey 1px solid"
																colSpan="7"><asp:label id="Label32" runat="server" CssClass="lblSubheader">Пълномощно на АСА</asp:label></TD>
															<td style="BORDER-RIGHT: lightgrey 1px solid; BORDER-TOP: lightgrey 1px solid; BORDER-LEFT: lightgrey 1px solid; BORDER-BOTTOM: lightgrey 1px solid"><IMG id="imgAtt" alt="Няма" src="images/delete.png" align="middle" runat="server"></TD>
														</TR>
														<TR>
															<td style="WIDTH: 60px"><asp:label id="Label41" runat="server" CssClass="lblDiv1">Заявено</asp:label></TD>
															<td style="WIDTH: 120px"><asp:textbox id="txtPowerOfAttorneyDate" runat="server" CssClass="enterDataBox" Width="88px"
																	MaxLength="10"></asp:textbox>&nbsp;<IMG id="lkPOAE" alt="Calendar" src="images/calendar.ico" width="18" align="middle" runat="server"></TD>
															<td style="WIDTH: 52px"><asp:label id="Label66" runat="server" CssClass="lblDiv1">Получено</asp:label></TD>
															<td style="WIDTH: 117px"><asp:textbox id="txtPowerOfAttorneyDoneDate" runat="server" CssClass="enterDataBox" Width="88px"
																	MaxLength="10"></asp:textbox>&nbsp;<IMG id="lkPOAD" alt="Calendar" src="images/calendar.ico" width="18" align="middle" runat="server"></TD>
															<td style="WIDTH: 40px"></TD>
															<td></TD>
															<td></TD>
															<td></TD>
														</TR>
														<TR>
															<td style="WIDTH: 60px"><asp:label id="Label85" runat="server" CssClass="lblDiv1" Visible="False">Заявени пълномощни</asp:label></TD>
															<td style="WIDTH: 120px"><asp:textbox id="txtPowerOfAttorney" runat="server" CssClass="enterDataBox" Width="88px" MaxLength="10"
																	Visible="False"></asp:textbox></TD>
															<td style="WIDTH: 52px"><asp:label id="Label106" runat="server" CssClass="lblDiv1" Visible="False">Получени пълномощни</asp:label></TD>
															<td style="WIDTH: 117px"><asp:textbox id="txtPowerOfAttorneyDone" runat="server" CssClass="enterDataBox" Width="88px"
																	MaxLength="10" Visible="False"></asp:textbox></TD>
															<td style="WIDTH: 40px"></TD>
															<td></TD>
															<td></TD>
															<td></TD>
														</TR>
														<TR>
															<td colSpan="7"><uc1:documents id="docPowerOfAttorney" runat="server"></uc1:documents></TD>
															<td></TD>
														</TR>
														<TR>
															<td style="BORDER-RIGHT: lightgrey 1px solid; BORDER-TOP: lightgrey 1px solid; BORDER-LEFT: lightgrey 1px solid; BORDER-BOTTOM: lightgrey 1px solid"
																colSpan="7"><asp:label id="Label5" runat="server" CssClass="lblSubheader">Актуално състояние на клиента</asp:label></TD>
															<td style="BORDER-RIGHT: lightgrey 1px solid; BORDER-TOP: lightgrey 1px solid; BORDER-LEFT: lightgrey 1px solid; BORDER-BOTTOM: lightgrey 1px solid"><IMG id="imgActual" alt="Няма" src="images/delete.png" align="middle" runat="server"></TD>
														</TR>
														<TR>
															<td style="WIDTH: 60px"><asp:label id="Label42" runat="server" CssClass="lblDiv1">Заявено</asp:label></TD>
															<td style="WIDTH: 120px"><asp:textbox id="txtCurrentStatus" runat="server" CssClass="enterDataBox" Width="88px" MaxLength="10"></asp:textbox>&nbsp;<IMG id="lkCurrentStatusE" alt="Calendar" src="images/calendar.ico" width="18" align="middle"
																	runat="server"></TD>
															<td style="WIDTH: 52px"><asp:label id="Label44" runat="server" CssClass="lblDiv1">Получен</asp:label></TD>
															<td style="WIDTH: 117px"><asp:textbox id="txtCurrentStatusDone" runat="server" CssClass="enterDataBox" Width="88px" MaxLength="10"></asp:textbox>&nbsp;<IMG id="lkCurrentStatusD" alt="Calendar" src="images/calendar.ico" width="18" align="middle"
																	runat="server"></TD>
															<td style="WIDTH: 40px"></TD>
															<td></TD>
															<td></TD>
															<td></TD>
														</TR>
														<TR>
															<td colSpan="7"><uc1:documents id="docCurrentStatus" runat="server"></uc1:documents></TD>
															<td></TD>
														</TR>
														<TR>
															<td style="BORDER-RIGHT: lightgrey 1px solid; BORDER-TOP: lightgrey 1px solid; BORDER-LEFT: lightgrey 1px solid; BORDER-BOTTOM: lightgrey 1px solid"
																colSpan="7"><asp:label id="Label6" runat="server" CssClass="lblSubheader">Регулационен план /ПУП</asp:label></TD>
															<td style="BORDER-RIGHT: lightgrey 1px solid; BORDER-TOP: lightgrey 1px solid; BORDER-LEFT: lightgrey 1px solid; BORDER-BOTTOM: lightgrey 1px solid"><IMG id="imgPlanReg" alt="Няма" src="images/delete.png" align="middle" runat="server"></TD>
														</TR>
														<TR>
															<td style="WIDTH: 60px"><asp:label id="Label45" runat="server" CssClass="lblDiv1">Заявен</asp:label></TD>
															<td style="WIDTH: 120px"><asp:textbox id="txtRegPlan" runat="server" CssClass="enterDataBox" Width="88px" MaxLength="10"></asp:textbox>&nbsp;<IMG id="lkRegPlanE" alt="Calendar" src="images/calendar.ico" width="18" align="middle"
																	runat="server"></TD>
															<td style="WIDTH: 52px"><asp:label id="Label46" runat="server" CssClass="lblDiv1">Получен</asp:label></TD>
															<td style="WIDTH: 117px"><asp:textbox id="txtRegPlanDone" runat="server" CssClass="enterDataBox" Width="88px" MaxLength="10"></asp:textbox>&nbsp;<IMG id="lkRegPlanD" alt="Calendar" src="images/calendar.ico" width="18" align="middle"
																	runat="server"></TD>
															<td style="WIDTH: 40px"><asp:label id="Label47" runat="server" CssClass="lblDiv1">Вх.номер</asp:label></TD>
															<td><asp:textbox id="txtRegPlanNumber" runat="server" CssClass="enterDataBox" Width="88px"></asp:textbox></TD>
															<td></TD>
															<td></TD>
														</TR>
														<TR>
															<td colSpan="7"><uc1:documents id="docRegPlan" runat="server"></uc1:documents></TD>
															<td></TD>
														</TR>
														<TR>
															<td style="BORDER-RIGHT: lightgrey 1px solid; BORDER-TOP: lightgrey 1px solid; BORDER-LEFT: lightgrey 1px solid; BORDER-BOTTOM: lightgrey 1px solid"
																colSpan="7"><asp:label id="Label7" runat="server" CssClass="lblSubheader">Застроителен план</asp:label></TD>
															<td style="BORDER-RIGHT: lightgrey 1px solid; BORDER-TOP: lightgrey 1px solid; BORDER-LEFT: lightgrey 1px solid; BORDER-BOTTOM: lightgrey 1px solid"><IMG id="imgPlanBuild" alt="Няма" src="images/delete.png" align="middle" runat="server"></TD>
														</TR>
														<TR>
															<td style="WIDTH: 60px"><asp:label id="Label56" runat="server" CssClass="lblDiv1">Заявен</asp:label></TD>
															<td style="WIDTH: 120px"><asp:textbox id="txtBuildPlan" runat="server" CssClass="enterDataBox" Width="88px" MaxLength="10"></asp:textbox>&nbsp;<IMG id="lkBuildPlanE" alt="Calendar" src="images/calendar.ico" width="18" align="middle"
																	runat="server"></TD>
															<td style="WIDTH: 52px"><asp:label id="Label52" runat="server" CssClass="lblDiv1">Получен</asp:label></TD>
															<td style="WIDTH: 117px"><asp:textbox id="txtBuildPlanDone" runat="server" CssClass="enterDataBox" Width="88px" MaxLength="10"></asp:textbox>&nbsp;<IMG id="lkBuildPlanD" alt="Calendar" src="images/calendar.ico" width="18" align="middle"
																	runat="server"></TD>
															<td style="WIDTH: 40px"><asp:label id="Label48" runat="server" CssClass="lblDiv1">Вх.номер</asp:label></TD>
															<td><asp:textbox id="txtBuildPlanNumber" runat="server" CssClass="enterDataBox" Width="88px"></asp:textbox></TD>
															<td></TD>
															<td></TD>
														</TR>
														<TR>
															<td colSpan="7"><uc1:documents id="docBuildPlan" runat="server"></uc1:documents></TD>
															<td></TD>
														</TR>
														<TR>
															<td style="BORDER-RIGHT: lightgrey 1px solid; BORDER-TOP: lightgrey 1px solid; BORDER-LEFT: lightgrey 1px solid; BORDER-BOTTOM: lightgrey 1px solid"
																colSpan="7"><asp:label id="Label33" runat="server" CssClass="lblSubheader">Предварителна виза /Мотив. предложение</asp:label></TD>
															<td style="BORDER-RIGHT: lightgrey 1px solid; BORDER-TOP: lightgrey 1px solid; BORDER-LEFT: lightgrey 1px solid; BORDER-BOTTOM: lightgrey 1px solid"><IMG id="imgPreVisa" alt="Няма" src="images/delete.png" align="middle" runat="server"></TD>
														</TR>
														<TR>
															<td style="WIDTH: 60px"><asp:label id="Label57" runat="server" CssClass="lblDiv1">Заявена</asp:label></TD>
															<td style="WIDTH: 120px"><asp:textbox id="txtPreVisa" runat="server" CssClass="enterDataBox" Width="88px" MaxLength="10"></asp:textbox>&nbsp;<IMG id="lkPreVisaE" alt="Calendar" src="images/calendar.ico" width="18" align="middle"
																	runat="server"></TD>
															<td style="WIDTH: 52px"><asp:label id="Label53" runat="server" CssClass="lblDiv1">Получена</asp:label></TD>
															<td style="WIDTH: 117px"><asp:textbox id="txtPreVisaDone" runat="server" CssClass="enterDataBox" Width="88px" MaxLength="10"></asp:textbox>&nbsp;<IMG id="lkPreVisaD" alt="Calendar" src="images/calendar.ico" width="18" align="middle"
																	runat="server"></TD>
															<td style="WIDTH: 40px"><asp:label id="Label49" runat="server" CssClass="lblDiv1">Вх.номер</asp:label></TD>
															<td><asp:textbox id="txtPreVisaNumber" runat="server" CssClass="enterDataBox" Width="88px"></asp:textbox></TD>
															<td></TD>
															<td></TD>
														</TR>
														<TR>
															<td colSpan="7"><uc1:documents id="docPreVisa" runat="server"></uc1:documents></TD>
															<td></TD>
														</TR>
														<TR>
															<td style="BORDER-RIGHT: lightgrey 1px solid; BORDER-TOP: lightgrey 1px solid; BORDER-LEFT: lightgrey 1px solid; BORDER-BOTTOM: lightgrey 1px solid"
																colSpan="7"><asp:label id="Label35" runat="server" CssClass="lblSubheader">Виза съобразно проекта + трафопост + сутерен</asp:label></TD>
															<td style="BORDER-RIGHT: lightgrey 1px solid; BORDER-TOP: lightgrey 1px solid; BORDER-LEFT: lightgrey 1px solid; BORDER-BOTTOM: lightgrey 1px solid"><IMG id="imgVisaPlus" alt="Няма" src="images/delete.png" align="middle" runat="server"></TD>
														</TR>
														<TR>
															<td style="WIDTH: 60px"><asp:label id="Label59" runat="server" CssClass="lblDiv1">Внесена</asp:label></TD>
															<td style="WIDTH: 120px"><asp:textbox id="txtVisaPlus" runat="server" CssClass="enterDataBox" Width="88px" MaxLength="10"></asp:textbox>&nbsp;<IMG id="lkVisaPlusE" alt="Calendar" src="images/calendar.ico" width="18" align="middle"
																	runat="server"></TD>
															<td style="WIDTH: 52px"><asp:label id="Label55" runat="server" CssClass="lblDiv1">Получена</asp:label></TD>
															<td style="WIDTH: 117px"><asp:textbox id="txtVisaPlusDone" runat="server" CssClass="enterDataBox" Width="88px" MaxLength="10"></asp:textbox>&nbsp;<IMG id="lkVisaPlusD" alt="Calendar" src="images/calendar.ico" width="18" align="middle"
																	runat="server"></TD>
															<td style="WIDTH: 40px"><asp:label id="Label51" runat="server" CssClass="lblDiv1">Вх.номер</asp:label></TD>
															<td><asp:textbox id="txtVisaPlusNumber" runat="server" CssClass="enterDataBox" Width="88px"></asp:textbox></TD>
															<td></TD>
															<td></TD>
														</TR>
														<TR>
															<td colSpan="7"><uc1:documents id="docVisaPlus" runat="server"></uc1:documents></TD>
															<td></TD>
														</TR>
														<TR>
															<td style="BORDER-RIGHT: lightgrey 1px solid; BORDER-TOP: lightgrey 1px solid; BORDER-LEFT: lightgrey 1px solid; BORDER-BOTTOM: lightgrey 1px solid"
																colSpan="7"><asp:label id="Label36" runat="server" CssClass="lblSubheader">Подземен кадастър</asp:label></TD>
															<td style="BORDER-RIGHT: lightgrey 1px solid; BORDER-TOP: lightgrey 1px solid; BORDER-LEFT: lightgrey 1px solid; BORDER-BOTTOM: lightgrey 1px solid"><IMG id="imgKadUnd" alt="Няма" src="images/delete.png" align="middle" runat="server"></TD>
														</TR>
														<TR>
															<td style="WIDTH: 60px"><asp:label id="Label60" runat="server" CssClass="lblDiv1">Заявен</asp:label></TD>
															<td style="WIDTH: 120px"><asp:textbox id="txtKadasterUndEnt" runat="server" CssClass="enterDataBox" Width="88px" MaxLength="10"></asp:textbox>&nbsp;<IMG id="lkKadUndE" alt="Calendar" src="images/calendar.ico" width="18" align="middle"
																	runat="server"></TD>
															<td style="WIDTH: 52px"><asp:label id="Label62" runat="server" CssClass="lblDiv1">Получен</asp:label></TD>
															<td style="WIDTH: 117px"><asp:textbox id="txtKadasterUndRec" runat="server" CssClass="enterDataBox" Width="88px" MaxLength="10"></asp:textbox>&nbsp;<IMG id="lkKadUndD" alt="Calendar" src="images/calendar.ico" width="18" align="middle"
																	runat="server"></TD>
															<td style="WIDTH: 40px"><asp:label id="Label64" runat="server" CssClass="lblDiv1">Вх.номер</asp:label></TD>
															<td><asp:textbox id="txtKadasterUndNumber" runat="server" CssClass="enterDataBox" Width="88px"></asp:textbox></TD>
															<td></TD>
															<td></TD>
														</TR>
														<TR>
															<td colSpan="7"><uc1:documents id="docKadasterUndEnt" runat="server"></uc1:documents></TD>
															<td></TD>
														</TR>
														<TR>
															<td style="BORDER-RIGHT: lightgrey 1px solid; BORDER-TOP: lightgrey 1px solid; BORDER-LEFT: lightgrey 1px solid; BORDER-BOTTOM: lightgrey 1px solid"
																colSpan="7"><asp:label id="Label37" runat="server" CssClass="lblSubheader">Надземен кадастър</asp:label></TD>
															<td style="BORDER-RIGHT: lightgrey 1px solid; BORDER-TOP: lightgrey 1px solid; BORDER-LEFT: lightgrey 1px solid; BORDER-BOTTOM: lightgrey 1px solid"><IMG id="imgKad" alt="Няма" src="images/delete.png" align="middle" runat="server"></TD>
														</TR>
														<TR>
															<td style="WIDTH: 60px"><asp:label id="Label61" runat="server" CssClass="lblDiv1">Заявен</asp:label></TD>
															<td style="WIDTH: 120px"><asp:textbox id="txtKadasterEnt" runat="server" CssClass="enterDataBox" Width="88px" MaxLength="10"></asp:textbox>&nbsp;<IMG id="lkKadasterE" alt="Calendar" src="images/calendar.ico" width="18" align="middle"
																	runat="server"></TD>
															<td style="WIDTH: 52px"><asp:label id="Label63" runat="server" CssClass="lblDiv1">Получен</asp:label></TD>
															<td style="WIDTH: 117px"><asp:textbox id="txtKadasterRec" runat="server" CssClass="enterDataBox" Width="88px" MaxLength="10"></asp:textbox>&nbsp;<IMG id="lkKadasterD" alt="Calendar" src="images/calendar.ico" width="18" align="middle"
																	runat="server"></TD>
															<td style="WIDTH: 40px"><asp:label id="Label65" runat="server" CssClass="lblDiv1">Вх.номер</asp:label></TD>
															<td><asp:textbox id="txtKadasterNumber" runat="server" CssClass="enterDataBox" Width="88px"></asp:textbox></TD>
															<td></TD>
															<td></TD>
														</TR>
														<TR>
															<td colSpan="7"><uc1:documents id="docKadasterEnt" runat="server"></uc1:documents></TD>
															<td></TD>
														</TR>
														<TR style="VISIBILITY: hidden">
															<td style="BORDER-RIGHT: lightgrey 1px solid; BORDER-TOP: lightgrey 1px solid; BORDER-LEFT: lightgrey 1px solid; BORDER-BOTTOM: lightgrey 1px solid"
																colSpan="7"><asp:label id="Label34" runat="server" CssClass="lblSubheader" Visible="False">Виза</asp:label></TD>
															<td style="BORDER-RIGHT: lightgrey 1px solid; BORDER-TOP: lightgrey 1px solid; BORDER-LEFT: lightgrey 1px solid; BORDER-BOTTOM: lightgrey 1px solid"><IMG id="imgVisa" alt="Няма" src="images/delete.png" align="middle" runat="server"></TD>
														</TR>
														<TR style="VISIBILITY: hidden">
															&gt;
															<td style="WIDTH: 60px"><asp:label id="Label58" runat="server" CssClass="lblDiv1">Внесена</asp:label></TD>
															<td style="WIDTH: 120px"><asp:textbox id="txtVisa" runat="server" CssClass="enterDataBox" Width="88px" MaxLength="10"></asp:textbox>&nbsp;<IMG id="lkVisaE" alt="Calendar" src="images/calendar.ico" width="18" align="middle"
																	runat="server"></TD>
															<td style="WIDTH: 52px"><asp:label id="Label54" runat="server" CssClass="lblDiv1">Получена</asp:label></TD>
															<td style="WIDTH: 117px"><asp:textbox id="txtVisaDone" runat="server" CssClass="enterDataBox" Width="88px" MaxLength="10"></asp:textbox>&nbsp;<IMG id="lkVisaD" alt="Calendar" src="images/calendar.ico" width="18" align="middle"
																	runat="server"></TD>
															<td style="WIDTH: 40px"><asp:label id="Label50" runat="server" CssClass="lblDiv1">Вх.номер</asp:label></TD>
															<td><asp:textbox id="txtVisaNumber" runat="server" CssClass="enterDataBox" Width="88px"></asp:textbox></TD>
															<td></TD>
															<td></TD>
														</TR>
													</TABLE>
													<INPUT type="hidden">
												</DIV>
												<!-- Div2 -->
												<div id="div2" style="PADDING-RIGHT: 10px; PADDING-LEFT: 10px; PADDING-BOTTOM: 10px; PADDING-TOP: 10px"
													runat="server"><asp:label id="Label8" runat="server" CssClass="Div2Header"> Инфраструктурни данни за проектиране</asp:label>&nbsp;
													<IMG id="imgInfra" alt="Няма" src="images/delete.png" align="middle" runat="server"><br>
													<br>
													<TABLE id="Table6" style="WIDTH: 640px" cellSpacing="0" cellPadding="2" border="0">
														<TR>
															<td style="BORDER-RIGHT: lightgrey 1px solid; BORDER-TOP: lightgrey 1px solid; BORDER-LEFT: lightgrey 1px solid; BORDER-BOTTOM: lightgrey 1px solid"
																colSpan="5"><asp:label id="Label9" runat="server" CssClass="Div2Subheader">ВИК</asp:label>&nbsp;<IMG id="imgVIK" alt="Няма" src="images/delete.png" align="middle" runat="server"></TD>
															<td style="BORDER-RIGHT: lightgrey 1px solid; BORDER-TOP: lightgrey 1px solid; BORDER-LEFT: lightgrey 1px solid; BORDER-BOTTOM: lightgrey 1px solid"></TD>
														</TR>
														<TR>
															<td style="WIDTH: 181px"><asp:label id="Label2" runat="server" CssClass="Div2RowTitle">Изходни данни</asp:label></TD>
															<td style="WIDTH: 36px"><asp:label id="Label12" runat="server">Внесен</asp:label></TD>
															<td style="WIDTH: 126px"><asp:textbox id="txtVIKEnt" runat="server" CssClass="enterDataBox" Width="88px" MaxLength="10"></asp:textbox>&nbsp;
																<IMG id="lkVIKEnt" alt="Calendar" src="images/calendar.ico" width="18" align="middle"
																	runat="server"></TD>
															<td><asp:label id="Label14" runat="server">Получен</asp:label></TD>
															<td><asp:textbox id="txtVIKRec" runat="server" CssClass="enterDataBox" Width="88px" MaxLength="10"></asp:textbox>&nbsp;
																<IMG id="lkVIKRec" alt="Calendar" src="images/calendar.ico" width="18" align="middle"
																	runat="server"></TD>
															<td><IMG id="iVIKEnt" alt="Няма" src="images/delete.png" align="middle" runat="server"></TD>
														</TR>
														<TR>
															<td style="WIDTH: 181px"><asp:label id="Label10" runat="server" CssClass="Div2RowTitle">Писмо и скица за присъед.</asp:label></TD>
															<td style="WIDTH: 36px"><asp:label id="Label74" runat="server">Внесен</asp:label></TD>
															<td style="WIDTH: 126px"><asp:textbox id="txtVIKLetter" runat="server" CssClass="enterDataBox" Width="88px" MaxLength="10"></asp:textbox>&nbsp;
																<IMG id="lkVIKLetter" alt="Calendar" src="images/calendar.ico" width="18" align="middle"
																	runat="server"></TD>
															<td></TD>
															<td></TD>
															<td><IMG id="iVIKLetter" alt="Няма" src="images/delete.png" align="middle" runat="server"></TD>
														</TR>
														<TR>
															<td style="WIDTH: 181px"><asp:label id="Label11" runat="server" CssClass="Div2RowTitle">Присъединителен договор</asp:label></TD>
															<td style="WIDTH: 36px"><asp:label id="Label73" runat="server">Внесен</asp:label></TD>
															<td style="WIDTH: 126px"><asp:textbox id="txtVIKContract" runat="server" CssClass="enterDataBox" Width="88px" MaxLength="10"></asp:textbox>&nbsp;
																<IMG id="lkVIKContract" alt="Calendar" src="images/calendar.ico" width="18" align="middle"
																	runat="server"></TD>
															<td></TD>
															<td>&nbsp;</TD>
															<td><IMG id="iVIKContract" alt="Няма" src="images/delete.png" align="middle" runat="server"></TD>
														</TR>
														<TR>
															<td style="WIDTH: 181px"><asp:label id="Label3" runat="server" CssClass="Div2RowTitle">Проект за външни връзки</asp:label></TD>
															<td style="WIDTH: 36px"><asp:label id="Label13" runat="server">Възложен</asp:label></TD>
															<td style="WIDTH: 126px"><asp:textbox id="txtVIKProjAss" runat="server" CssClass="enterDataBox" Width="88px" MaxLength="10"></asp:textbox>&nbsp;
																<IMG id="lkVIKProjAss" alt="Calendar" src="images/calendar.ico" width="18" align="middle"
																	runat="server"></TD>
															<td><asp:label id="Label15" runat="server">Съгласуван</asp:label></TD>
															<td><asp:textbox id="txtVIKProjAgg" runat="server" CssClass="enterDataBox" Width="88px" MaxLength="10"></asp:textbox>&nbsp;
																<IMG id="lkVIKProjAgg" alt="Calendar" src="images/calendar.ico" width="18" align="middle"
																	runat="server"></TD>
															<td><IMG id="iVIKProj" alt="Няма" src="images/delete.png" align="middle" runat="server"></TD>
														</TR>
														<TR>
															<td style="WIDTH: 181px"><asp:label id="Label95" runat="server">Вх.номер</asp:label></TD>
															<td style="WIDTH: 36px"><asp:textbox id="txtVIKNum" runat="server" Width="88px"></asp:textbox></TD>
															<td style="WIDTH: 126px">&nbsp;</TD>
															<td></TD>
															<td></TD>
															<td></TD>
														</TR>
														<TR>
															<td style="WIDTH: 181px"><asp:label id="Label82" runat="server">Сума</asp:label></TD>
															<td style="WIDTH: 36px"><asp:textbox id="txtVIKAmount" runat="server" Width="88px"></asp:textbox></TD>
															<td style="WIDTH: 126px"></TD>
															<td></TD>
															<td></TD>
															<td></TD>
														</TR>
														<TR>
															<td style="WIDTH: 181px" vAlign="top"><asp:label id="Label16" runat="server">Бележки</asp:label></TD>
															<td style="WIDTH: 36px" colSpan="4"><asp:textbox id="txtVIKNotes" runat="server" Columns="40" Rows="3" TextMode="MultiLine"></asp:textbox></TD>
															<td></TD>
														</TR>
														<tr height="20">
															<td colSpan="6"><uc1:documents id="docVIKEnt" runat="server"></uc1:documents></td>
														</tr>
														<TR>
															<td style="BORDER-RIGHT: lightgrey 1px solid; BORDER-TOP: lightgrey 1px solid; BORDER-LEFT: lightgrey 1px solid; BORDER-BOTTOM: lightgrey 1px solid"
																colSpan="5"></TD>
															<td style="BORDER-RIGHT: lightgrey 1px solid; BORDER-TOP: lightgrey 1px solid; BORDER-LEFT: lightgrey 1px solid; BORDER-BOTTOM: lightgrey 1px solid"></TD>
														</TR>
														<TR>
															<td style="BORDER-RIGHT: lightgrey 1px solid; BORDER-TOP: lightgrey 1px solid; BORDER-LEFT: lightgrey 1px solid; BORDER-BOTTOM: lightgrey 1px solid"
																colSpan="5"><asp:label id="Label4" runat="server" CssClass="Div2Subheader">Електрофикация</asp:label>&nbsp;<IMG id="imgEL" alt="Няма" src="images/delete.png" align="middle" runat="server">&nbsp;</TD>
															<td style="BORDER-RIGHT: lightgrey 1px solid; BORDER-TOP: lightgrey 1px solid; BORDER-LEFT: lightgrey 1px solid; BORDER-BOTTOM: lightgrey 1px solid"></TD>
														</TR>
														<TR>
															<td style="WIDTH: 181px"><asp:label id="Label68" runat="server" CssClass="Div2RowTitle">Изходни данни</asp:label></TD>
															<td style="WIDTH: 36px"><asp:label id="Label75" runat="server">Внесен</asp:label></TD>
															<td style="WIDTH: 126px"><asp:textbox id="txtELEnt" runat="server" CssClass="enterDataBox" Width="88px" MaxLength="10"></asp:textbox>&nbsp;<IMG id="lkELEnt" alt="Calendar" src="images/calendar.ico" width="18" align="middle"
																	runat="server"></TD>
															<td><asp:label id="Label79" runat="server">Получен</asp:label></TD>
															<td><asp:textbox id="txtELRec" runat="server" CssClass="enterDataBox" Width="88px" MaxLength="10"></asp:textbox>&nbsp;<IMG id="lkELRec" alt="Calendar" src="images/calendar.ico" width="18" align="middle"
																	runat="server"></TD>
															<td><IMG id="iELEnt" alt="Няма" src="images/delete.png" align="middle" runat="server"></TD>
														</TR>
														<TR>
															<td style="WIDTH: 181px"><asp:label id="Label70" runat="server" CssClass="Div2RowTitle">Писмо и скица за присъед.</asp:label></TD>
															<td style="WIDTH: 36px"><asp:label id="Label77" runat="server">Внесен</asp:label></TD>
															<td style="WIDTH: 126px"><asp:textbox id="txtELLetter" runat="server" CssClass="enterDataBox" Width="88px" MaxLength="10"></asp:textbox>&nbsp;<IMG id="lkELLetter" alt="Calendar" src="images/calendar.ico" width="18" align="middle"
																	runat="server"></TD>
															<td></TD>
															<td></TD>
															<td><IMG id="iELLetter" alt="Няма" src="images/delete.png" align="middle" runat="server"></TD>
														</TR>
														<TR>
															<td style="WIDTH: 181px"><asp:label id="Label71" runat="server" CssClass="Div2RowTitle">Присъединителен договор</asp:label></TD>
															<td style="WIDTH: 36px"><asp:label id="Label78" runat="server">Внесен</asp:label></TD>
															<td style="WIDTH: 126px"><asp:textbox id="txtELContract" runat="server" CssClass="enterDataBox" Width="88px" MaxLength="10"></asp:textbox>&nbsp;<IMG id="lkELContract" alt="Calendar" src="images/calendar.ico" width="18" align="middle"
																	runat="server"></TD>
															<td></TD>
															<td></TD>
															<td><IMG id="iELContract" alt="Няма" src="images/delete.png" align="middle" runat="server"></TD>
														</TR>
														<TR>
															<td style="WIDTH: 181px"><asp:label id="Label69" runat="server" CssClass="Div2RowTitle">Проект за външни връзки</asp:label></TD>
															<td style="WIDTH: 36px"><asp:label id="Label76" runat="server">Възложен</asp:label></TD>
															<td style="WIDTH: 126px"><asp:textbox id="txtELProjAss" runat="server" CssClass="enterDataBox" Width="88px" MaxLength="10"></asp:textbox>&nbsp;<IMG id="lkELProjAss" alt="Calendar" src="images/calendar.ico" width="18" align="middle"
																	runat="server"></TD>
															<td><asp:label id="Label80" runat="server">Съгласуван</asp:label></TD>
															<td><asp:textbox id="txtELProjAgg" runat="server" CssClass="enterDataBox" Width="88px" MaxLength="10"></asp:textbox>&nbsp;<IMG id="lkELProjAgg" alt="Calendar" src="images/calendar.ico" width="18" align="middle"
																	runat="server"></TD>
															<td><IMG id="iELProject" alt="Няма" src="images/delete.png" align="middle" runat="server"></TD>
														</TR>
														<TR>
															<td style="WIDTH: 181px; HEIGHT: 19px"><asp:label id="Label100" runat="server">Вх.номер</asp:label></TD>
															<td style="WIDTH: 36px"><asp:textbox id="txtElNumber" runat="server" Width="88px"></asp:textbox></TD>
															<td style="WIDTH: 126px">&nbsp;</TD>
															<td></TD>
															<td>&nbsp;</TD>
															<td></TD>
														</TR>
														<TR>
															<td style="WIDTH: 181px" vAlign="top"><asp:label id="Label29" runat="server">Сума</asp:label></TD>
															<td style="WIDTH: 248px" colSpan="4"><asp:textbox id="txtELAmount" runat="server" Width="88px"></asp:textbox></TD>
															<td></TD>
														</TR>
														<TR>
															<td style="WIDTH: 181px" vAlign="top"><asp:label id="Label72" runat="server">Бележки</asp:label></TD>
															<td style="WIDTH: 248px" colSpan="4"><asp:textbox id="txtELNotes" runat="server" Columns="40" Rows="3" TextMode="MultiLine"></asp:textbox></TD>
															<td></TD>
														</TR>
														<tr height="20">
															<td colSpan="6"><uc1:documents id="docElEnt" runat="server"></uc1:documents></td>
														</tr>
														<TR>
															<td style="BORDER-RIGHT: lightgrey 1px solid; BORDER-TOP: lightgrey 1px solid; BORDER-LEFT: lightgrey 1px solid; BORDER-BOTTOM: lightgrey 1px solid"
																colSpan="5"></TD>
															<td style="BORDER-RIGHT: lightgrey 1px solid; BORDER-TOP: lightgrey 1px solid; BORDER-LEFT: lightgrey 1px solid; BORDER-BOTTOM: lightgrey 1px solid"></TD>
														</TR>
														<TR>
															<td style="BORDER-RIGHT: lightgrey 1px solid; BORDER-TOP: lightgrey 1px solid; BORDER-LEFT: lightgrey 1px solid; BORDER-BOTTOM: lightgrey 1px solid"
																colSpan="5"><asp:label id="Label17" runat="server" CssClass="Div2Subheader">Топлофикация/Газ</asp:label>&nbsp;<IMG id="imgT" alt="Няма" src="images/delete.png" align="middle" runat="server"></TD>
															<td style="BORDER-RIGHT: lightgrey 1px solid; BORDER-TOP: lightgrey 1px solid; BORDER-LEFT: lightgrey 1px solid; BORDER-BOTTOM: lightgrey 1px solid"></TD>
														</TR>
														<TR>
															<td style="WIDTH: 181px"><asp:label id="Label18" runat="server" CssClass="Div2RowTitle">Изходни данни:</asp:label></TD>
															<td style="WIDTH: 36px"><asp:label id="Label87" runat="server">Внесен</asp:label></TD>
															<td style="WIDTH: 126px"><asp:textbox id="txtTEnt" runat="server" CssClass="enterDataBox" Width="88px" MaxLength="10"></asp:textbox>&nbsp;<IMG id="lkTEnt" alt="Calendar" src="images/calendar.ico" width="18" align="middle" runat="server"></TD>
															<td><asp:label id="Label83" runat="server">Получен</asp:label></TD>
															<td><asp:textbox id="txtTRec" runat="server" CssClass="enterDataBox" Width="88px" MaxLength="10"></asp:textbox>&nbsp;<IMG id="lkTRec" alt="Calendar" src="images/calendar.ico" width="18" align="middle" runat="server"></TD>
															<td><IMG id="iTEnt" alt="Няма" src="images/delete.png" align="middle" runat="server"></TD>
														</TR>
														<TR>
															<td style="WIDTH: 181px; HEIGHT: 30px"><asp:label id="Label20" runat="server" CssClass="Div2RowTitle">Писмо и скица за присъед.</asp:label></TD>
															<td style="WIDTH: 36px; HEIGHT: 30px"><asp:label id="Label89" runat="server">Внесен</asp:label></TD>
															<td style="WIDTH: 126px; HEIGHT: 30px"><asp:textbox id="txtTLetter" runat="server" CssClass="enterDataBox" Width="88px" MaxLength="10"></asp:textbox>&nbsp;<IMG id="lkTLetter" alt="Calendar" src="images/calendar.ico" width="18" align="middle"
																	runat="server"></TD>
															<td style="HEIGHT: 30px"></TD>
															<td style="HEIGHT: 30px"></TD>
															<td style="HEIGHT: 30px"><IMG id="iTLetter" alt="Няма" src="images/delete.png" align="textTop" runat="server"></TD>
														</TR>
														<TR>
															<td style="WIDTH: 181px"><asp:label id="Label21" runat="server" CssClass="Div2RowTitle">Присъединителен договор</asp:label></TD>
															<td style="WIDTH: 36px"><asp:label id="Label86" runat="server">Внесен</asp:label></TD>
															<td style="WIDTH: 126px"><asp:textbox id="txtTContract" runat="server" CssClass="enterDataBox" Width="88px" MaxLength="10"></asp:textbox>&nbsp;<IMG id="lkTContract" alt="Calendar" src="images/calendar.ico" width="18" align="middle"
																	runat="server"></TD>
															<td></TD>
															<td></TD>
															<td><IMG id="iTContract" alt="Няма" src="images/delete.png" align="middle" runat="server"></TD>
														</TR>
														<TR>
															<td style="WIDTH: 181px"><asp:label id="Label19" runat="server" CssClass="Div2RowTitle">Проект за външни връзки</asp:label></TD>
															<td style="WIDTH: 36px"><asp:label id="Label84" runat="server">Възложен</asp:label></TD>
															<td style="WIDTH: 126px"><asp:textbox id="txtTProjAss" runat="server" CssClass="enterDataBox" Width="88px" MaxLength="10"></asp:textbox>&nbsp;<IMG id="lkTProjAss" alt="Calendar" src="images/calendar.ico" width="18" align="middle"
																	runat="server"></TD>
															<td><asp:label id="Label88" runat="server">Съгласуван</asp:label></TD>
															<td><asp:textbox id="txtTProjAgg" runat="server" CssClass="enterDataBox" Width="88px" MaxLength="10"></asp:textbox>&nbsp;<IMG id="lkTProjAgg" alt="Calendar" src="images/calendar.ico" width="18" align="middle"
																	runat="server"></TD>
															<td><IMG id="iTProject" alt="Няма" src="images/delete.png" align="middle" runat="server"></TD>
														</TR>
														<TR>
															<td style="WIDTH: 181px"><asp:label id="Label94" runat="server">Вх.номер</asp:label></TD>
															<td style="WIDTH: 36px"><asp:textbox id="txtTNum" runat="server" Width="88px"></asp:textbox></TD>
															<td style="WIDTH: 126px"></TD>
															<td></TD>
															<td></TD>
															<td></TD>
														</TR>
														<TR>
															<td style="WIDTH: 181px" vAlign="top"><asp:label id="Label90" runat="server">Сума</asp:label></TD>
															<td style="WIDTH: 36px" colSpan="4"><asp:textbox id="txtTAmount" runat="server" Width="88px"></asp:textbox></TD>
															<td></TD>
														</TR>
														<TR>
															<td style="WIDTH: 181px" vAlign="top"><asp:label id="Label102" runat="server">Бележки</asp:label></TD>
															<td style="WIDTH: 36px" colSpan="4"><asp:textbox id="txtTNotes" runat="server" Columns="40" Rows="3" TextMode="MultiLine"></asp:textbox></TD>
															<td></TD>
														</TR>
														<TR height="20">
															<td colSpan="6"><uc1:documents id="docTEnt" runat="server"></uc1:documents></TD>
														</TR>
														<TR>
															<td style="BORDER-RIGHT: lightgrey 1px solid; BORDER-TOP: lightgrey 1px solid; BORDER-LEFT: lightgrey 1px solid; BORDER-BOTTOM: lightgrey 1px solid"
																colSpan="6"></TD>
														</TR>
														<TR>
															<td id="aaa" style="BORDER-RIGHT: lightgrey 1px solid; BORDER-TOP: lightgrey 1px solid; BORDER-LEFT: lightgrey 1px solid; BORDER-BOTTOM: lightgrey 1px solid"
																colSpan="6"><asp:label id="Label22" runat="server" CssClass="Div2Subheader">Пътни проекти</asp:label>&nbsp;<IMG id="imgPath" alt="Няма" src="images/delete.png" align="middle" runat="server"><BR>
															</TD>
														</TR>
														<TR>
															<td style="WIDTH: 181px"><asp:label id="Label23" runat="server" CssClass="Div2RowTitle">Изходни данни</asp:label></TD>
															<td style="WIDTH: 36px"><asp:label id="Label98" runat="server">Внесен</asp:label></TD>
															<td style="WIDTH: 126px"><asp:textbox id="txtPathEnt" runat="server" CssClass="enterDataBox" Width="88px" MaxLength="10"></asp:textbox>&nbsp;<IMG id="lkPathEnt" alt="Calendar" src="images/calendar.ico" width="18" align="middle"
																	runat="server"></TD>
															<td><asp:label id="Label91" runat="server">Получен</asp:label></TD>
															<td><asp:textbox id="txtPathRec" runat="server" CssClass="enterDataBox" Width="88px" MaxLength="10"></asp:textbox>&nbsp;<IMG id="lkPathRec" alt="Calendar" src="images/calendar.ico" width="18" align="middle"
																	runat="server"></TD>
															<td><IMG id="iPathEnt" alt="Няма" src="images/delete.png" align="middle" runat="server"></TD>
														</TR>
														<TR>
															<td style="WIDTH: 181px"><asp:label id="Label25" runat="server" CssClass="Div2RowTitle">Писмо и скица за присъед.</asp:label></TD>
															<td style="WIDTH: 36px"><asp:label id="Label105" runat="server">Внесен</asp:label></TD>
															<td style="WIDTH: 126px"><asp:textbox id="txtPathDate" runat="server" CssClass="enterDataBox" Width="88px" MaxLength="10"></asp:textbox>&nbsp;<IMG id="lkPathDate" alt="Calendar" src="images/calendar.ico" width="18" align="middle"
																	runat="server"></TD>
															<td><asp:label id="Label93" runat="server">Вх.номер</asp:label></TD>
															<td><asp:textbox id="txtPathNumber" runat="server" Width="88px"></asp:textbox></TD>
															<td><IMG id="iPathLetter" alt="Няма" src="images/delete.png" align="middle" runat="server"></TD>
														</TR>
														<TR>
															<td style="WIDTH: 181px"><asp:label id="Label24" runat="server" CssClass="Div2RowTitle">Пътен проект</asp:label></TD>
															<td style="WIDTH: 36px"><asp:label id="Label92" runat="server">Възложен</asp:label></TD>
															<td style="WIDTH: 126px"><asp:textbox id="txtPathProjAss" runat="server" CssClass="enterDataBox" Width="88px" MaxLength="10"></asp:textbox>&nbsp;<IMG id="lkPathProjAss" alt="Calendar" src="images/calendar.ico" width="18" align="middle"
																	runat="server"></TD>
															<td><asp:label id="Label97" runat="server">Съгласуван</asp:label></TD>
															<td><asp:textbox id="txtPathProjAgg" runat="server" CssClass="enterDataBox" Width="88px" MaxLength="10"></asp:textbox>&nbsp;<IMG id="lkPathProjAgg" alt="Calendar" src="images/calendar.ico" width="18" align="middle"
																	runat="server"></TD>
															<td><IMG id="iPathProj" alt="Няма" src="images/delete.png" align="middle" runat="server"></TD>
														</TR>
														<TR>
															<td style="WIDTH: 181px"><asp:label id="Label30" runat="server">Сума</asp:label></TD>
															<td style="WIDTH: 36px"><asp:textbox id="txtPathAmount" runat="server" Width="88px"></asp:textbox></TD>
															<td style="WIDTH: 126px"></TD>
															<td></TD>
															<td></TD>
															<td></TD>
														</TR>
														<TR>
															<td style="WIDTH: 181px" vAlign="top"><asp:label id="Label26" runat="server"> Бележки</asp:label></TD>
															<td style="WIDTH: 36px" colSpan="4"><asp:textbox id="txtPathNotes" runat="server" Columns="40" Rows="3" TextMode="MultiLine"></asp:textbox></TD>
															<td></TD>
														</TR>
														<tr height="20">
															<td colSpan="6"><uc1:documents id="docPathEnt" runat="server"></uc1:documents></td>
														</tr>
														<TR>
															<td style="BORDER-RIGHT: lightgrey 1px solid; BORDER-TOP: lightgrey 1px solid; BORDER-LEFT: lightgrey 1px solid; BORDER-BOTTOM: lightgrey 1px solid"
																colSpan="6"></TD>
														</TR>
														<TR>
															<td id="aa" style="BORDER-RIGHT: lightgrey 1px solid; BORDER-TOP: lightgrey 1px solid; BORDER-LEFT: lightgrey 1px solid; BORDER-BOTTOM: lightgrey 1px solid"
																colSpan="6"><asp:label id="Label27" runat="server" CssClass="Div2Subheader">Телефонизация</asp:label>&nbsp;<IMG id="imgPhone" alt="Няма" src="images/delete.png" align="middle" runat="server"><br>
															</TD>
														</TR>
														<TR>
															<td style="WIDTH: 181px"><asp:label id="Label28" runat="server" CssClass="Div2RowTitle">Изходни данни:</asp:label></TD>
															<td style="WIDTH: 36px"><asp:label id="Label99" runat="server">Внесен</asp:label></TD>
															<td style="WIDTH: 126px"><asp:textbox id="txtPhoneEnt" runat="server" CssClass="enterDataBox" Width="88px" MaxLength="10"></asp:textbox>&nbsp;<IMG id="lkPhoneEnt" alt="Calendar" src="images/calendar.ico" width="18" align="middle"
																	runat="server"></TD>
															<td><asp:label id="Label103" runat="server">Получен</asp:label></TD>
															<td><asp:textbox id="txtPhoneRec" runat="server" CssClass="enterDataBox" Width="88px" MaxLength="10"></asp:textbox>&nbsp;<IMG id="lkPhoneRec" alt="Calendar" src="images/calendar.ico" width="18" align="middle"
																	runat="server"></TD>
															<td><IMG id="iPhoneEnt" alt="Няма" src="images/delete.png" align="middle" runat="server"></TD>
														</TR>
														<TR>
															<td style="WIDTH: 181px; HEIGHT: 25px"></TD>
															<td style="WIDTH: 36px"><asp:label id="Label96" runat="server">Вх.номер</asp:label></TD>
															<td style="WIDTH: 126px" colSpan="3"><asp:textbox id="txtPhoneNumber" runat="server" Width="310px"></asp:textbox></TD>
															<td></TD>
														</TR>
														<TR>
															<td style="WIDTH: 181px; HEIGHT: 25px"><asp:label id="Label126" runat="server">Сума</asp:label></TD>
															<td style="WIDTH: 36px"><asp:textbox id="txtPhoneAmount" runat="server" Width="88px"></asp:textbox></TD>
															<td style="WIDTH: 126px" colSpan="3"></TD>
															<td></TD>
														</TR>
														<TR>
															<td style="WIDTH: 181px" vAlign="top"><asp:label id="Label31" runat="server"> Бележки</asp:label></TD>
															<td style="WIDTH: 36px" colSpan="4"><asp:textbox id="txtPhoneNotes" runat="server" Columns="40" Rows="3" TextMode="MultiLine"></asp:textbox></TD>
															<td></TD>
														</TR>
													</TABLE>
													<uc1:documents id="docPhoneEnt" runat="server"></uc1:documents><br>
												</div>
												<br>
												<div id="div3" style="PADDING-RIGHT: 10px; PADDING-LEFT: 10px; PADDING-BOTTOM: 10px; PADDING-TOP: 10px"
													runat="server"><asp:label id="Label107" runat="server" CssClass="Div2Header"> Предаване/одобряване на проектната документация</asp:label>&nbsp;
													<IMG id="imgRazr" alt="Няма" src="images/delete.png" align="middle" runat="server"><br>
													<br>
													<TABLE id="Table66" cellSpacing="0" cellPadding="2" width="640" border="0">
														<TR>
															<td style="BORDER-RIGHT: lightgrey 1px solid; BORDER-TOP: lightgrey 1px solid; BORDER-LEFT: lightgrey 1px solid; WIDTH: 543px; BORDER-BOTTOM: lightgrey 1px solid"
																colSpan="6"><asp:label id="Label108" runat="server" CssClass="Div2Subheader">Идеен проект</asp:label><br>
															</TD>
															<td style="BORDER-RIGHT: lightgrey 1px solid; BORDER-TOP: lightgrey 1px solid; BORDER-LEFT: lightgrey 1px solid; BORDER-BOTTOM: lightgrey 1px solid"
																align="right" width="1"><IMG id="iIdea" alt="Няма" src="images/delete.png" align="middle" runat="server"></TD>
														</TR>
														<TR>
															<td style="WIDTH: 109px"><asp:label id="Label124" runat="server">Внесен</asp:label></TD>
															<td style="WIDTH: 122px"><asp:textbox id="txtIdeaProjectDone" runat="server" CssClass="enterDataBox" Width="88px" MaxLength="10"></asp:textbox>&nbsp;
																<IMG id="lkIdeaProjectDone" alt="Calendar" src="images/calendar.ico" width="18" align="middle"
																	runat="server"></TD>
															<td style="WIDTH: 91px"><asp:label id="Label113" runat="server">Съгласуван</asp:label></TD>
															<td style="WIDTH: 118px"><asp:textbox id="txtIdeaProjectAgreed" runat="server" CssClass="enterDataBox" Width="88px" MaxLength="10"></asp:textbox><IMG id="lkIdeaProjectAgreed" alt="Calendar" src="images/calendar.ico" width="18" align="middle"
																	runat="server"></TD>
															<td style="WIDTH: 56px"></TD>
															<td style="WIDTH: 86px" align="right"></TD>
															<td align="right" width="1"></TD>
														</TR>
														<TR>
															<td style="WIDTH: 109px"><asp:label id="Label112" runat="server">Вх.номер</asp:label></TD>
															<td style="WIDTH: 122px"><asp:textbox id="txtIdeaProjectNumber" runat="server" Width="88px"></asp:textbox></TD>
															<td style="WIDTH: 91px"><asp:label id="Label114" runat="server">Община</asp:label></TD>
															<td style="WIDTH: 118px"><asp:textbox id="txtIdeaProjectMun" runat="server" Width="88px"></asp:textbox></TD>
															<td style="WIDTH: 56px"></TD>
															<td style="WIDTH: 86px" align="right"></TD>
															<td align="right" width="1"></TD>
														</TR>
														<TR>
															<td style="WIDTH: 109px"><asp:label id="Label115" runat="server"> Бележки</asp:label></TD>
															<td style="WIDTH: 339px" colSpan="3"><asp:textbox id="txtIdeaProjectNotes" runat="server" Width="320px" Columns="40" Rows="3" TextMode="MultiLine"></asp:textbox></TD>
															<td style="WIDTH: 56px"></TD>
															<td style="WIDTH: 86px" align="right"></TD>
															<td align="right" width="1"></TD>
														</TR>
														<TR>
															<td style="WIDTH: 543px" colSpan="6"><uc1:documents id="docIdealProjectDone" runat="server"></uc1:documents></TD>
															<td width="1"></TD>
														</TR>
														<TR height="20">
															<td style="WIDTH: 543px" colSpan="6"></td>
															<td width="1"></TD>
														</TR>
														<TR>
															<td style="BORDER-RIGHT: lightgrey 1px solid; BORDER-TOP: lightgrey 1px solid; BORDER-LEFT: lightgrey 1px solid; WIDTH: 543px; BORDER-BOTTOM: lightgrey 1px solid"
																colSpan="6"><asp:label id="Label109" runat="server" CssClass="Div2Subheader">Технически проект</asp:label></TD>
															<td style="BORDER-RIGHT: lightgrey 1px solid; BORDER-TOP: lightgrey 1px solid; BORDER-LEFT: lightgrey 1px solid; BORDER-BOTTOM: lightgrey 1px solid"
																align="right" width="1"><IMG id="iTech" alt="Няма" src="images/delete.png" align="middle" runat="server"></TD>
														</TR>
														<TR>
															<td style="WIDTH: 109px"><asp:label id="Label125" runat="server">Внесен</asp:label></TD>
															<td style="WIDTH: 122px"><asp:textbox id="txtTechProjectDone" runat="server" CssClass="enterDataBox" Width="88px" MaxLength="10"></asp:textbox>&nbsp;
																<IMG id="lkTechProjectDone" alt="Calendar" src="images/calendar.ico" width="18" align="middle"
																	runat="server"></TD>
															<td style="WIDTH: 91px"><asp:label id="Label111" runat="server">Съгласуван</asp:label></TD>
															<td style="WIDTH: 118px"><asp:textbox id="txtTechProjectAgreed" runat="server" CssClass="enterDataBox" Width="88px" MaxLength="10"></asp:textbox><IMG id="lkTechProjectAgreed" alt="Calendar" src="images/calendar.ico" width="18" align="middle"
																	runat="server"></TD>
															<td style="WIDTH: 56px"></TD>
															<td style="WIDTH: 86px" align="right"></TD>
															<td align="right" width="1"></TD>
														</TR>
														<TR>
															<td style="WIDTH: 109px"><asp:label id="Label1" runat="server">Вх.номер</asp:label></TD>
															<td style="WIDTH: 122px"><asp:textbox id="txtTechProjectNumber" runat="server" Width="88px"></asp:textbox></TD>
															<td style="WIDTH: 91px"><asp:label id="Label67" runat="server">Община</asp:label></TD>
															<td style="WIDTH: 118px"><asp:textbox id="txtTechProjectMun" runat="server" Width="88px"></asp:textbox></TD>
															<td style="WIDTH: 56px"></TD>
															<td style="WIDTH: 86px" align="right"></TD>
															<td align="right" width="1"></TD>
														</TR>
														<TR>
															<td style="WIDTH: 109px"><asp:label id="Label101" runat="server"> Бележки</asp:label></TD>
															<td style="WIDTH: 339px" colSpan="3"><asp:textbox id="txtTechProjectNotes" runat="server" Width="320px" Columns="40" Rows="3" TextMode="MultiLine"></asp:textbox></TD>
															<td style="WIDTH: 56px"></TD>
															<td style="WIDTH: 86px" align="right"></TD>
															<td align="right" width="1"></TD>
														</TR>
														<TR>
															<td style="WIDTH: 109px"></TD>
															<td style="WIDTH: 122px"></TD>
															<td style="WIDTH: 91px"></TD>
															<td style="WIDTH: 118px"></TD>
															<td style="WIDTH: 56px"></TD>
															<td style="WIDTH: 86px" align="right"></TD>
															<td align="right" width="1"></TD>
														</TR>
														<tr height="15">
															<td style="WIDTH: 543px" colSpan="6"><uc1:documents id="docTechProjectDone" runat="server"></uc1:documents></td>
															<td width="1"></TD>
														</tr>
														<TR>
															<td style="BORDER-RIGHT: lightgrey 1px solid; BORDER-TOP: lightgrey 1px solid; BORDER-LEFT: lightgrey 1px solid; WIDTH: 543px; BORDER-BOTTOM: lightgrey 1px solid"
																colSpan="6"><asp:label id="Label118" runat="server" CssClass="Div2Subheader">Разрешение за строеж/ползване</asp:label></TD>
															<td style="BORDER-RIGHT: lightgrey 1px solid; BORDER-TOP: lightgrey 1px solid; BORDER-LEFT: lightgrey 1px solid; BORDER-BOTTOM: lightgrey 1px solid"
																align="right" width="1"><IMG id="iRazr" alt="Няма" src="images/delete.png" align="middle" runat="server"></TD>
														</TR>
														<TR>
															<td style="WIDTH: 109px"><asp:label id="Label110" runat="server" Visible="False">Заявено</asp:label></TD>
															<td style="WIDTH: 122px"><asp:textbox id="txtBuildPermission" runat="server" CssClass="enterDataBox" Width="88px" MaxLength="10"
																	Visible="False"></asp:textbox>&nbsp; <IMG id="lkBuildPermission" style="VISIBILITY: hidden" alt="Calendar" src="images/calendar.ico"
																	width="18" align="middle" runat="server"></TD>
															<td style="WIDTH: 91px"><asp:label id="Label116" runat="server">Получено</asp:label></TD>
															<td style="WIDTH: 118px"><asp:textbox id="txtBuildPermissionDone" runat="server" CssClass="enterDataBox" Width="88px"
																	MaxLength="10"></asp:textbox>&nbsp;<IMG id="lkBuildPermissionDone" alt="Calendar" src="images/calendar.ico" width="18" align="middle"
																	runat="server"></TD>
															<td style="WIDTH: 56px"><asp:label id="Label117" runat="server">Изх.номер</asp:label></TD>
															<td style="WIDTH: 86px"><asp:textbox id="txtBuildPermissionNumber" runat="server" CssClass="enterDataBox" Width="88px"
																	MaxLength="10"></asp:textbox>&nbsp;</TD>
															<td width="1"></TD>
														</TR>
														<TR>
															<td style="WIDTH: 109px"><asp:label id="Label127" runat="server"> Бележки</asp:label></TD>
															<td style="WIDTH: 339px" colSpan="3"><asp:textbox id="txtBuildPermissionNotes" runat="server" Width="320px" Columns="40" Rows="3"
																	TextMode="MultiLine"></asp:textbox></TD>
															<td style="WIDTH: 56px"></TD>
															<td style="WIDTH: 86px"></TD>
															<td width="1"></TD>
														</TR>
														<TR height="15">
															<td style="WIDTH: 543px" colSpan="6"><uc1:documents id="docBuildPermission" runat="server"></uc1:documents></TD>
															<td width="1"></TD>
														</TR>
														<TR>
															<td style="BORDER-RIGHT: lightgrey 1px solid; BORDER-TOP: lightgrey 1px solid; BORDER-LEFT: lightgrey 1px solid; WIDTH: 543px; BORDER-BOTTOM: lightgrey 1px solid"
																colSpan="6"><asp:label id="Label119" runat="server" CssClass="Div2Subheader">Други</asp:label></TD>
															<td style="BORDER-RIGHT: lightgrey 1px solid; BORDER-TOP: lightgrey 1px solid; BORDER-LEFT: lightgrey 1px solid; BORDER-BOTTOM: lightgrey 1px solid"
																align="right" width="1"><IMG id="iOther" alt="Няма" src="images/delete.png" align="middle" runat="server"></TD>
														</TR>
														<TR>
															<td style="WIDTH: 109px"><asp:label id="Label120" runat="server">Внесен</asp:label></TD>
															<td style="WIDTH: 122px"><asp:textbox id="txtOther" runat="server" CssClass="enterDataBox" Width="88px" MaxLength="10"></asp:textbox>&nbsp;
																<IMG id="lkOther" alt="Calendar" src="images/calendar.ico" width="18" align="middle"
																	runat="server"></TD>
															<td style="WIDTH: 91px"><asp:label id="Label121" runat="server">Получен</asp:label></TD>
															<td style="WIDTH: 118px"><asp:textbox id="txtOtherDone" runat="server" CssClass="enterDataBox" Width="88px" MaxLength="10"></asp:textbox>&nbsp;<IMG id="lkOtherDone" alt="Calendar" src="images/calendar.ico" width="18" align="middle"
																	runat="server"></TD>
															<td style="WIDTH: 56px"><asp:label id="Label122" runat="server">Изх.номер</asp:label></TD>
															<td style="WIDTH: 86px"><asp:textbox id="txtOtherNumber" runat="server" CssClass="enterDataBox" Width="88px" MaxLength="10"></asp:textbox>&nbsp;</TD>
															<td width="1"></TD>
														</TR>
														<TR>
															<td style="WIDTH: 109px"><asp:label id="Label123" runat="server">Други разходи</asp:label></TD>
															<td style="WIDTH: 122px"><asp:textbox id="txtOtherExpenses" runat="server" CssClass="enterDataBox" Width="88px" MaxLength="10"></asp:textbox></TD>
															<td style="WIDTH: 91px"></TD>
															<td style="WIDTH: 118px"></TD>
															<td style="WIDTH: 56px"></TD>
															<td style="WIDTH: 86px"></TD>
															<td width="1"></TD>
														</TR>
														<tr>
															<td colspan="7"><uc1:Documents id="docOther" runat="server"></uc1:Documents></TD>
														</tr>
													</TABLE>
												</div>
												&nbsp;
												<BR>
												<BR>
												<INPUT id="hdn" type="hidden" name="Hidden1" runat="server">
											</TD>
										</TR>
									</TABLE>
								</td>
							</tr>
						</table>
					</td>
				</tr>
			</table>
		</form>
	</body>
</HTML>
