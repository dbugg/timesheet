﻿<%@ Register TagPrefix="uc1" TagName="PageHeader" Src="../UserControls/PageHeader.ascx" %>
<%@ Register TagPrefix="activereportsweb" Namespace="GrapeCity.ActiveReports.Web" Assembly="GrapeCity.ActiveReports.Web.v7, Version=7.1.7470.0, Culture=neutral, PublicKeyToken=cc4967777c49a3ff" %>
<%@ Page language="c#" Codebehind="ProjectAnalysis.aspx.cs" AutoEventWireup="false" Inherits="Asa.Timesheet.WebPages.Reports.ProjectAnalysis"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
	<head runat="server">
		<title>Project Analysis</title>
		<meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
		<meta content="C#" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<LINK href="../styles/timesheet.css" type="text/css" rel="stylesheet">
		<script language="javascript" src="../PopupCalendar.js"></script>
		<script language="javascript">
		
		function SetGroupOrder()
		{
			var srcElement = event.srcElement;
			var newValue = srcElement.value;
			
			var arr = new Array();
			if (document.getElementById("ddlUserOrder")!=srcElement) arr.push(document.getElementById("ddlUserOrder"));
			if (document.getElementById("ddlProjectOrder")!=srcElement) arr.push(document.getElementById("ddlProjectOrder"));
			if (document.getElementById("ddlActivityOrder")!=srcElement) arr.push(document.getElementById("ddlActivityOrder"));
			if (document.getElementById("ddlBuildingTypeOrder")!=srcElement) arr.push(document.getElementById("ddlBuildingTypeOrder"));
			
			for (i=0; i<arr.length-1; i++)
			{
				for (j=i+1; j<arr.length; j++)
				if (arr[j].value<arr[i].value)
				{
					var k = arr[i]; arr[i] = arr[j]; arr[j] = k;
				}
				
			}
			
			var numbers = new Array();
			for (i=0; i<=3; i++) if (i!=newValue) numbers.push(i);
			
			for (i=0; i<numbers.length; i++) arr[i].value = numbers[i];
		}
		
		</script>
	</HEAD>
	<body bottomMargin="0" leftMargin="0" topMargin="0" rightMargin="0">
		<form id="Form1" method="post" runat="server">
			<table height="100%" cellSpacing="0" cellPadding="0" width="100%" border="0">
				<tr height="1">
					<td><uc1:pageheader id="header" runat="server"></uc1:pageheader></td>
				</tr>
				<tr style="DISPLAY: none" height="80">
					<td background="../images/copy1.gif">
						<TABLE id="Table2" height="100%" width="100%">
							<TR>
								<td style="WIDTH: 106px">&nbsp;&nbsp;&nbsp;&nbsp; <IMG height="80" alt="" src="../images/logo.jpg" width="64"></TD>
								<td noWrap><asp:label id="lblTitle" runat="server" Font-Bold="True" Font-Size="X-Small"></asp:label><br>
									<asp:label id="lblUser" runat="server" Font-Bold="True" Font-Size="Medium" ForeColor="SaddleBrown"></asp:label></TD>
							</TR>
						</TABLE>
					</td>
				</tr>
				<tr>
					<td>
						<table height="100%" cellSpacing="0" cellPadding="0" width="100%" border="0">
							<tr>
								<td class="menuLeft"><asp:placeholder id="menuHolder" runat="server"></asp:placeholder></td>
								
								<td style="PADDING-LEFT: 5px; PADDING-RIGHT: 5px; PADDING-TOP: 3px" vAlign="top" noWrap>
									<TABLE id="tblInfo" height="30" cellSpacing="0" cellPadding="3" width="880" border="0">
										<TR>
											<td vAlign="middle" noWrap><asp:label id="lblError" runat="server" ForeColor="Red" CssClass="ErrorLabel" EnableViewState="False"></asp:label><asp:label id="lblInfo" runat="server" CssClass="InfoLabel" EnableViewState="False"></asp:label></TD>
										</TR>
									</TABLE>
									<TABLE id="Table3" cellSpacing="0" cellPadding="0" width="880" border="0">
										<TR>
											<td style="HEIGHT: 5px" colSpan="2"></TD>
										</TR>
										<TR style="DISPLAY: none">
											<td colSpan="2"><IMG height="1" src="../images/dot.gif" width="100%"></TD>
										</TR>
										<TR>
											<td colSpan="2" height="3"><asp:checkbox id="ckDates" runat="server" CssClass="EnterDataLabel" AutoPostBack="True" Checked="True"
													Text="За целия период" Width="144px"></asp:checkbox></td>
										</TR>
										<TR>
											<td>
												<TABLE id="Table4" cellSpacing="0" cellPadding="3" border="0">
													<TR id="SelectDateRow">
														<td style="WIDTH: 139px; HEIGHT: 4px" vAlign="top" align="right"><asp:label id="lblStartDay" runat="server" CssClass="EnterDataLabel">Ден, седмица, нач.дата:</asp:label><asp:label id="lblSelStartDate" runat="server" Font-Bold="True" ForeColor="SaddleBrown" Width="100px"></asp:label></TD>
														<td style="WIDTH: 72px" vAlign="top">
															<TABLE id="Table1" cellSpacing="0" cellPadding="0" border="0">
																<TR>
																	<td><asp:calendar id="calStartDate" runat="server" ForeColor="#222222" Width="240px" SelectionMode="DayWeek"
																			BorderColor="#999999" CellPadding="0" BorderWidth="1px" BackColor="White" Height="240px" NextMonthText=""
																			PrevMonthText="" DayNameFormat="FirstLetter" SelectWeekText="+"
																			FirstDayOfWeek="Monday" BorderStyle="Solid" CssClass="aspCalendar" NextPrevFormat="ShortMonth" Font-Bold="True" Font-Size="11pt">
																			<TodayDayStyle ForeColor="#C73B2E" BackColor="White" BorderColor="#C73B2E" 
                                                                                BorderStyle="Solid" BorderWidth="2px" Height="32px" Width="32px"></TodayDayStyle>
																			<SelectorStyle Font-Size="10pt" Font-Names="Verdana" ForeColor="SaddleBrown" 
                                                                                BorderStyle="None"></SelectorStyle>
																			<DayStyle Font-Size="10pt" Font-Bold="False" BorderStyle="Dotted" 
                                                                                BorderColor="#CCCCCC" BorderWidth="1px" Height="30px" Width="32px"></DayStyle>
																			<NextPrevStyle Font-Bold="True" Wrap="False" HorizontalAlign="Center" 
                                                                                Height="16px" BorderStyle="None" VerticalAlign="Middle" Font-Size="9pt" 
                                                                                Font-Underline="True"></NextPrevStyle>
																			<DayHeaderStyle Font-Size="9pt" Height="1px" ForeColor="SaddleBrown"></DayHeaderStyle>
																			<SelectedDayStyle Font-Bold="True" ForeColor="#C73B2E" BorderColor="#C73B2E" 
                                                                                BackColor="White" BorderStyle="Solid" BorderWidth="2px"
                                                                                Font-Size="11pt" Height="32px" Width="32px"></SelectedDayStyle>
																			<TitleStyle Font-Size="13pt" Font-Bold="True" ForeColor="#C73B2E" 
                                                                                BackColor="White" HorizontalAlign="Center"></TitleStyle>
																			<OtherMonthDayStyle ForeColor="Gray" BackColor="#FDFDFD" Font-Size="6pt"></OtherMonthDayStyle>
																		    <WeekendDayStyle Font-Bold="True" Font-Italic="False" 
                                                                                ForeColor="#C73B2E" />
																		</asp:calendar>
																		
																	<td vAlign="top" noWrap></TD>
																</TR>
															</TABLE>
														</TD>
														<td style="WIDTH: 117px" vAlign="top" align="right"><asp:label id="lblEndDay" runat="server" CssClass="EnterDataLabel">Крайна дата:</asp:label><asp:label id="lblSelEndDate" runat="server" Font-Bold="True" ForeColor="SaddleBrown" Width="100px"></asp:label></TD>
														<td style="WIDTH: 278px; HEIGHT: 4px" vAlign="top">
															<TABLE id="Table6" cellSpacing="0" cellPadding="0" border="0">
																<TR>
																	<td><asp:calendar id="calEndDate" runat="server" ForeColor="#222222" Width="240px" SelectionMode="DayWeek"
																			BorderColor="#999999" CellPadding="0" BorderWidth="1px" BackColor="White" Height="240px" NextMonthText=""
																			PrevMonthText="" DayNameFormat="FirstLetter" SelectWeekText="+"
																			FirstDayOfWeek="Monday" BorderStyle="Solid" CssClass="aspCalendar" NextPrevFormat="ShortMonth" Font-Bold="True" Font-Size="11pt">
																			<TodayDayStyle ForeColor="#C73B2E" BackColor="White" BorderColor="#C73B2E" 
                                                                                BorderStyle="Solid" BorderWidth="2px" Height="32px" Width="32px"></TodayDayStyle>
																			<SelectorStyle Font-Size="10pt" Font-Names="Verdana" ForeColor="SaddleBrown" 
                                                                                BorderStyle="None"></SelectorStyle>
																			<DayStyle Font-Size="10pt" Font-Bold="False" BorderStyle="Dotted" 
                                                                                BorderColor="#CCCCCC" BorderWidth="1px" Height="30px" Width="32px"></DayStyle>
																			<NextPrevStyle Font-Bold="True" Wrap="False" HorizontalAlign="Center" 
                                                                                Height="16px" BorderStyle="None" VerticalAlign="Middle" Font-Size="9pt" 
                                                                                Font-Underline="True"></NextPrevStyle>
																			<DayHeaderStyle Font-Size="9pt" Height="1px" ForeColor="SaddleBrown"></DayHeaderStyle>
																			<SelectedDayStyle Font-Bold="True" ForeColor="#C73B2E" BorderColor="#C73B2E" 
                                                                                BackColor="White" BorderStyle="Solid" BorderWidth="2px"
                                                                                Font-Size="11pt" Height="32px" Width="32px"></SelectedDayStyle>
																			<TitleStyle Font-Size="13pt" Font-Bold="True" ForeColor="#C73B2E" 
                                                                                BackColor="White" HorizontalAlign="Center"></TitleStyle>
																			<OtherMonthDayStyle ForeColor="Gray" BackColor="#FDFDFD" Font-Size="6pt"></OtherMonthDayStyle>
																		    <WeekendDayStyle Font-Bold="True" Font-Italic="False" 
                                                                                ForeColor="#C73B2E" />
																		</asp:calendar>
																		
																	<td vAlign="top" noWrap></TD>
																</TR>
															</TABLE>
														</TD>
													</TR>
												</TABLE>
											</TD>
										</TR>
										<TR>
											<td style="HEIGHT: 3px" colSpan="2"></TD>
										</TR>
										<TR>
											<td colSpan="2"><IMG height="1" src="../images/dot.gif" width="100%"></TD>
										</TR>
										<TR>
											<td colSpan="2" height="3"></td>
										</TR>
										<tr>
											<td>
												<table cellSpacing="0" cellPadding="3" border="0">
													<TR>
														<td style="WIDTH: 159px" vAlign="top"><asp:label id="Label9" runat="server" CssClass="enterDataLabel" Width="100%">Тип:</asp:label></TD>
														<td style="WIDTH: 243px" vAlign="top"><asp:dropdownlist id="ddlProjectsStatus" runat="server" CssClass="EnterDataBox" AutoPostBack="True"
																Width="250px"></asp:dropdownlist></TD>
														<td vAlign="top"></TD>
														<td></TD>
														<td></TD>
														<td style="WIDTH: 8px"></TD>
														<td style="WIDTH: 8px"></TD>
													</TR>
													<TR>
														<td style="WIDTH: 159px" vAlign="top"><asp:label id="Label7" runat="server" CssClass="enterDataLabel" Width="100%">Сграда:</asp:label></TD>
														<td style="WIDTH: 243px" vAlign="top"><asp:dropdownlist id="ddlBuildingTypes" runat="server" CssClass="enterDataBox" AutoPostBack="True"
																Width="250px"></asp:dropdownlist></TD>
														<td vAlign="top"></TD>
														<td></TD>
														<td></TD>
														<td style="WIDTH: 8px"></TD>
														<td style="WIDTH: 8px"></TD>
													</TR>
													<TR>
														<td style="WIDTH: 159px" vAlign="top"><asp:label id="Label2" runat="server" CssClass="enterDataLabel" Width="100%">Проект:</asp:label></TD>
														<td style="WIDTH: 243px" vAlign="top"><asp:dropdownlist id="ddlProjects" runat="server" CssClass="EnterDataBox" Width="250px"></asp:dropdownlist></TD>
														<td vAlign="top"></TD>
														<td></TD>
														<td height="10"></TD>
														<td style="WIDTH: 8px" height="10"></TD>
														<td style="WIDTH: 8px" height="10"></TD>
													</TR>
													<TR>
														<td style="WIDTH: 159px" vAlign="top"><asp:label id="Label4" runat="server" CssClass="enterDataLabel" Width="100%">Задължения:</asp:label></TD>
														<td style="WIDTH: 243px" vAlign="top"><asp:dropdownlist id="ddlType" runat="server" CssClass="EnterDataBox" Width="250px">
																<asp:ListItem Value="-1">&lt;всички&gt;</asp:ListItem>
																<asp:ListItem Value="0">&lt;неплатени&gt;</asp:ListItem>
																<asp:ListItem Value="1">&lt;платени&gt;</asp:ListItem>
															</asp:dropdownlist></TD>
														<td vAlign="top"></TD>
														<td></TD>
														<td></TD>
														<td style="WIDTH: 8px"></TD>
														<td style="WIDTH: 8px"></TD>
													</TR>
													<TR>
														<td style="WIDTH: 159px" vAlign="top">
															<asp:label id="lbPayments" runat="server" CssClass="enterDataLabel" Width="100%">Към клиента:</asp:label></TD>
														<td style="WIDTH: 243px" vAlign="top">
															<asp:checkbox id="ckClient" runat="server" CssClass="DataLabel" Checked="True"></asp:checkbox></TD>
														<td vAlign="top"></TD>
														<td></TD>
														<td></TD>
														<td style="WIDTH: 8px"></TD>
														<td style="WIDTH: 8px"></TD>
													</TR>
													<TR>
														<td style="WIDTH: 159px" vAlign="top"><asp:label id="lbFeeAccount" runat="server" CssClass="enterDataLabel" Width="100%">Хонорар сметка:</asp:label></TD>
														<td style="WIDTH: 243px" vAlign="top"><asp:checkbox id="cbFeeAccount" runat="server" CssClass="enterDataBox" Width="250px"></asp:checkbox></TD>
														<td vAlign="top"></TD>
														<td></TD>
														<td></TD>
														<td style="WIDTH: 8px"></TD>
														<td style="WIDTH: 8px"></TD>
													</TR>
													<TR>
														<td style="WIDTH: 159px" vAlign="top">
															<asp:label id="Label11" runat="server" CssClass="enterDataLabel" Width="100%">Авторски надзор:</asp:label></TD>
														<td style="WIDTH: 243px" vAlign="top">
															<asp:checkbox id="ckAuthors" runat="server" CssClass="enterDataBox" Width="250px"></asp:checkbox></TD>
														<td vAlign="top"></TD>
														<td></TD>
														<td></TD>
														<td style="WIDTH: 8px"></TD>
														<td style="WIDTH: 8px"></TD>
													</TR>
													<TR>
														<td style="WIDTH: 159px" vAlign="top" height="10"><asp:label id="lbFinPokazateli" runat="server" CssClass="enterDataLabel" Width="100%">Финансови показатели:</asp:label></TD>
														<td style="WIDTH: 243px" vAlign="top" height="10"><asp:checkbox id="cbFinPokazateli" runat="server" CssClass="enterDataBox" Width="250px"></asp:checkbox></TD>
														<td vAlign="top" height="10"></TD>
														<td height="10"></TD>
														<td height="10"></TD>
														<td style="WIDTH: 8px" height="10"></TD>
														<td style="WIDTH: 8px" height="10"></TD>
													</TR>
													<TR>
														<td style="WIDTH: 159px" vAlign="top" height="10">
															<asp:label id="Label10" runat="server" CssClass="enterDataLabel" Width="100%">Фирма за финансови показатели:</asp:label></TD>
														<td style="WIDTH: 243px" vAlign="top" height="10">
															<asp:dropdownlist id="ddlCompany" runat="server" CssClass="EnterDataBox" Width="250px">
																<asp:ListItem Value="0">АСА</asp:ListItem>
																<asp:ListItem Value="1">АСИ</asp:ListItem>
															</asp:dropdownlist></TD>
														<td vAlign="top" height="10"></TD>
														<td height="10"></TD>
														<td height="10"><asp:imagebutton id="ibGenerate" runat="server" ImageUrl="../images/ie.gif"></asp:imagebutton></TD>
														<td style="WIDTH: 8px" height="10"><asp:imagebutton id="ibPdfExport" runat="server" ImageUrl="../images/pdf1.gif" ToolTip="Справка в PDF формат"></asp:imagebutton></TD>
														<td style="WIDTH: 8px" height="10"><asp:imagebutton id="ibXlsExport" runat="server" ImageUrl="../images/xls.gif" ToolTip="Справка в Excel формат"></asp:imagebutton></TD>
													</TR>
												</table>
											</td>
										</tr>
										<TR>
											<td style="HEIGHT: 5px" colSpan="2"></TD>
										</TR>
										<TR>
											<td colSpan="2"><IMG height="1" src="../images/dot.gif" width="100%"></TD>
										</TR>
										<TR>
											<td colSpan="2" height="3"></TD>
										</TR>
										<TR>
											<td colSpan="2"><IMG height="1" src="images/dot.gif" width="100%"></TD>
										</TR>
									</TABLE>
									<P>&nbsp;</P>
									<TABLE id="Table5" cellSpacing="0" cellPadding="0" width="100%" border="0">
										<TR>
											<td align="center"><asp:label id="lblReportTitle" runat="server" Font-Bold="True" Font-Size="11pt" EnableViewState="False"
													Visible="False">ФИНАНСОВ АНАЛИЗ ЗА ПРОЕКТ</asp:label></TD>
										</TR>
									</TABLE>
									<br>
									<TABLE id="tblHeader" cellSpacing="0" cellPadding="2" border="0" runat="server">
										<TR>
											<td vAlign="top"><asp:label id="Label5" runat="server" Font-Bold="True" Font-Size="9pt" Visible="false">ПРОЕКТ:</asp:label></TD>
											<td vAlign="top"><asp:label id="lblObekt" runat="server" Font-Size="9pt" Visible="false"></asp:label></TD>
										</TR>
									</TABLE>
									<br>
									<asp:label id="Label6" runat="server" Font-Bold="True" Font-Size="11pt" EnableViewState="False"
										Visible="False">Към клиента</asp:label><br>
									<asp:datagrid id="grdReport" runat="server" ForeColor="DimGray" CssClass="ReportGrid" EnableViewState="False"
										Width="100%" CellPadding="4" ShowHeader="True" AutoGenerateColumns="False">
										<HeaderStyle CssClass="reportGridHeader"></HeaderStyle>
										<Columns>
											<asp:BoundColumn DataField="PaymentID" Visible="false"></asp:BoundColumn>
											<asp:BoundColumn DataField="SubprojectID" Visible="false"></asp:BoundColumn>
											<asp:BoundColumn DataField="projectname" HeaderText="Проект"></asp:BoundColumn>
											<asp:BoundColumn DataField="SubprojectType" HeaderText="Фаза"></asp:BoundColumn>
											<asp:BoundColumn DataField="Amount" HeaderText="Обща сума (EUR)" DataFormatString="{0:n2}">
												<HeaderStyle HorizontalAlign="Right"></HeaderStyle>
												<ItemStyle HorizontalAlign="Right" Width="80px"></ItemStyle>
											</asp:BoundColumn>
											<asp:BoundColumn DataField="AmountBGN" HeaderText="Обща сума (BGN)" DataFormatString="{0:n2}">
												<HeaderStyle HorizontalAlign="Right"></HeaderStyle>
												<ItemStyle HorizontalAlign="Right" Width="80px"></ItemStyle>
											</asp:BoundColumn>
											<asp:TemplateColumn HeaderText="Дата на плащане">
												<ItemStyle HorizontalAlign="Center" Width="60px"></ItemStyle>
												<ItemTemplate>
													<asp:Label runat="server" Text='<%# Asa.Timesheet.Data.Util.TimeHelper.FormatDateO(DataBinder.Eval(Container, "DataItem.PaymentDate")) %>' ID="Label1" >
													</asp:Label>
												</ItemTemplate>
											</asp:TemplateColumn>
											<asp:BoundColumn DataField="PaymentPercent" DataFormatString="{0:n2}" ItemStyle-Width="40px" HeaderText="Процент"></asp:BoundColumn>
											<asp:TemplateColumn HeaderText="Платено">
												<HeaderStyle HorizontalAlign="Center"></HeaderStyle>
												<ItemStyle HorizontalAlign="Center" Width="40px"></ItemStyle>
												<ItemTemplate>
													<asp:Image id=imgDone ImageUrl='<%# GetImage(DataBinder.Eval(Container, "DataItem.Done")) %>' Runat=server>
													</asp:Image>
												</ItemTemplate>
											</asp:TemplateColumn>
											<asp:BoundColumn DataField="TotalPercent" DataFormatString="{0:n2}" ItemStyle-Width="40px" HeaderText="Процент"
												Visible="False"></asp:BoundColumn>
										</Columns>
									</asp:datagrid>
									<P>
										<asp:label id="lbReport1" runat="server" Font-Size="11pt" Font-Bold="True" EnableViewState="False"
											Visible="False">Проекти с плащания по 
            фази</asp:label></P>
									<P>
										<asp:datagrid id="grdReport1" runat="server" ForeColor="DimGray" EnableViewState="False" CssClass="ReportGrid"
											Width="100%" CellPadding="4" AutoGenerateColumns="False" ShowHeader="True">
											<HeaderStyle CssClass="reportGridHeader"></HeaderStyle>
											<Columns>
												<asp:BoundColumn Visible="False" DataField="PaymentID"></asp:BoundColumn>
												<asp:BoundColumn Visible="False" DataField="SubprojectID"></asp:BoundColumn>
												<asp:BoundColumn DataField="projectname" HeaderText="Проект"></asp:BoundColumn>
												<asp:BoundColumn DataField="SubprojectType" HeaderText="Фаза"></asp:BoundColumn>
												<asp:BoundColumn DataField="Amount" HeaderText="Обща сума (EUR)" DataFormatString="{0:n2}">
													<HeaderStyle HorizontalAlign="Right"></HeaderStyle>
													<ItemStyle HorizontalAlign="Right" Width="80px"></ItemStyle>
												</asp:BoundColumn>
												<asp:BoundColumn DataField="AmountBGN" HeaderText="Обща сума (BGN)" DataFormatString="{0:n2}">
													<HeaderStyle HorizontalAlign="Right"></HeaderStyle>
													<ItemStyle HorizontalAlign="Right" Width="80px"></ItemStyle>
												</asp:BoundColumn>
												<asp:TemplateColumn HeaderText="Дата на плащане">
													<ItemStyle HorizontalAlign="Center" Width="60px"></ItemStyle>
													<ItemTemplate>
														<asp:Label runat="server" Text='<%# Asa.Timesheet.Data.Util.TimeHelper.FormatDateO(DataBinder.Eval(Container, "DataItem.PaymentDate")) %>' ID="Label1" >
														</asp:Label>
													</ItemTemplate>
												</asp:TemplateColumn>
												<asp:BoundColumn DataField="PaymentPercent" HeaderText="Процент" DataFormatString="{0:n2}">
													<ItemStyle Width="40px"></ItemStyle>
												</asp:BoundColumn>
												<asp:TemplateColumn HeaderText="Платено">
													<HeaderStyle HorizontalAlign="Center"></HeaderStyle>
													<ItemStyle HorizontalAlign="Center" Width="40px"></ItemStyle>
													<ItemTemplate>
														<asp:Image id=imgDone ImageUrl='<%# GetImage(DataBinder.Eval(Container, "DataItem.Done")) %>' Runat=server>
														</asp:Image>
													</ItemTemplate>
												</asp:TemplateColumn>
												<asp:BoundColumn DataField="TotalPercent" DataFormatString="{0:n2}" ItemStyle-Width="40px" HeaderText="Процент"
													Visible="False"></asp:BoundColumn>
											</Columns>
										</asp:datagrid><br>
										<asp:label id="lbgrdReport" runat="server" Font-Bold="True" Font-Size="11pt" EnableViewState="False"
											Visible="False"> Към подизпълнители</asp:label><br>
									</P>
									<asp:datagrid id="grdReportPhases" runat="server" ForeColor="DimGray" CssClass="ReportGrid" EnableViewState="False"
										Width="100%" CellPadding="4" ShowHeader="True" AutoGenerateColumns="False">
										<HeaderStyle CssClass="reportGridHeader"></HeaderStyle>
										<Columns>
											<asp:BoundColumn DataField="PaymentID" Visible="false"></asp:BoundColumn>
											<asp:BoundColumn DataField="ProjectSubcontracterID" Visible="false"></asp:BoundColumn>
											<asp:BoundColumn DataField="projectname" HeaderText="Проект" Visible="false"></asp:BoundColumn>
											<asp:BoundColumn DataField="SubcontracterName" HeaderText="Подизпълнител"></asp:BoundColumn>
											<asp:BoundColumn DataField="SubcontracterType" HeaderText="Специалност"></asp:BoundColumn>
											<asp:BoundColumn DataField="Amount" HeaderText="Обща сума (EUR)" DataFormatString="{0:n2}">
												<HeaderStyle HorizontalAlign="Right"></HeaderStyle>
												<ItemStyle HorizontalAlign="Right" Width="80px"></ItemStyle>
											</asp:BoundColumn>
											<asp:BoundColumn DataField="AmountBGN" HeaderText="Обща сума (BGN)" DataFormatString="{0:n2}">
												<HeaderStyle HorizontalAlign="Right"></HeaderStyle>
												<ItemStyle HorizontalAlign="Right" Width="80px"></ItemStyle>
											</asp:BoundColumn>
											<asp:TemplateColumn HeaderText="Дата на плащане">
												<ItemStyle HorizontalAlign="Right" Width="60px"></ItemStyle>
												<ItemTemplate>
													<asp:Label runat="server" Text='<%# Asa.Timesheet.Data.Util.TimeHelper.FormatDateO(DataBinder.Eval(Container, "DataItem.PaymentDate")) %>' ID="Label3" >
													</asp:Label>
												</ItemTemplate>
											</asp:TemplateColumn>
											<asp:BoundColumn DataField="PaymentPercent" DataFormatString="{0:n2}" ItemStyle-Width="40px" HeaderText="Процент"></asp:BoundColumn>
											<asp:TemplateColumn HeaderText="Платено">
												<HeaderStyle HorizontalAlign="Center"></HeaderStyle>
												<ItemStyle HorizontalAlign="Center" Width="40px"></ItemStyle>
												<ItemTemplate>
													<asp:Image id="Image1" ImageUrl='<%# GetImage(DataBinder.Eval(Container, "DataItem.Done")) %>' Runat=server>
													</asp:Image>
												</ItemTemplate>
											</asp:TemplateColumn>
											<asp:BoundColumn DataField="TotalPercent" DataFormatString="{0:n2}" ItemStyle-Width="40px" HeaderText="Процент"
												Visible="False"></asp:BoundColumn>
										</Columns>
									</asp:datagrid><asp:label id="lblNoDataFound" runat="server" CssClass="InfoLabel" EnableViewState="False"
										Visible="False"></asp:label><br>
									<br>
									<asp:label id="lbWorkTime" runat="server" Font-Bold="True" Font-Size="11pt" EnableViewState="False"
										Visible="False">Работно време</asp:label><br>
									<asp:datagrid id="grdWorkTime" runat="server" ForeColor="DimGray" CssClass="ReportGrid" EnableViewState="False"
										Width="100%" CellPadding="4" AutoGenerateColumns="False">
										<HeaderStyle CssClass="reportGridHeader"></HeaderStyle>
										<Columns>
											<asp:BoundColumn DataField="FullName" HeaderText="Служител"></asp:BoundColumn>
											<asp:TemplateColumn HeaderText="Общо(часа)">
												<ItemStyle HorizontalAlign="Right" Width="90px"></ItemStyle>
												<ItemTemplate>
													<asp:Label runat="server" Text='<%# Asa.Timesheet.Data.Util.TimeHelper.HoursStringFromMinutes((int)DataBinder.Eval(Container, "DataItem.TotalTime"),false) %>' ID="Label8" >
													</asp:Label>
												</ItemTemplate>
											</asp:TemplateColumn>
											<asp:BoundColumn HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center" DataField="Amount"
												HeaderText="Възнаграждение (BGN)" DataFormatString="{0:n2}"></asp:BoundColumn>
										</Columns>
									</asp:datagrid>
									<P>
										<TABLE id="Table7" cellSpacing="0" cellPadding="0" width="100%" border="0">
											<TR>
												<td align="center"><asp:label id="lbFeeAccountTitle" runat="server" Font-Bold="True" Font-Size="11pt" EnableViewState="False"
														Visible="False">ХОНОРАР СМЕТКА</asp:label></TD>
											</TR>
										</TABLE>
									</P>
									<P><asp:datagrid id="grdFeeAccount" runat="server" ForeColor="DimGray" CssClass="ReportGrid" EnableViewState="False"
											Width="100%" CellPadding="4" ShowHeader="True" AutoGenerateColumns="False">
											<HeaderStyle CssClass="reportGridHeader"></HeaderStyle>
											<Columns>
												<asp:BoundColumn DataField="SubprojectID" Visible="false"></asp:BoundColumn>
												<asp:BoundColumn DataField="projectname" HeaderText="Описание"></asp:BoundColumn>
												<asp:BoundColumn DataField="ClientName" HeaderText="Клиент"></asp:BoundColumn>
												<asp:BoundColumn DataField="Area" HeaderText="Кв.метра" DataFormatString="{0:n2}" ItemStyle-HorizontalAlign="Right"></asp:BoundColumn>
												<asp:BoundColumn DataField="EURForArea" HeaderText="EUR/Кв.метър" DataFormatString="{0:n2}" ItemStyle-HorizontalAlign="Right"></asp:BoundColumn>
												<asp:BoundColumn DataField="EUR" HeaderText="Общо ЕUR" DataFormatString="{0:n2}" ItemStyle-HorizontalAlign="Right"></asp:BoundColumn>
												<asp:BoundColumn DataField="Amount" HeaderText="Платени" DataFormatString="{0:n2}" ItemStyle-HorizontalAlign="Right"></asp:BoundColumn>
												<asp:BoundColumn HeaderText="Платени %" ItemStyle-HorizontalAlign="Right"></asp:BoundColumn>
												<asp:BoundColumn DataField="AmountLeft" HeaderText="Дължими към" DataFormatString="{0:n2}" ItemStyle-HorizontalAlign="Right"></asp:BoundColumn>
												<asp:BoundColumn HeaderText="Дължими %" ItemStyle-HorizontalAlign="Right"></asp:BoundColumn>
												<asp:BoundColumn DataField="ORDERCLAUSE" Visible="false"></asp:BoundColumn>
											</Columns>
										</asp:datagrid><br>
									<P>
										<TABLE id="Table8" cellSpacing="0" cellPadding="0" width="100%" border="0">
											<TR>
												<td align="center"><asp:label id="lbFinancePokazateliTitle" runat="server" Font-Size="11pt" Font-Bold="True" EnableViewState="False"
														Visible="False">ФИНАНСОВИ ПОКАЗАТЕЛИ</asp:label></TD>
											</TR>
										</TABLE>
									</P>
									<P>
										<asp:label id="lbEUR" runat="server" Font-Size="11pt" Font-Bold="True" EnableViewState="False"
											Visible="False">в EUR</asp:label><asp:datagrid id="grdFinancePokazateli" runat="server" ForeColor="DimGray" EnableViewState="False"
											CssClass="ReportGrid" Width="100%" CellPadding="4" AutoGenerateColumns="False" ShowHeader="True">
											<HeaderStyle CssClass="reportGridHeader"></HeaderStyle>
											<Columns>
												<asp:BoundColumn DataField="ProjectID" Visible="False"></asp:BoundColumn>
												<asp:BoundColumn HeaderStyle-Font-Size="8" DataField="ProjectName" HeaderText="ПРОЕКТ"></asp:BoundColumn>
												<asp:BoundColumn HeaderStyle-Font-Size="8" DataField="Area" HeaderText="M2" DataFormatString="{0:n0}"
													ItemStyle-HorizontalAlign="Right"></asp:BoundColumn>
												<asp:BoundColumn HeaderStyle-Font-Size="8" DataField="Payments" HeaderText="ПРИХОДИ EUR" DataFormatString="{0:n2}"
													ItemStyle-HorizontalAlign="Right"></asp:BoundColumn>
												<asp:BoundColumn HeaderStyle-Font-Size="8" DataField="PaymentArea" HeaderText="ПРИХОДИ EUR/M2" DataFormatString="{0:n2}"
													ItemStyle-HorizontalAlign="Right"></asp:BoundColumn>
												<asp:BoundColumn HeaderStyle-Font-Size="8" DataField="WorkedHours" HeaderText="РАБ. ЧАСОВЕ" DataFormatString="{0:n1}"
													ItemStyle-HorizontalAlign="Right"></asp:BoundColumn>
												<asp:BoundColumn HeaderStyle-Font-Size="8" DataField="WorkedSalary" HeaderText="ЗАПЛАТИ EUR" DataFormatString="{0:n2}"
													ItemStyle-HorizontalAlign="Right"></asp:BoundColumn>
												<asp:BoundColumn HeaderStyle-Font-Size="8" DataField="AdminSalary" HeaderText="ОБЩИ АДМИН. РАЗХОДИ EUR"
													DataFormatString="{0:n2}" ItemStyle-HorizontalAlign="Right"></asp:BoundColumn>
												<asp:BoundColumn HeaderStyle-Font-Size="8" DataField="LeaderSalary" HeaderText="РАЗХОДИ РЪКОВОДИТЕЛ EUR"
													DataFormatString="{0:n2}" ItemStyle-HorizontalAlign="Right"></asp:BoundColumn>
												<asp:BoundColumn HeaderStyle-Font-Size="8" DataField="StaffSalary" HeaderText="РАЗХОДИ ЗАПЛАТИ АДМИНИСТРАЦИЯ EUR"
													DataFormatString="{0:n2}" ItemStyle-HorizontalAlign="Right"></asp:BoundColumn>
												<asp:BoundColumn HeaderStyle-Font-Size="8" DataField="SubcontracterSalary" HeaderText="ПОДИЗП. РАЗХОДИ EUR"
													DataFormatString="{0:n2}" ItemStyle-HorizontalAlign="Right"></asp:BoundColumn>
												<asp:BoundColumn HeaderStyle-Font-Size="8" DataField="TotalSalary" HeaderText="ВСИЧКО РАЗХОДИ EUR"
													DataFormatString="{0:n2}" ItemStyle-HorizontalAlign="Right"></asp:BoundColumn>
												<asp:BoundColumn HeaderStyle-Font-Size="8" DataField="ExpencesPerHour" HeaderText="РАЗХОДИ НА ЧАС EUR"
													DataFormatString="{0:n2}" ItemStyle-HorizontalAlign="Right"></asp:BoundColumn>
												<asp:BoundColumn DataField="ExpencesPerArea" HeaderText="РАЗХОДИ EUR /М2" DataFormatString="{0:n2}">
													<HeaderStyle Font-Size="8pt"></HeaderStyle>
													<ItemStyle HorizontalAlign="Right"></ItemStyle>
												</asp:BoundColumn>
												<asp:BoundColumn HeaderStyle-Font-Size="8" DataField="PaymentPerHour" HeaderText="ПРИХОДИ НА ЧАС EUR"
													DataFormatString="{0:n2}" ItemStyle-HorizontalAlign="Right"></asp:BoundColumn>
												<asp:BoundColumn HeaderStyle-Font-Size="8" DataField="ImcomePerHour" HeaderText="ПЕЧАЛБА НА ЧАС EUR"
													DataFormatString="{0:n2}" ItemStyle-HorizontalAlign="Right"></asp:BoundColumn>
												<asp:BoundColumn HeaderStyle-Font-Size="8" DataField="TotalIncome" HeaderText="ВСИЧКО ПЕЧАЛБА EUR"
													DataFormatString="{0:n2}" ItemStyle-HorizontalAlign="Right"></asp:BoundColumn>
											</Columns>
										</asp:datagrid>
										<asp:label id="lbBGN" runat="server" Font-Size="11pt" Font-Bold="True" EnableViewState="False"
											Visible="False">в лева</asp:label>
										<asp:datagrid id="grdReportBGN" runat="server" ForeColor="DimGray" EnableViewState="False" CssClass="ReportGrid"
											Width="100%" CellPadding="4" AutoGenerateColumns="False" ShowHeader="True">
											<HeaderStyle CssClass="reportGridHeader"></HeaderStyle>
											<Columns>
												<asp:BoundColumn Visible="False" DataField="ProjectID">
													<HeaderStyle Font-Size="8pt"></HeaderStyle>
												</asp:BoundColumn>
												<asp:BoundColumn DataField="ProjectName" HeaderText="ПРОЕКТ">
													<HeaderStyle Font-Size="8pt"></HeaderStyle>
												</asp:BoundColumn>
												<asp:BoundColumn DataField="Area" HeaderText="M2" DataFormatString="{0:n0}">
													<HeaderStyle Font-Size="8pt"></HeaderStyle>
													<ItemStyle HorizontalAlign="Right"></ItemStyle>
												</asp:BoundColumn>
												<asp:BoundColumn DataField="Payments" HeaderText="ПРИХОДИ BGN" DataFormatString="{0:n2}">
													<HeaderStyle Font-Size="8pt"></HeaderStyle>
													<ItemStyle HorizontalAlign="Right"></ItemStyle>
												</asp:BoundColumn>
												<asp:BoundColumn DataField="PaymentArea" HeaderText="ПРИХОДИ BGN/M2" DataFormatString="{0:n2}">
													<HeaderStyle Font-Size="8pt"></HeaderStyle>
													<ItemStyle HorizontalAlign="Right"></ItemStyle>
												</asp:BoundColumn>
												<asp:BoundColumn DataField="WorkedHours" HeaderText="РАБ. ЧАСОВЕ" DataFormatString="{0:n1}">
													<HeaderStyle Font-Size="8pt"></HeaderStyle>
													<ItemStyle HorizontalAlign="Right"></ItemStyle>
												</asp:BoundColumn>
												<asp:BoundColumn DataField="WorkedSalary" HeaderText="ЗАПЛАТИ BGN" DataFormatString="{0:n2}">
													<HeaderStyle Font-Size="8pt"></HeaderStyle>
													<ItemStyle HorizontalAlign="Right"></ItemStyle>
												</asp:BoundColumn>
												<asp:BoundColumn DataField="AdminSalary" HeaderText="ОБЩИ АДМИН. РАЗХОДИ BGN" DataFormatString="{0:n2}">
													<HeaderStyle Font-Size="8pt"></HeaderStyle>
													<ItemStyle HorizontalAlign="Right"></ItemStyle>
												</asp:BoundColumn>
												<asp:BoundColumn DataField="LeaderSalary" HeaderText="РАЗХОДИ РЪКОВОДИТЕЛ BGN" DataFormatString="{0:n2}">
													<HeaderStyle Font-Size="8pt"></HeaderStyle>
													<ItemStyle HorizontalAlign="Right"></ItemStyle>
												</asp:BoundColumn>
												<asp:BoundColumn DataField="StaffSalary" HeaderText="РАЗХОДИ ЗАПЛАТИ АДМИНИСТРАЦИЯ BGN" DataFormatString="{0:n2}">
													<HeaderStyle Font-Size="8pt"></HeaderStyle>
													<ItemStyle HorizontalAlign="Right"></ItemStyle>
												</asp:BoundColumn>
												<asp:BoundColumn DataField="SubcontracterSalary" HeaderText="ПОДИЗП. РАЗХОДИ BGN" DataFormatString="{0:n2}">
													<HeaderStyle Font-Size="8pt"></HeaderStyle>
													<ItemStyle HorizontalAlign="Right"></ItemStyle>
												</asp:BoundColumn>
												<asp:BoundColumn DataField="TotalSalary" HeaderText="ВСИЧКО РАЗХОДИ BGN" DataFormatString="{0:n2}">
													<HeaderStyle Font-Size="8pt"></HeaderStyle>
													<ItemStyle HorizontalAlign="Right"></ItemStyle>
												</asp:BoundColumn>
												<asp:BoundColumn DataField="ExpencesPerHour" HeaderText="РАЗХОДИ НА ЧАС BGN" DataFormatString="{0:n2}">
													<HeaderStyle Font-Size="8pt"></HeaderStyle>
													<ItemStyle HorizontalAlign="Right"></ItemStyle>
												</asp:BoundColumn>
												<asp:BoundColumn DataField="ExpencesPerArea" HeaderText="РАЗХОДИ BGN /М2" DataFormatString="{0:n2}">
													<HeaderStyle Font-Size="8pt"></HeaderStyle>
													<ItemStyle HorizontalAlign="Right"></ItemStyle>
												</asp:BoundColumn>
												<asp:BoundColumn DataField="PaymentPerHour" HeaderText="ПРИХОДИ НА ЧАС BGN" DataFormatString="{0:n2}">
													<HeaderStyle Font-Size="8pt"></HeaderStyle>
													<ItemStyle HorizontalAlign="Right"></ItemStyle>
												</asp:BoundColumn>
												<asp:BoundColumn DataField="ImcomePerHour" HeaderText="ПЕЧАЛБА НА ЧАС BGN" DataFormatString="{0:n2}">
													<HeaderStyle Font-Size="8pt"></HeaderStyle>
													<ItemStyle HorizontalAlign="Right"></ItemStyle>
												</asp:BoundColumn>
												<asp:BoundColumn DataField="TotalIncome" HeaderText="ВСИЧКО ПЕЧАЛБА BGN" DataFormatString="{0:n2}">
													<HeaderStyle Font-Size="8pt"></HeaderStyle>
													<ItemStyle HorizontalAlign="Right"></ItemStyle>
												</asp:BoundColumn>
											</Columns>
										</asp:datagrid><BR>
										<asp:label id="lbAuth" runat="server" Font-Size="11pt" Font-Bold="True" EnableViewState="False"
											Visible="False">Авторски надзор</asp:label><BR>
										<asp:label id="lbTotal" runat="server" Font-Size="9pt" Font-Bold="True"></asp:label><BR>
										<asp:DataGrid id="grdAuthors" runat="server" ForeColor="DimGray" EnableViewState="False" CssClass="ReportGrid"
											Width="100%" CellPadding="4" Visible="False" AutoGenerateColumns="False" GridLines="Horizontal">
											<HeaderStyle Font-Bold="True" CssClass="reportGridHeader"></HeaderStyle>
											<Columns>
												<asp:BoundColumn DataField="UserName" HeaderText="Служител">
													<HeaderStyle Wrap="False" Width="250px"></HeaderStyle>
													<ItemStyle Wrap="False" VerticalAlign="Top"></ItemStyle>
												</asp:BoundColumn>
												<asp:BoundColumn DataField="WorkDate" HeaderText="Дата" DataFormatString="{0:dd.MM.yyyy}">
													<HeaderStyle Wrap="False" Width="50px"></HeaderStyle>
													<ItemStyle Wrap="False" VerticalAlign="Top"></ItemStyle>
												</asp:BoundColumn>
												<asp:BoundColumn DataField="StartHour" HeaderText="Нач.час">
													<HeaderStyle Wrap="False" Width="50px"></HeaderStyle>
													<ItemStyle Wrap="False" HorizontalAlign="Right" VerticalAlign="Top"></ItemStyle>
												</asp:BoundColumn>
												<asp:BoundColumn DataField="EndHour" HeaderText="Кр.час">
													<HeaderStyle Wrap="False" Width="50px"></HeaderStyle>
													<ItemStyle Wrap="False" HorizontalAlign="Right" VerticalAlign="Top"></ItemStyle>
												</asp:BoundColumn>
												<asp:BoundColumn DataField="Minutes" HeaderText="Протокол">
													<HeaderStyle Wrap="False" Width="200px"></HeaderStyle>
													<ItemStyle VerticalAlign="Top"></ItemStyle>
												</asp:BoundColumn>
												<asp:BoundColumn DataField="AdminMinutes" HeaderText="Админ.бележки">
													<HeaderStyle Wrap="False" Width="200px"></HeaderStyle>
													<ItemStyle VerticalAlign="Top"></ItemStyle>
												</asp:BoundColumn>
											</Columns>
										</asp:DataGrid><BR>
										<asp:label id="lbAuth1" runat="server" Font-Size="11pt" Font-Bold="True" EnableViewState="False"
											Visible="False">Плащания за авторски надзор</asp:label><BR>
										<asp:datagrid id="grd" runat="server" ForeColor="DimGray" EnableViewState="False" CssClass="ReportGrid"
											Width="100%" CellPadding="4" Visible="False" AutoGenerateColumns="False" ShowHeader="True">
											<HeaderStyle CssClass="reportGridHeader"></HeaderStyle>
											<Columns>
												<asp:BoundColumn Visible="False" DataField="PaymentID"></asp:BoundColumn>
												<asp:BoundColumn DataField="projectname" HeaderText="Проект"></asp:BoundColumn>
												<asp:BoundColumn DataField="Amount" HeaderText="Обща сума (EUR)" DataFormatString="{0:n2}">
													<HeaderStyle HorizontalAlign="Right"></HeaderStyle>
													<ItemStyle HorizontalAlign="Right" Width="80px"></ItemStyle>
												</asp:BoundColumn>
												<asp:BoundColumn DataField="AmountBGN" HeaderText="Обща сума (BGN)" DataFormatString="{0:n2}">
													<HeaderStyle HorizontalAlign="Right"></HeaderStyle>
													<ItemStyle HorizontalAlign="Right" Width="80px"></ItemStyle>
												</asp:BoundColumn>
												<asp:TemplateColumn HeaderText="Дата на плащане">
													<ItemStyle HorizontalAlign="Center" Width="60px"></ItemStyle>
													<ItemTemplate>
														<asp:Label runat="server" Text='<%# Asa.Timesheet.Data.Util.TimeHelper.FormatDateO(DataBinder.Eval(Container, "DataItem.PaymentDate")) %>' ID="Label1" >
														</asp:Label>
													</ItemTemplate>
												</asp:TemplateColumn>
												<asp:TemplateColumn HeaderText="Платено">
													<HeaderStyle HorizontalAlign="Center"></HeaderStyle>
													<ItemStyle HorizontalAlign="Center" Width="40px"></ItemStyle>
													<ItemTemplate>
														<asp:Image id=imgDone ImageUrl='<%# GetImage(DataBinder.Eval(Container, "DataItem.Done")) %>' Runat=server>
														</asp:Image>
													</ItemTemplate>
												</asp:TemplateColumn>
											</Columns>
										</asp:datagrid><br>
										<asp:label id="lbManHour1" runat="server" Font-Bold="True" Font-Size="9pt" Visible="false"
											DESIGNTIMEDRAGDROP="155">Човекочас(EUR):*</asp:label><asp:label id="lbMH2" runat="server" Font-Size="9pt" Visible="false" DESIGNTIMEDRAGDROP="162"></asp:label><br>
										<asp:label id="lbManHour2" runat="server" Font-Bold="True" Font-Size="9pt" Visible="false"
											DESIGNTIMEDRAGDROP="160">Човекочас(без подизпълнители, EUR):**</asp:label><asp:label id="lbMH1" runat="server" Font-Size="9pt" Visible="false" DESIGNTIMEDRAGDROP="157"></asp:label><br>
										<br>
									</P>
									<TABLE id="tblLegend" style="WIDTH: 768px; HEIGHT: 88px" runat="server">
										<TR>
											<td vAlign="top">* <STRONG>Човекочас </STRONG>е<STRONG> </STRONG>сума от всички 
												плащания от клиенти минус плащания към подизпълнители разделено на брой
												<BR>
												часове отчетени от служители на фирмата
											</TD>
											<td vAlign="top"></TD>
										</TR>
										<TR>
											<td vAlign="top">** <STRONG>Човекочас(без подизпълнители) </STRONG>е<STRONG> </STRONG>
												сума от всички плащания от клиенти&nbsp;разделени на брой
												<BR>
												часове отчетени от служители на фирмата
											</TD>
											<td vAlign="top"></TD>
										</TR>
									</TABLE>
									<asp:label id="lblCreatedByLbl" runat="server" Font-Bold="True" Font-Size="9pt" EnableViewState="False"
										Visible="False">ИЗГОТВИЛ СПРАВКАТА:</asp:label><asp:label id="lblCreatedBy" runat="server" Font-Size="9pt" EnableViewState="False" Visible="False"></asp:label><br>
								</td>
							</tr>
						</table>
					</td>
				</tr>
			</table>
		</form>
	</body>
</HTML>
