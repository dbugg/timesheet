using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Data.SqlClient;
using log4net;
using Asa.Timesheet.Data;
using Asa.Timesheet.Data.Entities;
using Asa.Timesheet.Data.Util;
using GrapeCity.ActiveReports.Export.Pdf.Section;
using GrapeCity.ActiveReports.Export.Excel.Section;

namespace Asa.Timesheet.WebPages.Reports
{
	/// <summary>
	/// Summary description for WorkTimes.
	/// </summary>
	public class DDSAnalisis : TimesheetPageBase
	{
		#region Web controls

		private static readonly ILog log = LogManager.GetLogger(typeof(WorkTimes));
		private static readonly DateTime MinReportDate = DateTime.ParseExact(System.Configuration.ConfigurationManager.AppSettings["MinReportDate"],
			"ddMMyyyy", System.Globalization.CultureInfo.InvariantCulture);
		protected System.Web.UI.WebControls.DropDownList ddlBuildingTypes;
		protected System.Web.UI.WebControls.Label lblTitle;
		protected System.Web.UI.WebControls.Label lblUser;
		protected System.Web.UI.WebControls.Label lblObekt;
		protected System.Web.UI.WebControls.Label Label5;
		protected System.Web.UI.WebControls.PlaceHolder menuHolder;
		protected System.Web.UI.WebControls.DropDownList ddlPeriodType;
		protected System.Web.UI.WebControls.Label lblStartDay;
		protected System.Web.UI.WebControls.Label lblEndDay;
		protected System.Web.UI.WebControls.Calendar calStartDate;
		protected System.Web.UI.WebControls.Calendar calEndDate;
		protected System.Web.UI.WebControls.Label lblSelStartDate;
		protected System.Web.UI.WebControls.Label lblSelEndDate;
		protected System.Web.UI.WebControls.Label lblError;
		protected System.Web.UI.WebControls.Label lblInfo;
		protected System.Web.UI.WebControls.ImageButton ibGenerate;
		protected System.Web.UI.WebControls.ImageButton ibPdfExport;
		protected System.Web.UI.WebControls.ImageButton ibXlsExport;
		protected Asa.Timesheet.WebPages.UserControls.PageHeader header;
		protected System.Web.UI.WebControls.CheckBox ckDates;
		protected System.Web.UI.WebControls.DataGrid grdReport;
		protected System.Web.UI.WebControls.Label lblReportTitle;
		protected System.Web.UI.WebControls.Label lblCreatedBy;
		protected System.Web.UI.HtmlControls.HtmlTable tblFooter;
		protected System.Web.UI.WebControls.Label lblCreatedByLbl;
		protected System.Web.UI.WebControls.Label Label2;
		protected System.Web.UI.WebControls.DropDownList ddlClients;
		protected System.Web.UI.WebControls.DataGrid grdReportPhases;
		protected System.Web.UI.WebControls.Label lbgrdReport;
		protected System.Web.UI.HtmlControls.HtmlTable tblHeader;
		protected System.Web.UI.WebControls.Label lblProject;
		protected System.Web.UI.WebControls.DropDownList ddlProject;
		protected System.Web.UI.WebControls.Label Label1;
		protected System.Web.UI.WebControls.Label Label4;
		protected System.Web.UI.WebControls.Label lbProject;
		protected System.Web.UI.WebControls.Label lbClient;
    protected System.Web.UI.WebControls.DropDownList ddlProjectsStatus;
		protected System.Web.UI.WebControls.Label Label8;
		protected System.Web.UI.WebControls.Label Label6;
		protected System.Web.UI.WebControls.Label lblNoDataFound;

		#endregion

		#region Page load

        private void Page_Load(object sender, System.EventArgs e)
        {
            if (!this.IsPostBack)
            {
                header.PageTitle = Resource.ResourceManager["DDS_PageTitle"];
                header.UserName = this.LoggedUser.UserName;





                //
                calStartDate.SelectedDate = calStartDate.VisibleDate = new DateTime(DateTime.Today.Year, DateTime.Today.Month, 1);
                lblSelStartDate.Text = "(" + Resource.ResourceManager["reports_SelectDate"] + ")";
                calEndDate.SelectedDate = calEndDate.VisibleDate = DateTime.Today;
                lblSelEndDate.Text = "(" + Resource.ResourceManager["reports_SelectDate"] + ")";
                LoadClients("0");
                UIHelpers.LoadProjectStatus(ddlProjectsStatus, (int)ProjectsData.ProjectsByStatus.Active);
                UIHelpers.LoadBuildingTypes("", ddlBuildingTypes);
                LoadProjects();
            }
            else
                UIHelpers.LoadBuildingTypes(ddlBuildingTypes.SelectedValue, ddlBuildingTypes, (ProjectsData.ProjectsByStatus)int.Parse(ddlProjectsStatus.SelectedValue));



            UIHelpers.CreateMenuReports(menuHolder, LoggedUser);

        }
        private bool LoadClients(string selectedValue)
        {
            SqlDataReader reader = null;
            try
            {
                reader = ClientsData.SelectClients(0, (int)ClientTypes.Client, false);
                ddlClients.DataSource = reader;
                ddlClients.DataValueField = "ClientID";
                ddlClients.DataTextField = "ClientName";
                ddlClients.DataBind();

                ddlClients.Items.Insert(0, new ListItem(Resource.ResourceManager["reports_ddlAllClients"], "0"));
                if (ddlClients.Items.FindByValue(selectedValue) != null)
                    ddlClients.SelectedValue = selectedValue;
            }
            catch (Exception ex)
            {
                log.Info(ex);
                return false;
            }
            finally
            {
                if (reader != null) reader.Close();
            }
            return true;
        }
		#endregion

		#region Web Form Designer generated code
        override protected void OnInit(EventArgs e)
        {
            //
            // CODEGEN: This call is required by the ASP.NET Web Form Designer.
            //
            InitializeComponent();
            base.OnInit(e);
        }
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
        private void InitializeComponent()
        {
            this.ckDates.CheckedChanged += new System.EventHandler(this.ckDates_CheckedChanged);
            this.calStartDate.SelectionChanged += new System.EventHandler(this.calStartDate_SelectionChanged);
            this.calEndDate.SelectionChanged += new System.EventHandler(this.calEndDate_SelectionChanged);
            this.ddlProjectsStatus.SelectedIndexChanged += new System.EventHandler(this.ddlProjectsStatus_SelectedIndexChanged);
            this.ddlBuildingTypes.SelectedIndexChanged += new System.EventHandler(this.ddlBuildingTypes_SelectedIndexChanged);
            this.ibGenerate.Click += new System.Web.UI.ImageClickEventHandler(this.ibGenerate_Click);
            this.ibPdfExport.Click += new System.Web.UI.ImageClickEventHandler(this.ibPdfExport_Click);
            this.ibXlsExport.Click += new System.Web.UI.ImageClickEventHandler(this.ibXlsExport_Click);
            this.grdReportPhases.ItemDataBound += new System.Web.UI.WebControls.DataGridItemEventHandler(this.grdReportPhases_ItemDataBound);
            this.Load += new System.EventHandler(this.Page_Load);

        }
		#endregion



        private bool LoadProjects()
        {
            SqlDataReader reader = null;

            try
            {
                reader = ProjectsData.SelectProjectNamesClear(this.ProjectsStatus, int.Parse(ddlBuildingTypes.SelectedValue));

                ddlProject.DataSource = reader;
                ddlProject.DataValueField = "ProjectID";
                ddlProject.DataTextField = "ProjectName";
                ddlProject.DataBind();

                ddlProject.Items.Insert(0, new ListItem("<" + Resource.ResourceManager["reports_ddlAllProjects"] + ">", "-1"));
                ddlProject.SelectedValue = "-1";
            }
            catch (Exception ex)
            {
                log.Error(ex);
                return false;
            }

            finally
            {
                if (reader != null) reader.Close();
            }
            return true;
        }

	

		#region Menu

//		private void CreateMenu()
//		{
//			UserControls.MenuTable menu = new UserControls.MenuTable();
//			menu.ID = "MenuTable";
//
//			// 'Links' group
//			ArrayList menuItems = new ArrayList();
//
//			menuItems.Add(new MenuItemInfo(Resource.ResourceManager["menuItem_Hours"], "../Hours.aspx", true));
//			menuItems.Add(new MenuItemInfo(Resource.ResourceManager["menuItem_Projects"], "../Projects.aspx"));
//			if (LoggedUser.IsLeader || LoggedUser.IsAssistant) 
//			{
//				menuItems.Add(new MenuItemInfo(Resource.ResourceManager["menuItem_Clients"], "../Clients.aspx"));
//								menuItems.Add(new MenuItemInfo(Resource.ResourceManager["menuItem_Subcontracters"], "../Subcontracters.aspx"));
//				if (LoggedUser.IsLeader)
//				menuItems.Add(new MenuItemInfo(Resource.ResourceManager["menuItem_Users"], "../Users.aspx"));	
//			}
//			menuItems.Add(new MenuItemInfo(Resource.ResourceManager["menuItem_Emails"], "../Emails.aspx"));
//
//			menu.AddMenuGroup(Resource.ResourceManager["menuHeader_Links"], 20, menuItems);
//			
//			// 'New' group
//			menuItems = new ArrayList();
//			menuItems.Add(new MenuItemInfo(Resource.ResourceManager["menuItem_NewProject"], "../EditProject.aspx"));
//			if (LoggedUser.IsLeader || LoggedUser.IsAssistant)
//			{
//				menuItems.Add(new MenuItemInfo(Resource.ResourceManager["menuItem_NewClient"], "../EditClient.aspx"));
//				if (LoggedUser.IsLeader)
//				menuItems.Add(new MenuItemInfo(Resource.ResourceManager["menuItem_NewUser"], "../EditUser.aspx"));
//			}
//			menuItems.Add(new MenuItemInfo(Resource.ResourceManager["menuItem_NewEmail"], "../EditEmail.aspx"));
//			menuItems.Add(new MenuItemInfo(Resource.ResourceManager["newcontracter_PageTitle"], "../EditSubContracter.aspx"));
//
//
//			menu.AddMenuGroup(Resource.ResourceManager["menuHeader_New"], 30, menuItems);
//
//			// "Reports' group
//			menuItems = new ArrayList();
//			//menuItems.Add(new MenuItemInfo(Resource.ResourceManager["menuItem_WorkTimeReport"], "#"));
//			menuItems.Add(new MenuItemInfo(Resource.ResourceManager["menuItem_MinutesReport"], "Minutes.aspx"));
//			if (LoggedUser.IsLeader || LoggedUser.IsAssistant)
//				menuItems.Add(new MenuItemInfo(Resource.ResourceManager["menuItem_MailReport"], "../MailForm.aspx"));
//			
//			if (LoggedUser.IsLeader)
//			{
//				menuItems.Add(new MenuItemInfo(Resource.ResourceManager["menuItem_NotEnteredWorkTime"], "EmptyHours.aspx"));
//				menuItems.Add(new MenuItemInfo(Resource.ResourceManager["menuItem_Calendar"], "Calendar.aspx"));
//			}
//			menu.AddMenuGroup(Resource.ResourceManager["menuHeader_Reports"], 30, menuItems);
//
//			
//
//			menuItems = new ArrayList();
//			menuItems.Add(new MenuItemInfo(Resource.ResourceManager["menuItem_Help"], SessionManager.GetReportsHelpLink(),true,Pages.WorkTimes));
//			menu.AddMenuGroup("", 10, menuItems);
//
//
//			menuHolder.Controls.Add(menu);
//		}

		#endregion


        protected string GetImage(object Done)
        {
            if (Done == null || Done == System.DBNull.Value)
                return "~/images/delete.png";
            int nDone = (int)Done;
            if (nDone == 1)
                return "~/images/done.gif";
            return "~/images/delete.png";
        }

        private DataView GetData(bool add)
        {
            int nID = int.Parse(ddlClients.SelectedValue);
            DateTime dtStart, dtEnd;
            dtStart = dtEnd = Constants.DateMax;
            if (!ckDates.Checked)
            {
                dtStart = calStartDate.SelectedDate;
                dtEnd = calEndDate.SelectedDate;
                lblObekt.Text = calStartDate.SelectedDate.ToLongDateString() + " - " + calEndDate.SelectedDate.ToLongDateString();
            }
            DataView dv = ReportsData.ExecuteReportsDDSAnalysis(nID, dtStart, dtEnd, int.Parse(ddlProject.SelectedValue));
            if (dv != null && dv.Count > 0 && add)
            {
                DataRowView drv = dv[dv.Count - 1];
                DataRowView drv1 = dv.AddNew();
                drv1.Row.ItemArray = drv.Row.ItemArray;
                //dv.Table.Rows.Add(drv1.Row.ItemArray);
                if (drv["Amount"] != DBNull.Value)
                {
                    decimal d = (decimal)drv["Amount"];
                    drv1["Amount"] = drv1["AmountBGN"] = d * Constants.DDS * decimal.Parse(System.Configuration.ConfigurationManager.AppSettings["fixedBGNEUR"]);
                    drv1["projectname"] = Resource.ResourceManager["title_DDS"];
                }

                dv.Table.AcceptChanges();
            }
            return dv;
        }
        private void ibGenerate_Click(object sender, System.Web.UI.ImageClickEventArgs e)
        {

            lblCreatedBy.Text = this.LoggedUser.UserName;
            Label5.Visible = true;
            Label1.Visible = true;
            Label4.Visible = true;
            lblReportTitle.Visible = true;
            lblObekt.Visible = true;
            lblCreatedBy.Visible = true;
            lblCreatedByLbl.Visible = true;
            lblObekt.Text = ckDates.Text;


            lbProject.Text = ddlProject.SelectedItem.Text;
            lbProject.Visible = true;
            lbClient.Text = ddlClients.SelectedItem.Text;
            lbClient.Visible = true;


            grdReportPhases.DataSource = GetData(true);
            grdReportPhases.DataBind();
            if (grdReportPhases.Items.Count == 0)
            {
                grdReportPhases.Visible = false;
                lblNoDataFound.Visible = true;
                lblNoDataFound.Text = Resource.ResourceManager["reports_WorkTimes_NoDataMatch"];

            }
            else
                grdReportPhases.Visible = true;
        }

        private void grdReport_ItemDataBound(object sender, DataGridItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Item
                || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                if (e.Item.Cells[1].Text == "&nbsp;")
                {
                    e.Item.CssClass = "ReportsTotal";
                    e.Item.Cells[4].Text = "";
                    e.Item.Cells[5].Text = "";

                }
                else if (e.Item.Cells[0].Text == "&nbsp;")
                {
                    e.Item.CssClass = "ReportsSubTotal";
                    e.Item.Cells[4].Text = "";

                }

            }
        }

        private void grdReportPhases_ItemDataBound(object sender, System.Web.UI.WebControls.DataGridItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Item
                || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                if (e.Item.Cells[1].Text == "&nbsp;")
                {
                    e.Item.CssClass = "ReportsTotal";
                    e.Item.Cells[3].Text = "";
                    //e.Item.Cells[5].Text="";
                    e.Item.Cells[6].Text = "";
                    e.Item.Cells[7].Text = "";
                    e.Item.Cells[8].Text = "";

                }
                else if (e.Item.Cells[0].Text == "&nbsp;")
                {
                    e.Item.CssClass = "ReportsSubTotal";
                    e.Item.Cells[6].Text = "";
                    e.Item.Cells[7].Text = "";
                    e.Item.Cells[8].Text = "";

                }

            }
        }

        private void ckDates_CheckedChanged(object sender, System.EventArgs e)
        {
            if (calStartDate.SelectedDates.Count > 0)
                lblSelStartDate.Text = ckDates.Checked ? String.Concat("(", MinReportDate.ToString("d"), ")") :
                    String.Concat("(", calStartDate.SelectedDates[0].ToString("d"), ")");
            if (calEndDate.SelectedDates.Count > 0)
                lblSelEndDate.Text = ckDates.Checked ? String.Concat("(", DateTime.Today.ToString("d"), ")") :
                    String.Concat("(", calEndDate.SelectedDates[0].ToString("d"), ")");

            calStartDate.Enabled = calEndDate.Enabled = !ckDates.Checked;
        }

        private void calStartDate_SelectionChanged(object sender, System.EventArgs e)
        {
            lblSelStartDate.Text = String.Concat("(", calStartDate.SelectedDates[0].ToString("d"), ")");
            if (calStartDate.SelectedDates.Count > 1)
            {
                calEndDate.SelectedDate = calStartDate.SelectedDates[calStartDate.SelectedDates.Count - 1];
                calEndDate.VisibleDate = calEndDate.SelectedDate;

                calStartDate.SelectedDate = calStartDate.SelectedDates[0];
                lblSelEndDate.Text = String.Concat("(", calEndDate.SelectedDate.ToString("d"), ")"); ;
            }
        }

        private void calEndDate_SelectionChanged(object sender, System.EventArgs e)
        {
            lblSelEndDate.Text = String.Concat("(", calEndDate.SelectedDates[0].ToString("d"), ")");
        }
        private string GetPeriod()
        {
            string s = ckDates.Text;
            if (!ckDates.Checked)
            {
                DateTime dtStart = calStartDate.SelectedDate;
                DateTime dtEnd = calEndDate.SelectedDate;
                s = calStartDate.SelectedDate.ToLongDateString() + " - " + calEndDate.SelectedDate.ToLongDateString();
            }
            return s;
        }
        private void ibPdfExport_Click(object sender, System.Web.UI.ImageClickEventArgs e)
        {
            PdfExport pdf = new PdfExport();

            pdf.NeverEmbedFonts = "";




            GrapeCity.ActiveReports.SectionReport report = new DDSRpt(true, lblReportTitle.Text, GetPeriod(), ddlProject.SelectedItem.Text, ddlClients.SelectedItem.Text);
            report.PageSettings.Orientation = GrapeCity.ActiveReports.Document.Section.PageOrientation.Landscape;
            DataView dv = GetData(false);
            dv.RowFilter = "PaymentID is not null";
            report.DataSource = dv;
            report.Run();

            System.IO.MemoryStream memoryFile = new System.IO.MemoryStream();

            pdf.Export(report.Document, memoryFile);

            Response.Clear();
            Response.AppendHeader("content-disposition"
                , "attachment; filename=" + "DDSAnalysis.pdf");
            Response.ContentType = "application/pdf";

            memoryFile.WriteTo(Response.OutputStream);
            Response.End();
        }

        private void ibXlsExport_Click(object sender, System.Web.UI.ImageClickEventArgs e)
        {
            XlsExport pdf = new XlsExport();



            GrapeCity.ActiveReports.SectionReport report = new DDSRpt(false, lblReportTitle.Text, GetPeriod(), ddlProject.SelectedItem.Text, ddlClients.SelectedItem.Text);
            report.PageSettings.Orientation = GrapeCity.ActiveReports.Document.Section.PageOrientation.Landscape;
            DataView dv = GetData(false);
            dv.RowFilter = "PaymentID is not null";
            report.DataSource = dv;
            report.Run();


            System.IO.MemoryStream memoryFile = new System.IO.MemoryStream();
            pdf.Export(report.Document, memoryFile);

            Response.Clear();
            Response.AppendHeader("content-disposition"
                , "attachment; filename=" + "DDSAnalysis.xls");
            Response.ContentType = "application/pdf";

            memoryFile.WriteTo(Response.OutputStream);
            Response.End();
        }

        private void ddlProjectsStatus_SelectedIndexChanged(object sender, System.EventArgs e)
        {
            LoadProjects();
        }

        private void ddlBuildingTypes_SelectedIndexChanged(object sender, System.EventArgs e)
        {
            LoadProjects();
        }

    #region Properties

    public ProjectsData.ProjectsByStatus ProjectsStatus
    {
      get
      {
        try
        {
          return  (ProjectsData.ProjectsByStatus)int.Parse(ddlProjectsStatus.SelectedValue);
        }
        catch { return ProjectsData.ProjectsByStatus.AllProjects; }
      }
    }

    #endregion
	}
}
