using System;
using System.Data;
using Asa.Timesheet.Data.Reports;
using Asa.Timesheet.Data;
using Asa.Timesheet.Data.Entities;
using GrapeCity.ActiveReports;
using GrapeCity.ActiveReports.Controls;
using GrapeCity.ActiveReports.SectionReportModel;
using GrapeCity.ActiveReports.Document.Section;
using GrapeCity.ActiveReports.Document;

namespace Asa.Timesheet.WebPages.Reports
{
    public class BasicInfoSubreport : GrapeCity.ActiveReports.SectionReport
	{
		int _projectID;
		int _startPageNumber;
		bool _fullReport;

        public BasicInfoSubreport(int projectID, bool fullReport, int startPageNumber)
        {
            InitializeComponent();

            _projectID = projectID;
            _fullReport = fullReport;
            _startPageNumber = startPageNumber;

        }

        private void BasicInfoSubreport_ReportStart(object sender, System.EventArgs eArgs)
        {
            if (!this._fullReport)
            {
                this.txtPSLbl.Visible = false;
                this.txtPaymentScheme.Visible = false;
                this.txtPriceLbl.Visible = false;
                this.txtRate.Visible = false;
                this.txtEuroLbl.Visible = false;
                this.txtAmountEUR.Visible = false;
                this.txtBGNLbl.Visible = false;
                this.txtAmountBGN.Visible = false;
            }

            this.DataSource = ProjectsReportData.GetProjectBasicInfo(_projectID);
        }

        private void PageHeader_Format(object sender, System.EventArgs eArgs)
        {
            if (this.PageNumber == 1) this.PageHeader.Visible = false;
            else
            {
                this.PageHeader.Visible = true;
                txtPageHeader.Text = String.Concat(this.Fields["ProjectName"].Value.ToString(), " - ",
                    Resource.ResourceManager["Reports_strBasicInfo"]);
            }
        }

        private void ReportHeader_Format(object sender, System.EventArgs eArgs)
        {
            this.txtProjectNameHeader.Text = this.Fields["ProjectName"].Value.ToString();
            txtReportTitle.Text = Resource.ResourceManager["Reports_strBasicInfo"];
        }

        private void Detail_Format(object sender, System.EventArgs eArgs)
        {

            decimal area = 0;
            try
            {
                area = (decimal)this.Fields["Area"].Value;
            }
            catch
            { }

            ProjectUsersVector puv = ProjectUserDAL.LoadCollection("ProjectUsersSelByProjectProc", SQLParms.CreateProjectUsersSelByProjectProc(_projectID));
            if (puv.Count > 0)
            {
                string s = "";
                foreach (ProjectUserData pud in puv)
                {
                    s += ", ";
                    UserInfo ui = UsersData.SelectUserByID(pud.UserID);
                    s += ui.FullName.ToString();
                }
                string s1 = s.Substring(2);
                TextBox30.Text = s1;
            }
            else
            {
                TextBox29.Visible = false;
                TextBox30.Visible = false;
            }
            ProjectClientsVector pcv = ProjectClientDAL.LoadCollection("ProjectClientsSelByProjectProc", SQLParms.CreateProjectClientsSelByProjectProc(_projectID, (int)ClientTypes.Client));
            if (pcv.Count > 0)
            {
                string s = txtClient.Text;
                foreach (ProjectClientData pud in pcv)
                {
                    s += ", ";
                    ClientData ui = ClientDAL.Load(pud.ClientID);
                    s += ui.ClientName.ToString();
                }
                //				string s1=s.Substring(2);
                txtClient.Text = s;
            }
            //			else
            //			{
            //				TextBox29.Visible = false;
            //				TextBox30.Visible = false;
            //			}

            if (!this._fullReport) return;

            decimal totalAmountEUR = 0;

            decimal fix = decimal.Parse(System.Configuration.ConfigurationManager.AppSettings["fixedBGNEUR"]);

            decimal rate = 0;
            try
            {
                rate = (decimal)this.Fields["Rate"].Value;
            }
            catch
            { }

            int paymentSchemeID = (int)PaymentSchemes.Area;
            try
            {
                paymentSchemeID = (int)this.Fields["PaymentSchemeID"].Value;
            }
            catch
            { }

            if (paymentSchemeID == (int)PaymentSchemes.Fixed) totalAmountEUR = rate;
            else totalAmountEUR = area * rate;

            this.txtAmountEUR.Text = UIHelpers.FormatDecimal2(totalAmountEUR);
            this.txtAmountBGN.Text = UIHelpers.FormatDecimal2(totalAmountEUR * fix);
        }

        private void PageFooter_Format(object sender, System.EventArgs eArgs)
        {
            lblPageFooter.Text = this.Fields["ProjectName"].Value.ToString();
            lblPage.Text = string.Concat(Resource.ResourceManager["rpt_Page"], " ",
                (this.PageNumber + _startPageNumber - 1).ToString());
        }

		#region ActiveReports Designer generated code
















































        public void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(BasicInfoSubreport));
            this.Detail = new GrapeCity.ActiveReports.SectionReportModel.Detail();
            this.ReportHeader = new GrapeCity.ActiveReports.SectionReportModel.ReportHeader();
            this.ReportFooter = new GrapeCity.ActiveReports.SectionReportModel.ReportFooter();
            this.PageHeader = new GrapeCity.ActiveReports.SectionReportModel.PageHeader();
            this.PageFooter = new GrapeCity.ActiveReports.SectionReportModel.PageFooter();
            this.txtReportTitle = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtProjectNameHeader = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.Line3 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.txtPageHeader = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.Line1 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.txtInvestor = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtAdmAddress = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCode = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtAddress = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtClient = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtArea = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.TextBox11 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtStartDate = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.TextBox12 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtEndDate = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtEndDateContractLbl = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtEndDateContract = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtPSLbl = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtPaymentScheme = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtPriceLbl = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtRate = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtEuroLbl = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtAmountEUR = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtBGNLbl = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtAmountBGN = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.TextBox8 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.TextBox19 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.TextBox20 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.TextBox21 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.TextBox22 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.TextBox23 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtAdminNameLbl = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtAdminName = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.TextBox24 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtBuildingType = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.TextBox25 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.TextBox26 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.TextBox29 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.TextBox30 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.Line11 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.lblPageFooter = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lblPage = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Line4 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            ((System.ComponentModel.ISupportInitialize)(this.txtReportTitle)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtProjectNameHeader)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPageHeader)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtInvestor)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAdmAddress)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCode)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAddress)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtClient)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtArea)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TextBox11)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtStartDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TextBox12)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtEndDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtEndDateContractLbl)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtEndDateContract)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPSLbl)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPaymentScheme)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPriceLbl)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtRate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtEuroLbl)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAmountEUR)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtBGNLbl)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAmountBGN)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TextBox8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TextBox19)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TextBox20)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TextBox21)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TextBox22)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TextBox23)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAdminNameLbl)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAdminName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TextBox24)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtBuildingType)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TextBox25)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TextBox26)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TextBox29)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TextBox30)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblPageFooter)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblPage)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            // 
            // Detail
            // 
            this.Detail.CanShrink = true;
            this.Detail.ColumnSpacing = 0F;
            this.Detail.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
						this.txtInvestor,
						this.txtAdmAddress,
						this.txtCode,
						this.txtAddress,
						this.txtClient,
						this.txtArea,
						this.TextBox11,
						this.txtStartDate,
						this.TextBox12,
						this.txtEndDate,
						this.txtEndDateContractLbl,
						this.txtEndDateContract,
						this.txtPSLbl,
						this.txtPaymentScheme,
						this.txtPriceLbl,
						this.txtRate,
						this.txtEuroLbl,
						this.txtAmountEUR,
						this.txtBGNLbl,
						this.txtAmountBGN,
						this.TextBox8,
						this.TextBox19,
						this.TextBox20,
						this.TextBox21,
						this.TextBox22,
						this.TextBox23,
						this.txtAdminNameLbl,
						this.txtAdminName,
						this.TextBox24,
						this.txtBuildingType,
						this.TextBox25,
						this.TextBox26,
						this.TextBox29,
						this.TextBox30});
            this.Detail.Height = 4.1875F;
            this.Detail.Name = "Detail";
            // 
            // ReportHeader
            // 
            this.ReportHeader.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
						this.txtReportTitle,
						this.txtProjectNameHeader,
						this.Line3});
            this.ReportHeader.Height = 0.7868056F;
            this.ReportHeader.Name = "ReportHeader";
            // 
            // ReportFooter
            // 
            this.ReportFooter.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
						this.Line4});
            this.ReportFooter.Height = 0.1145833F;
            this.ReportFooter.Name = "ReportFooter";
            // 
            // PageHeader
            // 
            this.PageHeader.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
						this.txtPageHeader,
						this.Line1});
            this.PageHeader.Height = 0.5902778F;
            this.PageHeader.Name = "PageHeader";
            // 
            // PageFooter
            // 
            this.PageFooter.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
						this.Line11,
						this.lblPageFooter,
						this.lblPage});
            this.PageFooter.Height = 0.2201389F;
            this.PageFooter.Name = "PageFooter";
            // 
            // txtReportTitle
            // 
            this.txtReportTitle.Height = 0.2755906F;
            this.txtReportTitle.Left = 0F;
            this.txtReportTitle.Name = "txtReportTitle";
            this.txtReportTitle.Style = "font-size: 14pt; font-weight: bold";
            this.txtReportTitle.Text = "???? ??????????";
            this.txtReportTitle.Top = 0F;
            this.txtReportTitle.Width = 6.299212F;
            // 
            // txtProjectNameHeader
            // 
            this.txtProjectNameHeader.Height = 0.2362205F;
            this.txtProjectNameHeader.Left = 0F;
            this.txtProjectNameHeader.Name = "txtProjectNameHeader";
            this.txtProjectNameHeader.Style = "font-size: 12pt; font-weight: bold";
            this.txtProjectNameHeader.Text = "txtProjectNameHeader";
            this.txtProjectNameHeader.Top = 0.2362205F;
            this.txtProjectNameHeader.Width = 6.299212F;
            // 
            // Line3
            // 
            this.Line3.Height = 0F;
            this.Line3.Left = 0F;
            this.Line3.LineWeight = 2F;
            this.Line3.Name = "Line3";
            this.Line3.Top = 0.551181F;
            this.Line3.Width = 7.086611F;
            this.Line3.X1 = 0F;
            this.Line3.X2 = 7.086611F;
            this.Line3.Y1 = 0.551181F;
            this.Line3.Y2 = 0.551181F;
            // 
            // txtPageHeader
            // 
            this.txtPageHeader.Height = 0.2362205F;
            this.txtPageHeader.Left = 0F;
            this.txtPageHeader.Name = "txtPageHeader";
            this.txtPageHeader.Style = "font-size: 12pt; font-weight: bold";
            this.txtPageHeader.Text = "???? ??????????";
            this.txtPageHeader.Top = 0F;
            this.txtPageHeader.Width = 7.086611F;
            // 
            // Line1
            // 
            this.Line1.Height = 0F;
            this.Line1.Left = 0F;
            this.Line1.LineWeight = 2F;
            this.Line1.Name = "Line1";
            this.Line1.Top = 0.2362205F;
            this.Line1.Width = 7.086611F;
            this.Line1.X1 = 0F;
            this.Line1.X2 = 7.086611F;
            this.Line1.Y1 = 0.2362205F;
            this.Line1.Y2 = 0.2362205F;
            // 
            // txtInvestor
            // 
            this.txtInvestor.DataField = "Investor";
            this.txtInvestor.Height = 0.2F;
            this.txtInvestor.Left = 1.771653F;
            this.txtInvestor.Name = "txtInvestor";
            this.txtInvestor.Style = "font-weight: bold";
            this.txtInvestor.Text = "txtInvestor";
            this.txtInvestor.Top = 0F;
            this.txtInvestor.Width = 5.31496F;
            // 
            // txtAdmAddress
            // 
            this.txtAdmAddress.DataField = "AddInfo";
            this.txtAdmAddress.Height = 0.2F;
            this.txtAdmAddress.Left = 1.771653F;
            this.txtAdmAddress.Name = "txtAdmAddress";
            this.txtAdmAddress.Style = "font-weight: bold";
            this.txtAdmAddress.Text = "txtAdmAddress";
            this.txtAdmAddress.Top = 0.8267716F;
            this.txtAdmAddress.Width = 5.31496F;
            // 
            // txtCode
            // 
            this.txtCode.DataField = "ProjectCode";
            this.txtCode.Height = 0.2F;
            this.txtCode.Left = 1.771653F;
            this.txtCode.Name = "txtCode";
            this.txtCode.Style = "font-weight: bold";
            this.txtCode.Text = "txtCode";
            this.txtCode.Top = 0.2755906F;
            this.txtCode.Width = 5.31496F;
            // 
            // txtAddress
            // 
            this.txtAddress.DataField = "Address";
            this.txtAddress.Height = 0.2F;
            this.txtAddress.Left = 1.771653F;
            this.txtAddress.Name = "txtAddress";
            this.txtAddress.Style = "font-weight: bold";
            this.txtAddress.Text = "txtAddress";
            this.txtAddress.Top = 2.204724F;
            this.txtAddress.Width = 5.31496F;
            // 
            // txtClient
            // 
            this.txtClient.DataField = "ClientName";
            this.txtClient.Height = 0.2F;
            this.txtClient.Left = 1.771653F;
            this.txtClient.Name = "txtClient";
            this.txtClient.Style = "font-weight: bold";
            this.txtClient.Text = "txtClient";
            this.txtClient.Top = 2.480315F;
            this.txtClient.Width = 5.31496F;
            // 
            // txtArea
            // 
            this.txtArea.DataField = "Area";
            this.txtArea.Height = 0.2F;
            this.txtArea.Left = 1.771653F;
            this.txtArea.Name = "txtArea";
            this.txtArea.Style = "font-weight: bold";
            this.txtArea.Text = "txtArea";
            this.txtArea.Top = 1.102362F;
            this.txtArea.Width = 1.181102F;
            // 
            // TextBox11
            // 
            this.TextBox11.Height = 0.2F;
            this.TextBox11.Left = 0F;
            this.TextBox11.Name = "TextBox11";
            this.TextBox11.Text = "??????? ????:";
            this.TextBox11.Top = 1.377953F;
            this.TextBox11.Width = 1.771653F;
            // 
            // txtStartDate
            // 
            this.txtStartDate.DataField = "StartDate";
            this.txtStartDate.Height = 0.2F;
            this.txtStartDate.Left = 1.771653F;
            this.txtStartDate.Name = "txtStartDate";
            this.txtStartDate.OutputFormat = resources.GetString("txtStartDate.OutputFormat");
            this.txtStartDate.Style = "font-weight: bold";
            this.txtStartDate.Text = "txtStartDate";
            this.txtStartDate.Top = 1.377953F;
            this.txtStartDate.Width = 1.181102F;
            // 
            // TextBox12
            // 
            this.TextBox12.Height = 0.2F;
            this.TextBox12.Left = 2.952756F;
            this.TextBox12.Name = "TextBox12";
            this.TextBox12.Text = "?????? ???? ?? ??????:";
            this.TextBox12.Top = 1.653543F;
            this.TextBox12.Width = 1.771653F;
            // 
            // txtEndDate
            // 
            this.txtEndDate.DataField = "EndDate";
            this.txtEndDate.Height = 0.2F;
            this.txtEndDate.Left = 4.72441F;
            this.txtEndDate.Name = "txtEndDate";
            this.txtEndDate.OutputFormat = resources.GetString("txtEndDate.OutputFormat");
            this.txtEndDate.Style = "font-weight: bold";
            this.txtEndDate.Text = "txtEndDate";
            this.txtEndDate.Top = 1.653543F;
            this.txtEndDate.Width = 1.181102F;
            // 
            // txtEndDateContractLbl
            // 
            this.txtEndDateContractLbl.Height = 0.2F;
            this.txtEndDateContractLbl.Left = 2.952756F;
            this.txtEndDateContractLbl.Name = "txtEndDateContractLbl";
            this.txtEndDateContractLbl.Text = "?????? ???? ?? ???????:";
            this.txtEndDateContractLbl.Top = 1.377953F;
            this.txtEndDateContractLbl.Width = 1.771653F;
            // 
            // txtEndDateContract
            // 
            this.txtEndDateContract.DataField = "EndDateContract";
            this.txtEndDateContract.Height = 0.2F;
            this.txtEndDateContract.Left = 4.72441F;
            this.txtEndDateContract.Name = "txtEndDateContract";
            this.txtEndDateContract.OutputFormat = resources.GetString("txtEndDateContract.OutputFormat");
            this.txtEndDateContract.Style = "font-weight: bold";
            this.txtEndDateContract.Text = "txtEndDateContract";
            this.txtEndDateContract.Top = 1.377953F;
            this.txtEndDateContract.Width = 1.181102F;
            // 
            // txtPSLbl
            // 
            this.txtPSLbl.Height = 0.2F;
            this.txtPSLbl.Left = 0F;
            this.txtPSLbl.Name = "txtPSLbl";
            this.txtPSLbl.Text = "????? ?? ???????:";
            this.txtPSLbl.Top = 3.375F;
            this.txtPSLbl.Width = 1.771653F;
            // 
            // txtPaymentScheme
            // 
            this.txtPaymentScheme.DataField = "PaymentScheme";
            this.txtPaymentScheme.Height = 0.2F;
            this.txtPaymentScheme.Left = 1.771653F;
            this.txtPaymentScheme.Name = "txtPaymentScheme";
            this.txtPaymentScheme.Style = "font-weight: bold";
            this.txtPaymentScheme.Text = "txtPaymentScheme";
            this.txtPaymentScheme.Top = 3.375F;
            this.txtPaymentScheme.Width = 2.952756F;
            // 
            // txtPriceLbl
            // 
            this.txtPriceLbl.Height = 0.2F;
            this.txtPriceLbl.Left = 0F;
            this.txtPriceLbl.Name = "txtPriceLbl";
            this.txtPriceLbl.Text = "?????????? ????:";
            this.txtPriceLbl.Top = 3.650591F;
            this.txtPriceLbl.Width = 1.771653F;
            // 
            // txtRate
            // 
            this.txtRate.DataField = "Rate";
            this.txtRate.Height = 0.2F;
            this.txtRate.Left = 1.771653F;
            this.txtRate.Name = "txtRate";
            this.txtRate.OutputFormat = resources.GetString("txtRate.OutputFormat");
            this.txtRate.Style = "font-weight: bold";
            this.txtRate.Text = "txtRate";
            this.txtRate.Top = 3.650591F;
            this.txtRate.Width = 1.181102F;
            // 
            // txtEuroLbl
            // 
            this.txtEuroLbl.Height = 0.2F;
            this.txtEuroLbl.Left = 0F;
            this.txtEuroLbl.Name = "txtEuroLbl";
            this.txtEuroLbl.Text = "???? ????(EUR):";
            this.txtEuroLbl.Top = 3.926181F;
            this.txtEuroLbl.Width = 1.771653F;
            // 
            // txtAmountEUR
            // 
            this.txtAmountEUR.Height = 0.2F;
            this.txtAmountEUR.Left = 1.771653F;
            this.txtAmountEUR.Name = "txtAmountEUR";
            this.txtAmountEUR.Style = "font-weight: bold";
            this.txtAmountEUR.Text = "TextBox16";
            this.txtAmountEUR.Top = 3.926181F;
            this.txtAmountEUR.Width = 1.181102F;
            // 
            // txtBGNLbl
            // 
            this.txtBGNLbl.Height = 0.2F;
            this.txtBGNLbl.Left = 2.952756F;
            this.txtBGNLbl.Name = "txtBGNLbl";
            this.txtBGNLbl.Text = "???? ????(BGN):";
            this.txtBGNLbl.Top = 3.926181F;
            this.txtBGNLbl.Width = 1.771653F;
            // 
            // txtAmountBGN
            // 
            this.txtAmountBGN.Height = 0.2F;
            this.txtAmountBGN.Left = 4.72441F;
            this.txtAmountBGN.Name = "txtAmountBGN";
            this.txtAmountBGN.Style = "font-weight: bold";
            this.txtAmountBGN.Text = "txtAmountBGN";
            this.txtAmountBGN.Top = 3.926181F;
            this.txtAmountBGN.Width = 1.181102F;
            // 
            // TextBox8
            // 
            this.TextBox8.Height = 0.2F;
            this.TextBox8.Left = 0F;
            this.TextBox8.Name = "TextBox8";
            this.TextBox8.Text = "???????? ???:";
            this.TextBox8.Top = 1.102362F;
            this.TextBox8.Width = 1.771653F;
            // 
            // TextBox19
            // 
            this.TextBox19.Height = 0.2F;
            this.TextBox19.Left = 0F;
            this.TextBox19.Name = "TextBox19";
            this.TextBox19.Text = "??????:";
            this.TextBox19.Top = 2.480315F;
            this.TextBox19.Width = 1.771653F;
            // 
            // TextBox20
            // 
            this.TextBox20.Height = 0.2F;
            this.TextBox20.Left = 0F;
            this.TextBox20.Name = "TextBox20";
            this.TextBox20.Text = "????? - ??. ?:";
            this.TextBox20.Top = 0.8267716F;
            this.TextBox20.Width = 1.771653F;
            // 
            // TextBox21
            // 
            this.TextBox21.Height = 0.2F;
            this.TextBox21.Left = 0F;
            this.TextBox21.Name = "TextBox21";
            this.TextBox21.Text = "?????:";
            this.TextBox21.Top = 2.204724F;
            this.TextBox21.Width = 1.771653F;
            // 
            // TextBox22
            // 
            this.TextBox22.Height = 0.2F;
            this.TextBox22.Left = 0F;
            this.TextBox22.Name = "TextBox22";
            this.TextBox22.Text = "??? ?? ???????:";
            this.TextBox22.Top = 0.2755906F;
            this.TextBox22.Width = 1.771653F;
            // 
            // TextBox23
            // 
            this.TextBox23.Height = 0.2F;
            this.TextBox23.Left = 0F;
            this.TextBox23.Name = "TextBox23";
            this.TextBox23.Text = "??????????:";
            this.TextBox23.Top = 0.01197502F;
            this.TextBox23.Width = 1.771653F;
            // 
            // txtAdminNameLbl
            // 
            this.txtAdminNameLbl.Height = 0.2F;
            this.txtAdminNameLbl.Left = 0F;
            this.txtAdminNameLbl.Name = "txtAdminNameLbl";
            this.txtAdminNameLbl.Text = "??????????????? ???:";
            this.txtAdminNameLbl.Top = 0.551181F;
            this.txtAdminNameLbl.Width = 1.771653F;
            // 
            // txtAdminName
            // 
            this.txtAdminName.DataField = "AdministrativeName";
            this.txtAdminName.Height = 0.2F;
            this.txtAdminName.Left = 1.771653F;
            this.txtAdminName.Name = "txtAdminName";
            this.txtAdminName.Style = "font-weight: bold";
            this.txtAdminName.Text = "txtAdminName";
            this.txtAdminName.Top = 0.551181F;
            this.txtAdminName.Width = 5.31496F;
            // 
            // TextBox24
            // 
            this.TextBox24.Height = 0.2F;
            this.TextBox24.Left = 0F;
            this.TextBox24.Name = "TextBox24";
            this.TextBox24.Text = "??? ??????:";
            this.TextBox24.Top = 1.929134F;
            this.TextBox24.Width = 1.771653F;
            // 
            // txtBuildingType
            // 
            this.txtBuildingType.DataField = "BuildingType";
            this.txtBuildingType.Height = 0.2F;
            this.txtBuildingType.Left = 1.771653F;
            this.txtBuildingType.Name = "txtBuildingType";
            this.txtBuildingType.Style = "font-weight: bold";
            this.txtBuildingType.Text = "txtBuildingType";
            this.txtBuildingType.Top = 1.929134F;
            this.txtBuildingType.Width = 5.31496F;
            // 
            // TextBox25
            // 
            this.TextBox25.Height = 0.2F;
            this.TextBox25.Left = 0F;
            this.TextBox25.Name = "TextBox25";
            this.TextBox25.Text = "???????????:";
            this.TextBox25.Top = 2.755905F;
            this.TextBox25.Width = 1.771653F;
            // 
            // TextBox26
            // 
            this.TextBox26.DataField = "ProjectManager";
            this.TextBox26.Height = 0.2F;
            this.TextBox26.Left = 1.771653F;
            this.TextBox26.Name = "TextBox26";
            this.TextBox26.Style = "font-weight: bold";
            this.TextBox26.Text = "TextBox26";
            this.TextBox26.Top = 2.755905F;
            this.TextBox26.Width = 5.31496F;
            // 
            // TextBox29
            // 
            this.TextBox29.Height = 0.2F;
            this.TextBox29.Left = 0F;
            this.TextBox29.Name = "TextBox29";
            this.TextBox29.Text = "????:";
            this.TextBox29.Top = 3.0625F;
            this.TextBox29.Width = 1.771653F;
            // 
            // TextBox30
            // 
            this.TextBox30.Height = 0.2F;
            this.TextBox30.Left = 1.771653F;
            this.TextBox30.Name = "TextBox30";
            this.TextBox30.Style = "font-weight: bold";
            this.TextBox30.Top = 3.0625F;
            this.TextBox30.Width = 5.31496F;
            // 
            // Line11
            // 
            this.Line11.Height = 0F;
            this.Line11.Left = 0.006944444F;
            this.Line11.LineWeight = 1F;
            this.Line11.Name = "Line11";
            this.Line11.Top = 0.006944444F;
            this.Line11.Width = 7.086611F;
            this.Line11.X1 = 0.006944444F;
            this.Line11.X2 = 7.093555F;
            this.Line11.Y1 = 0.006944444F;
            this.Line11.Y2 = 0.006944444F;
            // 
            // lblPageFooter
            // 
            this.lblPageFooter.Height = 0.1968504F;
            this.lblPageFooter.HyperLink = null;
            this.lblPageFooter.Left = 0F;
            this.lblPageFooter.Name = "lblPageFooter";
            this.lblPageFooter.Style = "font-size: 8pt; font-weight: bold; text-align: left";
            this.lblPageFooter.Text = "lblPageFooter";
            this.lblPageFooter.Top = 0.02460628F;
            this.lblPageFooter.Width = 6.299212F;
            // 
            // lblPage
            // 
            this.lblPage.Height = 0.1968504F;
            this.lblPage.HyperLink = null;
            this.lblPage.Left = 6.299212F;
            this.lblPage.Name = "lblPage";
            this.lblPage.Style = "font-size: 8pt; font-weight: bold; text-align: right";
            this.lblPage.Text = "lblPage";
            this.lblPage.Top = 0.02460628F;
            this.lblPage.Width = 0.7874014F;
            // 
            // Line4
            // 
            this.Line4.Height = 0F;
            this.Line4.Left = 0F;
            this.Line4.LineWeight = 2F;
            this.Line4.Name = "Line4";
            this.Line4.Top = 0.0384952F;
            this.Line4.Width = 7.086611F;
            this.Line4.X1 = 0F;
            this.Line4.X2 = 7.086611F;
            this.Line4.Y1 = 0.0384952F;
            this.Line4.Y2 = 0.0384952F;
            // 
            // ActiveReport1
            // 
            this.MasterReport = false;
            this.PageSettings.DefaultPaperSize = false;
            this.PageSettings.Margins.Bottom = 0.7875F;
            this.PageSettings.Margins.Left = 1.377778F;
            this.PageSettings.Margins.Right = 0.39375F;
            this.PageSettings.Margins.Top = 0.7875F;
            this.PageSettings.PaperHeight = 11.69306F;
            this.PageSettings.PaperKind = System.Drawing.Printing.PaperKind.A4;
            this.PageSettings.PaperWidth = 8.268056F;
            this.PrintWidth = 7.086611F;
            this.Sections.Add(this.ReportHeader);
            this.Sections.Add(this.PageHeader);
            this.Sections.Add(this.Detail);
            this.Sections.Add(this.PageFooter);
            this.Sections.Add(this.ReportFooter);
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule(resources.GetString("$this.StyleSheet"), "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: inherit; font-style: inherit; font-variant: inherit; font-weight: bo" +
                        "ld; font-size: 16pt; font-size-adjust: inherit; font-stretch: inherit", "Heading1", "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: Times New Roman; font-style: italic; font-variant: inherit; font-wei" +
                        "ght: bold; font-size: 14pt; font-size-adjust: inherit; font-stretch: inherit", "Heading2", "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: inherit; font-style: inherit; font-variant: inherit; font-weight: bo" +
                        "ld; font-size: 13pt; font-size-adjust: inherit; font-stretch: inherit", "Heading3", "Normal"));
            ((System.ComponentModel.ISupportInitialize)(this.txtReportTitle)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtProjectNameHeader)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPageHeader)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtInvestor)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAdmAddress)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCode)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAddress)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtClient)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtArea)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TextBox11)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtStartDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TextBox12)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtEndDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtEndDateContractLbl)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtEndDateContract)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPSLbl)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPaymentScheme)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPriceLbl)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtRate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtEuroLbl)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAmountEUR)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtBGNLbl)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAmountBGN)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TextBox8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TextBox19)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TextBox20)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TextBox21)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TextBox22)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TextBox23)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAdminNameLbl)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAdminName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TextBox24)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtBuildingType)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TextBox25)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TextBox26)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TextBox29)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TextBox30)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblPageFooter)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblPage)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();

            // Attach Report Events
            this.ReportStart += new System.EventHandler(this.BasicInfoSubreport_ReportStart);
            this.PageHeader.Format += new System.EventHandler(this.PageHeader_Format);
            this.ReportHeader.Format += new System.EventHandler(this.ReportHeader_Format);
            this.Detail.Format += new System.EventHandler(this.Detail_Format);
            this.PageFooter.Format += new System.EventHandler(this.PageFooter_Format);
        }

		#endregion

        private ReportHeader ReportHeader;
        private TextBox txtReportTitle;
        private TextBox txtProjectNameHeader;
        private Line Line3;
        private PageHeader PageHeader;
        private TextBox txtPageHeader;
        private Line Line1;
        private Detail Detail;
        private TextBox txtInvestor;
        private TextBox txtAdmAddress;
        private TextBox txtCode;
        private TextBox txtAddress;
        private TextBox txtClient;
        private TextBox txtArea;
        private TextBox TextBox11;
        private TextBox txtStartDate;
        private TextBox TextBox12;
        private TextBox txtEndDate;
        private TextBox txtEndDateContractLbl;
        private TextBox txtEndDateContract;
        private TextBox txtPSLbl;
        private TextBox txtPaymentScheme;
        private TextBox txtPriceLbl;
        private TextBox txtRate;
        private TextBox txtEuroLbl;
        private TextBox txtAmountEUR;
        private TextBox txtBGNLbl;
        private TextBox txtAmountBGN;
        private TextBox TextBox8;
        private TextBox TextBox19;
        private TextBox TextBox20;
        private TextBox TextBox21;
        private TextBox TextBox22;
        private TextBox TextBox23;
        private TextBox txtAdminNameLbl;
        private TextBox txtAdminName;
        private TextBox TextBox24;
        private TextBox txtBuildingType;
        private TextBox TextBox25;
        private TextBox TextBox26;
        private TextBox TextBox29;
        private TextBox TextBox30;
        private PageFooter PageFooter;
        private Line Line11;
        private Label lblPageFooter;
        private Label lblPage;
        private ReportFooter ReportFooter;
        private Line Line4;
	}
}
