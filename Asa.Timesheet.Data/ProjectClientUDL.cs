using System;
using System.Data;
using System.Collections;
using System.Data.SqlClient;

namespace Asa.Timesheet.Data
{
	/// <summary>
	/// Summary description for ProjectClientUDL.
	/// </summary>
	public class ProjectClientUDL
	{
		private static SqlCommand CreateProjectClientsDelByProjectProc (int ProjectID)
		{
			SqlCommand cmd = new SqlCommand();
			cmd.CommandType = System.Data.CommandType.StoredProcedure;
			cmd.CommandText = "ProjectClientsDelByProjectProc";
			cmd.Parameters.Add(new System.Data.SqlClient.SqlParameter("@ProjectID", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.Input, true, 10, 0, "ProjectID", DataRowVersion.Current, null));
			if(ProjectID>0)
				cmd.Parameters["@ProjectID"].Value = ProjectID;

			return cmd;
		}

		public static int ExecuteProjectClientsDelByProjectProc(int ProjectID)
		{
			int RetObj;
			System.Data.SqlClient.SqlCommand cmd;
			cmd = CreateProjectClientsDelByProjectProc(ProjectID);
			SqlConnection cnDAL = new SqlConnection();

			try
			{
				cnDAL =  CommonDAL.OpenConnection();
				cmd.Connection = cnDAL;
				RetObj = cmd.ExecuteNonQuery();
				return RetObj;
		
			}
			catch(SqlException ex)
			{

				throw new ApplicationException("ProjectClientsDelByProjectProc",ex);
			}

			finally
			{
				CommonDAL.CloseConnection(cnDAL);
			}
		}
		
	}
}