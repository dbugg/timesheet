using System;
using System.Data;
using System.Data.SqlClient;
using Microsoft.ApplicationBlocks.Data;
using System.Configuration;

namespace Asa.Timesheet.Data.Reports
{
	/// <summary>
	/// Summary description for AbsenceReport.
	/// </summary>
	public class AbsenceReportData
	{
    public static DataSet SelectAbsenceReportDS(int userID, int projectID, DateTime startDate, DateTime endDate, bool IsActive)
    {
      string spName = "ReportsSelectAbsence";

      #region Set parameters

      SqlParameter[] parms = SqlHelperParameterCache.GetSpParameterSet(DBManager.GetConnectionString(), spName);
      
      parms[0].Value = userID == 0 ? null : (object)userID;
      parms[1].Value = projectID == 0 ? null : (object)projectID;
      parms[2].Value = startDate;
      parms[3].Value = endDate;	
	  parms[4].Value = IsActive;
      #endregion

      DataSet ds = SqlHelper.ExecuteDataset(DBManager.GetConnectionString(), CommandType.StoredProcedure, spName, parms);
      
      ds.Tables[0].TableName = "Users";
      ds.Tables[1].TableName = "WorkDays";

      ds.Tables.Add(new DataTable("Projects"));
      ds.Tables["Projects"].Columns.Add(new DataColumn("ProjectID", System.Type.GetType("System.Int32")));
      ds.Tables["Projects"].Columns.Add(new DataColumn("Name", System.Type.GetType("System.String")));
      ds.Tables["Projects"].Columns.Add(new DataColumn("DaysString", System.Type.GetType("System.String")));
      ds.Tables["Projects"].Columns.Add(new DataColumn("TimeInMinutes", System.Type.GetType("System.Int32")));
      ds.Tables["Projects"].Columns.Add(new DataColumn("DaysCount", System.Type.GetType("System.Int32")));

      DataRow dr = ds.Tables["Projects"].NewRow();
      dr["ProjectID"] = int.Parse(ConfigurationSettings.AppSettings["Platen"]) ; dr["Name"] = Resource.ResourceManager["strPlatenOtpusk"]; ds.Tables["Projects"].Rows.Add(dr);
      dr = ds.Tables["Projects"].NewRow();
      dr["ProjectID"] = int.Parse(ConfigurationSettings.AppSettings["Neplaten"]); dr["Name"] = Resource.ResourceManager["strNeplatenOtpusk"];ds.Tables["Projects"].Rows.Add(dr);
      dr = ds.Tables["Projects"].NewRow();
      dr["ProjectID"] =int.Parse(ConfigurationSettings.AppSettings["Bolen"]); dr["Name"] = Resource.ResourceManager["strBolnichni"];ds.Tables["Projects"].Rows.Add(dr);
      
      return ds;
    }
	
		public static DataSet SelectAbsenceReportDSUnactiveUsers(int userID, int projectID, DateTime startDate, DateTime endDate)
		{
			string spName = "ReportsSelectAbsenceUnactiveUsers";

			#region Set parameters

			SqlParameter[] parms = SqlHelperParameterCache.GetSpParameterSet(DBManager.GetConnectionString(), spName);
      
			parms[0].Value = userID == 0 ? null : (object)userID;
			parms[1].Value = projectID == 0 ? null : (object)projectID;
			parms[2].Value = startDate;
			parms[3].Value = endDate;
		
			#endregion

			DataSet ds = SqlHelper.ExecuteDataset(DBManager.GetConnectionString(), CommandType.StoredProcedure, spName, parms);
      
			ds.Tables[0].TableName = "Users";
			ds.Tables[1].TableName = "WorkDays";

			ds.Tables.Add(new DataTable("Projects"));
			ds.Tables["Projects"].Columns.Add(new DataColumn("ProjectID", System.Type.GetType("System.Int32")));
			ds.Tables["Projects"].Columns.Add(new DataColumn("Name", System.Type.GetType("System.String")));
			ds.Tables["Projects"].Columns.Add(new DataColumn("DaysString", System.Type.GetType("System.String")));
			ds.Tables["Projects"].Columns.Add(new DataColumn("TimeInMinutes", System.Type.GetType("System.Int32")));
			ds.Tables["Projects"].Columns.Add(new DataColumn("DaysCount", System.Type.GetType("System.Int32")));

			DataRow dr = ds.Tables["Projects"].NewRow();
			dr["ProjectID"] = int.Parse(ConfigurationSettings.AppSettings["Platen"]) ; dr["Name"] = Resource.ResourceManager["strPlatenOtpusk"]; ds.Tables["Projects"].Rows.Add(dr);
			dr = ds.Tables["Projects"].NewRow();
			dr["ProjectID"] = int.Parse(ConfigurationSettings.AppSettings["Neplaten"]); dr["Name"] = Resource.ResourceManager["strNeplatenOtpusk"];ds.Tables["Projects"].Rows.Add(dr);
			dr = ds.Tables["Projects"].NewRow();
			dr["ProjectID"] =int.Parse(ConfigurationSettings.AppSettings["Bolen"]); dr["Name"] = Resource.ResourceManager["strBolnichni"];ds.Tables["Projects"].Rows.Add(dr);
      
			return ds;
		}
    public static DataTable GetDaysStringForUser(DataRow userRow) 
    { 
      DataSet dsAbsence = userRow.Table.DataSet;

      DataTable dtTemp = dsAbsence.Tables["Projects"].Copy();
      int userID = (int)userRow["UserID"];

      for (int i=dtTemp.Rows.Count-1; i>=0; i--)
      {
        DataRow rowProject = dtTemp.Rows[i];
        string filter = "UserID = "+userID.ToString()+" AND ProjectID = "+rowProject["ProjectID"].ToString();
        DataRow[] rows = dsAbsence.Tables["WorkDays"].Select(filter);

        int daysCount = rows.Length;
        int workMinutes = 0;

        System.Text.StringBuilder sbDays = new System.Text.StringBuilder();

        if (rows.Length>0)
        {
          DateTime startDate = ((DateTime)rows[0]["WorkDate"]).Date;
          DateTime endDate = ((DateTime)rows[0]["WorkDate"]).Date;

          DateTime dtCurrent = ((DateTime)rows[0]["WorkDate"]).Date;

          for (int j=0; j<rows.Length; j++)
          {
           
            DataRow row = rows[j];

            workMinutes+=(int)row["TimeInMinutes"];
          
            dtCurrent = ((DateTime)row["WorkDate"]).Date;

            if (dtCurrent==endDate) continue;

            // sbDays.Append(dtCurrent.ToString("dd.MM.yy")+", ");

            if (dtCurrent.Date.AddDays(-1)!=endDate)
            {
              if (startDate==endDate) sbDays.Append(startDate.ToString("dd.MM.yy")+", ");
              else sbDays.Append(startDate.ToString("dd.MM.yy")+" - "+ endDate.ToString("dd.MM.yy")+", ");

              startDate = endDate = dtCurrent;
            }
            else
            {
              endDate = dtCurrent;
            }
          }

          if (startDate==endDate)
          {
            sbDays.Append(dtCurrent.ToString("dd.MM.yy"));
          }
          else
          {
            sbDays.Append(startDate.ToString("dd.MM.yy")+" - "+ dtCurrent.ToString("dd.MM.yy"));
          }
        }

        if (sbDays.Length>0)
        {
          rowProject["DaysString"] = sbDays.ToString();
          rowProject["DaysCount"] = daysCount;
          rowProject["TimeInMinutes"] = workMinutes;
        }

        else dtTemp.Rows.Remove(rowProject);
      }

      return dtTemp;
      

    }
	}
}
