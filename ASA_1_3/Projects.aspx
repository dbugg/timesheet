﻿<%@ Register TagPrefix="radg" Namespace="Telerik.WebControls" Assembly="RadGrid" %>
<%@ Register TagPrefix="uc1" TagName="PageHeader" Src="UserControls/PageHeader.ascx" %>
<%@ Page language="c#" Codebehind="Projects.aspx.cs" AutoEventWireup="false" Inherits="Asa.Timesheet.WebPages.Projects" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
	<head runat="server">
		<title>Projects</title>
		<script language="javascript"> 
function search() { 
     if ( event.keyCode ==  13 ) { 
     if(Form1.btnSearch!=null)
         Form1.btnSearch.click(); 
        event.returnValue=false; 
    } 



} 



		</script>
		<meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
		<meta content="C#" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<LINK href="styles/timesheet.css" type="text/css" rel="stylesheet">
	</HEAD>
	<body bottomMargin="0" leftMargin="0" topMargin="0" rightMargin="0">
		<form id="Form1" method="post" runat="server">
			<table height="100%" cellSpacing="0" cellPadding="0" width="100%" border="0">
				<tr height="1">
					<td><uc1:pageheader id="header" runat="server"></uc1:pageheader></td>
				</tr>
				<tr>
					<td>
						<table height="100%" cellSpacing="0" cellPadding="0" width="100%" border="0">
							<tr>
								<td class="menuLeft"><asp:placeholder id="menuHolder" runat="server"></asp:placeholder></td>
								
								<td style="PADDING-RIGHT: 5px; PADDING-LEFT: 5px; PADDING-TOP: 3px" vAlign="top" noWrap>
									<P><br>
										<asp:label id="lblError" runat="server" CssClass="ErrorLabel" EnableViewState="False" ForeColor="Red"></asp:label><asp:label id="lblInfo" runat="server" CssClass="InfoLabel" EnableViewState="False"></asp:label><BR>
										<TABLE id="Table1" style="WIDTH: 856px; HEIGHT: 44px" cellSpacing="1" cellPadding="1" width="856"
											border="0">
											<TR>
												<td style="WIDTH: 121px"><asp:label id="lblName" runat="server" CssClass="enterDataLabel" EnableViewState="False" Font-Bold="True"
														Width="112px">Име на проект:</asp:label></TD>
												<td style="WIDTH: 159px"><asp:textbox id="txtProject" runat="server" CssClass="enterDataBox" Width="165px" MaxLength="30"
														onkeydown="search()"></asp:textbox></TD>
												<td style="WIDTH: 372px"><asp:dropdownlist id="ddlSearch" runat="server" CssClass="enterDataBox" Width="180px" AutoPostBack="True"></asp:dropdownlist><asp:dropdownlist id="ddlBuildingTypes" runat="server" CssClass="enterDataBox" Width="186px" AutoPostBack="True"></asp:dropdownlist></TD>
												<td><asp:button id="btnSearch" runat="server" CssClass="ActionButton" Text="Търси" Width="82px"></asp:button>&nbsp;
													<asp:button id="btnClear" runat="server" CssClass="ActionButton" Text="Изчисти" Width="82px"></asp:button></TD>
											</TR>
										</TABLE>
									</P>
									<asp:panel id="grid" 
										runat="server" Width="98%" Height="450px">
										<asp:datagrid id="grdProjects" runat="server" CssClass="Grid" Width="100%" AutoGenerateColumns="False"
											CellPadding="4" PageSize="2" AllowSorting="True">
											<ItemStyle CssClass="GridItem"></ItemStyle>
											<HeaderStyle Wrap="False" CssClass="GridHeader"></HeaderStyle>
											<Columns>
												<asp:TemplateColumn HeaderText="#">
													<ItemStyle Width="10px" ForeColor="DimGray"></ItemStyle>
													<ItemTemplate>
														<asp:Label CssClass="menuTable" runat="server" Text='<%# (int)DataBinder.Eval(Container, "ItemIndex")+1 %>' ID="Label1" NAME="Label1">
														</asp:Label>
													</ItemTemplate>
												</asp:TemplateColumn>
												<asp:TemplateColumn SortExpression="Name" HeaderText="Кратко име(само на латиница)" ItemStyle-Width="20%">
													<ItemTemplate>
														<asp:LinkButton id="lnk" runat="server" CssClass="menuTable"  CommandName="Edit">
															<span Class="menuTable">
																<%# DataBinder.Eval(Container, "DataItem.ProjectName") %>
															</span>
														</asp:LinkButton>
													</ItemTemplate>
												</asp:TemplateColumn>
												<asp:TemplateColumn HeaderText="Код" SortExpression="Code">
													<ItemStyle Width="10px"></ItemStyle>
													<ItemTemplate>
														<asp:Label ForeColor='<%# GetForeColor(DataBinder.Eval(Container, "DataItem.ProjectColor") )%>' BackColor='<%# GetColor(DataBinder.Eval(Container, "DataItem.ProjectColor") )%>' CssClass="menuTable" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.ProjectCode") %>' ID="Label2" >
														</asp:Label>
													</ItemTemplate>
												</asp:TemplateColumn>
												<asp:BoundColumn DataField="StartDate" SortExpression="StartDate" HeaderText="Нач.дата" DataFormatString="{0:dd.MM.yyyy}"
													Visible="False">
													<HeaderStyle Wrap="False" Width="70px"></HeaderStyle>
													<ItemStyle ForeColor="DimGray"></ItemStyle>
												</asp:BoundColumn>
												<asp:BoundColumn DataField="BuildingType" SortExpression="BuildingType" HeaderText="Тип сграда">
													<HeaderStyle Width="10%"></HeaderStyle>
													<ItemStyle ForeColor="DimGray"></ItemStyle>
												</asp:BoundColumn>
												<asp:BoundColumn DataField="Area" SortExpression="Area" HeaderText="Статична РЗП">
													<HeaderStyle Width="50px"></HeaderStyle>
													<ItemStyle Wrap="False" HorizontalAlign="Right" ForeColor="DimGray"></ItemStyle>
												</asp:BoundColumn>
												<asp:BoundColumn DataField="Address" SortExpression="Address" HeaderText="Адрес">
													<HeaderStyle Width="15%"></HeaderStyle>
													<ItemStyle ForeColor="DimGray"></ItemStyle>
												</asp:BoundColumn>
												<asp:BoundColumn DataField="ClientName" SortExpression="Client" HeaderText="Клиент">
													<HeaderStyle Width="10%"></HeaderStyle>
													<ItemStyle ForeColor="DimGray"></ItemStyle>
												</asp:BoundColumn>
												<asp:BoundColumn DataField="FullName" SortExpression="Leader" HeaderText="Ръководител">
													<HeaderStyle></HeaderStyle>
												</asp:BoundColumn>
												<asp:TemplateColumn>
													<HeaderStyle Wrap="False" Width="20px"></HeaderStyle>
													<ItemStyle Wrap="False" HorizontalAlign="Center"></ItemStyle>
													<ItemTemplate>
														<asp:ImageButton id="btnProfile" runat="server" ToolTip="Изходна проектна документация" ImageUrl="images/7.gif"></asp:ImageButton>
													</ItemTemplate>
												</asp:TemplateColumn>
												<asp:TemplateColumn>
													<HeaderStyle Wrap="False" Width="20px"></HeaderStyle>
													<ItemStyle Wrap="False" HorizontalAlign="Center"></ItemStyle>
													<ItemTemplate>
														<asp:ImageButton id="btnSubprojects" runat="server" ToolTip="Фази" ImageUrl="images/9.gif"></asp:ImageButton>
													</ItemTemplate>
												</asp:TemplateColumn>
												<asp:TemplateColumn>
													<HeaderStyle Wrap="False" Width="20px"></HeaderStyle>
													<ItemStyle Wrap="False" HorizontalAlign="Center"></ItemStyle>
													<ItemTemplate>
														<asp:ImageButton id="btnSubContracters" runat="server" ToolTip="Подизпълнители" ImageUrl="images/1.gif"></asp:ImageButton>
													</ItemTemplate>
												</asp:TemplateColumn>
												<asp:TemplateColumn>
													<HeaderStyle Wrap="False" Width="20px"></HeaderStyle>
													<ItemStyle Wrap="False" HorizontalAlign="Center"></ItemStyle>
													<ItemTemplate>
														<asp:ImageButton id="btnEdit" runat="server" ToolTip="Редактирай" ImageUrl="images/edit1.gif"></asp:ImageButton>
													</ItemTemplate>
												</asp:TemplateColumn>
												<asp:TemplateColumn>
													<HeaderStyle Wrap="False" Width="20px"></HeaderStyle>
													<ItemStyle Wrap="False" HorizontalAlign="Center"></ItemStyle>
													<ItemTemplate>
														<asp:ImageButton id="btnDelete" runat="server" Width="12px" Height="12px" ToolTip="Изтрий" ImageUrl="images/delete.gif"></asp:ImageButton>
													</ItemTemplate>
												</asp:TemplateColumn>
												<asp:BoundColumn DataField="IsActive" Visible="False"></asp:BoundColumn>
												<asp:BoundColumn DataField="Black" Visible="False"></asp:BoundColumn>
												<asp:BoundColumn DataField="Concluded" Visible="False"></asp:BoundColumn>
											</Columns>
										</asp:datagrid>
									</asp:panel>
									<TABLE id="Table4" 
										cellSpacing="0" cellPadding="4" width="100%" border="0">
										<TR>
											<td><asp:button id="btnNewProject" runat="server" CssClass="ActionButton"></asp:button>&nbsp;
												<asp:button id="btnExport" runat="server" CssClass="ActionButton" Text="Експорт"></asp:button>&nbsp;
												<asp:button id="btnMissing" runat="server" CssClass="ActionButton" Text="Липсваща информация"></asp:button>&nbsp;</TD>
										</TR>
									</TABLE>
								</td>
							</tr>
						</table>
					</td>
				</tr>
			</table>
			<radg:radgrid id="gridCalls" runat="server" CssClass="RadGrid" Width="100%" AutoGenerateColumns="False"
				GridLines="Horizontal">
				<PagerStyle CssClass="GridHeader" Mode="NumericPages"></PagerStyle>
				<ItemStyle HorizontalAlign="Center" CssClass="GridItem"></ItemStyle>
				<GroupPanel Visible="False"></GroupPanel>
				<HeaderStyle HorizontalAlign="Center" Wrap="False" CssClass="GridHeader"></HeaderStyle>
				<AlternatingItemStyle HorizontalAlign="Center" CssClass="GridItem"></AlternatingItemStyle>
				<GroupHeaderItemStyle BorderColor="Black" BackColor="Silver"></GroupHeaderItemStyle>
				<MasterTableView DataSourcePersistenceMode="NoPersistence" AllowCustomPaging="False" AllowSorting="True"
					PageSize="15" GridLines="Horizontal" AllowPaging="False" Visible="True">
					<Columns>
						<radg:GridBoundColumn UniqueName="ProjectName" HeaderButtonType="TextButton" HeaderText="Име на проект"
							DataField="ProjectName"></radg:GridBoundColumn>
						<radg:GridBoundColumn UniqueName="ProjectCode" HeaderButtonType="TextButton" HeaderText="Код на проект"
							DataField="ProjectCode"></radg:GridBoundColumn>
						<radg:GridBoundColumn UniqueName="StartDate" HeaderButtonType="TextButton" HeaderText="Нач.дата" DataField="StartDate"
							DataFormatString="{0:dd.MM.yyyy}"></radg:GridBoundColumn>
						<radg:GridBoundColumn UniqueName="BuildingType" HeaderButtonType="TextButton" HeaderText="Тип сграда"
							DataField="BuildingType"></radg:GridBoundColumn>
						<radg:GridBoundColumn UniqueName="Area" HeaderButtonType="TextButton" HeaderText="Статична РЗП" DataField="Area"></radg:GridBoundColumn>
						<radg:GridBoundColumn UniqueName="Address" HeaderButtonType="TextButton" HeaderText="Адрес" DataField="Address"></radg:GridBoundColumn>
						<radg:GridBoundColumn UniqueName="ClientName" HeaderButtonType="TextButton" HeaderText="Клиент" DataField="ClientName"></radg:GridBoundColumn>
						<radg:GridBoundColumn UniqueName="FullName" HeaderButtonType="TextButton" HeaderText="Ръководител" DataField="FullName"></radg:GridBoundColumn>
					</Columns>
					<RowIndicatorColumn Visible="False" UniqueName="RowIndicator">
						<HeaderStyle Width="20px"></HeaderStyle>
					</RowIndicatorColumn>
					<EditFormSettings>
						<EditColumn UniqueName="EditCommandColumn"></EditColumn>
					</EditFormSettings>
					<ExpandCollapseColumn ButtonType="ImageButton" Visible="False" UniqueName="ExpandColumn">
						<HeaderStyle Width="19px"></HeaderStyle>
					</ExpandCollapseColumn>
				</MasterTableView>
			</radg:radgrid></form>
	</body>
</HTML>
