using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Data.SqlClient;
using log4net;
using Asa.Timesheet.Data;
using Asa.Timesheet.Data.Entities;
using Asa.Timesheet.Data.Util;
using Asa.Timesheet.Data.Reports;

namespace Asa.Timesheet.WebPages
{
	/// <summary>
	/// Summary description for AbsenceRequest.
	/// </summary>
	public class AbsenceRequest : TimesheetPageBase
	{
       
		private int nCount_MAX=1000;
		private static readonly DateTime MinReportDate = DateTime.ParseExact(System.Configuration.ConfigurationManager.AppSettings["MinReportDate"],
			"ddMMyyyy", System.Globalization.CultureInfo.InvariantCulture);
		private static readonly ILog log = LogManager.GetLogger(typeof(AbsenceRequest));
		private const string _confirmNone="confirmNone";
		#region Web controls

		protected System.Web.UI.WebControls.PlaceHolder menuHolder;
		protected System.Web.UI.WebControls.Label lblInfo;
		protected System.Web.UI.WebControls.Label lblError;
		protected System.Web.UI.WebControls.Calendar calStartDate;
		protected System.Web.UI.WebControls.Calendar calEndDate;
		protected System.Web.UI.WebControls.Label lblStartDay;
		protected System.Web.UI.HtmlControls.HtmlTable tblFooter;
		protected System.Web.UI.WebControls.CheckBox ckDates;
		protected System.Web.UI.WebControls.Label lblEndDay;
		protected System.Web.UI.WebControls.Label lblSelEndDate;
		protected Asa.Timesheet.WebPages.UserControls.PageHeader header;
		protected System.Web.UI.WebControls.Label Label1;
		protected System.Web.UI.WebControls.DropDownList ddlProject;
        protected System.Web.UI.WebControls.DropDownList ddlUser;
		protected System.Web.UI.WebControls.DataGrid grdAbsence;
        protected System.Web.UI.WebControls.DataGrid grdReport;
		protected System.Web.UI.WebControls.Label lbReason;
		protected System.Web.UI.WebControls.TextBox txtReason;
		protected System.Web.UI.WebControls.Button btnSendRequestAbsence;
		protected System.Web.UI.WebControls.Label Label2;
		protected System.Web.UI.WebControls.Calendar calSt;
		protected System.Web.UI.WebControls.Label Label4;
		protected System.Web.UI.WebControls.Calendar calEn;
		protected System.Web.UI.WebControls.Label lblReportTitle;
		protected System.Web.UI.WebControls.ImageButton ibGenerate;
		protected System.Web.UI.WebControls.ImageButton ibPdfExport;
		protected System.Web.UI.WebControls.ImageButton ibXlsExport;
		protected System.Web.UI.WebControls.Label lbSt;
		protected System.Web.UI.WebControls.Label lbEn;
		protected System.Web.UI.WebControls.Label lbStt;
		protected System.Web.UI.WebControls.Label lbEnt;
		protected System.Web.UI.WebControls.Label lbInformation;
		protected System.Web.UI.HtmlControls.HtmlTable tblRequestAbsence;
		protected System.Web.UI.WebControls.Label Label3;
		protected System.Web.UI.WebControls.Label Label5;
		protected System.Web.UI.WebControls.Label lbUsedVacation;
		protected System.Web.UI.WebControls.Label lbVacationRest;
		protected System.Web.UI.WebControls.Label Label6;
		//protected System.Web.UI.WebControls.Label lbVacationPast;
		protected System.Web.UI.WebControls.RequiredFieldValidator RequiredFieldValidator1;
		private int DAYS_ABSENSE=2;
		
		#endregion
		
		#region Page load

        private void Page_Load(object sender, System.EventArgs e)
        {
            if (!(LoggedUser.IsAuthenticated)) ErrorRedirect(Resource.ResourceManager["noRightsAlert"]);

            if (!this.IsPostBack)
            {
                this.header.PageTitle = Resource.ResourceManager["PageTitleAbsenceRequest"];
                this.header.UserName = this.LoggedUser.UserName;

                calSt.SelectedDate = calEn.SelectedDate = DateTime.Now.AddDays(7);
                lbSt.Text = lbEn.Text = calSt.SelectedDate.ToString("d");

                LoadDDLAbsence();
                SqlDataReader reader = null;
                if (LoggedUser.IsEmployee || LoggedUser.IsAssistantOnly)
                {


                    try
                    {
                        reader = UsersData.SelectUserNamesOnlyActive(false);


                        ddlUser.DataSource = reader;
                        ddlUser.DataValueField = "UserID";
                        ddlUser.DataTextField = "FullName";

                        ddlUser.DataBind();
                        
                        ddlUser.Items.Insert(0, new ListItem("", "0"));
                        //DBUGG cannot be sub'd by himself:
                        ddlUser.Items.Remove(ddlUser.Items.FindByValue(LoggedUser.UserID.ToString()));
                    }
                    catch (Exception ex)
                    {
                        log.Info(ex);
                    }
                    ddlUser.Visible = true;
                    Label6.Visible = true;
                }
            }//!PostBack


            lblInfo.Visible = false;


            GenerateClick();
            UIHelpers.CreateMenu(menuHolder, LoggedUser);
            Page.ClientScript.RegisterOnSubmitStatement(this.GetType(), "submit", "ShowLoading()");
        }

        private void LoadDDLAbsence()
        {
            ListItem li = new ListItem(Resource.ResourceManager["strPlatenOtpusk"], System.Configuration.ConfigurationManager.AppSettings["Platen"]);
            ddlProject.Items.Add(li);
            UserInfo ui = UsersData.SelectUserByID(LoggedUser.UserID);
            if (ui != null && !ui.IsTraine)
            {
                li = new ListItem(Resource.ResourceManager["strNeplatenOtpusk"], System.Configuration.ConfigurationManager.AppSettings["Neplaten"]);
                ddlProject.Items.Add(li);
            }
            //			li = new ListItem(Resource.ResourceManager["strBolnichni"],System.Configuration.ConfigurationManager.AppSettings["Bolen"]);
            //			ddlProject.Items.Add(li);
        }
		#endregion


		#region Web Form Designer generated code
        override protected void OnInit(EventArgs e)
        {
            //
            // CODEGEN: This call is required by the ASP.NET Web Form Designer.
            //
            InitializeComponent();
            base.OnInit(e);
        }
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
        private void InitializeComponent()
        {
            this.calSt.DayRender += new System.Web.UI.WebControls.DayRenderEventHandler(this.calStartDate_DayRender);
            this.calSt.SelectionChanged += new System.EventHandler(this.calSt_SelectionChanged);
            this.calEn.DayRender += new System.Web.UI.WebControls.DayRenderEventHandler(this.calStartDate_DayRender);
            this.calEn.SelectionChanged += new System.EventHandler(this.calEn_SelectionChanged);
            this.btnSendRequestAbsence.Click += new System.EventHandler(this.btnSendRequestAbsence_Click);
            this.grdAbsence.ItemDataBound += new System.Web.UI.WebControls.DataGridItemEventHandler(this.grdAbsence_ItemDataBound);
            this.Load += new System.EventHandler(this.Page_Load);

        }
		#endregion

		#region DB

//		private bool LoadUsers()
//		{
//			return LoadUsers(-1);
//		}
//
//
//		private bool LoadUsers(int userID)
//		{
//			//if (this.LoggedUser.IsLeader)
//			//{
//			ddlUser.Visible = true;
//
//			SqlDataReader reader = null;
//			try
//			{
//				switch (ddlUserStatus.SelectedValue)
//				{
//					case "0":
//						reader = UsersData.SelectUserNames(true, true, (LoggedUser.HasPaymentRights||UIHelpers.GetIDParam()!=-1));break;
//					case "1":
//						reader = UsersData.SelectUserNames(false, true, (LoggedUser.HasPaymentRights||UIHelpers.GetIDParam()!=-1));break;
//					case "2":
//						reader = UsersData.SelectUserNames1();break;
//				}			
//				ddlUser.DataSource = reader;
//				ddlUser.DataValueField = "UserID";
//				ddlUser.DataTextField = "FullName";
//				ddlUser.DataBind();
//				ddlUser.Items.Insert(0,new ListItem("<"+Resource.ResourceManager["reports_ddlAllUsers"]+">","0"));
//				ddlUser.SelectedValue = "0";
//				if(userID != -1)
//				{
//					if(ddlUser.Items.FindByValue(userID.ToString())!= null)
//					{
//						//ckDates.Checked = true;
//						ddlUser.SelectedValue = userID.ToString();
//						ddlUserStatus.Visible = ddlUser.Visible = lblSluj.Visible = false;
//						//ckDates.Visible = ddlProject.Visible = ibGenerate.Visible =ibPdfExport.Visible= ibXlsExport.Visible= false;
//						//GenerateClick();
//						calEn.SelectedDate = calSt.SelectedDate = DateTime.Now.AddDays(7);
//						lbReason.Visible = txtReason.Visible = btnSendRequestAbsence.Visible = true;
//					}
//				}
//			}
//			catch (Exception ex)
//			{
//				log.Error(ex);
//				return false;
//			}
//			finally
//			{
//				if (reader!=null) reader.Close();
//			}
//			//}
//
//			return true;
//		}

        private ArrayList SelectEnteredDays(int userID, DateTime startDate, DateTime endDate)
        {
            ArrayList days = new ArrayList();

            SqlDataReader reader = null;
            try
            {
                reader = ReportsData.SelectEnteredDays(userID, startDate, endDate);
                while (reader.Read())
                {
                    days.Add(reader.GetDateTime(1));
                }
            }
            catch (Exception ex)
            {
                log.Error(ex);
                return null;
            }
            finally
            {
                if (reader != null) reader.Close();
            }

            return days;
        }


		#endregion

		#region Event handlers

        private void btnOK_Click(object sender, System.EventArgs e)
        {



        }
        private ArrayList GetData(out string title)
        {
            title = "";
            DateTime startDay = GetStartDate(), endDay = GetEndDate();

            //			if (ckDates.Checked) 
            //			{
            //				startDay = MinReportDate;
            //				endDay = DateTime.Today;
            //			}
            //			else
            //			{
            if (startDay < MinReportDate) startDay = MinReportDate;
            if (endDay > DateTime.Today) endDay = DateTime.Today;

            if (startDay > endDay)
            {
                lblError.Text = Resource.ResourceManager["reports_errorDate"];
                return null;
            }
            //			}

            int userID = LoggedUser.UserID;
            //	int startIndex, endIndex;

            //			if (userID == 0) { startIndex = 1; endIndex = ddlUser.Items.Count - 1; }
            //			else { startIndex = ddlUser.SelectedIndex; endIndex = ddlUser.SelectedIndex; }

            ArrayList reportData = new ArrayList();
            ArrayList days = null;

            //			for (int i=startIndex; i<=endIndex; i++)
            //			{
            string userName = LoggedUser.UserName;
            //userID = int.Parse(ddlUser.Items[i].Value);

            days = SelectEnteredDays(userID, startDay, endDay);

            if (days == null) // error loading days from DB, set error message, continue to next user
            {
                reportData.Add(new DaysListInfo(userID, userName, "Error"));
                //continue;
            }

            ArrayList notEnteredDays = TimeHelper.GetNotEnteredDaysList(startDay, endDay, days, true);

            string s = DaysListInfo.GetDaysString(notEnteredDays, false, "dd.MM.yy");
            if (s == String.Empty) s = Resource.ResourceManager["reports_EmptyDays_NoEmptyDays"];
            DaysListInfo di = new DaysListInfo(userID, userName, s);
            di.Days = TimeHelper.GetNotEnteredDaysList(startDay, endDay, days, false).Count;
            reportData.Add(di);

            //			}
            title = String.Concat(Resource.ResourceManager["reports_EmptyDays_GridTitle"],
                Resource.ResourceManager["rpt_periodFrom"], " ", startDay.ToShortDateString(), " ",
                Resource.ResourceManager["rpt_periodTo"], " " + endDay.ToShortDateString());
            lblReportTitle.Text = title;
            return reportData;
        }

		
		#endregion

		#region Util

		
		#endregion

        private DataView GetDataView(out string t)
        {
            string title;
            ArrayList al = GetData(out title);
            t = title;
            DataTable dt = new DataTable();
            dt.Columns.Add(new DataColumn("UserName"));
            dt.Columns.Add(new DataColumn("DaysString"));
            dt.Columns.Add(new DataColumn("Days"));

            foreach (DaysListInfo li in al)
            {
                DataRow dr = dt.NewRow();
                dr[0] = li.UserName;
                dr[1] = li.DaysString;
                dr[2] = li.Days.ToString();
                dt.Rows.Add(dr);
            }
            dt.AcceptChanges();
            return new DataView(dt);
        }

        private void GenerateClick()
        {
            int userID = LoggedUser.UserID;//notes:current user
            int projectID = 0;//notes:all

            //notes:start of the year
            DateTime startDate = GetStartDate();
            //notes:today
            DateTime endDate = GetEndDate();

            grdAbsence.DataSource = AbsenceReportData.SelectAbsenceReportDS(userID, projectID, startDate, endDate, false);

            grdAbsence.DataBind();
            grdAbsence.Visible = true;

            int nUsed = 0;
            int nPast = 0;
            lbVacationRest.Text = UsersData.GetTotalYearVacation(DateTime.Now, userID, out nUsed, out nPast).ToString();
            //lbVacationPast.Text = nPast.ToString();
            lbUsedVacation.Text = nUsed.ToString();
        }

        private void grdAbsence_ItemDataBound(object sender, System.Web.UI.WebControls.DataGridItemEventArgs e)
        {
            if ((e.Item.ItemType == ListItemType.Item) || (e.Item.ItemType == ListItemType.AlternatingItem))
            {
                DataGrid grdReport = (DataGrid)e.Item.FindControl("grdReport");

                DataRowView drv = (DataRowView)e.Item.DataItem;
                DataTable dt = AbsenceReportData.GetDaysStringForUser(drv.Row);

                grdReport.DataSource = dt;
                grdReport.DataBind();
                grdReport.Visible = true;
                //((Label)e.Item.FindControl("lblName")).Text = drv["Name"].ToString();
            }
        }

        private void calStartDate_DayRender(object sender, System.Web.UI.WebControls.DayRenderEventArgs e)
        {
            if (e.Day.Date < DateTime.Now.AddDays(DAYS_ABSENSE))
            {
                e.Cell.Controls.Clear();
                e.Cell.Text = e.Day.DayNumberText;
                e.Cell.ForeColor = System.Drawing.Color.NavajoWhite;

            }
        }

        private void btnSendRequestAbsence_Click(object sender, System.EventArgs e)
        {

            int vacationDays = UIHelpers.GetBusinessDays( calSt.SelectedDate, calEn.SelectedDate) + 1;
            int advanceDays = UIHelpers.GetBusinessDays(DateTime.Now, calSt.SelectedDate) + 1;
            int sinceLastVaction = UIHelpers.GetBusinessDays(getLastVacation(), calSt.SelectedDate) + 1;
            //double advanceDays = (DateTime.Now - calSt.SelectedDate).TotalDays;
           //DateTime.Now.Year
            if (sinceLastVaction <= 10) {
                lblError.Text = "���-������� �������� ����� ������� � 10 ���������� ���. �������� �� � ������� ���������: <small><a href=\"mailto:serafimov@asa-bg.com\">serafimov@asa-bg.com</a></small>";
                return;
            }
            if(vacationDays > 10){
                lblError.Text = "�� ������ ��� 10 ��� ������ �� �� �������� � ������� ���������: <small><a href=\"mailto:serafimov@asa-bg.com\">serafimov@asa-bg.com</a></small>";
                return;
            }
            if(vacationDays == 1 && advanceDays < 2 ){
                lblError.Text = "�� 3-������ ������ ������ �� �� ����� ���-����� 2 ��� �������������.";
                return;
            }
            if (vacationDays > 1 && vacationDays <= 3 && advanceDays < 5)
            {
                lblError.Text = "3-������ ������ ������ �� �� ����� 5 ��� �������������.";
                return;
            }
            if (vacationDays > 3 && vacationDays < 10 && advanceDays < 20)
            {
                lblError.Text = "������ ����� 4 � 10 ��� ������ �� �� ����� ���-����� 20��� �������������.";
                return;
            }
            if (calSt.SelectedDate > calEn.SelectedDate)
            {
                lblError.Text = Resource.ResourceManager["hoursPeriod_IncorrectDateMsg"];
                return;
            }
            if ((LoggedUser.IsEmployee || LoggedUser.IsAssistantOnly) && (ddlUser.SelectedIndex == 0))
            {
                AlertFieldNotEntered(Label6);
                lblError.Text = "����, �������� ���������!";
                return;
            }
            
            int nCount = 1;
            int nCountAll = 1;
            DateTime dtStart = calSt.SelectedDate;
            DateTime dtEnd = calEn.SelectedDate;
            DateTime dtStart1 = dtStart;
            if (dtStart1 < dtEnd)
            {
                while (dtStart1 < dtEnd && nCount < nCount_MAX)
                {
                    dtStart1 = dtStart1.AddDays(1);
                    if (!CalendarDAL.IsHoliday(dtStart1))
                        nCount++;
                    nCountAll++;
                }
            }
            int userID = LoggedUser.UserID;
            int nUsed = 0;
            int nPast = 0;
            int nTotal = UsersData.GetTotalYearVacation(DateTime.Now, userID, out nUsed, out nPast);
            nTotal += nPast;
            bool bNotAskedNone = (ViewState[_confirmNone] == null || (bool)ViewState[_confirmNone] != true);
            if (bNotAskedNone && ddlProject.SelectedValue == System.Configuration.ConfigurationManager.AppSettings["Platen"] && nCount > nTotal)
            {
                lblError.Text = Resource.ResourceManager["absense_IncorrectMsg"];
                ViewState.Add(_confirmNone, true);
                return;
            }
            int requestID = AbsenceRequestData.InsertAbsenceRequest(calSt.SelectedDate, calEn.SelectedDate, false, UIHelpers.ToInt(ddlProject.SelectedValue), LoggedUser.UserID, UIHelpers.ToInt(ddlUser.SelectedValue));

            string systemPath = this.Page.Request.Url.AbsoluteUri;
            int index = systemPath.LastIndexOf(@"/");
            systemPath = systemPath.Substring(0, index + 1);

            string sUsed = DateTime.Now.Year.ToString() + " - " + lbUsedVacation.Text;
            string sRest = DateTime.Now.Year.ToString() + " - " + lbVacationRest.Text;
            // int nRestAfter = int.Parse(lbVacationRest.Text)/* + int.Parse(lbVacationPast.Text)*/ - nCount;
            int nRestAfter = int.Parse(lbVacationRest.Text) - nCount;

            string sRestAfter = nRestAfter.ToString();
            sRestAfter = " - " + sRestAfter.Replace("-", "(-)");
            String SubstituteAndReason = null;
            if (LoggedUser.IsEmployee || LoggedUser.IsAssistantOnly)
            {
                SubstituteAndReason = txtReason.Text + "<br><div style='display: block; background-color: rgb(0, 128, 128); color:white; padding: 15px;'>�� ���� ��������� ��: <br><strong style='font-size: 14pt'>" + ddlUser.SelectedItem.Text + "</div></strong>";
            }
            else {
                SubstituteAndReason = txtReason.Text;
            }
            if (MailBO.SendMailRequestAbsence(LoggedUser, calSt.SelectedDate.ToString("dd.M"), calEn.SelectedDate.ToString("dd.M.yyyy"), SubstituteAndReason, requestID, ddlProject.SelectedItem.Text, systemPath, sUsed, sRestAfter, nCount.ToString(), nCountAll.ToString()))
            {
                lblInfo.Text = Resource.ResourceManager["AbsenceConfirmMailSend"];
                lblInfo.Visible = true;
                tblRequestAbsence.Visible = false;
            }
            else
            {
                lblError.Text = Resource.ResourceManager["AbsenceConfirmMailNotSend"];
                lblError.Visible = true;
            }
        }

        private void calSt_SelectionChanged(object sender, System.EventArgs e)
        {
            lbSt.Text = calSt.SelectedDate.ToString("d");
            lbEn.Text =calEn.SelectedDate.ToString("d");
        }

        private void calEn_SelectionChanged(object sender, System.EventArgs e)
        {
            lbSt.Text = calSt.SelectedDate.ToString("d");
            lbEn.Text = calEn.SelectedDate.ToString("d");
        }
        private DateTime GetStartDate()
        {
            return new DateTime(DateTime.Now.Year, 1, 1);
        }
        private DateTime GetEndDate()
        {
            return Constants.DateMax;
        }
        private DateTime getLastVacation() {
            DateTime lastVacation = DateTime.Now.AddYears(-1);

            SqlConnection conn = new SqlConnection(CommonDAL.ConnectionString); 
            conn.Open();
            SqlCommand cmd = new SqlCommand("select TOP 1 WorkDate from WorkTimes where ProjectID = 9 AND UserID = " + LoggedUser.UserID.ToString() + " order by WorkDate DESC", conn);
            SqlDataAdapter sda = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            sda.Fill(dt);
            conn.Close();
            if (dt.Rows.Count > 0){
                lastVacation = (DateTime)dt.Rows[0]["WorkDate"];
            }
            return lastVacation;
        }

	}
}
