﻿<%@ Page Title="Потребители" Language="C#" MasterPageFile="~/PM.Master" AutoEventWireup="True" CodeBehind="Users.aspx.cs" Inherits="PM.Users" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
  
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
<table height="100%" cellSpacing="0" cellPadding="0" width="100%" border="0">
	<tr>
					<td><br>
						<table height="100%" cellSpacing="0" cellPadding="0" width="100%" border="0">
							<tr>
								<td>
									<P>
										<asp:label id="lblError" runat="server" CssClass="ErrorLabel" EnableViewState="False" ForeColor="Red"></asp:label><asp:label id="lblInfo" runat="server" CssClass="InfoLabel" EnableViewState="False"></asp:label><BR>
										<TABLE id="tblSub" style="WIDTH: 100%; HEIGHT: 26px" cellSpacing="1" cellPadding="1" width="848"
											border="0">
											<TR>

												<td >
													<asp:textbox id="txtProjectPartName" runat="server" CssClass="enterDataBox" Width="180px" MaxLength="30"></asp:textbox></TD>

												<td ">
													<asp:dropdownlist id="ddlProjectStatus" runat="server" CssClass="enterDataBox" Width="160px" AutoPostBack="True"></asp:dropdownlist></TD>

												<td >

														<asp:dropdownlist id="ddlBuildingTypes" runat="server" CssClass="enterDataBox" Width="160px" AutoPostBack="True"></asp:dropdownlist>
												</TD>

												<td >
													<asp:dropdownlist id="ddlProjects" runat="server" CssClass="enterDataBox" Width="160px"></asp:dropdownlist></TD>

												<td >
													<asp:button id="btnSearch" runat="server" CssClass="ActionButton" Width="90px"></asp:button></TD>
                                                    <td>
													<asp:button id="btnClear" runat="server" CssClass="ActionButton" Width="90px"></asp:button></TD>
                                                    <td style="WIDTH: 100%; HEIGHT: 26px"><asp:button id="btnNew" runat="server" CssClass="ActionButton" style="float: right"></asp:button></td>
											</TR>
										</TABLE>
										<BR>
										<asp:panel id="grid" 
											runat="server" Width="98%">
											<asp:label id="lbSubName" runat="server" EnableViewState="False" Font-Bold="True" Font-Size="X-Small"></asp:label>
											<BR>
											<BR>
											<asp:datagrid id="grdAccountsSubcontracters" runat="server" CssClass="Grid" Width="100%" AllowSorting="True"
												PageSize="2" CellPadding="4" AutoGenerateColumns="False">
												<ItemStyle CssClass="GridItem"></ItemStyle>
												<HeaderStyle Wrap="False" CssClass="GridHeader"></HeaderStyle>
												<Columns>
													<asp:TemplateColumn HeaderText="#">
														<ItemStyle ForeColor="DimGray" Width="10px"></ItemStyle>
														<ItemTemplate>
															<asp:Label CssClass="menuTable" runat="server" Text='<%# (int)DataBinder.Eval(Container, "ItemIndex")+1 %>' ID="Label1" NAME="Label1">
															</asp:Label>
														</ItemTemplate>
													</asp:TemplateColumn>
													<asp:BoundColumn DataField="SubcontracterType" SortExpression="SubcontracterType"></asp:BoundColumn>
													<asp:TemplateColumn SortExpression="FullName">
														<ItemStyle Width="20%"></ItemStyle>
														<ItemTemplate>
															<asp:LinkButton id="lnk" runat="server" CssClass="menuTable" ForeColor="#804000" CommandName="Edit">
																<span Class="menuTable">
																	<%# DataBinder.Eval(Container, "DataItem.FullName") %>
																</span>
															</asp:LinkButton>
														</ItemTemplate>
													</asp:TemplateColumn>
													<asp:BoundColumn DataField="AccountName"  SortExpression="AccountName"></asp:BoundColumn>
													<asp:BoundColumn Visible="False" DataField="UserType"></asp:BoundColumn>
													<asp:BoundColumn DataField="Mail"  SortExpression="Mail"></asp:BoundColumn>
													<asp:BoundColumn DataField="Addres"  SortExpression="Addres"></asp:BoundColumn>
													<asp:BoundColumn DataField="Phone"  SortExpression="Phone"></asp:BoundColumn>
												</Columns>
											</asp:datagrid>
											<BR>
											<BR>
											<asp:label id="lbClientName" runat="server" EnableViewState="False" Font-Bold="True" Font-Size="X-Small"></asp:label>
											<BR>
											<BR>
											<asp:datagrid id="grdAccountsClients" runat="server" CssClass="Grid" Width="100%" AllowSorting="True"
												PageSize="2" CellPadding="4" AutoGenerateColumns="False">
												<ItemStyle CssClass="GridItem"></ItemStyle>
												<HeaderStyle Wrap="False" CssClass="GridHeader"></HeaderStyle>
												<Columns>
													<asp:TemplateColumn HeaderText="#">
														<ItemStyle ForeColor="DimGray" Width="10px"></ItemStyle>
														<ItemTemplate>
															<asp:Label CssClass="menuTable" runat="server" Text='<%# (int)DataBinder.Eval(Container, "ItemIndex")+1 %>' ID="Label1" NAME="Label1">
															</asp:Label>
														</ItemTemplate>
													</asp:TemplateColumn>
													<asp:TemplateColumn SortExpression="FullName">
														<ItemStyle Width="20%"></ItemStyle>
														<ItemTemplate>
															<asp:LinkButton id="lnk" runat="server" CssClass="menuTable" ForeColor="#804000" CommandName="Edit">
																<span Class="menuTable">
																	<%# DataBinder.Eval(Container, "DataItem.FullName") %>
																</span>
															</asp:LinkButton>
														</ItemTemplate>
													</asp:TemplateColumn>
													<asp:BoundColumn DataField="AccountName" SortExpression="AccountName"></asp:BoundColumn>
													<asp:BoundColumn Visible="False" DataField="UserType"></asp:BoundColumn>
													<asp:BoundColumn DataField="Mail" SortExpression="Mail"></asp:BoundColumn>
													<asp:BoundColumn DataField="Addres"  SortExpression="Addres"></asp:BoundColumn>
													<asp:BoundColumn DataField="Phone"  SortExpression="Phone"></asp:BoundColumn>
												</Columns>
											</asp:datagrid>
											<BR>
											<BR>
											<asp:label id="lbBuilderName" runat="server" EnableViewState="False" Font-Bold="True" Font-Size="X-Small"></asp:label>
											<BR>
											<BR>
											<asp:datagrid id="grdAccountsBuilders" runat="server" CssClass="Grid" Width="100%" AllowSorting="True"
												PageSize="2" CellPadding="4" AutoGenerateColumns="False">
												<ItemStyle CssClass="GridItem"></ItemStyle>
												<HeaderStyle Wrap="False" CssClass="GridHeader"></HeaderStyle>
												<Columns>
													<asp:TemplateColumn HeaderText="#">
														<ItemStyle ForeColor="DimGray" Width="10px"></ItemStyle>
														<ItemTemplate>
															<asp:Label CssClass="menuTable" runat="server" Text='<%# (int)DataBinder.Eval(Container, "ItemIndex")+1 %>' ID="Label1" NAME="Label1">
															</asp:Label>
														</ItemTemplate>
													</asp:TemplateColumn>
													<asp:TemplateColumn SortExpression="FullName">
														<ItemStyle Width="20%"></ItemStyle>
														<ItemTemplate>
															<asp:LinkButton id="lnk" runat="server" CssClass="menuTable" ForeColor="#804000" CommandName="Edit">
																<span Class="menuTable">
																	<%# DataBinder.Eval(Container, "DataItem.FullName") %>
																</span>
															</asp:LinkButton>
														</ItemTemplate>
													</asp:TemplateColumn>
													<asp:BoundColumn DataField="AccountName" SortExpression="AccountName"></asp:BoundColumn>
													<asp:BoundColumn Visible="False" DataField="UserType"></asp:BoundColumn>
													<asp:BoundColumn DataField="Mail" SortExpression="Mail"></asp:BoundColumn>
													<asp:BoundColumn DataField="Addres" SortExpression="Addres"></asp:BoundColumn>
													<asp:BoundColumn DataField="Phone" SortExpression="Phone"></asp:BoundColumn>
												</Columns>
											</asp:datagrid>
											<BR>
											<BR>
											<asp:label id="lbWatcherName" runat="server" EnableViewState="False" Font-Bold="True" Font-Size="X-Small"></asp:label>
											<BR>
											<BR>
											<asp:datagrid id="grdAccountsWatchers" runat="server" CssClass="Grid" Width="100%" AllowSorting="True"
												PageSize="2" CellPadding="4" AutoGenerateColumns="False">
												<ItemStyle CssClass="GridItem"></ItemStyle>
												<HeaderStyle Wrap="False" CssClass="GridHeader"></HeaderStyle>
												<Columns>
													<asp:TemplateColumn HeaderText="#">
														<ItemStyle ForeColor="DimGray" Width="10px"></ItemStyle>
														<ItemTemplate>
															<asp:Label CssClass="menuTable" runat="server" Text='<%# (int)DataBinder.Eval(Container, "ItemIndex")+1 %>' ID="Label1" NAME="Label1">
															</asp:Label>
														</ItemTemplate>
													</asp:TemplateColumn>
													<asp:TemplateColumn SortExpression="FullName">
														<ItemStyle Width="20%"></ItemStyle>
														<ItemTemplate>
															<asp:LinkButton id="lnk" runat="server" CssClass="menuTable" ForeColor="#804000" CommandName="Edit">
																<span Class="menuTable">
																	<%# DataBinder.Eval(Container, "DataItem.FullName") %>
																</span>
															</asp:LinkButton>
														</ItemTemplate>
													</asp:TemplateColumn>
													<asp:BoundColumn DataField="AccountName" SortExpression="AccountName"></asp:BoundColumn>
													<asp:BoundColumn Visible="False" DataField="UserType"></asp:BoundColumn>
													<asp:BoundColumn DataField="Mail" SortExpression="Mail"></asp:BoundColumn>
													<asp:BoundColumn DataField="Addres" Visible="False"></asp:BoundColumn>
													<asp:BoundColumn DataField="Phone" Visible="False"></asp:BoundColumn>
												</Columns>
											</asp:datagrid>
											<BR>
											<BR>
											<asp:label id="lbOtherName" runat="server" EnableViewState="False" Font-Bold="True" Font-Size="X-Small"></asp:label>
											<BR>
											<BR>
											<asp:datagrid id="grdAccountsOthers" runat="server" CssClass="Grid" Width="100%" AllowSorting="True"
												PageSize="2" CellPadding="4" AutoGenerateColumns="False">
												<ItemStyle CssClass="GridItem"></ItemStyle>
												<HeaderStyle Wrap="False" CssClass="GridHeader"></HeaderStyle>
												<Columns>
													<asp:TemplateColumn HeaderText="#">
														<ItemStyle ForeColor="DimGray" Width="10px"></ItemStyle>
														<ItemTemplate>
															<asp:Label CssClass="menuTable" runat="server" Text='<%# (int)DataBinder.Eval(Container, "ItemIndex")+1 %>' ID="Label1" NAME="Label1">
															</asp:Label>
														</ItemTemplate>
													</asp:TemplateColumn>
													<asp:TemplateColumn SortExpression="FullName">
														<ItemStyle Width="20%"></ItemStyle>
														<ItemTemplate>
															<asp:LinkButton id="lnk" runat="server" CssClass="menuTable" ForeColor="#804000" CommandName="Edit">
																<span Class="menuTable">
																	<%# DataBinder.Eval(Container, "DataItem.FullName") %>
																</span>
															</asp:LinkButton>
														</ItemTemplate>
													</asp:TemplateColumn>
													<asp:BoundColumn DataField="AccountName" SortExpression="AccountName"></asp:BoundColumn>
													<asp:BoundColumn Visible="False" DataField="UserType"></asp:BoundColumn>
													<asp:BoundColumn DataField="Mail" SortExpression="Mail"></asp:BoundColumn>
													<asp:BoundColumn Visible="False" DataField="Addres"></asp:BoundColumn>
													<asp:BoundColumn Visible="False" DataField="Phone"></asp:BoundColumn>
												</Columns>
											</asp:datagrid>
										</asp:panel>

									</P>
								</td>
							</tr>
						</table>
					</td>
				</tr>
</table>
</asp:Content>