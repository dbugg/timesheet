using System;
using System.Data;
using System.Data.SqlClient;
namespace Asa.Timesheet.Data
{
	/// <summary>
	/// Summary description for SQLParms.
	/// </summary>
	public class SQLParms
	{
		
		public static SqlParameter[] CreateMeetingsListForDatesProc (DateTime startdate, DateTime enddate, int projectid, int hotissuecategoryid)
		{
			SqlParameter[] parms = new SqlParameter[4];
			parms[0]= new System.Data.SqlClient.SqlParameter("@startdate", System.Data.SqlDbType.SmallDateTime, 8, System.Data.ParameterDirection.Input, true, 0, 0, "startdate", DataRowVersion.Current, null);
			parms[0].Value = startdate;
			parms[1]= new System.Data.SqlClient.SqlParameter("@enddate", System.Data.SqlDbType.SmallDateTime, 8, System.Data.ParameterDirection.Input, true, 0, 0, "enddate", DataRowVersion.Current, null);
			parms[1].Value = enddate;
			parms[2]= new System.Data.SqlClient.SqlParameter("@projectid", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.Input, true, 10, 0, "projectid", DataRowVersion.Current, null);
			parms[2].Value = projectid;
			parms[3]= new System.Data.SqlClient.SqlParameter("@hotissuecategoryid", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.Input, true, 10, 0, "hotissuecategoryid", DataRowVersion.Current, null);
			parms[3].Value = hotissuecategoryid;

			return parms;
		}


		public static SqlParameter[] CreateMailingsListProc1(DateTime startdate, DateTime enddate, int projectid,string sort,bool allSendResece,bool onlySendReseve,bool AllProject,bool IsActiveProject,bool NoProjects,int BuildingType,int clientID)
		{
			SqlParameter[] parms = new SqlParameter[9];
			parms[0]= new System.Data.SqlClient.SqlParameter("@startdate", System.Data.SqlDbType.SmallDateTime, 8, System.Data.ParameterDirection.Input, true, 0, 0, "startdate", DataRowVersion.Current, null);
			if(startdate!= Constants.DateMax)
				parms[0].Value = startdate;
			parms[1]= new System.Data.SqlClient.SqlParameter("@enddate", System.Data.SqlDbType.SmallDateTime, 8, System.Data.ParameterDirection.Input, true, 0, 0, "enddate", DataRowVersion.Current, null);
			if(enddate!= Constants.DateMax)
				parms[1].Value = enddate;
			parms[2]= new System.Data.SqlClient.SqlParameter("@projectid", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.Input, true, 10, 0, "projectid", DataRowVersion.Current, null);
			if(projectid>0)
				parms[2].Value = projectid;
			parms[3]= new System.Data.SqlClient.SqlParameter("@sort", System.Data.SqlDbType.NVarChar, 50, System.Data.ParameterDirection.Input, true, 10, 0, "sort", DataRowVersion.Current, null);
			if(sort !=null && sort!="")
				parms[3].Value = sort;
			parms[4]= new System.Data.SqlClient.SqlParameter("@Send", System.Data.SqlDbType.Bit, 1, System.Data.ParameterDirection.Input, true, 0, 0, "Send", DataRowVersion.Current, null);
			if(!allSendResece)
				parms[4].Value = onlySendReseve;
			parms[5]= new System.Data.SqlClient.SqlParameter("@ProjectIsActive", System.Data.SqlDbType.Bit, 1, System.Data.ParameterDirection.Input, true, 0, 0, "ProjectIsActive", DataRowVersion.Current, null);
			if(!AllProject)
				parms[5].Value = IsActiveProject;
			parms[6]= new System.Data.SqlClient.SqlParameter("@BuildingTypeID", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.Input, true, 10, 0, "BuildingTypeID", DataRowVersion.Current, null);
			if(BuildingType>0)
				parms[6].Value = BuildingType;
			parms[7]= new System.Data.SqlClient.SqlParameter("@NoProject", System.Data.SqlDbType.Bit, 1, System.Data.ParameterDirection.Input, true, 0, 0, "NoProject", DataRowVersion.Current, null);
			parms[7].Value = NoProjects;
			parms[8]= new System.Data.SqlClient.SqlParameter("@ClientID", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.Input, true, 10, 0, "ClientID", DataRowVersion.Current, null);
			if(clientID>0)
				parms[8].Value = clientID;

			return parms;
	
		}

		public static SqlParameter[] CreatePaymentsListByProjectAndClientsProc(int projectID,int clientID)
		{
			SqlParameter[] parms = new SqlParameter[2];
			parms[0]= new System.Data.SqlClient.SqlParameter("@ProjectID", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.Input, true, 10, 0, "ProjectID", DataRowVersion.Current, null);
			parms[0].Value = projectID;
			parms[1]= new System.Data.SqlClient.SqlParameter("@ClientID", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.Input, true, 10, 0, "ClientID", DataRowVersion.Current, null);
			parms[1].Value = clientID;
				
			return parms;
		}
		public static SqlParameter[] CreateProjectClientsSelByProjectProc (int ProjectID,int ClientType)
		{
			SqlParameter[] parms = new SqlParameter[2];
			parms[0]= new System.Data.SqlClient.SqlParameter("@ProjectID", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.Input, true, 10, 0, "ProjectID", DataRowVersion.Current, null);
			if(ProjectID>0)
				parms[0].Value = ProjectID;
			parms[1]= new System.Data.SqlClient.SqlParameter("@ClientType", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.Input, true, 10, 0, "ClientType", DataRowVersion.Current, null);
			parms[1].Value = ClientType;

			return parms;
		}
		public static SqlParameter[] CreateProjectContractsListByProjectProc (int ProjectID)
		{
			SqlParameter[] parms = new SqlParameter[1];
			parms[0]= new System.Data.SqlClient.SqlParameter("@ProjectID", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.Input, true, 10, 0, "ProjectID", DataRowVersion.Current, null);
			parms[0].Value = ProjectID;

			return parms;
		}
		public static SqlParameter[] CreateProtokolsSelProc1 (DateTime startdate, DateTime enddate, int projectid,string sort,int projectStatus, int buildingType)
		{
			SqlParameter[] parms = new SqlParameter[7];
			parms[0]= new System.Data.SqlClient.SqlParameter("@startdate", System.Data.SqlDbType.SmallDateTime, 8, System.Data.ParameterDirection.Input, true, 0, 0, "startdate", DataRowVersion.Current, null);
			if(startdate!= Constants.DateMax)
				parms[0].Value = startdate;
			parms[1]= new System.Data.SqlClient.SqlParameter("@enddate", System.Data.SqlDbType.SmallDateTime, 8, System.Data.ParameterDirection.Input, true, 0, 0, "enddate", DataRowVersion.Current, null);
			if(enddate!= Constants.DateMax)
				parms[1].Value = enddate;
			parms[2]= new System.Data.SqlClient.SqlParameter("@projectid", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.Input, true, 10, 0, "projectid", DataRowVersion.Current, null);
			if(projectid>0)
				parms[2].Value = projectid;
			parms[3]= new System.Data.SqlClient.SqlParameter("@sort", System.Data.SqlDbType.NVarChar, 50, System.Data.ParameterDirection.Input, true, 10, 0, "sort", DataRowVersion.Current, null);
			if(sort !=null && sort!="")
				parms[3].Value = sort;
			parms[4]= new System.Data.SqlClient.SqlParameter("@IsActive", System.Data.SqlDbType.Bit, 1, System.Data.ParameterDirection.Input, true, 0, 0, "IsActive", DataRowVersion.Current, null);
			parms[5]= new System.Data.SqlClient.SqlParameter("@Concluded", System.Data.SqlDbType.Bit, 1, System.Data.ParameterDirection.Input, true, 0, 0, "Concluded", DataRowVersion.Current, null);
			switch(projectStatus)
			{
				case (int)ProjectsData.ProjectsByStatus.Active:{parms[4].Value=true;parms[5].Value=false; break;}
				case (int)ProjectsData.ProjectsByStatus.Passive:{parms[4].Value=false;parms[5].Value=false; break;}
                case (int)ProjectsData.ProjectsByStatus.Conluded:{parms[5].Value=true; break;}
			}
			parms[6]= new System.Data.SqlClient.SqlParameter("@BuildingType", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.Input, true, 10, 0, "BuildingType", DataRowVersion.Current, null);
			if(buildingType>0)
				parms[6].Value = buildingType;
			return parms;
		}
		public static SqlParameter[] CreateProjectProtokolsSelByProtocolProc (int ProtokolID)
		{
			SqlParameter[] parms = new SqlParameter[1];
			parms[0]= new System.Data.SqlClient.SqlParameter("@ProtokolID", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.Input, true, 10, 0, "ProtokolID", DataRowVersion.Current, null);
			parms[0].Value = ProtokolID;

			return parms;
		}
		public static SqlParameter[] CreateProjectsSelProc (int ProjectID)
		{
			SqlParameter[] parms = new SqlParameter[1];
			parms[0]= new System.Data.SqlClient.SqlParameter("@ProjectID", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.Input, true, 10, 0, "ProjectID", DataRowVersion.Current, null);
			parms[0].Value = ProjectID;

			return parms;
		}
		public static SqlParameter[] CreateMeetingDocumentsSelByMeetingProc (int MeetingID)
		{
			SqlParameter[] parms = new SqlParameter[1];
			parms[0]= new System.Data.SqlClient.SqlParameter("@MeetingID", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.Input, true, 10, 0, "MeetingID", DataRowVersion.Current, null);
			parms[0].Value = MeetingID;

			return parms;
		}
		public static SqlParameter[] CreateMeetingProjectsSelByProjectsProc (int ProjectID)
		{
			SqlParameter[] parms = new SqlParameter[1];
			parms[0]= new System.Data.SqlClient.SqlParameter("@ProjectID", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.Input, true, 10, 0, "ProjectID", DataRowVersion.Current, null);
			parms[0].Value = ProjectID;

			return parms;
		}
		public static SqlParameter[] CreateMeetingProjectsSelByMeetingProc (int MeetingID)
		{
			SqlParameter[] parms = new SqlParameter[1];
			parms[0]= new System.Data.SqlClient.SqlParameter("@MeetingID", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.Input, true, 10, 0, "MeetingID", DataRowVersion.Current, null);
			parms[0].Value = MeetingID;

			return parms;
		}
		public static SqlParameter[] CreateProjectUsersSelByProjectProc (int ProjectID)
		{
			SqlParameter[] parms = new SqlParameter[1];
			parms[0]= new System.Data.SqlClient.SqlParameter("@ProjectID", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.Input, true, 10, 0, "ProjectID", DataRowVersion.Current, null);
			parms[0].Value = ProjectID;

			return parms;
		}
		public static SqlParameter[] CreateSalariesListProc (int UserID)
		{
			SqlParameter[] parms = new SqlParameter[1];
			parms[0]= new System.Data.SqlClient.SqlParameter("@UserID", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.Input, true, 10, 0, "UserID", DataRowVersion.Current, null);
			parms[0].Value = UserID;

			return parms;
		}

		public static SqlParameter[] CreateProjectDocumentsListProc (int ProjectID, int DocumentType)
		{
			SqlParameter[] parms = new SqlParameter[2];
			parms[0]= new System.Data.SqlClient.SqlParameter("@ProjectID", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.Input, true, 10, 0, "ProjectID", DataRowVersion.Current, null);

			parms[0].Value = ProjectID;
			parms[1]= new System.Data.SqlClient.SqlParameter("@DocumentType", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.Input, true, 10, 0, "DocumentType", DataRowVersion.Current, null);
			if(DocumentType>-1)
				parms[1].Value = DocumentType;

			return parms;
		}

		public static SqlParameter[] CreateProjectDocumentsListForPMProc (int ProjectID, bool IsSub,bool IsClient, bool IsBuilder)
		{
			SqlParameter[] parms = new SqlParameter[4];
			parms[0]= new System.Data.SqlClient.SqlParameter("@ProjectID", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.Input, true, 10, 0, "ProjectID", DataRowVersion.Current, null);
			parms[1]= new System.Data.SqlClient.SqlParameter("@IsSubcontracter", System.Data.SqlDbType.Bit, 1, System.Data.ParameterDirection.Input, true, 0, 0, "IsSubcontracter", DataRowVersion.Current, null);
			parms[2]= new System.Data.SqlClient.SqlParameter("@IsClient", System.Data.SqlDbType.Bit, 1, System.Data.ParameterDirection.Input, true, 0, 0, "IsClient", DataRowVersion.Current, null);
			parms[3]= new System.Data.SqlClient.SqlParameter("@IsBuilder", System.Data.SqlDbType.Bit, 1, System.Data.ParameterDirection.Input, true, 0, 0, "IsBuilder", DataRowVersion.Current, null);
			
			parms[0].Value = ProjectID;
			if(IsSub)
				parms[1].Value = true;
			if(IsClient)
				parms[2].Value = true;
			if(IsBuilder)
				parms[3].Value = true;

//			parms[1]= new System.Data.SqlClient.SqlParameter("@DocumentType", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.Input, true, 10, 0, "DocumentType", DataRowVersion.Current, null);
//			if(DocumentType>-1)
//				parms[1].Value = DocumentType;

			return parms;
		}
		
		public static SqlParameter[] CreateMeetingsListProc (DateTime startdate, DateTime enddate, int projectid, int clientid,string sort)
		{
			SqlParameter[] parms = new SqlParameter[5];
			parms[0]= new System.Data.SqlClient.SqlParameter("@startdate", System.Data.SqlDbType.SmallDateTime, 8, System.Data.ParameterDirection.Input, true, 0, 0, "startdate", DataRowVersion.Current, null);
			if(startdate!= Constants.DateMax)
				parms[0].Value = startdate;
			parms[1]= new System.Data.SqlClient.SqlParameter("@enddate", System.Data.SqlDbType.SmallDateTime, 8, System.Data.ParameterDirection.Input, true, 0, 0, "enddate", DataRowVersion.Current, null);
			if(enddate!= Constants.DateMax)
				parms[1].Value = enddate;
			parms[2]= new System.Data.SqlClient.SqlParameter("@projectid", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.Input, true, 10, 0, "projectid", DataRowVersion.Current, null);
			if(projectid>0)
				parms[2].Value = projectid;
			parms[3]= new System.Data.SqlClient.SqlParameter("@clientid", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.Input, true, 10, 0, "clientid", DataRowVersion.Current, null);
			if(clientid>0)
				parms[3].Value = clientid;
			parms[4]= new System.Data.SqlClient.SqlParameter("@sort", System.Data.SqlDbType.NVarChar, 50, System.Data.ParameterDirection.Input, true, 10, 0, "sort", DataRowVersion.Current, null);
			if(sort !=null && sort!="")
				parms[4].Value = sort;
			
			return parms;
		}


		public static SqlParameter[] CreateContentsList0Proc (int ProjectID)
		{
			SqlParameter[] parms = new SqlParameter[1];
			parms[0]= new System.Data.SqlClient.SqlParameter("@ProjectID", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.Input, true, 10, 0, "ProjectID", DataRowVersion.Current, null);
			parms[0].Value = ProjectID;

			return parms;
		}

		public static SqlParameter[] CreateContentsListAllProc (int ProjectID)
		{
			SqlParameter[] parms = new SqlParameter[1];
			parms[0]= new System.Data.SqlClient.SqlParameter("@ProjectID", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.Input, true, 10, 0, "ProjectID", DataRowVersion.Current, null);
			parms[0].Value = ProjectID;

			return parms;
		}
		public static SqlParameter[] CreateContentsListFilterProc (int ProjectID,int Filter)
		{
			SqlParameter[] parms = new SqlParameter[2];
			parms[0]= new System.Data.SqlClient.SqlParameter("@ProjectID", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.Input, true, 10, 0, "ProjectID", DataRowVersion.Current, null);
			parms[0].Value = ProjectID;
			parms[1]= new System.Data.SqlClient.SqlParameter("@Filter", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.Input, true, 10, 0, "Filter", DataRowVersion.Current, null);
			parms[1].Value = Filter;

			return parms;
		}
		public static SqlParameter[] CreatePaymentsListByAuthorsProc (int AuthorsID)
		{
			SqlParameter[] parms = new SqlParameter[1];
			parms[0]= new System.Data.SqlClient.SqlParameter("@AuthorsID", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.Input, true, 10, 0, "AuthorsID", DataRowVersion.Current, null);
			parms[0].Value = AuthorsID;

			return parms;
		}

		public static SqlParameter[] CreatePaymentsListBySubcontracterProc (int SubcontracterID)
		{
			SqlParameter[] parms = new SqlParameter[1];
			parms[0]= new System.Data.SqlClient.SqlParameter("@SubcontracterID", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.Input, true, 10, 0, "SubcontracterID", DataRowVersion.Current, null);
			parms[0].Value = SubcontracterID;

			return parms;
		}
		public static SqlParameter[] CreateProjectSubcontracterActivitiesListProc (int ProjectSubcontracterID)
		{
			SqlParameter[] parms = new SqlParameter[1];
			parms[0]= new System.Data.SqlClient.SqlParameter("@ProjectSubcontracterID", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.Input, true, 10, 0, "ProjectSubcontracterID", DataRowVersion.Current, null);
			parms[0].Value = ProjectSubcontracterID;

			return parms;
		}



		public static SqlParameter[] CreateSubprojectsListProc (int ProjectID)
		{
			SqlParameter[] parms = new SqlParameter[1];
			parms[0]= new System.Data.SqlClient.SqlParameter("@ProjectID", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.Input, true, 10, 0, "ProjectID", DataRowVersion.Current, null);
			parms[0].Value = ProjectID;

			return parms;
		}
		public static SqlParameter[] CreatePaymentsListBySubprojectProc (int SubprojectID)
		{
			SqlParameter[] parms = new SqlParameter[1];
			parms[0]= new System.Data.SqlClient.SqlParameter("@SubprojectID", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.Input, true, 10, 0, "SubprojectID", DataRowVersion.Current, null);
			parms[0].Value = SubprojectID;

			return parms;
		}

		public static SqlParameter[] CreateProjectSubcontractersListProc (int ProjectID)
		{
			SqlParameter[] parms = new SqlParameter[1];
			parms[0]= new System.Data.SqlClient.SqlParameter("@ProjectID", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.Input, true, 10, 0, "ProjectID", DataRowVersion.Current, null);
			parms[0].Value = ProjectID;

			return parms;
		}
		public static SqlParameter[] CreateProjectSubcontractersListProc1 (int ProjectID, bool HasPaymentRight)
		{
			SqlParameter[] parms = new SqlParameter[2];
			parms[0]= new System.Data.SqlClient.SqlParameter("@ProjectID", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.Input, true, 10, 0, "ProjectID", DataRowVersion.Current, null);
			parms[0].Value = ProjectID;
			parms[1]= new System.Data.SqlClient.SqlParameter("@HasPaymentRight", System.Data.SqlDbType.Bit, 1, System.Data.ParameterDirection.Input, true, 0, 0, "HasPaymentRight", DataRowVersion.Current, null);
			parms[1].Value = HasPaymentRight;
			
			return parms;
		}



		public static SqlParameter[] CreateSubcontractersListProc1 (int ProjectID, bool HasPaymentRight)
		{
			SqlParameter[] parms = new SqlParameter[5];
			parms[0]= new System.Data.SqlClient.SqlParameter("@SubcontracterTypeID", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.Input, true, 10, 0, "SubcontracterTypeID", DataRowVersion.Current, null);
			parms[1]= new System.Data.SqlClient.SqlParameter("@SortOrder", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.Input, true, 10, 0, "SortOrder", DataRowVersion.Current, null);
			parms[2]= new System.Data.SqlClient.SqlParameter("@HasPaymentRight", System.Data.SqlDbType.Bit, 1, System.Data.ParameterDirection.Input, true, 0, 0, "HasPaymentRight", DataRowVersion.Current, null);
			parms[3]= new System.Data.SqlClient.SqlParameter("@ProjectID", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.Input, true, 10, 0, "ProjectID", DataRowVersion.Current, null);
			parms[4]= new System.Data.SqlClient.SqlParameter("@ShowHide", System.Data.SqlDbType.Bit, 1, System.Data.ParameterDirection.Input, true, 0, 0, "ShowHide", DataRowVersion.Current, null);
			if(ProjectID>-1)
				parms[3].Value = ProjectID;
			parms[2].Value = HasPaymentRight;
			parms[4].Value = false;
			parms[1].Value = 2;
			return parms;
		}



	}
}
