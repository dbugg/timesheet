using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Data.SqlClient;
using log4net;
using Asa.Timesheet.Data;
using Asa.Timesheet.Data.Entities;
using Asa.Timesheet.Data.Util;
using GrapeCity.ActiveReports.Export.Pdf.Section;
using GrapeCity.ActiveReports.Export.Excel.Section;

namespace Asa.Timesheet.WebPages.Reports
{
	/// <summary>
	/// Summary description for WorkTimes.
	/// </summary>
	public class WorkTimes : TimesheetPageBase
	{
		private static int MAX_YEAR=2040;
		private static readonly ILog log = LogManager.GetLogger(typeof(WorkTimes));
		private enum LateColumns
		{
			UserID,
			Fullname,
			Date,
			StartTime,
			EndTime,
			LateMinutes,
			Details,
			TotalMinutes
		}
		private enum ReportTypes
		{
			Normal=1,
			OverTime,
			Trainees
		}
		#region Web controls

		protected System.Web.UI.WebControls.Label lblTitle;
		protected System.Web.UI.WebControls.Label lblUser;
		protected System.Web.UI.WebControls.PlaceHolder menuHolder;
		protected System.Web.UI.WebControls.Label lblSluj;
		protected System.Web.UI.WebControls.Label lblProject;
		protected System.Web.UI.WebControls.Label lblActivity;
		protected System.Web.UI.WebControls.Label lblBuildingType;
		protected System.Web.UI.WebControls.DropDownList ddlUser;
		protected System.Web.UI.WebControls.DropDownList ddlProject;
		protected System.Web.UI.WebControls.DropDownList ddlActivity;
		protected System.Web.UI.WebControls.DropDownList ddlBuildingType;
		protected System.Web.UI.WebControls.DropDownList ddlPeriodType;
		protected System.Web.UI.WebControls.Label lblStartDay;
		protected System.Web.UI.WebControls.Label lblEndDay;
		protected System.Web.UI.WebControls.Calendar calStartDate;
		protected System.Web.UI.WebControls.Calendar calEndDate;
		protected System.Web.UI.WebControls.Label lblSelStartDate;
		protected System.Web.UI.WebControls.Label lblSelEndDate;
		protected System.Web.UI.WebControls.Label lblError;
		protected System.Web.UI.WebControls.Label lblInfo;
		protected System.Web.UI.WebControls.ImageButton ibGenerate;
		protected System.Web.UI.WebControls.ImageButton ibPdfExport;
		protected System.Web.UI.WebControls.ImageButton ibXlsExport;
		protected System.Web.UI.WebControls.DropDownList ddlUserOrder;
		protected System.Web.UI.WebControls.DropDownList ddlProjectOrder;
		protected System.Web.UI.WebControls.DropDownList ddlActivityOrder;
		protected System.Web.UI.WebControls.DropDownList ddlBuildingTypeOrder;
		protected Asa.Timesheet.WebPages.UserControls.PageHeader header;
		protected System.Web.UI.WebControls.CheckBox ckDates;
		protected System.Web.UI.WebControls.DataGrid grdReport;
		protected System.Web.UI.WebControls.Label lblProektant;
		protected System.Web.UI.WebControls.Label Label6;
		protected System.Web.UI.WebControls.Label lblClientAddress;
		protected System.Web.UI.WebControls.Label lblClient;
		protected System.Web.UI.WebControls.Label Label3;
		protected System.Web.UI.WebControls.Label lblObekt;
		protected System.Web.UI.WebControls.Label Label1;
		protected System.Web.UI.HtmlControls.HtmlTable tblHeader;
		protected System.Web.UI.WebControls.Label lblReportTitle;
		protected System.Web.UI.WebControls.Label lblCreatedBy;
		protected System.Web.UI.HtmlControls.HtmlTable tblFooter;
		protected System.Web.UI.HtmlControls.HtmlTable tblFooter1;
		protected System.Web.UI.WebControls.Label Label2;
		protected System.Web.UI.WebControls.Label Label9;
		protected System.Web.UI.WebControls.Label Label10;
		protected System.Web.UI.WebControls.Label Label11;
		protected System.Web.UI.WebControls.Label Label12;
		protected System.Web.UI.WebControls.Label lblTotalHours;
		protected System.Web.UI.WebControls.Label lblDaysPO;
		protected System.Web.UI.WebControls.Label lblDaysNO;
		protected System.Web.UI.WebControls.Label lblDaysIll;
		protected System.Web.UI.WebControls.Label lblCreatedByLbl;
		protected System.Web.UI.WebControls.Label lblDaysPOString;
		protected System.Web.UI.WebControls.Label lblDaysIllString;
		protected System.Web.UI.WebControls.Label lblDaysNOString;
		protected System.Web.UI.WebControls.Label Label4;
		protected System.Web.UI.WebControls.Label Label13;
		protected System.Web.UI.WebControls.Label Label14;
		protected System.Web.UI.WebControls.Label lblTotalWorkDays;
		protected System.Web.UI.WebControls.Label lblTotalNonWorkDays;
		protected System.Web.UI.WebControls.Label Label15;
		protected System.Web.UI.WebControls.Label Label16;
		protected System.Web.UI.WebControls.Label Label17;
		protected System.Web.UI.WebControls.Label lblReportUser;
		protected System.Web.UI.WebControls.Label lblReportActivity;
		protected System.Web.UI.WebControls.Label Label18;
		protected System.Web.UI.WebControls.Label lblPeriod;
		protected System.Web.UI.WebControls.Label lblNoDataFound;
	   protected System.Web.UI.WebControls.DropDownList ddlProjectStatus;
	   protected System.Web.UI.WebControls.Label Label19;
		protected System.Web.UI.WebControls.Label lbSalary1;
		protected System.Web.UI.WebControls.Label lbSalary2;
		protected System.Web.UI.WebControls.DropDownList ddlUserStatus;
		protected System.Web.UI.WebControls.DataGrid grdUsers;
		protected System.Web.UI.WebControls.Label lbShowExtraGrid;
		protected System.Web.UI.WebControls.CheckBox cbShowExtraGrid;
		protected System.Web.UI.WebControls.Label lbNotWorkTime;
		protected System.Web.UI.WebControls.CheckBox cbNotWorkTime;
		protected System.Web.UI.WebControls.Label Label20;
		protected System.Web.UI.WebControls.DropDownList ddlMonth;
		protected System.Web.UI.WebControls.Label lbMonth;
		protected System.Web.UI.HtmlControls.HtmlTable table;
		protected System.Web.UI.HtmlControls.HtmlTable tblDays;
		protected System.Web.UI.HtmlControls.HtmlTable tblHeadFooter;
		protected System.Web.UI.WebControls.TextBox txtCoef;
		protected System.Web.UI.WebControls.Label lbCoef;
		protected System.Web.UI.WebControls.DataGrid grdOneEmployee;
		protected System.Web.UI.WebControls.CheckBox ckLate;
		protected System.Web.UI.WebControls.DataGrid grdLate;
		protected System.Web.UI.WebControls.Label lbShowLate;
		protected System.Web.UI.WebControls.Label lbTotalLateCount;
		protected System.Web.UI.WebControls.Label Label22;
		protected System.Web.UI.WebControls.Label lbTotalLate;
		protected System.Web.UI.HtmlControls.HtmlTable tblFilter;
		protected System.Web.UI.WebControls.Label Label21;
		protected System.Web.UI.WebControls.Label lblMissingHours;
		protected System.Web.UI.WebControls.Label lblReportBuildingType;
		public const int  MINUTES_IN_HOUR = 60;
		int nTotalTime=0;
		int nOverTime=0;
		int nUnderTime=0;
		int nWorkHours=0;
		#endregion

		private bool IsTrainees
		{
			get
			{
				return UIHelpers.GetIDParam()==(int)ReportTypes.Trainees;
			}

		}
		private bool IsOverTime
		{
			get
			{
				return UIHelpers.GetIDParam()==(int)ReportTypes.OverTime;
			}

		}
		
		#region Page load

        private void Page_Load(object sender, System.EventArgs e)
        {
            ckDates.Visible = true;
            grdOneEmployee.Visible = false;
            lbShowExtraGrid.Visible = cbShowExtraGrid.Visible = false;
            lbNotWorkTime.Visible = cbNotWorkTime.Visible = false;
            if (this.LoggedUser.HasPaymentRights)
            {

                lbShowExtraGrid.Visible = cbShowExtraGrid.Visible = true;
                tblFilter.Visible = true;
                ibXlsExport.Visible = ibPdfExport.Visible = true;
                //				if(UIHelpers.GetIDParam() != Constants._WorktimesID_OverTime)
                //					lbNotWorkTime.Visible = cbNotWorkTime.Visible = true;
            }
            else
            {

                tblFilter.Visible = false;
                ibXlsExport.Visible = ibPdfExport.Visible = false;

            }

            if (UIHelpers.GetIDParam() == (int)ReportTypes.Normal)
            {
                header.PageTitle = Resource.ResourceManager["reports_WorkTimes_Title"];
                lbShowLate.Visible = ckLate.Visible = true;

            }
            else if (IsOverTime)
            {
                lbShowLate.Visible = ckLate.Visible = false;
                header.PageTitle = lblReportTitle.Text = Resource.ResourceManager["reports_WorkTimesOvertime_Title"];
                lblReportTitle.Visible = true;
                cbShowExtraGrid.Visible = false;
                cbShowExtraGrid.Checked = true;
                ckDates.Checked = false;
                ckDates.Visible = false;
                lbShowExtraGrid.Visible = false;
                if (this.LoggedUser.HasPaymentRights)
                    lbCoef.Visible = txtCoef.Visible = true;
                else
                    lbCoef.Visible = txtCoef.Visible = false;


            }

            if (!this.IsPostBack)
            {
                if (IsOverTime)
                    txtCoef.Text = System.Configuration.ConfigurationManager.AppSettings["CoefOvertime"];
                header.UserName = this.LoggedUser.UserName;


                UIHelpers.LoadProjectStatusWithoutConcluded(ddlProjectStatus, (int)ProjectsData.ProjectsByStatus.AllProjects);
                if (!(LoadBuildingTypes() && LoadUsers("0") &&
                LoadActivities() && LoadProjects())) lblError.Text = "Error loading report controls.";

                ddlProjectOrder.Attributes["onchange"] = "SetGroupOrder()";
                ddlActivityOrder.Attributes["onchange"] = "SetGroupOrder()";
                ddlBuildingTypeOrder.Attributes["onchange"] = "SetGroupOrder()";
                ddlUserOrder.Attributes["onchange"] = "SetGroupOrder()";

                calStartDate.SelectedDate = calStartDate.VisibleDate = new DateTime(DateTime.Today.Year, DateTime.Today.Month, 1);
                lblSelStartDate.Text = "(" + Resource.ResourceManager["reports_SelectDate"] + ")";
                calEndDate.SelectedDate = calEndDate.VisibleDate = DateTime.Today;
                lblSelEndDate.Text = "(" + Resource.ResourceManager["reports_SelectDate"] + ")";
                //edited by Ivailo date:13.12.2007
                //noted:when the page is loaded, by default is not for all period
                ckDates.Checked = false;
                calStartDate.Enabled = calEndDate.Enabled = true;

                //notes:set checkbox for less worktime to be visible only when is worktime 
                //	not overtime


            }
            //else
            //	UIHelpers.LoadBuildingTypes("",ddlBuildingType,(ProjectsData.ProjectsByStatus) int.Parse(ddlProjectStatus.SelectedValue));

            tblHeader.EnableViewState = false;
            tblFooter.EnableViewState = false;
            tblFooter1.EnableViewState = false;
            tblHeader.Visible = false;
            tblFooter.Visible = false;
            tblHeadFooter.Visible = false;
            tblFooter1.Visible = false;
            UIHelpers.CreateMenuReports(menuHolder, LoggedUser);



            if (IsTrainees)
            {
                lblSluj.Text = Resource.ResourceManager["menuItem_TraineeReport"];
                header.PageTitle = Resource.ResourceManager["menuItem_TraineeReport"];
                ddlUserStatus.Visible = false;
                //cbNotWorkTime.Visible=lbNotWorkTime.Visible= false;
                //if(!IsPostBack)
                lblReportTitle.Text += " - " + Resource.ResourceManager["menuItem_TraineeReport"].ToUpper();
                if (!IsPostBack)
                {
                    LoadMonths();
                    cbShowExtraGrid.Checked = true;
                }
                tblDays.Visible = false;
                ddlMonth.Visible = lbMonth.Visible = true;
                ckDates.Visible = false;
            }
            else
            {

                ddlMonth.Visible = lbMonth.Visible = false;
            }
        }

		#endregion

		#region Web Form Designer generated code
        override protected void OnInit(EventArgs e)
        {
            //
            // CODEGEN: This call is required by the ASP.NET Web Form Designer.
            //
            InitializeComponent();
            base.OnInit(e);
        }
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
        private void InitializeComponent()
        {
            this.ckDates.CheckedChanged += new System.EventHandler(this.ckDates_CheckedChanged);
            this.calStartDate.SelectionChanged += new System.EventHandler(this.calStartDate_SelectionChanged);
            this.calEndDate.SelectionChanged += new System.EventHandler(this.calEndDate_SelectionChanged);
            this.ddlProjectStatus.SelectedIndexChanged += new System.EventHandler(this.ddlProjectStatus_SelectedIndexChanged);
            this.ddlBuildingType.SelectedIndexChanged += new System.EventHandler(this.ddlBuildingType_SelectedIndexChanged);
            this.ddlUserStatus.SelectedIndexChanged += new System.EventHandler(this.ddlUserStatus_SelectedIndexChanged);
            this.ibGenerate.Click += new System.Web.UI.ImageClickEventHandler(this.ibGenerate_Click);
            this.ibPdfExport.Click += new System.Web.UI.ImageClickEventHandler(this.ibPdfExport_Click);
            this.ibXlsExport.Click += new System.Web.UI.ImageClickEventHandler(this.ibXlsExport_Click);
            this.grdReport.ItemDataBound += new System.Web.UI.WebControls.DataGridItemEventHandler(this.grdReport_ItemDataBound);
            this.grdLate.ItemDataBound += new System.Web.UI.WebControls.DataGridItemEventHandler(this.grdLate_ItemDataBound);
            this.grdOneEmployee.ItemDataBound += new System.Web.UI.WebControls.DataGridItemEventHandler(this.grdOneEmployee_ItemDataBound);
            this.grdUsers.ItemCreated += new System.Web.UI.WebControls.DataGridItemEventHandler(this.grdUsers_ItemCreated);
            this.grdUsers.ItemDataBound += new System.Web.UI.WebControls.DataGridItemEventHandler(this.grdUsers_ItemDataBound);
            this.Load += new System.EventHandler(this.Page_Load);

        }
		#endregion

		#region Database

        private bool LoadBuildingTypes()
        {
            bool returnValue = UIHelpers.LoadBuildingTypes(ddlBuildingType);
            //			SqlDataReader reader = null;
            //			try
            //			{
            //				reader = DBManager.SelectBuildingTypes();
            //				ddlBuildingType.DataSource = reader;
            //				ddlBuildingType.DataValueField = "BuildingTypeID";
            //				ddlBuildingType.DataTextField = "BuildingType";
            //				ddlBuildingType.DataBind();
            //
            //				ddlBuildingType.Items.Insert(0, new ListItem("<"+Resource.ResourceManager["reports_ddlAllBuildingTypes"]+">", "0"));
            ddlBuildingType.Items.Insert(0, new ListItem("<" + Resource.ResourceManager["reports_ExcludeFromReport"] + ">", "-1"));
            //				ddlBuildingType.SelectedValue = "0";
            //			}
            //			catch (Exception ex)
            //			{
            //				log.Info(ex);
            //				return false;
            //			}
            //			finally 
            //			{
            //				if (reader!=null) reader.Close();
            //			}
            //
            return returnValue;
        }

        private bool LoadUsers(string selectedValue)
        {
            if (this.LoggedUser.IsLeader || this.LoggedUser.IsAccountant || LoggedUser.IsASI)
            {
                ddlUser.Visible = true;
                lblSluj.Visible = true;

                SqlDataReader reader = null;
                try
                {
                    if (IsTrainees)
                        reader = UsersData.SelectUserNamesTrainees(false);
                    else if (LoggedUser.IsASI)
                        reader = UsersData.SelectUserNamesConstruct(true);
                    else
                        switch (ddlUserStatus.SelectedValue)
                        {
                            case "0":
                                reader = UsersData.SelectUserNames(true, false); break;
                            case "1":
                                reader = UsersData.SelectUserNames(false, false); break;
                            case "2":
                                reader = UsersData.SelectUserNames1(); break;
                        }

                    ddlUser.DataSource = reader;
                    ddlUser.DataValueField = "UserID";
                    ddlUser.DataTextField = "FullName";
                    ddlUser.DataBind();

                    //					if(ddlUser.Items.FindByValue(this.LoggedUser.UserID.ToString())==null)
                    //						ddlUser.Items.Insert(0, new ListItem(this.LoggedUser.FullName,this.LoggedUser.UserID.ToString()));
                    if (!LoggedUser.IsASI)
                    {
                        if (IsTrainees)
                            ddlUser.Items.Insert(0, new ListItem("<" + Resource.ResourceManager["reports_ddlAllTrainees"] + ">", "0"));
                        else
                            ddlUser.Items.Insert(0, new ListItem("<" + Resource.ResourceManager["reports_ddlAllUsers"] + ">", "0"));
                        ddlUser.Items.Insert(0, new ListItem("<" + Resource.ResourceManager["reports_ExcludeFromReport"] + ">", "-1"));
                        ddlUser.SelectedValue = selectedValue;
                    }
                    else
                        ddlUserStatus.Visible = false;
                }
                catch (Exception ex)
                {
                    log.Info(ex);
                    return false;
                }
                finally
                {
                    if (reader != null) reader.Close();
                }
            }
            else
            {
                //	lblSluj.Visible = false;
                //	ddlUser.Visible = false;
                //	ddlUserOrder.Visible = false;
                ddlUser.Enabled = false;
                ddlUser.Items.Insert(0, new ListItem(this.LoggedUser.FullName, this.LoggedUser.UserID.ToString()));
                ddlUser.SelectedIndex = 0;
                ddlUserStatus.Visible = false;
            }

            return true;
        }

        private bool LoadActivities()
        {
            SqlDataReader reader = null;

            try
            {
                reader = DBManager.SelectActivities();

                ddlActivity.DataSource = reader;
                ddlActivity.DataValueField = "ActivitityID";
                ddlActivity.DataTextField = "ActivityName";
                ddlActivity.DataBind();

                ddlActivity.Items.Insert(0, new ListItem("<" + Resource.ResourceManager["reports_ddlAllActivities"] + ">", "0"));
                ddlActivity.Items.Insert(0, new ListItem("<" + Resource.ResourceManager["reports_ExcludeFromReport"] + ">", "-1"));
                ddlActivity.SelectedValue = "0";
            }
            catch (Exception ex)
            {
                log.Error(ex);
                return false;
            }
            finally
            {
                if (reader != null) reader.Close();
            }

            return true;
        }
		public ProjectsData.ProjectsByStatus ProjectsStatus
		{
			get
			{
				try
				{
					return  (ProjectsData.ProjectsByStatus)int.Parse(ddlProjectStatus.SelectedValue);
				}
				catch { return ProjectsData.ProjectsByStatus.AllProjects; }
			}
		}

        private bool LoadProjects()
        {
            SqlDataReader reader = null;

            //int projectStatus = int.Parse(ddlProjectStatus.SelectedValue);

            try
            {
                //reader = ProjectsData.SelectProjectNames(projectStatus);
                reader = ProjectsData.SelectProjectNamesClear(ProjectsStatus, int.Parse(ddlBuildingType.SelectedValue));

                ddlProject.DataSource = reader;
                ddlProject.DataValueField = "ProjectID";
                ddlProject.DataTextField = "ProjectName";
                ddlProject.DataBind();

                ddlProject.Items.Insert(0, new ListItem("<" + Resource.ResourceManager["reports_ddlAllProjects"] + ">", "0"));
                ddlProject.Items.Insert(0, new ListItem("<" + Resource.ResourceManager["reports_ExcludeFromReport"] + ">", "-1"));
                ddlProject.SelectedValue = "0";
            }
            catch (Exception ex)
            {
                log.Error(ex);

                throw ex;



            }

            finally
            {
                if (reader != null) reader.Close();
            }
            return true;
        }
        private void LoadMonths()
        {
            int selectedIndex = 0;
            int startYear = UIHelpers.ToInt(System.Configuration.ConfigurationManager.AppSettings["MinSalaryYear"]);
            for (int iyear = startYear; iyear <= MAX_YEAR; iyear++)
            {
                int _monts = 13;
                if (iyear == DateTime.Now.Year)
                    _monts = DateTime.Now.Month + 1;
                for (int iMonth = 1; iMonth < _monts; iMonth++)
                {
                    int ItemValue = (iyear - startYear) * 12 + iMonth;
                    ListItem li = new ListItem(GetMonthName(iMonth, iyear), ItemValue.ToString());
                    ddlMonth.Items.Add(li);
                    if (iyear == DateTime.Now.Year && iMonth == DateTime.Now.Month)
                        selectedIndex = ddlMonth.Items.IndexOf(li);
                }

            }
            ddlMonth.SelectedIndex = selectedIndex;

        }

        private string GetMonthName(int month, int year)
        {
            string smonth = "";
            switch (month)
            {
                case (1): { smonth = Resource.ResourceManager["MonthYanuary"]; break; }
                case (2): { smonth = Resource.ResourceManager["MonthFebruary"]; break; }
                case (3): { smonth = Resource.ResourceManager["MonthMarch"]; break; }
                case (4): { smonth = Resource.ResourceManager["MonthApril"]; break; }
                case (5): { smonth = Resource.ResourceManager["MonthMay"]; break; }
                case (6): { smonth = Resource.ResourceManager["MonthYuny"]; break; }
                case (7): { smonth = Resource.ResourceManager["MonthYuly"]; break; }
                case (8): { smonth = Resource.ResourceManager["MonthAugust"]; break; }
                case (9): { smonth = Resource.ResourceManager["MonthSeptember"]; break; }
                case (10): { smonth = Resource.ResourceManager["MonthOctober"]; break; }
                case (11): { smonth = Resource.ResourceManager["MonthNovember"]; break; }
                case (12): { smonth = Resource.ResourceManager["MonthDecember"]; break; }

            }
            return string.Format(Resource.ResourceManager["DateMonthAndYear"], smonth, year.ToString());
        }

        private DataSet LoadReportData(DateTime startDate, DateTime endDate,
                                        string selectExpr, string whereExpr, string groupByExpr, string orderByExpr, int overTime)
        {
            DataSet ds = null;
            try
            {
                if (IsTrainees)
                {
                    string val = ddlMonth.SelectedValue;
                    int ival = UIHelpers.ToInt(val);

                    int iYear = UIHelpers.ToInt(System.Configuration.ConfigurationManager.AppSettings["MinSalaryYear"]) + ival / 12;
                    int iMonth = ival % 12;
                    startDate = new DateTime(iYear, iMonth, 1);
                    endDate = new DateTime(iYear, iMonth, 1).AddMonths(1).AddDays(-1);
                    lblPeriod.Text = String.Concat(startDate.ToString("d MMM yyyy"), " ",
                        Resource.ResourceManager["reports_YearAbbr"], " - ",
                        endDate.ToString("d MMM yyyy"), " ",
                        Resource.ResourceManager["reports_YearAbbr"]);
                }
                if (overTime != Constants._WorktimesID_OverTime)
                    ds = ReportsData.SelectWorkTimesReport(startDate, endDate, selectExpr, whereExpr, groupByExpr, orderByExpr, overTime, IsTrainees);
            }
            catch (Exception ex)
            {
                log.Error(ex);
                return null;
            }

            return ds;
        }

        private HeaderInfo GetHeaderData()
        {
            int projectID = int.Parse(ddlProject.SelectedValue);

            HeaderInfo hi = new HeaderInfo();

            #region load report header data

            if (projectID > 0)
            {
                SqlDataReader reader = null;
                try
                {
                    reader = ReportsData.SelectReportHeaderInfo(projectID);
                    if (reader.Read())
                    {
                        hi.ProjectName = reader.GetString(1);
                        hi.ProjectCode = reader.IsDBNull(2) ? String.Empty : reader.GetString(2);
                        hi.ProjectAdminName = reader.IsDBNull(3) ? String.Empty : reader.GetString(3);

                        hi.ClientName = reader.IsDBNull(5) ? String.Empty : reader.GetString(5);
                        hi.ClientCity = reader.IsDBNull(6) ? String.Empty : reader.GetString(6);
                        hi.ClientAddress = reader.IsDBNull(7) ? String.Empty : reader.GetString(7);
                    }
                    else
                    {
                        lblInfo.Text = Resource.ResourceManager["reports_DataNotFound"];
                        return null;
                    }
                }
                catch (Exception ex)
                {
                    log.Error(ex);
                    lblInfo.Text = Resource.ResourceManager["reports_ErrorLoadData"];
                    return null;
                }
                finally
                {
                    if (reader != null) reader.Close();
                }
            }
            else
                if (projectID == -1)
                {
                    hi.ProjectAdminName = String.Empty;
                    hi.ProjectName = String.Empty;
                }
                else hi.ProjectName = Resource.ResourceManager["reports_All"];

            #endregion

            if (ddlUser.SelectedValue == "-1") hi.User = String.Empty;
            else
                if (ddlUser.SelectedValue == "0") hi.User = Resource.ResourceManager["reports_All"];
                else
                {
                    hi.User = ddlUser.SelectedItem.Text;
                    int userID = int.Parse(ddlUser.SelectedValue);
                    if (!this.LoggedUser.HasPaymentRights)
                        userID = LoggedUser.UserID;
                    UserInfo ui = UsersData.SelectUserByID(userID);
                    if (ui != null)
                        hi.User = ui.FullName;
                }
            if (ddlActivity.SelectedValue == "-1") hi.Activity = String.Empty;
            else
                if (ddlActivity.SelectedValue == "0") hi.Activity = Resource.ResourceManager["reports_All"];
                else hi.Activity = ddlActivity.SelectedItem.Text;

            if (ddlBuildingType.SelectedValue == "-1") hi.BuildingType = String.Empty;
            else
                if (ddlBuildingType.SelectedValue == "0") hi.BuildingType = Resource.ResourceManager["reports_All"];
                else hi.BuildingType = ddlBuildingType.SelectedItem.Text;

            hi.StartDate = ckDates.Checked ? Constants.DateMin : calStartDate.SelectedDate;
            hi.EndDate = ckDates.Checked ? Constants.DateMax : calEndDate.SelectedDate;

            if (IsTrainees)
            {
                string val = ddlMonth.SelectedValue;
                int ival = UIHelpers.ToInt(val);

                int iYear = UIHelpers.ToInt(System.Configuration.ConfigurationManager.AppSettings["MinSalaryYear"]) + ival / 12;
                int iMonth = ival % 12;
                if (iMonth == 0)
                {
                    iMonth = 12;
                    iYear--;
                }
                hi.StartDate = new DateTime(iYear, iMonth, 1);
                hi.EndDate = new DateTime(iYear, iMonth, 1).AddMonths(1).AddDays(-1);
            }
            if (ckDates.Checked && projectID > 0)
            {
                DateTime dt = CalendarDAL.Execute_ProjectStartDate(projectID);
                if (dt != Constants.DateMax)
                {
                    hi.StartDate = dt;
                    hi.EndDate = DateTime.Today.Date;
                }
            }
            hi.Proektant = Resource.ResourceManager["reports_GlavenProektant"];

            return hi;
        }

		#endregion

		#region Util

        private bool IsFilterCorrect()
        {
            if ((ddlUser.SelectedValue == "-1") && (ddlActivity.SelectedValue == "-1")
                && (ddlBuildingType.SelectedValue == "-1") && (ddlProject.SelectedValue == "-1"))
            {
                lblInfo.Text = Resource.ResourceManager["reports_FilterNotCorrect"];
                return false;
            }
            return true;
        }

		#endregion

		#region Create report for export

		/// <summary>
		/// Creates Active Report for export
		/// </summary>
		/// <param name="includeTimesColumn">If true - time values in report are dispalyed as digits(2.5), if false - as strings(2h30m)</param>
		/// <returns>WorkTimes Active report, null if creation unsuccessful</returns>
        private WorkTimesReport CreateReport(bool includeTimesColumn, bool forPDF)
        {
            BindGrid();
            #region check dates

            DateTime startDate, endDate;
            if (ckDates.Checked)
            {
                startDate = Constants.DateMin; endDate = Constants.DateMax;
            }
            else
            {
                startDate = calStartDate.SelectedDate; endDate = calEndDate.SelectedDate;
            }

            if (startDate > endDate)
            {
                lblError.Text = Resource.ResourceManager["reports_errorDate"];
                return null;
            }

            #endregion

            int userID = int.Parse(ddlUser.SelectedValue);
            if (!this.LoggedUser.HasPaymentRights)
                userID = LoggedUser.UserID; ;
            int projectID = int.Parse(ddlProject.SelectedValue);
            int activityID = int.Parse(ddlActivity.SelectedValue);
            int buildingTypeID = int.Parse(ddlBuildingType.SelectedValue);

            HeaderInfo hi = GetHeaderData();

            if (hi == null) return null;

            #region Hours summary

            DataSet dsWorkedHours = null;
            WorkedHoursInfo whi = null;

            if (userID > 0)
            {
                try
                {
                    dsWorkedHours = ReportsData.SelectWorkTimesHoursSummary(userID, projectID, activityID, buildingTypeID,
                        startDate, endDate, (ddlProjectStatus.SelectedIndex == 1), (ddlProjectStatus.SelectedIndex == 0), (ddlUserStatus.SelectedIndex == 1),
                        (ddlUserStatus.SelectedIndex == 0), (UIHelpers.GetIDParam() == Constants._WorktimesID_OverTime));
                }
                catch (Exception ex)
                {
                    log.Error(ex);
                }
                UserInfo ui = UsersData.SelectUserByID(userID);
                if (dsWorkedHours != null && ui != null)
                    whi = new WorkedHoursInfo(dsWorkedHours, ui.IsTraine);
                if (ckLate.Checked && whi != null)
                {
                    DataSet ds1 = BindGrid();
                    if (ds1.Tables.Count > 0 && ds1.Tables[0].Rows.Count > 0)
                    {
                        ds1.Tables[0].Rows.RemoveAt(ds1.Tables[0].Rows.Count - 1);
                        whi.SetLates(ds1);
                    }

                }
            }
            #endregion

            string selectExpr, groupBy, orderBy, whereExpr, returnColumns;
            GetSqlExpressions(out selectExpr, out whereExpr, out groupBy, out orderBy, out returnColumns);


            DataSet ds = LoadReportData(startDate, endDate, selectExpr, whereExpr, groupBy, orderBy, UIHelpers.GetIDParam());

            if (ds == null)
            {
                lblError.Text = Resource.ResourceManager["reports_ErrorLoadReportData"];
                return null;
            }

            DataTable dtRawData = ds.Tables[0];

            if ((dtRawData.Columns["ProjectID"] != null) && (dtRawData.Columns["BuildingTypeID"] != null)
                && (returnColumns.IndexOf("ProjectID") < returnColumns.IndexOf("BuildingTypeID")))
            {
                for (int i = 0; i < dtRawData.Rows.Count; i++)
                {
                    DataRow dr = dtRawData.Rows[i];
                    if (dr["BuildingType"].ToString().Length > 0) dtRawData.Rows[i]["ProjectName"] = String.Concat(dr["ProjectName"], " - ", dr["BuildingType"].ToString());
                }
                dtRawData.Columns.Remove("BuildingTypeID");
                dtRawData.Columns.Remove("BuildingType");

                returnColumns = returnColumns.Replace(",BuildingTypeID,BuildingType", "");


            }

            DataView dv = new DataView(dtRawData);
            if (whi != null)
                whi.SetTotals(nWorkHours, nOverTime, nUnderTime, nTotalTime);
            WorkTimesReport workTimesReport =
                new WorkTimesReport(hi, whi, this.LoggedUser.UserName,
                returnColumns, includeTimesColumn, forPDF, UIHelpers.GetIDParam() == Constants._WorktimesID_OverTime);
            if (dv.Count > 0) workTimesReport.DataSource = dv;

            return workTimesReport;
        }

		#endregion

		#region Event handlers

        private void ibGenerate_Click(object sender, System.Web.UI.ImageClickEventArgs e)
        {
            if (!IsFilterCorrect()) return;
            if (UIHelpers.GetIDParam() == Constants._WorktimesID_OverTime) InquireIsOverTime();
            //notes:set the grid,which shows salary and hour for all users to show 
            //	the time which they work less in function BindGrinNotWorkTime()
            if (cbNotWorkTime.Checked) { BindGrinNotWorkTime(); return; }
            BindGrid();
        }

        private void calStartDate_SelectionChanged(object sender, System.EventArgs e)
        {
            lblSelStartDate.Text = String.Concat("(", calStartDate.SelectedDates[0].ToString("dd.MM.yyyy"), ")");
            if (calStartDate.SelectedDates.Count > 1)
            {
                calEndDate.SelectedDate = calStartDate.SelectedDates[calStartDate.SelectedDates.Count - 1];
                calEndDate.VisibleDate = calEndDate.SelectedDate;

                calStartDate.SelectedDate = calStartDate.SelectedDates[0];
                lblSelEndDate.Text = String.Concat("(", calEndDate.SelectedDate.ToString("dd.MM.yyyy"), ")"); ;
            }

        }

        private void calEndDate_SelectionChanged(object sender, System.EventArgs e)
        {
            lblSelEndDate.Text = String.Concat("(", calEndDate.SelectedDates[0].ToString("dd.MM.yyyy"), ")");
        }

        private void CreateTraineeReport(bool bPdf)
        {
            GrapeCity.ActiveReports.SectionReport report = null;
            report = new Asa.Timesheet.WebPages.Reports.Trainees(GetHeaderData(), bPdf, SessionManager.LoggedUserInfo.FullName, BindGrid());
            if (bPdf)
            {
                PdfExport pdf = new PdfExport();
                pdf.NeverEmbedFonts = "";

                report.Run();
                System.IO.MemoryStream memoryFile = new System.IO.MemoryStream();
                pdf.Export(report.Document, memoryFile);

                Response.Clear();
                Response.AppendHeader("content-disposition"
                    , "attachment; filename=" + "trainees.pdf");
                Response.ContentType = "application/pdf";

                memoryFile.WriteTo(Response.OutputStream);
                Response.End();
            }
            else
            {
                XlsExport xls = new XlsExport();
                xls.RemoveVerticalSpace = true;

                report.Run();
                System.IO.MemoryStream memoryFile = new System.IO.MemoryStream();
                xls.Export(report.Document, memoryFile);

                Response.Clear();
                Response.AppendHeader("content-disposition"
                    , "attachment; filename=" + "trainees.xls");
                Response.ContentType = "application/xls";

                memoryFile.WriteTo(Response.OutputStream);
                Response.End();
            }
        }
        private void CreateOverTimeReport(bool bPdf)
        {
            GrapeCity.ActiveReports.SectionReport report = null;
            if (int.Parse(ddlUser.SelectedValue) > 0)
                report = new OverTimeReport1(GetHeaderData(), bPdf, SessionManager.LoggedUserInfo.FullName, BindGrid(), lblReportUser.Text);
            else
                report = new OverTimeReport(GetHeaderData(), bPdf, SessionManager.LoggedUserInfo.FullName, BindGrid());
            if (bPdf)
            {
                PdfExport pdf = new PdfExport();
                pdf.NeverEmbedFonts = "";

                report.Run();
                System.IO.MemoryStream memoryFile = new System.IO.MemoryStream();
                pdf.Export(report.Document, memoryFile);

                Response.Clear();
                Response.AppendHeader("content-disposition"
                    , "attachment; filename=" + "overtime.pdf");
                Response.ContentType = "application/pdf";

                memoryFile.WriteTo(Response.OutputStream);
                Response.End();
            }
            else
            {
                XlsExport xls = new XlsExport();
                xls.RemoveVerticalSpace = true;

                report.Run();
                System.IO.MemoryStream memoryFile = new System.IO.MemoryStream();
                xls.Export(report.Document, memoryFile);

                Response.Clear();
                Response.AppendHeader("content-disposition"
                    , "attachment; filename=" + "overtime.xls");
                Response.ContentType = "application/xls"; //???

                memoryFile.WriteTo(Response.OutputStream);
                Response.End();
            }
        }


        private void CreateLateReport(bool bPdf)
        {
            GrapeCity.ActiveReports.SectionReport report = null;

            // Summary


            DateTime startDate, endDate;
            if (ckDates.Checked)
            {
                startDate = Constants.DateMin; endDate = Constants.DateMax;
            }
            else
            {
                startDate = calStartDate.SelectedDate; endDate = calEndDate.SelectedDate;
            }

            int userID = int.Parse(ddlUser.SelectedValue);
            if (!this.LoggedUser.HasPaymentRights)
                userID = LoggedUser.UserID;
            int projectID = int.Parse(ddlProject.SelectedValue);
            int activityID = int.Parse(ddlActivity.SelectedValue);
            int buildingTypeID = int.Parse(ddlBuildingType.SelectedValue);

            HeaderInfo hi = GetHeaderData();



            DataSet dsWorkedHours = null;
            WorkedHoursInfo whi = null;
            DataSet ds1 = BindGrid();
            if (ds1.Tables.Count > 0 && ds1.Tables[0].Rows.Count > 0)
            {
                ds1.Tables[0].Rows.RemoveAt(ds1.Tables[0].Rows.Count - 1);
            }
            bool IsTraine = false;
            if (userID > 0)
            {
                try
                {
                    dsWorkedHours = ReportsData.SelectWorkTimesHoursSummary(userID, projectID, activityID, buildingTypeID,
                        startDate, endDate, (ddlProjectStatus.SelectedIndex == 1), (ddlProjectStatus.SelectedIndex == 0), (ddlUserStatus.SelectedIndex == 1),
                        (ddlUserStatus.SelectedIndex == 0), (UIHelpers.GetIDParam() == Constants._WorktimesID_OverTime));
                }
                catch (Exception ex)
                {
                    log.Error(ex);
                }

                UserInfo ui = UsersData.SelectUserByID(userID);
                IsTraine = ui.IsTraine;
                if (dsWorkedHours != null && ui != null)
                    whi = new WorkedHoursInfo(dsWorkedHours, ui.IsTraine);
                if (ckLate.Checked && whi != null)
                {
                    whi.SetTotals(nWorkHours, nOverTime, nUnderTime, nTotalTime);
                    whi.SetLates(ds1);

                }
            }

            report = new LateReport(GetHeaderData(), bPdf, SessionManager.LoggedUserInfo.FullName, ds1, whi, IsTraine);
            if (bPdf)
            {
                PdfExport pdf = new PdfExport();
                pdf.NeverEmbedFonts = "";

                report.Run();
                System.IO.MemoryStream memoryFile = new System.IO.MemoryStream();
                pdf.Export(report.Document, memoryFile);

                Response.Clear();
                Response.AppendHeader("content-disposition"
                    , "attachment; filename=" + "latereport.pdf");
                Response.ContentType = "application/pdf";

                memoryFile.WriteTo(Response.OutputStream);
                Response.End();
            }
            else
            {
                XlsExport xls = new XlsExport();
                xls.RemoveVerticalSpace = true;

                report.Run();
                System.IO.MemoryStream memoryFile = new System.IO.MemoryStream();
                xls.Export(report.Document, memoryFile);

                Response.Clear();
                Response.AppendHeader("content-disposition"
                    , "attachment; filename=" + "latereport.xls");
                Response.ContentType = "application/xls"; //???

                memoryFile.WriteTo(Response.OutputStream);
                Response.End();
            }
        }
        private void ibPdfExport_Click(object sender, System.Web.UI.ImageClickEventArgs e)
        {
            if (cbNotWorkTime.Checked)
                return;
            if (!IsFilterCorrect()) return;

            if (IsOverTime)
            {
                CreateOverTimeReport(true);
                return;
            }
            if (ckLate.Checked)
            {
                CreateLateReport(true);
                return;
            }

            if (IsTrainees && cbShowExtraGrid.Checked)
            {
                CreateTraineeReport(true);
                return;
            }
            WorkTimesReport workTimesReport = CreateReport(false, true);

            if (workTimesReport == null) return;

            PdfExport pdf = new PdfExport();
            pdf.NeverEmbedFonts = "";

            workTimesReport.Run();
            System.IO.MemoryStream memoryFile = new System.IO.MemoryStream();
            pdf.Export(workTimesReport.Document, memoryFile);

            Response.Clear();
            Response.AppendHeader("content-disposition"
                , "attachment; filename=" + "worktimes.pdf");
            Response.ContentType = "application/pdf";

            memoryFile.WriteTo(Response.OutputStream);
            Response.End();
        }

        private void ibXlsExport_Click(object sender, System.Web.UI.ImageClickEventArgs e)
        {
            if (cbNotWorkTime.Checked)
                return;
            if (!IsFilterCorrect()) return;

            if (IsOverTime)
            {
                CreateOverTimeReport(false);
                return;
            }
            if (ckLate.Checked)
            {
                CreateLateReport(false);
                return;
            }
            if (IsTrainees && cbShowExtraGrid.Checked)
            {
                CreateTraineeReport(false);
                return;
            }
            WorkTimesReport workTimesReport = CreateReport(true, false);

            if (workTimesReport == null) return;

            XlsExport xls = new XlsExport();
            xls.RemoveVerticalSpace = true;

            workTimesReport.Run();
            System.IO.MemoryStream memoryFile = new System.IO.MemoryStream();
            xls.Export(workTimesReport.Document, memoryFile);

            Response.Clear();
            Response.AppendHeader("content-disposition"
                , "attachment; filename=" + "worktimes.xls");
            Response.ContentType = "application/xls"; //???

            memoryFile.WriteTo(Response.OutputStream);
            Response.End();
        }

        private void ckDates_CheckedChanged(object sender, System.EventArgs e)
        {
            bool bEnable = !ckDates.Checked;
            calStartDate.Enabled = calEndDate.Enabled = bEnable;

            if (bEnable)
            {
                lblSelStartDate.Text = String.Concat("(" + calStartDate.SelectedDate.ToString("d"), ")");
                lblSelEndDate.Text = String.Concat("(" + calEndDate.SelectedDate.ToString("d"), ")");
            }
            else
            {
                lblSelStartDate.Text = lblSelEndDate.Text = String.Concat("(", Resource.ResourceManager["reports_SelectDate"], ")");
            }
        }

        private void grdReport_ItemDataBound(object sender, System.Web.UI.WebControls.DataGridItemEventArgs e)
        {

            int columns = e.Item.Cells.Count;

            if ((e.Item.ItemType == ListItemType.Item) || (e.Item.ItemType == ListItemType.AlternatingItem))
            {
                TableCell tc;
                DataRowView drv = (DataRowView)e.Item.DataItem;

                switch ((int)drv["RowType"])
                {
                    case 1: //first header
                        e.Item.Cells.Clear();
                        tc = new TableCell();
                        tc.ColumnSpan = columns;
                        tc.ForeColor = System.Drawing.Color.Black;
                        tc.BorderWidth = 0;
                        tc.Font.Bold = true;
                        tc.Font.Size = new FontUnit("11pt");
                        tc.BackColor = System.Drawing.Color.White;

                        tc.Text = drv[1].ToString();//Math.Abs((int)drv["RowType"])-1].ToString();
                        e.Item.Cells.Add(tc);
                        break;

                    case -1: // first group footer
                        e.Item.Cells.Clear();

                        tc = new TableCell();
                        tc.ColumnSpan = columns - 1;
                        tc.ForeColor = System.Drawing.Color.Black;
                        tc.BorderWidth = 0;
                        tc.Font.Bold = true;
                        tc.Font.Size = new FontUnit("10pt");
                        tc.BackColor = System.Drawing.Color.White;
                        //	tc.Text = Resource.ResourceManager["reports_TotalFor"]+" "+
                        //								drv[Math.Abs((int)drv["RowType"])-1].ToString();
                        tc.Text = Resource.ResourceManager["reports_Total"];
                        tc.Width = Unit.Percentage(80);
                        e.Item.Cells.Add(tc);

                        tc = new TableCell();
                        tc.ColumnSpan = 1;
                        tc.ForeColor = System.Drawing.Color.Black;
                        tc.BorderWidth = 0;
                        tc.Font.Bold = true;
                        tc.Font.Size = new FontUnit("10pt");
                        tc.BackColor = System.Drawing.Color.White;
                        tc.Text = TimeHelper.HoursStringFromMinutes((int)drv["Time"], false);
                        tc.Width = Unit.Percentage(20);
                        tc.HorizontalAlign = HorizontalAlign.Right;
                        e.Item.Cells.Add(tc);

                        break;

                    case 10: // to be corrected
                        e.Item.Cells.Clear();
                        tc = new TableCell(); tc.BorderWidth = 0; e.Item.Cells.Add(tc);
                        TableRow tr = (TableRow)tc.Parent;
                        tr.Height = 10;
                        break;
                    case 5: // to bre corrected
                        e.Item.Cells.Clear();
                        tc = new TableCell(); tc.BorderWidth = 0;
                        e.Item.Cells.Add(tc);
                        tr = (TableRow)tc.Parent;
                        tr.Height = 5;
                        break;
                    case 2: // 2nd group header
                        e.Item.Cells.Clear();

                        tc = new TableCell();
                        tc.ColumnSpan = columns;
                        tc.ForeColor = System.Drawing.Color.Black;
                        tc.BorderWidth = 2;

                        tc.BackColor = System.Drawing.Color.WhiteSmoke;
                        tc.Text = drv[3].ToString();//Math.Abs((int)drv["RowType"])-1].ToString();
                        e.Item.Cells.Add(tc);
                        break;
                    case -2: // 2nd group footer
                        e.Item.Cells.Clear();

                        tc = new TableCell();
                        tc.ColumnSpan = columns - 1;
                        tc.ForeColor = System.Drawing.Color.Black;
                        tc.BorderWidth = 2;
                        tc.Style.Add("border-right-width", "0px");

                        tc.BackColor = System.Drawing.Color.WhiteSmoke;
                        //	tc.Text = Resource.ResourceManager["reports_TotalFor"]+" "+drv[Math.Abs((int)drv["RowType"])-1].ToString();
                        tc.Text = Resource.ResourceManager["reports_Total"];
                        tc.Width = Unit.Percentage(80);
                        e.Item.Cells.Add(tc);

                        //hours cell
                        tc = new TableCell();
                        tc.ColumnSpan = 1;
                        tc.ForeColor = System.Drawing.Color.Black;
                        tc.BorderWidth = 2;
                        tc.Style.Add("border-left-width", "0px");

                        tc.BackColor = System.Drawing.Color.WhiteSmoke;
                        tc.Text = TimeHelper.HoursStringFromMinutes((int)drv["Time"], false);
                        tc.Width = Unit.Percentage(20);
                        e.Item.Cells.Add(tc); tc.Font.Bold = true;
                        tc.HorizontalAlign = HorizontalAlign.Right;

                        break;
                    case 0: //normal item
                        if (LoggedUser.HasPaymentRights)
                        {
                            tc = e.Item.Cells[columns - 2];
                            if (drv["Amount"] is decimal)
                                e.Item.Cells[columns - 2].Text = UIHelpers.FormatDecimal2((decimal)drv["Amount"]);
                            //e.Item.Cells[columns-2].HorizontalAlign = HorizontalAlign.Right;
                            e.Item.Cells[columns - 2].Font.Bold = true;
                        }
                        else
                            e.Item.Cells[columns - 2].Text = "";
                        tc = e.Item.Cells[columns - 1];
                        e.Item.Cells[columns - 1].Text = TimeHelper.HoursStringFromMinutes((int)drv["Time"], false);
                        e.Item.Cells[columns - 1].HorizontalAlign = HorizontalAlign.Right;
                        e.Item.Cells[columns - 1].Font.Bold = true;

                        //	e.Item.Cells[columns-1].HorizontalAlign = HorizontalAlign.Right;
                        //	e.Item.Cells[0].Text = String.Empty;
                        //	e.Item.Cells.RemoveAt(0);
                        //	e.Item.Cells[1].ColumnSpan = 2;
                        break;

                    // Grand total
                    case -100:
                        e.Item.Cells.Clear();

                        tc = new TableCell();
                        if (LoggedUser.HasPaymentRights)
                            tc.ColumnSpan = columns - 2;
                        else
                            tc.ColumnSpan = columns - 1;
                        tc.Style.Add("border-right-width", "0px");

                        /*tc.ForeColor=System.Drawing.Color.White;
                        tc.Font.Bold = true;
                        tc.BorderWidth = 2;
                        tc.BackColor = System.Drawing.Color.Peru;*/

                        tc.CssClass = "reportGridHeader";

                        tc.Text = Resource.ResourceManager["reports_Total"];
                        tc.Width = Unit.Percentage(80);
                        e.Item.Cells.Add(tc);
                        if (LoggedUser.HasPaymentRights)
                        {
                            tc = new TableCell();
                            tc.ColumnSpan = 1;

                            tc.Style.Add("border-left-width", "0px");
                            tc.Width = Unit.Percentage(20);
                            //tc.HorizontalAlign = HorizontalAlign.Right;

                            tc.CssClass = "reportGridHeader";
                            tc.Text = drv["Amount"].ToString();

                            e.Item.Cells.Add(tc);
                        }
                        //hours cell
                        tc = new TableCell();
                        tc.ColumnSpan = 1;

                        tc.Style.Add("border-left-width", "0px");
                        tc.Width = Unit.Percentage(20);
                        tc.HorizontalAlign = HorizontalAlign.Right;

                        tc.CssClass = "reportGridHeader";
                        /*
                        tc.ForeColor=System.Drawing.Color.Black;
                        tc.BackColor = System.Drawing.Color.WhiteSmoke;
                        tc.Font.Bold = true;tc.BorderWidth = 2;
                        */


                        tc.Text = TimeHelper.HoursStringFromMinutes((int)drv["Time"], false);

                        e.Item.Cells.Add(tc);


                        break;
                }
            }
        }

        private void grdUsers_ItemDataBound(object sender, System.Web.UI.WebControls.DataGridItemEventArgs e)
        {


            bool IsOverTime = (UIHelpers.GetIDParam() == Constants._WorktimesID_OverTime);
            if ((e.Item.ItemType == ListItemType.Item) || (e.Item.ItemType == ListItemType.AlternatingItem))
            {
                decimal counts = 0;
                if (IsOverTime)
                {
                    counts = UIHelpers.ParseDecimal(e.Item.Cells[4].Text) + UIHelpers.ParseDecimal(e.Item.Cells[5].Text);
                    e.Item.Cells[6].Text = counts.ToString();
                }
                else
                {

                }
                int minutes = UIHelpers.ToInt(e.Item.Cells[3].Text);
                if (minutes != 0)
                    e.Item.Cells[3].Text = TimeHelper.HoursStringFromMinutes(minutes, false);
                else
                    e.Item.Cells[3].Text = TimeHelper.HoursStringFromMinutes(minutes, true);
                minutes = UIHelpers.ToInt(e.Item.Cells[4].Text);
                if (minutes != 0)
                    e.Item.Cells[4].Text = TimeHelper.HoursStringFromMinutes(minutes, false);
                else
                    e.Item.Cells[4].Text = TimeHelper.HoursStringFromMinutes(minutes, true);
                minutes = UIHelpers.ToInt(e.Item.Cells[5].Text);
                if (minutes != 0)
                    e.Item.Cells[5].Text = TimeHelper.HoursStringFromMinutes(minutes, false);
                else
                    e.Item.Cells[5].Text = TimeHelper.HoursStringFromMinutes(minutes, true);
                minutes = UIHelpers.ToInt(e.Item.Cells[6].Text);
                if (minutes != 0)
                    e.Item.Cells[6].Text = TimeHelper.HoursStringFromMinutes(minutes, false);
                else
                    e.Item.Cells[6].Text = TimeHelper.HoursStringFromMinutes(minutes, true);
                int minutes1 = UIHelpers.ToInt(e.Item.Cells[8].Text);
                if (minutes1 != 0)
                    e.Item.Cells[8].Text = TimeHelper.HoursStringFromMinutes(minutes1, false);
                else
                    e.Item.Cells[8].Text = TimeHelper.HoursStringFromMinutes(minutes1, true);

                minutes1 = UIHelpers.ToInt(e.Item.Cells[9].Text);
                if (minutes1 != 0)
                    e.Item.Cells[9].Text = TimeHelper.HoursStringFromMinutes(minutes1, false);
                else
                    e.Item.Cells[9].Text = TimeHelper.HoursStringFromMinutes(minutes1, true);
            }
        }
		#endregion

		#region Html
        private void SetHeaderInfo(HeaderInfo hi)
        {
            tblHeader.Visible = true;

            lblObekt.Text = (hi.ProjectAdminName == String.Empty) ? hi.ProjectName : hi.ProjectAdminName;
            if (lblObekt.Text == String.Empty)
            {
                tblHeader.Rows[0].Visible = false; tblHeader.Rows[1].Visible = false;
                tblHeader.Rows[2].Visible = false; tblHeader.Rows[3].Visible = false;
            }

            if (hi.ClientName == String.Empty)
            {
                tblHeader.Rows[1].Visible = false; tblHeader.Rows[2].Visible = false;
            }
            else
            {
                lblClient.Text = hi.ClientName;
                lblClientAddress.Text = String.Concat(hi.ClientCity, ", ", hi.ClientAddress).Trim(',').Trim().Trim(',');
                if (lblClientAddress.Text == String.Empty) tblHeader.Rows[2].Visible = false;
            }

            lblProektant.Text = Resource.ResourceManager["reports_GlavenProektant"];

            if (hi.StartDate == Constants.DateMin) tblHeader.Rows[4].Visible = false;
            else
            {
                if (!IsTrainees)
                    lblPeriod.Text = String.Concat(hi.StartDate.ToString("d MMM yyyy"), " ",
                        Resource.ResourceManager["reports_YearAbbr"], " - ",
                        hi.EndDate.ToString("d MMM yyyy"), " ",
                        Resource.ResourceManager["reports_YearAbbr"]);
            }

            lblReportUser.Text = hi.User;
            tblHeader.Rows[5].Visible = hi.User != String.Empty;

            lblReportActivity.Text = hi.Activity;
            tblHeader.Rows[6].Visible = hi.Activity != String.Empty;

            lblReportBuildingType.Text = hi.BuildingType;
            tblHeader.Rows[7].Visible = hi.BuildingType != String.Empty;

            lblCreatedBy.Text = this.LoggedUser.UserName;
        }
        private DataSet BindGrid()
        {
            #region check dates

            DateTime startDate, endDate;
            if (ckDates.Checked)
            {
                startDate = Constants.DateMin; endDate = Constants.DateMax;
            }
            else
            {
                startDate = calStartDate.SelectedDate; endDate = calEndDate.SelectedDate;
            }

            if (startDate > endDate)
            {
                lblError.Text = Resource.ResourceManager["reports_errorDate"];
                return null;
            }

            #endregion

            int userID = int.Parse(ddlUser.SelectedValue);
            if (!this.LoggedUser.HasPaymentRights)
                userID = LoggedUser.UserID;

            int projectID = int.Parse(ddlProject.SelectedValue);
            int activityID = int.Parse(ddlActivity.SelectedValue);
            int buildingTypeID = int.Parse(ddlBuildingType.SelectedValue);
            bool IsActive = (ddlProjectStatus.SelectedIndex == 1);
            bool IsActiveAll = (ddlProjectStatus.SelectedIndex == 0);

            #region Hours summary

            DataSet dsWorkedHours = null;
            WorkedHoursInfo whi = null;
            //bool bOverTime=UIHelpers.GetIDParam()==Constants._WorktimesID_OverTime;
            DataSet dsWorkedHoursOfUsers = null;

            if (userID > 0 && !IsTrainees && !IsOverTime)
            {
                try
                {
                    dsWorkedHours = ReportsData.SelectWorkTimesHoursSummary(userID, projectID, activityID, buildingTypeID,
                        startDate, endDate, IsActive, IsActiveAll, (ddlUserStatus.SelectedIndex == 1), (ddlUserStatus.SelectedIndex == 0),
                        (UIHelpers.GetIDParam() == Constants._WorktimesID_OverTime));
                }
                catch (Exception ex)
                {
                    log.Error(ex);
                }

                UserInfo ui = UsersData.SelectUserByID(userID);
                if (dsWorkedHours != null && ui != null)
                {
                    whi = new WorkedHoursInfo(dsWorkedHours, ui.IsTraine);
                    lbSalary2.Text = whi.TotalSalary.ToString("0.00");
                    int nMinutes = (int)(whi.TotalHours * (decimal)MINUTES_IN_HOUR);
                    lblTotalHours.Text = TimeHelper.HoursStringFromMinutes(nMinutes, true);
                    if (UIHelpers.GetIDParam() == Constants._WorktimesID_OverTime)
                    {
                        decimal totHours = whi.TotalHours - whi.TotalHoursInWorkTime;
                        lblTotalHours.Text = totHours.ToString("0.00");
                    }
                    //					lblHoursWT.Text = whi.TotalHoursInWorkTime.ToString("0.00");
                    //					lblHoursNWT.Text = whi.TotalHoursInNonWorkTime.ToString("0.00");
                    //					lblHoursWND.Text = whi.TotalHoursWeekends.ToString("0.00");

                    lblTotalWorkDays.Text = whi.TotalWorkDays.ToString();
                    lblTotalNonWorkDays.Text = whi.TotalNonWorkDays.ToString();

                    lblDaysPO.Text = whi.DaysPlatenOtpusk.ToString();
                    lblDaysNO.Text = whi.DaysNeplatenOtpusk.ToString();
                    lblDaysIll.Text = whi.DaysIll.ToString();

                    lblDaysPOString.Text = whi.DaysPlatenOtpuskString;
                    lblDaysNOString.Text = whi.DaysNeplatenOtpuskString;
                    lblDaysIllString.Text = whi.DaysIllString;
                    lbSalary1.Visible = lbSalary2.Visible = false;
                    if (LoggedUser.HasPaymentRights)
                    {
                        lbSalary1.Visible = lbSalary2.Visible = true;


                    }
                    tblFooter.Visible = true;
                    tblHeadFooter.Visible = true;
                    tblFooter1.Visible = true;
                }

            }
            if (IsTrainees)
            {
                string val = ddlMonth.SelectedValue;
                int ival = UIHelpers.ToInt(val);

                int iYear = UIHelpers.ToInt(System.Configuration.ConfigurationManager.AppSettings["MinSalaryYear"]) + ival / 12;
                int iMonth = ival % 12;
                if (iMonth == 0)
                {
                    iMonth = 12;
                    iYear--;
                }
                startDate = new DateTime(iYear, iMonth, 1);
                endDate = new DateTime(iYear, iMonth, 1).AddMonths(1).AddDays(-1);
            }
            /* LATE * /////////////////////////// */
            UserInfo uii = null;
            if (userID > 0)
            {

                uii = UsersData.SelectUserByID(userID);
                if (uii != null && uii.IsTraine)
                {

                    Label19.Visible = Label22.Visible = false;
                    Label11.Visible = Label12.Visible =

                        lbTotalLateCount.Visible = lbTotalLate.Visible = lblDaysNOString.Visible = lblDaysIllString.Visible = false;
                    lblDaysNO.Visible = lblDaysIll.Visible = false;

                }
            }
            DataSet dssLate = null;
            if (ckLate.Checked || userID > 0)
            {
                dssLate = ReportsData.SelectReportsWorkTimesLate(projectID, activityID, buildingTypeID,
                    startDate, endDate, IsActive, IsActiveAll, (ddlUserStatus.SelectedIndex == 1), (ddlUserStatus.SelectedIndex == 0), userID);

                if (whi != null)
                {
                    whi.SetLates(dssLate);
                    lbTotalLate.Text = whi.TotalHoursLate;
                    lbTotalLateCount.Text = whi.TotalLateCount.ToString();
                }
            }
            DataSet dssOverTime = null;
            if (userID > 0)
            {
                decimal dCoef = 0;
                if (txtCoef.Text != "")
                    dCoef = decimal.Parse(txtCoef.Text);
                dssOverTime = ReportsData.SelectWorkTimesHoursSummaryOfUsersForOverTimeByUsr(projectID, activityID, buildingTypeID,
                    startDate, endDate, IsActive, IsActiveAll, (ddlUserStatus.SelectedIndex == 1), (ddlUserStatus.SelectedIndex == 0), dCoef, userID);
                if (dssOverTime.Tables.Count > 0 && dssOverTime.Tables[0].Rows.Count > 0)
                {
                    nTotalTime = (int)dssOverTime.Tables[0].Rows[0]["TotalTime"];

                    lblTotalHours.Text = TimeHelper.HoursStringFromMinutes(nTotalTime, true);
                    nOverTime = (int)dssOverTime.Tables[0].Rows[0]["OverTime"];
                    lblTotalNonWorkDays.Text = TimeHelper.HoursStringFromMinutes(nOverTime, true);
                    nUnderTime = -(int)dssOverTime.Tables[0].Rows[0]["UnderTime"];
                    lblMissingHours.Text = TimeHelper.HoursStringFromMinutes(nUnderTime, true);
                    nWorkHours = (nTotalTime - nOverTime);
                    lblTotalWorkDays.Text = TimeHelper.HoursStringFromMinutes(nWorkHours, true);
                }
            }
            if (ckLate.Checked)
            {
                if (uii != null && uii.IsTraine)
                {
                    grdLate.Columns[(int)LateColumns.LateMinutes].Visible = false;
                }
                lbSalary2.Visible = false;

                //lblReportTitle.Text=Resource.ResourceManager["reports_WorkTimes_Late_Title"];
                lblReportTitle.Visible = true;
                int nLate = 0;
                int nTotal = 0;

                foreach (DataRow dr in dssLate.Tables[0].Rows)
                {
                    if (dr["MinutesLate"] != DBNull.Value)
                        nLate += (int)dr["MinutesLate"];
                    if (dr["TotalMinutes"] != DBNull.Value)
                        nTotal += (int)dr["TotalMinutes"];
                }
                DataRow gt = dssLate.Tables[0].NewRow();

                gt["FullName"] = Resource.ResourceManager["reports_Total"];
                gt["UserID"] = -1;
                gt["MinutesLate"] = nLate;
                gt["TotalMinutes"] = nTotal;
                dssLate.Tables[0].Rows.Add(gt);

                grdLate.DataSource = dssLate.Tables[0];
                grdLate.DataBind();
                grdLate.Visible = true;

                return dssLate;
            }

            if (cbShowExtraGrid.Checked)
            {


                try
                {
                    if (UIHelpers.GetIDParam() == Constants._WorktimesID_NormalTime || IsTrainees)
                        dsWorkedHoursOfUsers = ReportsData.SelectWorkTimesHoursSummaryOfUsers(projectID, activityID, buildingTypeID,
                            startDate, endDate, IsActive, IsActiveAll, (ddlUserStatus.SelectedIndex == 1), (ddlUserStatus.SelectedIndex == 0), IsTrainees, userID);
                    else
                    {
                        if (userID <= 0)
                            dsWorkedHoursOfUsers = ReportsData.SelectWorkTimesHoursSummaryOfUsersForOverTime(projectID, activityID, buildingTypeID,
                                startDate, endDate, IsActive, IsActiveAll, (ddlUserStatus.SelectedIndex == 1), (ddlUserStatus.SelectedIndex == 0), decimal.Parse(txtCoef.Text), userID);

                    }
                }
                catch (Exception ex)
                {
                    lblError.Text = ex.Message;
                    log.Error(ex);
                }
                if (IsTrainees)
                {
                    int nMinutes = int.Parse(System.Configuration.ConfigurationManager.AppSettings["MinHourTrainee"]) * 60;
                    foreach (DataRow dr in dsWorkedHoursOfUsers.Tables[0].Rows)
                    {

                        if ((int)dr["WorkedMinutes"] > nMinutes)
                        {
                            SalariesVector salaries = SalaryUDL.GetSalariesByUser((int)dr["UserID"]);
                            dr["Amount"] = UIHelpers.FormatDecimal2(SalaryUDL.GetSalaryByMonthYearType(salaries, startDate.Year, startDate.Month, (int)SalaryTypes.Main));
                        }
                    }
                }
                if (dsWorkedHoursOfUsers != null)
                    grdUsers.DataSource = dsWorkedHoursOfUsers.Tables[0];
                else
                    grdUsers.DataSource = null;
                //bool IsOverTime = (UIHelpers.GetIDParam() == Constants._WorktimesID_OverTime);
                if (IsOverTime)
                {
                    grdUsers.Columns[9].HeaderText = grdUsers.Columns[4].HeaderText;
                    grdUsers.Columns[3].HeaderText = Resource.ResourceManager["reports_WorkTimes_Normative"];
                    grdUsers.Columns[5].HeaderText = Resource.ResourceManager["reports_WorkTimes_Worked"];
                }
                grdUsers.DataBind();

                if (userID > 0 && IsOverTime)
                {
                    //DataSet dssOverTime=ReportsData.SelectWorkTimesHoursSummaryOfUsersForOverTimeByUsr(projectID, activityID, buildingTypeID,
                    //	startDate, endDate, IsActive, IsActiveAll,(ddlUserStatus.SelectedIndex == 1),(ddlUserStatus.SelectedIndex == 0),decimal.Parse(txtCoef.Text),userID);
                    DataTable dt = dssOverTime.Tables[0].Copy();
                    DataRow newRow = dt.NewRow();
                    newRow["UserID"] = -1;
                    int WorkedMinutesNormal = 0;
                    int TotalTime = 0;
                    int OverTime = 0;
                    int UnderTime = 0;
                    double TotalAmount = 0;
                    foreach (DataRow dr in dt.Rows)
                    {
                        WorkedMinutesNormal += (int)dr["WorkedMinutesNormal"];
                        TotalTime += (int)dr["TotalTime"];
                        OverTime += (int)dr["OverTime"];
                        UnderTime += (int)dr["UnderTime"];
                        TotalAmount += (double)dr["TotalAmount"];
                    }
                    newRow["WorkedMinutesNormal"] = WorkedMinutesNormal;
                    newRow["TotalTime"] = TotalTime;
                    newRow["OverTime"] = OverTime;
                    newRow["UnderTime"] = UnderTime;
                    newRow["TotalAmount"] = TotalAmount;
                    newRow["monthyear"] = Resource.ResourceManager["reports_Total"];
                    dt.Rows.Add(newRow);


                    grdOneEmployee.DataSource = dt;
                    grdOneEmployee.DataBind();
                    grdOneEmployee.Visible = true;
                    if (!LoggedUser.HasPaymentRights)
                        grdOneEmployee.Columns[grdOneEmployee.Columns.Count - 1].Visible = false;
                    HeaderInfo hii = GetHeaderData();
                    SetHeaderInfo(hii);
                    return dssOverTime;
                }

                if (grdUsers.Items.Count == 0)
                {
                    grdUsers.Visible = false;
                    lblNoDataFound.Visible = true;
                    lblNoDataFound.Text = Resource.ResourceManager["reports_WorkTimes_NoDataMatch"];
                }

                if (IsOverTime)
                {
                    grdUsers.Columns[3].Visible = true;

                    grdUsers.Columns[1].Visible = false;
                    grdUsers.Columns[2].Visible = false;
                    grdUsers.Columns[4].Visible = false; ;

                    grdUsers.Columns[5].Visible = true;
                    grdUsers.Columns[6].Visible = false;

                    grdUsers.Columns[7].Visible = false;
                    grdUsers.Columns[8].Visible = false;
                    grdUsers.Columns[9].Visible = true;

                    grdUsers.Columns[10].Visible = true;

                }
                else
                {
                    grdUsers.Columns[1].Visible = false;
                    grdUsers.Columns[3].Visible = false;
                    grdUsers.Columns[4].Visible = false;
                    grdUsers.Columns[5].Visible = false;
                    grdUsers.Columns[7].Visible = false;
                    grdUsers.Columns[8].Visible = false;
                    grdUsers.Columns[9].Visible = false;
                    grdUsers.Columns[10].Visible = false;
                }
            }
            else
                grdUsers.Visible = false;
            #endregion

            string selectExpr, groupByExpr, orderByExpr, whereExpr, returnColumns;
            GetSqlExpressions(out selectExpr, out whereExpr, out groupByExpr, out orderByExpr, out returnColumns);

            DataSet ds = LoadReportData(startDate, endDate, selectExpr, whereExpr, groupByExpr, orderByExpr, UIHelpers.GetIDParam());

            if (ds == null)
            {
                //lblError.Text = Resource.ResourceManager["reports_ErrorLoadReportData"];
                return dsWorkedHoursOfUsers;
            }

            DataTable dtRawData = ds.Tables[0];
            dtRawData.Columns["TotalTime"].ColumnName = "Time";

            if ((dtRawData.Columns["ProjectID"] != null) && (dtRawData.Columns["BuildingTypeID"] != null)
                && (returnColumns.IndexOf("ProjectID") < returnColumns.IndexOf("BuildingTypeID")))
            {
                for (int i = 0; i < dtRawData.Rows.Count; i++)
                {
                    DataRow dr = dtRawData.Rows[i];
                    if (dr["BuildingType"].ToString().Length > 0) dtRawData.Rows[i]["ProjectName"] = String.Concat(dr["ProjectName"], " - ", dr["BuildingType"].ToString());
                }
                dtRawData.Columns.Remove("BuildingTypeID");
                dtRawData.Columns.Remove("BuildingType");

                returnColumns = returnColumns.Replace(",BuildingTypeID,BuildingType", "");
            }

            #region Set header info

            HeaderInfo hi = GetHeaderData();

            if (hi == null) return dsWorkedHoursOfUsers;
            SetHeaderInfo(hi);


            #endregion

            lblReportTitle.Visible = true;
            lblCreatedBy.Visible = true;
            lblCreatedByLbl.Visible = true;

            DataTable rawData = ds.Tables[0];
            if (rawData.Rows.Count == 0)
            {
                lblNoDataFound.Visible = true;
                lblNoDataFound.Text = Resource.ResourceManager["reports_WorkTimes_NoDataMatch"];
                return dsWorkedHoursOfUsers;
            }
            int numberOfGroups = ((rawData.Columns.Count - 1) / 2) - 1;
            if (UIHelpers.GetIDParam() == Constants._WorktimesID_OverTime) numberOfGroups = ((rawData.Columns.Count - 1) / 2) - 1;

            rawData.Columns.Add("RowType", System.Type.GetType("System.Int32"), "0");

            #region get UI names
            //Get DB column names, and corresponding UI names
            string[] dbColumnNames = returnColumns.Split(',');

            string[] columnNames = new string[dbColumnNames.Length / 2 + 1];

            int index = 0;
            for (int i = 1; i < dbColumnNames.Length; i += 2)
            {
                switch (dbColumnNames[i])
                {
                    case "FullName": columnNames[index] = Resource.ResourceManager["reports_ColumnNameUser"];
                        break;
                    case "ActivityName": columnNames[index] = Resource.ResourceManager["reports_ColumnNameActivity"];
                        break;
                    case "ProjectName": columnNames[index] = Resource.ResourceManager["reports_ColumnNameProject"];
                        break;
                    case "BuildingType": columnNames[index] = Resource.ResourceManager["reports_ColumnNameBuildingType"];
                        break;
                    //					case "Amount" : columnNames[index] = Resource.ResourceManager["reports_ColumnNameAmount"];
                    //						break;

                    default: columnNames[index] = String.Empty; break;
                }
                index++;
            }

            //			for (int i=0; i<columnNames.Length; i++) grdReport.Columns[i].HeaderText = columnNames[i];
            //
            //
            //			grdReport.Columns[grdReport.Columns.Count-1].HeaderText = "Amount";

            #endregion

            for (int i = 0; i < numberOfGroups; i++)
            {
                //if(rawData.Columns[i*2+1].ColumnName!="Amount")
                rawData.Columns[i * 2 + 1].ColumnName = "Group" + (i + 1).ToString() + "Name";
            };

            //create summaries for only first 2 groups
            for (int i = numberOfGroups; i < 4; i++) grdReport.Columns.RemoveAt(numberOfGroups);

            System.Int64 gtTime64 = 0;

            DataTable reportTable = ds.Tables[0];
            gtTime64 = (System.Int64)reportTable.Compute("Sum(Time)", "");

            decimal gtAmount = (decimal)reportTable.Compute("Sum(Amount)", "");
            int gtTime = 0;
            try
            {
                gtTime = int.Parse(gtTime64.ToString());
            }
            catch
            {
                gtTime = 0;
            }

            if (numberOfGroups > 2)
            {
                reportTable = AddGroupSummaries(reportTable, 1, 10);
                reportTable = AddGroupSummaries(reportTable, 2, 5);

                grdReport.Columns.RemoveAt(0);
                grdReport.Columns.RemoveAt(0);
            }
            else
                if (numberOfGroups == 2)
                {
                    grdReport.Columns.RemoveAt(0);
                    reportTable = AddGroupSummaries(reportTable, 1, 5);
                }

            //Add grandTotal row
            DataRow gtRow = reportTable.NewRow();
            gtRow["RowType"] = -100;
            gtRow["Time"] = gtTime;
            gtRow["Amount"] = gtAmount;
            reportTable.Rows.Add(gtRow);

            //edited by Ivailo date:14.12.2007
            //noted:if it's overtime the text in the grid is different and it's getting from resources
            if (UIHelpers.GetIDParam() == Constants._WorktimesID_OverTime)
            {
                for (int i = 0; i < grdReport.Columns.Count; i++)
                {
                    if (grdReport.Columns[i].HeaderText == Resource.ResourceManager["WorkTimePaymentGridNotOverTime"])
                    {
                        string s = Resource.ResourceManager["WorkTimePaymentGrid"];
                        grdReport.Columns[i].HeaderText = s;
                        break;
                    }
                }
            }

            if (LoggedUser.HasPaymentRights)
            {
                grdReport.DataSource = reportTable;
                grdReport.DataBind();
                grdReport.Visible = true;
            }
            else
                grdReport.Visible = false;


            //edited by Ivailo date:14.12.2007
            //noted: the text for normal time and overtime is different
            if (UIHelpers.GetIDParam() == Constants._WorktimesID_NormalTime)
                lbSalary1.Text = Resource.ResourceManager["WorkTime_lbSalary1_normal"];
            else
                lbSalary1.Text = Resource.ResourceManager["WorkTime_lbSalary1_over"];
            grdReport.ShowHeader = LoggedUser.HasPaymentRights;
            return dsWorkedHoursOfUsers;
        }

        private void InquireIsOverTime()
        {
            lblReportTitle.Text = Resource.ResourceManager["lbWorkTimeOvertime"];
            tblFooter1.Visible = false;
            Label10.Visible = Label11.Visible = Label12.Visible = false;
            Label9.Visible = lblDaysPO.Visible = lblDaysPOString.Visible = lblDaysNO.Visible = lblDaysNOString.Visible = lblDaysIll.Visible = lblDaysIllString.Visible = false;
            Label13.Visible = Label14.Visible = lblTotalWorkDays.Visible = lblTotalNonWorkDays.Visible = false;

        }


        private void BindGrinNotWorkTime()
        {
            #region check dates

            DateTime startDate, endDate;
            if (ckDates.Checked)
            {
                lblError.Visible = true;
                lblError.Text = Resource.ResourceManager["ErrorPeriodWrong"];
                return;
            }
            else
            {
                lblError.Visible = false;
                startDate = calStartDate.SelectedDate; endDate = calEndDate.SelectedDate;
            }

            if (startDate > endDate)
            {
                lblError.Text = Resource.ResourceManager["reports_errorDate"];
                return;
            }

            #endregion

            //lblReportTitle.Text = Resource.ResourceManager["lbWorkTimeOvertime"];
            lblCreatedBy.Text = this.LoggedUser.UserName;
            lblReportTitle.Visible = true;
            lblCreatedBy.Visible = true;
            lblCreatedByLbl.Visible = true;
            tblHeader.Visible = false;
            lblReportTitle.Text = Resource.ResourceManager["TitleNotWorkTime"];
            //notes:the code is still not working
            DataSet dsWorkedHoursOfUsers = null;
            try
            {
                int userID = -1;
                if (UIHelpers.ToInt(ddlUser.SelectedValue) > 0)
                    userID = UIHelpers.ToInt(ddlUser.SelectedValue);
                dsWorkedHoursOfUsers = ReportsData.SelectNotWorkTimesHoursSummaryOfUsers(startDate, endDate, (ddlUserStatus.SelectedIndex == 1), (ddlUserStatus.SelectedIndex == 0), userID);
            }
            catch (Exception ex)
            {
                log.Error(ex);
            }
            DataTable dt = dsWorkedHoursOfUsers.Tables[0];
            DataRow dr = dt.NewRow();
            dr[0] = Resource.ResourceManager["reports_Total"];
            int Minutes = 0;
            decimal Sum = 0;
            foreach (DataRow d in dt.Rows)
            {
                Sum += (decimal)d["Amount"];
                Minutes += (int)d["WorkedMinutes"];
            }
            dr["WorkedMinutes"] = Minutes;
            dr["Amount"] = Sum;
            dt.Rows.Add(dr);
            if (dsWorkedHoursOfUsers != null)
                grdUsers.DataSource = dt;
            else
                grdUsers.DataSource = null;
            grdUsers.DataBind();

            if (grdUsers.Items.Count == 0)
            {
                grdUsers.Visible = false;
                lblNoDataFound.Visible = true;
                lblNoDataFound.Text = Resource.ResourceManager["reports_WorkTimes_NoDataMatch"];
            }
            else
                grdUsers.Items[grdUsers.Items.Count - 1].CssClass = "ReportsTotal";
            lblNoDataFound.Visible = false;
            if (!LoggedUser.HasPaymentRights)
                grdUsers.Columns[7].Visible = false;
            else
                grdUsers.Columns[7].Visible = true;
            grdUsers.Columns[1].Visible = false;
            grdUsers.Columns[2].Visible = false;
            grdUsers.Columns[3].Visible = false;
            grdUsers.Columns[4].Visible = false;
            grdUsers.Columns[5].Visible = false;
            grdUsers.Columns[6].Visible = false;

            grdUsers.Columns[8].Visible = true;

        }

		#endregion

		#region Html util

        private DataTable CreateTable(DataTable dt)
        {
            DataTable reportTable = new DataTable();
            for (int i = 0; i < dt.Columns.Count; i++)
            {
                string groupName = dt.Columns[i].ColumnName;
                reportTable.Columns.Add(new DataColumn(groupName, dt.Columns[i].DataType));
            }

            return reportTable;
        }


        private DataTable AddGroupSummaries(DataTable dtUngrouped, int groupOrder, int emptyRowType)
        {
            DataTable rt = CreateTable(dtUngrouped);
            DataRow dr;

            int idColumnIndex = (groupOrder - 1) * 2;
            int nameColumnIndex = idColumnIndex + 1;

            int id = 0; //-1;
            object o = dtUngrouped.Rows[0][idColumnIndex];
            //if (dtUngrouped.Rows[0][idColumnIndex]!=System.DBNull.Value)
            //						 id = (int)dtUngrouped.Rows[0][idColumnIndex];

            if ((int)dtUngrouped.Rows[0]["RowType"] == 0)
                id = (int)dtUngrouped.Rows[0][idColumnIndex];

            int summary = 0;
            decimal amount = 0;
            if ((int)dtUngrouped.Rows[0]["RowType"] == 0)
            {
                dr = rt.NewRow();

                dr[idColumnIndex] = id;
                dr[nameColumnIndex] = dtUngrouped.Rows[0][nameColumnIndex].ToString();
                dr["RowType"] = groupOrder;
                dr["Time"] = 0;
                dr["Amount"] = 0;
                rt.Rows.Add(dr);
            }

            for (int i = 0; i < dtUngrouped.Rows.Count; i++)
            {
                int tempID = 0;//-1;
                if ((int)dtUngrouped.Rows[i]["RowType"] == 0) tempID = (int)dtUngrouped.Rows[i][idColumnIndex];
                //		if (dtUngrouped.Rows[i][nameColumnIndex]!=null) 
                //		tempName = dtUngrouped.Rows[i][nameColumnIndex].ToString();

                if (tempID != id)
                {
                    if (id != 0)//-1)
                    {
                        dr = rt.NewRow();
                        dr[idColumnIndex] = id;
                        dr[nameColumnIndex] = dtUngrouped.Rows[i - 1][nameColumnIndex].ToString();

                        dr["RowType"] = -groupOrder; dr["Time"] = summary;
                        dr["Amount"] = amount;
                        rt.Rows.Add(dr);

                        if ((emptyRowType > 0))
                        {
                            dr = rt.NewRow();
                            dr["RowType"] = emptyRowType;
                            dr["Time"] = 0;
                            dr["Amount"] = (decimal)0;
                            rt.Rows.Add(dr);
                        }
                    }

                    if (tempID != 0)//-1)
                    {
                        //new header
                        dr = rt.NewRow();
                        dr[idColumnIndex] = tempID;
                        dr[nameColumnIndex] = dtUngrouped.Rows[i][nameColumnIndex].ToString();
                        dr["RowType"] = groupOrder; dr["Time"] = 0;
                        dr["Amount"] = (decimal)0;
                        rt.Rows.Add(dr);
                    }

                    id = tempID;
                    //name = tempName; 
                    summary = 0;
                }
                if (dtUngrouped.Rows[i]["Amount"] is int)
                    amount += (int)dtUngrouped.Rows[i]["Amount"];
                else if (dtUngrouped.Rows[i]["Amount"] is decimal)
                    amount += (decimal)dtUngrouped.Rows[i]["Amount"];
                summary += (int)dtUngrouped.Rows[i]["Time"];
                dr = rt.NewRow();
                dr.ItemArray = dtUngrouped.Rows[i].ItemArray;
                rt.Rows.Add(dr);
            }

            if (groupOrder < 2)
            {
                dr = rt.NewRow();
                dr[idColumnIndex] = id;
                dr[nameColumnIndex] = dtUngrouped.Rows[dtUngrouped.Rows.Count - 1]["Time"];

                dr["RowType"] = -groupOrder; dr["Time"] = summary;
                dr["Amount"] = amount;
                rt.Rows.Add(dr);
            }

            return rt;
        }

		#endregion

		#region Get sql expressions

		#region 1
        private void GetSqlExpressions1(out string select, out string where, out string groupBy, out string orderBy, out string returnColumns)
        {
            groupBy = ""; orderBy = String.Empty;
            select = "";
            where = ""; returnColumns = "";

            string[] selects = new string[4];
            string[] wheres = new string[4];
            string[] groupBys = new string[4];
            string[] orderBys = new string[4];
            string[] returnColumnss = new string[4];

            int i = int.Parse(ddlUserOrder.SelectedValue);
            if (ddlUser.SelectedValue != "-1")
            {
                selects[i] = "WorkTimes.UserID, FullName";
                if (ddlUser.SelectedValue != "0") wheres[i] = String.Concat("WorkTimes.UserID=", ddlUser.SelectedValue);
                else wheres[i] = string.Empty;
                groupBys[i] = "FullName,WorkTimes.UserID";
                orderBys[i] = "FullName,WorkTimes.UserID";
                returnColumnss[i] = "FullName,UserID";
            }
            else
            {
                wheres[i] = string.Empty;
                selects[i] = String.Empty;
                groupBys[i] = String.Empty;
                orderBys[i] = String.Empty;
                returnColumnss[i] = String.Empty;
            }

            i = int.Parse(ddlActivityOrder.SelectedValue);

            if (ddlActivity.SelectedValue != "-1")
            {
                selects[i] = "isnull(WorkTimes.ActivityID,0) as ActivityID, isnull(ActivityName,N'" +
                    Resource.ResourceManager["reports_ActivityNotSpecified"] + "') as Activity";
                if (ddlActivity.SelectedValue != "0") wheres[i] = String.Concat("WorkTimes.ActivityID = ", ddlActivity.SelectedValue);
                else wheres[i] = string.Empty;
                groupBys[i] = "ActivityName,WorkTimes.ActivityID";
                orderBys[i] = "Activity,WorkTimes.ActivityID";
                returnColumnss[i] = "ActivityName,ActivityID";
            }
            else
            {
                selects[i] = String.Empty;
                wheres[i] = String.Empty;
                groupBys[i] = String.Empty;
                orderBys[i] = String.Empty;
                returnColumnss[i] = String.Empty;
            }

            i = int.Parse(ddlBuildingTypeOrder.SelectedValue);

            if (ddlBuildingType.SelectedValue != "-1")
            {
                selects[i] = String.Concat("isnull(Projects.BuildingTypeID,0) as BuildingTypeID, isnull(BuildingTypes.BuildingType,N'",
                Resource.ResourceManager["reports_NotSpecified"], "') as BuildingType");
                if (ddlBuildingType.SelectedValue != "0") wheres[i] = String.Concat("Projects.BuildingTypeID = ", ddlBuildingType.SelectedValue);
                else wheres[i] = string.Empty;
                groupBys[i] = "BuildingType,Projects.BuildingTypeID";
                orderBys[i] = "BuildingType,Projects.BuildingTypeID";
                returnColumnss[i] = "BuildingType,BuildingTypeID";
            }
            else
            {
                selects[i] = string.Empty;
                wheres[i] = String.Empty;
                groupBys[i] = String.Empty;
                orderBys[i] = String.Empty;
                returnColumnss[i] = String.Empty;
            }

            i = int.Parse(ddlProjectOrder.SelectedValue);

            if (ddlProject.SelectedValue != "-1")
            {
                selects[i] = "WorkTimes.ProjectID, isnull(Projects.AdministrativeName, Projects.ProjectName) as Project";
                if (ddlProject.SelectedValue != "0") wheres[i] = String.Concat("WorkTimes.ProjectID = ", ddlProject.SelectedValue);
                else wheres[i] = string.Empty;
                groupBys[i] = "AdministrativeName,ProjectName,WorkTimes.ProjectID";
                orderBys[i] = "Project,workTimes.ProjectID";
                returnColumnss[i] = "ProjectName,ProjectID";
            }
            else
            {
                selects[i] = string.Empty;
                wheres[i] = string.Empty;
                groupBys[i] = String.Empty;
                orderBys[i] = String.Empty;
                returnColumnss[i] = String.Empty;
            }


            for (i = 0; i < selects.Length; i++)
                if (selects[i] != String.Empty) select += selects[i] + ",";

            select = select.Trim(',');

            for (i = 0; i < wheres.Length; i++)
                if (wheres[i] != String.Empty) where += wheres[i] + " and ";
            where += "10=10";

            for (i = 0; i < groupBys.Length; i++)
                if (groupBys[i] != String.Empty) groupBy += groupBys[i] + ",";

            groupBy = groupBy.Trim(',');

            for (i = 0; i < orderBys.Length; i++)
                if (orderBys[i] != String.Empty) orderBy += orderBys[i] + ",";

            orderBy = orderBy.Trim(',');

            for (i = 0; i < returnColumnss.Length; i++)
                if (returnColumnss[i] != String.Empty) returnColumns += returnColumnss[i] + ",";

            returnColumns = returnColumns.Trim(',');

        }
		#endregion

        private void GetSqlExpressions(out string select, out string where, out string groupBy, out string orderBy, out string returnColumns)
        {
            #region User selections

            int userID = int.Parse(ddlUser.SelectedValue);
            if (!this.LoggedUser.HasPaymentRights)
                userID = LoggedUser.UserID;
            int userOrder = int.Parse(ddlUserOrder.SelectedValue);

            int projectOrder = int.Parse(ddlProjectOrder.SelectedValue);
            int projectID = int.Parse(ddlProject.SelectedValue);

            int projectStatus = int.Parse(ddlProjectStatus.SelectedValue);

            int activityID = int.Parse(ddlActivity.SelectedValue);
            int activityOrder = int.Parse(ddlActivityOrder.SelectedValue);

            int buildingTypeID = int.Parse(ddlBuildingType.SelectedValue);
            int buildingTypeOrder = int.Parse(ddlBuildingTypeOrder.SelectedValue);

            #endregion

            bool excludeBT = ((projectOrder < buildingTypeOrder) && (projectID >= 0) && (buildingTypeID > -1));

            string[] selects = new string[4];
            string[] wheres = new string[4];
            string[] groupBys = new string[4];
            string[] orderBys = new string[4];
            string[] returnColumnss = new string[4];

            // User
            int i = userOrder;
            if (userID != -1)
            {
                selects[i] = "UserID,FullName";
                //				wheres[i] = (userID!=0) ? String.Concat("UserID=", userID.ToString()) : String.Empty;
                string SUserStatus = String.Empty;
                if (ddlUserStatus.SelectedValue == "1") SUserStatus = "isactiveUser=1";
                else
                    if (ddlUserStatus.SelectedValue == "2") SUserStatus = "isactiveUser=0";
                wheres[i] = (ddlUser.SelectedValue != "0")
                    ? String.Concat("UserID=", userID.ToString()) : SUserStatus;
                groupBys[i] = "FullName,UserID";
                orderBys[i] = "FullName,UserID";
                returnColumnss[i] = "UserID,FullName";
            }
            else
            {
                wheres[i] = string.Empty;
                selects[i] = String.Empty;
                groupBys[i] = String.Empty;
                orderBys[i] = String.Empty;
                returnColumnss[i] = String.Empty;
            }

            //Activity
            i = activityOrder;
            if (activityID != -1)
            {
                selects[i] = "isnull(ActivityID,-1) as ActivityID, isnull(ActivityName,N'" +
                    Resource.ResourceManager["reports_ActivityNotSpecified"] + "') as ActivityName";
                wheres[i] = (activityID != 0) ? String.Concat("ActivityID=", activityID.ToString()) : String.Empty;
                groupBys[i] = "ActivityName,ActivityID";
                orderBys[i] = "ActivityName,ActivityID";
                returnColumnss[i] = "ActivityID,ActivityName";
            }
            else
            {
                selects[i] = String.Empty;
                wheres[i] = String.Empty;
                groupBys[i] = String.Empty;
                orderBys[i] = String.Empty;
                returnColumnss[i] = String.Empty;
            }

            //Building type
            i = buildingTypeOrder;
            if (buildingTypeID != -1)
            {
                if (excludeBT) selects[i] = "isnull(BuildingTypeID,-1) as BuildingTypeID, BuildingType as BuildingType";
                else selects[i] = String.Concat("isnull(BuildingTypeID,-1) as BuildingTypeID, isnull(BuildingType,N'",
                                Resource.ResourceManager["reports_BuildingTypeNotSpecified"], "') as BuildingType");

                wheres[i] = (buildingTypeID != 0) ? String.Concat("BuildingTypeID=", buildingTypeID.ToString()) : String.Empty;
                groupBys[i] = "BuildingType,BuildingTypeID";
                orderBys[i] = "BuildingType,BuildingTypeID";
                returnColumnss[i] = "BuildingTypeID,BuildingType";
            }
            else
            {
                selects[i] = string.Empty;
                wheres[i] = String.Empty;
                groupBys[i] = String.Empty;
                orderBys[i] = String.Empty;
                returnColumnss[i] = String.Empty;
            }

            //project
            i = projectOrder;
            if (projectID != -1)
            {
                selects[i] = "ProjectID,ProjectName";

                string sProjectStatus = String.Empty;
                if (projectStatus == 1) sProjectStatus = "IsActive=1";
                else
                    if (projectStatus == 2) sProjectStatus = "IsActive=0";
                wheres[i] = (ddlProject.SelectedValue != "0")
          ? String.Concat("ProjectID=", projectID.ToString()) : sProjectStatus;

                groupBys[i] = "ProjectName,ProjectID";
                orderBys[i] = "ProjectName,ProjectID";
                returnColumnss[i] = "ProjectID,ProjectName";
            }
            else
            {
                selects[i] = string.Empty;
                wheres[i] = string.Empty;
                groupBys[i] = String.Empty;
                orderBys[i] = String.Empty;
                returnColumnss[i] = String.Empty;
            }

            //select
            System.Text.StringBuilder sbSelect = new System.Text.StringBuilder();
            for (i = 0; i < selects.Length; i++)
                if (selects[i] != String.Empty) sbSelect.Append(selects[i] + ",");
            select = sbSelect.ToString().Trim(',');
            //select = select + ",MAX(Coef) as c";
            // where
            System.Text.StringBuilder sbWhere = new System.Text.StringBuilder();
            for (i = 0; i < wheres.Length; i++)
                if (wheres[i] != String.Empty) sbWhere.Append(wheres[i] + " and ");
            sbWhere.Append("10=10");
            where = sbWhere.ToString();

            //group by
            System.Text.StringBuilder sbGroupBy = new System.Text.StringBuilder();
            for (i = 0; i < groupBys.Length; i++)
                if (groupBys[i] != String.Empty) sbGroupBy.Append(groupBys[i] + ",");
            groupBy = sbGroupBy.ToString().Trim(',');

            // order by
            System.Text.StringBuilder sbOrderBy = new System.Text.StringBuilder();
            for (i = 0; i < orderBys.Length; i++)
                if (orderBys[i] != String.Empty) sbOrderBy.Append(orderBys[i] + ",");
            orderBy = sbOrderBy.ToString().Trim(',');

            returnColumns = "";
            for (i = 0; i < returnColumnss.Length; i++)
                if (returnColumnss[i] != String.Empty) returnColumns += returnColumnss[i] + ",";

            returnColumns = returnColumns.Trim(',');

        }
		#endregion

        private void ddlProjectStatus_SelectedIndexChanged(object sender, System.EventArgs e)
        {
            UIHelpers.LoadBuildingTypes("", ddlBuildingType, (ProjectsData.ProjectsByStatus)int.Parse(ddlProjectStatus.SelectedValue));
            LoadProjects();
        }

        private void ddlBuildingType_SelectedIndexChanged(object sender, System.EventArgs e)
        {
            LoadProjects();
        }

        private void ddlUserStatus_SelectedIndexChanged(object sender, System.EventArgs e)
        {
            LoadUsers("0");
        }

        private void grdUsers_ItemCreated(object sender, System.Web.UI.WebControls.DataGridItemEventArgs e)
        {

        }

        private void grdOneEmployee_ItemDataBound(object sender, System.Web.UI.WebControls.DataGridItemEventArgs e)
        {

            if ((e.Item.ItemType == ListItemType.Item) || (e.Item.ItemType == ListItemType.AlternatingItem))
            {
                DataRowView drv = (DataRowView)e.Item.DataItem;
                if ((int)drv["UserID"] == -1)
                {
                    e.Item.CssClass = "reportGridHeader";
                    e.Item.ForeColor = System.Drawing.Color.Black;

                    e.Item.BackColor = System.Drawing.Color.WhiteSmoke;
                }
                int minutes = UIHelpers.ToInt(e.Item.Cells[1].Text);
                if (minutes != 0)
                    e.Item.Cells[1].Text = TimeHelper.HoursStringFromMinutes(minutes, false);
                else
                    e.Item.Cells[1].Text = TimeHelper.HoursStringFromMinutes(minutes, true);
                minutes = UIHelpers.ToInt(e.Item.Cells[2].Text);
                if (minutes != 0)
                    e.Item.Cells[2].Text = TimeHelper.HoursStringFromMinutes(minutes, false);
                else
                    e.Item.Cells[2].Text = TimeHelper.HoursStringFromMinutes(minutes, true);
                minutes = UIHelpers.ToInt(e.Item.Cells[3].Text);
                if (minutes != 0)
                    e.Item.Cells[3].Text = TimeHelper.HoursStringFromMinutes(minutes, false);
                else
                    e.Item.Cells[3].Text = TimeHelper.HoursStringFromMinutes(minutes, true);
                minutes = UIHelpers.ToInt(e.Item.Cells[4].Text);
                if (minutes != 0)
                    e.Item.Cells[4].Text = TimeHelper.HoursStringFromMinutes(minutes, false);
                else
                    e.Item.Cells[4].Text = TimeHelper.HoursStringFromMinutes(minutes, true);
            }
        }

        private void grdLate_ItemDataBound(object sender, System.Web.UI.WebControls.DataGridItemEventArgs e)
        {
            if ((e.Item.ItemType == ListItemType.Item) || (e.Item.ItemType == ListItemType.AlternatingItem))
            {

                DataRowView drv = (DataRowView)e.Item.DataItem;
                if ((int)drv["UserID"] == -1)
                {
                    e.Item.CssClass = "reportGridHeader";
                    e.Item.ForeColor = System.Drawing.Color.Black;

                    e.Item.BackColor = System.Drawing.Color.WhiteSmoke;
                }
                int minutes = UIHelpers.ToInt(e.Item.Cells[(int)LateColumns.LateMinutes].Text);
                if (minutes != 0)
                    e.Item.Cells[(int)LateColumns.LateMinutes].Text = TimeHelper.HoursStringFromMinutes(minutes, false);
                else
                    e.Item.Cells[(int)LateColumns.LateMinutes].Text = TimeHelper.HoursStringFromMinutes(minutes, true);

                minutes = UIHelpers.ToInt(e.Item.Cells[(int)LateColumns.TotalMinutes].Text);
                if (minutes != 0)
                    e.Item.Cells[(int)LateColumns.TotalMinutes].Text = TimeHelper.HoursStringFromMinutes(minutes, false);
                else
                    e.Item.Cells[(int)LateColumns.TotalMinutes].Text = TimeHelper.HoursStringFromMinutes(minutes, true);
            }
        }

		
		#region HeaderInfo

		

		#endregion

		#region WorkedHoursInfo

		public class WorkedHoursInfo
		{
			int _totalMinutes = 0;
			decimal _totalSalary = 0;
			int _totalMinutesWT = 0;
			int _totalMinutesNWT = 0;
			int _totalMinutesWeekend = 0;
			int _totalWorkDays;
			int _totalNonWorkDays;
			int _missingHours=0;

			int _totalLates;
			int _totalMinutesLate;

			int _daysNO = 0;
			string _daysNOString = String.Empty;
			int _daysPO;
			string _daysPOString;

			int _daysB;
			string _daysBString;
			public bool IsTraine=false;
            public WorkedHoursInfo(DataSet source, bool isTraine)
            {
                IsTraine = isTraine;
                CalculateHoursValues(source.Tables[0]);

                GetDays(source.Tables[1], out _daysPO, out _daysPOString);
                GetDays(source.Tables[2], out _daysNO, out _daysNOString);
                GetDays(source.Tables[3], out _daysB, out _daysBString);
            }
            public void SetTotals(int totalMinutesWT, int totalMinutesNWT, int missingHours, int nTotalTime)
            {
                _totalMinutesWT = totalMinutesWT;
                _totalMinutesNWT = totalMinutesNWT;
                _missingHours = missingHours;
                _totalMinutes = nTotalTime;
            }
            public void SetLates(DataSet source)
            {
                int nLateCount = 0;
                int nLate = 0;
                foreach (DataRow dr in source.Tables[0].Rows)
                {
                    if (dr["MinutesLate"] != DBNull.Value)
                    {
                        nLate += (int)dr["MinutesLate"];
                        if ((int)dr["MinutesLate"] > 0)
                            nLateCount++;
                    }

                }
                _totalLates = nLateCount;
                _totalMinutesLate = nLate;
            }
            private void CalculateHoursValues(DataTable dt)
            {
                _totalMinutes = 0;
                _totalMinutesWT = 0;
                _totalMinutesNWT = 0;
                _totalMinutesWeekend = 0;
                _totalNonWorkDays = 0;
                _totalWorkDays = 0;
                _totalSalary = 0;
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    DateTime tempDate = (DateTime)dt.Rows[i]["WorkDate"];
                    int tempMinutes = (int)dt.Rows[i]["WorkedMinutes"];
                    int tempMinutesNormal = (int)dt.Rows[i]["WorkedMinutesNormal"];
                    int tempMinutesOverTime = (int)dt.Rows[i]["WorkedMinutesOverTime"];
                    int tempMinutesWeekend = (int)dt.Rows[i]["WorkedMinutesWeekend"];
                    if (UIHelpers.GetIDParam() == Constants._WorktimesID_NormalTime)
                        _totalSalary += (decimal)dt.Rows[i]["AmountNormal"] + (decimal)dt.Rows[i]["AmountOverTime"];
                    else
                        _totalSalary += (decimal)dt.Rows[i]["AmountOverTime"];
                    _totalMinutes += tempMinutes;
                    _totalMinutesWT += tempMinutesNormal;
                    _totalMinutesNWT += tempMinutesOverTime;
                    _totalMinutesWeekend += tempMinutesWeekend;
                    if ((tempDate.DayOfWeek == DayOfWeek.Saturday) || (tempDate.DayOfWeek == DayOfWeek.Sunday))
                    {
                        //						_totalMinutesWeekend+= tempMinutes;
                        _totalNonWorkDays++;
                    }
                    else
                    {
                        //						if (tempMinutes>480) 
                        //						{
                        //							_totalMinutesWT+= 480;
                        //							_totalMinutesNWT += (tempMinutes-480);	
                        //						}
                        //						else _totalMinutesWT+=tempMinutes;
                        _totalWorkDays++;
                    }

                }
            }

            private void GetDays(DataTable dt, out int days, out string daysString)
            {
                days = dt.Rows.Count;
                daysString = String.Empty;

                if (days == 0) return;

                DateTime startDate = (DateTime)dt.Rows[0]["WorkDate"];
                DateTime endDate = (DateTime)dt.Rows[0]["WorkDate"];

                for (int i = 1; i < dt.Rows.Count; i++)
                {
                    DateTime tempDate = (DateTime)dt.Rows[i]["WorkDate"];

                    if (endDate.Date.AddDays(1) == tempDate.Date) endDate = tempDate;
                    else
                    {

                        if (startDate.Date == endDate.Date) daysString += startDate.ToString("dd.MM") + "; ";
                        else daysString += startDate.ToString("dd.MM") + "-" + endDate.ToString("dd.MM") + "; ";
                        startDate = tempDate; endDate = tempDate;
                    }

                }
                if (startDate.Date == endDate.Date) daysString += startDate.ToString("dd.MM") + "; ";
                else daysString += startDate.ToString("dd.MM") + "-" + endDate.ToString("dd.MM") + "; ";
            }

			#region properties

			public decimal TotalHours
			{
				get
				{
					return TimeHelper.HoursFromMinutes(_totalMinutes);
				}
			}
			public decimal TotalSalary
			{
				get
				{
					return _totalSalary;
				}
			}
			public decimal TotalHoursInWorkTime
			{
				get
				{
					return TimeHelper.HoursFromMinutes(_totalMinutesWT);
				}
			}
			public string TotalHoursLate
			{
				get
				{
					return TimeHelper.HoursStringFromMinutes(_totalMinutesLate,true);
				}
			}
			public int TotalLateCount
			{
				get
				{
					return _totalLates;
				}
			}
			public decimal TotalHoursInNonWorkTime
			{
				get
				{
					return TimeHelper.HoursFromMinutes(_totalMinutesNWT);
				}
			}
			public decimal MissingHours
			{
				get
				{
					return TimeHelper.HoursFromMinutes(_missingHours);
				}
			}

			public decimal TotalHoursWeekends
			{
				get
				{
					return TimeHelper.HoursFromMinutes(_totalMinutesWeekend);
				}
			}

			public int DaysNeplatenOtpusk
			{
				get
				{
					return _daysNO;
				}
			}

			public string DaysNeplatenOtpuskString
			{
				get
				{
					return _daysNOString;
				}
			}

			public int DaysPlatenOtpusk
			{
				get
				{
					return _daysPO;
				}
			}

			public string DaysPlatenOtpuskString
			{
				get
				{
					return _daysPOString;
				}
			}

			public int DaysIll
			{
				get
				{
					return _daysB;
				}
			}

			public string DaysIllString
			{
				get
				{
					return _daysBString;
				}
			}

			public int TotalWorkDays
			{
				get
				{
					return _totalWorkDays;
				}
			}

			public int TotalNonWorkDays
			{
				get
				{
					return _totalNonWorkDays;
				}
			}



			#endregion

			
		}

		#endregion
	}
}
