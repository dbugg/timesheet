namespace Asa.Timesheet.WebPages.UserControls
{
	using System;
	using System.Data;
	using System.Drawing;
	using System.Web;
	using System.Web.UI.WebControls;
	using System.Web.UI.HtmlControls;

	/// <summary>
	///		Summary description for EditForm.
	/// </summary>
	public class EditForm : System.Web.UI.UserControl
	{
    protected System.Web.UI.WebControls.TextBox txtElements;
    protected System.Web.UI.WebControls.TextBox hdnVMButtons;
    protected System.Web.UI.WebControls.TextBox hdnEMButtons;
    protected System.Web.UI.WebControls.TextBox hdnExcls;
    protected System.Web.UI.WebControls.TextBox hdnViewMode;

    private void Page_Load(object sender, System.EventArgs e)
    {

    }

		#region Web Form Designer generated code
    override protected void OnInit(EventArgs e)
    {
        //
        // CODEGEN: This call is required by the ASP.NET Web Form Designer.
        //
        InitializeComponent();
        base.OnInit(e);
    }
		
		/// <summary>
		///		Required method for Designer support - do not modify
		///		the contents of this method with the code editor.
		/// </summary>
    private void InitializeComponent()
    {
        this.Load += new System.EventHandler(this.Page_Load);

    }
		#endregion

    public void InitEdit(string disableIDs, string vmButtons, string emButtons, string excl)
    {
        this.txtElements.Text = disableIDs;
        this.hdnVMButtons.Text = vmButtons;
        this.hdnEMButtons.Text = emButtons;
        this.hdnExcls.Text = excl;


        string script = "<script>InitEdit('" + hdnViewMode.ClientID + "','" + txtElements.ClientID + "','" +
          hdnVMButtons.ClientID + "','" + hdnEMButtons.ClientID + "','" + hdnExcls.ClientID + "');</script>";

        Page.ClientScript.RegisterStartupScript(this.GetType(), "InitEdit", script);
    }
	}
}
