using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Data.SqlClient;
using log4net;
using Asa.Timesheet.Data;
using Asa.Timesheet.Data.Entities;
using Asa.Timesheet.Data.Util;
using Asa.Timesheet.Data.Reports;
using GrapeCity.ActiveReports.Export.Pdf.Section;
using GrapeCity.ActiveReports.Export.Excel.Section;
namespace Asa.Timesheet.WebPages.Reports
{
	/// <summary>
	/// Summary description for EmptyHours.
	/// </summary>
	public class EmptyHours : TimesheetPageBase
	{
		private static readonly DateTime MinReportDate = DateTime.ParseExact(System.Configuration.ConfigurationManager.AppSettings["MinReportDate"],
			 "ddMMyyyy", System.Globalization.CultureInfo.InvariantCulture);
		private static readonly ILog log = LogManager.GetLogger(typeof(EmptyHours));

		#region Web controls

		protected System.Web.UI.WebControls.PlaceHolder menuHolder;
		protected System.Web.UI.WebControls.Label lblInfo;
		protected System.Web.UI.WebControls.Label lblError;
		protected System.Web.UI.WebControls.Calendar calStartDate;
		protected System.Web.UI.WebControls.Calendar calEndDate;
		protected System.Web.UI.WebControls.Label lblStartDay;
		protected System.Web.UI.HtmlControls.HtmlTable tblFooter;
		protected System.Web.UI.WebControls.CheckBox ckDates;
		protected System.Web.UI.WebControls.Label lblSelStartDate;
		protected System.Web.UI.WebControls.Label lblEndDay;
		protected System.Web.UI.WebControls.Label lblSelEndDate;
		protected System.Web.UI.WebControls.Label lblSluj;
		protected System.Web.UI.WebControls.DropDownList ddlUser;
		protected Asa.Timesheet.WebPages.UserControls.PageHeader header;
		protected System.Web.UI.WebControls.DataGrid grdReport;
		protected System.Web.UI.WebControls.ImageButton ibXlsExport;
		protected System.Web.UI.WebControls.ImageButton ibPdfExport;
		protected System.Web.UI.WebControls.ImageButton ibGenerate;
		protected System.Web.UI.WebControls.DropDownList ddlUserStatus;
		protected System.Web.UI.WebControls.Label lblReportTitle;

		#endregion
		
		#region Page load

        private void Page_Load(object sender, System.EventArgs e)
        {
            if (!(LoggedUser.IsLeader || LoggedUser.IsAdmin || LoggedUser.IsAccountant)) ErrorRedirect(Resource.ResourceManager["noRightsAlert"]);

            if (!this.IsPostBack)
            {
                this.header.PageTitle = Resource.ResourceManager["report_NotEntered_PageTitle"];
                this.header.UserName = this.LoggedUser.UserName;

                if (!LoadUsers()) ErrorRedirect("dsfsadfsadf<br>" + Resource.ResourceManager["error_HitRefresh"]);

                calStartDate.SelectedDate = new DateTime(DateTime.Today.Year, DateTime.Today.Month, 1);
                calStartDate.VisibleDate = calStartDate.SelectedDate;
                lblSelStartDate.Text = String.Concat("(", calStartDate.SelectedDates[0].ToString("d"), ")");
                calEndDate.SelectedDate = DateTime.Today;
                calEndDate.VisibleDate = calEndDate.SelectedDate;
                lblSelEndDate.Text = String.Concat("(", calEndDate.SelectedDates[0].ToString("d"), ")");
            }


            UIHelpers.CreateMenuReports(menuHolder, LoggedUser);
        }

		#endregion

		#region Web Form Designer generated code
        override protected void OnInit(EventArgs e)
        {
            //
            // CODEGEN: This call is required by the ASP.NET Web Form Designer.
            //
            InitializeComponent();
            base.OnInit(e);
        }
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
        private void InitializeComponent()
        {
            this.ckDates.CheckedChanged += new System.EventHandler(this.ckDates_CheckedChanged);
            this.calStartDate.SelectionChanged += new System.EventHandler(this.calStartDate_SelectionChanged);
            this.calEndDate.SelectionChanged += new System.EventHandler(this.calEndDate_SelectionChanged);
            this.ddlUserStatus.SelectedIndexChanged += new System.EventHandler(this.ddlUserStatus_SelectedIndexChanged);
            this.ibGenerate.Click += new System.Web.UI.ImageClickEventHandler(this.ibGenerate_Click);
            this.ibPdfExport.Click += new System.Web.UI.ImageClickEventHandler(this.ibPdfExport_Click);
            this.ibXlsExport.Click += new System.Web.UI.ImageClickEventHandler(this.ibXlsExport_Click);
            this.Load += new System.EventHandler(this.Page_Load);

        }
		#endregion

		#region DB

        private bool LoadUsers()
        {
            //if (this.LoggedUser.IsLeader)
            {
                ddlUser.Visible = true;

                SqlDataReader reader = null;
                try
                {
                    switch (ddlUserStatus.SelectedValue)
                    {
                        case "0":
                            reader = UsersData.SelectUserNames(true, false, false, false); break;
                        case "1":
                            reader = UsersData.SelectUserNames(false, false, false, false); break;
                        case "2":
                            reader = UsersData.SelectUserNames1(false); break;
                    }
                    ddlUser.DataSource = reader;
                    ddlUser.DataValueField = "UserID";
                    ddlUser.DataTextField = "FullName";
                    ddlUser.DataBind();
                    ddlUser.Items.Insert(0, new ListItem("<" + Resource.ResourceManager["reports_ddlAllUsers"] + ">", "0"));
                    ddlUser.SelectedValue = "0";
                }
                catch (Exception ex)
                {
                    log.Error(ex);
                    return false;
                }
                finally
                {
                    if (reader != null) reader.Close();
                }
            }

            return true;
        }

        private ArrayList SelectEnteredDays(int userID, DateTime startDate, DateTime endDate)
        {
            ArrayList days = new ArrayList();

            SqlDataReader reader = null;
            try
            {
                reader = ReportsData.SelectEnteredDays(userID, startDate, endDate);
                while (reader.Read())
                {
                    days.Add(reader.GetDateTime(1));
                }
            }
            catch (Exception ex)
            {
                log.Error(ex);
                return null;
            }
            finally
            {
                if (reader != null) reader.Close();
            }

            return days;
        }

		#endregion

		#region Event handlers

        private void btnOK_Click(object sender, System.EventArgs e)
        {



        }
        private ArrayList GetData(out string title)
        {
            title = "";
            DateTime startDay = calStartDate.SelectedDate, endDay = calEndDate.SelectedDate;

            if (ckDates.Checked)
            {
                startDay = MinReportDate;
                endDay = DateTime.Today;
            }
            else
            {
                if (startDay < MinReportDate) startDay = MinReportDate;
                if (endDay > DateTime.Today) endDay = DateTime.Today;

                if (startDay > endDay)
                {
                    lblError.Text = Resource.ResourceManager["reports_errorDate"];
                    return null;
                }
            }

            int userID = int.Parse(ddlUser.SelectedValue);
            int startIndex, endIndex;

            if (userID == 0) { startIndex = 1; endIndex = ddlUser.Items.Count - 1; }
            else { startIndex = ddlUser.SelectedIndex; endIndex = ddlUser.SelectedIndex; }

            ArrayList reportData = new ArrayList();
            ArrayList days = null;

            for (int i = startIndex; i <= endIndex; i++)
            {
                DateTime _startDay = startDay;
                string userName = ddlUser.Items[i].Text;
                userID = int.Parse(ddlUser.Items[i].Value);

                UserInfo ui = UsersData.SelectUserByID(userID);
                if (ui.WorkStartedDate != Constants.DateMax)
                    if (ui.WorkStartedDate > _startDay)
                    {
                        _startDay = ui.WorkStartedDate;
                    }


                days = SelectEnteredDays(userID, _startDay, endDay);

                if (days == null) // error loading days from DB, set error message, continue to next user
                {
                    reportData.Add(new DaysListInfo(userID, userName, "Error"));
                    continue;
                }

                ArrayList notEnteredDays = TimeHelper.GetNotEnteredDaysList(_startDay, endDay, days, true);




                string s = DaysListInfo.GetDaysString(notEnteredDays, false, "dd.MM.yy");
                if (s == String.Empty) s = Resource.ResourceManager["reports_EmptyDays_NoEmptyDays"];
                DaysListInfo di = new DaysListInfo(userID, userName, s);
                di.Days = TimeHelper.GetNotEnteredDaysList(_startDay, endDay, days, false).Count;
                reportData.Add(di);

            }
            title = String.Concat(Resource.ResourceManager["reports_EmptyDays_GridTitle"],
                Resource.ResourceManager["rpt_periodFrom"], " ", startDay.ToShortDateString(), " ",
                Resource.ResourceManager["rpt_periodTo"], " " + endDay.ToShortDateString());
            lblReportTitle.Text = title;
            return reportData;
        }

        private void ckDates_CheckedChanged(object sender, System.EventArgs e)
        {
            lblSelStartDate.Text = ckDates.Checked ? String.Concat("(", MinReportDate.ToString("d"), ")") :
                String.Concat("(", calStartDate.SelectedDates[0].ToString("d"), ")");
            lblSelEndDate.Text = ckDates.Checked ? String.Concat("(", DateTime.Today.ToString("d"), ")") :
                String.Concat("(", calEndDate.SelectedDates[0].ToString("d"), ")");

            calStartDate.Enabled = calEndDate.Enabled = !ckDates.Checked;
        }

        private void calStartDate_SelectionChanged(object sender, System.EventArgs e)
        {
            lblSelStartDate.Text = String.Concat("(", calStartDate.SelectedDates[0].ToString("d"), ")");
            if (calStartDate.SelectedDates.Count > 1)
            {
                calEndDate.SelectedDate = calStartDate.SelectedDates[calStartDate.SelectedDates.Count - 1];
                calEndDate.VisibleDate = calEndDate.SelectedDate;

                calStartDate.SelectedDate = calStartDate.SelectedDates[0];
                lblSelEndDate.Text = String.Concat("(", calEndDate.SelectedDate.ToString("d"), ")"); ;
            }
        }

        private void calEndDate_SelectionChanged(object sender, System.EventArgs e)
        {
            lblSelEndDate.Text = String.Concat("(", calEndDate.SelectedDates[0].ToString("d"), ")");
        }

		#endregion

		#region Util

		
		#endregion

		#region Menu

//		private void CreateMenu()
//		{
//			UserControls.MenuTable menu = new UserControls.MenuTable();
//			menu.ID = "MenuTable";
//
//			// 'Links' group
//			ArrayList menuItems = new ArrayList();
//
//			menuItems.Add(new MenuItemInfo(Resource.ResourceManager["menuItem_Hours"], "../Hours.aspx", true));
//			menuItems.Add(new MenuItemInfo(Resource.ResourceManager["menuItem_Projects"], "../Projects.aspx"));
//			if (LoggedUser.IsLeader )
//			menuItems.Add(new MenuItemInfo(Resource.ResourceManager["menuItem_Clients"], "../Clients.aspx"));
//			if (LoggedUser.IsLeader )		
//			menuItems.Add(new MenuItemInfo(Resource.ResourceManager["menuItem_Subcontracters"], "../Subcontracters.aspx"));
//			if (LoggedUser.IsLeader)
//			menuItems.Add(new MenuItemInfo(Resource.ResourceManager["menuItem_Users"], "../Users.aspx"));	
//			menuItems.Add(new MenuItemInfo(Resource.ResourceManager["menuItem_Emails"], "../Emails.aspx"));
//
//			menu.AddMenuGroup(Resource.ResourceManager["menuHeader_Links"], 20, menuItems);
//			
//			// 'New' group
//			menuItems = new ArrayList();
//			menuItems.Add(new MenuItemInfo(Resource.ResourceManager["menuItem_NewProject"], "../EditProject.aspx"));
//			if (LoggedUser.IsLeader )
//			menuItems.Add(new MenuItemInfo(Resource.ResourceManager["menuItem_NewClient"], "../EditClient.aspx"));
//			if (LoggedUser.IsLeader)
//			menuItems.Add(new MenuItemInfo(Resource.ResourceManager["menuItem_NewUser"], "../EditUser.aspx"));
//			menuItems.Add(new MenuItemInfo(Resource.ResourceManager["menuItem_NewEmail"], "../EditEmail.aspx"));
//			menuItems.Add(new MenuItemInfo(Resource.ResourceManager["newcontracter_PageTitle"], "../EditSubContracter.aspx"));
//
//
//			menu.AddMenuGroup(Resource.ResourceManager["menuHeader_New"], 30, menuItems);
//
//			// "Reports' group
//			menuItems = new ArrayList();
//			
//			menuItems.Add(new MenuItemInfo(Resource.ResourceManager["menuItem_WorkTimeReport"], "WorkTimes.aspx"));
//			menuItems.Add(new MenuItemInfo(Resource.ResourceManager["menuItem_MinutesReport"], "Minutes.aspx"));
//			menuItems.Add(new MenuItemInfo(Resource.ResourceManager["menuItem_MailReport"], "../MailForm.aspx"));
//			if (LoggedUser.IsLeader)
//			menuItems.Add(new MenuItemInfo(Resource.ResourceManager["menuItem_Calendar"], "Calendar.aspx"));
//
//			menu.AddMenuGroup(Resource.ResourceManager["menuHeader_Reports"], 30, menuItems);
//
//			menuItems = new ArrayList();
//			menuItems.Add(new MenuItemInfo(Resource.ResourceManager["menuItem_Help"], SessionManager.GetReportsHelpLink(),true,Pages.WorkTimes));
//			menu.AddMenuGroup("", 10, menuItems);
//
//			menuHolder.Controls.Add(menu);
//		}

		#endregion

        private DataView GetDataView(out string t)
        {
            string title;
            ArrayList al = GetData(out title);
            t = title;
            DataTable dt = new DataTable();
            dt.Columns.Add(new DataColumn("UserName"));
            dt.Columns.Add(new DataColumn("DaysString"));
            dt.Columns.Add(new DataColumn("Days"));

            foreach (DaysListInfo li in al)
            {
                DataRow dr = dt.NewRow();
                dr[0] = li.UserName;
                dr[1] = li.DaysString;
                dr[2] = li.Days.ToString();
                dt.Rows.Add(dr);
            }
            dt.AcceptChanges();
            return new DataView(dt);
        }

        private void ibPdfExport_Click(object sender, System.Web.UI.ImageClickEventArgs e)
        {
            PdfExport pdf = new PdfExport();

            pdf.NeverEmbedFonts = "";
            string title;
            DataView dv = GetDataView(out title);

            GrapeCity.ActiveReports.SectionReport report = new EmptyHoursRpt(true, title);

            report.DataSource = dv;
            report.Run();

            System.IO.MemoryStream memoryFile = new System.IO.MemoryStream();
            pdf.Export(report.Document, memoryFile);

            Response.Clear();
            Response.AppendHeader("content-disposition"
                , "attachment; filename=" + "EmptyHours.pdf");
            Response.ContentType = "application/pdf";

            memoryFile.WriteTo(Response.OutputStream);
            Response.End();
        }

        private void ibXlsExport_Click(object sender, System.Web.UI.ImageClickEventArgs e)
        {
            XlsExport pdf = new XlsExport();



            string title;
            DataView dv = GetDataView(out title);

            GrapeCity.ActiveReports.SectionReport report = new EmptyHoursRpt(false, title);

            report.DataSource = dv;
            report.Run();


            System.IO.MemoryStream memoryFile = new System.IO.MemoryStream();
            pdf.Export(report.Document, memoryFile);

            Response.Clear();
            Response.AppendHeader("content-disposition"
                , "attachment; filename=" + "EmptyHours.xls");
            //Response.ContentType = "application/pdf";

            memoryFile.WriteTo(Response.OutputStream);
            Response.End();
        }

        private void ibGenerate_Click(object sender, System.Web.UI.ImageClickEventArgs e)
        {
            string title;
            grdReport.DataSource = GetData(out title);
            grdReport.DataBind();
        }

        private void ddlUserStatus_SelectedIndexChanged(object sender, System.EventArgs e)
        {
            LoadUsers();
        }
		
	
	}
}
