using System;
using System.Web;
using System.Web.UI.WebControls;
using System.Collections;
using System.Globalization;
using log4net;
namespace Asa.Timesheet.Data.Util
{

	#region Time Helper

	public class TimeHelper
	{
		private static readonly ILog log = LogManager.GetLogger(typeof(TimeHelper));
		public static DateTime GetDate(string s)
		{
			DateTime dt = Constants.DateMax;
			try
			{
				if(s!="")
					dt=DateTime.ParseExact(s,"dd.MM.yyyy",NumberFormatInfo.InvariantInfo);
				return dt;
			}
			catch
			{
				log.Error("Invalid Date");
				return dt;
			}
		}
		public static int TimeInMinutes(string sTime)
		{
			char[] separators = {':', '.' };
			string[] arr = sTime.Split(separators);
			try
			{
				return int.Parse(arr[0])*60 + int.Parse(arr[1]);
			}
			catch 
			{
				return -1;
			}
		}

		public static int TimeInMinutes(string sTime, char separator)
		{
			string[] arr = sTime.Split(separator);
			try
			{
				return int.Parse(arr[0])*60 + int.Parse(arr[1]);
			}
			catch 
			{
				return -1;
			}
		}

		public static string HoursStringFromMinutes(int inMinutes, bool useZero)
		{
			int hours = (inMinutes/60);
			int minutes = (inMinutes%60);

			string sHour, sMin;

			if (useZero) return  String.Concat(hours.ToString(),Resource.ResourceManager["common_HourAbbr"],
													"",minutes.ToString(), Resource.ResourceManager["common_MinuteAbbr"]);
			else
			{
				sHour = (hours == 0) ? String.Empty : String.Concat(hours.ToString(), Resource.ResourceManager["common_HourAbbr"]);
				sMin = (minutes == 0) ? String.Empty : String.Concat(minutes.ToString(),Resource.ResourceManager["common_MinuteAbbr"]);

				return String.Concat(sHour,sMin);
			}
			
		}

		public static decimal HoursFromMinutes(int minutes)
		{
			decimal ret = new Decimal(minutes);
			ret/=60;
			return ret;
		}

		#endregion

		public static ArrayList GetNotEnteredDaysList(DateTime startDay, DateTime endDay, 
			ArrayList enteredDays,bool includeWeekends)
		{
			DateTime dt = startDay;
			ArrayList notEnteredDays = new ArrayList();
			
			while (dt<=endDay)
			{
				if (enteredDays.BinarySearch(dt) < 0) 
				{
					if (includeWeekends) notEnteredDays.Add(dt);
					else
					if (!CalendarDAL.IsHoliday(dt)) notEnteredDays.Add(dt);
				}

				dt = dt.AddDays(1);
			}

			return notEnteredDays;
		}
		
		public static string InvokePopupCal(TextBox Fields) 
		{
			// Define character array to trim from language strings
			char[] TrimChars= new char[] {',' ,' '};

			// Get culture array of month names and convert to string for
			// passing to the popup calendar
			string  MonthNameString="";
			foreach(string Month in DateTimeFormatInfo.CurrentInfo.MonthNames)
				MonthNameString += Month + ",";
	
			MonthNameString = MonthNameString.TrimEnd(TrimChars);

			// Get culture array of day names and convert to string for
			// passing to the popup calendar
			string DayNameString = "";
			foreach(string Day in DateTimeFormatInfo.CurrentInfo.DayNames)
				DayNameString += Day.Substring(0, 3) + ",";
			DayNameString = DayNameString.TrimEnd(TrimChars);

			//Get the short date pattern for the culture
			string FormatString  = Constants.TextDateFormat;
			//DateTimeFormatInfo.CurrentInfo.ShortDatePattern.ToString();

			return "javascript:popupCal('Cal','" + Fields.ClientID + "','" + FormatString + "','" +  MonthNameString + "','" +  DayNameString + "');";


		}
		public static string FormatDateO(object odt)
		{
			if(odt==null || odt==DBNull.Value)
				return "";
			DateTime dt = (DateTime)odt;
			if(dt==Constants.DateMin || dt==Constants.DateMax || dt==new DateTime())
				return "";
			return dt.ToString("dd.MM.yyyy");
		}
		public static string FormatDate(DateTime dt)
		{
			if(dt==Constants.DateMin || dt==Constants.DateMax || dt==new DateTime())
				return "";
			return dt.ToString("dd.MM.yyyy", System.Globalization.CultureInfo.InvariantCulture);
		}
		public static string FormatDateInv(DateTime dt)
		{
			if(dt==Constants.DateMin || dt==Constants.DateMax || dt==new DateTime())
				return "";
			return dt.ToString("dd.MM.yyyy", System.Globalization.CultureInfo.InvariantCulture);
		}
	}
}
