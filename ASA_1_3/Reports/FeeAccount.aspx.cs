using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Data.SqlClient;
using log4net;
using Asa.Timesheet.Data;
using Asa.Timesheet.Data.Entities;
using Asa.Timesheet.Data.Util;
using GrapeCity.ActiveReports.Export.Pdf.Section;
using GrapeCity.ActiveReports.Export.Excel.Section;

namespace Asa.Timesheet.WebPages.Reports
{
	/// <summary>
	/// Summary description for WorkTimes.
	/// </summary>
	public class FeeAccount : TimesheetPageBase
	{
		private static readonly ILog log = LogManager.GetLogger(typeof(FeeAccount));
	
		#region Web controls

		protected System.Web.UI.WebControls.Label lblTitle;
		protected System.Web.UI.WebControls.Label lblUser;
		protected System.Web.UI.WebControls.Label lblObekt;
		protected System.Web.UI.WebControls.Label Label5;
		protected System.Web.UI.WebControls.PlaceHolder menuHolder;
		protected System.Web.UI.WebControls.DropDownList ddlPeriodType;
		protected System.Web.UI.WebControls.Label lblStartDay;
		protected System.Web.UI.WebControls.Label lblEndDay;
		protected System.Web.UI.WebControls.Calendar calStartDate;
		protected System.Web.UI.WebControls.Calendar calEndDate;
		protected System.Web.UI.WebControls.Label lblSelStartDate;
		protected System.Web.UI.WebControls.Label lblSelEndDate;
		protected System.Web.UI.WebControls.Label lblError;
		protected System.Web.UI.WebControls.Label lblInfo;
		protected System.Web.UI.WebControls.ImageButton ibGenerate;
		protected System.Web.UI.WebControls.ImageButton ibPdfExport;
		protected System.Web.UI.WebControls.ImageButton ibXlsExport;
		protected Asa.Timesheet.WebPages.UserControls.PageHeader header;
		protected System.Web.UI.WebControls.CheckBox ckDates;
		protected System.Web.UI.WebControls.DataGrid grdReport;
		protected System.Web.UI.WebControls.Label lblReportTitle;
		protected System.Web.UI.WebControls.Label lblCreatedBy;
		protected System.Web.UI.HtmlControls.HtmlTable tblFooter;
		protected System.Web.UI.WebControls.Label lblCreatedByLbl;
		protected System.Web.UI.WebControls.Label Label2;
		protected System.Web.UI.WebControls.DropDownList ddlType;
		protected System.Web.UI.WebControls.Label Label4;
		protected System.Web.UI.HtmlControls.HtmlTable tblHeader;
		protected System.Web.UI.WebControls.Label Label6;
		protected System.Web.UI.HtmlControls.HtmlTable tblLegend;
		protected System.Web.UI.WebControls.DropDownList ddlProjectsStatus;
		protected System.Web.UI.WebControls.Label Label7;
		protected System.Web.UI.WebControls.DropDownList ddlBuildingTypes;
		protected System.Web.UI.WebControls.CheckBox ckClient;
		protected System.Web.UI.WebControls.Label Label1;
		protected System.Web.UI.WebControls.DropDownList ddlProjects;
		protected System.Web.UI.WebControls.Label lblNoDataFound;

		#endregion

		#region Page load

        private void Page_Load(object sender, System.EventArgs e)
        {
            if (!this.LoggedUser.HasPaymentRights) ErrorRedirect(Resource.ResourceManager["noRightsAlert"]);
            if (!this.IsPostBack)
            {
                header.PageTitle = Resource.ResourceManager["FeeAccount_Title"];
                header.UserName = this.LoggedUser.UserName;


                UIHelpers.LoadProjectStatus(ddlProjectsStatus, (int)ProjectsData.ProjectsByStatus.Active);
                UIHelpers.LoadBuildingTypes("", ddlBuildingTypes);

                //
                calStartDate.SelectedDate = calStartDate.VisibleDate = new DateTime(DateTime.Today.Year, DateTime.Today.Month, 1);
                lblSelStartDate.Text = "(" + Resource.ResourceManager["reports_SelectDate"] + ")";
                calEndDate.SelectedDate = calEndDate.VisibleDate = DateTime.Today;
                lblSelEndDate.Text = "(" + Resource.ResourceManager["reports_SelectDate"] + ")";
                LoadProjects();

            }
            else
                UIHelpers.LoadBuildingTypes(ddlBuildingTypes.SelectedValue, ddlBuildingTypes, (ProjectsData.ProjectsByStatus)int.Parse(ddlProjectsStatus.SelectedValue));



            UIHelpers.CreateMenuReports(menuHolder, LoggedUser);

        }
		
		#endregion

		#region Web Form Designer generated code
        override protected void OnInit(EventArgs e)
        {
            //
            // CODEGEN: This call is required by the ASP.NET Web Form Designer.
            //
            InitializeComponent();
            base.OnInit(e);
        }
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
        private void InitializeComponent()
        {
            this.ckDates.CheckedChanged += new System.EventHandler(this.ckDates_CheckedChanged);
            this.calStartDate.SelectionChanged += new System.EventHandler(this.calStartDate_SelectionChanged);
            this.calEndDate.SelectionChanged += new System.EventHandler(this.calEndDate_SelectionChanged);
            this.ddlProjectsStatus.SelectedIndexChanged += new System.EventHandler(this.ddlProjectsStatus_SelectedIndexChanged);
            this.ddlBuildingTypes.SelectedIndexChanged += new System.EventHandler(this.ddlBuildingTypes_SelectedIndexChanged);
            this.ibGenerate.Click += new System.Web.UI.ImageClickEventHandler(this.ibGenerate_Click);
            this.ibPdfExport.Click += new System.Web.UI.ImageClickEventHandler(this.ibPdfExport_Click);
            this.ibXlsExport.Click += new System.Web.UI.ImageClickEventHandler(this.ibXlsExport_Click);
            this.grdReport.ItemDataBound += new System.Web.UI.WebControls.DataGridItemEventHandler(this.grdReport_ItemDataBound);
            this.Load += new System.EventHandler(this.Page_Load);

        }
		#endregion

		#region EventHandlers
        private void ibGenerate_Click(object sender, System.Web.UI.ImageClickEventArgs e)
        {

            int nID = UIHelpers.ToInt(ddlProjects.SelectedValue);
            lblCreatedBy.Text = this.LoggedUser.UserName;

            lblReportTitle.Visible = true;

            lblCreatedBy.Visible = true;
            lblCreatedByLbl.Visible = true;
            if (ddlProjects.SelectedValue != "0")
            {
                lblObekt.Text = ProjectsData.SelectProjectName(nID);
                Label5.Visible = lblObekt.Visible = true;
            }
            else
            {
                Label5.Visible = lblObekt.Visible = false;
            }
            DateTime dtStart = Constants.DateMax;
            DateTime dtEnd = Constants.DateMax;
            if (!ckDates.Checked)
            {
                dtStart = calStartDate.SelectedDate;
                dtEnd = calEndDate.SelectedDate;
            }
            if (!ckDates.Checked)
            {
                string HeaderText = string.Concat(grdReport.Columns[8].HeaderText, " ", dtEnd.ToShortDateString());
                grdReport.Columns[8].HeaderText = HeaderText;
            }
            else
            {
                string HeaderText = string.Concat(grdReport.Columns[8].HeaderText, " ", DateTime.Now.ToShortDateString());
                grdReport.Columns[8].HeaderText = HeaderText;
            }

            DataSet ds = ReportsData.ExecuteReportsFeeAccount(nID, dtStart, dtEnd, ddlProjectsStatus.SelectedIndex, UIHelpers.ToInt(ddlBuildingTypes.SelectedValue));
            DataTable dt1 = ds.Tables[0];
            DataTable dt2 = ds.Tables[1];
            DataTable dt = dt1.Clone();
            for (int i = dt2.Rows.Count - 1; i > -1; i--)
            {
                DataRow dr = dt2.Rows[i];
                if (dr["ORDERCLAUSE"].ToString() == "3")
                {
                    string Client = dr["ClientName"].ToString();
                    if (Client.Length == 0)
                        dt2.Rows.RemoveAt(i);
                }
            }
            //			foreach(DataRow dr in dt2.Rows)
            //			{
            //				if(dr["ORDERCLAUSE"].ToString() == "3")
            //				{
            //					string Client = dr["ClientName"].ToString();
            //					if(Client.Length==0)
            //						dt2.Rows.Remove(dr);
            //				}
            //			}
            if (dt1.Rows.Count != 0)
            {
                for (int i = 0; i < dt1.Rows.Count - 1; i++)
                {
                    DataRow dr = dt.NewRow();
                    dr.ItemArray = dt1.Rows[i].ItemArray;
                    dt.Rows.Add(dr);
                }
                if (dt2.Rows.Count == 0)
                {
                    DataRow dr = dt.NewRow();
                    dr.ItemArray = dt1.Rows[dt1.Rows.Count - 1].ItemArray;
                    dt.Rows.Add(dr);
                }
            }
            if (dt2.Rows.Count != 0)
            {
                for (int i = 0; i < dt2.Rows.Count - 1; i++)
                {
                    DataRow dr = dt.NewRow();
                    dr.ItemArray = dt2.Rows[i].ItemArray;
                    dt.Rows.Add(dr);
                }
                if (dt1.Rows.Count != 0)
                {
                    DataRow dr1 = dt.NewRow();
                    dr1.ItemArray = dt2.Rows[dt2.Rows.Count - 1].ItemArray;
                    DataRow drEnd1 = dt1.Rows[dt1.Rows.Count - 1];
                    decimal db1 = (decimal)drEnd1[4];
                    DataRow drEnd2 = dt2.Rows[dt2.Rows.Count - 1];
                    decimal db2 = (decimal)drEnd2[4];
                    decimal db = db1 + db2;
                    dr1[4] = db;
                    db = (decimal)dt1.Rows[dt1.Rows.Count - 1][5] + (decimal)dt2.Rows[dt2.Rows.Count - 1][5];
                    dr1[5] = db.ToString();
                    db = (decimal)dt1.Rows[dt1.Rows.Count - 1][6] + (decimal)dt2.Rows[dt2.Rows.Count - 1][6];
                    dr1[6] = db.ToString();
                    dt.Rows.Add(dr1);
                }
                else
                {
                    DataRow dr2 = dt.NewRow();
                    dr2.ItemArray = dt2.Rows[dt2.Rows.Count - 1].ItemArray;
                    dt.Rows.Add(dr2);
                }
            }
            DataView dv = new DataView(dt);

            grdReport.DataSource = dv;

            grdReport.DataBind();
            if (grdReport.Items.Count == 0)
            {
                lblNoDataFound.Visible = true;
                grdReport.Visible = false;
            }
            else
            {
                grdReport.Visible = true;

                lblNoDataFound.Visible = false;
            }

        }
        private void ckDates_CheckedChanged(object sender, System.EventArgs e)
        {
            bool bEnable = !ckDates.Checked;
            calStartDate.Enabled = calEndDate.Enabled = bEnable;

            if (bEnable)
            {
                lblSelStartDate.Text = String.Concat("(" + calStartDate.SelectedDate.ToString("d"), ")");

                lblSelEndDate.Text = String.Concat("(" + calEndDate.SelectedDate.ToString("d"), ")");
            }
            else
            {
                lblSelStartDate.Text = lblSelEndDate.Text = String.Concat("(", Resource.ResourceManager["reports_SelectDate"], ")");
            }

        }

        private void calStartDate_SelectionChanged(object sender, System.EventArgs e)
        {
            lblSelStartDate.Text = String.Concat("(", calStartDate.SelectedDates[0].ToString("dd.MM.yyyy"), ")");
            if (calStartDate.SelectedDates.Count > 1)
            {
                calEndDate.SelectedDate = calStartDate.SelectedDates[calStartDate.SelectedDates.Count - 1];
                calEndDate.VisibleDate = calEndDate.SelectedDate;

                calStartDate.SelectedDate = calStartDate.SelectedDates[0];
                lblSelEndDate.Text = String.Concat("(", calEndDate.SelectedDate.ToString("dd.MM.yyyy"), ")"); ;

            }

        }

        private void calEndDate_SelectionChanged(object sender, System.EventArgs e)
        {
            lblSelEndDate.Text = String.Concat("(", calEndDate.SelectedDates[0].ToString("dd.MM.yyyy"), ")");

        }

        private void grdReport_ItemDataBound(object sender, DataGridItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Item
                || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                if (UIHelpers.ToInt(e.Item.Cells[2].Text) > 0)
                {
                    double sum1 = UIHelpers.ToDouble(e.Item.Cells[2].Text);
                    double sum2 = UIHelpers.ToDouble(e.Item.Cells[4].Text);
                    double delenie = sum2 / sum1;
                    e.Item.Cells[3].Text = delenie.ToString();
                }
                if (e.Item.Cells[10].Text == "2")
                {
                    e.Item.CssClass = "ReportsSubTotal";
                    //					e.Item.Cells[2].Text="";
                    e.Item.Cells[3].Text = "";
                    e.Item.Cells[4].Text = "";
                }
                if (e.Item.Cells[10].Text == "4")
                {
                    e.Item.CssClass = "ReportsSubTotal";
                    //					e.Item.Cells[2].Text="";
                }
                if (e.Item.Cells[10].Text == "3")
                {
                    e.Item.Cells[3].Text = "";
                    e.Item.Cells[4].Text = "";
                }
                if (e.Item.Cells[1].Text == string.Concat(Resource.ResourceManager["reports_Total"], ":"))
                {
                    e.Item.CssClass = "ReportsTotal";
                    e.Item.Cells[2].Text = "";
                    e.Item.Cells[3].Text = "";
                    e.Item.Cells[4].Text = "";
                }
                decimal all = decimal.Parse(e.Item.Cells[5].Text);
                decimal payd = decimal.Parse(e.Item.Cells[6].Text);
                if (all == 0)
                {
                    e.Item.Cells[7].Text = string.Concat("100.00%");
                    e.Item.Cells[9].Text = string.Concat("0.00%");
                }
                else
                {
                    decimal per = 100 * payd / all;
                    string textPercent = per.ToString("0.00");
                    textPercent = string.Concat(textPercent, "%");
                    e.Item.Cells[7].Text = textPercent;
                    per = 100 - per;
                    textPercent = per.ToString("0.00");
                    textPercent = string.Concat(textPercent, "%");
                    e.Item.Cells[9].Text = textPercent;
                }
            }

        }

        private void ibPdfExport_Click(object sender, System.Web.UI.ImageClickEventArgs e)
        {
            PdfExport pdf = new PdfExport();
            int nID = UIHelpers.ToInt(ddlProjects.SelectedValue);
            pdf.NeverEmbedFonts = "";
            string ProjectName = string.Empty;
            if (ddlProjects.SelectedValue != "0")
            {
                ProjectName = ProjectsData.SelectProjectName(nID);
            }
            string DateTo = DateTime.Now.ToShortDateString();
            DateTime dtStart = Constants.DateMax;
            DateTime dtEnd = Constants.DateMax;
            if (!ckDates.Checked)
            {
                dtStart = calStartDate.SelectedDate;
                dtEnd = calEndDate.SelectedDate;
                DateTo = dtEnd.ToShortDateString();
            }
            DataSet ds = ReportsData.ExecuteReportsFeeAccount(nID, dtStart, dtEnd, ddlProjectsStatus.SelectedIndex, UIHelpers.ToInt(ddlBuildingTypes.SelectedValue));
            DataTable dt1 = ds.Tables[0];
            DataTable dt2 = ds.Tables[1];
            DataTable dt = dt1.Clone();
            for (int i = dt2.Rows.Count - 1; i > -1; i--)
            {
                DataRow dr = dt2.Rows[i];
                if (dr["ORDERCLAUSE"].ToString() == "3")
                {
                    string Client = dr["ClientName"].ToString();
                    if (Client.Length == 0)
                        dt2.Rows.RemoveAt(i);
                }
            }
            if (dt1.Rows.Count != 0)
            {
                for (int i = 0; i < dt1.Rows.Count - 1; i++)
                {
                    DataRow dr = dt.NewRow();
                    dr.ItemArray = dt1.Rows[i].ItemArray;
                    dt.Rows.Add(dr);
                }
                if (dt2.Rows.Count == 0)
                {
                    DataRow dr = dt.NewRow();
                    dr.ItemArray = dt1.Rows[dt1.Rows.Count - 1].ItemArray;
                    dt.Rows.Add(dr);
                }
            }
            if (dt2.Rows.Count != 0)
            {
                for (int i = 0; i < dt2.Rows.Count - 1; i++)
                {
                    DataRow dr = dt.NewRow();
                    dr.ItemArray = dt2.Rows[i].ItemArray;
                    dt.Rows.Add(dr);
                }
                if (dt1.Rows.Count != 0)
                {
                    DataRow dr1 = dt.NewRow();
                    dr1.ItemArray = dt2.Rows[dt2.Rows.Count - 1].ItemArray;
                    DataRow drEnd1 = dt1.Rows[dt1.Rows.Count - 1];
                    decimal db1 = (decimal)drEnd1[4];
                    DataRow drEnd2 = dt2.Rows[dt2.Rows.Count - 1];
                    decimal db2 = (decimal)drEnd2[4];
                    decimal db = db1 + db2;
                    dr1[4] = db;
                    db = (decimal)dt1.Rows[dt1.Rows.Count - 1][5] + (decimal)dt2.Rows[dt2.Rows.Count - 1][5];
                    dr1[5] = db.ToString();
                    db = (decimal)dt1.Rows[dt1.Rows.Count - 1][6] + (decimal)dt2.Rows[dt2.Rows.Count - 1][6];
                    dr1[6] = db.ToString();
                    dt.Rows.Add(dr1);
                }
                else
                {
                    DataRow dr2 = dt.NewRow();
                    dr2.ItemArray = dt2.Rows[dt2.Rows.Count - 1].ItemArray;
                    dt.Rows.Add(dr2);
                }
            }
            DataView dv = new DataView(dt);

            GrapeCity.ActiveReports.SectionReport report = new FeeAccountRpt(true, ProjectName, dv, this.LoggedUser.FullName, DateTo);
            report.PageSettings.Orientation = GrapeCity.ActiveReports.Document.Section.PageOrientation.Landscape;

            report.Run();

            System.IO.MemoryStream memoryFile = new System.IO.MemoryStream();

            pdf.Export(report.Document, memoryFile);

            Response.Clear();
            Response.AppendHeader("content-disposition"
                , "attachment; filename=" + "FeeAccount.pdf");
            Response.ContentType = "application/pdf";

            memoryFile.WriteTo(Response.OutputStream);
            Response.End();
        }

        private void ibXlsExport_Click(object sender, System.Web.UI.ImageClickEventArgs e)
        {
            XlsExport pdf = new XlsExport();
            int nID = UIHelpers.ToInt(ddlProjects.SelectedValue);

            string ProjectName = string.Empty;
            if (ddlProjects.SelectedValue != "0")
            {
                ProjectName = ProjectsData.SelectProjectName(nID);
            }
            string DateTo = DateTime.Now.ToShortDateString();
            DateTime dtStart = Constants.DateMax;
            DateTime dtEnd = Constants.DateMax;
            if (!ckDates.Checked)
            {
                dtStart = calStartDate.SelectedDate;
                dtEnd = calEndDate.SelectedDate;
                DateTo = dtEnd.ToShortDateString();
            }
            DataSet ds = ReportsData.ExecuteReportsFeeAccount(nID, dtStart, dtEnd, ddlProjectsStatus.SelectedIndex, UIHelpers.ToInt(ddlBuildingTypes.SelectedValue));
            DataTable dt1 = ds.Tables[0];
            DataTable dt2 = ds.Tables[1];
            DataTable dt = dt1.Clone();
            for (int i = dt2.Rows.Count - 1; i > -1; i--)
            {
                DataRow dr = dt2.Rows[i];
                if (dr["ORDERCLAUSE"].ToString() == "3")
                {
                    string Client = dr["ClientName"].ToString();
                    if (Client.Length == 0)
                        dt2.Rows.RemoveAt(i);
                }
            }
            if (dt1.Rows.Count != 0)
            {
                for (int i = 0; i < dt1.Rows.Count - 1; i++)
                {
                    DataRow dr = dt.NewRow();
                    dr.ItemArray = dt1.Rows[i].ItemArray;
                    dt.Rows.Add(dr);
                }
                if (dt2.Rows.Count == 0)
                {
                    DataRow dr = dt.NewRow();
                    dr.ItemArray = dt1.Rows[dt1.Rows.Count - 1].ItemArray;
                    dt.Rows.Add(dr);
                }
            }
            if (dt2.Rows.Count != 0)
            {
                for (int i = 0; i < dt2.Rows.Count - 1; i++)
                {
                    DataRow dr = dt.NewRow();
                    dr.ItemArray = dt2.Rows[i].ItemArray;
                    dt.Rows.Add(dr);
                }
                if (dt1.Rows.Count != 0)
                {
                    DataRow dr1 = dt.NewRow();
                    dr1.ItemArray = dt2.Rows[dt2.Rows.Count - 1].ItemArray;
                    DataRow drEnd1 = dt1.Rows[dt1.Rows.Count - 1];
                    decimal db1 = (decimal)drEnd1[4];
                    DataRow drEnd2 = dt2.Rows[dt2.Rows.Count - 1];
                    decimal db2 = (decimal)drEnd2[4];
                    decimal db = db1 + db2;
                    dr1[4] = db;
                    db = (decimal)dt1.Rows[dt1.Rows.Count - 1][5] + (decimal)dt2.Rows[dt2.Rows.Count - 1][5];
                    dr1[5] = db.ToString();
                    db = (decimal)dt1.Rows[dt1.Rows.Count - 1][6] + (decimal)dt2.Rows[dt2.Rows.Count - 1][6];
                    dr1[6] = db.ToString();
                    dt.Rows.Add(dr1);
                }
                else
                {
                    DataRow dr2 = dt.NewRow();
                    dr2.ItemArray = dt2.Rows[dt2.Rows.Count - 1].ItemArray;
                    dt.Rows.Add(dr2);
                }
            }
            DataView dv = new DataView(dt);

            GrapeCity.ActiveReports.SectionReport report = new FeeAccountRpt(false, ProjectName, dv, this.LoggedUser.FullName, DateTo);
            report.PageSettings.Orientation = GrapeCity.ActiveReports.Document.Section.PageOrientation.Landscape;

            report.Run();

            System.IO.MemoryStream memoryFile = new System.IO.MemoryStream();

            pdf.Export(report.Document, memoryFile);

            Response.Clear();
            Response.AppendHeader("content-disposition"
                , "attachment; filename=" + "FeeAccount.xls");
            Response.ContentType = "application/pdf";

            memoryFile.WriteTo(Response.OutputStream);
            Response.End();
        }

        private void ddlProjectsStatus_SelectedIndexChanged(object sender, System.EventArgs e)
        {
            LoadProjects();
        }

        private void ddlBuildingTypes_SelectedIndexChanged(object sender, System.EventArgs e)
        {
            LoadProjects();
        }

		
      
		
		#endregion
	
		#region Bind

        protected string GetImage(object Done)
        {
            if (Done == null || Done == System.DBNull.Value)
                return "~/images/delete.png";
            int nDone = (int)Done;
            if (nDone == 1)
                return "~/images/done.gif";
            return "~/images/delete.png";
        }
        private bool LoadProjects()
        {

            SqlDataReader reader = null;

            try
            {
                ddlProjects.Items.Clear();
                ddlProjects.Items.Add(new ListItem("<" + Resource.ResourceManager["reports_ddlAllProjects"] + ">", "0"));

                reader = ProjectsData.SelectProjectNamesClear(this.ProjectsStatus, int.Parse(ddlBuildingTypes.SelectedValue));

                while (reader.Read())
                {
                    int projectID = reader.GetInt32(0);
                    ddlProjects.Items.Add(new ListItem(reader.GetString(1), projectID.ToString()));

                }
            }
            catch (Exception ex)
            {
                log.Error(ex);
                return false;
            }

            finally
            {
                if (reader != null) reader.Close();
            }


            return true;
        }
		


		#endregion

		
		
		#region Properties

		public ProjectsData.ProjectsByStatus ProjectsStatus
		{
			get
			{
				try
				{
					return  (ProjectsData.ProjectsByStatus)int.Parse(ddlProjectsStatus.SelectedValue);
				}
				catch { return ProjectsData.ProjectsByStatus.AllProjects; }
			}
		}

		#endregion
	}
}
