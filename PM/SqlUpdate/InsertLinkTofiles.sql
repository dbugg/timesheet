USE [asatimesheet]
GO
/****** Object:  StoredProcedure [dbo].[FilesPMInsProc]    Script Date: 7/26/2017 3:54:13 PM ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO



------------------------------------------------------------------------------------------------------------------------
-- Date Created: Sunday, May 22, 2005
-- Created By:   Generated by CodeSmith
------------------------------------------------------------------------------------------------------------------------

ALTER PROCEDURE [dbo].[FilesPMInsProc]
	@FileSubject nvarchar(100),
	@FileMinutes nvarchar(4000),
	@FileVersion int,
	@CreatedBy nvarchar(50),
	@FileType nchar(50),
	@DateCreated datetime,
	@MailForDownload bit, 
	@CategoryID int,
	@FileNameSave nvarchar(100),
	@IsClientVisible bit,
	@IsSubcontractorVisible	bit,
	@IsBuilderVisible bit,
	@IsNadzorVisible bit,
	@IsOtherVisible bit,
	@Link varchar(30),
	@FileID int OUTPUT
AS

INSERT INTO dbo.FilesPM (
	FileSubject,
	FileMinutes,
	FileVersion,
	CreatedBy,
	FileType,
	DateCreated,
	MailForDownload,
	CategoryID,
	FileNameSave,
	IsClientVisible,
	IsSubcontractorVisible,
	IsBuilderVisible,
	IsNadzorVisible,
	IsOtherVisible, 
	Link
) VALUES (
	@FileSubject,
	@FileMinutes,
	@FileVersion,
	@CreatedBy,
	@FileType,
	@DateCreated,
	@MailForDownload,
	@CategoryID,
	@FileNameSave,
	@IsClientVisible,
	@IsSubcontractorVisible,
	@IsBuilderVisible,
	@IsNadzorVisible,
	@IsOtherVisible,
	@Link
)

SET @FileID = @@IDENTITY


