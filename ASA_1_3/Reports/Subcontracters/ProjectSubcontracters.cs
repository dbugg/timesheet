using System;
using System.Data;
using Asa.Timesheet.Data;
using Asa.Timesheet.Data.Reports;
using GrapeCity.ActiveReports;
using GrapeCity.ActiveReports.Controls;
using GrapeCity.ActiveReports.SectionReportModel;
using GrapeCity.ActiveReports.Document.Section;
using GrapeCity.ActiveReports.Document;

namespace Asa.Timesheet.WebPages.Reports
{
    public class ProjectSubcontracters : GrapeCity.ActiveReports.SectionReport
	{
		int _projectID = 0;
		int _pos = 0;
		bool _isProjectReport=false;
		string _userName = String.Empty;
		bool _hpr;
		
		public bool HasData
		{
			get { return ((DataTable)this.DataSource).Rows.Count > 0; }
		}
        public ProjectSubcontracters(int projectID, string userName, bool isProjectReport, bool hpr)
        {
            _projectID = projectID;
            _userName = userName;
            _isProjectReport = isProjectReport;
            _hpr = hpr;
            InitializeComponent();
        }

        private void ProjectSubcontracters_ReportStart(object sender, System.EventArgs eArgs)
        {

            DataSet dsReport = SubcontractersReportData.SelectSubcontractersReportData(_projectID, _hpr);
            this.DataSource = dsReport.Tables[0];

            this.SubReport1.Report = new ProjectSubcontractersSubreport(_isProjectReport, _hpr);
            //    this.SubReport1.Report.DataSource = dsReport.Tables[1];
            if (_isProjectReport)
            {
                TextBox4.Visible = false;
                SubReport1.Top = 0;
                lblObekt.Visible = txtAddress.Visible = txtLeader.Visible = txtLeaderInfo.Visible = txtObekt.Visible = TextBox1.Visible = false;
            }
        }

        private void ProjectSubcontracters_ReportEnd(object sender, System.EventArgs eArgs)
        {
            Logo logo = new Logo();
            logo.Run();

            for (int i = 0; i < this.Document.Pages.Count; i++)
                this.Document.Pages[i].Overlay(logo.Document.Pages[0]);
        }

        private void Detail_Format(object sender, System.EventArgs eArgs)
        {
            System.Collections.ArrayList al = new System.Collections.ArrayList();
            if (this.Fields["Ext"].Value != System.DBNull.Value && (string)this.Fields["Ext"].Value != "")
                al.Add(String.Concat(Resource.ResourceManager["Reports_strVutreshenTel"], ": ", this.Fields["Ext"].Value.ToString()));
            //			if (this.Fields["Mobile"].Value != System.DBNull.Value && (string)this.Fields["Mobile"].Value !="") 
            //				al.Add( String.Concat(Resource.ResourceManager["Reports_strMobilenTel"], ": ", this.Fields["Mobile"].Value.ToString()) );
            //			if (this.Fields["HomePhone"].Value != System.DBNull.Value && (string)this.Fields["HomePhone"].Value !="") 
            //				al.Add( String.Concat(Resource.ResourceManager["Reports_strDomashenTel"], ": ", this.Fields["HomePhone"].Value.ToString()) );
            if (this.Fields["Mail"].Value != System.DBNull.Value && (string)this.Fields["Mail"].Value != "")
                al.Add(String.Concat(Resource.ResourceManager["Reports_strEmail"], ": ", this.Fields["Mail"].Value.ToString()));

            System.Text.StringBuilder sb = new System.Text.StringBuilder();

            if (al.Count > 0)
            {
                for (int i = 0; i < al.Count - 1; i++) sb.Append((string)al[i] + ", ");
                sb.Append((string)al[al.Count - 1]);
            }

            this.txtLeaderInfo.Text = sb.ToString();
        }

        private void ProjectSubcontracters_FetchData(object sender, FetchEventArgs eArgs)
        {
            if (eArgs.EOF) return;

            DataTable dt = (DataTable)this.DataSource;

            DataRow[] dr = dt.Rows[_pos].GetChildRows("Rel1");
            if (dr.Length > 0) this.SubReport1.Report.DataSource = dr;

            _pos++;
        }

		#region ActiveReports Designer generated code














        public void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ProjectSubcontracters));
            this.Detail = new GrapeCity.ActiveReports.SectionReportModel.Detail();
            this.ReportHeader = new GrapeCity.ActiveReports.SectionReportModel.ReportHeader();
            this.ReportFooter = new GrapeCity.ActiveReports.SectionReportModel.ReportFooter();
            this.PageHeader = new GrapeCity.ActiveReports.SectionReportModel.PageHeader();
            this.PageFooter = new GrapeCity.ActiveReports.SectionReportModel.PageFooter();
            this.txtReportTitle = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.lblObekt = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtObekt = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtAddress = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.TextBox1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtLeader = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtLeaderInfo = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.SubReport1 = new GrapeCity.ActiveReports.SectionReportModel.SubReport();
            this.TextBox4 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            ((System.ComponentModel.ISupportInitialize)(this.txtReportTitle)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblObekt)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtObekt)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAddress)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TextBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtLeader)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtLeaderInfo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TextBox4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            // 
            // Detail
            // 
            this.Detail.ColumnSpacing = 0F;
            this.Detail.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
						this.lblObekt,
						this.txtObekt,
						this.txtAddress,
						this.TextBox1,
						this.txtLeader,
						this.txtLeaderInfo,
						this.SubReport1,
						this.TextBox4});
            this.Detail.Height = 2.051389F;
            this.Detail.Name = "Detail";
            // 
            // ReportHeader
            // 
            this.ReportHeader.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
						this.txtReportTitle});
            this.ReportHeader.Height = 0.4159722F;
            this.ReportHeader.Name = "ReportHeader";
            // 
            // ReportFooter
            // 
            this.ReportFooter.Height = 0.25F;
            this.ReportFooter.Name = "ReportFooter";
            // 
            // PageHeader
            // 
            this.PageHeader.Height = 0F;
            this.PageHeader.Name = "PageHeader";
            // 
            // PageFooter
            // 
            this.PageFooter.Height = 0.25F;
            this.PageFooter.Name = "PageFooter";
            // 
            // txtReportTitle
            // 
            this.txtReportTitle.Height = 0.2755906F;
            this.txtReportTitle.Left = 0F;
            this.txtReportTitle.Name = "txtReportTitle";
            this.txtReportTitle.Style = "font-size: 14pt; font-weight: bold; text-align: justify; ddo-char-set: 1";
            this.txtReportTitle.Text = "?????????????? ?? ?????? ????????? ?????????:";
            this.txtReportTitle.Top = 0F;
            this.txtReportTitle.Width = 6.299212F;
            // 
            // lblObekt
            // 
            this.lblObekt.Height = 0.2362205F;
            this.lblObekt.Left = 0F;
            this.lblObekt.Name = "lblObekt";
            this.lblObekt.Style = "font-size: 10pt";
            this.lblObekt.Text = "?????:";
            this.lblObekt.Top = 0F;
            this.lblObekt.Width = 1.181102F;
            // 
            // txtObekt
            // 
            this.txtObekt.DataField = "AdministrativeName";
            this.txtObekt.Height = 0.2362205F;
            this.txtObekt.Left = 1.181102F;
            this.txtObekt.Name = "txtObekt";
            this.txtObekt.Style = "font-size: 11pt; font-weight: bold";
            this.txtObekt.Text = "txtObekt";
            this.txtObekt.Top = 0F;
            this.txtObekt.Width = 5.118111F;
            // 
            // txtAddress
            // 
            this.txtAddress.DataField = "AddInfo";
            this.txtAddress.Height = 0.2F;
            this.txtAddress.Left = 1.181102F;
            this.txtAddress.Name = "txtAddress";
            this.txtAddress.Style = "font-size: 10pt; font-weight: normal";
            this.txtAddress.Text = "txtAddress";
            this.txtAddress.Top = 0.2755906F;
            this.txtAddress.Width = 5.118111F;
            // 
            // TextBox1
            // 
            this.TextBox1.Height = 0.1968504F;
            this.TextBox1.Left = 0.02460628F;
            this.TextBox1.Name = "TextBox1";
            this.TextBox1.Style = "font-size: 10pt";
            this.TextBox1.Text = "???????????:";
            this.TextBox1.Top = 0.551181F;
            this.TextBox1.Width = 1.181102F;
            // 
            // txtLeader
            // 
            this.txtLeader.DataField = "FullName";
            this.txtLeader.Height = 0.1968504F;
            this.txtLeader.Left = 1.1875F;
            this.txtLeader.Name = "txtLeader";
            this.txtLeader.Style = "font-size: 10pt; font-weight: bold";
            this.txtLeader.Text = "TextBox2";
            this.txtLeader.Top = 0.5625F;
            this.txtLeader.Width = 5.118111F;
            // 
            // txtLeaderInfo
            // 
            this.txtLeaderInfo.DataField = "FullName";
            this.txtLeaderInfo.Height = 0.2362205F;
            this.txtLeaderInfo.Left = 1.181102F;
            this.txtLeaderInfo.Name = "txtLeaderInfo";
            this.txtLeaderInfo.Style = "font-size: 10pt; font-weight: normal";
            this.txtLeaderInfo.Text = "TextBox3";
            this.txtLeaderInfo.Top = 0.7480313F;
            this.txtLeaderInfo.Width = 5.118111F;
            // 
            // SubReport1
            // 
            this.SubReport1.CloseBorder = false;
            this.SubReport1.Height = 0.1968504F;
            this.SubReport1.Left = 0F;
            this.SubReport1.Name = "SubReport1";
            this.SubReport1.Report = null;
            this.SubReport1.Top = 1.299212F;
            this.SubReport1.Width = 6.692914F;
            // 
            // TextBox4
            // 
            this.TextBox4.Height = 0.1968504F;
            this.TextBox4.Left = 0F;
            this.TextBox4.Name = "TextBox4";
            this.TextBox4.Style = "font-size: 10pt";
            this.TextBox4.Text = "?????????????? ?? ?????? ????????? ?????????:";
            this.TextBox4.Top = 1.062992F;
            this.TextBox4.Width = 5.25F;
            // 
            // ActiveReport1
            // 
            this.MasterReport = false;
            this.PageSettings.Margins.Left = 1.39375F;
            this.PageSettings.Margins.Right = 0.60625F;
            this.PageSettings.PaperHeight = 11F;
            this.PageSettings.PaperWidth = 8.5F;
            this.PrintWidth = 6.760417F;
            this.Sections.Add(this.ReportHeader);
            this.Sections.Add(this.PageHeader);
            this.Sections.Add(this.Detail);
            this.Sections.Add(this.PageFooter);
            this.Sections.Add(this.ReportFooter);
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule(resources.GetString("$this.StyleSheet"), "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: inherit; font-style: inherit; font-variant: inherit; font-weight: bo" +
                        "ld; font-size: 16pt; font-size-adjust: inherit; font-stretch: inherit", "Heading1", "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: Times New Roman; font-style: italic; font-variant: inherit; font-wei" +
                        "ght: bold; font-size: 14pt; font-size-adjust: inherit; font-stretch: inherit", "Heading2", "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: inherit; font-style: inherit; font-variant: inherit; font-weight: bo" +
                        "ld; font-size: 13pt; font-size-adjust: inherit; font-stretch: inherit", "Heading3", "Normal"));
            ((System.ComponentModel.ISupportInitialize)(this.txtReportTitle)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblObekt)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtObekt)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAddress)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TextBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtLeader)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtLeaderInfo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TextBox4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();

            // Attach Report Events
            this.ReportStart += new System.EventHandler(this.ProjectSubcontracters_ReportStart);
            this.ReportEnd += new System.EventHandler(this.ProjectSubcontracters_ReportEnd);
            this.Detail.Format += new System.EventHandler(this.Detail_Format);
            this.FetchData += new GrapeCity.ActiveReports.SectionReport.FetchEventHandler(this.ProjectSubcontracters_FetchData);
        }

		#endregion

        private ReportHeader ReportHeader;
        private TextBox txtReportTitle;
        private PageHeader PageHeader;
        private Detail Detail;
        private TextBox lblObekt;
        private TextBox txtObekt;
        private TextBox txtAddress;
        private TextBox TextBox1;
        private TextBox txtLeader;
        private TextBox txtLeaderInfo;
        private SubReport SubReport1;
        private TextBox TextBox4;
        private PageFooter PageFooter;
        private ReportFooter ReportFooter;
	}
}
