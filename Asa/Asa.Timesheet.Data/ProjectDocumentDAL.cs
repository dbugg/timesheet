using System;
using System.Data;
using System.Data.SqlClient; 
using System.Collections;
using Microsoft.ApplicationBlocks.Data;

namespace Asa.Timesheet.Data
{
	public class ProjectDocumentDAL
	{
		/// <summary>
		/// Loads a ProjectDocumentData object from the database using the given ProjectDocumentID
		/// </summary>
		/// <param name="projectDocumentID">The primary key value</param>
		/// <returns>A ProjectDocumentData object</returns>
		public static ProjectDocumentData Load(int projectDocumentID)
		{
			SqlParameter[] parameterValues = new SqlParameter[] { new SqlParameter("@ProjectDocumentID", projectDocumentID) };
			using (SqlDataReader reader = SqlHelper.ExecuteReader(CommonDAL.ConnectionString, CommandType.StoredProcedure, "ProjectDocumentsSelProc", parameterValues))
			{
				if (reader.Read())
				{
					ProjectDocumentData result = new ProjectDocumentData();
					LoadFromReader(reader,result);
					return result;
				}
				else
					return null;
			}
		}

		/// <summary>
		/// Loads a ProjectDocumentData object from the database using the given where clause
		/// </summary>
		/// <param name="whereClause">The filter expression for the query</param>
		/// <returns>A ProjectDocumentData object</returns>
		public static ProjectDocumentData Load(string whereClause)
		{
			SqlParameter[] parameterValues = new SqlParameter[] { new SqlParameter("@WhereClause", whereClause) };
			using (SqlDataReader reader = SqlHelper.ExecuteReader(CommonDAL.ConnectionString, CommandType.StoredProcedure, "ProjectDocumentsListProc", parameterValues))
			{
				if (reader.Read())
				{
					ProjectDocumentData result = new ProjectDocumentData();
					LoadFromReader(reader,result);
					return result;
				}
				else
					return null;
			}
		}
		public static int Save( ProjectDocumentData source)
		{
			if (source.ProjectDocumentID == -1)
				return Insert(source);
			else
				return Update(source);	
		}

		public static void Delete( ProjectDocumentData source)
		{
			Delete(source.ProjectDocumentID);
		}
		/// <summary>
		/// Loads a collection of ProjectDocumentData objects from the database.
		/// </summary>
		/// <returns>A collection containing all of the ProjectDocumentData objects in the database.</returns>
		public static ProjectDocumentsVector LoadCollection()
		{
			SqlParameter[] parms = new SqlParameter[] {};
			return LoadCollection("ProjectDocumentsListProc", parms);
		}
	  
		/// <summary>
		/// Saves a collection of ProjectDocumentData objects to the database.
		/// </summary>
		/// <returns>A collection containing all of the ProjectDocumentData objects in the database.</returns>
		public static  void  SaveCollection(ProjectDocumentsVector col,string spName, SqlParameter[] parms)
		{
			ProjectDocumentsVector colOld = LoadCollection(spName,parms);
			UpdateCollection(colOld,col);
		}
		public static  void  SaveCollection(ProjectDocumentsVector col)
		{
			ProjectDocumentsVector colOld = LoadCollection();
			UpdateCollection(colOld,col);
		}
		public static  void  SaveCollection(ProjectDocumentsVector col, string whereClause )
		{
			ProjectDocumentsVector colOld = LoadCollection(whereClause);
			UpdateCollection(colOld,col);
		}
		/// <summary>
		/// Loads a collection of ProjectDocumentData objects from the database.
		/// </summary>
		/// <returns>A collection containing all of the ProjectDocumentData objects in the database.</returns>
		public static ProjectDocumentsVector LoadCollection(string whereClause)
		{
			SqlParameter[] parms = new SqlParameter[] { new SqlParameter("@WhereClause", whereClause) };
			return LoadCollection("ProjectDocumentsListProc", parms);
		}

		/// <summary>
		/// Loads a collection of ProjectDocumentData objects from the database.
		/// </summary>
		/// <returns>A collection containing all of the ProjectDocumentData objects in the database.</returns>
		public static ProjectDocumentsVector LoadCollection(string spName, SqlParameter[] parms)
		{
			ProjectDocumentsVector result = new ProjectDocumentsVector();
			using (SqlDataReader reader = SqlHelper.ExecuteReader(CommonDAL.ConnectionString, CommandType.StoredProcedure, spName, parms))
			{
				while (reader.Read())
				{
					ProjectDocumentData tmp = new ProjectDocumentData();
					LoadFromReader(reader, tmp);
					result.Add(tmp);
				}
			}
			return result;
		}
		/// <summary>
		/// Deletes a ProjectDocumentData object from the database.
		/// </summary>
		/// <param name="projectDocumentID">The primary key value</param>
		public static void Delete(int projectDocumentID)
		{
			SqlParameter[] parameterValues = new SqlParameter[] { new SqlParameter("@ProjectDocumentID", projectDocumentID) };
			SqlHelper.ExecuteNonQuery(CommonDAL.ConnectionString, CommandType.StoredProcedure, "ProjectDocumentsDelProc", parameterValues);
		}

		public static void LoadFromReader(SqlDataReader reader, ProjectDocumentData target)
		{
			if (reader != null && !reader.IsClosed)
			{
				//target.Key = reader.GetInt32(0);
				if (!reader.IsDBNull(0)) target.ProjectDocumentID = reader.GetInt32(0);
				if (!reader.IsDBNull(1)) target.ProjectID = reader.GetInt32(1);
				if (!reader.IsDBNull(2)) target.DocumentType = reader.GetInt32(2);
				if (!reader.IsDBNull(3)) target.DocumentDiscription = reader.GetString(3);
			}
		}
		
	  
		public static int Insert( ProjectDocumentData source)
		{
			SqlParameter[] parameterValues = GetInsertParameterValues(source);
			SqlHelper.ExecuteNonQuery(CommonDAL.ConnectionString, CommandType.StoredProcedure, "ProjectDocumentsInsProc", parameterValues);
			source.ProjectDocumentID = (int) parameterValues[0].Value;
			return source.ProjectDocumentID;
		}

		public static int Update( ProjectDocumentData source)
		{
			SqlParameter[] parameterValues = GetUpdateParameterValues(source);
			SqlHelper.ExecuteNonQuery(CommonDAL.ConnectionString, CommandType.StoredProcedure,
				"ProjectDocumentsUpdProc", parameterValues);
			return source.ProjectDocumentID;
		}
		private static void UpdateCollection(ProjectDocumentsVector colOld,ProjectDocumentsVector col)
		{
			// Delete all old items
			foreach( ProjectDocumentData itemOld in colOld)
			{
				if(!ExistsKey(itemOld.ProjectDocumentID, col))
					Delete(itemOld.ProjectDocumentID);
			}
			foreach( ProjectDocumentData item in col)
			{
				if(item.ProjectDocumentID <= -1)
					Insert(item);
				else
					Update(item);
			}
		}
		private static bool ExistsKey(int Key, ProjectDocumentsVector col)
		{
			foreach( ProjectDocumentData item in col)
				if(item.ProjectDocumentID==Key)
					return true;
			return false;
		}

		private static SqlParameter GetSqlParameter(string name, ParameterDirection direction, object value)
		{
			SqlParameter p = new SqlParameter(name, value);
			p.Direction = direction;
			return p;
		}

		private static SqlParameter[] GetInsertParameterValues( ProjectDocumentData source)
		{
			SqlParameter[] prams = new SqlParameter[4];
			prams[0] = GetSqlParameter("@ProjectDocumentID", ParameterDirection.Output, source.ProjectDocumentID);
			prams[1] = GetSqlParameter("@ProjectID", ParameterDirection.Input, source.ProjectID);
			prams[2] = GetSqlParameter("@DocumentType", ParameterDirection.Input, source.DocumentType);
			prams[3] = GetSqlParameter("@DocumentDiscription", ParameterDirection.Input, source.DocumentDiscription);
			return prams;
		}

		private static SqlParameter[] GetUpdateParameterValues( ProjectDocumentData source)
		{
			return new SqlParameter[] {
										  new SqlParameter("@ProjectDocumentID", source.ProjectDocumentID),
										  new SqlParameter("@ProjectID", source.ProjectID),
										  new SqlParameter("@DocumentType", source.DocumentType),
											new SqlParameter("@@DocumentDiscription", source.DocumentDiscription),
									  };
		}
	}
}   

