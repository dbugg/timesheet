using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Data.SqlClient;
using Asa.Timesheet.Data;
using Asa.Timesheet.Data.Entities;
using log4net;
using Asa.Timesheet.WebPages.UserControls;
using Asa.Timesheet.Data.Util;

namespace Asa.Timesheet.WebPages
{
	/// <summary>
	/// Summary description for EditProjectSubContracter.
	/// </summary>
	public class EditProjectSubContracter : TimesheetPageBase
	{
    #region Web controls

		protected System.Web.UI.WebControls.PlaceHolder menuHolder;
		protected System.Web.UI.WebControls.Label lblName;
		protected System.Web.UI.WebControls.Label lbProject;
		protected System.Web.UI.WebControls.Label lblProjectCode;
		protected System.Web.UI.WebControls.DropDownList ddlTypes;
		protected System.Web.UI.WebControls.Label lblAdministrativeName;
		protected System.Web.UI.WebControls.Label Label1;
		protected System.Web.UI.WebControls.Label lblArea;
		protected System.Web.UI.WebControls.TextBox txtArea;
		protected System.Web.UI.WebControls.Button btnSave;
		protected System.Web.UI.WebControls.Button btnCancel;
		protected System.Web.UI.WebControls.Label lblInfo;
		protected System.Web.UI.WebControls.Label lblError;
		protected System.Web.UI.WebControls.TextBox txtNote;
		protected System.Web.UI.WebControls.Label Label4;
		protected System.Web.UI.WebControls.Label Label6;
		protected System.Web.UI.WebControls.Button btnNew;
		protected System.Web.UI.WebControls.Button btnDel;
		protected System.Web.UI.WebControls.Button btnDelete;
		protected Asa.Timesheet.WebPages.UserControls.PageHeader header;
		protected System.Web.UI.WebControls.Label lblClient;
		protected System.Web.UI.WebControls.DropDownList ddlScheme;
		protected System.Web.UI.WebControls.Label lblAddress;
		protected System.Web.UI.WebControls.TextBox txtRate;
		protected System.Web.UI.WebControls.Button btn;
		protected System.Web.UI.WebControls.Label lblManager;
		protected System.Web.UI.WebControls.Label lbEUR;
		protected System.Web.UI.WebControls.Label Label3;
		protected System.Web.UI.WebControls.Label lbBGN;
		protected System.Web.UI.WebControls.Label Label2;
		protected System.Web.UI.WebControls.Label Label7;
		protected System.Web.UI.WebControls.Label Label8;
		protected System.Web.UI.WebControls.Label Label9;
		protected System.Web.UI.WebControls.Label Label10;
		protected System.Web.UI.WebControls.DataList dlPayments;
		protected System.Web.UI.HtmlControls.HtmlTable tbl;
		protected System.Web.UI.WebControls.Label lblStartDate;
		protected System.Web.UI.WebControls.TextBox txtStartDate;
		protected System.Web.UI.WebControls.Label Label11;
		protected System.Web.UI.WebControls.TextBox txtEndDate;
		protected System.Web.UI.HtmlControls.HtmlImage lkCalendar1;
		protected System.Web.UI.HtmlControls.HtmlImage lkCalendar2;
		protected System.Web.UI.WebControls.DropDownList dllSub;
		protected System.Web.UI.WebControls.DropDownList ddlNames;
		protected System.Web.UI.WebControls.DataGrid grdSub1;
		protected System.Web.UI.WebControls.TextBox txtFoldersGiven;
		protected System.Web.UI.WebControls.TextBox txtFoldersArchive;
		protected System.Web.UI.WebControls.Label Label12;
		protected System.Web.UI.WebControls.DropDownList ddlPhase;
		protected System.Web.UI.WebControls.Button btnNewPayment;
		protected System.Web.UI.WebControls.Button btnNote;
		protected System.Web.UI.WebControls.Label Label5;
    protected System.Web.UI.WebControls.Button btnEdit;
    protected Asa.Timesheet.WebPages.UserControls.EditForm editCtrl;

    #endregion

		private static readonly ILog log = LogManager.GetLogger(typeof(EditProjectSubContracter));
		
    private void Page_Load(object sender, System.EventArgs e)
		{
		if(Page.IsPostBack == false)
		{
			// Set up form for data change checking when
			// first loaded.
			this.CheckForDataChanges= true;
			this.BypassPromptIds =
				new string[] { "btnSave",  "btnEdit", "btnDelete","btnNote","ddlTypes","ddlPhase","btnNewPayment","btnNew","btn"};
		}
			int PID = UIHelpers.GetPIDParam();
			string projectName, projectCode, administrativeName,add;
			decimal area=0;
			int clientID=0;
			bool phases;
			if (!ProjectsData.SelectProject(PID, out projectName, out  projectCode, out administrativeName, out area, out add, 
				out clientID,out phases))
			{
				log.Error("Project not found " + PID);
				
			}
			else
			{
				lbProject.Text=projectCode+ " "+administrativeName;
			}
			int nSID=UIHelpers.GetSIDParam();

			if(!LoggedUser.HasPaymentRights)
				tbl.Visible=false;
			
			if (!LoggedUser.HasRole) ErrorRedirect(Resource.ResourceManager["noRightsAlert"]);
			lkCalendar1.Attributes["onclick"] = TimeHelper.InvokePopupCal(txtStartDate);
			lkCalendar2.Attributes["onclick"] = TimeHelper.InvokePopupCal(txtEndDate);
			UIHelpers.CreateMenu(menuHolder,LoggedUser);
			if(!IsPostBack)
			{
				LoadSubActivities();
				btnNewPayment.Text = Resource.ResourceManager["editsubprojects_btnNewPayment"];
				SubcontracterActivities=new ProjectSubcontracterActivitiesVector();
				ddlScheme.DataSource=SubprojectsUDL.SelectSchemes();
				ddlScheme.DataValueField="PaymentSchemeID";
				ddlScheme.DataTextField="PaymentScheme";
				ddlScheme.DataBind();

				ddlTypes.DataSource=SubprojectsUDL.SelectSubcontracterTypes();
				ddlTypes.DataValueField="SubcontracterTypeID";
				ddlTypes.DataTextField="SubcontracterType";
				ddlTypes.DataBind();
				
				SubprojectsVector sv = SubprojectDAL.LoadCollection("SubprojectsListProc",SQLParms.CreateSubprojectsListProc(PID));

				ddlPhase.DataSource=sv;
				ddlPhase.DataValueField="SubprojectID";
				ddlPhase.DataTextField="SubprojectType";
				ddlPhase.DataBind();
				
				header.PageTitle = string.Concat(Resource.ResourceManager["editprsubcontracters_PageTitle"]," ",projectName);
				header.UserName = LoggedUser.UserName;
				btnNew.Text = Resource.ResourceManager["prsubcontracters_btnNew"];
				if(nSID<=0 && area>0)
					txtArea.Text=area.ToString();

			
				
				if ((!this.LoggedUser.HasPaymentRights) || nSID<=0 ) btnDelete.Visible = false;
				else SetConfirmDelete(btnDelete, Resource.ResourceManager["prsubcontracters_del"]);
			
			
				if(nSID<=0)
				{
					//load subproject technical
					int ID =0;
					SubprojectData sd = sv.GetByTypeID((int)SubprojectTypes.Technical);
					PaymentsVector pv = null;
					if(sd==null)
					{
						lblError.Text=Resource.ResourceManager["prsubcontracters_noPhase"];
						return;
					}
					if(phases)
					{
						
						if(sd!=null)
						
							ID=sd.SubprojectID;
						pv=PaymentDAL.LoadCollection("PaymentsListBySubprojectProc", SQLParms.CreatePaymentsListBySubprojectProc(ID));
					}
					else
					{
						ID=PID;
						pv=PaymentDAL.LoadCollection("PaymentsListByProjectProc", SQLParms.CreatePaymentsListBySubprojectProc(ID));
					}
					foreach(PaymentData pd in pv)
					{
						pd.Amount=0;
						pd.PaidAmount=0;
						pd.PaymentDate=Constants.DateMax;
						pd.Done=false;
						pd.Notes="";
					}
					SessionTable=pv;
					//SessionTable =UIHelpers.GetNewPaymentsTable(0);
					dlPayments.DataSource=SessionTable;
					dlPayments.DataBind();
					SubcontracterActivities=new ProjectSubcontracterActivitiesVector();
					
					if(ddlPhase.Items.FindByValue(((int)SubprojectTypes.Technical).ToString()) !=null)
						ddlPhase.SelectedValue=((int)SubprojectTypes.Technical).ToString();
					ChangeSubact();
				}
				else
				{
					SetFromID(nSID);
				}
				
				ChangePhase();
			}
			//BindGrid();
			Recalculate();
			
      InitEdit();
		}

		private void BindGrid()
		{
			grdSub1.DataSource = SubcontracterActivities;
			grdSub1.DataBind();
			grdSub1.Visible=SubcontracterActivities.Count>0;

		}
		private void SetFromID(int nSID)
		{
			ProjectSubcontracterData sd = ProjectSubcontracterDAL.Load(nSID);
			if(sd!=null)
			{
				txtArea.Text=UIHelpers.FormatDecimal2(sd.Area);
				txtEndDate.Text=TimeHelper.FormatDate(sd.EndDate);
				txtStartDate.Text=TimeHelper.FormatDate(sd.StartDate);
				txtNote.Text=sd.Note;
				ddlScheme.SelectedValue=sd.PaymentSchemeID.ToString();
				txtRate.Text=UIHelpers.FormatDecimal2( sd.Rate);
				ListItem li= ddlTypes.Items.FindByText(sd.SubcontracterType.ToString());
				ddlTypes.SelectedIndex=ddlTypes.Items.IndexOf(li);
				ChangeSubact();

				ProjectSubcontracterActivitiesVector psav = ProjectSubcontracterActivitieDAL.LoadCollection("ProjectSubcontracterActivitiesListProc", SQLParms.CreateProjectSubcontracterActivitiesListProc(nSID));
				foreach( ProjectSubcontracterActivitieData d in psav)
				{
					if(d.Done)

					{
						ProjectSubcontracterActivitieData dd= SubcontracterActivities.GetBySubID(d.SubactivityID);
						if(dd!=null)
							dd.Done=true;
					}
				}

				if(ddlNames.Items.FindByValue(sd.SubcontracterID.ToString())!=null)
					ddlNames.SelectedValue=sd.SubcontracterID.ToString();
				if(sd.FoldersGiven>0)
					txtFoldersGiven.Text =sd.FoldersGiven.ToString();
				if(sd.FordersArchive>0)
					txtFoldersArchive.Text=sd.FordersArchive.ToString();
				ddlPhase.SelectedValue=sd.SubprojectID.ToString();
				PaymentsVector pv = PaymentDAL.LoadCollection("PaymentsListBySubcontracterProc", SQLParms.CreatePaymentsListBySubcontracterProc(nSID));
				SessionTable=pv;
				dlPayments.DataSource=pv;
				dlPayments.DataBind();

				
				//SubcontracterActivities=psav;
				BindGrid();
			}
		}
		

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
      this.ddlPhase.SelectedIndexChanged += new System.EventHandler(this.ddlPhase_SelectedIndexChanged);
      this.ddlTypes.SelectedIndexChanged += new System.EventHandler(this.ddlTypes_SelectedIndexChanged);
      this.grdSub1.ItemCommand += new System.Web.UI.WebControls.DataGridCommandEventHandler(this.grdSub1_ItemCommand);
      this.grdSub1.DeleteCommand += new System.Web.UI.WebControls.DataGridCommandEventHandler(this.grdSub1_DeleteCommand);
      this.btnNew.Click += new System.EventHandler(this.btnNew_Click);
      this.btn.Click += new System.EventHandler(this.btn_Click);
      this.dlPayments.ItemDataBound += new System.Web.UI.WebControls.DataListItemEventHandler(this.dlPayments_ItemDataBound);
      this.btnNewPayment.Click += new System.EventHandler(this.btnNewPayment_Click);
      this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
      this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
      this.btnNote.Click += new System.EventHandler(this.btnNote_Click);
      this.btnDelete.Click += new System.EventHandler(this.btnDelete_Click);
      this.Load += new System.EventHandler(this.Page_Load);

    }
		#endregion
		

		private void btnSave_Click(object sender, System.EventArgs e)
		{
			if (ddlPhase.Items.Count==0 || ddlPhase.SelectedValue==null || ddlPhase.SelectedValue=="") 
			{
				AlertFieldNotEntered(Label12);
				Dirty=true;
				return;
			}
			 SetRec();
			int ID = UIHelpers.GetSIDParam();
			int PID = UIHelpers.GetPIDParam();
			ProjectSubcontracterData sd = new ProjectSubcontracterData();
			sd.Area= UIHelpers.ParseDecimal(txtArea.Text);
			sd.EndDate= TimeHelper.GetDate(txtEndDate.Text);
			sd.StartDate=TimeHelper.GetDate(txtStartDate.Text);
			sd.Note= txtNote.Text;
			sd.PaymentSchemeID= int.Parse(ddlScheme.SelectedValue);
			sd.ProjectID= PID;
				
		
			if (ddlNames.SelectedValue==null || ddlNames.SelectedValue=="") 
			{
				AlertFieldNotEntered(lblAdministrativeName);
				Dirty=true;
				return;
			}
			decimal totall=0;
			decimal dEUR=0;
			for(int i=0;i<dlPayments.Items.Count;i++)
			{
				decimal per=0;
				decimal am=0;

				TextBox txtPer=(TextBox) dlPayments.Items[i].FindControl("txtPer");
				if(txtPer!=null && txtPer.Text!="")
				{
					per = UIHelpers.ParseDecimal(txtPer.Text);
					totall+=per;
				}
				TextBox txtAmount=(TextBox) dlPayments.Items[i].FindControl("txtAmount");
				if(txtAmount!=null && txtAmount.Text!="")
				{
					am = UIHelpers.ParseDecimal(txtAmount.Text);
					dEUR+=am;
				
				}
				
			}

			if(txtRate.Text!="")
				sd.Rate=UIHelpers.ParseDecimal(txtRate.Text);
			sd.ProjectSubcontracterID=ID;
			sd.SubcontracterID=int.Parse(ddlNames.SelectedValue);
			PaymentSchemes ps =(PaymentSchemes)sd.PaymentSchemeID;
			if(ps==PaymentSchemes.Fixed)
				sd.TotalAmount=sd.Rate;
			else if(ps==PaymentSchemes.Area)
				sd.TotalAmount=sd.Rate*(decimal)sd.Area;
			if( LoggedUser.HasPaymentRights)
			{
				if(dEUR!=sd.TotalAmount)
				{
					lblError.Text=Resource.ResourceManager["editsubprojects_errorTotal"];
					Dirty=true;
					return;
				}
			}
			if(txtFoldersGiven.Text!="")
				sd.FoldersGiven=int.Parse(txtFoldersGiven.Text);
			if(txtFoldersArchive.Text!="")
				sd.FordersArchive=int.Parse(txtFoldersArchive.Text);
			sd.SubprojectID=int.Parse(ddlPhase.SelectedValue);
			ID=ProjectSubcontracterDAL.Save(sd);
//			int total=0;
//			decimal dEUR=0;
			Dirty=false;
			bool done=true;
			ProjectSubcontracterActivitiesVector psa = new ProjectSubcontracterActivitiesVector();
			for(int i=0;i<grdSub1.Items.Count;i++)
			{
				CheckBox ckDone=(CheckBox) grdSub1.Items[i].FindControl("ckDone");
				int SSID=int.Parse(grdSub1.Items[i].Cells[0].Text);
				if(!ckDone.Checked)
					done=false;
				

				ProjectSubcontracterActivitieData psad = new ProjectSubcontracterActivitieData(-1,ID,SSID,ckDone.Checked);
				psa.Add(psad);
			}
			if(sd.Done!=done)
			{
				sd.Done=done;
				ProjectSubcontracterDAL.Save(sd);
			}

			SubprojectsUDL.ExecuteProjectSubactivitiesDelBySubcontracterProc(ID);
			foreach(ProjectSubcontracterActivitieData psad in psa)
				ProjectSubcontracterActivitieDAL.Save(psad);
			if( LoggedUser.HasPaymentRights)
			{
				PaymentsVector pv = new PaymentsVector();
				for(int i=0;i<dlPayments.Items.Count;i++)
				{
					decimal per=0;
					decimal am=0;

					TextBox txtPer=(TextBox) dlPayments.Items[i].FindControl("txtPer");
					if(txtPer!=null && txtPer.Text!="")
					{
						per =UIHelpers.ParseDecimal(txtPer.Text);
						//totall+=per;
					}
					TextBox txtAmount=(TextBox) dlPayments.Items[i].FindControl("txtAmount");
					if(txtAmount!=null && txtAmount.Text!="")
					{
						am = UIHelpers.ParseDecimal(txtAmount.Text);
						//dEUR+=am;
						if(am>0)
							per=am*100/dEUR;
					}
					CheckBox ckPaid=(CheckBox) dlPayments.Items[i].FindControl("ckPaid");
					TextBox txt=(TextBox) dlPayments.Items[i].FindControl("txtDate");
					TextBox txtNotes=(TextBox) dlPayments.Items[i].FindControl("txtNotes");
					if(per>0)
					{
						PaymentData pd= new PaymentData(-1, -1,ID,per,am,ckPaid.Checked,TimeHelper.GetDate(txt.Text),am,false);
						pd.Notes=txtNotes.Text;
						pv.Add(pd);
					}
				}
				SubprojectsUDL.ExecutePaymentsDelBySubcontracterProc(ID);
				foreach(PaymentData pd in pv)
					PaymentDAL.Save(pd);
			}
//			if(total!=100 || dEUR!=sd.TotalAmount)
//			{
//				lblError.Text=Resource.ResourceManager["editsubprojects_errorTotal"];
//				return;
//			}
		
			Response.Redirect("ProjectSubcontracters.aspx?pid="+UIHelpers.GetPIDParam());
		}

		private void btnCancel_Click(object sender, System.EventArgs e)
		{
			Response.Redirect("ProjectSubcontracters.aspx?pid="+UIHelpers.GetPIDParam());
		}

		private void btnDelete_Click(object sender, System.EventArgs e)
		{
			ProjectSubcontracterDAL.Delete(UIHelpers.GetSIDParam());
			Response.Redirect("ProjectSubcontracters.aspx?pid="+UIHelpers.GetPIDParam());
		}
		private void ChangeSubact()
		{
			 LoadSubActivities();
			int ID=UIHelpers.ToInt(ddlTypes.SelectedValue);
			ddlNames.DataSource=SubprojectsUDL.SelectSubcontracters(ID,1,LoggedUser.HasPaymentRights);
			ddlNames.DataValueField="SubcontracterID";
			ddlNames.DataTextField="SubcontracterName";
			ddlNames.DataBind();
			BindGrid();
		}
		private void LoadSubActivities()
		{
			
			

				int ID=UIHelpers.ToInt(ddlTypes.SelectedValue);
			SubcontracterActivities.Clear();
			if(ID>0)
			{
				SqlDataReader reader = null;
				
				try
				{
					reader = SubprojectsUDL.SelectSubactivities(ID);
					while(reader.Read())
					{
						int SID = (int)reader["SubactivityID"];
						ProjectSubcontracterActivitieData psa = new ProjectSubcontracterActivitieData(-1,-1,SID,false);
						psa.Subactivity= (string)reader["Subactivity"];
						SubcontracterActivities.Add(psa);
					}
				}
				catch (Exception ex)
				{
					log.Info(ex);
					lblError.Text = Resource.ResourceManager["projects_ErrorLoadProjects"];
					return;
				}
				finally
				{
					if (reader!=null) reader.Close();
				}
			
			}
			
			BindGrid();
		}
		private void ddlTypes_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			ChangeSubact();
		}

		private void btnNew_Click(object sender, System.EventArgs e)
		{
			if(dllSub.SelectedIndex>=0)
			{
				for(int i=0;i<grdSub1.Items.Count;i++)
				{
					CheckBox ckDone=(CheckBox) grdSub1.Items[i].FindControl("ckDone");
										SubcontracterActivities[i].Done=ckDone.Checked;
				}

				ProjectSubcontracterActivitieData psa = new ProjectSubcontracterActivitieData(-1,-1,int.Parse(dllSub.SelectedValue),false);
				psa.Subactivity= dllSub.SelectedItem.Text;
				SubcontracterActivities.Add(psa);
				BindGrid();
			}
		}

		private void grdSub1_ItemCommand(object source, System.Web.UI.WebControls.DataGridCommandEventArgs e)
		{
//			ImageButton ib = (ImageButton)e.CommandSource;
//			string s = ib.ID;
//			switch (ib.ID)
//			{
//				case "btnDel": 
//					SubcontracterActivities.RemoveAt(e.Item.ItemIndex);
//					
//					BindGrid();
//					break;
//			}
		}

		private void grdSub1_DeleteCommand(object source, System.Web.UI.WebControls.DataGridCommandEventArgs e)
		{
			SubcontracterActivities.RemoveAt(e.Item.ItemIndex);
					
			BindGrid();
		}

		

		private void dlPayments_ItemDataBound(object sender, System.Web.UI.WebControls.DataListItemEventArgs e)
		{
			HtmlImage hi=(HtmlImage) e.Item.FindControl("lkCal");
			TextBox txt=(TextBox) e.Item.FindControl("txtDate");
			if(txt!=null&& hi!=null)
				hi.Attributes["onclick"] = TimeHelper.InvokePopupCal(txt);
		}

		private decimal Recalculate()
		{
			decimal rate=0;
			if(txtRate.Text!="")
				rate=UIHelpers.ParseDecimal(txtRate.Text);
			decimal EUR=0;
			PaymentSchemes ps =(PaymentSchemes) int.Parse(ddlScheme.SelectedValue);
			if(ps==PaymentSchemes.Fixed)
				EUR=rate;
			else if(ps==PaymentSchemes.Area)
				EUR=rate*UIHelpers.ParseDecimal(txtArea.Text);
			decimal fix=decimal.Parse( System.Configuration.ConfigurationSettings.AppSettings["fixedBGNEUR"]);
			lbEUR.Text=UIHelpers.FormatDecimal2(EUR);
			lbBGN.Text=UIHelpers.FormatDecimal2(EUR*fix);
			return EUR;
		}
		private void SetRec()
		{
			decimal EUR=Recalculate();
			decimal dTotal=0;
			for(int i=0;i<dlPayments.Items.Count;i++)
			{
				decimal per=0;
				

				TextBox txtPer=(TextBox) dlPayments.Items[i].FindControl("txtPer");
				if(txtPer!=null && txtPer.Text!="")
				{
					per = UIHelpers.ParseDecimal(txtPer.Text);
					
				}
				
				TextBox txtAmount=(TextBox) dlPayments.Items[i].FindControl("txtAmount");
				CheckBox ckPaid=(CheckBox) dlPayments.Items[i].FindControl("ckPaid");
				if(txtAmount!=null )
				{

					if(txtAmount.Text=="")
					{
						if(i==dlPayments.Items.Count-1 && txtPer.Text=="")
						{
							if(EUR>0)
							{
								if(!ckPaid.Checked)
									txtAmount.Text=UIHelpers.FormatDecimal2(EUR-dTotal);
								decimal d = UIHelpers.ParseDecimal(txtAmount.Text);
								per= d/EUR;
								txtPer.Text=UIHelpers.FormatDecimal4(per*100);
							}
						}
						else
						{
							if(!ckPaid.Checked)
								txtAmount.Text=UIHelpers.FormatDecimal2((decimal)per * EUR/100);
						}
					}
					else if(txtPer.Text=="")
					{
						decimal d = UIHelpers.ParseDecimal(txtAmount.Text);
						per= d/EUR;
						txtPer.Text=UIHelpers.FormatDecimal4(per*100);
					}
					else 
					{
						if(!ckPaid.Checked)
							txtAmount.Text=UIHelpers.FormatDecimal2((decimal)per * EUR/100);
					}
					dTotal+=UIHelpers.ParseDecimal(txtAmount.Text);
		
				}

				
			}
		}
		private void btn_Click(object sender, System.EventArgs e)
		{
			
			 SetRec();
		}
		private void ChangePhase()
		{
			if(ddlPhase.Items.Count>0)
			{
				int nPhase = int.Parse(ddlPhase.SelectedValue);
				SubprojectData sd = SubprojectDAL.Load(nPhase);
				if(sd!=null)
				{
					if(UIHelpers.GetSIDParam()<=0)
					{
						txtStartDate.Text=TimeHelper.FormatDate(sd.StartDate);
						txtEndDate.Text=TimeHelper.FormatDate(sd.EndDate);
						txtArea.Text=sd.Area.ToString();
					}
				}
			}
		}
		private void ddlPhase_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			ChangePhase();
		}

		private void btnNewPayment_Click(object sender, System.EventArgs e)
		{
			PaymentData pd = new PaymentData(-1,-1,-1,0,0,false,new DateTime(),0,false);
			SessionTable.Add(pd);
			dlPayments.DataSource=SessionTable;
			dlPayments.DataBind();
			
		}

		private void btnNote_Click(object sender, System.EventArgs e)
		{
			decimal area=0;
			int clientID=0;
			bool phases;
			string projectName, projectCode, administrativeName,add;
			
      ProjectsData.SelectProject(UIHelpers.GetPIDParam(), out projectName, out  projectCode, out administrativeName, out area, out add, 
				out clientID,out phases);
			
      string sManager="";
			if(ddlNames.SelectedIndex>=0)
			{
				int ID = int.Parse(ddlNames.SelectedValue);
				SubcontracterData sd = SubcontracterDAL.Load(ID);
				if(sd!=null)
					sManager=sd.Manager;	
			}
			

			UIHelpers.Protokol(false, this.Page,lbProject.Text,ddlNames.SelectedItem.Text,sManager, UIHelpers.FormatDecimal(area),add,txtFoldersGiven.Text);

		}

    #region UI

    private void InitEdit()
    {
      int nSID = UIHelpers.GetSIDParam();
      if (nSID <=0) return;

      string cont = "tblForm;tbl;tblFolders";
      string vmButtons = this.btnEdit.ClientID;
      string emButtons = this.btnSave.ClientID+";"+this.btnDelete.ClientID;

      string exl = String.Empty;

      editCtrl.InitEdit(cont, vmButtons, emButtons, exl);
    }

    #endregion


	}
}
