using System;
using Asa.Timesheet.Data;
using Asa.Timesheet.Data.Util;
using System.Web;
using GrapeCity.ActiveReports;
using GrapeCity.ActiveReports.Controls;
using GrapeCity.ActiveReports.SectionReportModel;
using GrapeCity.ActiveReports.Document.Section;
using GrapeCity.ActiveReports.Document;

namespace Asa.Timesheet.WebPages.Reports
{
    public class MinutesReport : GrapeCity.ActiveReports.SectionReport
	{
		private string _projectName = String.Empty;
		string _projectCode = String.Empty;
		string _administrativeName = String.Empty;
		string _clientCity = String.Empty;
		string _clientAddress = String.Empty;
		string _clientName = String.Empty;
		string _createdBy = String.Empty;

		bool _includeTimesColumn = true;
        private Picture picture1;
        private Picture picture2;
		bool _forPDF;

        public MinutesReport(Minutes.HeaderInfo headerInfo, string createdBy, bool includeTimesColumn, bool forPDF)
        {
            InitializeComponent();

            #region Init variables

            _projectName = headerInfo.ProjectName;
            _projectCode = headerInfo.ProjectCode;
            _administrativeName = headerInfo.ProjectAdminName;
            _clientName = headerInfo.ClientName;
            _clientCity = headerInfo.ClientAddress;
            _clientAddress = headerInfo.ClientCity;
            _createdBy = createdBy;

            _includeTimesColumn = includeTimesColumn;
            _forPDF = forPDF;

            #endregion

            #region Page settings

            this.PageSettings.Margins.Top = SectionReport.CmToInch(1f);
            this.PageSettings.Margins.Bottom = SectionReport.CmToInch(1f);
            this.PageSettings.Gutter = 0F;
            this.PageSettings.Margins.Left = SectionReport.CmToInch(1f);
            this.PageSettings.Margins.Right = SectionReport.CmToInch(1f);


            if (_forPDF)
            {
                this.PrintWidth = SectionReport.CmToInch(18f);
                this.PageSettings.Margins.Left = SectionReport.CmToInch(1.5f);
            }
            else
            {
                this.PrintWidth = SectionReport.CmToInch(17f);
                this.PageFooter.Visible = false;
                this.txtContacts1.Visible = true;
            }
            #endregion

            this.SubReport1.Report = new MinutesHeader(headerInfo, forPDF);

            this.txtCreatedBy.Text = String.Concat("                                                 ", _createdBy);
            this.lblReportDate.Text = String.Concat(DateTime.Now.ToString("dd MMM yyyy "), Resource.ResourceManager["reports_YearAbbr"]);
        }

        private void MinutesReport_FetchData(object sender, FetchEventArgs eArgs)
        {
            int stMinutes = TimeHelper.TimeInMinutes(this.Fields["StartHour"].Value.ToString());
            int etMinutes = TimeHelper.TimeInMinutes(this.Fields["EndHour"].Value.ToString());
            if ((stMinutes < 0) || (etMinutes < 0))
            {
                stMinutes = 0; etMinutes = 0;
            }

            this.Fields["Time"].Value = etMinutes - stMinutes;
        }

		#region Activity

        private void ghActivity_Format(object sender, System.EventArgs eArgs)
        {
            if ((int)this.Fields["ActivityID"].Value == 0)
            {
                this.ghActivity.Visible = false;
                this.gfActivity.Visible = false;
            }
            else this.ghActivity.Visible = true;
        }

        private void gfActivity_Format(object sender, System.EventArgs eArgs)
        {
            this.txtActivityTotal.Text = Resource.ResourceManager["reports_Total"]; //Resource.ResourceManager["rpt_TotalFor"] + " " + this.txtActivityName.Text;

            if (!_includeTimesColumn)
            {
                this.txtActivityTotalHours.Text =
                                TimeHelper.HoursStringFromMinutes(int.Parse(txtActivityTotalHours.Text), false);
            }
            else this.txtActivityTotalHours.Value = TimeHelper.HoursFromMinutes(int.Parse(txtActivityTotalHours.Text)).ToString("0.00"); ;
        }

		#endregion


        private void gfUser_Format(object sender, System.EventArgs eArgs)
        {
            this.txtUserTotalLbl.Text = Resource.ResourceManager["reports_Total"];

            if (!_includeTimesColumn)
            {
                this.txtUserTotalHours.Text = TimeHelper.HoursStringFromMinutes(int.Parse(txtUserTotalHours.Text), false);
            }
            else this.txtUserTotalHours.Text = TimeHelper.HoursFromMinutes(int.Parse(txtUserTotalHours.Text)).ToString("0.00");
        }

		#region Page header/footer

        private void PageHeader_Format(object sender, System.EventArgs eArgs)
        {
            if (this.PageNumber == 1)
            {
                //this.picPageHeader.Visible = false;
                this.tbSlujitel.Location = new System.Drawing.PointF(this.tbSlujitel.Location.X, SectionReport.CmToInch(0.5f));
                this.tbData.Location = new System.Drawing.PointF(this.tbData.Location.X, SectionReport.CmToInch(0.5f));
                this.tbNachChas.Location = new System.Drawing.PointF(this.tbNachChas.Location.X, SectionReport.CmToInch(0.5f));
                this.tbKrChas.Location = new System.Drawing.PointF(this.tbKrChas.Location.X, SectionReport.CmToInch(0.5f));
                this.tbProtokol.Location = new System.Drawing.PointF(this.tbProtokol.Location.X, SectionReport.CmToInch(0.5f));
                this.tbAdmin.Location = new System.Drawing.PointF(this.tbAdmin.Location.X, SectionReport.CmToInch(0.5f));
                this.Line2.Location = new System.Drawing.PointF(this.Line2.Location.X, SectionReport.CmToInch(1f));
                this.Line12.Location = new System.Drawing.PointF(this.Line12.Location.X, SectionReport.CmToInch(0.5f));
            }
            else
            {
                //this.picPageHeader.Visible = true;
            }

            if ((!_forPDF) && (this.PageNumber > 1)) this.PageHeader.Visible = false;
        }

        private void PageFooter_Format(object sender, System.EventArgs eArgs)
        {
            this.lblPage.Text = Resource.ResourceManager["rpt_Page"] + this.PageNumber.ToString();
        }
		#endregion

        private void GrandTotalFooter_Format(object sender, System.EventArgs eArgs)
        {
            txtProjectTotal.Text = Resource.ResourceManager["reports_Total"];
            if (_includeTimesColumn)
            {
                txtProjectTotalMinutes.Text = TimeHelper.HoursFromMinutes(int.Parse(txtProjectTotalMinutes.Text)).ToString("0.00");
            }
            else txtProjectTotalMinutes.Text = TimeHelper.HoursStringFromMinutes(int.Parse(txtProjectTotalMinutes.Text), false);
        }

        private void MinutesReport_ReportStart(object sender, System.EventArgs eArgs)
        {
            if ((this.DataSource == null) || (((System.Data.DataTable)this.DataSource).Rows.Count == 0))
            {
                this.FetchData -= new GrapeCity.ActiveReports.SectionReport.FetchEventHandler(this.MinutesReport_FetchData);
                this.ghActivity.Format -= new System.EventHandler(this.ghActivity_Format);
                this.gfActivity.Format -= new System.EventHandler(this.gfActivity_Format);
                this.gfUser.Format -= new System.EventHandler(this.gfUser_Format);
                this.GrandTotalFooter.Format -= new System.EventHandler(this.GrandTotalFooter_Format);

                //this.ghActivity.Visible = false;
                this.txtActivityName.Visible = false;
                this.Line5.Visible = false;
                this.gfActivity.Visible = false;
                this.ghUser.Visible = false;
                this.gfUser.Visible = false;
                this.GrandTotalFooter.Visible = false;
                this.PageHeader.Visible = false;
                this.Detail.Visible = false;


                this.txtNoData.Visible = true;
                this.txtNoData.Text = Resource.ResourceManager["reports_WorkTimes_NoDataMatch"];

            }
        }

		#region ActiveReports Designer generated code


















































        public void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MinutesReport));
            this.Detail = new GrapeCity.ActiveReports.SectionReportModel.Detail();
            this.txtUserName = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtStartHour = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtEndHour = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtMinutes = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtAdminMinutes = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtDetailWorkDate = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.ReportHeader = new GrapeCity.ActiveReports.SectionReportModel.ReportHeader();
            this.SubReport1 = new GrapeCity.ActiveReports.SectionReportModel.SubReport();
            this.ReportFooter = new GrapeCity.ActiveReports.SectionReportModel.ReportFooter();
            this.txtContacts1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.lblReportDate = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lblProektant = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.txtCreatedBy = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.PageHeader = new GrapeCity.ActiveReports.SectionReportModel.PageHeader();
            this.Line2 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.tbSlujitel = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.tbData = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.tbNachChas = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.tbKrChas = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.tbAdmin = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.tbProtokol = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.Line12 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.PageFooter = new GrapeCity.ActiveReports.SectionReportModel.PageFooter();
            this.Line11 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.lblPage = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.txtContacts = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.GrandTotalHeader = new GrapeCity.ActiveReports.SectionReportModel.GroupHeader();
            this.GrandTotalFooter = new GrapeCity.ActiveReports.SectionReportModel.GroupFooter();
            this.Line9 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.Line8 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.txtProjectTotal = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtProjectTotalMinutes = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.ghActivity = new GrapeCity.ActiveReports.SectionReportModel.GroupHeader();
            this.txtActivityName = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.Line6 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.Line5 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.txtNoData = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.gfActivity = new GrapeCity.ActiveReports.SectionReportModel.GroupFooter();
            this.txtActivityTotal = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.Line7 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.txtActivityTotalHours = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.ghUser = new GrapeCity.ActiveReports.SectionReportModel.GroupHeader();
            this.gfUser = new GrapeCity.ActiveReports.SectionReportModel.GroupFooter();
            this.txtUserTotalLbl = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.Line3 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.Line4 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.txtUserTotalHours = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.picture1 = new GrapeCity.ActiveReports.SectionReportModel.Picture();
            this.picture2 = new GrapeCity.ActiveReports.SectionReportModel.Picture();
            ((System.ComponentModel.ISupportInitialize)(this.txtUserName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtStartHour)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtEndHour)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtMinutes)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAdminMinutes)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDetailWorkDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtContacts1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblReportDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblProektant)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCreatedBy)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbSlujitel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbData)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbNachChas)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbKrChas)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbAdmin)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbProtokol)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblPage)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtContacts)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtProjectTotal)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtProjectTotalMinutes)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtActivityName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtNoData)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtActivityTotal)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtActivityTotalHours)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtUserTotalLbl)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtUserTotalHours)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picture1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picture2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            // 
            // Detail
            // 
            this.Detail.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
            this.txtUserName,
            this.txtStartHour,
            this.txtEndHour,
            this.txtMinutes,
            this.txtAdminMinutes,
            this.txtDetailWorkDate});
            this.Detail.Height = 0.2F;
            this.Detail.Name = "Detail";
            // 
            // txtUserName
            // 
            this.txtUserName.DataField = "UserName";
            this.txtUserName.Height = 0.1968504F;
            this.txtUserName.Left = 0F;
            this.txtUserName.Name = "txtUserName";
            this.txtUserName.Style = "color: DimGray; font-size: 9pt; vertical-align: middle";
            this.txtUserName.Text = "tbUserName";
            this.txtUserName.Top = 0F;
            this.txtUserName.Width = 1.811024F;
            // 
            // txtStartHour
            // 
            this.txtStartHour.DataField = "StartHour";
            this.txtStartHour.Height = 0.1968504F;
            this.txtStartHour.Left = 2.598425F;
            this.txtStartHour.MultiLine = false;
            this.txtStartHour.Name = "txtStartHour";
            this.txtStartHour.Style = "color: DimGray; font-size: 9pt; text-align: center; vertical-align: middle; white" +
    "-space: nowrap";
            this.txtStartHour.Text = "StartHour";
            this.txtStartHour.Top = 0F;
            this.txtStartHour.Width = 0.472441F;
            // 
            // txtEndHour
            // 
            this.txtEndHour.DataField = "EndHour";
            this.txtEndHour.Height = 0.1968504F;
            this.txtEndHour.Left = 3.070866F;
            this.txtEndHour.MultiLine = false;
            this.txtEndHour.Name = "txtEndHour";
            this.txtEndHour.Style = "color: DimGray; font-size: 9pt; text-align: center; vertical-align: middle; white" +
    "-space: inherit";
            this.txtEndHour.Text = "EndHour";
            this.txtEndHour.Top = 0F;
            this.txtEndHour.Width = 0.472441F;
            // 
            // txtMinutes
            // 
            this.txtMinutes.DataField = "Minutes";
            this.txtMinutes.Height = 0.1968504F;
            this.txtMinutes.Left = 3.543307F;
            this.txtMinutes.Name = "txtMinutes";
            this.txtMinutes.Style = "color: DimGray; font-size: 9pt; vertical-align: middle";
            this.txtMinutes.Text = "Minutes";
            this.txtMinutes.Top = 0F;
            this.txtMinutes.Width = 1.279528F;
            // 
            // txtAdminMinutes
            // 
            this.txtAdminMinutes.DataField = "AdminMinutes";
            this.txtAdminMinutes.Height = 0.1968504F;
            this.txtAdminMinutes.Left = 4.822834F;
            this.txtAdminMinutes.Name = "txtAdminMinutes";
            this.txtAdminMinutes.Style = "color: DimGray; font-size: 9pt; vertical-align: middle";
            this.txtAdminMinutes.Text = "AdminMinutes";
            this.txtAdminMinutes.Top = 0F;
            this.txtAdminMinutes.Width = 1.279528F;
            // 
            // txtDetailWorkDate
            // 
            this.txtDetailWorkDate.DataField = "WorkDate";
            this.txtDetailWorkDate.Height = 0.1968504F;
            this.txtDetailWorkDate.Left = 1.811024F;
            this.txtDetailWorkDate.Name = "txtDetailWorkDate";
            this.txtDetailWorkDate.OutputFormat = resources.GetString("txtDetailWorkDate.OutputFormat");
            this.txtDetailWorkDate.Style = "color: DimGray; font-size: 9pt; text-align: center; vertical-align: middle";
            this.txtDetailWorkDate.Text = "WorkDate";
            this.txtDetailWorkDate.Top = 0F;
            this.txtDetailWorkDate.Width = 0.7874014F;
            // 
            // ReportHeader
            // 
            this.ReportHeader.CanShrink = true;
            this.ReportHeader.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
            this.SubReport1,
            this.picture1});
            this.ReportHeader.Height = 1.114583F;
            this.ReportHeader.Name = "ReportHeader";
            // 
            // SubReport1
            // 
            this.SubReport1.CloseBorder = false;
            this.SubReport1.Height = 0.1968504F;
            this.SubReport1.Left = 0F;
            this.SubReport1.Name = "SubReport1";
            this.SubReport1.Report = null;
            this.SubReport1.Top = 0.8661417F;
            this.SubReport1.Width = 7.086611F;
            // 
            // ReportFooter
            // 
            this.ReportFooter.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
            this.txtContacts1,
            this.lblReportDate,
            this.lblProektant,
            this.txtCreatedBy});
            this.ReportFooter.Height = 1.379861F;
            this.ReportFooter.KeepTogether = true;
            this.ReportFooter.Name = "ReportFooter";
            // 
            // txtContacts1
            // 
            this.txtContacts1.Height = 0.2F;
            this.txtContacts1.Left = 0F;
            this.txtContacts1.Name = "txtContacts1";
            this.txtContacts1.Style = "font-weight: bold; text-align: center";
            this.txtContacts1.Text = "Sofia 1700, 4A, Simeonovsko shose Blvd., tel.: +3592 8621978, +3592 8623348, fax." +
    ":+3592 8621614, e-mail: office@asa-bg.com, website: www.asa-bg.com";
            this.txtContacts1.Top = 0.3937007F;
            this.txtContacts1.Visible = false;
            this.txtContacts1.Width = 7.086611F;
            // 
            // lblReportDate
            // 
            this.lblReportDate.Height = 0.2F;
            this.lblReportDate.HyperLink = null;
            this.lblReportDate.Left = 0F;
            this.lblReportDate.Name = "lblReportDate";
            this.lblReportDate.Style = "font-size: 9pt; font-weight: normal; vertical-align: bottom";
            this.lblReportDate.Text = "lblReportDate";
            this.lblReportDate.Top = 0.7874014F;
            this.lblReportDate.Width = 7.086611F;
            // 
            // lblProektant
            // 
            this.lblProektant.Height = 0.2F;
            this.lblProektant.HyperLink = null;
            this.lblProektant.Left = 0F;
            this.lblProektant.Name = "lblProektant";
            this.lblProektant.Style = "font-size: 9pt; font-weight: normal; vertical-align: bottom";
            this.lblProektant.Text = "???????? ?????????:. . . . . . . . . . . . . . . . . . . . . . .";
            this.lblProektant.Top = 0.9842521F;
            this.lblProektant.Width = 7.086611F;
            // 
            // txtCreatedBy
            // 
            this.txtCreatedBy.Height = 0.2F;
            this.txtCreatedBy.Left = 0F;
            this.txtCreatedBy.Name = "txtCreatedBy";
            this.txtCreatedBy.Style = "font-size: 9pt";
            this.txtCreatedBy.Text = "txtCreatedBy";
            this.txtCreatedBy.Top = 1.181102F;
            this.txtCreatedBy.Width = 7.086611F;
            // 
            // PageHeader
            // 
            this.PageHeader.CanShrink = true;
            this.PageHeader.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
            this.Line2,
            this.tbSlujitel,
            this.tbData,
            this.tbNachChas,
            this.tbKrChas,
            this.tbAdmin,
            this.tbProtokol,
            this.Line12,
            this.picture2});
            this.PageHeader.Height = 1.259722F;
            this.PageHeader.Name = "PageHeader";
            this.PageHeader.Format += new System.EventHandler(this.PageHeader_Format);
            // 
            // Line2
            // 
            this.Line2.Height = 0F;
            this.Line2.Left = 0F;
            this.Line2.LineWeight = 2F;
            this.Line2.Name = "Line2";
            this.Line2.Top = 1.220472F;
            this.Line2.Width = 7.086611F;
            this.Line2.X1 = 0F;
            this.Line2.X2 = 7.086611F;
            this.Line2.Y1 = 1.220472F;
            this.Line2.Y2 = 1.220472F;
            // 
            // tbSlujitel
            // 
            this.tbSlujitel.Height = 0.1968504F;
            this.tbSlujitel.Left = 0F;
            this.tbSlujitel.Name = "tbSlujitel";
            this.tbSlujitel.Style = "font-size: 9pt; font-weight: bold";
            this.tbSlujitel.Text = "????????";
            this.tbSlujitel.Top = 1.023622F;
            this.tbSlujitel.Width = 1.811024F;
            // 
            // tbData
            // 
            this.tbData.Height = 0.1968504F;
            this.tbData.HyperLink = null;
            this.tbData.Left = 1.811024F;
            this.tbData.Name = "tbData";
            this.tbData.Style = "font-size: 9pt; font-weight: bold; text-align: center";
            this.tbData.Text = "????";
            this.tbData.Top = 1.023622F;
            this.tbData.Width = 0.7874014F;
            // 
            // tbNachChas
            // 
            this.tbNachChas.Height = 0.1968504F;
            this.tbNachChas.HyperLink = null;
            this.tbNachChas.Left = 2.598425F;
            this.tbNachChas.Name = "tbNachChas";
            this.tbNachChas.Style = "font-size: 8pt; font-weight: bold; white-space: nowrap";
            this.tbNachChas.Text = "???.???";
            this.tbNachChas.Top = 1.023622F;
            this.tbNachChas.Width = 0.472441F;
            // 
            // tbKrChas
            // 
            this.tbKrChas.Height = 0.1968504F;
            this.tbKrChas.HyperLink = null;
            this.tbKrChas.Left = 3.070866F;
            this.tbKrChas.Name = "tbKrChas";
            this.tbKrChas.Style = "font-size: 8pt; font-weight: bold; white-space: nowrap";
            this.tbKrChas.Text = "??.???";
            this.tbKrChas.Top = 1.023622F;
            this.tbKrChas.Width = 0.472441F;
            // 
            // tbAdmin
            // 
            this.tbAdmin.Height = 0.1968504F;
            this.tbAdmin.HyperLink = null;
            this.tbAdmin.Left = 4.822834F;
            this.tbAdmin.Name = "tbAdmin";
            this.tbAdmin.Style = "font-size: 9pt; font-weight: bold; text-align: justify";
            this.tbAdmin.Text = "?????.???????";
            this.tbAdmin.Top = 1.023622F;
            this.tbAdmin.Width = 1.279528F;
            // 
            // tbProtokol
            // 
            this.tbProtokol.Height = 0.2F;
            this.tbProtokol.Left = 3.543307F;
            this.tbProtokol.Name = "tbProtokol";
            this.tbProtokol.Style = "font-size: 9pt; font-weight: bold; text-align: justify";
            this.tbProtokol.Text = "????????";
            this.tbProtokol.Top = 1.023622F;
            this.tbProtokol.Width = 1.279528F;
            // 
            // Line12
            // 
            this.Line12.Height = 0F;
            this.Line12.Left = 0F;
            this.Line12.LineWeight = 1F;
            this.Line12.Name = "Line12";
            this.Line12.Top = 0.9842521F;
            this.Line12.Width = 7.086611F;
            this.Line12.X1 = 0F;
            this.Line12.X2 = 7.086611F;
            this.Line12.Y1 = 0.9842521F;
            this.Line12.Y2 = 0.9842521F;
            // 
            // PageFooter
            // 
            this.PageFooter.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
            this.Line11,
            this.lblPage,
            this.txtContacts});
            this.PageFooter.Height = 0.5909722F;
            this.PageFooter.Name = "PageFooter";
            this.PageFooter.Format += new System.EventHandler(this.PageFooter_Format);
            // 
            // Line11
            // 
            this.Line11.Height = 0F;
            this.Line11.Left = 0F;
            this.Line11.LineWeight = 1F;
            this.Line11.Name = "Line11";
            this.Line11.Top = 0.1181103F;
            this.Line11.Width = 7.086611F;
            this.Line11.X1 = 0F;
            this.Line11.X2 = 7.086611F;
            this.Line11.Y1 = 0.1181103F;
            this.Line11.Y2 = 0.1181103F;
            // 
            // lblPage
            // 
            this.lblPage.Height = 0.1968504F;
            this.lblPage.HyperLink = null;
            this.lblPage.Left = 5.31496F;
            this.lblPage.Name = "lblPage";
            this.lblPage.Style = "font-size: 8pt; font-weight: bold; text-align: right";
            this.lblPage.Text = "Page";
            this.lblPage.Top = 0.1181103F;
            this.lblPage.Width = 1.771653F;
            // 
            // txtContacts
            // 
            this.txtContacts.Height = 0.2F;
            this.txtContacts.Left = 0F;
            this.txtContacts.Name = "txtContacts";
            this.txtContacts.Style = "font-weight: bold; text-align: center";
            this.txtContacts.Text = "Sofia 1700, 4A, Simeonovsko shose Blvd., tel.: +3592 8621978, +3592 8623348, fax." +
    ":+3592 8621614, e-mail: office@asa-bg.com, website: www.asa-bg.com";
            this.txtContacts.Top = 0.3937007F;
            this.txtContacts.Width = 7.086611F;
            // 
            // GrandTotalHeader
            // 
            this.GrandTotalHeader.Height = 0F;
            this.GrandTotalHeader.Name = "GrandTotalHeader";
            this.GrandTotalHeader.Visible = false;
            // 
            // GrandTotalFooter
            // 
            this.GrandTotalFooter.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
            this.Line9,
            this.Line8,
            this.txtProjectTotal,
            this.txtProjectTotalMinutes});
            this.GrandTotalFooter.Height = 0.4965278F;
            this.GrandTotalFooter.Name = "GrandTotalFooter";
            this.GrandTotalFooter.Format += new System.EventHandler(this.GrandTotalFooter_Format);
            // 
            // Line9
            // 
            this.Line9.Height = 0F;
            this.Line9.Left = 0F;
            this.Line9.LineWeight = 2F;
            this.Line9.Name = "Line9";
            this.Line9.Top = 0.4827756F;
            this.Line9.Width = 7.086611F;
            this.Line9.X1 = 7.086611F;
            this.Line9.X2 = 0F;
            this.Line9.Y1 = 0.4827756F;
            this.Line9.Y2 = 0.4827756F;
            // 
            // Line8
            // 
            this.Line8.Height = 0F;
            this.Line8.Left = 0F;
            this.Line8.LineWeight = 2F;
            this.Line8.Name = "Line8";
            this.Line8.Top = 0.2268701F;
            this.Line8.Width = 7.086611F;
            this.Line8.X1 = 7.086611F;
            this.Line8.X2 = 0F;
            this.Line8.Y1 = 0.2268701F;
            this.Line8.Y2 = 0.2268701F;
            // 
            // txtProjectTotal
            // 
            this.txtProjectTotal.Height = 0.1968504F;
            this.txtProjectTotal.Left = 0F;
            this.txtProjectTotal.Name = "txtProjectTotal";
            this.txtProjectTotal.Style = "font-weight: bold";
            this.txtProjectTotal.Text = "txtProjectTotal";
            this.txtProjectTotal.Top = 0.2662401F;
            this.txtProjectTotal.Width = 6.102362F;
            // 
            // txtProjectTotalMinutes
            // 
            this.txtProjectTotalMinutes.DataField = "Time";
            this.txtProjectTotalMinutes.Height = 0.2F;
            this.txtProjectTotalMinutes.Left = 6.102362F;
            this.txtProjectTotalMinutes.Name = "txtProjectTotalMinutes";
            this.txtProjectTotalMinutes.Style = "font-weight: bold; text-align: right";
            this.txtProjectTotalMinutes.SummaryRunning = GrapeCity.ActiveReports.SectionReportModel.SummaryRunning.All;
            this.txtProjectTotalMinutes.SummaryType = GrapeCity.ActiveReports.SectionReportModel.SummaryType.GrandTotal;
            this.txtProjectTotalMinutes.Text = "txtProjectTotalMinutes";
            this.txtProjectTotalMinutes.Top = 0.2564797F;
            this.txtProjectTotalMinutes.Width = 0.9842521F;
            // 
            // ghActivity
            // 
            this.ghActivity.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
            this.txtActivityName,
            this.Line6,
            this.Line5,
            this.txtNoData});
            this.ghActivity.DataField = "ActivityID";
            this.ghActivity.Height = 0.275F;
            this.ghActivity.Name = "ghActivity";
            this.ghActivity.Format += new System.EventHandler(this.ghActivity_Format);
            // 
            // txtActivityName
            // 
            this.txtActivityName.DataField = "ActivityName";
            this.txtActivityName.Height = 0.1968504F;
            this.txtActivityName.Left = 0F;
            this.txtActivityName.MultiLine = false;
            this.txtActivityName.Name = "txtActivityName";
            this.txtActivityName.Style = "font-family: Arial; font-size: 9pt; font-weight: bold; vertical-align: middle";
            this.txtActivityName.SummaryGroup = "ghActivity";
            this.txtActivityName.Text = "ActivityName";
            this.txtActivityName.Top = 0.03937007F;
            this.txtActivityName.Width = 7.086611F;
            // 
            // Line6
            // 
            this.Line6.Height = 0F;
            this.Line6.Left = 0F;
            this.Line6.LineWeight = 1F;
            this.Line6.Name = "Line6";
            this.Line6.Top = 0.0138889F;
            this.Line6.Visible = false;
            this.Line6.Width = 7.086611F;
            this.Line6.X1 = 0F;
            this.Line6.X2 = 7.086611F;
            this.Line6.Y1 = 0.0138889F;
            this.Line6.Y2 = 0.0138889F;
            // 
            // Line5
            // 
            this.Line5.Height = 0F;
            this.Line5.Left = 0F;
            this.Line5.LineWeight = 1F;
            this.Line5.Name = "Line5";
            this.Line5.Top = 0.2362205F;
            this.Line5.Width = 7.086611F;
            this.Line5.X1 = 0F;
            this.Line5.X2 = 7.086611F;
            this.Line5.Y1 = 0.2362205F;
            this.Line5.Y2 = 0.2362205F;
            // 
            // txtNoData
            // 
            this.txtNoData.Height = 0.1968504F;
            this.txtNoData.Left = 0F;
            this.txtNoData.Name = "txtNoData";
            this.txtNoData.Style = "color: Black; font-size: 10pt; font-weight: bold";
            this.txtNoData.Text = "TextBox2";
            this.txtNoData.Top = 0F;
            this.txtNoData.Visible = false;
            this.txtNoData.Width = 6.102362F;
            // 
            // gfActivity
            // 
            this.gfActivity.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
            this.txtActivityTotal,
            this.Line7,
            this.txtActivityTotalHours});
            this.gfActivity.Height = 0.21875F;
            this.gfActivity.Name = "gfActivity";
            this.gfActivity.Format += new System.EventHandler(this.gfActivity_Format);
            // 
            // txtActivityTotal
            // 
            this.txtActivityTotal.DataField = "ActivityName";
            this.txtActivityTotal.Height = 0.1968504F;
            this.txtActivityTotal.Left = 0F;
            this.txtActivityTotal.Name = "txtActivityTotal";
            this.txtActivityTotal.Style = "font-family: Arial; font-size: 9pt; font-weight: bold; vertical-align: middle";
            this.txtActivityTotal.SummaryGroup = "ghActivity";
            this.txtActivityTotal.Text = "txtActivityTotal";
            this.txtActivityTotal.Top = 0F;
            this.txtActivityTotal.Width = 6.102362F;
            // 
            // Line7
            // 
            this.Line7.Height = 0F;
            this.Line7.Left = 0F;
            this.Line7.LineWeight = 1F;
            this.Line7.Name = "Line7";
            this.Line7.Top = 0F;
            this.Line7.Width = 7.086611F;
            this.Line7.X1 = 0F;
            this.Line7.X2 = 7.086611F;
            this.Line7.Y1 = 0F;
            this.Line7.Y2 = 0F;
            // 
            // txtActivityTotalHours
            // 
            this.txtActivityTotalHours.DataField = "Time";
            this.txtActivityTotalHours.Height = 0.1968504F;
            this.txtActivityTotalHours.Left = 6.102362F;
            this.txtActivityTotalHours.Name = "txtActivityTotalHours";
            this.txtActivityTotalHours.OutputFormat = resources.GetString("txtActivityTotalHours.OutputFormat");
            this.txtActivityTotalHours.Style = "font-size: 9pt; font-weight: bold; text-align: right";
            this.txtActivityTotalHours.SummaryGroup = "ghActivity";
            this.txtActivityTotalHours.SummaryRunning = GrapeCity.ActiveReports.SectionReportModel.SummaryRunning.Group;
            this.txtActivityTotalHours.SummaryType = GrapeCity.ActiveReports.SectionReportModel.SummaryType.SubTotal;
            this.txtActivityTotalHours.Text = "txtActivityTotalHours";
            this.txtActivityTotalHours.Top = 0F;
            this.txtActivityTotalHours.Width = 0.9842521F;
            // 
            // ghUser
            // 
            this.ghUser.DataField = "UserID";
            this.ghUser.Height = 0F;
            this.ghUser.Name = "ghUser";
            // 
            // gfUser
            // 
            this.gfUser.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
            this.txtUserTotalLbl,
            this.Line3,
            this.Line4,
            this.txtUserTotalHours});
            this.gfUser.Height = 0.2076389F;
            this.gfUser.Name = "gfUser";
            this.gfUser.Format += new System.EventHandler(this.gfUser_Format);
            // 
            // txtUserTotalLbl
            // 
            this.txtUserTotalLbl.DataField = "UserName";
            this.txtUserTotalLbl.Height = 0.1968504F;
            this.txtUserTotalLbl.Left = 0F;
            this.txtUserTotalLbl.Name = "txtUserTotalLbl";
            this.txtUserTotalLbl.Style = "color: Black; font-size: 9pt; vertical-align: middle";
            this.txtUserTotalLbl.Text = "TotalForUser";
            this.txtUserTotalLbl.Top = 0F;
            this.txtUserTotalLbl.Width = 6.102362F;
            // 
            // Line3
            // 
            this.Line3.Height = 0F;
            this.Line3.Left = 0F;
            this.Line3.LineColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(192)))), ((int)(((byte)(192)))));
            this.Line3.LineStyle = GrapeCity.ActiveReports.SectionReportModel.LineStyle.Dot;
            this.Line3.LineWeight = 1F;
            this.Line3.Name = "Line3";
            this.Line3.Top = 0F;
            this.Line3.Width = 7.086611F;
            this.Line3.X1 = 0F;
            this.Line3.X2 = 7.086611F;
            this.Line3.Y1 = 0F;
            this.Line3.Y2 = 0F;
            // 
            // Line4
            // 
            this.Line4.Height = 0F;
            this.Line4.Left = 0F;
            this.Line4.LineColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(192)))), ((int)(((byte)(192)))));
            this.Line4.LineStyle = GrapeCity.ActiveReports.SectionReportModel.LineStyle.Dot;
            this.Line4.LineWeight = 1F;
            this.Line4.Name = "Line4";
            this.Line4.Top = 0.1968504F;
            this.Line4.Width = 7.086611F;
            this.Line4.X1 = 0F;
            this.Line4.X2 = 7.086611F;
            this.Line4.Y1 = 0.1968504F;
            this.Line4.Y2 = 0.1968504F;
            // 
            // txtUserTotalHours
            // 
            this.txtUserTotalHours.DataField = "Time";
            this.txtUserTotalHours.Height = 0.1968504F;
            this.txtUserTotalHours.Left = 6.102362F;
            this.txtUserTotalHours.Name = "txtUserTotalHours";
            this.txtUserTotalHours.OutputFormat = resources.GetString("txtUserTotalHours.OutputFormat");
            this.txtUserTotalHours.Style = "font-size: 9pt; text-align: right";
            this.txtUserTotalHours.SummaryGroup = "ghUser";
            this.txtUserTotalHours.SummaryRunning = GrapeCity.ActiveReports.SectionReportModel.SummaryRunning.Group;
            this.txtUserTotalHours.SummaryType = GrapeCity.ActiveReports.SectionReportModel.SummaryType.SubTotal;
            this.txtUserTotalHours.Text = "txtUserTotalHours";
            this.txtUserTotalHours.Top = 0F;
            this.txtUserTotalHours.Width = 0.9842521F;
            // 
            // picture1
            // 
            this.picture1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.picture1.Height = 0.875F;
            this.picture1.HyperLink = null;
            this.picture1.ImageData = ((System.IO.Stream)(resources.GetObject("picture1.ImageData")));
            this.picture1.Left = 2.070497F;
            this.picture1.LineColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.picture1.Name = "picture1";
            this.picture1.PictureAlignment = GrapeCity.ActiveReports.SectionReportModel.PictureAlignment.BottomRight;
            this.picture1.SizeMode = GrapeCity.ActiveReports.SectionReportModel.SizeModes.Zoom;
            this.picture1.Top = 0.1197915F;
            this.picture1.Width = 2.952756F;
            // 
            // picture2
            // 
            this.picture2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.picture2.Height = 0.875F;
            this.picture2.HyperLink = null;
            this.picture2.ImageData = ((System.IO.Stream)(resources.GetObject("picture2.ImageData")));
            this.picture2.Left = 2.123F;
            this.picture2.LineColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.picture2.Name = "picture2";
            this.picture2.PictureAlignment = GrapeCity.ActiveReports.SectionReportModel.PictureAlignment.BottomRight;
            this.picture2.SizeMode = GrapeCity.ActiveReports.SectionReportModel.SizeModes.Zoom;
            this.picture2.Top = 0F;
            this.picture2.Width = 2.952756F;
            // 
            // MinutesReport
            // 
            this.MasterReport = false;
            this.PageSettings.DefaultPaperSize = false;
            this.PageSettings.Margins.Bottom = 0.2131944F;
            this.PageSettings.Margins.Left = 0.2083333F;
            this.PageSettings.Margins.Right = 0.2083333F;
            this.PageSettings.Margins.Top = 0.2131944F;
            this.PageSettings.PaperHeight = 11.69291F;
            this.PageSettings.PaperKind = System.Drawing.Printing.PaperKind.A4;
            this.PageSettings.PaperWidth = 8.268056F;
            this.PrintWidth = 7.09375F;
            this.Sections.Add(this.ReportHeader);
            this.Sections.Add(this.PageHeader);
            this.Sections.Add(this.GrandTotalHeader);
            this.Sections.Add(this.ghActivity);
            this.Sections.Add(this.ghUser);
            this.Sections.Add(this.Detail);
            this.Sections.Add(this.gfUser);
            this.Sections.Add(this.gfActivity);
            this.Sections.Add(this.GrandTotalFooter);
            this.Sections.Add(this.PageFooter);
            this.Sections.Add(this.ReportFooter);
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule(resources.GetString("$this.StyleSheet"), "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: inherit; font-style: inherit; font-variant: inherit; font-weight: bo" +
            "ld; font-size: 16pt; font-size-adjust: inherit; font-stretch: inherit", "Heading1", "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: Times New Roman; font-style: italic; font-variant: inherit; font-wei" +
            "ght: bold; font-size: 14pt; font-size-adjust: inherit; font-stretch: inherit", "Heading2", "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: inherit; font-style: inherit; font-variant: inherit; font-weight: bo" +
            "ld; font-size: 13pt; font-size-adjust: inherit; font-stretch: inherit", "Heading3", "Normal"));
            this.FetchData += new GrapeCity.ActiveReports.SectionReport.FetchEventHandler(this.MinutesReport_FetchData);
            this.ReportStart += new System.EventHandler(this.MinutesReport_ReportStart);
            ((System.ComponentModel.ISupportInitialize)(this.txtUserName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtStartHour)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtEndHour)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtMinutes)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAdminMinutes)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDetailWorkDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtContacts1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblReportDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblProektant)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCreatedBy)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbSlujitel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbData)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbNachChas)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbKrChas)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbAdmin)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbProtokol)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblPage)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtContacts)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtProjectTotal)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtProjectTotalMinutes)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtActivityName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtNoData)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtActivityTotal)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtActivityTotalHours)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtUserTotalLbl)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtUserTotalHours)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picture1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picture2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();

        }

		#endregion

        private ReportHeader ReportHeader;
        private SubReport SubReport1;
        private PageHeader PageHeader;
        private Line Line2;
        private TextBox tbSlujitel;
        private Label tbData;
        private Label tbNachChas;
        private Label tbKrChas;
        private Label tbAdmin;
        private TextBox tbProtokol;
        private Line Line12;
        private GroupHeader GrandTotalHeader;
        private GroupHeader ghActivity;
        private TextBox txtActivityName;
        private Line Line6;
        private Line Line5;
        private TextBox txtNoData;
        private GroupHeader ghUser;
        private Detail Detail;
        private TextBox txtUserName;
        private TextBox txtStartHour;
        private TextBox txtEndHour;
        private TextBox txtMinutes;
        private TextBox txtAdminMinutes;
        private TextBox txtDetailWorkDate;
        private GroupFooter gfUser;
        private TextBox txtUserTotalLbl;
        private Line Line3;
        private Line Line4;
        private TextBox txtUserTotalHours;
        private GroupFooter gfActivity;
        private TextBox txtActivityTotal;
        private Line Line7;
        private TextBox txtActivityTotalHours;
        private GroupFooter GrandTotalFooter;
        private Line Line9;
        private Line Line8;
        private TextBox txtProjectTotal;
        private TextBox txtProjectTotalMinutes;
        private PageFooter PageFooter;
        private Line Line11;
        private Label lblPage;
        private TextBox txtContacts;
        private ReportFooter ReportFooter;
        private TextBox txtContacts1;
        private Label lblReportDate;
        private Label lblProektant;
        private TextBox txtCreatedBy;
	
	}
}
