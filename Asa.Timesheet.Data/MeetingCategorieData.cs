using System;
using System.Collections;
using System.Text;

namespace Asa.Timesheet.Data
{
	public class MeetingCategorieData
	{
		private int _meetingCategoryID = -1;
		private int _meetingID = -1;
		private int _hotIssueCategoryID = -1;

		/// <summary>
		/// Initialize an new empty Login object.
		/// </summary>
		public MeetingCategorieData()
		{
		}
		
		/// <summary>
		/// Initialize a new Login object with the given parameters.
		/// </summary>
		public MeetingCategorieData(int meetingCategoryID, int meetingID, int hotIssueCategoryID)
		{
			this._meetingCategoryID = meetingCategoryID;
			this._meetingID = meetingID;
			this._hotIssueCategoryID = hotIssueCategoryID;
		}	
		
	  
		public override string ToString()
		{
			StringBuilder builder = new StringBuilder();
			builder.Append("_meetingCategoryID: " + _meetingCategoryID.ToString()).Append("\r\n");
			builder.Append("_meetingID: " + _meetingID.ToString()).Append("\r\n");
			builder.Append("_hotIssueCategoryID: " + _hotIssueCategoryID.ToString()).Append("\r\n");
			builder.Append("\r\n");
			return builder.ToString();
		}	
		public object GetByIndex(int nIndex)
		{
			switch(nIndex)
			{
				case 0:
					return _meetingCategoryID;
				case 1:
					return _meetingID;
				case 2:
					return _hotIssueCategoryID;
			}
			return null;
		}
		#region Properties
	
		public int MeetingCategoryID
		{
			get { return _meetingCategoryID; }
			set { _meetingCategoryID = value; }
		}
	
		public int MeetingID
		{
			get { return _meetingID; }
			set { _meetingID = value; }
		}
	
		public int HotIssueCategoryID
		{
			get { return _hotIssueCategoryID; }
			set { _hotIssueCategoryID = value; }
		}
	
		#endregion
	}
}


