using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using Asa.Timesheet.Data;
using Asa.Timesheet.Data.Entities;
using log4net;
using System.Data.SqlClient;
using GrapeCity.ActiveReports.Export.Pdf.Section;
using GrapeCity.ActiveReports.Data;
using GrapeCity.ActiveReports.Export.Excel.Section;
namespace PM
{
	/// <summary>
	/// Summary description for Logs.
	/// </summary>
	public partial class Logs : BasePage
	{
		#region WebControls
		
		protected System.Web.UI.WebControls.Button btnBack;
		protected PM.UserControls.PageHeader header;
	
		private static readonly ILog log = LogManager.GetLogger(typeof(Logs));

		#endregion

		#region enums

		private enum gridColomns
		{
			ActionDate= 0,
			NotDownload,
			Account,
			FileSubject,
			FileMinutes,
			FileDate,
			CategoryName,
			ProjectName,
			link,
			fileID,
			Deleted
		}


		#endregion

		#region Page_Load

        protected void Page_Load(object sender, System.EventArgs e)
        {
            if (!LoggedUser.IsAuthenticated) ErrorRedirect(UIHelpers.GetText(Language, "noRightsAlert"));
            //if(UIHelpers.GetPIDParam() == -1 && UIHelpers.GetCIDParam() == -1 && UIHelpers.GetIDParam() == -1)	ErrorRedirect(Resource.ResourceManager["noRightsAlert"]);
            //notes:if user is not autherized he can't see logs
            bool b = (LoggedUser.UserRoleID > 0);//== (int)Roles.Director);
            if (!b) ErrorRedirect(UIHelpers.GetText(Language, "noRightsAlert"));

            LoadResourses();

            if (!this.IsPostBack)
            {

                //TODO
                ddlCategories.Enabled = ddlFiles.Enabled = false;
                UIHelpers.LoadProjectStatus(ddlProjectsStatus, LoggedUser.HasPaymentRights);
                UIHelpers.LoadBuildingTypes(ddlBuildingTypes);
                UIHelpers.LoadProjects(ddlProjects, ddlProjectsStatus, ddlBuildingTypes);
                //grdLogs.CurrentPageIndex = 1;
                //BindGrid();

                //lblError.Text = "";
                ddlCategories.Items.Clear();
                ddlCategories.Items.Insert(0, new ListItem(UIHelpers.GetText(Language, "DDL_all"), "-1"));
                ddlCategories.SelectedValue = "-1";
                ddlFiles.Items.Clear();
                ddlFiles.Items.Insert(0, new ListItem(UIHelpers.GetText(Language, "DDL_all"), "-1"));
                ddlFiles.SelectedValue = "-1";


                ddlUser.DataSource = UIHelpers.GetAllPMUsers().Values;
                ddlUser.DataBind();
                ddlUser.Items.Insert(0, new ListItem(UIHelpers.GetText(Language, "DDL_all"), "-1"));
                ddlUser.SelectedValue = "-1";

                ddlType.Items.Clear();
                ddlType.Items.Add(new ListItem(UIHelpers.GetText(Language, "DDL_all"), "-1"));
                ddlType.Items.Add(new ListItem(UIHelpers.GetText(Language, "Name_LogType1"), "1"));
                ddlType.Items.Add(new ListItem(UIHelpers.GetText(Language, "Name_LogType2"), "2"));
                ddlType.SelectedValue = "-1";
            }

        }

//		private bool LoadBuildingTypes(DropDownList ddlBuildingTypes, string selectedValue)
//		{
//			SqlDataReader reader = null;
//			try
//			{
//				reader = DBManager.SelectBuildingTypes();
//				ddlBuildingTypes.DataSource = reader;
//				ddlBuildingTypes.DataValueField = "BuildingTypeID";
//				ddlBuildingTypes.DataTextField = "BuildingType";
//				ddlBuildingTypes.DataBind();
//				if(ddlBuildingTypes.Items.FindByValue(System.Configuration.ConfigurationManager.AppSettings["BuildingTypeNotInPM"])!=null)
//					ddlBuildingTypes.Items.Remove(ddlBuildingTypes.Items.FindByValue(System.Configuration.ConfigurationManager.AppSettings["BuildingTypeNotInPM"]));
//				
//				ddlBuildingTypes.Items.Insert(0, new ListItem(Resource.ResourceManager["reports_typeBuilding"], "0"));
//				ddlBuildingTypes.SelectedValue = selectedValue;
//			}
//			catch
//			{
//				
//				return false;
//			}
//			finally 
//			{
//				if (reader!=null) reader.Close();
//			}
//
//			return true;
//		}

        private void LoadResourses()
        {
            string lang = Language;
            lblName.Text = UIHelpers.GetText(lang, "Name_ProjectName");
            Label1.Text = UIHelpers.GetText(lang, "Name_CategoryName");
            Label2.Text = UIHelpers.GetText(lang, "Name_FileNameName");
            btnSearch.Text = UIHelpers.GetText(lang, "Name_btnShowName");
            lbBuildingType.Text = UIHelpers.GetText(lang, "Name_lbBuildingTypeName");
            lbProjectType.Text = UIHelpers.GetText(lang, "Name_TypeName");
            lbUser.Text = UIHelpers.GetText(lang, "Name_UserName");
            lbType.Text = UIHelpers.GetText(lang, "Name_LogType");


            grdLogs.Columns[(int)gridColomns.ActionDate].HeaderText = UIHelpers.GetText(lang, "ActionName_DateColomn");
            grdLogs.Columns[(int)gridColomns.NotDownload].HeaderText = UIHelpers.GetText(lang, "Name_NotDownloadColomn");
            grdLogs.Columns[(int)gridColomns.Account].HeaderText = UIHelpers.GetText(lang, "Name_AccountColomn");
            grdLogs.Columns[(int)gridColomns.FileSubject].HeaderText = UIHelpers.GetText(lang, "Name_FileSubjectColomn");
            grdLogs.Columns[(int)gridColomns.FileMinutes].HeaderText = UIHelpers.GetText(lang, "Name_FileMinutesColomn");
            grdLogs.Columns[(int)gridColomns.FileDate].HeaderText = UIHelpers.GetText(lang, "Name_DateColomn1");
            grdLogs.Columns[(int)gridColomns.CategoryName].HeaderText = UIHelpers.GetText(lang, "Name_CategoryNameColomn");
            grdLogs.Columns[(int)gridColomns.ProjectName].HeaderText = UIHelpers.GetText(lang, "Name_ProjectNameColomn");


            Header.PageTitle = UIHelpers.GetText(lang, "pmPageTitle_Logs");
            //TODO
        }
		#endregion

		#region Web Form Designer generated code
        override protected void OnInit(EventArgs e)
        {
            //
            // CODEGEN: This call is required by the ASP.NET Web Form Designer.
            //
            InitializeComponent();
            base.OnInit(e);
        }
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
        private void InitializeComponent()
        {
            this.grdLogs.ItemCreated += new System.Web.UI.WebControls.DataGridItemEventHandler(this.grdLogs_ItemCreated);
            this.grdLogs.ItemCommand += new System.Web.UI.WebControls.DataGridCommandEventHandler(this.grdLogs_ItemCommand);
            this.grdLogs.PageIndexChanged += new System.Web.UI.WebControls.DataGridPageChangedEventHandler(this.grdLogs_PageIndexChanged);
            this.grdLogs.ItemDataBound += new System.Web.UI.WebControls.DataGridItemEventHandler(this.grdLogs_ItemDataBound);

        }
		#endregion

		#region DB

        private ProjectLogsVector BindGrid()
        {

            ProjectLogsVector flv = new ProjectLogsVector();
            string sUser = null;
            int nLogType = -1;
            if (ddlUser.SelectedIndex > 0)
                sUser = ddlUser.SelectedValue;
            if (ddlType.SelectedIndex > 0)
                nLogType = UIHelpers.ToInt(ddlType.SelectedValue);
            if (UIHelpers.ToInt(ddlProjects.SelectedValue) > 0)
            {
                if (UIHelpers.ToInt(ddlCategories.SelectedValue) > 0)
                {
                    if (UIHelpers.ToInt(ddlFiles.SelectedValue) > 0)
                    {

                        flv = ProjectLogDAL.LoadCollection(-1, -1, UIHelpers.ToInt(ddlFiles.SelectedValue), SortOrder, int.Parse(ddlProjectsStatus.SelectedValue), int.Parse(ddlBuildingTypes.SelectedValue), sUser, nLogType);
                        grdLogs.Columns[(int)gridColomns.ProjectName].Visible = false;
                        grdLogs.Columns[(int)gridColomns.CategoryName].Visible = false;
                    }
                    else
                    {
                        flv = ProjectLogDAL.LoadCollection(-1, UIHelpers.ToInt(ddlCategories.SelectedValue), -1, SortOrder, int.Parse(ddlProjectsStatus.SelectedValue), int.Parse(ddlBuildingTypes.SelectedValue), sUser, nLogType);
                        grdLogs.Columns[(int)gridColomns.ProjectName].Visible = false;
                        grdLogs.Columns[(int)gridColomns.CategoryName].Visible = true;
                    }
                }
                else
                {
                    flv = ProjectLogDAL.LoadCollection(UIHelpers.ToInt(ddlProjects.SelectedValue), -1, -1, SortOrder, int.Parse(ddlProjectsStatus.SelectedValue), int.Parse(ddlBuildingTypes.SelectedValue), sUser, nLogType);
                    grdLogs.Columns[(int)gridColomns.ProjectName].Visible = true;
                    grdLogs.Columns[(int)gridColomns.CategoryName].Visible = true;
                }
            }
            else
            {
                flv = ProjectLogDAL.LoadCollection(-1, -1, -1, SortOrder, int.Parse(ddlProjectsStatus.SelectedValue), int.Parse(ddlBuildingTypes.SelectedValue), sUser, nLogType);
                grdLogs.Columns[(int)gridColomns.ProjectName].Visible = true;
                grdLogs.Columns[(int)gridColomns.CategoryName].Visible = true;
            }
            if (SessionManager.Lang == Languages.EN)
                grdLogs.Columns[(int)gridColomns.Account].Visible = false;
            grdLogs.DataSource = flv;
            grdLogs.DataKeyField = "ProjectLogID";
            grdLogs.DataBind();
            if (grdLogs.Items.Count > 0)
            {
                grid.Visible = true;
                lblInfo.Text = "";
            }
            else
            {
                grid.Visible = false;
                lblInfo.Text = UIHelpers.GetText(Language, "No_Logs_Found");
            }
            return flv;
        }
		

//		private void LoadProjects()
//		{
//			SqlDataReader reader = null;
//			try
//			{
//				reader = ProjectsData.SelectProjectNamesClear(this.ProjectsStatus,int.Parse(ddlBuildingTypes.SelectedValue));
//			
//					
//				
//			
//
//				//				if(LoggedUser.IsUser)
//				//					reader = ProjectsData.SelectProjects(1/*SortOrder*/,  Active,  txtProject.Text.Trim(), int.Parse(ddlBuildingTypes.SelectedValue), ddlSearch.SelectedValue=="2",true,-1,-1);
//				if (reader!=null)
//				{
//					ddlProjects.DataSource = reader;
//					ddlProjects.DataValueField = "ProjectID";
//					ddlProjects.DataTextField = "ProjectName";
//					ddlProjects.DataBind();
//
//					ddlProjects.Items.Insert(0, new ListItem("<"+Resource.ResourceManager["reports_ddlAllProjects"]+">", "-1"));
//					ddlProjects.SelectedValue = "-1";
//				}
//				BindGrid();
//			}
//			catch(Exception ex)
//			{
//				log.Error(UIHelpers.GetLogErrorMessage(ex));
//			}
//		}
	
	
		#endregion

		#region EventHandlers

        private void grdLogs_ItemDataBound(object sender, System.Web.UI.WebControls.DataGridItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Item
                || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                if (e.Item.Cells[(int)gridColomns.Deleted].Text == "True")
                {
                    e.Item.Cells[(int)gridColomns.NotDownload].Text = UIHelpers.GetText(Language, "Log_Deleted");
                    e.Item.Cells[(int)gridColomns.link].Visible = false;
                }
                else if (e.Item.Cells[(int)gridColomns.NotDownload].Text == "True")
                {
                    e.Item.Cells[(int)gridColomns.NotDownload].Text = UIHelpers.GetText(Language, "Log_NotDownload");
                }
                else
                {
                    e.Item.Cells[(int)gridColomns.NotDownload].Text = UIHelpers.GetText(Language, "Log_Download");
                }
            }
        }


        protected void ddlProjects_SelectedIndexChanged(object sender, System.EventArgs e)
        {
            int nProjectID = UIHelpers.ToInt(ddlProjects.SelectedValue);
            ProjectCategoriesVector pcv = new ProjectCategoriesVector();
            if (UIHelpers.ToInt(ddlProjects.SelectedValue) > 0)
                pcv = ProjectCategoryDAL.LoadCollection(nProjectID, (int)PMUserType.User);

            if (nProjectID > 0)
            {
                ddlUser.DataSource = UIHelpers.GetAllPMUsers(UIHelpers.ToInt(ddlProjects.SelectedValue), LoggedUser.HasPaymentRights).Values;
                ddlUser.DataBind();
                ddlUser.Items.Insert(0, new ListItem(UIHelpers.GetText(Language, "DDL_all"), "-1"));
                ddlUser.SelectedValue = "-1";

            }
            else
            {
                ddlUser.DataSource = UIHelpers.GetAllPMUsers().Values;
                ddlUser.DataBind();
                ddlUser.Items.Insert(0, new ListItem(UIHelpers.GetText(Language, "DDL_all"), "-1"));
                ddlUser.SelectedValue = "-1";

            }


            if (pcv.Count > 0)
            {
                ddlCategories.Enabled = true;
                ddlFiles.Enabled = false;
                ddlCategories.DataSource = pcv;
                ddlCategories.DataTextField = "CategoryName";
                ddlCategories.DataValueField = "CategoryID";
                ddlCategories.DataBind();
                ddlCategories.Items.Insert(0, new ListItem(UIHelpers.GetText(Language, "DDL_all"), "-1"));
                ddlCategories.SelectedValue = "-1";
                ddlFiles.Items.Clear();
                ddlFiles.Items.Insert(0, new ListItem(UIHelpers.GetText(Language, "DDL_all"), "-1"));
                ddlFiles.SelectedValue = "-1";
                ddlFiles.Enabled = false;


            }
            else
            {
                ddlCategories.Items.Clear();
                ddlCategories.Items.Insert(0, new ListItem(UIHelpers.GetText(Language, "DDL_none"), "-1"));
                ddlCategories.SelectedValue = "-1";
                ddlCategories.Enabled = false;
                ddlFiles.Items.Clear();
                ddlFiles.Items.Insert(0, new ListItem(UIHelpers.GetText(Language, "DDL_none"), "-1"));
                ddlFiles.SelectedValue = "-1";
                ddlFiles.Enabled = false;


            }
        }


        protected void ddlCategories_SelectedIndexChanged(object sender, System.EventArgs e)
        {
            FilesPMVector pcv = new FilesPMVector();
            if (UIHelpers.ToInt(ddlCategories.SelectedValue) > 0)
                pcv = FilePMDAL.LoadCollectionByCategory(UIHelpers.ToInt(ddlCategories.SelectedValue), LoggedUser.UserType, SortOrder);
            if (pcv.Count > 0)
            {
                ddlFiles.Enabled = true;
                ddlFiles.DataSource = pcv;
                ddlFiles.DataTextField = "FileSubject";
                ddlFiles.DataValueField = "FileID";
                ddlFiles.DataBind();
                ddlFiles.Items.Insert(0, new ListItem(UIHelpers.GetText(Language, "DDL_all"), "-1"));
                ddlFiles.SelectedValue = "-1";
            }
            else
            {
                ddlFiles.Items.Clear();
                ddlFiles.Items.Insert(0, new ListItem(UIHelpers.GetText(Language, "DDL_none"), "-1"));
                ddlFiles.SelectedValue = "-1";
                ddlFiles.Enabled = false;
            }
        }


        protected void btnSearch_Click(object sender, System.EventArgs e)
        {
            grdLogs.CurrentPageIndex = 0;
            BindGrid();
        }


        private void grdLogs_PageIndexChanged(object source, System.Web.UI.WebControls.DataGridPageChangedEventArgs e)
        {
            grdLogs.CurrentPageIndex = e.NewPageIndex;
            BindGrid();
        }

        protected void ddlProjectsStatus_SelectedIndexChanged(object sender, System.EventArgs e)
        {
            UIHelpers.LoadProjects(ddlProjects, ddlProjectsStatus, ddlBuildingTypes);
            ddlCategories.Items.Clear();
            ddlCategories.Items.Insert(0, new ListItem(UIHelpers.GetText(Language, "DDL_all"), "-1"));
            ddlCategories.SelectedValue = "-1";
            ddlCategories.Enabled = false;

            ddlFiles.Items.Clear();
            ddlFiles.Items.Insert(0, new ListItem(UIHelpers.GetText(Language, "DDL_all"), "-1"));
            ddlFiles.SelectedValue = "-1";
            ddlFiles.Enabled = false;
        }

        protected void ddlBuildingTypes_SelectedIndexChanged(object sender, System.EventArgs e)
        {
            UIHelpers.LoadProjects(ddlProjects, ddlProjectsStatus, ddlBuildingTypes);

            ddlCategories.Items.Clear();
            ddlCategories.Items.Insert(0, new ListItem(UIHelpers.GetText(Language, "DDL_all"), "-1"));
            ddlCategories.SelectedValue = "-1";
            ddlCategories.Enabled = false;

            ddlFiles.Items.Clear();
            ddlFiles.Items.Insert(0, new ListItem(UIHelpers.GetText(Language, "DDL_all"), "-1"));
            ddlFiles.SelectedValue = "-1";
            ddlFiles.Enabled = false;
        }

        private void grdLogs_ItemCommand(object source, System.Web.UI.WebControls.DataGridCommandEventArgs e)
        {
            if (e.CommandName == "Sort")
            {
                int sortOrder = SortOrder;
                switch ((string)e.CommandArgument)
                {
                    case "ActionDate": if (sortOrder == 1) sortOrder = -1; else sortOrder = 1;
                        SortOrder = sortOrder;
                        BindGrid(); break;
                    case "CreateOrDownload": if (sortOrder == 2) sortOrder = -2; else sortOrder = 2;
                        SortOrder = sortOrder;
                        BindGrid(); break;
                    case "ActionAccount": if (sortOrder == 3) sortOrder = -3; else sortOrder = 3;
                        SortOrder = sortOrder;
                        BindGrid(); break;
                    case "FileSubject": if (sortOrder == 4) sortOrder = -4; else sortOrder = 4;
                        SortOrder = sortOrder;
                        BindGrid(); break;
                    case "FileMinutes": if (sortOrder == 5) sortOrder = -5; else sortOrder = 5;
                        SortOrder = sortOrder;
                        BindGrid(); break;
                    case "CategoryName": if (sortOrder == 6) sortOrder = -6; else sortOrder = 6;
                        SortOrder = sortOrder;
                        BindGrid(); break;
                    case "ProjectName": if (sortOrder == 7) sortOrder = -7; else sortOrder = 7;
                        SortOrder = sortOrder;
                        BindGrid(); break;
                }

                return;
            }
            if (e.CommandName == "Edit")
            {
                int ID = UIHelpers.ToInt(e.Item.Cells[(int)gridColomns.fileID].Text);
                FilePMData dat = FilePMDAL.Load(ID);
                if (dat != null)
                {
                    ProjectCategoryData pcd = ProjectCategoryDAL.Load(dat.CatetegoryID);

                    if (pcd != null)
                        Response.Redirect("Files.aspx?cid=" + dat.CatetegoryID.ToString() + "&pid=" + pcd.ProjectID.ToString());
                }
                //				string filePath = /*Server.MapPath(*/string.Concat(System.Configuration.ConfigurationManager.AppSettings["FilePath"],@"\",dat.ProjectName,@"\",dat.CategoryName,@"\",dat.FileNameSave,dat.FileType);//);
                //			
                //				Response.Cache.SetExpires(DateTime.Now.AddSeconds(-1));
                //				Response.Cache.SetCacheability(HttpCacheability.Private);
                //				Response.AppendHeader("content-disposition", "attachment; filename="+dat.FileNameSave+dat.FileType);
                //				//Response.AppendHeader("content-disposition", "attachment; filename="+ID+e.Item.Cells[(int)gridColomns.Type].Text);
                //     
                //				Response.Charset = "";
                //				Response.TransmitFile( filePath );
                //				//notes:here send message if file is tracking
                //				//FilePMData fPMd = FilePMDAL.Load(ID);
                //				if(dat.MailForDownload&&LoggedUser.FullName!=dat.CreatedBy)
                //				{
                //					UserInfoPM MailToUser = UsersData.SelectUserPMDownLoadByName(dat.CreatedBy);
                //					MailBO.PMSendMailDownload(LoggedUser.Account,dat,MailToUser.Mail);
                //				}
                //			
                //				//notes:Save Log in DateBase
                //				ProjectLogData pld = new ProjectLogData(-1,ID,DateTime.Now,false,LoggedUser.FullName);
                //				ProjectLogDAL.Save(pld);
                //				Response.End();
                //				return;
            }
        }


        private void grdLogs_ItemCreated(object sender, System.Web.UI.WebControls.DataGridItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Header)
            {
                string imgUrl = "";
                int sortOrder = SortOrder;

                if (sortOrder != 0)
                {
                    imgUrl = (sortOrder > 0) ? "images/sup.gif" : "images/sdown.gif";
                    Label sep = new Label();
                    sep.Width = 2;
                    e.Item.Cells[Math.Abs(sortOrder) - 1].Controls.Add(sep);
                    ImageButton ib = new ImageButton();
                    ib.Height = 11; ib.Width = 11; ib.ImageUrl = imgUrl;
                    e.Item.Cells[Math.Abs(sortOrder) - 1].Controls.Add(ib);
                }
            }
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                LinkButton lb = (LinkButton)e.Item.FindControl("lnk");
                lb.Text = UIHelpers.GetText(Language, "log_Load");

            }

        }
	
		#endregion

        protected void ibPdfExport_Click(object sender, System.Web.UI.ImageClickEventArgs e)
        {
            PdfExport pdf = new PdfExport();

            pdf.NeverEmbedFonts = "";
            ProjectLogsVector plg = BindGrid();

            string ds = Serialization.SerializeObjectToXmlString(plg);
            GrapeCity.ActiveReports.SectionReport rpt = new Reports.rptLogs();
            rpt.PageSettings.Orientation = GrapeCity.ActiveReports.Document.Section.PageOrientation.Landscape;
            XMLDataSource source;
            source = new XMLDataSource();
            source.LoadXML((string)ds);
            source.RecordsetPattern = "//ProjectLogData";
            rpt.DataSource = source;
            rpt.Run(false);

            System.IO.MemoryStream memoryFile = new System.IO.MemoryStream();

            pdf.Export(rpt.Document, memoryFile);

            Response.Clear();
            Response.AppendHeader("content-disposition"
                , "attachment; filename=" + "Logs.pdf");
            Response.ContentType = "application/pdf";

            memoryFile.WriteTo(Response.OutputStream);
            Response.End();
        }

        protected void ibXlsExport_Click(object sender, System.Web.UI.ImageClickEventArgs e)
        {
            XlsExport xls = new XlsExport();


            ProjectLogsVector plg = BindGrid();

            string ds = Serialization.SerializeObjectToXmlString(plg);
            GrapeCity.ActiveReports.SectionReport rpt = new Reports.rptLogs();
            rpt.PageSettings.Orientation = GrapeCity.ActiveReports.Document.Section.PageOrientation.Landscape;
            XMLDataSource source;
            source = new XMLDataSource();
            source.LoadXML((string)ds);
            source.RecordsetPattern = "//ProjectLogData";
            rpt.DataSource = source;
            rpt.Run(false);

            System.IO.MemoryStream memoryFile = new System.IO.MemoryStream();

            xls.Export(rpt.Document, memoryFile);

            Response.Clear();
            Response.AppendHeader("content-disposition"
                , "attachment; filename=" + "Logs.xls");
            Response.ContentType = "application/xls";

            memoryFile.WriteTo(Response.OutputStream);
            Response.End();
        }
	
		public ProjectsData.ProjectsByStatus ProjectsStatus
		{
			get
			{
				try
				{
					return (ProjectsData.ProjectsByStatus)int.Parse(ddlProjectsStatus.SelectedValue);
				}
				catch { return ProjectsData.ProjectsByStatus.AllProjects; }
			}
		}

		

		
		

	}
}
