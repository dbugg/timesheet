using System;
using System.Data;
using System.Data.SqlClient; 
using System.Collections;
using Microsoft.ApplicationBlocks.Data;

namespace Asa.Timesheet.Data
{
	public class MeetingDAL
	{
		public static string SelectMeetingNumber(int projectID, DateTime dt)
		{
			string spName = "MeetingSelectNumberByProject";

			SqlParameter[] storedParams = SqlHelperParameterCache.GetSpParameterSet(DBManager.GetConnectionString(), spName);
			storedParams[0].Value = projectID;
			storedParams[1].Value = dt;
			object o = SqlHelper.ExecuteScalar(DBManager.GetConnectionString(), CommandType.StoredProcedure, spName, storedParams);
			return o == null ? String.Empty : ((int)o).ToString();
		}
		/// <summary>
		/// Loads a MeetingData object from the database using the given MeetingID
		/// </summary>
		/// <param name="meetingID">The primary key value</param>
		/// <returns>A MeetingData object</returns>
		public static MeetingData Load(DateTime meetingDate, int ProjectID, int ActivityID, int StartTimeID, int EndTimeID)
		{
			SqlParameter[] parameterValues = new SqlParameter[] { new SqlParameter("@MeetingDate", meetingDate), 
			new SqlParameter("@ProjectID", ProjectID),new SqlParameter("@ActivityID", ActivityID),
			new SqlParameter("@StartTimeID", StartTimeID),new SqlParameter("@EndTimeID", EndTimeID),};
			using (SqlDataReader reader = SqlHelper.ExecuteReader(CommonDAL.ConnectionString, CommandType.StoredProcedure, "MeetingsSelProc", parameterValues))
			{
				if (reader.Read())
				{
					MeetingData result = new MeetingData();
					LoadFromReader(reader,result);
					return result;
				}
				else
					return null;
			}
		}
		public static MeetingData Load(int meetingID)
		{
			SqlParameter[] parameterValues = new SqlParameter[] { new SqlParameter("@MeetingID", meetingID) };
			using (SqlDataReader reader = SqlHelper.ExecuteReader(CommonDAL.ConnectionString, CommandType.StoredProcedure, "MeetingsSelByIDProc", parameterValues))
			{
				if (reader.Read())
				{
					MeetingData result = new MeetingData();
					LoadFromReader(reader,result);
					return result;
				}
				else
					return null;
			}
		}
		/// <summary>
		/// Loads a collection of MeetingData objects from the database.
		/// </summary>
		/// <returns>A collection containing all of the MeetingData objects in the database.</returns>
		public static MeetingsVector LoadCollection()
		{
			SqlParameter[] parms = new SqlParameter[] {};
			return LoadCollection("MeetingsListProc", parms);
		}
	  
		
		/// <summary>
		/// Loads a collection of MeetingData objects from the database.
		/// </summary>
		/// <returns>A collection containing all of the MeetingData objects in the database.</returns>
		public static MeetingsVector LoadCollection(string whereClause)
		{
			SqlParameter[] parms = new SqlParameter[] { new SqlParameter("@WhereClause", whereClause) };
			return LoadCollection("MeetingsListProc", parms);
		}

		/// <summary>
		/// Loads a collection of MeetingData objects from the database.
		/// </summary>
		/// <returns>A collection containing all of the MeetingData objects in the database.</returns>
		public static MeetingsVector LoadCollection(string spName, SqlParameter[] parms)
		{
			MeetingsVector result = new MeetingsVector();
			using (SqlDataReader reader = SqlHelper.ExecuteReader(CommonDAL.ConnectionString, CommandType.StoredProcedure, spName, parms))
			{
				while (reader.Read())
				{
					MeetingData tmp = new MeetingData();
					LoadFromReader(reader, tmp);
					result.Add(tmp);
				}
			}
			return result;
		}
		/// <summary>
		/// Loads a MeetingData object from the database using the given where clause
		/// </summary>
		/// <param name="whereClause">The filter expression for the query</param>
		/// <returns>A MeetingData object</returns>
		public static MeetingData Load(string whereClause)
		{
			SqlParameter[] parameterValues = new SqlParameter[] { new SqlParameter("@WhereClause", whereClause) };
			using (SqlDataReader reader = SqlHelper.ExecuteReader(CommonDAL.ConnectionString, CommandType.StoredProcedure, "MeetingsListProc", parameterValues))
			{
				if (reader.Read())
				{
					MeetingData result = new MeetingData();
					LoadFromReader(reader,result);
					return result;
				}
				else
					return null;
			}
		}
		public static int Save( MeetingData source)
		{
			if (source.MeetingID == -1)
				return Insert(source);
			else
				return Update(source);	
		}

		public static void Delete( MeetingData source)
		{
			Delete(source.MeetingID);
		}
		/// <summary>
		/// Deletes a MeetingData object from the database.
		/// </summary>
		/// <param name="meetingID">The primary key value</param>
		public static void Delete(int meetingID)
		{
			SqlParameter[] parameterValues = new SqlParameter[] { new SqlParameter("@MeetingID", meetingID) };
			SqlHelper.ExecuteNonQuery(CommonDAL.ConnectionString, CommandType.StoredProcedure, "MeetingsDelProc", parameterValues);
		}

		public static void LoadFromReader(SqlDataReader reader, MeetingData target)
		{
			if (reader != null && !reader.IsClosed)
			{
				//target.Key = reader.GetInt32(0);
				if (!reader.IsDBNull(0)) target.MeetingID = reader.GetInt32(0);
				if (!reader.IsDBNull(1)) target.MeetingDate = reader.GetDateTime(1);
				if (!reader.IsDBNull(2)) target.ProjectID = reader.GetInt32(2);
				if (!reader.IsDBNull(3)) target.ActivityID = reader.GetInt32(3);
				if (!reader.IsDBNull(4)) target.StartTimeID = reader.GetInt32(4);
				if (!reader.IsDBNull(5)) target.EndTimeID = reader.GetInt32(5);
				if (!reader.IsDBNull(6)) target.Clients = reader.GetString(6);
				if (!reader.IsDBNull(7)) target.Subcontracters = reader.GetString(7);
				if (!reader.IsDBNull(8)) target.Users = reader.GetString(8);
				if (!reader.IsDBNull(9)) target.CreatedByID = reader.GetInt32(9);
				if (!reader.IsDBNull(10)) target.Approved = reader.GetBoolean(10);
				if (!reader.IsDBNull(11)) target.ApprovedID = reader.GetInt32(11);
				if (!reader.IsDBNull(12)) target.ProjectName = reader.GetString(12);
				if (!reader.IsDBNull(13)) target.StartHour = reader.GetString(13);
				if (!reader.IsDBNull(14)) target.EndHour = reader.GetString(14);
				if (!reader.IsDBNull(15)) target.HasScan = reader.GetBoolean(15);
				if (!reader.IsDBNull(16)) target.Notes = reader.GetString(16);
				if (!reader.IsDBNull(17)) target.OtherPeople = reader.GetString(17);
				if (!reader.IsDBNull(18)) target.Place = reader.GetString(18);
				if (!reader.IsDBNull(19)) target.ClientID = reader.GetInt32(19);
				if (!reader.IsDBNull(20)) target.OtherMails = reader.GetString(20);
				if(reader.FieldCount>21)
				{
					if (!reader.IsDBNull(21)) target.ASA = reader.GetBoolean(21);
				}
				if(reader.FieldCount>22)
				{
					if (!reader.IsDBNull(22)) target.PlaceEN = reader.GetString(22);
				}
				
				
			}
		}
		
	  
		public static int Insert( MeetingData source)
		{
			SqlParameter[] parameterValues = GetInsertParameterValues(source);
			SqlHelper.ExecuteNonQuery(CommonDAL.ConnectionString, CommandType.StoredProcedure, "MeetingsInsProc", parameterValues);
			source.MeetingID = (int) parameterValues[0].Value;
			return source.MeetingID;
		}

		public static int Update( MeetingData source)
		{
			SqlParameter[] parameterValues = GetUpdateParameterValues(source);
			SqlHelper.ExecuteNonQuery(CommonDAL.ConnectionString, CommandType.StoredProcedure,
				"MeetingsUpdProc", parameterValues);
			return source.MeetingID;
		}
		

		private static SqlParameter GetSqlParameter(string name, ParameterDirection direction, object value)
		{
			SqlParameter p = new SqlParameter(name, value);
			p.Direction = direction;
			return p;
		}

		private static SqlParameter[] GetInsertParameterValues( MeetingData source)
		{
			SqlParameter[] prams = new SqlParameter[19];
			prams[0] = GetSqlParameter("@MeetingID", ParameterDirection.Output, source.MeetingID);
			prams[1] = GetSqlParameter("@MeetingDate", ParameterDirection.Input, source.MeetingDate);
			prams[2] = GetSqlParameter("@ProjectID", ParameterDirection.Input, source.ProjectID);
			if(source.ApprovedID>0)
				prams[3] = GetSqlParameter("@ActivityID", ParameterDirection.Input, source.ActivityID);
			else
				prams[3] = GetSqlParameter("@ActivityID", ParameterDirection.Input, DBNull.Value);
			prams[4] = GetSqlParameter("@StartTimeID", ParameterDirection.Input, source.StartTimeID);
			prams[5] = GetSqlParameter("@EndTimeID", ParameterDirection.Input, source.EndTimeID);
			prams[6] = GetSqlParameter("@Clients", ParameterDirection.Input, source.Clients);
			prams[7] = GetSqlParameter("@Subcontracters", ParameterDirection.Input, source.Subcontracters);
			prams[8] = GetSqlParameter("@Users", ParameterDirection.Input, source.Users);
			prams[9] = GetSqlParameter("@CreatedByID", ParameterDirection.Input, source.CreatedByID);
			prams[10] = GetSqlParameter("@Approved", ParameterDirection.Input, source.Approved);
			if(source.ApprovedID>0)
				prams[11] = GetSqlParameter("@ApprovedID", ParameterDirection.Input, source.ApprovedID);
			else
				prams[11] = GetSqlParameter("@ApprovedID", ParameterDirection.Input, DBNull.Value);
			prams[12] = GetSqlParameter("@HasScan", ParameterDirection.Input, source.HasScan);
			prams[13] = GetSqlParameter("@Notes", ParameterDirection.Input, source.Notes);
			prams[14] = GetSqlParameter("@OtherPeople", ParameterDirection.Input, source.OtherPeople);
			prams[15] = GetSqlParameter("@Place", ParameterDirection.Input, source.Place);
			prams[16] = GetSqlParameter("@OtherMails", ParameterDirection.Input, source.OtherMails);
			prams[17] = GetSqlParameter("@ASATasks", ParameterDirection.Input, source.ASA);
			prams[18] = GetSqlParameter("@PlaceEN", ParameterDirection.Input, source.PlaceEN);
			return prams;
		}

		private static SqlParameter[] GetUpdateParameterValues( MeetingData source)
		{
			return new SqlParameter[] {
										  new SqlParameter("@MeetingID", source.MeetingID),
										  new SqlParameter("@MeetingDate", source.MeetingDate),
										  new SqlParameter("@ProjectID", source.ProjectID),
										  new SqlParameter("@ActivityID", source.ActivityID),
										  new SqlParameter("@StartTimeID", source.StartTimeID),
										  new SqlParameter("@EndTimeID", source.EndTimeID),
										  new SqlParameter("@Clients", source.Clients),
										  new SqlParameter("@Subcontracters", source.Subcontracters),
										  new SqlParameter("@Users", source.Users),
										  new SqlParameter("@CreatedByID", source.CreatedByID),
										  new SqlParameter("@Approved", source.Approved),
										  new SqlParameter("@ApprovedID", source.ApprovedID),
											new SqlParameter("@HasScan", source.HasScan),
					new SqlParameter("@Notes", source.Notes),
					new SqlParameter("@OtherPeople", source.OtherPeople),
					new SqlParameter("@Place", source.Place),
				new SqlParameter("@OtherMails", source.OtherMails),
				new SqlParameter("@ASATasks", source.ASA),
new SqlParameter("@PlaceEN", source.PlaceEN),
									  };
		}
	}
}   

