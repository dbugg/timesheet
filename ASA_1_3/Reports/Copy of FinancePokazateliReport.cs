using System;
using DataDynamics.ActiveReports;
using DataDynamics.ActiveReports.Document;
using System.Data;

namespace Asa.Timesheet.WebPages.Reports
{
	public class OverTimeReport : ActiveReport
	{
		private bool _pdf;
		private string _createdBy= string.Empty;
	
		public FinancePokazateliReport(DataView dv,bool pdf,string createdBy)
		{
			_pdf=pdf;
			_createdBy = createdBy;
			
			InitializeReport();
			this.DataSource = dv;
			this.ReportEnd += new EventHandler(SubAnalysisRpt1_ReportEnd);
			
		}
		private void ReportFooter_Format(object sender, System.EventArgs eArgs)
		{
			txtCreatedBy.Text = _createdBy;
		}

		private void PageHeader_Format(object sender, System.EventArgs eArgs)
		{
			if(!_pdf)
				if(this.PageNumber >1)
					this.PageHeader.Visible= false;
		}

		#region ActiveReports Designer generated code
		private DataDynamics.ActiveReports.ReportHeader ReportHeader = null;
		private DataDynamics.ActiveReports.TextBox txtHeader = null;
		private DataDynamics.ActiveReports.PageHeader PageHeader = null;
		private DataDynamics.ActiveReports.TextBox txtHeaderSubcontracterSalary = null;
		private DataDynamics.ActiveReports.TextBox txtHeaderImcomePerHour = null;
		private DataDynamics.ActiveReports.TextBox txtHeaderPaymentPerHour = null;
		private DataDynamics.ActiveReports.TextBox txtHeaderExpencesPerHour = null;
		private DataDynamics.ActiveReports.TextBox txtHeaderTotalIncome = null;
		private DataDynamics.ActiveReports.TextBox txtHeaderTotalSalary = null;
		private DataDynamics.ActiveReports.TextBox txtHeaderProjectName = null;
		private DataDynamics.ActiveReports.TextBox txtHeaderAdminSalary = null;
		private DataDynamics.ActiveReports.TextBox txtHeaderWorkedSalary = null;
		private DataDynamics.ActiveReports.TextBox txtHeaderWorkedHours = null;
		private DataDynamics.ActiveReports.TextBox txtHeaderPaymentArea = null;
		private DataDynamics.ActiveReports.TextBox txtHeaderPayments = null;
		private DataDynamics.ActiveReports.TextBox txtHeaderArea = null;
		private DataDynamics.ActiveReports.Detail Detail = null;
		private DataDynamics.ActiveReports.TextBox txtExpencesPerHour = null;
		private DataDynamics.ActiveReports.TextBox txtTotalIncome = null;
		private DataDynamics.ActiveReports.TextBox txtTotalSalary = null;
		private DataDynamics.ActiveReports.TextBox txtSubcontracterSalary = null;
		private DataDynamics.ActiveReports.TextBox txtImcomePerHour = null;
		private DataDynamics.ActiveReports.TextBox txtPaymentPerHour = null;
		private DataDynamics.ActiveReports.TextBox txtProjectName = null;
		private DataDynamics.ActiveReports.TextBox txtWorkedSalary = null;
		private DataDynamics.ActiveReports.TextBox txtAdminSalary = null;
		private DataDynamics.ActiveReports.TextBox txtWorkedHours = null;
		private DataDynamics.ActiveReports.TextBox txtPaymentArea = null;
		private DataDynamics.ActiveReports.TextBox txtPayments = null;
		private DataDynamics.ActiveReports.TextBox txtArea = null;
		private DataDynamics.ActiveReports.TextBox txtProjectID = null;
		private DataDynamics.ActiveReports.Line Line2 = null;
		private DataDynamics.ActiveReports.PageFooter PageFooter = null;
		private DataDynamics.ActiveReports.ReportFooter ReportFooter = null;
		private DataDynamics.ActiveReports.Line Line3 = null;
		private DataDynamics.ActiveReports.Label Label7 = null;
		private DataDynamics.ActiveReports.TextBox txtCreatedBy = null;
		public void InitializeReport()
		{
			this.LoadLayout(this.GetType(), "Asa.Timesheet.WebPages.Reports.FinancePokazateliReport.rpx");
			this.ReportHeader = ((DataDynamics.ActiveReports.ReportHeader)(this.Sections["ReportHeader"]));
			this.PageHeader = ((DataDynamics.ActiveReports.PageHeader)(this.Sections["PageHeader"]));
			this.Detail = ((DataDynamics.ActiveReports.Detail)(this.Sections["Detail"]));
			this.PageFooter = ((DataDynamics.ActiveReports.PageFooter)(this.Sections["PageFooter"]));
			this.ReportFooter = ((DataDynamics.ActiveReports.ReportFooter)(this.Sections["ReportFooter"]));
			this.txtHeader = ((DataDynamics.ActiveReports.TextBox)(this.ReportHeader.Controls[0]));
			this.txtHeaderSubcontracterSalary = ((DataDynamics.ActiveReports.TextBox)(this.PageHeader.Controls[0]));
			this.txtHeaderImcomePerHour = ((DataDynamics.ActiveReports.TextBox)(this.PageHeader.Controls[1]));
			this.txtHeaderPaymentPerHour = ((DataDynamics.ActiveReports.TextBox)(this.PageHeader.Controls[2]));
			this.txtHeaderExpencesPerHour = ((DataDynamics.ActiveReports.TextBox)(this.PageHeader.Controls[3]));
			this.txtHeaderTotalIncome = ((DataDynamics.ActiveReports.TextBox)(this.PageHeader.Controls[4]));
			this.txtHeaderTotalSalary = ((DataDynamics.ActiveReports.TextBox)(this.PageHeader.Controls[5]));
			this.txtHeaderProjectName = ((DataDynamics.ActiveReports.TextBox)(this.PageHeader.Controls[6]));
			this.txtHeaderAdminSalary = ((DataDynamics.ActiveReports.TextBox)(this.PageHeader.Controls[7]));
			this.txtHeaderWorkedSalary = ((DataDynamics.ActiveReports.TextBox)(this.PageHeader.Controls[8]));
			this.txtHeaderWorkedHours = ((DataDynamics.ActiveReports.TextBox)(this.PageHeader.Controls[9]));
			this.txtHeaderPaymentArea = ((DataDynamics.ActiveReports.TextBox)(this.PageHeader.Controls[10]));
			this.txtHeaderPayments = ((DataDynamics.ActiveReports.TextBox)(this.PageHeader.Controls[11]));
			this.txtHeaderArea = ((DataDynamics.ActiveReports.TextBox)(this.PageHeader.Controls[12]));
			this.txtExpencesPerHour = ((DataDynamics.ActiveReports.TextBox)(this.Detail.Controls[0]));
			this.txtTotalIncome = ((DataDynamics.ActiveReports.TextBox)(this.Detail.Controls[1]));
			this.txtTotalSalary = ((DataDynamics.ActiveReports.TextBox)(this.Detail.Controls[2]));
			this.txtSubcontracterSalary = ((DataDynamics.ActiveReports.TextBox)(this.Detail.Controls[3]));
			this.txtImcomePerHour = ((DataDynamics.ActiveReports.TextBox)(this.Detail.Controls[4]));
			this.txtPaymentPerHour = ((DataDynamics.ActiveReports.TextBox)(this.Detail.Controls[5]));
			this.txtProjectName = ((DataDynamics.ActiveReports.TextBox)(this.Detail.Controls[6]));
			this.txtWorkedSalary = ((DataDynamics.ActiveReports.TextBox)(this.Detail.Controls[7]));
			this.txtAdminSalary = ((DataDynamics.ActiveReports.TextBox)(this.Detail.Controls[8]));
			this.txtWorkedHours = ((DataDynamics.ActiveReports.TextBox)(this.Detail.Controls[9]));
			this.txtPaymentArea = ((DataDynamics.ActiveReports.TextBox)(this.Detail.Controls[10]));
			this.txtPayments = ((DataDynamics.ActiveReports.TextBox)(this.Detail.Controls[11]));
			this.txtArea = ((DataDynamics.ActiveReports.TextBox)(this.Detail.Controls[12]));
			this.txtProjectID = ((DataDynamics.ActiveReports.TextBox)(this.Detail.Controls[13]));
			this.Line2 = ((DataDynamics.ActiveReports.Line)(this.Detail.Controls[14]));
			this.Line3 = ((DataDynamics.ActiveReports.Line)(this.ReportFooter.Controls[0]));
			this.Label7 = ((DataDynamics.ActiveReports.Label)(this.ReportFooter.Controls[1]));
			this.txtCreatedBy = ((DataDynamics.ActiveReports.TextBox)(this.ReportFooter.Controls[2]));
			// Attach Report Events
			this.ReportFooter.Format += new System.EventHandler(this.ReportFooter_Format);
			this.PageHeader.Format += new System.EventHandler(this.PageHeader_Format);
		}

		#endregion

		private void SubAnalysisRpt1_ReportEnd(object sender, EventArgs e)
		{
			if (!this._pdf) return;

			Logo1 logo = new Logo1();
			logo.Run();

			for (int i=0; i<this.Document.Pages.Count; i++)
				this.Document.Pages[i].Overlay(logo.Document.Pages[0]);
		}
	}
}
