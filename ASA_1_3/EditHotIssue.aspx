﻿<%@ Register TagPrefix="uc1" TagName="PageHeader" Src="UserControls/PageHeader.ascx" %>
<%@ Register TagPrefix="uc1" TagName="EditForm" Src="UserControls/EditForm.ascx" %>
<%@ Page language="c#" Codebehind="EditHotIssue.aspx.cs" AutoEventWireup="false" Inherits="Asa.Timesheet.WebPages.EditHotIssue" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
	<head runat="server">
		<title>EditHotIssue</title>
		<meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
		<meta content="C#" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<LINK href="styles/timesheet.css" type="text/css" rel="stylesheet">
		<script language="javascript" src="PopupCalendar.js"></script>
	</HEAD>
	<body bottomMargin="0" leftMargin="0" topMargin="0" rightMargin="0">
		<form id="Form1" method="post" runat="server">
			<table height="100%" cellSpacing="0" cellPadding="0" width="100%" border="0">
				<tr height="1">
					<td><uc1:pageheader id="header" runat="server"></uc1:pageheader><uc1:editform id="editCtrl" runat="server"></uc1:editform></td>
				</tr>
				<tr>
					<td>
						<table height="100%" cellSpacing="0" cellPadding="0" width="100%" border="0">
							<tr>
								<td class="menuLeft"><asp:placeholder id="menuHolder" runat="server"></asp:placeholder></td>
								
								<td style="PADDING-LEFT: 5px; PADDING-TOP: 3px" vAlign="top" noWrap>
									<TABLE id="Table3" cellSpacing="0" cellPadding="0" width="100%" border="0">
										<TR>
											<td width="1%"></TD>
											<td vAlign="top" width="476" style="WIDTH: 476px">
												<TABLE id="tblForm" cellSpacing="0" cellPadding="3" width="100%" border="0">
													<TBODY>
                                                    <TR>

														<td><asp:linkbutton id="linkBack" Runat="server" ><i class="fa fa-chevron-left fa-2x" id="backBtn"></i>Назад</asp:linkbutton>
                                                        <asp:button id="btnEdit" style="DISPLAY: none" runat="server" EnableViewState="False" CssClass="ActionButton"
																Text="Редактирай"></asp:button>
                                                           
															<asp:button id="Button1" runat="server" EnableViewState="False" CssClass="ActionButton"
																Text="Нова Задача"></asp:button></TD>
														<td style="WIDTH: 93px"><asp:button id="btnDelete" runat="server" Visible="False" EnableViewState="False" CssClass="ActionButton"
																Text="Изтрий"></asp:button>
                                                                 <asp:button id="btnSave" runat="server" EnableViewState="False" CssClass="ActionButton" Text="Запиши" style="float: right"></asp:button></TD>
													</TR>
                                                        <TR>
														<td>
                                                            

													    </TR>
                                                        <TR>
															<td colspan="2"><label for="ckStop" class="enterDataLabel" style="color: red; float: left; width: auto">Поради липса на решение сроковете по изпълнение на договора спират</label>
                                                                <asp:checkbox id="ckStop" runat="server" CssClass="EnterDataBox" AutoPostBack="True" style="border: red; margin-top: 10px"></asp:checkbox>
																<asp:label id="lbDate" runat="server" CssClass="enterDataBox"></asp:label></TD>
														</TR>
														<TR>

															<td style="WIDTH: 154px">
                                                                <asp:label id="Label1" runat="server" Visible="True" EnableViewState="False" CssClass="enterDataLabel" Width="100%">Тип:</asp:label>
                                                                <asp:dropdownlist id="ddlProjectsStatus" runat="server" CssClass="EnterDataBox" Width="280px" AutoPostBack="True"></asp:dropdownlist></TD>
															<td style="WIDTH: 154px"><asp:label id="Label8" runat="server" CssClass="enterDataLabel" Width="100%">Сграда:</asp:label>
															    <asp:dropdownlist id="ddlBuildingTypes" runat="server" CssClass="enterDataBox" Width="280px" AutoPostBack="True"></asp:dropdownlist></TD>
														</TR>
														<TR>
															<td><asp:label id="lbProjectName" runat="server" Visible="True" EnableViewState="False" CssClass="enterDataLabel" >Проект:</asp:label>
                                                               <asp:label id="txtProjectNameText" runat="server" CssClass="enterDataBox" Width="90%"></asp:label><asp:dropdownlist id="ddlProject" runat="server" CssClass="EnterDataBox" Width="280px" AutoPostBack="True"></asp:dropdownlist></TD>

															<td>
																<asp:label id="Label14" runat="server" Font-Bold="True" CssClass="enterDataLabel" Width="100%">Стандартни теми:</asp:label>
																<asp:dropdownlist id="ddlStandardCat" runat="server" CssClass="EnterDataBox" Width="280px"></asp:dropdownlist></TD>
														</TR>
														<TR>
															<td><asp:label id="lbHotIssueCategory" runat="server" CssClass="enterDataLabel" Width="100%" Font-Bold="True">Тема:</asp:label>
                                                                <asp:dropdownlist id="ddlHotIssueCategory" runat="server" CssClass="EnterDataBox" Width="280px"></asp:dropdownlist><asp:label id="lbCategorySet" CssClass="EnterDataBox" Width="90%" Runat="server"></asp:label></TD>

															<td>
																<asp:label id="Label11" runat="server" Font-Bold="True" CssClass="enterDataLabel" Width="100%">Нова тема:</asp:label>
                                                                <asp:textbox id="txtCategoryNew" CssClass="EnterDataBox" Width="280px" Runat="server"></asp:textbox></TD>
														</TR>
														<TR>
															<td colspan="2"><asp:label id="lbHotIssueIme" runat="server" CssClass="enterDataLabel" Width="100%" Font-Bold="True">Задача:</asp:label>
                                                                <asp:label id="lbHotIssueImeText" runat="server" CssClass="enterDataBox" Width="90%"></asp:label><asp:textbox id="txtHotIssueIme" runat="server" CssClass="enterDataBox" Width="280px" TextMode="MultiLine"
																	Height="56px"></asp:textbox>
															</TD>
														</TR>
														<TR>
															<td colspan="2"><asp:label id="lbDiscription" runat="server" CssClass="enterDataLabel" Width="100%">Описание:</asp:label>
                                                                <asp:textbox id="txtDiscription" runat="server" CssClass="enterDataBox" Width="280px" TextMode="MultiLine"
																	Height="96px" Rows="5"></asp:textbox><asp:label id="lbDiscriptionText" runat="server" CssClass="enterDataBox" Width="90%"></asp:label></TD>
														</TR>
														<TR>
															<td colspan="2"><asp:label id="lbComment" runat="server" CssClass="enterDataLabel" Width="100%">Коментар:</asp:label>
                                                                <asp:textbox id="txtComment" runat="server" CssClass="enterDataBox" Width="280px" TextMode="MultiLine"
																	Height="96px" Rows="5" MaxLength="10"></asp:textbox></TD>
														</TR>
														<TR>
															<td><asp:label id="lbAssigned" runat="server" CssClass="enterDataLabel" Width="100%">Възложено на:</asp:label>
                                                                <asp:dropdownlist id="ddlAssigned" runat="server" CssClass="EnterDataBox" Width="280px"></asp:dropdownlist>&nbsp;</TD>

															<td ><asp:label id="Label7" runat="server" CssClass="enterDataLabel" Width="100%">Възложено на(още):</asp:label>
                                                                <asp:textbox id="txtAssignedToText" runat="server" CssClass="enterDataBox" Width="280px" Rows="5"></asp:textbox></TD>
														</TR>
														<TR>

															<td><asp:label id="lbCreated" runat="server" CssClass="enterDataLabel" Width="100%">Дата на създаване:</asp:label>
                                                                <asp:textbox id="txtCreated" runat="server" CssClass="enterDataBox" Style="width: 88px !important" MaxLength="10"></asp:textbox>&nbsp;
																<IMG id="lkDateCreated" alt="Calendar" src="images/calendar.ico" width="18" align="absBottom"
																	runat="server">
																<asp:label id="Label15" runat="server" Visible="False"></asp:label></TD>

															<td><asp:label id="lbDeadline" runat="server" CssClass="enterDataLabel" Width="100%">Краен срок:</asp:label>
                                                                <asp:textbox id="txtDeadline" runat="server" CssClass="enterDataBox" Style="width: 88px !important" MaxLength="10"></asp:textbox>&nbsp;
																<IMG id="lkCalendar1" alt="Calendar" src="images/calendar.ico" width="18" align="absBottom"
																	runat="server">&nbsp;
																<asp:label id="lbMainID" runat="server" Visible="False"></asp:label></TD>
														</TR>
														<TR>
															<td><asp:label id="lbStatus" runat="server" CssClass="enterDataLabel" Width="100%" Font-Bold="True">Статус:</asp:label>
                                                                <asp:dropdownlist id="ddlStatus" runat="server" CssClass="EnterDataBox" Width="280px"></asp:dropdownlist>
                                                                <asp:label id="lbAssignedBy" runat="server" CssClass="enterDataLabel" Width="100%" Font-Bold="True">Възложено от:</asp:label>
                                                                <asp:label id="lbBy" runat="server" CssClass="enterDataBox" Width="90%"></asp:label></td>
                                                            <td>
                                                                <label for="cbToClient" class="enterDataLabel" Font-Bold="True">Към възложителя:</asp:label>
                                                                    <asp:checkbox id="cbToClient" runat="server" CssClass="EnterDataBox"></asp:checkbox>
                                                                </TD>
															
														</TR>

													</TBODY>
												</TABLE>
											</TD>
											<td vAlign="top" width="50%"><asp:datalist id="dlOldIssues" runat="server" Width="100%" RepeatColumns="1" RepeatDirection="Horizontal">
													<EditItemStyle VerticalAlign="Top"></EditItemStyle>
													<ItemStyle VerticalAlign="Top"></ItemStyle>
													<ItemTemplate>
														<TABLE cellSpacing="0" cellPadding="3" width="100%" border="0">
															<TR>
																<td style="WIDTH: 168px">
																	<asp:label id="Label4" runat="server" CssClass="enterDataLabel" Width="100%">Дата на промяна:</asp:label></TD>
																<td>
																	<asp:label id="DLlbCreatedDate" runat="server" CssClass="enterDataBox" Width="100%" text='<%# DataBinder.Eval(Container, "DataItem.CreatedDate") %>'>
																	</asp:label></TD>
															</TR>
															<TR>
																<td style="WIDTH: 168px">
																	<asp:label id="Label3" runat="server" Width="100%" CssClass="enterDataLabel">Коментар:</asp:label></TD>
																<td>
																	<asp:label id="DLlbComment" runat="server" CssClass="enterDataBox" Width="100%" text='<%# DataBinder.Eval(Container, "DataItem.HotIssueComment") %>'>
																	</asp:label></TD>
															</TR>
															<TR>
																<td style="WIDTH: 168px">
																	<asp:label id="Label9" runat="server" CssClass="enterDataLabel" Width="100%">Възложено на:</asp:label></TD>
																<td>
																	<asp:label id="DLlbAssignedTo" runat="server" CssClass="enterDataBox" Width="100%" Text='<%# AssignedToText(DataBinder.Eval(Container, "DataItem.AssignedTo"),DataBinder.Eval(Container, "DataItem.AssignedToType"),DataBinder.Eval(Container, "DataItem.AssignedToMore")) %>'>
																	</asp:label></TD>
															</TR>
															<TR>
																<td style="WIDTH: 168px">
																	<asp:label id="Label10" runat="server" CssClass="enterDataLabel" Width="100%">Краен срок:</asp:label></TD>
																<td>
																	<asp:label id="DLlbDeadline" runat="server" CssClass="enterDataBox" Width="100%" text='<%# Asa.Timesheet.Data.Util.TimeHelper.FormatDateO(DataBinder.Eval(Container, "DataItem.HotIssueDeadline")) %>'>
																	</asp:label></TD>
															</TR>
															<TR>
																<td style="WIDTH: 168px">
																	<asp:label id="Label13" runat="server" CssClass="enterDataLabel" Width="100%">Статус:</asp:label></TD>
																<td>
																	<asp:label id="DLlbStatus" runat="server" CssClass="enterDataBox" Width="100%" text='<%# DataBinder.Eval(Container, "DataItem.HotIssueStatusName") %>'>
																	</asp:label></TD>
															</TR>
															<TR>
																<td style="WIDTH: 168px">
																	<asp:label id="Label5" runat="server" CssClass="enterDataLabel" Width="100%">Спрян е срока на изпълнение:</asp:label></TD>
																<td>
																	<asp:label id="Label6" runat="server" CssClass="enterDataBox" Width="100%" text='<%# FormatDate(DataBinder.Eval(Container, "DataItem.ExecutionStopped")) %>'>
																	</asp:label></TD>
															</TR>
														</TABLE>
													</ItemTemplate>
												</asp:datalist></TD>
										</TR>
									</TABLE>
									<br>
									&nbsp;
									<asp:label id="lblInfo" runat="server" EnableViewState="False" CssClass="InfoLabel"></asp:label><asp:label id="lblError" runat="server" EnableViewState="False" CssClass="ErrorLabel" ForeColor="Red"></asp:label></td>
							</tr>
						</table>
					</td>
				</tr>
			</table>
		</form>
	</body>
</HTML>
