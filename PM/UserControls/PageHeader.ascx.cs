namespace PM.UserControls
{
	using System;
	using System.Data;
	using System.Drawing;
	using System.Web;
	using System.Web.UI.WebControls;
	using System.Web.UI.HtmlControls;
	using Asa.Timesheet.Data;

	/// <summary>
	///		Summary description for PageHeader.
	/// </summary>
	public partial class PageHeader : System.Web.UI.UserControl
	{
		protected System.Web.UI.WebControls.LinkButton lbLogOut;
        
		
		public delegate void EditHandler(object sender, System.EventArgs e);
		public event EditHandler EditClicked;

        protected virtual void FireEditClicked(object sender, System.EventArgs e)
        {
            if (EditClicked != null)
            {
                EditClicked(this, e);
            }

        }

        protected void Page_Load(object sender, System.EventArgs e)
        {
            LoadResourses();
        }

        private void LoadResourses()
        {
            string lang = (Session["lang"] == null) ? Languages.BG.ToString() : (string)Session["lang"];
            if (lang == "BG")
            {
                btnBG.Visible = false;
                btnEN.Visible = true;
            }
            if (lang == "EN")
            {
                btnBG.Visible = true;
                btnEN.Visible = false;
            }
            HyperLink2.Text = UIHelpers.GetText(lang, "Name_QuestionAboutSystemHeader");
            btnLogOut.Text = UIHelpers.GetText(lang, "Name_btnExitHeader");
            lkHelp.Text = UIHelpers.GetText(lang, "Name_Help");
            if (SessionManager.LoggedUserInfoPM.Account == null || SessionManager.LoggedUserInfoPM.Account == "")
            {
                btnLogOut.Visible = false;
                menuHolder.Visible = false;
                lkHelp.Visible = false;
            }
            else
            {
                if (SessionManager.LoggedUserInfoPM.IsUser)
                    lkHelp.NavigateUrl = "../Help/Help1.html";
                else if (SessionManager.LoggedUserInfoPM.IsSubcontracter)
                    lkHelp.NavigateUrl = "../Help/Help2.html";
                else
                    lkHelp.NavigateUrl = "../Help/Help3.html";
                UIHelpers.CreateMenu(menuHolder, SessionManager.LoggedUserInfoPM, lang);
            }

        }

	
		#region Web Form Designer generated code
        override protected void OnInit(EventArgs e)
        {
            //
            // CODEGEN: This call is required by the ASP.NET Web Form Designer.
            //
            InitializeComponent();
            base.OnInit(e);
        }
		
		/// <summary>
		///		Required method for Designer support - do not modify
		///		the contents of this method with the code editor.
		/// </summary>
        private void InitializeComponent()
        {

        }
		#endregion

        private void lbLogOut_Click(object sender, System.EventArgs e)
        {
            System.Web.Security.FormsAuthentication.SignOut();
            Session.Abandon();
            Response.Redirect(Request.ApplicationPath + "/Login.aspx");
        }

        protected void btnLogOut_Click(object sender, System.EventArgs e)
        {
            System.Web.Security.FormsAuthentication.SignOut();
            Session.Abandon();
            Response.Redirect(Request.ApplicationPath);//+"/hours.aspx");//"/Login.aspx");
        }

        private void btnEdit_Click(object sender, System.EventArgs e)
        {
            btnEdit.Visible = !btnEdit.Visible;
            FireEditClicked(this, e);
        }

        protected void btnBG_Click(object sender, System.EventArgs e)
        {
            ChangeLanguage(Languages.BG.ToString());
        }

        protected void btnEN_Click(object sender, System.EventArgs e)
        {
            ChangeLanguage(Languages.EN.ToString());
        }

        private void ChangeLanguage(string lang)
        {
            Session["lang"] = lang;
            string s = Request.PhysicalPath;
            int index = s.LastIndexOf(@"\");
            if (index != -1)
            {
                s = string.Concat(s, setParams());
                s = s.Substring(index + 1);
                Response.Redirect(s);
            }
        }
        private string setParams()
        {
            string returnValue = "";
            if (UIHelpers.GetIDParam() != -1)
            {
                returnValue = string.Concat("?id=", UIHelpers.GetIDParam());
            }
            if (UIHelpers.GetCIDParam() != -1)
            {
                if (returnValue.Length > 0)
                    returnValue = string.Concat(returnValue, "&cid=", UIHelpers.GetCIDParam());
                else
                    returnValue = string.Concat("?cid=", UIHelpers.GetCIDParam());
            }
            if (UIHelpers.GetPIDParam() != -1)
            {
                if (returnValue.Length > 0)
                    returnValue = string.Concat(returnValue, "&pid=", UIHelpers.GetPIDParam());
                else
                    returnValue = string.Concat("?pid=", UIHelpers.GetPIDParam());
            }
            return returnValue;
        }


		public bool EditMode
		{
			set { btnEdit.Visible = !value; }
			get {return !btnEdit.Visible;}

		}
		public string PageTitle
		{
			get { return lblTitle.Text; }
			set { lblTitle.Text = value; }
		}

		public string UserName
		{
			set { lblUser.Text = value; }
		}
		
	}
}
