using System;
using System.Data;
using GrapeCity.ActiveReports;
using GrapeCity.ActiveReports.Controls;
using GrapeCity.ActiveReports.SectionReportModel;
using GrapeCity.ActiveReports.Document.Section;
using GrapeCity.ActiveReports.Document;

namespace Asa.Timesheet.WebPages.Reports
{
    public class ClientRpt : GrapeCity.ActiveReports.SectionReport
	{
        public ClientRpt(bool pdf, string title, string type, string project, string client, DataView dv1, DataView dv2)
        {
            _forPDF = pdf;
            InitializeComponent();
            lbTitle.Text = title;
            if (type.Length > 0 && !type.StartsWith("<"))
                lbZad.Text = type;
            else
                Label4.Visible = false;
            if (project.Length > 0 && !project.StartsWith("<"))
                lbProject.Text = project;
            else
                Label5.Visible = false;
            if (client.Length > 0 && !client.StartsWith("<"))
                lbClient.Text = client;
            else
                Label6.Visible = false;
            this.ReportEnd += new EventHandler(SubAnalysisRpt_ReportEnd);
            if (dv1.Count > 0)
                this.SubReport1.Report = new ClientSubreport1(dv1);
            if (dv2.Count > 0)
                this.SubReport2.Report = new ClientSubreport2(dv2);
            else lbPh.Visible = false;

        }
		private bool _forPDF=true;
		#region ActiveReports Designer generated code













        public void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ClientRpt));
            this.Detail = new GrapeCity.ActiveReports.SectionReportModel.Detail();
            this.ReportHeader = new GrapeCity.ActiveReports.SectionReportModel.ReportHeader();
            this.ReportFooter = new GrapeCity.ActiveReports.SectionReportModel.ReportFooter();
            this.lbTitle = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label4 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label5 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label6 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lbZad = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lbProject = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lbClient = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.SubReport1 = new GrapeCity.ActiveReports.SectionReportModel.SubReport();
            this.lbPh = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.SubReport2 = new GrapeCity.ActiveReports.SectionReportModel.SubReport();
            ((System.ComponentModel.ISupportInitialize)(this.lbTitle)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbZad)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbProject)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbClient)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbPh)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            // 
            // Detail
            // 
            this.Detail.ColumnSpacing = 0F;
            this.Detail.Height = 0F;
            this.Detail.Name = "Detail";
            // 
            // ReportHeader
            // 
            this.ReportHeader.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
						this.lbTitle,
						this.Label4,
						this.Label5,
						this.Label6,
						this.lbZad,
						this.lbProject,
						this.lbClient,
						this.SubReport1,
						this.lbPh,
						this.SubReport2});
            this.ReportHeader.Height = 1.790972F;
            this.ReportHeader.Name = "ReportHeader";
            // 
            // ReportFooter
            // 
            this.ReportFooter.Height = 0F;
            this.ReportFooter.Name = "ReportFooter";
            // 
            // lbTitle
            // 
            this.lbTitle.Height = 0.2F;
            this.lbTitle.HyperLink = null;
            this.lbTitle.Left = 1F;
            this.lbTitle.Name = "lbTitle";
            this.lbTitle.Style = "font-size: 12pt; font-weight: bold; text-align: center";
            this.lbTitle.Text = "Label4";
            this.lbTitle.Top = 0F;
            this.lbTitle.Width = 9.125F;
            // 
            // Label4
            // 
            this.Label4.Height = 0.2F;
            this.Label4.HyperLink = null;
            this.Label4.Left = 1F;
            this.Label4.Name = "Label4";
            this.Label4.Style = "font-weight: bold";
            this.Label4.Text = "??????????:";
            this.Label4.Top = 0.4375F;
            this.Label4.Width = 2.5F;
            // 
            // Label5
            // 
            this.Label5.Height = 0.2F;
            this.Label5.HyperLink = null;
            this.Label5.Left = 1F;
            this.Label5.Name = "Label5";
            this.Label5.Style = "font-weight: bold";
            this.Label5.Text = "??????:";
            this.Label5.Top = 0.625F;
            this.Label5.Width = 2.5F;
            // 
            // Label6
            // 
            this.Label6.Height = 0.2000001F;
            this.Label6.HyperLink = null;
            this.Label6.Left = 1F;
            this.Label6.Name = "Label6";
            this.Label6.Style = "font-weight: bold";
            this.Label6.Text = "??????:";
            this.Label6.Top = 0.8125F;
            this.Label6.Width = 2.5F;
            // 
            // lbZad
            // 
            this.lbZad.Height = 0.2F;
            this.lbZad.HyperLink = null;
            this.lbZad.Left = 3.5F;
            this.lbZad.Name = "lbZad";
            this.lbZad.Style = "";
            this.lbZad.Text = "";
            this.lbZad.Top = 0.4375F;
            this.lbZad.Width = 6.625F;
            // 
            // lbProject
            // 
            this.lbProject.Height = 0.2000001F;
            this.lbProject.HyperLink = null;
            this.lbProject.Left = 3.5F;
            this.lbProject.Name = "lbProject";
            this.lbProject.Style = "";
            this.lbProject.Text = "";
            this.lbProject.Top = 0.625F;
            this.lbProject.Width = 6.625F;
            // 
            // lbClient
            // 
            this.lbClient.Height = 0.2F;
            this.lbClient.HyperLink = null;
            this.lbClient.Left = 3.5F;
            this.lbClient.Name = "lbClient";
            this.lbClient.Style = "";
            this.lbClient.Text = "";
            this.lbClient.Top = 0.8125F;
            this.lbClient.Width = 6.625F;
            // 
            // SubReport1
            // 
            this.SubReport1.CloseBorder = false;
            this.SubReport1.Height = 0.1875F;
            this.SubReport1.Left = 1F;
            this.SubReport1.Name = "SubReport1";
            this.SubReport1.Report = null;
            this.SubReport1.Top = 1.125F;
            this.SubReport1.Width = 9.125F;
            // 
            // lbPh
            // 
            this.lbPh.Height = 0.2000001F;
            this.lbPh.HyperLink = null;
            this.lbPh.Left = 1F;
            this.lbPh.Name = "lbPh";
            this.lbPh.Style = "font-weight: bold";
            this.lbPh.Text = "??????? ? ???????? ?? ????:";
            this.lbPh.Top = 1.375F;
            this.lbPh.Width = 9.125F;
            // 
            // SubReport2
            // 
            this.SubReport2.CloseBorder = false;
            this.SubReport2.Height = 0.1875F;
            this.SubReport2.Left = 1F;
            this.SubReport2.Name = "SubReport2";
            this.SubReport2.Report = null;
            this.SubReport2.Top = 1.5625F;
            this.SubReport2.Width = 9.125F;
            // 
            // ActiveReport1
            // 
            this.MasterReport = false;
            this.PageSettings.PaperHeight = 11F;
            this.PageSettings.PaperWidth = 8.5F;
            this.PrintWidth = 10.125F;
            this.Sections.Add(this.ReportHeader);
            this.Sections.Add(this.Detail);
            this.Sections.Add(this.ReportFooter);
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule(resources.GetString("$this.StyleSheet"), "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: inherit; font-style: inherit; font-variant: inherit; font-weight: bo" +
                        "ld; font-size: 16pt; font-size-adjust: inherit; font-stretch: inherit", "Heading1", "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: Times New Roman; font-style: italic; font-variant: inherit; font-wei" +
                        "ght: bold; font-size: 14pt; font-size-adjust: inherit; font-stretch: inherit", "Heading2", "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: inherit; font-style: inherit; font-variant: inherit; font-weight: bo" +
                        "ld; font-size: 13pt; font-size-adjust: inherit; font-stretch: inherit", "Heading3", "Normal"));
            ((System.ComponentModel.ISupportInitialize)(this.lbTitle)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbZad)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbProject)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbClient)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbPh)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();
        }

		#endregion

        private void SubAnalysisRpt_ReportEnd(object sender, EventArgs e)
        {
            if (!this._forPDF) return;

            Logo1 logo = new Logo1();
            logo.Run();

            for (int i = 0; i < this.Document.Pages.Count; i++)
                this.Document.Pages[i].Overlay(logo.Document.Pages[0]);
        }

        private void SubAnalysisRpt_ReportStart(object sender, EventArgs e)
        {
            //			if (!_forPDF)
            //			{
            //				this.PageSettings.Margins.Left = CmToInch(2f);
            //				this.PrintWidth = CmToInch(17f);
            //				Label1.Location = new System.Drawing.PointF(0, Label1.Location.Y);
            //				lbTitle
            //				// lblObekt.Width = CmToInch(6f);
            ////				txtObekt.Location = new System.Drawing.PointF(lblObekt.Location.X+lblObekt.Width, txtObekt.Location.Y);
            ////				txtAddress.Location = new System.Drawing.PointF(txtObekt.Location.X, txtAddress.Location.Y);
            ////
            ////				this.SubReport1.Location = new System.Drawing.PointF(0, this.SubReport1.Location.Y);
            //		}
        }

        private ReportHeader ReportHeader;
        private Label lbTitle;
        private Label Label4;
        private Label Label5;
        private Label Label6;
        private Label lbZad;
        private Label lbProject;
        private Label lbClient;
        private SubReport SubReport1;
        private Label lbPh;
        private SubReport SubReport2;
        private Detail Detail;
        private ReportFooter ReportFooter;
	}
}
