using System;
using Asa.Timesheet.Data;
using GrapeCity.ActiveReports;
using GrapeCity.ActiveReports.Controls;
using GrapeCity.ActiveReports.SectionReportModel;
using GrapeCity.ActiveReports.Document.Section;
using GrapeCity.ActiveReports.Document;

namespace Asa.Timesheet.WebPages.Reports
{
    public class SubactivitiesSubreport : GrapeCity.ActiveReports.SectionReport
	{
        public SubactivitiesSubreport()
        {
            InitializeComponent();
        }

        private void Detail_Format(object sender, System.EventArgs eArgs)
        {

        }

        private void SubactivitiesSubreport_FetchData(object sender, FetchEventArgs eArgs)
        {
            this.Fields.Add("Status");

            bool done = false;
            try
            {
                done = (bool)this.Fields["Done"].Value;
            }
            catch
            { }

            this.Fields["Status"].Value = done ? Resource.ResourceManager["Reports_Yes"]
                : Resource.ResourceManager["Reports_No"];
        }

		#region ActiveReports Designer generated code



        public void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(SubactivitiesSubreport));
            this.Detail = new GrapeCity.ActiveReports.SectionReportModel.Detail();
            this.txtSubactivity = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtStatus = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            ((System.ComponentModel.ISupportInitialize)(this.txtSubactivity)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtStatus)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            // 
            // Detail
            // 
            this.Detail.ColumnSpacing = 0F;
            this.Detail.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
						this.txtSubactivity,
						this.txtStatus});
            this.Detail.Height = 0.2034722F;
            this.Detail.Name = "Detail";
            // 
            // txtSubactivity
            // 
            this.txtSubactivity.DataField = "Subactivity";
            this.txtSubactivity.Height = 0.2F;
            this.txtSubactivity.Left = 0F;
            this.txtSubactivity.Name = "txtSubactivity";
            this.txtSubactivity.Style = "font-weight: bold";
            this.txtSubactivity.Text = "txtSubactivity";
            this.txtSubactivity.Top = 0F;
            this.txtSubactivity.Width = 3.543307F;
            // 
            // txtStatus
            // 
            this.txtStatus.DataField = "Status";
            this.txtStatus.Height = 0.2F;
            this.txtStatus.Left = 3.617126F;
            this.txtStatus.Name = "txtStatus";
            this.txtStatus.Text = "txtStatus";
            this.txtStatus.Top = 0F;
            this.txtStatus.Width = 1.181102F;
            // 
            // ActiveReport1
            // 
            this.MasterReport = false;
            this.PageSettings.Margins.Bottom = 0F;
            this.PageSettings.Margins.Left = 0F;
            this.PageSettings.Margins.Right = 0F;
            this.PageSettings.Margins.Top = 0F;
            this.PageSettings.PaperHeight = 11F;
            this.PageSettings.PaperWidth = 8.5F;
            this.PrintWidth = 4.72441F;
            this.Sections.Add(this.Detail);
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule(resources.GetString("$this.StyleSheet"), "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: inherit; font-style: inherit; font-variant: inherit; font-weight: bo" +
                        "ld; font-size: 16pt; font-size-adjust: inherit; font-stretch: inherit", "Heading1", "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: Times New Roman; font-style: italic; font-variant: inherit; font-wei" +
                        "ght: bold; font-size: 14pt; font-size-adjust: inherit; font-stretch: inherit", "Heading2", "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: inherit; font-style: inherit; font-variant: inherit; font-weight: bo" +
                        "ld; font-size: 13pt; font-size-adjust: inherit; font-stretch: inherit", "Heading3", "Normal"));
            ((System.ComponentModel.ISupportInitialize)(this.txtSubactivity)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtStatus)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();

            // Attach Report Events
            this.Detail.Format += new System.EventHandler(this.Detail_Format);
            this.FetchData += new GrapeCity.ActiveReports.SectionReport.FetchEventHandler(this.SubactivitiesSubreport_FetchData);
        }

		#endregion

        private Detail Detail;
        private TextBox txtSubactivity;
        private TextBox txtStatus;
	}
}
