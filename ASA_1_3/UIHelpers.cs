using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Data.SqlClient;
using Asa.Timesheet.Data;
using Asa.Timesheet.Data.Entities;
using log4net;
using Asa.Timesheet.WebPages.UserControls;
using Asa.Timesheet.Data.Util;
using System.Globalization;
using System.Text;
using System.Configuration;
using System.Reflection;
using System.Text.RegularExpressions;

namespace Asa.Timesheet.WebPages
{
	/// <summary>
	/// Summary description for UIHelpers.
	/// </summary>
	public class UIHelpers
	{
		private static readonly ILog log = LogManager.GetLogger(typeof(UIHelpers));
		
		public static readonly int N_TEXT=10;
		public static readonly int N_FIXED=250;
		public static decimal[] payments  = new decimal[]{0.3M, 0.3M,0.3M,0.1M};

		public static DataTable CreateGridSource(DataTable table, out decimal TotalMinutes)
		{
			DataTable dt = new DataTable();
			dt.Columns.Add(new DataColumn("ActivityID", System.Type.GetType("System.Int32")));
			dt.Columns.Add(new DataColumn("ActivityName", System.Type.GetType("System.String")));
			dt.Columns.Add(new DataColumn("UserID", System.Type.GetType("System.Int32")));
			dt.Columns.Add(new DataColumn("UserName", System.Type.GetType("System.String")));
			dt.Columns.Add(new DataColumn("WorkDate", System.Type.GetType("System.DateTime")));
			dt.Columns.Add(new DataColumn("StartHour", System.Type.GetType("System.String")));
			dt.Columns.Add(new DataColumn("EndHour", System.Type.GetType("System.String")));
			dt.Columns.Add(new DataColumn("Minutes", System.Type.GetType("System.String")));
			dt.Columns.Add(new DataColumn("AdminMinutes", System.Type.GetType("System.String")));
			dt.Columns.Add(new DataColumn("Time", System.Type.GetType("System.Int32")));
			dt.Columns.Add(new DataColumn("RowType", System.Type.GetType("System.Int32")));

			table.Columns.Add("Time", System.Type.GetType("System.Int32"));
			table.Columns.Add("RowType", System.Type.GetType("System.Int32"));
			
			
			int aid = (int)table.Rows[0]["ActivityID"];
			int uid = (int)table.Rows[0]["UserID"];

			int userTotal = 0, activityTotal = 0, projectTotal = 0;
			
			DataRow dr;
			
			dr = dt.NewRow();
			dr["RowType"] = 6;
			dt.Rows.Add(dr);
			
			if (aid>0)
			{
				dr = dt.NewRow();
				dr["ActivityName"] = table.Rows[0]["ActivityName"];
				dr["RowType"] = 3;
				dt.Rows.Add(dr);
			}

			for (int i = 0; i<table.Rows.Count; i++)
			{
				int stMinutes = TimeHelper.TimeInMinutes((string)table.Rows[i]["StartHour"]);
				int etMinutes = TimeHelper.TimeInMinutes((string)table.Rows[i]["EndHour"]); 
				if ((stMinutes < 0) || (etMinutes < 0)) { stMinutes = 0; etMinutes = 0; }
				table.Rows[i]["Time"] = etMinutes - stMinutes;
											
				table.Rows[i]["RowType"] = 0;
				
				DataRow currentRow = table.Rows[i];
				
				if ((int)currentRow["ActivityID"]!=aid)
				{
					//user total
					dr = dt.NewRow();
					//	dr["UserID"] = uid;
					//	dr["ActivityID"] = aid;
					dr["UserName"] = table.Rows[i-1]["UserName"];
					dr["Time"] = userTotal;
					dr["RowType"] = 1;
					dt.Rows.Add(dr);
					

					dr = dt.NewRow();
					dr["UserID"] = 0;
					dr["ActivityID"] = aid;
					dr["ActivityName"] = table.Rows[i-1]["ActivityName"];
					dr["Time"] = activityTotal;
					dr["RowType"] = 2;
					
					dt.Rows.Add(dr);	
					
					// reset totals
					userTotal = (int)currentRow["Time"];
					activityTotal = userTotal;
					aid = (int)currentRow["ActivityID"];
					uid = (int)currentRow["UserID"];

					//add activity header rows
					dr = dt.NewRow();
					dr["RowType"] = 6;
					dt.Rows.Add(dr);

					dr = dt.NewRow();
					dr["ActivityName"] = table.Rows[i]["ActivityName"];
					dr["RowType"] = 3;
					dt.Rows.Add(dr);
				}
				else
				{
					activityTotal += (int)currentRow["Time"];
					
					if (uid!=(int)currentRow["UserID"])
					{
						dr = dt.NewRow();
						dr["UserID"] = uid;
						dr["UserName"] = table.Rows[i-1]["UserName"];
						dr["Time"] = userTotal;

						dt.Rows.Add(dr);
						dr["RowType"] = 1;
						
						uid = (int)currentRow["UserID"];
						userTotal = (int)currentRow["Time"];
					}
					else
					{
						userTotal+=(int)currentRow["Time"];
						//	dr["UserName"] = "";
					}
				}

				projectTotal+=(int)table.Rows[i]["Time"];
				dr = dt.NewRow();
				dr.ItemArray = table.Rows[i].ItemArray;
				dt.Rows.Add(dr);

			}

			string userName = table.Rows[table.Rows.Count-1]["UserName"].ToString(), activityName = table.Rows[table.Rows.Count-1]["ActivityName"].ToString();

			// last user summary row
			dr = dt.NewRow();
			dr["UserID"] = uid;
			dr["ActivityID"] = aid;
			dr["UserName"] = userName;
			dr["Time"] = userTotal;
			dr["RowType"] = 1;
			dt.Rows.Add(dr);
					
			//last activity summary row
			if (aid>0)
			{
				dr = dt.NewRow();
				dr["UserID"] = 0;
				dr["ActivityID"] = aid;
				dr["ActivityName"] = activityName;
				dr["Time"] = activityTotal;
				dr["RowType"] = 2;
					
				dt.Rows.Add(dr);	
			}
			// project total row

			dr = dt.NewRow();
			dr["RowType"] = 6;
			dt.Rows.Add(dr);

			dr = dt.NewRow();
			dr["Time"] = projectTotal;
			dr["RowType"] = 7;
			dt.Rows.Add(dr);	
			TotalMinutes=projectTotal;
			return dt;
		}

		public static DataSet LoadWorkTimes(int projectID, int activityID, int userID, string userStatus)
		{
			DataSet ds = null;
			bool IsActiveUser = false;
			bool IsAll = false;
			switch (userStatus)
			{
				case "0":IsAll = true;break;
				case "1":IsActiveUser = true;break;
				case "2":break;
			}

			try
			{
				ds = ReportsData.SelectMinutesReport(projectID, activityID, userID, IsActiveUser, IsAll);
			}
			catch (Exception ex)
			{
				log.Error(ex);
				return null;
			}

			return ds;
		}
		
		public static string StripHTML(string source)
		{
			try
			{
				string result;

				// Remove HTML Development formatting
				// Replace line breaks with space
				// because browsers inserts space
				result = source.Replace("\r", " ");
				// Replace line breaks with space
				// because browsers inserts space
				result = result.Replace("\n", " ");
				// Remove step-formatting
				result = result.Replace("\t", string.Empty);
				// Remove repeating spaces because browsers ignore them
				result = System.Text.RegularExpressions.Regex.Replace(result,
					@"( )+", " ");

				// Remove the header (prepare first by clearing attributes)
				result = System.Text.RegularExpressions.Regex.Replace(result,
					@"<( )*head([^>])*>", "<head>",
					System.Text.RegularExpressions.RegexOptions.IgnoreCase);
				result = System.Text.RegularExpressions.Regex.Replace(result,
					@"(<( )*(/)( )*head( )*>)", "</head>",
					System.Text.RegularExpressions.RegexOptions.IgnoreCase);
				result = System.Text.RegularExpressions.Regex.Replace(result,
					"(<head>).*(</head>)", string.Empty,
					System.Text.RegularExpressions.RegexOptions.IgnoreCase);

				// remove all scripts (prepare first by clearing attributes)
				result = System.Text.RegularExpressions.Regex.Replace(result,
					@"<( )*script([^>])*>", "<script>",
					System.Text.RegularExpressions.RegexOptions.IgnoreCase);
				result = System.Text.RegularExpressions.Regex.Replace(result,
					@"(<( )*(/)( )*script( )*>)", "</script>",
					System.Text.RegularExpressions.RegexOptions.IgnoreCase);
				//result = System.Text.RegularExpressions.Regex.Replace(result,
				//         @"(<script>)([^(<script>\.</script>)])*(</script>)",
				//         string.Empty,
				//         System.Text.RegularExpressions.RegexOptions.IgnoreCase);
				result = System.Text.RegularExpressions.Regex.Replace(result,
					@"(<script>).*(</script>)", string.Empty,
					System.Text.RegularExpressions.RegexOptions.IgnoreCase);

				// remove all styles (prepare first by clearing attributes)
				result = System.Text.RegularExpressions.Regex.Replace(result,
					@"<( )*style([^>])*>", "<style>",
					System.Text.RegularExpressions.RegexOptions.IgnoreCase);
				result = System.Text.RegularExpressions.Regex.Replace(result,
					@"(<( )*(/)( )*style( )*>)", "</style>",
					System.Text.RegularExpressions.RegexOptions.IgnoreCase);
				result = System.Text.RegularExpressions.Regex.Replace(result,
					"(<style>).*(</style>)", string.Empty,
					System.Text.RegularExpressions.RegexOptions.IgnoreCase);

				// insert tabs in spaces of <td> tags
				result = System.Text.RegularExpressions.Regex.Replace(result,
					@"<( )*td([^>])*>", "\t",
					System.Text.RegularExpressions.RegexOptions.IgnoreCase);

				// insert line breaks in places of <BR> and <LI> tags
				result = System.Text.RegularExpressions.Regex.Replace(result,
					@"<( )*br( )*>", "\r",
					System.Text.RegularExpressions.RegexOptions.IgnoreCase);
				result = System.Text.RegularExpressions.Regex.Replace(result,
					@"<( )*li( )*>", "\r",
					System.Text.RegularExpressions.RegexOptions.IgnoreCase);

				// insert line paragraphs (double line breaks) in place
				// if <P>, <DIV> and <TR> tags
				result = System.Text.RegularExpressions.Regex.Replace(result,
					@"<( )*div([^>])*>", "\r\r",
					System.Text.RegularExpressions.RegexOptions.IgnoreCase);
				result = System.Text.RegularExpressions.Regex.Replace(result,
					@"<( )*tr([^>])*>", "\r\r",
					System.Text.RegularExpressions.RegexOptions.IgnoreCase);
				result = System.Text.RegularExpressions.Regex.Replace(result,
					@"<( )*p([^>])*>", "\r\r",
					System.Text.RegularExpressions.RegexOptions.IgnoreCase);

				// Remove remaining tags like <a>, links, images,
				// comments etc - anything that's enclosed inside < >
				result = System.Text.RegularExpressions.Regex.Replace(result,
					@"<[^>]*>", string.Empty,
					System.Text.RegularExpressions.RegexOptions.IgnoreCase);

				// replace special characters:
				result = System.Text.RegularExpressions.Regex.Replace(result,
					@" ", " ",
					System.Text.RegularExpressions.RegexOptions.IgnoreCase);

				result = System.Text.RegularExpressions.Regex.Replace(result,
					@"&bull;", " * ",
					System.Text.RegularExpressions.RegexOptions.IgnoreCase);
				result = System.Text.RegularExpressions.Regex.Replace(result,
					@"&lsaquo;", "<",
					System.Text.RegularExpressions.RegexOptions.IgnoreCase);
				result = System.Text.RegularExpressions.Regex.Replace(result,
					@"&rsaquo;", ">",
					System.Text.RegularExpressions.RegexOptions.IgnoreCase);
				result = System.Text.RegularExpressions.Regex.Replace(result,
					@"&trade;", "(tm)",
					System.Text.RegularExpressions.RegexOptions.IgnoreCase);
				result = System.Text.RegularExpressions.Regex.Replace(result,
					@"&frasl;", "/",
					System.Text.RegularExpressions.RegexOptions.IgnoreCase);
				result = System.Text.RegularExpressions.Regex.Replace(result,
					@"&lt;", "<",
					System.Text.RegularExpressions.RegexOptions.IgnoreCase);
				result = System.Text.RegularExpressions.Regex.Replace(result,
					@"&gt;", ">",
					System.Text.RegularExpressions.RegexOptions.IgnoreCase);
				result = System.Text.RegularExpressions.Regex.Replace(result,
					@"&copy;", "(c)",
					System.Text.RegularExpressions.RegexOptions.IgnoreCase);
				result = System.Text.RegularExpressions.Regex.Replace(result,
					@"&reg;", "(r)",
					System.Text.RegularExpressions.RegexOptions.IgnoreCase);
				// Remove all others. More can be added, see
				// http://hotwired.lycos.com/webmonkey/reference/special_characters/
				result = System.Text.RegularExpressions.Regex.Replace(result,
					@"&(.{2,6});", string.Empty,
					System.Text.RegularExpressions.RegexOptions.IgnoreCase);

				// for testing
				//System.Text.RegularExpressions.Regex.Replace(result,
				//       this.txtRegex.Text,string.Empty,
				//       System.Text.RegularExpressions.RegexOptions.IgnoreCase);

				// make line breaking consistent
				result = result.Replace("\n", "\r");

				// Remove extra line breaks and tabs:
				// replace over 2 breaks with 2 and over 4 tabs with 4.
				// Prepare first to remove any whitespaces in between
				// the escaped characters and remove redundant tabs in between line breaks
				result = System.Text.RegularExpressions.Regex.Replace(result,
					"(\r)( )+(\r)", "\r\r",
					System.Text.RegularExpressions.RegexOptions.IgnoreCase);
				result = System.Text.RegularExpressions.Regex.Replace(result,
					"(\t)( )+(\t)", "\t\t",
					System.Text.RegularExpressions.RegexOptions.IgnoreCase);
				result = System.Text.RegularExpressions.Regex.Replace(result,
					"(\t)( )+(\r)", "\t\r",
					System.Text.RegularExpressions.RegexOptions.IgnoreCase);
				result = System.Text.RegularExpressions.Regex.Replace(result,
					"(\r)( )+(\t)", "\r\t",
					System.Text.RegularExpressions.RegexOptions.IgnoreCase);
				// Remove redundant tabs
				result = System.Text.RegularExpressions.Regex.Replace(result,
					"(\r)(\t)+(\r)", "\r\r",
					System.Text.RegularExpressions.RegexOptions.IgnoreCase);
				// Remove multiple tabs following a line break with just one tab
				result = System.Text.RegularExpressions.Regex.Replace(result,
					"(\r)(\t)+", "\r\t",
					System.Text.RegularExpressions.RegexOptions.IgnoreCase);
				// Initial replacement target string for line breaks
				string breaks = "\r\r\r";
				// Initial replacement target string for tabs
				string tabs = "\t\t\t\t\t";
				for (int index = 0; index < result.Length; index++)
				{
					result = result.Replace(breaks, "\r\r");
					result = result.Replace(tabs, "\t\t\t\t");
					breaks = breaks + "\r";
					tabs = tabs + "\t";
				}

				// That's it.
				return result;
			}
			catch
			{
				//MessageBox.Show("Error");
				return source;
			}
		}
		public static bool CanEditIssue(DateTime dtTask)
		{
			return true;
			/*if(SessionManager.LoggedUserInfo.IsLeader)
				return true;
			if(!SessionManager.LoggedUserInfo.IsAssistant)
				return false;
			int nDays=int.Parse(System.Configuration.ConfigurationManager.AppSettings["EditDays"]);
			if(dtTask.AddDays(nDays)<DateTime.Now)
				return false;
			return true;*/

		}

		public static DataTable GetBGNTable(DataTable dt1)
		{
			DataTable dt = dt1.Copy();
			decimal fix=decimal.Parse( System.Configuration.ConfigurationManager.AppSettings["fixedBGNEUR"]);
			foreach(DataRow dr in dt.Rows)
			{
				for(int i =0;i<dt.Columns.Count;i++)
				{
					if(dt.Columns[i].DataType==typeof(decimal) && dt.Columns[i].ColumnName!="ProjectID"
						&& dt.Columns[i].ColumnName!="Area" && dt.Columns[i].ColumnName!="WorkedHours")
					{
						if(dr[i] != DBNull.Value)
							dr[i]=UIHelpers.FormatDecimal2((decimal)dr[i]*fix);
						
					}
				}
			}
			return dt;
		}
		public static string Get4Replace(string sWhat, string sWith)
		{
			//string[] arrs = sWith.Split('\r');
			sWith=sWith.Replace("\r\n","\\r");
			sWith=sWith.Replace("'","`");
			return string.Concat("docRange.Find.Execute( '",sWhat,"', false, false, false, false, false, true,1, false, '", sWith,"', 2); ");
		}
		public static DateTime GetDate(TextBox t)
		{
			DateTime dt = Constants.DateMax;
			string s= t.Text;
			try
			{
				if(s!="")
					dt=DateTime.ParseExact(s,"dd.MM.yyyy",System.Globalization.NumberFormatInfo.InvariantInfo);
				return dt;
			}
			catch
			{
				
				return dt;
			}
		}
		public static string FormatDate(DateTime dt)
		{
			if(dt==Constants.DateMin || dt==Constants.DateMax || dt==new DateTime())
				return "";
			return dt.ToString("dd.MM.yyyy");
		}
		public static DateTime GetDate(string s)
		{
			DateTime dt = Constants.DateMax;
			

			try
			{
				if(s!="")
					dt=DateTime.ParseExact(s,"dd.MM.yyyy",System.Globalization.NumberFormatInfo.InvariantInfo);
				return dt;
			}
			catch
			{
				
				return dt;
			}
		}
        public static int GetBusinessDays(DateTime start, DateTime end)
        {
            if (start.DayOfWeek == DayOfWeek.Saturday)
            {
                start = start.AddDays(2);
            }
            else if (start.DayOfWeek == DayOfWeek.Sunday)
            {
                start = start.AddDays(1);
            }

            if (end.DayOfWeek == DayOfWeek.Saturday)
            {
                end = end.AddDays(-1);
            }
            else if (end.DayOfWeek == DayOfWeek.Sunday)
            {
                end = end.AddDays(-2);
            }

            int diff = (int)end.Subtract(start).TotalDays;

            int result = diff / 7 * 5 + diff % 7;

            if (end.DayOfWeek < start.DayOfWeek)
            {
                return result - 2;
            }
            else
            {
                return result;
            }
        }
//		public static bool LoadBuildingTypes(DropDownList ddlBuildingTypes, string selectedValue)
//		{
//			SqlDataReader reader = null;
//			try
//			{
//				reader = DBManager.SelectBuildingTypes();
//				ddlBuildingTypes.DataSource = reader;
//				ddlBuildingTypes.DataValueField = "BuildingTypeID";
//				ddlBuildingTypes.DataTextField = "BuildingType";
//				ddlBuildingTypes.DataBind();
//
//				ddlBuildingTypes.Items.Insert(0, new ListItem(Resource.ResourceManager["reports_typeBuilding"], "0"));
//				ddlBuildingTypes.SelectedValue = selectedValue;
//			}
//			catch
//			{
//				
//				return false;
//			}
//			finally 
//			{
//				if (reader!=null) reader.Close();
//			}
//
//			return true;
//		}
		public static void CreateMenuReports(PlaceHolder menuHolder, UserInfo LoggedUser)
		{
			UserControls.MenuTable menu = new UserControls.MenuTable();
			menu.ID = "MenuTable";

			// 'Links' group
			ArrayList menuItems = new ArrayList();
			//Edit by Ivailo date: 04.12.2007
			//notes: add LoggedUser.HasWorkTimeOver 
			if(LoggedUser.HasWorkTimeOver || LoggedUser.HasWorkTime || LoggedUser.IsSecretary || LoggedUser.HasPaymentRights   )
				menuItems.Add(new MenuItemInfo(Resource.ResourceManager["menuItem_Hours"], "../Hours.aspx", true));
               
                menuItems.Add(new MenuItemInfo(Resource.ResourceManager["menuItem_Projects"], "../Projects.aspx"));
			//if (LoggedUser.IsLeader || LoggedUser.IsAssistant) 
		{
			//menuItems.Add(new MenuItemInfo(Resource.ResourceManager["menuItem_Clients"], "../Clients.aspx"+"?id="+((int)ClientTypes.Client).ToString()));
			//menuItems.Add(new MenuItemInfo(Resource.ResourceManager["menuItem_Subcontracters"], "../Subcontracters.aspx"));
			//menuItems.Add(new MenuItemInfo(Resource.ResourceManager["menuItem_Users"], "../Users.aspx"));	
		}
			//			menuItems.Add(new MenuItemInfo(Resource.ResourceManager["menuItem_Emails"], "../Emails.aspx"));

			//Edit by Ivailo date: 04.12.2007
			//notes: add LoggedUser.HasWorkTimeOver 
			if(LoggedUser.HasWorkTimeOver ||LoggedUser.HasWorkTime || LoggedUser.HasPaymentRights ||LoggedUser.IsSecretary  )
			{
				menuItems.Add(new MenuItemInfo(Resource.ResourceManager["menuItem_Meetings"], "../Meetings.aspx",true));
				menuItems.Add(new MenuItemInfo(Resource.ResourceManager["menuItem_Protokols"], "../Protokols.aspx",false));
			}
			if(LoggedUser.HasPaymentRights || LoggedUser.IsSecretary ||LoggedUser.IsLayer)
			{
				menuItems.Add(new MenuItemInfo(Resource.ResourceManager["menuItem_Corespondency"], "../Corespondency.aspx"));
			}
            if (LoggedUser.HasPaymentRights || LoggedUser.IsSecretary || LoggedUser.IsLayer || LoggedUser.IsAssistant)
			{
				menuItems.Add(new MenuItemInfo(Resource.ResourceManager["auto_PageTitle"], "../Automobiles.aspx",true));
			}
			if(LoggedUser.IsLeader)
			{
				menuItems.Add(new MenuItemInfo(Resource.ResourceManager["menuItem_Schedule"], "../Schedule.aspx",true));
			}
			//menuItems.Add(new MenuItemInfo(Resource.ResourceManager["menuItem_Builders"], "../Clients.aspx"+"?id="+((int)ClientTypes.Builder).ToString(),true));
			//menuItems.Add(new MenuItemInfo(Resource.ResourceManager["menuItem_Contacts"], "../Clients.aspx"+"?id="+((int)ClientTypes.All).ToString(),true));
			menuItems.Add(new MenuItemInfo(Resource.ResourceManager["menuItem_AbsebceRequest"], "../AbsenceRequest.aspx",true));

			//menuItems.Add(new MenuItemInfo(Resource.ResourceManager["menuItem_HotIssuesASA"], "../HotIssues.aspx?ASA=1",true));
			menuItems.Add(new MenuItemInfo(Resource.ResourceManager["menuItem_HotIssues"], "../HotIssues.aspx",false));
			
			//NEW
			menuItems.Add(new MenuItemInfo(Resource.ResourceManager["menuItem_MinutesReport"], "Minutes.aspx"));
			if (LoggedUser.IsLeader || LoggedUser.IsAdmin || LoggedUser.IsAccountant)
			{
				menuItems.Add(new MenuItemInfo(Resource.ResourceManager["menuItem_Calendar"], "Calendar.aspx"));
			}
			
			menuItems.Add(new MenuItemInfo(Resource.ResourceManager["reports_Folders_Title"], "Folders.aspx"));
			if(LoggedUser.HasPaymentRights||LoggedUser.IsSecretary)
			{
				menuItems.Add(new MenuItemInfo(Resource.ResourceManager["reports_ContentFinished_Title"], "BuildingPercentFinished.aspx",false));
				menuItems.Add(new MenuItemInfo(Resource.ResourceManager["reports_SubsFinished_Title"], "BuildingPercentFinished.aspx?subs=1",false));
			}
			menu.AddMenuGroup(Resource.ResourceManager["menuHeader_Links"], 20, menuItems);
			// 'New' group
			menuItems = new ArrayList();
			menuItems.Add(new MenuItemInfo(Resource.ResourceManager["menuItem_NewProject"], "../EditProject.aspx"));
			//if (LoggedUser.IsLeader || LoggedUser.IsAssistant)
		{
			menuItems.Add(new MenuItemInfo(Resource.ResourceManager["menuItem_NewClient"], "../EditClient.aspx"+"?id="+((int)ClientTypes.Client).ToString()));
			if (LoggedUser.IsLeader || LoggedUser.IsAdmin || LoggedUser.IsAccountant )
				menuItems.Add(new MenuItemInfo(Resource.ResourceManager["menuItem_NewUser"], "../EditUser.aspx"));
		}
			//menuItems.Add(new MenuItemInfo(Resource.ResourceManager["menuItem_NewEmail"], "../EditEmail.aspx"));
			menuItems.Add(new MenuItemInfo(Resource.ResourceManager["newcontracter_PageTitle"], "../EditSubContracter.aspx"));

			if(/*LoggedUser.IsAssistantOnly ||*/ LoggedUser.HasPaymentRights /*||LoggedUser.IsSecretary*/)
				menu.AddMenuGroup(Resource.ResourceManager["menuHeader_New"], 30, menuItems);

			// "Reports' group
			menuItems = new ArrayList();
			//Edit by Ivailo date: 04.12.2007
			//notes: add LoggedUser.HasWorkTimeOver 
			UserInfo ui = UsersData.SelectUserByID(LoggedUser.UserID);
			if(LoggedUser.IsLeader)
			{
				menuItems.Add(new MenuItemInfo(Resource.ResourceManager["menuItem_WorkTimeRepormenuItem_Eff"], "Efficiency.aspx",true));
				
			}
			if(LoggedUser.HasPaymentRights)
			{
				menuItems.Add(new MenuItemInfo(Resource.ResourceManager["menuItem_WorkTimeReport"], "Worktimes.aspx"+"?id=1"));
			}
			if(/*ui!=null &&  ui.HasWorkTime ||*/ LoggedUser.HasPaymentRights)
			{
				menuItems.Add(new MenuItemInfo(Resource.ResourceManager["menuItem_WorkTimeOvertimeReport"], "Worktimes.aspx"+"?id=2"));
			}
			if (LoggedUser.IsAccountant || LoggedUser.IsLeader || LoggedUser.IsSecretary || LoggedUser.IsAdmin) 
				menuItems.Add(new MenuItemInfo(Resource.ResourceManager["menuItem_Absence"], "Absence.aspx"));
			

		
			//if (LoggedUser.IsLeader || LoggedUser.IsAssistant)
			//menuItems.Add(new MenuItemInfo(Resource.ResourceManager["menuItem_MailReport"], "../MailForm.aspx"));
			
			if (LoggedUser.IsLeader || LoggedUser.IsAdmin || LoggedUser.IsAccountant)
			{
				menuItems.Add(new MenuItemInfo(Resource.ResourceManager["menuItem_NotEnteredWorkTime"], "EmptyHours.aspx"));
				//menuItems.Add(new MenuItemInfo(Resource.ResourceManager["menuItem_Calendar"], "Calendar.aspx"));
				menuItems.Add(new MenuItemInfo(Resource.ResourceManager["menuItem_MontlyExpence"], "MontlyExpence.aspx",true));
			}
			
			
			if(LoggedUser.HasPaymentRights)
			{
				menuItems.Add(new MenuItemInfo(Resource.ResourceManager["menuItem_TraineeReport"], "Worktimes.aspx"+"?id=3",true));
			}
			menu.AddMenuGroup(Resource.ResourceManager["menuHeader_Reports"], 30, menuItems);
			if(LoggedUser.HasPaymentRights)
			{
				menuItems = new ArrayList();
				menuItems.Add(new MenuItemInfo(Resource.ResourceManager["menuItem_ProjectAnalysis"], "ProjectAnalysis.aspx"));
				menuItems.Add(new MenuItemInfo(Resource.ResourceManager["menuItem_ClientAnalysis"], "ClientAnalysis.aspx"));
				menuItems.Add(new MenuItemInfo(Resource.ResourceManager["menuItem_SubcontracterAnalysis"], "SubcontracterAnalysis.aspx"));
				if(LoggedUser.IsLeader)
					menuItems.Add(new MenuItemInfo(Resource.ResourceManager["menuItem_Chronology"], "Chronology.aspx",true));
				
				menuItems.Add(new MenuItemInfo(Resource.ResourceManager["menuItem_DDSAnalysis"], "DDSAnalysis.aspx"));
				menuItems.Add(new MenuItemInfo(Resource.ResourceManager["menuItem_ASI"], "SubcontracterAnalysis.aspx?Type=ASI"));
				menuItems.Add(new MenuItemInfo(Resource.ResourceManager["menuItem_AN"], "Minutes.aspx?Type=AN"));
				menuItems.Add(new MenuItemInfo(Resource.ResourceManager["menuItem_FeeAccount"], "FeeAccount.aspx",false));
				menuItems.Add(new MenuItemInfo(Resource.ResourceManager["menuItem_FinancePokazateli"], "FinancePokazateli.aspx",false));

				menu.AddMenuGroup(Resource.ResourceManager["menuHeader_Analysis"], 30, menuItems);
			}	
			if(LoggedUser.IsASI)
			{
				menuItems = new ArrayList();
				menuItems.Add(new MenuItemInfo(Resource.ResourceManager["menuItem_ASI"], "SubcontracterAnalysis.aspx?Type=ASI"));
				menu.AddMenuGroup(Resource.ResourceManager["menuHeader_Analysis"], 30, menuItems);
	
			}
			menuItems = new ArrayList();
			//menuItems.Add(new MenuItemInfo(Resource.ResourceManager["menuItem_Docs"], "images/ASA_ISO.pdf",true,Pages.Projects));
			menuItems.Add(new MenuItemInfo(Resource.ResourceManager["menuItem_Help"], SessionManager.GetReportsHelpLink(),true,GetPageFromRequest()));
			menu.AddMenuGroup("", 10, menuItems);


			menuHolder.Controls.Add(menu);
		}
		public static string GetPageFromRequest()
		{
			string sPath = HttpContext.Current.Request.Path;
			string sAppPath = HttpContext.Current.Request.ApplicationPath;
			string sPage  = sPath.Remove(0, sAppPath.Length+1);
			int nTill = sPage.IndexOf(".aspx");
			sPage = sPage.Substring(0,nTill);
			
			return sPage;
			
		}
		public static void CreateMenu(PlaceHolder menuHolder, UserInfo LoggedUser)
		{
			
			UserControls.MenuTable menu = new UserControls.MenuTable();
			menu.ID = "MenuTable";

			// 'Links' group
			ArrayList menuItems = new ArrayList();
			//Edit by Ivailo date: 04.12.2007
			//notes: add LoggedUser.HasWorkTimeOver 
			if(LoggedUser.HasWorkTimeOver || LoggedUser.HasWorkTime || LoggedUser.IsSecretary || LoggedUser.HasPaymentRights || LoggedUser.IsAdmin)
			//	menuItems.Add(new MenuItemInfo(Resource.ResourceManager["menuItem_Hours"], "Hours.aspx", true));
            menuItems.Add(new MenuItemInfo("<i class='fa fa-clock'></i> Работно време", "Hours.aspx", true));
			
			menuItems.Add(new MenuItemInfo(Resource.ResourceManager["menuItem_Projects"], "Projects.aspx"));
		//	menuItems.Add(new MenuItemInfo(Resource.ResourceManager["menuItem_Clients"], "Clients.aspx"+"?id="+((int)ClientTypes.Client).ToString()));
		//	menuItems.Add(new MenuItemInfo(Resource.ResourceManager["menuItem_Subcontracters"], "Subcontracters.aspx"));
			

			//if (LoggedUser.IsLeader || LoggedUser.IsAdmin)
			//menuItems.Add(new MenuItemInfo(Resource.ResourceManager["menuItem_Users"], "Users.aspx"));
			//			menuItems.Add(new MenuItemInfo(Resource.ResourceManager["menuItem_Emails"], "Emails.aspx"));
			//Edit by Ivailo date: 04.12.2007
			//notes: add LoggedUser.HasWorkTimeOver 
			if(LoggedUser.HasWorkTimeOver || LoggedUser.HasWorkTime || LoggedUser.IsAssistant || LoggedUser.IsSecretary || LoggedUser.HasPaymentRights  )
			{
				menuItems.Add(new MenuItemInfo(Resource.ResourceManager["menuItem_Meetings"], "Meetings.aspx",true));
				menuItems.Add(new MenuItemInfo(Resource.ResourceManager["menuItem_Protokols"], "Protokols.aspx",false));
			}
			if(LoggedUser.HasPaymentRights || LoggedUser.IsSecretary|| LoggedUser.IsLayer)
			{
				menuItems.Add(new MenuItemInfo(Resource.ResourceManager["menuItem_Corespondency"], "Corespondency.aspx"));
			}
			if(LoggedUser.HasPaymentRights || LoggedUser.IsSecretary|| LoggedUser.IsLayer || LoggedUser.IsAssistant)
			{
				menuItems.Add(new MenuItemInfo(Resource.ResourceManager["auto_PageTitle"], "Automobiles.aspx",true));
			}
			if(LoggedUser.IsLeader)
			{
				menuItems.Add(new MenuItemInfo(Resource.ResourceManager["menuItem_Schedule"], "Schedule.aspx",true));
			}
			//menuItems.Add(new MenuItemInfo(Resource.ResourceManager["menuItem_Builders"], "Clients.aspx"+"?id="+((int)ClientTypes.Builder).ToString(),true));
			//menuItems.Add(new MenuItemInfo(Resource.ResourceManager["menuItem_Contacts"], "Clients.aspx"+"?id="+((int)ClientTypes.All).ToString(),true));
            menuItems.Add(new MenuItemInfo("<i class='fa fa-briefcase'></i> Заяви отпуск", "AbsenceRequest.aspx", true));
            menuItems.Add(new MenuItemInfo("<i class='fa fa-book'></i> График отпуски", "reports/AbsenceSchedule.aspx", true));
            menuItems.Add(new MenuItemInfo("<i class='fa fa-tasks'></i> Задачи", "Tasks.aspx", true));
			//menuItems.Add(new MenuItemInfo(Resource.ResourceManager["menuItem_HotIssuesASA"], "HotIssues.aspx?ASA=1",true));
			menuItems.Add(new MenuItemInfo(Resource.ResourceManager["menuItem_HotIssues"], "HotIssues.aspx",false));
			//NEW
			
			if (LoggedUser.IsLeader || LoggedUser.IsAdmin || LoggedUser.IsAccountant)
			{
                menuItems.Add(new MenuItemInfo(Resource.ResourceManager["reports_Folders_Title"], "reports/Folders.aspx")); ;
				menuItems.Add(new MenuItemInfo(Resource.ResourceManager["menuItem_Calendar"], "reports/Calendar.aspx"));
                menuItems.Add(new MenuItemInfo(Resource.ResourceManager["menuItem_MinutesReport"], "reports/Minutes.aspx"));
			}
			
			if(LoggedUser.HasPaymentRights||LoggedUser.IsSecretary)
			{
				menuItems.Add(new MenuItemInfo(Resource.ResourceManager["reports_ContentFinished_Title"], "reports/BuildingPercentFinished.aspx",false));
				menuItems.Add(new MenuItemInfo(Resource.ResourceManager["reports_SubsFinished_Title"], "reports/BuildingPercentFinished.aspx?subs=1",false));
			}
			menu.AddMenuGroup(Resource.ResourceManager["menuHeader_Links"], 20, menuItems);
			
			// 'New' grop
			menuItems = new ArrayList();
			menuItems.Add(new MenuItemInfo(Resource.ResourceManager["menuItem_NewProject"], "editproject.aspx"));
			menuItems.Add(new MenuItemInfo(Resource.ResourceManager["menuItem_NewClient"], "editclient.aspx"+"?id="+((int)ClientTypes.Client).ToString()));
			if (LoggedUser.IsLeader || LoggedUser.IsAdmin || LoggedUser.IsAccountant )
				menuItems.Add(new MenuItemInfo(Resource.ResourceManager["menuItem_NewUser"], "EditUser.aspx"));
			//menuItems.Add(new MenuItemInfo(Resource.ResourceManager["menuItem_NewEmail"], "editemail.aspx"));
			menuItems.Add(new MenuItemInfo(Resource.ResourceManager["newcontracter_PageTitle"], "EditSubContracter.aspx"));

			if(/*LoggedUser.IsAssistantOnly ||*/ LoggedUser.HasPaymentRights /*||LoggedUser.IsSecretary*/)
				menu.AddMenuGroup(Resource.ResourceManager["menuHeader_New"], 30, menuItems);

			// 
			menuItems = new ArrayList();
			//Edit by Ivailo date: 04.12.2007
			//notes: add LoggedUser.HasWorkTimeOver 
			UserInfo ui = UsersData.SelectUserByID(LoggedUser.UserID);
			if(LoggedUser.IsLeader)
			{
				menuItems.Add(new MenuItemInfo(Resource.ResourceManager["menuItem_WorkTimeRepormenuItem_Eff"], "reports/Efficiency.aspx",true));
				
			}
			if(LoggedUser.HasPaymentRights)
			{
				menuItems.Add(new MenuItemInfo(Resource.ResourceManager["menuItem_WorkTimeReport"], "reports/worktimes.aspx" + "?id=1"));
			}
			if(/*ui!=null &&  ui.HasWorkTime || */LoggedUser.HasPaymentRights)
				{
				menuItems.Add(new MenuItemInfo(Resource.ResourceManager["menuItem_WorkTimeOvertimeReport"], "reports/worktimes.aspx"+"?id=2"));
			}
            if (LoggedUser.IsAccountant || LoggedUser.IsLeader || LoggedUser.IsSecretary) 
				menuItems.Add(new MenuItemInfo(Resource.ResourceManager["menuItem_Absence"], "reports/Absence.aspx"));
			
			
			//if (LoggedUser.IsLeader || LoggedUser.IsAssistant)
			//menuItems.Add(new MenuItemInfo(Resource.ResourceManager["menuItem_MailReport"], "MailForm.aspx"));
			if (LoggedUser.IsLeader || LoggedUser.IsAdmin || LoggedUser.IsAccountant)
			{
				menuItems.Add(new MenuItemInfo(Resource.ResourceManager["menuItem_NotEnteredWorkTime"], "reports/EmptyHours.aspx"));
				//menuItems.Add(new MenuItemInfo(Resource.ResourceManager["menuItem_Calendar"], "reports/Calendar.aspx"));
				menuItems.Add(new MenuItemInfo(Resource.ResourceManager["menuItem_MontlyExpence"], "reports/MontlyExpence.aspx",true));

			}
			
			if(LoggedUser.HasPaymentRights)
			{
				menuItems.Add(new MenuItemInfo(Resource.ResourceManager["menuItem_TraineeReport"], "reports/Worktimes.aspx"+"?id=3",true));
			}
			menu.AddMenuGroup(Resource.ResourceManager["menuHeader_Reports"], 30, menuItems);
			if(LoggedUser.HasPaymentRights)
				UIHelpers.AddMenuAnalysis(menu,LoggedUser);
			if(LoggedUser.IsASI)
			{
				menuItems = new ArrayList();
				menuItems.Add(new MenuItemInfo(Resource.ResourceManager["menuItem_ASI"], "reports/SubcontracterAnalysis.aspx?Type=ASI"));
				menu.AddMenuGroup(Resource.ResourceManager["menuHeader_Analysis"], 30, menuItems);
			}

			menuHolder.Controls.Add(menu);
		}
		public static void AddMenuAnalysis(MenuTable menu, UserInfo loggedUser)
		{
			ArrayList menuItems = new ArrayList();
			menuItems.Add(new MenuItemInfo(Resource.ResourceManager["menuItem_ProjectAnalysis"], "reports/ProjectAnalysis.aspx"));
			menuItems.Add(new MenuItemInfo(Resource.ResourceManager["menuItem_ClientAnalysis"], "reports/ClientAnalysis.aspx"));
			menuItems.Add(new MenuItemInfo(Resource.ResourceManager["menuItem_SubcontracterAnalysis"], "reports/SubcontracterAnalysis.aspx"));
			if(loggedUser.IsLeader)
				menuItems.Add(new MenuItemInfo(Resource.ResourceManager["menuItem_Chronology"], "reports/Chronology.aspx",true));
			menuItems.Add(new MenuItemInfo(Resource.ResourceManager["menuItem_DDSAnalysis"], "reports/DDSAnalysis.aspx"));
			menuItems.Add(new MenuItemInfo(Resource.ResourceManager["menuItem_ASI"], "reports/SubcontracterAnalysis.aspx?Type=ASI"));
			menuItems.Add(new MenuItemInfo(Resource.ResourceManager["menuItem_AN"], "reports/Minutes.aspx?Type=AN"));
			menuItems.Add(new MenuItemInfo(Resource.ResourceManager["menuItem_FeeAccount"], "reports/FeeAccount.aspx",false));
			menuItems.Add(new MenuItemInfo(Resource.ResourceManager["menuItem_FinancePokazateli"], "reports/FinancePokazateli.aspx",false));

			menu.AddMenuGroup(Resource.ResourceManager["menuHeader_Analysis"], 30, menuItems);
			
		}
		public static decimal ParseDecimal(string s)
		{
			if(s == null || s == "")
				return 0;
            //DBUGG
            //byte[] test;
            //test = ASCIIEncoding.ASCII.GetBytes(s);
            //DBUGG
            s = s.Replace(".", ",");
            s = Regex.Replace(s, "[^0-9,]", "");

			decimal d=0;
			try
			{
                var numberFormatInfo = new NumberFormatInfo { NumberDecimalSeparator = "," };
                d = decimal.Parse(s, numberFormatInfo);
			}
			catch
			{
				TimesheetPageBase.ErrorRedirect(Resource.ResourceManager["errorParse"]);
			}
			return d;
		}
		public static string Get4ReplacePlus(string sWhat, string sWith)
		{
			//string[] arrs = sWith.Split('\r');
			
			
			string[] s = new string[N_TEXT+1];
			int n=sWith.Length/N_FIXED;
			int nCur=0;
			sWith=sWith.Replace("'","`");
			sWith=sWith.Replace("\r\n","|");
			for(int i=0;i<n;i++)
			{
				if(nCur+N_FIXED>=sWith.Length)
				{
					s[i]=sWith.Substring(nCur);
					nCur=sWith.Length;
					s[i]=s[i].Replace("|","\\r");
					break;
				}
				else
				{
					s[i] = sWith.Substring(nCur,N_FIXED);
					nCur+=N_FIXED;
				}
				s[i]=s[i].Replace("|","\\r");
				
			}
			if(nCur<sWith.Length-1)
			{
				s[n]=sWith.Substring(nCur).Replace("|","\\r");
			}
			string sRet="";
			for(int i=0;i<s.Length;i++)
			{
				sRet+=string.Concat("docRange.Find.Execute( '",sWhat+i.ToString()+"}","', false, false, false, false, false, true,1, false, '", s[i],"', 2); ");
			}
			return sRet;
		}
		
		public static void FrontPage(System.Web.UI.Page page,string name, string investor,string addinfo,string code)
		{
			
			
			string DocumentPath= System.Configuration.ConfigurationManager.AppSettings["DocumentPath"];
			//string sPath = HttpContext.Current.Request.MapPath(DocumentPath + Constants._ORDERADD +((Statuses)md.StatusID).ToString()+Constants._DOT);
			string oTemplate = DocumentPath + Constants.FRONT_PAGE ;

			StringBuilder scr =new StringBuilder();
			scr.Append("<script language=\"javascript\">");
			scr.Append("onload=showdoc;");
			scr.Append("function showdoc(){");
			scr.Append("var word = new ActiveXObject('Word.Application');");
			scr.Append("word.Visible = true;");
			scr.Append("var t=word.Documents.Add('");
			scr.Append(oTemplate);
			scr.Append("', false, 0);   ");
			scr.Append("var docRange = t.Range();");
			scr.Append(UIHelpers.Get4Replace(Constants._PROJECT,name));
			scr.Append(UIHelpers.Get4Replace(Constants._INVESTOR,investor));
			scr.Append(UIHelpers.Get4Replace(Constants._ADDINFO,addinfo));
			scr.Append(UIHelpers.Get4Replace(Constants._CODE,code));
			
			
			
			scr.Append("}");
			scr.Append("</" + "script>");

            page.ClientScript.RegisterClientScriptBlock(page.GetType(), "showdoc", scr.ToString());

			
			
		}
		public static void Meeting(System.Web.UI.Page page,string project, string investor,string date,string manager,string user,string text,string sClients,string sSubs,string sUsers,string address)
		{
			
			
			string DocumentPath= System.Configuration.ConfigurationManager.AppSettings["DocumentPath"];
			//string sPath = HttpContext.Current.Request.MapPath(DocumentPath + Constants._ORDERADD +((Statuses)md.StatusID).ToString()+Constants._DOT);
			string oTemplate = DocumentPath + Constants.MEETING ;

			StringBuilder scr =new StringBuilder();
			scr.Append("<script language=\"javascript\">");
			scr.Append("onload=showdoc;");
			scr.Append("function showdoc(){");
			scr.Append("var word = new ActiveXObject('Word.Application');");
			scr.Append("word.Visible = true;");
			scr.Append("var t=word.Documents.Add('");
			scr.Append(oTemplate);
			scr.Append("', false, 0);   ");
			scr.Append("var docRange = t.Range();");
			scr.Append(UIHelpers.Get4Replace(Constants._PROJECT,project));
			scr.Append(UIHelpers.Get4Replace(Constants._INVESTOR,investor));
			scr.Append(UIHelpers.Get4Replace(Constants._DATE,date));
			scr.Append(UIHelpers.Get4Replace(Constants._MANAGER,manager));
			scr.Append(UIHelpers.Get4Replace(Constants._USER,user));
			scr.Append(UIHelpers.Get4Replace(Constants._USERS,sUsers));
			scr.Append(UIHelpers.Get4Replace(Constants._SUBCONTRACTERS,sSubs));
			scr.Append(UIHelpers.Get4Replace(Constants._CLIENTS,sClients));
			scr.Append(UIHelpers.Get4Replace(Constants._ADDRESS,address));
			scr.Append(UIHelpers.Get4ReplacePlus(Constants._TEXT,text));
			
			scr.Append("}");
			scr.Append("</" + "script>");

            page.ClientScript.RegisterClientScriptBlock(page.GetType(), "showdoc", scr.ToString());

			
			
		}
		public static void Subcontracter(System.Web.UI.Page page,string project, string subcontracter,string manager,string address,string lastname)
		{
			
			
			string DocumentPath= System.Configuration.ConfigurationManager.AppSettings["DocumentPath"];
			//string sPath = HttpContext.Current.Request.MapPath(DocumentPath + Constants._ORDERADD +((Statuses)md.StatusID).ToString()+Constants._DOT);
			string oTemplate = DocumentPath + Constants.BLANKAS ;

			StringBuilder scr =new StringBuilder();
			scr.Append("<script language=\"javascript\">");
			scr.Append("onload=showdoc;");
			scr.Append("function showdoc(){");
			scr.Append("var word = new ActiveXObject('Word.Application');");
			scr.Append("word.Visible = true;");
			scr.Append("var t=word.Documents.Add('");
			scr.Append(oTemplate);
			scr.Append("', false, 0);   ");
			scr.Append("var docRange = t.Range();");
			scr.Append(UIHelpers.Get4Replace(Constants._PROJECT,project));
			scr.Append(UIHelpers.Get4Replace(Constants._SUBCONTRACTER,subcontracter));
			
			scr.Append(UIHelpers.Get4Replace(Constants._ADDRESS,address));
			scr.Append(UIHelpers.Get4Replace(Constants._LASTNAME,lastname));
			scr.Append(UIHelpers.Get4Replace(Constants._MANAGER,manager));
			
			scr.Append("}");
			scr.Append("</" + "script>");

            page.ClientScript.RegisterClientScriptBlock(page.GetType(), "showdoc", scr.ToString());

			
			
		}
		public static void Udo(System.Web.UI.Page page,string name,string addinfo)
		{
			
			
			string DocumentPath= System.Configuration.ConfigurationManager.AppSettings["DocumentPath"];
			//string sPath = HttpContext.Current.Request.MapPath(DocumentPath + Constants._ORDERADD +((Statuses)md.StatusID).ToString()+Constants._DOT);
			string oTemplate = DocumentPath + Constants.UDO ;

			StringBuilder scr =new StringBuilder();
			scr.Append("<script language=\"javascript\">");
			scr.Append("onload=showdoc;");
			scr.Append("function showdoc(){");
			scr.Append("var word = new ActiveXObject('Word.Application');");
			scr.Append("word.Visible = true;");
			scr.Append("var t=word.Documents.Add('");
			scr.Append(oTemplate);
			scr.Append("', false, 0);   ");
			scr.Append("var docRange = t.Range();");
			scr.Append(UIHelpers.Get4Replace(Constants._PROJECT,name));
			scr.Append(UIHelpers.Get4Replace(Constants._ADDRESS,addinfo));
			
			
			
			scr.Append("}");
			scr.Append("</" + "script>");

            page.ClientScript.RegisterClientScriptBlock(page.GetType(), "showdoc", scr.ToString());

			
			
		}

		public static void Protokol(bool first, System.Web.UI.Page page,string name, string investor,string manager, string area,string addinfo, string Papki)
		{
			
			
			string DocumentPath= System.Configuration.ConfigurationManager.AppSettings["DocumentPath"];
			//string sPath = HttpContext.Current.Request.MapPath(DocumentPath + Constants._ORDERADD +((Statuses)md.StatusID).ToString()+Constants._DOT);
			string oTemplate = DocumentPath + Constants.PROTOKOL1 ;
			if(!first)
				oTemplate = DocumentPath + Constants.PROTOKOL2 ;
			StringBuilder scr =new StringBuilder();
			scr.Append("<script language=\"javascript\">");
			scr.Append("onload=showdoc;");
			scr.Append("function showdoc(){");
			scr.Append("var word = new ActiveXObject('Word.Application');");
			scr.Append("word.Visible = true;");
			scr.Append("var t=word.Documents.Add('");
			scr.Append(oTemplate);
			scr.Append("', false, 0);   ");
			scr.Append("var docRange = t.Range();");
			scr.Append(UIHelpers.Get4Replace(Constants._PROJECT,name));
			scr.Append(UIHelpers.Get4Replace(Constants._INVESTOR,investor));
			scr.Append(UIHelpers.Get4Replace(Constants._MANAGER,manager));
			scr.Append(UIHelpers.Get4Replace(Constants._AREA,area));
			scr.Append(UIHelpers.Get4Replace(Constants._ADDRESS,addinfo));
			scr.Append(UIHelpers.Get4Replace(Constants._PAPKI,Papki));
			
			
			
			scr.Append("}");
			scr.Append("</" + "script>");

            page.ClientScript.RegisterClientScriptBlock(page.GetType(), "showdoc", scr.ToString());

			
			
		}
		public static void LetterToClient(System.Web.UI.Page page,string name, string investor, string address, string user)
		{
			
			
			string DocumentPath= System.Configuration.ConfigurationManager.AppSettings["DocumentPath"];
			//string sPath = HttpContext.Current.Request.MapPath(DocumentPath + Constants._ORDERADD +((Statuses)md.StatusID).ToString()+Constants._DOT);
			string oTemplate = DocumentPath + Constants.LetterToClient ;

			StringBuilder scr =new StringBuilder();
			scr.Append("<script language=\"javascript\">");
			scr.Append("onload=showdoc;");
			scr.Append("function showdoc(){");
			scr.Append("var word = new ActiveXObject('Word.Application');");
			scr.Append("word.Visible = true;");
			scr.Append("var t=word.Documents.Add('");
			scr.Append(oTemplate);
			scr.Append("', false, 0);   ");
			scr.Append("var docRange = t.Range();");
			scr.Append(UIHelpers.Get4Replace(Constants._PROJECT,name));
			scr.Append(UIHelpers.Get4Replace(Constants._INVESTOR,investor));
			scr.Append(UIHelpers.Get4Replace(Constants._ADDRESS,address));
			scr.Append(UIHelpers.Get4Replace(Constants._USER,user));
			
			
			
			scr.Append("}");
			scr.Append("</" + "script>");

            page.ClientScript.RegisterClientScriptBlock(page.GetType(), "showdoc", scr.ToString());

			
			
		}
		public static void Reg(System.Web.UI.Page page,string name, string client, string address, string user)
		{
			
			
			string DocumentPath= System.Configuration.ConfigurationManager.AppSettings["DocumentPath"];
			//string sPath = HttpContext.Current.Request.MapPath(DocumentPath + Constants._ORDERADD +((Statuses)md.StatusID).ToString()+Constants._DOT);
			string oTemplate = DocumentPath + Constants.DOGOVOR ;

			StringBuilder scr =new StringBuilder();
			scr.Append("<script language=\"javascript\">");
			scr.Append("onload=showdoc;");
			scr.Append("function showdoc(){");
			scr.Append("var word = new ActiveXObject('Word.Application');");
			scr.Append("word.Visible = true;");
			scr.Append("var t=word.Documents.Add('");
			scr.Append(oTemplate);
			scr.Append("', false, 0);   ");
			scr.Append("var docRange = t.Range();");
			scr.Append(UIHelpers.Get4Replace(Constants._PROJECT,name));
			scr.Append(UIHelpers.Get4Replace(Constants._CLIENT,client));
			scr.Append(UIHelpers.Get4Replace(Constants._ADDRESS,address));
			scr.Append(UIHelpers.Get4Replace(Constants._USER,user));
			
			
			
			scr.Append("}");
			scr.Append("</" + "script>");

            page.ClientScript.RegisterClientScriptBlock(page.GetType(), "showdoc", scr.ToString());

			
			
		}
		//		public static void Clear(System.Web.UI.Page page)
		//		{
		//			StringBuilder scr =new StringBuilder();
		//			scr.Append("<script language=\"javascript\">");
		//			scr.Append("onload=showdoc;");
		//			scr.Append("function close(){document.close();");
		//			
		//			
		//			
		//			scr.Append("}");
		//			scr.Append("</" + "script>");
		//			
		//			page.RegisterClientScriptBlock("showdoc", scr.ToString());
		//		}
		public static void Note(System.Web.UI.Page page,string name, string addinfo,string text, string user)
		{
			
			
			string DocumentPath= System.Configuration.ConfigurationManager.AppSettings["DocumentPath"];
			//string sPath = HttpContext.Current.Request.MapPath(DocumentPath + Constants._ORDERADD +((Statuses)md.StatusID).ToString()+Constants._DOT);
			string oTemplate = DocumentPath + Constants.NOTES ;

			StringBuilder scr =new StringBuilder();
			scr.Append("<script language=\"javascript\">");
			scr.Append("onload=showdoc;");
			scr.Append("function showdoc(){");
			scr.Append("var word = new ActiveXObject('Word.Application');");
			scr.Append("word.Visible = true;");
			scr.Append("var t=word.Documents.Add('");
			scr.Append(oTemplate);
			scr.Append("', false, 0);   ");
			scr.Append("var docRange = t.Range();");
			scr.Append(UIHelpers.Get4Replace(Constants._PROJECT,name));
			
		
			scr.Append(UIHelpers.Get4ReplacePlus(Constants._TEXT,text));
			scr.Append(UIHelpers.Get4Replace(Constants._USER,user));
			scr.Append(UIHelpers.Get4Replace(Constants._ADDINFO,addinfo));
			scr.Append("}");
			scr.Append("</" + "script>");

            page.ClientScript.RegisterClientScriptBlock(page.GetType(), "showdoc", scr.ToString());

			
			
		}
		
		public static void Fax(System.Web.UI.Page page,string name, string addinfo,string client, string fax,string user)
		{
			
			
			string DocumentPath= System.Configuration.ConfigurationManager.AppSettings["DocumentPath"];
			//string sPath = HttpContext.Current.Request.MapPath(DocumentPath + Constants._ORDERADD +((Statuses)md.StatusID).ToString()+Constants._DOT);
			string oTemplate = DocumentPath + Constants.FAX_COVER ;

			StringBuilder scr =new StringBuilder();
			scr.Append("<script language=\"javascript\">");
			scr.Append("onload=showdoc;");
			scr.Append("function showdoc(){");
			scr.Append("var word = new ActiveXObject('Word.Application');");
			scr.Append("word.Visible = true;");
			scr.Append("var t=word.Documents.Add('");
			scr.Append(oTemplate);
			scr.Append("', false, 0);   ");
			scr.Append("var docRange = t.Range();");
			scr.Append(UIHelpers.Get4Replace(Constants._PROJECT,name));
			scr.Append(UIHelpers.Get4Replace(Constants._DATE,TimeHelper.FormatDate(DateTime.Now)));
			scr.Append(UIHelpers.Get4Replace(Constants._CLIENT,client));
			scr.Append(UIHelpers.Get4Replace(Constants._FAX,fax));
			scr.Append(UIHelpers.Get4Replace(Constants._USER,user));
			scr.Append(UIHelpers.Get4Replace(Constants._ADDINFO,addinfo));
			scr.Append("}");
			scr.Append("</" + "script>");

            page.ClientScript.RegisterClientScriptBlock(page.GetType(), "showdoc", scr.ToString());

			
			
		}
		public static int GetSIDParam()
		{
			int nID=-1;
			string sID= HttpContext.Current.Request.Params["sid"];
			if(sID!=null && sID!="")
			{
				nID= ToInt(sID);
			}
			return nID;
		}
		public static int GetPIDParam()
		{
			int nID=-1;
			string sID= HttpContext.Current.Request.Params["pid"];
			if(sID!=null && sID!="")
			{
				nID= ToInt(sID);
			}
			return nID;
		}
		public static int GetIDParam()
		{
			int nID=-1;
			string sID= HttpContext.Current.Request.Params["id"];
			if(sID!=null && sID!="")
			{
				nID= ToInt(sID);
			}
			return nID;
		}
		public static int GetMIDParam()
		{
			int nID=-1;
			string sID= HttpContext.Current.Request.Params["mid"];
			if(sID!=null && sID!="")
			{
				nID= ToInt(sID);
			}
			return nID;
		}
		/// <summary>
		/// Converts to int without throwing excpetion if unsuccess
		/// </summary>
		/// <param name="sID">String to parse</param>
		/// <returns>The parsed integer or -1 if unsuccess</returns>
		public static int ToInt(string sID)
		{
			double resNum;
			if(!Double.TryParse(sID, NumberStyles.Integer,NumberFormatInfo.InvariantInfo,out resNum))
				return -1;
			return (int) resNum;
		}
		/// <summary>
		/// Converts to double without throwing excpetion if unsuccess
		/// </summary>
		/// <param name="sID">String to parse</param>
		/// <returns>The parsed double or -1 if unsuccess</returns>
		public static double ToDouble(string sID)
		{
			double resNum;
			if(!Double.TryParse(sID, NumberStyles.Float,NumberFormatInfo.InvariantInfo,out resNum))
				return 0;
			return resNum;
		}
		public static PaymentsVector GetNewPaymentsTable(decimal total)
		{
			//			DataTable dt = new DataTable();
			//			dt.Columns.Add(new DataColumn("PaymentPercent", typeof(decimal)));
			//			dt.Columns.Add(new DataColumn("Amount", typeof(decimal)));
			//			dt.Columns.Add(new DataColumn("PaymentDate", typeof(DateTime)));
			PaymentsVector pv= new PaymentsVector();
			foreach(decimal d in payments)
			{
				PaymentData pd = new PaymentData(-1,-1,-1,d*100,d*total,false,new DateTime(),0,false,-1);
				pv.Add(pd);
			}
			return pv;
			//				DataRow dr = dt.NewRow();	
			//				dr[0]=(d*100);
			//				dr[1]=d*total;
			//				dt.Rows.Add(dr);
			//			}
			//
			//			dt.AcceptChanges();
			//			return dt;
		}
		public static string FormatDecimal(decimal d)
		{
			NumberFormatInfo nfi = new CultureInfo( "bg-BG", false ).NumberFormat;
			nfi.NumberDecimalDigits = 0;
			return d.ToString("n",nfi);
		}
		public static string FormatDecimal2(decimal d)
		{
			NumberFormatInfo nfi = new CultureInfo( "bg-BG", false ).NumberFormat;
			nfi.NumberDecimalDigits = 2;
			return d.ToString("n",nfi);
		}
		public static string FormatDecimal4(decimal d)
		{
			NumberFormatInfo nfi = new CultureInfo( "bg-BG", false ).NumberFormat;
			nfi.NumberDecimalDigits = 8;
			return d.ToString("n",nfi);
		}
		public static string FormatDecimaln(decimal d)
		{
			NumberFormatInfo nfi = new CultureInfo( "bg-BG", false ).NumberFormat;
			nfi.NumberDecimalDigits = 8;
			return d.ToString("n",nfi).TrimEnd('0').TrimEnd(',');
		}
		
		public static void AddValueByID(string sID,ListBox lsFrom,ListBox lsTo)
		{
			ListItem li;
			for(int i=0;i<lsFrom.Items.Count;i++)
				if(lsFrom.Items[i].Value==sID)
				{
					li = lsFrom.Items[i];
					lsTo.Items.Add(li);
					lsFrom.Items.Remove(li);
					return;
				}
		}
		public static bool FirstSelect(ListBox ls, Label lb)
		{
			if(ls.SelectedItem==null)
			{
				lb.Text="Please select an item first.";
				return true;
			}
			lb.Text="";
			return false;
		}
		public static void SetSelected(string sel,ListBox lsPersons,ListBox lsPersonsSel)
		{
			string[] ss = sel.Split(';');
			foreach(string s in ss)
				AddValueByID(s,lsPersons,lsPersonsSel);
			
		}
		public static string GetSelected(ListBox lsProjectsSel)
		{
			StringBuilder sb = new StringBuilder();
			for(int i=0;i<lsProjectsSel.Items.Count;i++)
			{
				sb.Append(lsProjectsSel.Items[i].Value);
				sb.Append(',');
			}
			string ret = sb.ToString();
			ret = ret.TrimEnd(new char[] {';'});
			return ret;
		}
		public static ArrayList AddSelectedListBoxValues(ListBox lsFrom,ListBox lsTo)
		{
			ArrayList al = new ArrayList();
			for(int i=0;i<lsFrom.Items.Count;i++)
				if(lsFrom.Items[i].Selected==true)
				{
					lsTo.Items.Add(lsFrom.Items[i]);
					al.Add(lsFrom.Items[i]);
				}
			for(int i=0;i<al.Count;i++)
				lsFrom.Items.Remove((ListItem)al[i]);
			lsFrom.SelectedIndex=-1;
			lsTo.SelectedIndex=-1;
			return al;
		}
		public static bool CheckForUniqueNameAccount(string newName,string oldName)
		{
			if(newName == oldName)
				return true;
			SqlDataReader reader = UsersData.SelectUsers(-1,1/*don't matter sortOrder*/);
			while(reader.Read())
			{
				string s = reader.GetString(6);
				if(s == newName)
					return false;
			}
			SubcontractersVector scv = SubcontracterDAL.LoadCollection();
			foreach(SubcontracterData scd in scv)
				if(scd.AccountName==newName)
					return false;
			ClientsVector cv = ClientDAL.LoadCollection(-1);
			foreach(ClientData cd in cv)
				if(cd.AccountName == newName)
					return false;
			return true;
		}
		
		
		#region FinancePokazateli
		private enum FirstTableFinancePokazateli
		{
			ProjectID = 0,
			ProjectName,
			Area,
			PaymentsNotPhase,
			PaymentsPhase,
			WorkedHours,
			WorkedSalary,
			AdminSalary,
			StaffSalary,
			LeaderSalary,
			SubcontracterSalary,
		}

		private enum ReportTableFinancePokazateli
		{
			ProjectID = 0,
			ProjectName,
			Area,
			Payments,
			PaymentArea,
			WorkedHours,
			WorkedSalary,
			AdminSalary,
			StaffSalary,
			LeaderSalary,
			SubcontracterSalary,
			TotalSalary,
			TotalIncome,
			ExpencesPerHour,
			ExpencesPerArea,
			PaymentPerHour,
			ImcomePerHour,
			

		}

	
		public static DataTable CreateTableForFinancePokazateli(DataTable dt)
		{
			DataTable dtDefault = new DataTable();
			foreach(ReportTableFinancePokazateli s in  Enum.GetValues(typeof(ReportTableFinancePokazateli)))
				if(s!=ReportTableFinancePokazateli.ProjectName)
					dtDefault.Columns.Add(s.ToString(),typeof(decimal));
				else
					dtDefault.Columns.Add(s.ToString());

			DataTable dtReturn = dtDefault.Clone();
				
			try
			{
//				foreach(ReportTableFinancePokazateliFinancePokazateli s in  Enum.GetValues(typeof(ReportTableFinancePokazateli)))
//					if(s!=ReportTableFinancePokazateli.ProjectName)
//						dtReturn.Columns.Add(s.ToString(),typeof(decimal));
//					else
//						dtReturn.Columns.Add(s.ToString());

				decimal d = 0;
				decimal d1 = 0;
				
				DataRow drAll = dtReturn.NewRow();
				DataRow drMinus = dtReturn.NewRow();
				DataRow drPlus = dtReturn.NewRow();
					
				drAll[(int)ReportTableFinancePokazateli.ProjectID] = -1;
				drAll[(int)ReportTableFinancePokazateli.ProjectName] = Resource.ResourceManager["lbTotal"];
			
				drPlus[(int)ReportTableFinancePokazateli.ProjectID] = -1;
				drPlus[(int)ReportTableFinancePokazateli.ProjectName] = Resource.ResourceManager["lbTotalPlus"];
				drMinus[(int)ReportTableFinancePokazateli.ProjectID] = -1;
				drMinus[(int)ReportTableFinancePokazateli.ProjectName] = Resource.ResourceManager["lbTotalMinus"];
					
				int dtCount = dt.Rows.Count;
				
				decimal dArea = 0;
				decimal dPayments = 0;
				decimal dWorkedHours = 0;
				decimal dWorkedSalary = 0;
				decimal dAdminSalary = 0;
				decimal dLeaderSalary = 0;
				decimal dStaffSalary = 0;
				decimal dSubcontracterSalary = 0;

				decimal dIncomePlus = 0;
				decimal dIncomeHourPlus = 0;
				decimal dIncomeMinus = 0;
				decimal dIncomeHourMinus = 0;
				decimal dWHPlus=0;
				decimal dWHMinus=0;
				
				foreach(DataRow dr in dt.Rows)
				{
//					if(dr[(int)FirstTableFinancePokazateli.WorkedHours]==DBNull.Value)
//						continue;
					DataRow drNew = dtReturn.NewRow();
					drNew[(int)ReportTableFinancePokazateli.ProjectID] = (int)dr[(int)FirstTableFinancePokazateli.ProjectID];
					drNew[(int)ReportTableFinancePokazateli.ProjectName] = (string)dr[(int)FirstTableFinancePokazateli.ProjectName];
					//Area
					if(dr[(int)FirstTableFinancePokazateli.Area]!=DBNull.Value)
						d = (decimal)dr[(int)FirstTableFinancePokazateli.Area];
					else 
						d=0;
					drNew[(int)ReportTableFinancePokazateli.Area] = d;
					dArea += d;
					
					//WorkedHours
					if(dr[(int)FirstTableFinancePokazateli.WorkedHours]!=DBNull.Value)
						d = ((decimal)dr[(int)FirstTableFinancePokazateli.WorkedHours])/60;
					else 
						d=0;
					drNew[(int)ReportTableFinancePokazateli.WorkedHours] = d;
					dWorkedHours += d;
					//WorkedSalary
					if(dr[(int)FirstTableFinancePokazateli.WorkedSalary]!=DBNull.Value)
						d = (decimal)dr[(int)FirstTableFinancePokazateli.WorkedSalary];
					else 
						d=0;
					drNew[(int)ReportTableFinancePokazateli.WorkedSalary] = d;
					dWorkedSalary += d;
					//AdminSalary
					if(dr[(int)FirstTableFinancePokazateli.AdminSalary]!=DBNull.Value)
						d = (decimal)dr[(int)FirstTableFinancePokazateli.AdminSalary];
					else
						d=0;
					drNew[(int)ReportTableFinancePokazateli.AdminSalary] = d;

					decimal dExpenses=0;
					if(dr[(int)FirstTableFinancePokazateli.LeaderSalary]!=DBNull.Value)
					{
						decimal dSum=(decimal)dr[(int)FirstTableFinancePokazateli.LeaderSalary];
						dLeaderSalary+=dSum;
						dExpenses += dSum;
						drNew[(int)ReportTableFinancePokazateli.LeaderSalary] = dSum;
					}
					if(dr[(int)FirstTableFinancePokazateli.StaffSalary]!=DBNull.Value)
					{
						decimal dSum=(decimal)dr[(int)FirstTableFinancePokazateli.StaffSalary];
						dStaffSalary+=dSum;
						dExpenses += dSum;
						drNew[(int)ReportTableFinancePokazateli.StaffSalary] = dSum;
					}
					

					dAdminSalary+=d;
					//SubcontracterSalary
					if(dr[(int)FirstTableFinancePokazateli.SubcontracterSalary]!=DBNull.Value)
						d = (decimal)dr[(int)FirstTableFinancePokazateli.SubcontracterSalary];
					else
						d=0;
					drNew[(int)ReportTableFinancePokazateli.SubcontracterSalary] =d;
					dSubcontracterSalary += d;
					//Payments				
					if(dr[(int)FirstTableFinancePokazateli.PaymentsPhase]!=DBNull.Value)
						d = (decimal)dr[(int)FirstTableFinancePokazateli.PaymentsPhase];
					else
						d=0;
					if(dr[(int)FirstTableFinancePokazateli.PaymentsNotPhase]!=DBNull.Value)
						d1 = (decimal)dr[(int)FirstTableFinancePokazateli.PaymentsNotPhase];
					else
						d1=0;
					drNew[(int)ReportTableFinancePokazateli.Payments] = d+d1;
					dPayments += d+d1;
					
					//PaymentArea		
					if((decimal)drNew[(int)ReportTableFinancePokazateli.Area] != 0)
						d = (decimal)drNew[(int)ReportTableFinancePokazateli.Payments]/(decimal)drNew[(int)ReportTableFinancePokazateli.Area];
					else
						d = 0;
					drNew[(int)ReportTableFinancePokazateli.PaymentArea] = d;
					//TotalSalary		
					drNew[(int)ReportTableFinancePokazateli.TotalSalary] = (decimal)drNew[(int)ReportTableFinancePokazateli.WorkedSalary]+(decimal)drNew[(int)ReportTableFinancePokazateli.AdminSalary]+(decimal)drNew[(int)ReportTableFinancePokazateli.SubcontracterSalary]+dExpenses;
					//TotalIncome		
					decimal income=(decimal)drNew[(int)ReportTableFinancePokazateli.Payments]-(decimal)drNew[(int)ReportTableFinancePokazateli.TotalSalary];
					drNew[(int)ReportTableFinancePokazateli.TotalIncome] = income;
					if(income>0)
					{
						dIncomePlus+=income;
						if((decimal)drNew[(int)ReportTableFinancePokazateli.WorkedHours] != 0)
						dWHPlus+=(decimal)drNew[(int)ReportTableFinancePokazateli.WorkedHours];
					}
					else
					{
						dIncomeMinus+=income;
						if((decimal)drNew[(int)ReportTableFinancePokazateli.WorkedHours] != 0)
						dWHMinus+=(decimal)drNew[(int)ReportTableFinancePokazateli.WorkedHours];
					}
						//ExpencesPerHour		
					if((decimal)drNew[(int)ReportTableFinancePokazateli.WorkedHours] != 0)
						d = (decimal) drNew[(int)ReportTableFinancePokazateli.TotalSalary]/(decimal)drNew[(int)ReportTableFinancePokazateli.WorkedHours];
					else
						d = 0;
					drNew[(int)ReportTableFinancePokazateli.ExpencesPerHour] =d;
					//ExpencesPerArea		
					if((decimal)drNew[(int)ReportTableFinancePokazateli.Area] != 0)
						d = (decimal) drNew[(int)ReportTableFinancePokazateli.TotalSalary]/(decimal)drNew[(int)ReportTableFinancePokazateli.Area];
					else
						d = 0;
					drNew[(int)ReportTableFinancePokazateli.ExpencesPerArea] =d;
					//PaymentPerHour		
					if((decimal)drNew[(int)ReportTableFinancePokazateli.WorkedHours] != 0)
						d = (decimal) drNew[(int)ReportTableFinancePokazateli.Payments]/(decimal)drNew[(int)ReportTableFinancePokazateli.WorkedHours];
					else
						d = 0;
					drNew[(int)ReportTableFinancePokazateli.PaymentPerHour] = d;
					//ImcomePerHour		
					decimal incomehour= (decimal)drNew[(int)ReportTableFinancePokazateli.PaymentPerHour] - (decimal)drNew[(int)ReportTableFinancePokazateli.ExpencesPerHour];
					drNew[(int)ReportTableFinancePokazateli.ImcomePerHour] =incomehour;
					if(incomehour>0)
						dIncomeHourPlus+=incomehour;
					else
						dIncomeHourMinus+=incomehour;
					
					dtReturn.Rows.Add(drNew);
				}

//				//Area
//				drAll[(int)ReportTableFinancePokazateli.Area] = dArea;
//				//WorkedHours
//				drAll[(int)ReportTableFinancePokazateli.WorkedHours] = dWorkedHours;
//				//WorkedSalary
//				drAll[(int)ReportTableFinancePokazateli.WorkedSalary] = dWorkedSalary;
//				//AdminSalary
//				drAll[(int)ReportTableFinancePokazateli.AdminSalary] = dAdminSalary;
//				//AdminSalary
//				drAll[(int)ReportTableFinancePokazateli.StaffSalary] = dStaffSalary;
//				//AdminSalary
//				drAll[(int)ReportTableFinancePokazateli.LeaderSalary] = dLeaderSalary;
//				//SubcontracterSalary
//				drAll[(int)ReportTableFinancePokazateli.SubcontracterSalary] =dSubcontracterSalary;
//				//Payments				
//				drAll[(int)ReportTableFinancePokazateli.Payments] = dPayments;
//				//PaymentArea		
//				if((decimal)drAll[(int)ReportTableFinancePokazateli.Area] != 0)
//					d = (decimal)drAll[(int)ReportTableFinancePokazateli.Payments]/(decimal)drAll[(int)ReportTableFinancePokazateli.Area];
//				else
//					d = 0;
//				drAll[(int)ReportTableFinancePokazateli.PaymentArea] = d;
//				//TotalSalary		
//				drAll[(int)ReportTableFinancePokazateli.TotalSalary] = (decimal)drAll[(int)ReportTableFinancePokazateli.WorkedSalary]+(decimal)drAll[(int)ReportTableFinancePokazateli.AdminSalary]+(decimal)drAll[(int)ReportTableFinancePokazateli.SubcontracterSalary];
//				//TotalIncome		
//				drAll[(int)ReportTableFinancePokazateli.TotalIncome] = (decimal)drAll[(int)ReportTableFinancePokazateli.Payments]-(decimal)drAll[(int)ReportTableFinancePokazateli.TotalSalary];
//				//ExpencesPerHour		
//				if((decimal)drAll[(int)ReportTableFinancePokazateli.WorkedHours] != 0)
//					d = (decimal) drAll[(int)ReportTableFinancePokazateli.TotalSalary]/(decimal)drAll[(int)ReportTableFinancePokazateli.WorkedHours];
//				else
//					d = 0;
//				drAll[(int)ReportTableFinancePokazateli.ExpencesPerHour] =d;
//				//PaymentPerHour		
//				if((decimal)drAll[(int)ReportTableFinancePokazateli.WorkedHours] != 0)
//					d = (decimal) drAll[(int)ReportTableFinancePokazateli.Payments]/(decimal)drAll[(int)ReportTableFinancePokazateli.WorkedHours];
//				else
//					d = 0;
//				drAll[(int)ReportTableFinancePokazateli.PaymentPerHour] = d;
//				//ImcomePerHour		
//				drAll[(int)ReportTableFinancePokazateli.ImcomePerHour] = (decimal)drAll[(int)ReportTableFinancePokazateli.PaymentPerHour] - (decimal)drAll[(int)ReportTableFinancePokazateli.ExpencesPerHour];
			
				for(int i=0;i<dtReturn.Columns.Count;i++)

					if(i!=(int)ReportTableFinancePokazateli.ProjectID && i!=(int)ReportTableFinancePokazateli.ProjectName)
					{
						decimal dVal=0;	
						foreach(DataRow dr in dtReturn.Rows)
						{
							if(dr[i]!=DBNull.Value)
								dVal+=(decimal)dr[i];
						}
						drAll[i]=dVal;
					}
				if((decimal)drAll[(int)ReportTableFinancePokazateli.WorkedHours] != 0)
				{
					drAll[(int)ReportTableFinancePokazateli.PaymentPerHour] =(decimal) drAll[(int)ReportTableFinancePokazateli.Payments]/(decimal)drAll[(int)ReportTableFinancePokazateli.WorkedHours];
					drAll[(int)ReportTableFinancePokazateli.ExpencesPerHour] =(decimal) drAll[(int)ReportTableFinancePokazateli.TotalSalary]/(decimal)drAll[(int)ReportTableFinancePokazateli.WorkedHours];
					drAll[(int)ReportTableFinancePokazateli.ImcomePerHour] =(decimal) drAll[(int)ReportTableFinancePokazateli.TotalIncome]/(decimal)drAll[(int)ReportTableFinancePokazateli.WorkedHours];
					drMinus[(int)ReportTableFinancePokazateli.ImcomePerHour] =dIncomeHourMinus;
					drPlus[(int)ReportTableFinancePokazateli.ImcomePerHour] =dIncomeHourPlus;
				}
				else
				{
					drAll[(int)ReportTableFinancePokazateli.PaymentPerHour] =0;
					drAll[(int)ReportTableFinancePokazateli.ExpencesPerHour] =0;
					drAll[(int)ReportTableFinancePokazateli.ImcomePerHour] =0;
				}
				if((decimal)drAll[(int)ReportTableFinancePokazateli.Area] != 0)
					drAll[(int)ReportTableFinancePokazateli.PaymentArea] = (decimal)drAll[(int)ReportTableFinancePokazateli.Payments]/(decimal)drAll[(int)ReportTableFinancePokazateli.Area];
				else
					drAll[(int)ReportTableFinancePokazateli.PaymentArea] =0;
				if(dWHPlus>0)
					drPlus[(int)ReportTableFinancePokazateli.ImcomePerHour] =dIncomePlus/dWHPlus;
				else
					drPlus[(int)ReportTableFinancePokazateli.ImcomePerHour] =0;
				if(dWHMinus>0)
					drMinus[(int)ReportTableFinancePokazateli.ImcomePerHour] =dIncomeMinus/dWHMinus;
				else
					drMinus[(int)ReportTableFinancePokazateli.ImcomePerHour] =0;
				
				
				drMinus[(int)ReportTableFinancePokazateli.TotalIncome] =dIncomeMinus;
				drPlus[(int)ReportTableFinancePokazateli.TotalIncome] =dIncomePlus;
				dtReturn.Rows.Add(drMinus);
				dtReturn.Rows.Add(drPlus);
				dtReturn.Rows.Add(drAll);
			
				return dtReturn;
			}
			catch
			{
				return dtDefault;
			}
		}
		#endregion

		#region ProjectFiltar
		//notes:function to load BuildingType:get ddl and set default value
		public static bool LoadBuildingTypes(DropDownList ddl, ProjectsData.ProjectsByStatus status)
		{
			return LoadBuildingTypes("0",ddl, status);

		}
		public static bool LoadBuildingTypes(DropDownList ddl)
		{
			return LoadBuildingTypes("0",ddl, ProjectsData.ProjectsByStatus.Active);

		}
	
		public static bool LoadBuildingTypes(string selectedValue,DropDownList ddl)
		{
			return LoadBuildingTypes("0",ddl, ProjectsData.ProjectsByStatus.Active);
		}
		//notes:function to load BuildingType:get ddl and set wanted value to ddl
		public static bool LoadBuildingTypes(string selectedValue,DropDownList ddl, ProjectsData.ProjectsByStatus status)
		{
			SqlDataReader reader = null;
			try
			{
				if(status== ProjectsData.ProjectsByStatus.Active)
					reader = DBManager.SelectActiveBuildingTypes();
				else
					reader=DBManager.SelectBuildingTypes();
				ddl.DataSource = reader;
				ddl.DataValueField = "BuildingTypeID";
				ddl.DataTextField = "BuildingType";
				ddl.DataBind();
				//				if(ddl.Items.FindByValue(System.Configuration.ConfigurationManager.AppSettings["BuildingTypeNotInPM"])!=null)
				//					ddl.Items.Remove(ddl.Items.FindByValue(System.Configuration.ConfigurationManager.AppSettings["BuildingTypeNotInPM"]));
				ddl.Items.Insert(0, new ListItem(Resource.ResourceManager["reports_typeBuilding"], "0"));
				if(ddl.Items.FindByValue(selectedValue)!=null)
					ddl.SelectedValue = selectedValue;
			}
			catch (Exception ex)
			{
				log.Error(ex.Message);
				return false;
			}
			finally 
			{
				if (reader!=null) reader.Close();
			}

			return true;
		}

		
		//notes:function to load project statuses ddl and set default value active project
		public static void LoadProjectStatus(DropDownList ddl)
		{
			LoadProjectStatus(ddl,(int)ProjectsData.ProjectsByStatus.Active);
		}

		
		//notes:function to load project statuses ddl and set wanted value to ddl
		public static void LoadProjectStatus(DropDownList ddl,int StartValue)
		{
			LoadProjectStatus( ddl, StartValue,true);
		}

		//notes:function to load project statuses ddl and set wanted value to ddl, 
		//	AllListItemsVisibles is if some group can see only all listItem and others see only active
		public static void LoadProjectStatus(DropDownList ddl,int StartValue,bool AllListItemsVisibles)
		{
			if(AllListItemsVisibles)
			{
				ListItem li = new ListItem(Resource.ResourceManager["Name_ddlProjectStatusName2_BG"],((int)ProjectsData.ProjectsByStatus.AllProjects).ToString());
				ddl.Items.Add(li);
				li = new ListItem(Resource.ResourceManager["Name_ddlProjectStatusName1_BG"],((int)ProjectsData.ProjectsByStatus.Active).ToString());
				ddl.Items.Add(li);
				li = new ListItem(Resource.ResourceManager["Name_ddlProjectStatusName3_BG"],((int)ProjectsData.ProjectsByStatus.Passive).ToString());
				ddl.Items.Add(li);
				li = new ListItem(Resource.ResourceManager["Name_ddlProjectStatusName4_BG"],((int)ProjectsData.ProjectsByStatus.Conluded).ToString());
				ddl.Items.Add(li);
				if(ddl.Items.FindByValue(StartValue.ToString())!= null)
					ddl.SelectedValue = StartValue.ToString();
			}
			else
			{
				ListItem li = new ListItem(Resource.ResourceManager["Name_ddlProjectStatusName1_BG"],((int)ProjectsData.ProjectsByStatus.Active).ToString());
				ddl.Items.Add(li);
				li = new ListItem(Resource.ResourceManager["Name_ddlProjectStatusName3_BG"],((int)ProjectsData.ProjectsByStatus.Passive).ToString());
				ddl.Items.Add(li);
				if(ddl.Items.Count>0)
					ddl.SelectedIndex = 0;
			}
		}

	
		//notes:function to load project statuses ddl but without option for concluded projects and set wanted value to ddl
		public static void LoadProjectStatusWithoutConcluded(DropDownList ddl,int StartValue)
		{
			ListItem li = new ListItem(Resource.ResourceManager["Name_ddlProjectStatusName2_BG"],((int)ProjectsData.ProjectsByStatus.AllProjects).ToString());
			ddl.Items.Add(li);
			li = new ListItem(Resource.ResourceManager["Name_ddlProjectStatusName1_BG"],((int)ProjectsData.ProjectsByStatus.Active).ToString());
			ddl.Items.Add(li);
			li = new ListItem(Resource.ResourceManager["Name_ddlProjectStatusName3_BG"],((int)ProjectsData.ProjectsByStatus.Passive).ToString());
			ddl.Items.Add(li);
			if(ddl.Items.FindByValue(StartValue.ToString())!= null)
			ddl.SelectedValue = StartValue.ToString();//((int)ProjectsData.ProjectsByStatus.Active).ToString();
		}

	
		//notes:function to load projects ddl taking values of Status and building types
		public static bool LoadProjects(DropDownList ddlProjects,DropDownList ddlProjectsStatus,DropDownList ddlBuildingTypes)
		{
			SqlDataReader reader = null;
			
			try
			{
				//bool isBuildingTypeGradoustrShow = false;
				ddlProjects.Items.Clear();
				ddlProjects.Items.Add(new ListItem("<"+Resource.ResourceManager["reports_ddlAllProjects"]+">","0"));
				
//				if(ddlBuildingTypes.SelectedValue == System.Configuration.ConfigurationManager.AppSettings["BuildingTypeNotInPM"])
//					isBuildingTypeGradoustrShow = true;

				reader = ProjectsData.SelectProjectNamesClear((ProjectsData.ProjectsByStatus)int.Parse(ddlProjectsStatus.SelectedValue), int.Parse(ddlBuildingTypes.SelectedValue));
			
				while (reader.Read())
				{
					int projectID = reader.GetInt32(0);
					ddlProjects.Items.Add(new ListItem(reader.GetString(1), projectID.ToString()));
					
				}
			}
			catch (Exception ex)
			{
				log.Error(ex.Message);
				return false;
			}

			finally
			{
				if (reader!=null) reader.Close();
			}
			
			
			return true;
		}

	
		#endregion

		#region HotIssueObjects

		
		public static bool LoadHotIssueCategories(DropDownList ddl,int ProjectID)
		{
			if(ProjectID == -1)
			{
				ddl.Items.Insert(0, new ListItem(Resource.ResourceManager["btnChooseNew_New"], "0"));
				return true;
			}
			SqlDataReader reader = null;
			try
			{

				reader = DBManager.SelectHotIssueCategories(ProjectID);
				ddl.DataSource = reader;
				ddl.DataValueField = "HotIssueCategoryID";
				ddl.DataTextField = "HotIssueCategoryName";
				ddl.DataBind();
				
				ddl.Items.Insert(0, new ListItem("<"+Resource.ResourceManager["btnChooseNew_New"]+">", "0"));
				ddl.SelectedValue = "0";
			}
			catch (Exception ex)
			{
				log.Error(ex.Message);
				return false;
			}
			finally 
			{
				if (reader!=null) reader.Close();
			}
			return true;
		}
		public static bool LoadHotIssueCategories(DataGrid grd,int ProjectID, int nColumn)
		{
			
			SqlDataReader reader = null;
			try
			{

				reader = DBManager.SelectHotIssueCategories(ProjectID);
				grd.DataSource = reader;
				grd.DataKeyField = "HotIssueCategoryID";
				
				grd.DataBind();
				grd.Visible=grd.Items.Count>0;
				TimesheetPageBase.SetConfirmDelete(grd, "btnDelete", nColumn);
				
			}
			catch (Exception ex)
			{
				log.Error(ex.Message);
				return false;
			}
			finally 
			{
				if (reader!=null) reader.Close();
			}
			return true;
		}
		public static bool LoadHotIssueCategories(DropDownList ddl,int ProjectID,int buildingType,int projectStatus)
		{
			SqlDataReader reader = null;
			try
			{

				reader = DBManager.SelectHotIssueCategories(ProjectID,buildingType,projectStatus);
				ddl.DataSource = reader;
				ddl.DataValueField = "HotIssueCategoryID";
				ddl.DataTextField = "HotIssueCategoryName";
				ddl.DataBind();
				ddl.Items.Insert(0, new ListItem(Resource.ResourceManager["Corespondency_All"], "0"));
				ddl.SelectedValue = "0";
			}
			catch (Exception ex)
			{
				log.Error(ex.Message);
				return false;
			}
			finally 
			{
				if (reader!=null) reader.Close();
			}
			return true;
		}

		public static bool LoadHotIssueCategories(DropDownList ddl)
		{
			SqlDataReader reader = null;
			try
			{

				reader = DBManager.SelectHotIssueCategories(-1);
				ddl.DataSource = reader;
				ddl.DataValueField = "HotIssueCategoryID";
				ddl.DataTextField = "HotIssueCategoryName";
				ddl.DataBind();
				ddl.Items.Insert(0, new ListItem(Resource.ResourceManager["Corespondency_All"], "0"));
				ddl.SelectedValue = "0";
			}
			catch (Exception ex)
			{
				log.Error(ex.Message);
				return false;
			}
			finally 
			{
				if (reader!=null) reader.Close();
			}
			return true;
		}

//		public static bool LoadHotIssuePriorities(DropDownList ddl)
//		{
//			SqlDataReader reader = null;
//			try
//			{
//
//				reader = DBManager.SelectHotIssuePriorities();
//				ddl.DataSource = reader;
//				ddl.DataValueField = "HotIssuePriorityID";
//				ddl.DataTextField = "HotIssuePriorityName";
//				ddl.DataBind();
//				ddl.Items.Insert(0, new ListItem(Resource.ResourceManager["Corespondency_All"], "0"));
//				ddl.SelectedValue = "0";
//			}
//			catch (Exception ex)
//			{
//				log.Error(ex.Message);
//				return false;
//			}
//			finally 
//			{
//				if (reader!=null) reader.Close();
//			}
//			return true;
//		}

		public static bool LoadHotIssueStatuses(DropDownList ddl)
		{
			SqlDataReader reader = null;
			try
			{

				reader = DBManager.SelectHotIssueStatuses();
				ddl.DataSource = reader;
				ddl.DataValueField = "HotIssueStatusID";
				ddl.DataTextField = "HotIssueStatusName";
				ddl.DataBind();
				ddl.Items.Insert(0, new ListItem(Resource.ResourceManager["Corespondency_All"], "0"));
				ddl.SelectedIndex = 1;
			}
			catch (Exception ex)
			{
				log.Error(ex.Message);
				return false;
			}
			finally 
			{
				if (reader!=null) reader.Close();
			}
			return true;
		}

		public static void LoadItems(int projectID, out string Clients, out string Team, out string Subs, out string Builders)
		{
			Builders=Subs=Team=Clients="";
			SqlDataReader reader = null;

			try
			{
				reader = ProjectsData.SelectProject(projectID);
				if(reader.Read())
				{

					
					int clientID = reader.IsDBNull(5) ? 0 : reader.GetInt32(5);
					ClientData cd = ClientDAL.Load(clientID);
					if(cd!=null)
						Clients+=cd.ClientName+", ";
					int projectManagerID = reader.IsDBNull(11) ? 0 : reader.GetInt32(11);
					UserInfo ud = UsersData.SelectUserByID(projectManagerID);
					if(ud!=null && ud.UserID>0)
					{
						Team+=ud.FullName+", ";
					}
				}
			}
			catch (Exception ex)
			{
				log.Info(ex);
				return;
			}

			finally 
			{ 
				if (reader!=null) reader.Close(); 
			}
			ProjectUsersVector puv = ProjectUserDAL.LoadCollection("ProjectUsersSelByProjectProc",SQLParms.CreateProjectUsersSelByProjectProc(projectID));
			foreach(ProjectUserData pud in puv)
			{

				UserInfo ud = UsersData.SelectUserByID(pud.UserID);
				Team+=ud.FullName+", ";
		
			}  
			ProjectClientsVector pcvClients=		ProjectClientDAL.LoadCollection("ProjectClientsSelByProjectProc",SQLParms.CreateProjectClientsSelByProjectProc(projectID,(int)ClientTypes.Client));
			foreach(ProjectClientData pcd in pcvClients)
			{
				Clients+=ClientDAL.Load(pcd.ClientID).ClientName+", ";
			}
			ProjectSubcontractersVector psv= ProjectSubcontracterDAL.LoadCollection("ProjectSubcontractersListProc1",SQLParms.CreateProjectSubcontractersListProc1 (projectID,false));
			foreach(ProjectSubcontracterData pcd in psv)
			{
				Subs+=pcd.SubcontracterType+" - "+pcd.SubcontracterName+";";
			}
			ProjectClientsVector pcv=		ProjectClientDAL.LoadCollection("ProjectClientsSelByProjectProc",SQLParms.CreateProjectClientsSelByProjectProc(projectID,(int)ClientTypes.Builder));

			ProjectClientsVector pcv1=		ProjectClientDAL.LoadCollection("ProjectClientsSelByProjectProc",SQLParms.CreateProjectClientsSelByProjectProc(projectID,(int)ClientTypes.Supervisors));
			ProjectClientsVector pcv2=		ProjectClientDAL.LoadCollection("ProjectClientsSelByProjectProc",SQLParms.CreateProjectClientsSelByProjectProc(projectID,(int)ClientTypes.Brokers));
			ProjectClientsVector pcv3=		ProjectClientDAL.LoadCollection("ProjectClientsSelByProjectProc",SQLParms.CreateProjectClientsSelByProjectProc(projectID,(int)ClientTypes.ProjectManagers));
			pcv.AddRange(pcv1);
			pcv.AddRange(pcv2);
			pcv.AddRange(pcv3);
			foreach(ProjectClientData pcd in pcv)
			{
				Builders+=ClientDAL.Load(pcd.ClientID).ClientName+", ";
			}
		}
		public static bool LoadHotIssueAssigneds(DropDownList ddl,int projectID)
		{
			//SqlDataReader reader = null;
			try
			{
				const string _userValue = "UserValue";
				const string _userName = "UserName";
				string nameOfProcedure = "UsersListArcgitectProc";
			
				DataTable dtUsers = CommonDAL.CreateTableFromReader(UsersData.SelectUsersSplit(Constants.DirectorRole, 1,nameOfProcedure,-1,projectID));
				DataTable dtSubs = CommonDAL.CreateTableFromReader(SubprojectsUDL.SelectSubcontracters(-1,1,false,projectID,false));
				ProjectClientsVector pcv=		ProjectClientDAL.LoadCollection("ProjectClientsSelByProjectProc",SQLParms.CreateProjectClientsSelByProjectProc(projectID,(int)ClientTypes.Builder));

				ProjectClientsVector pcv1=		ProjectClientDAL.LoadCollection("ProjectClientsSelByProjectProc",SQLParms.CreateProjectClientsSelByProjectProc(projectID,(int)ClientTypes.Supervisors));
				ProjectClientsVector pcv2=		ProjectClientDAL.LoadCollection("ProjectClientsSelByProjectProc",SQLParms.CreateProjectClientsSelByProjectProc(projectID,(int)ClientTypes.Client));
				ProjectClientsVector pcv3=		ProjectClientDAL.LoadCollection("ProjectClientsSelByProjectProc",SQLParms.CreateProjectClientsSelByProjectProc(projectID,(int)ClientTypes.Brokers));
				ProjectClientsVector pcv4=		ProjectClientDAL.LoadCollection("ProjectClientsSelByProjectProc",SQLParms.CreateProjectClientsSelByProjectProc(projectID,(int)ClientTypes.ProjectManagers));
				pcv.AddRange(pcv1);
				pcv.AddRange(pcv2);
				pcv.AddRange(pcv3);
				pcv.AddRange(pcv4);
				DataTable dtDDL = new DataTable();
				dtDDL.Columns.Add(_userValue);
				dtDDL.Columns.Add(_userName);
				foreach(DataRow dr in dtSubs.Rows)
				{
					DataRow drNew = dtDDL.NewRow();
					drNew[_userValue] = string.Concat((int)HotIssueUserTypes.Subcontracters,"_",dr["SubcontracterID"]);
					drNew[_userName] = string.Concat(dr["SubcontracterType"]," - ",dr["SubcontracterName"]);
					dtDDL.Rows.Add(drNew);
				}
				DataRow drNew1 = dtDDL.NewRow();
				drNew1[_userValue] = string.Concat((int)HotIssueUserTypes.ASA_all,"_1"/*ASA_all has only one value*/);
				drNew1[_userName] = Resource.ResourceManager["HotIssue_ASAall"];
				dtDDL.Rows.Add(drNew1);
				foreach(ProjectClientData pcd in pcv)
				{
					
					ClientData cd = ClientDAL.Load(pcd.ClientID);
					if(cd!=null)
					{
						DataRow drNew = dtDDL.NewRow();
						drNew[_userValue] = string.Concat((int)HotIssueUserTypes.Clients,"_",pcd.ClientID);
						drNew[_userName] = cd.ClientName;
							dtDDL.Rows.Add(drNew);
					}
				}
				int nID=ProjectsData.SelectProjectClientID(projectID);
				if(nID>0)
				{
					ClientData cd = ClientDAL.Load(nID);
					if(cd!=null)
					{
						DataRow drNew = dtDDL.NewRow();
						drNew[_userValue] = string.Concat((int)HotIssueUserTypes.Clients,"_",nID);
						drNew[_userName] = cd.ClientName;
						dtDDL.Rows.Add(drNew);
					}
				}
				foreach(DataRow dr in dtUsers.Rows)
				{
					DataRow drNew = dtDDL.NewRow();
					drNew[_userValue] = string.Concat((int)HotIssueUserTypes.ASA_users,"_",dr["UserID"]);
					drNew[_userName] = dr["FullName"];
					dtDDL.Rows.Add(drNew);
				}
				
				DataView dv = new DataView(dtDDL);

				ddl.DataSource = dv;
				ddl.DataValueField = "UserValue";
				ddl.DataTextField = "UserName";
				ddl.DataBind();
				ddl.Items.Insert(0, new ListItem(Resource.ResourceManager["Corespondency_All"], "0"));
				ddl.SelectedValue = "0";
			}
			catch (Exception ex)
			{
				log.Error(ex.Message);
				return false;
			}
			finally 
			{
				//if (reader!=null) reader.Close();
			}
			return true;
		}

		public static string GetHotIssueUserNameAssigned(int userID,int UserType)
		{
			switch(UserType)
			{
				case (int)HotIssueUserTypes.ASA_users:{ UserInfo ui = UsersData.SelectUserByID(userID); return ui.FullName;}
				case (int)HotIssueUserTypes.Subcontracters: {SubcontracterData dat = SubcontracterDAL.Load(userID); return string.Concat(dat.SubcontracterName," - ",dat.SubcontracterType);}
				case (int)HotIssueUserTypes.ASA_all:{ return Resource.ResourceManager["HotIssue_ASAall"];}
					case (int)HotIssueUserTypes.Clients: {ClientData dat = ClientDAL.Load(userID); return string.Concat(dat.ClientName);}
				default: return "";


			}
		}
		public static string GetHotIssueUserNameAssignedEN(int userID,int UserType)
		{
			switch(UserType)
			{
				case (int)HotIssueUserTypes.ASA_users:{ UserInfo ui = UsersData.SelectUserByID(userID); return ui.UserNameEN;}
				case (int)HotIssueUserTypes.Subcontracters: {SubcontracterData dat = SubcontracterDAL.Load(userID); return string.Concat(dat.SubcontracterNameEN);}
				case (int)HotIssueUserTypes.ASA_all:{ return "ASA";}
				case (int)HotIssueUserTypes.Clients: {ClientData dat = ClientDAL.Load(userID); return string.Concat(dat.ClientNameEN);}
				default: return "";


			}
		}


		public static DataView CreateDataViewFromHotIssuesVector(HotIssuesVector hiv, bool bAll)
		{
			try
			{
				DataTable dt = new DataTable();
				MemberInfo[] mia = typeof(HotIssueData).GetMember("*");
				foreach(MemberInfo mi in mia)
					dt.Columns.Add(mi.Name,mi.DeclaringType);
				foreach(HotIssueData dat in hiv)
				{
					DataRow dr = dt.NewRow();
					foreach(MemberInfo mi in mia)
						if(dat.GetByName(mi.Name)!=null)
							dr[mi.Name] = dat.GetByName(mi.Name);
					if(!bAll && dat.ToClient || bAll)
						dt.Rows.Add(dr);
				}
				return new DataView(dt);
			}
			catch(Exception ex)
			{
				log.Error(ex.Message);
				return new DataView();
			}
		}
		
	
		private const string _projectCode = "ProjectCode";
		private const string _projectName = "ProjectName";
        private const string _projectDescription = "ProjectDescription";
		public static DataView HotIssueCreateDVprojects(int MainProjectID,bool bEnglish)
		{
			try
			{
				//notes:create table of project, so in report to show them with name and code
				DataTable dtProjects = new DataTable();
				dtProjects.Columns.Add(_projectCode);
                dtProjects.Columns.Add(_projectName);
				dtProjects.Columns.Add(_projectDescription);
                
				//notes:method for main project
				SqlDataReader reader = ProjectsData.SelectProject(MainProjectID);
				if(reader.Read())
				{
					DataRow drMainProject = dtProjects.NewRow();
					if(bEnglish)
					{
						drMainProject[_projectCode] = reader.IsDBNull(2) ? String.Empty : reader.GetString(2);
						drMainProject[_projectName] = reader.IsDBNull(1) ? String.Empty : reader.GetString(1);;
                        drMainProject[_projectDescription] = reader.IsDBNull(37) ? String.Empty : reader.GetString(37);
					}
					else
					{
						drMainProject[_projectCode] = reader.IsDBNull(2) ? String.Empty : reader.GetString(2);
						drMainProject[_projectName] = reader.IsDBNull(1) ? String.Empty : reader.GetString(1);
                        drMainProject[_projectDescription] = reader.IsDBNull(12) ? String.Empty : reader.GetString(12);
					}
						dtProjects.Rows.Add(drMainProject);
				}
				
				//notes:method for other projects
//				foreach(MeetingProjectData mpd in mpv)
//				{
//					reader = ProjectsData.SelectProject(mpd.ProjectID);
//					if(reader.Read())
//					{
//						DataRow drProject = dtProjects.NewRow();
//						drProject[_projectCode] = reader.IsDBNull(2) ? String.Empty : reader.GetString(2);
//						drProject[_projectName] = reader.IsDBNull(12) ? String.Empty : reader.GetString(12);;
//						dtProjects.Rows.Add(drProject);
//					}
//				}
				//notes:create dataview to set in report
				return new DataView(dtProjects);
			}
			catch(Exception ex)
			{
				log.Error(ex.Message);
				return new DataView();
			}
		}
	
		private const string _userName = "UserName";
		private const string _userMail = "UserMail";
		private const string _userFrom = "UserFrom";
		public static DataView HotIssueCreateDVusers(string users)
		{
			try
			{
				
				
				string[] arrUsers = users.Split(';');
				
				//notes:create table of project, so in report to show them with name and code
				DataTable dtUsers = new DataTable();
				dtUsers.Columns.Add(_userName);
				dtUsers.Columns.Add(_userFrom);
				dtUsers.Columns.Add(_userMail);
				//notes:method for Users
				for(int i=0;i<arrUsers.Length;i++)
				{
					string[] arrUsersNames = arrUsers[i].Trim().Split(',');
					string name="";
					string email="";
					if(arrUsersNames.Length==0)
						continue;
					else if(arrUsersNames.Length==1)
					{
						email=arrUsersNames[0];
					}
					else if(arrUsersNames.Length==2)
					{
						name=arrUsersNames[0];
						email=arrUsersNames[1];
					}
					DataRow drUser = dtUsers.NewRow();
					drUser[_userName] = name.Trim();
					drUser[_userFrom] = "";
					drUser[_userMail] = email.Trim();
					dtUsers.Rows.Add(drUser);
			
				}
				
				//notes:create dataview to set in report
				return new DataView(dtUsers);
			}
			catch(Exception ex)
			{
				log.Error(ex.Message);
				return new DataView();
			}
		}
		public static DataTable HotIssueCreateDVusers(int nProjectID)
		{
			DataTable dtUsers = new DataTable();
			dtUsers.Columns.Add(_userName);
			dtUsers.Columns.Add(_userFrom);
			dtUsers.Columns.Add(_userMail);
			
			SqlDataReader reader = null;

			try
			{
				reader = ProjectsData.SelectProject(nProjectID);
				if(reader.Read())
				{

					
					int clientID = reader.IsDBNull(5) ? 0 : reader.GetInt32(5);
					ClientData cd = ClientDAL.Load(clientID);
					if(cd!=null)
					{
						DataRow drClient = dtUsers.NewRow();
						drClient[_userName] = cd.ClientNameFirst;
						
						drClient[_userFrom] = cd.ClientName;
						drClient[_userMail] = cd.Email;
						dtUsers.Rows.Add(drClient);
					}
					int projectManagerID = reader.IsDBNull(11) ? 0 : reader.GetInt32(11);
					UserInfo ud = UsersData.SelectUserByID(projectManagerID);
					if(ud!=null && ud.UserID>0)
					{
						DataRow drUser = dtUsers.NewRow();
						drUser[_userName] = ud.FullName;
						drUser[_userFrom] = Resource.ResourceManager["HotIssue_ASAall"];
						drUser[_userMail] = ud.Mail;
						dtUsers.Rows.Add(drUser);
					}
				}
			}
			catch (Exception ex)
			{
				log.Info(ex);
				return null;
			}

			finally 
			{ 
				if (reader!=null) reader.Close(); 
			}
			ProjectUsersVector puv = ProjectUserDAL.LoadCollection("ProjectUsersSelByProjectProc",SQLParms.CreateProjectUsersSelByProjectProc(nProjectID));
			foreach(ProjectUserData pud in puv)
			{

				
				UserInfo ui = UsersData.SelectUserByID(pud.UserID);
				if(ui == null)
					continue;
				DataRow drUser = dtUsers.NewRow();
				drUser[_userName] = ui.FullName;
				drUser[_userFrom] = Resource.ResourceManager["HotIssue_ASAall"];
				drUser[_userMail] = ui.Mail;
				dtUsers.Rows.Add(drUser);
		
			}  
			ProjectClientsVector pcvClients=		ProjectClientDAL.LoadCollection("ProjectClientsSelByProjectProc",SQLParms.CreateProjectClientsSelByProjectProc(nProjectID,(int)ClientTypes.Client));
			
			ProjectSubcontractersVector psv= ProjectSubcontracterDAL.LoadCollection("ProjectSubcontractersListProc1",SQLParms.CreateProjectSubcontractersListProc1 (nProjectID,false));
			foreach(ProjectSubcontracterData pcd in psv)
			{
				DataRow drSubs = dtUsers.NewRow();
				SubcontracterData  sd = SubcontracterDAL.Load(pcd.SubcontracterID);
				if(sd!=null)
				{
					drSubs[_userName] = sd.SubcontracterNameFirst;
					drSubs[_userFrom] = sd.SubcontracterName;
					drSubs[_userMail] = sd.Email;
					dtUsers.Rows.Add(drSubs);
				}
			}
			ProjectClientsVector pcv=		ProjectClientDAL.LoadCollection("ProjectClientsSelByProjectProc",SQLParms.CreateProjectClientsSelByProjectProc(nProjectID,(int)ClientTypes.Builder));
			pcvClients.AddRange(pcv);
			ProjectClientsVector pcv1=		ProjectClientDAL.LoadCollection("ProjectClientsSelByProjectProc",SQLParms.CreateProjectClientsSelByProjectProc(nProjectID,(int)ClientTypes.Supervisors));
			pcvClients.AddRange(pcv1);
			foreach(ProjectClientData pcd in pcvClients)
			{
				
				ClientData datcl = ClientDAL.Load(pcd.ClientID);
				if(datcl == null)
					continue;
				DataRow drClient = dtUsers.NewRow();
				drClient[_userName] = datcl.ClientNameFirst;
//				if(datcl.ClientType==(int)ClientTypes.Builder)
//					drClient[_userName] +=Resource.ResourceManager["Name_Builder"];
				if(datcl.ClientType==(int)ClientTypes.Supervisors)
					drClient[_userName] +=Resource.ResourceManager["Name_Nadzor"];
				drClient[_userFrom] = datcl.ClientName;
				drClient[_userMail] = datcl.Email;
				dtUsers.Rows.Add(drClient);
			}
			return dtUsers;
		}
		public static DataView HotIssueCreateDVusers(string users,string subs,string clients,string other, bool bEnglish)
		{
			try
			{
				SubcontractersVector ssv = SubcontracterDAL.LoadCollection();
				ClientsVector cv = ClientDAL.LoadCollection(0);
				string[] arrUsers = users.Split(';');
				string[] arrSubs = subs.Split(';');
				string[] arrClients = clients.Split(';');
				string[] arrOther = other.Split(new char[]{';'});
				//notes:create table of project, so in report to show them with name and code
				DataTable dtUsers = new DataTable();
				dtUsers.Columns.Add(_userName);
				dtUsers.Columns.Add(_userFrom);
				dtUsers.Columns.Add(_userMail);
				//notes:method for Users
				foreach(string userID in arrUsers)
				{
					if(UIHelpers.ToInt(userID)==-1)
						continue;
					UserInfo ui = UsersData.SelectUserByID(UIHelpers.ToInt(userID));
					if(ui == null)
						continue;
					DataRow drUser = dtUsers.NewRow();
					
					if(bEnglish)
					{
						drUser[_userName] = ui.UserNameEN;
						drUser[_userFrom] = "ASA";
					}
					else
					{
						drUser[_userName] = ui.FullName;
						drUser[_userFrom] = Resource.ResourceManager["HotIssue_ASAall"];
					}
					drUser[_userMail] = ui.Mail;
					dtUsers.Rows.Add(drUser);
			
				}
				//notes:method for Subcontracters
				foreach(string subID in arrSubs)
				{
					if(UIHelpers.ToInt(subID)==-1)
						continue;
					SubcontracterData datsub = ssv.GetByID(UIHelpers.ToInt(subID));
					if(datsub == null||datsub.Hide)
						continue;
					DataRow drSubs = dtUsers.NewRow();
					if(bEnglish)
					{
						drSubs[_userName] = datsub.NameEN;
						drSubs[_userFrom] = datsub.SubcontracterNameEN;
					
					}
					else
					{
						drSubs[_userName] = datsub.SubcontracterNameFirst;
						drSubs[_userFrom] = datsub.SubcontracterName;
					}
					drSubs[_userMail] = datsub.Email;
					dtUsers.Rows.Add(drSubs);
			
				}
				//notes:method for Clients
				foreach(string clID in arrClients)
				{
					if(UIHelpers.ToInt(clID)==-1)
						continue;
					ClientData datcl = cv.GetByID(UIHelpers.ToInt(clID));
					if(datcl == null)
						continue;
					DataRow drClient = dtUsers.NewRow();
					if(bEnglish)
					{
						drClient[_userName] = datcl.RepresentativeEN;
						drClient[_userFrom] = datcl.ClientNameEN;
					}
					else
					{
						drClient[_userName] = datcl.ClientNameFirst;
						//					if(datcl.ClientType==(int)ClientTypes.Builder)
						//						drClient[_userName] +=Resource.ResourceManager["Name_Builder"];
						if(datcl.ClientType==(int)ClientTypes.Supervisors)
							drClient[_userName] +=Resource.ResourceManager["Name_Nadzor"];
						drClient[_userFrom] = datcl.ClientName;
					}
					drClient[_userMail] = datcl.Email;
					dtUsers.Rows.Add(drClient);
			
				}
				//notes:method for Clients
				foreach(string clID in arrOther)
				{
					string[] arrUsersNames = clID.Trim().Split(',');
					string name="";
					string email="";
					if(arrUsersNames.Length==0)
						continue;
					else if(arrUsersNames.Length==1)
					{
						name=arrUsersNames[0];
					}
					else if(arrUsersNames.Length==2)
					{
						name=arrUsersNames[0];
						email=arrUsersNames[1];
					}
					string[] company = name.Split('-');
					string companyName="";
					if(company.Length>1)
					{
						name=company[0];
						companyName=company[1];
					}
					DataRow drClient = dtUsers.NewRow();
					drClient[_userName] = name.Trim();
					drClient[_userFrom] = companyName;
					drClient[_userMail] = email;
					dtUsers.Rows.Add(drClient);
			
				}
				//notes:create dataview to set in report
				return new DataView(dtUsers);
			}
			catch(Exception ex)
			{
				log.Error(ex.Message);
				return new DataView();
			}
		}
	
		public static HotIssuesVector HotIssueCreateHotIssuesVector(MeetingData dat,MeetingProjectsVector mpv, bool bAll,bool bOnlyMeeting, bool bEnglish, int nProjectID)
		{
			try
			{
				int nDays=int.Parse(System.Configuration.ConfigurationManager.AppSettings["EditDays"]);
				HotIssuesVector vecAll = null;
				if(bOnlyMeeting)
					vecAll = HotIssueDAL.LoadCollectionForMeeting(nProjectID,dat.MeetingDate.AddYears(nDays),dat.ASA,dat.MeetingID, -1,  -1, dat.MeetingDate.AddDays(-nDays), bAll,bOnlyMeeting,bEnglish,dat.ProjectID!=nProjectID,false);
				else
					vecAll = HotIssueDAL.LoadCollectionForMeeting(nProjectID,dat.MeetingDate.AddYears(1),dat.ASA,dat.MeetingID, -1,  -1,  Constants.DateMax, bAll,bOnlyMeeting,bEnglish,dat.ProjectID!=nProjectID,false);
				
//				foreach(MeetingProjectData mpd in mpv)
//				{
//					HotIssuesVector vec = HotIssueDAL.LoadCollectionForMeeting(mpd.ProjectID,dat.MeetingDate.AddDays(1),dat.ASA);
//					vecAll.AddRange(vec);
//				}		
				//notes:create dataview to set in report
				return vecAll;
			}
			catch(Exception ex)
			{
				log.Error(ex.Message);
				return new HotIssuesVector();
			}
		}
		public static HotIssuesVector HotIssueCreateHotIssuesVector(int nProjectID, bool bASA, int nCategoryID, int nStatusID, DateTime dtStart, DateTime dtEnd, bool bAll, bool bOnlyMeeting)
		{
			try
			{
				int nDays=int.Parse(System.Configuration.ConfigurationManager.AppSettings["EditDays"]);
				HotIssuesVector vecAll = HotIssueDAL.LoadCollectionForMeeting(nProjectID,DateTime.Now.Date.AddDays(nDays),bASA,-1,nCategoryID,nStatusID,dtStart,bAll,bOnlyMeeting,false,false,false);
				
				return vecAll;
			}
			catch(Exception ex)
			{
				log.Error(ex.Message);
				return new HotIssuesVector();
			}
		}
		#endregion

		#region Users
		
		public static void SetUserOccupationDDL(DropDownList ddl)
		{
			ListItem li = new ListItem();
			ddl.Items.Add(li);
			li = new ListItem(Resource.ResourceManager["EditUserOccupationAchitekt"],Resource.ResourceManager["EditUserOccupationAchitekt"]);
			ddl.Items.Add(li);
			li = new ListItem(Resource.ResourceManager["EditUserOccupationIngener"],Resource.ResourceManager["EditUserOccupationIngener"]);
			ddl.Items.Add(li);
			li = new ListItem(Resource.ResourceManager["EditUserOccupationTechnic"],Resource.ResourceManager["EditUserOccupationTechnic"]);
			ddl.Items.Add(li);
		}


		#endregion
	}
	public enum BuildingGridTitleNames1
	{
		A,
		B,
		C,
		D,
		E,
		F,
		G,
		H,
		I,
		J,
		K
	}
	
}
