﻿<%@ Register TagPrefix="radg" Namespace="Telerik.WebControls" Assembly="RadGrid" %>
<%@ Register TagPrefix="uc1" TagName="PageHeader" Src="UserControls/PageHeader.ascx" %>
<%@ Page language="c#" Codebehind="Schedule.aspx.cs" AutoEventWireup="false" Inherits="Asa.Timesheet.WebPages.Schedule" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
	<head runat="server">
		<title>Schedule</title>
		<meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
		<meta content="C#" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<LINK href="styles/timesheet.css" type="text/css" rel="stylesheet">
		<script language="javascript" src="PopupCalendar.js"></script>
	</HEAD>
	<body bottomMargin="0" leftMargin="0" topMargin="0" rightMargin="0">
		<form id="Form1" method="post" runat="server">
			<table height="100%" cellSpacing="0" cellPadding="0" width="100%" border="0">
				<TBODY>
					<tr height="1">
						<td><uc1:pageheader id="header" runat="server"></uc1:pageheader></td>
					</tr>
					<tr>
						<td>
							<table height="100%" cellSpacing="0" cellPadding="0" width="100%" border="0">
								<TBODY>
									<tr>
										<td class="menuLeft"><asp:placeholder id="menuHolder" runat="server"></asp:placeholder></td>
										
										<td style="PADDING-LEFT: 5px; PADDING-RIGHT: 5px; PADDING-TOP: 3px" vAlign="top" noWrap>
											<TABLE id="tblInfo" height="30" cellSpacing="0" cellPadding="3" width="880" border="0">
												<TR>
													<td vAlign="middle" noWrap><asp:label id="lblError" runat="server" CssClass="ErrorLabel" EnableViewState="False" ForeColor="Red"></asp:label><asp:label id="lblInfo" runat="server" CssClass="InfoLabel" EnableViewState="False"></asp:label></TD>
												</TR>
											</TABLE>
											<TABLE id="Table3" cellSpacing="0" cellPadding="0" width="880" border="0">
												<tr>
													<td>
														<table style="WIDTH: 728px" cellSpacing="0" cellPadding="3" border="0">
															<TR>
																<td style="WIDTH: 139px; HEIGHT: 21px" vAlign="top"><asp:label id="lbMountCount" runat="server" CssClass="enterDataLabel">Брой месеци:</asp:label></TD>
																<td style="WIDTH: 274px; HEIGHT: 21px" vAlign="top"><asp:dropdownlist id="ddlMonthCount" runat="server" CssClass="EnterDataBox" Width="50px" AutoPostBack="false"></asp:dropdownlist>&nbsp;<asp:dropdownlist id="ddlDaysOrWeek" runat="server" CssClass="EnterDataBox" Width="195px" AutoPostBack="True"></asp:dropdownlist></TD>
																<td style="HEIGHT: 21px" vAlign="top"></TD>
															</TR>
															<TR>
																<td style="WIDTH: 139px" vAlign="top"><asp:label id="lblBuildingType" runat="server" CssClass="enterDataLabel">Вид сграда:</asp:label></TD>
																<td style="WIDTH: 274px" vAlign="top"><asp:dropdownlist id="ddlBuildingType" runat="server" CssClass="EnterDataBox" Width="250px" AutoPostBack="True"></asp:dropdownlist></TD>
																<td vAlign="top"><asp:imagebutton id="ibGenerate" runat="server" ImageUrl="images/ie.gif" Visible="False"></asp:imagebutton></NOBR><asp:imagebutton id="ibXlsExport" runat="server" ImageUrl="images/xls.gif" ToolTip="График"></asp:imagebutton></TD>
															</TR>
														</table>
														<TABLE id="Table5" cellSpacing="0" cellPadding="0" width="100%" border="0">
															<TR>
																<td align="center"><asp:label id="lblReportTitle" runat="server" EnableViewState="False" Visible="False" Font-Bold="True"
																		Font-Size="11pt">Активни Проекти</asp:label></TD>
															</TR>
														</TABLE>
														<asp:button style="Z-INDEX: 0" id="btnUncheck" CssClass="ActionButton" Width="160px" Runat="server"
															text="Изключи всички"></asp:button>
													</td>
												</tr>
												<TR>
													<td style="HEIGHT: 5px" colSpan="2"><asp:datagrid id="grdReport" runat="server" CssClass="ReportGrid" ForeColor="DimGray" Width="100%"
															Visible="False" CellPadding="4" ShowHeader="True" AutoGenerateColumns="False">
															<HeaderStyle CssClass="reportGridHeader"></HeaderStyle>
															<Columns>
																<asp:TemplateColumn HeaderText="Включени проекти в графика" HeaderStyle-Width="150px">
																	<ItemStyle HorizontalAlign="Center"></ItemStyle>
																	<ItemTemplate>
																		<asp:CheckBox ID="cbShow" Runat="server" Checked="True"></asp:CheckBox>
																	</ItemTemplate>
																</asp:TemplateColumn>
																<asp:BoundColumn DataField="ProjectName" HeaderText="Име на проект"></asp:BoundColumn>
															</Columns>
														</asp:datagrid><br>
														<asp:datagrid id="grdReport1" runat="server" CssClass="ReportGrid" ForeColor="DimGray" Width="100%"
															CellPadding="4" ShowHeader="True" AutoGenerateColumns="False">
															<HeaderStyle CssClass="reportGridHeader"></HeaderStyle>
															<Columns>
																<asp:BoundColumn DataField="ProjectScheduleID" HeaderText="schID" Visible="False"></asp:BoundColumn>
																<asp:BoundColumn DataField="ProjectID" HeaderText="pID" Visible="False"></asp:BoundColumn>
																<asp:BoundColumn DataField="SubcontracterID" HeaderText="subID" Visible="False"></asp:BoundColumn>
																<asp:BoundColumn DataField="IsProjectMain" HeaderText="pm" Visible="False"></asp:BoundColumn>
																<asp:TemplateColumn HeaderText="Включени проекти в графика" HeaderStyle-Width="10%">
																	<ItemStyle HorizontalAlign="Center"></ItemStyle>
																	<ItemTemplate>
																		<asp:CheckBox ID="cbShow1" Runat="server" Checked="True" Visible='<%# GetVisible(DataBinder.Eval(Container, "DataItem.ProjectScheduleID"))%>'>
																		</asp:CheckBox>
																	</ItemTemplate>
																</asp:TemplateColumn>
																<asp:BoundColumn DataField="ProjectName" HeaderText="Име на проект" HeaderStyle-Width="30%"></asp:BoundColumn>
																<asp:BoundColumn DataField="SubcontracterName" HeaderText="Име на подизпълнител" HeaderStyle-Width="30%"></asp:BoundColumn>
																<asp:TemplateColumn HeaderText="Начална дата" HeaderStyle-Width="15%">
																	<ItemStyle HorizontalAlign="Center"></ItemStyle>
																	<ItemTemplate>
																		<asp:TextBox ID="txtStartDate" Runat="server" Visible='<%# GetVisibleForTextBox(DataBinder.Eval(Container, "DataItem.ProjectScheduleID"))%>' text='<%# GetDateFromDB(DataBinder.Eval(Container, "DataItem.StartDate")) %>' Width="80px" >
																		</asp:TextBox>&nbsp;<IMG id="lkCalendar1" alt="Calendar" src="images/calendar.ico" width="18" align="absBottom"
																			runat="server" Visible='<%# GetVisibleForTextBox(DataBinder.Eval(Container, "DataItem.ProjectScheduleID"))%>'>
																	</ItemTemplate>
																</asp:TemplateColumn>
																<asp:TemplateColumn HeaderText="Крайна дата" HeaderStyle-Width="15%">
																	<ItemStyle HorizontalAlign="Center"></ItemStyle>
																	<ItemTemplate>
																		<asp:TextBox ID="txtEndDate" Runat="server" Visible='<%# GetVisibleForTextBox(DataBinder.Eval(Container, "DataItem.ProjectScheduleID"))%>' text='<%# GetDateFromDB(DataBinder.Eval(Container, "DataItem.EndDate")) %>' Width="80px" >
																		</asp:TextBox>&nbsp;<IMG id="lkCalendar2" alt="Calendar" src="images/calendar.ico" width="18" align="absBottom"
																			runat="server" Visible='<%# GetVisibleForTextBox(DataBinder.Eval(Container, "DataItem.ProjectScheduleID"))%>'>
																	</ItemTemplate>
																</asp:TemplateColumn>
																<asp:TemplateColumn HeaderText="Забележки" HeaderStyle-Width="15%">
																	<ItemStyle HorizontalAlign="Center"></ItemStyle>
																	<ItemTemplate>
																		<asp:TextBox ID="txtNotes" Runat="server" Visible='<%# GetVisibleForTextBox(DataBinder.Eval(Container, "DataItem.ProjectScheduleID"))%>' text='<%# DataBinder.Eval(Container, "DataItem.Notes") %>' Width="80px" >
																		</asp:TextBox>
																	</ItemTemplate>
																</asp:TemplateColumn>
															</Columns>
														</asp:datagrid><br>
														<asp:label id="lblNoDataFound" runat="server" CssClass="InfoLabel" EnableViewState="False"
															Visible="False">Не са намерени данни съответстващи на избрания филтър.</asp:label></TD>
												</TR>
												<TR>
													<td colSpan="2"><IMG height="1" src="../images/dot.gif" width="100%"></TD>
												</TR>
												<TR>
													<td><asp:button id="btnSave" CssClass="ActionButton" text="Запиши" Runat="server"></asp:button></TD>
												</TR>
												<TR>
													<td colSpan="2" height="3"></TD>
												</TR>
												<TR>
													<td colSpan="2"><IMG height="1" src="images/dot.gif" width="100%"></TD>
												</TR>
											</TABLE>
										</td>
									</tr>
								</TBODY>
							</table>
						</td>
					</tr>
				</TBODY>
			</table>
		</form>
		</TR></TBODY></TABLE>
	</body>
</HTML>
