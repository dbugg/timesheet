﻿<%@ Page Title="Нов файл" Language="C#" MasterPageFile="~/PM.Master" AutoEventWireup="True" CodeBehind="EditFile.aspx.cs" Inherits="PM.EditFile" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
  
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

<table height="100%" cellSpacing="0" cellPadding="0" width="100%" border="0">
				<TBODY>

					<tr>
						<td>
							<table height="100%" cellSpacing="0" cellPadding="0" width="100%" border="0">
								<tr>
									<td style="PADDING-RIGHT: 5px; PADDING-LEFT: 5px; PADDING-TOP: 3px" vAlign="top" noWrap>
										<table>
											<tr>
												<td vAlign="middle"><asp:linkbutton id="linkBack" Runat="server" CssClass="Brown" onclick="link_Click"></asp:linkbutton></td>
											</tr>
										</table>
										<P><asp:label id="lblError" runat="server" CssClass="ErrorLabel" ForeColor="Red" EnableViewState="False"></asp:label><asp:label id="lblInfo" runat="server" CssClass="InfoLabel" EnableViewState="False"></asp:label><BR>
											<table id="tblForm" cellSpacing="0" cellPadding="3" width="701" border="0">
												<tr>
													<td style="WIDTH: 225px"><asp:label id="lbProjectName" runat="server" CssClass="enterDataLabel" EnableViewState="False"
															Width="224px" Font-Bold="True"></asp:label></td>
													<td style="WIDTH: 478px"><asp:label id="txtProjectName" runat="server" CssClass="enterDataBox" Width="350px"></asp:label></TD>
												</tr>
												<tr>
													<td style="WIDTH: 225px"><asp:label id="lblCategoryName" runat="server" CssClass="enterDataLabel" EnableViewState="False"
															Width="224px" Font-Bold="True"></asp:label></td>
													<td style="WIDTH: 478px"><asp:label id="txtCategoryName" runat="server" CssClass="enterDataBox" Width="350px"></asp:label></TD>
												</tr>
												<tr>
													<td style="WIDTH: 225px"><asp:label id="lbFile" runat="server" CssClass="enterDataLabel" EnableViewState="False" Width="224px"
															Font-Bold="True"></asp:label></td>
													<td style="WIDTH: 478px"><INPUT id="FileCtrl" style="WIDTH: 350px; HEIGHT: 20px" type="file" size="39" name="File1"
															runat="server"></TD>
												</tr>
												<tr>
													<td style="WIDTH: 225px"><asp:label id="lbFileSubject" runat="server" CssClass="enterDataLabel" EnableViewState="False"
															Width="224px" Font-Bold="True"></asp:label></td>
													<td style="WIDTH: 478px"><asp:textbox id="txtFileSubject" runat="server" CssClass="enterDataBox" Width="350px" TextMode="MultiLine"
															Rows="2"></asp:textbox>&nbsp;<IMG height="16" alt="" src="images/required1.gif" width="16"></TD>
												</tr>
												<tr>
													<td style="WIDTH: 225px"><asp:label id="lbFileMinutes" runat="server" CssClass="enterDataLabel" EnableViewState="False"
															Width="224px" Font-Bold="True"></asp:label></td>
													<td style="WIDTH: 478px"><asp:textbox id="txtFileMinutes" runat="server" CssClass="enterDataBox" Width="550px" TextMode="MultiLine"
															Rows="3" Height="100px"></asp:textbox></TD>
												</tr>
												<tr>
													<td style="WIDTH: 225px"><asp:label id="lbFileVersion" runat="server" CssClass="enterDataLabel" EnableViewState="False"
															Width="224px" Font-Bold="True"></asp:label></td>
													<td style="WIDTH: 478px"><asp:label id="txtFileVersion" runat="server" CssClass="enterDataBox" Width="350px"></asp:label></TD>
												</tr>
												<tr>
													<td style="WIDTH: 225px"><asp:label id="lbType" runat="server" CssClass="enterDataLabel" EnableViewState="False" Width="224px"
															Font-Bold="True"></asp:label></td>
													<td style="WIDTH: 478px"><asp:label id="txtType" runat="server" CssClass="enterDataBox" Width="350px"></asp:label><asp:label id="txtFileNameSave" runat="server" CssClass="enterDataBox" Width="350px" Visible="False"></asp:label></TD>
												</tr>
												<tr>
													<td style="WIDTH: 225px"><asp:label id="lbMail" runat="server" CssClass="enterDataLabel" EnableViewState="False" Width="224px"
															Font-Bold="True"></asp:label></td>
													<td style="WIDTH: 478px">
                                                                                                 	<asp:datagrid id="grdProjects" runat="server" CssClass="Grid" Width="100%" 
																ShowFooter="false" AutoGenerateColumns="False" CellPadding="4" PageSize="2" AllowSorting="false">
																<ItemStyle CssClass="GridItem"></ItemStyle>
																<Columns>

																	<asp:BoundColumn DataField="Name" HeaderText="<i class='fas fa-user'></i>" ></asp:BoundColumn>
															
																	<asp:BoundColumn DataField="Email" HeaderText="<i class='fas fa-at'></i>"></asp:BoundColumn>

																	<asp:BoundColumn DataField="Type" HeaderText=""></asp:BoundColumn>
														
																	<asp:TemplateColumn HeaderText="<i class='fas fa-envelope'></i>" >
                                                                    <ItemTemplate>
                                                                        <asp:CheckBox runat="server" id="selector"/>
                                                                    </ItemTemplate>
                                                                    </asp:TemplateColumn>
                                                                    <asp:BoundColumn DataField="Id" Visible=false />
                                                                    <asp:BoundColumn DataField="InternalType" Visible=false />
																</Columns>
                                                                
															</asp:datagrid><asp:checkboxlist Visible=false id="cblMail" runat="server" CssClass="enterDataBox" Width="450px" RepeatDirection="Vertical"></asp:checkboxlist>
                                                    </TD>
												</tr>
												<tr>
													<td style="WIDTH: 225px"><asp:label id="lbLaterMail" runat="server" CssClass="enterDataLabel" EnableViewState="False"
															Width="224px" Font-Bold="True"></asp:label></td>
													<td style="WIDTH: 478px">&nbsp;<asp:checkbox id="cbLaterMail" runat="server" CssClass="enterDataBox" Width="350px"></asp:checkbox></TD>
												</tr>
												<tr>
													<td style="WIDTH: 225px"><asp:label id="lbVisibles" runat="server" CssClass="enterDataLabel" EnableViewState="False"
															Font-Bold="True"></asp:label></td>
													<td style="WIDTH: 478px"><asp:checkboxlist id="cblVisibles" runat="server" CssClass="enterDataBox" Width="350px" RepeatDirection="Horizontal"></asp:checkboxlist></TD>
												</tr>
											</table>
											<TABLE id="Table5" cellSpacing="0" cellPadding="0" width="100%" border="0">
												<TR>
													<td><asp:button id="btnSave" runat="server" CssClass="ActionButton Default" onclick="btnSave_Click"></asp:button></TD>
												</TR>
												<tr>
													<td style="HEIGHT: 20px"></TD>
												</tr>
												<TR>
													<td><asp:label id="lbAskedToAchive" runat="server" EnableViewState="False" Width="100%" Font-Bold="True"></asp:label></TD>
													<P></P>
												</TR>
											</TABLE>
											<TABLE id="Table6" cellSpacing="0" cellPadding="0" width="100%" border="0">
												<TR>
													<td style="HEIGHT: 20px"></TD>
												</TR>
												<TR>
													<td style="HEIGHT: 20px"><asp:button id="btnShowArchive" runat="server" CssClass="ActionButton" Width="144px" onclick="btnShowArchive_Click"></asp:button></TD>
												</TR>
												<TR>
													<td style="HEIGHT: 20px"></TD>
												</TR>
												<TR>
													<td style="HEIGHT: 15px"><asp:label id="lbArchive" runat="server" EnableViewState="False" Width="100%" Font-Bold="True"></asp:label></TD>
												</TR>
												<TR>
													<td style="HEIGHT: 20px"></TD>
												</TR>
												<TR>
													<td><asp:panel id="grid" 
															runat="server" Width="98%" Height="200px">
															<asp:datagrid id="grdFiles" runat="server" CssClass="Grid" Width="100%" ShowHeader="false"
																ShowFooter="false" AutoGenerateColumns="False" CellPadding="4" PageSize="2" AllowSorting="false">
																<ItemStyle CssClass="GridItem"></ItemStyle>
																<Columns>
																	<asp:TemplateColumn SortExpression="FileNameSave">
																		<ItemStyle Width="40%"></ItemStyle>
																		<ItemTemplate>
																			<asp:LinkButton id="lnk" runat="server" CssClass="menuTable" ForeColor="#804000" CommandName="Edit"></asp:LinkButton>
																		</ItemTemplate>
																	</asp:TemplateColumn>
																	<asp:BoundColumn DataField="FileName" Visible="False"></asp:BoundColumn>
																</Columns>
															</asp:datagrid>
														</asp:panel></TD>
												</TR>

											</TABLE>
										</P>
									</td>
								</tr>
							</table>
		</form>
		</TD></TR></TBODY></TABLE>
</asp:Content>