using System;
using System.Data;
using System.Data.SqlClient; 
using System.Collections;
using Microsoft.ApplicationBlocks.Data;

namespace Asa.Timesheet.Data
{
	public class ProjectUserDAL
	{
		/// <summary>
		/// Loads a ProjectUserData object from the database using the given ProjectUserID
		/// </summary>
		/// <param name="projectUserID">The primary key value</param>
		/// <returns>A ProjectUserData object</returns>
		public static ProjectUserData Load(int projectUserID)
		{
			SqlParameter[] parameterValues = new SqlParameter[] { new SqlParameter("@ProjectUserID", projectUserID) };
			using (SqlDataReader reader = SqlHelper.ExecuteReader(CommonDAL.ConnectionString, CommandType.StoredProcedure, "ProjectUsersSelProc", parameterValues))
			{
				if (reader.Read())
				{
					ProjectUserData result = new ProjectUserData();
					LoadFromReader(reader,result);
					return result;
				}
				else
					return null;
			}
		}

		/// <summary>
		/// Loads a ProjectUserData object from the database using the given where clause
		/// </summary>
		/// <param name="whereClause">The filter expression for the query</param>
		/// <returns>A ProjectUserData object</returns>
		public static ProjectUserData Load(string whereClause)
		{
			SqlParameter[] parameterValues = new SqlParameter[] { new SqlParameter("@WhereClause", whereClause) };
			using (SqlDataReader reader = SqlHelper.ExecuteReader(CommonDAL.ConnectionString, CommandType.StoredProcedure, "ProjectUsersListProc", parameterValues))
			{
				if (reader.Read())
				{
					ProjectUserData result = new ProjectUserData();
					LoadFromReader(reader,result);
					return result;
				}
				else
					return null;
			}
		}
		public static int Save( ProjectUserData source)
		{
			if (source.ProjectUserID == -1)
				return Insert(source);
			else
				return Update(source);	
		}

		public static void Delete( ProjectUserData source)
		{
			Delete(source.ProjectUserID);
		}
		/// <summary>
		/// Loads a collection of ProjectUserData objects from the database.
		/// </summary>
		/// <returns>A collection containing all of the ProjectUserData objects in the database.</returns>
		public static ProjectUsersVector LoadCollection()
		{
			SqlParameter[] parms = new SqlParameter[] {};
			return LoadCollection("ProjectUsersListProc", parms);
		}
	  
		/// <summary>
		/// Saves a collection of ProjectUserData objects to the database.
		/// </summary>
		/// <returns>A collection containing all of the ProjectUserData objects in the database.</returns>
		public static  void  SaveCollection(ProjectUsersVector col,string spName, SqlParameter[] parms)
		{
			ProjectUsersVector colOld = LoadCollection(spName,parms);
			UpdateCollection(colOld,col);
		}
		public static  void  SaveCollection(ProjectUsersVector col)
		{
			ProjectUsersVector colOld = LoadCollection();
			UpdateCollection(colOld,col);
		}
		public static  void  SaveCollection(ProjectUsersVector col, string whereClause )
		{
			ProjectUsersVector colOld = LoadCollection(whereClause);
			UpdateCollection(colOld,col);
		}
		/// <summary>
		/// Loads a collection of ProjectUserData objects from the database.
		/// </summary>
		/// <returns>A collection containing all of the ProjectUserData objects in the database.</returns>
		public static ProjectUsersVector LoadCollection(string whereClause)
		{
			SqlParameter[] parms = new SqlParameter[] { new SqlParameter("@WhereClause", whereClause) };
			return LoadCollection("ProjectUsersListProc", parms);
		}

		/// <summary>
		/// Loads a collection of ProjectUserData objects from the database.
		/// </summary>
		/// <returns>A collection containing all of the ProjectUserData objects in the database.</returns>
		public static ProjectUsersVector LoadCollection(string spName, SqlParameter[] parms)
		{
			ProjectUsersVector result = new ProjectUsersVector();
			using (SqlDataReader reader = SqlHelper.ExecuteReader(CommonDAL.ConnectionString, CommandType.StoredProcedure, spName, parms))
			{
				while (reader.Read())
				{
					ProjectUserData tmp = new ProjectUserData();
					LoadFromReader(reader, tmp);
					result.Add(tmp);
				}
			}
			return result;
		}
        public static ProjectUsersVector LoadCollection2(string spName, SqlParameter[] parms)
        {
            ProjectUsersVector result = new ProjectUsersVector();
            using (SqlDataReader reader = SqlHelper.ExecuteReader(CommonDAL.ConnectionString, CommandType.StoredProcedure, spName, parms))
            {
                while (reader.Read())
                {
                    ProjectUserData2 tmp = new ProjectUserData2();
                    LoadFromReader(reader, tmp);
                    if (!reader.IsDBNull(3))
                    {
                        tmp.Name = reader.GetString(3);
                        tmp.Email = reader.GetString(4);
                    }
                    result.Add(tmp);
                }
            }
            return result;
        }
		/// <summary>
		/// Deletes a ProjectUserData object from the database.
		/// </summary>
		/// <param name="projectUserID">The primary key value</param>
		public static void Delete(int projectUserID)
		{
			SqlParameter[] parameterValues = new SqlParameter[] { new SqlParameter("@ProjectUserID", projectUserID) };
			SqlHelper.ExecuteNonQuery(CommonDAL.ConnectionString, CommandType.StoredProcedure, "ProjectUsersDelProc", parameterValues);
		}

		public static void LoadFromReader(SqlDataReader reader, ProjectUserData target)
		{
			if (reader != null && !reader.IsClosed)
			{
				//target.Key = reader.GetInt32(0);
				if (!reader.IsDBNull(0)) target.ProjectUserID = reader.GetInt32(0);
				if (!reader.IsDBNull(1)) target.ProjectID = reader.GetInt32(1);
				if (!reader.IsDBNull(2)) target.UserID = reader.GetInt32(2);
			}
		}
		
	  
		public static int Insert( ProjectUserData source)
		{
			SqlParameter[] parameterValues = GetInsertParameterValues(source);
			SqlHelper.ExecuteNonQuery(CommonDAL.ConnectionString, CommandType.StoredProcedure, "ProjectUsersInsProc", parameterValues);
			source.ProjectUserID = (int) parameterValues[0].Value;
			return source.ProjectUserID;
		}

		public static int Update( ProjectUserData source)
		{
			SqlParameter[] parameterValues = GetUpdateParameterValues(source);
			SqlHelper.ExecuteNonQuery(CommonDAL.ConnectionString, CommandType.StoredProcedure,
				"ProjectUsersUpdProc", parameterValues);
			return source.ProjectUserID;
		}
		private static void UpdateCollection(ProjectUsersVector colOld,ProjectUsersVector col)
		{
			// Delete all old items
			foreach( ProjectUserData itemOld in colOld)
			{
				if(!ExistsKey(itemOld.ProjectUserID, col))
					Delete(itemOld.ProjectUserID);
			}
			foreach( ProjectUserData item in col)
			{
				if(item.ProjectUserID <= -1)
					Insert(item);
				else
					Update(item);
			}
		}
		private static bool ExistsKey(int Key, ProjectUsersVector col)
		{
			foreach( ProjectUserData item in col)
				if(item.ProjectUserID==Key)
					return true;
			return false;
		}

		private static SqlParameter GetSqlParameter(string name, ParameterDirection direction, object value)
		{
			SqlParameter p = new SqlParameter(name, value);
			p.Direction = direction;
			return p;
		}

		private static SqlParameter[] GetInsertParameterValues( ProjectUserData source)
		{
			SqlParameter[] prams = new SqlParameter[3];
			prams[0] = GetSqlParameter("@ProjectUserID", ParameterDirection.Output, source.ProjectUserID);
			prams[1] = GetSqlParameter("@ProjectID", ParameterDirection.Input, source.ProjectID);
			prams[2] = GetSqlParameter("@UserID", ParameterDirection.Input, source.UserID);
			return prams;
		}

		private static SqlParameter[] GetUpdateParameterValues( ProjectUserData source)
		{
			return new SqlParameter[] {
										  new SqlParameter("@ProjectUserID", source.ProjectUserID),
										  new SqlParameter("@ProjectID", source.ProjectID),
										  new SqlParameter("@UserID", source.UserID)
									  };
		}
	}
}   

