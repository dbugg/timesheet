using System;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using Asa.Timesheet.Data;
using log4net;
using Asa.Timesheet.WebPages.UserControls;




namespace Asa.Timesheet.WebPages
{
	/// <summary>
	/// Summary description for Authors.
	/// </summary>
	public class Contracts : TimesheetPageBase
	{
		protected System.Web.UI.WebControls.PlaceHolder menuHolder;
		protected System.Web.UI.WebControls.Label lblError;
		protected System.Web.UI.WebControls.Label lblInfo;
		protected System.Web.UI.WebControls.Panel grid;
		protected System.Web.UI.WebControls.Button btnProject;
		protected Asa.Timesheet.WebPages.UserControls.PageHeader header;
		protected System.Web.UI.WebControls.Label Label6;
		protected System.Web.UI.WebControls.Label lbScanDocNew;
		protected System.Web.UI.WebControls.Button btnScanDocAdd;
		protected System.Web.UI.WebControls.DataList dlDocs;
		protected System.Web.UI.HtmlControls.HtmlInputFile FileCtrl;
		protected System.Web.UI.WebControls.Label Label1;
		protected System.Web.UI.WebControls.TextBox txtContractName;
		private static readonly ILog log = LogManager.GetLogger(typeof(Contracts));

        private void Page_Load(object sender, System.EventArgs e)
        {
            lblError.Visible = false;
            if (!IsPostBack)
            {

                int nID = UIHelpers.GetPIDParam();

                header.UserName = this.LoggedUser.UserName;
                int PID = UIHelpers.GetPIDParam();
                string projectName, projectCode, administrativeName, add;
                decimal area = 0;
                int clientID = 0;
                bool phases; bool isPUP;
                if (!ProjectsData.SelectProject(PID, out projectName, out  projectCode, out administrativeName, out area, out add,
                    out clientID, out phases, out isPUP))
                {
                    log.Error("Project not found " + PID);

                }
                else
                {
                    header.PageTitle = Resource.ResourceManager["TitleContracts"] + " - " + projectName;//projectCode+ " "+administrativeName;
                }
                BindGrid();

            }
            if (!(LoggedUser.IsLayer || LoggedUser.IsSecretary || LoggedUser.HasPaymentRights))
            {
                ErrorRedirect(Resource.ResourceManager["noRightsAlert"]);
            }

            UIHelpers.CreateMenu(menuHolder, LoggedUser);


        }

	
		#region Web Form Designer generated code
        override protected void OnInit(EventArgs e)
        {
            //
            // CODEGEN: This call is required by the ASP.NET Web Form Designer.
            //
            InitializeComponent();
            base.OnInit(e);
        }
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
        private void InitializeComponent()
        {
            this.btnScanDocAdd.Click += new System.EventHandler(this.btnScanDocAdd_Click);
            this.dlDocs.ItemCommand += new System.Web.UI.WebControls.DataListCommandEventHandler(this.dlDocs_ItemCommand);
            this.btnProject.Click += new System.EventHandler(this.btnProject_Click);
            this.Load += new System.EventHandler(this.Page_Load);

        }
		#endregion

        private void btnProject_Click(object sender, System.EventArgs e)
        {
            Response.Redirect("EditProject.aspx?pid=" + UIHelpers.GetPIDParam());
        }


        public void SaveContract()
        {
            try
            {
                int ID = UIHelpers.GetPIDParam();
                if (FileCtrl.PostedFile.FileName.Length != 0)
                {
                    string name = FileCtrl.PostedFile.FileName;
                    int n = name.LastIndexOf(".");
                    if (n != -1)
                    {
                        string ext = name.Substring(n);
                        if (ext.ToLower() == System.Configuration.ConfigurationManager.AppSettings["Extension"].ToLower())
                        {
                            ProjectContractData mdd = new ProjectContractData(-1, ID, txtContractName.Text);
                            ProjectContractDAL.Save(mdd);
                            log.Info(string.Format("Contract {0} of project {2} has been INSERTED by {1}", mdd.ProjectContractID, string.Concat(LoggedUser.FullName, " ( userID ", LoggedUser.UserID, " )"), UIHelpers.GetPIDParam().ToString()));
                            string path = string.Concat(System.Configuration.ConfigurationManager.AppSettings["ScanPathContracts"], mdd.ProjectContractID, ext);
                            string mapped = Request.MapPath(path);
                            FileCtrl.PostedFile.SaveAs(mapped);
                            //						UserInfo Director = UsersData.UsersListByRoleProc(1);
                            //						
                            //						MailBO.SendMailMinutes(Director.Mail, mapped,ddlProject.SelectedItem.Text,txtStartDate.Text);
                        }
                    }
                }
            }
            catch (Exception e)
            {
                log.Error(e.Message);
                lblError.Text = "Error";
                lblError.Visible = true;
            }
        }
        protected string GetURL(int ID)
        {
            return string.Concat(System.Configuration.ConfigurationManager.AppSettings["ScanPathContracts"], ID, System.Configuration.ConfigurationManager.AppSettings["Extension"]);
        }

        protected bool GetVis()
        {
            if (LoggedUser.HasPaymentRights)
                return true;
            else
                return false;
        }
        private void BindGrid()
        {

            int ID = UIHelpers.GetPIDParam();
            ProjectContractsVector ppv = ProjectContractDAL.LoadCollection("ProjectContractsListByProjectProc", SQLParms.CreateProjectContractsListByProjectProc(ID));
            dlDocs.DataSource = ppv;
            dlDocs.DataKeyField = "ProjectContractID";
            dlDocs.DataBind();

            //			if (!LoggedUser.HasPaymentRights)
            ////				grdMeetings.Columns[(int)GridColumns.DeleteItem].Visible = false;
            //				dlDocs.GridLines[GridLines.Horizontal].Visible = false;
            try
            {
                for (int i = 0; i < dlDocs.Items.Count; i++)
                {
                    string js = string.Concat("return confirm('", Resource.ResourceManager["grid_ConfirmDeleteForProtokols"], " ?');");
                    System.Web.UI.WebControls.WebControl btn = (System.Web.UI.WebControls.WebControl)dlDocs.Items[i].FindControl("btnDel");
                    btn.Attributes["onclick"] = js;
                    System.Web.UI.WebControls.WebControl btnPDF = (System.Web.UI.WebControls.WebControl)dlDocs.Items[i].FindControl("btnScan");
                    btnPDF.ToolTip = ppv[i].ProjectContractName;
                }
            }
            catch
            {

            }

        }

        private void btnScanDocAdd_Click(object sender, System.EventArgs e)
        {
            try
            {
                SaveContract();
                BindGrid();
            }
            catch
            {
                lblError.Text = "error";
            }
        }


        private void dlDocs_ItemCommand(object source, System.Web.UI.WebControls.DataListCommandEventArgs e)
        {
            ImageButton ib = (ImageButton)e.CommandSource;
            string s = ib.ID;
            switch (ib.ID)
            {
                case "btnDel":
                    int ID = (int)dlDocs.DataKeys[e.Item.ItemIndex];
                    ProjectContractDAL.Delete(ID);
                    log.Info(string.Format("Contract {0} of project {2} has been DELETED by {1}", ID, string.Concat(LoggedUser.FullName, " ( userID ", LoggedUser.UserID, " )"), UIHelpers.GetPIDParam().ToString()));
                    BindGrid();
                    break;
            }
        }

		

		

		

		
		

		

	
	}
}
