<%@ Register TagPrefix="uc1" TagName="PageHeader" Src="UserControls/PageHeader.ascx" %>
<%@ Register TagPrefix="uc1" TagName="EditForm" Src="UserControls/EditForm.ascx" %>
<%@ Page language="c#" Codebehind="EditMeeting.aspx.cs" AutoEventWireup="false" Inherits="Asa.Timesheet.WebPages.EditMeeting" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
	<HEAD>
		<title>Meeting</title>
		<meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
		<meta content="C#" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<LINK href="styles/timesheet.css" type="text/css" rel="stylesheet">
		<script language="javascript" src="PopupCalendar.js"></script>
		<script language="javascript">
		
		function MailDblClick()
		{
			var src, dest;
			
			src = event.srcElement;
			switch (src.id)
			{
				case "lbUsers" : dest = document.getElementById("lbSelectedUsers");
								 break;
				case "lbSelectedUsers" : dest = document.getElementById("lbUsers");
								break;
				case "lbClients" : dest = document.getElementById("lbSelectedClients");
								 break;
				case "lbSelectedClients" : dest = document.getElementById("lbClients");
								break;
				case "lbSelectedUsersMails" : dest = document.getElementById("lbUsersMails");
								break;
				case "lbUsersMails" : dest = document.getElementById("lbSelectedUsersMails");
								break;
			}
			
			index = src.selectedIndex;
			if (index==-1) return;
			
			var email = src.options[index].innerText;
			var value = src.options[index].value;
			
			src.options.remove(index);
			var opt = document.createElement("OPTION");
			dest.options.add(opt); 
			opt.innerText = email;
			opt.value = value;
			
			UpdateContainer(src);
			UpdateContainer(dest);
		} 
		
		function ArrowButtonClick()
		{
			var btn = event.srcElement;
			var src, dest;
			
			switch (btn.id)
			{
				case "lbMeetingProjects":
				case "inputProjectToRight" : src = document.getElementById("lbMeetingProjects");
									  dest = document.getElementById("lbSelectedMeetingProjects");
									  break;
									  
				case "lbSelectedMeetingProjects":
				case "inputProjectToLeft" :  src = document.getElementById("lbSelectedMeetingProjects");
									  dest = document.getElementById("lbMeetingProjects");
									  break;
				case "lbUsers":
				case "inputToRight" : src = document.getElementById("lbUsers");
									  dest = document.getElementById("lbSelectedUsers");
									  break;
									  
				case "lbSelectedUsers":
				case "inputToLeft" :  src = document.getElementById("lbSelectedUsers");
									  dest = document.getElementById("lbUsers");
									  break;
				case "lbSelectedUsersMails" :					  
				case "inputUsersToLeft" :   dest = document.getElementById("lbUsersMails");
											src = document.getElementById("lbSelectedUsersMails");
											break;
				case "lbUsersMails":
				case "inputUsersToRight" :  dest = document.getElementById("lbSelectedUsersMails");
											src = document.getElementById("lbUsersMails");
											break;
				case "lbSelectedClients" :
				case "inputUsersToLeft1" :   dest = document.getElementById("lbClients");
											src = document.getElementById("lbSelectedClients");
											break;
				case "lbClients" :
				case "inputUsersToRight1" :  dest = document.getElementById("lbSelectedClients");
											src = document.getElementById("lbClients");
											break;
			}
			
			//for (i=src.options.length-1;i>=0 ;i--)
			var email, value, opt;
			
			for (i=0; i<src.options.length; i++)
			if (src.options[i].selected)
			{ 
				email = src.options[i].innerText;
				value = src.options[i].value;
				opt = document.createElement("OPTION");
				dest.options.add(opt); 
				opt.innerText = email;
				opt.value = value;
			}
			
			for (i=src.options.length-1;i>=0 ;i--)
			if (src.options[i].selected) src.options.remove(i);
			
			UpdateContainer(dest);
			UpdateContainer(src);
			
		}
		
		function UpdateContainer(listBox)
		{
			var container = document.getElementById("hdn"+listBox.id);
			
			var arr = new Array();
			for (i=0; i<listBox.options.length; i++) arr.push(listBox.options[i].value);
			container.value = arr.join(";");
			
		}
		
		
		
		</script>
	</HEAD>
	<body bottomMargin="0" leftMargin="0" topMargin="0" rightMargin="0">
		<form id="Form1" method="post" runat="server">
			<table height="100%" cellSpacing="0" cellPadding="0" width="100%" border="0">
				<tr height="1">
					<td><uc1:pageheader id="header" runat="server"></uc1:pageheader><uc1:editform id="editCtrl" runat="server"></uc1:editform></td>
				</tr>
				<TR height="1">
					<td noWrap background="images/line.gif" height="1"></td>
				</TR>
				<tr>
					<td>
						<table height="100%" cellSpacing="0" cellPadding="0" width="100%" border="0">
							<tr>
								<td style="BACKGROUND-IMAGE: url(images/left110.gif)" vAlign="top" noWrap width="110"><asp:placeholder id="menuHolder" runat="server"></asp:placeholder></td>
								<td noWrap width="1" bgColor="lightgrey"></td>
								<td style="PADDING-LEFT: 5px; PADDING-TOP: 3px" vAlign="top" noWrap>
									<TABLE id="Table3" cellSpacing="0" cellPadding="0" width="880" border="0">
										<TR>
											<TD style="WIDTH: 4px"></TD>
											<TD>
												<TABLE id="tblForm" cellSpacing="0" cellPadding="3" width="100%" border="0">
													<TBODY>
														<TR>
															<TD style="WIDTH: 168px"><asp:label id="Label8" runat="server" CssClass="enterDataLabel" Width="100%">Сграда:</asp:label></TD>
															<TD><asp:dropdownlist id="ddlBuildingTypes" runat="server" CssClass="enterDataBox" Width="250px" AutoPostBack="True"></asp:dropdownlist><asp:dropdownlist id="ddlProjectsStatus" runat="server" CssClass="EnterDataBox" Width="250px" AutoPostBack="True">
																	<asp:ListItem Value="0">всички проекти</asp:ListItem>
																	<asp:ListItem Value="1">активни проекти</asp:ListItem>
																	<asp:ListItem Value="2">пасивни проекти</asp:ListItem>
																	<asp:ListItem Value="3">приключени проекти</asp:ListItem>
																</asp:dropdownlist></TD>
														</TR>
														<TR>
															<TD style="WIDTH: 168px"><asp:label id="lblName" runat="server" CssClass="enterDataLabel" Width="100%" Font-Bold="True"
																	EnableViewState="False">Главен Проект:</asp:label></TD>
															<TD><asp:dropdownlist id="ddlProject" runat="server" CssClass="EnterDataBox" Width="250px" AutoPostBack="True"></asp:dropdownlist><IMG height="16" alt="" src="images/required1.gif" width="16">
															</TD>
														</TR>
														<TR>
															<TD style="WIDTH: 168px"><asp:label id="lbProjects" runat="server" CssClass="enterDataLabel" Width="100%" EnableViewState="False">Проекти:</asp:label></TD>
															<TD>
																<P>
																	<TABLE id="Table1" cellSpacing="0" cellPadding="0" width="100%" border="0">
																		<TR>
																			<TD style="WIDTH: 228px">
																				<P><asp:listbox id="lbMeetingProjects1" runat="server" CssClass="enterDataBox" Width="300px" Visible="False"
																						Height="106px" SelectionMode="Multiple"></asp:listbox></P>
																				<P><asp:listbox id="lbMeetingProjects" ondblclick="ArrowButtonClick();" runat="server" CssClass="enterDataBox"
																						Width="300px" Height="106px" SelectionMode="Multiple"></asp:listbox></P>
																			</TD>
																			<TD style="WIDTH: 34px">
																				<TABLE id="Table2" border="0">
																					<TR>
																						<TD><INPUT class="ActionButton" id="inputProjectToRight" style="WIDTH: 26px; HEIGHT: 18px"
																								onclick="ArrowButtonClick();" type="button" size="20" value=">" name="Button2"></TD>
																					</TR>
																					<TR>
																						<TD><INPUT class="ActionButton" id="inputProjectToLeft" style="WIDTH: 26px; HEIGHT: 18px" onclick="ArrowButtonClick();"
																								type="button" size="20" value="<" name="Button1"></TD>
																					</TR>
																				</TABLE>
																			</TD>
																			<TD>
																				<P><asp:listbox id="lbSelectedMeetingProjects" ondblclick="ArrowButtonClick();" runat="server" CssClass="enterDataBox"
																						Width="300px" Height="106px" SelectionMode="Multiple"></asp:listbox><INPUT id="hdnlbMeetingProjects" style="DISPLAY: none; WIDTH: 42px; HEIGHT: 19px" type="text"
																						size="1" name="Text1" runat="server"><INPUT id="hdnlbSelectedMeetingProjects" style="DISPLAY: none; WIDTH: 34px; HEIGHT: 19px"
																						type="text" size="1" name="Text2" runat="server"><INPUT id="hdnlbMeetingProjects1" style="DISPLAY: none; WIDTH: 42px; HEIGHT: 19px" type="text"
																						size="1" name="Text1" runat="server"></P>
																			</TD>
																		</TR>
																	</TABLE>
																</P>
															</TD>
														</TR>
														<TR>
															<TD style="WIDTH: 168px"><asp:label id="lbDate" runat="server" CssClass="enterDataLabel" Width="100%" Font-Bold="True">Дата:</asp:label></TD>
															<TD><asp:textbox id="txtStartDate" runat="server" CssClass="enterDataBox" Width="88px" MaxLength="10"></asp:textbox>&nbsp;<IMG id="lkCalendar1" alt="Calendar" src="images/calendar.ico" width="18" align="absBottom"
																	runat="server">&nbsp;<IMG height="16" alt="" src="images/required1.gif" width="16"></TD>
														</TR>
														<TR>
															<TD style="WIDTH: 168px; HEIGHT: 22px"><asp:label id="lbStart" runat="server" CssClass="enterDataLabel" Width="100%" Font-Bold="True"> Начален час:</asp:label></TD>
															<TD style="HEIGHT: 22px"><asp:dropdownlist id="ddlStartTime" Width="88px" Runat="server">
																	<asp:ListItem></asp:ListItem>
																</asp:dropdownlist>&nbsp;<IMG height="16" alt="" src="images/required1.gif" width="16"></TD>
														</TR>
														<TR>
															<TD style="WIDTH: 168px"><asp:label id="lbEnd" runat="server" CssClass="enterDataLabel" Width="100%" Font-Bold="True">Краен час:</asp:label></TD>
															<TD><asp:dropdownlist id="ddlEndTime" Width="88px" Runat="server">
																	<asp:ListItem></asp:ListItem>
																</asp:dropdownlist>&nbsp;<IMG height="16" alt="" src="images/required1.gif" width="16"></TD>
														</TR>
														<TR>
															<TD style="WIDTH: 168px"><asp:label id="lbPlace" runat="server" CssClass="enterDataLabel" Width="100%" Font-Bold="True">Място на провеждане:</asp:label></TD>
															<TD><asp:textbox id="txtPlace" runat="server" CssClass="enterDataBox" Width="250px" MaxLength="1000"></asp:textbox>&nbsp;<IMG height="16" alt="" src="images/required1.gif" width="16">
															</TD>
														</TR>
														<TR>
															<TD style="WIDTH: 168px"></TD>
															<TD><asp:button id="btnReload" runat="server" CssClass="ActionButton" Width="235px" Text="Покажи Подизпълнители и Клиенти"
																	BackColor="White" DESIGNTIMEDRAGDROP="2337"></asp:button></TD>
											</TD>
										</TR>
										<TR>
											<TD style="WIDTH: 168px"><asp:label id="Label3" runat="server" CssClass="enterDataLabel" Width="100%">Служители:</asp:label></TD>
											<TD>
												<TABLE id="Table8" cellSpacing="0" cellPadding="0" width="100%" border="0">
													<TR>
														<TD style="WIDTH: 228px"><asp:listbox id="lbUsersMails" ondblclick="ArrowButtonClick();" runat="server" CssClass="enterDataBox"
																Width="300px" Height="106px" SelectionMode="Multiple"></asp:listbox></TD>
														<TD style="WIDTH: 34px">
															<TABLE id="Table9" border="0">
																<TR>
																	<TD><INPUT class="ActionButton" id="inputUsersToRight" style="WIDTH: 26px; HEIGHT: 18px" onclick="ArrowButtonClick();"
																			type="button" size="20" value=">" name="Button2"></TD>
																</TR>
																<TR>
																	<TD><INPUT class="ActionButton" id="inputUsersToLeft" style="WIDTH: 26px; HEIGHT: 18px" onclick="ArrowButtonClick();"
																			type="button" size="20" value="<" name="Button1"></TD>
																</TR>
															</TABLE>
														</TD>
														<TD><asp:listbox id="lbSelectedUsersMails" ondblclick="ArrowButtonClick();" runat="server" CssClass="enterDataBox"
																Width="300px" Height="106px" SelectionMode="Multiple"></asp:listbox><INPUT id="hdnlbUsersMails" style="DISPLAY: none; WIDTH: 42px; HEIGHT: 19px" type="text"
																size="1" name="Text1" runat="server"><INPUT id="hdnlbSelectedUsersMails" style="DISPLAY: none; WIDTH: 34px; HEIGHT: 19px" type="text"
																size="1" name="Text2" runat="server"></TD>
													</TR>
												</TABLE>
											</TD>
										</TR>
										<TR>
											<TD style="WIDTH: 168px"><asp:label id="Label1" runat="server" CssClass="enterDataLabel" Width="100%">Подизпълнители:</asp:label></TD>
											<TD>
												<TABLE id="Table8" cellSpacing="0" cellPadding="0" border="0">
													<TR>
														<TD style="WIDTH: 179px"><asp:listbox id="lbUsers" ondblclick="ArrowButtonClick();" runat="server" CssClass="enterDataBox"
																Width="300px" Height="106px" SelectionMode="Multiple"></asp:listbox></TD>
														<TD style="WIDTH: 34px">
															<TABLE id="Table9" border="0">
																<TR>
																	<TD><INPUT class="ActionButton" id="inputToRight" style="WIDTH: 26px; HEIGHT: 18px" onclick="ArrowButtonClick();"
																			type="button" size="20" value=">" name="Button2"></TD>
																</TR>
																<TR>
																	<TD><INPUT class="ActionButton" id="inputToLeft" style="WIDTH: 26px; HEIGHT: 18px" onclick="ArrowButtonClick();"
																			type="button" size="20" value="<" name="Button1"></TD>
																</TR>
															</TABLE>
														</TD>
														<TD><asp:listbox id="lbSelectedUsers" ondblclick="ArrowButtonClick();" runat="server" CssClass="enterDataBox"
																Width="300px" Height="106px" SelectionMode="Multiple"></asp:listbox><INPUT id="hdnlbUsers" style="DISPLAY: none; WIDTH: 42px; HEIGHT: 19px" type="text" size="1"
																name="Text1" runat="server"><INPUT id="hdnlbSelectedUsers" style="DISPLAY: none; WIDTH: 34px; HEIGHT: 19px" type="text"
																size="1" name="Text2" runat="server"><INPUT id="hdn1lbSelectedUsers" style="DISPLAY: none; WIDTH: 34px; HEIGHT: 19px" type="text"
																size="1" name="Text2" runat="server"></TD>
													</TR>
												</TABLE>
											</TD>
										</TR>
										<TR>
											<TD style="WIDTH: 168px"><asp:label id="Label2" runat="server" CssClass="enterDataLabel" Width="100%">Клиенти:</asp:label></TD>
											<TD>
												<TABLE id="Table8" cellSpacing="0" cellPadding="0" border="0">
													<TR>
														<TD style="WIDTH: 179px"><asp:listbox id="lbClients" ondblclick="ArrowButtonClick();" runat="server" CssClass="enterDataBox"
																Width="300px" Height="106" SelectionMode="Multiple"></asp:listbox></TD>
														<TD style="WIDTH: 34px">
															<TABLE id="Table9" border="0">
																<TR>
																	<TD><INPUT class="ActionButton" id="inputUsersToRight1" style="WIDTH: 26px; HEIGHT: 18px" onclick="ArrowButtonClick();"
																			type="button" size="20" value=">" name="Button2"></TD>
																</TR>
																<TR>
																	<TD><INPUT class="ActionButton" id="inputUsersToLeft1" style="WIDTH: 26px; HEIGHT: 18px" onclick="ArrowButtonClick();"
																			type="button" size="20" value="<" name="Button1"></TD>
																</TR>
															</TABLE>
														</TD>
														<TD><asp:listbox id="lbSelectedClients" ondblclick="ArrowButtonClick();" runat="server" CssClass="enterDataBox"
																Width="300px" Height="106px" SelectionMode="Multiple"></asp:listbox><INPUT id="hdnlbClients" style="DISPLAY: none; WIDTH: 42px; HEIGHT: 19px" type="text" size="1"
																name="Text1" runat="server"><INPUT id="hdnlbSelectedClients" style="DISPLAY: none; WIDTH: 34px; HEIGHT: 19px" type="text"
																size="1" name="Text2" runat="server"><INPUT id="hdn1lbSelectedClients" style="DISPLAY: none; WIDTH: 34px; HEIGHT: 19px" type="text"
																size="1" name="Text2" runat="server"></TD>
													</TR>
												</TABLE>
											</TD>
										</TR>
										<TR>
											<TD style="WIDTH: 168px"><asp:label id="Label7" runat="server" CssClass="enterDataLabel" Width="100%">Други поканени:</asp:label></TD>
											<TD><asp:textbox id="txtOtherPeople" runat="server" CssClass="enterDataBox" Width="712px" MaxLength="1000"></asp:textbox></TD>
										</TR>
										<TR>
											<TD style="WIDTH: 168px"><asp:label id="Label9" runat="server" CssClass="enterDataLabel" Width="100%">Тема(бележки):</asp:label></TD>
											<TD><asp:textbox id="txtNotes" runat="server" CssClass="enterDataBox" Width="368px" Height="50px"
													MaxLength="10" TextMode="MultiLine"></asp:textbox></TD>
										</TR>
										<TR>
											<TD style="WIDTH: 168px"><asp:label id="Label6" runat="server" CssClass="enterDataLabel" Width="100%">Сканирани бележки:</asp:label></TD>
											<TD><asp:datalist id="dlDocs" runat="server" RepeatDirection="Horizontal">
													<ItemTemplate>
														<asp:HyperLink id="btnScan" runat="server" ToolTip="Сканиран документ" NavigateUrl='<%# GetURL((int)DataBinder.Eval(Container, "DataItem.MeetingDocument"))%>' Target=_blank>
															<img border="0" src="images/pdf.gif" /></asp:HyperLink>
														<br />
														<asp:ImageButton id="btnDel" runat="server" ToolTip="Изтрий" ImageUrl="images/delete.gif" Visible='<%# GetVis()%>' >
														</asp:ImageButton>
													</ItemTemplate>
												</asp:datalist><INPUT id="FileCtrl" type="file" name="File1" runat="server">
												<asp:button id="btnScanDocAdd" runat="server" CssClass="ActionButton" Text="Добави"></asp:button><asp:hyperlink id="btnScan" runat="server" Visible="False" Target="_blank" ToolTip="Сканиран документ">
													<img border="0" src="images/pdf.gif" /></asp:hyperlink>
											</TD>
										</TR>
										<TR>
											<TD style="WIDTH: 168px"><asp:label id="Label5" runat="server" CssClass="enterDataLabel" Width="100%">Потвърдена:</asp:label></TD>
											<TD><asp:checkbox id="ckApproved" runat="server"></asp:checkbox></TD>
										</TR>
									</TABLE>
								</td>
							</tr>
							<TR>
								<TD style="HEIGHT: 5px" colSpan="2"></TD>
							</TR>
							<TR>
								<TD colSpan="2"><IMG height="1" src="images/dot.gif" width="100%"></TD>
							</TR>
							<TR>
								<td colSpan="2" height="3"></td>
							</TR>
							<TR>
								<TD style="WIDTH: 4px"></TD>
								<TD>
									<TABLE id="Table5" cellSpacing="0" cellPadding="0" border="0">
										<TR>
											<TD style="WIDTH: 97px"><asp:button id="btnSave" runat="server" CssClass="ActionButton" EnableViewState="False" Text="Запиши"></asp:button><asp:button id="btnEdit" style="DISPLAY: none" runat="server" CssClass="ActionButton" EnableViewState="False"
													Text="Редактирай"></asp:button></TD>
											<TD style="WIDTH: 245px"><asp:button id="btnCancel" runat="server" CssClass="ActionButton" EnableViewState="False" Text="Откажи"></asp:button></TD>
											<TD><asp:button id="btnDelete" runat="server" CssClass="ActionButton" EnableViewState="False" Text="Изтрий"></asp:button></TD>
										</TR>
									</TABLE>
								</TD>
							</TR>
							<TR>
								<TD height="3"></TD>
							</TR>
							<TR>
								<TD colSpan="2"><IMG height="1" src="images/dot.gif" width="100%"></TD>
							</TR>
						</table>
						<br>
						&nbsp;
						<asp:label id="lblInfo" runat="server" CssClass="InfoLabel" EnableViewState="False"></asp:label><asp:label id="lblError" runat="server" CssClass="ErrorLabel" EnableViewState="False" ForeColor="Red"></asp:label></td>
				</tr>
			</table>
			</TD></TR></TBODY></TABLE></form>
	</body>
</HTML>
