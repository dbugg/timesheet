using System;
using System.Data;
using Asa.Timesheet.Data;
using Asa.Timesheet.Data.Reports;
using GrapeCity.ActiveReports;
using GrapeCity.ActiveReports.Controls;
using GrapeCity.ActiveReports.SectionReportModel;
using GrapeCity.ActiveReports.Document.Section;
using GrapeCity.ActiveReports.Document;

namespace Asa.Timesheet.WebPages.Reports
{
    public class Folders : GrapeCity.ActiveReports.SectionReport
	{
		int _projectID = 0;
		int _pos = 0;
		bool _forPDF = true;
		string _userName = String.Empty;
    int _projectStatus = 0;
		bool _hpr = false;


        public Folders(int projectID, int projectStatus, string userName, bool forPDF, bool hpr)
        {
            _projectID = projectID;
            _forPDF = forPDF;
            _userName = userName;
            _hpr = hpr;
            InitializeComponent();
        }

        private void Folders_ReportStart(object sender, System.EventArgs eArgs)
        {
            DataSet dsReport = FoldersReportData.SelectFoldersReportData(_projectID, _projectStatus, _hpr);
            this.DataSource = dsReport.Tables[0];
            this.SubReport1.Report = new FoldersSubreport();

            if (!_forPDF)
            {
                this.PageSettings.Margins.Left = CmToInch(2f);
                this.PrintWidth = CmToInch(16f);

                txtReportTitle.Location = new System.Drawing.PointF(0, txtReportTitle.Location.Y);

                //Obekt i adres
                lblObekt.Location = new System.Drawing.PointF(0, lblObekt.Location.Y);
                lblObekt.Size = new System.Drawing.SizeF(CmToInch(5f), lblObekt.Height);

                txtObekt.Location = new System.Drawing.PointF(lblObekt.Location.X + lblObekt.Width, txtObekt.Location.Y);
                txtObekt.Size = new System.Drawing.SizeF(CmToInch(11f), txtObekt.Height);

                txtAddress.Location = new System.Drawing.PointF(txtObekt.Location.X, txtAddress.Location.Y);
                txtAddress.Size = new System.Drawing.SizeF(txtObekt.Width, txtObekt.Height);

                //Papki
                txtPapkiLbl.Location = new System.Drawing.PointF(0, txtPapkiLbl.Location.Y);
                txtPapkiLbl.Size = new System.Drawing.SizeF(CmToInch(5f), txtPapkiLbl.Height);

                //   txtGivenLbl.Location = new System.Drawing.PointF(txtPapkiLbl.Location.X+txtPapkiLbl.Size.Width, txtGivenLbl.Location.Y);
                txtFoldersGiven.Location = new System.Drawing.PointF(CmToInch(10), txtFoldersGiven.Location.Y);
                // txtPapkiLbl.Location.X+txtPapkiLbl.Size.Width, txtFoldersGiven.Location.Y);

                //    txtArchiveLbl.Location = new System.Drawing.PointF(txtFoldersGiven.Location.X+txtFoldersGiven.Size.Width, txtArchiveLbl.Location.Y);
                txtFoldersArchive.Location = new System.Drawing.PointF(CmToInch(13), txtFoldersArchive.Location.Y);
                //txtFoldersGiven.Location.X+txtFoldersGiven.Size.Width, txtFoldersArchive.Location.Y);

                this.SubReport1.Location = new System.Drawing.PointF(0, this.SubReport1.Location.Y);

                // footer
                totalASALbl.Location = new System.Drawing.PointF(0, totalASALbl.Location.Y);
                txtTotalFoldersASAGiven.Location = new System.Drawing.PointF(totalASALbl.Location.X + totalASALbl.Size.Width,
                    txtTotalFoldersASAGiven.Location.Y);
                txtTotalFoldersASAArchive.Location = new System.Drawing.PointF(txtTotalFoldersASAGiven.Location.X + txtTotalFoldersASAGiven.Size.Width,
                    txtTotalFoldersASAArchive.Location.Y);

                totalSubLbl.Location = new System.Drawing.PointF(0, totalSubLbl.Location.Y);
                txtTotalFoldersGiven.Location = new System.Drawing.PointF(totalSubLbl.Location.X + totalSubLbl.Size.Width,
                    txtTotalFoldersGiven.Location.Y);
                txtTotalFoldersArchive.Location = new System.Drawing.PointF(txtTotalFoldersGiven.Location.X + txtTotalFoldersGiven.Size.Width,
                    txtTotalFoldersArchive.Location.Y);

                lblCreatedBy.Location = new System.Drawing.PointF(0, lblCreatedBy.Location.Y);
                txtCreatedBy.Location = new System.Drawing.PointF(0, txtCreatedBy.Location.Y);
                //	txtCreatedBy.Size = new System.Drawing.SizeF( CmToInch(14f), txtCreatedBy.Size.Height);


            }
        }

        private void Folders_FetchData(object sender, FetchEventArgs eArgs)
        {
            if (eArgs.EOF) return;

            DataTable dt = (DataTable)this.DataSource;

            DataRow[] dr = dt.Rows[_pos].GetChildRows("Rel1");
            if (dr.Length > 0) this.SubReport1.Report.DataSource = dr;
            //   else this.SubReport1.Report.DataSource = dr;

            //  if (_pos==dt.Rows.Count-1) this.Detail.NewPage = NewPage.None;

            _pos++;
        }

        private void Folders_ReportEnd(object sender, System.EventArgs eArgs)
        {

            if (!this._forPDF) return;

            Logo logo = new Logo();
            logo.Run();

            for (int i = 0; i < this.Document.Pages.Count; i++)
                this.Document.Pages[i].Overlay(logo.Document.Pages[0]);
        }

        private void Detail_Format(object sender, System.EventArgs eArgs)
        {
            this.txtFoldersGiven.Text = this.Fields["FoldersGiven"].Value == System.DBNull.Value ?
                String.Concat(Resource.ResourceManager["Reports_strFoldersGiven"], " - 0") :
                String.Concat(Resource.ResourceManager["Reports_strFoldersGiven"], " - ", this.Fields["FoldersGiven"].Value.ToString());

            this.txtFoldersArchive.Text = this.Fields["FoldersArchive"].Value == System.DBNull.Value ?
                String.Concat(Resource.ResourceManager["Reports_strFoldersArchive"], " - 0") :
                String.Concat(Resource.ResourceManager["Reports_strFoldersArchive"], " - ", this.Fields["FoldersArchive"].Value.ToString());

        }

        private void ReportFooter_Format(object sender, System.EventArgs eArgs)
        {
            int foldersGiven = 0, foldersArchive = 0;

            FoldersReportData.GetTotalASAFolders((DataTable)this.DataSource, out foldersGiven, out foldersArchive);
            txtTotalFoldersASAGiven.Text = foldersGiven.ToString();
            txtTotalFoldersASAArchive.Text = foldersArchive.ToString();


            FoldersReportData.GetTotalSubcontractersFolders(((DataTable)this.DataSource).DataSet.Tables[1], out foldersGiven, out foldersArchive);

            txtTotalFoldersGiven.Text = foldersGiven.ToString();
            txtTotalFoldersArchive.Text = foldersArchive.ToString();

            txtCreatedBy.Text = _userName;
        }

		#region ActiveReports Designer generated code

























        public void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Folders));
            this.Detail = new GrapeCity.ActiveReports.SectionReportModel.Detail();
            this.ReportHeader = new GrapeCity.ActiveReports.SectionReportModel.ReportHeader();
            this.ReportFooter = new GrapeCity.ActiveReports.SectionReportModel.ReportFooter();
            this.PageHeader = new GrapeCity.ActiveReports.SectionReportModel.PageHeader();
            this.PageFooter = new GrapeCity.ActiveReports.SectionReportModel.PageFooter();
            this.txtReportTitle = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.lblObekt = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtObekt = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtAddress = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.SubReport1 = new GrapeCity.ActiveReports.SectionReportModel.SubReport();
            this.txtPapkiLbl = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtFoldersGiven = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtFoldersArchive = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.TextBox1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.TextBox2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.totalSubLbl = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtTotalFoldersGiven = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtTotalFoldersArchive = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.totalASALbl = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtTotalFoldersASAGiven = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtTotalFoldersASAArchive = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.Line1 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.lblCreatedBy = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.txtCreatedBy = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.TextBox3 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            ((System.ComponentModel.ISupportInitialize)(this.txtReportTitle)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblObekt)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtObekt)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAddress)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPapkiLbl)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtFoldersGiven)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtFoldersArchive)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TextBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TextBox2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.totalSubLbl)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTotalFoldersGiven)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTotalFoldersArchive)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.totalASALbl)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTotalFoldersASAGiven)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTotalFoldersASAArchive)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblCreatedBy)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCreatedBy)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TextBox3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            // 
            // Detail
            // 
            this.Detail.ColumnSpacing = 0F;
            this.Detail.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
						this.lblObekt,
						this.txtObekt,
						this.txtAddress,
						this.SubReport1,
						this.txtPapkiLbl,
						this.txtFoldersGiven,
						this.txtFoldersArchive,
						this.TextBox1,
						this.TextBox2});
            this.Detail.Height = 1.208333F;
            this.Detail.KeepTogether = true;
            this.Detail.Name = "Detail";
            // 
            // ReportHeader
            // 
            this.ReportHeader.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
						this.txtReportTitle});
            this.ReportHeader.Height = 0.7868056F;
            this.ReportHeader.Name = "ReportHeader";
            // 
            // ReportFooter
            // 
            this.ReportFooter.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
						this.totalSubLbl,
						this.txtTotalFoldersGiven,
						this.txtTotalFoldersArchive,
						this.totalASALbl,
						this.txtTotalFoldersASAGiven,
						this.txtTotalFoldersASAArchive,
						this.Line1,
						this.lblCreatedBy,
						this.txtCreatedBy,
						this.TextBox3});
            this.ReportFooter.Height = 2.1875F;
            this.ReportFooter.Name = "ReportFooter";
            // 
            // PageHeader
            // 
            this.PageHeader.Height = 0F;
            this.PageHeader.Name = "PageHeader";
            // 
            // PageFooter
            // 
            this.PageFooter.Height = 0F;
            this.PageFooter.Name = "PageFooter";
            // 
            // txtReportTitle
            // 
            this.txtReportTitle.Height = 0.2755906F;
            this.txtReportTitle.Left = 0.3937007F;
            this.txtReportTitle.Name = "txtReportTitle";
            this.txtReportTitle.Style = "font-size: 15pt; font-weight: bold; text-align: center";
            this.txtReportTitle.Text = "?????";
            this.txtReportTitle.Top = 0F;
            this.txtReportTitle.Width = 6.299212F;
            // 
            // lblObekt
            // 
            this.lblObekt.Height = 0.2362205F;
            this.lblObekt.Left = 0.7874014F;
            this.lblObekt.Name = "lblObekt";
            this.lblObekt.Style = "font-size: 11pt";
            this.lblObekt.Text = "?????:";
            this.lblObekt.Top = 0F;
            this.lblObekt.Width = 1.181102F;
            // 
            // txtObekt
            // 
            this.txtObekt.DataField = "ProjectName";
            this.txtObekt.Height = 0.2362205F;
            this.txtObekt.Left = 1.968504F;
            this.txtObekt.Name = "txtObekt";
            this.txtObekt.Style = "font-size: 12pt; font-weight: bold";
            this.txtObekt.Text = "txtObekt";
            this.txtObekt.Top = 0F;
            this.txtObekt.Width = 4.330709F;
            // 
            // txtAddress
            // 
            this.txtAddress.DataField = "Address";
            this.txtAddress.Height = 0.2F;
            this.txtAddress.Left = 1.968504F;
            this.txtAddress.Name = "txtAddress";
            this.txtAddress.Style = "font-size: 11pt; font-weight: normal";
            this.txtAddress.Text = "txtAddress";
            this.txtAddress.Top = 0.2706694F;
            this.txtAddress.Width = 4.330709F;
            // 
            // SubReport1
            // 
            this.SubReport1.CloseBorder = false;
            this.SubReport1.Height = 0.1968504F;
            this.SubReport1.Left = 0.3937007F;
            this.SubReport1.Name = "SubReport1";
            this.SubReport1.Report = null;
            this.SubReport1.Top = 0.8661417F;
            this.SubReport1.Width = 6.299212F;
            // 
            // txtPapkiLbl
            // 
            this.txtPapkiLbl.Height = 0.2362205F;
            this.txtPapkiLbl.Left = 0.7874014F;
            this.txtPapkiLbl.Name = "txtPapkiLbl";
            this.txtPapkiLbl.Style = "font-size: 11pt";
            this.txtPapkiLbl.Text = "????? ???:";
            this.txtPapkiLbl.Top = 0.5905511F;
            this.txtPapkiLbl.Width = 1.181102F;
            // 
            // txtFoldersGiven
            // 
            this.txtFoldersGiven.DataField = "FoldersGiven";
            this.txtFoldersGiven.Height = 0.2362205F;
            this.txtFoldersGiven.Left = 2.362205F;
            this.txtFoldersGiven.Name = "txtFoldersGiven";
            this.txtFoldersGiven.Style = "font-size: 11pt; font-weight: normal; text-align: center";
            this.txtFoldersGiven.Text = "txtFoldersGiven";
            this.txtFoldersGiven.Top = 0.5905511F;
            this.txtFoldersGiven.Width = 1.181102F;
            // 
            // txtFoldersArchive
            // 
            this.txtFoldersArchive.DataField = "FoldersArchive";
            this.txtFoldersArchive.Height = 0.2362205F;
            this.txtFoldersArchive.Left = 3.937008F;
            this.txtFoldersArchive.Name = "txtFoldersArchive";
            this.txtFoldersArchive.Style = "font-size: 11pt; font-weight: normal; text-align: center";
            this.txtFoldersArchive.Text = "txtFoldersArchive";
            this.txtFoldersArchive.Top = 0.5905511F;
            this.txtFoldersArchive.Width = 1.181102F;
            // 
            // TextBox1
            // 
            this.TextBox1.DataField = "FoldersGiven";
            this.TextBox1.Height = 0.2362205F;
            this.TextBox1.Left = 2.362205F;
            this.TextBox1.Name = "TextBox1";
            this.TextBox1.Style = "font-size: 12pt; font-weight: bold";
            this.TextBox1.Text = "TextBox1";
            this.TextBox1.Top = 0.8661417F;
            this.TextBox1.Visible = false;
            this.TextBox1.Width = 1.574803F;
            // 
            // TextBox2
            // 
            this.TextBox2.DataField = "FoldersArchive";
            this.TextBox2.Height = 0.2362205F;
            this.TextBox2.Left = 3.937008F;
            this.TextBox2.Name = "TextBox2";
            this.TextBox2.Style = "font-size: 12pt; font-weight: bold";
            this.TextBox2.Text = "TextBox2";
            this.TextBox2.Top = 0.8661417F;
            this.TextBox2.Visible = false;
            this.TextBox2.Width = 1.574803F;
            // 
            // totalSubLbl
            // 
            this.totalSubLbl.Height = 0.2362205F;
            this.totalSubLbl.Left = 0.3937007F;
            this.totalSubLbl.Name = "totalSubLbl";
            this.totalSubLbl.Style = "font-size: 11pt";
            this.totalSubLbl.Text = "???? ????? ??????????????:";
            this.totalSubLbl.Top = 0.7930611F;
            this.totalSubLbl.Width = 3.937008F;
            // 
            // txtTotalFoldersGiven
            // 
            this.txtTotalFoldersGiven.DataField = "FoldersGiven";
            this.txtTotalFoldersGiven.Height = 0.2362205F;
            this.txtTotalFoldersGiven.Left = 4.330709F;
            this.txtTotalFoldersGiven.Name = "txtTotalFoldersGiven";
            this.txtTotalFoldersGiven.Style = "font-size: 12pt; font-weight: bold; text-align: center";
            this.txtTotalFoldersGiven.Top = 0.7930611F;
            this.txtTotalFoldersGiven.Width = 1.181102F;
            // 
            // txtTotalFoldersArchive
            // 
            this.txtTotalFoldersArchive.DataField = "FoldersArchive";
            this.txtTotalFoldersArchive.Height = 0.2362205F;
            this.txtTotalFoldersArchive.Left = 5.511811F;
            this.txtTotalFoldersArchive.Name = "txtTotalFoldersArchive";
            this.txtTotalFoldersArchive.Style = "font-size: 12pt; font-weight: bold; text-align: center";
            this.txtTotalFoldersArchive.Top = 0.7930611F;
            this.txtTotalFoldersArchive.Width = 1.181102F;
            // 
            // totalASALbl
            // 
            this.totalASALbl.Height = 0.2362205F;
            this.totalASALbl.Left = 0.3937007F;
            this.totalASALbl.Name = "totalASALbl";
            this.totalASALbl.Style = "font-size: 11pt";
            this.totalASALbl.Text = "???? ????? ???:";
            this.totalASALbl.Top = 0.5174705F;
            this.totalASALbl.Width = 3.937008F;
            // 
            // txtTotalFoldersASAGiven
            // 
            this.txtTotalFoldersASAGiven.DataField = "FoldersGiven";
            this.txtTotalFoldersASAGiven.Height = 0.2362205F;
            this.txtTotalFoldersASAGiven.Left = 4.330709F;
            this.txtTotalFoldersASAGiven.Name = "txtTotalFoldersASAGiven";
            this.txtTotalFoldersASAGiven.Style = "font-size: 12pt; font-weight: bold; text-align: center";
            this.txtTotalFoldersASAGiven.Top = 0.5027066F;
            this.txtTotalFoldersASAGiven.Width = 1.181102F;
            // 
            // txtTotalFoldersASAArchive
            // 
            this.txtTotalFoldersASAArchive.DataField = "FoldersArchive";
            this.txtTotalFoldersASAArchive.Height = 0.2362205F;
            this.txtTotalFoldersASAArchive.Left = 5.511811F;
            this.txtTotalFoldersASAArchive.Name = "txtTotalFoldersASAArchive";
            this.txtTotalFoldersASAArchive.Style = "font-size: 12pt; font-weight: bold; text-align: center";
            this.txtTotalFoldersASAArchive.Top = 0.5027066F;
            this.txtTotalFoldersASAArchive.Width = 1.181102F;
            // 
            // Line1
            // 
            this.Line1.Height = 0F;
            this.Line1.Left = 0.3937007F;
            this.Line1.LineWeight = 1F;
            this.Line1.Name = "Line1";
            this.Line1.Top = 0.4006452F;
            this.Line1.Width = 6.299212F;
            this.Line1.X1 = 0.3937007F;
            this.Line1.X2 = 6.692913F;
            this.Line1.Y1 = 0.4006452F;
            this.Line1.Y2 = 0.4006452F;
            // 
            // lblCreatedBy
            // 
            this.lblCreatedBy.Height = 0.2738189F;
            this.lblCreatedBy.HyperLink = null;
            this.lblCreatedBy.Left = 0.3937007F;
            this.lblCreatedBy.Name = "lblCreatedBy";
            this.lblCreatedBy.Style = "font-size: 11pt";
            this.lblCreatedBy.Text = "????????: ......................................";
            this.lblCreatedBy.Top = 1.536171F;
            this.lblCreatedBy.Width = 3.937008F;
            // 
            // txtCreatedBy
            // 
            this.txtCreatedBy.Height = 0.2F;
            this.txtCreatedBy.Left = 1.181102F;
            this.txtCreatedBy.Name = "txtCreatedBy";
            this.txtCreatedBy.Style = "font-size: 10pt; font-weight: normal";
            this.txtCreatedBy.Text = "txtCreatedBy";
            this.txtCreatedBy.Top = 1.80684F;
            this.txtCreatedBy.Width = 3.937008F;
            // 
            // TextBox3
            // 
            this.TextBox3.Height = 0.3937007F;
            this.TextBox3.Left = 0F;
            this.TextBox3.Name = "TextBox3";
            this.TextBox3.Top = 1.093258F;
            this.TextBox3.Width = 5.51181F;
            // 
            // ActiveReport1
            // 
            this.MasterReport = false;
            this.PageSettings.DefaultPaperSize = false;
            this.PageSettings.Margins.Bottom = 0.4097222F;
            this.PageSettings.Margins.Left = 1.377778F;
            this.PageSettings.Margins.Right = 0.8034722F;
            this.PageSettings.Margins.Top = 0.8034722F;
            this.PageSettings.PaperHeight = 11.69306F;
            this.PageSettings.PaperKind = System.Drawing.Printing.PaperKind.A4;
            this.PageSettings.PaperWidth = 8.268056F;
            this.PrintWidth = 7.086611F;
            this.Sections.Add(this.ReportHeader);
            this.Sections.Add(this.PageHeader);
            this.Sections.Add(this.Detail);
            this.Sections.Add(this.PageFooter);
            this.Sections.Add(this.ReportFooter);
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule(resources.GetString("$this.StyleSheet"), "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: inherit; font-style: inherit; font-variant: inherit; font-weight: bo" +
                        "ld; font-size: 16pt; font-size-adjust: inherit; font-stretch: inherit", "Heading1", "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: Times New Roman; font-style: italic; font-variant: inherit; font-wei" +
                        "ght: bold; font-size: 14pt; font-size-adjust: inherit; font-stretch: inherit", "Heading2", "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: inherit; font-style: inherit; font-variant: inherit; font-weight: bo" +
                        "ld; font-size: 13pt; font-size-adjust: inherit; font-stretch: inherit", "Heading3", "Normal"));
            ((System.ComponentModel.ISupportInitialize)(this.txtReportTitle)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblObekt)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtObekt)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAddress)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPapkiLbl)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtFoldersGiven)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtFoldersArchive)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TextBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TextBox2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.totalSubLbl)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTotalFoldersGiven)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTotalFoldersArchive)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.totalASALbl)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTotalFoldersASAGiven)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTotalFoldersASAArchive)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblCreatedBy)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCreatedBy)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TextBox3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();

            // Attach Report Events
            this.ReportStart += new System.EventHandler(this.Folders_ReportStart);
            this.FetchData += new GrapeCity.ActiveReports.SectionReport.FetchEventHandler(this.Folders_FetchData);
            this.ReportEnd += new System.EventHandler(this.Folders_ReportEnd);
            this.Detail.Format += new System.EventHandler(this.Detail_Format);
            this.ReportFooter.Format += new System.EventHandler(this.ReportFooter_Format);
        }

		#endregion

        private ReportHeader ReportHeader;
        private TextBox txtReportTitle;
        private PageHeader PageHeader;
        private Detail Detail;
        private TextBox lblObekt;
        private TextBox txtObekt;
        private TextBox txtAddress;
        private SubReport SubReport1;
        private TextBox txtPapkiLbl;
        private TextBox txtFoldersGiven;
        private TextBox txtFoldersArchive;
        private TextBox TextBox1;
        private TextBox TextBox2;
        private PageFooter PageFooter;
        private ReportFooter ReportFooter;
        private TextBox totalSubLbl;
        private TextBox txtTotalFoldersGiven;
        private TextBox txtTotalFoldersArchive;
        private TextBox totalASALbl;
        private TextBox txtTotalFoldersASAGiven;
        private TextBox txtTotalFoldersASAArchive;
        private Line Line1;
        private Label lblCreatedBy;
        private TextBox txtCreatedBy;
        private TextBox TextBox3;
	}
}
