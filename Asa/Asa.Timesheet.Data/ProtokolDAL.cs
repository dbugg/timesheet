using System;
using System.Data;
using System.Data.SqlClient; 
using System.Collections;
using Microsoft.ApplicationBlocks.Data;

namespace Asa.Timesheet.Data
{
	public class ProtokolDAL
	{
		/// <summary>
		/// Loads a ProtokolData object from the database using the given ProtokolID
		/// </summary>
		/// <param name="protokolID">The primary key value</param>
		/// <returns>A ProtokolData object</returns>
		public static ProtokolData Load(int protokolID)
		{
			SqlParameter[] parameterValues = new SqlParameter[] { new SqlParameter("@ProtokolID", protokolID) };
			using (SqlDataReader reader = SqlHelper.ExecuteReader(CommonDAL.ConnectionString, CommandType.StoredProcedure, "ProtokolsSelProc", parameterValues))
			{
				if (reader.Read())
				{
					ProtokolData result = new ProtokolData();
					LoadFromReader(reader,result);
					return result;
				}
				else
					return null;
			}
		}

		/// <summary>
		/// Loads a ProtokolData object from the database using the given where clause
		/// </summary>
		/// <param name="whereClause">The filter expression for the query</param>
		/// <returns>A ProtokolData object</returns>
		public static ProtokolData Load(string whereClause)
		{
			SqlParameter[] parameterValues = new SqlParameter[] { new SqlParameter("@WhereClause", whereClause) };
			using (SqlDataReader reader = SqlHelper.ExecuteReader(CommonDAL.ConnectionString, CommandType.StoredProcedure, "ProtokolsListProc", parameterValues))
			{
				if (reader.Read())
				{
					ProtokolData result = new ProtokolData();
					LoadFromReader(reader,result);
					return result;
				}
				else
					return null;
			}
		}
		public static int Save( ProtokolData source)
		{
			if (source.ProtokolID == -1)
				return Insert(source);
			else
				return Update(source);	
		}

		public static void Delete( ProtokolData source)
		{
			Delete(source.ProtokolID);
		}
		/// <summary>
		/// Loads a collection of ProtokolData objects from the database.
		/// </summary>
		/// <returns>A collection containing all of the ProtokolData objects in the database.</returns>
		public static ProtokolsVector LoadCollection()
		{
			SqlParameter[] parms = new SqlParameter[] {};
			return LoadCollection("ProtokolsListProc", parms);
		}
	  
		/// <summary>
		/// Saves a collection of ProtokolData objects to the database.
		/// </summary>
		/// <returns>A collection containing all of the ProtokolData objects in the database.</returns>
		public static  void  SaveCollection(ProtokolsVector col,string spName, SqlParameter[] parms)
		{
			ProtokolsVector colOld = LoadCollection(spName,parms);
			UpdateCollection(colOld,col);
		}
		public static  void  SaveCollection(ProtokolsVector col)
		{
			ProtokolsVector colOld = LoadCollection();
			UpdateCollection(colOld,col);
		}
		public static  void  SaveCollection(ProtokolsVector col, string whereClause )
		{
			ProtokolsVector colOld = LoadCollection(whereClause);
			UpdateCollection(colOld,col);
		}
		/// <summary>
		/// Loads a collection of ProtokolData objects from the database.
		/// </summary>
		/// <returns>A collection containing all of the ProtokolData objects in the database.</returns>
		public static ProtokolsVector LoadCollection(string whereClause)
		{
			SqlParameter[] parms = new SqlParameter[] { new SqlParameter("@WhereClause", whereClause) };
			return LoadCollection("ProtokolsListProc", parms);
		}

		/// <summary>
		/// Loads a collection of ProtokolData objects from the database.
		/// </summary>
		/// <returns>A collection containing all of the ProtokolData objects in the database.</returns>
		public static ProtokolsVector LoadCollection(string spName, SqlParameter[] parms)
		{
			ProtokolsVector result = new ProtokolsVector();
			using (SqlDataReader reader = SqlHelper.ExecuteReader(CommonDAL.ConnectionString, CommandType.StoredProcedure, spName, parms))
			{
				while (reader.Read())
				{
					ProtokolData tmp = new ProtokolData();
					LoadFromReader(reader, tmp);
					result.Add(tmp);
				}
			}
			return result;
		}
		/// <summary>
		/// Deletes a ProtokolData object from the database.
		/// </summary>
		/// <param name="protokolID">The primary key value</param>
		public static void Delete(int protokolID)
		{
			SqlParameter[] parameterValues = new SqlParameter[] { new SqlParameter("@ProtokolID", protokolID) };
			SqlHelper.ExecuteNonQuery(CommonDAL.ConnectionString, CommandType.StoredProcedure, "ProtokolsDelProc", parameterValues);
		}

		public static void LoadFromReader(SqlDataReader reader, ProtokolData target)
		{
			if (reader != null && !reader.IsClosed)
			{
				//target.Key = reader.GetInt32(0);
				if (!reader.IsDBNull(0)) target.ProtokolID = reader.GetInt32(0);
				if (!reader.IsDBNull(1)) target.ProtokolDate = reader.GetDateTime(1);
				if (!reader.IsDBNull(2)) target.ProjectID = reader.GetInt32(2);
				if (!reader.IsDBNull(3)) target.CreatedByID = reader.GetInt32(3);
				if (!reader.IsDBNull(4)) target.HasScan = reader.GetBoolean(4);
				if (!reader.IsDBNull(5)) target.Notes = reader.GetString(5);
				if (!reader.IsDBNull(6)) target.ProjectName = reader.GetString(6);
			}
		}
		
	  
		public static int Insert( ProtokolData source)
		{
			SqlParameter[] parameterValues = GetInsertParameterValues(source);
			SqlHelper.ExecuteNonQuery(CommonDAL.ConnectionString, CommandType.StoredProcedure, "ProtokolsInsProc", parameterValues);
			source.ProtokolID = (int) parameterValues[0].Value;
			return source.ProtokolID;
		}

		public static int Update( ProtokolData source)
		{
			SqlParameter[] parameterValues = GetUpdateParameterValues(source);
			SqlHelper.ExecuteNonQuery(CommonDAL.ConnectionString, CommandType.StoredProcedure,
				"ProtokolsUpdProc", parameterValues);
			return source.ProtokolID;
		}
		private static void UpdateCollection(ProtokolsVector colOld,ProtokolsVector col)
		{
			// Delete all old items
			foreach( ProtokolData itemOld in colOld)
			{
				if(!ExistsKey(itemOld.ProtokolID, col))
					Delete(itemOld.ProtokolID);
			}
			foreach( ProtokolData item in col)
			{
				if(item.ProtokolID <= -1)
					Insert(item);
				else
					Update(item);
			}
		}
		private static bool ExistsKey(int Key, ProtokolsVector col)
		{
			foreach( ProtokolData item in col)
				if(item.ProtokolID==Key)
					return true;
			return false;
		}

		private static SqlParameter GetSqlParameter(string name, ParameterDirection direction, object value)
		{
			SqlParameter p = new SqlParameter(name, value);
			p.Direction = direction;
			return p;
		}

		private static SqlParameter[] GetInsertParameterValues( ProtokolData source)
		{
			SqlParameter[] prams = new SqlParameter[6];
			prams[0] = GetSqlParameter("@ProtokolID", ParameterDirection.Output, source.ProtokolID);
			prams[1] = GetSqlParameter("@ProtokolDate", ParameterDirection.Input, source.ProtokolDate);
			prams[2] = GetSqlParameter("@ProjectID", ParameterDirection.Input, source.ProjectID);
			prams[3] = GetSqlParameter("@CreatedByID", ParameterDirection.Input, source.CreatedByID);
			prams[4] = GetSqlParameter("@HasScan", ParameterDirection.Input, source.HasScan);
			prams[5] = GetSqlParameter("@Notes", ParameterDirection.Input, source.Notes);
			return prams;
		}

		private static SqlParameter[] GetUpdateParameterValues( ProtokolData source)
		{
			return new SqlParameter[] {
										  new SqlParameter("@ProtokolID", source.ProtokolID),
										  new SqlParameter("@ProtokolDate", source.ProtokolDate),
										  new SqlParameter("@ProjectID", source.ProjectID),
										  new SqlParameter("@CreatedByID", source.CreatedByID),
										  new SqlParameter("@HasScan", source.HasScan),
										  new SqlParameter("@Notes", source.Notes)
									  };
		}
	}
}   

