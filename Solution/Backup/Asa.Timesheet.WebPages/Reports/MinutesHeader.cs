using System;
using Asa.Timesheet.Data;
using GrapeCity.ActiveReports;
using GrapeCity.ActiveReports.Controls;
using GrapeCity.ActiveReports.SectionReportModel;
using GrapeCity.ActiveReports.Document.Section;
using GrapeCity.ActiveReports.Document;

namespace Asa.Timesheet.WebPages.Reports
{
    public class MinutesHeader : GrapeCity.ActiveReports.SectionReport
	{
        public MinutesHeader(Minutes.HeaderInfo headerInfo, bool forPDF)
        {
            InitializeComponent();

            this.txtObekt.Text = headerInfo.ProjectAdminName;
            this.txtClient.Text = headerInfo.ClientName;
            this.txtClientAddress.Text = String.Concat(headerInfo.ClientCity, ", ", headerInfo.ClientAddress).Trim(',').Trim().Trim(',');
            this.txtProektant.Text = headerInfo.Proektant;

            if (forPDF)
            {
                this.txtObekt.Location = new System.Drawing.PointF(SectionReport.CmToInch(3.5f), this.txtObekt.Location.Y);
                this.txtClient.Location = new System.Drawing.PointF(SectionReport.CmToInch(3.5f), this.txtClient.Location.Y);
                this.txtClientAddress.Location = new System.Drawing.PointF(SectionReport.CmToInch(3.5f), this.txtClientAddress.Location.Y);
                this.txtProektant.Location = new System.Drawing.PointF(SectionReport.CmToInch(3.5f), this.txtProektant.Location.Y);
                this.txtFilter1.Location = new System.Drawing.PointF(SectionReport.CmToInch(3.5f), this.txtFilter1.Location.Y);
                this.txtFilter2.Location = new System.Drawing.PointF(SectionReport.CmToInch(3.5f), this.txtFilter2.Location.Y);

            }

            if (this.txtObekt.Text == String.Empty)
            {
                this.GroupHeader1.Visible = false;
                this.GroupHeader2.Visible = false;
                this.GroupHeader3.Visible = false;
            }
            if (this.txtClient.Text == String.Empty)
            {
                this.GroupHeader2.Visible = false;
            }

            SetFilterInfo(headerInfo);

        }

        private void SetFilterInfo(Minutes.HeaderInfo hi)
        {
            int currentLabel = 1;

            if (hi.User != String.Empty)
            {
                Label lbl = (Label)this.GroupHeader5.Controls["lblFilter" + currentLabel.ToString()];
                lbl.Visible = true;
                lbl.Text = Resource.ResourceManager["reports_User"];
                TextBox txt = (TextBox)this.GroupHeader5.Controls["txtFilter" + currentLabel.ToString()];
                txt.Visible = true;
                txt.Text = hi.User;
                currentLabel++;
            }

            if (hi.Activity != String.Empty)
            {
                Label lbl = (Label)this.GroupHeader5.Controls["lblFilter" + currentLabel.ToString()];
                lbl.Visible = true;
                lbl.Text = Resource.ResourceManager["reports_Activity"];
                TextBox txt = (TextBox)this.GroupHeader5.Controls["txtFilter" + currentLabel.ToString()];
                txt.Visible = true;
                txt.Text = hi.Activity;
                currentLabel++;
            }

        }

		#region ActiveReports Designer generated code

























        public void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MinutesHeader));
            this.Detail = new GrapeCity.ActiveReports.SectionReportModel.Detail();
            this.GroupHeader1 = new GrapeCity.ActiveReports.SectionReportModel.GroupHeader();
            this.lblObekt = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.txtObekt = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.Label3 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.GroupFooter1 = new GrapeCity.ActiveReports.SectionReportModel.GroupFooter();
            this.GroupHeader2 = new GrapeCity.ActiveReports.SectionReportModel.GroupHeader();
            this.lblClient = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.txtClient = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtClientAddress = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.GroupFooter2 = new GrapeCity.ActiveReports.SectionReportModel.GroupFooter();
            this.GroupHeader3 = new GrapeCity.ActiveReports.SectionReportModel.GroupHeader();
            this.lblProektant = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.txtProektant = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.GroupFooter3 = new GrapeCity.ActiveReports.SectionReportModel.GroupFooter();
            this.GroupHeader5 = new GrapeCity.ActiveReports.SectionReportModel.GroupHeader();
            this.lblFilter1 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.txtFilter1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.lblFilter2 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.txtFilter2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.GroupFooter5 = new GrapeCity.ActiveReports.SectionReportModel.GroupFooter();
            this.GroupHeader4 = new GrapeCity.ActiveReports.SectionReportModel.GroupHeader();
            this.txtTitle = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.Line10 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.GroupFooter4 = new GrapeCity.ActiveReports.SectionReportModel.GroupFooter();
            ((System.ComponentModel.ISupportInitialize)(this.lblObekt)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtObekt)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblClient)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtClient)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtClientAddress)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblProektant)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtProektant)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblFilter1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtFilter1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblFilter2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtFilter2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTitle)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            // 
            // Detail
            // 
            this.Detail.Height = 0F;
            this.Detail.Name = "Detail";
            this.Detail.Visible = false;
            // 
            // GroupHeader1
            // 
            this.GroupHeader1.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
            this.lblObekt,
            this.txtObekt,
            this.Label3});
            this.GroupHeader1.Height = 0.3958333F;
            this.GroupHeader1.Name = "GroupHeader1";
            // 
            // lblObekt
            // 
            this.lblObekt.Height = 0.2F;
            this.lblObekt.HyperLink = null;
            this.lblObekt.Left = 0F;
            this.lblObekt.Name = "lblObekt";
            this.lblObekt.Style = "font-size: 9pt; font-weight: bold";
            this.lblObekt.Text = "�����:";
            this.lblObekt.Top = 0.1968504F;
            this.lblObekt.Width = 1.811024F;
            // 
            // txtObekt
            // 
            this.txtObekt.Height = 0.2F;
            this.txtObekt.Left = 1.811024F;
            this.txtObekt.Name = "txtObekt";
            this.txtObekt.Style = "font-size: 9pt";
            this.txtObekt.Text = "txtObekt";
            this.txtObekt.Top = 0.1968504F;
            this.txtObekt.Width = 4.291338F;
            // 
            // Label3
            // 
            this.Label3.Height = 0.1968504F;
            this.Label3.HyperLink = null;
            this.Label3.Left = 0F;
            this.Label3.Name = "Label3";
            this.Label3.Style = "color: White; font-size: 9pt; font-weight: bold; vertical-align: bottom; white-sp" +
    "ace: nowrap";
            this.Label3.Text = ".";
            this.Label3.Top = 0F;
            this.Label3.Width = 7.086611F;
            // 
            // GroupFooter1
            // 
            this.GroupFooter1.Name = "GroupFooter1";
            this.GroupFooter1.Visible = false;
            // 
            // GroupHeader2
            // 
            this.GroupHeader2.CanShrink = true;
            this.GroupHeader2.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
            this.lblClient,
            this.txtClient,
            this.txtClientAddress});
            this.GroupHeader2.Height = 0.3930556F;
            this.GroupHeader2.Name = "GroupHeader2";
            // 
            // lblClient
            // 
            this.lblClient.Height = 0.2F;
            this.lblClient.HyperLink = null;
            this.lblClient.Left = 0F;
            this.lblClient.Name = "lblClient";
            this.lblClient.Style = "font-size: 9pt; font-weight: bold; vertical-align: bottom";
            this.lblClient.Text = "������:";
            this.lblClient.Top = 0F;
            this.lblClient.Width = 1.811024F;
            // 
            // txtClient
            // 
            this.txtClient.Height = 0.2F;
            this.txtClient.Left = 1.811024F;
            this.txtClient.Name = "txtClient";
            this.txtClient.Style = "font-size: 9pt; vertical-align: bottom";
            this.txtClient.Text = "txtClient";
            this.txtClient.Top = 0F;
            this.txtClient.Width = 4.291338F;
            // 
            // txtClientAddress
            // 
            this.txtClientAddress.Height = 0.2F;
            this.txtClientAddress.Left = 1.811024F;
            this.txtClientAddress.Name = "txtClientAddress";
            this.txtClientAddress.Style = "font-size: 9pt";
            this.txtClientAddress.Text = "txtClientAddress";
            this.txtClientAddress.Top = 0.1968504F;
            this.txtClientAddress.Width = 4.291338F;
            // 
            // GroupFooter2
            // 
            this.GroupFooter2.Height = 0F;
            this.GroupFooter2.Name = "GroupFooter2";
            this.GroupFooter2.Visible = false;
            // 
            // GroupHeader3
            // 
            this.GroupHeader3.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
            this.lblProektant,
            this.txtProektant});
            this.GroupHeader3.Height = 0.1965278F;
            this.GroupHeader3.Name = "GroupHeader3";
            // 
            // lblProektant
            // 
            this.lblProektant.Height = 0.2F;
            this.lblProektant.HyperLink = null;
            this.lblProektant.Left = 0F;
            this.lblProektant.Name = "lblProektant";
            this.lblProektant.Style = "font-size: 9pt; font-weight: bold";
            this.lblProektant.Text = "������ ���������:";
            this.lblProektant.Top = 0F;
            this.lblProektant.Width = 1.811024F;
            // 
            // txtProektant
            // 
            this.txtProektant.Height = 0.2F;
            this.txtProektant.Left = 1.811024F;
            this.txtProektant.Name = "txtProektant";
            this.txtProektant.Style = "font-size: 9pt";
            this.txtProektant.Text = "txtProektant";
            this.txtProektant.Top = 0.001886666F;
            this.txtProektant.Width = 4.291338F;
            // 
            // GroupFooter3
            // 
            this.GroupFooter3.Height = 0F;
            this.GroupFooter3.Name = "GroupFooter3";
            this.GroupFooter3.Visible = false;
            // 
            // GroupHeader5
            // 
            this.GroupHeader5.CanShrink = true;
            this.GroupHeader5.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
            this.lblFilter1,
            this.txtFilter1,
            this.lblFilter2,
            this.txtFilter2});
            this.GroupHeader5.Height = 0.3930556F;
            this.GroupHeader5.Name = "GroupHeader5";
            // 
            // lblFilter1
            // 
            this.lblFilter1.Height = 0.2F;
            this.lblFilter1.HyperLink = null;
            this.lblFilter1.Left = 0F;
            this.lblFilter1.Name = "lblFilter1";
            this.lblFilter1.Style = "font-size: 9pt; font-weight: bold; vertical-align: middle";
            this.lblFilter1.Text = "";
            this.lblFilter1.Top = 0F;
            this.lblFilter1.Visible = false;
            this.lblFilter1.Width = 1.811024F;
            // 
            // txtFilter1
            // 
            this.txtFilter1.Height = 0.2F;
            this.txtFilter1.Left = 1.811024F;
            this.txtFilter1.Name = "txtFilter1";
            this.txtFilter1.Style = "font-size: 9pt; vertical-align: middle";
            this.txtFilter1.Text = null;
            this.txtFilter1.Top = 0F;
            this.txtFilter1.Visible = false;
            this.txtFilter1.Width = 4.291338F;
            // 
            // lblFilter2
            // 
            this.lblFilter2.Height = 0.2F;
            this.lblFilter2.HyperLink = null;
            this.lblFilter2.Left = 0F;
            this.lblFilter2.Name = "lblFilter2";
            this.lblFilter2.Style = "font-size: 9pt; font-weight: bold; vertical-align: middle";
            this.lblFilter2.Text = "";
            this.lblFilter2.Top = 0.1968504F;
            this.lblFilter2.Visible = false;
            this.lblFilter2.Width = 1.811024F;
            // 
            // txtFilter2
            // 
            this.txtFilter2.Height = 0.2F;
            this.txtFilter2.Left = 1.811024F;
            this.txtFilter2.Name = "txtFilter2";
            this.txtFilter2.Style = "font-size: 9pt; vertical-align: middle";
            this.txtFilter2.Text = null;
            this.txtFilter2.Top = 0.1968504F;
            this.txtFilter2.Visible = false;
            this.txtFilter2.Width = 4.291338F;
            // 
            // GroupFooter5
            // 
            this.GroupFooter5.Height = 0F;
            this.GroupFooter5.Name = "GroupFooter5";
            this.GroupFooter5.Visible = false;
            // 
            // GroupHeader4
            // 
            this.GroupHeader4.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
            this.txtTitle,
            this.Line10});
            this.GroupHeader4.Height = 1.436806F;
            this.GroupHeader4.Name = "GroupHeader4";
            // 
            // txtTitle
            // 
            this.txtTitle.Height = 0.1968504F;
            this.txtTitle.Left = 0F;
            this.txtTitle.Name = "txtTitle";
            this.txtTitle.Style = "font-size: 11pt; font-weight: bold; text-align: center; vertical-align: top";
            this.txtTitle.Text = "�������� �� �����";
            this.txtTitle.Top = 0.5905511F;
            this.txtTitle.Width = 7.086611F;
            // 
            // Line10
            // 
            this.Line10.Height = 0F;
            this.Line10.Left = 0F;
            this.Line10.LineWeight = 2F;
            this.Line10.Name = "Line10";
            this.Line10.Top = 1.377953F;
            this.Line10.Visible = false;
            this.Line10.Width = 7.086611F;
            this.Line10.X1 = 0F;
            this.Line10.X2 = 7.086611F;
            this.Line10.Y1 = 1.377953F;
            this.Line10.Y2 = 1.377953F;
            // 
            // GroupFooter4
            // 
            this.GroupFooter4.Height = 0F;
            this.GroupFooter4.Name = "GroupFooter4";
            this.GroupFooter4.Visible = false;
            // 
            // MinutesHeader
            // 
            this.MasterReport = false;
            this.PageSettings.PaperHeight = 11F;
            this.PageSettings.PaperWidth = 8.5F;
            this.PrintWidth = 7.458333F;
            this.Sections.Add(this.GroupHeader1);
            this.Sections.Add(this.GroupHeader2);
            this.Sections.Add(this.GroupHeader3);
            this.Sections.Add(this.GroupHeader5);
            this.Sections.Add(this.GroupHeader4);
            this.Sections.Add(this.Detail);
            this.Sections.Add(this.GroupFooter4);
            this.Sections.Add(this.GroupFooter5);
            this.Sections.Add(this.GroupFooter3);
            this.Sections.Add(this.GroupFooter2);
            this.Sections.Add(this.GroupFooter1);
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule(resources.GetString("$this.StyleSheet"), "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: inherit; font-style: inherit; font-variant: inherit; font-weight: bo" +
            "ld; font-size: 16pt; font-size-adjust: inherit; font-stretch: inherit", "Heading1", "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: Times New Roman; font-style: italic; font-variant: inherit; font-wei" +
            "ght: bold; font-size: 14pt; font-size-adjust: inherit; font-stretch: inherit", "Heading2", "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: inherit; font-style: inherit; font-variant: inherit; font-weight: bo" +
            "ld; font-size: 13pt; font-size-adjust: inherit; font-stretch: inherit", "Heading3", "Normal"));
            ((System.ComponentModel.ISupportInitialize)(this.lblObekt)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtObekt)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblClient)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtClient)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtClientAddress)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblProektant)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtProektant)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblFilter1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtFilter1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblFilter2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtFilter2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTitle)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();

        }

		#endregion

        private GroupHeader GroupHeader1;
        private Label lblObekt;
        private TextBox txtObekt;
        private Label Label3;
        private GroupHeader GroupHeader2;
        private Label lblClient;
        private TextBox txtClient;
        private TextBox txtClientAddress;
        private GroupHeader GroupHeader3;
        private Label lblProektant;
        private TextBox txtProektant;
        private GroupHeader GroupHeader5;
        private Label lblFilter1;
        private TextBox txtFilter1;
        private Label lblFilter2;
        private TextBox txtFilter2;
        private GroupHeader GroupHeader4;
        private TextBox txtTitle;
        private Line Line10;
        private Detail Detail;
        private GroupFooter GroupFooter4;
        private GroupFooter GroupFooter5;
        private GroupFooter GroupFooter3;
        private GroupFooter GroupFooter2;
        private GroupFooter GroupFooter1;
	}
}
