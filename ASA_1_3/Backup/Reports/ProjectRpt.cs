using System;
using System.Data;
using GrapeCity.ActiveReports;
using GrapeCity.ActiveReports.Controls;
using GrapeCity.ActiveReports.SectionReportModel;
using GrapeCity.ActiveReports.Document.Section;
using GrapeCity.ActiveReports.Document;

namespace Asa.Timesheet.WebPages.Reports
{
    public class ProjectRpt : GrapeCity.ActiveReports.SectionReport
	{
        public ProjectRpt(bool pdf, string title, string type, string project, string client, DataView dv1, DataView dv2, bool bClient, bool isPUP, DataView dv1ForAll, bool bFinPokazateli, DataView dvFinPokazateli, string createdBy, DataView dvBGN, DataView dvWorkTime)
        {
            this.PageSettings.Margins.Right = 0;
            _forPDF = pdf;
            InitializeComponent();
            lbTitle.Text = title;
            lbZad.Text = type;
            lbProject.Text = project;
            lbClient.Text = client;
            if (bClient)
            {
                Label5.Visible = Label6.Visible = Label7.Visible = Label8.Visible =
                    lbProject.Visible = lbClient.Visible = false;
            }
            this.ReportEnd += new EventHandler(SubAnalysisRpt_ReportEnd);
            if (dv1ForAll.Count > 0)
            {
                this.SubReport3.Report = new ProjectSubreport1(dv1ForAll, false);
                this.SubReport1.Report = new ProjectSubreport1(dv1, true);
            }
            else
            {
                if (dv1.Count > 0)
                    this.SubReport1.Report = new ProjectSubreport1(dv1, isPUP);
                else
                    lbCL.Visible = false;
                this.SubReport3.Visible = this.Label9.Visible = false;
            }
            if (dv2.Count > 0 && !bClient)
                this.SubReport2.Report = new ProjectSubreport2(dv2);
            else lbPh.Visible = false;
            if (dvWorkTime != null && dvWorkTime.Count > 0 && !bClient)
                SubReport6.Report = new ProjectSubreport3(dvWorkTime);
            else
                Label12.Visible = false;

            if (bFinPokazateli)
            {
                SubReport4.Report = new FinancePokazateliSub(dvFinPokazateli);
                //SubReport5.Report= new FinancePokazateliSub1(dvBGN);
            }
            else
            {
                Label10.Visible = Label11.Visible = false;
                this.PrintWidth = 10f;
            }

        }
		private bool _forPDF=true;
		#region ActiveReports Designer generated code




























        public void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ProjectRpt));
            this.Detail = new GrapeCity.ActiveReports.SectionReportModel.Detail();
            this.ReportHeader = new GrapeCity.ActiveReports.SectionReportModel.ReportHeader();
            this.ReportFooter = new GrapeCity.ActiveReports.SectionReportModel.ReportFooter();
            this.GroupHeader1 = new GrapeCity.ActiveReports.SectionReportModel.GroupHeader();
            this.GroupFooter1 = new GrapeCity.ActiveReports.SectionReportModel.GroupFooter();
            this.GroupHeader2 = new GrapeCity.ActiveReports.SectionReportModel.GroupHeader();
            this.GroupFooter2 = new GrapeCity.ActiveReports.SectionReportModel.GroupFooter();
            this.lbTitle = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label4 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label5 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label6 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lbZad = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lbProject = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lbClient = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.SubReport1 = new GrapeCity.ActiveReports.SectionReportModel.SubReport();
            this.lbCL = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.SubReport3 = new GrapeCity.ActiveReports.SectionReportModel.SubReport();
            this.Label9 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label12 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.SubReport6 = new GrapeCity.ActiveReports.SectionReportModel.SubReport();
            this.lbPh = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.SubReport2 = new GrapeCity.ActiveReports.SectionReportModel.SubReport();
            this.Label10 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.SubReport4 = new GrapeCity.ActiveReports.SectionReportModel.SubReport();
            this.Label11 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.SubReport5 = new GrapeCity.ActiveReports.SectionReportModel.SubReport();
            this.Label7 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label8 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            ((System.ComponentModel.ISupportInitialize)(this.lbTitle)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbZad)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbProject)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbClient)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbCL)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label12)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbPh)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label11)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            // 
            // Detail
            // 
            this.Detail.ColumnSpacing = 0F;
            this.Detail.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
						this.lbPh,
						this.SubReport2});
            this.Detail.Height = 0.5F;
            this.Detail.Name = "Detail";
            // 
            // ReportHeader
            // 
            this.ReportHeader.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
						this.lbTitle,
						this.Label4,
						this.Label5,
						this.Label6,
						this.lbZad,
						this.lbProject,
						this.lbClient,
						this.SubReport1,
						this.lbCL,
						this.SubReport3,
						this.Label9,
						this.Label12,
						this.SubReport6});
            this.ReportHeader.Height = 2.510417F;
            this.ReportHeader.Name = "ReportHeader";
            // 
            // ReportFooter
            // 
            this.ReportFooter.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
						this.Label7,
						this.Label8});
            this.ReportFooter.Height = 1.270833F;
            this.ReportFooter.Name = "ReportFooter";
            this.ReportFooter.Visible = false;
            // 
            // GroupHeader1
            // 
            this.GroupHeader1.Height = 0F;
            this.GroupHeader1.Name = "GroupHeader1";
            // 
            // GroupFooter1
            // 
            this.GroupFooter1.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
						this.Label11,
						this.SubReport5});
            this.GroupFooter1.Height = 0.5104167F;
            this.GroupFooter1.Name = "GroupFooter1";
            // 
            // GroupHeader2
            // 
            this.GroupHeader2.Height = 0F;
            this.GroupHeader2.Name = "GroupHeader2";
            // 
            // GroupFooter2
            // 
            this.GroupFooter2.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
						this.Label10,
						this.SubReport4});
            this.GroupFooter2.Height = 0.5506945F;
            this.GroupFooter2.Name = "GroupFooter2";
            // 
            // lbTitle
            // 
            this.lbTitle.Height = 0.2F;
            this.lbTitle.HyperLink = null;
            this.lbTitle.Left = 0.625F;
            this.lbTitle.Name = "lbTitle";
            this.lbTitle.Style = "font-size: 12pt; font-weight: bold; text-align: center";
            this.lbTitle.Text = "Label4";
            this.lbTitle.Top = 0F;
            this.lbTitle.Width = 9.15F;
            // 
            // Label4
            // 
            this.Label4.Height = 0.2F;
            this.Label4.HyperLink = null;
            this.Label4.Left = 0.625F;
            this.Label4.Name = "Label4";
            this.Label4.Style = "font-weight: bold";
            this.Label4.Text = "??????????:";
            this.Label4.Top = 0.4375F;
            this.Label4.Width = 2.375F;
            // 
            // Label5
            // 
            this.Label5.Height = 0.2F;
            this.Label5.HyperLink = null;
            this.Label5.Left = 0.625F;
            this.Label5.Name = "Label5";
            this.Label5.Style = "font-weight: bold";
            this.Label5.Text = "????????? (EUR):*";
            this.Label5.Top = 0.625F;
            this.Label5.Width = 2.375F;
            // 
            // Label6
            // 
            this.Label6.Height = 0.2000001F;
            this.Label6.HyperLink = null;
            this.Label6.Left = 0.625F;
            this.Label6.Name = "Label6";
            this.Label6.Style = "font-weight: bold";
            this.Label6.Text = "?????????(??? ??????., EUR):**";
            this.Label6.Top = 0.8125F;
            this.Label6.Width = 2.375F;
            // 
            // lbZad
            // 
            this.lbZad.Height = 0.2F;
            this.lbZad.HyperLink = null;
            this.lbZad.Left = 3.0625F;
            this.lbZad.Name = "lbZad";
            this.lbZad.Style = "";
            this.lbZad.Text = "";
            this.lbZad.Top = 0.4375F;
            this.lbZad.Width = 6.775F;
            // 
            // lbProject
            // 
            this.lbProject.Height = 0.2000001F;
            this.lbProject.HyperLink = null;
            this.lbProject.Left = 3.0625F;
            this.lbProject.Name = "lbProject";
            this.lbProject.Style = "";
            this.lbProject.Text = "";
            this.lbProject.Top = 0.625F;
            this.lbProject.Width = 6.775F;
            // 
            // lbClient
            // 
            this.lbClient.Height = 0.2000001F;
            this.lbClient.HyperLink = null;
            this.lbClient.Left = 3.0625F;
            this.lbClient.Name = "lbClient";
            this.lbClient.Style = "";
            this.lbClient.Text = "";
            this.lbClient.Top = 0.8125F;
            this.lbClient.Width = 6.775F;
            // 
            // SubReport1
            // 
            this.SubReport1.CloseBorder = false;
            this.SubReport1.Height = 0.188F;
            this.SubReport1.Left = 0.625F;
            this.SubReport1.Name = "SubReport1";
            this.SubReport1.Report = null;
            this.SubReport1.Top = 1.375F;
            this.SubReport1.Width = 10.813F;
            // 
            // lbCL
            // 
            this.lbCL.Height = 0.2000001F;
            this.lbCL.HyperLink = null;
            this.lbCL.Left = 0.625F;
            this.lbCL.Name = "lbCL";
            this.lbCL.Style = "font-weight: bold";
            this.lbCL.Text = "??? ???????:";
            this.lbCL.Top = 1.1875F;
            this.lbCL.Width = 9.15F;
            // 
            // SubReport3
            // 
            this.SubReport3.CloseBorder = false;
            this.SubReport3.Height = 0.1875F;
            this.SubReport3.Left = 0.625F;
            this.SubReport3.Name = "SubReport3";
            this.SubReport3.Report = null;
            this.SubReport3.Top = 1.8125F;
            this.SubReport3.Width = 10.8125F;
            // 
            // Label9
            // 
            this.Label9.Height = 0.2000001F;
            this.Label9.HyperLink = null;
            this.Label9.Left = 0.625F;
            this.Label9.Name = "Label9";
            this.Label9.Style = "font-weight: bold";
            this.Label9.Text = "??????? ? ???????? ?? ????:";
            this.Label9.Top = 1.625F;
            this.Label9.Width = 9.15F;
            // 
            // Label12
            // 
            this.Label12.Height = 0.2000001F;
            this.Label12.HyperLink = null;
            this.Label12.Left = 0.625F;
            this.Label12.Name = "Label12";
            this.Label12.Style = "font-weight: bold";
            this.Label12.Text = "??????? ?????:";
            this.Label12.Top = 2.0625F;
            this.Label12.Width = 9.15F;
            // 
            // SubReport6
            // 
            this.SubReport6.CloseBorder = false;
            this.SubReport6.Height = 0.188F;
            this.SubReport6.Left = 0.625F;
            this.SubReport6.Name = "SubReport6";
            this.SubReport6.Report = null;
            this.SubReport6.Top = 2.25F;
            this.SubReport6.Width = 10.813F;
            // 
            // lbPh
            // 
            this.lbPh.Height = 0.2000001F;
            this.lbPh.HyperLink = null;
            this.lbPh.Left = 0.625F;
            this.lbPh.Name = "lbPh";
            this.lbPh.Style = "font-weight: bold";
            this.lbPh.Text = "??? ??????????????:";
            this.lbPh.Top = 0.0625F;
            this.lbPh.Width = 9.15F;
            // 
            // SubReport2
            // 
            this.SubReport2.CloseBorder = false;
            this.SubReport2.Height = 0.188F;
            this.SubReport2.Left = 0.625F;
            this.SubReport2.Name = "SubReport2";
            this.SubReport2.Report = null;
            this.SubReport2.Top = 0.25F;
            this.SubReport2.Width = 10.813F;
            // 
            // Label10
            // 
            this.Label10.Height = 0.2000001F;
            this.Label10.HyperLink = null;
            this.Label10.Left = 0.625F;
            this.Label10.Name = "Label10";
            this.Label10.Style = "font-weight: bold";
            this.Label10.Text = "????????? ??????????(EUR):";
            this.Label10.Top = 0.1875F;
            this.Label10.Width = 9.15F;
            // 
            // SubReport4
            // 
            this.SubReport4.CloseBorder = false;
            this.SubReport4.Height = 0.188F;
            this.SubReport4.Left = 0.0625F;
            this.SubReport4.Name = "SubReport4";
            this.SubReport4.Report = null;
            this.SubReport4.Top = 0.375F;
            this.SubReport4.Width = 14.5F;
            // 
            // Label11
            // 
            this.Label11.Height = 0.2000001F;
            this.Label11.HyperLink = null;
            this.Label11.Left = 0.625F;
            this.Label11.Name = "Label11";
            this.Label11.Style = "font-weight: bold";
            this.Label11.Text = "????????? ??????????(BGN):";
            this.Label11.Top = 0.0625F;
            this.Label11.Width = 9.15F;
            // 
            // SubReport5
            // 
            this.SubReport5.CloseBorder = false;
            this.SubReport5.Height = 0.1875F;
            this.SubReport5.Left = 0.0625F;
            this.SubReport5.Name = "SubReport5";
            this.SubReport5.Report = null;
            this.SubReport5.Top = 0.25F;
            this.SubReport5.Width = 14.5F;
            // 
            // Label7
            // 
            this.Label7.Height = 0.25F;
            this.Label7.HyperLink = null;
            this.Label7.Left = 1F;
            this.Label7.Name = "Label7";
            this.Label7.Style = "font-size: 9pt";
            this.Label7.Text = "* ????????? ? ???? ?? ?????? ???????? ?? ??????? ????? ???????? ??? ?????????????" +
                "? ????????? ?? ???? ?????? ???????? ?? ????????? ?? ???????  ";
            this.Label7.Top = 0.5625F;
            this.Label7.Width = 9.15F;
            // 
            // Label8
            // 
            this.Label8.Height = 0.25F;
            this.Label8.HyperLink = null;
            this.Label8.Left = 1F;
            this.Label8.Name = "Label8";
            this.Label8.Style = "font-size: 9pt";
            this.Label8.Text = "** ?????????(??? ??????????????) ? ???? ?? ?????? ???????? ?? ??????? ????????? ?" +
                "? ???? ?????? ???????? ?? ????????? ?? ??????? ";
            this.Label8.Top = 0.8125F;
            this.Label8.Width = 9.15F;
            // 
            // ActiveReport1
            // 
            this.MasterReport = false;
            this.PageSettings.PaperHeight = 11F;
            this.PageSettings.PaperWidth = 8.5F;
            this.PrintWidth = 14.63542F;
            this.Sections.Add(this.ReportHeader);
            this.Sections.Add(this.GroupHeader1);
            this.Sections.Add(this.GroupHeader2);
            this.Sections.Add(this.Detail);
            this.Sections.Add(this.GroupFooter2);
            this.Sections.Add(this.GroupFooter1);
            this.Sections.Add(this.ReportFooter);
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule(resources.GetString("$this.StyleSheet"), "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: inherit; font-style: inherit; font-variant: inherit; font-weight: bo" +
                        "ld; font-size: 16pt; font-size-adjust: inherit; font-stretch: inherit", "Heading1", "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: Times New Roman; font-style: italic; font-variant: inherit; font-wei" +
                        "ght: bold; font-size: 14pt; font-size-adjust: inherit; font-stretch: inherit", "Heading2", "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: inherit; font-style: inherit; font-variant: inherit; font-weight: bo" +
                        "ld; font-size: 13pt; font-size-adjust: inherit; font-stretch: inherit", "Heading3", "Normal"));
            ((System.ComponentModel.ISupportInitialize)(this.lbTitle)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbZad)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbProject)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbClient)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbCL)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label12)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbPh)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label11)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();
        }

		#endregion

        private void SubAnalysisRpt_ReportEnd(object sender, EventArgs e)
        {
            if (!this._forPDF) return;

            Logo1 logo = new Logo1();
            logo.Run();

            for (int i = 0; i < this.Document.Pages.Count; i++)
                this.Document.Pages[i].Overlay(logo.Document.Pages[0]);
        }

        private void SubAnalysisRpt_ReportStart(object sender, EventArgs e)
        {
            //			if (!_forPDF)
            //			{
            //				this.PageSettings.Margins.Left = CmToInch(2f);
            //				this.PrintWidth = CmToInch(17f);
            //				Label1.Location = new System.Drawing.PointF(0, Label1.Location.Y);
            //				lbTitle
            //				// lblObekt.Width = CmToInch(6f);
            ////				txtObekt.Location = new System.Drawing.PointF(lblObekt.Location.X+lblObekt.Width, txtObekt.Location.Y);
            ////				txtAddress.Location = new System.Drawing.PointF(txtObekt.Location.X, txtAddress.Location.Y);
            ////
            ////				this.SubReport1.Location = new System.Drawing.PointF(0, this.SubReport1.Location.Y);
            //		}
        }

        private ReportHeader ReportHeader;
        private Label lbTitle;
        private Label Label4;
        private Label Label5;
        private Label Label6;
        private Label lbZad;
        private Label lbProject;
        private Label lbClient;
        private SubReport SubReport1;
        private Label lbCL;
        private SubReport SubReport3;
        private Label Label9;
        private Label Label12;
        private SubReport SubReport6;
        private GroupHeader GroupHeader1;
        private GroupHeader GroupHeader2;
        private Detail Detail;
        private Label lbPh;
        private SubReport SubReport2;
        private GroupFooter GroupFooter2;
        private Label Label10;
        private SubReport SubReport4;
        private GroupFooter GroupFooter1;
        private Label Label11;
        private SubReport SubReport5;
        private ReportFooter ReportFooter;
        private Label Label7;
        private Label Label8;
	}
}
