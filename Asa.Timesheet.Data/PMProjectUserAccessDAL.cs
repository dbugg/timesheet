using System;
using System.Data;
using System.Data.SqlClient; 
using System.Collections;
using Microsoft.ApplicationBlocks.Data;

namespace Asa.Timesheet.Data
{
	public class PMProjectUserAccessDAL
	{
		/// <summary>
		/// Loads a PMProjectUserAccessData object from the database using the given PMProjectUserAccessID
		/// </summary>
		/// <param name="PMProjectUserAccessID">The primary key value</param>
		/// <returns>A PMProjectUserAccessData object</returns>
		public static PMProjectUserAccessData Load(int PMProjectUserAccessID)
		{
			SqlParameter[] parameterValues = new SqlParameter[] { new SqlParameter("@PMProjectUserAccessID", PMProjectUserAccessID) };
			using (SqlDataReader reader = SqlHelper.ExecuteReader(CommonDAL.ConnectionString, CommandType.StoredProcedure, "PMProjectUserAccessSelProc", parameterValues))
			{
				if (reader.Read())
				{
					PMProjectUserAccessData result = new PMProjectUserAccessData();
					LoadFromReader(reader,result);
					return result;
				}
				else
					return null;
			}
		}

		/// <summary>
		/// Loads a PMProjectUserAccessData object from the database using the given where clause
		/// </summary>
		/// <param name="whereClause">The filter expression for the query</param>
		/// <returns>A PMProjectUserAccessData object</returns>
		public static PMProjectUserAccessData Load(string whereClause)
		{
			SqlParameter[] parameterValues = new SqlParameter[] { new SqlParameter("@WhereClause", whereClause) };
			using (SqlDataReader reader = SqlHelper.ExecuteReader(CommonDAL.ConnectionString, CommandType.StoredProcedure, "PMProjectUserAccessListProc", parameterValues))
			{
				if (reader.Read())
				{
					PMProjectUserAccessData result = new PMProjectUserAccessData();
					LoadFromReader(reader,result);
					return result;
				}
				else
					return null;
			}
		}
		public static int Save( PMProjectUserAccessData source)
		{
			if (source.PMProjectUserAccessID == -1)
				return Insert(source);
			else
				return Update(source);	
		}

		public static void Delete( PMProjectUserAccessData source)
		{
			Delete(source.PMProjectUserAccessID);
		}
		/// <summary>
		/// Loads a collection of PMProjectUserAccessData objects from the database.
		/// </summary>
		/// <returns>A collection containing all of the PMProjectUserAccessData objects in the database.</returns>
		public static PMProjectUserAccesssVector LoadCollection()
		{
			SqlParameter[] parms = new SqlParameter[] {};
			return LoadCollection("PMProjectUserAccessListProc", parms);
		}

		/// <summary>
		/// Loads a collection of PMProjectUserAccessData objects from the database.
		/// </summary>
		/// <returns>A collection containing all of the PMProjectUserAccessData objects in the database.</returns>
		public static PMProjectUserAccesssVector LoadCollectionForUser(int UserType,int UserID)
		{
			
			return LoadCollectionForUser(UserType,UserID,-1);
		}

		public static PMProjectUserAccesssVector LoadCollectionForUser(int UserType,int UserID,int ProjectID)
		{
			
			SqlParameter[] parms = new SqlParameter[] {   new SqlParameter("@UserType",(UserType>0)?(object)UserType:DBNull.Value),
														  new SqlParameter("@UserAccessID", (UserID>0)?(object)UserID:DBNull.Value),
															new SqlParameter("@ProjectID",(ProjectID>0)?(object)ProjectID:DBNull.Value),
			};
			return LoadCollection("PMProjectUserAccessListProc", parms);
		}
	  
		/// <summary>
		/// Saves a collection of PMProjectUserAccessData objects to the database.
		/// </summary>
		/// <returns>A collection containing all of the PMProjectUserAccessData objects in the database.</returns>
		public static  void  SaveCollection(PMProjectUserAccesssVector col,string spName, SqlParameter[] parms)
		{
			PMProjectUserAccesssVector colOld = LoadCollection(spName,parms);
			UpdateCollection(colOld,col);
		}
		public static  void  SaveCollection(PMProjectUserAccesssVector col)
		{
			PMProjectUserAccesssVector colOld = LoadCollection();
			UpdateCollection(colOld,col);
		}
		public static  void  SaveCollection(PMProjectUserAccesssVector col, string whereClause )
		{
			PMProjectUserAccesssVector colOld = LoadCollection(whereClause);
			UpdateCollection(colOld,col);
		}
		/// <summary>
		/// Loads a collection of PMProjectUserAccessData objects from the database.
		/// </summary>
		/// <returns>A collection containing all of the PMProjectUserAccessData objects in the database.</returns>
		public static PMProjectUserAccesssVector LoadCollection(string whereClause)
		{
			SqlParameter[] parms = new SqlParameter[] { new SqlParameter("@WhereClause", whereClause) };
			return LoadCollection("PMProjectUserAccessListProc", parms);
		}

		
		

		/// <summary>
		/// Loads a collection of PMProjectUserAccessData objects from the database.
		/// </summary>
		/// <returns>A collection containing all of the PMProjectUserAccessData objects in the database.</returns>
		public static PMProjectUserAccesssVector LoadCollection(string spName, SqlParameter[] parms)
		{
			PMProjectUserAccesssVector result = new PMProjectUserAccesssVector();
			using (SqlDataReader reader = SqlHelper.ExecuteReader(CommonDAL.ConnectionString, CommandType.StoredProcedure, spName, parms))
			{
				while (reader.Read())
				{
					PMProjectUserAccessData tmp = new PMProjectUserAccessData();
					LoadFromReader(reader, tmp);
					result.Add(tmp);
				}
			}
			return result;
		}
		/// <summary>
		/// Deletes a PMProjectUserAccessData object from the database.
		/// </summary>
		/// <param name="PMProjectUserAccessID">The primary key value</param>
		public static void Delete(int PMProjectUserAccessID)
		{
			SqlParameter[] parameterValues = new SqlParameter[] { new SqlParameter("@PMProjectUserAccessID", PMProjectUserAccessID) };
			SqlHelper.ExecuteNonQuery(CommonDAL.ConnectionString, CommandType.StoredProcedure, "PMProjectUserAccessDelProc", parameterValues);
		}

		public static void LoadFromReader(SqlDataReader reader, PMProjectUserAccessData target)
		{
			if (reader != null && !reader.IsClosed)
			{
				//target.Key = reader.GetInt32(0);
				if (!reader.IsDBNull(0)) target.PMProjectUserAccessID = reader.GetInt32(0);
				if (!reader.IsDBNull(1)) target.UserType = reader.GetInt32(1);
				if (!reader.IsDBNull(2)) target.UserAccessID = reader.GetInt32(2);
				if (!reader.IsDBNull(3)) target.ProjectID = reader.GetInt32(3);
				if (!reader.IsDBNull(4)) target.HasAccess = reader.GetBoolean(4);
				if (!reader.IsDBNull(5)) target.IsActiveGroup = reader.GetBoolean(5);
				if (!reader.IsDBNull(6)) target.ProjectMail = reader.GetString(6);
				if (!reader.IsDBNull(7)) target.Responsible = reader.GetString(7);
			}
		}
		
	  
		public static int Insert( PMProjectUserAccessData source)
		{
			SqlParameter[] parameterValues = GetInsertParameterValues(source);
			SqlHelper.ExecuteNonQuery(CommonDAL.ConnectionString, CommandType.StoredProcedure, "PMProjectUserAccessInsProc", parameterValues);
			source.PMProjectUserAccessID = (int) parameterValues[0].Value;
			return source.PMProjectUserAccessID;
		}

		public static int Update( PMProjectUserAccessData source)
		{
			SqlParameter[] parameterValues = GetUpdateParameterValues(source);
			SqlHelper.ExecuteNonQuery(CommonDAL.ConnectionString, CommandType.StoredProcedure,
				"PMProjectUserAccessUpdProc", parameterValues);
			return source.PMProjectUserAccessID;
		}
		private static void UpdateCollection(PMProjectUserAccesssVector colOld,PMProjectUserAccesssVector col)
		{
			// Delete all old items
			foreach( PMProjectUserAccessData itemOld in colOld)
			{
				if(!ExistsKey(itemOld.PMProjectUserAccessID, col))
					Delete(itemOld.PMProjectUserAccessID);
			}
			foreach( PMProjectUserAccessData item in col)
			{
				if(item.PMProjectUserAccessID <= -1)
					Insert(item);
				else
					Update(item);
			}
		}
		private static bool ExistsKey(int Key, PMProjectUserAccesssVector col)
		{
			foreach( PMProjectUserAccessData item in col)
				if(item.PMProjectUserAccessID==Key)
					return true;
			return false;
		}

		private static SqlParameter GetSqlParameter(string name, ParameterDirection direction, object value)
		{
			SqlParameter p = new SqlParameter(name, value);
			p.Direction = direction;
			return p;
		}

		private static SqlParameter[] GetInsertParameterValues( PMProjectUserAccessData source)
		{
			SqlParameter[] prams = new SqlParameter[8];
			prams[0] = GetSqlParameter("@PMProjectUserAccessID", ParameterDirection.Output, source.PMProjectUserAccessID);
			prams[1] = GetSqlParameter("@UserType", ParameterDirection.Input,  source.UserType);
			prams[2] = GetSqlParameter("@UserAccessID", ParameterDirection.Input, source.UserAccessID);
			prams[3] = GetSqlParameter("@ProjectID", ParameterDirection.Input, source.ProjectID);
			prams[4] = GetSqlParameter("@HasAccess", ParameterDirection.Input, source.HasAccess);
			prams[5] = GetSqlParameter("@IsActiveGroup", ParameterDirection.Input, source.IsActiveGroup);
			prams[6] = GetSqlParameter("@ProjectMail", ParameterDirection.Input, (source.ProjectMail.Length>0)?(object)source.ProjectMail:DBNull.Value);
			prams[7] = GetSqlParameter("@Responsible", ParameterDirection.Input, (source.Responsible.Length>0)?(object)source.Responsible:DBNull.Value);
			return prams;
		}

		private static SqlParameter[] GetUpdateParameterValues( PMProjectUserAccessData source)
		{
			return new SqlParameter[] {
										  new SqlParameter("@PMProjectUserAccessID", source.PMProjectUserAccessID),
										  new SqlParameter("@UserType", source.UserType),
										  new SqlParameter("@UserAccessID", source.UserAccessID),
										  new SqlParameter("@ProjectID", source.ProjectID),
										  new SqlParameter("@HasAccess", source.HasAccess),
									      new SqlParameter("@IsActiveGroup", source.IsActiveGroup),
										  new SqlParameter("@ProjectMail",(source.ProjectMail.Length>0)?(object)source.ProjectMail:DBNull.Value),
											new SqlParameter("@Responsible",(source.Responsible.Length>0)?(object)source.Responsible:DBNull.Value)
			};
		}
	}
}
