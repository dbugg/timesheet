﻿<%@ Page language="c#" Codebehind="MontlyExpence.aspx.cs" AutoEventWireup="false" Inherits="Asa.Timesheet.WebPages.Reports.MontlyExpence" %>
<%@ Register TagPrefix="uc1" TagName="PageHeader" Src="../UserControls/PageHeader.ascx" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
	<head runat="server">
		<title>MontlyExpence</title>
		<meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
		<meta content="C#" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<LINK href="../styles/timesheet.css" type="text/css" rel="stylesheet">
	</HEAD>
	<body bottomMargin="0" leftMargin="0" topMargin="0" rightMargin="0">
		<form id="Form1" method="post" runat="server">
			<table height="100%" cellSpacing="0" cellPadding="0" width="100%" border="0">
				<tr height="1">
					<td><uc1:pageheader id="header" runat="server"></uc1:pageheader></td>
				</tr>
				<tr style="DISPLAY: none" height="80">
					<td background="../images/copy1.gif">
						<TABLE id="Table2" height="100%" width="100%">
							<TR>
								<td style="WIDTH: 106px">&nbsp;&nbsp;&nbsp;&nbsp; <IMG height="80" alt="" src="../images/logo.jpg" width="64"></TD>
								<td noWrap><asp:label id="lblTitle" runat="server" Font-Size="X-Small" Font-Bold="True"></asp:label><br>
									<asp:label id="lblUser" runat="server" Font-Size="Medium" Font-Bold="True" ForeColor="SaddleBrown"></asp:label></TD>
							</TR>
						</TABLE>
					</td>
				</tr>
				<tr>
					<td>
						<table height="100%" cellSpacing="0" cellPadding="0" width="100%" border="0">
							<tr>
								<td class="menuLeft"><asp:placeholder id="menuHolder" runat="server"></asp:placeholder></td>
								
								<td style="PADDING-RIGHT: 5px; PADDING-LEFT: 5px; PADDING-TOP: 3px" vAlign="top" noWrap>
									<TABLE id="tblInfo" height="30" cellSpacing="0" cellPadding="3" width="880" border="0">
										<TR>
											<td vAlign="middle" noWrap><asp:label id="lblError" runat="server" ForeColor="Red" EnableViewState="False" CssClass="ErrorLabel"></asp:label><asp:label id="lblInfo" runat="server" EnableViewState="False" CssClass="InfoLabel"></asp:label></TD>
										</TR>
									</TABLE>
									<TABLE id="Table3" cellSpacing="0" cellPadding="0" width="880" border="0">
										<TR>
											<td style="HEIGHT: 18px" colSpan="2"></TD>
										</TR>
										<tr>
											<td>
												<table cellSpacing="0" cellPadding="3" border="0">
													<TR>
														<td style="WIDTH: 159px" vAlign="top">
															<asp:label id="Label9" runat="server" CssClass="EnterDataLabel" Width="100%">Месец:</asp:label></TD>
														<td style="WIDTH: 243px" vAlign="top"><asp:dropdownlist id="ddlMonth" runat="server" CssClass="EnterDataBox" Width="250px" AutoPostBack="True"></asp:dropdownlist></TD>
													</TR>
													<TR>
														<td style="WIDTH: 159px" vAlign="top">
															<asp:label id="lbExpences" runat="server" CssClass="EnterDataLabel" Width="100%">АСА Разходи (EUR):</asp:label></TD>
														<td style="WIDTH: 243px" vAlign="top"><asp:TextBox ID="txtMonthExpence" Runat="server" CssClass="EnterDataBox"></asp:TextBox></TD>
													</TR>
													<TR>
														<td style="WIDTH: 159px" vAlign="top">
															<asp:label id="Label1" runat="server" CssClass="EnterDataLabel" Width="100%">АСИ Разходи (EUR):</asp:label></TD>
														<td style="WIDTH: 243px" vAlign="top">
															<asp:TextBox id="txtASI" CssClass="EnterDataBox" Runat="server"></asp:TextBox></TD>
													</TR>
													<TR>
														<td style="WIDTH: 159px" vAlign="top"></TD>
														<td style="WIDTH: 243px" vAlign="top"><asp:Button ID="btnSave" Runat="server" CssClass="ActionButton" Text="Запиши"></asp:Button>
															<asp:label id="lbID" runat="server" Visible="False"></asp:label></TD>
													</TR>
												</table>
											</td>
										</tr>
										<TR>
											<td style="HEIGHT: 5px" colSpan="2"></TD>
										</TR>
										<TR>
											<td colSpan="2"><IMG height="1" src="../images/dot.gif" width="100%"></TD>
										</TR>
										<TR>
											<td colSpan="2" height="3"><asp:panel id="grid" 
													runat="server" Width="98%" Height="450px">
													<asp:datagrid id="grdMain" runat="server" CssClass="Grid" Width="100%" AutoGenerateColumns="False"
														CellPadding="4" PageSize="2" AllowSorting="True">
														<ItemStyle CssClass="GridItem"></ItemStyle>
														<HeaderStyle Wrap="False" CssClass="GridHeader"></HeaderStyle>
														<Columns>
															<asp:TemplateColumn HeaderText="Месец/Година">
																<HeaderStyle Wrap="False" Width="30px"></HeaderStyle>
																<ItemStyle ForeColor="DimGray"></ItemStyle>
																<ItemTemplate>
																	<asp:Label id="lnk" runat="server">
																		<span Class="menuTable">
																			<%# GetMonth((DateTime)DataBinder.Eval(Container, "DataItem.MonthOfYear"))%>
																		</span>
																	</asp:Label>
																</ItemTemplate>
															</asp:TemplateColumn>
															<asp:BoundColumn DataField="Expences" HeaderText="АСА Разходи (EUR)">
																<HeaderStyle Wrap="False" Width="70px"></HeaderStyle>
																<ItemStyle ForeColor="DimGray"></ItemStyle>
															</asp:BoundColumn>
															<asp:BoundColumn DataField="ExpencesASI" HeaderText="АСИ Разходи (EUR)">
																<HeaderStyle Wrap="False" Width="70px"></HeaderStyle>
																<ItemStyle ForeColor="DimGray"></ItemStyle>
															</asp:BoundColumn>
														</Columns>
													</asp:datagrid>
												</asp:panel></TD>
										</TR>
										<TR>
											<td colSpan="2"><IMG height="1" src="images/dot.gif" width="100%"></TD>
										</TR>
									</TABLE>
								</td>
							</tr>
						</table>
					</td>
				</tr>
			</table>
		</form>
	</body>
</HTML>
