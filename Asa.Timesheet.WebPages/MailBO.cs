using System;
using System.Web;
using System.Collections;
using System.Web.UI.WebControls;
using System.Web.Caching;
using System.Configuration;
using System.Web.Security;
using System.Data;
using System.Web.Mail;
using System.Text;
using System.Collections.Specialized;
using Asa.Timesheet.Data;
using log4net;
using Asa.Timesheet.Data.Entities;

namespace Asa.Timesheet.WebPages
{
	
	/// <summary>
	/// Summary description for MailBO.
	/// </summary>
	public class MailBO
	{
		private static readonly ILog log = LogManager.GetLogger(typeof(MailBO));
		private const string _BR="<BR>";
		private const string _SP=" ";
		private const string _ITEM=" - ";
		
		#region getmail text old

		public static string GetMailText1(MailInfo mi, out string subject)
		{
			decimal area = 0;
			subject="";

			string projectName, projectCode, administrativeName,add;
			int clientID=0;
			bool phases;
			if (!ProjectsData.SelectProject(mi.ProjectID, out projectName, out  projectCode, out administrativeName, out area, out add, 
				out clientID,out phases))
			{
				log.Error("Project not found" + mi.ProjectID);
				return null;
			}

			UserInfo ui = UsersData.SelectUserByID(mi.UserID);
			if(ui==null)
			{
				log.Error("User not found" + mi.UserID);
				return null;
			}
//			if(mi.IsFromHours)
//				subject = ui.UserName;
//			else
			subject = string.Concat(projectCode,_SP,projectName);

			StringBuilder sb = new StringBuilder();
			//project
			sb.Append("<H4>");
			sb.Append(Resource.ResourceManager["mail_Project"]);
			sb.Append(projectCode);
			sb.Append(_SP);
			sb.Append(projectName);
			sb.Append("</H4>");
			//minutes
			if(mi.Minutes.Count>0 && mi.Minutes[0].Length>0)
			{
				sb.Append("<H5>");
				sb.Append(Resource.ResourceManager["reports_Minutes_Title"]);
				sb.Append("</H5>");
				sb.Append("<UL>");
				/*
				foreach(string s in mi.Minutes)
				{
					sb.Append("<LI>");
					sb.Append(s);
					sb.Append("</LI>");
				}*/

				foreach(string s in mi.Minutes)
				{
					//s = s.Replace("\r\n","
					string[] rows = s.Split('\r');
					sb.Append("<LI>");

					if (rows.Length==1) sb.Append(s);
					else
					{
						sb.Append("<UL>");
						for (int i=0; i<rows.Length; i++)
						{
							sb.Append("<LI>");
							sb.Append(rows[i]);
							sb.Append("</LI>");
						}
						sb.Append("</UL>");
					}
					
					sb.Append("</LI>");
				}

				sb.Append("</UL>");
			}
			//admin minutes
			if(mi.AdminMinutes.Count>0 && mi.AdminMinutes[0].Length>0)
			{
				sb.Append("<H5>");
				sb.Append(Resource.ResourceManager["mail_Admin"]);
				sb.Append("</H5>");
				sb.Append("<UL>");
				/*
				foreach(string s in mi.AdminMinutes)
				{
					sb.Append("<LI>");
					sb.Append(s);
					sb.Append("</LI>");
				}*/

				foreach(string s in mi.AdminMinutes)
				{
					//s = s.Replace("\r\n","
					string[] rows = s.Split('\r');
					sb.Append("<LI>");

					if (rows.Length==1) sb.Append(s);
					else
					{
						sb.Append("<UL>");
						for (int i=0; i<rows.Length; i++)
						{
							sb.Append("<LI>");
							sb.Append(rows[i]);
							sb.Append("</LI>");
						}
						sb.Append("</UL>");
					}
					
					sb.Append("</LI>");
				}

				sb.Append("</UL>");
			}
			return sb.ToString();
		}

		public static string GetMailText2(MailInfo mi, out string subject)
		{
			decimal area = 0;
			subject="";

			string projectName, projectCode, administrativeName,add;
			int clientID=0;
			bool phases;
			if (!ProjectsData.SelectProject(mi.ProjectID, out projectName, out  projectCode, out administrativeName, out area, out add, 
				out clientID,out phases))
			{
				log.Error("Project not found" + mi.ProjectID);
				return null;
			}

			string activity = String.Empty;
			try
			{
				activity = DBManager.SelectActivityName(mi.ActivityID);
			}
			catch (Exception ex)
			{
				log.Error(ex);
			}

			UserInfo ui = UsersData.SelectUserByID(mi.UserID);
			if(ui==null)
			{
				log.Error("User not found" + mi.UserID);
				return null;
			}
	
			subject = string.Concat(projectCode,_SP,projectName);

			return CreateLayoutHtml(projectName, projectCode, projectName, ui.FullName, mi.Minutes[0], mi.AdminMinutes[0]);	
		}


		#endregion
		
		#region Get mail text

		public static string GetMailText(MailInfo mi, string reqUserName, out string subject)
		{
			decimal area = 0;
			subject="";

			string projectName = String.Empty, 
				projectCode = String.Empty, administrativeName = String.Empty, 
				userName = String.Empty, activityName = String.Empty, clientName = String.Empty, 
				minutes = String.Empty, adminMinutes = String.Empty;
	
			System.Data.SqlClient.SqlDataReader reader = null;
			try
			{
				reader = ReportsData.SelectMailReportHours(mi.WorkTimeID);
				if (reader.Read())
				{
					projectName = reader.GetString(4);
					projectCode = reader.IsDBNull(5) ? String.Empty : reader.GetString(5);
					area = reader.IsDBNull(6) ? 0 : reader.GetInt32(6);
					administrativeName = reader.IsDBNull(7) ? String.Empty : reader.GetString(7);
					userName = reader.GetString(2);
					activityName = reader.IsDBNull(10) ? String.Empty : reader.GetString(10);
					clientName = reader.IsDBNull(11) ? String.Empty : reader.GetString(11);
					minutes = reader.IsDBNull(8) ? String.Empty : reader.GetString(8);
					adminMinutes = reader.IsDBNull(9) ? String.Empty : reader.GetString(9);

				}
			}
			catch (Exception ex)
			{
				log.Error(ex);
				return null;
			}
			finally
			{
				if (reader!=null) reader.Close();
			}

			subject = string.Concat(projectCode,_SP,projectName);
			
			if (administrativeName == String.Empty) administrativeName = projectName;
			
			return String.Concat(CreateInfoHeaderHtml(administrativeName, projectCode, clientName, activityName, DateTime.Today),
									CreateLayoutHtml(projectName, projectCode, administrativeName, userName, minutes, adminMinutes),
									CreateFooterHtml(reqUserName));
		}

		public static void GetMailText(int projectID, int userID, DateTime startDate, DateTime endDate, string reqUserName, 
			out string headerHtml, out string html, out String footerHtml, out string subject)
		{

			DataSet ds = ReportsData.SelectMailReport(projectID, userID, startDate, endDate);
			
			DataTable dtProjectInfo = ds.Tables[0];
			string projectCode = dtProjectInfo.Rows[0]["ProjectCode"].ToString();
			string projectName = dtProjectInfo.Rows[0]["ProjectName"].ToString();
			string administrativeName = dtProjectInfo.Rows[0]["AdministrativeName"].ToString();
			string clientName = dtProjectInfo.Rows[0]["ClientName"].ToString();

			subject = string.Concat(projectCode,_SP,projectName);
			
			DataTable dt = ds.Tables[1];
			
			string userName = String.Empty;
			if (userID>0 && ds.Tables[1].Rows.Count>0)
			{
				userName = ds.Tables[1].Rows[0]["FullName"].ToString();
			}

			headerHtml = CreateInfoHeaderHtml(administrativeName, projectCode, clientName,"", DateTime.Today); 
			html = CreateLayoutHtml(ds, userName, startDate, endDate);
			footerHtml = CreateFooterHtml(reqUserName);
		}

		public static string GetMailText(int projectID, int userID, DateTime startDate, DateTime endDate, string reqUserName, out string subject)
		{
			string s = String.Empty;

			DataSet ds = ReportsData.SelectMailReport(projectID, userID, startDate, endDate);
			
			DataTable dtProjectInfo = ds.Tables[0];
			string projectCode = dtProjectInfo.Rows[0]["ProjectCode"].ToString();
			string projectName = dtProjectInfo.Rows[0]["ProjectName"].ToString();
			string administrativeName = dtProjectInfo.Rows[0]["AdministrativeName"].ToString();
			string clientName = dtProjectInfo.Rows[0]["ClientName"].ToString();

			if (administrativeName == String.Empty) administrativeName = projectName;

			subject = string.Concat(projectCode,_SP,projectName);
			
			DataTable dt = ds.Tables[1];
			
			string userName = String.Empty;
			if (userID>0 && ds.Tables[1].Rows.Count>0)
			{
				userName = ds.Tables[1].Rows[0]["FullName"].ToString();
			}

			s = String.Concat(CreateInfoHeaderHtml(administrativeName, projectCode, clientName,Resource.ResourceManager["reports_All"], DateTime.Today), 
						CreateLayoutHtml(ds, userName, startDate, endDate), CreateFooterHtml(reqUserName));
			
			return s;
		}

		#endregion

		#region Send mail

		public static void SendMail(string sTo, string sSubj, string sBody)
		{
			try
			{
				MailMessage Message = new MailMessage();
				Message.To = sTo;
				Message.From = ConfigurationSettings.AppSettings["MailFrom"];;
				Message.BodyEncoding= Encoding.UTF8;
			
				
				Message.Subject = sSubj;
				Message.Body = sBody;
				Message.BodyFormat = System.Web.Mail.MailFormat.Html;

				string sAuthMail = ConfigurationSettings.AppSettings["AuthMail"];
				if(sAuthMail!="")
				{
					Message.Fields.Add("http://schemas.microsoft.com/cdo/configuration/sendusing", "2");	//basic authentication
					Message.Fields.Add("http://schemas.microsoft.com/cdo/configuration/smtpauthenticate", "1");	//basic authentication
					Message.Fields.Add("http://schemas.microsoft.com/cdo/configuration/sendusername", sAuthMail); //set your username here
					Message.Fields.Add("http://schemas.microsoft.com/cdo/configuration/sendpassword", ConfigurationSettings.AppSettings["AuthPass"]);	//set your password here
				}

				SmtpMail.SmtpServer = ConfigurationSettings.AppSettings["SMTP"];
				SmtpMail.Send(Message);
				
			}
			catch(Exception ex)
			{
				log.Error(ex);
			}
		}
		public static void SendMailFromMeetings(string sTo, string sSubj, string sBody)
		{
			try
			{
				MailMessage Message = new MailMessage();
				Message.To = sTo;
				Message.From = ConfigurationSettings.AppSettings["MailFromMeetings"];;
				Message.BodyEncoding= Encoding.UTF8;
			
				
				Message.Subject = sSubj;
				Message.Body = sBody;
				Message.BodyFormat = System.Web.Mail.MailFormat.Html;

				string sAuthMail = ConfigurationSettings.AppSettings["AuthMailMeetings"];
				if(sAuthMail!="")
				{
					Message.Fields.Add("http://schemas.microsoft.com/cdo/configuration/sendusing", "2");	//basic authentication
					Message.Fields.Add("http://schemas.microsoft.com/cdo/configuration/smtpauthenticate", "1");	//basic authentication
					Message.Fields.Add("http://schemas.microsoft.com/cdo/configuration/sendusername", sAuthMail); //set your username here
					Message.Fields.Add("http://schemas.microsoft.com/cdo/configuration/sendpassword", ConfigurationSettings.AppSettings["AuthPassMeetings"]);	//set your password here
				}

				SmtpMail.SmtpServer = ConfigurationSettings.AppSettings["SMTP"];
				SmtpMail.Send(Message);
				
			}
			catch(Exception ex)
			{
				log.Error(ex);
			}
		}
		public static void SendMailFromProtocol(string sTo, string sSubj, string sBody)
		{
			try
			{
				MailMessage Message = new MailMessage();
				Message.To = sTo;
				Message.From = ConfigurationSettings.AppSettings["MailFromMeetings"];;
				Message.BodyEncoding= Encoding.UTF8;
			
				
				Message.Subject = sSubj;
				Message.Body = sBody;
				Message.BodyFormat = System.Web.Mail.MailFormat.Html;

				string sAuthMail = ConfigurationSettings.AppSettings["AuthMailMeetings"];
				if(sAuthMail!="")
				{
					Message.Fields.Add("http://schemas.microsoft.com/cdo/configuration/sendusing", "2");	//basic authentication
					Message.Fields.Add("http://schemas.microsoft.com/cdo/configuration/smtpauthenticate", "1");	//basic authentication
					Message.Fields.Add("http://schemas.microsoft.com/cdo/configuration/sendusername", sAuthMail); //set your username here
					Message.Fields.Add("http://schemas.microsoft.com/cdo/configuration/sendpassword", ConfigurationSettings.AppSettings["AuthPassMeetings"]);	//set your password here
				}

				SmtpMail.SmtpServer = ConfigurationSettings.AppSettings["SMTP"];
				SmtpMail.Send(Message);
				
			}
			catch(Exception ex)
			{
				log.Error(ex);
			}
		}

		#endregion
		
		public static void SendMailOfficer (string project, string date)
		{
			SendMailFromProtocol(ConfigurationSettings.AppSettings["MeetingEmail"],string.Format(Resource.ResourceManager["Protocol_subject"],date,project)
				, string.Format( Resource.ResourceManager["Protocol_body1"],project,date));

		}
		public static void SendMailOfficer (string project, string date, string start, string end)
		{
			SendMailFromMeetings(ConfigurationSettings.AppSettings["MeetingEmail"],string.Format(Resource.ResourceManager["meeting_subject"],date,start,project)
				, string.Format( Resource.ResourceManager["meeting_body1"],project,date,start,end));

		}
		public static void SendMailFirst(string users, string project, string date, string start, string end,string subs, string clients,string place, string other, int nClient,string notes,string date1)
		{
			
			StringCollection scMail,scNames;
			string sPeople = GetAllPeople(users,clients,subs,other,out scMail,out scNames);

			string sClient = "";
			if(nClient>0)
			{
				ClientData cd= ClientDAL.Load(nClient);
				if(cd!=null)
					sClient= cd.ClientName;
			}
			StringBuilder sMails=new StringBuilder();
			foreach(string s in scMail)
			{
				sMails.Append(s);
				sMails.Append(";");
			}
			try
			{
				MailMessage Message = new MailMessage();
				Message.To = sMails.ToString();
				Message.From = ConfigurationSettings.AppSettings["MailFromMeetings"];;
				Message.BodyEncoding= Encoding.UTF8;
		
			
				Message.Subject = string.Format(Resource.ResourceManager["meeting_subject"],date1,start,project);
				Message.Body = string.Format( Resource.ResourceManager["meeting_body"],project,sClient,"",date,start,place, sPeople,notes);
				Message.BodyFormat = System.Web.Mail.MailFormat.Html;
				
					
				string sAuthMail = ConfigurationSettings.AppSettings["AuthMailMeetings"];
				if(sAuthMail!="")
				{
					Message.Fields.Add("http://schemas.microsoft.com/cdo/configuration/sendusing", "2");	//basic authentication
					Message.Fields.Add("http://schemas.microsoft.com/cdo/configuration/smtpauthenticate", "1");	//basic authentication
					Message.Fields.Add("http://schemas.microsoft.com/cdo/configuration/sendusername", sAuthMail); //set your username here
					Message.Fields.Add("http://schemas.microsoft.com/cdo/configuration/sendpassword", ConfigurationSettings.AppSettings["AuthPassMeetings"]);	//set your password here
				}

				SmtpMail.SmtpServer = ConfigurationSettings.AppSettings["SMTP"];
				SmtpMail.Send(Message);
			
			}
			catch(Exception ex)
			{
				log.Error(ex);
			}
				
			
		}
		private static string GetName(string sFull)
		{
			int n= sFull.LastIndexOf(" ");
			if(n!=-1)
				return sFull.Substring(n+1);
			return sFull;
		}
		public static string GetAllPeople(string users, string clients, string subs,string other, out StringCollection scMails,out StringCollection scNames)
		{
			StringBuilder sb = new StringBuilder();
			int n=1;

			scMails = new StringCollection();
			scNames= new StringCollection();
			string [] sclients=clients.Split(';');
			foreach(string s in sclients)
			{
				if(s!=null && s.Length>0)
				{
					int i = int.Parse(s);
					ClientData cd = ClientDAL.Load(i);
					sb.Append(n);
					sb.Append(". ");
					sb.Append(cd.ClientNameAndAll);
					sb.Append("<BR>");
					if(cd.Email!="")
					{
						scMails.Add(cd.Email);
						scNames.Add(GetName(cd.Manager));
					}
					n++;
				}
			}
			string [] susers=users.Split(';');
			foreach(string s in susers)
			{
				if(s!=null && s.Length>0)
				{
					int i = int.Parse(s);
					UserInfo ui= UsersData.SelectUserByID(i);
					sb.Append(n);
					sb.Append(". ");
					sb.Append(ui.FullName);
					sb.Append("<BR>");
					//if(cd.Email!="")
					{
						scMails.Add(ui.Mail);
						scNames.Add(ui.LastName);
					}
					n++;
				}
			}
			string [] ssubs=subs.Split(';');
			foreach(string s in ssubs)
			{
				if(s!=null && s.Length>0)
				{
					int i = int.Parse(s);
					SubcontracterData cd = SubcontracterDAL.Load(i);
					sb.Append(n);
					sb.Append(". ");
					sb.Append(cd.SubcontracterNameAndAll);
					sb.Append("<BR>");
					if(cd.Email!="")
					{
						scMails.Add(cd.Email);
						scNames.Add(GetName(cd.Manager));
					}
					n++;
				}
			}
			if(other!="")
			{
				sb.Append(n);
				sb.Append(". ");
				sb.Append(other);
				sb.Append("<BR>");
			}
			return sb.ToString();
		}
		public static void SendMailMinutes(string users, string path,string project, string date, string start, string end,string subs, string clients,string place, string other, int nClient,string notes,string date1)
		{
			
			

			StringCollection scMail,scNames;
			string sPeople = GetAllPeople(users,clients,subs,other,out scMail,out scNames);

			string sClient = "";
			if(nClient>0)
			{
				ClientData cd= ClientDAL.Load(nClient);
				if(cd!=null)
					sClient= cd.ClientName;
			}
			StringBuilder sMails=new StringBuilder();
			foreach(string s in scMail)
			{
				sMails.Append(s);
				sMails.Append(";");
			}
			try
			{
				MailMessage Message = new MailMessage();
				Message.To = sMails.ToString();
				Message.From = ConfigurationSettings.AppSettings["MailFromMeetings"];
				Message.BodyEncoding= Encoding.UTF8;
		
			
				Message.Subject = string.Format(Resource.ResourceManager["meeting_subject2"],date1,start,project);
				Message.Body = string.Format( Resource.ResourceManager["meeting_body2"],project,sClient,"",date,start,place, sPeople,notes);
				Message.BodyFormat = System.Web.Mail.MailFormat.Html;
				Message.Attachments.Add(new MailAttachment(path));
					
				string sAuthMail = ConfigurationSettings.AppSettings["AuthMailMeetings"];
				if(sAuthMail!="")
				{
					Message.Fields.Add("http://schemas.microsoft.com/cdo/configuration/sendusing", "2");	//basic authentication
					Message.Fields.Add("http://schemas.microsoft.com/cdo/configuration/smtpauthenticate", "1");	//basic authentication
					Message.Fields.Add("http://schemas.microsoft.com/cdo/configuration/sendusername", sAuthMail); //set your username here
					Message.Fields.Add("http://schemas.microsoft.com/cdo/configuration/sendpassword", ConfigurationSettings.AppSettings["AuthPassMeetings"]);	//set your password here
				}

				SmtpMail.SmtpServer = ConfigurationSettings.AppSettings["SMTP"];
				SmtpMail.Send(Message);
			
			}
			catch(Exception ex)
			{
				log.Error(ex);
			}
				
			
		}
		public static void SendMailMinutes(string Mails, string path,string project, string date)
		{
			
			try
			{
				MailMessage Message = new MailMessage();
				Message.To = Mails;
				Message.From = ConfigurationSettings.AppSettings["MailFromMeetings"];
				Message.BodyEncoding= Encoding.UTF8;
		
			
				Message.Subject = string.Format(Resource.ResourceManager["reports_Minutes_Title"]);
				Message.Body = string.Format( Resource.ResourceManager["Protocol_body1"],project,date);
				Message.BodyFormat = System.Web.Mail.MailFormat.Html;
				Message.Attachments.Add(new MailAttachment(path));
					
				string sAuthMail = ConfigurationSettings.AppSettings["AuthMailMeetings"];
				if(sAuthMail!="")
				{
					Message.Fields.Add("http://schemas.microsoft.com/cdo/configuration/sendusing", "2");	//basic authentication
					Message.Fields.Add("http://schemas.microsoft.com/cdo/configuration/smtpauthenticate", "1");	//basic authentication
					Message.Fields.Add("http://schemas.microsoft.com/cdo/configuration/sendusername", sAuthMail); //set your username here
					Message.Fields.Add("http://schemas.microsoft.com/cdo/configuration/sendpassword", ConfigurationSettings.AppSettings["AuthPassMeetings"]);	//set your password here
				}

				SmtpMail.SmtpServer = ConfigurationSettings.AppSettings["SMTP"];
				SmtpMail.Send(Message);
			
			}
			catch(Exception ex)
			{
				log.Error(ex);
			}
				
			
		}
		public static void SendMailApproved(string users, string path,string project, string date, string start, string end,string subs, string clients,string place, string other, int nClient,string notes,string date1)
		{
			
			StringCollection scMail,scNames;
			string sPeople = GetAllPeople(users,clients,subs,other,out scMail,out scNames);

			string sClient = "";
			if(nClient>0)
			{
				ClientData cd= ClientDAL.Load(nClient);
				if(cd!=null)
					sClient= cd.ClientName;
			}
			StringBuilder sMails=new StringBuilder();
			foreach(string s in scMail)
			{
				sMails.Append(s);
				sMails.Append(";");
			}
			try
			{
				MailMessage Message = new MailMessage();
				Message.To = sMails.ToString();
					Message.From = ConfigurationSettings.AppSettings["MailFromMeetings"];;
					Message.BodyEncoding= Encoding.UTF8;
		
			
					Message.Subject = string.Format(Resource.ResourceManager["meeting_subject3"],date1,start,project);
					Message.Body = string.Format( Resource.ResourceManager["meeting_body3"],project,sClient,"",date,start,place, sPeople,notes);
					Message.BodyFormat = System.Web.Mail.MailFormat.Html;
					Message.Attachments.Add(new MailAttachment(path));
					
					string sAuthMail = ConfigurationSettings.AppSettings["AuthMailMeetings"];
					if(sAuthMail!="")
					{
						Message.Fields.Add("http://schemas.microsoft.com/cdo/configuration/sendusing", "2");	//basic authentication
						Message.Fields.Add("http://schemas.microsoft.com/cdo/configuration/smtpauthenticate", "1");	//basic authentication
						Message.Fields.Add("http://schemas.microsoft.com/cdo/configuration/sendusername", sAuthMail); //set your username here
						Message.Fields.Add("http://schemas.microsoft.com/cdo/configuration/sendpassword", ConfigurationSettings.AppSettings["AuthPassMeetings"]);	//set your password here
					}

					SmtpMail.SmtpServer = ConfigurationSettings.AppSettings["SMTP"];
					SmtpMail.Send(Message);
			
				}
				catch(Exception ex)
				{
					log.Error(ex);
				}
				
			
		}

		public static void SendProjectMail(string project, string type, string sum,string sdate,string projectNameShort,string projectCode)
		{
			project = projectCode+" "+ projectNameShort+", "+ project;
			StringCollection mails = null, names = null;
			MailDAL.LoadLeadersAndOfficersMails(out names, out mails);
			System.Text.StringBuilder sbMailBody = new System.Text.StringBuilder();

			string subject=Resource.ResourceManager["project_mail_subject"]+ ": "+projectCode+" "+ projectNameShort;
			string body=Resource.ResourceManager["project_mail_body"];
			sbMailBody.Append(string.Format(body,type,project,sum,sdate));
			sbMailBody.Append(String.Concat("<br><br><b>", DateTime.Today.ToString("d"), "</b>"));

			// send mails
			
			for (int i=0; i<mails.Count; i++)
			{
				SendMail(mails[i], subject, sbMailBody.ToString());
			}
		}
		public static void SendNewProjectMail(string project)
		{
			StringCollection mails = null, names = null;
			MailDAL.LoadSecrAndOfficersMails(out names, out mails);
			System.Text.StringBuilder sbMailBody = new System.Text.StringBuilder();

			string subject=Resource.ResourceManager["newproject_mail_subject"];
			string body=Resource.ResourceManager["newproject_mail_body"];
			
			sbMailBody.Append(string.Format(body,project));
			sbMailBody.Append(String.Concat("<br><br><b>", DateTime.Today.ToString("d"), "</b>"));

			// send mails
			
			for (int i=0; i<mails.Count; i++)
			{
				SendMail(mails[i], subject, sbMailBody.ToString());
			}
		}
		public static void SendProjectMail1(string project, string sdate,string projectNameShort,string projectCode)
		{
			project = string.Concat(projectCode," ", projectNameShort,", ", project);
			StringCollection mails = null, names = null;
			MailDAL.LoadLeadersAndOfficersMails(out names, out mails);
			System.Text.StringBuilder sbMailBody = new System.Text.StringBuilder();

			string subject=Resource.ResourceManager["project_mail_subject1"]+": "+projectCode+" "+ projectNameShort;
			string body=Resource.ResourceManager["project_mail_body1"];
			sbMailBody.Append(string.Format(body,sdate,project));
			sbMailBody.Append(String.Concat("<br><br><b>", DateTime.Today.ToString("d"), "</b>"));

			// send mails
			
			for (int i=0; i<mails.Count; i++)
			{
				SendMail(mails[i], subject, sbMailBody.ToString());
			}
		}
		#region CreateHtml

		#region Recipients header

		public static string CreateRecipientsHeaderHtml(string mailTo, string mailCopyTo)
		{
			int fontSize = 8;

			System.Web.UI.WebControls.Table tblLayout = new System.Web.UI.WebControls.Table();
			tblLayout.BorderWidth=0;

			TableRow tr = new TableRow();
			TableCell tc = new TableCell();

			tr = new TableRow(); tr.Height = Unit.Pixel(10); tc = new TableCell(); tr.Cells.Add(tc); tblLayout.Rows.Add(tr);

			tr = new TableRow(); tc = new TableCell(); tc.Wrap = false; tc.Width = 65;
			tc.Font.Size = FontUnit.Point(fontSize); tc.VerticalAlign = VerticalAlign.Top;	
			tc.Text = String.Concat("<B>", Resource.ResourceManager["reports_MailReport_MailTo"],": ", "</B>");
			tr.Cells.Add(tc);

			tc = new TableCell();
			tc.Font.Size = FontUnit.Point(fontSize); tc.VerticalAlign = VerticalAlign.Top;	
			tc.Text = mailTo;
			tr.Cells.Add(tc);
			tblLayout.Rows.Add(tr);

			tr = new TableRow();
			tc = new TableCell();
			tc.Font.Size = FontUnit.Point(fontSize);tc.VerticalAlign = VerticalAlign.Top;	
			tc.Text = String.Concat("<B>", Resource.ResourceManager["reports_MailReport_MailCopyTo"],": ", "</B>");
			tr.Cells.Add(tc);
			tc = new TableCell();
			tc.Font.Size = FontUnit.Point(fontSize);tc.VerticalAlign = VerticalAlign.Top;	
			tc.Text = mailCopyTo;
			tr.Cells.Add(tc);
			tblLayout.Rows.Add(tr);

			System.Text.StringBuilder sb = new System.Text.StringBuilder();
			System.Web.UI.HtmlTextWriter tw = new System.Web.UI.HtmlTextWriter(new System.IO.StringWriter(sb));

			tblLayout.RenderControl(tw);
			return sb.ToString();
		}

		#endregion

		#region header html

		public static string CreateInfoHeaderHtml(string administrativeName, string projectCode,
			string clientName, string activity, DateTime date)
		{
			int fontSize = 8;

			System.Web.UI.WebControls.Table tblLayout = new System.Web.UI.WebControls.Table();
			tblLayout.BorderWidth=0;
			//tblLayout.CellPadding = 0;
			//tblLayout.CellSpacing = 0;

			TableRow tr = new TableRow();
			TableCell tc = new TableCell();

			tr = new TableRow(); tr.Height = Unit.Pixel(10); tc = new TableCell(); tr.Cells.Add(tc);
			tblLayout.Rows.Add(tr);

			tr = new TableRow();
			tc = new TableCell(); tc.VerticalAlign = VerticalAlign.Top;
			tc.Font.Size = FontUnit.Point(fontSize);
			tc.Text = String.Concat("<B>", Resource.ResourceManager["reports_MailReport_Obekt"],": ", "</B>");
			tr.Cells.Add(tc);
			tc = new TableCell();
			tc.Font.Size = FontUnit.Point(fontSize);tc.VerticalAlign = VerticalAlign.Top;
			tc.Text = administrativeName;
			tr.Cells.Add(tc);
			tblLayout.Rows.Add(tr);

			tr = new TableRow();
			tc = new TableCell();
			tc.Font.Size = FontUnit.Point(fontSize);
			tc.Text = String.Concat("<B>", Resource.ResourceManager["reports_MailReport_Code"],": ", "</B>");tc.VerticalAlign = VerticalAlign.Top;
			tr.Cells.Add(tc);
			tc = new TableCell();
			tc.Font.Size = FontUnit.Point(fontSize);tc.VerticalAlign = VerticalAlign.Top;
			tc.Text = projectCode;
			tr.Cells.Add(tc);
			tblLayout.Rows.Add(tr);
			
			
			tr = new TableRow();
			tc = new TableCell();
			tc.Font.Size = FontUnit.Point(fontSize);
			tc.Text = String.Concat("<B>", Resource.ResourceManager["reports_MailReport_Client"],": ", "</B>"); tc.VerticalAlign = VerticalAlign.Top;
			tr.Cells.Add(tc);
			tc = new TableCell();
			tc.Font.Size = FontUnit.Point(fontSize);tc.VerticalAlign = VerticalAlign.Top;
			tc.Text = clientName;
			tr.Cells.Add(tc);
			tblLayout.Rows.Add(tr);

			
			tr = new TableRow(); tr.Height = Unit.Pixel(10); tc = new TableCell(); tr.Cells.Add(tc);
			tblLayout.Rows.Add(tr);

			tr = new TableRow();
			tc = new TableCell();tc.Font.Size = FontUnit.Point(fontSize);tc.VerticalAlign = VerticalAlign.Top;
			tc.Text = String.Concat("<B>", Resource.ResourceManager["reports_MailReport_Activity"],": ", "</B>");
			tr.Cells.Add(tc);
			tc = new TableCell();
			tc.Font.Size = FontUnit.Point(fontSize);tc.VerticalAlign = VerticalAlign.Top;
			tc.Text = activity;
			tr.Cells.Add(tc);
			tblLayout.Rows.Add(tr);
			
			tr = new TableRow();
			tc = new TableCell();
			tc.Font.Size = FontUnit.Point(fontSize);tc.VerticalAlign = VerticalAlign.Top;
			tc.Text = String.Concat("<B>", Resource.ResourceManager["reports_MailReport_Date"],": ", "</B>"); 
			tr.Cells.Add(tc);
			tc = new TableCell();
			tc.Font.Size = FontUnit.Point(fontSize);tc.VerticalAlign = VerticalAlign.Top;
			tc.Text = date.ToShortDateString();
			tr.Cells.Add(tc);
			tblLayout.Rows.Add(tr);

			
			tr = new TableRow(); tr.Height = Unit.Pixel(20); tc = new TableCell(); tr.Cells.Add(tc);
			tblLayout.Rows.Add(tr);

			System.Text.StringBuilder sb = new System.Text.StringBuilder();
			System.Web.UI.HtmlTextWriter tw = new System.Web.UI.HtmlTextWriter(new System.IO.StringWriter(sb));

			tblLayout.RenderControl(tw);
			return sb.ToString();
		}


		public static string CreateFooterHtml(string userName)
		{
			string html = String.Concat("<br><br><br><B>", Resource.ResourceManager["reports_MailReport_CreatedBy"], ": </B>", userName, "<br>");
			return html;
		}

		#endregion

		public static string CreateLayoutHtml(DataSet ds, string userName, DateTime startDate, DateTime endDate)
		{
			System.Web.UI.WebControls.Table tblLayout = new System.Web.UI.WebControls.Table();
			
			TableRow tr = new TableRow();
			TableCell tc = new TableCell();

			#region Add header

			//Header
			DataTable dtProjectInfo = ds.Tables[0];
			string projectCode = dtProjectInfo.Rows[0]["ProjectCode"].ToString();
			string projectName = 
				(dtProjectInfo.Rows[0]["AdministrativeName"].ToString() == String.Empty) ? dtProjectInfo.Rows[0]["ProjectName"].ToString() : dtProjectInfo.Rows[0]["AdministrativeName"].ToString();

			System.Text.StringBuilder userHeaderHtml = new System.Text.StringBuilder();
			
			userHeaderHtml.Append("<H5>");

			if (userName!=String.Empty) userHeaderHtml.Append(
											String.Concat(Resource.ResourceManager["reports_MailReport_ForUser"]," ", userName));
			else if( ds.Tables[1].Rows.Count>0) userHeaderHtml.Append(Resource.ResourceManager["reports_MailReport_ForAllUsers"]);

			if(startDate!=Constants.DateMax && endDate!=Constants.DateMax)
				if (startDate == endDate) userHeaderHtml.Append(String.Concat(" ", Resource.ResourceManager["reports_For"], " ", startDate.ToString("d")));
				else userHeaderHtml.Append(String.Concat(Resource.ResourceManager["reports_ForPeriodFrom"],
						 startDate.ToString("d"), " ", Resource.ResourceManager["reports_PeriodTo"], " ", endDate.ToString("d")));
			
			userHeaderHtml.Append("<H5>");

			tr = new System.Web.UI.WebControls.TableRow();
			tblLayout.Rows.Add(tr);
			tc = new System.Web.UI.WebControls.TableCell();
			tc.ColumnSpan = 2; tc.HorizontalAlign = HorizontalAlign.Center;
			tc.Text = String.Concat("<H4>", Resource.ResourceManager["mail_Project"], 
				projectCode, " ", projectName, "</H4>", userHeaderHtml.ToString());
			tr.Cells.Add(tc);

			#endregion
			
			DataTable dt = ds.Tables[1];
			DateTime tempDate = new DateTime(1900, 1,1);

			for (int i = 0; i<dt.Rows.Count; i++)
			{
				DataRow dr = dt.Rows[i];

				#region Add Date
				
				DateTime currentDate = (DateTime)dr["WorkDate"];
				if (currentDate!=tempDate)
				{
					tr = new TableRow();
					tc = new TableCell(); tc.ColumnSpan = 2; tc.Font.Bold = true;
					tc.Text = ((DateTime)dr["WorkDate"]).ToString("d");
					tr.Cells.Add(tc);
					tblLayout.Rows.Add(tr);
					tempDate = currentDate;
					
					tr = new TableRow(); tr.Height = Unit.Pixel(5);
					tc = new TableCell(); tc.ColumnSpan = 2;
					tr.Cells.Add(tc);
					tblLayout.Rows.Add(tr);
				
				}

				#endregion

				#region Add minutes

				//minutes row
				string minutes = dr["Minutes"].ToString();
				string adminMunutes = dr["AdminMinutes"].ToString();

//				if (minutes ==String.Empty) minutes = Resource.ResourceManager["reports_MailReport_NoMinutes"];
//				if (adminMunutes == String.Empty) adminMunutes = Resource.ResourceManager["reports_MailReport_NoMinutes"];

				tr = new TableRow();
				TableCell td = new TableCell(); td.Width = Unit.Percentage(50); td.VerticalAlign = VerticalAlign.Top;
				if (minutes !=String.Empty)
					td.Text = GetMinuteHtml(minutes);
				tr.Cells.Add(td);

				td = new TableCell();
				td.ForeColor = System.Drawing.Color.Red; td.VerticalAlign = VerticalAlign.Top;
				if (adminMunutes !=String.Empty)
					td.Text = GetMinuteHtml(adminMunutes);
				tr.Cells.Add(td);
				tblLayout.Rows.Add(tr);
			
				// space
				tr = new TableRow(); tr.Height = Unit.Pixel(5);
				tc = new TableCell(); tc.ColumnSpan = 2;
				tr.Cells.Add(tc);
				tblLayout.Rows.Add(tr);
				#endregion

			}

			System.Text.StringBuilder sb = new System.Text.StringBuilder();
			System.Web.UI.HtmlTextWriter tw = new System.Web.UI.HtmlTextWriter(new System.IO.StringWriter(sb));

			tblLayout.RenderControl(tw);
			return sb.ToString();
		}

		public static string CreateLayoutHtml(string projectName, string projectCode , string administrativeName, string userName, string minutes, string adminMinutes)
		{
			System.Web.UI.WebControls.Table tblLayout = new System.Web.UI.WebControls.Table();
			
			TableRow tr = new System.Web.UI.WebControls.TableRow();
			tblLayout.Rows.Add(tr);
			System.Web.UI.WebControls.TableCell tc = new System.Web.UI.WebControls.TableCell();
			tc.ColumnSpan = 2; tc.HorizontalAlign = HorizontalAlign.Center;
			tc.Text = String.Concat("<H4>", Resource.ResourceManager["mail_Project"], 
				projectCode, " ", administrativeName, "</H4>");
			tr.Cells.Add(tc);

//			if (minutes == String.Empty) minutes = Resource.ResourceManager["reports_MailReport_NoMinutes"];
//			if (adminMinutes == String.Empty) adminMinutes = Resource.ResourceManager["reports_MailReport_NoMinutes"];

			tr = new TableRow();
			TableCell td = new TableCell(); td.Width = Unit.Percentage(50); td.VerticalAlign = VerticalAlign.Top;
			if (minutes != String.Empty)
				td.Text = GetMinuteHtml(minutes);
			tr.Cells.Add(td);

			td = new TableCell();
			td.ForeColor = System.Drawing.Color.Red; td.VerticalAlign = VerticalAlign.Top;
			if (adminMinutes != String.Empty)	
				td.Text = GetMinuteHtml(adminMinutes);
			tr.Cells.Add(td);
			tblLayout.Rows.Add(tr);
			
			System.Text.StringBuilder sb = new System.Text.StringBuilder();
			System.Web.UI.HtmlTextWriter tw = new System.Web.UI.HtmlTextWriter(new System.IO.StringWriter(sb));

			tblLayout.RenderControl(tw);
			return sb.ToString();
		}

		private static string GetMinuteHtml(string minute)
		{
			System.Text.StringBuilder sb = new System.Text.StringBuilder();

			minute = minute.Replace("\r\n", "\r");

			string[] rows = minute.Split('\r');
			
			for (int i=0; i<rows.Length; i++)
			{
				sb.Append("<LI>");
				sb.Append(rows[i]);
				sb.Append("</LI>");
			}
			return sb.ToString();

		}

		#endregion
	}
	
}
