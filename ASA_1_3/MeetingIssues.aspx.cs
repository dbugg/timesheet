using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Data.SqlClient;
using log4net;
using Asa.Timesheet.Data;
using Asa.Timesheet.Data.Entities;
using Asa.Timesheet.Data.Util;
using GrapeCity.ActiveReports.Export.Pdf.Section;
using GrapeCity.ActiveReports.Export.Excel.Section;
using Asa.Timesheet.WebPages.Reports;

using System.IO;
using System.Configuration;
using GrapeCity.ActiveReports.Export.Word.Section;

namespace Asa.Timesheet.WebPages
{
	/// <summary>
	/// Summary description for MeetingIssues.
	/// </summary>
	public class MeetingIssues : TimesheetPageBase
	{
		private static readonly ILog log = LogManager.GetLogger(typeof(MeetingIssues));
		protected System.Web.UI.WebControls.Label lbHotIssueCategory;
		protected System.Web.UI.WebControls.Label txtCategoryText;
		protected System.Web.UI.WebControls.Label lbCategoryText;
		protected System.Web.UI.WebControls.Label lbAssigned;
		protected System.Web.UI.WebControls.Label txtAssignedText;
		protected System.Web.UI.WebControls.Label lbAssignedText;
		protected System.Web.UI.WebControls.Label lbAssignedTypeText;
		protected System.Web.UI.WebControls.Label lbHotIssueIme;
		protected System.Web.UI.WebControls.Label txtHotIssueIme;
		protected System.Web.UI.WebControls.Label Label13;
		protected System.Web.UI.WebControls.Label Label14;
		protected System.Web.UI.WebControls.Label lbDiscription;
		protected System.Web.UI.WebControls.Label txtDiscription;
		protected System.Web.UI.WebControls.Label lbDeadline;
		protected System.Web.UI.WebControls.Label txtDeadlineText;
		protected System.Web.UI.WebControls.Label txtDeadline;
		protected System.Web.UI.WebControls.Label lbMainID;
		protected System.Web.UI.WebControls.Label lbStatus;
		protected System.Web.UI.WebControls.Label txtStatusText;
		protected System.Web.UI.WebControls.Label lbStatusText;
		protected System.Web.UI.WebControls.Label lbComment;
		protected System.Web.UI.WebControls.Label txtComment;
		protected System.Web.UI.WebControls.Label lbToClient;
		protected System.Web.UI.WebControls.Label Label12;
		protected System.Web.UI.WebControls.Label lbToClientText;
		protected System.Web.UI.WebControls.Label Label11;
		protected System.Web.UI.WebControls.CheckBox ckIncluded;
		protected System.Web.UI.WebControls.PlaceHolder menuHolder;
		protected System.Web.UI.WebControls.Label Label15;
		protected System.Web.UI.WebControls.DropDownList ddlProject1;
		protected System.Web.UI.WebControls.Label Label16;
		protected System.Web.UI.WebControls.CheckBoxList lst;
		protected System.Web.UI.WebControls.CheckBox ckAll;
		protected System.Web.UI.WebControls.CheckBox ckInclude;
		protected Asa.Timesheet.WebPages.UserControls.PageHeader header;
		protected System.Web.UI.WebControls.Button btnSave;
		protected System.Web.UI.WebControls.Button btnSave1;
		protected System.Web.UI.WebControls.Button btnNew;
		protected System.Web.UI.WebControls.Button btnNew1;
		protected System.Web.UI.WebControls.Button btnMinutes;
		protected System.Web.UI.WebControls.Button btnCancelUP;
		protected System.Web.UI.WebControls.Button btnPDF1;
		protected System.Web.UI.WebControls.Button btnMark;
		protected System.Web.UI.WebControls.Button btnMark1;
		protected System.Web.UI.WebControls.DataList dlHotIssues;


        private void Page_Load(object sender, System.EventArgs e)
        {
            if (!this.IsPostBack)
            {
                if (UIHelpers.GetIDParam() <= 0)
                {

                    header.PageTitle = Resource.ResourceManager["newMeeting_PageTitle"];

                }
                else
                {
                    header.PageTitle = Resource.ResourceManager["editMeeting_PageTitle"];

                }
                header.UserName = this.LoggedUser.UserName;

                if (UIHelpers.GetIDParam() > 0)
                {
                    LoadFromID(UIHelpers.GetIDParam());
                    //Load Categories
                    int nID = int.Parse(ddlProject1.SelectedValue);
                    LoadHotIssueCategories(nID);



                }



            }




            UIHelpers.CreateMenu(menuHolder, LoggedUser);
        }	
		
		#region Web Form Designer generated code
        override protected void OnInit(EventArgs e)
        {
            //
            // CODEGEN: This call is required by the ASP.NET Web Form Designer.
            //
            InitializeComponent();
            base.OnInit(e);
        }
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
        private void InitializeComponent()
        {
            this.ddlProject1.SelectedIndexChanged += new System.EventHandler(this.ddlProject1_SelectedIndexChanged);
            this.ckAll.CheckedChanged += new System.EventHandler(this.ckAll_CheckedChanged);
            this.btnCancelUP.Click += new System.EventHandler(this.btnCancelUP_Click);
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            this.btnNew.Click += new System.EventHandler(this.btnNew_Click);
            this.btnMinutes.Click += new System.EventHandler(this.btnMinutes_Click);
            this.dlHotIssues.ItemCommand += new System.Web.UI.WebControls.DataListCommandEventHandler(this.dlHotIssues_ItemCommand);
            this.btnSave1.Click += new System.EventHandler(this.btnSave_Click);
            this.btnNew1.Click += new System.EventHandler(this.btnNew_Click);
            this.btnPDF1.Click += new System.EventHandler(this.btnMinutes_Click);
            this.btnMark1.Click += new System.EventHandler(this.btnMark_Click);
            this.btnMark.Click += new System.EventHandler(this.btnMark_Click);
            this.Load += new System.EventHandler(this.Page_Load);

        }
		#endregion

        private void Reload()
        {
            LoadHotIssues(UIHelpers.GetIDParam());
        }
        private void LoadFromID(int ID)
        {
            ddlProject1.Items.Clear();
            MeetingData md = MeetingDAL.Load(ID);
            ddlProject1.Items.Add(new ListItem(md.ProjectName, md.ProjectID.ToString()));
            MeetingProjectsVector mpv = MeetingProjectDAL.LoadCollection("MeetingProjectsSelByMeetingProc", SQLParms.CreateMeetingProjectsSelByMeetingProc(ID));
            foreach (MeetingProjectData mpd in mpv)
            {
                string projectName, projectCode, administrativeName, add;
                decimal area = 0;
                int clientID = 0;
                bool phases; bool isPUP;
                ProjectsData.SelectProject(mpd.ProjectID, out projectName, out  projectCode, out administrativeName, out area, out add,
                    out clientID, out phases, out isPUP);
                if (ddlProject1.Items.FindByValue(mpd.ProjectID.ToString()) == null)
                    ddlProject1.Items.Add(new ListItem(projectName, mpd.ProjectID.ToString()));


            }
            LoadHotIssues(ID);
        }
        public void LoadHotIssueCategories(int ProjectID)
        {
            lst.DataSource = HotIssueCategoryDAL.LoadCollection(ProjectID); ;
            lst.DataTextField = "HotIssueCategoryName";
            lst.DataValueField = "HotIssueCategoryID";
            lst.DataBind();

            int nID = UIHelpers.GetIDParam();
            if (nID > 0)
            {
                int mainProjectId = UIHelpers.ToInt(ddlProject1.SelectedValue);
                MeetingCategoriesVector mcv = MeetingCategorieDAL.LoadCollection(nID, mainProjectId);
                foreach (ListItem li in lst.Items)
                {
                    int nValue = int.Parse(li.Value);
                    if (mcv.GetByCategoryID(nValue) != null)
                        li.Selected = true;
                }
            }

        }
        private void LoadHotIssues(int ID)
        {
            MeetingData md = MeetingDAL.Load(ID);
            int mainProjectId = UIHelpers.ToInt(ddlProject1.SelectedValue);
            //int nMeetingID=UIHelpers.GetIDParam();

            if (mainProjectId > 0)
            {


                //int nDays=int.Parse(System.Configuration.ConfigurationManager.AppSettings["EditDays"]);

                HotIssuesVector vecAll = HotIssueDAL.LoadCollectionForMeeting(mainProjectId, DateTime.Now.AddYears(1), ckInclude.Checked, UIHelpers.GetIDParam(), -1, -1, Constants.DateMax, true, false, false, false, true);


                dlHotIssues.DataSource = vecAll;
                dlHotIssues.DataKeyField = "HotIssueID";
                dlHotIssues.DataBind();

            }
            else
                dlHotIssues.Visible = false;
            MeetingHotIssuesVector mhv = MeetingHotIssueDAL.LoadCollection(ID);
            for (int index = 0; index < dlHotIssues.Items.Count; index++)
            {
                DataListItem dli = dlHotIssues.Items[index];

                CheckBox ckIn = (CheckBox)dli.FindControl("ckIncluded");
                int IDD = (int)((DataList)dlHotIssues).DataKeys[index];
                MeetingHotIssueData mht = mhv.GetByIssueID(IDD);
                if (mht != null)
                    ckIn.Checked = true;
            }
        }
        protected string GetTextToClient(bool toClient)
        {
            if (toClient)
                return Resource.ResourceManager["Reports_Yes"];
            else
                return Resource.ResourceManager["Reports_No"];
        }
        public string GetExecutionDate(object d)
        {
            if (d == null || d == DBNull.Value || !(d is DateTime))
                return "";
            DateTime dt = (DateTime)d;
            if (dt == Constants.DateMin || dt == Constants.DateMax || dt == new DateTime())
                return "";
            return dt.ToString("dd.MM.yyyy");
        }
        private void SaveCategories()
        {
            int nMeetingID = UIHelpers.GetIDParam();
            MeetingCategorieDAL.Delete(nMeetingID, int.Parse(ddlProject1.SelectedValue));
            foreach (ListItem li in lst.Items)
            {
                if (li.Selected)
                {
                    MeetingCategorieDAL.Insert(new MeetingCategorieData(-1, UIHelpers.GetIDParam(), int.Parse(li.Value)));
                }
            }
        }
        private void SaveIssues(bool bAll)
        {
            int mainProjectId = UIHelpers.ToInt(ddlProject1.SelectedValue);
            MeetingCategoriesVector v = MeetingCategorieDAL.LoadCollection(UIHelpers.GetIDParam(), mainProjectId);
            int nMeetingID = UIHelpers.GetIDParam();
            MeetingHotIssueDAL.Delete(nMeetingID, int.Parse(ddlProject1.SelectedValue));
            for (int index = 0; index < dlHotIssues.Items.Count; index++)
            {
                DataListItem dli = dlHotIssues.Items[index];

                CheckBox ckIn = (CheckBox)dli.FindControl("ckIncluded");

                if (ckIn.Checked || bAll)
                {
                    int ID = (int)((DataList)dlHotIssues).DataKeys[index];
                    HotIssueData hid = HotIssueDAL.Load(ID);
                    if (hid != null && v.GetByCategoryID(hid.HotIssueCategoryID) != null)
                    {
                        MeetingHotIssueData mht = new MeetingHotIssueData(-1, nMeetingID, ID);
                        MeetingHotIssueDAL.Save(mht);
                    }
                }
            }

        }
        private void Save()
        {
            SaveCategories();
            SaveIssues(false);
            Reload();
        }
        private void btnSave_Click(object sender, System.EventArgs e)
        {
            Save();
        }

        private void dlHotIssues_ItemCommand(object source, System.Web.UI.WebControls.DataListCommandEventArgs e)
        {
            Save();
            if (e.CommandSource is Button)
            {
                Button ib = (Button)e.CommandSource;
                string s = ib.ID;
                switch (ib.ID)
                {
                    case "btnEdit":
                        {
                            int HotIssueID = (int)((DataList)source).DataKeys[e.Item.ItemIndex];
                            Response.Redirect("EditHotIssue.aspx?mid=" + UIHelpers.GetIDParam().ToString() + "&id=" + HotIssueID.ToString());
                        }
                        break;
                }
            }
        }

        private void btnNew_Click(object sender, System.EventArgs e)
        {
            Response.Redirect("EditHotIssue.aspx?mid=" + UIHelpers.GetIDParam().ToString() + "&pid=" + ddlProject1.SelectedValue);
        }

        private void btnMinutes_Click(object sender, System.EventArgs e)
        {
            Save();
            ExportMinutes(true, false, false, false, false);
        }
        private void ExportMinutes(bool bAll, bool bExcel, bool bWord, bool bOnlyMeeting, bool bEnglish)
        {
            int IDD = UIHelpers.GetIDParam();
            int nProjectID = int.Parse(ddlProject1.SelectedValue);
            string PathOfPDF = string.Concat(System.Configuration.ConfigurationManager.AppSettings["MeetingHotIssuesFilesPath"], IDD, System.Configuration.ConfigurationManager.AppSettings["Extension"]);

            {



                MeetingData md = MeetingDAL.Load(IDD);
                //				MeetingProjectsVector mpv = MeetingProjectDAL.LoadCollection("MeetingProjectsSelByMeetingProc",SQLParms.CreateMeetingProjectsSelByMeetingProc(IDD));
                //				foreach(MeetingProjectData mpd in mpv)
                //					if(mpd.ProjectID==md.ProjectID)
                //					{
                //						mpv.Remove(mpd);
                //						break;
                //					}

                DataView dvProjects = UIHelpers.HotIssueCreateDVprojects(nProjectID, bEnglish);
                DataView dvUsers = UIHelpers.HotIssueCreateDVusers(md.Users, md.Subcontracters, md.Clients, md.OtherPeople, bEnglish);
                HotIssuesVector hiv = UIHelpers.HotIssueCreateHotIssuesVector(md, null, bAll, bOnlyMeeting, bEnglish, nProjectID);
                string meetingNumber = MeetingDAL.SelectMeetingNumber(md.ProjectID, md.MeetingDate);
                GrapeCity.ActiveReports.SectionReport report = new ReportMeetingHotIssuesAll(md.MeetingDate, md.StartHour, md.EndHour, md.Place, md.Notes, dvProjects, dvUsers, hiv, meetingNumber, bAll, md.OtherPeople, md.OtherMails, nProjectID, LoggedUser.FullName, 0, md.ASA);
                report.PageSettings.Orientation = GrapeCity.ActiveReports.Document.Section.PageOrientation.Default;
                report.Run(false);
                if (bEnglish)
                {
                    string sName = "";
                    SqlDataReader reader = null;
                    try
                    {
                        reader = UsersData.SelectUser(LoggedUser.UserID);
                        reader.Read();

                        if (!reader.IsDBNull(27))
                            sName = reader.GetString(27);
                    }
                    catch (Exception ex)
                    {
                        log.Error(ex);

                    }
                    finally
                    {
                        if (reader != null) reader.Close();
                    }
                    report = new ReportMeetingHotIssuesAllEN(md.MeetingDate, md.StartHour, md.EndHour, md.PlaceEN, md.Notes, dvProjects, dvUsers, hiv, meetingNumber, bAll, md.OtherPeople, md.OtherMails, nProjectID, sName, report.Document.Pages.Count, md.ASA);
                }
                else
                    report = new ReportMeetingHotIssuesAll(md.MeetingDate, md.StartHour, md.EndHour, md.Place, md.Notes, dvProjects, dvUsers, hiv, meetingNumber, bAll, md.OtherPeople, md.OtherMails, nProjectID, LoggedUser.FullName, report.Document.Pages.Count, md.ASA);
                report.PageSettings.Orientation = GrapeCity.ActiveReports.Document.Section.PageOrientation.Default;
                report.Run();


                string projectName, projectCode, administrativeName, add;
                decimal area = 0;
                int clientID = 0;
                bool phases; bool isPUP;
                ProjectsData.SelectProject(nProjectID, out projectName, out  projectCode, out administrativeName, out area, out add,
                    out clientID, out phases, out isPUP);
                string sFileName = md.MeetingDate.ToString("yyMMdd") + "_" + projectCode + "_PCM" + meetingNumber;
                if (bExcel)
                {
                    XlsExport xls = new XlsExport();
                    System.IO.MemoryStream memoryFile = new System.IO.MemoryStream();

                    xls.Export(report.Document, memoryFile);

                    Response.Clear();
                    Response.AppendHeader("content-disposition"
                        , "attachment; filename=" + sFileName + ".xls");
                    Response.ContentType = "application/xls";

                    memoryFile.WriteTo(Response.OutputStream);
                    Response.End();
                }
                else if (bWord)
                {
                    RtfExport rtf = new RtfExport();
                    System.IO.MemoryStream memoryFile = new System.IO.MemoryStream();

                    rtf.Export(report.Document, memoryFile);

                    Response.Clear();
                    Response.AppendHeader("content-disposition"
                        , "attachment; filename=" + sFileName + ".rtf");
                    Response.ContentType = "application/rtf";

                    memoryFile.WriteTo(Response.OutputStream);
                    Response.End();
                }
                else
                {
                    PdfExport pdf = new PdfExport();

                    pdf.NeverEmbedFonts = "";
                    System.IO.MemoryStream memoryFile = new System.IO.MemoryStream();

                    pdf.Export(report.Document, memoryFile);

                    Response.Clear();
                    Response.AppendHeader("content-disposition"
                        , "attachment; filename=" + sFileName + ".pdf");
                    Response.ContentType = "application/pdf";

                    memoryFile.WriteTo(Response.OutputStream);
                    Response.End();
                }
            }
        }

        private void btnCancelUP_Click(object sender, System.EventArgs e)
        {
            Response.Redirect("EditMeeting.aspx?id=" + UIHelpers.GetIDParam().ToString());
        }

        private void ckAll_CheckedChanged(object sender, System.EventArgs e)
        {
            SaveIssues(false);
            foreach (ListItem li in lst.Items)
            {
                li.Selected = ckAll.Checked;
            }
            Save();
            LoadHotIssues(UIHelpers.GetIDParam());
        }

        private void ddlProject1_SelectedIndexChanged(object sender, System.EventArgs e)
        {
            ckAll.Checked = false;
            LoadHotIssueCategories(int.Parse(ddlProject1.SelectedValue));
            LoadHotIssues(UIHelpers.GetIDParam());
        }
        protected string AssignedToText(object userID, object userType, object text)
        {
            if ((!(userID is int)) || (!(userType is int)) || (!(text is string)))
                return "";
            string sText = (string)text;
            if (sText.Trim().Length > 0 && (int)userID > 0)
            {
                sText = ", " + sText;
            }
            return UIHelpers.GetHotIssueUserNameAssigned((int)userID, (int)userType) + sText;
        }

        private void btnMark_Click(object sender, System.EventArgs e)
        {
            SaveIssues(true);
            LoadHotIssues(UIHelpers.GetIDParam());
        }
		
	}
}
