using System;
using System.Data;
using System.Data.SqlClient;
using Microsoft.ApplicationBlocks.Data;

namespace Asa.Timesheet.Data.Reports
{
	/// <summary>
	/// Summary description for Pokazateli.
	/// </summary>
	public class PokazateliReportData
	{
    public enum ContentTypes
    {
      Suteren = 1,
      Parter = 2,
	  Etaj = 3,
      KvadraturaNaChertej = 19,
    }

    public static DataSet SelectPokazateliReportData(int projectID, int building)
    {
      string spName = "Projects_PokazateliReport";

      SqlParameter[] parms = SqlHelperParameterCache.GetSpParameterSet(DBManager.GetConnectionString(), spName);
      parms[0].Value = projectID;
		if(building>0)
		parms[1].Value = building;
      DataSet ds = SqlHelper.ExecuteDataset(DBManager.GetConnectionString(), CommandType.StoredProcedure, spName, parms);

      ds.Relations.Add("Rel1", ds.Tables[0].Columns["ProjectID"],ds.Tables[1].Columns["ProjectID"], false);

      return ds;
    }

	}
}
