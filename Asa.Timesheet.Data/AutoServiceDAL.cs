using System;
using System.Data;
using System.Data.SqlClient; 
using System.Collections;
using Microsoft.ApplicationBlocks.Data;

namespace Asa.Timesheet.Data
{
	public class AutoServiceDAL
	{
		/// <summary>
		/// Loads a AutoServiceData object from the database using the given AutoServiceID
		/// </summary>
		/// <param name="autoServiceID">The primary key value</param>
		/// <returns>A AutoServiceData object</returns>
		public static AutoServiceData Load(int autoServiceID)
		{
			SqlParameter[] parameterValues = new SqlParameter[] { new SqlParameter("@AutoServiceID", autoServiceID) };
			using (SqlDataReader reader = SqlHelper.ExecuteReader(CommonDAL.ConnectionString, CommandType.StoredProcedure, "AutoServicesSelProc", parameterValues))
			{
				if (reader.Read())
				{
					AutoServiceData result = new AutoServiceData();
					LoadFromReader(reader,result);
					return result;
				}
				else
					return null;
			}
		}

		/// <summary>
		/// Loads a AutoServiceData object from the database using the given where clause
		/// </summary>
		/// <param name="whereClause">The filter expression for the query</param>
		/// <returns>A AutoServiceData object</returns>
		public static AutoServiceData Load(string whereClause)
		{
			SqlParameter[] parameterValues = new SqlParameter[] { new SqlParameter("@WhereClause", whereClause) };
			using (SqlDataReader reader = SqlHelper.ExecuteReader(CommonDAL.ConnectionString, CommandType.StoredProcedure, "AutoServicesListProc", parameterValues))
			{
				if (reader.Read())
				{
					AutoServiceData result = new AutoServiceData();
					LoadFromReader(reader,result);
					return result;
				}
				else
					return null;
			}
		}
		public static int Save( AutoServiceData source)
		{
			if (source.AutoServiceID == -1)
				return Insert(source);
			else
				return Update(source);	
		}

		public static void Delete( AutoServiceData source)
		{
			Delete(source.AutoServiceID);
		}
		/// <summary>
		/// Loads a collection of AutoServiceData objects from the database.
		/// </summary>
		/// <returns>A collection containing all of the AutoServiceData objects in the database.</returns>
		public static AutoServicesVector LoadCollection()
		{
			SqlParameter[] parms = new SqlParameter[] {};
			return LoadCollection("AutoServicesListProc", parms);
		}
	  
		/// <summary>
		/// Saves a collection of AutoServiceData objects to the database.
		/// </summary>
		/// <returns>A collection containing all of the AutoServiceData objects in the database.</returns>
		public static  void  SaveCollection(AutoServicesVector col,string spName, SqlParameter[] parms)
		{
			AutoServicesVector colOld = LoadCollection(spName,parms);
			UpdateCollection(colOld,col);
		}
		public static  void  SaveCollection(AutoServicesVector col)
		{
			AutoServicesVector colOld = LoadCollection();
			UpdateCollection(colOld,col);
		}
		public static  void  SaveCollection(AutoServicesVector col, string whereClause )
		{
			AutoServicesVector colOld = LoadCollection(whereClause);
			UpdateCollection(colOld,col);
		}
		/// <summary>
		/// Loads a collection of AutoServiceData objects from the database.
		/// </summary>
		/// <returns>A collection containing all of the AutoServiceData objects in the database.</returns>
		public static AutoServicesVector LoadCollection(string whereClause)
		{
			SqlParameter[] parms = new SqlParameter[] { new SqlParameter("@WhereClause", whereClause) };
			return LoadCollection("AutoServicesListProc", parms);
		}

		/// <summary>
		/// Loads a collection of AutoServiceData objects from the database.
		/// </summary>
		/// <returns>A collection containing all of the AutoServiceData objects in the database.</returns>
		public static AutoServicesVector LoadCollection(string spName, SqlParameter[] parms)
		{
			AutoServicesVector result = new AutoServicesVector();
			using (SqlDataReader reader = SqlHelper.ExecuteReader(CommonDAL.ConnectionString, CommandType.StoredProcedure, spName, parms))
			{
				while (reader.Read())
				{
					AutoServiceData tmp = new AutoServiceData();
					LoadFromReader(reader, tmp);
					result.Add(tmp);
				}
			}
			return result;
		}
		/// <summary>
		/// Deletes a AutoServiceData object from the database.
		/// </summary>
		/// <param name="autoServiceID">The primary key value</param>
		public static void Delete(int autoServiceID)
		{
			SqlParameter[] parameterValues = new SqlParameter[] { new SqlParameter("@AutoServiceID", autoServiceID) };
			SqlHelper.ExecuteNonQuery(CommonDAL.ConnectionString, CommandType.StoredProcedure, "AutoServicesInactiveProc", parameterValues);
		}

		public static void LoadFromReader(SqlDataReader reader, AutoServiceData target)
		{
			if (reader != null && !reader.IsClosed)
			{
				//target.Key = reader.GetInt32(0);
				if (!reader.IsDBNull(0)) target.AutoServiceID = reader.GetInt32(0);
				if (!reader.IsDBNull(1)) target.AutoServiceName = reader.GetString(1);
				if (!reader.IsDBNull(2)) target.AutoServicePerson = reader.GetString(2);
				if (!reader.IsDBNull(3)) target.AutoServicePhones = reader.GetString(3);
				if (!reader.IsDBNull(4)) target.IsActive = reader.GetBoolean(4);
			}
		}
		
	  
		public static int Insert( AutoServiceData source)
		{
			SqlParameter[] parameterValues = GetInsertParameterValues(source);
			SqlHelper.ExecuteNonQuery(CommonDAL.ConnectionString, CommandType.StoredProcedure, "AutoServicesInsProc", parameterValues);
			source.AutoServiceID = (int) parameterValues[0].Value;
			return source.AutoServiceID;
		}

		public static int Update( AutoServiceData source)
		{
			SqlParameter[] parameterValues = GetUpdateParameterValues(source);
			SqlHelper.ExecuteNonQuery(CommonDAL.ConnectionString, CommandType.StoredProcedure,
				"AutoServicesUpdProc", parameterValues);
			return source.AutoServiceID;
		}
		private static void UpdateCollection(AutoServicesVector colOld,AutoServicesVector col)
		{
			// Delete all old items
			foreach( AutoServiceData itemOld in colOld)
			{
				if(!ExistsKey(itemOld.AutoServiceID, col))
					Delete(itemOld.AutoServiceID);
			}
			foreach( AutoServiceData item in col)
			{
				if(item.AutoServiceID <= -1)
					Insert(item);
				else
					Update(item);
			}
		}
		private static bool ExistsKey(int Key, AutoServicesVector col)
		{
			foreach( AutoServiceData item in col)
				if(item.AutoServiceID==Key)
					return true;
			return false;
		}

		private static SqlParameter GetSqlParameter(string name, ParameterDirection direction, object value)
		{
			SqlParameter p = new SqlParameter(name, value);
			p.Direction = direction;
			return p;
		}

		private static SqlParameter[] GetInsertParameterValues( AutoServiceData source)
		{
			SqlParameter[] prams = new SqlParameter[5];
			prams[0] = GetSqlParameter("@AutoServiceID", ParameterDirection.Output, source.AutoServiceID);
			prams[1] = GetSqlParameter("@AutoServiceName", ParameterDirection.Input, source.AutoServiceName);
			prams[2] = GetSqlParameter("@AutoServicePerson", ParameterDirection.Input, source.AutoServicePerson);
			prams[3] = GetSqlParameter("@AutoServicePhones", ParameterDirection.Input, source.AutoServicePhones);
			prams[4] = GetSqlParameter("@IsActive", ParameterDirection.Input, source.IsActive);
			return prams;
		}

		private static SqlParameter[] GetUpdateParameterValues( AutoServiceData source)
		{
			return new SqlParameter[] {
										  new SqlParameter("@AutoServiceID", source.AutoServiceID),
										  new SqlParameter("@AutoServiceName", source.AutoServiceName),
										  new SqlParameter("@AutoServicePerson", source.AutoServicePerson),
										  new SqlParameter("@AutoServicePhones", source.AutoServicePhones),
										  new SqlParameter("@IsActive", source.IsActive)
									  };
		}
	}
}   

