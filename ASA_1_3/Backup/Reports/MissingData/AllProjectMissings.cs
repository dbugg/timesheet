using System;
using System.Collections;
using GrapeCity.ActiveReports;

namespace Asa.Timesheet.WebPages.Reports.MissingData
{
	/// <summary>
	/// Summary description for AllProjectMissings.
	/// </summary>
	public class AllProjectMissings
	{
        public static SectionReport CreateProjectMissings(ArrayList al)
        {
            GrapeCity.ActiveReports.SectionReport rptMain = new GrapeCity.ActiveReports.SectionReport();
            //int currentPage = 1;

            GrapeCity.ActiveReports.SectionReport rptTemp = null;

            foreach (int projectID in al)
            {
                rptTemp = new MissingDataReport(projectID);
                rptTemp.Run();
                rptMain.Document.Pages.AddRange(rptTemp.Document.Pages);
            }
            return rptMain;
            //currentPage+= rptTemp.Document.Pages.Count;
        }
	}
}
