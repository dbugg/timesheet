﻿<%@ Register TagPrefix="uc1" TagName="PageHeader" Src="UserControls/PageHeader.ascx" %>
<%@ Page language="c#" Codebehind="EditAutomobile.aspx.cs" AutoEventWireup="false" Inherits="Asa.Timesheet.WebPages.EditAutomobile" %>
<%@ Register TagPrefix="uc1" TagName="EditForm" Src="UserControls/EditForm.ascx" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
	<HEAD>
		<title>Edit Automobile</title>
		<meta name="GENERATOR" content="Microsoft Visual Studio .NET 7.1">
		<meta name="CODE_LANGUAGE" content="C#">
		<meta name="vs_defaultClientScript" content="JavaScript">
		<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5">
		<LINK rel="stylesheet" type="text/css" href="styles/timesheet.css">
		<script language="javascript" src="PopupCalendar.js"></script>
	</HEAD>
	<body onload="if(document.getElementById('txtAccountPass')!=null) document.getElementById('txtAccountPass').value=document.getElementById('hdnAccountPass').value;"
		bottomMargin="0" leftMargin="0" rightMargin="0" topMargin="0">
		<form id="Form1" method="post" runat="server">
			<table border="0" cellSpacing="0" cellPadding="0" width="100%" height="100%">
				<tr height="1">
					<td><uc1:pageheader id="header" runat="server"></uc1:pageheader><uc1:editform id="editCtrl" runat="server"></uc1:editform></td>
				</tr>
				<tr>
					<td>
						<table border="0" cellSpacing="0" cellPadding="0" width="100%" height="100%">
							<tr>
								<td class="menuLeft"><asp:placeholder id="menuHolder" runat="server"></asp:placeholder></td>
								<td bgColor="lightgrey" width="1" noWrap></td>
								<td style="PADDING-LEFT: 5px; PADDING-TOP: 3px" vAlign="top" noWrap>
									<table width="880">
										<TR>
											<td style="HEIGHT: 5px" colSpan="2"></TD>
										</TR>
										<TR>
											<td colSpan="2"><IMG src="images/dot.gif" width="100%" height="1"></TD>
										</TR>
										<TR>
											<td height="3" colSpan="2"></td>
										</TR>
										<TR>
											<td style="WIDTH: 4px"></TD>
											<td>
												<TABLE id="Table6" border="0" cellSpacing="0" cellPadding="0">
													<TR>
														<td style="WIDTH: 97px"><asp:button id="btnSaveUP" runat="server" Text="Запиши" CssClass="ActionButton" EnableViewState="False"></asp:button><asp:button style="DISPLAY: none" id="btnEditUP" runat="server" Text="Редактирай" CssClass="ActionButton"
																EnableViewState="False"></asp:button></TD>
														<td style="WIDTH: 245px"><asp:button id="btnCancelUP" runat="server" Text="Откажи" CssClass="ActionButton" EnableViewState="False"></asp:button></TD>
														<td></TD>
													</TR>
												</TABLE>
											</TD>
										</TR>
										<TR>
											<td height="3"></TD>
										</TR>
										<TR>
											<td colSpan="2"><IMG src="images/dot.gif" width="100%" height="1"></TD>
										</TR>
									</table>
									<table width="880">
										<tr>
											<td vAlign="top">
												<TABLE id="Table3" border="0" cellSpacing="0" cellPadding="0">
													<TR>
														<td style="WIDTH: 4px"></TD>
														<td>
															<TABLE id="tblForm" border="0" cellSpacing="0" cellPadding="3" width="100%">
																<TR>
																	<td style="WIDTH: 168px"><asp:label id="lblName" runat="server" CssClass="enterDataLabel" EnableViewState="False" Width="100%"
																			Font-Bold="True">Марка:</asp:label></TD>
																	<td><asp:textbox id="txtMarka" runat="server" CssClass="enterDataBox" Width="336px"></asp:textbox>&nbsp;<IMG alt="" src="images/required1.gif" width="16" height="16"></TD>
																</TR>
																<TR>
																	<td style="WIDTH: 168px"><asp:label id="Label16" runat="server" CssClass="enterDataLabel" Width="100%" Font-Bold="True">Номер на автомобил:</asp:label></TD>
																	<td><asp:textbox id="txtNomer" runat="server" CssClass="enterDataBox" Width="336px"></asp:textbox>&nbsp;<IMG style="Z-INDEX: 0" alt="" src="images/required1.gif" width="16" height="16"></TD>
																</TR>
																<TR>
																	<td style="WIDTH: 168px"><asp:label id="Label12" runat="server" CssClass="enterDataLabel" Width="100%" Font-Bold="True">Ползва се от:</asp:label></TD>
																	<td><asp:dropdownlist id="ddlUser" runat="server" Width="336px" Height="20px"></asp:dropdownlist></TD>
																</TR>
																<TR>
																	<td style="WIDTH: 168px"><asp:label id="lbDate" runat="server" CssClass="enterDataLabel" Width="100%" Font-Bold="True">Гражд. отговорност валидна до:</asp:label></TD>
																	<td><asp:textbox id="txtCivilDate" runat="server" CssClass="enterDataBox" Width="88px" MaxLength="10"></asp:textbox>&nbsp;
																		<IMG id="imgCivil" alt="Calendar" align="absBottom" src="images/calendar.ico" width="18"
																			runat="server"></TD>
																</TR>
																<TR>
																	<td style="WIDTH: 168px"><asp:label id="Label3" runat="server" CssClass="enterDataLabel" Width="100%" Font-Bold="True">Застраховател гражд.отговорност:</asp:label></TD>
																	<td><asp:dropdownlist id="ddlCivil" runat="server" Width="336px" Height="20px"></asp:dropdownlist></TD>
																</TR>
																<TR>
																	<td style="WIDTH: 168px"><asp:label id="Label4" runat="server" CssClass="enterDataLabel" Width="100%" Font-Bold="True">Каско валидно до:</asp:label></TD>
																	<td><asp:textbox id="txtKaskoDate" runat="server" CssClass="enterDataBox" Width="88px" MaxLength="10"></asp:textbox>&nbsp;
																		<IMG id="imgKasko" alt="Calendar" align="absBottom" src="images/calendar.ico" width="18"
																			runat="server"></TD>
																</TR>
																<TR>
																	<td style="WIDTH: 168px"><asp:label id="Label5" runat="server" CssClass="enterDataLabel" Width="100%" Font-Bold="True">Застраховател Каско:</asp:label></TD>
																	<td><asp:dropdownlist id="ddlKasko" runat="server" Width="336px" Height="20px"></asp:dropdownlist></TD>
																</TR>
																<TR>
																	<td style="WIDTH: 168px"><asp:label id="Label1" runat="server" CssClass="enterDataLabel" Width="100%" Font-Bold="True">Зелена карта валидна до:</asp:label></TD>
																	<td><asp:textbox id="txtGreencardDate" runat="server" CssClass="enterDataBox" Width="88px" MaxLength="10"></asp:textbox>&nbsp;
																		<IMG id="imgGreencard" alt="Calendar" align="absBottom" src="images/calendar.ico" width="18"
																			runat="server"></TD>
																</TR>
																<TR>
																	<td style="WIDTH: 168px"><asp:label id="Label2" runat="server" CssClass="enterDataLabel" Width="100%" Font-Bold="True">Застраховател Зелена карта:</asp:label></TD>
																	<td><asp:dropdownlist id="ddlGreencard" runat="server" Width="336px" Height="20px"></asp:dropdownlist></TD>
																</TR>
																<TR>
																	<td style="WIDTH: 168px"><asp:label id="Label6" runat="server" CssClass="enterDataLabel" Width="100%" Font-Bold="True">Годишен преглед валиден до:</asp:label></TD>
																	<td><asp:textbox id="txtAnnualCheck" runat="server" CssClass="enterDataBox" Width="88px" MaxLength="10"></asp:textbox>&nbsp;
																		<IMG id="imgCheck" alt="Calendar" align="absBottom" src="images/calendar.ico" width="18"
																			runat="server"></TD>
																</TR>
																<TR>
																	<td style="WIDTH: 168px"><asp:label id="Label7" runat="server" CssClass="enterDataLabel" Width="100%" Font-Bold="True">Последна смяна на масло на:</asp:label></TD>
																	<td><asp:textbox id="txtDateOil" runat="server" CssClass="enterDataBox" Width="88px" MaxLength="10"></asp:textbox>&nbsp;
																		<IMG id="imgOil" alt="Calendar" align="absBottom" src="images/calendar.ico" width="18"
																			runat="server"></TD>
																</TR>
																<TR>
																	<td style="WIDTH: 168px"><asp:label id="Label8" runat="server" CssClass="enterDataLabel" Width="100%" Font-Bold="True">Последна смяна масло на км:</asp:label></TD>
																	<td><asp:textbox id="txtOilKM" runat="server" CssClass="enterDataBox" Width="336px"></asp:textbox></TD>
																</TR>
																<TR>
																	<td style="WIDTH: 168px"><asp:label id="Label9" runat="server" CssClass="enterDataLabel" Width="100%" Font-Bold="True">Следваща смяна на масло на:</asp:label></TD>
																	<td><asp:textbox id="txtDateOilNext" runat="server" CssClass="enterDataBox" Width="88px" MaxLength="10"></asp:textbox>&nbsp;
																		<IMG id="imgOilNext" alt="Calendar" align="absBottom" src="images/calendar.ico" width="18"
																			runat="server"></TD>
																</TR>
																<TR>
																	<td style="WIDTH: 168px"><asp:label id="Label10" runat="server" CssClass="enterDataLabel" Width="100%" Font-Bold="True">Следваща смяна масло на км:</asp:label></TD>
																	<td><asp:textbox id="txtOilNextKM" runat="server" CssClass="enterDataBox" Width="336px"></asp:textbox></TD>
																</TR>
																<TR>
																	<td style="WIDTH: 168px"><asp:label id="Label11" runat="server" CssClass="enterDataLabel" Width="100%" Font-Bold="True">Сервиз:</asp:label></TD>
																	<td><asp:dropdownlist id="ddlService" runat="server" Width="336px" Height="20px"></asp:dropdownlist></TD>
																</TR>
															</TABLE>
														</TD>
													</TR>
													<TR>
														<td style="HEIGHT: 5px" colSpan="2"></TD>
													</TR>
													<TR>
														<td height="3"></TD>
													</TR>
												</TABLE>
											</td>
											<td width="20"></td>
											<td vAlign="top" align="left"><asp:datalist id="dlOldIssues" runat="server" Width="100%" RepeatDirection="Horizontal" RepeatColumns="1">
													<EditItemStyle VerticalAlign="Top"></EditItemStyle>
													<ItemStyle VerticalAlign="Top"></ItemStyle>
													<ItemTemplate>
														<TABLE cellSpacing="0" cellPadding="3" width="100%" border="0">
															<TR>
																<td style="WIDTH: 168px">
																	<asp:label id="Label13" runat="server" CssClass="enterDataLabel" Width="100%">Ремонт:</asp:label></TD>
																<td>
																	<asp:label id="DLlbCreatedDate" runat="server" CssClass="enterDataBox" Width="100%" text='<%# DataBinder.Eval(Container, "DataItem.AutoRepairDescription") %>'>
																	</asp:label></TD>
															</TR>
															<TR>
																<td style="WIDTH: 168px">
																	<asp:label id="Label14" runat="server" CssClass="enterDataLabel" Width="100%">Сервиз:</asp:label></TD>
																<td>
																	<asp:label id="Label15" runat="server" CssClass="enterDataBox" Width="100%" text='<%# DataBinder.Eval(Container, "DataItem.AutoServiceName") %>'>
																	</asp:label></TD>
															</TR>
															<TR>
																<td style="WIDTH: 168px">
																	<asp:label id="Label18" runat="server" CssClass="enterDataLabel" Width="100%">Дата:</asp:label></TD>
																<td>
																	<asp:label id="Label19" runat="server" CssClass="enterDataBox" Width="100%" text='<%# Asa.Timesheet.Data.Util.TimeHelper.FormatDate((DateTime)DataBinder.Eval(Container, "DataItem.DateAdded")) %>'>
																	</asp:label></TD>
															</TR>
															</asp:Label>
															<TR>
																<td colSpan="2"><IMG height="1" src="images/dot.gif" width="100%"></TD>
															</TR>
														</TABLE>
													</ItemTemplate>
												</asp:datalist><asp:label style="Z-INDEX: 0" id="lblNew" runat="server" Width="92px" Font-Bold="True">Нов ремонт:</asp:label><BR>
												<asp:textbox style="Z-INDEX: 0" id="txtNewRepair" runat="server" CssClass="enterDataBox" Width="280px"
													TextMode="MultiLine" Height="56px"></asp:textbox><BR>
												<asp:label style="Z-INDEX: 0" id="Label17" runat="server" Width="92px" Font-Bold="True">Дата:</asp:label><BR>
												<asp:textbox style="Z-INDEX: 0" id="txtDateRepair" runat="server" CssClass="enterDataBox" Width="280px"></asp:textbox>&nbsp;
												<IMG id="imgRepair" alt="Calendar" align="absBottom" src="images/calendar.ico" width="18"
													runat="server"><BR>
												<asp:button style="Z-INDEX: 0" id="btnAdd" runat="server" Text="Добави" CssClass="ActionButton"
													EnableViewState="False"></asp:button></td>
										</tr>
									</table>
									<table width="880">
										<TR>
											<td colSpan="2"><IMG src="images/dot.gif" width="100%" height="1"></TD>
										</TR>
										<TR>
											<td height="3" colSpan="2"></td>
										</TR>
										<TR>
											<td style="WIDTH: 4px"></TD>
											<td align="left">
												<TABLE id="Table5" border="0" cellSpacing="0" cellPadding="0" align="left">
													<TR>
														<td style="WIDTH: 97px"><asp:button id="btnSave" runat="server" Text="Запиши" CssClass="ActionButton" EnableViewState="False"></asp:button><asp:button style="DISPLAY: none" id="btnEdit" runat="server" Text="Редактирай" CssClass="ActionButton"
																EnableViewState="False"></asp:button></TD>
														<td style="WIDTH: 245px"><asp:button id="btnCancel" runat="server" Text="Откажи" CssClass="ActionButton" EnableViewState="False"></asp:button></TD>
														<td></TD>
													</TR>
												</TABLE>
											</TD>
										</TR>
										<TR>
											<td colSpan="2"><IMG height="1" src="images/dot.gif" width="100%"></TD>
										</TR>
									</table>
									<br>
									&nbsp;
									<asp:label id="lblInfo" runat="server" EnableViewState="False" CssClass="InfoLabel"></asp:label><asp:label id="lblError" runat="server" CssClass="ErrorLabel" EnableViewState="False" ForeColor="Red"></asp:label></td>
							</tr>
						</table>
					</td>
				</tr>
			</table>
		</form>
	</body>
</HTML>
