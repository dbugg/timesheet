﻿<%@ Page language="c#" Codebehind="Subcontracters.aspx.cs" AutoEventWireup="false" Inherits="Asa.Timesheet.WebPages.Subcontracters" %>
<%@ Register TagPrefix="uc1" TagName="PageHeader" Src="UserControls/PageHeader.ascx" %>
<%@ Register TagPrefix="radg" Namespace="Telerik.WebControls" Assembly="RadGrid" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
	<head runat="server">
		<title>Subcontracters</title>
		<meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
		<meta content="C#" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<LINK href="styles/timesheet.css" type="text/css" rel="stylesheet">
	</HEAD>
	<body bottomMargin="0" leftMargin="0" topMargin="0" rightMargin="0">
		<form id="Form1" method="post" runat="server">
			<table height="100%" cellSpacing="0" cellPadding="0" width="100%" border="0">
				<tr height="1">
					<td>
						<uc1:PageHeader id="header" runat="server"></uc1:PageHeader>
					</td>
				</tr>
				<tr>
					<td>
						<table height="100%" cellSpacing="0" cellPadding="0" width="100%" border="0">
							<tr>
								<td class="menuLeft"><asp:placeholder id="menuHolder" runat="server"></asp:placeholder></td>
								
								<td style="PADDING-RIGHT: 5px; PADDING-LEFT: 5px; PADDING-TOP: 3px" vAlign="top" noWrap><br>
									<asp:label id="lblError" runat="server" ForeColor="Red" Visible="False" EnableViewState="False"
										CssClass="ErrorLabel"></asp:label><asp:label id="lblInfo" runat="server" Visible="False" EnableViewState="False" CssClass="InfoLabel"></asp:label><br>
									<br>
									<TABLE id="Table1" style="WIDTH: 848px; HEIGHT: 26px" cellSpacing="1" cellPadding="1" width="848"
										border="0">
										<TR>
											<td style="WIDTH: 105px"><asp:label id="lblName" runat="server" CssClass="enterDataLabel" EnableViewState="False" Font-Bold="True"
													Width="100%">Тип:</asp:label></TD>
											<td>
												<asp:dropdownlist id="ddlProjectsStatus" runat="server" CssClass="enterDataBox" Width="250px" AutoPostBack="True">
												</asp:dropdownlist>&nbsp;&nbsp;&nbsp;</TD>
										</TR>
										<TR>
											<td style="WIDTH: 105px"><asp:label id="Label2" runat="server" CssClass="enterDataLabel" EnableViewState="False" Font-Bold="True"
													Width="100%">Сграда:</asp:label></TD>
											<td><asp:dropdownlist id="ddlBuildingTypes" runat="server" CssClass="enterDataBox" Width="250px" AutoPostBack="True"></asp:dropdownlist></TD>
										</TR>
										<TR>
											<td style="WIDTH: 105px"><asp:label id="Label3" runat="server" CssClass="enterDataLabel" EnableViewState="False" Font-Bold="True"
													Width="100%">Проект:</asp:label></TD>
											<td>
												<asp:DropDownList id="ddlProject" runat="server" CssClass="enterDataBox" Width="250px"></asp:DropDownList></TD>
										</TR>
										<TR>
											<td style="WIDTH: 105px"></TD>
											<td>
												<asp:button id="btnShow" runat="server" CssClass="ActionButton" Text="Покажи"></asp:button></TD>
										</TR>
									</TABLE>
									<p></p>
									<asp:panel id="grid" 
										runat="server" Height="450px" Width="100%">
										<asp:datagrid id="grdClients" runat="server" CssClass="Grid" Width="100%" BackColor="Transparent"
											CellPadding="4" AutoGenerateColumns="False" AllowSorting="True">
											<AlternatingItemStyle CssClass="GridAltItem" BackColor="WhiteSmoke"></AlternatingItemStyle>
											<ItemStyle CssClass="GridItem"></ItemStyle>
											<HeaderStyle CssClass="GridHeader"></HeaderStyle>
											<Columns>
												<asp:TemplateColumn HeaderText="#">
													<ItemStyle Width="10px" ForeColor="DimGray"></ItemStyle>
													<ItemTemplate>
														<asp:Label CssClass="menuTable" runat="server" Text='<%# (int)DataBinder.Eval(Container, "ItemIndex")+1 %>' ID="Label1" NAME="Label1">
														</asp:Label>
													</ItemTemplate>
												</asp:TemplateColumn>
												<asp:TemplateColumn HeaderText="Име" SortExpression="Name">
													<ItemTemplate>
														<asp:LinkButton id="lnk" runat="server" CssClass="menuTable"  CommandName="Edit">
															<span Class="menuTable">
																<%# DataBinder.Eval(Container, "DataItem.SubcontracterName") %>
															</span>
														</asp:LinkButton>
													</ItemTemplate>
												</asp:TemplateColumn>
												<asp:BoundColumn DataField="SubcontracterType" SortExpression="Type" HeaderText="Тип">
													<HeaderStyle Width="50px"></HeaderStyle>
													<ItemStyle ForeColor="DimGray"></ItemStyle>
												</asp:BoundColumn>
												<asp:BoundColumn DataField="Address" SortExpression="Address" HeaderText="Адрес">
													<ItemStyle ForeColor="DimGray"></ItemStyle>
												</asp:BoundColumn>
												<asp:BoundColumn DataField="Phone" SortExpression="Phone" HeaderText="Телефон"></asp:BoundColumn>
												<asp:TemplateColumn>
													<HeaderStyle Wrap="False" Width="20px"></HeaderStyle>
													<ItemStyle Wrap="False" HorizontalAlign="Center"></ItemStyle>
													<ItemTemplate>
														<asp:ImageButton id="ibEditItem" runat="server" ImageUrl="images/edit1.gif" ToolTip="Редактирай"></asp:ImageButton>
													</ItemTemplate>
												</asp:TemplateColumn>
												<asp:TemplateColumn>
													<HeaderStyle Wrap="False" Width="20px"></HeaderStyle>
													<ItemStyle Wrap="False" HorizontalAlign="Center"></ItemStyle>
													<ItemTemplate>
														<asp:ImageButton id="ibDeleteItem" runat="server" Width="12px" Height="12px" ImageUrl="images/delete.gif"
															ToolTip="Изтрий"></asp:ImageButton>
													</ItemTemplate>
												</asp:TemplateColumn>
											</Columns>
										</asp:datagrid>
									</asp:panel>
									<TABLE id="Table4" 
										cellSpacing="0" cellPadding="4" width="100%" border="0">
										<TR>
											<td noWrap><asp:button id="btnNewClient" runat="server" CssClass="ActionButton" Width="200px"></asp:button>&nbsp;
												<asp:button id="btnExport" runat="server" CssClass="ActionButton" Text="Експорт"></asp:button>&nbsp;
												<asp:button id="btnShowHidedSubContracters" runat="server" CssClass="ActionButton" Text="Покажи скрити"
													Width="112px"></asp:button></TD>
										</TR>
									</TABLE>
								</td>
							</tr>
						</table>
					</td>
				</tr>
			</table>
			<radg:radgrid id="gridCalls" runat="server" Width="100%" CssClass="RadGrid" GridLines="Horizontal"
				AutoGenerateColumns="False">
				<PagerStyle CssClass="GridHeader" Mode="NumericPages"></PagerStyle>
				<ItemStyle HorizontalAlign="Center" CssClass="GridItem"></ItemStyle>
				<GroupPanel Visible="False"></GroupPanel>
				<HeaderStyle HorizontalAlign="Center" Wrap="False" CssClass="GridHeader"></HeaderStyle>
				<AlternatingItemStyle HorizontalAlign="Center" CssClass="GridItem"></AlternatingItemStyle>
				<GroupHeaderItemStyle BorderColor="Black" BackColor="Silver"></GroupHeaderItemStyle>
				<MasterTableView DataSourcePersistenceMode="NoPersistence" AllowCustomPaging="False" AllowSorting="True"
					PageSize="15" GridLines="Horizontal" AllowPaging="False" Visible="True">
					<Columns>
						<radg:GridBoundColumn UniqueName="SubcontracterName" HeaderButtonType="TextButton" HeaderText="Име" DataField="SubcontracterName"></radg:GridBoundColumn>
						<radg:GridBoundColumn UniqueName="SubcontracterType" HeaderButtonType="TextButton" HeaderText="Тип" DataField="SubcontracterType"></radg:GridBoundColumn>
						<radg:GridBoundColumn UniqueName="Delo" HeaderButtonType="TextButton" HeaderText="Фирмено дело" DataField="Delo"></radg:GridBoundColumn>
						<radg:GridBoundColumn UniqueName="Bulstat" HeaderButtonType="TextButton" HeaderText="Булстат" DataField="Bulstat"></radg:GridBoundColumn>
						<radg:GridBoundColumn UniqueName="NDR" HeaderButtonType="TextButton" HeaderText="НДР" DataField="NDR"></radg:GridBoundColumn>
						<radg:GridBoundColumn UniqueName="Address" HeaderButtonType="TextButton" HeaderText="Адрес" DataField="Address"></radg:GridBoundColumn>
						<radg:GridBoundColumn UniqueName="Phone" HeaderButtonType="TextButton" HeaderText="Телефон" DataField="Phone"></radg:GridBoundColumn>
						<radg:GridBoundColumn UniqueName="Fax" HeaderButtonType="TextButton" HeaderText="Факс" DataField="Fax"></radg:GridBoundColumn>
						<radg:GridBoundColumn UniqueName="Email" HeaderButtonType="TextButton" HeaderText="Ел.адрес" DataField="Email"></radg:GridBoundColumn>
						<radg:GridBoundColumn UniqueName="Webpage" HeaderButtonType="TextButton" HeaderText="Уебсайт" DataField="Webpage"></radg:GridBoundColumn>
						<radg:GridBoundColumn UniqueName="Manager" HeaderButtonType="TextButton" HeaderText="Представлявано от"
							DataField="Manager"></radg:GridBoundColumn>
						<radg:GridBoundColumn UniqueName="Name1" HeaderButtonType="TextButton" HeaderText="Представител-договор:"
							DataField="Name1"></radg:GridBoundColumn>
						<radg:GridBoundColumn UniqueName="Phone1" HeaderButtonType="TextButton" HeaderText="Телефон" DataField="Phone1"></radg:GridBoundColumn>
						<radg:GridBoundColumn UniqueName="Email1" HeaderButtonType="TextButton" HeaderText="Ел.адрес" DataField="Email1"></radg:GridBoundColumn>
						<radg:GridBoundColumn UniqueName="Name2" HeaderButtonType="TextButton" HeaderText="Представител-техн. въпроси:"
							DataField="Name2"></radg:GridBoundColumn>
						<radg:GridBoundColumn UniqueName="Phone2" HeaderButtonType="TextButton" HeaderText="Телефон" DataField="Phone2"></radg:GridBoundColumn>
						<radg:GridBoundColumn UniqueName="Email2" HeaderButtonType="TextButton" HeaderText="Ел.адрес" DataField="Email2"></radg:GridBoundColumn>
					</Columns>
					<RowIndicatorColumn Visible="False" UniqueName="RowIndicator">
						<HeaderStyle Width="20px"></HeaderStyle>
					</RowIndicatorColumn>
					<EditFormSettings>
						<EditColumn UniqueName="EditCommandColumn"></EditColumn>
					</EditFormSettings>
					<ExpandCollapseColumn ButtonType="ImageButton" Visible="False" UniqueName="ExpandColumn">
						<HeaderStyle Width="19px"></HeaderStyle>
					</ExpandCollapseColumn>
				</MasterTableView>
			</radg:radgrid>
		</form>
	</body>
</HTML>
