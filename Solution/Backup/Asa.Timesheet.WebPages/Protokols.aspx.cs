using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Data.SqlClient;
using log4net;
using Asa.Timesheet.Data;
using Asa.Timesheet.Data.Entities;
using Asa.Timesheet.Data.Util;
using GrapeCity.ActiveReports.Export.Pdf.Section;
using GrapeCity.ActiveReports.Export.Excel.Section;
using Asa.Timesheet.WebPages.Reports;

using System.IO;

namespace Asa.Timesheet.WebPages
{
	/// <summary>
	/// Summary description for Meetings.
	/// </summary>
	public class Protokols : TimesheetPageBase
	{
		#region WebControls
		private static readonly ILog log = LogManager.GetLogger(typeof(Protokols));
		protected System.Web.UI.WebControls.PlaceHolder menuHolder;
		protected System.Web.UI.WebControls.Label lblError;
		protected System.Web.UI.WebControls.Label lblInfo;
		protected System.Web.UI.WebControls.Panel grid;
		protected System.Web.UI.WebControls.Button btnExport;
		protected System.Web.UI.WebControls.DataGrid grdMeetings;
		protected System.Web.UI.WebControls.Button btnNew;
		protected Telerik.WebControls.RadGrid gridCalls;
		protected System.Web.UI.WebControls.Button btnSearch;
		protected System.Web.UI.WebControls.Label Label8;
		protected System.Web.UI.WebControls.Label lblProject;
		protected System.Web.UI.WebControls.DropDownList ddlProject;
		protected System.Web.UI.WebControls.DropDownList ddlProjectsStatus;
		protected System.Web.UI.WebControls.Label Label4;
		protected System.Web.UI.WebControls.TextBox txtStartDate;
		protected System.Web.UI.HtmlControls.HtmlImage lkCalendar1;
		protected System.Web.UI.WebControls.DropDownList ddlBuildingTypes;
		protected System.Web.UI.WebControls.TextBox txtEndDate;
		protected System.Web.UI.HtmlControls.HtmlImage lkCalendar2;
		protected System.Web.UI.WebControls.Label Label2;
		protected Asa.Timesheet.WebPages.UserControls.PageHeader header;
		#endregion
	
		#region enums
	
		private enum GridColumns
		{
			Number=0,
			Name ,
			MeetingDate,
			Scan,
			DeleteItem
		}
	
		#endregion
		
		#region PageLoad
        private void Page_Load(object sender, System.EventArgs e)
        {
            if (!(LoggedUser.HasPaymentRights || LoggedUser.IsSecretary || LoggedUser.IsAssistant || LoggedUser.IsAdmin))
            {
                ErrorRedirect(Resource.ResourceManager["noRightsAlert"]);
            }
            lkCalendar1.Attributes["onclick"] = TimeHelper.InvokePopupCal(txtStartDate);
            lkCalendar2.Attributes["onclick"] = TimeHelper.InvokePopupCal(txtEndDate);
            UIHelpers.CreateMenu(menuHolder, LoggedUser);

            if (!this.IsPostBack)
            {
                header.PageTitle = Resource.ResourceManager["Protokols_PageTitle"];
                header.UserName = this.LoggedUser.UserName;
                UIHelpers.LoadProjectStatus(ddlProjectsStatus, (int)ProjectsData.ProjectsByStatus.Active);
                UIHelpers.LoadBuildingTypes("", ddlBuildingTypes);
                LoadProjects();
                BindGrid();

            }
            else
                UIHelpers.LoadBuildingTypes(ddlBuildingTypes.SelectedValue, ddlBuildingTypes, (ProjectsData.ProjectsByStatus)int.Parse(ddlProjectsStatus.SelectedValue));

            if (!(LoggedUser.IsSecretary || LoggedUser.HasPaymentRights))
            {
                btnNew.Visible = false;
            }
        }
        private bool LoadProjects()
        {
            return UIHelpers.LoadProjects(ddlProject, ddlProjectsStatus, ddlBuildingTypes);
            //			SqlDataReader reader = null;
            //			
            //			try
            //			{
            //				reader = ProjectsData.SelectProjectNamesClear(this.ProjectsStatus, int.Parse(ddlBuildingTypes.SelectedValue));
            //			
            //				ddlProject.DataSource = reader;
            //				ddlProject.DataValueField = "ProjectID";
            //				ddlProject.DataTextField = "ProjectName";
            //				ddlProject.DataBind();
            //
            //				ddlProject.Items.Insert(0, new ListItem("<"+Resource.ResourceManager["reports_ddlAllProjects"]+">", "-1"));
            //				ddlProject.SelectedValue = "-1";
            //			}
            //			catch (Exception ex)
            //			{
            //				log.Error(ex);
            //				return false;
            //			}
            //
            //			finally
            //			{
            //				if (reader!=null) reader.Close();
            //			}
            //			return true;
        }

		#endregion
	
		#region HTML
        protected bool GetVisible(object o)
        {
            if (o == DBNull.Value)
                return false;
            else return (bool)o;
        }
        protected string GetURL(int ID)
        {
            int mdID = -1;
            ProjectProtocolsVector mdv = ProjectProtokolDAL.LoadCollection("ProjectProtokolsSelByProtocolProc", SQLParms.CreateProjectProtokolsSelByProtocolProc(ID));
            foreach (ProjectProtokolData mdd in mdv)
            {
                mdID = mdd.ProjectProtokolID;
                break;
            }
            return string.Concat(System.Configuration.ConfigurationManager.AppSettings["ScanPathProtocol"], mdID, System.Configuration.ConfigurationManager.AppSettings["Extension"]);
        }	
		#endregion

		#region Web Form Designer generated code
        override protected void OnInit(EventArgs e)
        {
            //
            // CODEGEN: This call is required by the ASP.NET Web Form Designer.
            //
            InitializeComponent();
            base.OnInit(e);
        }
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
        private void InitializeComponent()
        {
            this.ddlProjectsStatus.SelectedIndexChanged += new System.EventHandler(this.ddlProjectsStatus_SelectedIndexChanged);
            this.ddlBuildingTypes.SelectedIndexChanged += new System.EventHandler(this.Dropdownlist1_SelectedIndexChanged);
            this.btnSearch.Click += new System.EventHandler(this.btnSearch_Click);
            this.grdMeetings.ItemCommand += new System.Web.UI.WebControls.DataGridCommandEventHandler(this.grdMeetings_ItemCommand);
            this.grdMeetings.SortCommand += new System.Web.UI.WebControls.DataGridSortCommandEventHandler(this.grdMeetings_SortCommand);
            this.btnNew.Click += new System.EventHandler(this.btnNew_Click);
            this.Load += new System.EventHandler(this.Page_Load);

        }
		#endregion

		#region BindGrid

        private void BindGrid()
        {

            grdMeetings.DataSource = ProtokolDAL.LoadCollection("ProtokolsSelProc1", SQLParms.CreateProtokolsSelProc1(TimeHelper.GetDate(txtStartDate.Text),
                TimeHelper.GetDate(txtEndDate.Text), int.Parse(ddlProject.SelectedValue), (string)this.ViewState["Sort"], UIHelpers.ToInt(ddlProjectsStatus.SelectedValue), UIHelpers.ToInt(ddlBuildingTypes.SelectedValue)));
            grdMeetings.DataKeyField = "ProtokolID";
            grdMeetings.DataBind();
            if (!LoggedUser.HasPaymentRights)
                grdMeetings.Columns[(int)GridColumns.DeleteItem].Visible = false;
            try
            {
                SetConfirmDelete(grdMeetings, "btnDelete", 1);
            }
            catch
            {

            }
        }
		#endregion

        public string GetAccepted(object o)
        {
            if (o == DBNull.Value || o == null)
                return "";
            int nAccepted = (int)o;
            if (nAccepted == -1)
                return "";
            if (nAccepted == 0)
                return Resource.ResourceManager["Protocols_Accepted"];

            return Resource.ResourceManager["Protocols_Sent"];
        }
		public ProjectsData.ProjectsByStatus ProjectsStatus
		{
			get
			{
				try
				{
					return (ProjectsData.ProjectsByStatus)int.Parse(ddlProjectsStatus.SelectedValue);
				}
				catch { return ProjectsData.ProjectsByStatus.AllProjects; }
			}
		}
	
		
		#region Event Handlers
        private void btnNew_Click(object sender, System.EventArgs e)
        {
            Response.Redirect("EditProtokols.aspx");
        }

        private void btnSearch_Click(object sender, System.EventArgs e)
        {
            BindGrid();
        }
        private void ddlProjectsStatus_SelectedIndexChanged(object sender, System.EventArgs e)
        {
            LoadProjects();
        }

        private void Dropdownlist1_SelectedIndexChanged(object sender, System.EventArgs e)
        {
            LoadProjects();
        }
        private void grdMeetings_ItemCommand(object source, System.Web.UI.WebControls.DataGridCommandEventArgs e)
        {
            if (e.CommandName == "Edit")
            {
                string SID = ((DataGrid)source).DataKeys[e.Item.ItemIndex].ToString();
                Response.Redirect("EditProtokols.aspx" + "?id=" + SID);
                return;
            }
            if (!(e.CommandSource is ImageButton))
                return;
            ImageButton ib = (ImageButton)e.CommandSource;
            string s = ib.ID;
            switch (ib.ID)
            {
                case "btnDelete": int ID = (int)((DataGrid)source).DataKeys[e.Item.ItemIndex];
                    ProtokolDAL.Delete(ID);
                    log.Info(string.Format("Protocol {0} has been DELETED by {1}", ID, string.Concat(LoggedUser.FullName, " ( userID ", LoggedUser.UserID, " )")));
                    BindGrid();
                    break;


            }
        }
		#endregion

        private void grdMeetings_SortCommand(object source, System.Web.UI.WebControls.DataGridSortCommandEventArgs e)
        {
            ViewState.Add("Sort", Sorting.SetSort(grdMeetings, (string)ViewState["Sort"], e.SortExpression));

            BindGrid();
        }
	}
}
