using System;
using System.Data;
using System.Data.SqlClient; 
using System.Collections;
using Microsoft.ApplicationBlocks.Data;

namespace Asa.Timesheet.Data
{
	/// <summary>
	/// Summary description for FilePMDAL.
	/// </summary>
	public class FilePMDAL
	{
		/// <summary>
		/// Loads a FilePMData object from the database using the given FilePMID
		/// </summary>
		/// <param name="FilePMID">The primary key value</param>
		/// <returns>A FilePMData object</returns>
		public static FilePMData Load(int FileID)
		{
			SqlParameter[] parameterValues = new SqlParameter[] { new SqlParameter("@FileID", FileID) };
			using (SqlDataReader reader = SqlHelper.ExecuteReader(CommonDAL.ConnectionString, CommandType.StoredProcedure, "FilesPMSelProc", parameterValues))
			{
				if (reader.Read())
				{
					FilePMData result = new FilePMData();
					LoadFromReader(reader,result);
					return result;
				}
				else
					return null;
			}
		}

        /// <summary>
        /// Loads a FilePMData object from the database using the given FilePMID
        /// </summary>
        /// <param name="FilePMID">The primary key value</param>
        /// <returns>A FilePMData object</returns>
        public static FilePMData LoadLink(string link)
        {
            SqlParameter[] parameterValues = new SqlParameter[] { new SqlParameter("@Link", link) };
            using (SqlDataReader reader = SqlHelper.ExecuteReader(CommonDAL.ConnectionString, CommandType.StoredProcedure, "FilesPMSelLinkProc", parameterValues))
            {
                if (reader.Read())
                {
                    FilePMData result = new FilePMData();
                    LoadFromReaderLink(reader, result);
                    return result;
                }
                else
                    return null;
            }
        }

		/// <summary>
		/// Loads a FilePMData object from the database using the given where clause
		/// </summary>
		/// <param name="whereClause">The filter expression for the query</param>
		/// <returns>A FilePMData object</returns>
		public static FilePMData Load(string whereClause)
		{
			SqlParameter[] parameterValues = new SqlParameter[] { new SqlParameter("@WhereClause", whereClause) };
			using (SqlDataReader reader = SqlHelper.ExecuteReader(CommonDAL.ConnectionString, CommandType.StoredProcedure, "FilesPMListProc", parameterValues))
			{
				if (reader.Read())
				{
					FilePMData result = new FilePMData();
					LoadFromReader(reader,result);
					return result;
				}
				else
					return null;
			}
		}
		public static int Save( FilePMData source)
		{
			if (source.FileID == -1)
				return Insert(source);
			else
				return Update(source);	
		}

		public static void Delete( FilePMData source)
		{
			Delete(source.FileID);
		}
		/// <summary>
		/// Loads a collection of FilePMData objects from the database.
		/// </summary>
		/// <returns>A collection containing all of the FilePMData objects in the database.</returns>
		public static FilesPMVector LoadCollection()
		{
			SqlParameter[] parms = new SqlParameter[] {};
			return LoadCollection("FilesPMListProc", parms);
		}
		/// <summary>
		/// Loads a collection of FilePMData objects from the database.
		/// </summary>
		/// <returns>A collection containing all of the FilePMData objects in the database.</returns>
		public static FilesPMVector LoadCollectionByCategory(int CategoryID,int userType,int sortOrder)
		{
			SqlParameter[] parms = new SqlParameter[] {new SqlParameter("@CategoryID",CategoryID),new SqlParameter("@UserType",userType),new SqlParameter("@SortOrder",sortOrder)};
			return LoadCollection("FilesPMListProc", parms);
		}
		/// <summary>
		/// Saves a collection of FilePMData objects to the database.
		/// </summary>
		/// <returns>A collection containing all of the FilePMData objects in the database.</returns>
		public static  void  SaveCollection(FilesPMVector col,string spName, SqlParameter[] parms)
		{
			FilesPMVector colOld = LoadCollection(spName,parms);
			UpdateCollection(colOld,col);
		}
		public static  void  SaveCollection(FilesPMVector col)
		{
			FilesPMVector colOld = LoadCollection();
			UpdateCollection(colOld,col);
		}
		public static  void  SaveCollection(FilesPMVector col, string whereClause )
		{
			FilesPMVector colOld = LoadCollection(whereClause);
			UpdateCollection(colOld,col);
		}
		/// <summary>
		/// Loads a collection of FilePMData objects from the database.
		/// </summary>
		/// <returns>A collection containing all of the FilePMData objects in the database.</returns>
		public static FilesPMVector LoadCollection(string whereClause)
		{
			SqlParameter[] parms = new SqlParameter[] { new SqlParameter("@WhereClause", whereClause) };
			return LoadCollection("FilesPMListProc", parms);
		}

		/// <summary>
		/// Loads a collection of FilePMData objects from the database.
		/// </summary>
		/// <returns>A collection containing all of the FilePMData objects in the database.</returns>
		public static FilesPMVector LoadCollection(string spName, SqlParameter[] parms)
		{
			FilesPMVector result = new FilesPMVector();
			using (SqlDataReader reader = SqlHelper.ExecuteReader(CommonDAL.ConnectionString, CommandType.StoredProcedure, spName, parms))
			{
				while (reader.Read())
				{
					FilePMData tmp = new FilePMData();
					LoadFromReader(reader, tmp);
					result.Add(tmp);
				}
			}
			return result;
		}
		/// <summary>
		/// Deletes a FilePMData object from the database.
		/// </summary>
		/// <param name="FilePMID">The primary key value</param>
		public static void Delete(int FilePMID)
		{
			SqlParameter[] parameterValues = new SqlParameter[] { new SqlParameter("@FileID", FilePMID) };
			SqlHelper.ExecuteNonQuery(CommonDAL.ConnectionString, CommandType.StoredProcedure, "FilesPMDelProc", parameterValues);
		}

		public static void LoadFromReader(SqlDataReader reader, FilePMData target)
		{
			if (reader != null && !reader.IsClosed)
			{
				//target.Key = reader.GetInt32(0);
				if (!reader.IsDBNull(0)) target.FileID = reader.GetInt32(0);
				if (!reader.IsDBNull(1)) target.FileSubject = reader.GetString(1);
				if (!reader.IsDBNull(2)) target.FileMinutes = reader.GetString(2);
				if (!reader.IsDBNull(3)) target.FileVersion = reader.GetInt32(3);
				if (!reader.IsDBNull(4)) target.CreatedBy = reader.GetString(4);
				if (!reader.IsDBNull(5)) target.FileType = reader.GetString(5);
				if (!reader.IsDBNull(6)) target.DateCreated = reader.GetDateTime(6);
				if (!reader.IsDBNull(7)) target.MailForDownload= reader.GetBoolean(7);
				if (!reader.IsDBNull(8)) target.CatetegoryID = reader.GetInt32(8);
				if (!reader.IsDBNull(9)) target.FileNameSave = reader.GetString(9);
				if (!reader.IsDBNull(10)) target.IsClientVisible = reader.GetBoolean(10);
				if (!reader.IsDBNull(11)) target.IsSubcontractorVisible = reader.GetBoolean(11);
				if (!reader.IsDBNull(12)) target.IsBuilderVisible = reader.GetBoolean(12);
				if (!reader.IsDBNull(13)) target.CategoryName = reader.GetString(13);
				if (!reader.IsDBNull(14)) target.ProjectName = reader.GetString(14);
				if (!reader.IsDBNull(15)) target.IsNadzorVisible = reader.GetBoolean(15);
				if (!reader.IsDBNull(16)) target.IsOtherVisible = reader.GetBoolean(16);
			}
			
		}


        public static void LoadFromReaderLink(SqlDataReader reader, FilePMData target)
        {
            if (reader != null && !reader.IsClosed)
            {
                //target.Key = reader.GetInt32(0);
                if (!reader.IsDBNull(0)) target.FileID = reader.GetInt32(0);
                if (!reader.IsDBNull(1)) target.FileNameSave = reader.GetString(1);
                if (!reader.IsDBNull(2)) target.FileType = reader.GetString(2);
                if (!reader.IsDBNull(3)) target.CategoryName = reader.GetString(3);
                if (!reader.IsDBNull(4)) target.ProjectName = reader.GetString(4);
            }

        }
		
	  
		public static int Insert( FilePMData source)
		{
			SqlParameter[] parameterValues = GetInsertParameterValues(source);
			SqlHelper.ExecuteNonQuery(CommonDAL.ConnectionString, CommandType.StoredProcedure, "FilesPMInsProc", parameterValues);
			source.FileID = (int) parameterValues[0].Value;
			return source.FileID;
		}

		public static int Update( FilePMData source)
		{
			SqlParameter[] parameterValues = GetUpdateParameterValues(source);
			SqlHelper.ExecuteNonQuery(CommonDAL.ConnectionString, CommandType.StoredProcedure,
				"FilesPMUpdProc", parameterValues);
			return source.FileID;
		}
		private static void UpdateCollection(FilesPMVector colOld,FilesPMVector col)
		{
			// Delete all old items
			foreach( FilePMData itemOld in colOld)
			{
				if(!ExistsKey(itemOld.FileID, col))
					Delete(itemOld.FileID);
			}
			foreach( FilePMData item in col)
			{
				if(item.FileID <= -1)
					Insert(item);
				else
					Update(item);
			}
		}
		private static bool ExistsKey(int Key, FilesPMVector col)
		{
			foreach( FilePMData item in col)
				if(item.FileID==Key)
					return true;
			return false;
		}

		private static SqlParameter GetSqlParameter(string name, ParameterDirection direction, object value)
		{
			SqlParameter p = new SqlParameter(name, value);
			p.Direction = direction;
			return p;
		}

		private static SqlParameter[] GetInsertParameterValues( FilePMData source)
		{
			SqlParameter[] prams = new SqlParameter[16];
			prams[0] = GetSqlParameter("@FileID", ParameterDirection.Output, source.FileID);
			prams[1] = GetSqlParameter("@FileSubject", ParameterDirection.Input, source.FileSubject);
			prams[2] = GetSqlParameter("@FileMinutes", ParameterDirection.Input, source.FileMinutes);
			prams[3] = GetSqlParameter("@FileVersion", ParameterDirection.Input, source.FileVersion );
			prams[4] = GetSqlParameter("@CreatedBy", ParameterDirection.Input, source.CreatedBy);
			prams[5] = GetSqlParameter("@FileType", ParameterDirection.Input, source.FileType );
			prams[6] = GetSqlParameter("@DateCreated", ParameterDirection.Input, source.DateCreated);
			prams[7] = GetSqlParameter("@MailForDownload", ParameterDirection.Input, source.MailForDownload );
			prams[8] = GetSqlParameter("@CategoryID", ParameterDirection.Input, source.CatetegoryID);
			prams[9] = GetSqlParameter("@FileNameSave", ParameterDirection.Input, source.FileNameSave);
			prams[10] = GetSqlParameter("@IsClientVisible", ParameterDirection.Input, source.IsClientVisible );
			prams[11] = GetSqlParameter("@IsSubcontractorVisible", ParameterDirection.Input, source.IsSubcontractorVisible);
			prams[12] = GetSqlParameter("@IsBuilderVisible", ParameterDirection.Input, source.IsBuilderVisible);
			prams[13] = GetSqlParameter("@IsNadzorVisible", ParameterDirection.Input, source.IsNadzorVisible);
			prams[14] = GetSqlParameter("@IsOtherVisible", ParameterDirection.Input, source.IsOtherVisible);
            prams[15] = GetSqlParameter("@Link", ParameterDirection.Input, source.Link);
			return prams;
		}

		private static SqlParameter[] GetUpdateParameterValues( FilePMData source)
		{
			return new SqlParameter[] {
										  new SqlParameter("@FileID", source.FileID),
										  new SqlParameter("@FileSubject", source.FileSubject),
										  new SqlParameter("@FileMinutes", source.FileMinutes),
										  new SqlParameter("@FileVersion", source.FileVersion),
										  new SqlParameter("@CreatedBy", source.CreatedBy),
										  new SqlParameter("@FileType", source.FileType),
										  new SqlParameter("@DateCreated", source.DateCreated),
										  new SqlParameter("@MailForDownload", source.MailForDownload),
										  new SqlParameter("@CategoryID", source.CatetegoryID),
				 new SqlParameter("@FileNameSave", source.FileNameSave),
										  new SqlParameter("@IsClientVisible", source.IsClientVisible),
										  new SqlParameter("@IsSubcontractorVisible", source.IsSubcontractorVisible),
										  new SqlParameter("@IsBuilderVisible", source.IsBuilderVisible),
				new SqlParameter("@IsNadzorVisible", source.IsNadzorVisible),
				new SqlParameter("@IsOtherVisible", source.IsOtherVisible),
			};
		}
	}
}
