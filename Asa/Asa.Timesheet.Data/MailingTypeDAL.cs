using System;
using System.Data;
using System.Data.SqlClient; 
using System.Collections;
using Microsoft.ApplicationBlocks.Data;

namespace Asa.Timesheet.Data
{
	/// <summary>
	/// Summary description for MailingTypeDAL.
	/// </summary>
	
	public class MailingTypeDAL
	{
		/// <summary>
		/// Loads a MailingTypeData object from the database using the given MailingTypeID
		/// </summary>
		/// <param name="MailingTypeID">The primary key value</param>
		/// <returns>A MailingTypeData object</returns>
		public static MailingTypeData Load(int MailingTypeID)
		{
			SqlParameter[] parameterValues = new SqlParameter[] { new SqlParameter("@MailingTypeID", MailingTypeID) };
			using (SqlDataReader reader = SqlHelper.ExecuteReader(CommonDAL.ConnectionString, CommandType.StoredProcedure, "MailingTypesSelProc", parameterValues))
			{
				if (reader.Read())
				{
					MailingTypeData result = new MailingTypeData();
					LoadFromReader(reader,result);
					return result;
				}
				else
					return null;
			}
		}

		/// <summary>
		/// Loads a MailingTypeData object from the database using the given where clause
		/// </summary>
		/// <param name="whereClause">The filter expression for the query</param>
		/// <returns>A MailingTypeData object</returns>
		public static MailingTypeData Load(string whereClause)
		{
			SqlParameter[] parameterValues = new SqlParameter[] { new SqlParameter("@WhereClause", whereClause) };
			using (SqlDataReader reader = SqlHelper.ExecuteReader(CommonDAL.ConnectionString, CommandType.StoredProcedure, "MailingTypesListProc", parameterValues))
			{
				if (reader.Read())
				{
					MailingTypeData result = new MailingTypeData();
					LoadFromReader(reader,result);
					return result;
				}
				else
					return null;
			}
		}
		public static int Save( MailingTypeData source)
		{
			if (source.MailingTypeID == -1)
				return Insert(source);
			else
				return Update(source);	
		}

		public static void Delete( MailingTypeData source)
		{
			Delete(source.MailingTypeID);
		}
		/// <summary>
		/// Loads a collection of MailingTypeData objects from the database.
		/// </summary>
		/// <returns>A collection containing all of the MailingTypeData objects in the database.</returns>
		public static MailingTypesVector LoadCollection()
		{
			SqlParameter[] parms = new SqlParameter[] {};
			return LoadCollection("MailingTypesListProc", parms);
		}
	  
		/// <summary>
		/// Saves a collection of MailingTypeData objects to the database.
		/// </summary>
		/// <returns>A collection containing all of the MailingTypeData objects in the database.</returns>
		public static  void  SaveCollection(MailingTypesVector col,string spName, SqlParameter[] parms)
		{
			MailingTypesVector colOld = LoadCollection(spName,parms);
			UpdateCollection(colOld,col);
		}
		public static  void  SaveCollection(MailingTypesVector col)
		{
			MailingTypesVector colOld = LoadCollection();
			UpdateCollection(colOld,col);
		}
		public static  void  SaveCollection(MailingTypesVector col, string whereClause )
		{
			MailingTypesVector colOld = LoadCollection(whereClause);
			UpdateCollection(colOld,col);
		}
		/// <summary>
		/// Loads a collection of MailingTypeData objects from the database.
		/// </summary>
		/// <returns>A collection containing all of the MailingTypeData objects in the database.</returns>
		public static MailingTypesVector LoadCollection(string whereClause)
		{
			SqlParameter[] parms = new SqlParameter[] { new SqlParameter("@WhereClause", whereClause) };
			return LoadCollection("MailingTypesListProc", parms);
		}

		/// <summary>
		/// Loads a collection of MailingTypeData objects from the database.
		/// </summary>
		/// <returns>A collection containing all of the MailingTypeData objects in the database.</returns>
		public static MailingTypesVector LoadCollection(string spName, SqlParameter[] parms)
		{
			MailingTypesVector result = new MailingTypesVector();
			using (SqlDataReader reader = SqlHelper.ExecuteReader(CommonDAL.ConnectionString, CommandType.StoredProcedure, spName, parms))
			{
				while (reader.Read())
				{
					MailingTypeData tmp = new MailingTypeData();
					LoadFromReader(reader, tmp);
					result.Add(tmp);
				}
			}
			return result;
		}
		/// <summary>
		/// Deletes a MailingTypeData object from the database.
		/// </summary>
		/// <param name="MailingTypeID">The primary key value</param>
		public static void Delete(int MailingTypeID)
		{
			SqlParameter[] parameterValues = new SqlParameter[] { new SqlParameter("@MailingTypeID", MailingTypeID) };
			SqlHelper.ExecuteNonQuery(CommonDAL.ConnectionString, CommandType.StoredProcedure, "MailingTypesDelProc", parameterValues);
		}

		public static void LoadFromReader(SqlDataReader reader, MailingTypeData target)
		{
			if (reader != null && !reader.IsClosed)
			{
				//target.Key = reader.GetInt32(0);
				if (!reader.IsDBNull(0)) target.MailingTypeID = reader.GetInt32(0);
				if (!reader.IsDBNull(1)) target.MailingTypeName = reader.GetString(1);
			}
		}
		
	  
		public static int Insert( MailingTypeData source)
		{
			SqlParameter[] parameterValues = GetInsertParameterValues(source);
			SqlHelper.ExecuteNonQuery(CommonDAL.ConnectionString, CommandType.StoredProcedure, "MailingTypesInsProc", parameterValues);
			source.MailingTypeID = (int) parameterValues[0].Value;
			return source.MailingTypeID;
		}

		public static int Update( MailingTypeData source)
		{
			SqlParameter[] parameterValues = GetUpdateParameterValues(source);
			SqlHelper.ExecuteNonQuery(CommonDAL.ConnectionString, CommandType.StoredProcedure,
				"MailingTypesUpdProc", parameterValues);
			return source.MailingTypeID;
		}
		private static void UpdateCollection(MailingTypesVector colOld,MailingTypesVector col)
		{
			// Delete all old items
			foreach( MailingTypeData itemOld in colOld)
			{
				if(!ExistsKey(itemOld.MailingTypeID, col))
					Delete(itemOld.MailingTypeID);
			}
			foreach( MailingTypeData item in col)
			{
				if(item.MailingTypeID <= -1)
					Insert(item);
				else
					Update(item);
			}
		}
		private static bool ExistsKey(int Key, MailingTypesVector col)
		{
			foreach( MailingTypeData item in col)
				if(item.MailingTypeID==Key)
					return true;
			return false;
		}

		private static SqlParameter GetSqlParameter(string name, ParameterDirection direction, object value)
		{
			SqlParameter p = new SqlParameter(name, value);
			p.Direction = direction;
			return p;
		}

		private static SqlParameter[] GetInsertParameterValues( MailingTypeData source)
		{
			SqlParameter[] prams = new SqlParameter[2];
			prams[0] = GetSqlParameter("@MailingTypeID", ParameterDirection.Output, source.MailingTypeID);
			prams[1] = GetSqlParameter("@MailingTypeName", ParameterDirection.Input, source.MailingTypeName);
			return prams;
		}

		private static SqlParameter[] GetUpdateParameterValues( MailingTypeData source)
		{
			return new SqlParameter[] {
										  new SqlParameter("@MailingTypeID", source.MailingTypeID),
										  new SqlParameter("@MailingTypeName", source.MailingTypeName)
									  };
		}
	}
}
