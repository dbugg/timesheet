﻿<%@ Register TagPrefix="uc1" TagName="PageHeader" Src="UserControls/PageHeader.ascx" %>
<%@ Page language="c#" Codebehind="EditSubContracter.aspx.cs" AutoEventWireup="false" Inherits="Asa.Timesheet.WebPages.EditSubContracter" %>
<%@ Register TagPrefix="uc1" TagName="EditForm" Src="UserControls/EditForm.ascx" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
	<head runat="server">
		<title>Subcontracter</title>
		<script language="javascript" src="PopupCalendar.js"></script>
		<meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
		<meta content="C#" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<LINK href="styles/timesheet.css" type="text/css" rel="stylesheet">
	</HEAD>
	<body bottomMargin="0" leftMargin="0" topMargin="0" rightMargin="0" onload="document.getElementById('txtAccountPass').value=document.getElementById('hdnAccountPass').value;">
		<form id="Form1" method="post" runat="server">
			<table height="100%" cellSpacing="0" cellPadding="0" width="100%" border="0">
				<tr height="1">
					<td><uc1:pageheader id="header" runat="server"></uc1:pageheader>
						<uc1:EditForm id="editCtrl" runat="server"></uc1:EditForm></td>
				</tr>
				<tr>
					<td>
						<table height="100%" cellSpacing="2" cellPadding="2" width="800" border="0">
							<tr>
								<td class="menuLeft"><asp:placeholder id="menuHolder" runat="server"></asp:placeholder></td>
								
								<td style="PADDING-LEFT: 5px; PADDING-TOP: 3px" vAlign="top" noWrap>
									<table width="600">
										<TR>
											<td style="HEIGHT: 5px" colSpan="2"></TD>
										</TR>
										<TR>
											<td colSpan="2"><IMG height="1" src="images/dot.gif" width="100%"></TD>
										</TR>
										<TR>
											<td colSpan="2" height="3"></td>
										</TR>
										<TR>
											<td>
												<TABLE id="Table6" cellSpacing="0" cellPadding="0" border="0">
													<TR>
														<td style="WIDTH: 97px" noWrap>
															<asp:button id="btnSaveUP" runat="server" CssClass="ActionButton" Text="Запиши"></asp:button>
															<asp:button id="btnEditUP" runat="server" CssClass="ActionButton" Text="Редактирай" style="DISPLAY:none"></asp:button>
														</TD>
														<td style="WIDTH: 321px" noWrap><asp:button id="btnCancelUP" runat="server" CssClass="ActionButton" Text="Откажи"></asp:button></TD>
														<td></TD>
													</TR>
												</TABLE>
											</TD>
										</TR>
										<TR>
											<td style="WIDTH: 171px" height="3"></TD>
										</TR>
										<TR>
											<td colSpan="2"><IMG height="1" src="images/dot.gif" width="100%"></TD>
										</TR>
									</table>
									<TABLE id="Table3" cellSpacing="0" width="880" border="0">
										<TBODY>
											<TR>
												<td style="WIDTH: 4px"></TD>
												<td>
													<TABLE id="tblForm" cellSpacing="0" cellPadding="3" width="800" border="0">
														<TBODY>
															<TR>
																<td style="WIDTH: 180px" colSpan="1" rowSpan="1"><asp:label id="lblName" runat="server" CssClass="enterDataLabel" Font-Bold="True" EnableViewState="False">Име на подизпълнител:</asp:label></TD>
																<td>
																	<asp:textbox id="txtName" runat="server" CssClass="enterDataBox" Width="336px"></asp:textbox>&nbsp;<IMG height="16" alt="" src="images/required1.gif" width="16"></TD>
															</TR>
															<TR>
																<td style="WIDTH: 180px">
																	<asp:label id="Label17" runat="server" CssClass="enterDataLabel" Width="100%">Име(английски):</asp:label></TD>
																<td>
																	<asp:textbox id="txtNameEN" runat="server" CssClass="enterDataBox" Width="336px"></asp:textbox></TD>
															</TR>
															<TR>
																<td style="WIDTH: 180px" colSpan="1" rowSpan="1"><asp:label id="lblProjectCode" runat="server" CssClass="enterDataLabel" Font-Bold="True">Специалност:</asp:label></TD>
																<td><asp:dropdownlist id="ddlTypes" runat="server" CssClass="enterDataBox" Width="336px" AutoPostBack="True"></asp:dropdownlist>&nbsp;<IMG height="16" alt="" src="images/required1.gif" width="16"></TD>
															</TR>
											</TR>
											<TR>
												<td style="WIDTH: 180px"><asp:label id="Label5" runat="server" CssClass="enterDataLabel" Width="100%">Фирмено дело:</asp:label></TD>
												<td><asp:textbox id="txtDelo" runat="server" CssClass="enterDataBox" Width="336px" TextMode="MultiLine"></asp:textbox></TD>
											</TR>
											<TR>
												<td style="WIDTH: 180px"><asp:label id="Label6" runat="server" CssClass="enterDataLabel" Width="100%">Булстат:</asp:label></TD>
												<td><asp:textbox id="txtBulstat" runat="server" CssClass="enterDataBox"></asp:textbox></TD>
											</TR>
											<TR>
												<td style="WIDTH: 180px"><asp:label id="Label7" runat="server" CssClass="enterDataLabel" Width="100%">Номер по НДР:</asp:label></TD>
												<td><asp:textbox id="txtNDR" runat="server" CssClass="enterDataBox"></asp:textbox></TD>
											</TR>
											<TR>
												<td style="WIDTH: 180px"><asp:label id="lblAddress" runat="server" CssClass="enterDataLabel">Адрес на управление:</asp:label></TD>
												<td><asp:textbox id="txtAddress" runat="server" CssClass="enterDataBox" Width="336px"></asp:textbox></TD>
											</TR>
											<TR>
												<td style="WIDTH: 180px"><asp:label id="Label1" runat="server" CssClass="enterDataLabel">Представлявано от:</asp:label></TD>
												<td><asp:textbox id="txtManager" runat="server" CssClass="enterDataBox" Width="336px"></asp:textbox></TD>
											</TR>
											<TR>
												<td style="WIDTH: 180px"><asp:label id="Label2" runat="server" CssClass="enterDataLabel">Телефон:</asp:label></TD>
												<td><asp:textbox id="txtPhone" runat="server" CssClass="enterDataBox" Width="336px"></asp:textbox></TD>
											</TR>
											<TR>
												<td style="WIDTH: 180px"><asp:label id="Label8" runat="server" CssClass="enterDataLabel">Факс:</asp:label></TD>
												<td><asp:textbox id="txtFax" runat="server" CssClass="enterDataBox" Width="336px"></asp:textbox></TD>
											</TR>
											<TR>
												<td style="WIDTH: 180px"><asp:label id="lblEmail" runat="server" CssClass="enterDataLabel">Е-мейл:</asp:label></TD>
												<td><asp:textbox id="txtEmail" runat="server" CssClass="enterDataBox" Width="336px"></asp:textbox></TD>
											</TR>
											<TR>
												<td style="WIDTH: 180px">
													<asp:label id="Label15" runat="server" CssClass="enterDataLabel">Уебсайт:</asp:label></TD>
												<td>
													<asp:textbox id="txtWebsite" runat="server" CssClass="enterDataBox" Width="336px"></asp:textbox></TD>
											</TR>
											<TR>
												<td style="WIDTH: 180px">
													<asp:label id="Label16" runat="server" CssClass="enterDataLabel">Скрий:</asp:label></TD>
												<td>
													<asp:CheckBox id="cbHide" runat="server"></asp:CheckBox></TD>
											</TR>
											<TR>
												<td style="WIDTH: 180px"></TD>
												<td><asp:label id="Label9" runat="server" CssClass="enterDataBox"> Представител 1:</asp:label></TD>
											</TR>
											<TR>
												<td style="WIDTH: 180px"><asp:label id="Label3" runat="server" CssClass="enterDataLabel">Име:</asp:label></TD>
												<td><asp:textbox id="txtRepr1" runat="server" CssClass="enterDataBox" Width="336px"></asp:textbox></TD>
											</TR>
											<TR>
												<td style="WIDTH: 180px"><asp:label id="Label10" runat="server" CssClass="enterDataLabel">Моб. телефон:</asp:label></TD>
												<td><asp:textbox id="txtPhone1" runat="server" CssClass="enterDataBox" Width="336px"></asp:textbox></TD>
											</TR>
											<TR>
												<td style="WIDTH: 180px">
													<asp:label id="Label11" runat="server" CssClass="enterDataLabel">Е-мейл:</asp:label></TD>
												<td>
													<asp:textbox id="txtEmail1" runat="server" CssClass="enterDataBox" Width="336px"></asp:textbox></TD>
											</TR>
											<TR>
												<td style="WIDTH: 180px">
													<asp:label id="Label18" runat="server" CssClass="enterDataLabel">Име(английски):</asp:label></TD>
												<td>
													<asp:textbox id="txtReprEN" runat="server" CssClass="enterDataBox" Width="336px"></asp:textbox></TD>
											</TR>
											<TR>
												<td style="WIDTH: 180px"></TD>
												<td>
													<asp:label id="Label12" runat="server" CssClass="enterDataBox"> Представител 2:</asp:label></TD>
											</TR>
											<TR>
												<td style="WIDTH: 180px"><asp:label id="Label4" runat="server" CssClass="enterDataLabel"> Име:</asp:label></TD>
												<td><asp:textbox id="txtRepr2" runat="server" CssClass="enterDataBox" Width="336px"></asp:textbox></TD>
											</TR>
											<TR>
												<td style="WIDTH: 180px">
													<asp:label id="Label13" runat="server" CssClass="enterDataLabel">Моб. телефон:</asp:label></TD>
												<td>
													<asp:textbox id="txtPhone2" runat="server" CssClass="enterDataBox" Width="336px"></asp:textbox></TD>
											</TR>
											<TR>
												<td style="WIDTH: 180px">
													<asp:label id="Label14" runat="server" CssClass="enterDataLabel">Е-мейл:</asp:label></TD>
												<td>
													<asp:textbox id="txtEmail2" runat="server" CssClass="enterDataBox" Width="336px"></asp:textbox></TD>
											</TR>
											<TR>
												<td style="WIDTH: 180px">
													<asp:label id="lbAccountName" runat="server" CssClass="enterDataLabel">Потребителско име:</asp:label></TD>
												<td>
													<asp:textbox id="txtAccountName" runat="server" CssClass="enterDataBox" Width="336px"></asp:textbox></TD>
											</TR>
											<TR>
												<td style="WIDTH: 180px">
													<asp:label id="lbAccountPass" runat="server" CssClass="enterDataLabel">Парола:</asp:label></TD>
												<td>
													<asp:textbox id="txtAccountPass" runat="server" CssClass="enterDataBox" Width="336px" TextMode="Password"></asp:textbox>&nbsp;
													<asp:button id="btnAutoGenerate" EnableViewState="False" runat="server" CssClass="ActionButton"
														Text="Генериране"></asp:button>
													<asp:textbox id="txtHdnAccountPass" runat="server" Visible="False"></asp:textbox><INPUT id="hdnAccountPass" type="hidden" runat="server" NAME="hdnPassword"></TD>
											</TR>
										</TBODY>
									</TABLE>
								</td>
							</tr>
						</table>
						<table width="600">
							<TR>
								<td style="HEIGHT: 5px" colSpan="2"></TD>
							</TR>
							<TR>
								<td colSpan="2"><IMG height="1" src="images/dot.gif" width="100%"></TD>
							</TR>
							<TR>
								<td colSpan="2" height="3"></td>
							</TR>
							<TR>
								<td>
									<TABLE id="Table5" cellSpacing="0" cellPadding="0" border="0">
										<TR>
											<td style="WIDTH: 97px" noWrap>
												<asp:button id="btnSave" runat="server" CssClass="ActionButton" Text="Запиши"></asp:button>
												<asp:button id="btnEdit" runat="server" CssClass="ActionButton" Text="Редактирай" style="DISPLAY:none"></asp:button>
											</TD>
											<td style="WIDTH: 321px" noWrap><asp:button id="btnCancel" runat="server" CssClass="ActionButton" Text="Откажи"></asp:button></TD>
											<td><asp:button id="btnDelete" runat="server" CssClass="ActionButton" Text="Изтрий"></asp:button></TD>
										</TR>
									</TABLE>
								</TD>
							</TR>
							<TR>
								<td style="WIDTH: 171px" height="3"></TD>
							</TR>
							<TR>
								<td colSpan="2"><IMG height="1" src="images/dot.gif" width="100%"></TD>
							</TR>
						</table>
						&nbsp;
						<asp:label id="lblInfo" runat="server" EnableViewState="False" CssClass="InfoLabel"></asp:label><asp:label id="lblError" runat="server" EnableViewState="False" CssClass="ErrorLabel" ForeColor="Red"></asp:label></td>
				</tr>
			</table>
			</TD></TR></TBODY></TABLE></form>
	</body>
</HTML>
