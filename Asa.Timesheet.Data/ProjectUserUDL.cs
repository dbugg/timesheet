using System;
using System.Data;
using System.Collections;
using System.Data.SqlClient;

namespace Asa.Timesheet.Data
{
	/// <summary>
	/// Summary description for ProjectUserUDL.
	/// </summary>
	public class ProjectUserUDL
	{
		private static SqlCommand CreateProjectUsersDelByProjectProc (int ProjectID)
		{
			SqlCommand cmd = new SqlCommand();
			cmd.CommandType = System.Data.CommandType.StoredProcedure;
			cmd.CommandText = "ProjectUsersDelByProjectProc";
			cmd.Parameters.Add(new System.Data.SqlClient.SqlParameter("@ProjectID", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.Input, true, 10, 0, "ProjectID", DataRowVersion.Current, null));
			if(ProjectID>0)
				cmd.Parameters["@ProjectID"].Value = ProjectID;

			return cmd;
		}

		public static int ExecuteProjectUsersDelByProjectProc(int ProjectID)
		{
			int RetObj;
			System.Data.SqlClient.SqlCommand cmd;
			cmd = CreateProjectUsersDelByProjectProc(ProjectID);
			SqlConnection cnDAL = new SqlConnection();

			try
			{
				cnDAL =  CommonDAL.OpenConnection();
				cmd.Connection = cnDAL;
				RetObj = cmd.ExecuteNonQuery();
				return RetObj;
		
			}
			catch(SqlException ex)
			{

				throw new ApplicationException("ProjectUsersDelByProjectProc",ex);
			}

			finally
			{
				CommonDAL.CloseConnection(cnDAL);
			}
		}
		
	}
}
