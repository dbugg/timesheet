using System;
using GrapeCity.ActiveReports;
using GrapeCity.ActiveReports.Controls;
using GrapeCity.ActiveReports.SectionReportModel;
using GrapeCity.ActiveReports.Document.Section;
using GrapeCity.ActiveReports.Document;

namespace Asa.Timesheet.WebPages.Reports
{
    public class DDSRpt : GrapeCity.ActiveReports.SectionReport
	{
        public DDSRpt(bool pdf, string title, string type, string project, string client)
        {
            _forPDF = pdf;
            InitializeComponent();
            lbTitle.Text = title;
            lbZad.Text = type;

            if (project.Length > 0 && !project.StartsWith("<"))
                lbProject.Text = project;
            else
                Label5.Visible = false;
            if (client.Length > 0 && !client.StartsWith("<"))
                lbClient.Text = client;
            else
                Label6.Visible = false;
            this.ReportEnd += new EventHandler(SubAnalysisRpt_ReportEnd);

        }
		private bool _forPDF=true;
		#region ActiveReports Designer generated code






































        public void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(DDSRpt));
            this.Detail = new GrapeCity.ActiveReports.SectionReportModel.Detail();
            this.ReportHeader = new GrapeCity.ActiveReports.SectionReportModel.ReportHeader();
            this.ReportFooter = new GrapeCity.ActiveReports.SectionReportModel.ReportFooter();
            this.PageHeader = new GrapeCity.ActiveReports.SectionReportModel.PageHeader();
            this.PageFooter = new GrapeCity.ActiveReports.SectionReportModel.PageFooter();
            this.GroupHeader1 = new GrapeCity.ActiveReports.SectionReportModel.GroupHeader();
            this.GroupFooter1 = new GrapeCity.ActiveReports.SectionReportModel.GroupFooter();
            this.lbTitle = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label4 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label5 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label6 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lbZad = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lbProject = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lbClient = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label1 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label2 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label3 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Line1 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.Label9 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label10 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label11 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label12 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.TextBox1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.TextBox2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.TextBox3 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.TextBox4 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.TextBox5 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.TextBox9 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.TextBox10 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.TextBox6 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.Label8 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.TextBox8 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.Line2 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.TextBox11 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.TextBox7 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.Label7 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Line3 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.TextBox12 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            ((System.ComponentModel.ISupportInitialize)(this.lbTitle)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbZad)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbProject)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbClient)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label11)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label12)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TextBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TextBox2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TextBox3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TextBox4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TextBox5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TextBox9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TextBox10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TextBox6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TextBox8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TextBox11)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TextBox7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TextBox12)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            // 
            // Detail
            // 
            this.Detail.ColumnSpacing = 0F;
            this.Detail.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
						this.TextBox1,
						this.TextBox2,
						this.TextBox3,
						this.TextBox4,
						this.TextBox5,
						this.TextBox9,
						this.TextBox10});
            this.Detail.Height = 0.2291667F;
            this.Detail.Name = "Detail";
            // 
            // ReportHeader
            // 
            this.ReportHeader.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
						this.lbTitle,
						this.Label4,
						this.Label5,
						this.Label6,
						this.lbZad,
						this.lbProject,
						this.lbClient});
            this.ReportHeader.Height = 1.259722F;
            this.ReportHeader.Name = "ReportHeader";
            // 
            // ReportFooter
            // 
            this.ReportFooter.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
						this.TextBox7,
						this.Label7,
						this.Line3,
						this.TextBox12});
            this.ReportFooter.Height = 0.5194445F;
            this.ReportFooter.Name = "ReportFooter";
            // 
            // PageHeader
            // 
            this.PageHeader.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
						this.Label1,
						this.Label2,
						this.Label3,
						this.Line1,
						this.Label9,
						this.Label10,
						this.Label11,
						this.Label12});
            this.PageHeader.Height = 0.3645833F;
            this.PageHeader.Name = "PageHeader";
            // 
            // PageFooter
            // 
            this.PageFooter.Height = 0.02083333F;
            this.PageFooter.Name = "PageFooter";
            // 
            // GroupHeader1
            // 
            this.GroupHeader1.DataField = "projectname";
            this.GroupHeader1.Height = 0F;
            this.GroupHeader1.Name = "GroupHeader1";
            // 
            // GroupFooter1
            // 
            this.GroupFooter1.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
						this.TextBox6,
						this.Label8,
						this.TextBox8,
						this.Line2,
						this.TextBox11});
            this.GroupFooter1.Height = 0.71875F;
            this.GroupFooter1.Name = "GroupFooter1";
            // 
            // lbTitle
            // 
            this.lbTitle.Height = 0.2F;
            this.lbTitle.HyperLink = null;
            this.lbTitle.Left = 1F;
            this.lbTitle.Name = "lbTitle";
            this.lbTitle.Style = "font-size: 12pt; font-weight: bold; text-align: center";
            this.lbTitle.Text = "Label4";
            this.lbTitle.Top = 0F;
            this.lbTitle.Width = 7.5625F;
            // 
            // Label4
            // 
            this.Label4.Height = 0.2F;
            this.Label4.HyperLink = null;
            this.Label4.Left = 1F;
            this.Label4.Name = "Label4";
            this.Label4.Style = "font-weight: bold";
            this.Label4.Text = "??????:";
            this.Label4.Top = 0.4375F;
            this.Label4.Width = 1.5F;
            // 
            // Label5
            // 
            this.Label5.Height = 0.2F;
            this.Label5.HyperLink = null;
            this.Label5.Left = 1F;
            this.Label5.Name = "Label5";
            this.Label5.Style = "font-weight: bold";
            this.Label5.Text = "??????:";
            this.Label5.Top = 0.625F;
            this.Label5.Width = 1.5F;
            // 
            // Label6
            // 
            this.Label6.Height = 0.2000001F;
            this.Label6.HyperLink = null;
            this.Label6.Left = 1F;
            this.Label6.Name = "Label6";
            this.Label6.Style = "font-weight: bold";
            this.Label6.Text = "??????:";
            this.Label6.Top = 0.8125F;
            this.Label6.Width = 1.5F;
            // 
            // lbZad
            // 
            this.lbZad.Height = 0.2F;
            this.lbZad.HyperLink = null;
            this.lbZad.Left = 2.5F;
            this.lbZad.Name = "lbZad";
            this.lbZad.Style = "";
            this.lbZad.Text = "";
            this.lbZad.Top = 0.4375F;
            this.lbZad.Width = 6.0625F;
            // 
            // lbProject
            // 
            this.lbProject.Height = 0.2000001F;
            this.lbProject.HyperLink = null;
            this.lbProject.Left = 2.5F;
            this.lbProject.Name = "lbProject";
            this.lbProject.Style = "";
            this.lbProject.Text = "";
            this.lbProject.Top = 0.625F;
            this.lbProject.Width = 6.0625F;
            // 
            // lbClient
            // 
            this.lbClient.Height = 0.2F;
            this.lbClient.HyperLink = null;
            this.lbClient.Left = 2.5F;
            this.lbClient.Name = "lbClient";
            this.lbClient.Style = "";
            this.lbClient.Text = "";
            this.lbClient.Top = 0.8125F;
            this.lbClient.Width = 6.0625F;
            // 
            // Label1
            // 
            this.Label1.Height = 0.2F;
            this.Label1.HyperLink = null;
            this.Label1.Left = 1F;
            this.Label1.Name = "Label1";
            this.Label1.Style = "font-weight: bold";
            this.Label1.Text = "??????";
            this.Label1.Top = 0.0625F;
            this.Label1.Width = 2.125F;
            // 
            // Label2
            // 
            this.Label2.Height = 0.2F;
            this.Label2.HyperLink = null;
            this.Label2.Left = 3.0625F;
            this.Label2.Name = "Label2";
            this.Label2.Style = "font-weight: bold";
            this.Label2.Text = "????";
            this.Label2.Top = 0.0625F;
            this.Label2.Width = 2.5625F;
            // 
            // Label3
            // 
            this.Label3.Height = 0.2F;
            this.Label3.HyperLink = null;
            this.Label3.Left = 5.625F;
            this.Label3.Name = "Label3";
            this.Label3.Style = "font-weight: bold; text-align: right";
            this.Label3.Text = "???o  (EUR)";
            this.Label3.Top = 0.0625F;
            this.Label3.Width = 0.9375F;
            // 
            // Line1
            // 
            this.Line1.Height = 0F;
            this.Line1.Left = 1F;
            this.Line1.LineWeight = 1F;
            this.Line1.Name = "Line1";
            this.Line1.Top = 0.3125F;
            this.Line1.Width = 9.1875F;
            this.Line1.X1 = 1F;
            this.Line1.X2 = 10.1875F;
            this.Line1.Y1 = 0.3125F;
            this.Line1.Y2 = 0.3125F;
            // 
            // Label9
            // 
            this.Label9.Height = 0.2F;
            this.Label9.HyperLink = null;
            this.Label9.Left = 7.5F;
            this.Label9.Name = "Label9";
            this.Label9.Style = "font-weight: bold; text-align: right";
            this.Label9.Text = "???? ???????";
            this.Label9.Top = 0.0625F;
            this.Label9.Width = 1.0625F;
            // 
            // Label10
            // 
            this.Label10.Height = 0.2F;
            this.Label10.HyperLink = null;
            this.Label10.Left = 9.3125F;
            this.Label10.Name = "Label10";
            this.Label10.Style = "font-weight: bold; text-align: right";
            this.Label10.Text = "???????";
            this.Label10.Top = 0.0625F;
            this.Label10.Width = 0.8125F;
            // 
            // Label11
            // 
            this.Label11.Height = 0.2F;
            this.Label11.HyperLink = null;
            this.Label11.Left = 8.5625F;
            this.Label11.Name = "Label11";
            this.Label11.Style = "font-weight: bold; text-align: right";
            this.Label11.Text = "???????";
            this.Label11.Top = 0.0625F;
            this.Label11.Width = 0.75F;
            // 
            // Label12
            // 
            this.Label12.Height = 0.2F;
            this.Label12.HyperLink = null;
            this.Label12.Left = 6.5625F;
            this.Label12.Name = "Label12";
            this.Label12.Style = "font-weight: bold; text-align: right";
            this.Label12.Text = "???o  (??.)";
            this.Label12.Top = 0.0625F;
            this.Label12.Width = 0.9375F;
            // 
            // TextBox1
            // 
            this.TextBox1.DataField = "projectname";
            this.TextBox1.Height = 0.2F;
            this.TextBox1.Left = 1F;
            this.TextBox1.Name = "TextBox1";
            this.TextBox1.Text = "TextBox1";
            this.TextBox1.Top = 0F;
            this.TextBox1.Width = 2.125F;
            // 
            // TextBox2
            // 
            this.TextBox2.DataField = "SubprojectType";
            this.TextBox2.Height = 0.2F;
            this.TextBox2.Left = 3.114583F;
            this.TextBox2.Name = "TextBox2";
            this.TextBox2.Text = "TextBox2";
            this.TextBox2.Top = 0F;
            this.TextBox2.Width = 2.510417F;
            // 
            // TextBox3
            // 
            this.TextBox3.DataField = "Amount";
            this.TextBox3.Height = 0.2F;
            this.TextBox3.Left = 5.635417F;
            this.TextBox3.Name = "TextBox3";
            this.TextBox3.OutputFormat = resources.GetString("TextBox3.OutputFormat");
            this.TextBox3.Style = "text-align: right";
            this.TextBox3.Text = "TextBox3";
            this.TextBox3.Top = 0F;
            this.TextBox3.Width = 0.9270833F;
            // 
            // TextBox4
            // 
            this.TextBox4.DataField = "PaymentDate";
            this.TextBox4.Height = 0.2F;
            this.TextBox4.Left = 7.5F;
            this.TextBox4.Name = "TextBox4";
            this.TextBox4.OutputFormat = resources.GetString("TextBox4.OutputFormat");
            this.TextBox4.Style = "text-align: right";
            this.TextBox4.Text = "TextBox4";
            this.TextBox4.Top = 0F;
            this.TextBox4.Width = 1.0625F;
            // 
            // TextBox5
            // 
            this.TextBox5.DataField = "DoneString";
            this.TextBox5.Height = 0.2F;
            this.TextBox5.Left = 9.3125F;
            this.TextBox5.Name = "TextBox5";
            this.TextBox5.Style = "font-size: 9.75pt; text-align: right; ddo-char-set: 204";
            this.TextBox5.Text = "TextBox5";
            this.TextBox5.Top = 0F;
            this.TextBox5.Width = 0.8125F;
            // 
            // TextBox9
            // 
            this.TextBox9.DataField = "PaymentPercent";
            this.TextBox9.Height = 0.2F;
            this.TextBox9.Left = 8.5625F;
            this.TextBox9.Name = "TextBox9";
            this.TextBox9.OutputFormat = resources.GetString("TextBox9.OutputFormat");
            this.TextBox9.Style = "text-align: right";
            this.TextBox9.Text = "TextBox9";
            this.TextBox9.Top = 0F;
            this.TextBox9.Width = 0.75F;
            // 
            // TextBox10
            // 
            this.TextBox10.DataField = "AmountBGN";
            this.TextBox10.Height = 0.2F;
            this.TextBox10.Left = 6.5625F;
            this.TextBox10.Name = "TextBox10";
            this.TextBox10.OutputFormat = resources.GetString("TextBox10.OutputFormat");
            this.TextBox10.Style = "text-align: right";
            this.TextBox10.Text = "TextBox10";
            this.TextBox10.Top = 0F;
            this.TextBox10.Width = 0.9270833F;
            // 
            // TextBox6
            // 
            this.TextBox6.DataField = "Amount";
            this.TextBox6.Height = 0.2F;
            this.TextBox6.Left = 5.635417F;
            this.TextBox6.Name = "TextBox6";
            this.TextBox6.OutputFormat = resources.GetString("TextBox6.OutputFormat");
            this.TextBox6.Style = "text-align: right";
            this.TextBox6.SummaryGroup = "GroupHeader1";
            this.TextBox6.SummaryRunning = GrapeCity.ActiveReports.SectionReportModel.SummaryRunning.Group;
            this.TextBox6.SummaryType = GrapeCity.ActiveReports.SectionReportModel.SummaryType.SubTotal;
            this.TextBox6.Text = "TextBox6";
            this.TextBox6.Top = 0.0625F;
            this.TextBox6.Width = 0.9270833F;
            // 
            // Label8
            // 
            this.Label8.Height = 0.2F;
            this.Label8.HyperLink = null;
            this.Label8.Left = 1F;
            this.Label8.Name = "Label8";
            this.Label8.Style = "";
            this.Label8.Text = "???? ?? ";
            this.Label8.Top = 0.0625F;
            this.Label8.Width = 0.625F;
            // 
            // TextBox8
            // 
            this.TextBox8.DataField = "projectname";
            this.TextBox8.Height = 0.2F;
            this.TextBox8.Left = 1.625F;
            this.TextBox8.Name = "TextBox8";
            this.TextBox8.Top = 0.0625F;
            this.TextBox8.Width = 4F;
            // 
            // Line2
            // 
            this.Line2.Height = 0F;
            this.Line2.Left = 1F;
            this.Line2.LineWeight = 1F;
            this.Line2.Name = "Line2";
            this.Line2.Top = 0.0625F;
            this.Line2.Width = 9.125F;
            this.Line2.X1 = 1F;
            this.Line2.X2 = 10.125F;
            this.Line2.Y1 = 0.0625F;
            this.Line2.Y2 = 0.0625F;
            // 
            // TextBox11
            // 
            this.TextBox11.DataField = "AmountBGN";
            this.TextBox11.Height = 0.2F;
            this.TextBox11.Left = 6.5625F;
            this.TextBox11.Name = "TextBox11";
            this.TextBox11.OutputFormat = resources.GetString("TextBox11.OutputFormat");
            this.TextBox11.Style = "text-align: right";
            this.TextBox11.SummaryGroup = "GroupHeader1";
            this.TextBox11.SummaryRunning = GrapeCity.ActiveReports.SectionReportModel.SummaryRunning.Group;
            this.TextBox11.SummaryType = GrapeCity.ActiveReports.SectionReportModel.SummaryType.SubTotal;
            this.TextBox11.Text = "TextBox11";
            this.TextBox11.Top = 0.0625F;
            this.TextBox11.Width = 0.9270833F;
            // 
            // TextBox7
            // 
            this.TextBox7.DataField = "Amount";
            this.TextBox7.Height = 0.2F;
            this.TextBox7.Left = 5.625F;
            this.TextBox7.Name = "TextBox7";
            this.TextBox7.OutputFormat = resources.GetString("TextBox7.OutputFormat");
            this.TextBox7.Style = "text-align: right";
            this.TextBox7.SummaryGroup = "GroupHeader1";
            this.TextBox7.SummaryRunning = GrapeCity.ActiveReports.SectionReportModel.SummaryRunning.All;
            this.TextBox7.SummaryType = GrapeCity.ActiveReports.SectionReportModel.SummaryType.GrandTotal;
            this.TextBox7.Top = 0.1875F;
            this.TextBox7.Width = 1F;
            // 
            // Label7
            // 
            this.Label7.Height = 0.2F;
            this.Label7.HyperLink = null;
            this.Label7.Left = 1F;
            this.Label7.Name = "Label7";
            this.Label7.Style = "font-weight: bold";
            this.Label7.Text = "????:";
            this.Label7.Top = 0.1875F;
            this.Label7.Width = 3F;
            // 
            // Line3
            // 
            this.Line3.Height = 0F;
            this.Line3.Left = 1F;
            this.Line3.LineWeight = 2F;
            this.Line3.Name = "Line3";
            this.Line3.Top = 0.1875F;
            this.Line3.Width = 9.125F;
            this.Line3.X1 = 1F;
            this.Line3.X2 = 10.125F;
            this.Line3.Y1 = 0.1875F;
            this.Line3.Y2 = 0.1875F;
            // 
            // TextBox12
            // 
            this.TextBox12.DataField = "AmountBGN";
            this.TextBox12.Height = 0.2F;
            this.TextBox12.Left = 6.625F;
            this.TextBox12.Name = "TextBox12";
            this.TextBox12.OutputFormat = resources.GetString("TextBox12.OutputFormat");
            this.TextBox12.Style = "text-align: right";
            this.TextBox12.SummaryGroup = "GroupHeader1";
            this.TextBox12.SummaryRunning = GrapeCity.ActiveReports.SectionReportModel.SummaryRunning.All;
            this.TextBox12.SummaryType = GrapeCity.ActiveReports.SectionReportModel.SummaryType.GrandTotal;
            this.TextBox12.Top = 0.1875F;
            this.TextBox12.Width = 0.875F;
            // 
            // ActiveReport1
            // 
            this.MasterReport = false;
            this.PageSettings.PaperHeight = 11F;
            this.PageSettings.PaperWidth = 8.5F;
            this.PrintWidth = 10.125F;
            this.Sections.Add(this.ReportHeader);
            this.Sections.Add(this.PageHeader);
            this.Sections.Add(this.GroupHeader1);
            this.Sections.Add(this.Detail);
            this.Sections.Add(this.GroupFooter1);
            this.Sections.Add(this.PageFooter);
            this.Sections.Add(this.ReportFooter);
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule(resources.GetString("$this.StyleSheet"), "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: inherit; font-style: inherit; font-variant: inherit; font-weight: bo" +
                        "ld; font-size: 16pt; font-size-adjust: inherit; font-stretch: inherit", "Heading1", "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: Times New Roman; font-style: italic; font-variant: inherit; font-wei" +
                        "ght: bold; font-size: 14pt; font-size-adjust: inherit; font-stretch: inherit", "Heading2", "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: inherit; font-style: inherit; font-variant: inherit; font-weight: bo" +
                        "ld; font-size: 13pt; font-size-adjust: inherit; font-stretch: inherit", "Heading3", "Normal"));
            ((System.ComponentModel.ISupportInitialize)(this.lbTitle)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbZad)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbProject)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbClient)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label11)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label12)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TextBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TextBox2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TextBox3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TextBox4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TextBox5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TextBox9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TextBox10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TextBox6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TextBox8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TextBox11)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TextBox7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TextBox12)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();
        }

		#endregion

        private void SubAnalysisRpt_ReportEnd(object sender, EventArgs e)
        {
            if (!this._forPDF) return;

            Logo1 logo = new Logo1();
            logo.Run();

            for (int i = 0; i < this.Document.Pages.Count; i++)
                this.Document.Pages[i].Overlay(logo.Document.Pages[0]);
        }

        private void SubAnalysisRpt_ReportStart(object sender, EventArgs e)
        {
            //			if (!_forPDF)
            //			{
            //				this.PageSettings.Margins.Left = CmToInch(2f);
            //				this.PrintWidth = CmToInch(17f);
            //				Label1.Location = new System.Drawing.PointF(0, Label1.Location.Y);
            //				lbTitle
            //				// lblObekt.Width = CmToInch(6f);
            ////				txtObekt.Location = new System.Drawing.PointF(lblObekt.Location.X+lblObekt.Width, txtObekt.Location.Y);
            ////				txtAddress.Location = new System.Drawing.PointF(txtObekt.Location.X, txtAddress.Location.Y);
            ////
            ////				this.SubReport1.Location = new System.Drawing.PointF(0, this.SubReport1.Location.Y);
            //		}
        }

        private ReportHeader ReportHeader;
        private Label lbTitle;
        private Label Label4;
        private Label Label5;
        private Label Label6;
        private Label lbZad;
        private Label lbProject;
        private Label lbClient;
        private PageHeader PageHeader;
        private Label Label1;
        private Label Label2;
        private Label Label3;
        private Line Line1;
        private Label Label9;
        private Label Label10;
        private Label Label11;
        private Label Label12;
        private GroupHeader GroupHeader1;
        private Detail Detail;
        private TextBox TextBox1;
        private TextBox TextBox2;
        private TextBox TextBox3;
        private TextBox TextBox4;
        private TextBox TextBox5;
        private TextBox TextBox9;
        private TextBox TextBox10;
        private GroupFooter GroupFooter1;
        private TextBox TextBox6;
        private Label Label8;
        private TextBox TextBox8;
        private Line Line2;
        private TextBox TextBox11;
        private PageFooter PageFooter;
        private ReportFooter ReportFooter;
        private TextBox TextBox7;
        private Label Label7;
        private Line Line3;
        private TextBox TextBox12;
	}
}
