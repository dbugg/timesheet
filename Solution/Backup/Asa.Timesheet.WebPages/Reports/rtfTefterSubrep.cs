using System;
using System.Data;
using GrapeCity.ActiveReports;
using GrapeCity.ActiveReports.Controls;
using GrapeCity.ActiveReports.SectionReportModel;
using GrapeCity.ActiveReports.Document.Section;
using GrapeCity.ActiveReports.Document;

namespace Asa.Timesheet.WebPages.Reports
{
    public class rtfTefterSubrep : GrapeCity.ActiveReports.SectionReport
	{
		int i=0;
		string _Name = string.Empty;
        public rtfTefterSubrep(DataView dv)
        {

            InitializeComponent();
            this.DataSource = dv;
        }

        private void PageHeader_Format(object sender, System.EventArgs eArgs)
        {

        }
        private void Detail_Format(object sender, System.EventArgs eArgs)
        {
            i++;
            this.lbNomer.Text = string.Concat(i, '.');
        }
	

		#region ActiveReports Designer generated code







        public void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(rtfTefterSubrep));
            this.Detail = new GrapeCity.ActiveReports.SectionReportModel.Detail();
            this.PageHeader = new GrapeCity.ActiveReports.SectionReportModel.PageHeader();
            this.PageFooter = new GrapeCity.ActiveReports.SectionReportModel.PageFooter();
            this.txtName = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.Label1 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Line1 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.lbNomer = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            ((System.ComponentModel.ISupportInitialize)(this.txtName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbNomer)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            // 
            // Detail
            // 
            this.Detail.ColumnSpacing = 0F;
            this.Detail.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
						this.txtName,
						this.Label1,
						this.Line1,
						this.lbNomer});
            this.Detail.Height = 0.2895833F;
            this.Detail.Name = "Detail";
            // 
            // PageHeader
            // 
            this.PageHeader.Height = 0F;
            this.PageHeader.Name = "PageHeader";
            // 
            // PageFooter
            // 
            this.PageFooter.Height = 0F;
            this.PageFooter.Name = "PageFooter";
            // 
            // txtName
            // 
            this.txtName.DataField = "Names";
            this.txtName.Height = 0.2F;
            this.txtName.Left = 0.3F;
            this.txtName.Name = "txtName";
            this.txtName.Top = 0F;
            this.txtName.Width = 3.7F;
            // 
            // Label1
            // 
            this.Label1.Height = 0.2F;
            this.Label1.HyperLink = null;
            this.Label1.Left = 4F;
            this.Label1.Name = "Label1";
            this.Label1.Style = "font-size: 6pt; text-align: center; vertical-align: bottom; ddo-char-set: 1";
            this.Label1.Text = "(??????)";
            this.Label1.Top = 0.09F;
            this.Label1.Width = 1F;
            // 
            // Line1
            // 
            this.Line1.Height = 0F;
            this.Line1.Left = 4F;
            this.Line1.LineWeight = 1F;
            this.Line1.Name = "Line1";
            this.Line1.Top = 0.2F;
            this.Line1.Width = 1F;
            this.Line1.X1 = 4F;
            this.Line1.X2 = 5F;
            this.Line1.Y1 = 0.2F;
            this.Line1.Y2 = 0.2F;
            // 
            // lbNomer
            // 
            this.lbNomer.Height = 0.2F;
            this.lbNomer.Left = 0F;
            this.lbNomer.Name = "lbNomer";
            this.lbNomer.Style = "text-align: right";
            this.lbNomer.Top = 0F;
            this.lbNomer.Width = 0.3F;
            // 
            // ActiveReport1
            // 
            this.MasterReport = false;
            this.PageSettings.PaperHeight = 11F;
            this.PageSettings.PaperWidth = 8.5F;
            this.PrintWidth = 5.041667F;
            this.Sections.Add(this.PageHeader);
            this.Sections.Add(this.Detail);
            this.Sections.Add(this.PageFooter);
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule(resources.GetString("$this.StyleSheet"), "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: inherit; font-style: inherit; font-variant: inherit; font-weight: bo" +
                        "ld; font-size: 16pt; font-size-adjust: inherit; font-stretch: inherit", "Heading1", "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: Times New Roman; font-style: italic; font-variant: inherit; font-wei" +
                        "ght: bold; font-size: 14pt; font-size-adjust: inherit; font-stretch: inherit", "Heading2", "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: inherit; font-style: inherit; font-variant: inherit; font-weight: bo" +
                        "ld; font-size: 13pt; font-size-adjust: inherit; font-stretch: inherit", "Heading3", "Normal"));
            ((System.ComponentModel.ISupportInitialize)(this.txtName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbNomer)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();

            // Attach Report Events
            this.PageHeader.Format += new System.EventHandler(this.PageHeader_Format);
            this.Detail.Format += new System.EventHandler(this.Detail_Format);
        }

		#endregion

        private PageHeader PageHeader;
        private Detail Detail;
        private TextBox txtName;
        private Label Label1;
        private Line Line1;
        private TextBox lbNomer;
        private PageFooter PageFooter;
	}
}
