using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Data.SqlClient;
using log4net;
using Asa.Timesheet.Data;
using Asa.Timesheet.Data.Entities;
using Asa.Timesheet.Data.Util;
using DataDynamics.ActiveReports.Export.Pdf;
using DataDynamics.ActiveReports.Export.Xls;
using Asa.Timesheet.WebPages.Reports;
using DataDynamics.ActiveReports.Export.Rtf;
using System.IO;
using Asa.Timesheet.Data.Reports;
using Asa.Timesheet.WebPages.UserControls;

namespace Asa.Timesheet.WebPages
{
	/// <summary>
	/// Summary description for Buildings.
	/// </summary>
	public class Buildings : TimesheetPageBase 
	{
		#region WebControls
		protected System.Web.UI.WebControls.CheckBox cbCopyLastBuilding;
		protected Asa.Timesheet.WebPages.UserControls.PageHeader header;
		protected System.Web.UI.WebControls.DataList dlBuildings;
		protected System.Web.UI.WebControls.PlaceHolder menuHolder;
		protected System.Web.UI.WebControls.Label lblError;
		protected System.Web.UI.WebControls.Label lblInfo;
		protected System.Web.UI.WebControls.Label lblTitle;
		protected System.Web.UI.WebControls.Button btnMakeOzn;
		protected System.Web.UI.WebControls.Button btnCancelOzn;
		protected System.Web.UI.WebControls.Button btnDeleteSelected;
		protected System.Web.UI.WebControls.Button btnNewBuilding;
		protected System.Web.UI.WebControls.Button btnSave;
		protected System.Web.UI.WebControls.Button btnCancel;
		private static readonly ILog log = LogManager.GetLogger(typeof(Buildings));
		protected System.Web.UI.WebControls.Label lbPhases;
		protected System.Web.UI.WebControls.DropDownList ddlPhases;
		
		#endregion
		
		public const int ArchitectConst = 8;
		public const string RazdelConst = ".";
//		System.Configuration.ConfigurationSettings.AppSettings["ScanPath"]
		
		#region PageLoad
	
		private void Page_Load(object sender, System.EventArgs e)
		{
			if (!LoggedUser.HasRole) ErrorRedirect(Resource.ResourceManager["noRightsAlert"]);
			UIHelpers.CreateMenu(menuHolder,LoggedUser);
			string projectName, projectCode, administrativeName,add;
			decimal area=0;int clientID=0;
			int PID=UIHelpers.GetPIDParam();
			bool phases;
			if (!ProjectsData.SelectProject(PID, out projectName, out  projectCode, out administrativeName, out area, out add, out clientID,out phases))
			{
				log.Error("Project not found " + projectName);
				
			}
			if(!IsPostBack)
			{
				header.PageTitle = Resource.ResourceManager["prbuilding_PageTitle"]+" "+projectName;//+ " "+administrativeName
				header.UserName = LoggedUser.UserName;
				int projectID = UIHelpers.GetPIDParam();
				ddlPhasesBind();
				AllPlusClick();
				LoadFirst();
				
			}
			
				if(!LoggedUser.IsLeader)
				{
					UserInfo ui = UsersData.SelectUserByID(ProjectsData.SelectProjectManagerID(UIHelpers.GetPIDParam()));
					if(ui==null || ui.UserID!=LoggedUser.UserID)
						btnDeleteSelected.Visible = false;
				}
//			string js = string.Concat("return confirm('",Resource.ResourceManager["btn_CanselOzn"]," ?');");
//			btnCancelOzn.Attributes["onclick"] = js;
		}
		
	
		#endregion
		
		#region BindGrid
	
		private void LoadFirst()
		{
			int projectID = UIHelpers.GetPIDParam();
			int nMaxBuildings=int.Parse(System.Configuration.ConfigurationSettings.AppSettings["MaxBuildings"]);
			for(int i=1;i<=nMaxBuildings;i++)
			{
				this.Session["DistrTable"+i.ToString()] = null;
				for(int j=1;j<=ArchitectConst;j++)
					this.Session["ContentsTable"+i.ToString()+"_"+j.ToString()] = null;
				this.Session["ContentsTableNew"+i.ToString()] = null;
			}
			ContentsVector cv1 = ContentDAL.LoadCollection("ContentsList0Proc", SQLParms.CreateContentsList0Proc(projectID));
			ContentsVector[] cv2 = new ContentsVector[ArchitectConst];
			for(int j=1;j<ArchitectConst;j++)
			{
				ContentsVector cvFilter = ContentDAL.LoadCollection("ContentsListArchitectProc", SQLParms.CreateContentsListFilterProc(projectID,j));
				cv2[j] = cvFilter;
			}
			ContentsVector cv = ContentDAL.LoadCollection("ContentsListArchitectProc", SQLParms.CreateContentsListFilterProc(projectID,9));
			cv2[0] = cv;
			ContentsVector cv3 = ContentDAL.LoadCollection("ContentsList8Proc", SQLParms.CreateContentsList0Proc(projectID));
			MakeOrder(cv1,cv2,cv3);
			int nMax= cv1.GetMaxBuilding();
			for(int j=1;j<=ArchitectConst;j++)
			{
				if(cv2[j-1].GetMaxBuilding()>nMax)
					nMax = cv2[j-1].GetMaxBuilding();
			}
			if(cv3.GetMaxBuilding()>nMax)
				nMax = cv3.GetMaxBuilding();
			ArrayList al = new ArrayList();
			for(int i=1;i<=nMax;i++)
			{
				al.Add(i);
				for(int k=0;k<ArchitectConst;k++)
				{
					ContentsVector cv2Vector = cv2[k];
					SetContentsTable(i,cv2Vector.GetByNumber(i),k+1);
				}
				SetContentsTableNew(i,cv3.GetByNumber(i));
				SetDistrTable(i,cv1.GetByNumber(i));
			}
			dlBuildings.DataSource=al;
			dlBuildings.DataBind();
			IsOneOrMany(al.Count);
			NameOfContent();
			BindGrid();
			RefreshTotals();
			if(al.Count == 1)
				if(IsCodeThere(0))
					ReplaceCodePhases(CodeOfPhase(),0);
		}
		private void BindGrid()
		{
			ContentsVector cv = new ContentsVector();
			for(int i=0;i<dlBuildings.Items.Count;i++)
			{
				TextBox txtS=(TextBox) dlBuildings.Items[i].FindControl("txtS");
				txtS.Text = "";
				DropDownList ddlContents =(DropDownList) dlBuildings.Items[i].FindControl("ddlContents");
				ddlContentBind(i);
				DropDownList ddlContentsConstructors =(DropDownList) dlBuildings.Items[i].FindControl("ddlContentsConstructors");
				ddlContentsConstructors.DataSource=SubprojectsUDL.SelectContentTypesConstructors(-1);
				ddlContentsConstructors.DataValueField="ContentTypeID";
				ddlContentsConstructors.DataTextField="ContentType";
				ddlContentsConstructors.DataBind();
				string Constructor = Resource.ResourceManager["Contructor"];
				if(LoggedUser.UserRole!=Constructor)
				{
					ddlContentsConstructors.Visible = false;
					Button btnAddConstructorsOnly = (Button) dlBuildings.Items[i].FindControl("btnAddConstructorsOnly");
					btnAddConstructorsOnly.Visible =false;
				}
				string nameOfResourses = string.Empty;
				DataList dlBuildingContentArchitect = (DataList) dlBuildings.Items[i].FindControl("dlBuildingContentArchitect");
				ArrayList al = new ArrayList();
				for(int k=0;k<ArchitectConst;k++)
				{
					al.Add(k);
				}
				dlBuildingContentArchitect.DataSource=al;
				dlBuildingContentArchitect.DataBind();
				for(int k=0;k<ArchitectConst;k++)
				{
					DataGrid grd =(DataGrid) dlBuildingContentArchitect.Items[k].FindControl("grdContentArchitect");
					ContentsVector cv1 = GetContentsTable(i+1,k+1);
					grd.DataSource=cv1;
					grd.DataKeyField="ContentID";
					grd.DataBind();
					grd.Visible=grd.Items.Count>0;
					grd.TabIndex=(short)(i+1);
					if(grd.Items.Count>0)
					{
						ImageButton iArrowUp = (ImageButton) grd.Items[0].FindControl("iArrowUp");
						iArrowUp.Visible = false;
						ImageButton iArrowDown = (ImageButton) grd.Items[grd.Items.Count-1].FindControl("iArrowDown");
						iArrowDown.Visible = false;
						if(txtS.Text.Length==0)
							txtS.Text = cv1[0].Signature;
						for(int ii=0;ii<grd.Items.Count;ii++)
						{
							DropDownList ddlCP = (DropDownList)grd.Items[ii].FindControl("ddlContentFinishedPhase1");
							ddlContentFinishedBind(ddlCP);
							ddlCP.SelectedIndex = cv1[ii].ContentFinishedPhase1/10;
							ddlCP = (DropDownList)grd.Items[ii].FindControl("ddlContentFinishedPhase2");
							ddlContentFinishedBind(ddlCP);
							ddlCP.SelectedIndex = cv1[ii].ContentFinishedPhase2/10;
							ddlCP = (DropDownList)grd.Items[ii].FindControl("ddlContentFinishedPhase3");
							ddlContentFinishedBind(ddlCP);
							ddlCP.SelectedIndex = cv1[ii].ContentFinishedPhase3/10;
							ddlCP = (DropDownList)grd.Items[ii].FindControl("ddlContentFinishedPhase4");
							ddlContentFinishedBind(ddlCP);
							ddlCP.SelectedIndex = cv1[ii].ContentFinishedPhase4/10;
						}
					}
						nameOfResourses = string.Concat("lbArchitectsContent",k);
					Label lb = (Label)dlBuildingContentArchitect.Items[k].FindControl("lbGridName");
					lb.Text = Resource.ResourceManager[nameOfResourses];
					if(k==1)
					{
						DataGrid grdDistr =(DataGrid) dlBuildingContentArchitect.Items[k].FindControl("grdDistr");
						grdDistr.Visible=true;
						cv = GetDistrTable(i+1);
						grdDistr.DataSource= cv;
						grdDistr.DataKeyField="ContentID";
						grdDistr.DataBind();
						grdDistr.Visible=grdDistr.Items.Count>0;
						grdDistr.TabIndex=(short)(i+1);
						TextBox txtCF = (TextBox)dlBuildings.Items[i].FindControl("txtContentFinished1");
						if(grdDistr.Items.Count>0)
						{
							ImageButton imgArrowUp = (ImageButton) grdDistr.Items[0].FindControl("imgArrowUp");
							imgArrowUp.Visible = false;
							ImageButton imgArrowDown = (ImageButton) grdDistr.Items[grdDistr.Items.Count-1].FindControl("imgArrowDown");
							imgArrowDown.Visible = false;
							if(txtS.Text.Length==0)
								txtS.Text = cv[0].Signature;
							for(int ii=0;ii<grdDistr.Items.Count;ii++)
							{
								DropDownList ddlCP1 = (DropDownList)grdDistr.Items[ii].FindControl("ddlContentFinishedPhase1");
								ddlContentFinishedBind(ddlCP1);
								ddlCP1.SelectedIndex = cv[ii].ContentFinishedPhase1/10;
								ddlCP1 = (DropDownList)grdDistr.Items[ii].FindControl("ddlContentFinishedPhase2");
								ddlContentFinishedBind(ddlCP1);
								ddlCP1.SelectedIndex = cv[ii].ContentFinishedPhase2/10;
								ddlCP1 = (DropDownList)grdDistr.Items[ii].FindControl("ddlContentFinishedPhase3");
								ddlContentFinishedBind(ddlCP1);
								ddlCP1.SelectedIndex = cv[ii].ContentFinishedPhase3/10;
								ddlCP1 = (DropDownList)grdDistr.Items[ii].FindControl("ddlContentFinishedPhase4");
								ddlContentFinishedBind(ddlCP1);
								ddlCP1.SelectedIndex = cv[ii].ContentFinishedPhase4/10;
							}
						}
					}
					bool b = GetPlusMinus(i+1,k+1);
					if(b)
						btnPlusMultipleClick(i+1,k+1);
					else
						btnMinusMultipleClick(i+1,k+1);
				}
				DataGrid grdNew =(DataGrid) dlBuildings.Items[i].FindControl("grdNew");
				ContentsVector cv3=GetContentsTableNew(i+1);
				grdNew.DataSource=cv3;
				grdNew.DataKeyField="ContentID";
				grdNew.DataBind();
				grdNew.Visible=grdNew.Items.Count>0;
				grdNew.TabIndex=(short)(i+1);
				if(grdNew.Items.Count>0)
				{
					ImageButton iArrowUpNew = (ImageButton) grdNew.Items[0].FindControl("iArrowUpNew");
					iArrowUpNew.Visible = false;
					ImageButton iArrowDownNew = (ImageButton) grdNew.Items[grdNew.Items.Count-1].FindControl("iArrowDownNew");
					iArrowDownNew.Visible = false;
					if(txtS.Text.Length==0)
						txtS.Text = cv3[0].Signature;
					for(int ii=0;ii<grdNew.Items.Count;ii++)
					{
						DropDownList ddlCP2 = (DropDownList)grdNew.Items[ii].FindControl("ddlContentFinishedPhase1");
						ddlContentFinishedBind(ddlCP2);
						ddlCP2.SelectedIndex = cv3[ii].ContentFinishedPhase1/10;
						ddlCP2 = (DropDownList)grdNew.Items[ii].FindControl("ddlContentFinishedPhase2");
						ddlContentFinishedBind(ddlCP2);
						ddlCP2.SelectedIndex = cv3[ii].ContentFinishedPhase2/10;
						ddlCP2 = (DropDownList)grdNew.Items[ii].FindControl("ddlContentFinishedPhase3");
						ddlContentFinishedBind(ddlCP2);
						ddlCP2.SelectedIndex = cv3[ii].ContentFinishedPhase3/10;
						ddlCP2 = (DropDownList)grdNew.Items[ii].FindControl("ddlContentFinishedPhase4");
						ddlContentFinishedBind(ddlCP2);
						ddlCP2.SelectedIndex = cv3[ii].ContentFinishedPhase4/10;
					}
				}
//				if(cv.Count>0)
//				{
//					txtS.Text=cv[0].Signature;
//				}
				
				if(IsCodeThere(i))
					ReplaceCodePhases(CodeOfPhase(),i);

				
				
				if(!(LoggedUser.IsLeader))
				{
//					Button btn = (Button) dlBuildings.Items[i].FindControl("btnMakeCurrentOzn");
//					btn.Visible = false;
					Button btn = (Button) dlBuildings.Items[i].FindControl("btnCanselCurrentOzn");
					UserInfo ui = UsersData.SelectUserByID(ProjectsData.SelectProjectManagerID(UIHelpers.GetPIDParam()));
					if(ui==null || ui.UserID!=LoggedUser.UserID)
						btn.Visible = false;
				}
				try
				{
					DropDownList dll = (DropDownList) dlBuildings.Items[i].FindControl("txtS1");
					string S1 = txtS.Text.Substring(0,1);
					ListItem li = new ListItem(S1);
					int index = dll.Items.IndexOf(li);
					dll.SelectedIndex = index;
					DropDownList dll1 = (DropDownList) dlBuildings.Items[i].FindControl("txtS2");
					string S2 = txtS.Text.Substring(1);
					li = new ListItem(S2);
					index = dll1.Items.IndexOf(li);
					dll1.SelectedIndex = index;
					if(!CheckContentTitle(string.Concat(dll.SelectedValue,dll1.SelectedValue),i,false))
						MakeNewContentTitle(i,false);
				}
				catch
				{
					MakeNewContentTitle(i,false);
				}
			}
			HideOthers();
		}
		
		private void IsOneOrMany(int nMax)
		{
			if(nMax==1)
			{
				lblTitle.Text = Resource.ResourceManager["reports_MailReport_Obekt"];
				Label lb = (Label) dlBuildings.Items[0].FindControl("Label1");
				lb.Visible = false;
				lb = (Label) dlBuildings.Items[0].FindControl("Label2");
				lb.Visible = false;
				DropDownList dll1 = (DropDownList) dlBuildings.Items[0].FindControl("txtS1");
				dll1.Visible = false;
				DropDownList dll2 = (DropDownList) dlBuildings.Items[0].FindControl("txtS2");
				dll2.Visible = false;
				btnNewBuilding.Text = Resource.ResourceManager["NewBuildingButtonMany"];
			}
			else
			{
				lblTitle.Text = Resource.ResourceManager["dlBuildings"];
				Label lb = (Label) dlBuildings.Items[0].FindControl("Label1");
				lb.Visible = true;
				lb = (Label) dlBuildings.Items[0].FindControl("Label2");
				lb.Visible = true;
				DropDownList dll1 = (DropDownList) dlBuildings.Items[0].FindControl("txtS1");
				dll1.Visible = true;
				DropDownList dll2 = (DropDownList) dlBuildings.Items[0].FindControl("txtS2");
				dll2.Visible = true;
				btnNewBuilding.Text = Resource.ResourceManager["NewBuildingButtonOne"];
			}
		}
		private ListItem SelectedContentValue(int index)
		{
			try
			{
				DropDownList ddl = (DropDownList) dlBuildings.Items[index].FindControl("ddlContents");
				return ddl.SelectedItem;
			}
			catch
			{
				return null;
			}
		}
		
	
		#endregion
		
		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
			this.ddlPhases.SelectedIndexChanged += new System.EventHandler(this.ddlPhases_SelectedIndexChanged);
			this.dlBuildings.ItemCreated += new System.Web.UI.WebControls.DataListItemEventHandler(this.dlBuildings_ItemCreated);
			this.dlBuildings.ItemCommand += new System.Web.UI.WebControls.DataListCommandEventHandler(this.dlBuildings_ItemCommand);
			this.dlBuildings.ItemDataBound += new System.Web.UI.WebControls.DataListItemEventHandler(this.dlBuildings_ItemDataBound);
			this.btnNewBuilding.Click += new System.EventHandler(this.btnNewBuilding_Click);
			this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
			this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click_1);
			this.btnDeleteSelected.Click += new System.EventHandler(this.btnDeleteSelected_Click);
			this.Load += new System.EventHandler(this.Page_Load);

		}
		#endregion
		
		#region SessionTables
	
		protected void SetDistrTable(int n,ContentsVector cv)
		{
			int projectID = UIHelpers.GetPIDParam();
			ChecksVector checks = GetChecksTable(projectID);
			for(int i=0;i<cv.Count;i++)
			{
				int index = checks.IndexOf(cv[i].ContentID,true);
				if (index!=-1)
					cv[i].IsChecked = checks[index].IsChecked;
			}		
			this.Session["DistrTable"+n.ToString()] = cv;
		}
		protected ContentsVector GetDistrTable(int n)
		{
			string name= "DistrTable"+n.ToString();
			
			if (this.Session[name] != null)
			{
				return (ContentsVector)this.Session[name];
			}
			ContentsVector pv =  new ContentsVector();
			this.Session[name]=pv;
			return pv;
		}
		protected void SetContentsTable(int n,ContentsVector cv,int Filter)
		{
			int projectID = UIHelpers.GetPIDParam();
			ChecksVector checks = GetChecksTable(projectID);
			for(int i=0;i<cv.Count;i++)
			{
				int index = checks.IndexOf(cv[i].ContentID,true);
				if (index!=-1)
					cv[i].IsChecked = checks[index].IsChecked;
			}		
			this.Session["ContentsTable"+n.ToString()+"_"+Filter.ToString()] = cv;
		}
		protected void SetContentsTableNew(int n,ContentsVector cv)
		{
			int projectID = UIHelpers.GetPIDParam();
			ChecksVector checks = GetChecksTable(projectID);
			for(int i=0;i<cv.Count;i++)
			{
				int index = checks.IndexOf(cv[i].ContentID,true);
				if (index!=-1)
					cv[i].IsChecked = checks[index].IsChecked;
			}		
			this.Session["ContentsTableNew"+n.ToString()] = cv;
		}
		protected ContentsVector GetContentsTable(int n,int Filter)
		{
			string name= "ContentsTable"+n.ToString()+"_"+Filter.ToString();
			
			if (this.Session[name] != null)
			{
				return (ContentsVector)this.Session[name];
			}
			ContentsVector pv =  new ContentsVector();
			this.Session[name]=pv;
			return pv;
		}
		protected ContentsVector GetContentsTableNew(int n)
		{
			string name= "ContentsTableNew"+n.ToString();
			
			if (this.Session[name] != null)
			{
				return (ContentsVector)this.Session[name];
			}
			ContentsVector pv =  new ContentsVector();
			this.Session[name]=pv;
			return pv;
		}
		protected ChecksVector GetChecksTable(int n)
		{
			string name= "ChecksTable"+n.ToString();
			
			if (this.Session[name] != null)
			{
				return (ChecksVector)this.Session[name];
			}
			ChecksVector pv =  new ChecksVector();
			this.Session[name]=pv;
			return pv;
		}
		private ContentsVector GetDistr(int projectID,DataGrid grdDistr,string sign)
		{
			ContentsVector cv= new ContentsVector();
			for(int i=0;i<grdDistr.Items.Count;i++)
			{
				TextBox  txtKota=(TextBox) grdDistr.Items[i].FindControl("txtKota");
				TextBox  txtA=(TextBox) grdDistr.Items[i].FindControl("txtA");
				TextBox  txtMa=(TextBox) grdDistr.Items[i].FindControl("txtMa");
				CheckBox  cb =(CheckBox) grdDistr.Items[i].FindControl("cbReportCode");
				DropDownList ddlContentFinishedPhase1 = (DropDownList) grdDistr.Items[i].FindControl("ddlContentFinishedPhase1");
				DropDownList ddlContentFinishedPhase2 = (DropDownList) grdDistr.Items[i].FindControl("ddlContentFinishedPhase2");
				DropDownList ddlContentFinishedPhase3 = (DropDownList) grdDistr.Items[i].FindControl("ddlContentFinishedPhase3");
				DropDownList ddlContentFinishedPhase4 = (DropDownList) grdDistr.Items[i].FindControl("ddlContentFinishedPhase4");

				decimal k=0;
				decimal a=0;
				if(txtKota.Text!="")
					k=UIHelpers.ParseDecimal(txtKota.Text);

				if(txtA.Text!="")
					a=UIHelpers.ParseDecimal(txtA.Text);
				int SSID=int.Parse(grdDistr.Items[i].Cells[0].Text);
				Label lbOzn = (Label) grdDistr.Items[i].FindControl("lbOzn");
				DropDownList ddlRevision = (DropDownList) grdDistr.Items[i].FindControl("ddlRevision");
				if(dlBuildings.Items.Count<2)
					sign = "";
				ContentData cd = new ContentData(-1,projectID,SSID,k,a,"", txtMa.Text,grdDistr.TabIndex, sign,lbOzn.Text,ddlRevision.SelectedItem.Text,i,
					ddlContentSelectedValue(ddlContentFinishedPhase1,ddlContentFinishedPhase1.SelectedIndex),ddlContentSelectedValue(ddlContentFinishedPhase2,ddlContentFinishedPhase2.SelectedIndex),
					ddlContentSelectedValue(ddlContentFinishedPhase3,ddlContentFinishedPhase3.SelectedIndex),ddlContentSelectedValue(ddlContentFinishedPhase4,ddlContentFinishedPhase4.SelectedIndex)); 
				cd.ContentType= grdDistr.Items[i].Cells[3].Text;
				cd.IsChecked = cb.Checked;
				cv.Add(cd);
			}
			return cv;
		}
		private ContentsVector GetContents(int projectID, DataGrid grd,string sign)
		{
			ContentsVector cv= new ContentsVector();
			for(int i=0;i<grd.Items.Count;i++)
			{
				TextBox  txtT=(TextBox) grd.Items[i].FindControl("txtT");
				TextBox  txtMa=(TextBox) grd.Items[i].FindControl("txtMa");
				CheckBox  cb =(CheckBox) grd.Items[i].FindControl("cbReportCode");
				DropDownList ddlContentFinishedPhase1 = (DropDownList) grd.Items[i].FindControl("ddlContentFinishedPhase1");
				DropDownList ddlContentFinishedPhase2 = (DropDownList) grd.Items[i].FindControl("ddlContentFinishedPhase2");
				DropDownList ddlContentFinishedPhase3 = (DropDownList) grd.Items[i].FindControl("ddlContentFinishedPhase3");
				DropDownList ddlContentFinishedPhase4 = (DropDownList) grd.Items[i].FindControl("ddlContentFinishedPhase4");

				int SSID=int.Parse(grd.Items[i].Cells[0].Text);
				Label lbOzn = (Label) grd.Items[i].FindControl("lbOzn");
				DropDownList ddlRevision = (DropDownList) grd.Items[i].FindControl("ddlRevision");
				if(dlBuildings.Items.Count<2)
					sign = "";
				ContentData cd = new ContentData(-1,projectID,SSID,0,0,txtT.Text, txtMa.Text,grd.TabIndex, sign,lbOzn.Text,ddlRevision.SelectedItem.Text, i,
					ddlContentSelectedValue(ddlContentFinishedPhase1,ddlContentFinishedPhase1.SelectedIndex),ddlContentSelectedValue(ddlContentFinishedPhase2,ddlContentFinishedPhase2.SelectedIndex),
					ddlContentSelectedValue(ddlContentFinishedPhase3,ddlContentFinishedPhase3.SelectedIndex),ddlContentSelectedValue(ddlContentFinishedPhase4,ddlContentFinishedPhase4.SelectedIndex));  	
				cd.ContentType= grd.Items[i].Cells[3].Text;
				cd.IsChecked = cb.Checked;
				cv.Add(cd);
			}
			return cv;
		}

		private void SetPlusMinus(int first,int second,bool b)
		{
			this.Session["PlusMinus"+first.ToString()+"_"+second.ToString()] = b;
		}
		private bool GetPlusMinus(int first,int second)
		{
			string name= "PlusMinus"+first.ToString()+"_"+second.ToString();
			if (this.Session[name] != null)
			{
				return (bool)this.Session[name];
			}
			bool b =  false;
			this.Session[name]=b;
			return b;
		}
	
		#endregion
	
		#region OrderAndCode
		
		private void MakeOrder(ContentsVector cv1,ContentsVector[] cv2, ContentsVector cv3)
		{
			if (IsOrder(cv1,cv2,cv3)) return;
			else
			{
				int nMax = cv1.GetMaxBuilding();
				for(int j=1;j<=ArchitectConst;j++)
				{
					if(cv2[j-1].GetMaxBuilding()>nMax)
						nMax = cv2[j-1].GetMaxBuilding();
				}
				if(cv3.GetMaxBuilding()>nMax)
					nMax = cv3.GetMaxBuilding();
				for(int i=1;i<=nMax;i++)
				{
					int NewOrder = 1;
					for(int j=0;j<cv1.Count;j++)						
						if (cv1[j].BuildingNumber == i)
						{
							cv1[j].ContentOrder = NewOrder;
							NewOrder++;
						}
					for(int k=0;k<ArchitectConst;k++)
					{
						ContentsVector cv2Vector = cv2[k];
						NewOrder = 1;
						for(int j=0;j<cv2[k].Count;j++)		
							if (cv2Vector[j].BuildingNumber == i)
							{
								cv2Vector[j].ContentOrder = NewOrder;
								NewOrder++;
							}	
					}
					NewOrder = 1;
					for(int j=0;j<cv3.Count;j++)							
						if (cv3[j].BuildingNumber == i)
						{
							cv3[j].ContentOrder = NewOrder;
							NewOrder++;
						}
				}	
			}
		}
		private bool IsOrder(ContentsVector cv1,ContentsVector[] cv2, ContentsVector cv3)
		{
			if(cv1.Count!=0)
				if(cv1[0].ContentOrder!=0) return true;
				else return false;
			else
				for(int k=0;k<ArchitectConst;k++)
				{
					if(cv2[k].Count!=0)
					{
						if(cv2[k][0].ContentOrder!=0) return true;
						else return false;}
				}
				
			if(cv3.Count!=0)
				if(cv3[0].ContentOrder!=0) return true;
				else return false;		
			else return false;
		}			
		private bool IsCodeThere(int nIndex)
		{
				DataList dlist =(DataList) dlBuildings.Items[nIndex].FindControl("dlBuildingContentArchitect");
				for(int k=0;k<ArchitectConst;k++)
				{
					DataGrid grd =(DataGrid) dlist.Items[k].FindControl("grdContentArchitect");
					for(int j=0;j<grd.Items.Count;j++)
					{   
						Label lbOzn = (Label) grd.Items[j].FindControl("lbOzn");
						if (lbOzn.Text==null||lbOzn.Text=="") return false;
						else return true;
					}
					if(k==1)
					{
						grd =(DataGrid) dlist.Items[k].FindControl("grdDistr");
						for(int j=0;j<grd.Items.Count;j++)
						{   
							Label lbOzn = (Label) grd.Items[j].FindControl("lbOzn");
							if (lbOzn.Text==null||lbOzn.Text=="") return false;
							else return true;
						}
					}
				}
				DataGrid grdNew =(DataGrid) dlBuildings.Items[nIndex].FindControl("grdNew");
				for(int j=0;j<grdNew.Items.Count;j++)
				{   
					Label lbOzn = (Label) grdNew.Items[j].FindControl("lbOzn");
					if (lbOzn.Text==null||lbOzn.Text=="") return false;
					else return true;
				}
			
			return false;
		}
		private int maxSgn(DataGrid dg,string s)
		{
			int max = 0;
			for(int i=0;i<dg.Items.Count;i++)
			{
				Label lbOzn = (Label) dg.Items[i].FindControl("lbOzn");
				if( lbOzn.Text.StartsWith(s))
				{
					int index = lbOzn.Text.LastIndexOf(RazdelConst);
					string Nomer ="";
					if( (dg.Items[i].Cells[0].Text == System.Configuration.ConfigurationSettings.AppSettings["DetailV"])||(dg.Items[i].Cells[0].Text == System.Configuration.ConfigurationSettings.AppSettings["DetailH"]))
						Nomer = lbOzn.Text.Substring(index-2,2);
					else
						Nomer = lbOzn.Text.Substring(index+1);
					if(max<int.Parse(Nomer))
						max = int.Parse(Nomer);
				}	
			}
			return max;
		}

		private string GetPrefix(string ContentTypeID,DataSet ds)
		{
			int index = UIHelpers.ToInt(ContentTypeID);
			DataRow dr = ds.Tables[0].Rows[index-1];
			string s = dr["ContentPrefix"].ToString();
			return s;
		}
		
		private void ReplaceCode(string NewCode,int index)
		{
			DataList dlist = (DataList) dlBuildings.Items[index].FindControl("dlBuildingContentArchitect");
			for(int k=1;k<=ArchitectConst;k++)
			{
				DataGrid grdOne =(DataGrid) dlist.Items[k-1].FindControl("grdContentArchitect");
				for(int i=0;i<grdOne.Items.Count;i++)
				{
					Label lbOzn = (Label)grdOne.Items[i].FindControl("lbOzn");
					if(k==6 &&((grdOne.Items[i].Cells[0].Text == System.Configuration.ConfigurationSettings.AppSettings["DetailV"])||(grdOne.Items[i].Cells[0].Text == System.Configuration.ConfigurationSettings.AppSettings["DetailH"]))) 
						lbOzn.Text = ReplaceCodeText(lbOzn.Text,NewCode,true);
					else
						lbOzn.Text = ReplaceCodeText(lbOzn.Text,NewCode,false);
					SetContentsTable(index+1,GetContents(UIHelpers.GetPIDParam(),grdOne,NewCode),k);
				}
				if(k==2)
				{
					grdOne =(DataGrid) dlist.Items[k-1].FindControl("grdDistr");
					for(int i=0;i<grdOne.Items.Count;i++)
					{
						if(grdOne.Items[i].Cells[0].Text!=System.Configuration.ConfigurationSettings.AppSettings["GroundArea"])
						{
							Label lbOzn = (Label)grdOne.Items[i].FindControl("lbOzn");
							lbOzn.Text = ReplaceCodeText(lbOzn.Text,NewCode,false);
							SetDistrTable(index+1,GetDistr(UIHelpers.GetPIDParam(),grdOne,NewCode));
						}
					}
				}
			}
			DataGrid grdNew =(DataGrid) dlBuildings.Items[index].FindControl("grdNew");
			for(int i=0;i<grdNew.Items.Count;i++)
			{
				Label lbOzn = (Label)grdNew.Items[i].FindControl("lbOzn");
				lbOzn.Text = ReplaceCodeText(lbOzn.Text,NewCode,false);
				SetContentsTableNew(index+1,GetContents(UIHelpers.GetPIDParam(),grdNew,NewCode));
			}
		}

		private string ReplaceCodeText(string text,string New,bool Detail)
		{
			int index = 0;
			if((text.Length == 6 && !Detail)||(text.Length == 8 && Detail))
				if(New == "")
					return text;
				else
					return string.Concat(text.Substring(0,2),New,RazdelConst,text.Substring(2));
			index = text.IndexOf(RazdelConst,2);
			if(New == "")
			{
				return string.Concat(text[0],text[1],text.Substring(index+1));
			}
			return string.Concat(text[0],text[1],New,RazdelConst,text.Substring(index+1));
//			string NewText = text.Substring(index);
//			return string.Concat(New,NewText);
		}
		private void ReplaceCodePhases(string NewCode,int index)
		{
			bool IsOneBuilding = (dlBuildings.Items.Count == 1);
			DataList dlist = (DataList) dlBuildings.Items[index].FindControl("dlBuildingContentArchitect");
			for(int k=1;k<=ArchitectConst;k++)
			{
				DataGrid grdOne =(DataGrid) dlist.Items[k-1].FindControl("grdContentArchitect");
				for(int i=0;i<grdOne.Items.Count;i++)
				{
					Label lbOzn = (Label)grdOne.Items[i].FindControl("lbOzn");
					if(k==6 &&((grdOne.Items[i].Cells[0].Text == System.Configuration.ConfigurationSettings.AppSettings["DetailV"])||(grdOne.Items[i].Cells[0].Text == System.Configuration.ConfigurationSettings.AppSettings["DetailH"]))) 
						lbOzn.Text = ReplaceCodePhasesText(lbOzn.Text,NewCode,true,IsOneBuilding);
					else
						lbOzn.Text = ReplaceCodePhasesText(lbOzn.Text,NewCode,false,IsOneBuilding);
				}
				if(k==2)
				{

					grdOne =(DataGrid) dlist.Items[k-1].FindControl("grdDistr");
					for(int i=0;i<grdOne.Items.Count;i++)
					{
						if(grdOne.Items[i].Cells[0].Text!=System.Configuration.ConfigurationSettings.AppSettings["GroundArea"])
						{
							Label lbOzn = (Label)grdOne.Items[i].FindControl("lbOzn");
							lbOzn.Text = ReplaceCodePhasesText(lbOzn.Text,NewCode,false,IsOneBuilding);
						}
					}
				}
			}
			DataGrid grdNew =(DataGrid) dlBuildings.Items[index].FindControl("grdNew");
			for(int i=0;i<grdNew.Items.Count;i++)
			{
				Label lbOzn = (Label)grdNew.Items[i].FindControl("lbOzn");
				lbOzn.Text = ReplaceCodePhasesText(lbOzn.Text,NewCode,false,IsOneBuilding);
			}
		}
		private string ReplaceCodePhasesText(string text,string New,bool Detail,bool OneBuilding)
		{
			int index = 0;
			if(OneBuilding)
			{
				if((text.Length == 4 && !Detail)||(text.Length == 6 && Detail))
					if(New == "")
						return text;
					else
						return string.Concat(New,RazdelConst,text);
			}
			else
			{
				if((text.Length < 8 && !Detail)||(text.Length < 10 && Detail))
					if(New == "")
						return text;
					else
						return string.Concat(New,RazdelConst,text);
			}
			index = text.IndexOf(RazdelConst);
			if(New == "")
				return text.Substring(2);
				
			return string.Concat(New,RazdelConst,text.Substring(index+1));
		}
		#endregion
		
		#region SaveAndDelete
		
		private void DeleteSelected()
		{
			for(int i=0;i<dlBuildings.Items.Count;i++)
			{
				DataList dlist = (DataList) dlBuildings.Items[i].FindControl("dlBuildingContentArchitect");
				for(int k=1;k<=ArchitectConst;k++)
				{
					ContentsVector cv = GetContentsTable(i+1,k);
					DataGrid grd =(DataGrid) dlist.Items[k-1].FindControl("grdContentArchitect");
					for(int j=grd.Items.Count-1;j>=0;j--)
					{
						CheckBox  cb =(CheckBox) grd.Items[j].FindControl("cbReportCode");
						if(cb.Checked==true)
						{
							cv.RemoveAt(j);
						}
					}
					SetContentsTable(i+1,cv,k);
					if(k==2)
					{
						cv = GetDistrTable(i+1);
						grd =(DataGrid) dlist.Items[1].FindControl("grdDistr");
						for(int j=grd.Items.Count-1;j>=0;j--)
						{
							CheckBox  cb =(CheckBox) grd.Items[j].FindControl("cbReportCode");
							if(cb.Checked==true)
							{
								cv.RemoveAt(j);
							}
						}
						SetDistrTable(i+1,cv);
					}
				}
				ContentsVector cv1 = GetContentsTableNew(i+1);
				DataGrid grdNew =(DataGrid) dlBuildings.Items[i].FindControl("grdNew");
				for(int j=grdNew.Items.Count-1;j>=0;j--)
				{
					CheckBox  cb =(CheckBox) grdNew.Items[j].FindControl("cbReportCode");
					if(cb.Checked==true)
					{
						cv1.RemoveAt(j);
					}
				}
				SetContentsTableNew(i+1,cv1);
			}
			
			BindGrid();
			IsOneOrMany(dlBuildings.Items.Count);
		}

		private void RevisionSave()
		{
			for(int i=0;i<dlBuildings.Items.Count;i++)
			{
				DataList dlist = (DataList) dlBuildings.Items[i].FindControl("dlBuildingContentArchitect");
				for(int k=1;k<=ArchitectConst;k++)
				{
					DataGrid grd =(DataGrid) dlist.Items[k-1].FindControl("grdContentArchitect");
					for(int j=0;j<grd.Items.Count;j++)
					{   
						TextBox txt = (TextBox) grd.Items[j].FindControl("txtRevision");
						if (txt.Text!=null&&txt.Text!="")
						{
							int index = txt.Text.IndexOf("R");
							string Nomer = txt.Text.Substring(index+1);
							txt.Text = string.Concat("R",Nomer);
						}
					}
					if(k==1)
					{
						grd =(DataGrid) dlist.Items[k-1].FindControl("grdDistr");
						for(int j=0;j<grd.Items.Count;j++)
						{   
							TextBox txt = (TextBox) grd.Items[j].FindControl("txtRevision");
							if (txt.Text!=null&&txt.Text!="")
							{
								int index = txt.Text.IndexOf("R");
								string Nomer = txt.Text.Substring(index+1);
								txt.Text = string.Concat("R",Nomer);
							}
						}
					}
				}
				DataGrid grdNew =(DataGrid) dlBuildings.Items[i].FindControl("grdNew");
				for(int j=0;j<grdNew.Items.Count;j++)
				{   
					TextBox txt = (TextBox) grdNew.Items[j].FindControl("txtRevision");
					if (txt.Text!=null&&txt.Text!="")
					{
						int index = txt.Text.IndexOf("R");
						string Nomer = txt.Text.Substring(index+1);
						txt.Text = string.Concat("R",Nomer);
					}
				}
			}
		}
		
		private void SaveClick()
		{	
			if(!CheckTitleOfBuilding())
			{
				lblError.Text = Resource.ResourceManager["ErrorSameTitleNames"];
				lblError.Visible = true;
				return;
			}
				for(int i=0;i<dlBuildings.Items.Count;i++)
				{
					if(IsCodeThere(i))
						ReplaceCodePhases("",i);
				}
			
			RevisionSave();
			int projectID = UIHelpers.GetPIDParam();
			SubprojectsUDL.ExecuteContentsDelByProjectProc(projectID);
			ChecksVector checksOld = new ChecksVector();
			for(int i=0;i<dlBuildings.Items.Count;i++)
			{				
				TextBox txtS=(TextBox)dlBuildings.Items[i].FindControl("txtS");
				DataGrid grdNew =(DataGrid) dlBuildings.Items[i].FindControl("grdNew");
				ContentsVector cv = new ContentsVector();
				cv.AddRange(GetContents(projectID,grdNew,txtS.Text));
				DataList dlist = (DataList) dlBuildings.Items[i].FindControl("dlBuildingContentArchitect");
				for(int k=1;k<=ArchitectConst;k++)
				{
					DataGrid grd =(DataGrid) dlist.Items[k-1].FindControl("grdContentArchitect");
					cv.AddRange(GetContents(projectID,grd,txtS.Text));
					if(k==2)
					{
						grd =(DataGrid) dlist.Items[k-1].FindControl("grdDistr");
						cv.AddRange(GetDistr(projectID,grd,txtS.Text));
					}
				}
				ChecksVector checks = new ChecksVector();
				foreach(ContentData cd in cv) 
				{
					ContentDAL.Save(cd);
					ChecksData checksNew = new ChecksData();
					checksNew.ContID = cd.ContentID;
					checksNew.IsChecked = cd.IsChecked;
					checks.Add(checksNew);
				}
				checksOld.AddRange(checks);
			}	
			this.Session["ChecksTable" + projectID.ToString()] = checksOld;
			ProjectsData.UpdateProjectPhase(projectID, CodeOfPhase());
			LogBind();
			LoadFirst();
		}
		private void LogBind()
		{
			Label lb = new Label();
			DropDownList  ddl = new DropDownList();
			for(int i=0;i<dlBuildings.Items.Count;i++)
			{
				DataList dlist = (DataList) dlBuildings.Items[i].FindControl("dlBuildingContentArchitect");
				for(int k=1;k<=ArchitectConst;k++)
				{
					DataGrid grd =(DataGrid) dlist.Items[k-1].FindControl("grdContentArchitect");
					for(int j=grd.Items.Count-1;j>=0;j--)
					{
						ddl =(DropDownList) grd.Items[j].FindControl("ddlRevision");
						if(ddl.SelectedIndex==1)
						{
							lb = (Label) grd.Items[j].FindControl("lbOzn");
							log.Info( "A revision is added for project " + UIHelpers.GetPIDParam() + " by user "  + LoggedUser.UserID);
						}
					}
					if(k==2)
					{
						
						grd =(DataGrid) dlist.Items[1].FindControl("grdDistr");
						for(int j=grd.Items.Count-1;j>=0;j--)
						{
							ddl =(DropDownList) grd.Items[j].FindControl("ddlRevision");
							if(ddl.SelectedIndex==1)
							{
								lb = (Label) grd.Items[j].FindControl("lbOzn");
								log.Info(Resource.ResourceManager["LogRevision"] + " " + lb.Text + " " + Resource.ResourceManager["LogRevisionChange"] + LoggedUser.FullName);
							}
						}
					}
				}
				DataGrid grdNew =(DataGrid) dlBuildings.Items[i].FindControl("grdNew");
				for(int j=grdNew.Items.Count-1;j>=0;j--)
				{
					ddl =(DropDownList) grdNew.Items[j].FindControl("ddlRevision");
					if(ddl.SelectedIndex==1)
					{
						lb = (Label) grdNew.Items[j].FindControl("lbOzn");
						log.Info(Resource.ResourceManager["LogRevision"] + " " + lb.Text + " " + Resource.ResourceManager["LogRevisionChange"] + LoggedUser.FullName);
					}
				}
			}
		}
		private bool ChecksSave()
		{
			int projectID = UIHelpers.GetPIDParam();
			ChecksVector checks = new ChecksVector();
			for(int i=0;i<dlBuildings.Items.Count;i++)
			{
				DataList dlist = (DataList) dlBuildings.Items[i].FindControl("dlBuildingContentArchitect");
				for(int k=1;k<=ArchitectConst;k++)
				{
					ContentsVector cv = GetContentsTable(i+1,k);
					DataGrid grd =(DataGrid) dlist.Items[k-1].FindControl("grdContentArchitect");
					for(int j=grd.Items.Count-1;j>=0;j--)
					{
						if(cv[j].ContentID!=-1)
						{
							CheckBox  cb =(CheckBox) grd.Items[j].FindControl("cbReportCode");
							ChecksData cd = new ChecksData(cv[j].ContentID,cb.Checked);
							checks.Add(cd);
						}
						else
							return false;
					}
					SetContentsTable(i+1,cv,k);
					if(k==2)
					{
						cv = GetDistrTable(i+1);
						grd =(DataGrid) dlist.Items[k-1].FindControl("grdDistr");
						for(int j=grd.Items.Count-1;j>=0;j--)
						{
							if(cv[j].ContentID!=-1)
							{
								CheckBox  cb =(CheckBox) grd.Items[j].FindControl("cbReportCode");
								ChecksData cd = new ChecksData(cv[j].ContentID,cb.Checked);
								checks.Add(cd);
							}
							else
								return false;
						}
						SetDistrTable(i+1,cv);
					}
				}
				ContentsVector cv1 = GetContentsTableNew(i+1);
				DataGrid grdNew =(DataGrid) dlBuildings.Items[i].FindControl("grdNew");
				for(int j=0;j<grdNew.Items.Count;j++)
				{
					if(cv1[j].ContentID!=-1)
					{
						CheckBox  cb =(CheckBox) grdNew.Items[j].FindControl("cbReportCode");
						ChecksData cd = new ChecksData(cv1[j].ContentID,cb.Checked);
						checks.Add(cd);
					}
					else
						return false;
				}
			}
			this.Session["ChecksTable" + projectID.ToString()] = checks;
			return true;
		}

		private int GridCount(DataGrid dg)
		{
			int Count = 0;
			for(int i=0;i<dg.Items.Count;i++)
			{
				Label lbOzn = (Label) dg.Items[i].FindControl("lbOzn");
				if(lbOzn.Text != "")
				{
					Count++;
				}
			}
			return Count;
		}

		private bool CheckTitleOfBuilding()
		{
			for(int i = dlBuildings.Items.Count-1;i>-1;i--)
			{
				DropDownList ddl1 = (DropDownList) dlBuildings.Items[i].FindControl("txtS1");
				DropDownList ddl2 = (DropDownList) dlBuildings.Items[i].FindControl("txtS2");
				for(int j=0;j<i;j++)
				{
					DropDownList ddl11 = (DropDownList) dlBuildings.Items[j].FindControl("txtS1");
					if(ddl1.SelectedItem.Text == ddl11.SelectedItem.Text)
					{
						DropDownList ddl12 = (DropDownList) dlBuildings.Items[j].FindControl("txtS2");
						if(ddl2.SelectedItem.Text == ddl12.SelectedItem.Text)
							return false;
					}
				}
			}
				return true;
		}
		
		#endregion

		//refresh
		private void RefreshTotals()
		{
			for(int i=0;i<dlBuildings.Items.Count;i++)
			{
			
				DataList dlist = (DataList) dlBuildings.Items[i].FindControl("dlBuildingContentArchitect");
				DataGrid grdDistr =(DataGrid)dlist.Items[1].FindControl("grdDistr");
				decimal a=0;
				for(int j=0;j<grdDistr.Items.Count;j++)
				{    
					TextBox  txtA=(TextBox) grdDistr.Items[j].FindControl("txtA");
				 
					if(txtA.Text!=""&&grdDistr.Items[j].Cells[0].Text!="19")
						a+=UIHelpers.ParseDecimal(txtA.Text);
				}
				
					
				//for(int k=0;i<grdDistr.Items.Count;i++)
				
				Label lblTotal=(Label) dlBuildings.Items[i].FindControl("lblTotal");

				lblTotal.Text=Resource.ResourceManager["lbTotal"]+UIHelpers.FormatDecimal2(a)+" "+ Resource.ResourceManager["Reports_strSquareMeters"];
				
			}
		}
			
		private string GetSignature(int j)
		{
			TextBox txtS=(TextBox) dlBuildings.Items[j-1].FindControl("txtS");
			if(txtS!=null)
				return txtS.Text;
			return "";
		}
		
		private void ddlContentBind(int index)
		{
			ListItem SelectedContent = SelectedContentValue(index);
			DropDownList ddlContents = (DropDownList) dlBuildings.Items[index].FindControl("ddlContents");
			Button btn = (Button) dlBuildings.Items[index].FindControl("btnAdd");
			ddlContents.DataSource = SubprojectsUDL.SelectContentTypes1(GetPlusMinus(index+1,1),GetPlusMinus(index+1,2),GetPlusMinus(index+1,3),GetPlusMinus(index+1,4),GetPlusMinus(index+1,5),GetPlusMinus(index+1,6),GetPlusMinus(index+1,7),GetPlusMinus(index+1,8));
			ddlContents.DataValueField="ContentTypeID";
			ddlContents.DataTextField="ContentType";
			ddlContents.DataBind();
			if(ddlContents.Items.Count!=0)
			{
				ddlContents.Visible = true;
				btn.Visible = true;
			}
			else
			{
				ddlContents.Visible = false;
				btn.Visible = false;
			}
			if(	ddlContents.Items.Contains(SelectedContent))
			{
				int ind = ddlContents.Items.IndexOf(SelectedContent);
				ddlContents.SelectedIndex = ind;
			}
		}

	
		#region dlBuildings
		
			
		private void btnPlusMultipleClick(int index,int secondindex)
		{
				DataList dlist = (DataList) dlBuildings.Items[index-1].FindControl("dlBuildingContentArchitect");
				Button 	btnPlus = (Button) dlist.Items[secondindex-1].FindControl("btnPlusMultiple");
				btnPlus.Visible = false;
				Label 	lbPlusCount = (Label) dlist.Items[secondindex-1].FindControl("lbGridNameCount");
				lbPlusCount.Visible = false;
				Button 	btnMinus = (Button) dlist.Items[secondindex-1].FindControl("btnMinusMultiple");
				btnMinus.Visible = true;
				DataGrid dg = (DataGrid) dlist.Items[secondindex-1].FindControl("grdContentArchitect");
				dg.Visible = true;
				lbPlusCount.Text = string.Concat(Resource.ResourceManager["dlBuildingContentCount"],GridCount(dg));
				SetPlusMinus(index,secondindex,true);
				Label lbPercentFinished = (Label) dlist.Items[secondindex-1].FindControl("lbPercentFinished");
				lbPercentFinished.Visible = false;
			if(secondindex == 2)
			{
				DataGrid dg1 = (DataGrid) dlist.Items[secondindex-1].FindControl("grdDistr");
				dg1.Visible = true;
				lbPlusCount.Text = string.Concat(Resource.ResourceManager["dlBuildingContentCount"],GridCount(dg)+GridCount(dg1));
				}
//			}
			IsOneOrMany(dlBuildings.Items.Count);
			ddlContentBind(index-1);
		}
		private void btnMinusMultipleClick(int index,int secondindex)
		{
				DataList dlist = (DataList) dlBuildings.Items[index-1].FindControl("dlBuildingContentArchitect");
				Button 	btnPlus = (Button) dlist.Items[secondindex-1].FindControl("btnPlusMultiple");
				btnPlus.Visible = true;
				Label 	lbPlusCount = (Label) dlist.Items[secondindex-1].FindControl("lbGridNameCount");
				lbPlusCount.Visible = true;
				Button 	btnMinus = (Button) dlist.Items[secondindex-1].FindControl("btnMinusMultiple");
				btnMinus.Visible = false;
				DataGrid dg = (DataGrid) dlist.Items[secondindex-1].FindControl("grdContentArchitect");
				dg.Visible = false;
				lbPlusCount.Text = string.Concat(Resource.ResourceManager["dlBuildingContentCount"],GridCount(dg));
				SetPlusMinus(index,secondindex,false);
			Label lbPercentFinished = (Label) dlist.Items[secondindex-1].FindControl("lbPercentFinished");
			lbPercentFinished.Visible = true;
			if(dg.Items.Count!=0)
			{
				
				lbPercentFinished.Text = string.Concat(Resource.ResourceManager["PercentFinished"],PercentSum(dg),"%");
			}
			else
			{
				
				lbPercentFinished.Text = string.Concat(Resource.ResourceManager["PercentFinished"],"0%");
			}
		
			if(secondindex == 2)
			{
				DataGrid dg1 = (DataGrid) dlist.Items[secondindex-1].FindControl("grdDistr");
				dg1.Visible = false;
				lbPlusCount.Text = string.Concat(Resource.ResourceManager["dlBuildingContentCount"],GridCount(dg)+GridCount(dg1));
				if(dg1.Items.Count!=0)
				{
					lbPercentFinished.Text = string.Concat(Resource.ResourceManager["PercentFinished"],PercentSum(dg,dg1),"%");
					lbPercentFinished.Visible = true;
				}
			}
//			}
			IsOneOrMany(dlBuildings.Items.Count);
			ddlContentBind(index-1);
		}
		private void AllPlusClick()
		{
			int nMaxBuildings=int.Parse(System.Configuration.ConfigurationSettings.AppSettings["MaxBuildings"]);
			for(int i=0;i<nMaxBuildings;i++)
			{
				for (int k=1;k<=ArchitectConst;k++)
				{
					SetPlusMinus(i+1,k,false);
				}
			}
			}
		private void NameOfContent()
		{
			for(int i=0;i<dlBuildings.Items.Count;i++)
			{
				ArrayList al = new ArrayList();
				for(int nMenu=0;nMenu<Enum.GetNames(typeof(BuildingGridTitleNames1)).Length;nMenu++)
				{
					al.Add(Enum.GetNames(typeof(BuildingGridTitleNames1)).GetValue(nMenu));
				}
				DropDownList dll = (DropDownList) dlBuildings.Items[i].FindControl("txtS1");
				dll.DataSource = al;
				dll.DataBind();
				al = new ArrayList();
				al.Add(string.Empty);
				for(int nMenu=0;nMenu<9;nMenu++)
				{
					al.Add(nMenu+1);
				}
				dll = (DropDownList) dlBuildings.Items[i].FindControl("txtS2");
				dll.DataSource = al;
				dll.DataBind();
				
			}
		}
		private bool CheckContentTitle(string S1,int index,bool All)
		{
			int indexNew = index;
			if(All)
				indexNew = dlBuildings.Items.Count;
			for(int i=0;i<index;i++)
			{
				if(index!=i)
				{
					TextBox dll = (TextBox) dlBuildings.Items[i].FindControl("txtS");
					string S1Other = dll.Text;
					if(S1 == S1Other)
						return false;
				}
			}
			return true;
		}
		private void MakeNewContentTitle(int index,bool All)
		{
			int indexNew= index;
			if(All)
				indexNew = dlBuildings.Items.Count;
			DropDownList dll1 = (DropDownList) dlBuildings.Items[index].FindControl("txtS1");
			DropDownList dll2 = (DropDownList) dlBuildings.Items[index].FindControl("txtS2");
			for(int j=0;j<dll1.Items.Count;j++)
			{
				for(int k=0;k<dll2.Items.Count;k++)
				{
					bool DaNe = true;
					dll1.SelectedIndex = j;
					dll2.SelectedIndex = k;
					string TitleNew = string.Concat(dll1.SelectedValue,dll2.SelectedValue);		
					for(int i=0;i<indexNew;i++)
					{
						if(i!=index)
						{
							TextBox dll = (TextBox) dlBuildings.Items[i].FindControl("txtS");
							string S1Other = dll.Text;
							if(TitleNew == S1Other)
								DaNe = false;
						}
					}
					if(DaNe)
					{
						dll1.SelectedIndex = j;
						dll2.SelectedIndex = k;
						TextBox dll = (TextBox) dlBuildings.Items[index].FindControl("txtS");
						dll.Text = string.Concat(dll1.SelectedValue,dll2.SelectedValue);
						return;
					}   
				}
			}
			return;
		}
		private void btnPlusAllClick(int index)
		{
			for(int k=0; k<ArchitectConst;k++)
			{
				btnPlusMultipleClick(index,k+1);
			}
		}
		private void btnMinusAllClick(int index)
		{
			for(int k=0;k<ArchitectConst;k++)
			{
				btnMinusMultipleClick(index,k+1);
			}

		}
		private void BuildingNewClick()
		{
			int projectID = UIHelpers.GetPIDParam();
			for(int i=0;i<dlBuildings.Items.Count;i++)
			{
				DataGrid grdDistr = new DataGrid();
				DataGrid[] grd = new DataGrid[ArchitectConst];
				DataList dlist = (DataList) dlBuildings.Items[i].FindControl("dlBuildingContentArchitect");
				for(int k=1;k<=ArchitectConst;k++)
				{

					DataGrid grdOne =(DataGrid) dlist.Items[k-1].FindControl("grdContentArchitect");
					grd[k-1]=grdOne;
					if(k==2)
					{
						grdDistr =(DataGrid) dlist.Items[k-1].FindControl("grdDistr");
					}
				}
				DataGrid grdNew =(DataGrid) dlBuildings.Items[i].FindControl("grdNew");
				ContentsVector cv = new ContentsVector();
				TextBox txtS=(TextBox) dlBuildings.Items[i].FindControl("txtS");
				SetDistrTable(i+1, GetDistr(projectID,grdDistr,txtS.Text));
				for(int k=1;k<=ArchitectConst;k++)
				{
					
					SetContentsTable(i+1,GetContents(projectID,grd[k-1],txtS.Text),k);
				}
				SetContentsTableNew(i+1,GetContents(projectID,grdNew,txtS.Text));		
			}
			ArrayList al = new ArrayList();
			for(int i=1;i<=dlBuildings.Items.Count+1;i++)
			{
				al.Add(i);		
			}
			dlBuildings.DataSource=al;
			dlBuildings.DataBind();
			IsOneOrMany(al.Count);
			
			NameOfContent();
			BindGrid();
			if(dlBuildings.Items.Count == 2)
			{
				if(IsCodeThere(0))
				{
					DropDownList txtS1 = (DropDownList)dlBuildings.Items[0].FindControl("txtS1");
					DropDownList txtS2 = (DropDownList)dlBuildings.Items[0].FindControl("txtS2");
					ReplaceCode(string.Concat(txtS1.SelectedItem.Text,txtS2.SelectedItem.Text) ,0);
				}
			}
		}
		private void BuildingNewCopyClick()
		{
			int projectID = UIHelpers.GetPIDParam();
			for(int i=0;i<dlBuildings.Items.Count;i++)
			{
					DataGrid grdDistr = new DataGrid();
				DataGrid[] grd = new DataGrid[ArchitectConst];
				DataList dlist = (DataList) dlBuildings.Items[i].FindControl("dlBuildingContentArchitect");
				for(int k=1;k<=ArchitectConst;k++)
				{

					DataGrid grdOne =(DataGrid) dlist.Items[k-1].FindControl("grdContentArchitect");
					grd[k-1]=grdOne;
					if(k==2)
					{
						grdDistr =(DataGrid) dlist.Items[k-1].FindControl("grdDistr");
					}
				}
				DataGrid grdNew =(DataGrid) dlBuildings.Items[i].FindControl("grdNew");
				ContentsVector cv = new ContentsVector();
				TextBox txtS=(TextBox) dlBuildings.Items[i].FindControl("txtS");
				SetDistrTable(i+1, GetDistr(projectID,grdDistr,txtS.Text));
				for(int k=1;k<=ArchitectConst;k++)
				{
					SetContentsTable(i+1,GetContents(projectID,grd[k-1],txtS.Text),k);
				}
				SetContentsTableNew(i+1,GetContents(projectID,grdNew,txtS.Text));		
			}
			int i1=dlBuildings.Items.Count;
			DataGrid grdDistr1 =new DataGrid();		
			DataGrid[] grd1 = new DataGrid[ArchitectConst];
			DataList dlist1 = (DataList) dlBuildings.Items[i1-1].FindControl("dlBuildingContentArchitect");
			for(int k=1;k<=ArchitectConst;k++)
			{
				DataGrid grdOne =(DataGrid) dlist1.Items[k-1].FindControl("grdContentArchitect");
				grd1[k-1]=grdOne;
				if(k==2)
				{
					grdDistr1 =(DataGrid) dlist1.Items[k-1].FindControl("grdDistr");
				}
			}
			DataGrid grdNew1 =(DataGrid) dlBuildings.Items[i1-1].FindControl("grdNew");
			TextBox txtS1 =(TextBox) dlBuildings.Items[i1-1].FindControl("txtS");
			ContentsVector cvector = GetDistr(projectID,grdDistr1,txtS1.Text);
			foreach(ContentData cd in cvector)
				cd.BuildingNumber++;
			SetDistrTable(i1+1, cvector);
			for(int k=1;k<=ArchitectConst;k++)
			{
				cvector = GetContents(projectID,grd1[k-1],txtS1.Text);
				foreach(ContentData cd in cvector)
					cd.BuildingNumber++;
				SetContentsTable(i1+1,cvector,k);
			}
			cvector = GetContents(projectID,grdNew1,txtS1.Text);
			foreach(ContentData cd in cvector)
				cd.BuildingNumber++;
			SetContentsTableNew(i1+1,cvector);
			ArrayList al = new ArrayList();
			for(int i=1;i<=dlBuildings.Items.Count+1;i++)
			{
				al.Add(i);		
			}
			dlBuildings.DataSource=al;
			dlBuildings.DataBind();	
			IsOneOrMany(al.Count);
			
			NameOfContent();
			BindGrid();
			txtS1 =(TextBox) dlBuildings.Items[i1].FindControl("txtS");
			if(IsCodeThere(al.Count-1))
				ReplaceCode(txtS1.Text,i1);
			if(dlBuildings.Items.Count == 2)
			{
				if(IsCodeThere(0))
				{
					DropDownList txtS11 = (DropDownList)dlBuildings.Items[0].FindControl("txtS1");
					DropDownList txtS12 = (DropDownList)dlBuildings.Items[0].FindControl("txtS2");
					ReplaceCode(string.Concat(txtS11.SelectedItem.Text,txtS12.SelectedItem.Text) ,0);
				}
			}
		}
		
		private void MakeCurrentOznToGrids(int nIndex)
		{
			if (!IsCodeThere(nIndex))
			{
				DataSet dsPrefix = SubprojectsUDL.SelectContentsPrefix();
				
				TextBox txtS = (TextBox) dlBuildings.Items[nIndex].FindControl("txtS");
				DataList dlist =(DataList) dlBuildings.Items[nIndex].FindControl("dlBuildingContentArchitect");
				for(int k=0;k<ArchitectConst;k++)
				{
					DataGrid grd =(DataGrid) dlist.Items[k].FindControl("grdContentArchitect");
					DataGrid grdDistr =(DataGrid) dlist.Items[1].FindControl("grdDistr");
					if(k==1)
					{
						for(int j=0;j<grdDistr.Items.Count;j++)
						{  
							if(grdDistr.Items[j].Cells[0].Text!="19")
							{
								Label lbOzn = (Label) grdDistr.Items[j].FindControl("lbOzn");
								string s = GetPrefix(grdDistr.Items[j].Cells[0].Text,dsPrefix);
								if(dlBuildings.Items.Count == 1)
								{
									int maxsgn1 = maxSgn(grdDistr,string.Concat(s,RazdelConst));
									int maxsgn = maxSgn(grd,string.Concat(s,RazdelConst));
									if(maxsgn1 > maxsgn) maxsgn = maxsgn1;
									string sPadded=(maxsgn+1).ToString().PadLeft(2,'0');
									lbOzn.Text = string.Concat(s,RazdelConst,sPadded);
								}
								else
								{
									int maxsgn1 = maxSgn(grdDistr,string.Concat(txtS.Text,RazdelConst,s,RazdelConst));
									int maxsgn = maxSgn(grd,string.Concat(txtS.Text,RazdelConst,s,RazdelConst));
									if(maxsgn1 > maxsgn) maxsgn = maxsgn1;
									string sPadded=(maxsgn+1).ToString().PadLeft(2,'0');
									lbOzn.Text = string.Concat(txtS.Text,RazdelConst,s,RazdelConst,sPadded);
								}
							}
						}
					}
					for(int j=0;j<grd.Items.Count;j++)
					{  
						Label lbOzn = (Label) grd.Items[j].FindControl("lbOzn");
						string s = GetPrefix(grd.Items[j].Cells[0].Text,dsPrefix);
						if(dlBuildings.Items.Count == 1)
						{
							int maxsgn = maxSgn(grd,string.Concat(s,RazdelConst));
							if(k==1)
							{
								int maxsgn1 = maxSgn(grdDistr,string.Concat(s,RazdelConst));
								if(maxsgn1 > maxsgn) maxsgn = maxsgn1;
							}
							string sPadded=(maxsgn+1).ToString().PadLeft(2,'0');
							lbOzn.Text = string.Concat(s,RazdelConst,sPadded);
						}
						else
						{
							int maxsgn = maxSgn(grd,string.Concat(txtS.Text,RazdelConst,s,RazdelConst));
							if(k==1)
							{
								int maxsgn1 = maxSgn(grdDistr,string.Concat(txtS.Text,RazdelConst,s,RazdelConst));
								if(maxsgn1 > maxsgn) maxsgn = maxsgn1;
							}
							string sPadded=(maxsgn+1).ToString().PadLeft(2,'0');
							lbOzn.Text = string.Concat(txtS.Text,RazdelConst,s,RazdelConst,sPadded);
						}
						if(k==5)
						{
							if( (grd.Items[j].Cells[0].Text == System.Configuration.ConfigurationSettings.AppSettings["DetailV"]))
							{
								lbOzn.Text = string.Concat(lbOzn.Text,RazdelConst,"V");
							}
							if( (grd.Items[j].Cells[0].Text == System.Configuration.ConfigurationSettings.AppSettings["DetailH"]))
							{
								lbOzn.Text = string.Concat(lbOzn.Text,RazdelConst,"H");
							}
						}
					}
				}
				DataGrid grdNew =(DataGrid) dlBuildings.Items[nIndex].FindControl("grdNew");
				for(int j=0;j<grdNew.Items.Count;j++)
				{   
					Label lbOzn = (Label) grdNew.Items[j].FindControl("lbOzn");
					string s = GetPrefix(grdNew.Items[j].Cells[0].Text,dsPrefix);
					if(dlBuildings.Items.Count == 1)
					{
						int maxsgn = maxSgn(grdNew,string.Concat(s,RazdelConst));
						string sPadded=(maxsgn+1).ToString().PadLeft(2,'0');
						lbOzn.Text = string.Concat(s,RazdelConst,sPadded);
					}
					else
					{
						int maxsgn = maxSgn(grdNew,string.Concat(txtS.Text,RazdelConst,s,RazdelConst));
						string sPadded=(maxsgn+1).ToString().PadLeft(2,'0');
						lbOzn.Text = string.Concat(txtS.Text,RazdelConst,s,RazdelConst,sPadded);
					}
				}
				
				SaveClick();
			
				
				ReplaceCodePhases(CodeOfPhase(),nIndex);
				
			}
			else
			{
				lblError.Text = Resource.ResourceManager["errorCodeExist"];
			}
		}
		private void CanselCurrentOznToGrids(int nIndex)
		{
			
				DataList dlist =(DataList) dlBuildings.Items[nIndex].FindControl("dlBuildingContentArchitect");
				for(int k=0;k<ArchitectConst;k++)
				{
					DataGrid grd =(DataGrid) dlist.Items[k].FindControl("grdContentArchitect");
					for(int j=0;j<grd.Items.Count;j++)
					{   
						Label lbOzn = (Label) grd.Items[j].FindControl("lbOzn");
						lbOzn.Text = "";
					}
					if(k==1)
					{
						grd =(DataGrid) dlist.Items[k].FindControl("grdDistr");
						for(int j=0;j<grd.Items.Count;j++)
						{   
							Label lbOzn = (Label) grd.Items[j].FindControl("lbOzn");
							lbOzn.Text = "";
						}
					}
				}
				DataGrid grdNew =(DataGrid) dlBuildings.Items[nIndex].FindControl("grdNew");
				for(int j=0;j<grdNew.Items.Count;j++)
				{   
					Label lbOzn = (Label) grdNew.Items[j].FindControl("lbOzn");
					lbOzn.Text = "";
				}
			
			SaveClick();
		}
	
		private void ddlContentFinishedBind(DropDownList ddl)
		{
			ddl.Items.Add("0%");
			ddl.Items.Add("10%");
			ddl.Items.Add("20%");
			ddl.Items.Add("30%");
			ddl.Items.Add("40%");
			ddl.Items.Add("50%");
			ddl.Items.Add("60%");
			ddl.Items.Add("70%");
			ddl.Items.Add("80%");
			ddl.Items.Add("90%");
			ddl.Items.Add("100%");
			ddl.DataBind();
			ddl.SelectedIndex = 0;
		}
		private void ddlPhasesBind()
		{

			ddlPhases.Items.Add(Resource.ResourceManager["Reports_IdeenProekt"]);
			ddlPhases.Items.Add(Resource.ResourceManager["Reports_TehnicheskiProekt"]);
			ddlPhases.Items.Add(Resource.ResourceManager["ItemWorkProject"]);
			ddlPhases.DataBind();
			int PID=UIHelpers.GetPIDParam();
			string PhaseCode = "";
			SqlDataReader reader = null;
			try
			{
				reader = ProjectsData.SelectProject(PID);
				if(reader.Read())
				{
					if(!reader.IsDBNull(31))
						PhaseCode = reader.GetString(31);
				}
			}
			catch (Exception ex)
			{
				log.Info(ex);
				return;
			}

			finally 
			{ 
				if (reader!=null) reader.Close(); 
			}
			switch (PhaseCode)
			{
				case "P":ddlPhases.SelectedIndex = 0;return;
				case "A":ddlPhases.SelectedIndex = 1;return;
				case "C":ddlPhases.SelectedIndex = 2;return;
				default:ddlPhases.SelectedIndex = 0;return;
			}
			
		}

		private int ddlContentSelectedValue(DropDownList ddl,int index)
		{
			try
			{
				string percent = ddl.Items[index].Text;
				string percentNew = percent.Substring(0,percent.Length-1);
				return int.Parse(percentNew);
			}
			catch
			{
				return 0;
			}
				}
		private void HideOthers()
		{
			int VisiblePhase = ddlPhases.SelectedIndex;
			for(int i=0;i<dlBuildings.Items.Count;i++)
			{
				DataList dlist =(DataList) dlBuildings.Items[i].FindControl("dlBuildingContentArchitect");
				for(int k=0;k<ArchitectConst;k++)
				{
					DataGrid grd =(DataGrid) dlist.Items[k].FindControl("grdContentArchitect");
					for(int j=0;j<grd.Items.Count;j++)
					{   
						switch(VisiblePhase)
						{
							case 0:
							{
								DropDownList ddl = (DropDownList) grd.Items[j].FindControl("ddlContentFinishedPhase1");
								ddl.Visible=true; 
								ddl = (DropDownList) grd.Items[j].FindControl("ddlContentFinishedPhase2");
								ddl.Visible=false; 
								ddl = (DropDownList) grd.Items[j].FindControl("ddlContentFinishedPhase3");
								ddl.Visible=false; break;
							}
							case 1:
							{
								DropDownList ddl = (DropDownList) grd.Items[j].FindControl("ddlContentFinishedPhase1");
								ddl.Visible=false; 
								ddl = (DropDownList) grd.Items[j].FindControl("ddlContentFinishedPhase2");
								ddl.Visible=true; 
								ddl = (DropDownList) grd.Items[j].FindControl("ddlContentFinishedPhase3");
								ddl.Visible=false; break;
							}
							case 2:
							{
								DropDownList ddl = (DropDownList) grd.Items[j].FindControl("ddlContentFinishedPhase1");
								ddl.Visible=false; 
								ddl = (DropDownList) grd.Items[j].FindControl("ddlContentFinishedPhase2");
								ddl.Visible=false; 
								ddl = (DropDownList) grd.Items[j].FindControl("ddlContentFinishedPhase3");
								ddl.Visible=true; break;
							}
						}
					}
					if(k==1)
					{
						grd =(DataGrid) dlist.Items[k].FindControl("grdDistr");
						for(int j=0;j<grd.Items.Count;j++)
						{   
							switch(VisiblePhase)
							{
								case 0:
								{
									DropDownList ddl = (DropDownList) grd.Items[j].FindControl("ddlContentFinishedPhase1");
									ddl.Visible=true; 
									ddl = (DropDownList) grd.Items[j].FindControl("ddlContentFinishedPhase2");
									ddl.Visible=false; 
									ddl = (DropDownList) grd.Items[j].FindControl("ddlContentFinishedPhase3");
									ddl.Visible=false; 
									if(grd.Items[j].Cells[0].Text == "19")
									{
										ddl = (DropDownList) grd.Items[j].FindControl("ddlContentFinishedPhase1");
										ddl.Visible=false; 
									}break;
								}
								case 1:
								{
									DropDownList ddl = (DropDownList) grd.Items[j].FindControl("ddlContentFinishedPhase1");
									ddl.Visible=false; 
									ddl = (DropDownList) grd.Items[j].FindControl("ddlContentFinishedPhase2");
									ddl.Visible=true; 
									ddl = (DropDownList) grd.Items[j].FindControl("ddlContentFinishedPhase3");
									ddl.Visible=false;
									if(grd.Items[j].Cells[0].Text == "19")
									{
										ddl = (DropDownList) grd.Items[j].FindControl("ddlContentFinishedPhase2");
										ddl.Visible=false; 
									} break;
								}
								case 2:
								{
									DropDownList ddl = (DropDownList) grd.Items[j].FindControl("ddlContentFinishedPhase1");
									ddl.Visible=false; 
									ddl = (DropDownList) grd.Items[j].FindControl("ddlContentFinishedPhase2");
									ddl.Visible=false; 
									ddl = (DropDownList) grd.Items[j].FindControl("ddlContentFinishedPhase3");
									ddl.Visible=true; 
									if(grd.Items[j].Cells[0].Text == "19")
									{
										ddl.Visible=false; 
									}
									break;
								}
							}
						}
					}
				
				}
				DataGrid grdNew =(DataGrid) dlBuildings.Items[i].FindControl("grdNew");
				for(int j=0;j<grdNew.Items.Count;j++)
				{   
					switch(VisiblePhase)
					{
						case 0:
						{
							DropDownList ddl = (DropDownList) grdNew.Items[j].FindControl("ddlContentFinishedPhase1");
							ddl.Visible=true; 
							ddl = (DropDownList) grdNew.Items[j].FindControl("ddlContentFinishedPhase2");
							ddl.Visible=false; 
							ddl = (DropDownList) grdNew.Items[j].FindControl("ddlContentFinishedPhase3");
							ddl.Visible=false; break;
						}
						case 1:
						{
							DropDownList ddl = (DropDownList) grdNew.Items[j].FindControl("ddlContentFinishedPhase1");
							ddl.Visible=false; 
							ddl = (DropDownList) grdNew.Items[j].FindControl("ddlContentFinishedPhase2");
							ddl.Visible=true; 
							ddl = (DropDownList) grdNew.Items[j].FindControl("ddlContentFinishedPhase3");
							ddl.Visible=false; break;
						}
						case 2:
						{
							DropDownList ddl = (DropDownList) grdNew.Items[j].FindControl("ddlContentFinishedPhase1");
							ddl.Visible=false; 
							ddl = (DropDownList) grdNew.Items[j].FindControl("ddlContentFinishedPhase2");
							ddl.Visible=false; 
							ddl = (DropDownList) grdNew.Items[j].FindControl("ddlContentFinishedPhase3");
							ddl.Visible=true; break;
						}
					}
				}
			}
		}
		private string CodeOfPhase()
		{
			int VisiblePhase = ddlPhases.SelectedIndex;
			switch(VisiblePhase)
			{
				case 0:return "P";
				case 1:return "A";
				case 2:return "C";
				default:return "";
			}
		}
		
		private int PercentSum(DataGrid dg)
		{
			int sum = 0;
			DropDownList ddl = new DropDownList();
			for(int i=0;i<dg.Items.Count;i++)
			{
				switch(ddlPhases.SelectedIndex)
				{
					case 0: ddl = (DropDownList) dg.Items[i].FindControl("ddlContentFinishedPhase1");break;
					case 1: ddl = (DropDownList) dg.Items[i].FindControl("ddlContentFinishedPhase2");break;
					case 2: ddl = (DropDownList) dg.Items[i].FindControl("ddlContentFinishedPhase3");break;
				}
				sum += ddlContentSelectedValue(ddl,ddl.SelectedIndex);
			}
			try
			{
				return sum/dg.Items.Count;
			}
			catch
			{
				return 0;
			}
		}
		private int PercentSum(DataGrid dg,DataGrid dg1)
		{
			int kvadratura = 0;
			int sum = 0;
			DropDownList ddl = new DropDownList();
			for(int i=0;i<dg.Items.Count;i++)
			{
				switch(ddlPhases.SelectedIndex)
				{
					case 0: ddl = (DropDownList) dg.Items[i].FindControl("ddlContentFinishedPhase1");break;
					case 1: ddl = (DropDownList) dg.Items[i].FindControl("ddlContentFinishedPhase2");break;
					case 2: ddl = (DropDownList) dg.Items[i].FindControl("ddlContentFinishedPhase3");break;
				}
				sum += ddlContentSelectedValue(ddl,ddl.SelectedIndex);
			}
			for(int i=0;i<dg1.Items.Count;i++)
			{
				if(dg1.Items[i].Cells[0].Text == "19")
					kvadratura++;
				switch(ddlPhases.SelectedIndex)
				{
					case 0: ddl = (DropDownList) dg1.Items[i].FindControl("ddlContentFinishedPhase1");break;
					case 1: ddl = (DropDownList) dg1.Items[i].FindControl("ddlContentFinishedPhase2");break;
					case 2: ddl = (DropDownList) dg1.Items[i].FindControl("ddlContentFinishedPhase3");break;
				}
				sum += ddlContentSelectedValue(ddl,ddl.SelectedIndex);
			}
			try
			{
				return sum/(dg.Items.Count+dg1.Items.Count-kvadratura);
			}
			catch
			{
				return 0;
			}
		}
		
		
		#endregion
			
		#region EventHandlers
			
		private void btnDeleteSelected_Click(object sender, System.EventArgs e)
		{
			DeleteSelected();
		}
		private void btnCancel_Click_1(object sender, System.EventArgs e)
		{
			ChecksSave();
			Response.Redirect("EditProject.aspx?pid="+UIHelpers.GetPIDParam());
		}
		private void btnSave_Click(object sender, System.EventArgs e)
		{
			SaveClick();
		}
				
		private void grdDistr_ItemCommand(object source, DataGridCommandEventArgs e)
		{
			int projectID = UIHelpers.GetPIDParam();
			int nNum =int.Parse(e.Item.Cells[12].Text);
			DataList dlist =(DataList) dlBuildings.Items[nNum-1].FindControl("dlBuildingContentArchitect");
			DataGrid dg = (DataGrid) source;
			if(e.CommandName == "Up")
			{
//				DataGrid dg =(DataGrid) dlBuildings.Items[nNum-1].FindControl("grdDistr");
				int ii = e.Item.ItemIndex;	
				TextBox txtS=(TextBox) dlBuildings.Items[dg.TabIndex-1].FindControl("txtS");
				string sign="";
				if(txtS!=null)
					sign=txtS.Text;
				ContentsVector DistrTable=GetDistr(projectID,dg,sign);
				ContentData cd = DistrTable[ii];
				ContentData cdUp = DistrTable[ii-1];
				DistrTable.RemoveAt(ii);
				DistrTable.Insert(ii,cdUp);
				DistrTable.RemoveAt(ii-1);
				DistrTable.Insert(ii-1,cd);
				SetDistrTable(nNum,DistrTable);
				
				BindGrid();
				
				return;
			}
			if(e.CommandName ==	"Down")
			{
//				DataGrid dg =(DataGrid) dlBuildings.Items[nNum-1].FindControl("grdDistr");
				int ii = e.Item.ItemIndex;				
				TextBox txtS=(TextBox) dlBuildings.Items[dg.TabIndex-1].FindControl("txtS");
				string sign="";
				if(txtS!=null)
					sign=txtS.Text;
				ContentsVector DistrTable=GetDistr(projectID,dg,sign);
				ContentData cd = DistrTable[ii];
				ContentData cdDown = DistrTable[ii+1];
				DistrTable.RemoveAt(ii);
				DistrTable.Insert(ii,cdDown);
				DistrTable.RemoveAt(ii+1);
				DistrTable.Insert(ii+1,cd);
				SetDistrTable(nNum,DistrTable);
				BindGrid();
				return;
			}
			if(e.CommandName == "Delete")
			{					
//				DataGrid g = (DataGrid) source;
				TextBox txtS=(TextBox) dlBuildings.Items[dg.TabIndex-1].FindControl("txtS");
				string sign="";
				if(txtS!=null)
					sign=txtS.Text;
				ContentsVector DistrTable=GetDistr(projectID,dg,sign);
				DistrTable.RemoveAt(e.Item.ItemIndex);
				SetDistrTable(nNum,DistrTable);
				BindGrid();
				return;
			}
		}
		private void grdContentArchitect_ItemCommand(object source, DataGridCommandEventArgs e)
		{
			int projectID = UIHelpers.GetPIDParam();
			int nNum =int.Parse(e.Item.Cells[11].Text);
			DataList dlist =(DataList) dlBuildings.Items[nNum-1].FindControl("dlBuildingContentArchitect");
			DataGrid g = (DataGrid) source;

			int Filtar = 1;
			for(int k=0;k<ArchitectConst;k++)
			{
				if(g == (DataGrid) dlist.Items[k].FindControl("grdContentArchitect"))
					Filtar = k+1;
			}
			switch(e.CommandName)
			{
				case "Delete":
				{
					TextBox txtS=(TextBox) dlBuildings.Items[g.TabIndex-1].FindControl("txtS");
					string sign="";
					if(txtS!=null)
						sign=txtS.Text;
					ContentsVector DistrTable=GetContents(projectID,g,sign);
					DistrTable.RemoveAt(e.Item.ItemIndex);
					SetContentsTable(nNum,DistrTable,Filtar);
					BindGrid();
					return;
				}
				case "Up":
				{
					int ii = e.Item.ItemIndex;	
					TextBox txtS=(TextBox) dlBuildings.Items[g.TabIndex-1].FindControl("txtS");
					string sign="";
					if(txtS!=null)
						sign=txtS.Text;
					ContentsVector DistrTable=GetContents(projectID,g,sign);
					ContentData cd = DistrTable[ii];
					ContentData cdUp = DistrTable[ii-1];
					DistrTable.RemoveAt(ii);
					DistrTable.Insert(ii,cdUp);
					DistrTable.RemoveAt(ii-1);
					DistrTable.Insert(ii-1,cd);
					SetContentsTable(nNum,DistrTable,Filtar);
					BindGrid();
					return;
				}
				case "Down":
				{
					int ii = e.Item.ItemIndex;				
					TextBox txtS=(TextBox) dlBuildings.Items[g.TabIndex-1].FindControl("txtS");
					string sign="";
					if(txtS!=null)
						sign=txtS.Text;
					ContentsVector DistrTable=GetContents(projectID,g,sign);
					ContentData cd = DistrTable[ii];
					ContentData cdDown = DistrTable[ii+1];
					DistrTable.RemoveAt(ii);
					DistrTable.Insert(ii,cdDown);
					DistrTable.RemoveAt(ii+1);
					DistrTable.Insert(ii+1,cd);
					SetContentsTable(nNum,DistrTable,Filtar);
					BindGrid();
					return;
				}
			}
		}
		private void grdNew_ItemCommand(object source, DataGridCommandEventArgs e)
		{
			int projectID = UIHelpers.GetPIDParam();
			int nNum =int.Parse(e.Item.Cells[11].Text);
			switch(e.CommandName)
			{
				case "Delete":
				{
					DataGrid g = (DataGrid) source;
					TextBox txtS=(TextBox) dlBuildings.Items[g.TabIndex-1].FindControl("txtS");
					string sign="";
					if(txtS!=null)
						sign=txtS.Text;
					ContentsVector DistrTable=GetContents(projectID,g,sign);
					DistrTable.RemoveAt(e.Item.ItemIndex);
					SetContentsTableNew(nNum,DistrTable);
					BindGrid();
					return;
				}
				case "Up":
				{
					DataGrid dg =(DataGrid) dlBuildings.Items[nNum-1].FindControl("grdNew");
					int ii = e.Item.ItemIndex;	
					TextBox txtS=(TextBox) dlBuildings.Items[dg.TabIndex-1].FindControl("txtS");
					string sign="";
					if(txtS!=null)
						sign=txtS.Text;
					ContentsVector DistrTable=GetContents(projectID,dg,sign);
					ContentData cd = DistrTable[ii];
					ContentData cdUp = DistrTable[ii-1];
					DistrTable.RemoveAt(ii);
					DistrTable.Insert(ii,cdUp);
					DistrTable.RemoveAt(ii-1);
					DistrTable.Insert(ii-1,cd);
					SetContentsTableNew(nNum,DistrTable);
					BindGrid();
					return;
				}
				case "Down":
				{
					DataGrid dg =(DataGrid) dlBuildings.Items[nNum-1].FindControl("grdNew");
					int ii = e.Item.ItemIndex;				
					TextBox txtS=(TextBox) dlBuildings.Items[dg.TabIndex-1].FindControl("txtS");
					string sign="";
					if(txtS!=null)
						sign=txtS.Text;
					ContentsVector DistrTable=GetContents(projectID,dg,sign);
					ContentData cd = DistrTable[ii];
					ContentData cdDown = DistrTable[ii+1];
					DistrTable.RemoveAt(ii);
					DistrTable.Insert(ii,cdDown);
					DistrTable.RemoveAt(ii+1);
					DistrTable.Insert(ii+1,cd);
					SetContentsTableNew(nNum,DistrTable);
					BindGrid();
					return;
				}
			}
		}

		private void dlBuildings_ItemCreated(object sender, System.Web.UI.WebControls.DataListItemEventArgs e)
		{
			DataList dlist =(DataList)e.Item.FindControl("dlBuildingContentArchitect");
			dlist.ItemCommand+=new DataListCommandEventHandler(dlist_ItemCommand);
			dlist.ItemCreated+=new DataListItemEventHandler(dlist_ItemCreated);
			dlist.ItemDataBound+=new DataListItemEventHandler(dlist_ItemDataBound);
			DataGrid grdNew =(DataGrid)e.Item.FindControl("grdNew");
			grdNew.ItemCommand+=new DataGridCommandEventHandler(grdNew_ItemCommand);
			grdNew.ItemDataBound+=new DataGridItemEventHandler(grdNew_ItemDataBound);
			DropDownList txtS1 = (DropDownList) e.Item.FindControl("txtS1");
			txtS1.SelectedIndexChanged+=new EventHandler(txtS1_SelectedIndexChanged);
			DropDownList txtS2 = (DropDownList) e.Item.FindControl("txtS2");
			txtS2.SelectedIndexChanged+=new EventHandler(txtS2_SelectedIndexChanged);
		}
		private void dlist_ItemCreated(object sender, System.Web.UI.WebControls.DataListItemEventArgs e)
		{
			DataGrid grd =(DataGrid)e.Item.FindControl("grdContentArchitect");
			grd.ItemCommand+=new DataGridCommandEventHandler(grdContentArchitect_ItemCommand);
			grd.ItemDataBound+=new DataGridItemEventHandler(grdContentArchitect_ItemDataBound);
			grd =(DataGrid)e.Item.FindControl("grdDistr");
			grd.ItemCommand+=new DataGridCommandEventHandler(grdDistr_ItemCommand);
			grd.ItemDataBound+=new DataGridItemEventHandler(grdDistr_ItemDataBound);
			
		}
		private void btnNewBuilding_Click(object sender, System.EventArgs e)
		{
			if(cbCopyLastBuilding.Checked)
				BuildingNewCopyClick();
			else
				BuildingNewClick();
		}
		private void dlBuildings_ItemCommand(object source, System.Web.UI.WebControls.DataListCommandEventArgs e)
		{
			int nIndex=e.Item.ItemIndex+1;
			lblError.Text="";
			int projectID = UIHelpers.GetPIDParam();
			TextBox txtS=(TextBox)e.Item.FindControl("txtS");
			
			if(e.CommandSource is Button )
			{
				Button ib = (Button)e.CommandSource;
				string s = ib.ID;
				if(ib.ID=="btnAdd")
				{
					DropDownList ddlContents =(DropDownList)e.Item.FindControl("ddlContents");
					int ID = int.Parse(ddlContents.SelectedValue);
					SqlDataReader reader=null;
					DataList dlist = (DataList) e.Item.FindControl("dlBuildingContentArchitect");
					ContentsVector DistrTable=GetDistr(projectID,(DataGrid)dlist.Items[1].FindControl("grdDistr"),txtS.Text);
					ContentsVector[] ContentsTable = new ContentsVector[ArchitectConst];
//					ContentsVector DistrTable1=GetContents(projectID,(DataGrid)e.Item.FindControl("grdArchitectCode1"),txtS.Text);
//					ContentsTable[0] = DistrTable1;
					for(int k=1;k<=ArchitectConst;k++)
					{
						ContentsVector ContentsTableOne=GetContents(projectID,(DataGrid)dlist.Items[k-1].FindControl("grdContentArchitect"),txtS.Text);
						ContentsTable[k-1] = ContentsTableOne;
					}
					try
					{
						reader = SubprojectsUDL.SelectOneContentType(ID);
						reader.Read();
						int filter = reader.GetInt32(2);
						if(nIndex>1 && ID==(int)PokazateliReportData.ContentTypes.KvadraturaNaChertej)
						{
							lblError.Text=Resource.ResourceManager["errorBuildingsQ"];
							return;
						}
						ContentData ct = new ContentData(-1,-1,ID,0,0,"","",nIndex,txtS.Text,"","",0,0,0,0,0);
						ct.ContentType=reader.GetString(1);
						DataSet dsPrefix = SubprojectsUDL.SelectContentsPrefix();
						string PrefixAdd = GetPrefix(ddlContents.SelectedValue,dsPrefix);
						int Filtar = UIHelpers.ToInt(PrefixAdd);
						if(filter==0)
						{
							if(IsCodeThere(nIndex-1))
							{
								if(ddlContents.SelectedValue!=System.Configuration.ConfigurationSettings.AppSettings["GroundArea"])
								{
									DataGrid grdDistr = (DataGrid) dlist.Items[1].FindControl("grdDistr");
									DataGrid grd = (DataGrid) dlist.Items[1].FindControl("grdContentArchitect");
									if(dlBuildings.Items.Count == 1)
									{
										int maxsgn1 = maxSgn(grd,string.Concat(CodeOfPhase(),RazdelConst,PrefixAdd,RazdelConst));
										int maxsgn = maxSgn(grdDistr,string.Concat(CodeOfPhase(),RazdelConst,PrefixAdd,RazdelConst));
										if(maxsgn1>maxsgn) maxsgn = maxsgn1;
										string sPadded=(maxsgn+1).ToString().PadLeft(2,'0');
										ct.ContentCode = string.Concat(CodeOfPhase(),RazdelConst,PrefixAdd,RazdelConst,sPadded);
									}
									else
									{
										int maxsgn1 = maxSgn(grd,string.Concat(CodeOfPhase(),RazdelConst,txtS.Text,RazdelConst,PrefixAdd,RazdelConst));
										int maxsgn = maxSgn(grdDistr,string.Concat(CodeOfPhase(),RazdelConst,txtS.Text,RazdelConst,PrefixAdd,RazdelConst));
										if(maxsgn1>maxsgn) maxsgn = maxsgn1;
										string sPadded=(maxsgn+1).ToString().PadLeft(2,'0');
										ct.ContentCode = string.Concat(CodeOfPhase(),RazdelConst,txtS.Text,RazdelConst,PrefixAdd,RazdelConst,sPadded);
									}
								}
							}
							DistrTable.Add(ct);
							SetDistrTable(nIndex,DistrTable);
							ct.ContentOrder = DistrTable.Count;
							
						}
						else	
						{
							if(IsCodeThere(nIndex-1))
							{
								if(Filtar == 9) Filtar = 0;
								DataGrid grd = (DataGrid) dlist.Items[Filtar].FindControl("grdContentArchitect");
								if(dlBuildings.Items.Count == 1)
								{
									int maxsgn = maxSgn(grd,string.Concat(CodeOfPhase(),RazdelConst,PrefixAdd,RazdelConst));
									if(Filtar == 1)
									{
										DataGrid grdDistr = (DataGrid) dlist.Items[1].FindControl("grdDistr");
										int maxsgn1 = maxSgn(grdDistr,string.Concat(CodeOfPhase(),"-",PrefixAdd,RazdelConst));
										if(maxsgn1>maxsgn) maxsgn = maxsgn1;
									}
									string sPadded=(maxsgn+1).ToString().PadLeft(2,'0');
									ct.ContentCode = string.Concat(CodeOfPhase(),RazdelConst,PrefixAdd,RazdelConst,sPadded);
									if(Filtar==5)
									{
										if( (ct.ContentTypeID.ToString() == System.Configuration.ConfigurationSettings.AppSettings["DetailV"]))
										{
											ct.ContentCode = string.Concat(ct.ContentCode,RazdelConst,"V");
										}
										if( (ct.ContentTypeID.ToString() == System.Configuration.ConfigurationSettings.AppSettings["DetailH"]))
										{
											ct.ContentCode = string.Concat(ct.ContentCode,RazdelConst,"H");
										}
									}
								}
								else
								{
									int maxsgn = maxSgn(grd,string.Concat(CodeOfPhase(),RazdelConst,txtS.Text,RazdelConst,PrefixAdd,RazdelConst));
									if(Filtar == 1)
									{
										DataGrid grdDistr = (DataGrid) dlist.Items[1].FindControl("grdDistr");
										int maxsgn1 = maxSgn(grdDistr,string.Concat(CodeOfPhase(),RazdelConst,txtS.Text,RazdelConst,PrefixAdd,RazdelConst));
										if(maxsgn1>maxsgn) maxsgn = maxsgn1;
									}
									string sPadded=(maxsgn+1).ToString().PadLeft(2,'0');
									ct.ContentCode = string.Concat(CodeOfPhase(),RazdelConst,txtS.Text,RazdelConst,PrefixAdd,RazdelConst,sPadded);
									if(Filtar==5)
									{
										if (ct.ContentTypeID.ToString() == System.Configuration.ConfigurationSettings.AppSettings["DetailV"])
										{
											ct.ContentCode = string.Concat(ct.ContentCode,RazdelConst,"V");
										}
										if(ct.ContentTypeID.ToString() == System.Configuration.ConfigurationSettings.AppSettings["DetailH"])
										{
											ct.ContentCode = string.Concat(ct.ContentCode,RazdelConst,"H");
										}
									}
								}
							}
							ContentsTable[Filtar].Add(ct);
							SetContentsTable(nIndex,ContentsTable[Filtar],Filtar+1);
							ct.ContentOrder = ContentsTable[Filtar].Count;
							
						}
						BindGrid();
					}
					catch 
					{
				
				
					}

					finally 
					{ 
						if (reader!=null) reader.Close(); 
					}
				}
				else if(ib.ID=="btnAddConstructorsOnly")
				{
					DropDownList ddlContentsConstructors =(DropDownList)e.Item.FindControl("ddlContentsConstructors");
					int ID = int.Parse(ddlContentsConstructors.SelectedValue);
					SqlDataReader reader=null;
					
					ContentsVector ContentsTableNew = GetContents(projectID,(DataGrid)e.Item.FindControl("grdNew"),txtS.Text);					
					try
					{
						reader = SubprojectsUDL.SelectOneContentType(ID);
						reader.Read();
						ContentData ct = new ContentData(-1,-1,ID,0,0,"","",nIndex,txtS.Text,"","",0,0,0,0,0);
						ct.ContentType=reader.GetString(1);
						DataSet dsPrefix = SubprojectsUDL.SelectContentsPrefix();
						string PrefixAdd = GetPrefix(ddlContentsConstructors.SelectedValue,dsPrefix);
						if(IsCodeThere(nIndex-1))
						{
							DataGrid grdNew = (DataGrid) dlBuildings.Items[nIndex-1].FindControl("grdNew");
							int maxsgn = maxSgn(grdNew,string.Concat(CodeOfPhase(),RazdelConst,PrefixAdd,RazdelConst));
							string sPadded=(maxsgn+1).ToString().PadLeft(2,'0');
							ct.ContentCode = string.Concat(CodeOfPhase(),RazdelConst,PrefixAdd,RazdelConst,sPadded);
						}
						ContentsTableNew.Add(ct);
						SetContentsTableNew(nIndex,ContentsTableNew);
						ct.ContentOrder = ContentsTable.Count;
						BindGrid();
					}
					catch 
					{
					}

					finally 
					{ 
						if (reader!=null) reader.Close(); 
					}
				}
				else if(ib.ID=="btnRefresh")
				{
					RefreshTotals();
				}
				else if(ib.ID=="btnMarkAll")
				{
					DataList dlist = (DataList) dlBuildings.Items[nIndex-1].FindControl("dlBuildingContentArchitect");
					for(int k=1;k<=ArchitectConst;k++)
					{
						DataGrid grd = (DataGrid) dlist.Items[k-1].FindControl("grdContentArchitect");
						for(int j=0;j<grd.Items.Count;j++)
						{
							CheckBox cbReportCode = (CheckBox) grd.Items[j].FindControl("cbReportCode");
							cbReportCode.Checked=true;
						}
//						lbPlusCount = (Label) dlist.Items[k-1].FindControl("lbGridNameCount");
//						lbPlusCount.Text = string.Concat(Resource.ResourceManager["dlBuildingContentCount"],GridCount(grd));
						if(k==2)
						{
							grd = (DataGrid) dlist.Items[k-1].FindControl("grdDistr");
							for(int j=0;j<grd.Items.Count;j++)
							{
								CheckBox cbReportCode = (CheckBox) grd.Items[j].FindControl("cbReportCode");
								cbReportCode.Checked=true;
							}
						}
					}
					DataGrid grdNew = (DataGrid) dlBuildings.Items[nIndex-1].FindControl("grdNew");
					for(int j=0;j<grdNew.Items.Count;j++)
					{
						CheckBox cbReportCode = (CheckBox) grdNew.Items[j].FindControl("cbReportCode");
						cbReportCode.Checked=true;
					}

				}
				else if(ib.ID=="btnUnmarkAll")
				{
					DataList dlist = (DataList) dlBuildings.Items[nIndex-1].FindControl("dlBuildingContentArchitect");
					for(int k=1;k<=ArchitectConst;k++)
					{
						DataGrid grd = (DataGrid) dlist.Items[k-1].FindControl("grdContentArchitect");
						for(int j=0;j<grd.Items.Count;j++)
						{
							CheckBox cbReportCode = (CheckBox) grd.Items[j].FindControl("cbReportCode");
							cbReportCode.Checked=false;
						}
						if(k==2)
						{
							grd = (DataGrid) dlist.Items[k-1].FindControl("grdDistr");
							for(int j=0;j<grd.Items.Count;j++)
							{
								CheckBox cbReportCode = (CheckBox) grd.Items[j].FindControl("cbReportCode");
								cbReportCode.Checked=false;
							}
						}
					}
					DataGrid grdNew = (DataGrid) dlBuildings.Items[nIndex-1].FindControl("grdNew");
					for(int j=0;j<grdNew.Items.Count;j++)
					{
						CheckBox cbReportCode = (CheckBox) grdNew.Items[j].FindControl("cbReportCode");
						cbReportCode.Checked=false;
					}
				}
				else if(ib.ID=="btnPlusAll")
				{
					btnPlusAllClick(nIndex);
				}
				else if(ib.ID=="btnMinusAll")
				{
					btnMinusAllClick(nIndex);
				}
				else if(ib.ID=="btnMakeCurrentOzn")
				{
					MakeCurrentOznToGrids(nIndex-1);
				}
				else if(ib.ID=="btnCanselCurrentOzn")
				{
					CanselCurrentOznToGrids(nIndex-1);
				}
				}		
					else
				{
					if(e.CommandSource is LinkButton)
					{
						LinkButton ib = (LinkButton)e.CommandSource;
						string s = ib.ID;
						if(ib.ID=="btnContents")
						{
							bool hasChanges = ChecksSave();
							if(!hasChanges) 
							{lblError.Text = Resource.ResourceManager["ChangeOnBuildings"]; lblError.Visible=true;return;}
							ChecksVector checks = GetChecksTable(projectID);
							PdfExport rtf = new PdfExport();
							rtf.NeverEmbedFonts = "";
							DataDynamics.ActiveReports.ActiveReport report = new Contents(projectID,LoggedUser.FullName,false,nIndex,checks,CodeOfPhase());
							report.Run();
							System.IO.MemoryStream memoryFile = new System.IO.MemoryStream();

							rtf.Export(report.Document, memoryFile);
							Response.Clear();

							Response.AppendHeader( "content-disposition","attachment; filename="+"Contents.pdf");
							Response.ContentType = "application/pdf";


							memoryFile.WriteTo(Response.OutputStream); 
							Response.End(); 
						}
					
						else if(ib.ID=="btnTech")
						{
							bool hasChanges = ChecksSave();
							if(!hasChanges) 
							{lblError.Text = Resource.ResourceManager["ChangeOnBuildings"]; lblError.Visible=true;return;}
							ChecksVector checks = GetChecksTable(projectID);
							PdfExport pdf = new PdfExport();
							XlsExport xls = new XlsExport();
		
							pdf.NeverEmbedFonts = "";

							DataDynamics.ActiveReports.ActiveReport report = new Pokazateli(projectID,LoggedUser.FullName,nIndex,false,checks,CodeOfPhase());
							report.Run();
							System.IO.MemoryStream memoryFile = new System.IO.MemoryStream();

							pdf.Export(report.Document, memoryFile);
							Response.Clear();

							Response.AppendHeader( "content-disposition","attachment; filename="+"Pokazateli.pdf");
							Response.ContentType = "application/pdf";


							memoryFile.WriteTo(Response.OutputStream); 
							Response.End(); 
						}
					}
					else
					{
						if(e.CommandSource is ImageButton)
						{
							ImageButton ib = (ImageButton)e.CommandSource;
							switch (ib.ID)
							{
								case "img":
								{
									bool hasChanges = ChecksSave();
									if(!hasChanges) 
									{lblError.Text = Resource.ResourceManager["ChangeOnBuildings"]; lblError.Visible=true;return;}
									RtfExport rtf = new RtfExport();
									ChecksVector checks = GetChecksTable(projectID);
									DataDynamics.ActiveReports.ActiveReport report = new Contents(projectID,LoggedUser.FullName,true,nIndex,checks,CodeOfPhase());
									report.Run();
									System.IO.MemoryStream memoryFile = new System.IO.MemoryStream();

									rtf.Export(report.Document, memoryFile);
									Response.Clear();

									Response.AppendHeader( "content-disposition","attachment; filename="+"Contents.rtf");
									Response.ContentType = "application/pdf";


									memoryFile.WriteTo(Response.OutputStream); 
									Response.End(); 
								}
									break;
								case "btnContentsExcel":
								{
									bool hasChanges = ChecksSave();
									if(!hasChanges) 
									{lblError.Text = Resource.ResourceManager["ChangeOnBuildings"]; lblError.Visible=true;return;}
									XlsExport rtf = new XlsExport();
									ChecksVector checks = GetChecksTable(projectID);
									DataDynamics.ActiveReports.ActiveReport report = new Contents(projectID,LoggedUser.FullName,true,nIndex,checks,CodeOfPhase());
									report.Run();
									System.IO.MemoryStream memoryFile = new System.IO.MemoryStream();

									rtf.Export(report.Document, memoryFile);
									Response.Clear();

									Response.AppendHeader( "content-disposition","attachment; filename="+"Contents.xls");
									Response.ContentType = "application/pdf";


									memoryFile.WriteTo(Response.OutputStream); 
									Response.End(); 

							
								}
									break;
								case "imgTechWord":
								{
									bool hasChanges = ChecksSave();
									if(!hasChanges) 
									{lblError.Text = Resource.ResourceManager["ChangeOnBuildings"]; lblError.Visible=true;return;}
									RtfExport pdf = new RtfExport();
									ChecksVector checks = GetChecksTable(projectID);
		
							

									DataDynamics.ActiveReports.ActiveReport report = new Pokazateli(projectID,LoggedUser.FullName,nIndex,true,checks,CodeOfPhase());
									report.Run();
									System.IO.MemoryStream memoryFile = new System.IO.MemoryStream();

									pdf.Export(report.Document, memoryFile);
									Response.Clear();

									Response.AppendHeader( "content-disposition","attachment; filename="+"Pokazateli.rtf");
									Response.ContentType = "application/pdf";


									memoryFile.WriteTo(Response.OutputStream); 
									Response.End(); 
								}
									break;
								case "imgTechExcel":
								{
									bool hasChanges = ChecksSave();
									if(!hasChanges) 
									{lblError.Text = Resource.ResourceManager["ChangeOnBuildings"]; lblError.Visible=true;return;}
									XlsExport rtf = new XlsExport();
									ChecksVector checks = GetChecksTable(projectID);
		
							

									DataDynamics.ActiveReports.ActiveReport report = new Pokazateli(projectID,LoggedUser.FullName,nIndex,true,checks,CodeOfPhase());
									report.Run();
									System.IO.MemoryStream memoryFile = new System.IO.MemoryStream();

									rtf.Export(report.Document, memoryFile);
									Response.Clear();

									Response.AppendHeader( "content-disposition","attachment; filename="+"Pokazateli.xls");
									Response.ContentType = "application/pdf";


									memoryFile.WriteTo(Response.OutputStream); 
									Response.End(); 
								}
									break;
						
							}
						}
					}
				}
		}
		
		private void dlist_ItemCommand(object source, System.Web.UI.WebControls.DataListCommandEventArgs e)
		{
			DataList dList = (DataList) source;
			int nIndex = 1;
			for(int i=0;i<dlBuildings.Items.Count;i++)
			{
				if(dList == (DataList) dlBuildings.Items[i].FindControl("dlBuildingContentArchitect"))
					nIndex = i+1;
			}	
			int Filtar=e.Item.ItemIndex+1;
			Button ib = (Button)e.CommandSource;
			string s = ib.ID;
			if(ib.ID=="btnPlusMultiple")
			{
				btnPlusMultipleClick(nIndex,Filtar);
			}
			else if(ib.ID=="btnMinusMultiple")
			{
				btnMinusMultipleClick(nIndex,Filtar);
			}
		}	
		private void txtS1_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			DropDownList dll1 = (DropDownList) sender;
			int index = 0;
			for(int i=0;i<dlBuildings.Items.Count;i++)
			{
				DropDownList txt = (DropDownList)dlBuildings.Items[i].FindControl("txtS1");
				if(dll1 == txt)
				{index = i;break;}
			}
			DropDownList dll2 = (DropDownList) dlBuildings.Items[index].FindControl("txtS2");
			TextBox txt1 = (TextBox) dlBuildings.Items[index].FindControl("txtS");
			txt1.Text = string.Concat(dll1.SelectedValue,dll2.SelectedValue);
			if(IsCodeThere(index))
				ReplaceCode(txt1.Text,index);
		}
		private void txtS2_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			DropDownList dll2 = (DropDownList) sender;
			int index = 0;
			for(int i=0;i<dlBuildings.Items.Count;i++)
			{
				DropDownList txt = (DropDownList)dlBuildings.Items[i].FindControl("txtS2");
				if(dll2 == txt)
				{index = i;break;}
			}
			DropDownList dll1 = (DropDownList) dlBuildings.Items[index].FindControl("txtS1");
			TextBox txt1 = (TextBox) dlBuildings.Items[index].FindControl("txtS");
			txt1.Text = string.Concat(dll1.SelectedValue,dll2.SelectedValue);
			
			if(IsCodeThere(index))
				ReplaceCode(txt1.Text,index);
		}
		private void dlBuildings_ItemDataBound(object sender, System.Web.UI.WebControls.DataListItemEventArgs e)
		{
		
		}
		private void dlist_ItemDataBound(object sender, System.Web.UI.WebControls.DataListItemEventArgs e)
		{
		
		}
		private void ddlPhases_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			HideOthers();
			for(int i=0;i<dlBuildings.Items.Count;i++)
			{
				string NewCode = CodeOfPhase();
				if(IsCodeThere(i))
					ReplaceCodePhases(NewCode,i);
				DataList dlist = (DataList) dlBuildings.Items[i].FindControl("dlBuildingContentArchitect");
				for(int k=0;k<ArchitectConst;k++)
				{
					DataGrid dg = (DataGrid) dlist.Items[k].FindControl("grdContentArchitect");
					Label lbPercentFinished = (Label) dlist.Items[k].FindControl("lbPercentFinished");
					if((dg.Items.Count!=0)&&(lbPercentFinished.Visible))
					{
						lbPercentFinished.Visible = true;
						lbPercentFinished.Text = string.Concat(Resource.ResourceManager["PercentFinished"],PercentSum(dg),"%");
					}
					if(k == 1)
					{
						DataGrid dg1 = (DataGrid) dlist.Items[k].FindControl("grdDistr");
						if((dg1.Items.Count!=0)&&(lbPercentFinished.Visible))
						{
							lbPercentFinished.Text = string.Concat(Resource.ResourceManager["PercentFinished"],PercentSum(dg,dg1),"%");
							lbPercentFinished.Visible = true;
						}
					}
				}
			}
		}
	
		private void grdDistr_ItemDataBound(object sender, System.Web.UI.WebControls.DataGridItemEventArgs e)
		{
			if(e.Item.ItemType==ListItemType.Item
				|| e.Item.ItemType==ListItemType.AlternatingItem)
			{
				TextBox tb = (TextBox) e.Item.FindControl("txtRevision");
				DropDownList ddl = (DropDownList) e.Item.FindControl("ddlRevision");
				if(tb.Text.Length == 0)
				{
					ddl.Items.Insert(0,new ListItem("","0"));
					ddl.Items.Insert(1,new ListItem("R1","1"));
					ddl.SelectedIndex=0;
				}
				else
				{
					int numb = UIHelpers.ToInt(tb.Text.Substring(1));
					ddl.Items.Insert(0,new ListItem(string.Concat("R",numb),"0"));
					ddl.Items.Insert(1,new ListItem(string.Concat("R",numb+1),"1"));
					ddl.SelectedIndex=0;
				}

			}
		}
		
		private void grdContentArchitect_ItemDataBound(object sender, System.Web.UI.WebControls.DataGridItemEventArgs e)
		{
			if(e.Item.ItemType==ListItemType.Item
				|| e.Item.ItemType==ListItemType.AlternatingItem)
			{
				TextBox tb = (TextBox) e.Item.FindControl("txtRevision");
				DropDownList ddl = (DropDownList) e.Item.FindControl("ddlRevision");
				if(tb.Text.Length == 0)
				{
					ddl.Items.Insert(0,new ListItem("","0"));
					ddl.Items.Insert(1,new ListItem("R1","1"));
					ddl.SelectedIndex=0;
				}
				else
				{
					int numb = UIHelpers.ToInt(tb.Text.Substring(1));
					ddl.Items.Insert(0,new ListItem(string.Concat("R",numb),"0"));
					ddl.Items.Insert(1,new ListItem(string.Concat("R",numb+1),"1"));
					ddl.SelectedIndex=0;
				}

			}
		}
		
		private void grdNew_ItemDataBound(object sender, System.Web.UI.WebControls.DataGridItemEventArgs e)
		{
			if(e.Item.ItemType==ListItemType.Item
				|| e.Item.ItemType==ListItemType.AlternatingItem)
			{
				TextBox tb = (TextBox) e.Item.FindControl("txtRevision");
				DropDownList ddl = (DropDownList) e.Item.FindControl("ddlRevision");
				if(tb.Text.Length == 0)
				{
					ddl.Items.Insert(0,new ListItem("","0"));
					ddl.Items.Insert(1,new ListItem("R1","1"));
					ddl.SelectedIndex=0;
				}
				else
				{
					int numb = UIHelpers.ToInt(tb.Text.Substring(1));
					ddl.Items.Insert(0,new ListItem(string.Concat("R",numb),"0"));
					ddl.Items.Insert(1,new ListItem(string.Concat("R",numb+1),"1"));
					ddl.SelectedIndex=0;
				}

			}
		}
		
		
		#endregion

		

	}
}
