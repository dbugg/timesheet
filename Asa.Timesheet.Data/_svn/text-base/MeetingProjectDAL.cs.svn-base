using System;
using System.Data;
using System.Data.SqlClient; 
using System.Collections;
using Microsoft.ApplicationBlocks.Data;

namespace Asa.Timesheet.Data
{
	public class MeetingProjectDAL
	{
		/// <summary>
		/// Loads a MeetingProjectData object from the database using the given MeetingProjectID
		/// </summary>
		/// <param name="meetingProjectID">The primary key value</param>
		/// <returns>A MeetingProjectData object</returns>
		public static MeetingProjectData Load(int meetingProjectID)
		{
			SqlParameter[] parameterValues = new SqlParameter[] { new SqlParameter("@MeetingProjectID", meetingProjectID) };
			using (SqlDataReader reader = SqlHelper.ExecuteReader(CommonDAL.ConnectionString, CommandType.StoredProcedure, "MeetingProjectsSelProc", parameterValues))
			{
				if (reader.Read())
				{
					MeetingProjectData result = new MeetingProjectData();
					LoadFromReader(reader,result);
					return result;
				}
				else
					return null;
			}
		}

		/// <summary>
		/// Loads a MeetingProjectData object from the database using the given where clause
		/// </summary>
		/// <param name="whereClause">The filter expression for the query</param>
		/// <returns>A MeetingProjectData object</returns>
		public static MeetingProjectData Load(string whereClause)
		{
			SqlParameter[] parameterValues = new SqlParameter[] { new SqlParameter("@WhereClause", whereClause) };
			using (SqlDataReader reader = SqlHelper.ExecuteReader(CommonDAL.ConnectionString, CommandType.StoredProcedure, "MeetingProjectsListProc", parameterValues))
			{
				if (reader.Read())
				{
					MeetingProjectData result = new MeetingProjectData();
					LoadFromReader(reader,result);
					return result;
				}
				else
					return null;
			}
		}
		public static int Save( MeetingProjectData source)
		{
			if (source.MeetingProjectID == -1)
				return Insert(source);
			else
				return Update(source);	
		}

		public static void Delete( MeetingProjectData source)
		{
			Delete(source.MeetingProjectID);
		}
		/// <summary>
		/// Loads a collection of MeetingProjectData objects from the database.
		/// </summary>
		/// <returns>A collection containing all of the MeetingProjectData objects in the database.</returns>
		public static MeetingProjectsVector LoadCollection()
		{
			SqlParameter[] parms = new SqlParameter[] {};
			return LoadCollection("MeetingProjectsListProc", parms);
		}
	  
		/// <summary>
		/// Saves a collection of MeetingProjectData objects to the database.
		/// </summary>
		/// <returns>A collection containing all of the MeetingProjectData objects in the database.</returns>
		public static  void  SaveCollection(MeetingProjectsVector col,string spName, SqlParameter[] parms)
		{
			MeetingProjectsVector colOld = LoadCollection(spName,parms);
			UpdateCollection(colOld,col);
		}
		public static  void  SaveCollection(MeetingProjectsVector col)
		{
			MeetingProjectsVector colOld = LoadCollection();
			UpdateCollection(colOld,col);
		}
		public static  void  SaveCollection(MeetingProjectsVector col, string whereClause )
		{
			MeetingProjectsVector colOld = LoadCollection(whereClause);
			UpdateCollection(colOld,col);
		}
		/// <summary>
		/// Loads a collection of MeetingProjectData objects from the database.
		/// </summary>
		/// <returns>A collection containing all of the MeetingProjectData objects in the database.</returns>
		public static MeetingProjectsVector LoadCollection(string whereClause)
		{
			SqlParameter[] parms = new SqlParameter[] { new SqlParameter("@WhereClause", whereClause) };
			return LoadCollection("MeetingProjectsListProc", parms);
		}

		/// <summary>
		/// Loads a collection of MeetingProjectData objects from the database.
		/// </summary>
		/// <returns>A collection containing all of the MeetingProjectData objects in the database.</returns>
		public static MeetingProjectsVector LoadCollection(string spName, SqlParameter[] parms)
		{
			MeetingProjectsVector result = new MeetingProjectsVector();
			using (SqlDataReader reader = SqlHelper.ExecuteReader(CommonDAL.ConnectionString, CommandType.StoredProcedure, spName, parms))
			{
				while (reader.Read())
				{
					MeetingProjectData tmp = new MeetingProjectData();
					LoadFromReader(reader, tmp);
					result.Add(tmp);
				}
			}
			return result;
		}
		/// <summary>
		/// Deletes a MeetingProjectData object from the database.
		/// </summary>
		/// <param name="meetingProjectID">The primary key value</param>
		public static void Delete(int meetingProjectID)
		{
			SqlParameter[] parameterValues = new SqlParameter[] { new SqlParameter("@MeetingProjectID", meetingProjectID) };
			SqlHelper.ExecuteNonQuery(CommonDAL.ConnectionString, CommandType.StoredProcedure, "MeetingProjectsDelProc", parameterValues);
		}

		public static void LoadFromReader(SqlDataReader reader, MeetingProjectData target)
		{
			if (reader != null && !reader.IsClosed)
			{
				//target.Key = reader.GetInt32(0);
				if (!reader.IsDBNull(0)) target.MeetingProjectID = reader.GetInt32(0);
				if (!reader.IsDBNull(1)) target.MeetingID = reader.GetInt32(1);
				if (!reader.IsDBNull(2)) target.ProjectID = reader.GetInt32(2);
			}
		}
		
	  
		public static int Insert( MeetingProjectData source)
		{
			SqlParameter[] parameterValues = GetInsertParameterValues(source);
			SqlHelper.ExecuteNonQuery(CommonDAL.ConnectionString, CommandType.StoredProcedure, "MeetingProjectsInsProc", parameterValues);
			source.MeetingProjectID = (int) parameterValues[0].Value;
			return source.MeetingProjectID;
		}

		public static int Update( MeetingProjectData source)
		{
			SqlParameter[] parameterValues = GetUpdateParameterValues(source);
			SqlHelper.ExecuteNonQuery(CommonDAL.ConnectionString, CommandType.StoredProcedure,
				"MeetingProjectsUpdProc", parameterValues);
			return source.MeetingProjectID;
		}
		private static void UpdateCollection(MeetingProjectsVector colOld,MeetingProjectsVector col)
		{
			// Delete all old items
			foreach( MeetingProjectData itemOld in colOld)
			{
				if(!ExistsKey(itemOld.MeetingProjectID, col))
					Delete(itemOld.MeetingProjectID);
			}
			foreach( MeetingProjectData item in col)
			{
				if(item.MeetingProjectID <= -1)
					Insert(item);
				else
					Update(item);
			}
		}
		private static bool ExistsKey(int Key, MeetingProjectsVector col)
		{
			foreach( MeetingProjectData item in col)
				if(item.MeetingProjectID==Key)
					return true;
			return false;
		}

		private static SqlParameter GetSqlParameter(string name, ParameterDirection direction, object value)
		{
			SqlParameter p = new SqlParameter(name, value);
			p.Direction = direction;
			return p;
		}

		private static SqlParameter[] GetInsertParameterValues( MeetingProjectData source)
		{
			SqlParameter[] prams = new SqlParameter[3];
			prams[0] = GetSqlParameter("@MeetingProjectID", ParameterDirection.Output, source.MeetingProjectID);
			prams[1] = GetSqlParameter("@MeetingID", ParameterDirection.Input, source.MeetingID);
			prams[2] = GetSqlParameter("@ProjectID", ParameterDirection.Input, source.ProjectID);
			return prams;
		}

		private static SqlParameter[] GetUpdateParameterValues( MeetingProjectData source)
		{
			return new SqlParameter[] {
										  new SqlParameter("@MeetingProjectID", source.MeetingProjectID),
										  new SqlParameter("@MeetingID", source.MeetingID),
										  new SqlParameter("@ProjectID", source.ProjectID)
									  };
		}
	}
}   

