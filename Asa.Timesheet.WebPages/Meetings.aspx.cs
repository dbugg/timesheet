using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Data.SqlClient;
using log4net;
using Asa.Timesheet.Data;
using Asa.Timesheet.Data.Entities;
using Asa.Timesheet.Data.Util;
using DataDynamics.ActiveReports.Export.Pdf;
using DataDynamics.ActiveReports.Export.Xls;
using Asa.Timesheet.WebPages.Reports;
using DataDynamics.ActiveReports.Export.Rtf;
using System.IO;

namespace Asa.Timesheet.WebPages
{
	/// <summary>
	/// Summary description for Meetings.
	/// </summary>
	public class Meetings : TimesheetPageBase
	{
		#region WebControls
		private static readonly ILog log = LogManager.GetLogger(typeof(Meetings));
		protected System.Web.UI.WebControls.PlaceHolder menuHolder;
		protected System.Web.UI.WebControls.Label lblError;
		protected System.Web.UI.WebControls.Label lblInfo;
		protected System.Web.UI.WebControls.Panel grid;
		protected System.Web.UI.WebControls.Button btnExport;
		protected System.Web.UI.WebControls.DataGrid grdMeetings;
		protected System.Web.UI.WebControls.Button btnNew;
		protected Telerik.WebControls.RadGrid gridCalls;
		protected System.Web.UI.WebControls.Button btnSearch;
		protected System.Web.UI.WebControls.Label Label8;
		protected System.Web.UI.WebControls.Label lblProject;
		protected System.Web.UI.WebControls.DropDownList ddlProject;
		protected System.Web.UI.WebControls.DropDownList ddlProjectsStatus;
		protected System.Web.UI.WebControls.Label Label2;
		protected System.Web.UI.WebControls.DropDownList ddlClients;
		protected System.Web.UI.WebControls.Label Label4;
		protected System.Web.UI.WebControls.TextBox txtStartDate;
		protected System.Web.UI.HtmlControls.HtmlImage lkCalendar1;
		protected System.Web.UI.WebControls.DropDownList ddlBuildingTypes;
		protected System.Web.UI.WebControls.TextBox txtEndDate;
		protected System.Web.UI.HtmlControls.HtmlImage lkCalendar2;
		protected System.Web.UI.WebControls.Button btnProtokols;
		protected Asa.Timesheet.WebPages.UserControls.PageHeader header;
		#endregion
		private enum GridColumns
		{
			Number=0,
			Name ,
			MeetingDate,
			
			StartHour,
			EndHour,
			Confirm,
			Notebook,
			Scan,
			pdfExp,
			DeleteItem
		}
		private void Page_Load(object sender, System.EventArgs e)
		{
			if (!(LoggedUser.HasPaymentRights || LoggedUser.IsAssistant))
			{
				ErrorRedirect(Resource.ResourceManager["noRightsAlert"]);
			}
			lkCalendar1.Attributes["onclick"] = TimeHelper.InvokePopupCal(txtStartDate);
			lkCalendar2.Attributes["onclick"] = TimeHelper.InvokePopupCal(txtEndDate);
			UIHelpers.CreateMenu(menuHolder,LoggedUser);
			
			if (!this.IsPostBack)
			{
				header.PageTitle = Resource.ResourceManager["meeting_PageTitle"];
				header.UserName = this.LoggedUser.UserName;
				
				UIHelpers.LoadBuildingTypes(ddlBuildingTypes, "");
				LoadClients("0");
				LoadProjects();
				BindGrid();
			}
			if(!(LoggedUser.IsAssistantOnly || LoggedUser.HasPaymentRights || LoggedUser.IsSecretary || LoggedUser.IsASI))
			{
				btnNew.Visible=false;
			}
		}
		private bool LoadClients(string selectedValue)
		{
			SqlDataReader reader = null;
			try
			{
				reader = ClientsData.SelectClients(0);
				ddlClients.DataSource = reader;
				ddlClients.DataValueField = "ClientID";
				ddlClients.DataTextField = "ClientName";
				ddlClients.DataBind();

				ddlClients.Items.Insert(0, new ListItem(Resource.ResourceManager["reports_ddlAllClients"], "0"));
				if(ddlClients.Items.FindByValue( selectedValue)!=null)
					ddlClients.SelectedValue = selectedValue;
			}
			catch (Exception ex)
			{
				log.Info(ex);
				return false;
			}
			finally 
			{
				if (reader!=null) reader.Close();
			}
			return true;
		}
		protected bool GetVisible(object o)
		{
			if(o==DBNull.Value)
				return false;
			else return (bool)o;
		}
		protected string GetURL(int ID)
		{
			int mdID=-1;
			MeetingDocumentsVector mdv = MeetingDocumentDAL.LoadCollection("MeetingDocumentsSelByMeetingProc",SQLParms.CreateMeetingDocumentsSelByMeetingProc(ID));
			foreach(MeetingDocumentData mdd in mdv)
			{
				mdID = mdd.MeetingDocument;
				break;
			}
			return string.Concat(System.Configuration.ConfigurationSettings.AppSettings["ScanPath"],mdID,System.Configuration.ConfigurationSettings.AppSettings["Extension"]);
			//return Request.MapPath(path);
		}
		protected string GetURL1(int ID)
		{
			return string.Concat(System.Configuration.ConfigurationSettings.AppSettings["ScanPath"],"NoteBook",ID,".pdf");
			//return Request.MapPath(path);
		}
		
		protected string GetImage(object Done)
		{
			if(Done==null || Done==System.DBNull.Value)
				return "~/images/delete.png";
			bool bDone=(bool)Done;
			if(bDone)
				return 	"~/images/done.gif";
			return "~/images/delete.png";
		}
		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
			this.ddlBuildingTypes.SelectedIndexChanged += new System.EventHandler(this.Dropdownlist1_SelectedIndexChanged);
			this.ddlProjectsStatus.SelectedIndexChanged += new System.EventHandler(this.ddlProjectsStatus_SelectedIndexChanged);
			this.btnSearch.Click += new System.EventHandler(this.btnSearch_Click);
			this.grdMeetings.ItemCommand += new System.Web.UI.WebControls.DataGridCommandEventHandler(this.grdMeetings_ItemCommand);
			this.grdMeetings.SortCommand += new System.Web.UI.WebControls.DataGridSortCommandEventHandler(this.grdMeetings_SortCommand);
			this.btnNew.Click += new System.EventHandler(this.btnNew_Click);
			this.Load += new System.EventHandler(this.Page_Load);

		}
		#endregion

		private void grdMeetings_ItemCommand(object source, System.Web.UI.WebControls.DataGridCommandEventArgs e)
		{
			if(e.CommandName=="Edit")
			{
				string SID = ((DataGrid)source).DataKeys[e.Item.ItemIndex].ToString();
				Response.Redirect("EditMeeting.aspx"+"?id="+SID); 
				return;
			}
			if(!(e.CommandSource is ImageButton))
				return;
			ImageButton ib = (ImageButton)e.CommandSource;
			string s = ib.ID;
			switch (ib.ID)
			{
					case "ibPdfExport": 
					int IDD = (int)((DataGrid)source).DataKeys[e.Item.ItemIndex];
					PDFExport(IDD);
					break;
				case "btnDelete": int ID = (int)((DataGrid)source).DataKeys[e.Item.ItemIndex];
					MeetingDAL.Delete(ID);
					BindGrid();
					break;
//				case "btnNotebook":
//					int IDD = (int)((DataGrid)source).DataKeys[e.Item.ItemIndex];
//					ShowNotebook(IDD);
//					break;
				
				
			}
		}
	
		private void BindGrid()
		{

			MeetingsVector mv =MeetingDAL.LoadCollection("MeetingsListProc",SQLParms.CreateMeetingsListProc(TimeHelper.GetDate(txtStartDate.Text),
				TimeHelper.GetDate(txtEndDate.Text),int.Parse(ddlProject.SelectedValue),int.Parse(ddlClients.SelectedValue),(string)this.ViewState["Sort"]));			
			if (int.Parse(ddlProject.SelectedValue) != -1)
			{
				MeetingProjectsVector mpv = MeetingProjectDAL.LoadCollection("MeetingProjectsSelByProjectsProc",SQLParms.CreateMeetingProjectsSelByProjectsProc(int.Parse(ddlProject.SelectedValue)));
				foreach (MeetingProjectData mpd in mpv)
				{
					bool IsSelected = false;
					foreach (MeetingData md in mv)
						if (md.MeetingID == mpd.MeetingID) {IsSelected=true;break;}
					if (!IsSelected)
					{
						MeetingData mdAdd = MeetingDAL.Load(mpd.MeetingID);
						if ((mdAdd.MeetingDate<TimeHelper.GetDate(txtEndDate.Text))&&(mdAdd.MeetingDate>TimeHelper.GetDate(txtStartDate.Text)))
							if ((int.Parse(ddlClients.SelectedValue) == 0)||(int.Parse(ddlClients.SelectedValue) == mdAdd.ClientID))
								mv.Add(mdAdd);
					}
				}
			}							
				foreach(MeetingData md in mv)
				{
					MeetingProjectsVector mpv =  MeetingProjectDAL.LoadCollection("MeetingProjectsSelByMeetingProc",SQLParms.CreateMeetingProjectsSelByMeetingProc(md.MeetingID));
					if(mpv.Count>0)
					{
						foreach(MeetingProjectData mpd in mpv)
						{		
							if (md.ProjectID!=mpd.ProjectID)
							{
								md.ProjectName +=", "+ProjectsData.SelectProjectName(mpd.ProjectID);
							}
						}				
					}
				}			
			grdMeetings.DataSource =mv;
			grdMeetings.DataKeyField = "MeetingID";
			grdMeetings.DataBind();
			if (!LoggedUser.HasPaymentRights)
				grdMeetings.Columns[(int)GridColumns.DeleteItem].Visible = false;
			try
			{
				SetConfirmDelete(grdMeetings, "btnDelete", 1);
			}
			catch (Exception ex)
			{
				
			}
		}
		private void btnNew_Click(object sender, System.EventArgs e)
		{
			Response.Redirect("EditMeeting.aspx");
		}

		private void btnSearch_Click(object sender, System.EventArgs e)
		{
			BindGrid();
		}
		public ProjectsData.ProjectsByStatus ProjectsStatus
		{
			get
			{
				try
				{
					return (ProjectsData.ProjectsByStatus)int.Parse(ddlProjectsStatus.SelectedValue);
				}
				catch { return ProjectsData.ProjectsByStatus.AllProjects; }
			}
		}
		private bool LoadProjects()
		{
			SqlDataReader reader = null;
			
			try
			{
				reader = ProjectsData.SelectProjectNamesClear(this.ProjectsStatus, int.Parse(ddlBuildingTypes.SelectedValue));
			
				ddlProject.DataSource = reader;
				ddlProject.DataValueField = "ProjectID";
				ddlProject.DataTextField = "ProjectName";
				ddlProject.DataBind();

				ddlProject.Items.Insert(0, new ListItem("<"+Resource.ResourceManager["reports_ddlAllProjects"]+">", "-1"));
				ddlProject.SelectedValue = "-1";
			}
			catch (Exception ex)
			{
				log.Error(ex);
				return false;
			}

			finally
			{
				if (reader!=null) reader.Close();
			}
			return true;
		}

		private void ddlProjectsStatus_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			 LoadProjects();
		}

		private void Dropdownlist1_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			 LoadProjects();
		}
		#region ShowNotebook
//		private void ShowNotebook(int ID)
//		{
//			PdfExport pdf = new PdfExport();
//			pdf.NeverEmbedFonts = "";
//
//
//			MeetingData md = MeetingDAL.Load(ID);
//			if(md!=null)
//			{
//				DataDynamics.ActiveReports.ActiveReport report =new rptTefter(md.ProjectName, string.Concat(md.MeetingDate.ToShortDateString()," ",md.StartHour,"-",md.EndHour));
//			
//				report.Run();
//				pdf.Export(report.Document,
//				Response.Clear();
//
//				Response.AppendHeader( "content-disposition","attachment; filename="+"notebook.pdf");
//				Response.ContentType = "application/pdf";
//
//
//				memoryFile.WriteTo(Response.OutputStream); 
//				Response.End(); 
//			}
//		}
		#endregion

		private void PDFExport(int ID)
		{
			PdfExport pdf = new PdfExport();
			pdf.NeverEmbedFonts = "";
			MeetingData md = MeetingDAL.Load(ID);
			string time = string.Concat(md.StartHour.ToString()," - ",md.EndHour.ToString());
			DataView dvProjects = ReportsData.ExecuteReportsMeetingProjecsAnalysis(md.MeetingID);
			DataTable dtProjects = dvProjects.Table;
			if (dtProjects.Rows.Count == 0)
			{
				DataRow dr = dtProjects.NewRow();
				dr[0] = md.ProjectName;
				dtProjects.Rows.Add(dr);
			 }
			string[] arr = md.Users.Split(';');
			DataTable dtUsers = new DataTable();
			dtUsers.Columns.Add("ProjectName");
			foreach (string s in arr)
			{
				int user = UIHelpers.ToInt(s);
				if (user!=-1)
				{
					UserInfo ud = UsersData.SelectUserByID(user); 				
					DataRow dr = dtUsers.NewRow();
					dr[0]=ud.FullName;
					dtUsers.Rows.Add(dr);
				}
			}	
			DataView dvUsers = new DataView(dtUsers);
			string[] arr1 = md.Subcontracters.Split(';');
			DataTable dtSub = new DataTable();
			dtSub.Columns.Add("ProjectName");
			foreach (string s in arr1)
			{
				int sub = UIHelpers.ToInt(s);
				if(sub!=-1)
				{
					SubcontracterData sd = SubcontracterDAL.Load(sub); 
					DataRow dr = dtSub.NewRow();
					dr[0]=sd.SubcontracterName;
					dtSub.Rows.Add(dr);
				}
			}	
			DataView dvSub = new DataView(dtSub);
			string[] arr2 = md.Clients.Split(';');
			DataTable dtClients = new DataTable();
			dtClients.Columns.Add("ProjectName");
			foreach (string s in arr2)
			{
				int client = UIHelpers.ToInt(s);
				if (client!=-1)
				{
					ClientData cd = ClientDAL.Load(client);		
					DataRow dr = dtClients.NewRow();
					dr[0]=cd.ClientName;
					dtClients.Rows.Add(dr);
				}
			}	
			DataView dvClients = new DataView(dtClients);
			
			//			dv1.RowFilter="ProjectID is not null";
			
			DataDynamics.ActiveReports.ActiveReport report = new MeetingRpt(true, md.MeetingDate.ToShortDateString(), 
				time, md.Place, dvProjects, dvUsers, dvSub, dvClients, md.OtherPeople, md.Notes);
			report.PageSettings.Orientation=DataDynamics.ActiveReports.Document.PageOrientation.Default;
			
			


			report.Run();

			System.IO.MemoryStream memoryFile = new System.IO.MemoryStream();
			
			pdf.Export(report.Document, memoryFile);
		
			Response.Clear();
			Response.AppendHeader( "content-disposition"
				,"attachment; filename="+"MeetingReport.pdf");
			Response.ContentType = "application/pdf";
				
			memoryFile.WriteTo(Response.OutputStream); 
			Response.End();	
		}

		private void btnProtokols_Click(object sender, System.EventArgs e)
		{
			Response.Redirect("Protokols.aspx");
		}

		private void grdMeetings_SortCommand(object source, System.Web.UI.WebControls.DataGridSortCommandEventArgs e)
		{
			ViewState.Add("Sort",Sorting.SetSort(grdMeetings,(string)ViewState["Sort"],e.SortExpression));

			BindGrid();
		}
	}
}
