using System;
using GrapeCity.ActiveReports;
using GrapeCity.ActiveReports.Controls;
using GrapeCity.ActiveReports.SectionReportModel;
using GrapeCity.ActiveReports.Document.Section;
using GrapeCity.ActiveReports.Document;

namespace Asa.Timesheet.WebPages.Reports
{
    public class AbsenceSubreport : GrapeCity.ActiveReports.SectionReport
	{
		//int _givenTotal = 0, _archiveTotal = 0;

        public AbsenceSubreport()
        {
            InitializeComponent();
        }



        private void Detail_Format(object sender, System.EventArgs eArgs)
        {
            this.Detail.SizeToFit(true);
        }

        private void FoldersSubreport_FetchData(object sender, FetchEventArgs eArgs)
        {
            if (eArgs.EOF) return;
        }

		
		#region ActiveReports Designer generated code






        public void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(AbsenceSubreport));
            this.Detail = new GrapeCity.ActiveReports.SectionReportModel.Detail();
            this.ReportHeader = new GrapeCity.ActiveReports.SectionReportModel.ReportHeader();
            this.ReportFooter = new GrapeCity.ActiveReports.SectionReportModel.ReportFooter();
            this.txtAbsence = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtFoldersArchive = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtDaysString = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            ((System.ComponentModel.ISupportInitialize)(this.txtAbsence)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtFoldersArchive)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDaysString)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            // 
            // Detail
            // 
            this.Detail.ColumnSpacing = 0F;
            this.Detail.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
						this.txtAbsence,
						this.txtFoldersArchive,
						this.txtDaysString});
            this.Detail.Height = 0.3534722F;
            this.Detail.Name = "Detail";
            // 
            // ReportHeader
            // 
            this.ReportHeader.Height = 0F;
            this.ReportHeader.Name = "ReportHeader";
            // 
            // ReportFooter
            // 
            this.ReportFooter.Height = 0.1979167F;
            this.ReportFooter.Name = "ReportFooter";
            // 
            // txtAbsence
            // 
            this.txtAbsence.DataField = "Name";
            this.txtAbsence.Height = 0.2F;
            this.txtAbsence.Left = 0F;
            this.txtAbsence.Name = "txtAbsence";
            this.txtAbsence.OutputFormat = resources.GetString("txtAbsence.OutputFormat");
            this.txtAbsence.Style = "font-size: 10pt; font-weight: bold";
            this.txtAbsence.Top = 0F;
            this.txtAbsence.Width = 1.968504F;
            // 
            // txtFoldersArchive
            // 
            this.txtFoldersArchive.DataField = "DaysCount";
            this.txtFoldersArchive.Height = 0.2F;
            this.txtFoldersArchive.Left = 5.118111F;
            this.txtFoldersArchive.Name = "txtFoldersArchive";
            this.txtFoldersArchive.OutputFormat = resources.GetString("txtFoldersArchive.OutputFormat");
            this.txtFoldersArchive.Style = "font-size: 10pt; text-align: center";
            this.txtFoldersArchive.Top = 0F;
            this.txtFoldersArchive.Width = 1.574803F;
            // 
            // txtDaysString
            // 
            this.txtDaysString.DataField = "DaysString";
            this.txtDaysString.Height = 0.2F;
            this.txtDaysString.Left = 1.968504F;
            this.txtDaysString.Name = "txtDaysString";
            this.txtDaysString.Top = 0F;
            this.txtDaysString.Width = 3.149606F;
            // 
            // ActiveReport1
            // 
            this.MasterReport = false;
            this.PageSettings.DefaultPaperSize = false;
            this.PageSettings.Margins.Bottom = 0.8034722F;
            this.PageSettings.Margins.Left = 0.8034722F;
            this.PageSettings.Margins.Right = 0.8034722F;
            this.PageSettings.Margins.Top = 0.8034722F;
            this.PageSettings.PaperHeight = 11.69306F;
            this.PageSettings.PaperKind = System.Drawing.Printing.PaperKind.A4;
            this.PageSettings.PaperWidth = 8.268056F;
            this.PrintWidth = 6.692914F;
            this.Sections.Add(this.ReportHeader);
            this.Sections.Add(this.Detail);
            this.Sections.Add(this.ReportFooter);
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule(resources.GetString("$this.StyleSheet"), "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: inherit; font-style: inherit; font-variant: inherit; font-weight: bo" +
                        "ld; font-size: 16pt; font-size-adjust: inherit; font-stretch: inherit", "Heading1", "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: Times New Roman; font-style: italic; font-variant: inherit; font-wei" +
                        "ght: bold; font-size: 14pt; font-size-adjust: inherit; font-stretch: inherit", "Heading2", "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: inherit; font-style: inherit; font-variant: inherit; font-weight: bo" +
                        "ld; font-size: 13pt; font-size-adjust: inherit; font-stretch: inherit", "Heading3", "Normal"));
            ((System.ComponentModel.ISupportInitialize)(this.txtAbsence)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtFoldersArchive)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDaysString)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();

            // Attach Report Events
            this.Detail.Format += new System.EventHandler(this.Detail_Format);
            this.FetchData += new GrapeCity.ActiveReports.SectionReport.FetchEventHandler(this.FoldersSubreport_FetchData);
        }

		#endregion

        private ReportHeader ReportHeader;
        private Detail Detail;
        private TextBox txtAbsence;
        private TextBox txtFoldersArchive;
        private TextBox txtDaysString;
        private ReportFooter ReportFooter;
	}
}
