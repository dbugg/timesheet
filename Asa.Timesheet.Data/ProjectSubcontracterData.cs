using System;
using System.Collections;
using System.Text;

namespace Asa.Timesheet.Data
{
	public class ProjectSubcontracterData
	{
		private int _projectSubcontracterID = -1;
		private int _projectID = -1;
		private int _subcontracterID = -1;
		private int _paymentSchemeID = -1;
		private decimal _area = -1;
		private decimal _rate;
		private decimal _totalAmount;
		private DateTime _startDate;
		private DateTime _endDate;
		private string _note = String.Empty;
		private bool _done;
		private int _foldersGiven = -1;
		private int _fordersArchive = -1;
		private string _subcontracterType= String.Empty;
		private string _subcontracterName= String.Empty;
		private int _subprojectID = -1;
		/// <summary>
		/// Initialize an new empty Login object.
		/// </summary>
		public ProjectSubcontracterData()
		{
		}
		
		/// <summary>
		/// Initialize a new Login object with the given parameters.
		/// </summary>
		public ProjectSubcontracterData(int projectSubcontracterID, int projectID, int subcontracterID, int paymentSchemeID, int area, decimal rate, decimal totalAmount, DateTime startDate, DateTime endDate, string note, bool done, int foldersGiven, int fordersArchive)
		{
			this._projectSubcontracterID = projectSubcontracterID;
			this._projectID = projectID;
			this._subcontracterID = subcontracterID;
			this._paymentSchemeID = paymentSchemeID;
			this._area = area;
			this._rate = rate;
			this._totalAmount = totalAmount;
			this._startDate = startDate;
			this._endDate = endDate;
			this._note = note;
			this._done = done;
			this._foldersGiven = foldersGiven;
			this._fordersArchive = fordersArchive;
		}	
		
	  
		public override string ToString()
		{
			StringBuilder builder = new StringBuilder();
			builder.Append("_projectSubcontracterID: " + _projectSubcontracterID.ToString()).Append("\r\n");
			builder.Append("_projectID: " + _projectID.ToString()).Append("\r\n");
			builder.Append("_subcontracterID: " + _subcontracterID.ToString()).Append("\r\n");
			builder.Append("_paymentSchemeID: " + _paymentSchemeID.ToString()).Append("\r\n");
			builder.Append("_area: " + _area.ToString()).Append("\r\n");
			builder.Append("_rate: " + _rate.ToString()).Append("\r\n");
			builder.Append("_totalAmount: " + _totalAmount.ToString()).Append("\r\n");
			builder.Append("_startDate: " + _startDate.ToString()).Append("\r\n");
			builder.Append("_endDate: " + _endDate.ToString()).Append("\r\n");
			builder.Append("_note: " + _note.ToString()).Append("\r\n");
			builder.Append("_done: " + _done.ToString()).Append("\r\n");
			builder.Append("_foldersGiven: " + _foldersGiven.ToString()).Append("\r\n");
			builder.Append("_fordersArchive: " + _fordersArchive.ToString()).Append("\r\n");
			builder.Append("\r\n");
			return builder.ToString();
		}	
		public object GetByIndex(int nIndex)
		{
			switch(nIndex)
			{
				case 0:
					return _projectSubcontracterID;
				case 1:
					return _projectID;
				case 2:
					return _subcontracterID;
				case 3:
					return _paymentSchemeID;
				case 4:
					return _area;
				case 5:
					return _rate;
				case 6:
					return _totalAmount;
				case 7:
					return _startDate;
				case 8:
					return _endDate;
				case 9:
					return _note;
				case 10:
					return _done;
				case 11:
					return _foldersGiven;
				case 12:
					return _fordersArchive;
			}
			return null;
		}
		#region Properties
		public int SubprojectID
		{
			get { return _subprojectID; }
			set { _subprojectID = value; }
		}
		
		public string SubcontracterType
		{
			get { return _subcontracterType; }
			set { _subcontracterType = value; }
		}
		public string SubcontracterName
		{
			get { return _subcontracterName; }
			set { _subcontracterName = value; }
		}
		public int ProjectSubcontracterID
		{
			get { return _projectSubcontracterID; }
			set { _projectSubcontracterID = value; }
		}
	
		public int ProjectID
		{
			get { return _projectID; }
			set { _projectID = value; }
		}
	
		public int SubcontracterID
		{
			get { return _subcontracterID; }
			set { _subcontracterID = value; }
		}
	
		public int PaymentSchemeID
		{
			get { return _paymentSchemeID; }
			set { _paymentSchemeID = value; }
		}
	
		public decimal Area
		{
			get { return _area; }
			set { _area = value; }
		}
	
		public decimal Rate
		{
			get { return _rate; }
			set { _rate = value; }
		}
	
		public decimal TotalAmount
		{
			get { return _totalAmount; }
			set { _totalAmount = value; }
		}
	
		public DateTime StartDate
		{
			get { return _startDate; }
			set { _startDate = value; }
		}
	
		public DateTime EndDate
		{
			get { return _endDate; }
			set { _endDate = value; }
		}
	
		public string Note
		{
			get { return _note; }
			set { _note = value; }
		}
	
		public bool Done
		{
			get { return _done; }
			set { _done = value; }
		}
	
		public int FoldersGiven
		{
			get { return _foldersGiven; }
			set { _foldersGiven = value; }
		}
	
		public int FordersArchive
		{
			get { return _fordersArchive; }
			set { _fordersArchive = value; }
		}
	
		#endregion
	}

    public class ProjectSubcontracterData2 : ProjectSubcontracterData
    {
        string _email;

        public string Email
        {
            get { return _email; }
            set { _email = value; }
        }
    }
}


