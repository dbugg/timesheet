﻿<%@ Register TagPrefix="uc1" TagName="PageHeader" Src="../UserControls/PageHeader.ascx" %>
<%@ Page language="c#" Codebehind="BuildingPercentFinished.aspx.cs" AutoEventWireup="false" Inherits="Asa.Timesheet.WebPages.Reports.BuildingPercentFinished" smartNavigation="True"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
	<head runat="server">
		<title>BuildingPercentFinished</title>
		<meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
		<meta content="C#" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<LINK href="../styles/timesheet.css" type="text/css" rel="stylesheet">
	</HEAD>
	<body bottomMargin="0" leftMargin="0" topMargin="0" rightMargin="0">
		<form id="Form1" method="post" runat="server">
			<table height="100%" cellSpacing="0" cellPadding="0" width="100%" border="0">
				<tr height="1">
					<td><uc1:pageheader id="header" runat="server"></uc1:pageheader></td>
				</tr>
				<tr>
					<td>
						<table height="100%" cellSpacing="0" cellPadding="0" width="100%" border="0">
							<tr>
								<td class="menuLeft"><asp:placeholder id="menuHolder" runat="server"></asp:placeholder></td>
								
								<td style="PADDING-RIGHT: 5px; PADDING-LEFT: 5px; PADDING-TOP: 3px" vAlign="top" noWrap>
									<TABLE id="tblInfo" height="30" cellSpacing="0" cellPadding="3" width="880" border="0">
										<TR>
											<td vAlign="middle" noWrap><asp:label id="lblInfo" runat="server" EnableViewState="False" CssClass="InfoLabel"></asp:label><asp:label id="lblError" runat="server" EnableViewState="False" CssClass="ErrorLabel" ForeColor="Red"></asp:label></TD>
										</TR>
									</TABLE>
									<TABLE id="Table3" cellSpacing="0" cellPadding="0" width="880" border="0">
										<TR>
											<td style="HEIGHT: 5px" colSpan="2"></TD>
										</TR>
										<TR>
											<td colSpan="2"><IMG height="1" src="../images/dot.gif" width="100%"></TD>
										</TR>
										<TR>
											<td colSpan="2" height="3"></td>
										</TR>
										<tr>
											<td>
												<table cellSpacing="0" cellPadding="3" border="0">
													<TR>
														<td style="WIDTH: 139px" vAlign="top"><asp:label id="Label1" runat="server" CssClass="enterDataLabel" Width="100%">Тип:</asp:label></TD>
														<td style="WIDTH: 243px" vAlign="top"><asp:dropdownlist id="ddlProjectStatus" runat="server" CssClass="EnterDataBox" Width="250px" AutoPostBack="True"></asp:dropdownlist></TD>
														<td vAlign="top"></TD>
														<td></TD>
														<td></TD>
														<td style="WIDTH: 8px"></TD>
														<td style="WIDTH: 8px"></TD>
													</TR>
													<TR>
														<td style="WIDTH: 139px" vAlign="top"><asp:label id="Label7" runat="server" CssClass="enterDataLabel" Width="100%">Сграда:</asp:label></TD>
														<td style="WIDTH: 243px" vAlign="top"><asp:dropdownlist id="ddlBuildingTypes" runat="server" CssClass="enterDataBox" Width="250px" AutoPostBack="True"></asp:dropdownlist></TD>
														<td vAlign="top"></TD>
														<td></TD>
														<td></TD>
														<td style="WIDTH: 8px"></TD>
														<td style="WIDTH: 8px"></TD>
													</TR>
													<TR>
														<td style="WIDTH: 139px" vAlign="top"><asp:label id="Label2" runat="server" CssClass="enterDataLabel" Width="100%">Проект:</asp:label></TD>
														<td style="WIDTH: 243px" vAlign="top"><asp:dropdownlist id="ddlProjects" runat="server" CssClass="EnterDataBox" Width="250px"></asp:dropdownlist></TD>
														<td vAlign="top"></TD>
														<td></TD>
														<td></TD>
														<td style="WIDTH: 8px"></TD>
														<td style="WIDTH: 8px"></TD>
													</TR>
													<TR>
														<td style="WIDTH: 139px"><asp:label id="lbPhase" runat="server" CssClass="enterDataLabel" Width="100%">Фаза:</asp:label></TD>
														<td style="WIDTH: 243px" vAlign="top"><asp:dropdownlist id="ddlPhases" runat="server" CssClass="EnterDataBox" Width="250px"></asp:dropdownlist></TD>
														<td></TD>
														<td><asp:imagebutton id="ibGenerate" runat="server" ImageUrl="../images/ie.gif"></asp:imagebutton></TD>
														<td style="WIDTH: 8px"><asp:imagebutton id="ibPdfExport" runat="server" ImageUrl="../images/pdf1.gif" ToolTip="Справка в PDF формат"></asp:imagebutton></TD>
														<td style="WIDTH: 8px"><asp:imagebutton id="ibXlsExport" runat="server" ImageUrl="../images/xls.gif" ToolTip="Справка в Excel формат"></asp:imagebutton></TD>
													</TR>
												</table>
											</td>
										</tr>
										<TR>
											<td colSpan="2"><IMG height="1" src="../images/dot.gif" width="100%"></TD>
										</TR>
										<TR>
											<td colSpan="2"><IMG height="1" src="images/dot.gif" width="100%"></TD>
										</TR>
									</TABLE>
									<P>&nbsp;</P>
									<TABLE id="Table5" cellSpacing="0" cellPadding="0" width="100%" border="0">
										<TR>
											<td align="center"><asp:label id="lbSubs" runat="server" EnableViewState="False" Visible="False" Font-Bold="True"
													Font-Size="11pt"> СПРАВКА ЗА ИЗПЪЛНЕНИЕ ПО ПОДИЗПЪЛНИТЕЛИ</asp:label></TD>
										</TR>
									</TABLE>
									<asp:datagrid id="grdSub" runat="server" EnableViewState="False" CssClass="ReportGrid" ForeColor="DimGray"
										Width="100%" ShowHeader="True" AutoGenerateColumns="False" CellPadding="4">
										<HeaderStyle CssClass="reportGridHeader"></HeaderStyle>
										<Columns>
											<asp:BoundColumn DataField="ProjectID" HeaderText="" Visible="false"></asp:BoundColumn>
											<asp:BoundColumn DataField="ProjectSubcontracterID" HeaderText="" Visible="false"></asp:BoundColumn>
											<asp:BoundColumn DataField="projectname" HeaderText="Проект"></asp:BoundColumn>
											<asp:BoundColumn DataField="SubcontracterName" HeaderText="Подизпълнител"></asp:BoundColumn>
											<asp:BoundColumn DataField="Subactivity" HeaderText="Дейност"></asp:BoundColumn>
											<asp:TemplateColumn HeaderText="Изпълнено">
												<HeaderStyle HorizontalAlign="Center"></HeaderStyle>
												<ItemStyle HorizontalAlign="Center" Width="40px"></ItemStyle>
												<ItemTemplate>
													<asp:Image id=imgDone ImageUrl='<%# GetImage(DataBinder.Eval(Container, "DataItem.Done")) %>' Runat=server>
													</asp:Image>
												</ItemTemplate>
											</asp:TemplateColumn>
										</Columns>
									</asp:datagrid><asp:label id="lblNoDataFound1" runat="server" EnableViewState="False" CssClass="InfoLabel"
										Visible="False"></asp:label>
									<TABLE id="Table5" cellSpacing="0" cellPadding="0" width="100%" border="0">
										<TR>
											<td align="center"><BR>
												<BR>
												<asp:label id="lblReportTitle" runat="server" EnableViewState="False" Visible="False" Font-Bold="True"
													Font-Size="11pt"> СПРАВКА ЗА ИЗПЪЛНЕНИЕ ПО ОБЕКТИ</asp:label></TD>
										</TR>
									</TABLE>
									<br>
									<asp:datagrid id="grdReport" runat="server" EnableViewState="False" CssClass="ReportGrid" ForeColor="DimGray"
										Width="100%" ShowHeader="True" AutoGenerateColumns="False" CellPadding="4">
										<HeaderStyle CssClass="reportGridHeader"></HeaderStyle>
										<Columns>
											<asp:BoundColumn DataField="ContentID" Visible="false"></asp:BoundColumn>
											<asp:BoundColumn DataField="projectname" HeaderText="Съдържание"></asp:BoundColumn>
											<asp:BoundColumn DataField="ContentCode" HeaderText="Код" HeaderStyle-Width="70px"></asp:BoundColumn>
											<asp:BoundColumn DataField="ContentFinishedPhase1" HeaderText="% по Идеен Проект" HeaderStyle-Width="180px"
												ItemStyle-HorizontalAlign="Center"></asp:BoundColumn>
											<asp:BoundColumn DataField="ContentFinishedPhase2" HeaderText="% по Технически Проект" HeaderStyle-Width="180px"
												ItemStyle-HorizontalAlign="Center"></asp:BoundColumn>
											<asp:BoundColumn DataField="ContentFinishedPhase3" HeaderText="% по Работен Проект" HeaderStyle-Width="180px"
												ItemStyle-HorizontalAlign="Center"></asp:BoundColumn>
										</Columns>
									</asp:datagrid>
									<P><BR>
										<asp:label id="lblNoDataFound" runat="server" EnableViewState="False" CssClass="InfoLabel"
											Visible="False"></asp:label></P>
									<P><BR>
										<asp:label id="lblCreatedByLbl" runat="server" EnableViewState="False" Visible="False" Font-Bold="True"
											Font-Size="9pt">ИЗГОТВИЛ СПРАВКАТА:</asp:label><asp:label id="lblCreatedBy" runat="server" EnableViewState="False" Visible="False" Font-Size="9pt"></asp:label><BR>
									</P>
								</td>
							</tr>
						</table>
					</td>
				</tr>
			</table>
		</form>
	</body>
</HTML>
