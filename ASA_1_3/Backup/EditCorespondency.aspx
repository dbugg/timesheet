<%@ Page language="c#" Codebehind="EditCorespondency.aspx.cs" AutoEventWireup="false" Inherits="Asa.Timesheet.WebPages.EditCorespondency" %>
<%@ Register TagPrefix="uc1" TagName="EditForm" Src="UserControls/EditForm.ascx" %>
<%@ Register TagPrefix="uc1" TagName="PageHeader" Src="UserControls/PageHeader.ascx" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
	<HEAD>
		<title>EditCorespondency</title>
		<meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
		<meta content="C#" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<LINK href="styles/timesheet.css" type="text/css" rel="stylesheet">
		<script language="javascript" src="PopupCalendar.js"></script>
	</HEAD>
	<body bottomMargin="0" leftMargin="0" topMargin="0" rightMargin="0">
		<form id="Form1" method="post" runat="server">
			<table height="100%" cellSpacing="0" cellPadding="0" width="100%" border="0">
				<tr height="1">
					<td><uc1:pageheader id="header" runat="server"></uc1:pageheader><uc1:editform id="editCtrl" runat="server"></uc1:editform></td>
				</tr>
				<tr>
					<td>
						<table height="100%" cellSpacing="0" cellPadding="0" width="100%" border="0">
							<tr>
								<td class="menuLeft"><asp:placeholder id="menuHolder" runat="server"></asp:placeholder></td>
								<td style="PADDING-LEFT: 5px; PADDING-TOP: 3px" vAlign="top" noWrap>
									<TABLE id="Table3" cellSpacing="0" cellPadding="0" width="880" border="0">
										<TR>
											<td style="HEIGHT: 5px" colSpan="2"></TD>
										</TR>

										<TR>
											<td colSpan="2" height="3"></td>
										</TR>
										<TR>
											<td style="WIDTH: 4px"></TD>
											<td>
												<TABLE id="Table6" cellSpacing="0" cellPadding="0" border="0">
													<TR>
														<td style="WIDTH: 97px"><asp:button id="btnSaveUP" runat="server" CssClass="ActionButton" EnableViewState="False" Text="Запиши"></asp:button><asp:button id="btnEditUP" style="DISPLAY: none" runat="server" CssClass="ActionButton" EnableViewState="False"
																Text="Редактирай"></asp:button></TD>
														<td style="WIDTH: 245px"><asp:button id="btnCancelUP" runat="server" CssClass="ActionButton" EnableViewState="False"
																Text="Откажи"></asp:button></TD>
														<td></TD>
													</TR>
												</TABLE>
											</TD>
										</TR>
										<TR>
											<td height="3"></TD>
										</TR>

										<TR>
											<td style="WIDTH: 4px"></TD>
											<td>
												<TABLE id="tblForm" cellSpacing="0" cellPadding="3" width="100%" border="0">
													<TBODY>
														<TR>
															<td style="WIDTH: 168px"><asp:label id="Label1" runat="server" CssClass="enterDataLabel" Width="100%" Visible="True"
																	EnableViewState="False">Тип:</asp:label></TD>
															<td><asp:dropdownlist id="ddlProjectsStatus" runat="server" CssClass="EnterDataBox" Width="250px" AutoPostBack="True"></asp:dropdownlist></TD>
														</TR>
														<TR>
															<td style="WIDTH: 168px"><asp:label id="Label8" runat="server" CssClass="enterDataLabel" Width="100%">Сграда:</asp:label></TD>
															<td><asp:dropdownlist id="ddlBuildingTypes" runat="server" CssClass="enterDataBox" Width="250px" AutoPostBack="True"></asp:dropdownlist></TD>
														</TR>
														<TR>
															<td style="WIDTH: 168px"><asp:label id="lblName" runat="server" CssClass="enterDataLabel" Width="100%" Visible="True"
																	EnableViewState="False">Проект:</asp:label></TD>
															<td><asp:dropdownlist id="ddlProject" runat="server" CssClass="EnterDataBox" Width="250px" AutoPostBack="True"></asp:dropdownlist></TD>
														</TR>
														<TR>
															<td style="WIDTH: 168px"><asp:label id="lbClient" runat="server" CssClass="enterDataLabel" Width="100%">Клиент:</asp:label></TD>
															<td><asp:dropdownlist id="ddlClients" runat="server" CssClass="EnterDataBox" Width="250px"></asp:dropdownlist></TD>
														</TR>
														<TR>
															<td style="WIDTH: 168px"><asp:label id="lbEnterPerson" runat="server" CssClass="enterDataLabel" Width="100%">Входящ номер:</asp:label></TD>
															<td><asp:textbox id="txtEnterNumber" runat="server" CssClass="enterDataBox" Width="88px" MaxLength="10"></asp:textbox></TD>
														</TR>
														<TR>
															<td style="WIDTH: 168px"><asp:label id="lbDate" runat="server" CssClass="enterDataLabel" Width="100%" Font-Bold="True">Дата:</asp:label></TD>
															<td><asp:textbox id="txtStartDate" runat="server" CssClass="enterDataBox" Width="88px" MaxLength="10"></asp:textbox>&nbsp;<IMG id="lkCalendar1" alt="Calendar" src="images/calendar.ico" width="18" align="absBottom"
																	runat="server">&nbsp;<IMG height="16" alt="" src="images/required1.gif" width="16"></TD>
														</TR>
														
														<TR>
															<td style="WIDTH: 168px"></TD>
															<td><asp:radiobuttonlist id="rbSendRes" runat="server" RepeatDirection="Horizontal"></asp:radiobuttonlist></TD>
														</TR>
														<TR>
															<td style="WIDTH: 168px"><asp:label id="lbEnterNumber" runat="server" CssClass="enterDataLabel" Width="100%">Изпратено до/получено от:</asp:label></TD>
															<td><asp:textbox id="txtPerson" runat="server" CssClass="enterDataBox" Width="250px" MaxLength="200"></asp:textbox></TD>
														</TR>
														<TR>
															<td style="WIDTH: 168px"><asp:label id="lbMailingType" runat="server" CssClass="enterDataLabel" Width="100%" Font-Bold="True">Тип:</asp:label></TD>
															<td><asp:CheckBoxList id="cblMailingType" Runat="server" RepeatDirection="Horizontal" CssClass="enterDataBox"></asp:CheckBoxList></TD>
														</TR>
														<TR>
															<td style="WIDTH: 168px"><asp:label id="lbNotes" runat="server" CssClass="enterDataLabel" Width="100%">Забележки:</asp:label></TD>
															<td><asp:textbox id="txtNotes" runat="server" CssClass="enterDataBox" Width="248px" MaxLength="10"
																	TextMode="MultiLine"></asp:textbox></TD>
														</TR>
														<TR>
															<td style="WIDTH: 168px"><asp:label id="lbScanDocNew" runat="server" CssClass="enterDataLabel" Width="100%">Сканиран документ:</asp:label></TD>
															<td><INPUT id="FileCtrl" type="file" name="File1" runat="server">
																<asp:label id="lbSD" runat="server" Visible="False"></asp:label><asp:label id="lbExt" runat="server" Visible="False"></asp:label></TD>
														</TR>
														<TR>
															<td style="WIDTH: 168px"><asp:label id="lbSendLetter" runat="server" CssClass="enterDataLabel" Width="100%">Изпрати е-мейли:</asp:label></TD>
															<td><asp:CheckBox id="cbSendLetter" runat="server" CssClass="EnterDataBox"></asp:CheckBox></TD>
														</TR>
													</TBODY>
												</TABLE>
											</TD>
										</TR>
										<TR>
											<td style="HEIGHT: 5px" colSpan="2"></TD>
										</TR>
										<TR>
											<td colSpan="2" height="3"></td>
										</TR>
										<TR>
											<td style="WIDTH: 4px"></TD>
											<td>
												<TABLE id="Table5" cellSpacing="0" cellPadding="0" border="0">
													<TR>
														<td style="WIDTH: 97px"><asp:button id="btnSave" runat="server" CssClass="ActionButton" EnableViewState="False" Text="Запиши"></asp:button><asp:button id="btnEdit" style="DISPLAY: none" runat="server" CssClass="ActionButton" EnableViewState="False"
																Text="Редактирай"></asp:button></TD>
														<td style="WIDTH: 245px"><asp:button id="btnCancel" runat="server" CssClass="ActionButton" EnableViewState="False" Text="Откажи"></asp:button>&nbsp;<asp:ImageButton id="btnFile" runat="server" ToolTip="Изтегли" ImageUrl="images/pdf.gif"></asp:ImageButton></TD>
														<td><asp:button id="btnDelete" runat="server" CssClass="ActionButton" Visible="False" EnableViewState="False"
																Text="Изтрий"></asp:button></TD>
													</TR>
												</TABLE>
											</TD>
										</TR>
										<TR>
											<td height="3"></TD>
										</TR>
									</TABLE>
									<br>
									&nbsp;
									<asp:label id="lblInfo" runat="server" CssClass="InfoLabel" EnableViewState="False"></asp:label><asp:label id="lblError" runat="server" CssClass="ErrorLabel" EnableViewState="False" ForeColor="Red"></asp:label></td>
							</tr>
						</table>
					</td>
				</tr>
			</table>
		</form>
	</body>
</HTML>
