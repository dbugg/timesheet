using System;
using System.Data;
using Asa.Timesheet.Data;
using GrapeCity.ActiveReports;
using GrapeCity.ActiveReports.Controls;
using GrapeCity.ActiveReports.SectionReportModel;
using GrapeCity.ActiveReports.Document.Section;
using GrapeCity.ActiveReports.Document;

namespace Asa.Timesheet.WebPages.Reports
{
    public class ProjectSubcontractersSubreport : GrapeCity.ActiveReports.SectionReport
	{
		
		private bool _hpr;
        public ProjectSubcontractersSubreport(bool ProjectInfo, bool hpr)
        {
            _hpr = hpr;
            InitializeComponent();
            if (ProjectInfo)
            {

                //txtContractRep.Visible=txtContractRepInfo.Visible=
                //txtTechRep.Visible=txtTechRepInfo.Visible=false;
                //this.Detail.Height=this.Detail.Height/2;
            }
        }

        private void ProjectSubcontractersSubreport_FetchData(object sender, FetchEventArgs eArgs)
        {

        }

        private void Detail_Format(object sender, System.EventArgs eArgs)
        {
            if (this.Fields.Count > 0)
            {
                this.txtSubcontracter.Text = String.Concat(this.Fields["SubcontracterName"].Value.ToString(), " - ", this.Fields["SubcontracterType"].Value.ToString());

                System.Text.StringBuilder sb = new System.Text.StringBuilder();
                if (this.Fields["Phone"].Value != System.DBNull.Value && (string)this.Fields["Phone"].Value != "")
                {
                    sb.Append(Resource.ResourceManager["Reports_strPhone"] + ": " + this.Fields["Phone"].Value.ToString());
                    sb.Append(", ");
                }
                if (this.Fields["Fax"].Value != System.DBNull.Value && (string)this.Fields["Fax"].Value != "")
                {
                    sb.Append(Resource.ResourceManager["Reports_strFax"] + ": " + this.Fields["Fax"].Value.ToString());
                    sb.Append(", ");
                }
                if (this.Fields["Email"].Value != System.DBNull.Value && (string)this.Fields["Email"].Value != "")
                {
                    sb.Append(Resource.ResourceManager["Reports_strEmail"] + ": " + this.Fields["Email"].Value.ToString());
                    sb.Append(", ");
                }
                if (this.Fields["Webpage"].Value != System.DBNull.Value && (string)this.Fields["Webpage"].Value != "")
                {
                    sb.Append("Web site: " + this.Fields["Webpage"].Value.ToString());
                }
                this.txtSubcontracterInfo.Text = sb.ToString().TrimEnd(new char[] { ',', ' ' });


                #region Contract info

                //Contract Info
                System.Text.StringBuilder sbContractRepInfo = new System.Text.StringBuilder();
                if (this.Fields["Phone1"].Value.ToString().Length > 0)
                    sbContractRepInfo.Append(
                        String.Concat(Resource.ResourceManager["Reports_strPhone"], ": ",
                        this.Fields["Phone1"].Value.ToString()));

                if (this.Fields["Email1"].Value.ToString().Length > 0)
                {
                    if (sbContractRepInfo.Length > 0) sbContractRepInfo.Append(", ");
                    sbContractRepInfo.Append(
                        String.Concat(Resource.ResourceManager["Reports_strEmail"], ": ", this.Fields["Email1"].Value.ToString()));
                }
                //this.txtContractRepInfo.Text = sbContractRepInfo.ToString();

                #endregion

                //Technical info
                System.Text.StringBuilder sbTechRepInfo = new System.Text.StringBuilder();
                if (this.Fields["Phone2"].Value.ToString().Length > 0)
                    sbTechRepInfo.Append(
                        String.Concat(Resource.ResourceManager["Reports_strPhone"], ": ",
                        this.Fields["Phone2"].Value.ToString()));

                if (this.Fields["Email2"].Value.ToString().Length > 0)
                {
                    if (sbTechRepInfo.Length > 0) sbTechRepInfo.Append(", ");
                    sbTechRepInfo.Append(
                        String.Concat(Resource.ResourceManager["Reports_strEmail"], ": ", this.Fields["Email2"].Value.ToString()));
                }
                //this.txtTechRepInfo.Text = sbTechRepInfo.ToString();
            }
            else
                this.Detail.Visible = false;
        }
		public bool HasData
		{
			get { return ((DataTable)this.DataSource).Rows.Count > 0; }
		}
		#region ActiveReports Designer generated code








        public void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ProjectSubcontractersSubreport));
            this.Detail = new GrapeCity.ActiveReports.SectionReportModel.Detail();
            this.txtSubcontracter = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtSubcontracterInfo = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.Line1 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.TextBox3 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.TextBox4 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtAddress = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtManager = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            ((System.ComponentModel.ISupportInitialize)(this.txtSubcontracter)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSubcontracterInfo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TextBox3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TextBox4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAddress)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtManager)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            // 
            // Detail
            // 
            this.Detail.ColumnSpacing = 0F;
            this.Detail.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
						this.txtSubcontracter,
						this.txtSubcontracterInfo,
						this.Line1,
						this.TextBox3,
						this.TextBox4,
						this.txtAddress,
						this.txtManager});
            this.Detail.Height = 0.8229167F;
            this.Detail.Name = "Detail";
            // 
            // txtSubcontracter
            // 
            this.txtSubcontracter.DataField = "SubcontracterName";
            this.txtSubcontracter.Height = 0.2362205F;
            this.txtSubcontracter.Left = 0F;
            this.txtSubcontracter.Name = "txtSubcontracter";
            this.txtSubcontracter.Style = "font-size: 10pt; font-weight: bold";
            this.txtSubcontracter.Text = "TextBox2";
            this.txtSubcontracter.Top = 0F;
            this.txtSubcontracter.Width = 6.692914F;
            // 
            // txtSubcontracterInfo
            // 
            this.txtSubcontracterInfo.DataField = "FullName";
            this.txtSubcontracterInfo.Height = 0.1968504F;
            this.txtSubcontracterInfo.Left = 2.5F;
            this.txtSubcontracterInfo.Name = "txtSubcontracterInfo";
            this.txtSubcontracterInfo.Style = "font-size: 10pt; font-weight: normal";
            this.txtSubcontracterInfo.Text = "TextBox3";
            this.txtSubcontracterInfo.Top = 0.625F;
            this.txtSubcontracterInfo.Width = 4.125F;
            // 
            // Line1
            // 
            this.Line1.Height = 0F;
            this.Line1.Left = 0F;
            this.Line1.LineWeight = 1F;
            this.Line1.Name = "Line1";
            this.Line1.Top = 0F;
            this.Line1.Width = 6.692914F;
            this.Line1.X1 = 0F;
            this.Line1.X2 = 6.692914F;
            this.Line1.Y1 = 0F;
            this.Line1.Y2 = 0F;
            // 
            // TextBox3
            // 
            this.TextBox3.DataField = "FullName";
            this.TextBox3.Height = 0.1968504F;
            this.TextBox3.Left = 0F;
            this.TextBox3.Name = "TextBox3";
            this.TextBox3.Style = "font-size: 10pt; font-weight: normal";
            this.TextBox3.Text = "????? ?? ??????????:";
            this.TextBox3.Top = 0.2362205F;
            this.TextBox3.Width = 2.5F;
            // 
            // TextBox4
            // 
            this.TextBox4.DataField = "FullName";
            this.TextBox4.Height = 0.1968504F;
            this.TextBox4.Left = 0F;
            this.TextBox4.Name = "TextBox4";
            this.TextBox4.Style = "font-size: 10pt; font-weight: normal";
            this.TextBox4.Text = "?????????????? ??:";
            this.TextBox4.Top = 0.4330709F;
            this.TextBox4.Width = 2.5F;
            // 
            // txtAddress
            // 
            this.txtAddress.DataField = "Address";
            this.txtAddress.Height = 0.1968504F;
            this.txtAddress.Left = 2.505905F;
            this.txtAddress.Name = "txtAddress";
            this.txtAddress.Style = "font-size: 10pt; font-weight: bold";
            this.txtAddress.Text = "txtAddress";
            this.txtAddress.Top = 0.2362205F;
            this.txtAddress.Width = 4.119095F;
            // 
            // txtManager
            // 
            this.txtManager.DataField = "Manager";
            this.txtManager.Height = 0.1968504F;
            this.txtManager.Left = 2.505905F;
            this.txtManager.Name = "txtManager";
            this.txtManager.Style = "font-size: 10pt; font-weight: bold";
            this.txtManager.Text = "txtManager";
            this.txtManager.Top = 0.4330709F;
            this.txtManager.Width = 4.119095F;
            // 
            // ActiveReport1
            // 
            this.MasterReport = false;
            this.PageSettings.PaperHeight = 11F;
            this.PageSettings.PaperWidth = 8.5F;
            this.PrintWidth = 6.692914F;
            this.Sections.Add(this.Detail);
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule(resources.GetString("$this.StyleSheet"), "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: inherit; font-style: inherit; font-variant: inherit; font-weight: bo" +
                        "ld; font-size: 16pt; font-size-adjust: inherit; font-stretch: inherit", "Heading1", "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: Times New Roman; font-style: italic; font-variant: inherit; font-wei" +
                        "ght: bold; font-size: 14pt; font-size-adjust: inherit; font-stretch: inherit", "Heading2", "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: inherit; font-style: inherit; font-variant: inherit; font-weight: bo" +
                        "ld; font-size: 13pt; font-size-adjust: inherit; font-stretch: inherit", "Heading3", "Normal"));
            ((System.ComponentModel.ISupportInitialize)(this.txtSubcontracter)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSubcontracterInfo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TextBox3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TextBox4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAddress)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtManager)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();

            // Attach Report Events
            this.FetchData += new GrapeCity.ActiveReports.SectionReport.FetchEventHandler(this.ProjectSubcontractersSubreport_FetchData);
            this.Detail.Format += new System.EventHandler(this.Detail_Format);
        }

		#endregion

        private Detail Detail;
        private TextBox txtSubcontracter;
        private TextBox txtSubcontracterInfo;
        private Line Line1;
        private TextBox TextBox3;
        private TextBox TextBox4;
        private TextBox txtAddress;
        private TextBox txtManager;
	}
}
