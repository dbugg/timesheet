using System;
using System.Data;
using System.Data.SqlClient;
using Microsoft.ApplicationBlocks.Data;

namespace Asa.Timesheet.Data
{
	/// <summary>
	/// Summary description for MailsData.
	/// </summary>
	public class EmailsData
	{
		public static SqlDataReader SelectEmail(int emailID)
		{
			string spName = "EmailsSelProc";

			SqlParameter[] storedParams = SqlHelperParameterCache.GetSpParameterSet(DBManager.GetConnectionString(), spName);
			storedParams[0].Value = emailID;

			return SqlHelper.ExecuteReader(DBManager.GetConnectionString(), CommandType.StoredProcedure, spName, storedParams);
		}
		
		public static void UpdateEmail(int emailID, string name, string email)//, int clientID)
		{
			string spName = "EmailsUpdProc";

			SqlParameter[] storedParams = SqlHelperParameterCache.GetSpParameterSet(DBManager.GetConnectionString(), spName);
			
			storedParams[0].Value = emailID;
			storedParams[1].Value = name;
			storedParams[2].Value = email;
			//storedParams[3].Value = (clientID == 0) ? null : (object) clientID;

			SqlHelper.ExecuteNonQuery(DBManager.GetConnectionString(), CommandType.StoredProcedure, spName, storedParams);
		}

		public static int InsertEmail(string name, string email)//, int clientID)
		{
			string spName = "EmailsInsProc";

			SqlParameter[] storedParams = SqlHelperParameterCache.GetSpParameterSet(DBManager.GetConnectionString(), spName);
			
			storedParams[0].Value = name;
			storedParams[1].Value = email;
		//	storedParams[2].Value = (clientID == 0) ? null : (object) clientID;

			SqlHelper.ExecuteNonQuery(DBManager.GetConnectionString(), CommandType.StoredProcedure, spName, storedParams);

			return (int)storedParams[2].Value;
		}

		public static void DeleteEmail(int emailID)
		{
			string spName = "EmailsDelProc";

			SqlParameter[] storedParams = SqlHelperParameterCache.GetSpParameterSet(DBManager.GetConnectionString(), spName);
			storedParams[0].Value = emailID;

			SqlHelper.ExecuteNonQuery(DBManager.GetConnectionString(), CommandType.StoredProcedure, spName, storedParams);
		}

		public static SqlDataReader SelectEmails(int sortOrder)
		{
			string spName = "EmailsListProc";

			SqlParameter[] storedParams = SqlHelperParameterCache.GetSpParameterSet(DBManager.GetConnectionString(), spName);
			storedParams[0].Value = sortOrder;

			return SqlHelper.ExecuteReader(DBManager.GetConnectionString(), CommandType.StoredProcedure, spName, storedParams);
		}

	}
}
