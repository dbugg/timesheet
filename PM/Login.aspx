﻿<%@ Page Title="Добре дошли!" Language="C#" MasterPageFile="~/PM.Master" AutoEventWireup="True" CodeBehind="Login.aspx.cs" Inherits="PM.Login" %>
<%@ Register Assembly="GoogleReCaptcha" Namespace="GoogleReCaptcha" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

<table height="100%" cellSpacing="0" cellPadding="0" width="100%" border="0">

	<tr>
		<td>
			<table height="100%" cellSpacing="0" cellPadding="0" width="100%" border="0">
				<tr>
					<td style="PADDING-LEFT: 5px; PADDING-TOP: 3px" vAlign="top" noWrap>
                    <TABLE id="Table3" cellSpacing="0" width="890" border="0">
							<TR>
								<td style="WIDTH: 4px"></TD>
								<td>
									<TABLE id="Table4" cellSpacing="0" cellPadding="3" border="0">
										<TR>
											<td style="WIDTH: 141px" colSpan="1" rowSpan="1">
												<asp:label id="lblLoginName" runat="server" Font-Bold="True" CssClass="enterDataLabel" EnableViewState="False"></asp:label></TD>
											<td style="WIDTH: 207px">
												<asp:textbox id="txtLoginName" onkeydown="testForEnterR()" runat="server" Width="200px" CssClass="enterDataBox"></asp:textbox>&nbsp;</TD>
											<td style="WIDTH: 479px"></TD>
										</TR>
										<TR>
											<td style="WIDTH: 141px" colSpan="1" rowSpan="1">
												<asp:label id="lblPassword" runat="server" CssClass="enterDataLabel" Font-Bold="True"></asp:label></TD>
											<td style="WIDTH: 207px">
												<asp:textbox id="txtPassword" onkeydown="testForEnterR()" runat="server" Width="200px" CssClass="enterDataBox"
													TextMode="Password"></asp:textbox>&nbsp;</TD>
											<td style="WIDTH: 479px">
												<asp:button id="btnEnter" runat="server" CssClass="ActionButton" onclick="btnEnter_Click"></asp:button></TD>
										</TR>
                                                                                    <tr>
                                            <td></td>
                                            <td colspan="2" style="width: 479px" runat="server" id="rowCaptcha" visible="false">
                                            <cc1:GoogleReCaptcha ID="ctrlGoogleReCaptcha" runat="server" PublicKey="6Ld2e1YUAAAAAGnmKVDupVv9ViOhz-BMDoWVoC0y" PrivateKey="6Ld2e1YUAAAAAMBvAvjnaZt36JlY9suyaeZQPrqJ" />

                                                </td>
                                                </tr>
									</TABLE>
								</TD>
							</TR>
							<TR>
								<td style="HEIGHT: 5px" colSpan="2"></TD>
							</TR>
							<TR>
								<td colSpan="2"><IMG height="1" src="images/dot.gif" width="100%"></TD>
							</TR>
							<TR>
								<td style="WIDTH: 4px" height="3" colspan="2"></TD>
							</TR>
							<TR>
								<td style="WIDTH: 4px" colSpan="1" height="3"></TD>
								<td>&nbsp;
									<asp:label id="lblError" runat="server" ForeColor="Red" EnableViewState="False"></asp:label>
									<asp:label id="lblInfo" runat="server" EnableViewState="False"></asp:label></td>
							</TR>
						</TABLE>
						<BR>
						<BR>
						<BR>
					</td>
				</tr>
			</table>
		</td>
	</tr>
</table>
</asp:Content>