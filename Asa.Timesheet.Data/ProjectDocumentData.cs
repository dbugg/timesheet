using System;
using System.Collections;
using System.Text;

namespace Asa.Timesheet.Data
{
	public class ProjectDocumentData
	{
		private int _projectDocumentID = -1;
		private int _projectID = -1;
		private int _documentType = -1;
		private string _documentDiscription = string.Empty;
		private bool _isSubcontracterVisible = false;
		private bool _isClientVisible = false;
		private bool _isBuilderVisible = false;

		/// <summary>
		/// Initialize an new empty Login object.
		/// </summary>
		public ProjectDocumentData()
		{
		}
		
		/// <summary>
		/// Initialize a new Login object with the given parameters.
		/// </summary>
		public ProjectDocumentData(int projectDocumentID, int projectID, int documentType, string documentDiscription)
		{
			this._projectDocumentID = projectDocumentID;
			this._projectID = projectID;
			this._documentType = documentType;
			this._documentDiscription = documentDiscription;
		}	
		/// <summary>
		/// Initialize a new Login object with the given parameters.
		/// </summary>
		public ProjectDocumentData(int projectDocumentID, int projectID, int documentType, string documentDiscription, bool isSubcontracterVisible,bool isClientVisible,bool isBuilderVisible)
		{
			this._projectDocumentID = projectDocumentID;
			this._projectID = projectID;
			this._documentType = documentType;
			this._documentDiscription = documentDiscription;
			this._isSubcontracterVisible = isSubcontracterVisible;
			this._isClientVisible = isClientVisible;
			this._isBuilderVisible = isBuilderVisible;
		}	
	  
		public override string ToString()
		{
			StringBuilder builder = new StringBuilder();
			builder.Append("_projectDocumentID: " + _projectDocumentID.ToString()).Append("\r\n");
			builder.Append("_projectID: " + _projectID.ToString()).Append("\r\n");
			builder.Append("_documentType: " + _documentType.ToString()).Append("\r\n");
			builder.Append("_isSubcontracterVisible: " + _isSubcontracterVisible.ToString()).Append("\r\n");
			builder.Append("_isClientVisible: " + _isClientVisible.ToString()).Append("\r\n");
			builder.Append("_isBuilderVisible: " + _isBuilderVisible.ToString()).Append("\r\n");
			builder.Append("\r\n");
			return builder.ToString();
		}	
		public object GetByIndex(int nIndex)
		{
			switch(nIndex)
			{
				case 0:
					return _projectDocumentID;
				case 1:
					return _projectID;
				case 2:
					return _documentType;
				case 3:
					return _documentDiscription;
				case 4:
					return _isSubcontracterVisible;
				case 5:
					return _isClientVisible;
				case 6:
					return _isBuilderVisible;
			}
			return null;
		}
		#region Properties
	
		public int ProjectDocumentID
		{
			get { return _projectDocumentID; }
			set { _projectDocumentID = value; }
		}
	
		public int ProjectID
		{
			get { return _projectID; }
			set { _projectID = value; }
		}
	
		public int DocumentType
		{
			get { return _documentType; }
			set { _documentType = value; }
		}
		
		public string DocumentDiscription
		{
			get { return _documentDiscription; }
			set { _documentDiscription = value; }
		}

		public bool IsSubcontracterVisible
		{
			get { return _isSubcontracterVisible; }
			set { _isSubcontracterVisible = value; }
		}

		public bool IsClientVisible
		{
			get { return _isClientVisible; }
			set { _isClientVisible = value; }
		}

		public bool IsBuilderVisible
		{
			get { return _isBuilderVisible; }
			set { _isBuilderVisible = value; }
		}
		#endregion
	}
}


