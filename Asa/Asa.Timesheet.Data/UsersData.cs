using System;
using System.Data;
using System.Data.SqlClient;
using Microsoft.ApplicationBlocks.Data;
using Asa.Timesheet.Data.Entities;

namespace Asa.Timesheet.Data
{
	/// <summary>
	/// Summary description for UsersData.
	/// </summary>
	public class UsersData
	{
		public static void RestoreUser(int userID)
		{
			string spName = "UserRestoryProc";
			SqlParameter[] storedParams = SqlHelperParameterCache.GetSpParameterSet(DBManager.GetConnectionString(), spName);
			storedParams[0].Value = userID;

			SqlHelper.ExecuteNonQuery(DBManager.GetConnectionString(), CommandType.StoredProcedure, spName, storedParams);
		}
		public static SqlDataReader SelectUsersExEmploys(int loggedRoleID, int sortOrder, string spName)
		{
			
			
			SqlParameter[] storedParams = SqlHelperParameterCache.GetSpParameterSet(DBManager.GetConnectionString(), spName);
			storedParams[0].Value = loggedRoleID;
			storedParams[1].Value = sortOrder;

			return SqlHelper.ExecuteReader(DBManager.GetConnectionString(), CommandType.StoredProcedure, spName, storedParams);
		}
		public static UserInfo SelectUserByAccount(string account)
		{
			string spName = "UsersSelByAccountProc";

			SqlParameter[] storedParams = SqlHelperParameterCache.GetSpParameterSet(
				DBManager.GetConnectionString(), spName);
			storedParams[0].Value = account;
			
			UserInfo ui = new UserInfo();

			SqlDataReader reader = null;
			try
			{
				reader = SqlHelper.ExecuteReader(DBManager.GetConnectionString(), CommandType.StoredProcedure, spName, storedParams);
				return GetUserInfo(reader);
			}
			finally
			{
				if (reader!=null && !reader.IsClosed)
					reader.Close();
			}
			//return ui;
		}
		
		//peppi
		public static UserInfo SelectUserByID(int userID)
		{
			SqlDataReader reader =	SelectUser(userID);
			return GetUserInfo(reader);
		}
		
		private static UserInfo GetUserInfo(SqlDataReader reader)
		{
			UserInfo ui = new UserInfo();
			if (!reader.Read()) return ui;
			ui.UserID = reader.GetInt32(0);
			ui.UserRoleID = reader.IsDBNull(1) ? 0 : reader.GetInt32(1);
			ui.FirstName = reader.IsDBNull(2) ? String.Empty : reader.GetString(2);
			ui.MiddleName = reader.IsDBNull(3) ? String.Empty : reader.GetString(3);
			ui.LastName = reader.GetString(4);
			ui.Account = reader.GetString(5);
			ui.Mail = reader.GetString(6);
			ui.FullName = reader.IsDBNull(7) ? String.Empty : reader.GetString(7);
			ui.Occupation=reader.IsDBNull(11) ? String.Empty : reader.GetString(11);
			ui.UserRole=reader.IsDBNull(12) ? String.Empty : reader.GetString(12);
			ui.Ext=reader.IsDBNull(13) ? String.Empty : reader.GetString(13);
			ui.Mobile=reader.IsDBNull(14) ? String.Empty : reader.GetString(14);
			ui.HomePhone=reader.IsDBNull(15) ? String.Empty : reader.GetString(15);
			ui.CV=reader.IsDBNull(16) ? String.Empty : reader.GetString(16);
			ui.Salary=reader.IsDBNull(17) ?0 : reader.GetDecimal(17);
			ui.Bonus=reader.IsDBNull(18) ? 0 : reader.GetDecimal(18);
			// if (!reader.IsDBNull(19))
			//		{ui.IsTraine=reader.GetBoolean(19);}
			// else
			//		{ui.IsTraine=false;}
			if (!reader.IsDBNull(22))
			{ui.HasWorkTime=reader.GetBoolean(22);}
			else
			{ui.HasWorkTime=true;}
			ui.WorkStartedDate=reader.IsDBNull(23) ? Constants.DateMax : reader.GetDateTime(23);
			//Add by Ivailo date: 04.12.2007
			if (!reader.IsDBNull(24))
			{ui.HasWorkTimeOver=reader.GetBoolean(24);}
			else
			{ui.HasWorkTimeOver=true;}

			if (reader!=null && !reader.IsClosed) reader.Close();
			return ui;
		}

		public static SqlDataReader SelectUserNames()
		{
			
			string spName = "UsersNamesListProc";
			return SqlHelper.ExecuteReader(DBManager.GetConnectionString(), CommandType.StoredProcedure, spName);
		}
		public static SqlDataReader SelectUserNames(bool notForWorkTime)
		{
			string spName = "UsersNamesListProc";
			SqlParameter[] storedParams = SqlHelperParameterCache.GetSpParameterSet(DBManager.GetConnectionString(), spName);
			storedParams[3].Value = notForWorkTime;
			
			return SqlHelper.ExecuteReader(DBManager.GetConnectionString(), CommandType.StoredProcedure, spName, storedParams);
		}
		//Add by Ivailo date: 04.12.2007
		//notes: that function is only called from EmptyHours.aspx
		//notForWorkTimeOver - when a user has param HasWorkTimeOver = true, it don't show in the list of users in EmptyHours.aspx
		//this function is for users with param IsActive = false
		public static SqlDataReader SelectUserNames1(bool notForWorkTimeOver)
		{
			string spName = "UsersNamesListProc1";
		
			SqlParameter[] storedParams = SqlHelperParameterCache.GetSpParameterSet(DBManager.GetConnectionString(), spName);
			storedParams[3].Value = notForWorkTimeOver;

			return SqlHelper.ExecuteReader(DBManager.GetConnectionString(), CommandType.StoredProcedure, spName, storedParams);
		}
		public static SqlDataReader SelectUserNames1()
		{
			return SelectUserNames1(true);
		}
		public static SqlDataReader SelectUserNamesOnlyActive(bool notForWorkTime)
		{
			string spName = "UsersNamesListOnlyActiveProc";
			SqlParameter[] storedParams = SqlHelperParameterCache.GetSpParameterSet(DBManager.GetConnectionString(), spName);
			storedParams[0].Value = notForWorkTime;
			return SqlHelper.ExecuteReader(DBManager.GetConnectionString(), CommandType.StoredProcedure, spName,storedParams);
		}

		//Add by Ivailo date: 04.12.2007
		//notes: that function is only called from EmptyHours.aspx
		//notForWorkTimeOver - when a user has param HasWorkTimeOver = true, it don't show in the list of users in EmptyHours.aspx
		public static SqlDataReader SelectUserNames(bool includeInactive, bool includeSpecialUsers, bool notForWorkTime,bool notForWorkTimeOver)
		{
			string spName = "UsersNamesListProc";

			SqlParameter[] storedParams = SqlHelperParameterCache.GetSpParameterSet(DBManager.GetConnectionString(), spName);
			storedParams[0].Value = includeInactive;
			storedParams[1].Value = includeSpecialUsers;
			storedParams[3].Value = notForWorkTime;
			storedParams[4].Value = notForWorkTimeOver;

			return SqlHelper.ExecuteReader(DBManager.GetConnectionString(), CommandType.StoredProcedure, spName, storedParams);
		}
		public static SqlDataReader SelectUserNames(bool includeInactive, bool includeSpecialUsers, bool notForWorkTime)
		{
			return SelectUserNames(includeInactive, includeSpecialUsers, notForWorkTime, true);
		}

		public static SqlDataReader SelectUserNames(bool includeInactive,bool notForWorkTime)
		{
			string spName = "UsersNamesListProc";

			SqlParameter[] storedParams = SqlHelperParameterCache.GetSpParameterSet(DBManager.GetConnectionString(), spName);
			storedParams[0].Value = includeInactive;
			storedParams[3].Value = notForWorkTime;

			return SqlHelper.ExecuteReader(DBManager.GetConnectionString(), CommandType.StoredProcedure, spName, storedParams);
		}
		public static SqlDataReader SelectUserNamesConstruct(bool includeInactive)
		{
			string spName = "UsersNamesListProc";

			SqlParameter[] storedParams = SqlHelperParameterCache.GetSpParameterSet(DBManager.GetConnectionString(), spName);
			storedParams[0].Value = includeInactive;
			storedParams[2].Value = Resource.ResourceManager["Contructor"];
			
			return SqlHelper.ExecuteReader(DBManager.GetConnectionString(), CommandType.StoredProcedure, spName, storedParams);
		}
		public static SqlDataReader SelectUsers(int loggedRoleID, int sortOrder)
		{
			string spName = "UsersListProc";
			
			SqlParameter[] storedParams = SqlHelperParameterCache.GetSpParameterSet(DBManager.GetConnectionString(), spName);
			storedParams[0].Value = loggedRoleID;
			storedParams[1].Value = sortOrder;

			return SqlHelper.ExecuteReader(DBManager.GetConnectionString(), CommandType.StoredProcedure, spName, storedParams);
		}
		public static SqlDataReader SelectUsersSplit(int loggedRoleID, int sortOrder, string spName)
		{
			
			
			SqlParameter[] storedParams = SqlHelperParameterCache.GetSpParameterSet(DBManager.GetConnectionString(), spName);
			storedParams[0].Value = loggedRoleID;
			storedParams[1].Value = sortOrder;

			return SqlHelper.ExecuteReader(DBManager.GetConnectionString(), CommandType.StoredProcedure, spName, storedParams);
		}
		private static SqlCommand CreateUsersDataViewComandProc(int loggedRoleID, int sortOrder, string spName)
		{
			SqlCommand cmd = new SqlCommand();
			cmd.CommandType = System.Data.CommandType.StoredProcedure;
			cmd.CommandText = spName;
			cmd.Parameters.Add(new System.Data.SqlClient.SqlParameter("@LoggedRoleID", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.Input, true, 10, 0, "LoggedRoleID", DataRowVersion.Current, null));
			cmd.Parameters["@LoggedRoleID"].Value = loggedRoleID;
			cmd.Parameters.Add(new System.Data.SqlClient.SqlParameter("@SortOrder", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.Input, true, 10, 0, "SortOrder", DataRowVersion.Current, null));
			cmd.Parameters["@SortOrder"].Value = sortOrder;
			return cmd;
		}

		public static System.Data.DataView ExecuteUsersDataViewComandProc(int loggedRoleID, int sortOrder, string spName)
		{
			System.Data.SqlClient.SqlDataReader RetObj;
			System.Data.SqlClient.SqlCommand cmd;
			cmd = CreateUsersDataViewComandProc(loggedRoleID, sortOrder, spName);
			SqlConnection cnDAL = new SqlConnection();

			try
			{
				cnDAL =  CommonDAL.OpenConnection();
				cmd.Connection = cnDAL;
				RetObj = cmd.ExecuteReader();
				return new DataView(CommonDAL.CreateTableFromReader(RetObj));
		
			}
			catch(SqlException ex)
			{

				throw new ApplicationException("UsersDataViewComandProc",ex);
			}

			finally
			{
				CommonDAL.CloseConnection(cnDAL);
			}
		}

		public static SqlDataReader SelectUsersIsASI(int loggedRoleID, int sortOrder,bool IsASI)
		{
			string spName = "UsersListIsASIProc";
			
			SqlParameter[] storedParams = SqlHelperParameterCache.GetSpParameterSet(DBManager.GetConnectionString(), spName);
			storedParams[0].Value = loggedRoleID;
			storedParams[1].Value = sortOrder;
			storedParams[2].Value = IsASI;

			return SqlHelper.ExecuteReader(DBManager.GetConnectionString(), CommandType.StoredProcedure, spName, storedParams);
		}

		public static DataSet SelectUsersDS(int loggedRoleID, int sortOrder)
		{
			string spName = "UsersListProc";
			
			SqlParameter[] storedParams = SqlHelperParameterCache.GetSpParameterSet(DBManager.GetConnectionString(), spName);
			storedParams[0].Value = loggedRoleID;
			storedParams[1].Value = sortOrder;

			return SqlHelper.ExecuteDataset(DBManager.GetConnectionString(), CommandType.StoredProcedure,
				spName, storedParams);
		}

		public static SqlDataReader SelectUser(int userID)
		{
			string spName = "UsersSelProc";
			
			SqlParameter[] storedParams = SqlHelperParameterCache.GetSpParameterSet(DBManager.GetConnectionString(), spName);
			storedParams[0].Value = userID;

			return SqlHelper.ExecuteReader(DBManager.GetConnectionString(), CommandType.StoredProcedure, spName, storedParams);
		}

		public static void InactiveUser(int userID)
		{
			string spName = "UsersInactiveProc";
			
			SqlParameter[] storedParams = SqlHelperParameterCache.GetSpParameterSet(DBManager.GetConnectionString(), spName);
			storedParams[0].Value = userID;

			SqlHelper.ExecuteNonQuery(DBManager.GetConnectionString(), CommandType.StoredProcedure,
														spName, storedParams);
		}

		public static void DeleteUser(int userID)
		{
			string spName = "UsersDelProc";
			
			SqlParameter[] storedParams = SqlHelperParameterCache.GetSpParameterSet(DBManager.GetConnectionString(), spName);
			storedParams[0].Value = userID;

			SqlHelper.ExecuteNonQuery(DBManager.GetConnectionString(), CommandType.StoredProcedure, spName, storedParams);
		}

		public static void UpdateUser(int userID, int roleID, string firstName, string secondName, string lastName, 
																	string account, string mail, string password,
			string occupation, string userrole, string ext, string mobile, string home, string cv, decimal salary, 
			decimal bonus, bool istraine, string mobilePersonal, bool isASI, bool HasWorkTime, DateTime WorkDateStart, bool HasWorkTimeOver)
		{
			string spName = "UsersUpdProc";
			
			SqlParameter[] storedParams = SqlHelperParameterCache.GetSpParameterSet(DBManager.GetConnectionString(), spName);
			storedParams[0].Value = userID;
			storedParams[1].Value = (roleID == 0) ? null : (object)roleID;
			storedParams[2].Value = firstName==String.Empty ? null : firstName;
			storedParams[3].Value = secondName == String.Empty ? null : secondName;
			storedParams[4].Value = lastName;
			storedParams[5].Value = account;
			storedParams[6].Value = mail;
			storedParams[7].Value = (password==String.Empty) ? null : password;
			storedParams[8].Value = (occupation==String.Empty) ? null : occupation;
			storedParams[9].Value = (userrole==String.Empty) ? null : userrole;
			storedParams[10].Value = (ext==String.Empty) ? null : ext;
			storedParams[11].Value = (mobile==String.Empty) ? null : mobile;
			storedParams[12].Value = (home==String.Empty) ? null : home;
			storedParams[13].Value = (cv==String.Empty) ? null : cv;
			storedParams[14].Value = salary;
			storedParams[15].Value = bonus;
			storedParams[16].Value = istraine;
			storedParams[17].Value = mobilePersonal;
			storedParams[18].Value = isASI;
			storedParams[19].Value = HasWorkTime;
			if(WorkDateStart==Constants.DateMax)
				storedParams[20].Value  =null;
			else
				storedParams[20].Value  = WorkDateStart;
			storedParams[21].Value = HasWorkTimeOver;

			SqlHelper.ExecuteNonQuery(DBManager.GetConnectionString(), CommandType.StoredProcedure, spName, storedParams);
		}

		public static int InsertUser(int roleID, string firstName, string secondName, string lastName, 
			string account, string mail, bool isActive, string password,
			string occupation, string userrole, string ext, string mobile, string home, string cv, decimal salary, 
			decimal bonus, bool istraine, string mobilePersonal, bool isASI, bool HasWorkTime, DateTime WorkDayStart, bool HasWorkTimeOver )
		{
			string spName = "UsersInsProc";
			
			SqlParameter[] storedParams = SqlHelperParameterCache.GetSpParameterSet(DBManager.GetConnectionString(), spName);
			storedParams[0].Value = (roleID == 0) ? null : (object)roleID;
			storedParams[1].Value = firstName==String.Empty ? null : firstName;
			storedParams[2].Value = secondName == String.Empty ? null : secondName;
			storedParams[3].Value = lastName;
			storedParams[4].Value = account;
			storedParams[5].Value = mail;
			storedParams[6].Value = isActive;
			storedParams[7].Value = (password==String.Empty) ? null : password;
			storedParams[8].Value = (occupation==String.Empty) ? null : occupation;
			storedParams[9].Value = (userrole==String.Empty) ? null : userrole;
			storedParams[10].Value = (ext==String.Empty) ? null : ext;
			storedParams[11].Value = (mobile==String.Empty) ? null : mobile;
			storedParams[12].Value = (home==String.Empty) ? null : home;
			storedParams[13].Value = (cv==String.Empty) ? null : cv;
			storedParams[14].Value = salary;
			storedParams[15].Value = bonus;
			storedParams[16].Value = istraine;
			storedParams[17].Value = mobilePersonal;
			storedParams[18].Value = isASI;
			storedParams[19].Value = HasWorkTime;
			if(WorkDayStart==Constants.DateMax)
				storedParams[20].Value  =null;
			else
				storedParams[20].Value  = WorkDayStart;
			storedParams[21].Value = HasWorkTimeOver;

			SqlHelper.ExecuteNonQuery(DBManager.GetConnectionString(), CommandType.StoredProcedure, spName, storedParams);

			return (int)storedParams[22].Value;
		}

		public static SqlDataReader SelectUsersEmails()
		{
			string spName = "UsersEmailsListProc";
			return SqlHelper.ExecuteReader(DBManager.GetConnectionString(), CommandType.StoredProcedure, spName);
		}

		public static SqlDataReader SelectLeadersEmails()
		{
			string spName = "UsersLeadersMailsListProc";

			return SqlHelper.ExecuteReader(DBManager.GetConnectionString(), CommandType.StoredProcedure, spName);
		}

		public static SqlDataReader SelectLeadersOfficersEmails()
		{
			string spName = "UsersLeadersOfficersMailsListProc1";

			return SqlHelper.ExecuteReader(DBManager.GetConnectionString(), CommandType.StoredProcedure, spName);
		}
		public static SqlDataReader SelectLeadersOfficersEmailsOnly()
		{
			string spName = "UsersLeadersOfficersMailsListProc";

			return SqlHelper.ExecuteReader(DBManager.GetConnectionString(), CommandType.StoredProcedure, spName);
		}
		public static SqlDataReader SelectSecretariesOfficersEmails()
		{
			string spName = "UsersSecretariesOfficersMailsListProc";

			return SqlHelper.ExecuteReader(DBManager.GetConnectionString(), CommandType.StoredProcedure, spName);
		}
		public static SqlDataReader SelectUsersMainAccMailListProc()
		{
			string spName = "UsersMainAccMailListProc";

			return SqlHelper.ExecuteReader(DBManager.GetConnectionString(), CommandType.StoredProcedure, spName);
		}
		public static UserInfo UsersListByRoleProc(int RoleID)
		{
			string spName = "UsersListByRoleProc";

			SqlParameter[] storedParams = SqlHelperParameterCache.GetSpParameterSet(
				DBManager.GetConnectionString(), spName);
			storedParams[0].Value = RoleID;
			
			UserInfo ui = new UserInfo();

			SqlDataReader reader = null;
			try
			{
				reader = SqlHelper.ExecuteReader(DBManager.GetConnectionString(), CommandType.StoredProcedure, spName, storedParams);
				return GetUserInfo(reader);
			}
			finally
			{
				if (reader!=null && !reader.IsClosed)
					reader.Close();
			}
		}
	}
}
