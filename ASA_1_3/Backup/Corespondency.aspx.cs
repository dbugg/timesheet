using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Data.SqlClient;
using Asa.Timesheet.Data;
using Asa.Timesheet.Data.Entities;
using Asa.Timesheet.Data.Util;
using log4net;

namespace Asa.Timesheet.WebPages
{
	/// <summary>
	/// Summary description for Corespondency.
	/// </summary>
	public class Corespondency : TimesheetPageBase
	{



		#region Web controls
		private static readonly ILog log = LogManager.GetLogger(typeof(Protokols));
		protected System.Web.UI.HtmlControls.HtmlImage lkCalendar1;
		protected System.Web.UI.HtmlControls.HtmlImage lkCalendar2;
		protected System.Web.UI.WebControls.Button btnNew;
		protected System.Web.UI.WebControls.DropDownList ddlProject;
		protected System.Web.UI.WebControls.TextBox txtStartDate;
		protected System.Web.UI.WebControls.DropDownList ddlBuildingTypes;
		protected System.Web.UI.WebControls.TextBox txtEndDate;
		protected System.Web.UI.WebControls.DropDownList ddlSendReseve;
		protected System.Web.UI.WebControls.Label lblInfo;
		protected System.Web.UI.WebControls.Label lblError;
		protected System.Web.UI.WebControls.PlaceHolder menuHolder;
		protected System.Web.UI.WebControls.Label Label4;
		protected System.Web.UI.WebControls.Label lblProject;
		protected System.Web.UI.WebControls.Button btnSearch;
		protected System.Web.UI.WebControls.Panel grid;
		protected System.Web.UI.WebControls.Button btnExport;
		protected Asa.Timesheet.WebPages.UserControls.PageHeader header;
		protected System.Web.UI.WebControls.Label lbSendReseve;
		protected System.Web.UI.WebControls.Button btnBackToProject;
		protected System.Web.UI.WebControls.DataGrid grdMain;
protected System.Web.UI.WebControls.DropDownList ddlClients;
		
		protected System.Web.UI.WebControls.Label lbClient;
#endregion
		
		//private MailingTypesVector _mtv = new MailingTypesVector();
		protected System.Web.UI.WebControls.DropDownList ddlProjectsStatus;
		protected System.Web.UI.WebControls.Label Label1;
		protected System.Web.UI.WebControls.Label Label2;
		private const string _noProject = "-1";
	
		private enum gridColumns
		{
			MailingNumber=0,
			SendBG,
			Send,
			MailingDate,
			ProjectName,
			ClientName,
			MailingTypeName,
			Notes,
			HasScan,
			btnScan,
			DeleteItem,
			ext
		}

	
		#region PageLoad
        private void Page_Load(object sender, System.EventArgs e)
        {
            // Put user code to initialize the page here
            lkCalendar1.Attributes["onclick"] = TimeHelper.InvokePopupCal(txtStartDate);
            lkCalendar2.Attributes["onclick"] = TimeHelper.InvokePopupCal(txtEndDate);
            UIHelpers.CreateMenu(menuHolder, LoggedUser);

            if (!this.IsPostBack)
            {

                header.PageTitle = Resource.ResourceManager["corespondency_PageTitle"];
                header.UserName = this.LoggedUser.UserName;
                if (UIHelpers.GetPIDParam() != -1)
                {
                    UIHelpers.LoadProjectStatusWithoutConcluded(ddlProjectsStatus, (int)ProjectsData.ProjectsByStatus.AllProjects);
                }
                else
                {
                    UIHelpers.LoadProjectStatusWithoutConcluded(ddlProjectsStatus, (int)ProjectsData.ProjectsByStatus.Active);
                }
                UIHelpers.LoadBuildingTypes("", ddlBuildingTypes);
                //UIHelpers.LoadBuildingTypes(ddlBuildingTypes, "");
                LoadProjects();
                LoadSendReseve();
                LoadClients();

                if (UIHelpers.GetPIDParam() != -1)
                {
                    lblProject.Visible = ddlBuildingTypes.Visible =
                    Label1.Visible = ddlProjectsStatus.Visible = false;
                    ListItem li = ddlProject.Items.FindByValue(UIHelpers.GetPIDParam().ToString());
                    if (li != null)
                    {
                        ddlProject.SelectedValue = UIHelpers.GetPIDParam().ToString();
                    }
                    else
                        ErrorRedirect(Resource.ResourceManager["ErrorLoadProject"]);
                    ddlProject.Enabled = false;
                    btnBackToProject.Visible = true;
                    lbClient.Visible = ddlClients.Visible = false;
                }
                else
                {
                    btnBackToProject.Visible = false;
                }
                //this.ViewState["Sort"] = grdMain.Columns[(int)gridColumns.MailingNumber].SortExpression;
                //_mtv = MailingTypeDAL.LoadCollection();

                BindGrid();
            }
            else

                UIHelpers.LoadBuildingTypes(ddlBuildingTypes.SelectedValue, ddlBuildingTypes, (ProjectsData.ProjectsByStatus)int.Parse(ddlProjectsStatus.SelectedValue));

            if (!(LoggedUser.HasPaymentRights || LoggedUser.IsSecretary || LoggedUser.IsLayer))
            {
                ErrorRedirect(Resource.ResourceManager["noRightsAlert"]);
            }
        }

        private bool LoadProjects()
        {
            bool returnBool = UIHelpers.LoadProjects(ddlProject, ddlProjectsStatus, ddlBuildingTypes);

            //			SqlDataReader reader = null;
            //			
            //			try
            //			{
            //				reader = ProjectsData.SelectProjectNamesClear(this.ProjectsStatus, int.Parse(ddlBuildingTypes.SelectedValue));
            //			
            //				ddlProject.DataSource = reader;
            //				ddlProject.DataValueField = "ProjectID";
            //				ddlProject.DataTextField = "ProjectName";
            //				ddlProject.DataBind();

            if (ddlProject.Items.Count > 0)
                ddlProject.Items.Insert(1, new ListItem(Resource.ResourceManager["reports_ddlNoProjects"], _noProject));
            else
                ddlProject.Items.Insert(0, new ListItem(Resource.ResourceManager["reports_ddlNoProjects"], _noProject));
            //				ddlProject.Items.Insert(0, new ListItem("<"+Resource.ResourceManager["reports_ddlAllProjects"]+">", "-1"));
            //				ddlProject.SelectedValue = "-1";
            //			}
            //			catch (Exception ex)
            //			{
            //				log.Error(ex);
            //				return false;
            //			}
            //
            //			finally
            //			{
            //				if (reader!=null) reader.Close();
            //			}
            //			return true;
            return returnBool;
        }

        private void LoadSendReseve()
        {
            ListItem li = new ListItem(Resource.ResourceManager["Corespondency_All"]);
            ddlSendReseve.Items.Add(li);
            li = new ListItem(Resource.ResourceManager["Corespondency_Send1"]);
            ddlSendReseve.Items.Add(li);
            li = new ListItem(Resource.ResourceManager["Corespondency_Reseve1"]);
            ddlSendReseve.Items.Add(li);
            ddlSendReseve.SelectedIndex = 0;
        }
        private bool LoadClients()
        {
            SqlDataReader reader = null;
            try
            {
                //reader = ClientsData.SelectClients(0,(int)ClientTypes.Client,false);
                reader = ClientsData.SelectClientsFromProjects(0, (int)ClientTypes.Client, UIHelpers.ToInt(ddlProject.SelectedValue), UIHelpers.ToInt(ddlBuildingTypes.SelectedValue), UIHelpers.ToInt(ddlProjectsStatus.SelectedValue));
                ddlClients.DataSource = reader;
                ddlClients.DataValueField = "ClientID";
                ddlClients.DataTextField = "ClientName";
                ddlClients.DataBind();

                ddlClients.Items.Insert(0, new ListItem(Resource.ResourceManager["reports_ddlAllClients"], "0"));
                ddlClients.SelectedIndex = 0;
            }
            catch (Exception ex)
            {
                log.Info(ex);
                return false;
            }
            finally
            {
                if (reader != null) reader.Close();
            }
            return true;
        }
		#endregion

		#region Web Form Designer generated code
        override protected void OnInit(EventArgs e)
        {
            //
            // CODEGEN: This call is required by the ASP.NET Web Form Designer.
            //
            InitializeComponent();
            base.OnInit(e);
        }
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
        private void InitializeComponent()
        {
            this.ddlProjectsStatus.SelectedIndexChanged += new System.EventHandler(this.ddlProjectsStatus_SelectedIndexChanged);
            this.ddlBuildingTypes.SelectedIndexChanged += new System.EventHandler(this.Dropdownlist1_SelectedIndexChanged);
            this.ddlProject.SelectedIndexChanged += new System.EventHandler(this.ddlProject_SelectedIndexChanged);
            this.btnSearch.Click += new System.EventHandler(this.btnSearch_Click);
            this.grdMain.ItemCommand += new System.Web.UI.WebControls.DataGridCommandEventHandler(this.grdMain_ItemCommand);
            this.grdMain.SortCommand += new System.Web.UI.WebControls.DataGridSortCommandEventHandler(this.grdMain_SortCommand);
            this.grdMain.ItemDataBound += new System.Web.UI.WebControls.DataGridItemEventHandler(this.grdMain_ItemDataBound);
            this.btnNew.Click += new System.EventHandler(this.btnNew_Click);
            this.btnBackToProject.Click += new System.EventHandler(this.btnBackToProject_Click);
            this.Load += new System.EventHandler(this.Page_Load);

        }
		#endregion

		
		public ProjectsData.ProjectsByStatus ProjectsStatus
		{
			get
			{
				try
				{
					return (ProjectsData.ProjectsByStatus)int.Parse(ddlProjectsStatus.SelectedValue);
				}
				catch { return ProjectsData.ProjectsByStatus.AllProjects; }
			}
		}


		#region HTML
        protected bool GetVisible(object o)
        {
            if (o == DBNull.Value)
                return false;
            else return (bool)o;
        }
        protected string GetSendReseve(object o)
        {
            if (o == DBNull.Value)
                return string.Empty;
            else
                if ((bool)o)
                    return Resource.ResourceManager["Corespondency_Send"];
                else
                    return Resource.ResourceManager["Corespondency_Reseve"];
        }
        protected string GetURL(int ID)
        {
            //			int mdID=-1;
            //			ProjectProtocolsVector mdv = ProjectProtokolDAL.LoadCollection("ProjectProtokolsSelByProtocolProc",SQLParms.CreateProjectProtokolsSelByProtocolProc(ID));
            //			foreach(ProjectProtokolData mdd in mdv)
            //			{
            //				mdID = mdd.ProjectProtokolID;
            //				break;
            //			}
            return string.Concat(System.Configuration.ConfigurationManager.AppSettings["ScanPathMailing"], ID, System.Configuration.ConfigurationManager.AppSettings["Extension"]);
        }	
		#endregion

		#region BindGrid

        private void BindGrid()
        {
            MailingsVector mv = MailingDAL.LoadCollection("MailingsListProc1", SQLParms.CreateMailingsListProc1(TimeHelper.GetDate(txtStartDate.Text),
                TimeHelper.GetDate(txtEndDate.Text), int.Parse(ddlProject.SelectedValue), (string)this.ViewState["Sort"], ddlSendReseve.SelectedIndex == 0, ddlSendReseve.SelectedIndex == 1,
                ddlProjectsStatus.SelectedValue == "0", ddlProjectsStatus.SelectedValue == "1", ddlProject.SelectedValue == _noProject, UIHelpers.ToInt(ddlBuildingTypes.SelectedValue), UIHelpers.ToInt(ddlClients.SelectedValue)));
            grdMain.DataSource = mv;
            grdMain.DataKeyField = "MailingID";
            grdMain.DataBind();
            if (!LoggedUser.IsLeader)
                grdMain.Columns[(int)gridColumns.DeleteItem].Visible = false;
            try
            {
                SetConfirmDelete(grdMain, "btnDelete", 1);
            }
            catch
            {

            }
        }
		#endregion

		#region Event Handlers
        private void btnNew_Click(object sender, System.EventArgs e)
        {
            if (UIHelpers.GetPIDParam() != -1)
                Response.Redirect("EditCorespondency.aspx" + "?pid=" + UIHelpers.GetPIDParam());
            else
                Response.Redirect("EditCorespondency.aspx");

        }

        private void btnSearch_Click(object sender, System.EventArgs e)
        {
            BindGrid();
        }
        private void ddlProjectsStatus_SelectedIndexChanged(object sender, System.EventArgs e)
        {
            LoadProjects();
            LoadClients();
        }

        private void Dropdownlist1_SelectedIndexChanged(object sender, System.EventArgs e)
        {
            LoadProjects();
            LoadClients();
        }
        private void grdMain_ItemCommand(object source, System.Web.UI.WebControls.DataGridCommandEventArgs e)
        {
            if (e.CommandName == "Edit")
            {
                string SID = ((DataGrid)source).DataKeys[e.Item.ItemIndex].ToString();
                if (UIHelpers.GetPIDParam() != -1)
                    Response.Redirect("EditCorespondency.aspx" + "?id=" + SID + "&pid=" + UIHelpers.GetPIDParam());
                else
                    Response.Redirect("EditCorespondency.aspx" + "?id=" + SID);
                return;
            }
            if (!(e.CommandSource is ImageButton))
                return;
            ImageButton ib = (ImageButton)e.CommandSource;
            string s = ib.ID;
            switch (ib.ID)
            {
                case "btnDelete": int ID = (int)((DataGrid)source).DataKeys[e.Item.ItemIndex];
                    MailingDAL.Delete(ID);
                    log.Info(string.Format("Mailing {0} has been DELETED by {1}", ID, string.Concat(LoggedUser.FullName, " ( userID ", LoggedUser.UserID, " )")));
                    BindGrid();
                    break;
                case "btnFile":
                    int id = (int)((DataGrid)source).DataKeys[e.Item.ItemIndex];

                    string filePath = /*Server.MapPath(*/string.Concat(System.Configuration.ConfigurationManager.AppSettings["ScanPathMailing"], id, e.Item.Cells[(int)gridColumns.ext].Text);//);

                    Response.Cache.SetExpires(DateTime.Now.AddSeconds(-1));
                    Response.Cache.SetCacheability(HttpCacheability.Private);
                    Response.AppendHeader("content-disposition", "attachment; filename=" + id.ToString() + e.Item.Cells[(int)gridColumns.ext].Text);
                    //Response.AppendHeader("content-disposition", "attachment; filename="+ID+e.Item.Cells[(int)gridColomns.Type].Text);

                    Response.Charset = "";
                    Response.TransmitFile(filePath);
                    Response.End();

                    break;

            }
        }
		#endregion

        private void grdMain_SortCommand(object source, System.Web.UI.WebControls.DataGridSortCommandEventArgs e)
        {
            ViewState.Add("Sort", Sorting.SetSort(grdMain, (string)ViewState["Sort"], e.SortExpression));

            BindGrid();
        }

        private void btnBackToProject_Click(object sender, System.EventArgs e)
        {
            Response.Redirect("EditProject.aspx" + "?pid=" + UIHelpers.GetPIDParam());
        }

        private void grdMain_ItemDataBound(object sender, System.Web.UI.WebControls.DataGridItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Item
                || e.Item.ItemType == ListItemType.AlternatingItem)
            {

                e.Item.Cells[(int)gridColumns.MailingTypeName].Text = GetMailingTypesName(e.Item.Cells[(int)gridColumns.MailingTypeName].Text);

            }
        }
        private string GetMailingTypesName(string mailingTypesIDs)
        {
            string returnValue = "";
            string[] ArrayNumbers = mailingTypesIDs.Split(Constants._separator);
            foreach (string sMailingTypeID in ArrayNumbers)
            {
                int iMailingTypeID = UIHelpers.ToInt(sMailingTypeID);
                if (iMailingTypeID == -1)
                    continue;
                MailingTypesVector _mtv = MailingTypes;
                for (int i = 0; i < _mtv.Count; i++)
                {
                    if (_mtv[i].MailingTypeID == iMailingTypeID)
                    {
                        returnValue = string.Concat(returnValue, " ", _mtv[i].MailingTypeName, ",");
                        break;
                    }
                }
            }
            returnValue = returnValue.TrimStart(' ');
            returnValue = returnValue.TrimEnd(',');
            return returnValue;
        }

        private void ddlProject_SelectedIndexChanged(object sender, System.EventArgs e)
        {
            LoadClients();
        }

		private MailingTypesVector MailingTypes
		{
			get
			{
				if(this.Session["mailingtypes"]!=null)
					return (MailingTypesVector) this.Session["mailingtypes"];
				else
				{
					this.Session["mailingtypes"] = MailingTypeDAL.LoadCollection();
					return (MailingTypesVector) this.Session["mailingtypes"];
				}
			}
		}
	}
}
