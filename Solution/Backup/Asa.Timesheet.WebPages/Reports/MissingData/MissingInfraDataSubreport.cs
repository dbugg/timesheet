using System;
using System.Drawing;
using Asa.Timesheet.Data;
using Asa.Timesheet.Data.Reports;
using GrapeCity.ActiveReports;
using GrapeCity.ActiveReports.Controls;
using GrapeCity.ActiveReports.SectionReportModel;
using GrapeCity.ActiveReports.Document.Section;
using GrapeCity.ActiveReports.Document;

namespace Asa.Timesheet.WebPages.Reports
{
    public class MissingInfraDataSubreport : GrapeCity.ActiveReports.SectionReport
	{
		//string _projectName;
    bool _requiredOnly;
		//int _startPageNumber;
    
    PointF lokT = new PointF(CmToInch(0.2f), CmToInch(0.8f));
    PointF lokD = new PointF(CmToInch(5.5f), CmToInch(0.8f));
    int _pos = 0;
    ProjectsReportDS.InfrastructureDataItemsRow cr = null;


    public MissingInfraDataSubreport(bool requiredOnly)
    {
        InitializeComponent();

        _requiredOnly = requiredOnly;
    }

    private void Detail_Format(object sender, System.EventArgs eArgs)
    {
        label1.Visible = label2.Visible = label3.Visible = label4.Visible = label5.Visible = false;
        txtInfo1.Visible = txtInfo2.Visible = txtInfo3.Visible = txtInfo4.Visible = txtInfo5.Visible = false;

        int pos = 1;

        #region Izhodni danni

        System.Text.StringBuilder missing = new System.Text.StringBuilder();

        if (cr.IsIzhDanniDate1Null()) missing.Append(cr.IzhDanniDate1Label.Trim(':') + ", ");
        if (cr.IsIzhDanniDate2Null()) missing.Append(cr.IzhDanniDate2Label.Trim(':'));

        string m = missing.ToString();
        m = m.Trim().Trim(',');

        if (m.Length > 0)
        {
            TextBox lbl = (TextBox)this.Detail.Controls["label" + pos.ToString()];
            TextBox info = (TextBox)this.Detail.Controls["txtInfo" + pos.ToString()];
            lbl.Visible = true; info.Visible = true;

            lbl.Text = cr.IzhDanniLabel;
            info.Text = m;

            pos++;
        }

        #endregion

        #region Pismo i skica za prisuedinjavane

        if (cr.PismoShow)
        {
            missing = new System.Text.StringBuilder();

            if (cr.IsPismoDate1Null()) missing.Append(cr.PismoDate1Label.Trim(':'));

            m = missing.ToString().Trim().Trim(',');

            if (m.Length > 0)
            {
                TextBox lbl = (TextBox)this.Detail.Controls["label" + pos.ToString()];
                TextBox info = (TextBox)this.Detail.Controls["txtInfo" + pos.ToString()];
                lbl.Visible = true; info.Visible = true;

                lbl.Text = cr.PismoLabel;
                info.Text = m;

                pos++;
            }
        }

        #endregion

        #region Prosuedinitelen dogovor

        if (cr.PrisDogovorShow)
        {
            missing = new System.Text.StringBuilder();

            if (cr.IsPrisDogovorDate1Null()) missing.Append(cr.PrisDogovorDate1Label.Trim(':'));

            m = missing.ToString().Trim().Trim(',');

            if (m.Length > 0)
            {
                TextBox lbl = (TextBox)this.Detail.Controls["label" + pos.ToString()];
                TextBox info = (TextBox)this.Detail.Controls["txtInfo" + pos.ToString()];
                lbl.Visible = true; info.Visible = true;

                lbl.Text = cr.PrisDogovorLabel;
                info.Text = m;

                pos++;
            }
        }

        #endregion

        #region Project vun6ni vruzki

        if (cr.ProjectShow)
        {
            missing = new System.Text.StringBuilder();

            if (cr.IsProjectDate1Null()) missing.Append(cr.ProjectDate1Label.Trim(':') + ", ");
            if (cr.IsProjectDate2Null()) missing.Append(cr.ProjectDate2Label.Trim(':'));

            m = missing.ToString().Trim().Trim(',');

            if (m.Length > 0)
            {
                TextBox lbl = (TextBox)this.Detail.Controls["label" + pos.ToString()];
                TextBox info = (TextBox)this.Detail.Controls["txtInfo" + pos.ToString()];
                lbl.Visible = true; info.Visible = true;

                lbl.Text = cr.ProjectLabel;
                info.Text = m;

                pos++;
            }
        }

        #endregion

        missing = new System.Text.StringBuilder();

        if (!_requiredOnly)
        {
            if (cr.IsNumberNull()) missing.Append(Resource.ResourceManager["Reports_IzhNomer"] + ", ");
            if (cr.IsNumberNull()) missing.Append(Resource.ResourceManager["Reports_Suma"] + ", ");

            m = missing.ToString().Trim().Trim(',');

            if (m.Length > 0)
            {
                TextBox lbl = (TextBox)this.Detail.Controls["label" + pos.ToString()];
                TextBox info = (TextBox)this.Detail.Controls["txtInfo" + pos.ToString()];
                //lbl.Visible = true; 
                info.Visible = true;

                //   lbl.Text = cr.ProjectLabel; 
                info.Text = m;

                pos++;
            }
        }

        this.Detail.Height = ((TextBox)this.Detail.Controls["label" + (pos - 1).ToString()]).Location.Y + CmToInch(1f);
    }


    private void MissingInfraDataSubreport_InfrastrukturniDanniSubreport_FetchData(object sender, FetchEventArgs eArgs)
    {
        if (!eArgs.EOF)
        {
            ProjectsReportDS.InfrastructureDataItemsDataTable tbl =
              (ProjectsReportDS.InfrastructureDataItemsDataTable)this.DataSource;

            cr = (ProjectsReportDS.InfrastructureDataItemsRow)tbl.Rows[_pos];

            _pos++;
        }
    }

		#region ActiveReports Designer generated code

















    public void InitializeComponent()
    {
        System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MissingInfraDataSubreport));
        this.Detail = new GrapeCity.ActiveReports.SectionReportModel.Detail();
        this.ReportHeader = new GrapeCity.ActiveReports.SectionReportModel.ReportHeader();
        this.ReportFooter = new GrapeCity.ActiveReports.SectionReportModel.ReportFooter();
        this.txtReportTitle = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
        this.Line3 = new GrapeCity.ActiveReports.SectionReportModel.Line();
        this.txtTitle = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
        this.label1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
        this.txtInfo1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
        this.label2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
        this.txtInfo2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
        this.label3 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
        this.txtInfo3 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
        this.label4 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
        this.txtInfo4 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
        this.Line1 = new GrapeCity.ActiveReports.SectionReportModel.Line();
        this.label5 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
        this.txtInfo5 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
        ((System.ComponentModel.ISupportInitialize)(this.txtReportTitle)).BeginInit();
        ((System.ComponentModel.ISupportInitialize)(this.txtTitle)).BeginInit();
        ((System.ComponentModel.ISupportInitialize)(this.label1)).BeginInit();
        ((System.ComponentModel.ISupportInitialize)(this.txtInfo1)).BeginInit();
        ((System.ComponentModel.ISupportInitialize)(this.label2)).BeginInit();
        ((System.ComponentModel.ISupportInitialize)(this.txtInfo2)).BeginInit();
        ((System.ComponentModel.ISupportInitialize)(this.label3)).BeginInit();
        ((System.ComponentModel.ISupportInitialize)(this.txtInfo3)).BeginInit();
        ((System.ComponentModel.ISupportInitialize)(this.label4)).BeginInit();
        ((System.ComponentModel.ISupportInitialize)(this.txtInfo4)).BeginInit();
        ((System.ComponentModel.ISupportInitialize)(this.label5)).BeginInit();
        ((System.ComponentModel.ISupportInitialize)(this.txtInfo5)).BeginInit();
        ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
        // 
        // Detail
        // 
        this.Detail.ColumnSpacing = 0F;
        this.Detail.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
						this.txtTitle,
						this.label1,
						this.txtInfo1,
						this.label2,
						this.txtInfo2,
						this.label3,
						this.txtInfo3,
						this.label4,
						this.txtInfo4,
						this.Line1,
						this.label5,
						this.txtInfo5});
        this.Detail.Height = 1.978472F;
        this.Detail.KeepTogether = true;
        this.Detail.Name = "Detail";
        // 
        // ReportHeader
        // 
        this.ReportHeader.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
						this.txtReportTitle,
						this.Line3});
        this.ReportHeader.Height = 0.3131945F;
        this.ReportHeader.Name = "ReportHeader";
        // 
        // ReportFooter
        // 
        this.ReportFooter.Height = 0F;
        this.ReportFooter.Name = "ReportFooter";
        // 
        // txtReportTitle
        // 
        this.txtReportTitle.Height = 0.2362205F;
        this.txtReportTitle.Left = 0F;
        this.txtReportTitle.Name = "txtReportTitle";
        this.txtReportTitle.Style = "font-size: 12pt; font-weight: bold";
        this.txtReportTitle.Text = "??????????????? ????? ?? ???????????";
        this.txtReportTitle.Top = 0F;
        this.txtReportTitle.Width = 6.692914F;
        // 
        // Line3
        // 
        this.Line3.Height = 0F;
        this.Line3.Left = 0F;
        this.Line3.LineWeight = 2F;
        this.Line3.Name = "Line3";
        this.Line3.Top = 0.2362205F;
        this.Line3.Width = 7.086611F;
        this.Line3.X1 = 0F;
        this.Line3.X2 = 7.086611F;
        this.Line3.Y1 = 0.2362205F;
        this.Line3.Y2 = 0.2362205F;
        // 
        // txtTitle
        // 
        this.txtTitle.DataField = "Title";
        this.txtTitle.Height = 0.2165354F;
        this.txtTitle.Left = 0F;
        this.txtTitle.Name = "txtTitle";
        this.txtTitle.Style = "font-size: 11pt; font-weight: bold";
        this.txtTitle.Text = "txtTitle";
        this.txtTitle.Top = 0F;
        this.txtTitle.Width = 6.299212F;
        // 
        // label1
        // 
        this.label1.Height = 0.2F;
        this.label1.Left = 0F;
        this.label1.Name = "label1";
        this.label1.Style = "font-weight: bold";
        this.label1.Text = "??????? ?????:";
        this.label1.Top = 0.3149606F;
        this.label1.Visible = false;
        this.label1.Width = 2.362205F;
        // 
        // txtInfo1
        // 
        this.txtInfo1.Height = 0.2F;
        this.txtInfo1.Left = 2.362205F;
        this.txtInfo1.Name = "txtInfo1";
        this.txtInfo1.Text = "??????:";
        this.txtInfo1.Top = 0.3149606F;
        this.txtInfo1.Visible = false;
        this.txtInfo1.Width = 3.937008F;
        // 
        // label2
        // 
        this.label2.DataField = "PismoLabel";
        this.label2.Height = 0.2F;
        this.label2.Left = 0F;
        this.label2.Name = "label2";
        this.label2.Style = "font-weight: bold";
        this.label2.Text = "????? ? ????? ?? ???????.";
        this.label2.Top = 0.5905511F;
        this.label2.Visible = false;
        this.label2.Width = 2.362205F;
        // 
        // txtInfo2
        // 
        this.txtInfo2.DataField = "PismoDate1Label";
        this.txtInfo2.Height = 0.2F;
        this.txtInfo2.Left = 2.362205F;
        this.txtInfo2.Name = "txtInfo2";
        this.txtInfo2.Text = "TextBox7";
        this.txtInfo2.Top = 0.5905511F;
        this.txtInfo2.Visible = false;
        this.txtInfo2.Width = 3.937008F;
        // 
        // label3
        // 
        this.label3.DataField = "PrisDogovorLabel";
        this.label3.Height = 0.2F;
        this.label3.Left = 0F;
        this.label3.Name = "label3";
        this.label3.Style = "font-weight: bold";
        this.label3.Text = "TextBox9";
        this.label3.Top = 0.8661417F;
        this.label3.Visible = false;
        this.label3.Width = 2.362205F;
        // 
        // txtInfo3
        // 
        this.txtInfo3.DataField = "PrisDogovorDate1Label";
        this.txtInfo3.Height = 0.2F;
        this.txtInfo3.Left = 2.362205F;
        this.txtInfo3.Name = "txtInfo3";
        this.txtInfo3.Text = "TextBox10";
        this.txtInfo3.Top = 0.8661417F;
        this.txtInfo3.Visible = false;
        this.txtInfo3.Width = 3.937008F;
        // 
        // label4
        // 
        this.label4.DataField = "ProjectLabel";
        this.label4.Height = 0.2F;
        this.label4.Left = 0F;
        this.label4.Name = "label4";
        this.label4.Style = "font-weight: bold";
        this.label4.Text = "TextBox12";
        this.label4.Top = 1.141733F;
        this.label4.Visible = false;
        this.label4.Width = 2.362205F;
        // 
        // txtInfo4
        // 
        this.txtInfo4.DataField = "ProjectDate1Label";
        this.txtInfo4.Height = 0.2F;
        this.txtInfo4.Left = 2.362205F;
        this.txtInfo4.Name = "txtInfo4";
        this.txtInfo4.Text = "TextBox13";
        this.txtInfo4.Top = 1.141733F;
        this.txtInfo4.Visible = false;
        this.txtInfo4.Width = 3.937008F;
        // 
        // Line1
        // 
        this.Line1.Height = 0F;
        this.Line1.Left = 0F;
        this.Line1.LineWeight = 1F;
        this.Line1.Name = "Line1";
        this.Line1.Top = 0.2362205F;
        this.Line1.Width = 7.086611F;
        this.Line1.X1 = 0F;
        this.Line1.X2 = 7.086611F;
        this.Line1.Y1 = 0.2362205F;
        this.Line1.Y2 = 0.2362205F;
        // 
        // label5
        // 
        this.label5.DataField = "ProjectLabel";
        this.label5.Height = 0.2F;
        this.label5.Left = 0F;
        this.label5.Name = "label5";
        this.label5.Style = "font-weight: bold";
        this.label5.Text = "TextBox12";
        this.label5.Top = 1.417323F;
        this.label5.Visible = false;
        this.label5.Width = 2.362205F;
        // 
        // txtInfo5
        // 
        this.txtInfo5.DataField = "ProjectDate1Label";
        this.txtInfo5.Height = 0.2F;
        this.txtInfo5.Left = 2.362205F;
        this.txtInfo5.Name = "txtInfo5";
        this.txtInfo5.Text = "TextBox13";
        this.txtInfo5.Top = 1.417323F;
        this.txtInfo5.Visible = false;
        this.txtInfo5.Width = 3.937008F;
        // 
        // ActiveReport1
        // 
        this.MasterReport = false;
        this.PageSettings.DefaultPaperSize = false;
        this.PageSettings.Margins.Bottom = 0.9840278F;
        this.PageSettings.Margins.Left = 0.9840278F;
        this.PageSettings.Margins.Right = 0.9840278F;
        this.PageSettings.Margins.Top = 0.9840278F;
        this.PageSettings.PaperHeight = 11.69306F;
        this.PageSettings.PaperKind = System.Drawing.Printing.PaperKind.A4;
        this.PageSettings.PaperName = "Custom paper";
        this.PageSettings.PaperWidth = 8.268056F;
        this.PrintWidth = 6.692914F;
        this.Sections.Add(this.ReportHeader);
        this.Sections.Add(this.Detail);
        this.Sections.Add(this.ReportFooter);
        this.StyleSheet.Add(new DDCssLib.StyleSheetRule(resources.GetString("$this.StyleSheet"), "Normal"));
        this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: inherit; font-style: inherit; font-variant: inherit; font-weight: bo" +
                    "ld; font-size: 16pt; font-size-adjust: inherit; font-stretch: inherit", "Heading1", "Normal"));
        this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: Times New Roman; font-style: italic; font-variant: inherit; font-wei" +
                    "ght: bold; font-size: 14pt; font-size-adjust: inherit; font-stretch: inherit", "Heading2", "Normal"));
        this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: inherit; font-style: inherit; font-variant: inherit; font-weight: bo" +
                    "ld; font-size: 13pt; font-size-adjust: inherit; font-stretch: inherit", "Heading3", "Normal"));
        ((System.ComponentModel.ISupportInitialize)(this.txtReportTitle)).EndInit();
        ((System.ComponentModel.ISupportInitialize)(this.txtTitle)).EndInit();
        ((System.ComponentModel.ISupportInitialize)(this.label1)).EndInit();
        ((System.ComponentModel.ISupportInitialize)(this.txtInfo1)).EndInit();
        ((System.ComponentModel.ISupportInitialize)(this.label2)).EndInit();
        ((System.ComponentModel.ISupportInitialize)(this.txtInfo2)).EndInit();
        ((System.ComponentModel.ISupportInitialize)(this.label3)).EndInit();
        ((System.ComponentModel.ISupportInitialize)(this.txtInfo3)).EndInit();
        ((System.ComponentModel.ISupportInitialize)(this.label4)).EndInit();
        ((System.ComponentModel.ISupportInitialize)(this.txtInfo4)).EndInit();
        ((System.ComponentModel.ISupportInitialize)(this.label5)).EndInit();
        ((System.ComponentModel.ISupportInitialize)(this.txtInfo5)).EndInit();
        ((System.ComponentModel.ISupportInitialize)(this)).EndInit();

        // Attach Report Events
        this.Detail.Format += new System.EventHandler(this.Detail_Format);
        this.FetchData += new GrapeCity.ActiveReports.SectionReport.FetchEventHandler(this.MissingInfraDataSubreport_InfrastrukturniDanniSubreport_FetchData);
    }

		#endregion

        private ReportHeader ReportHeader;
        private TextBox txtReportTitle;
        private Line Line3;
        private Detail Detail;
        private TextBox txtTitle;
        private TextBox label1;
        private TextBox txtInfo1;
        private TextBox label2;
        private TextBox txtInfo2;
        private TextBox label3;
        private TextBox txtInfo3;
        private TextBox label4;
        private TextBox txtInfo4;
        private Line Line1;
        private TextBox label5;
        private TextBox txtInfo5;
        private ReportFooter ReportFooter;
	}
}
