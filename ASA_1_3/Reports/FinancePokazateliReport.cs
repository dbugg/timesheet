using System;
using System.Data;
using GrapeCity.ActiveReports;
using GrapeCity.ActiveReports.Controls;
using GrapeCity.ActiveReports.SectionReportModel;
using GrapeCity.ActiveReports.Document.Section;
using GrapeCity.ActiveReports.Document;

namespace Asa.Timesheet.WebPages.Reports
{
    public class FinancePokazateliReport : GrapeCity.ActiveReports.SectionReport
	{
		private bool _pdf;
		private string _createdBy= string.Empty;

        public FinancePokazateliReport(DataView dv, bool pdf, string createdBy, DataView dvBGN, string Period)
        {
            _pdf = pdf;
            _createdBy = createdBy;

            InitializeComponent();
            this.DataSource = dvBGN;
            this.ReportEnd += new EventHandler(SubAnalysisRpt1_ReportEnd);
            SubReport1.Report = new FinancePokazateliSub(dv);
            this.Detail.Format += new EventHandler(Detail_Format);
            int nCount = dv.Count;
            decimal dTotal = 0;
            if (dv[nCount - 1]["WorkedSalary"] != DBNull.Value)
                dTotal += (decimal)dv[nCount - 1]["WorkedSalary"];
            if (dv[nCount - 1]["AdminSalary"] != DBNull.Value)
                dTotal += (decimal)dv[nCount - 1]["AdminSalary"];
            if (dv[nCount - 1]["LeaderSalary"] != DBNull.Value)
                dTotal += (decimal)dv[nCount - 1]["LeaderSalary"];
            if (dv[nCount - 1]["StaffSalary"] != DBNull.Value)
                dTotal += (decimal)dv[nCount - 1]["StaffSalary"];
            lblTotalEur.Text = UIHelpers.FormatDecimal(dTotal);

            nCount = dvBGN.Count;
            dTotal = 0;
            if (dvBGN[nCount - 1]["WorkedSalary"] != DBNull.Value)
                dTotal += (decimal)dvBGN[nCount - 1]["WorkedSalary"];
            if (dvBGN[nCount - 1]["AdminSalary"] != DBNull.Value)
                dTotal += (decimal)dvBGN[nCount - 1]["AdminSalary"];
            if (dvBGN[nCount - 1]["LeaderSalary"] != DBNull.Value)
                dTotal += (decimal)dvBGN[nCount - 1]["LeaderSalary"];
            if (dvBGN[nCount - 1]["StaffSalary"] != DBNull.Value)
                dTotal += (decimal)dvBGN[nCount - 1]["StaffSalary"];
            lbTotalBGN.Text = UIHelpers.FormatDecimal(dTotal);
            lblPeriod.Text = Period;
        }
        private void ReportFooter_Format(object sender, System.EventArgs eArgs)
        {
            txtCreatedBy.Text = _createdBy;
        }

        private void PageHeader_Format(object sender, System.EventArgs eArgs)
        {
            if (!_pdf)
                if (this.PageNumber > 1)
                    this.PageHeader.Visible = false;
        }

		#region ActiveReports Designer generated code























































        public void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FinancePokazateliReport));
            this.Detail = new GrapeCity.ActiveReports.SectionReportModel.Detail();
            this.ReportHeader = new GrapeCity.ActiveReports.SectionReportModel.ReportHeader();
            this.ReportFooter = new GrapeCity.ActiveReports.SectionReportModel.ReportFooter();
            this.PageHeader = new GrapeCity.ActiveReports.SectionReportModel.PageHeader();
            this.PageFooter = new GrapeCity.ActiveReports.SectionReportModel.PageFooter();
            this.GroupHeader1 = new GrapeCity.ActiveReports.SectionReportModel.GroupHeader();
            this.GroupFooter1 = new GrapeCity.ActiveReports.SectionReportModel.GroupFooter();
            this.txtHeader = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.Label9 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Line5 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.lblPeriod = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.SubReport1 = new GrapeCity.ActiveReports.SectionReportModel.SubReport();
            this.Label8 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Line6 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.txtHeaderSubcontracterSalary = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtHeaderImcomePerHour = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtHeaderPaymentPerHour = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtHeaderExpencesPerHour = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtHeaderTotalIncome = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtHeaderTotalSalary = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtHeaderProjectName = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtHeaderAdminSalary = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtHeaderWorkedSalary = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtHeaderWorkedHours = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtHeaderPaymentArea = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtHeaderPayments = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtHeaderArea = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.TextBox3 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.TextBox4 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.TextBox5 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.Label12 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lblTotalEur = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.txtImcomePerHour = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.Line2 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.txtExpencesPerHour = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtTotalIncome = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtTotalSalary = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtSubcontracterSalary = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtPaymentPerHour = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtProjectName = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtWorkedSalary = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtAdminSalary = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtWorkedHours = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtPaymentArea = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtPayments = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtArea = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtProjectID = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.TextBox1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.TextBox2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.TextBox6 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.Label10 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lbTotalBGN = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Line3 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.Label7 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.txtCreatedBy = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            ((System.ComponentModel.ISupportInitialize)(this.txtHeader)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblPeriod)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtHeaderSubcontracterSalary)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtHeaderImcomePerHour)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtHeaderPaymentPerHour)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtHeaderExpencesPerHour)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtHeaderTotalIncome)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtHeaderTotalSalary)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtHeaderProjectName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtHeaderAdminSalary)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtHeaderWorkedSalary)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtHeaderWorkedHours)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtHeaderPaymentArea)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtHeaderPayments)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtHeaderArea)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TextBox3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TextBox4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TextBox5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label12)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblTotalEur)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtImcomePerHour)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtExpencesPerHour)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTotalIncome)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTotalSalary)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSubcontracterSalary)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPaymentPerHour)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtProjectName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtWorkedSalary)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAdminSalary)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtWorkedHours)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPaymentArea)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPayments)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtArea)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtProjectID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TextBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TextBox2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TextBox6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbTotalBGN)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCreatedBy)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            // 
            // Detail
            // 
            this.Detail.ColumnSpacing = 0F;
            this.Detail.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
						this.txtImcomePerHour,
						this.Line2,
						this.txtExpencesPerHour,
						this.txtTotalIncome,
						this.txtTotalSalary,
						this.txtSubcontracterSalary,
						this.txtPaymentPerHour,
						this.txtProjectName,
						this.txtWorkedSalary,
						this.txtAdminSalary,
						this.txtWorkedHours,
						this.txtPaymentArea,
						this.txtPayments,
						this.txtArea,
						this.txtProjectID,
						this.TextBox1,
						this.TextBox2,
						this.TextBox6});
            this.Detail.Height = 0.1979167F;
            this.Detail.Name = "Detail";
            // 
            // ReportHeader
            // 
            this.ReportHeader.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
						this.txtHeader,
						this.Label9,
						this.Line5,
						this.lblPeriod});
            this.ReportHeader.Height = 0.9569445F;
            this.ReportHeader.Name = "ReportHeader";
            // 
            // ReportFooter
            // 
            this.ReportFooter.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
						this.Line3,
						this.Label7,
						this.txtCreatedBy});
            this.ReportFooter.Height = 1.020833F;
            this.ReportFooter.Name = "ReportFooter";
            // 
            // PageHeader
            // 
            this.PageHeader.Height = 0F;
            this.PageHeader.Name = "PageHeader";
            // 
            // PageFooter
            // 
            this.PageFooter.Height = 0F;
            this.PageFooter.Name = "PageFooter";
            // 
            // GroupHeader1
            // 
            this.GroupHeader1.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
						this.SubReport1,
						this.Label8,
						this.Line6,
						this.txtHeaderSubcontracterSalary,
						this.txtHeaderImcomePerHour,
						this.txtHeaderPaymentPerHour,
						this.txtHeaderExpencesPerHour,
						this.txtHeaderTotalIncome,
						this.txtHeaderTotalSalary,
						this.txtHeaderProjectName,
						this.txtHeaderAdminSalary,
						this.txtHeaderWorkedSalary,
						this.txtHeaderWorkedHours,
						this.txtHeaderPaymentArea,
						this.txtHeaderPayments,
						this.txtHeaderArea,
						this.TextBox3,
						this.TextBox4,
						this.TextBox5,
						this.Label12,
						this.lblTotalEur});
            this.GroupHeader1.Height = 1.299306F;
            this.GroupHeader1.Name = "GroupHeader1";
            // 
            // GroupFooter1
            // 
            this.GroupFooter1.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
						this.Label10,
						this.lbTotalBGN});
            this.GroupFooter1.Height = 0.2909722F;
            this.GroupFooter1.Name = "GroupFooter1";
            // 
            // txtHeader
            // 
            this.txtHeader.Height = 0.2F;
            this.txtHeader.Left = 0.5625F;
            this.txtHeader.Name = "txtHeader";
            this.txtHeader.Style = "font-size: 12pt; font-weight: bold; text-align: center";
            this.txtHeader.Text = "????????? ??????????";
            this.txtHeader.Top = 0F;
            this.txtHeader.Width = 14.6875F;
            // 
            // Label9
            // 
            this.Label9.Height = 0.2F;
            this.Label9.HyperLink = null;
            this.Label9.Left = 0.5625F;
            this.Label9.Name = "Label9";
            this.Label9.Style = "font-size: 11pt; text-align: left";
            this.Label9.Text = "? EUR";
            this.Label9.Top = 0.6875F;
            this.Label9.Width = 1.875F;
            // 
            // Line5
            // 
            this.Line5.Height = 0F;
            this.Line5.Left = 0.5625F;
            this.Line5.LineWeight = 1F;
            this.Line5.Name = "Line5";
            this.Line5.Top = 0.938F;
            this.Line5.Width = 14.6875F;
            this.Line5.X1 = 0.5625F;
            this.Line5.X2 = 15.25F;
            this.Line5.Y1 = 0.938F;
            this.Line5.Y2 = 0.938F;
            // 
            // lblPeriod
            // 
            this.lblPeriod.Height = 0.2F;
            this.lblPeriod.HyperLink = null;
            this.lblPeriod.Left = 0.5625F;
            this.lblPeriod.Name = "lblPeriod";
            this.lblPeriod.Style = "font-size: 11pt; text-align: left";
            this.lblPeriod.Text = "??????:";
            this.lblPeriod.Top = 0.4375F;
            this.lblPeriod.Width = 14.6875F;
            // 
            // SubReport1
            // 
            this.SubReport1.CloseBorder = false;
            this.SubReport1.Height = 0.1875F;
            this.SubReport1.Left = 0F;
            this.SubReport1.Name = "SubReport1";
            this.SubReport1.Report = null;
            this.SubReport1.Top = 0F;
            this.SubReport1.Width = 15.25F;
            // 
            // Label8
            // 
            this.Label8.Height = 0.2F;
            this.Label8.HyperLink = null;
            this.Label8.Left = 0.5625F;
            this.Label8.Name = "Label8";
            this.Label8.Style = "font-size: 11pt; text-align: left";
            this.Label8.Text = "? ????";
            this.Label8.Top = 0.75F;
            this.Label8.Width = 1.875F;
            // 
            // Line6
            // 
            this.Line6.Height = 0F;
            this.Line6.Left = 0.5625F;
            this.Line6.LineWeight = 1F;
            this.Line6.Name = "Line6";
            this.Line6.Top = 0.9375F;
            this.Line6.Width = 14.6875F;
            this.Line6.X1 = 0.5625F;
            this.Line6.X2 = 15.25F;
            this.Line6.Y1 = 0.9375F;
            this.Line6.Y2 = 0.9375F;
            // 
            // txtHeaderSubcontracterSalary
            // 
            this.txtHeaderSubcontracterSalary.Height = 0.3F;
            this.txtHeaderSubcontracterSalary.Left = 8.895F;
            this.txtHeaderSubcontracterSalary.Name = "txtHeaderSubcontracterSalary";
            this.txtHeaderSubcontracterSalary.Style = "font-size: 8pt; font-weight: bold; text-align: right; ddo-char-set: 1";
            this.txtHeaderSubcontracterSalary.Text = "??????. ??????? BGN";
            this.txtHeaderSubcontracterSalary.Top = 1F;
            this.txtHeaderSubcontracterSalary.Width = 1F;
            // 
            // txtHeaderImcomePerHour
            // 
            this.txtHeaderImcomePerHour.Height = 0.3F;
            this.txtHeaderImcomePerHour.Left = 13.41F;
            this.txtHeaderImcomePerHour.Name = "txtHeaderImcomePerHour";
            this.txtHeaderImcomePerHour.Style = "font-size: 8pt; font-weight: bold; text-align: right; ddo-char-set: 1";
            this.txtHeaderImcomePerHour.Text = "??????? ?? ??? BGN";
            this.txtHeaderImcomePerHour.Top = 1F;
            this.txtHeaderImcomePerHour.Width = 0.8F;
            // 
            // txtHeaderPaymentPerHour
            // 
            this.txtHeaderPaymentPerHour.Height = 0.3F;
            this.txtHeaderPaymentPerHour.Left = 12.57F;
            this.txtHeaderPaymentPerHour.Name = "txtHeaderPaymentPerHour";
            this.txtHeaderPaymentPerHour.Style = "font-size: 8pt; font-weight: bold; text-align: right; ddo-char-set: 1";
            this.txtHeaderPaymentPerHour.Text = "??????? ?? ??? BGN";
            this.txtHeaderPaymentPerHour.Top = 1F;
            this.txtHeaderPaymentPerHour.Width = 0.8F;
            // 
            // txtHeaderExpencesPerHour
            // 
            this.txtHeaderExpencesPerHour.Height = 0.3F;
            this.txtHeaderExpencesPerHour.Left = 10.915F;
            this.txtHeaderExpencesPerHour.Name = "txtHeaderExpencesPerHour";
            this.txtHeaderExpencesPerHour.Style = "font-size: 8pt; font-weight: bold; text-align: right; ddo-char-set: 1";
            this.txtHeaderExpencesPerHour.Text = "??????? ?? ??? BGN";
            this.txtHeaderExpencesPerHour.Top = 1F;
            this.txtHeaderExpencesPerHour.Width = 0.8F;
            // 
            // txtHeaderTotalIncome
            // 
            this.txtHeaderTotalIncome.Height = 0.3F;
            this.txtHeaderTotalIncome.Left = 14.22F;
            this.txtHeaderTotalIncome.Name = "txtHeaderTotalIncome";
            this.txtHeaderTotalIncome.Style = "font-size: 8pt; font-weight: bold; text-align: right; ddo-char-set: 1";
            this.txtHeaderTotalIncome.Text = "?????? ??????? BGN";
            this.txtHeaderTotalIncome.Top = 1F;
            this.txtHeaderTotalIncome.Width = 1F;
            // 
            // txtHeaderTotalSalary
            // 
            this.txtHeaderTotalSalary.Height = 0.3F;
            this.txtHeaderTotalSalary.Left = 9.905F;
            this.txtHeaderTotalSalary.Name = "txtHeaderTotalSalary";
            this.txtHeaderTotalSalary.Style = "font-size: 8pt; font-weight: bold; text-align: right; ddo-char-set: 1";
            this.txtHeaderTotalSalary.SummaryRunning = GrapeCity.ActiveReports.SectionReportModel.SummaryRunning.Group;
            this.txtHeaderTotalSalary.Text = "?????? ??????? BGN";
            this.txtHeaderTotalSalary.Top = 1F;
            this.txtHeaderTotalSalary.Width = 1F;
            // 
            // txtHeaderProjectName
            // 
            this.txtHeaderProjectName.Height = 0.3F;
            this.txtHeaderProjectName.Left = 0.5625F;
            this.txtHeaderProjectName.Name = "txtHeaderProjectName";
            this.txtHeaderProjectName.Style = "font-size: 8pt; font-weight: bold; text-align: left; ddo-char-set: 1";
            this.txtHeaderProjectName.Text = "??????";
            this.txtHeaderProjectName.Top = 1F;
            this.txtHeaderProjectName.Width = 1.875F;
            // 
            // txtHeaderAdminSalary
            // 
            this.txtHeaderAdminSalary.Height = 0.3F;
            this.txtHeaderAdminSalary.Left = 6.46F;
            this.txtHeaderAdminSalary.Name = "txtHeaderAdminSalary";
            this.txtHeaderAdminSalary.Style = "font-size: 8pt; font-weight: bold; text-align: right; ddo-char-set: 1";
            this.txtHeaderAdminSalary.Text = "?????. ????. BGN";
            this.txtHeaderAdminSalary.Top = 1F;
            this.txtHeaderAdminSalary.Width = 0.8F;
            // 
            // txtHeaderWorkedSalary
            // 
            this.txtHeaderWorkedSalary.Height = 0.3F;
            this.txtHeaderWorkedSalary.Left = 5.5F;
            this.txtHeaderWorkedSalary.Name = "txtHeaderWorkedSalary";
            this.txtHeaderWorkedSalary.Style = "font-size: 8pt; font-weight: bold; text-align: right; ddo-char-set: 1";
            this.txtHeaderWorkedSalary.Text = "??????? BGN";
            this.txtHeaderWorkedSalary.Top = 1F;
            this.txtHeaderWorkedSalary.Width = 0.95F;
            // 
            // txtHeaderWorkedHours
            // 
            this.txtHeaderWorkedHours.Height = 0.3F;
            this.txtHeaderWorkedHours.Left = 4.75F;
            this.txtHeaderWorkedHours.Name = "txtHeaderWorkedHours";
            this.txtHeaderWorkedHours.Style = "font-size: 8pt; font-weight: bold; text-align: right; ddo-char-set: 1";
            this.txtHeaderWorkedHours.Text = "???. ??????";
            this.txtHeaderWorkedHours.Top = 1F;
            this.txtHeaderWorkedHours.Width = 0.75F;
            // 
            // txtHeaderPaymentArea
            // 
            this.txtHeaderPaymentArea.Height = 0.3F;
            this.txtHeaderPaymentArea.Left = 2.9375F;
            this.txtHeaderPaymentArea.Name = "txtHeaderPaymentArea";
            this.txtHeaderPaymentArea.Style = "font-size: 8pt; font-weight: bold; text-align: right; ddo-char-set: 1";
            this.txtHeaderPaymentArea.Text = "??????? BGN";
            this.txtHeaderPaymentArea.Top = 1F;
            this.txtHeaderPaymentArea.Width = 0.8825001F;
            // 
            // txtHeaderPayments
            // 
            this.txtHeaderPayments.Height = 0.3F;
            this.txtHeaderPayments.Left = 3.83F;
            this.txtHeaderPayments.Name = "txtHeaderPayments";
            this.txtHeaderPayments.Style = "font-size: 8pt; font-weight: bold; text-align: right; ddo-char-set: 1";
            this.txtHeaderPayments.Text = "???????  BGN/M2";
            this.txtHeaderPayments.Top = 1F;
            this.txtHeaderPayments.Width = 0.92F;
            // 
            // txtHeaderArea
            // 
            this.txtHeaderArea.Height = 0.3F;
            this.txtHeaderArea.Left = 2.4375F;
            this.txtHeaderArea.Name = "txtHeaderArea";
            this.txtHeaderArea.Style = "font-size: 8pt; font-weight: bold; text-align: right; ddo-char-set: 1";
            this.txtHeaderArea.Text = "M2";
            this.txtHeaderArea.Top = 1F;
            this.txtHeaderArea.Width = 0.49F;
            // 
            // TextBox3
            // 
            this.TextBox3.Height = 0.3F;
            this.TextBox3.Left = 7.25F;
            this.TextBox3.Name = "TextBox3";
            this.TextBox3.Style = "font-size: 8pt; font-weight: bold; text-align: right; ddo-char-set: 1";
            this.TextBox3.Text = "?????. ????. BGN";
            this.TextBox3.Top = 1F;
            this.TextBox3.Width = 0.8F;
            // 
            // TextBox4
            // 
            this.TextBox4.Height = 0.3F;
            this.TextBox4.Left = 8.0625F;
            this.TextBox4.Name = "TextBox4";
            this.TextBox4.Style = "font-size: 8pt; font-weight: bold; text-align: right; ddo-char-set: 1";
            this.TextBox4.Text = "??????- ???????";
            this.TextBox4.Top = 1F;
            this.TextBox4.Width = 0.8F;
            // 
            // TextBox5
            // 
            this.TextBox5.Height = 0.3F;
            this.TextBox5.Left = 11.75F;
            this.TextBox5.Name = "TextBox5";
            this.TextBox5.Style = "font-size: 8pt; font-weight: bold; text-align: right; ddo-char-set: 1";
            this.TextBox5.Text = "??????? BGN/M2";
            this.TextBox5.Top = 1F;
            this.TextBox5.Width = 0.8F;
            // 
            // Label12
            // 
            this.Label12.Height = 0.2F;
            this.Label12.HyperLink = null;
            this.Label12.Left = 2.4375F;
            this.Label12.Name = "Label12";
            this.Label12.Style = "font-size: 9pt; font-weight: bold; ddo-char-set: 1";
            this.Label12.Text = "????? ??????? ???, ??? ?????????????? (EUR):";
            this.Label12.Top = 0.375F;
            this.Label12.Width = 3.065F;
            // 
            // lblTotalEur
            // 
            this.lblTotalEur.Height = 0.2F;
            this.lblTotalEur.HyperLink = null;
            this.lblTotalEur.Left = 5.5125F;
            this.lblTotalEur.Name = "lblTotalEur";
            this.lblTotalEur.Style = "font-weight: bold";
            this.lblTotalEur.Text = "";
            this.lblTotalEur.Top = 0.375F;
            this.lblTotalEur.Width = 1F;
            // 
            // txtImcomePerHour
            // 
            this.txtImcomePerHour.DataField = "ImcomePerHour";
            this.txtImcomePerHour.Height = 0.2F;
            this.txtImcomePerHour.Left = 13.435F;
            this.txtImcomePerHour.Name = "txtImcomePerHour";
            this.txtImcomePerHour.OutputFormat = resources.GetString("txtImcomePerHour.OutputFormat");
            this.txtImcomePerHour.Style = "text-align: right";
            this.txtImcomePerHour.Text = "ImcomePerHour";
            this.txtImcomePerHour.Top = 0F;
            this.txtImcomePerHour.Width = 0.8F;
            // 
            // Line2
            // 
            this.Line2.Height = 0F;
            this.Line2.Left = 0.5625F;
            this.Line2.LineWeight = 1F;
            this.Line2.Name = "Line2";
            this.Line2.Top = 0F;
            this.Line2.Width = 14.6875F;
            this.Line2.X1 = 0.5625F;
            this.Line2.X2 = 15.25F;
            this.Line2.Y1 = 0F;
            this.Line2.Y2 = 0F;
            // 
            // txtExpencesPerHour
            // 
            this.txtExpencesPerHour.DataField = "ExpencesPerHour";
            this.txtExpencesPerHour.Height = 0.2F;
            this.txtExpencesPerHour.Left = 10.915F;
            this.txtExpencesPerHour.Name = "txtExpencesPerHour";
            this.txtExpencesPerHour.OutputFormat = resources.GetString("txtExpencesPerHour.OutputFormat");
            this.txtExpencesPerHour.Style = "text-align: right";
            this.txtExpencesPerHour.Text = "ExpencesPerHour";
            this.txtExpencesPerHour.Top = 0F;
            this.txtExpencesPerHour.Width = 0.8F;
            // 
            // txtTotalIncome
            // 
            this.txtTotalIncome.DataField = "TotalIncome";
            this.txtTotalIncome.Height = 0.2F;
            this.txtTotalIncome.Left = 14.245F;
            this.txtTotalIncome.Name = "txtTotalIncome";
            this.txtTotalIncome.OutputFormat = resources.GetString("txtTotalIncome.OutputFormat");
            this.txtTotalIncome.Style = "text-align: right";
            this.txtTotalIncome.Text = "TotalIncome";
            this.txtTotalIncome.Top = 0F;
            this.txtTotalIncome.Width = 1F;
            // 
            // txtTotalSalary
            // 
            this.txtTotalSalary.DataField = "TotalSalary";
            this.txtTotalSalary.Height = 0.2F;
            this.txtTotalSalary.Left = 9.905F;
            this.txtTotalSalary.Name = "txtTotalSalary";
            this.txtTotalSalary.OutputFormat = resources.GetString("txtTotalSalary.OutputFormat");
            this.txtTotalSalary.Style = "text-align: right";
            this.txtTotalSalary.Text = "TotalSalary";
            this.txtTotalSalary.Top = 0F;
            this.txtTotalSalary.Width = 1F;
            // 
            // txtSubcontracterSalary
            // 
            this.txtSubcontracterSalary.DataField = "SubcontracterSalary";
            this.txtSubcontracterSalary.Height = 0.2F;
            this.txtSubcontracterSalary.Left = 8.895F;
            this.txtSubcontracterSalary.Name = "txtSubcontracterSalary";
            this.txtSubcontracterSalary.OutputFormat = resources.GetString("txtSubcontracterSalary.OutputFormat");
            this.txtSubcontracterSalary.Style = "text-align: right";
            this.txtSubcontracterSalary.Text = "SubcontracterSalary";
            this.txtSubcontracterSalary.Top = 0F;
            this.txtSubcontracterSalary.Width = 1F;
            // 
            // txtPaymentPerHour
            // 
            this.txtPaymentPerHour.DataField = "PaymentPerHour";
            this.txtPaymentPerHour.Height = 0.2F;
            this.txtPaymentPerHour.Left = 12.57F;
            this.txtPaymentPerHour.Name = "txtPaymentPerHour";
            this.txtPaymentPerHour.OutputFormat = resources.GetString("txtPaymentPerHour.OutputFormat");
            this.txtPaymentPerHour.Style = "text-align: right";
            this.txtPaymentPerHour.Text = "PaymentPerHour";
            this.txtPaymentPerHour.Top = 0F;
            this.txtPaymentPerHour.Width = 0.8F;
            // 
            // txtProjectName
            // 
            this.txtProjectName.DataField = "ProjectName";
            this.txtProjectName.Height = 0.2F;
            this.txtProjectName.Left = 0.5625F;
            this.txtProjectName.Name = "txtProjectName";
            this.txtProjectName.Text = "ProjectName";
            this.txtProjectName.Top = 0F;
            this.txtProjectName.Width = 1.875F;
            // 
            // txtWorkedSalary
            // 
            this.txtWorkedSalary.DataField = "WorkedSalary";
            this.txtWorkedSalary.Height = 0.2F;
            this.txtWorkedSalary.Left = 5.5125F;
            this.txtWorkedSalary.Name = "txtWorkedSalary";
            this.txtWorkedSalary.OutputFormat = resources.GetString("txtWorkedSalary.OutputFormat");
            this.txtWorkedSalary.Style = "text-align: right";
            this.txtWorkedSalary.Text = "WorkedSalary";
            this.txtWorkedSalary.Top = 0F;
            this.txtWorkedSalary.Width = 1F;
            // 
            // txtAdminSalary
            // 
            this.txtAdminSalary.DataField = "AdminSalary";
            this.txtAdminSalary.Height = 0.2F;
            this.txtAdminSalary.Left = 6.5225F;
            this.txtAdminSalary.Name = "txtAdminSalary";
            this.txtAdminSalary.OutputFormat = resources.GetString("txtAdminSalary.OutputFormat");
            this.txtAdminSalary.Style = "text-align: right";
            this.txtAdminSalary.Text = "AdminSalary";
            this.txtAdminSalary.Top = 0F;
            this.txtAdminSalary.Width = 0.8F;
            // 
            // txtWorkedHours
            // 
            this.txtWorkedHours.DataField = "WorkedHours";
            this.txtWorkedHours.Height = 0.2F;
            this.txtWorkedHours.Left = 4.7025F;
            this.txtWorkedHours.Name = "txtWorkedHours";
            this.txtWorkedHours.OutputFormat = resources.GetString("txtWorkedHours.OutputFormat");
            this.txtWorkedHours.Style = "text-align: right";
            this.txtWorkedHours.Text = "WorkedHours";
            this.txtWorkedHours.Top = 0F;
            this.txtWorkedHours.Width = 0.8F;
            // 
            // txtPaymentArea
            // 
            this.txtPaymentArea.DataField = "PaymentArea";
            this.txtPaymentArea.Height = 0.2F;
            this.txtPaymentArea.Left = 3.8925F;
            this.txtPaymentArea.Name = "txtPaymentArea";
            this.txtPaymentArea.OutputFormat = resources.GetString("txtPaymentArea.OutputFormat");
            this.txtPaymentArea.Style = "text-align: right";
            this.txtPaymentArea.Text = "PaymentArea";
            this.txtPaymentArea.Top = 0F;
            this.txtPaymentArea.Width = 0.8F;
            // 
            // txtPayments
            // 
            this.txtPayments.DataField = "Payments";
            this.txtPayments.Height = 0.2F;
            this.txtPayments.Left = 2.9375F;
            this.txtPayments.Name = "txtPayments";
            this.txtPayments.OutputFormat = resources.GetString("txtPayments.OutputFormat");
            this.txtPayments.Style = "text-align: right";
            this.txtPayments.Text = "Payments";
            this.txtPayments.Top = 0F;
            this.txtPayments.Width = 0.8825001F;
            // 
            // txtArea
            // 
            this.txtArea.DataField = "Area";
            this.txtArea.Height = 0.2F;
            this.txtArea.Left = 2.4375F;
            this.txtArea.Name = "txtArea";
            this.txtArea.OutputFormat = resources.GetString("txtArea.OutputFormat");
            this.txtArea.Style = "text-align: right; vertical-align: top";
            this.txtArea.Text = "Area";
            this.txtArea.Top = 0F;
            this.txtArea.Width = 0.49F;
            // 
            // txtProjectID
            // 
            this.txtProjectID.DataField = "ProjectID";
            this.txtProjectID.Height = 0.2F;
            this.txtProjectID.Left = 0.5625F;
            this.txtProjectID.Name = "txtProjectID";
            this.txtProjectID.Text = "ProjectID ";
            this.txtProjectID.Top = 0F;
            this.txtProjectID.Visible = false;
            this.txtProjectID.Width = 0.4375F;
            // 
            // TextBox1
            // 
            this.TextBox1.DataField = "LeaderSalary";
            this.TextBox1.Height = 0.2F;
            this.TextBox1.Left = 7.322917F;
            this.TextBox1.Name = "TextBox1";
            this.TextBox1.OutputFormat = resources.GetString("TextBox1.OutputFormat");
            this.TextBox1.Style = "text-align: right";
            this.TextBox1.Text = "AdminSalary";
            this.TextBox1.Top = 0F;
            this.TextBox1.Width = 0.8F;
            // 
            // TextBox2
            // 
            this.TextBox2.DataField = "StaffSalary";
            this.TextBox2.Height = 0.2F;
            this.TextBox2.Left = 8.125F;
            this.TextBox2.Name = "TextBox2";
            this.TextBox2.OutputFormat = resources.GetString("TextBox2.OutputFormat");
            this.TextBox2.Style = "text-align: right";
            this.TextBox2.Text = "AdminSalary";
            this.TextBox2.Top = 0F;
            this.TextBox2.Width = 0.75F;
            // 
            // TextBox6
            // 
            this.TextBox6.DataField = "ExpencesPerArea";
            this.TextBox6.Height = 0.2F;
            this.TextBox6.Left = 11.74833F;
            this.TextBox6.Name = "TextBox6";
            this.TextBox6.OutputFormat = resources.GetString("TextBox6.OutputFormat");
            this.TextBox6.Style = "text-align: right";
            this.TextBox6.Text = "ExpencesPerArea";
            this.TextBox6.Top = 0F;
            this.TextBox6.Width = 0.8F;
            // 
            // Label10
            // 
            this.Label10.Height = 0.2F;
            this.Label10.HyperLink = null;
            this.Label10.Left = 2.4375F;
            this.Label10.Name = "Label10";
            this.Label10.Style = "font-size: 9pt; font-weight: bold; ddo-char-set: 1";
            this.Label10.Text = "????? ??????? ???, ??? ??????????????(??.):";
            this.Label10.Top = 0F;
            this.Label10.Width = 3.065F;
            // 
            // lbTotalBGN
            // 
            this.lbTotalBGN.Height = 0.2F;
            this.lbTotalBGN.HyperLink = null;
            this.lbTotalBGN.Left = 5.5125F;
            this.lbTotalBGN.Name = "lbTotalBGN";
            this.lbTotalBGN.Style = "font-weight: bold";
            this.lbTotalBGN.Text = "";
            this.lbTotalBGN.Top = 0F;
            this.lbTotalBGN.Width = 1F;
            // 
            // Line3
            // 
            this.Line3.Height = 0F;
            this.Line3.Left = 0.5625F;
            this.Line3.LineWeight = 1F;
            this.Line3.Name = "Line3";
            this.Line3.Top = 0.375F;
            this.Line3.Width = 14.6875F;
            this.Line3.X1 = 0.5625F;
            this.Line3.X2 = 15.25F;
            this.Line3.Y1 = 0.375F;
            this.Line3.Y2 = 0.375F;
            // 
            // Label7
            // 
            this.Label7.Height = 0.2F;
            this.Label7.HyperLink = null;
            this.Label7.Left = 0.5625F;
            this.Label7.Name = "Label7";
            this.Label7.Style = "font-size: 11pt; text-align: right";
            this.Label7.Text = "????????: ......................................";
            this.Label7.Top = 0.57F;
            this.Label7.Width = 2F;
            // 
            // txtCreatedBy
            // 
            this.txtCreatedBy.Height = 0.2F;
            this.txtCreatedBy.Left = 3.3825F;
            this.txtCreatedBy.Name = "txtCreatedBy";
            this.txtCreatedBy.Style = "font-size: 10pt; font-weight: normal; text-align: justify";
            this.txtCreatedBy.Text = "txtCreatedBy";
            this.txtCreatedBy.Top = 0.8125F;
            this.txtCreatedBy.Width = 1.8F;
            // 
            // ActiveReport1
            // 
            this.MasterReport = false;
            this.PageSettings.PaperHeight = 11F;
            this.PageSettings.PaperWidth = 8.5F;
            this.PrintWidth = 15.27083F;
            this.Sections.Add(this.ReportHeader);
            this.Sections.Add(this.PageHeader);
            this.Sections.Add(this.GroupHeader1);
            this.Sections.Add(this.Detail);
            this.Sections.Add(this.GroupFooter1);
            this.Sections.Add(this.PageFooter);
            this.Sections.Add(this.ReportFooter);
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule(resources.GetString("$this.StyleSheet"), "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: inherit; font-style: inherit; font-variant: inherit; font-weight: bo" +
                        "ld; font-size: 16pt; font-size-adjust: inherit; font-stretch: inherit", "Heading1", "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: Times New Roman; font-style: italic; font-variant: inherit; font-wei" +
                        "ght: bold; font-size: 14pt; font-size-adjust: inherit; font-stretch: inherit", "Heading2", "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: inherit; font-style: inherit; font-variant: inherit; font-weight: bo" +
                        "ld; font-size: 13pt; font-size-adjust: inherit; font-stretch: inherit", "Heading3", "Normal"));
            ((System.ComponentModel.ISupportInitialize)(this.txtHeader)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblPeriod)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtHeaderSubcontracterSalary)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtHeaderImcomePerHour)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtHeaderPaymentPerHour)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtHeaderExpencesPerHour)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtHeaderTotalIncome)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtHeaderTotalSalary)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtHeaderProjectName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtHeaderAdminSalary)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtHeaderWorkedSalary)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtHeaderWorkedHours)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtHeaderPaymentArea)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtHeaderPayments)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtHeaderArea)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TextBox3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TextBox4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TextBox5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label12)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblTotalEur)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtImcomePerHour)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtExpencesPerHour)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTotalIncome)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTotalSalary)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSubcontracterSalary)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPaymentPerHour)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtProjectName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtWorkedSalary)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAdminSalary)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtWorkedHours)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPaymentArea)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPayments)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtArea)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtProjectID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TextBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TextBox2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TextBox6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbTotalBGN)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCreatedBy)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();

            // Attach Report Events
            this.ReportFooter.Format += new System.EventHandler(this.ReportFooter_Format);
            this.PageHeader.Format += new System.EventHandler(this.PageHeader_Format);
        }

		#endregion

        private void SubAnalysisRpt1_ReportEnd(object sender, EventArgs e)
        {
            if (!this._pdf) return;

            Logo1 logo = new Logo1();
            logo.Run();

            for (int i = 0; i < this.Document.Pages.Count; i++)
                this.Document.Pages[i].Overlay(logo.Document.Pages[0]);
        }
        private void Detail_Format(object sender, EventArgs e)
        {
            if (txtTotalIncome.Text.StartsWith("-"))
                txtPaymentArea.ForeColor = txtProjectName.ForeColor = txtArea.ForeColor = txtPayments.ForeColor =
                    txtWorkedHours.ForeColor = txtWorkedSalary.ForeColor = txtAdminSalary.ForeColor =
                    TextBox2.ForeColor = TextBox1.ForeColor = txtSubcontracterSalary.ForeColor =
                    txtTotalSalary.ForeColor = txtExpencesPerHour.ForeColor =
                    txtPaymentPerHour.ForeColor = txtImcomePerHour.ForeColor = txtTotalIncome.ForeColor = System.Drawing.Color.Red;
            else
                txtPaymentArea.ForeColor = txtProjectName.ForeColor = txtArea.ForeColor = txtPayments.ForeColor =
                    txtWorkedHours.ForeColor = txtWorkedSalary.ForeColor = txtAdminSalary.ForeColor =
                    TextBox2.ForeColor = TextBox1.ForeColor = txtSubcontracterSalary.ForeColor =
                    txtTotalSalary.ForeColor = txtExpencesPerHour.ForeColor =
                    txtPaymentPerHour.ForeColor = txtImcomePerHour.ForeColor = txtTotalIncome.ForeColor = System.Drawing.Color.Black;
        }

        private ReportHeader ReportHeader;
        private TextBox txtHeader;
        private Label Label9;
        private Line Line5;
        private Label lblPeriod;
        private PageHeader PageHeader;
        private GroupHeader GroupHeader1;
        private SubReport SubReport1;
        private Label Label8;
        private Line Line6;
        private TextBox txtHeaderSubcontracterSalary;
        private TextBox txtHeaderImcomePerHour;
        private TextBox txtHeaderPaymentPerHour;
        private TextBox txtHeaderExpencesPerHour;
        private TextBox txtHeaderTotalIncome;
        private TextBox txtHeaderTotalSalary;
        private TextBox txtHeaderProjectName;
        private TextBox txtHeaderAdminSalary;
        private TextBox txtHeaderWorkedSalary;
        private TextBox txtHeaderWorkedHours;
        private TextBox txtHeaderPaymentArea;
        private TextBox txtHeaderPayments;
        private TextBox txtHeaderArea;
        private TextBox TextBox3;
        private TextBox TextBox4;
        private TextBox TextBox5;
        private Label Label12;
        private Label lblTotalEur;
        private Detail Detail;
        private TextBox txtImcomePerHour;
        private Line Line2;
        private TextBox txtExpencesPerHour;
        private TextBox txtTotalIncome;
        private TextBox txtTotalSalary;
        private TextBox txtSubcontracterSalary;
        private TextBox txtPaymentPerHour;
        private TextBox txtProjectName;
        private TextBox txtWorkedSalary;
        private TextBox txtAdminSalary;
        private TextBox txtWorkedHours;
        private TextBox txtPaymentArea;
        private TextBox txtPayments;
        private TextBox txtArea;
        private TextBox txtProjectID;
        private TextBox TextBox1;
        private TextBox TextBox2;
        private TextBox TextBox6;
        private GroupFooter GroupFooter1;
        private Label Label10;
        private Label lbTotalBGN;
        private PageFooter PageFooter;
        private ReportFooter ReportFooter;
        private Line Line3;
        private Label Label7;
        private TextBox txtCreatedBy;
	}
}
