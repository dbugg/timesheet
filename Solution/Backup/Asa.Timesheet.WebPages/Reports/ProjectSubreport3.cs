using System;
using System.Data;
using Asa.Timesheet.Data.Util;
using GrapeCity.ActiveReports;
using GrapeCity.ActiveReports.Controls;
using GrapeCity.ActiveReports.SectionReportModel;
using GrapeCity.ActiveReports.Document.Section;
using GrapeCity.ActiveReports.Document;

namespace Asa.Timesheet.WebPages.Reports
{
    public class ProjectSubreport3 : GrapeCity.ActiveReports.SectionReport
	{
        public ProjectSubreport3(DataView dv)
        {
            InitializeComponent();
            this.DataSource = dv;
            this.Detail.Format += new EventHandler(Detail_Format);
            this.GroupFooter1.Format += new EventHandler(GroupFooter1_Format);
        }

		#region ActiveReports Designer generated code





















        public void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ProjectSubreport3));
            this.Detail = new GrapeCity.ActiveReports.SectionReportModel.Detail();
            this.PageHeader = new GrapeCity.ActiveReports.SectionReportModel.PageHeader();
            this.PageFooter = new GrapeCity.ActiveReports.SectionReportModel.PageFooter();
            this.GroupHeader1 = new GrapeCity.ActiveReports.SectionReportModel.GroupHeader();
            this.GroupFooter1 = new GrapeCity.ActiveReports.SectionReportModel.GroupFooter();
            this.Label19 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label12 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label13 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label20 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label21 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label22 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.TextBox12 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtTotalHours = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.TextBox10 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.TextBox15 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.TextBox16 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.TextBox17 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.Line2 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.Label8 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.txtTotalHoursT = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.TextBox14 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            ((System.ComponentModel.ISupportInitialize)(this.Label19)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label12)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label13)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label20)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label21)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label22)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TextBox12)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTotalHours)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TextBox10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TextBox15)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TextBox16)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TextBox17)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTotalHoursT)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TextBox14)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            // 
            // Detail
            // 
            this.Detail.ColumnSpacing = 0F;
            this.Detail.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
						this.TextBox12,
						this.txtTotalHours,
						this.TextBox10,
						this.TextBox15,
						this.TextBox16,
						this.TextBox17});
            this.Detail.Height = 0.2076389F;
            this.Detail.Name = "Detail";
            // 
            // PageHeader
            // 
            this.PageHeader.Height = 0F;
            this.PageHeader.Name = "PageHeader";
            // 
            // PageFooter
            // 
            this.PageFooter.Height = 0F;
            this.PageFooter.Name = "PageFooter";
            // 
            // GroupHeader1
            // 
            this.GroupHeader1.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
						this.Label19,
						this.Label12,
						this.Label13,
						this.Label20,
						this.Label21,
						this.Label22});
            this.GroupHeader1.Height = 0.2618056F;
            this.GroupHeader1.Name = "GroupHeader1";
            // 
            // GroupFooter1
            // 
            this.GroupFooter1.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
						this.Line2,
						this.Label8,
						this.txtTotalHoursT,
						this.TextBox14});
            this.GroupFooter1.Height = 0.2597222F;
            this.GroupFooter1.Name = "GroupFooter1";
            this.GroupFooter1.Visible = false;
            // 
            // Label19
            // 
            this.Label19.Height = 0.2F;
            this.Label19.HyperLink = null;
            this.Label19.Left = 0F;
            this.Label19.Name = "Label19";
            this.Label19.Style = "font-weight: bold";
            this.Label19.Text = "????????";
            this.Label19.Top = 0.0625F;
            this.Label19.Width = 2.6875F;
            // 
            // Label12
            // 
            this.Label12.Height = 0.2F;
            this.Label12.HyperLink = null;
            this.Label12.Left = 2.6875F;
            this.Label12.Name = "Label12";
            this.Label12.Style = "font-weight: bold; text-align: right";
            this.Label12.Text = "????(????)";
            this.Label12.Top = 0.0625F;
            this.Label12.Width = 1.4F;
            // 
            // Label13
            // 
            this.Label13.Height = 0.2F;
            this.Label13.HyperLink = null;
            this.Label13.Left = 4.0625F;
            this.Label13.Name = "Label13";
            this.Label13.Style = "font-weight: bold; text-align: right";
            this.Label13.Text = "?????????????? (BGN)";
            this.Label13.Top = 0.0625F;
            this.Label13.Width = 1.8125F;
            // 
            // Label20
            // 
            this.Label20.Height = 0.2F;
            this.Label20.HyperLink = null;
            this.Label20.Left = 5.875F;
            this.Label20.Name = "Label20";
            this.Label20.Style = "font-weight: bold; text-align: right";
            this.Label20.Text = "??/???";
            this.Label20.Top = 0.0625F;
            this.Label20.Width = 1.0625F;
            // 
            // Label21
            // 
            this.Label21.Height = 0.2F;
            this.Label21.HyperLink = null;
            this.Label21.Left = 6.9375F;
            this.Label21.Name = "Label21";
            this.Label21.Style = "font-weight: bold; text-align: right";
            this.Label21.Text = "???";
            this.Label21.Top = 0.0625F;
            this.Label21.Width = 1.0625F;
            // 
            // Label22
            // 
            this.Label22.Height = 0.2F;
            this.Label22.HyperLink = null;
            this.Label22.Left = 8F;
            this.Label22.Name = "Label22";
            this.Label22.Style = "font-weight: bold; text-align: right";
            this.Label22.Text = "??????";
            this.Label22.Top = 0.0625F;
            this.Label22.Width = 1.125F;
            // 
            // TextBox12
            // 
            this.TextBox12.DataField = "FullName";
            this.TextBox12.Height = 0.2F;
            this.TextBox12.Left = 0F;
            this.TextBox12.Name = "TextBox12";
            this.TextBox12.Text = "TextBox12";
            this.TextBox12.Top = 0F;
            this.TextBox12.Width = 2.6875F;
            // 
            // txtTotalHours
            // 
            this.txtTotalHours.DataField = "TotalHours";
            this.txtTotalHours.Height = 0.2F;
            this.txtTotalHours.Left = 2.6875F;
            this.txtTotalHours.Name = "txtTotalHours";
            this.txtTotalHours.OutputFormat = resources.GetString("txtTotalHours.OutputFormat");
            this.txtTotalHours.Style = "text-align: right";
            this.txtTotalHours.Text = "TextBox13";
            this.txtTotalHours.Top = 0F;
            this.txtTotalHours.Width = 1.375F;
            // 
            // TextBox10
            // 
            this.TextBox10.DataField = "Amount";
            this.TextBox10.Height = 0.2F;
            this.TextBox10.Left = 4.0625F;
            this.TextBox10.Name = "TextBox10";
            this.TextBox10.OutputFormat = resources.GetString("TextBox10.OutputFormat");
            this.TextBox10.Style = "text-align: right";
            this.TextBox10.Text = "TextBox10";
            this.TextBox10.Top = 0F;
            this.TextBox10.Width = 1.8125F;
            // 
            // TextBox15
            // 
            this.TextBox15.DataField = "PerHour";
            this.TextBox15.Height = 0.2F;
            this.TextBox15.Left = 5.875F;
            this.TextBox15.Name = "TextBox15";
            this.TextBox15.OutputFormat = resources.GetString("TextBox15.OutputFormat");
            this.TextBox15.Style = "text-align: right";
            this.TextBox15.Text = "TextBox13";
            this.TextBox15.Top = 0F;
            this.TextBox15.Width = 1.0625F;
            // 
            // TextBox16
            // 
            this.TextBox16.DataField = "TotalDays";
            this.TextBox16.Height = 0.2F;
            this.TextBox16.Left = 6.9375F;
            this.TextBox16.Name = "TextBox16";
            this.TextBox16.OutputFormat = resources.GetString("TextBox16.OutputFormat");
            this.TextBox16.Style = "text-align: right";
            this.TextBox16.Text = "TextBox13";
            this.TextBox16.Top = 0F;
            this.TextBox16.Width = 1.0625F;
            // 
            // TextBox17
            // 
            this.TextBox17.DataField = "TotalMonths";
            this.TextBox17.Height = 0.2F;
            this.TextBox17.Left = 8F;
            this.TextBox17.Name = "TextBox17";
            this.TextBox17.OutputFormat = resources.GetString("TextBox17.OutputFormat");
            this.TextBox17.Style = "text-align: right";
            this.TextBox17.Text = "TextBox13";
            this.TextBox17.Top = 0F;
            this.TextBox17.Width = 1.125F;
            // 
            // Line2
            // 
            this.Line2.Height = 0F;
            this.Line2.Left = 0F;
            this.Line2.LineWeight = 1F;
            this.Line2.Name = "Line2";
            this.Line2.Top = 0F;
            this.Line2.Width = 9.1875F;
            this.Line2.X1 = 0F;
            this.Line2.X2 = 9.1875F;
            this.Line2.Y1 = 0F;
            this.Line2.Y2 = 0F;
            // 
            // Label8
            // 
            this.Label8.Height = 0.2F;
            this.Label8.HyperLink = null;
            this.Label8.Left = 0.006944418F;
            this.Label8.Name = "Label8";
            this.Label8.Style = "";
            this.Label8.Text = "????:";
            this.Label8.Top = 0.006944444F;
            this.Label8.Width = 2.680556F;
            // 
            // txtTotalHoursT
            // 
            this.txtTotalHoursT.DataField = "TotalTime";
            this.txtTotalHoursT.Height = 0.2F;
            this.txtTotalHoursT.Left = 2.6875F;
            this.txtTotalHoursT.Name = "txtTotalHoursT";
            this.txtTotalHoursT.OutputFormat = resources.GetString("txtTotalHoursT.OutputFormat");
            this.txtTotalHoursT.Style = "text-align: right; vertical-align: top";
            this.txtTotalHoursT.SummaryGroup = "GroupHeader1";
            this.txtTotalHoursT.SummaryRunning = GrapeCity.ActiveReports.SectionReportModel.SummaryRunning.Group;
            this.txtTotalHoursT.SummaryType = GrapeCity.ActiveReports.SectionReportModel.SummaryType.SubTotal;
            this.txtTotalHoursT.Top = 0F;
            this.txtTotalHoursT.Width = 1.375F;
            // 
            // TextBox14
            // 
            this.TextBox14.DataField = "Amount";
            this.TextBox14.Height = 0.2F;
            this.TextBox14.Left = 4.0625F;
            this.TextBox14.Name = "TextBox14";
            this.TextBox14.OutputFormat = resources.GetString("TextBox14.OutputFormat");
            this.TextBox14.Style = "text-align: right; vertical-align: top";
            this.TextBox14.SummaryGroup = "GroupHeader1";
            this.TextBox14.SummaryRunning = GrapeCity.ActiveReports.SectionReportModel.SummaryRunning.Group;
            this.TextBox14.SummaryType = GrapeCity.ActiveReports.SectionReportModel.SummaryType.SubTotal;
            this.TextBox14.Text = "TextBox14";
            this.TextBox14.Top = 0F;
            this.TextBox14.Width = 1.8125F;
            // 
            // ActiveReport1
            // 
            this.MasterReport = false;
            this.PageSettings.PaperHeight = 11F;
            this.PageSettings.PaperWidth = 8.5F;
            this.PrintWidth = 9.15F;
            this.Sections.Add(this.PageHeader);
            this.Sections.Add(this.GroupHeader1);
            this.Sections.Add(this.Detail);
            this.Sections.Add(this.GroupFooter1);
            this.Sections.Add(this.PageFooter);
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule(resources.GetString("$this.StyleSheet"), "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: inherit; font-style: inherit; font-variant: inherit; font-weight: bo" +
                        "ld; font-size: 16pt; font-size-adjust: inherit; font-stretch: inherit", "Heading1", "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: Times New Roman; font-style: italic; font-variant: inherit; font-wei" +
                        "ght: bold; font-size: 14pt; font-size-adjust: inherit; font-stretch: inherit", "Heading2", "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: inherit; font-style: inherit; font-variant: inherit; font-weight: bo" +
                        "ld; font-size: 13pt; font-size-adjust: inherit; font-stretch: inherit", "Heading3", "Normal"));
            ((System.ComponentModel.ISupportInitialize)(this.Label19)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label12)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label13)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label20)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label21)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label22)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TextBox12)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTotalHours)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TextBox10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TextBox15)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TextBox16)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TextBox17)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTotalHoursT)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TextBox14)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();
        }

		#endregion

        private void GroupFooter1_Format(object sender, EventArgs e)
        {
            //txtTotalHoursT.Text = TimeHelper.HoursStringFromMinutes(int.Parse(txtTotalHoursT.Text), false);
        }

        private void Detail_Format(object sender, EventArgs e)
        {
            //txtTotalHours.Text = TimeHelper.HoursStringFromMinutes(int.Parse(txtTotalHours.Text), false);
        }

        private PageHeader PageHeader;
        private GroupHeader GroupHeader1;
        private Label Label19;
        private Label Label12;
        private Label Label13;
        private Label Label20;
        private Label Label21;
        private Label Label22;
        private Detail Detail;
        private TextBox TextBox12;
        private TextBox txtTotalHours;
        private TextBox TextBox10;
        private TextBox TextBox15;
        private TextBox TextBox16;
        private TextBox TextBox17;
        private GroupFooter GroupFooter1;
        private Line Line2;
        private Label Label8;
        private TextBox txtTotalHoursT;
        private TextBox TextBox14;
        private PageFooter PageFooter;
	}
}
