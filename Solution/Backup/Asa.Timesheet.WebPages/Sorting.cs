using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;


namespace Asa.Timesheet.WebPages
{
	public class Sorting
	{
		private const string _begin = "<IMG src='";
		private const string _end = "' border=0>";
		private const string _asc = "images/sup.gif";
		private const string _desc = "images/sdown.gif";

		public static string  SetSort(DataGrid grd,string sCurrent, string sNew)
		{
			string sRetVal;
			bool bAsc=true;
			if(sCurrent!=null && sCurrent!="" && sCurrent.StartsWith(sNew) && !sCurrent.EndsWith("DESC"))
			{
				bAsc = false;
				sRetVal = sNew + " DESC";
			}
			else 
				sRetVal = sNew;
			SetIcon(grd,sNew,bAsc);
			return sRetVal;
		}
		public static void SetIcon(DataGrid grd,string sSort)
		{
			if(sSort!=null)
			{
				int n = sSort.IndexOf(" DESC");
				bool bAsc = n==-1;
				if(n!=-1)
					sSort = sSort.Substring(0,n).Trim();
				SetIcon(grd,sSort,bAsc);
			}
		}
		public static void SetIcon(DataGrid grd,string sSort, bool bAsc)
		{
			ResetPrevious(grd);
			int nColumn = GetColumn(grd,sSort);
			string sIcon = bAsc?_asc:_desc;
			grd.Columns[nColumn].HeaderText =string.Format("{0}{1}{2}{3}",grd.Columns[nColumn].HeaderText,_begin,sIcon,_end);
		}
		public static string GetIconString(string sSort,  bool bAsc)
		{
			string sIcon = bAsc?_asc:_desc;
			return string.Format("{0}{1}{2}{3}",sSort,_begin,sIcon,_end);
	
		}
		public static void ResetPrevious(DataGrid grd)
		{
			for(int i =0;i<grd.Columns.Count;i++)
			{
				string sHeader = grd.Columns[i].HeaderText;
				int n= sHeader.IndexOf(_begin);
				if(n!=-1)
				{
					grd.Columns[i].HeaderText = sHeader.Substring(0,n);
				}
			}
		}
		public static int GetColumn(DataGrid grd,string sSort)
		{
			for(int i =0;i<grd.Columns.Count;i++)
				if(grd.Columns[i].SortExpression==sSort)
					return i;
			return -1;
		}
	}
}
