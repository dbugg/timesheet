using System;
using Asa.Timesheet.Data;
using GrapeCity.ActiveReports;
using GrapeCity.ActiveReports.Controls;
using GrapeCity.ActiveReports.SectionReportModel;
using GrapeCity.ActiveReports.Document.Section;
using GrapeCity.ActiveReports.Document;

namespace Asa.Timesheet.WebPages.Reports.Absence
{
    public class AbsenseRequest : GrapeCity.ActiveReports.SectionReport
	{
		private int nCount_MAX=1000;
        public AbsenseRequest(string sName, DateTime dtStart, DateTime dtEnd, DateTime dtFrom, string sType)
        {
            InitializeComponent();
            txtDate1.Text = DateTime.Now.ToShortDateString();
            int nCount = 1;
            DateTime dtStart1 = dtStart;
            if (dtStart1 < dtEnd)
            {
                while (dtStart1 < dtEnd && nCount < nCount_MAX)
                {
                    dtStart1 = dtStart1.AddDays(1);
                    if (!CalendarDAL.IsHoliday(dtStart1))
                        nCount++;
                }
            }
            txtDays.Text = nCount.ToString();
            if (dtFrom != new DateTime() && dtFrom != Constants.DateMax)
                txtFrom.Text = dtFrom.ToShortDateString();
            txtEndDate.Text = dtEnd.ToShortDateString();
            txtStartDate.Text = dtStart.ToShortDateString();
            txtType.Text = sType;
            txtName.Text = sName;
        }

		#region ActiveReports Designer generated code














































        public void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(AbsenseRequest));
            this.Detail = new GrapeCity.ActiveReports.SectionReportModel.Detail();
            this.Shape1 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
            this.Label1 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Line1 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.Line2 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.Label2 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.txtFrom = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.Label3 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label4 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label10 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label11 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label12 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label13 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label16 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label17 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label18 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Line3 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.Line5 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.Line6 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.Line9 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.Label21 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label22 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label23 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Line12 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.Label24 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label25 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.txtName = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.Line13 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.Label26 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.txtDays = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.Label27 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.txtType = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.Label28 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label29 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.txtStartDate = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.Label30 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.txtEndDate = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.Label31 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label32 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label33 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label34 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Line14 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.txtDate1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.Label35 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.PageHeader = new GrapeCity.ActiveReports.SectionReportModel.PageHeader();
            this.PageFooter = new GrapeCity.ActiveReports.SectionReportModel.PageFooter();
            ((System.ComponentModel.ISupportInitialize)(this.Label1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtFrom)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label11)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label12)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label13)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label16)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label17)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label18)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label21)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label22)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label23)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label24)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label25)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label26)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDays)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label27)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtType)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label28)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label29)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtStartDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label30)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtEndDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label31)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label32)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label33)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label34)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDate1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label35)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            // 
            // Detail
            // 
            this.Detail.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
            this.Shape1,
            this.Label1,
            this.Line1,
            this.Line2,
            this.Label2,
            this.txtFrom,
            this.Label3,
            this.Label4,
            this.Label10,
            this.Label11,
            this.Label12,
            this.Label13,
            this.Label16,
            this.Label17,
            this.Label18,
            this.Line3,
            this.Line5,
            this.Line6,
            this.Line9,
            this.Label21,
            this.Label22,
            this.Label23,
            this.Line12,
            this.Label24,
            this.Label25,
            this.txtName,
            this.Line13,
            this.Label26,
            this.txtDays,
            this.Label27,
            this.txtType,
            this.Label28,
            this.Label29,
            this.txtStartDate,
            this.Label30,
            this.txtEndDate,
            this.Label31,
            this.Label32,
            this.Label33,
            this.Label34,
            this.Line14,
            this.txtDate1,
            this.Label35});
            this.Detail.Height = 3.666667F;
            this.Detail.Name = "Detail";
            // 
            // Shape1
            // 
            this.Shape1.Height = 3.625F;
            this.Shape1.Left = 0F;
            this.Shape1.Name = "Shape1";
            this.Shape1.RoundingRadius = 9.999999F;
            this.Shape1.Top = 0F;
            this.Shape1.Width = 6.5F;
            // 
            // Label1
            // 
            this.Label1.Height = 0.2F;
            this.Label1.HyperLink = null;
            this.Label1.Left = 0F;
            this.Label1.Name = "Label1";
            this.Label1.Style = "font-weight: bold";
            this.Label1.Text = "����������:\r\n";
            this.Label1.Top = 0F;
            this.Label1.Width = 1.25F;
            // 
            // Line1
            // 
            this.Line1.Height = 1.25F;
            this.Line1.Left = 1.5F;
            this.Line1.LineWeight = 1F;
            this.Line1.Name = "Line1";
            this.Line1.Top = 0F;
            this.Line1.Width = 0F;
            this.Line1.X1 = 1.5F;
            this.Line1.X2 = 1.5F;
            this.Line1.Y1 = 0F;
            this.Line1.Y2 = 1.25F;
            // 
            // Line2
            // 
            this.Line2.Height = 0F;
            this.Line2.Left = 0F;
            this.Line2.LineWeight = 1F;
            this.Line2.Name = "Line2";
            this.Line2.Top = 0.5F;
            this.Line2.Width = 3.25F;
            this.Line2.X1 = 3.25F;
            this.Line2.X2 = 0F;
            this.Line2.Y1 = 0.5F;
            this.Line2.Y2 = 0.5F;
            // 
            // Label2
            // 
            this.Label2.Height = 0.2F;
            this.Label2.HyperLink = null;
            this.Label2.Left = 0F;
            this.Label2.Name = "Label2";
            this.Label2.Style = "";
            this.Label2.Text = "�������� ��:\r\n";
            this.Label2.Top = 0.5F;
            this.Label2.Width = 1F;
            // 
            // txtFrom
            // 
            this.txtFrom.Height = 0.2F;
            this.txtFrom.Left = 0F;
            this.txtFrom.Name = "txtFrom";
            this.txtFrom.Text = null;
            this.txtFrom.Top = 0.6875F;
            this.txtFrom.Width = 1.5F;
            // 
            // Label3
            // 
            this.Label3.Height = 0.2F;
            this.Label3.HyperLink = null;
            this.Label3.Left = 0F;
            this.Label3.Name = "Label3";
            this.Label3.Style = "";
            this.Label3.Text = "������� ���. ������";
            this.Label3.Top = 0.875F;
            this.Label3.Width = 1.375F;
            // 
            // Label4
            // 
            this.Label4.Height = 0.2F;
            this.Label4.HyperLink = null;
            this.Label4.Left = 0.125F;
            this.Label4.Name = "Label4";
            this.Label4.Style = "text-align: right";
            this.Label4.Text = "���";
            this.Label4.Top = 1.0625F;
            this.Label4.Width = 1.375F;
            // 
            // Label10
            // 
            this.Label10.Height = 0.2F;
            this.Label10.HyperLink = null;
            this.Label10.Left = 1.5F;
            this.Label10.Name = "Label10";
            this.Label10.Style = "text-align: left";
            this.Label10.Text = "������:";
            this.Label10.Top = 0.25F;
            this.Label10.Width = 1.375F;
            // 
            // Label11
            // 
            this.Label11.Height = 0.2F;
            this.Label11.HyperLink = null;
            this.Label11.Left = 1.5F;
            this.Label11.Name = "Label11";
            this.Label11.Style = "";
            this.Label11.Text = "������:";
            this.Label11.Top = 0.5F;
            this.Label11.Width = 1F;
            // 
            // Label12
            // 
            this.Label12.Height = 0.2F;
            this.Label12.HyperLink = null;
            this.Label12.Left = 1.8125F;
            this.Label12.Name = "Label12";
            this.Label12.Style = "text-align: right";
            this.Label12.Text = "���";
            this.Label12.Top = 0.6875F;
            this.Label12.Width = 1.375F;
            // 
            // Label13
            // 
            this.Label13.Height = 0.2F;
            this.Label13.HyperLink = null;
            this.Label13.Left = 1.5F;
            this.Label13.Name = "Label13";
            this.Label13.Style = "";
            this.Label13.Text = "��� ����� ��";
            this.Label13.Top = 0.875F;
            this.Label13.Width = 1F;
            // 
            // Label16
            // 
            this.Label16.Height = 0.2F;
            this.Label16.HyperLink = null;
            this.Label16.Left = 0F;
            this.Label16.Name = "Label16";
            this.Label16.Style = "";
            this.Label16.Text = "�� ����� �� ��������� �� ���� ��������� ��";
            this.Label16.Top = 1.364583F;
            this.Label16.Width = 3.25F;
            // 
            // Label17
            // 
            this.Label17.Height = 0.2F;
            this.Label17.HyperLink = null;
            this.Label17.Left = 1.875F;
            this.Label17.Name = "Label17";
            this.Label17.Style = "text-align: right";
            this.Label17.Text = "��� � ��������";
            this.Label17.Top = 1.8125F;
            this.Label17.Width = 1.375F;
            // 
            // Label18
            // 
            this.Label18.Height = 0.2F;
            this.Label18.HyperLink = null;
            this.Label18.Left = 0F;
            this.Label18.Name = "Label18";
            this.Label18.Style = "";
            this.Label18.Text = "������ �� ������ �����������";
            this.Label18.Top = 2.125F;
            this.Label18.Width = 3.25F;
            // 
            // Line3
            // 
            this.Line3.Height = 3.625F;
            this.Line3.Left = 3.26F;
            this.Line3.LineWeight = 1F;
            this.Line3.Name = "Line3";
            this.Line3.Top = 0F;
            this.Line3.Width = 0.0004169941F;
            this.Line3.X1 = 3.260417F;
            this.Line3.X2 = 3.26F;
            this.Line3.Y1 = 0F;
            this.Line3.Y2 = 3.625F;
            // 
            // Line5
            // 
            this.Line5.Height = 0F;
            this.Line5.Left = 0F;
            this.Line5.LineWeight = 1F;
            this.Line5.Name = "Line5";
            this.Line5.Top = 0.875F;
            this.Line5.Width = 3.25F;
            this.Line5.X1 = 3.25F;
            this.Line5.X2 = 0F;
            this.Line5.Y1 = 0.875F;
            this.Line5.Y2 = 0.875F;
            // 
            // Line6
            // 
            this.Line6.Height = 0F;
            this.Line6.Left = 0F;
            this.Line6.LineWeight = 1F;
            this.Line6.Name = "Line6";
            this.Line6.Top = 1.25F;
            this.Line6.Width = 3.25F;
            this.Line6.X1 = 3.25F;
            this.Line6.X2 = 0F;
            this.Line6.Y1 = 1.25F;
            this.Line6.Y2 = 1.25F;
            // 
            // Line9
            // 
            this.Line9.Height = 0F;
            this.Line9.Left = 0F;
            this.Line9.LineWeight = 1F;
            this.Line9.Name = "Line9";
            this.Line9.Top = 2.0625F;
            this.Line9.Width = 3.25F;
            this.Line9.X1 = 3.25F;
            this.Line9.X2 = 0F;
            this.Line9.Y1 = 2.0625F;
            this.Line9.Y2 = 2.0625F;
            // 
            // Label21
            // 
            this.Label21.Height = 0.2F;
            this.Label21.HyperLink = null;
            this.Label21.Left = 3.3125F;
            this.Label21.Name = "Label21";
            this.Label21.Style = "text-align: left";
            this.Label21.Text = "��:";
            this.Label21.Top = 0F;
            this.Label21.Width = 0.5625F;
            // 
            // Label22
            // 
            this.Label22.Height = 0.2F;
            this.Label22.HyperLink = null;
            this.Label22.Left = 3.3125F;
            this.Label22.Name = "Label22";
            this.Label22.Style = "text-align: left";
            this.Label22.Text = "................................................................................." +
    "..";
            this.Label22.Top = 0.25F;
            this.Label22.Width = 3.0625F;
            // 
            // Label23
            // 
            this.Label23.Height = 0.2F;
            this.Label23.HyperLink = null;
            this.Label23.Left = 3.3125F;
            this.Label23.Name = "Label23";
            this.Label23.Style = "text-align: left";
            this.Label23.Text = "................................................................................." +
    "..";
            this.Label23.Top = 0.4375F;
            this.Label23.Width = 3.0625F;
            // 
            // Line12
            // 
            this.Line12.Height = 0F;
            this.Line12.Left = 3.256944F;
            this.Line12.LineWeight = 1F;
            this.Line12.Name = "Line12";
            this.Line12.Top = 0.6944444F;
            this.Line12.Width = 3.243056F;
            this.Line12.X1 = 6.5F;
            this.Line12.X2 = 3.256944F;
            this.Line12.Y1 = 0.6944444F;
            this.Line12.Y2 = 0.6944444F;
            // 
            // Label24
            // 
            this.Label24.Height = 0.2F;
            this.Label24.HyperLink = null;
            this.Label24.Left = 4.375F;
            this.Label24.Name = "Label24";
            this.Label24.Style = "font-weight: bold; text-align: center";
            this.Label24.Text = "�����";
            this.Label24.Top = 0.75F;
            this.Label24.Width = 1.25F;
            // 
            // Label25
            // 
            this.Label25.Height = 0.2F;
            this.Label25.HyperLink = null;
            this.Label25.Left = 3.3125F;
            this.Label25.Name = "Label25";
            this.Label25.Style = "text-align: left";
            this.Label25.Text = "��:";
            this.Label25.Top = 1F;
            this.Label25.Width = 2.4375F;
            // 
            // txtName
            // 
            this.txtName.Height = 0.2F;
            this.txtName.Left = 3.3125F;
            this.txtName.Name = "txtName";
            this.txtName.Text = null;
            this.txtName.Top = 1.25F;
            this.txtName.Width = 3.125F;
            // 
            // Line13
            // 
            this.Line13.Height = 0F;
            this.Line13.Left = 3.256944F;
            this.Line13.LineWeight = 1F;
            this.Line13.Name = "Line13";
            this.Line13.Top = 1.506944F;
            this.Line13.Width = 3.243056F;
            this.Line13.X1 = 6.5F;
            this.Line13.X2 = 3.256944F;
            this.Line13.Y1 = 1.506944F;
            this.Line13.Y2 = 1.506944F;
            // 
            // Label26
            // 
            this.Label26.Height = 0.2F;
            this.Label26.HyperLink = null;
            this.Label26.Left = 3.3125F;
            this.Label26.Name = "Label26";
            this.Label26.Style = "text-align: left";
            this.Label26.Text = "���� �� �� ���� ��������";
            this.Label26.Top = 1.625F;
            this.Label26.Width = 2.0625F;
            // 
            // txtDays
            // 
            this.txtDays.Height = 0.2F;
            this.txtDays.Left = 3.3125F;
            this.txtDays.Name = "txtDays";
            this.txtDays.Text = null;
            this.txtDays.Top = 1.875F;
            this.txtDays.Width = 0.1875F;
            // 
            // Label27
            // 
            this.Label27.Height = 0.2F;
            this.Label27.HyperLink = null;
            this.Label27.Left = 3.5625F;
            this.Label27.Name = "Label27";
            this.Label27.Style = "text-align: left";
            this.Label27.Text = "������";
            this.Label27.Top = 1.875F;
            this.Label27.Width = 0.5F;
            // 
            // txtType
            // 
            this.txtType.Height = 0.2F;
            this.txtType.Left = 4.25F;
            this.txtType.Name = "txtType";
            this.txtType.Style = "font-weight: bold";
            this.txtType.Text = null;
            this.txtType.Top = 1.875F;
            this.txtType.Width = 2.1875F;
            // 
            // Label28
            // 
            this.Label28.Height = 0.2F;
            this.Label28.HyperLink = null;
            this.Label28.Left = 3.3125F;
            this.Label28.Name = "Label28";
            this.Label28.Style = "text-align: left; vertical-align: top";
            this.Label28.Text = "��";
            this.Label28.Top = 2.125F;
            this.Label28.Width = 3.125F;
            // 
            // Label29
            // 
            this.Label29.Height = 0.2F;
            this.Label29.HyperLink = null;
            this.Label29.Left = 3.3125F;
            this.Label29.Name = "Label29";
            this.Label29.Style = "text-align: left";
            this.Label29.Text = "������� ��";
            this.Label29.Top = 2.375F;
            this.Label29.Width = 0.75F;
            // 
            // txtStartDate
            // 
            this.txtStartDate.Height = 0.2F;
            this.txtStartDate.Left = 4.0625F;
            this.txtStartDate.Name = "txtStartDate";
            this.txtStartDate.Text = null;
            this.txtStartDate.Top = 2.375F;
            this.txtStartDate.Width = 1F;
            // 
            // Label30
            // 
            this.Label30.Height = 0.2F;
            this.Label30.HyperLink = null;
            this.Label30.Left = 5.0625F;
            this.Label30.Name = "Label30";
            this.Label30.Style = "text-align: left";
            this.Label30.Text = "��";
            this.Label30.Top = 2.375F;
            this.Label30.Width = 0.3125F;
            // 
            // txtEndDate
            // 
            this.txtEndDate.Height = 0.2F;
            this.txtEndDate.Left = 5.4375F;
            this.txtEndDate.Name = "txtEndDate";
            this.txtEndDate.Text = null;
            this.txtEndDate.Top = 2.375F;
            this.txtEndDate.Width = 0.9375F;
            // 
            // Label31
            // 
            this.Label31.Height = 0.2F;
            this.Label31.HyperLink = null;
            this.Label31.Left = 3.3125F;
            this.Label31.Name = "Label31";
            this.Label31.Style = "text-align: left; vertical-align: top";
            this.Label31.Text = "��������� �� �������� � �����.�....��";
            this.Label31.Top = 2.5625F;
            this.Label31.Width = 3.125F;
            // 
            // Label32
            // 
            this.Label32.Height = 0.2F;
            this.Label32.HyperLink = null;
            this.Label32.Left = 3.3125F;
            this.Label32.Name = "Label32";
            this.Label32.Style = "text-align: left";
            this.Label32.Text = "................................................................................." +
    "..";
            this.Label32.Top = 2.8125F;
            this.Label32.Width = 3.0625F;
            // 
            // Label33
            // 
            this.Label33.Height = 0.2F;
            this.Label33.HyperLink = null;
            this.Label33.Left = 3.3125F;
            this.Label33.Name = "Label33";
            this.Label33.Style = "text-align: left";
            this.Label33.Text = "����:";
            this.Label33.Top = 3.3125F;
            this.Label33.Width = 0.5625F;
            // 
            // Label34
            // 
            this.Label34.Height = 0.2F;
            this.Label34.HyperLink = null;
            this.Label34.Left = 5F;
            this.Label34.Name = "Label34";
            this.Label34.Style = "text-align: left";
            this.Label34.Text = "������:";
            this.Label34.Top = 3.3125F;
            this.Label34.Width = 0.5625F;
            // 
            // Line14
            // 
            this.Line14.Height = 0F;
            this.Line14.Left = 3.256944F;
            this.Line14.LineWeight = 1F;
            this.Line14.Name = "Line14";
            this.Line14.Top = 3.188F;
            this.Line14.Width = 3.243056F;
            this.Line14.X1 = 6.5F;
            this.Line14.X2 = 3.256944F;
            this.Line14.Y1 = 3.188F;
            this.Line14.Y2 = 3.188F;
            // 
            // txtDate1
            // 
            this.txtDate1.Height = 0.2F;
            this.txtDate1.Left = 3.875F;
            this.txtDate1.Name = "txtDate1";
            this.txtDate1.Text = null;
            this.txtDate1.Top = 3.3125F;
            this.txtDate1.Width = 0.875F;
            // 
            // Label35
            // 
            this.Label35.Height = 0.2F;
            this.Label35.HyperLink = null;
            this.Label35.Left = 1.8125F;
            this.Label35.Name = "Label35";
            this.Label35.Style = "text-align: right";
            this.Label35.Text = "���";
            this.Label35.Top = 1.0625F;
            this.Label35.Width = 1.375F;
            // 
            // PageHeader
            // 
            this.PageHeader.Height = 0F;
            this.PageHeader.Name = "PageHeader";
            // 
            // PageFooter
            // 
            this.PageFooter.Height = 0F;
            this.PageFooter.Name = "PageFooter";
            // 
            // AbsenseRequest
            // 
            this.MasterReport = false;
            this.PageSettings.PaperHeight = 11F;
            this.PageSettings.PaperWidth = 8.5F;
            this.PrintWidth = 6.645833F;
            this.Sections.Add(this.PageHeader);
            this.Sections.Add(this.Detail);
            this.Sections.Add(this.PageFooter);
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule(resources.GetString("$this.StyleSheet"), "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: inherit; font-style: inherit; font-variant: inherit; font-weight: bo" +
            "ld; font-size: 16pt; font-size-adjust: inherit; font-stretch: inherit", "Heading1", "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: Times New Roman; font-style: italic; font-variant: inherit; font-wei" +
            "ght: bold; font-size: 14pt; font-size-adjust: inherit; font-stretch: inherit", "Heading2", "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: inherit; font-style: inherit; font-variant: inherit; font-weight: bo" +
            "ld; font-size: 13pt; font-size-adjust: inherit; font-stretch: inherit", "Heading3", "Normal"));
            ((System.ComponentModel.ISupportInitialize)(this.Label1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtFrom)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label11)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label12)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label13)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label16)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label17)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label18)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label21)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label22)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label23)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label24)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label25)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label26)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDays)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label27)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtType)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label28)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label29)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtStartDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label30)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtEndDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label31)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label32)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label33)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label34)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDate1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label35)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();

        }

		#endregion

        private PageHeader PageHeader;
        private Detail Detail;
        private Shape Shape1;
        private Label Label1;
        private Line Line1;
        private Line Line2;
        private Label Label2;
        private TextBox txtFrom;
        private Label Label3;
        private Label Label4;
        private Label Label10;
        private Label Label11;
        private Label Label12;
        private Label Label13;
        private Label Label16;
        private Label Label17;
        private Label Label18;
        private Line Line3;
        private Line Line5;
        private Line Line6;
        private Line Line9;
        private Label Label21;
        private Label Label22;
        private Label Label23;
        private Line Line12;
        private Label Label24;
        private Label Label25;
        private TextBox txtName;
        private Line Line13;
        private Label Label26;
        private TextBox txtDays;
        private Label Label27;
        private TextBox txtType;
        private Label Label28;
        private Label Label29;
        private TextBox txtStartDate;
        private Label Label30;
        private TextBox txtEndDate;
        private Label Label31;
        private Label Label32;
        private Label Label33;
        private Label Label34;
        private Line Line14;
        private TextBox txtDate1;
        private Label Label35;
        private PageFooter PageFooter;
	}
}
