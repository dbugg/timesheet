using System;
using System.Data;
using System.Data.SqlClient; 
using System.Collections;
using Microsoft.ApplicationBlocks.Data;

namespace Asa.Timesheet.Data
{
	public class ClientDAL
	{
		/// <summary>
		/// Loads a ClientData object from the database using the given ClientID
		/// </summary>
		/// <param name="clientID">The primary key value</param>
		/// <returns>A ClientData object</returns>
		public static ClientData Load(int clientID)
		{
			SqlParameter[] parameterValues = new SqlParameter[] { new SqlParameter("@ClientID", clientID) };
			using (SqlDataReader reader = SqlHelper.ExecuteReader(CommonDAL.ConnectionString, CommandType.StoredProcedure, "ClientsSelProc", parameterValues))
			{
				if (reader.Read())
				{
					ClientData result = new ClientData();
					LoadFromReader(reader,result);
					return result;
				}
				else
					return null;
			}
		}

		/// <summary>
		/// Loads a ClientData object from the database using the given where clause
		/// </summary>
		/// <param name="whereClause">The filter expression for the query</param>
		/// <returns>A ClientData object</returns>
		public static ClientData Load(string whereClause)
		{
			SqlParameter[] parameterValues = new SqlParameter[] { new SqlParameter("@WhereClause", whereClause) };
			using (SqlDataReader reader = SqlHelper.ExecuteReader(CommonDAL.ConnectionString, CommandType.StoredProcedure, "ClientsListProc", parameterValues))
			{
				if (reader.Read())
				{
					ClientData result = new ClientData();
					LoadFromReader(reader,result);
					return result;
				}
				else
					return null;
			}
		}
		public static int Save( ClientData source)
		{
			if (source.ClientID == -1)
				return Insert(source);
			else
				return Update(source);	
		}

		public static void Delete( ClientData source)
		{
			Delete(source.ClientID);
		}
		/// <summary>
		/// Loads a collection of ClientData objects from the database.
		/// </summary>
		/// <returns>A collection containing all of the ClientData objects in the database.</returns>
		public static ClientsVector LoadCollection()
		{
			SqlParameter[] parms = new SqlParameter[] {};
			return LoadCollection("ClientsListProc", parms);
		}
	  
		/// <summary>
		/// Saves a collection of ClientData objects to the database.
		/// </summary>
		/// <returns>A collection containing all of the ClientData objects in the database.</returns>
		public static  void  SaveCollection(ClientsVector col,string spName, SqlParameter[] parms)
		{
			ClientsVector colOld = LoadCollection(spName,parms);
			UpdateCollection(colOld,col);
		}
		public static  void  SaveCollection(ClientsVector col)
		{
			ClientsVector colOld = LoadCollection();
			UpdateCollection(colOld,col);
		}
		public static  void  SaveCollection(ClientsVector col, string whereClause )
		{
			ClientsVector colOld = LoadCollection(whereClause);
			UpdateCollection(colOld,col);
		}
		/// <summary>
		/// Loads a collection of ClientData objects from the database.
		/// </summary>
		/// <returns>A collection containing all of the ClientData objects in the database.</returns>
		public static ClientsVector LoadCollection(string whereClause)
		{
			SqlParameter[] parms = new SqlParameter[] { new SqlParameter("@WhereClause", whereClause) };
			return LoadCollection("ClientsListProc", parms);
		}

		/// <summary>
		/// Loads a collection of ClientData objects from the database.
		/// </summary>
		/// <returns>A collection containing all of the ClientData objects in the database.</returns>
		public static ClientsVector LoadCollection(string spName, SqlParameter[] parms)
		{
			ClientsVector result = new ClientsVector();
			using (SqlDataReader reader = SqlHelper.ExecuteReader(CommonDAL.ConnectionString, CommandType.StoredProcedure, spName, parms))
			{
				while (reader.Read())
				{
					ClientData tmp = new ClientData();
					LoadFromReader(reader, tmp);
					result.Add(tmp);
				}
			}
			return result;
		}
		/// <summary>
		/// Deletes a ClientData object from the database.
		/// </summary>
		/// <param name="clientID">The primary key value</param>
		public static void Delete(int clientID)
		{
			SqlParameter[] parameterValues = new SqlParameter[] { new SqlParameter("@ClientID", clientID) };
			SqlHelper.ExecuteNonQuery(CommonDAL.ConnectionString, CommandType.StoredProcedure, "ClientsInactiveProc", parameterValues);
		}

		public static void LoadFromReader(SqlDataReader reader, ClientData target)
		{
			if (reader != null && !reader.IsClosed)
			{
				//target.Key = reader.GetInt32(0);
				if (!reader.IsDBNull(0)) target.ClientID = reader.GetInt32(0);
				if (!reader.IsDBNull(1)) target.ClientName = reader.GetString(1);
				if (!reader.IsDBNull(2)) target.City = reader.GetString(2);
				if (!reader.IsDBNull(3)) target.Address = reader.GetString(3);
				if (!reader.IsDBNull(4)) target.Email = reader.GetString(4);
				if (!reader.IsDBNull(5)) target.IsActive = reader.GetBoolean(5);
				if (!reader.IsDBNull(6)) target.Fax = reader.GetString(6);
				
				if (!reader.IsDBNull(7)) target.Manager = reader.GetString(7);
				//if (!reader.IsDBNull(8)) target.ManagerTitleID = reader.GetInt32(8);
				if (!reader.IsDBNull(8)) target.Representative1 = reader.GetString(8);
				if (!reader.IsDBNull(9)) target.Representative2 = reader.GetString(9);
				if (!reader.IsDBNull(10)) target.Delo = reader.GetString(10);
				if (!reader.IsDBNull(11)) target.Bulstat = reader.GetString(11);
				if (!reader.IsDBNull(12)) target.NDR = reader.GetString(12);
				if (!reader.IsDBNull(13)) target.Phone = reader.GetString(13);
				if (!reader.IsDBNull(14)) target.Webpage = reader.GetString(14);
				if (!reader.IsDBNull(15)) target.Phone1 = reader.GetString(15);
				if (!reader.IsDBNull(16)) target.Email1 = reader.GetString(16);
				if (!reader.IsDBNull(17)) target.Phone2 = reader.GetString(17);
				if (!reader.IsDBNull(18)) target.Email2 = reader.GetString(18);
				if (!reader.IsDBNull(19)) target.Status = reader.GetInt32(19);
			}
		}
		
	  
		public static int Insert( ClientData source)
		{
			SqlParameter[] parameterValues = GetInsertParameterValues(source);
			SqlHelper.ExecuteNonQuery(CommonDAL.ConnectionString, CommandType.StoredProcedure, "ClientsInsProc", parameterValues);
			source.ClientID = (int) parameterValues[0].Value;
			return source.ClientID;
		}

		public static int Update( ClientData source)
		{
			SqlParameter[] parameterValues = GetUpdateParameterValues(source);
			SqlHelper.ExecuteNonQuery(CommonDAL.ConnectionString, CommandType.StoredProcedure,
				"ClientsUpdProc", parameterValues);
			return source.ClientID;
		}
		private static void UpdateCollection(ClientsVector colOld,ClientsVector col)
		{
			// Delete all old items
			foreach( ClientData itemOld in colOld)
			{
				if(!ExistsKey(itemOld.ClientID, col))
					Delete(itemOld.ClientID);
			}
			foreach( ClientData item in col)
			{
				if(item.ClientID <= -1)
					Insert(item);
				else
					Update(item);
			}
		}
		private static bool ExistsKey(int Key, ClientsVector col)
		{
			foreach( ClientData item in col)
				if(item.ClientID==Key)
					return true;
			return false;
		}

		private static SqlParameter GetSqlParameter(string name, ParameterDirection direction, object value)
		{
			SqlParameter p = new SqlParameter(name, value);
			p.Direction = direction;
			return p;
		}

		private static SqlParameter[] GetInsertParameterValues( ClientData source)
		{
			SqlParameter[] prams = new SqlParameter[21];
			prams[0] = GetSqlParameter("@ClientID", ParameterDirection.Output, source.ClientID);
			prams[1] = GetSqlParameter("@ClientName", ParameterDirection.Input, source.ClientName);
			prams[2] = GetSqlParameter("@City", ParameterDirection.Input, source.City);
			prams[3] = GetSqlParameter("@Address", ParameterDirection.Input, source.Address);
			prams[4] = GetSqlParameter("@Email", ParameterDirection.Input, source.Email);
			prams[5] = GetSqlParameter("@Fax", ParameterDirection.Input, source.Fax);
			prams[6] = GetSqlParameter("@IsActive", ParameterDirection.Input, source.IsActive);
			prams[7] = GetSqlParameter("@Manager", ParameterDirection.Input, source.Manager);
			prams[8] = GetSqlParameter("@ManagerTitleID", ParameterDirection.Input, source.ManagerTitleID);
			prams[9] = GetSqlParameter("@Representative1", ParameterDirection.Input, source.Representative1);
			prams[10] = GetSqlParameter("@Representative2", ParameterDirection.Input, source.Representative2);
			prams[11] = GetSqlParameter("@Delo", ParameterDirection.Input, source.Delo);
			prams[12] = GetSqlParameter("@Bulstat", ParameterDirection.Input, source.Bulstat);
			prams[13] = GetSqlParameter("@NDR", ParameterDirection.Input, source.NDR);
			prams[14] = GetSqlParameter("@Phone", ParameterDirection.Input, source.Phone);
			prams[15] = GetSqlParameter("@Webpage", ParameterDirection.Input, source.Webpage);
			prams[16] = GetSqlParameter("@Phone1", ParameterDirection.Input, source.Phone1);
			prams[17] = GetSqlParameter("@Email1", ParameterDirection.Input, source.Email1);
			prams[18] = GetSqlParameter("@Phone2", ParameterDirection.Input, source.Phone2);
			prams[19] = GetSqlParameter("@Email2", ParameterDirection.Input, source.Email2);
			prams[20] = GetSqlParameter("@Status", ParameterDirection.Input, source.Status);
			return prams;
		}

		private static SqlParameter[] GetUpdateParameterValues( ClientData source)
		{
			return new SqlParameter[] {
										  new SqlParameter("@ClientID", source.ClientID),
										  new SqlParameter("@ClientName", source.ClientName),
										  new SqlParameter("@City", source.City),
										  new SqlParameter("@Address", source.Address),
										  new SqlParameter("@Email", source.Email),
										  new SqlParameter("@Fax", source.Fax),
										  new SqlParameter("@IsActive", source.IsActive),
										  new SqlParameter("@Manager", source.Manager),
										  new SqlParameter("@ManagerTitleID", source.ManagerTitleID),
										  new SqlParameter("@Representative1", source.Representative1),
										  new SqlParameter("@Representative2", source.Representative2),
										  new SqlParameter("@Delo", source.Delo),
										  new SqlParameter("@Bulstat", source.Bulstat),
										  new SqlParameter("@NDR", source.NDR),
										  new SqlParameter("@Phone", source.Phone),
										  new SqlParameter("@Webpage", source.Webpage),
										  new SqlParameter("@Phone1", source.Phone1),
										  new SqlParameter("@Email1", source.Email1),
										  new SqlParameter("@Phone2", source.Phone2),
										  new SqlParameter("@Email2", source.Email2),
										  new SqlParameter("@Status", source.Status)
									  };
		}
	}
}   

