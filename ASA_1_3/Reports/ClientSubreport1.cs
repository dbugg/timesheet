using System;
using System.Data;
using Asa.Timesheet.Data;
using GrapeCity.ActiveReports;
using GrapeCity.ActiveReports.Controls;
using GrapeCity.ActiveReports.SectionReportModel;
using GrapeCity.ActiveReports.Document.Section;
using GrapeCity.ActiveReports.Document;

namespace Asa.Timesheet.WebPages.Reports
{
    public class ClientSubreport1 : GrapeCity.ActiveReports.SectionReport
	{
		private decimal _EURpayd = 0;
		private decimal _EURnopayd = 0;
        public ClientSubreport1(DataView dv)
        {


            InitializeComponent();
            this.DataSource = dv;


        }
		private bool _forPDF=true;
        private void Detail_Format(object sender, System.EventArgs eArgs)
        {
            decimal eur = UIHelpers.ParseDecimal(this.TextBox3.Text);
            if (this.TextBox5.Text == Resource.ResourceManager["Reports_Yes"])
                _EURpayd += eur;
            else
                _EURnopayd += eur;
        }


        private void ReportFooter_Format(object sender, System.EventArgs eArgs)
        {
            decimal fix = decimal.Parse(System.Configuration.ConfigurationManager.AppSettings["fixedBGNEUR"]);
            this.txtPaydEUR.Text = _EURpayd.ToString("#,##0");
            decimal bgn = _EURpayd * fix;
            this.txtPaydBGN.Text = bgn.ToString("#,##0");
            this.txtNoPaydEUR.Text = _EURnopayd.ToString("#,##0");
            bgn = _EURnopayd * fix;
            this.txtNOPaydBGN.Text = bgn.ToString("#,##0");
            bgn = _EURnopayd + _EURpayd;
            this.txtAllEUR.Text = bgn.ToString("#,##0");
            bgn = bgn * fix;
            this.txtAllBGN.Text = bgn.ToString("#,##0");
        }

        private void GroupFooter1_Format(object sender, System.EventArgs eArgs)
        {
            this.TextBox8.Text = string.Concat(this.Label8.Text, " ", this.TextBox8.Text);
        }

		#region ActiveReports Designer generated code




































        public void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ClientSubreport1));
            this.Detail = new GrapeCity.ActiveReports.SectionReportModel.Detail();
            this.ReportHeader = new GrapeCity.ActiveReports.SectionReportModel.ReportHeader();
            this.ReportFooter = new GrapeCity.ActiveReports.SectionReportModel.ReportFooter();
            this.GroupHeader1 = new GrapeCity.ActiveReports.SectionReportModel.GroupHeader();
            this.GroupFooter1 = new GrapeCity.ActiveReports.SectionReportModel.GroupFooter();
            this.Label1 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label3 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Line1 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.Label9 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label10 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label11 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label12 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label16 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.TextBox5 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.TextBox9 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.TextBox4 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.TextBox3 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.TextBox1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.TextBox10 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.TextBox12 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.Line4 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.TextBox8 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.TextBox6 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.Label8 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Line2 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.TextBox11 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.Line3 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.txtPaydEUR = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.Label13 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.txtPaydBGN = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtNoPaydEUR = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.Label14 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.txtNOPaydBGN = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtAllEUR = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.Label15 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.txtAllBGN = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            ((System.ComponentModel.ISupportInitialize)(this.Label1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label11)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label12)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label16)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TextBox5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TextBox9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TextBox4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TextBox3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TextBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TextBox10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TextBox12)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TextBox8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TextBox6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TextBox11)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPaydEUR)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label13)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPaydBGN)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtNoPaydEUR)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label14)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtNOPaydBGN)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAllEUR)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label15)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAllBGN)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            // 
            // Detail
            // 
            this.Detail.CanShrink = true;
            this.Detail.ColumnSpacing = 0F;
            this.Detail.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
						this.TextBox5,
						this.TextBox9,
						this.TextBox4,
						this.TextBox3,
						this.TextBox1,
						this.TextBox10,
						this.TextBox12,
						this.Line4});
            this.Detail.Height = 0.2506945F;
            this.Detail.Name = "Detail";
            // 
            // ReportHeader
            // 
            this.ReportHeader.Height = 0F;
            this.ReportHeader.Name = "ReportHeader";
            // 
            // ReportFooter
            // 
            this.ReportFooter.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
						this.Line3,
						this.txtPaydEUR,
						this.Label13,
						this.txtPaydBGN,
						this.txtNoPaydEUR,
						this.Label14,
						this.txtNOPaydBGN,
						this.txtAllEUR,
						this.Label15,
						this.txtAllBGN});
            this.ReportFooter.Height = 0.6666667F;
            this.ReportFooter.Name = "ReportFooter";
            // 
            // GroupHeader1
            // 
            this.GroupHeader1.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
						this.Label1,
						this.Label3,
						this.Line1,
						this.Label9,
						this.Label10,
						this.Label11,
						this.Label12,
						this.Label16});
            this.GroupHeader1.DataField = "SubprojectID";
            this.GroupHeader1.Height = 0.3534722F;
            this.GroupHeader1.Name = "GroupHeader1";
            // 
            // GroupFooter1
            // 
            this.GroupFooter1.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
						this.TextBox8,
						this.TextBox6,
						this.Label8,
						this.Line2,
						this.TextBox11});
            this.GroupFooter1.Height = 0.2909722F;
            this.GroupFooter1.Name = "GroupFooter1";
            // 
            // Label1
            // 
            this.Label1.Height = 0.2F;
            this.Label1.HyperLink = null;
            this.Label1.Left = 0F;
            this.Label1.Name = "Label1";
            this.Label1.Style = "font-weight: bold";
            this.Label1.Text = "??????";
            this.Label1.Top = 0.0625F;
            this.Label1.Width = 2.5F;
            // 
            // Label3
            // 
            this.Label3.Height = 0.2F;
            this.Label3.HyperLink = null;
            this.Label3.Left = 4.5625F;
            this.Label3.Name = "Label3";
            this.Label3.Style = "font-weight: bold; text-align: right";
            this.Label3.Text = "???? (EUR)";
            this.Label3.Top = 0.0625F;
            this.Label3.Width = 1F;
            // 
            // Line1
            // 
            this.Line1.Height = 0F;
            this.Line1.Left = 0F;
            this.Line1.LineWeight = 1F;
            this.Line1.Name = "Line1";
            this.Line1.Top = 0.3125F;
            this.Line1.Width = 9.1875F;
            this.Line1.X1 = 0F;
            this.Line1.X2 = 9.1875F;
            this.Line1.Y1 = 0.3125F;
            this.Line1.Y2 = 0.3125F;
            // 
            // Label9
            // 
            this.Label9.Height = 0.2F;
            this.Label9.HyperLink = null;
            this.Label9.Left = 6.5F;
            this.Label9.Name = "Label9";
            this.Label9.Style = "font-weight: bold; text-align: right";
            this.Label9.Text = "???? ???????";
            this.Label9.Top = 0.0625F;
            this.Label9.Width = 1.0625F;
            // 
            // Label10
            // 
            this.Label10.Height = 0.2F;
            this.Label10.HyperLink = null;
            this.Label10.Left = 8.3125F;
            this.Label10.Name = "Label10";
            this.Label10.Style = "font-weight: bold; text-align: right";
            this.Label10.Text = "???????";
            this.Label10.Top = 0.0625F;
            this.Label10.Width = 0.812F;
            // 
            // Label11
            // 
            this.Label11.Height = 0.2F;
            this.Label11.HyperLink = null;
            this.Label11.Left = 7.5625F;
            this.Label11.Name = "Label11";
            this.Label11.Style = "font-weight: bold; text-align: right";
            this.Label11.Text = "???????";
            this.Label11.Top = 0.0625F;
            this.Label11.Width = 0.75F;
            // 
            // Label12
            // 
            this.Label12.Height = 0.2F;
            this.Label12.HyperLink = null;
            this.Label12.Left = 5.5625F;
            this.Label12.Name = "Label12";
            this.Label12.Style = "font-weight: bold; text-align: right";
            this.Label12.Text = "???? (??.)";
            this.Label12.Top = 0.0625F;
            this.Label12.Width = 0.9375F;
            // 
            // Label16
            // 
            this.Label16.Height = 0.2F;
            this.Label16.HyperLink = null;
            this.Label16.Left = 2.5F;
            this.Label16.Name = "Label16";
            this.Label16.Style = "font-weight: bold";
            this.Label16.Text = "??????";
            this.Label16.Top = 0.0625F;
            this.Label16.Width = 2.063F;
            // 
            // TextBox5
            // 
            this.TextBox5.DataField = "DoneString";
            this.TextBox5.Height = 0.2F;
            this.TextBox5.Left = 8.3125F;
            this.TextBox5.Name = "TextBox5";
            this.TextBox5.Style = "text-align: right; ddo-char-set: 204";
            this.TextBox5.Text = "TextBox5";
            this.TextBox5.Top = 0F;
            this.TextBox5.Width = 0.812F;
            // 
            // TextBox9
            // 
            this.TextBox9.DataField = "PaymentPercent";
            this.TextBox9.Height = 0.2F;
            this.TextBox9.Left = 7.5625F;
            this.TextBox9.Name = "TextBox9";
            this.TextBox9.OutputFormat = resources.GetString("TextBox9.OutputFormat");
            this.TextBox9.Style = "text-align: right";
            this.TextBox9.Text = "TextBox9";
            this.TextBox9.Top = 0F;
            this.TextBox9.Width = 0.75F;
            // 
            // TextBox4
            // 
            this.TextBox4.DataField = "PaymentDate";
            this.TextBox4.Height = 0.2F;
            this.TextBox4.Left = 6.5F;
            this.TextBox4.Name = "TextBox4";
            this.TextBox4.OutputFormat = resources.GetString("TextBox4.OutputFormat");
            this.TextBox4.Style = "text-align: right";
            this.TextBox4.Text = "TextBox4";
            this.TextBox4.Top = 0F;
            this.TextBox4.Width = 1.0625F;
            // 
            // TextBox3
            // 
            this.TextBox3.DataField = "Amount";
            this.TextBox3.Height = 0.2F;
            this.TextBox3.Left = 4.5625F;
            this.TextBox3.Name = "TextBox3";
            this.TextBox3.OutputFormat = resources.GetString("TextBox3.OutputFormat");
            this.TextBox3.Style = "text-align: right";
            this.TextBox3.Text = "TextBox3";
            this.TextBox3.Top = 0F;
            this.TextBox3.Width = 1F;
            // 
            // TextBox1
            // 
            this.TextBox1.DataField = "projectname";
            this.TextBox1.Height = 0.2F;
            this.TextBox1.Left = 0F;
            this.TextBox1.Name = "TextBox1";
            this.TextBox1.Text = "TextBox1";
            this.TextBox1.Top = 0.02083333F;
            this.TextBox1.Width = 2.5F;
            // 
            // TextBox10
            // 
            this.TextBox10.DataField = "AmountBGN";
            this.TextBox10.Height = 0.2F;
            this.TextBox10.Left = 5.563F;
            this.TextBox10.Name = "TextBox10";
            this.TextBox10.OutputFormat = resources.GetString("TextBox10.OutputFormat");
            this.TextBox10.Style = "text-align: right";
            this.TextBox10.Text = "TextBox10";
            this.TextBox10.Top = 0F;
            this.TextBox10.Width = 0.938F;
            // 
            // TextBox12
            // 
            this.TextBox12.DataField = "ClientName";
            this.TextBox12.Height = 0.2F;
            this.TextBox12.Left = 2.5F;
            this.TextBox12.Name = "TextBox12";
            this.TextBox12.Text = "TextBox12";
            this.TextBox12.Top = 0.021F;
            this.TextBox12.Width = 2.063F;
            // 
            // Line4
            // 
            this.Line4.Height = 0F;
            this.Line4.Left = 0F;
            this.Line4.LineWeight = 1F;
            this.Line4.Name = "Line4";
            this.Line4.Top = 0.25F;
            this.Line4.Width = 9.1875F;
            this.Line4.X1 = 0F;
            this.Line4.X2 = 9.1875F;
            this.Line4.Y1 = 0.25F;
            this.Line4.Y2 = 0.25F;
            // 
            // TextBox8
            // 
            this.TextBox8.DataField = "projectname";
            this.TextBox8.Height = 0.2F;
            this.TextBox8.Left = 0F;
            this.TextBox8.Name = "TextBox8";
            this.TextBox8.Top = 0F;
            this.TextBox8.Width = 4.563F;
            // 
            // TextBox6
            // 
            this.TextBox6.DataField = "Amount";
            this.TextBox6.Height = 0.2F;
            this.TextBox6.Left = 4.5625F;
            this.TextBox6.Name = "TextBox6";
            this.TextBox6.OutputFormat = resources.GetString("TextBox6.OutputFormat");
            this.TextBox6.Style = "text-align: right";
            this.TextBox6.SummaryGroup = "GroupHeader1";
            this.TextBox6.SummaryRunning = GrapeCity.ActiveReports.SectionReportModel.SummaryRunning.Group;
            this.TextBox6.SummaryType = GrapeCity.ActiveReports.SectionReportModel.SummaryType.SubTotal;
            this.TextBox6.Top = 0F;
            this.TextBox6.Width = 1F;
            // 
            // Label8
            // 
            this.Label8.Height = 0.2F;
            this.Label8.HyperLink = null;
            this.Label8.Left = 0F;
            this.Label8.Name = "Label8";
            this.Label8.Style = "";
            this.Label8.Text = "???? ?? ";
            this.Label8.Top = 0F;
            this.Label8.Visible = false;
            this.Label8.Width = 0.625F;
            // 
            // Line2
            // 
            this.Line2.Height = 0F;
            this.Line2.Left = 0.006944444F;
            this.Line2.LineWeight = 1F;
            this.Line2.Name = "Line2";
            this.Line2.Top = 0F;
            this.Line2.Width = 9.1875F;
            this.Line2.X1 = 0.006944444F;
            this.Line2.X2 = 9.194445F;
            this.Line2.Y1 = 0F;
            this.Line2.Y2 = 0F;
            // 
            // TextBox11
            // 
            this.TextBox11.DataField = "AmountBGN";
            this.TextBox11.Height = 0.2F;
            this.TextBox11.Left = 5.5625F;
            this.TextBox11.Name = "TextBox11";
            this.TextBox11.OutputFormat = resources.GetString("TextBox11.OutputFormat");
            this.TextBox11.Style = "text-align: right";
            this.TextBox11.SummaryGroup = "GroupHeader1";
            this.TextBox11.SummaryRunning = GrapeCity.ActiveReports.SectionReportModel.SummaryRunning.Group;
            this.TextBox11.SummaryType = GrapeCity.ActiveReports.SectionReportModel.SummaryType.SubTotal;
            this.TextBox11.Top = 0F;
            this.TextBox11.Width = 0.9375F;
            // 
            // Line3
            // 
            this.Line3.Height = 0F;
            this.Line3.Left = 0F;
            this.Line3.LineWeight = 1F;
            this.Line3.Name = "Line3";
            this.Line3.Top = 0.0625F;
            this.Line3.Width = 9.1875F;
            this.Line3.X1 = 0F;
            this.Line3.X2 = 9.1875F;
            this.Line3.Y1 = 0.0625F;
            this.Line3.Y2 = 0.0625F;
            // 
            // txtPaydEUR
            // 
            this.txtPaydEUR.DataField = "Amount";
            this.txtPaydEUR.Height = 0.2F;
            this.txtPaydEUR.Left = 4.5625F;
            this.txtPaydEUR.Name = "txtPaydEUR";
            this.txtPaydEUR.OutputFormat = resources.GetString("txtPaydEUR.OutputFormat");
            this.txtPaydEUR.Style = "text-align: right";
            this.txtPaydEUR.Top = 0.0625F;
            this.txtPaydEUR.Width = 1F;
            // 
            // Label13
            // 
            this.Label13.Height = 0.2F;
            this.Label13.HyperLink = null;
            this.Label13.Left = 0F;
            this.Label13.Name = "Label13";
            this.Label13.Style = "";
            this.Label13.Text = "???? ???????";
            this.Label13.Top = 0.0625F;
            this.Label13.Width = 4.5625F;
            // 
            // txtPaydBGN
            // 
            this.txtPaydBGN.DataField = "AmountBGN";
            this.txtPaydBGN.Height = 0.2F;
            this.txtPaydBGN.Left = 5.5625F;
            this.txtPaydBGN.Name = "txtPaydBGN";
            this.txtPaydBGN.OutputFormat = resources.GetString("txtPaydBGN.OutputFormat");
            this.txtPaydBGN.Style = "text-align: right";
            this.txtPaydBGN.Top = 0.0625F;
            this.txtPaydBGN.Width = 0.9375F;
            // 
            // txtNoPaydEUR
            // 
            this.txtNoPaydEUR.DataField = "Amount";
            this.txtNoPaydEUR.Height = 0.2F;
            this.txtNoPaydEUR.Left = 4.5625F;
            this.txtNoPaydEUR.Name = "txtNoPaydEUR";
            this.txtNoPaydEUR.OutputFormat = resources.GetString("txtNoPaydEUR.OutputFormat");
            this.txtNoPaydEUR.Style = "text-align: right";
            this.txtNoPaydEUR.Top = 0.25F;
            this.txtNoPaydEUR.Width = 1F;
            // 
            // Label14
            // 
            this.Label14.Height = 0.2F;
            this.Label14.HyperLink = null;
            this.Label14.Left = 0F;
            this.Label14.Name = "Label14";
            this.Label14.Style = "";
            this.Label14.Text = "???? ?????????";
            this.Label14.Top = 0.25F;
            this.Label14.Width = 4.5625F;
            // 
            // txtNOPaydBGN
            // 
            this.txtNOPaydBGN.DataField = "AmountBGN";
            this.txtNOPaydBGN.Height = 0.2F;
            this.txtNOPaydBGN.Left = 5.5625F;
            this.txtNOPaydBGN.Name = "txtNOPaydBGN";
            this.txtNOPaydBGN.OutputFormat = resources.GetString("txtNOPaydBGN.OutputFormat");
            this.txtNOPaydBGN.Style = "text-align: right";
            this.txtNOPaydBGN.Top = 0.25F;
            this.txtNOPaydBGN.Width = 0.9375F;
            // 
            // txtAllEUR
            // 
            this.txtAllEUR.DataField = "Amount";
            this.txtAllEUR.Height = 0.2F;
            this.txtAllEUR.Left = 4.5625F;
            this.txtAllEUR.Name = "txtAllEUR";
            this.txtAllEUR.OutputFormat = resources.GetString("txtAllEUR.OutputFormat");
            this.txtAllEUR.Style = "text-align: right";
            this.txtAllEUR.Top = 0.4375F;
            this.txtAllEUR.Width = 1F;
            // 
            // Label15
            // 
            this.Label15.Height = 0.2F;
            this.Label15.HyperLink = null;
            this.Label15.Left = 0F;
            this.Label15.Name = "Label15";
            this.Label15.Style = "";
            this.Label15.Text = "????";
            this.Label15.Top = 0.4375F;
            this.Label15.Width = 4.5625F;
            // 
            // txtAllBGN
            // 
            this.txtAllBGN.DataField = "AmountBGN";
            this.txtAllBGN.Height = 0.2F;
            this.txtAllBGN.Left = 5.5625F;
            this.txtAllBGN.Name = "txtAllBGN";
            this.txtAllBGN.OutputFormat = resources.GetString("txtAllBGN.OutputFormat");
            this.txtAllBGN.Style = "text-align: right";
            this.txtAllBGN.Top = 0.4375F;
            this.txtAllBGN.Width = 0.9375F;
            // 
            // ActiveReport1
            // 
            this.MasterReport = false;
            this.PageSettings.PaperHeight = 11F;
            this.PageSettings.PaperWidth = 8.5F;
            this.PrintWidth = 9.125F;
            this.Sections.Add(this.ReportHeader);
            this.Sections.Add(this.GroupHeader1);
            this.Sections.Add(this.Detail);
            this.Sections.Add(this.GroupFooter1);
            this.Sections.Add(this.ReportFooter);
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule(resources.GetString("$this.StyleSheet"), "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: inherit; font-style: inherit; font-variant: inherit; font-weight: bo" +
                        "ld; font-size: 16pt; font-size-adjust: inherit; font-stretch: inherit", "Heading1", "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: Times New Roman; font-style: italic; font-variant: inherit; font-wei" +
                        "ght: bold; font-size: 14pt; font-size-adjust: inherit; font-stretch: inherit", "Heading2", "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: inherit; font-style: inherit; font-variant: inherit; font-weight: bo" +
                        "ld; font-size: 13pt; font-size-adjust: inherit; font-stretch: inherit", "Heading3", "Normal"));
            ((System.ComponentModel.ISupportInitialize)(this.Label1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label11)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label12)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label16)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TextBox5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TextBox9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TextBox4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TextBox3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TextBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TextBox10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TextBox12)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TextBox8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TextBox6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TextBox11)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPaydEUR)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label13)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPaydBGN)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtNoPaydEUR)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label14)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtNOPaydBGN)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAllEUR)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label15)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAllBGN)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();

            // Attach Report Events
            this.Detail.Format += new System.EventHandler(this.Detail_Format);
            this.ReportFooter.Format += new System.EventHandler(this.ReportFooter_Format);
            this.GroupFooter1.Format += new System.EventHandler(this.GroupFooter1_Format);
        }

		#endregion

        private void SubAnalysisRpt_ReportEnd(object sender, EventArgs e)
        {
            if (!this._forPDF) return;

            Logo1 logo = new Logo1();
            logo.Run();

            for (int i = 0; i < this.Document.Pages.Count; i++)
                this.Document.Pages[i].Overlay(logo.Document.Pages[0]);
        }

        private void SubAnalysisRpt_ReportStart(object sender, EventArgs e)
        {
            //			if (!_forPDF)
            //			{
            //				this.PageSettings.Margins.Left = CmToInch(2f);
            //				this.PrintWidth = CmToInch(17f);
            //				Label1.Location = new System.Drawing.PointF(0, Label1.Location.Y);
            //				lbTitle
            //				// lblObekt.Width = CmToInch(6f);
            ////				txtObekt.Location = new System.Drawing.PointF(lblObekt.Location.X+lblObekt.Width, txtObekt.Location.Y);
            ////				txtAddress.Location = new System.Drawing.PointF(txtObekt.Location.X, txtAddress.Location.Y);
            ////
            ////				this.SubReport1.Location = new System.Drawing.PointF(0, this.SubReport1.Location.Y);
            //		}
        }

        private ReportHeader ReportHeader;
        private GroupHeader GroupHeader1;
        private Label Label1;
        private Label Label3;
        private Line Line1;
        private Label Label9;
        private Label Label10;
        private Label Label11;
        private Label Label12;
        private Label Label16;
        private Detail Detail;
        private TextBox TextBox5;
        private TextBox TextBox9;
        private TextBox TextBox4;
        private TextBox TextBox3;
        private TextBox TextBox1;
        private TextBox TextBox10;
        private TextBox TextBox12;
        private Line Line4;
        private GroupFooter GroupFooter1;
        private TextBox TextBox8;
        private TextBox TextBox6;
        private Label Label8;
        private Line Line2;
        private TextBox TextBox11;
        private ReportFooter ReportFooter;
        private Line Line3;
        private TextBox txtPaydEUR;
        private Label Label13;
        private TextBox txtPaydBGN;
        private TextBox txtNoPaydEUR;
        private Label Label14;
        private TextBox txtNOPaydBGN;
        private TextBox txtAllEUR;
        private Label Label15;
        private TextBox txtAllBGN;
	}
}
