﻿<%@ Register TagPrefix="uc1" TagName="PageHeader" Src="UserControls/PageHeader.ascx" %>
<%@ Page language="c#" Codebehind="Buildings.aspx.cs" AutoEventWireup="false" Inherits="Asa.Timesheet.WebPages.Buildings" smartNavigation="True"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
	<head runat="server">
		<title>Buildings</title>
		<meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
		<meta content="C#" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<LINK href="styles/timesheet.css" type="text/css" rel="stylesheet">
	</HEAD>
	<body bottomMargin="0" leftMargin="0" topMargin="0" rightMargin="0">
		<form id="Form1" method="post" runat="server">
			<table height="100%" cellSpacing="0" cellPadding="0" width="100%" border="0">
				<TBODY>
					<tr height="1">
						<td><uc1:pageheader id="header" runat="server"></uc1:pageheader></td>
					</tr>
					<tr>
						<td>
							<table height="100%" cellSpacing="0" cellPadding="0" width="100%" border="0">
								<TBODY>
									<tr>
										<td class="menuLeft"><asp:placeholder id="menuHolder" runat="server"></asp:placeholder></td>
										
										<td style="PADDING-LEFT: 120px; PADDING-RIGHT: 5px; PADDING-TOP: 3px" vAlign="top" noWrap><br>
											<asp:label id="lblError" runat="server" CssClass="ErrorLabel" ForeColor="Red" EnableViewState="False"></asp:label><asp:label id="lblInfo" runat="server" CssClass="InfoLabel" EnableViewState="False"></asp:label><br>
											<br>
											<TABLE id="table1" height="56" cellSpacing="0" cellPadding="0" width="720" border="0">
												<TBODY>
													<TR>
													</TR>
													<TR>
														<td style="WIDTH: 900px" align="center"><asp:label id="lblTitle" runat="server" CssClass="PageCaption" ForeColor="SaddleBrown" Text="ПОДОБЕКТИ"
																Font-Size="16px"></asp:label></TD>
													</TR>
													<tr>
														<td><asp:label id="lbPhases" runat="server" ForeColor="SaddleBrown" Text="Фаза:"></asp:label><asp:dropdownlist id="ddlPhases" Width="200px" AutoPostBack="True" Runat="server"></asp:dropdownlist></td>
													</tr>
												</TBODY>
											</TABLE>
											<asp:datalist id="dlBuildings" runat="server" RepeatDirection="Horizontal" RepeatColumns="1">
												<EditItemStyle VerticalAlign="Top"></EditItemStyle>
												<ItemStyle VerticalAlign="Top"></ItemStyle>
												<ItemTemplate>
													<p align="center">
														<asp:Label CssClass="PageCaption" runat="server" Text=' <%# "Подобект " +((int)DataBinder.Eval(Container, "ItemIndex")+1).ToString() %>' ID="Label1" NAME="Label1">
														</asp:Label></p>
													<asp:Label CssClass="PageCaption" runat="server" Text='Име на подобект: ' ID="Label2" NAME="Label1"></asp:Label>
													<asp:DropDownList Runat="server" ID="txtS1" AutoPostBack="true"></asp:DropDownList>
													<asp:DropDownList Runat="server" ID="txtS2" AutoPostBack="true"></asp:DropDownList>
													<asp:TextBox runat="server" ID="txtS" Visible="True"></asp:TextBox>
													<br>
													<asp:linkbutton id="btnContents" runat="server"  Font-Names="Verdana" Font-Size="X-Small"
														Font-Underline="True">Съдържание</asp:linkbutton>&nbsp;
													<asp:imagebutton id="img" runat="server" ImageUrl="images/word.gif" ToolTip="Съдържание"></asp:imagebutton>&nbsp;
													<asp:imagebutton id="btnContentsExcel" runat="server" ImageUrl="images/excel_ico2.gif" ToolTip="Съдържание"></asp:imagebutton><br>
													<asp:linkbutton id="btnTech" runat="server"  Font-Names="Verdana" Font-Size="X-Small"
														Font-Underline="True">Техн.-икономически. показатели</asp:linkbutton>
													<asp:imagebutton id="imgTechWord" runat="server" ImageUrl="images/word.gif" ToolTip="Съдържание"></asp:imagebutton>&nbsp;
													<asp:imagebutton id="imgTechExcel" runat="server" ImageUrl="images/excel_ico2.gif" ToolTip="Съдържание"></asp:imagebutton>
													<hr color="Silver">
													<asp:dropdownlist id="ddlContents" runat="server" CssClass="enterDataBox" Width="190px"></asp:dropdownlist>
													<asp:button id="btnAdd" runat="server" CssClass="ActionButton" Text="Добави"></asp:button>
													<asp:button id="btnRefresh" runat="server" CssClass="ActionButton" Text="Преизчисли"></asp:button>
													<asp:button id="btnMarkAll" runat="server" CssClass="ActionButton" Text="Маркирай"></asp:button>
													<asp:button id="btnUnmarkAll" runat="server" CssClass="ActionButton" Text="Размаркирай"></asp:button>
													<table id="Table4" CssClass="GridListLabel" runat="server">
														<tr>
															<td style="PADDING-LEFT: 3px;">
																<asp:Button ID="btnPlusAll" Runat="server" CssClass="ActionButton" Text="+" Width="20px"></asp:Button>
																<asp:Button ID="btnMinusAll" Runat="server" CssClass="ActionButton" Text="-" Width="20px"></asp:Button>
															</td>
														</tr>
													</table>
													<asp:datalist id="dlBuildingContentArchitect" runat="server" RepeatDirection="Horizontal" RepeatColumns="1">
														<EditItemStyle VerticalAlign="Top"></EditItemStyle>
														<ItemStyle VerticalAlign="Top"></ItemStyle>
														<ItemTemplate>
															<table id="tbBeforeGrid" CssClass="GridListLabel" runat="server">
																<tr>
																	<td width="55px">
																		<asp:Button ID="btnPlusMultiple" Runat="server" CssClass="ActionButton" Text="+" Width="20px"></asp:Button>
																	</td>
																	<td>
																		<asp:Label ID="lbGridName" Runat="server" Text="Разпределение" Width="200px" CssClass="GridListLabel"></asp:Label>
																	</td>
																	<td>
																		<asp:Label ID="lbGridNameCount" Runat="server" Text="Брой на чертежи:" CssClass="GridListLabel"
																			Width="150px"></asp:Label>
																	</td>
																	<td>
																		<asp:Label ID="lbPercentFinished" Runat="server" Text="% на изпълнение" CssClass="GridListLabel"
																			Width="200px"></asp:Label>
																	</td>
																</tr>
															</table>
															<table id="Table3" runat="server" CssClass="GridListLabel">
																<tr>
																	<td valign="top" width="25px">
																		<asp:Button ID="btnMinusMultiple" Runat="server" CssClass="ActionButton" Text="-" Width="20px"></asp:Button>
																	</td>
																	<td>
																		<asp:datagrid id="grdDistr" runat="server" CssClass="Grid" Width="750px" CellPadding="4" AutoGenerateColumns="False"
																			AllowSorting="true" Visible="false">
																			<HeaderStyle CssClass="GridHeader1"></HeaderStyle>
																			<Columns>
																				<asp:BoundColumn Visible="false" DataField="ContentTypeID"></asp:BoundColumn>
																				<asp:TemplateColumn HeaderStyle-Width="20px" ItemStyle-HorizontalAlign="Center">
																					<ItemTemplate>
																						<asp:CheckBox ID="cbReportCode" Runat="server" Checked='<%#  DataBinder.Eval(Container, "DataItem.IsChecked") %>' >
																						</asp:CheckBox>
																					</ItemTemplate>
																				</asp:TemplateColumn>
																				<asp:TemplateColumn HeaderText="Код" ItemStyle-HorizontalAlign="Left" HeaderStyle-Width="82px" ItemStyle-Wrap="False">
																					<ItemTemplate>
																						<asp:Label ID="lbOzn" Runat="server" Width="80px" Text='<%# DataBinder.Eval(Container, "DataItem.ContentCode") %>' Font-Bold="true">
																						</asp:Label>
																					</ItemTemplate>
																				</asp:TemplateColumn>
																				<asp:BoundColumn DataField="ContentType" HeaderText="Разпределение" HeaderStyle-Width="140px"></asp:BoundColumn>
																				<asp:TemplateColumn HeaderText="Кота" HeaderStyle-Width="65px" ItemStyle-HorizontalAlign="Center">
																					<ItemTemplate>
																						<asp:TextBox id=txtKota runat="server" Width="61px" Text='<%# Asa.Timesheet.WebPages.UIHelpers.FormatDecimal2((decimal)DataBinder.Eval(Container, "DataItem.Kota")) %>'>
																						</asp:TextBox>
																					</ItemTemplate>
																				</asp:TemplateColumn>
																				<asp:TemplateColumn HeaderText="ЗП Кв.метра" HeaderStyle-Width="85px" ItemStyle-HorizontalAlign="Center">
																					<ItemTemplate>
																						<asp:TextBox id=txtA runat="server" Width="61px" Text='<%# Asa.Timesheet.WebPages.UIHelpers.FormatDecimal2((decimal)DataBinder.Eval(Container, "DataItem.Area")) %>' >
																						</asp:TextBox>
																					</ItemTemplate>
																				</asp:TemplateColumn>
																				<asp:TemplateColumn HeaderText="Мащаб" HeaderStyle-Width="65px" ItemStyle-HorizontalAlign="Center">
																					<ItemTemplate>
																						<asp:TextBox id="txtMa" runat="server" Width="61px" Text='<%# DataBinder.Eval(Container, "DataItem.Scale") %>'>
																						</asp:TextBox>
																					</ItemTemplate>
																				</asp:TemplateColumn>
																				<asp:TemplateColumn HeaderText="Ревизия" ItemStyle-HorizontalAlign="Center" HeaderStyle-Width="55px">
																					<ItemTemplate>
																						<asp:TextBox ID="txtRevision" Runat="server" Width="32px" Text='<%# DataBinder.Eval(Container, "DataItem.ContentRevision") %>' Visible="false">
																						</asp:TextBox>
																						<asp:DropDownList ID="ddlRevision" Runat="server" Width="50px"></asp:DropDownList>
																					</ItemTemplate>
																				</asp:TemplateColumn>
																				<asp:TemplateColumn HeaderText="Доп. инфо" ItemStyle-HorizontalAlign="Center" HeaderStyle-Width="55px">
																					<ItemTemplate>
																						<asp:DropDownList ID="ddlOther" Runat="server" Width="80px"></asp:DropDownList>
																					</ItemTemplate>
																				</asp:TemplateColumn>
																				<asp:TemplateColumn HeaderStyle-Width="18px" ItemStyle-HorizontalAlign="Center">
																					<ItemTemplate>
																						<asp:ImageButton ID="imgArrowUp" runat="server" Width="12px" Height="12px" ImageUrl="images/arrow_up.jpg"
																							ToolTip="Нагоре" CommandName="Up"></asp:ImageButton>
																					</ItemTemplate>
																				</asp:TemplateColumn>
																				<asp:TemplateColumn HeaderStyle-Width="18px" ItemStyle-HorizontalAlign="Center">
																					<ItemTemplate>
																						<asp:ImageButton ID="imgArrowDown" runat="server" Width="12px" Height="12px" ImageUrl="images/arrow_down.jpg"
																							ToolTip="Надолу" CommandName="Down"></asp:ImageButton>
																					</ItemTemplate>
																				</asp:TemplateColumn>
																				<asp:TemplateColumn HeaderText="Изпълнение" HeaderStyle-Width="80px" ItemStyle-HorizontalAlign="Center">
																					<ItemTemplate>
																						<asp:DropDownList ID="ddlContentFinishedPhase1" Runat="server" Width="60px" Visible="false"></asp:DropDownList>
																						<asp:DropDownList ID="ddlContentFinishedPhase2" Runat="server" Width="60px" Visible="false"></asp:DropDownList>
																						<asp:DropDownList ID="ddlContentFinishedPhase3" Runat="server" Width="60px" Visible="false"></asp:DropDownList>
																						<asp:DropDownList ID="ddlContentFinishedPhase4" Runat="server" Width="60px" Visible="false"></asp:DropDownList>
																					</ItemTemplate>
																				</asp:TemplateColumn>
																				<asp:ButtonColumn Text="&lt;img border=0 alt='Изтрий' src='images/delete.gif'&gt;" CommandName="Delete"></asp:ButtonColumn>
																				<asp:BoundColumn DataField="BuildingNumber" HeaderText="Number" Visible="false"></asp:BoundColumn>
																			</Columns>
																		</asp:datagrid>
																	</td>
																</tr>
																<tr>
																	<td valign="top" width="25px">
																	</td>
																	<td>
																		<asp:datagrid id="grdContentArchitect" runat="server" CssClass="Grid" Width="750px" CellPadding="4"
																			AutoGenerateColumns="False" AllowSorting="true">
																			<HeaderStyle CssClass="GridHeader1"></HeaderStyle>
																			<Columns>
																				<asp:BoundColumn Visible="false" DataField="ContentTypeID"></asp:BoundColumn>
																				<asp:TemplateColumn HeaderStyle-Width="20px" ItemStyle-HorizontalAlign="Center">
																					<ItemTemplate>
																						<asp:CheckBox ID="cbReportCode" runat="server" Checked='<%#  DataBinder.Eval(Container, "DataItem.IsChecked") %>'>
																						</asp:CheckBox>
																					</ItemTemplate>
																				</asp:TemplateColumn>
																				<asp:TemplateColumn HeaderText="Код" ItemStyle-HorizontalAlign="Left" HeaderStyle-Width="82px" ItemStyle-Wrap="False">
																					<ItemTemplate>
																						<asp:Label ID="lbOzn" Runat="server" Width="80px" Text='<%# DataBinder.Eval(Container, "DataItem.ContentCode") %>' Font-Bold="true">
																						</asp:Label>
																					</ItemTemplate>
																				</asp:TemplateColumn>
																				<asp:BoundColumn DataField="ContentType" HeaderText="Съдържание" HeaderStyle-Width="140px"></asp:BoundColumn>
																				<asp:TemplateColumn HeaderText="Текст" HeaderStyle-Width="160px" ItemStyle-HorizontalAlign="Center">
																					<ItemTemplate>
																						<asp:TextBox id="txtT" runat="server" Width="120px" Text='<%# DataBinder.Eval(Container, "DataItem.ContentText") %>'>
																						</asp:TextBox>
																					</ItemTemplate>
																				</asp:TemplateColumn>
																				<asp:TemplateColumn HeaderText="Ниво" HeaderStyle-Width="160px" ItemStyle-HorizontalAlign="Center">
																					<ItemTemplate>
																						<asp:TextBox id="txtLevel" runat="server" Width="60px" Text='<%# DataBinder.Eval(Container, "DataItem.ContentLevel") %>'>
																						</asp:TextBox>
																					</ItemTemplate>
																				</asp:TemplateColumn>
																				<asp:TemplateColumn HeaderText="Мащаб" HeaderStyle-Width="65px" ItemStyle-HorizontalAlign="Center">
																					<ItemTemplate>
																						<asp:TextBox id="txtMa" runat="server" Width="61px" Text='<%# DataBinder.Eval(Container, "DataItem.Scale") %>'>
																						</asp:TextBox>
																					</ItemTemplate>
																				</asp:TemplateColumn>
																				<asp:TemplateColumn HeaderText="Ревизия" ItemStyle-HorizontalAlign="Center" HeaderStyle-Width="55px">
																					<ItemTemplate>
																						<asp:TextBox ID="txtRevision" Runat="server" Width="32px" Text='<%# DataBinder.Eval(Container, "DataItem.ContentRevision") %>' Visible="false">
																						</asp:TextBox>
																						<asp:DropDownList ID="ddlRevision" Runat="server" Width="50px"></asp:DropDownList>
																					</ItemTemplate>
																				</asp:TemplateColumn>
																				<asp:TemplateColumn HeaderText="Доп. инфо" ItemStyle-HorizontalAlign="Center" HeaderStyle-Width="55px">
																					<ItemTemplate>
																						<asp:DropDownList ID="ddlOther" Runat="server" Width="80px"></asp:DropDownList>
																					</ItemTemplate>
																				</asp:TemplateColumn>
																				<asp:TemplateColumn HeaderStyle-Width="18px" ItemStyle-HorizontalAlign="Center">
																					<ItemTemplate>
																						<asp:ImageButton ID="iArrowUp" runat="server" Width="12px" Height="12px" ImageUrl="images/arrow_up.jpg"
																							ToolTip="Нагоре" ImageAlign="left" CommandName="Up"></asp:ImageButton>
																					</ItemTemplate>
																				</asp:TemplateColumn>
																				<asp:TemplateColumn HeaderStyle-Width="18px" ItemStyle-HorizontalAlign="Center">
																					<ItemTemplate>
																						<asp:ImageButton ID="iArrowDown" runat="server" Width="12px" Height="12px" ImageUrl="images/arrow_down.jpg"
																							ToolTip="Надолу" ImageAlign="Right" CommandName="Down"></asp:ImageButton>
																					</ItemTemplate>
																				</asp:TemplateColumn>
																				<asp:TemplateColumn HeaderText="Изпълнение" HeaderStyle-Width="80px" ItemStyle-HorizontalAlign="Center">
																					<ItemTemplate>
																						<asp:DropDownList ID="ddlContentFinishedPhase1" Runat="server" Width="60px" Visible="false"></asp:DropDownList>
																						<asp:DropDownList ID="ddlContentFinishedPhase2" Runat="server" Width="60px" Visible="false"></asp:DropDownList>
																						<asp:DropDownList ID="ddlContentFinishedPhase3" Runat="server" Width="60px" Visible="false"></asp:DropDownList>
																						<asp:DropDownList ID="ddlContentFinishedPhase4" Runat="server" Width="60px" Visible="false"></asp:DropDownList>
																					</ItemTemplate>
																				</asp:TemplateColumn>
																				<asp:ButtonColumn Text="&lt;img border=0 alt='Изтрий' src='images/delete.gif'&gt;" CommandName="Delete"></asp:ButtonColumn>
																				<asp:BoundColumn DataField="BuildingNumber" HeaderText="Number" Visible="false"></asp:BoundColumn>
																			</Columns>
																		</asp:datagrid>
																	</td>
																</tr>
															</table>
														</ItemTemplate>
													</asp:datalist>
													<asp:dropdownlist id="ddlContentsConstructors" runat="server" CssClass="enterDataBox" Width="220px"></asp:dropdownlist>
													<asp:button id="btnAddConstructorsOnly" runat="server" CssClass="ActionButton" Text="Добави"></asp:button>
													<asp:datagrid id="grdNew" runat="server" CssClass="Grid" Width="750px" CellPadding="4" AutoGenerateColumns="False"
														AllowSorting="true">
														<HeaderStyle CssClass="GridHeader1"></HeaderStyle>
														<Columns>
															<asp:BoundColumn Visible="false" DataField="ContentTypeID"></asp:BoundColumn>
															<asp:TemplateColumn HeaderStyle-Width="20px" ItemStyle-HorizontalAlign="Center">
																<ItemTemplate>
																	<asp:CheckBox ID="cbReportCode" Runat="server" Checked='<%#  DataBinder.Eval(Container, "DataItem.IsChecked") %>' >
																	</asp:CheckBox>
																</ItemTemplate>
															</asp:TemplateColumn>
															<asp:BoundColumn DataField="ContentType" HeaderText="Съдържание" HeaderStyle-Width="140px"></asp:BoundColumn>
															<asp:TemplateColumn HeaderText="Текст" HeaderStyle-Width="160px" ItemStyle-HorizontalAlign="Center">
																<ItemTemplate>
																	<asp:TextBox id="txtT" runat="server" Width="130px" Text='<%# DataBinder.Eval(Container, "DataItem.ContentText") %>'>
																	</asp:TextBox>
																</ItemTemplate>
															</asp:TemplateColumn>
															<asp:TemplateColumn HeaderText="Ниво" HeaderStyle-Width="160px" ItemStyle-HorizontalAlign="Center">
																<ItemTemplate>
																	<asp:TextBox id="txtLevel" runat="server" Width="60px" Text='<%# DataBinder.Eval(Container, "DataItem.ContentLevel") %>'>
																	</asp:TextBox>
																</ItemTemplate>
															</asp:TemplateColumn>
															<asp:TemplateColumn HeaderText="Мащаб" HeaderStyle-Width="65px" ItemStyle-HorizontalAlign="Center">
																<ItemTemplate>
																	<asp:TextBox id="txtMa" runat="server" Width="61px" Text='<%# DataBinder.Eval(Container, "DataItem.Scale") %>'>
																	</asp:TextBox>
																</ItemTemplate>
															</asp:TemplateColumn>
															<asp:TemplateColumn HeaderText="Код" ItemStyle-HorizontalAlign="left" HeaderStyle-Width="82px" ItemStyle-Wrap="False">
																<ItemTemplate>
																	<asp:Label ID="lbOzn" Runat="server" Width="80px" Text='<%# DataBinder.Eval(Container, "DataItem.ContentCode") %>' Font-Bold="true">
																	</asp:Label>
																</ItemTemplate>
															</asp:TemplateColumn>
															<asp:TemplateColumn HeaderText="Ревизия" ItemStyle-HorizontalAlign="Center" HeaderStyle-Width="55px">
																<ItemTemplate>
																	<asp:TextBox ID="txtRevision" Runat="server" Width="32px" Text='<%# DataBinder.Eval(Container, "DataItem.ContentRevision") %>' Visible="false">
																	</asp:TextBox>
																	<asp:DropDownList ID="ddlRevision" Runat="server" Width="50px"></asp:DropDownList>
																</ItemTemplate>
															</asp:TemplateColumn>
															<asp:TemplateColumn HeaderText="Доп. инфо" ItemStyle-HorizontalAlign="Center" HeaderStyle-Width="55px">
																<ItemTemplate>
																	<asp:DropDownList ID="ddlOther" Runat="server" Width="80px"></asp:DropDownList>
																</ItemTemplate>
															</asp:TemplateColumn>
															<asp:TemplateColumn HeaderStyle-Width="18px" ItemStyle-HorizontalAlign="Center">
																<ItemTemplate>
																	<asp:ImageButton ID="iArrowUpNew" runat="server" Width="12px" Height="12px" ImageUrl="images/arrow_up.jpg"
																		ToolTip="Нагоре" ImageAlign="left" CommandName="Up"></asp:ImageButton>
																</ItemTemplate>
															</asp:TemplateColumn>
															<asp:TemplateColumn HeaderStyle-Width="18px" ItemStyle-HorizontalAlign="Center">
																<ItemTemplate>
																	<asp:ImageButton ID="iArrowDownNew" runat="server" Width="12px" Height="12px" ImageUrl="images/arrow_down.jpg"
																		ToolTip="Надолу" ImageAlign="Right" CommandName="Down"></asp:ImageButton>
																</ItemTemplate>
															</asp:TemplateColumn>
															<asp:TemplateColumn HeaderText="Изпълнение" HeaderStyle-Width="80px" ItemStyle-HorizontalAlign="Center">
																<ItemTemplate>
																	<asp:DropDownList ID="ddlContentFinishedPhase1" Runat="server" Width="60px" Visible="false"></asp:DropDownList>
																	<asp:DropDownList ID="ddlContentFinishedPhase2" Runat="server" Width="60px" Visible="false"></asp:DropDownList>
																	<asp:DropDownList ID="ddlContentFinishedPhase3" Runat="server" Width="60px" Visible="false"></asp:DropDownList>
																	<asp:DropDownList ID="ddlContentFinishedPhase4" Runat="server" Width="60px" Visible="false"></asp:DropDownList>
																</ItemTemplate>
															</asp:TemplateColumn>
															<asp:ButtonColumn Text="&lt;img border=0 alt='Изтрий' src='images/delete.gif'&gt;" CommandName="Delete"></asp:ButtonColumn>
															<asp:BoundColumn DataField="BuildingNumber" HeaderText="Number" Visible="false"></asp:BoundColumn>
														</Columns>
													</asp:datagrid>
													<asp:Label Runat="server" ID="lblTotal"></asp:Label>
													<asp:button id="btnMakeCurrentOzn" runat="server" CssClass="ActionButton" Text="Генерирай кодове"
														Width="150px"></asp:button>&nbsp;
													<asp:button id="btnCanselCurrentOzn" runat="server" CssClass="ActionButton" Text="Изтрий кодове"
														Width="150px"></asp:button>&nbsp;
													<br>
													<br>
													<br>
												</ItemTemplate>
											</asp:datalist>
											<TABLE id="Table5" style="WIDTH: 650px; HEIGHT: 18px" cellSpacing="0" cellPadding="0" border="0">
												<TBODY>
													<tr>
														<td align="left"><asp:checkbox id="cbCopyLastBuilding" Text="Копирай последния подобект" Runat="server"></asp:checkbox></td>
													</tr>
													<tr>
														<td><br>
														</td>
													</tr>
													<TR>
														<td align="left"><asp:button id="btnNewBuilding" runat="server" CssClass="ActionButton" EnableViewState="False"
																Text="Нов подобект" Width="200px"></asp:button>&nbsp;
															<asp:button id="btnSave" runat="server" CssClass="ActionButton" EnableViewState="False" Text="Запиши"
																Width="200px"></asp:button>&nbsp;
															<asp:button id="btnCancel" runat="server" CssClass="ActionButton" EnableViewState="False" Text="Връщане към проекта"
																Width="200px"></asp:button></TD>
													</TR>
													<tr>
														<td><br>
														</td>
													</tr>
													<tr>
														<td align="left"><asp:button id="btnDeleteSelected" runat="server" CssClass="ActionButton" Text="Изтрий избраното съдържание"
																Width="250px"></asp:button></td>
													</tr>
												</TBODY>
											</TABLE>
										</td>
									</tr>
								</TBODY>
							</table>
							<br>
							<br>
							<br>
						</td>
					</tr>
				</TBODY>
			</table>
		</form>
		</TD></TR></TBODY></TABLE></TD></TR></TBODY></TABLE></TD></TR></TBODY></TABLE></FORM>
	</body>
</HTML>
