//=============================================================================
// System  : ASP.NET Common Web Page Classes
// File    : ResSrvHandler.cs
// Author  : Eric Woodruff  (Eric@EWoodruff.us)
// Updated : 03/10/2006
// Compiler: Microsoft Visual C#
//
// This file contains a derived System.Web.IHttpHandler class that acts as a
// resource server to send resources to the client browser such as scripts,
// images, etc.
//
// Add the following to the <system.web> section of the Web.Config file in
// applications that make use of this custom control library.
//
//  <httpHandlers>
//      <!-- EWSoftware.Web namespace resource server handler -->
//      <add verb="*" path="EWSoftware.Web.aspx"
//           type="EWSoftware.Web.ResSrvHandler, EWSoftware.Web" />
//  </httpHandlers>
//
// This code may be used in compiled form in any way you desire.  This
// file may be redistributed unmodified by any means PROVIDING it is not
// sold for profit without the author's written consent, and providing
// that this notice and the author's name and all copyright notices
// remain intact.
//
// This code is provided "as is" with no warranty either express or
// implied.  The author accepts no liability for any damage or loss of
// business that this product may cause.
//
// Version     Date     Who  Comments
// ============================================================================
// 1.0.0.0  04/07/2003  EFW  Created the code
//=============================================================================

using System;
using System.IO;
using System.Reflection;
using System.Web;

// All classes go in the EWSoftware.Web namespace
namespace Asa.Timesheet.Data
{
	/// <summary>
	/// This is a derived <see cref="System.Web.IHttpHandler"/> class that
	/// acts as a resource server to send resources to the client browser
	/// such as scripts, images, etc.
	/// </summary>
	/// <remarks>This allows resources to be embedded in the control assembly
	/// so that they do not have to be distributed and installed separately.
	/// For more information about how this class works, see
	/// <a href="http://www.codeproject.com/aspnet/ResSrvPage.asp">
	/// A Resource Server Handler Class For Custom Controls</a> at
	/// <b>The Code Project</b>.</remarks>
	public class ResSrvHandler : System.Web.IHttpHandler
	{
		/// <summary>
		/// The ASPX page name that will cause requests to get routed
		/// to this handler.  The default value is <b>EWSoftware.Web.aspx</b>.
		/// </summary>
		public const string ResSrvHandlerPageName = "EWSoftware.Web.aspx";

		/// <summary>
		/// The path to the script resources.  The default value is
		/// "<b>EWSoftware.Web.Scripts.</b>".
		/// </summary>
		private const string ScriptResPath = "EWSoftware.Web.Scripts.";

		/// <summary>
		/// This property is used to indicate that the object instance can
		/// be used by other requests.  It always returns true.
		/// </summary>
		public bool IsReusable
		{
			get { return true; }
		}

		/// <summary>
		/// Load the resource specified in the query string and return it
		/// as the HTTP response.
		/// </summary>
		/// <param name="context">The context object for the request</param>
		public void ProcessRequest(HttpContext context)
		{
			Assembly asm;
			StreamReader sr = null;
			string resourceName;

			// Get the resource name and base the type on the extension
			resourceName = context.Request.QueryString["Res"];

			try
			{
				context.Response.Clear();

				// If caching is not disabled, set the cache parameters so that
				// the response is cached on the client for up to one day.
				if(context.Request.QueryString["NoCache"] == null)
				{
					context.Response.Cache.SetExpires(DateTime.Now.AddDays(1));
					context.Response.Cache.SetCacheability(HttpCacheability.Public);
					context.Response.Cache.SetValidUntilExpires(false);

					// Vary by parameter name.  Note that if you have more
					// than one, add additional lines to specify them.
					context.Response.Cache.VaryByParams["Res"] = true;
				}
				else
				{
					// The response is not cached
					context.Response.Cache.SetExpires(DateTime.Now.AddDays(-1));
					context.Response.Cache.SetCacheability(HttpCacheability.NoCache);
				}

				context.Response.ContentType = "text/javascript";
				asm = Assembly.GetExecutingAssembly();
				sr = new StreamReader(asm.GetManifestResourceStream(
					ResSrvHandler.ScriptResPath + resourceName));
				context.Response.Write(sr.ReadToEnd());
			}
			catch(Exception excp)
			{
				string msg = excp.Message.Replace("\r\n", " ");

				context.Response.Clear();
				context.Response.Cache.SetExpires(DateTime.Now.AddDays(-1));
				context.Response.Cache.SetCacheability(HttpCacheability.NoCache);

				// For script, write out an alert describing the problem.
				context.Response.ContentType = "text/javascript";
				context.Response.Write("alert(\"Could not load resource '" +
					resourceName + "': " + msg + "\");");
			}
			finally
			{
				if(sr != null)
					sr.Close();
			}
		}
	}
}
