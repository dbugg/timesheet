<%@ Page language="c#" Codebehind="Subcontracters.aspx.cs" AutoEventWireup="false" Inherits="Asa.Timesheet.WebPages.Subcontracters" %>
<%@ Register TagPrefix="uc1" TagName="PageHeader" Src="UserControls/PageHeader.ascx" %>
<%@ Register TagPrefix="radg" Namespace="Telerik.WebControls" Assembly="RadGrid" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
	<HEAD>
		<title>Subcontracters</title>
		<meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
		<meta content="C#" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<LINK href="styles/timesheet.css" type="text/css" rel="stylesheet">
	</HEAD>
	<body bottomMargin="0" leftMargin="0" topMargin="0" rightMargin="0">
		<form id="Form1" method="post" runat="server">
			<table height="100%" cellSpacing="0" cellPadding="0" width="100%" border="0">
				<tr height="1">
					<td>
						<uc1:PageHeader id="header" runat="server"></uc1:PageHeader>
					</td>
				</tr>
				<TR height="1">
					<td noWrap background="images/line.gif" height="1"></td>
				</TR>
				<tr>
					<td>
						<table height="100%" cellSpacing="0" cellPadding="0" width="100%" border="0">
							<tr>
								<td style="BACKGROUND-IMAGE: url(images/left110.gif)" vAlign="top" noWrap width="110"><asp:placeholder id="menuHolder" runat="server"></asp:placeholder></td>
								<td noWrap width="1" bgColor="lightgrey"></td>
								<td style="PADDING-RIGHT: 5px; PADDING-LEFT: 5px; PADDING-TOP: 3px" vAlign="top" noWrap><br>
									<asp:label id="lblError" runat="server" ForeColor="Red" Visible="False" EnableViewState="False"
										CssClass="ErrorLabel"></asp:label><asp:label id="lblInfo" runat="server" Visible="False" EnableViewState="False" CssClass="InfoLabel"></asp:label><br>
									<br>
									<asp:panel id="grid" style="BORDER-RIGHT: #d2b48c 2px solid; PADDING-RIGHT: 2px; BORDER-TOP: #d2b48c 2px solid; OVERFLOW-Y: scroll; PADDING-LEFT: 2px; OVERFLOW-X: auto; BORDER-LEFT: #d2b48c 2px solid; PADDING-TOP: 2px; BORDER-BOTTOM: #d2b48c 2px solid"
										runat="server" Height="450px" Width="100%">
										<asp:datagrid id="grdClients" runat="server" CssClass="Grid" Width="100%" AllowSorting="True"
											AutoGenerateColumns="False" CellPadding="4" BackColor="Transparent">
											<AlternatingItemStyle CssClass="GridAltItem" BackColor="WhiteSmoke"></AlternatingItemStyle>
											<ItemStyle CssClass="GridItem"></ItemStyle>
											<HeaderStyle CssClass="GridHeader"></HeaderStyle>
											<Columns>
											<asp:TemplateColumn HeaderText="#">
													<ItemStyle Width="10px" ForeColor="DimGray"></ItemStyle>
													<ItemTemplate>
														<asp:Label  CssClass="menuTable" runat="server" Text='<%# (int)DataBinder.Eval(Container, "ItemIndex")+1 %>' ID="Label1" NAME="Label1">
														</asp:Label>
													</ItemTemplate>
												</asp:TemplateColumn>
												<asp:TemplateColumn HeaderText="Име" SortExpression="Name">
													<ItemTemplate>
														<asp:LinkButton id="lnk" runat="server" CssClass="menuTable" ForeColor="#804000" CommandName="Edit">
															<span Class="menuTable">
																<%# DataBinder.Eval(Container, "DataItem.SubcontracterName") %>
															</span>
														</asp:LinkButton>
													</ItemTemplate>
												</asp:TemplateColumn>
												<asp:BoundColumn DataField="SubcontracterType" SortExpression="Type" HeaderText="Тип">
													<HeaderStyle Width="50px"></HeaderStyle>
													<ItemStyle ForeColor="DimGray"></ItemStyle>
												</asp:BoundColumn>
												<asp:BoundColumn DataField="Address" SortExpression="Address" HeaderText="Адрес">
													<ItemStyle ForeColor="DimGray"></ItemStyle>
												</asp:BoundColumn>
												<asp:BoundColumn DataField="Phone" SortExpression="Phone" HeaderText="Телефон"></asp:BoundColumn>
												<asp:TemplateColumn>
													<HeaderStyle Wrap="False" Width="20px"></HeaderStyle>
													<ItemStyle Wrap="False" HorizontalAlign="Center"></ItemStyle>
													<ItemTemplate>
														<asp:ImageButton id="ibEditItem" runat="server" ImageUrl="images/edit1.gif" ToolTip="Редактирай"></asp:ImageButton>
													</ItemTemplate>
												</asp:TemplateColumn>
												<asp:TemplateColumn>
													<HeaderStyle Wrap="False" Width="20px"></HeaderStyle>
													<ItemStyle Wrap="False" HorizontalAlign="Center"></ItemStyle>
													<ItemTemplate>
														<asp:ImageButton id="ibDeleteItem" runat="server" Width="12px" Height="12px" ImageUrl="images/delete.gif"
															ToolTip="Изтрий"></asp:ImageButton>
													</ItemTemplate>
												</asp:TemplateColumn>
											</Columns>
										</asp:datagrid>
									</asp:panel>
									<TABLE id="Table4" style="BORDER-RIGHT: tan thin solid; BORDER-TOP: tan thin solid; BORDER-LEFT: tan thin solid; BORDER-BOTTOM: tan thin solid"
										cellSpacing="0" cellPadding="4" width="100%" border="0">
										<TR>
											<TD noWrap><asp:button id="btnNewClient" runat="server" CssClass="ActionButton" Width="200px"></asp:button>&nbsp;
												<asp:button id="btnExport" runat="server" CssClass="ActionButton" Text="Експорт"></asp:button></TD>
										</TR>
									</TABLE>
								</td>
							</tr>
						</table>
					</td>
				</tr>
			</table>
			<radg:radgrid id="gridCalls" runat="server" Width="100%" CssClass="RadGrid" GridLines="Horizontal"
				AutoGenerateColumns="False">
				<PagerStyle CssClass="GridHeader" Mode="NumericPages"></PagerStyle>
				<ItemStyle HorizontalAlign="Center" CssClass="GridItem"></ItemStyle>
				<GroupPanel Visible="False"></GroupPanel>
				<HeaderStyle HorizontalAlign="Center" Wrap="False" CssClass="GridHeader"></HeaderStyle>
				<AlternatingItemStyle HorizontalAlign="Center" CssClass="GridItem"></AlternatingItemStyle>
				<GroupHeaderItemStyle BorderColor="Black" BackColor="Silver"></GroupHeaderItemStyle>
				<MasterTableView DataSourcePersistenceMode="NoPersistence" AllowCustomPaging="False" AllowSorting="True"
					PageSize="15" GridLines="Horizontal" AllowPaging="False" Visible="True">
					<Columns>
						<radg:GridBoundColumn UniqueName="SubcontracterName" HeaderButtonType="TextButton" HeaderText="Име" DataField="SubcontracterName"></radg:GridBoundColumn>
						<radg:GridBoundColumn UniqueName="SubcontracterType" HeaderButtonType="TextButton" HeaderText="Тип" DataField="SubcontracterType"></radg:GridBoundColumn>
						<radg:GridBoundColumn UniqueName="Delo" HeaderButtonType="TextButton" HeaderText="Фирмено дело" DataField="Delo"></radg:GridBoundColumn>
						<radg:GridBoundColumn UniqueName="Bulstat" HeaderButtonType="TextButton" HeaderText="Булстат" DataField="Bulstat"></radg:GridBoundColumn>
						<radg:GridBoundColumn UniqueName="NDR" HeaderButtonType="TextButton" HeaderText="НДР" DataField="NDR"></radg:GridBoundColumn>
						<radg:GridBoundColumn UniqueName="Address" HeaderButtonType="TextButton" HeaderText="Адрес" DataField="Address"></radg:GridBoundColumn>
						<radg:GridBoundColumn UniqueName="Phone" HeaderButtonType="TextButton" HeaderText="Телефон" DataField="Phone"></radg:GridBoundColumn>
						<radg:GridBoundColumn UniqueName="Fax" HeaderButtonType="TextButton" HeaderText="Факс" DataField="Fax"></radg:GridBoundColumn>
						<radg:GridBoundColumn UniqueName="Email" HeaderButtonType="TextButton" HeaderText="Ел.адрес" DataField="Email"></radg:GridBoundColumn>
						<radg:GridBoundColumn UniqueName="Webpage" HeaderButtonType="TextButton" HeaderText="Уебсайт" DataField="Webpage"></radg:GridBoundColumn>
						<radg:GridBoundColumn UniqueName="Manager" HeaderButtonType="TextButton" HeaderText="Представлявано от"
							DataField="Manager"></radg:GridBoundColumn>
						<radg:GridBoundColumn UniqueName="Name1" HeaderButtonType="TextButton" HeaderText="Представител-договор:" DataField="Name1"></radg:GridBoundColumn>
						<radg:GridBoundColumn UniqueName="Phone1" HeaderButtonType="TextButton" HeaderText="Телефон" DataField="Phone1"></radg:GridBoundColumn>
						<radg:GridBoundColumn UniqueName="Email1" HeaderButtonType="TextButton" HeaderText="Ел.адрес" DataField="Email1"></radg:GridBoundColumn>
						<radg:GridBoundColumn UniqueName="Name2" HeaderButtonType="TextButton" HeaderText="Представител-техн. въпроси:"
							DataField="Name2"></radg:GridBoundColumn>
						<radg:GridBoundColumn UniqueName="Phone2" HeaderButtonType="TextButton" HeaderText="Телефон" DataField="Phone2"></radg:GridBoundColumn>
						<radg:GridBoundColumn UniqueName="Email2" HeaderButtonType="TextButton" HeaderText="Ел.адрес" DataField="Email2"></radg:GridBoundColumn>
					</Columns>
					<RowIndicatorColumn Visible="False" UniqueName="RowIndicator">
						<HeaderStyle Width="20px"></HeaderStyle>
					</RowIndicatorColumn>
					<EditFormSettings>
						<EditColumn UniqueName="EditCommandColumn"></EditColumn>
					</EditFormSettings>
					<ExpandCollapseColumn ButtonType="ImageButton" Visible="False" UniqueName="ExpandColumn">
						<HeaderStyle Width="19px"></HeaderStyle>
					</ExpandCollapseColumn>
				</MasterTableView>
			</radg:radgrid>
		</form>
	</body>
</HTML>
