using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Data.SqlClient;
using log4net;
using Asa.Timesheet.Data;
using Asa.Timesheet.Data.Entities;
using Asa.Timesheet.Data.Util;
using Asa.Timesheet.Data.Reports;
using System.Configuration;
using GrapeCity.ActiveReports.Export.Pdf.Section;
namespace Asa.Timesheet.WebPages
{
	/// <summary>
	/// Summary description for AbsenceConfirm.
	/// </summary>
	public class AbsenceConfirms : TimesheetPageBase
	{
		#region Web Controls
	
		protected System.Web.UI.WebControls.PlaceHolder menuHolder;
		protected System.Web.UI.WebControls.Label lblError;
		protected System.Web.UI.WebControls.Label lblInfo;
		protected System.Web.UI.WebControls.Label lbUser;
		protected System.Web.UI.WebControls.Label txtUser;
		protected System.Web.UI.WebControls.Label lbDates;
		protected System.Web.UI.WebControls.Label lbRestType;
		protected System.Web.UI.WebControls.Label txtRestType;
		protected System.Web.UI.WebControls.Button btnConfirm;
		protected System.Web.UI.WebControls.Button btnRefuse;
		protected System.Web.UI.HtmlControls.HtmlTable tblRequestAbsence;
		protected Asa.Timesheet.WebPages.UserControls.PageHeader header;
		

		#endregion
		protected System.Web.UI.WebControls.Label txtDates;
		protected System.Web.UI.WebControls.Calendar calSt;
		protected System.Web.UI.WebControls.Label Label1;
		protected System.Web.UI.WebControls.Label Label2;
		protected System.Web.UI.WebControls.Calendar calEnd;
		protected System.Web.UI.WebControls.CheckBox ckNeplaten;

		private static readonly ILog log = LogManager.GetLogger(typeof(Hours));


        private void Page_Load(object sender, System.EventArgs e)
        {
            if (!(LoggedUser.IsLeader || LoggedUser.IsAccountant || LoggedUser.IsAccountantLow)) ErrorRedirect(Resource.ResourceManager["noRightsAlert"]);

            if (!this.IsPostBack)
            {
                this.header.PageTitle = Resource.ResourceManager["PageTitleAbsenceConfirm"];
                this.header.UserName = this.LoggedUser.UserName;

                LoadRequest();

            }

            UIHelpers.CreateMenu(menuHolder, LoggedUser);

        }
        private string SavePDF(string sName, DateTime dtStart, DateTime dtEnd, DateTime dtFrom, string sType)
        {
            int RequestID = UIHelpers.GetIDParam();
            string returnPath = string.Concat(System.Configuration.ConfigurationManager.AppSettings["AbsensePath"], RequestID, System.Configuration.ConfigurationManager.AppSettings["Extension"]);


            PdfExport pdf = new PdfExport();
            pdf.NeverEmbedFonts = "";


            GrapeCity.ActiveReports.SectionReport report = new Asa.Timesheet.WebPages.Reports.Absence.AbsenseRequest(sName, dtStart, dtEnd, dtFrom, sType);
            report.PageSettings.Orientation = GrapeCity.ActiveReports.Document.Section.PageOrientation.Default;
            report.Run(false);



            pdf.Export(report.Document, Request.MapPath(returnPath));
            return Request.MapPath(returnPath);
        }
	
		#region Web Form Designer generated code
        override protected void OnInit(EventArgs e)
        {
            //
            // CODEGEN: This call is required by the ASP.NET Web Form Designer.
            //
            InitializeComponent();
            base.OnInit(e);
        }
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
        private void InitializeComponent()
        {
            this.calSt.SelectionChanged += new System.EventHandler(this.calEnd_SelectionChanged);
            this.calEnd.SelectionChanged += new System.EventHandler(this.calEnd_SelectionChanged);
            this.btnConfirm.Click += new System.EventHandler(this.btnConfirm_Click);
            this.btnRefuse.Click += new System.EventHandler(this.btnRefuse_Click);
            this.Load += new System.EventHandler(this.Page_Load);

        }
		#endregion

        private void LoadRequest()
        {
            int RequestID = UIHelpers.GetIDParam();
            SqlDataReader reader = AbsenceRequestData.SelectAbsenceRequest(RequestID);
            if (reader.Read())
            {
                DateTime dtStart = reader.GetDateTime(1);
                DateTime dtEnd = reader.GetDateTime(2);
                calSt.SelectedDate = calSt.VisibleDate = dtStart;
                calEnd.SelectedDate = calEnd.VisibleDate = dtEnd;
                bool decided = reader.GetBoolean(3);
                int projectID = reader.GetInt32(4);
                int userID = reader.GetInt32(5);
                bool confirm = (reader.IsDBNull(6)) ? false : reader.GetBoolean(6);
                if (projectID == int.Parse(System.Configuration.ConfigurationManager.AppSettings["Neplaten"]))
                {
                    ckNeplaten.Visible = false;
                }

                if (decided)
                {
                    tblRequestAbsence.Visible = false;
                    if (confirm)
                        lblInfo.Text = Resource.ResourceManager["AbsenceConfirmDecidedTrue"];
                    else
                        lblInfo.Text = Resource.ResourceManager["AbsenceConfirmDecidedFalse"];
                    return;
                }
                else
                {
                    UserInfo ui = UsersData.SelectUserByID(userID);
                    txtUser.Text = ui.FullName;
                    string period;
                    if (dtStart == dtEnd)
                        period = dtStart.ToString("d");
                    else
                        period = string.Concat(dtStart.ToString("d"), " - ", dtEnd.ToString("d"));
                    txtDates.Text = period;
                    txtRestType.Text = ProjectsData.SelectProjectName(projectID);
                }
            }
        }

        private void btnConfirm_Click(object sender, System.EventArgs e)
        {
            if (calSt.SelectedDate > calEnd.SelectedDate)
            {
                lblError.Text = Resource.ResourceManager["hoursPeriod_IncorrectDateMsg"];
                return;
            }
            int RequestID = UIHelpers.GetIDParam();
            SqlDataReader reader = AbsenceRequestData.SelectAbsenceRequest(RequestID);
            DateTime dtStart = DateTime.MinValue;
            DateTime dtEnd = DateTime.MinValue;
            DateTime dtStartP = DateTime.MinValue;
            DateTime dtEndP = DateTime.MinValue;
            int projectID = -1;
            int userID = -1;
            int userIDSub = -1;
            bool hasChanges = false;
            if (reader.Read())
            {
                dtStartP = reader.GetDateTime(1);
                dtEndP = reader.GetDateTime(2);
                dtStart = calSt.SelectedDate;
                dtEnd = calEnd.SelectedDate;
                userIDSub = reader.GetInt32(5);
                if (dtStartP.Date != dtStart.Date
                    || dtEndP.Date != dtEnd.Date
                    )
                    hasChanges = true;
                bool decided = reader.GetBoolean(3);
                projectID = reader.GetInt32(4);
                userID = reader.GetInt32(5);
            }
            hasChanges = false;
            if (ckNeplaten.Checked)
                projectID = int.Parse(System.Configuration.ConfigurationManager.AppSettings["Neplaten"]);
            AbsenceRequestData.UpdateAbsenceRequest(UIHelpers.GetIDParam(), dtStart, dtEnd, true, projectID, userID, true);

            //start
            ArrayList workTimes = CreateWorkTimesList(projectID);

            try
            {
                WorkTimesData.InsertWorkTimesForPeriod(userID, dtStart, dtEnd, workTimes,
                    DateTime.Now, this.LoggedUser.UserID, false);
                log.Info(string.Format("Worktime ADD PERIOD for user {0} on dates from {1} to {2} by {3}", userID, dtStart, dtEnd, string.Concat(LoggedUser.FullName, " ( userID ", LoggedUser.UserID, " )")));

            }
            catch (Exception ex)
            {
                log.Error(ex.Message);
                return;
            }

            UserInfo userAbsence = UsersData.SelectUserByID(userID);
            string sType = Resource.ResourceManager["strPlatenOtpusk"];
            if (int.Parse(System.Configuration.ConfigurationManager.AppSettings["Neplaten"]) == projectID)
                sType = Resource.ResourceManager["strNeplatenOtpusk"];
            string sFile = SavePDF(userAbsence.FullName, dtStart, dtEnd, userAbsence.WorkStartedDate, sType);
            UserInfo userSub = UsersData.SelectUserByID(userIDSub);
            MailBO.SendMailConfirmAbsence(txtUser.Text, dtStart.ToString("d"), dtEnd.ToString("d"), txtRestType.Text, true, userAbsence, sFile, hasChanges, "<br><div style='display: block; background-color: rgb(0, 128, 128); color:white; padding: 15px;'>�� ���e ���������/a ��: <br><strong style='font-size: 14pt'>" + userSub.FullName + "</div></strong>");

            Response.Redirect("Hours.aspx");
        }

		//notes:create worktime info 
        private ArrayList CreateWorkTimesList(int iProjectID)
        {
            ArrayList workTimesList = new ArrayList();
            ArrayList timeIntervals = new ArrayList();


            WorkTimeInfo wti = new WorkTimeInfo();
            //DataGridItem item = (DataGridItem)GridHours.Items[i];

            #region process controls

            int workTimeID = -1;//int.Parse( ((HtmlInputHidden)item.FindControl("hdnWorkTimeID")).Value);
            wti.WorkTimeID = workTimeID;

            //DropDownList ddl = (DropDownList)item.FindControl("ddlProject");
            int projectID = iProjectID;//int.Parse(ddl.SelectedValue);
            wti.ProjectID = projectID;

            //ddl = (DropDownList)item.FindControl("ddlActivity");
            int activityID = 0;//int.Parse( ddl.SelectedValue);
            wti.ActivityID = activityID;//(activityID == 0) ? null : activityID;;

            //ddl = (DropDownList)item.FindControl("ddlStartTime");
            int startTimeID = Constants.HoursGridDefaultStartHourIDDefault;//int.Parse(ddl.SelectedValue);
            wti.StartTimeID = startTimeID;
            //string sStartTime = ddl.SelectedItem.Text;

            //ddl = (DropDownList)item.FindControl("ddlEndTime");
            int endTimeID = Constants.HoursGridDefaultEndHourIDDefault;//int.Parse( ddl.SelectedValue);
            wti.EndTimeID = endTimeID;
            //string sEndTime = ddl.SelectedItem.Text;


            //TextBox txt = (TextBox)item.FindControl("txtMinutes");
            string minutes = "";//txt.Text.Trim();
            wti.Minutes = (minutes == String.Empty) ? null : minutes;

            //txt = (TextBox)item.FindControl("txtAdminMinutes");
            string adminMinutes = "";//txt.Text.Trim();
            wti.AdminMinutes = (adminMinutes == String.Empty) ? null : adminMinutes;

            #endregion

            //	System.Web.UI.WebControls.Image imgWarning = (System.Web.UI.WebControls.Image)item.FindControl("imgWarning");

            #region validation


            //				if (wti.ProjectID == 0) 
            //				{
            //					if ((wti.ActivityID!=0) || (wti.StartTimeID!=0) || (wti.EndTimeID!=0) || (wti.Minutes!= null) || (wti.AdminMinutes!=null))
            //					{
            //						imgWarning.Style["display"] = "";
            //						lblError.Text = Resource.ResourceManager["hours_ErrorEmptyProjectField"];
            //						return null;
            //					}
            //					else continue;
            //				}
            //				else
            //				{
            //					string s = txtNoActivity.Value;
            //					if (txtNoActivity.Value.IndexOf("."+wti.ProjectID.ToString()+".")==-1)
            //					{
            //						if (wti.ActivityID == 0)
            //						{
            //							lblError.Text = Resource.ResourceManager["hours_ErrorMustHaveActivity"];
            //							imgWarning.Style["display"] = "";
            //							return null;
            //						}
            //					}
            //					else
            //					{
            //						if (wti.ActivityID!=0)
            //						{
            //							lblError.Text = Resource.ResourceManager["hours_ErrorMustNotHaveActivity"];
            //							imgWarning.Style["display"] = "";
            //							return null;
            //						}
            //					}
            //
            //					if ((wti.StartTimeID == 0) || (wti.EndTimeID == 0))
            //					{
            //						imgWarning.Style["display"] = "";
            //						lblError.Text = Resource.ResourceManager["hours_ErrorTimeEmpty"];
            //						return null;
            //					}
            //				}
            //				
            //				if (validateInput)
            //				{
            //					TimeInterval ti = new TimeInterval(TimeHelper.TimeInMinutes(sStartTime), TimeHelper.TimeInMinutes(sEndTime));
            //					//new
            //					if (CheckTimeOverlaps(ti, timeIntervals))
            //					{
            //						lblError.Text = Resource.ResourceManager["hours_ErrorTimesOverlap"];
            //						imgWarning.Style["display"] = "";
            //						return null;
            //					}
            //					//endnew
            //
            //					timeIntervals.Add(ti);
            //
            //				}

            #endregion

            workTimesList.Add(wti);



            return workTimesList;
        }


        private void btnRefuse_Click(object sender, System.EventArgs e)
        {
            if (calSt.SelectedDate > calEnd.SelectedDate)
            {
                lblError.Text = Resource.ResourceManager["hoursPeriod_IncorrectDateMsg"];
                return;
            }
            int RequestID = UIHelpers.GetIDParam();
            SqlDataReader reader = AbsenceRequestData.SelectAbsenceRequest(RequestID);
            DateTime dtStart = DateTime.MinValue;
            DateTime dtEnd = DateTime.MinValue;
            int projectID = -1; int userID = -1;
            if (reader.Read())
            {
                dtStart = calSt.SelectedDate;
                dtEnd = calEnd.SelectedDate;
                bool decided = reader.GetBoolean(3);
                projectID = reader.GetInt32(4);
                userID = reader.GetInt32(5);
            }
            AbsenceRequestData.UpdateAbsenceRequest(UIHelpers.GetIDParam(), dtStart, dtEnd, true, projectID, userID, false);

            UserInfo userAbsence = UsersData.SelectUserByID(userID);
            
            MailBO.SendMailConfirmAbsence(txtUser.Text, dtStart.ToString("d"), dtEnd.ToString("d"), txtRestType.Text, false, userAbsence, null, false, "");

            Response.Redirect("Hours.aspx");
        }

        private void calEnd_SelectionChanged(object sender, System.EventArgs e)
        {
            string period;
            DateTime dtStart = calSt.SelectedDate;
            DateTime dtEnd = calEnd.SelectedDate;

            if (dtStart == dtEnd)
                period = dtStart.ToString("d");
            else
                period = string.Concat(dtStart.ToString("d"), " - ", dtEnd.ToString("d"));
            txtDates.Text = period;
        }
	}
}
