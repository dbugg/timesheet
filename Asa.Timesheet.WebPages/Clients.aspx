<%@ Register TagPrefix="uc1" TagName="PageHeader" Src="UserControls/PageHeader.ascx" %>
<%@ Page language="c#" Codebehind="Clients.aspx.cs" AutoEventWireup="false" Inherits="Asa.Timesheet.WebPages.Clients" %>
<%@ Register TagPrefix="radg" Namespace="Telerik.WebControls" Assembly="RadGrid" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
	<HEAD>
		<title>Clients</title>
		<meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
		<meta content="C#" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<LINK href="styles/timesheet.css" type="text/css" rel="stylesheet">
	</HEAD>
	<body bottomMargin="0" leftMargin="0" topMargin="0" rightMargin="0">
		<form id="Form1" method="post" runat="server">
			<table height="100%" cellSpacing="0" cellPadding="0" width="100%" border="0">
				<tr height="1">
					<td>
						<uc1:PageHeader id="header" runat="server"></uc1:PageHeader>
					</td>
				</tr>
				<TR height="1">
					<td noWrap background="images/line.gif" height="1"></td>
				</TR>
				<tr>
					<td>
						<table height="100%" cellSpacing="0" cellPadding="0" width="100%" border="0">
							<tr>
								<td style="BACKGROUND-IMAGE: url(images/left110.gif)" vAlign="top" noWrap width="110"><asp:placeholder id="menuHolder" runat="server"></asp:placeholder></td>
								<td noWrap width="1" bgColor="lightgrey"></td>
								<td style="PADDING-RIGHT: 5px; PADDING-LEFT: 5px; PADDING-TOP: 3px" vAlign="top" noWrap><br>
									<asp:label id="lblError" runat="server" ForeColor="Red" Visible="False" EnableViewState="False"
										CssClass="ErrorLabel"></asp:label><asp:label id="lblInfo" runat="server" Visible="False" EnableViewState="False" CssClass="InfoLabel"></asp:label><br>
									<br>
									<asp:panel id="grid" style="BORDER-RIGHT: #d2b48c 2px solid; PADDING-RIGHT: 2px; BORDER-TOP: #d2b48c 2px solid; OVERFLOW-Y: scroll; PADDING-LEFT: 2px; OVERFLOW-X: auto; BORDER-LEFT: #d2b48c 2px solid; PADDING-TOP: 2px; BORDER-BOTTOM: #d2b48c 2px solid"
										runat="server" Height="450px" Width="100%">
										<asp:datagrid id="grdClients" runat="server" CssClass="Grid" Width="100%" BackColor="Transparent"
											CellPadding="4" AutoGenerateColumns="False" AllowSorting="True">
											<AlternatingItemStyle CssClass="GridAltItem" BackColor="WhiteSmoke"></AlternatingItemStyle>
											<ItemStyle CssClass="GridItem"></ItemStyle>
											<HeaderStyle CssClass="GridHeader"></HeaderStyle>
											<Columns>
												<asp:TemplateColumn HeaderText="#">
													<ItemStyle Width="10px" ForeColor="DimGray"></ItemStyle>
													<ItemTemplate>
														<asp:Label CssClass="menuTable" runat="server" Text='<%# (int)DataBinder.Eval(Container, "ItemIndex")+1 %>' ID="Label1" NAME="Label1">
														</asp:Label>
													</ItemTemplate>
												</asp:TemplateColumn>
												<asp:TemplateColumn HeaderText="Име" SortExpression="Name">
													<ItemTemplate>
														<asp:LinkButton id="lnk" runat="server" CssClass="menuTable" ForeColor="#804000" CommandName="Edit">
															<span Class="menuTable">
																<%# DataBinder.Eval(Container, "DataItem.ClientName") %>
															</span>
														</asp:LinkButton>
													</ItemTemplate>
												</asp:TemplateColumn>
												<asp:BoundColumn DataField="City" SortExpression="City" HeaderText="Град">
													<ItemStyle ForeColor="DimGray"></ItemStyle>
												</asp:BoundColumn>
												<asp:BoundColumn DataField="Address" SortExpression="Address" HeaderText="Адрес">
													<ItemStyle ForeColor="DimGray"></ItemStyle>
												</asp:BoundColumn>
												<asp:BoundColumn DataField="Email" SortExpression="EMail" HeaderText="Е-мейл"></asp:BoundColumn>
												<asp:TemplateColumn>
													<HeaderStyle Wrap="False" Width="20px"></HeaderStyle>
													<ItemStyle Wrap="False" HorizontalAlign="Center"></ItemStyle>
													<ItemTemplate>
														<asp:ImageButton id="ibEditItem" runat="server" ImageUrl="images/edit1.gif" ToolTip="Редактирай"></asp:ImageButton>
													</ItemTemplate>
												</asp:TemplateColumn>
												<asp:TemplateColumn>
													<HeaderStyle Wrap="False" Width="20px"></HeaderStyle>
													<ItemStyle Wrap="False" HorizontalAlign="Center"></ItemStyle>
													<ItemTemplate>
														<asp:ImageButton id="ibDeleteItem" runat="server" Width="12px" Height="12px" ImageUrl="images/delete.gif"
															ToolTip="Изтрий"></asp:ImageButton>
													</ItemTemplate>
												</asp:TemplateColumn>
											</Columns>
										</asp:datagrid>
									</asp:panel>
									<TABLE id="Table4" style="BORDER-RIGHT: tan thin solid; BORDER-TOP: tan thin solid; BORDER-LEFT: tan thin solid; BORDER-BOTTOM: tan thin solid"
										cellSpacing="0" cellPadding="4" width="100%" border="0">
										<TR>
											<TD noWrap><asp:button id="btnNewClient" runat="server" CssClass="ActionButton"></asp:button>&nbsp;
												<asp:button id="btnExport" runat="server" CssClass="ActionButton" Text="Експорт"></asp:button></TD>
										</TR>
									</TABLE>
								</td>
							</tr>
						</table>
					</td>
				</tr>
			</table>
			<radg:radgrid id="gridCalls" runat="server" Width="100%" CssClass="RadGrid" GridLines="Horizontal"
				AutoGenerateColumns="False">
				<PAGERSTYLE CssClass="GridHeader" Mode="NumericPages"></PAGERSTYLE>
				<ITEMSTYLE CssClass="GridItem" HorizontalAlign="Center"></ITEMSTYLE>
				<GROUPPANEL Visible="False"></GROUPPANEL>
				<HEADERSTYLE CssClass="GridHeader" HorizontalAlign="Center" Wrap="False"></HEADERSTYLE>
				<ALTERNATINGITEMSTYLE CssClass="GridItem" HorizontalAlign="Center"></ALTERNATINGITEMSTYLE>
				<GROUPHEADERITEMSTYLE BackColor="Silver" BorderColor="Black"></GROUPHEADERITEMSTYLE>
				<MASTERTABLEVIEW Visible="True" AllowSorting="True" GridLines="Horizontal" AllowPaging="False" PageSize="15"
					AllowCustomPaging="False" DataSourcePersistenceMode="NoPersistence">
					<COLUMNS>
						<RADG:GRIDBOUNDCOLUMN DataField="ClientName" HeaderText="Име" HeaderButtonType="TextButton" UniqueName="ClientName"></RADG:GRIDBOUNDCOLUMN>
						<RADG:GRIDBOUNDCOLUMN DataField="Delo" HeaderText="Фирмено дело" HeaderButtonType="TextButton" UniqueName="Delo"></RADG:GRIDBOUNDCOLUMN>
						<RADG:GRIDBOUNDCOLUMN DataField="Bulstat" HeaderText="Булстат" HeaderButtonType="TextButton" UniqueName="Bulstat"></RADG:GRIDBOUNDCOLUMN>
						<RADG:GRIDBOUNDCOLUMN DataField="NDR" HeaderText="НДР" HeaderButtonType="TextButton" UniqueName="NDR"></RADG:GRIDBOUNDCOLUMN>
						<RADG:GRIDBOUNDCOLUMN DataField="City" HeaderText="Град" HeaderButtonType="TextButton" UniqueName="City"></RADG:GRIDBOUNDCOLUMN>
						<RADG:GRIDBOUNDCOLUMN DataField="Address" HeaderText="Адрес" HeaderButtonType="TextButton" UniqueName="Address"></RADG:GRIDBOUNDCOLUMN>
						<RADG:GRIDBOUNDCOLUMN DataField="Phone" HeaderText="Телефон" HeaderButtonType="TextButton" UniqueName="Phone"></RADG:GRIDBOUNDCOLUMN>
						<RADG:GRIDBOUNDCOLUMN DataField="Fax" HeaderText="Факс" HeaderButtonType="TextButton" UniqueName="Fax"></RADG:GRIDBOUNDCOLUMN>
						<RADG:GRIDBOUNDCOLUMN DataField="Email" HeaderText="Ел.адрес" HeaderButtonType="TextButton" UniqueName="Email"></RADG:GRIDBOUNDCOLUMN>
						<RADG:GRIDBOUNDCOLUMN DataField="Webpage" HeaderText="Уебсайт" HeaderButtonType="TextButton" UniqueName="Webpage"></RADG:GRIDBOUNDCOLUMN>
						<RADG:GRIDBOUNDCOLUMN DataField="Manager" HeaderText="Представлявано от" HeaderButtonType="TextButton"
							UniqueName="Manager"></RADG:GRIDBOUNDCOLUMN>
						<RADG:GRIDBOUNDCOLUMN DataField="Representative1" HeaderText="Представител-договор:" HeaderButtonType="TextButton"
							UniqueName="Name1"></RADG:GRIDBOUNDCOLUMN>
						<RADG:GRIDBOUNDCOLUMN DataField="Phone1" HeaderText="Телефон" HeaderButtonType="TextButton" UniqueName="Phone1"></RADG:GRIDBOUNDCOLUMN>
						<RADG:GRIDBOUNDCOLUMN DataField="Email1" HeaderText="Ел.адрес" HeaderButtonType="TextButton" UniqueName="Email1"></RADG:GRIDBOUNDCOLUMN>
						<RADG:GRIDBOUNDCOLUMN DataField="Representative2" HeaderText="Представител-техн. въпроси:" HeaderButtonType="TextButton"
							UniqueName="Name2"></RADG:GRIDBOUNDCOLUMN>
						<RADG:GRIDBOUNDCOLUMN DataField="Phone2" HeaderText="Телефон" HeaderButtonType="TextButton" UniqueName="Phone2"></RADG:GRIDBOUNDCOLUMN>
						<RADG:GRIDBOUNDCOLUMN DataField="Email2" HeaderText="Ел.адрес" HeaderButtonType="TextButton" UniqueName="Email2"></RADG:GRIDBOUNDCOLUMN>
					</COLUMNS>
					<ROWINDICATORCOLUMN Visible="False" UniqueName="RowIndicator">
						<HEADERSTYLE Width="20px"></HEADERSTYLE>
					</ROWINDICATORCOLUMN>
					<EDITFORMSETTINGS>
						<EDITCOLUMN UniqueName="EditCommandColumn"></EDITCOLUMN>
					</EDITFORMSETTINGS>
					<EXPANDCOLLAPSECOLUMN Visible="False" UniqueName="ExpandColumn" ButtonType="ImageButton">
						<HEADERSTYLE Width="19px"></HEADERSTYLE>
					</EXPANDCOLLAPSECOLUMN>
				</MASTERTABLEVIEW>
			</radg:radgrid>
		</form>
	</body>
</HTML>
