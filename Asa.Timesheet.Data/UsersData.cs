using System;
using System.Data;
using System.Data.SqlClient;
using Microsoft.ApplicationBlocks.Data;
using Asa.Timesheet.Data.Entities;
using System.Collections.Specialized;
using Asa.Timesheet.Data.Reports;
namespace Asa.Timesheet.Data
{
	/// <summary>
	/// Summary description for UsersData.
	/// </summary>
	public class UsersData
	{
		private const int _TOTAL_MONTHS=12;
		public static StringCollection LoadMailsByProject(int ProjectID)
		{
			string spName = "ProjectUsersListMailsProc";
			SqlParameter[] parms = new SqlParameter[2];
			parms[0]= new System.Data.SqlClient.SqlParameter("@ProjectID", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.Input, true, 10, 0, "ProjectID", DataRowVersion.Current, null);
			parms[0].Value = ProjectID;
			parms[1]= new System.Data.SqlClient.SqlParameter("@IsManager", System.Data.SqlDbType.Bit, 1, System.Data.ParameterDirection.Input, true, 0, 0, "IsManager", DataRowVersion.Current, null);
			parms[1].Value = false;
			StringCollection result = new StringCollection();
			using (SqlDataReader reader = SqlHelper.ExecuteReader(CommonDAL.ConnectionString, CommandType.StoredProcedure, spName, parms))
			{
				while (reader.Read())
				{
					string tmp = "";
					if (reader != null && !reader.IsClosed)
					{
						if (!reader.IsDBNull(0)) tmp = reader.GetString(0);
					}
					if(tmp.Length>0)
						result.Add(tmp);
				}
			}
			parms = new SqlParameter[2];
			parms[0]= new System.Data.SqlClient.SqlParameter("@ProjectID", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.Input, true, 10, 0, "ProjectID", DataRowVersion.Current, null);
			parms[0].Value = ProjectID;
			parms[1]= new System.Data.SqlClient.SqlParameter("@IsManager", System.Data.SqlDbType.Bit, 1, System.Data.ParameterDirection.Input, true, 0, 0, "IsManager", DataRowVersion.Current, null);
			parms[1].Value = true;
			using (SqlDataReader reader = SqlHelper.ExecuteReader(CommonDAL.ConnectionString, CommandType.StoredProcedure, spName, parms))
			{
				while (reader.Read())
				{
					string tmp = "";
					if (reader != null && !reader.IsClosed)
					{
						if (!reader.IsDBNull(0)) tmp = reader.GetString(0);
					}
					if(tmp.Length>0)
						result.Add(tmp);
				}
			}
			return result;
		}
		public static void RestoreUser(int userID)
		{
			string spName = "UserRestoryProc";
			SqlParameter[] storedParams = SqlHelperParameterCache.GetSpParameterSet(DBManager.GetConnectionString(), spName);
			storedParams[0].Value = userID;

			SqlHelper.ExecuteNonQuery(DBManager.GetConnectionString(), CommandType.StoredProcedure, spName, storedParams);
		}
		public static SqlDataReader SelectUsersExEmploys(int loggedRoleID, int sortOrder, string spName)
		{
			
			
			SqlParameter[] storedParams = SqlHelperParameterCache.GetSpParameterSet(DBManager.GetConnectionString(), spName);
			storedParams[0].Value = loggedRoleID;
			storedParams[1].Value = sortOrder;

			return SqlHelper.ExecuteReader(DBManager.GetConnectionString(), CommandType.StoredProcedure, spName, storedParams);
		}
		public static UserInfo SelectUserByAccount(string account)
		{
			string spName = "UsersSelByAccountProc";

			SqlParameter[] storedParams = SqlHelperParameterCache.GetSpParameterSet(
				DBManager.GetConnectionString(), spName);
			storedParams[0].Value = account;
			
			UserInfo ui = new UserInfo();

			SqlDataReader reader = null;
			try
			{
				reader = SqlHelper.ExecuteReader(DBManager.GetConnectionString(), CommandType.StoredProcedure, spName, storedParams);
				return GetUserInfo(reader);
			}
			finally
			{
				if (reader!=null && !reader.IsClosed)
					reader.Close();
			}
			//return ui;
		}

		public static UserInfoPM SelectUserPMByAccount(string account)
		{
			UserInfoPM ui = new UserInfoPM();

			string spName = "UsersSelByUserAccountProc";

			SqlParameter[] storedParams = SqlHelperParameterCache.GetSpParameterSet(
				DBManager.GetConnectionString(), spName);
			storedParams[0].Value = account;
		
			SqlDataReader reader = null;
			try
			{
				reader = SqlHelper.ExecuteReader(DBManager.GetConnectionString(), CommandType.StoredProcedure, spName, storedParams);
				ui = GetUserInfoPM(reader);
				if(ui.UserID!=0)
				return ui;
			}
			finally
			{
				if (reader!=null && !reader.IsClosed)
					reader.Close();
			}
			spName = "UsersSelByClientAccountProc";

			storedParams = SqlHelperParameterCache.GetSpParameterSet(
				DBManager.GetConnectionString(), spName);
			storedParams[0].Value = account;
		
			try
			{
				reader = SqlHelper.ExecuteReader(DBManager.GetConnectionString(), CommandType.StoredProcedure, spName, storedParams);
				ui = GetUserInfoPM(reader);
				if(ui.UserID!=0)
					return ui;
			}
			finally
			{
				if (reader!=null && !reader.IsClosed)
					reader.Close();
			}
			spName = "UsersSelBySubcontracterAccountProc";

			storedParams = SqlHelperParameterCache.GetSpParameterSet(
				DBManager.GetConnectionString(), spName);
			storedParams[0].Value = account;
		
			try
			{
				reader = SqlHelper.ExecuteReader(DBManager.GetConnectionString(), CommandType.StoredProcedure, spName, storedParams);
				ui = GetUserInfoPM(reader);
				if(ui.UserID!=0)
					return ui;
			}
			finally
			{
				if (reader!=null && !reader.IsClosed)
					reader.Close();
			}
			return ui;
		}

		public static UserInfoPM SelectUserPMDownLoadByName(string account)
		{
			UserInfoPM ui = new UserInfoPM();

			string spName = "UsersSelByAccountPMProc";

			SqlParameter[] storedParams = SqlHelperParameterCache.GetSpParameterSet(
				DBManager.GetConnectionString(), spName);
			storedParams[0].Value = account;
		
			SqlDataReader reader = null;
			try
			{
				reader = SqlHelper.ExecuteReader(DBManager.GetConnectionString(), CommandType.StoredProcedure, spName, storedParams);
				ui = GetUserInfoPM(reader);
				if(ui.UserID!=0)
					return ui;
			}
			finally
			{
				if (reader!=null && !reader.IsClosed)
					reader.Close();
			}
			
			return ui;
		}

		//notes:hide subcontracter means if the user has'got access level don't see 
		//	subcontractors with option hide
		public static DataView SelectUsersPM(bool HideSubcontracters,int pMUserType, int Active, int projectID, int type/*, bool concluded*/, bool IsVisible, string search)
		{
			//DataView dvReturn = new DataView();
			

			string spName = "UsersListAccountsProc";

			SqlParameter[] storedParams = SqlHelperParameterCache.GetSpParameterSet(
				DBManager.GetConnectionString(), spName);
			storedParams[0].Value = HideSubcontracters;
//			if(Active>=0 && !concluded)
//				storedParams[1].Value = Active;
			if(projectID>0)
				storedParams[2].Value =projectID;
			if(type>0)
				storedParams[3].Value = type;
//			if(concluded)
//				storedParams[4].Value = true;
			switch(Active)
			{
				case (int)ProjectsData.ProjectsByStatus.Active:{storedParams[1].Value = true;storedParams[4].Value = false; break;}
				case (int)ProjectsData.ProjectsByStatus.Passive:{storedParams[1].Value = false;storedParams[4].Value = false; break;}
				case (int)ProjectsData.ProjectsByStatus.Conluded:{storedParams[4].Value = true; break;}

			}
			if(!IsVisible)
				storedParams[5].Value = true;
			if(search!="")
				storedParams[6].Value =search+'%';
			//SqlDataReader reader = null;
//			try
//			{

				//reader = SqlHelper.ExecuteReader(DBManager.GetConnectionString(), CommandType.StoredProcedure, spName);
				DataSet ds =  SqlHelper.ExecuteDataset(DBManager.GetConnectionString(), CommandType.StoredProcedure,
					spName,storedParams);
				//ui = GetUserInfoPM(reader);
				//if(ui.UserID!=0)
				//	return ui;
				if(ds.Tables.Count==0)
					return new DataView();

				//notes:Table 0 - subcontracters
				//	table 1 - Clients
				//	table 2 - Builders
				//	table 3 - PMUsers
				switch(pMUserType)
				{
					case ((int)PMUserType.Subcontracter): return new DataView(ds.Tables[0]);
					case ((int)PMUserType.Client): return new DataView(ds.Tables[1]);
					case ((int)PMUserType.Builder): return new DataView(ds.Tables[2]);
					case ((int)PMUserType.PMUser): return new DataView(ds.Tables[3]);
					case ((int)PMUserType.Supervisors): return new DataView(ds.Tables[4]);
					default:return new DataView();
				}
				
//				DataTable dtReturn = ds.Tables[0].Clone();
//				for(int i = 0; i<ds.Tables.Count;i++)
//				{
//					DataTable dt = ds.Tables[i];
//					foreach(DataRow dr in dt.Rows)
//					{
//						DataRow drNew = dtReturn.NewRow();
//						drNew.ItemArray = dr.ItemArray;
//						dtReturn.Rows.Add(drNew);
//					}
//				}
//				dvReturn = dtReturn.DefaultView;
//			}
//			finally
//			{
//				//if (reader!=null && !reader.IsClosed)
//				//	reader.Close();
//			}
//			return dvReturn;
		}
		
		public static DataView SelectUsersPMAll(bool HideSubcontracters,int pMUserType,string Sort)
		{
			string spName = "UsersListAccountsAllProc";

			SqlParameter[] storedParams = SqlHelperParameterCache.GetSpParameterSet(
				DBManager.GetConnectionString(), spName);
			storedParams[0].Value = HideSubcontracters;
			storedParams[1].Value = Sort;
			DataSet ds =  SqlHelper.ExecuteDataset(DBManager.GetConnectionString(), CommandType.StoredProcedure,
				spName,storedParams);
			if(ds.Tables.Count==0)
				return new DataView();

			//notes:Table 0 - subcontracters
			//	table 1 - Clients
			//	table 2 - Builders
			//	table 3 - PMUsers
			//	table 4 - Supervisors
			switch(pMUserType)
			{
				case ((int)PMUserType.Subcontracter): return new DataView(ds.Tables[0]);
				case ((int)PMUserType.Client): return new DataView(ds.Tables[1]);
				case ((int)PMUserType.Builder): return new DataView(ds.Tables[2]);
				case ((int)PMUserType.PMUser): return new DataView(ds.Tables[3]);
				case ((int)PMUserType.Supervisors): return new DataView(ds.Tables[4]);
				default:return new DataView();
			}
			
		}
		//peppi
		public static UserInfo SelectUserByID(int userID)
		{
			SqlDataReader reader =	SelectUser(userID);
			return GetUserInfo(reader);
		}
		
		private static UserInfo GetUserInfo(SqlDataReader reader)
		{
			UserInfo ui = new UserInfo();
			if (!reader.Read()) return ui;
			ui.UserID = reader.GetInt32(0);
			ui.UserRoleID = reader.IsDBNull(1) ? 0 : reader.GetInt32(1);
			ui.FirstName = reader.IsDBNull(2) ? String.Empty : reader.GetString(2);
			ui.MiddleName = reader.IsDBNull(3) ? String.Empty : reader.GetString(3);
			ui.LastName = reader.GetString(4);
			ui.Account = reader.GetString(5);
			ui.Mail = reader.GetString(6);
			ui.FullName = reader.IsDBNull(7) ? String.Empty : reader.GetString(7);
			ui.Occupation=reader.IsDBNull(11) ? String.Empty : reader.GetString(11);
			ui.UserRole=reader.IsDBNull(12) ? String.Empty : reader.GetString(12);
			ui.Ext=reader.IsDBNull(13) ? String.Empty : reader.GetString(13);
			ui.Mobile=reader.IsDBNull(14) ? String.Empty : reader.GetString(14);
			ui.HomePhone=reader.IsDBNull(15) ? String.Empty : reader.GetString(15);
			ui.CV=reader.IsDBNull(16) ? String.Empty : reader.GetString(16);
			ui.Salary=reader.IsDBNull(17) ?0 : reader.GetDecimal(17);
			ui.Bonus=reader.IsDBNull(18) ? 0 : reader.GetDecimal(18);
			 if (!reader.IsDBNull(19))
					{ui.IsTraine=reader.GetBoolean(19);}
			 else
					{ui.IsTraine=false;}
			if (!reader.IsDBNull(22))
			{ui.HasWorkTime=reader.GetBoolean(22);}
			else
			{ui.HasWorkTime=true;}
			ui.WorkStartedDate=reader.IsDBNull(23) ? Constants.DateMax : reader.GetDateTime(23);
			//Add by Ivailo date: 04.12.2007
			if (!reader.IsDBNull(24))
			{ui.HasWorkTimeOver=reader.GetBoolean(24);}
			else
			{ui.HasWorkTimeOver=true;}

			if(reader.FieldCount>27)
			{
				if (!reader.IsDBNull(27))
					ui.UserNameEN=reader.GetString(27);
			}
			if(reader.FieldCount>28)
			{
				if (!reader.IsDBNull(28))
					ui.PrivateEmail=reader.GetString(28);
			}
            if (reader.FieldCount > 29)
            {
                if (!reader.IsDBNull(29))
                    ui.Coefficient = reader.GetDecimal(18);
            }
			if (reader!=null && !reader.IsClosed) reader.Close();
			return ui;
		}

		private static UserInfoPM GetUserInfoPM(SqlDataReader reader)
		{
			UserInfoPM ui = new UserInfoPM();
			if (!reader.Read())
				return ui;
			ui.UserID = reader.GetInt32(0);
//			ui.UserRoleID = reader.IsDBNull(1) ? 0 : reader.GetInt32(1);
//			ui.FirstName = reader.IsDBNull(2) ? String.Empty : reader.GetString(2);
//			ui.MiddleName = reader.IsDBNull(3) ? String.Empty : reader.GetString(3);
//			ui.LastName = reader.GetString(4);
			ui.FullName = reader.GetString(1);
			ui.Account = reader.GetString(2);
			ui.Mail = reader.IsDBNull(3) ? String.Empty : reader.GetString(3);
			ui.UserRoleID = reader.GetInt32(4);
			ui.UserType = reader.GetInt32(5);
//			ui.Occupation=reader.IsDBNull(11) ? String.Empty : reader.GetString(11);
//			ui.UserRole=reader.IsDBNull(12) ? String.Empty : reader.GetString(12);
//			ui.Ext=reader.IsDBNull(13) ? String.Empty : reader.GetString(13);
//			ui.Mobile=reader.IsDBNull(14) ? String.Empty : reader.GetString(14);
//			ui.HomePhone=reader.IsDBNull(15) ? String.Empty : reader.GetString(15);
//			ui.CV=reader.IsDBNull(16) ? String.Empty : reader.GetString(16);
//			ui.Salary=reader.IsDBNull(17) ?0 : reader.GetDecimal(17);
//			ui.Bonus=reader.IsDBNull(18) ? 0 : reader.GetDecimal(18);
//			// if (!reader.IsDBNull(19))
//			//		{ui.IsTraine=reader.GetBoolean(19);}
//			// else
//			//		{ui.IsTraine=false;}
//			if (!reader.IsDBNull(22))
//			{ui.HasWorkTime=reader.GetBoolean(22);}
//			else
//			{ui.HasWorkTime=true;}
//			ui.WorkStartedDate=reader.IsDBNull(23) ? Constants.DateMax : reader.GetDateTime(23);
//			//Add by Ivailo date: 04.12.2007
//			if (!reader.IsDBNull(24))
//			{ui.HasWorkTimeOver=reader.GetBoolean(24);}
//			else
//			{ui.HasWorkTimeOver=true;}
//
			if (reader!=null && !reader.IsClosed) reader.Close();
			return ui;
		}

		public static SqlDataReader SelectUserNames()
		{
			
			string spName = "UsersNamesListProc";
			return SqlHelper.ExecuteReader(DBManager.GetConnectionString(), CommandType.StoredProcedure, spName);
		}
		public static SqlDataReader SelectUserNames(bool notForWorkTime)
		{
			string spName = "UsersNamesListProc";
			SqlParameter[] storedParams = SqlHelperParameterCache.GetSpParameterSet(DBManager.GetConnectionString(), spName);
			storedParams[3].Value = notForWorkTime;
			
			return SqlHelper.ExecuteReader(DBManager.GetConnectionString(), CommandType.StoredProcedure, spName, storedParams);
		}
		//Add by Ivailo date: 04.12.2007
		//notes: that function is only called from EmptyHours.aspx
		//notForWorkTimeOver - when a user has param HasWorkTimeOver = true, it don't show in the list of users in EmptyHours.aspx
		//this function is for users with param IsActive = false
		public static SqlDataReader SelectUserNames1(bool notForWorkTimeOver)
		{
			string spName = "UsersNamesListProc1";
		
			SqlParameter[] storedParams = SqlHelperParameterCache.GetSpParameterSet(DBManager.GetConnectionString(), spName);
			storedParams[3].Value = notForWorkTimeOver;

			return SqlHelper.ExecuteReader(DBManager.GetConnectionString(), CommandType.StoredProcedure, spName, storedParams);
		}
		public static SqlDataReader SelectUserNames1()
		{
			return SelectUserNames1(true);
		}
		public static SqlDataReader SelectUserNamesOnlyActive(bool notForWorkTime)
		{
			string spName = "UsersNamesListOnlyActiveProc";
			SqlParameter[] storedParams = SqlHelperParameterCache.GetSpParameterSet(DBManager.GetConnectionString(), spName);
			storedParams[0].Value = notForWorkTime;
			return SqlHelper.ExecuteReader(DBManager.GetConnectionString(), CommandType.StoredProcedure, spName,storedParams);
		}

		//Add by Ivailo date: 04.12.2007
		//notes: that function is only called from EmptyHours.aspx
		//notForWorkTimeOver - when a user has param HasWorkTimeOver = true, it don't show in the list of users in EmptyHours.aspx
		public static SqlDataReader SelectUserNames(bool includeInactive, bool includeSpecialUsers, bool notForWorkTime,bool notForWorkTimeOver)
		{
			string spName = "UsersNamesListProc";

			SqlParameter[] storedParams = SqlHelperParameterCache.GetSpParameterSet(DBManager.GetConnectionString(), spName);
			storedParams[0].Value = includeInactive;
			storedParams[1].Value = includeSpecialUsers;
			storedParams[3].Value = notForWorkTime;
			storedParams[4].Value = notForWorkTimeOver;

			return SqlHelper.ExecuteReader(DBManager.GetConnectionString(), CommandType.StoredProcedure, spName, storedParams);
		}
		public static SqlDataReader SelectUserNames(bool includeInactive, bool includeSpecialUsers, bool notForWorkTime)
		{
			return SelectUserNames(includeInactive, includeSpecialUsers, notForWorkTime, true);
		}

		public static SqlDataReader SelectUserNames(bool includeInactive,bool notForWorkTime)
		{
			string spName = "UsersNamesListProc";

			SqlParameter[] storedParams = SqlHelperParameterCache.GetSpParameterSet(DBManager.GetConnectionString(), spName);
			storedParams[0].Value = includeInactive;
			storedParams[3].Value = notForWorkTime;

			return SqlHelper.ExecuteReader(DBManager.GetConnectionString(), CommandType.StoredProcedure, spName, storedParams);
		}
		
		public static SqlDataReader SelectUserNamesConstruct(bool includeInactive)
		{
			string spName = "UsersNamesListProc";

			SqlParameter[] storedParams = SqlHelperParameterCache.GetSpParameterSet(DBManager.GetConnectionString(), spName);
			storedParams[0].Value = includeInactive;
			storedParams[2].Value = Resource.ResourceManager["Contructor"];
			
			return SqlHelper.ExecuteReader(DBManager.GetConnectionString(), CommandType.StoredProcedure, spName, storedParams);
		}
		public static SqlDataReader SelectUserNamesTrainees(bool includeInactive)
		{
			string spName = "UsersNamesTraineesListProc";

			SqlParameter[] storedParams = SqlHelperParameterCache.GetSpParameterSet(DBManager.GetConnectionString(), spName);
			storedParams[0].Value = includeInactive;
			
			
			return SqlHelper.ExecuteReader(DBManager.GetConnectionString(), CommandType.StoredProcedure, spName, storedParams);
		}
		public static SqlDataReader SelectUsers(int loggedRoleID, int sortOrder)
		{
			string spName = "UsersListProc";
			
			SqlParameter[] storedParams = SqlHelperParameterCache.GetSpParameterSet(DBManager.GetConnectionString(), spName);
			storedParams[0].Value = loggedRoleID;
			storedParams[1].Value = sortOrder;

			return SqlHelper.ExecuteReader(DBManager.GetConnectionString(), CommandType.StoredProcedure, spName, storedParams);
		}
		public static SqlDataReader SelectUsersSplit(int loggedRoleID, int sortOrder, string spName,int roleID)
		{
			return SelectUsersSplit(loggedRoleID, sortOrder, spName, roleID,-1);
			
//			SqlParameter[] storedParams = SqlHelperParameterCache.GetSpParameterSet(DBManager.GetConnectionString(), spName);
//			storedParams[0].Value = loggedRoleID;
//			storedParams[1].Value = sortOrder;
//			storedParams[2].Value = (roleID>0)?(object)roleID:null;
//
//			return SqlHelper.ExecuteReader(DBManager.GetConnectionString(), CommandType.StoredProcedure, spName, storedParams);
		}
		public static SqlDataReader SelectUsersSplit(int loggedRoleID, int sortOrder, string spName,int roleID,int projectID)
		{
			
			
			SqlParameter[] storedParams = SqlHelperParameterCache.GetSpParameterSet(DBManager.GetConnectionString(), spName);
			storedParams[0].Value = loggedRoleID;
			storedParams[1].Value = sortOrder;
			storedParams[2].Value = (roleID>0)?(object)roleID:null;
			storedParams[3].Value = (projectID>0)?(object)projectID:null;

			return SqlHelper.ExecuteReader(DBManager.GetConnectionString(), CommandType.StoredProcedure, spName, storedParams);
		}
		public static SqlDataReader SelectUsersSplit(int IsActive, string spName)
		{
			
			
			SqlParameter[] storedParams = SqlHelperParameterCache.GetSpParameterSet(DBManager.GetConnectionString(), spName);
			storedParams[0].Value = (IsActive>-1)?(object)IsActive:null;
			

			return SqlHelper.ExecuteReader(DBManager.GetConnectionString(), CommandType.StoredProcedure, spName, storedParams);
		}
		private static SqlCommand CreateUsersDataViewComandProc(int loggedRoleID, int sortOrder, string spName)
		{
			SqlCommand cmd = new SqlCommand();
			cmd.CommandType = System.Data.CommandType.StoredProcedure;
			cmd.CommandText = spName;
			cmd.Parameters.Add(new System.Data.SqlClient.SqlParameter("@LoggedRoleID", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.Input, true, 10, 0, "LoggedRoleID", DataRowVersion.Current, null));
			cmd.Parameters["@LoggedRoleID"].Value = loggedRoleID;
			cmd.Parameters.Add(new System.Data.SqlClient.SqlParameter("@SortOrder", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.Input, true, 10, 0, "SortOrder", DataRowVersion.Current, null));
			cmd.Parameters["@SortOrder"].Value = sortOrder;
			return cmd;
		}

		public static System.Data.DataView ExecuteUsersDataViewComandProc(int loggedRoleID, int sortOrder, string spName)
		{
			System.Data.SqlClient.SqlDataReader RetObj;
			System.Data.SqlClient.SqlCommand cmd;
			cmd = CreateUsersDataViewComandProc(loggedRoleID, sortOrder, spName);
			SqlConnection cnDAL = new SqlConnection();

			try
			{
				cnDAL =  CommonDAL.OpenConnection();
				cmd.Connection = cnDAL;
				RetObj = cmd.ExecuteReader();
				return new DataView(CommonDAL.CreateTableFromReader(RetObj));
		
			}
			catch(SqlException ex)
			{

				throw new ApplicationException("UsersDataViewComandProc",ex);
			}

			finally
			{
				CommonDAL.CloseConnection(cnDAL);
			}
		}

		public static SqlDataReader SelectUsersIsASI(int loggedRoleID, int sortOrder,bool IsASI)
		{
			string spName = "UsersListIsASIProc";
			
			SqlParameter[] storedParams = SqlHelperParameterCache.GetSpParameterSet(DBManager.GetConnectionString(), spName);
			storedParams[0].Value = loggedRoleID;
			storedParams[1].Value = sortOrder;
			storedParams[3].Value = IsASI;

			return SqlHelper.ExecuteReader(DBManager.GetConnectionString(), CommandType.StoredProcedure, spName, storedParams);
		}

		public static DataSet SelectUsersDS(int loggedRoleID, int sortOrder)
		{
			string spName = "UsersListProc";
			
			SqlParameter[] storedParams = SqlHelperParameterCache.GetSpParameterSet(DBManager.GetConnectionString(), spName);
			storedParams[0].Value = loggedRoleID;
			storedParams[1].Value = sortOrder;

			return SqlHelper.ExecuteDataset(DBManager.GetConnectionString(), CommandType.StoredProcedure,
				spName, storedParams);
		}

		public static SqlDataReader SelectUser(int userID)
		{
			string spName = "UsersSelProc";
			
			SqlParameter[] storedParams = SqlHelperParameterCache.GetSpParameterSet(DBManager.GetConnectionString(), spName);
			storedParams[0].Value = userID;

			return SqlHelper.ExecuteReader(DBManager.GetConnectionString(), CommandType.StoredProcedure, spName, storedParams);
		}

		public static void InactiveUser(int userID)
		{
			string spName = "UsersInactiveProc";
			
			SqlParameter[] storedParams = SqlHelperParameterCache.GetSpParameterSet(DBManager.GetConnectionString(), spName);
			storedParams[0].Value = userID;

			SqlHelper.ExecuteNonQuery(DBManager.GetConnectionString(), CommandType.StoredProcedure,
														spName, storedParams);
		}

		public static void DeleteUser(int userID)
		{
			string spName = "UsersDelProc";
			
			SqlParameter[] storedParams = SqlHelperParameterCache.GetSpParameterSet(DBManager.GetConnectionString(), spName);
			storedParams[0].Value = userID;

			SqlHelper.ExecuteNonQuery(DBManager.GetConnectionString(), CommandType.StoredProcedure, spName, storedParams);
		}

		public static void UpdateUser(int userID, int roleID, string firstName, string secondName, string lastName, 
																	string account, string mail, string password,
			string occupation, string userrole, string ext, string mobile, string home, string cv, decimal salary, 
			decimal bonus, bool istraine, string mobilePersonal, bool isASI, bool HasWorkTime, DateTime WorkDateStart, 
			bool HasWorkTimeOver,int manager, int vacation, string usernameEN, string privateemail, float coefficient)
		{
			string spName = "UsersUpdProc";
			
			SqlParameter[] storedParams = SqlHelperParameterCache.GetSpParameterSet(DBManager.GetConnectionString(), spName);
			storedParams[0].Value = userID;
			storedParams[1].Value = (roleID == 0) ? null : (object)roleID;
			storedParams[2].Value = firstName==String.Empty ? null : firstName;
			storedParams[3].Value = secondName == String.Empty ? null : secondName;
			storedParams[4].Value = lastName;
			storedParams[5].Value = account;
			storedParams[6].Value = mail;
			storedParams[7].Value = (password==String.Empty) ? null : password;
			storedParams[8].Value = (occupation==String.Empty) ? null : occupation;
			storedParams[9].Value = (userrole==String.Empty) ? null : userrole;
			storedParams[10].Value = (ext==String.Empty) ? null : ext;
			storedParams[11].Value = (mobile==String.Empty) ? null : mobile;
			storedParams[12].Value = (home==String.Empty) ? null : home;
			storedParams[13].Value = (cv==String.Empty) ? null : cv;
			storedParams[14].Value = salary;
			storedParams[15].Value = bonus;
			storedParams[16].Value = istraine;
			storedParams[17].Value = mobilePersonal;
			storedParams[18].Value = isASI;
			storedParams[19].Value = HasWorkTime;
			if(WorkDateStart==Constants.DateMax)
				storedParams[20].Value  =null;
			else
				storedParams[20].Value  = WorkDateStart;
			storedParams[21].Value = HasWorkTimeOver;
			if(manager>0)
				storedParams[22].Value  =manager;
			else
				storedParams[22].Value  = null;
			storedParams[23].Value = vacation;
			storedParams[24].Value = usernameEN;
			storedParams[25].Value = privateemail;
            storedParams[26].Value = coefficient;
			SqlHelper.ExecuteNonQuery(DBManager.GetConnectionString(), CommandType.StoredProcedure, spName, storedParams);
		}

		public static int InsertUser(int roleID, string firstName, string secondName, string lastName, 
			string account, string mail, bool isActive, string password,
			string occupation, string userrole, string ext, string mobile, string home, string cv, decimal salary, 
			decimal bonus, bool istraine, string mobilePersonal, bool isASI, bool HasWorkTime, DateTime WorkDayStart, 
			bool HasWorkTimeOver,int manager, int vacation,string usernameEN ,string privateemail, float coefficient)
		{
			string spName = "UsersInsProc";
			
			SqlParameter[] storedParams = SqlHelperParameterCache.GetSpParameterSet(DBManager.GetConnectionString(), spName);
			storedParams[0].Value = (roleID == 0) ? null : (object)roleID;
			storedParams[1].Value = firstName==String.Empty ? null : firstName;
			storedParams[2].Value = secondName == String.Empty ? null : secondName;
			storedParams[3].Value = lastName;
			storedParams[4].Value = account;
			storedParams[5].Value = mail;
			storedParams[6].Value = isActive;
			storedParams[7].Value = (password==String.Empty) ? null : password;
			storedParams[8].Value = (occupation==String.Empty) ? null : occupation;
			storedParams[9].Value = (userrole==String.Empty) ? null : userrole;
			storedParams[10].Value = (ext==String.Empty) ? null : ext;
			storedParams[11].Value = (mobile==String.Empty) ? null : mobile;
			storedParams[12].Value = (home==String.Empty) ? null : home;
			storedParams[13].Value = (cv==String.Empty) ? null : cv;
			storedParams[14].Value = salary;
			storedParams[15].Value = bonus;
			storedParams[16].Value = istraine;
			storedParams[17].Value = mobilePersonal;
			storedParams[18].Value = isASI;
			storedParams[19].Value = HasWorkTime;
			if(WorkDayStart==Constants.DateMax)
				storedParams[20].Value  =null;
			else
				storedParams[20].Value  = WorkDayStart;
			storedParams[21].Value = HasWorkTimeOver;
			if(manager>0)
				storedParams[22].Value  =manager;
			else
				storedParams[22].Value  =null;
			storedParams[23].Value = vacation;
			storedParams[24].Value = usernameEN;
			storedParams[25].Value = privateemail;
            storedParams[26].Value = coefficient;
			
			SqlHelper.ExecuteNonQuery(DBManager.GetConnectionString(), CommandType.StoredProcedure, spName, storedParams);

			return (int)storedParams[27].Value;
		}

		public static SqlDataReader SelectUsersEmails()
		{
			string spName = "UsersEmailsListProc";
			return SqlHelper.ExecuteReader(DBManager.GetConnectionString(), CommandType.StoredProcedure, spName);
		}

		public static SqlDataReader SelectLeadersEmails()
		{
			string spName = "UsersLeadersMailsListProc";

			return SqlHelper.ExecuteReader(DBManager.GetConnectionString(), CommandType.StoredProcedure, spName);
		}

		public static SqlDataReader SelectLeadersOfficersEmails()
		{
			string spName = "UsersLeadersOfficersMailsListProc1";

			return SqlHelper.ExecuteReader(DBManager.GetConnectionString(), CommandType.StoredProcedure, spName);
		}
		public static SqlDataReader SelectLeadersOfficersEmailsOnly()
		{
			string spName = "UsersLeadersOfficersMailsListProc";

			return SqlHelper.ExecuteReader(DBManager.GetConnectionString(), CommandType.StoredProcedure, spName);
		}
		public static SqlDataReader SelectSecretariesOfficersEmails()
		{
			string spName = "UsersSecretariesOfficersMailsListProc";

			return SqlHelper.ExecuteReader(DBManager.GetConnectionString(), CommandType.StoredProcedure, spName);
		}
		public static SqlDataReader SelectUsersMainAccMailListProc()
		{
			string spName = "UsersMainAccMailListProc";

			return SqlHelper.ExecuteReader(DBManager.GetConnectionString(), CommandType.StoredProcedure, spName);
		}
		public static UserInfo UsersListByRoleProc(int RoleID)
		{
			string spName = "UsersListByRoleProc";

			SqlParameter[] storedParams = SqlHelperParameterCache.GetSpParameterSet(
				DBManager.GetConnectionString(), spName);
			storedParams[0].Value = RoleID;
			
			UserInfo ui = new UserInfo();

			SqlDataReader reader = null;
			try
			{
				reader = SqlHelper.ExecuteReader(DBManager.GetConnectionString(), CommandType.StoredProcedure, spName, storedParams);
				return GetUserInfo(reader);
			}
			finally
			{
				if (reader!=null && !reader.IsClosed)
					reader.Close();
			}
		}
		public static int GetTotalYearVacation(DateTime uptoDate,int userID, out int nUsed, out int  nPrevious)
		{

			nUsed=0;
			int nPaid=int.Parse(System.Configuration.ConfigurationManager.AppSettings["Platen"]);
			DataSet ds = AbsenceReportData.SelectAbsenceReportDS( userID, nPaid, new DateTime(uptoDate.Year,1,1), Constants.DateMax,false);
			DateTime dtStart;
			nPrevious = GetUserPreviousYearVacation(userID, out dtStart);
			int TotalYearly = int.Parse(System.Configuration.ConfigurationManager.AppSettings["DaysForYearVacation"]);
			
			int nDaysThisYear = TotalYearly;
			if(dtStart.Year==uptoDate.Year)
			{
				int nMonths = _TOTAL_MONTHS-dtStart.Month;
				nDaysThisYear=nMonths*TotalYearly/_TOTAL_MONTHS;

			}
			if(ds.Tables["Projects"].Rows.Count>0 )
			{
				DataTable dt = AbsenceReportData.GetDaysStringForUserNew(userID,ds.Tables["Projects"].Rows[0],ds);
				if(dt.Rows.Count>0 && dt.Rows[0]["DaysCount"]!=DBNull.Value)
					nUsed=(int)dt.Rows[0]["DaysCount"];
			}
			UserInfo ui = UsersData.SelectUserByID(userID);
			if(ui!=null && ui.IsTraine)
			{
				nDaysThisYear = 0;
				DataSet dss = 
				
					ReportsData.SelectWorkTimesHoursSummaryOfUsers(-1, -1, -1,
					new DateTime(uptoDate.Year,1,1), uptoDate, true, false,true,true,true,userID);
				if(dss!=null && dss.Tables.Count>0 && dss.Tables[0].Rows.Count>0)
				{
					int nHoursInYear=int.Parse(System.Configuration.ConfigurationManager.AppSettings["HoursInYear"]);
					int nHours = 0;
					if(dss.Tables[0].Rows[0]["WorkedMinutes"]!=DBNull.Value)
						nHours =(int)dss.Tables[0].Rows[0]["WorkedMinutes"]/60;
					nDaysThisYear=(nHours*TotalYearly)/nHoursInYear;
					return nDaysThisYear-nUsed;
				}

			}
			if(ds.Tables["Projects"].Rows.Count>0 )
			{
			//	DataTable dt = AbsenceReportData.GetDaysStringForUserNew(userID,ds.Tables["Projects"].Rows[0],ds);
			//	if(dt.Rows.Count>0 && dt.Rows[0]["DaysCount"]!=DBNull.Value)
			//		nUsed=(int)dt.Rows[0]["DaysCount"];
				return nDaysThisYear-nUsed;
			}
			return nDaysThisYear;
		}
		public static int GetUserPreviousYearVacation(int UserID, out DateTime dtStart)
		{
			string spName = "UsersSelProc";
			dtStart = Constants.DateMin;
			SqlParameter[] storedParams = SqlHelperParameterCache.GetSpParameterSet(
				DBManager.GetConnectionString(), spName);
			storedParams[0].Value = UserID;
			
			
			SqlDataReader reader = null;
			try
			{
				reader = SqlHelper.ExecuteReader(DBManager.GetConnectionString(), CommandType.StoredProcedure, spName, storedParams);
				reader.Read();
				if(!reader.IsDBNull(23))
				{
					dtStart = reader.GetDateTime(23);
				}
				if (!reader.IsDBNull(26))
					return reader.GetInt32(26);
				
				return 0;
			}
			finally
			{
				if (reader!=null && !reader.IsClosed)
					reader.Close();
			}
		}
	}
}
