using System;
using System.Data;
using System.Data.SqlClient; 
using System.Collections;
using Microsoft.ApplicationBlocks.Data;

namespace Asa.Timesheet.Data
{
	public class MeetingHotIssueDAL
	{
		/// <summary>
		/// Loads a MeetingHotIssueData object from the database using the given MeetingHotIssuesID
		/// </summary>
		/// <param name="meetingHotIssuesID">The primary key value</param>
		/// <returns>A MeetingHotIssueData object</returns>
		public static MeetingHotIssueData Load(int meetingHotIssuesID)
		{
			SqlParameter[] parameterValues = new SqlParameter[] { new SqlParameter("@MeetingHotIssuesID", meetingHotIssuesID) };
			using (SqlDataReader reader = SqlHelper.ExecuteReader(CommonDAL.ConnectionString, CommandType.StoredProcedure, "MeetingHotIssuesSelProc", parameterValues))
			{
				if (reader.Read())
				{
					MeetingHotIssueData result = new MeetingHotIssueData();
					LoadFromReader(reader,result);
					return result;
				}
				else
					return null;
			}
		}

		/// <summary>
		/// Loads a MeetingHotIssueData object from the database using the given where clause
		/// </summary>
		/// <param name="whereClause">The filter expression for the query</param>
		/// <returns>A MeetingHotIssueData object</returns>
		public static MeetingHotIssueData Load(string whereClause)
		{
			SqlParameter[] parameterValues = new SqlParameter[] { new SqlParameter("@WhereClause", whereClause) };
			using (SqlDataReader reader = SqlHelper.ExecuteReader(CommonDAL.ConnectionString, CommandType.StoredProcedure, "MeetingHotIssuesListProc", parameterValues))
			{
				if (reader.Read())
				{
					MeetingHotIssueData result = new MeetingHotIssueData();
					LoadFromReader(reader,result);
					return result;
				}
				else
					return null;
			}
		}
		public static int Save( MeetingHotIssueData source)
		{
			if (source.MeetingHotIssuesID == -1)
				return Insert(source);
			else
				return Update(source);	
		}

		
		/// <summary>
		/// Loads a collection of MeetingHotIssueData objects from the database.
		/// </summary>
		/// <returns>A collection containing all of the MeetingHotIssueData objects in the database.</returns>
		public static MeetingHotIssuesVector LoadCollection()
		{
			SqlParameter[] parms = new SqlParameter[] {};
			return LoadCollection("MeetingHotIssuesListProc", parms);
		}
	  
		/// <summary>
		/// Saves a collection of MeetingHotIssueData objects to the database.
		/// </summary>
		/// <returns>A collection containing all of the MeetingHotIssueData objects in the database.</returns>
		
		
		/// <summary>
		/// Loads a collection of MeetingHotIssueData objects from the database.
		/// </summary>
		/// <returns>A collection containing all of the MeetingHotIssueData objects in the database.</returns>
		public static MeetingHotIssuesVector LoadCollection(int MeetingID)
		{
			SqlParameter[] parms = new SqlParameter[] { new SqlParameter("@MeetingID", MeetingID) };
			return LoadCollection("MeetingHotIssuesListProc", parms);
		}

		/// <summary>
		/// Loads a collection of MeetingHotIssueData objects from the database.
		/// </summary>
		/// <returns>A collection containing all of the MeetingHotIssueData objects in the database.</returns>
		public static MeetingHotIssuesVector LoadCollection(string spName, SqlParameter[] parms)
		{
			MeetingHotIssuesVector result = new MeetingHotIssuesVector();
			using (SqlDataReader reader = SqlHelper.ExecuteReader(CommonDAL.ConnectionString, CommandType.StoredProcedure, spName, parms))
			{
				while (reader.Read())
				{
					MeetingHotIssueData tmp = new MeetingHotIssueData();
					LoadFromReader(reader, tmp);
					result.Add(tmp);
				}
			}
			return result;
		}
		/// <summary>
		/// Deletes a MeetingHotIssueData object from the database.
		/// </summary>
		/// <param name="meetingHotIssuesID">The primary key value</param>
		public static void Delete(int meetingID, int projectid)
		{
			SqlParameter[] parameterValues = new SqlParameter[] { new SqlParameter("@MeetingID", meetingID),new SqlParameter("@ProjectID", projectid) };
			SqlHelper.ExecuteNonQuery(CommonDAL.ConnectionString, CommandType.StoredProcedure, "MeetingHotIssuesDelProc", parameterValues);
		}

		public static void LoadFromReader(SqlDataReader reader, MeetingHotIssueData target)
		{
			if (reader != null && !reader.IsClosed)
			{
				//target.Key = reader.GetInt32(0);
				if (!reader.IsDBNull(0)) target.MeetingHotIssuesID = reader.GetInt32(0);
				if (!reader.IsDBNull(1)) target.MeetingID = reader.GetInt32(1);
				if (!reader.IsDBNull(2)) target.HotIssueID = reader.GetInt32(2);
			}
		}
		
	  
		public static int Insert( MeetingHotIssueData source)
		{
			SqlParameter[] parameterValues = GetInsertParameterValues(source);
			SqlHelper.ExecuteNonQuery(CommonDAL.ConnectionString, CommandType.StoredProcedure, "MeetingHotIssuesInsProc", parameterValues);
			source.MeetingHotIssuesID = (int) parameterValues[0].Value;
			return source.MeetingHotIssuesID;
		}

		public static int Update( MeetingHotIssueData source)
		{
			SqlParameter[] parameterValues = GetUpdateParameterValues(source);
			SqlHelper.ExecuteNonQuery(CommonDAL.ConnectionString, CommandType.StoredProcedure,
				"MeetingHotIssuesUpdProc", parameterValues);
			return source.MeetingHotIssuesID;
		}
		
		private static bool ExistsKey(int Key, MeetingHotIssuesVector col)
		{
			foreach( MeetingHotIssueData item in col)
				if(item.MeetingHotIssuesID==Key)
					return true;
			return false;
		}

		private static SqlParameter GetSqlParameter(string name, ParameterDirection direction, object value)
		{
			SqlParameter p = new SqlParameter(name, value);
			p.Direction = direction;
			return p;
		}

		private static SqlParameter[] GetInsertParameterValues( MeetingHotIssueData source)
		{
			SqlParameter[] prams = new SqlParameter[3];
			prams[0] = GetSqlParameter("@MeetingHotIssuesID", ParameterDirection.Output, source.MeetingHotIssuesID);
			prams[1] = GetSqlParameter("@MeetingID", ParameterDirection.Input, source.MeetingID);
			prams[2] = GetSqlParameter("@HotIssueID", ParameterDirection.Input, source.HotIssueID);
			return prams;
		}

		private static SqlParameter[] GetUpdateParameterValues( MeetingHotIssueData source)
		{
			return new SqlParameter[] {
										  new SqlParameter("@MeetingHotIssuesID", source.MeetingHotIssuesID),
										  new SqlParameter("@MeetingID", source.MeetingID),
										  new SqlParameter("@HotIssueID", source.HotIssueID)
									  };
		}
	}
}   

