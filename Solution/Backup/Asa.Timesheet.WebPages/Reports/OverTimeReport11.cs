using System;
using System.Data;
using Asa.Timesheet.Data;
using Asa.Timesheet.Data.Util;
using GrapeCity.ActiveReports;
using GrapeCity.ActiveReports.Controls;
using GrapeCity.ActiveReports.SectionReportModel;
using GrapeCity.ActiveReports.Document.Section;
using GrapeCity.ActiveReports.Document;

namespace Asa.Timesheet.WebPages.Reports
{
    public class OverTimeReport1 : GrapeCity.ActiveReports.SectionReport
	{
		private bool _pdf;
        private Picture picReportHeader;
		private string _createdBy= string.Empty;

        public OverTimeReport1(HeaderInfo hi, bool pdf, string createdBy, DataSet ds, string sUser)
        {
            _pdf = pdf;
            _createdBy = createdBy;

            InitializeComponent();
            SetFilterInfo(hi);
            txtHeader.Text += sUser;
            this.DataSource = ds.Tables[0];
            this.ReportEnd += new EventHandler(SubAnalysisRpt1_ReportEnd);
            this.GroupFooter1.Format += new EventHandler(GroupFooter1_Format);

        }
        private void SetFilterInfo(HeaderInfo hi)
        {
            int currentLabel = 1;

            if (hi.StartDate > Constants.DateMin)
            {
                Label lbl = (Label)this.GroupHeader5.Controls["lblFilter" + currentLabel.ToString()];
                lbl.Visible = true;
                lbl.Text = Resource.ResourceManager["reports_Period"];
                TextBox txt = (TextBox)this.GroupHeader5.Controls["txtFilter" + currentLabel.ToString()];
                txt.Visible = true;
                txt.Text = String.Concat(hi.StartDate.ToString("d MMM yyyy"), " ",
                    Resource.ResourceManager["reports_YearAbbr"], " - ",
                    hi.EndDate.ToString("d MMM yyyy"), " ",
                    Resource.ResourceManager["reports_YearAbbr"]);
                currentLabel++;
            }

            if (hi.User != String.Empty)
            {
                Label lbl = (Label)this.GroupHeader5.Controls["lblFilter" + currentLabel.ToString()];
                lbl.Visible = true;
                lbl.Text = Resource.ResourceManager["reports_User"];
                TextBox txt = (TextBox)this.GroupHeader5.Controls["txtFilter" + currentLabel.ToString()];
                txt.Visible = true;
                txt.Text = hi.User;
                currentLabel++;
            }

            if (hi.Activity != String.Empty)
            {
                Label lbl = (Label)this.GroupHeader5.Controls["lblFilter" + currentLabel.ToString()];
                lbl.Visible = true;
                lbl.Text = Resource.ResourceManager["reports_Activity"];
                TextBox txt = (TextBox)this.GroupHeader5.Controls["txtFilter" + currentLabel.ToString()];
                txt.Visible = true;
                txt.Text = hi.Activity;
                currentLabel++;
            }

            if (hi.BuildingType != String.Empty)
            {
                Label lbl = (Label)this.GroupHeader5.Controls["lblFilter" + currentLabel.ToString()];
                lbl.Visible = true;
                lbl.Text = Resource.ResourceManager["reports_BuildingType"];
                TextBox txt = (TextBox)this.GroupHeader5.Controls["txtFilter" + currentLabel.ToString()];
                txt.Visible = true;
                txt.Text = hi.BuildingType;
                currentLabel++;
            }
            if (!SessionManager.LoggedUserInfo.HasPaymentRights)
            {
                Label5.Visible = false;
            }

        }
        private void ReportFooter_Format(object sender, System.EventArgs eArgs)
        {
            txtCreatedBy.Text = _createdBy;
        }

        private void PageHeader_Format(object sender, System.EventArgs eArgs)
        {
            if (!_pdf)
                if (this.PageNumber > 1)
                    this.PageHeader.Visible = false;
        }

        private void Detail_Format(object sender, System.EventArgs eArgs)
        {

            if (!_pdf)
            {
                if (txtNormativ.Text != null)
                    txtNormativ.Text = TimeHelper.HoursFromMinutes((int)decimal.Parse(txtNormativ.Text)).ToString("0.00");
                if (txtOvertime.Text != null)
                    txtOvertime.Text = TimeHelper.HoursFromMinutes((int)decimal.Parse(txtOvertime.Text)).ToString("0.00");
                if (txtTotal.Text != null)
                    txtTotal.Text = TimeHelper.HoursFromMinutes((int)decimal.Parse(txtTotal.Text)).ToString("0.00");
                if (txtUnder.Text != null)
                    txtUnder.Text = TimeHelper.HoursFromMinutes((int)decimal.Parse(txtUnder.Text)).ToString("0.00");
            }
            else
            {
                if (txtNormativ.Text != null)
                    txtNormativ.Text = TimeHelper.HoursStringFromMinutes((int)decimal.Parse(txtNormativ.Text), false);
                if (txtOvertime.Text != null)
                    txtOvertime.Text = TimeHelper.HoursStringFromMinutes((int)decimal.Parse(txtOvertime.Text), false);
                if (txtTotal.Text != null)
                    txtTotal.Text = TimeHelper.HoursStringFromMinutes((int)decimal.Parse(txtTotal.Text), false);
                if (txtUnder.Text != null)
                    txtUnder.Text = TimeHelper.HoursStringFromMinutes((int)decimal.Parse(txtUnder.Text), false);
            }
        }

		#region ActiveReports Designer generated code








































        public void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(OverTimeReport1));
            this.Detail = new GrapeCity.ActiveReports.SectionReportModel.Detail();
            this.txtProjectName = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtWorkedSalary = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtOvertime = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtTotal = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtNormativ = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtUnder = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.ReportHeader = new GrapeCity.ActiveReports.SectionReportModel.ReportHeader();
            this.picReportHeader = new GrapeCity.ActiveReports.SectionReportModel.Picture();
            this.txtHeader = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.ReportFooter = new GrapeCity.ActiveReports.SectionReportModel.ReportFooter();
            this.Line3 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.Label7 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.txtCreatedBy = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.PageHeader = new GrapeCity.ActiveReports.SectionReportModel.PageHeader();
            this.PageFooter = new GrapeCity.ActiveReports.SectionReportModel.PageFooter();
            this.GroupHeader5 = new GrapeCity.ActiveReports.SectionReportModel.GroupHeader();
            this.txtFilter3 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.Line10 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.Label4 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label5 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label6 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.TextBox2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.Label8 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lblFilter1 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.txtFilter1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.lblFilter2 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.txtFilter2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.lblFilter3 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lblFilter4 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.txtFilter4 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.Line11 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.Label9 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.GroupFooter1 = new GrapeCity.ActiveReports.SectionReportModel.GroupFooter();
            this.txtNormativT = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtTotalT = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtOvertimeT = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtUnderT = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.TextBox8 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.Label10 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Line12 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            ((System.ComponentModel.ISupportInitialize)(this.txtProjectName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtWorkedSalary)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtOvertime)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTotal)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtNormativ)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtUnder)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picReportHeader)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtHeader)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCreatedBy)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtFilter3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TextBox2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblFilter1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtFilter1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblFilter2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtFilter2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblFilter3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblFilter4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtFilter4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtNormativT)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTotalT)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtOvertimeT)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtUnderT)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TextBox8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            // 
            // Detail
            // 
            this.Detail.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
            this.txtProjectName,
            this.txtWorkedSalary,
            this.txtOvertime,
            this.txtTotal,
            this.txtNormativ,
            this.txtUnder});
            this.Detail.Height = 0.2076389F;
            this.Detail.Name = "Detail";
            this.Detail.Format += new System.EventHandler(this.Detail_Format);
            // 
            // txtProjectName
            // 
            this.txtProjectName.DataField = "monthyear";
            this.txtProjectName.Height = 0.2F;
            this.txtProjectName.Left = 0.25F;
            this.txtProjectName.Name = "txtProjectName";
            this.txtProjectName.Text = null;
            this.txtProjectName.Top = 0F;
            this.txtProjectName.Width = 1.0625F;
            // 
            // txtWorkedSalary
            // 
            this.txtWorkedSalary.DataField = "TotalAmount";
            this.txtWorkedSalary.Height = 0.2F;
            this.txtWorkedSalary.Left = 6.5F;
            this.txtWorkedSalary.Name = "txtWorkedSalary";
            this.txtWorkedSalary.OutputFormat = resources.GetString("txtWorkedSalary.OutputFormat");
            this.txtWorkedSalary.Style = "text-align: right";
            this.txtWorkedSalary.Text = "WorkedSalary";
            this.txtWorkedSalary.Top = 0F;
            this.txtWorkedSalary.Width = 0.875F;
            // 
            // txtOvertime
            // 
            this.txtOvertime.DataField = "UnderTime";
            this.txtOvertime.Height = 0.2F;
            this.txtOvertime.Left = 4.25F;
            this.txtOvertime.Name = "txtOvertime";
            this.txtOvertime.OutputFormat = resources.GetString("txtOvertime.OutputFormat");
            this.txtOvertime.Style = "text-align: right";
            this.txtOvertime.Text = "WorkedHours";
            this.txtOvertime.Top = 0F;
            this.txtOvertime.Width = 1.1875F;
            // 
            // txtTotal
            // 
            this.txtTotal.DataField = "TotalTime";
            this.txtTotal.Height = 0.2F;
            this.txtTotal.Left = 2.8125F;
            this.txtTotal.Name = "txtTotal";
            this.txtTotal.OutputFormat = resources.GetString("txtTotal.OutputFormat");
            this.txtTotal.Style = "text-align: right";
            this.txtTotal.Text = null;
            this.txtTotal.Top = 0F;
            this.txtTotal.Width = 1.4375F;
            // 
            // txtNormativ
            // 
            this.txtNormativ.DataField = "WorkedMinutesNormal";
            this.txtNormativ.Height = 0.2F;
            this.txtNormativ.Left = 1.3125F;
            this.txtNormativ.Name = "txtNormativ";
            this.txtNormativ.OutputFormat = resources.GetString("txtNormativ.OutputFormat");
            this.txtNormativ.Style = "text-align: right; vertical-align: top";
            this.txtNormativ.Text = null;
            this.txtNormativ.Top = 0F;
            this.txtNormativ.Width = 1.5F;
            // 
            // txtUnder
            // 
            this.txtUnder.DataField = "OverTime";
            this.txtUnder.Height = 0.2F;
            this.txtUnder.Left = 5.4375F;
            this.txtUnder.Name = "txtUnder";
            this.txtUnder.OutputFormat = resources.GetString("txtUnder.OutputFormat");
            this.txtUnder.Style = "text-align: right";
            this.txtUnder.Text = "WorkedHours";
            this.txtUnder.Top = 0F;
            this.txtUnder.Width = 1.083333F;
            // 
            // ReportHeader
            // 
            this.ReportHeader.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
            this.picReportHeader,
            this.txtHeader});
            this.ReportHeader.Height = 0.875F;
            this.ReportHeader.Name = "ReportHeader";
            // 
            // picReportHeader
            // 
            this.picReportHeader.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.picReportHeader.Height = 0.875F;
            this.picReportHeader.HyperLink = null;
            this.picReportHeader.ImageData = ((System.IO.Stream)(resources.GetObject("picReportHeader.ImageData")));
            this.picReportHeader.Left = 4.422F;
            this.picReportHeader.LineColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.picReportHeader.Name = "picReportHeader";
            this.picReportHeader.PictureAlignment = GrapeCity.ActiveReports.SectionReportModel.PictureAlignment.BottomRight;
            this.picReportHeader.SizeMode = GrapeCity.ActiveReports.SectionReportModel.SizeModes.Zoom;
            this.picReportHeader.Top = 0F;
            this.picReportHeader.Width = 2.952756F;
            // 
            // txtHeader
            // 
            this.txtHeader.Height = 0.2F;
            this.txtHeader.Left = 0.0625F;
            this.txtHeader.Name = "txtHeader";
            this.txtHeader.Style = "font-size: 12pt; font-weight: bold; text-align: center";
            this.txtHeader.Text = "������� �� ������������ �����\r\n";
            this.txtHeader.Top = 0.0625F;
            this.txtHeader.Width = 7.3125F;
            // 
            // ReportFooter
            // 
            this.ReportFooter.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
            this.Line3,
            this.Label7,
            this.txtCreatedBy});
            this.ReportFooter.Height = 0.8319445F;
            this.ReportFooter.Name = "ReportFooter";
            this.ReportFooter.Format += new System.EventHandler(this.ReportFooter_Format);
            // 
            // Line3
            // 
            this.Line3.Height = 0F;
            this.Line3.Left = 0.25F;
            this.Line3.LineWeight = 1F;
            this.Line3.Name = "Line3";
            this.Line3.Top = 0F;
            this.Line3.Width = 13.01944F;
            this.Line3.X1 = 0.25F;
            this.Line3.X2 = 13.26944F;
            this.Line3.Y1 = 0F;
            this.Line3.Y2 = 0F;
            // 
            // Label7
            // 
            this.Label7.Height = 0.2F;
            this.Label7.HyperLink = null;
            this.Label7.Left = 0.25F;
            this.Label7.Name = "Label7";
            this.Label7.Style = "font-size: 11pt; text-align: right";
            this.Label7.Text = "��������:";
            this.Label7.Top = 0.4375F;
            this.Label7.Width = 0.8125F;
            // 
            // txtCreatedBy
            // 
            this.txtCreatedBy.Height = 0.2F;
            this.txtCreatedBy.Left = 1.0625F;
            this.txtCreatedBy.Name = "txtCreatedBy";
            this.txtCreatedBy.Style = "font-size: 10pt; font-weight: normal; text-align: justify";
            this.txtCreatedBy.Text = "txtCreatedBy";
            this.txtCreatedBy.Top = 0.625F;
            this.txtCreatedBy.Width = 1.8F;
            // 
            // PageHeader
            // 
            this.PageHeader.Height = 0F;
            this.PageHeader.Name = "PageHeader";
            this.PageHeader.Format += new System.EventHandler(this.PageHeader_Format);
            // 
            // PageFooter
            // 
            this.PageFooter.Height = 0F;
            this.PageFooter.Name = "PageFooter";
            // 
            // GroupHeader5
            // 
            this.GroupHeader5.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
            this.txtFilter3,
            this.Line10,
            this.Label4,
            this.Label5,
            this.Label6,
            this.TextBox2,
            this.Label8,
            this.lblFilter1,
            this.txtFilter1,
            this.lblFilter2,
            this.txtFilter2,
            this.lblFilter3,
            this.lblFilter4,
            this.txtFilter4,
            this.Line11,
            this.Label9});
            this.GroupHeader5.Height = 1.401389F;
            this.GroupHeader5.Name = "GroupHeader5";
            // 
            // txtFilter3
            // 
            this.txtFilter3.Height = 0.2F;
            this.txtFilter3.Left = 2.024606F;
            this.txtFilter3.Name = "txtFilter3";
            this.txtFilter3.Style = "font-size: 9pt; vertical-align: middle";
            this.txtFilter3.Text = null;
            this.txtFilter3.Top = 0.3937007F;
            this.txtFilter3.Visible = false;
            this.txtFilter3.Width = 3.937008F;
            // 
            // Line10
            // 
            this.Line10.Height = 0F;
            this.Line10.Left = 0.2638889F;
            this.Line10.LineWeight = 2F;
            this.Line10.Name = "Line10";
            this.Line10.Top = 1.388889F;
            this.Line10.Width = 7.086611F;
            this.Line10.X1 = 0.2638889F;
            this.Line10.X2 = 7.3505F;
            this.Line10.Y1 = 1.388889F;
            this.Line10.Y2 = 1.388889F;
            // 
            // Label4
            // 
            this.Label4.Height = 0.375F;
            this.Label4.HyperLink = null;
            this.Label4.Left = 4.25F;
            this.Label4.Name = "Label4";
            this.Label4.Style = "font-size: 9pt; font-weight: bold; text-align: right; ddo-char-set: 1";
            this.Label4.Text = "������������ ������";
            this.Label4.Top = 1F;
            this.Label4.Width = 1.1875F;
            // 
            // Label5
            // 
            this.Label5.Height = 0.375F;
            this.Label5.HyperLink = null;
            this.Label5.Left = 6.5F;
            this.Label5.Name = "Label5";
            this.Label5.Style = "font-size: 9pt; font-weight: bold; text-align: right; ddo-char-set: 1";
            this.Label5.Text = "������� (��.)";
            this.Label5.Top = 1F;
            this.Label5.Width = 0.875F;
            // 
            // Label6
            // 
            this.Label6.Height = 0.197F;
            this.Label6.HyperLink = null;
            this.Label6.Left = 0.25F;
            this.Label6.Name = "Label6";
            this.Label6.Style = "font-size: 9pt; font-weight: bold; ddo-char-set: 1";
            this.Label6.Text = "��������";
            this.Label6.Top = 1F;
            this.Label6.Width = 1.0625F;
            // 
            // TextBox2
            // 
            this.TextBox2.Height = 0.197F;
            this.TextBox2.Left = 1.3125F;
            this.TextBox2.Name = "TextBox2";
            this.TextBox2.Style = "color: Black; font-size: 9pt; font-weight: bold; text-align: right; ddo-char-set:" +
    " 1";
            this.TextBox2.Text = "�������� ������";
            this.TextBox2.Top = 1F;
            this.TextBox2.Width = 1.5F;
            // 
            // Label8
            // 
            this.Label8.Height = 0.375F;
            this.Label8.HyperLink = null;
            this.Label8.Left = 2.8125F;
            this.Label8.Name = "Label8";
            this.Label8.Style = "font-size: 9pt; font-weight: bold; text-align: right; ddo-char-set: 1";
            this.Label8.Text = "���������� ������";
            this.Label8.Top = 1F;
            this.Label8.Width = 1.4375F;
            // 
            // lblFilter1
            // 
            this.lblFilter1.Height = 0.2F;
            this.lblFilter1.HyperLink = null;
            this.lblFilter1.Left = 0.25F;
            this.lblFilter1.Name = "lblFilter1";
            this.lblFilter1.Style = "font-size: 9pt; font-weight: bold; vertical-align: middle";
            this.lblFilter1.Text = "";
            this.lblFilter1.Top = 0F;
            this.lblFilter1.Visible = false;
            this.lblFilter1.Width = 1.75F;
            // 
            // txtFilter1
            // 
            this.txtFilter1.Height = 0.2F;
            this.txtFilter1.Left = 2.024606F;
            this.txtFilter1.Name = "txtFilter1";
            this.txtFilter1.Style = "font-size: 9pt; vertical-align: middle";
            this.txtFilter1.Text = null;
            this.txtFilter1.Top = 0F;
            this.txtFilter1.Visible = false;
            this.txtFilter1.Width = 3.937008F;
            // 
            // lblFilter2
            // 
            this.lblFilter2.Height = 0.2F;
            this.lblFilter2.HyperLink = null;
            this.lblFilter2.Left = 0.25F;
            this.lblFilter2.Name = "lblFilter2";
            this.lblFilter2.Style = "font-size: 9pt; font-weight: bold; vertical-align: middle";
            this.lblFilter2.Text = "";
            this.lblFilter2.Top = 0.1968504F;
            this.lblFilter2.Visible = false;
            this.lblFilter2.Width = 1.75F;
            // 
            // txtFilter2
            // 
            this.txtFilter2.Height = 0.2F;
            this.txtFilter2.Left = 2.024606F;
            this.txtFilter2.Name = "txtFilter2";
            this.txtFilter2.Style = "font-size: 9pt; vertical-align: middle";
            this.txtFilter2.Text = null;
            this.txtFilter2.Top = 0.1968504F;
            this.txtFilter2.Visible = false;
            this.txtFilter2.Width = 3.937008F;
            // 
            // lblFilter3
            // 
            this.lblFilter3.Height = 0.2F;
            this.lblFilter3.HyperLink = null;
            this.lblFilter3.Left = 0.25F;
            this.lblFilter3.Name = "lblFilter3";
            this.lblFilter3.Style = "font-size: 9pt; font-weight: bold; vertical-align: middle";
            this.lblFilter3.Text = "";
            this.lblFilter3.Top = 0.3937007F;
            this.lblFilter3.Visible = false;
            this.lblFilter3.Width = 1.75F;
            // 
            // lblFilter4
            // 
            this.lblFilter4.Height = 0.2F;
            this.lblFilter4.HyperLink = null;
            this.lblFilter4.Left = 0.25F;
            this.lblFilter4.Name = "lblFilter4";
            this.lblFilter4.Style = "font-size: 9pt; font-weight: bold; vertical-align: middle";
            this.lblFilter4.Text = "";
            this.lblFilter4.Top = 0.5905511F;
            this.lblFilter4.Visible = false;
            this.lblFilter4.Width = 1.75F;
            // 
            // txtFilter4
            // 
            this.txtFilter4.Height = 0.2F;
            this.txtFilter4.Left = 2.024606F;
            this.txtFilter4.Name = "txtFilter4";
            this.txtFilter4.Style = "font-size: 9pt; vertical-align: middle";
            this.txtFilter4.Text = null;
            this.txtFilter4.Top = 0.5905511F;
            this.txtFilter4.Visible = false;
            this.txtFilter4.Width = 3.937008F;
            // 
            // Line11
            // 
            this.Line11.Height = 0F;
            this.Line11.Left = 0.2569444F;
            this.Line11.LineWeight = 1F;
            this.Line11.Name = "Line11";
            this.Line11.Top = 1.006944F;
            this.Line11.Width = 13.01945F;
            this.Line11.X1 = 0.2569444F;
            this.Line11.X2 = 13.27639F;
            this.Line11.Y1 = 1.006944F;
            this.Line11.Y2 = 1.006944F;
            // 
            // Label9
            // 
            this.Label9.Height = 0.375F;
            this.Label9.HyperLink = null;
            this.Label9.Left = 5.4375F;
            this.Label9.Name = "Label9";
            this.Label9.Style = "font-size: 9pt; font-weight: bold; text-align: right; ddo-char-set: 1";
            this.Label9.Text = "����������� ������";
            this.Label9.Top = 1F;
            this.Label9.Width = 1.0625F;
            // 
            // GroupFooter1
            // 
            this.GroupFooter1.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
            this.txtNormativT,
            this.txtTotalT,
            this.txtOvertimeT,
            this.txtUnderT,
            this.TextBox8,
            this.Label10,
            this.Line12});
            this.GroupFooter1.Height = 0.1666667F;
            this.GroupFooter1.Name = "GroupFooter1";
            // 
            // txtNormativT
            // 
            this.txtNormativT.DataField = "WorkedMinutesNormal";
            this.txtNormativT.Height = 0.2F;
            this.txtNormativT.Left = 1.3125F;
            this.txtNormativT.Name = "txtNormativT";
            this.txtNormativT.OutputFormat = resources.GetString("txtNormativT.OutputFormat");
            this.txtNormativT.Style = "text-align: right; vertical-align: top";
            this.txtNormativT.SummaryRunning = GrapeCity.ActiveReports.SectionReportModel.SummaryRunning.Group;
            this.txtNormativT.SummaryType = GrapeCity.ActiveReports.SectionReportModel.SummaryType.GrandTotal;
            this.txtNormativT.Text = "TextBox4";
            this.txtNormativT.Top = 0F;
            this.txtNormativT.Width = 1.5F;
            // 
            // txtTotalT
            // 
            this.txtTotalT.DataField = "TotalTime";
            this.txtTotalT.Height = 0.2F;
            this.txtTotalT.Left = 2.8125F;
            this.txtTotalT.Name = "txtTotalT";
            this.txtTotalT.OutputFormat = resources.GetString("txtTotalT.OutputFormat");
            this.txtTotalT.Style = "text-align: right";
            this.txtTotalT.SummaryRunning = GrapeCity.ActiveReports.SectionReportModel.SummaryRunning.Group;
            this.txtTotalT.SummaryType = GrapeCity.ActiveReports.SectionReportModel.SummaryType.GrandTotal;
            this.txtTotalT.Text = "TextBox5";
            this.txtTotalT.Top = 0F;
            this.txtTotalT.Width = 1.4375F;
            // 
            // txtOvertimeT
            // 
            this.txtOvertimeT.DataField = "UnderTime";
            this.txtOvertimeT.Height = 0.2F;
            this.txtOvertimeT.Left = 4.25F;
            this.txtOvertimeT.Name = "txtOvertimeT";
            this.txtOvertimeT.OutputFormat = resources.GetString("txtOvertimeT.OutputFormat");
            this.txtOvertimeT.Style = "text-align: right";
            this.txtOvertimeT.SummaryRunning = GrapeCity.ActiveReports.SectionReportModel.SummaryRunning.Group;
            this.txtOvertimeT.SummaryType = GrapeCity.ActiveReports.SectionReportModel.SummaryType.GrandTotal;
            this.txtOvertimeT.Text = "WorkedHours";
            this.txtOvertimeT.Top = 0F;
            this.txtOvertimeT.Width = 1.1875F;
            // 
            // txtUnderT
            // 
            this.txtUnderT.DataField = "OverTime";
            this.txtUnderT.Height = 0.2F;
            this.txtUnderT.Left = 5.4375F;
            this.txtUnderT.Name = "txtUnderT";
            this.txtUnderT.OutputFormat = resources.GetString("txtUnderT.OutputFormat");
            this.txtUnderT.Style = "text-align: right";
            this.txtUnderT.SummaryRunning = GrapeCity.ActiveReports.SectionReportModel.SummaryRunning.Group;
            this.txtUnderT.SummaryType = GrapeCity.ActiveReports.SectionReportModel.SummaryType.GrandTotal;
            this.txtUnderT.Text = "WorkedHours";
            this.txtUnderT.Top = 0F;
            this.txtUnderT.Width = 1.083333F;
            // 
            // TextBox8
            // 
            this.TextBox8.DataField = "TotalAmount";
            this.TextBox8.Height = 0.2F;
            this.TextBox8.Left = 6.5F;
            this.TextBox8.Name = "TextBox8";
            this.TextBox8.OutputFormat = resources.GetString("TextBox8.OutputFormat");
            this.TextBox8.Style = "text-align: right";
            this.TextBox8.SummaryRunning = GrapeCity.ActiveReports.SectionReportModel.SummaryRunning.Group;
            this.TextBox8.SummaryType = GrapeCity.ActiveReports.SectionReportModel.SummaryType.GrandTotal;
            this.TextBox8.Text = "WorkedSalary";
            this.TextBox8.Top = 0F;
            this.TextBox8.Width = 0.875F;
            // 
            // Label10
            // 
            this.Label10.Height = 0.197F;
            this.Label10.HyperLink = null;
            this.Label10.Left = 0.25F;
            this.Label10.Name = "Label10";
            this.Label10.Style = "font-size: 9pt; font-weight: bold; ddo-char-set: 1";
            this.Label10.Text = "����:";
            this.Label10.Top = 0F;
            this.Label10.Width = 1.0625F;
            // 
            // Line12
            // 
            this.Line12.Height = 0F;
            this.Line12.Left = 0.25F;
            this.Line12.LineWeight = 1F;
            this.Line12.Name = "Line12";
            this.Line12.Top = 0F;
            this.Line12.Width = 13.01944F;
            this.Line12.X1 = 0.25F;
            this.Line12.X2 = 13.26944F;
            this.Line12.Y1 = 0F;
            this.Line12.Y2 = 0F;
            // 
            // OverTimeReport1
            // 
            this.MasterReport = false;
            this.PageSettings.PaperHeight = 11F;
            this.PageSettings.PaperWidth = 8.5F;
            this.PrintWidth = 7.374757F;
            this.Sections.Add(this.ReportHeader);
            this.Sections.Add(this.PageHeader);
            this.Sections.Add(this.GroupHeader5);
            this.Sections.Add(this.Detail);
            this.Sections.Add(this.GroupFooter1);
            this.Sections.Add(this.PageFooter);
            this.Sections.Add(this.ReportFooter);
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule(resources.GetString("$this.StyleSheet"), "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: inherit; font-style: inherit; font-variant: inherit; font-weight: bo" +
            "ld; font-size: 16pt; font-size-adjust: inherit; font-stretch: inherit", "Heading1", "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: Times New Roman; font-style: italic; font-variant: inherit; font-wei" +
            "ght: bold; font-size: 14pt; font-size-adjust: inherit; font-stretch: inherit", "Heading2", "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: inherit; font-style: inherit; font-variant: inherit; font-weight: bo" +
            "ld; font-size: 13pt; font-size-adjust: inherit; font-stretch: inherit", "Heading3", "Normal"));
            ((System.ComponentModel.ISupportInitialize)(this.txtProjectName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtWorkedSalary)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtOvertime)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTotal)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtNormativ)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtUnder)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picReportHeader)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtHeader)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCreatedBy)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtFilter3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TextBox2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblFilter1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtFilter1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblFilter2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtFilter2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblFilter3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblFilter4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtFilter4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtNormativT)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTotalT)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtOvertimeT)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtUnderT)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TextBox8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();

        }

		#endregion

        private void SubAnalysisRpt1_ReportEnd(object sender, EventArgs e)
        {
            if (!this._pdf) return;

            Logo1 logo = new Logo1();
            logo.Run();

            for (int i = 0; i < this.Document.Pages.Count; i++)
                this.Document.Pages[i].Overlay(logo.Document.Pages[0]);
        }

        private void GroupFooter1_Format(object sender, EventArgs e)
        {
            if (!_pdf)
            {
                if (txtNormativT.Text != null)
                    txtNormativT.Text = TimeHelper.HoursFromMinutes((int)decimal.Parse(txtNormativT.Text)).ToString("0.00");
                if (txtOvertimeT.Text != null)
                    txtOvertimeT.Text = TimeHelper.HoursFromMinutes((int)decimal.Parse(txtOvertimeT.Text)).ToString("0.00");
                if (txtTotalT.Text != null)
                    txtTotalT.Text = TimeHelper.HoursFromMinutes((int)decimal.Parse(txtTotalT.Text)).ToString("0.00");
                if (txtUnderT.Text != null)
                    txtUnderT.Text = TimeHelper.HoursFromMinutes((int)decimal.Parse(txtUnderT.Text)).ToString("0.00");
            }
            else
            {
                if (txtNormativT.Text != null)
                    txtNormativT.Text = TimeHelper.HoursStringFromMinutes((int)decimal.Parse(txtNormativT.Text), false);
                if (txtOvertimeT.Text != null)
                    txtOvertimeT.Text = TimeHelper.HoursStringFromMinutes((int)decimal.Parse(txtOvertimeT.Text), false);
                if (txtTotalT.Text != null)
                    txtTotalT.Text = TimeHelper.HoursStringFromMinutes((int)decimal.Parse(txtTotalT.Text), false);
                if (txtUnderT.Text != null)
                    txtUnderT.Text = TimeHelper.HoursStringFromMinutes((int)decimal.Parse(txtUnderT.Text), false);
            }
        }

        private ReportHeader ReportHeader;
        private TextBox txtHeader;
        private PageHeader PageHeader;
        private GroupHeader GroupHeader5;
        private TextBox txtFilter3;
        private Line Line10;
        private Label Label4;
        private Label Label5;
        private Label Label6;
        private TextBox TextBox2;
        private Label Label8;
        private Label lblFilter1;
        private TextBox txtFilter1;
        private Label lblFilter2;
        private TextBox txtFilter2;
        private Label lblFilter3;
        private Label lblFilter4;
        private TextBox txtFilter4;
        private Line Line11;
        private Label Label9;
        private Detail Detail;
        private TextBox txtProjectName;
        private TextBox txtWorkedSalary;
        private TextBox txtOvertime;
        private TextBox txtTotal;
        private TextBox txtNormativ;
        private TextBox txtUnder;
        private GroupFooter GroupFooter1;
        private TextBox txtNormativT;
        private TextBox txtTotalT;
        private TextBox txtOvertimeT;
        private TextBox txtUnderT;
        private TextBox TextBox8;
        private Label Label10;
        private Line Line12;
        private PageFooter PageFooter;
        private ReportFooter ReportFooter;
        private Line Line3;
        private Label Label7;
        private TextBox txtCreatedBy;
	}
}
