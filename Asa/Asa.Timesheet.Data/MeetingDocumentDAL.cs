using System;
using System.Data;
using System.Data.SqlClient; 
using System.Collections;
using Microsoft.ApplicationBlocks.Data;

namespace Asa.Timesheet.Data
{
	public class MeetingDocumentDAL
	{
		/// <summary>
		/// Loads a MeetingDocumentData object from the database using the given MeetingDocument
		/// </summary>
		/// <param name="meetingDocument">The primary key value</param>
		/// <returns>A MeetingDocumentData object</returns>
		public static MeetingDocumentData Load(int meetingDocument)
		{
			SqlParameter[] parameterValues = new SqlParameter[] { new SqlParameter("@MeetingDocument", meetingDocument) };
			using (SqlDataReader reader = SqlHelper.ExecuteReader(CommonDAL.ConnectionString, CommandType.StoredProcedure, "MeetingDocumentsSelProc", parameterValues))
			{
				if (reader.Read())
				{
					MeetingDocumentData result = new MeetingDocumentData();
					LoadFromReader(reader,result);
					return result;
				}
				else
					return null;
			}
		}

		/// <summary>
		/// Loads a MeetingDocumentData object from the database using the given where clause
		/// </summary>
		/// <param name="whereClause">The filter expression for the query</param>
		/// <returns>A MeetingDocumentData object</returns>
		public static MeetingDocumentData Load(string whereClause)
		{
			SqlParameter[] parameterValues = new SqlParameter[] { new SqlParameter("@WhereClause", whereClause) };
			using (SqlDataReader reader = SqlHelper.ExecuteReader(CommonDAL.ConnectionString, CommandType.StoredProcedure, "MeetingDocumentsListProc", parameterValues))
			{
				if (reader.Read())
				{
					MeetingDocumentData result = new MeetingDocumentData();
					LoadFromReader(reader,result);
					return result;
				}
				else
					return null;
			}
		}
		public static int Save( MeetingDocumentData source)
		{
			if (source.MeetingDocument == -1)
				return Insert(source);
			else
				return Update(source);	
		}

		public static void Delete( MeetingDocumentData source)
		{
			Delete(source.MeetingDocument);
		}
		/// <summary>
		/// Loads a collection of MeetingDocumentData objects from the database.
		/// </summary>
		/// <returns>A collection containing all of the MeetingDocumentData objects in the database.</returns>
		public static MeetingDocumentsVector LoadCollection()
		{
			SqlParameter[] parms = new SqlParameter[] {};
			return LoadCollection("MeetingDocumentsListProc", parms);
		}
	  
		/// <summary>
		/// Saves a collection of MeetingDocumentData objects to the database.
		/// </summary>
		/// <returns>A collection containing all of the MeetingDocumentData objects in the database.</returns>
		public static  void  SaveCollection(MeetingDocumentsVector col,string spName, SqlParameter[] parms)
		{
			MeetingDocumentsVector colOld = LoadCollection(spName,parms);
			UpdateCollection(colOld,col);
		}
		public static  void  SaveCollection(MeetingDocumentsVector col)
		{
			MeetingDocumentsVector colOld = LoadCollection();
			UpdateCollection(colOld,col);
		}
		public static  void  SaveCollection(MeetingDocumentsVector col, string whereClause )
		{
			MeetingDocumentsVector colOld = LoadCollection(whereClause);
			UpdateCollection(colOld,col);
		}
		/// <summary>
		/// Loads a collection of MeetingDocumentData objects from the database.
		/// </summary>
		/// <returns>A collection containing all of the MeetingDocumentData objects in the database.</returns>
		public static MeetingDocumentsVector LoadCollection(string whereClause)
		{
			SqlParameter[] parms = new SqlParameter[] { new SqlParameter("@WhereClause", whereClause) };
			return LoadCollection("MeetingDocumentsListProc", parms);
		}

		/// <summary>
		/// Loads a collection of MeetingDocumentData objects from the database.
		/// </summary>
		/// <returns>A collection containing all of the MeetingDocumentData objects in the database.</returns>
		public static MeetingDocumentsVector LoadCollection(string spName, SqlParameter[] parms)
		{
			MeetingDocumentsVector result = new MeetingDocumentsVector();
			using (SqlDataReader reader = SqlHelper.ExecuteReader(CommonDAL.ConnectionString, CommandType.StoredProcedure, spName, parms))
			{
				while (reader.Read())
				{
					MeetingDocumentData tmp = new MeetingDocumentData();
					LoadFromReader(reader, tmp);
					result.Add(tmp);
				}
			}
			return result;
		}
		/// <summary>
		/// Deletes a MeetingDocumentData object from the database.
		/// </summary>
		/// <param name="meetingDocument">The primary key value</param>
		public static void Delete(int meetingDocument)
		{
			SqlParameter[] parameterValues = new SqlParameter[] { new SqlParameter("@MeetingDocument", meetingDocument) };
			SqlHelper.ExecuteNonQuery(CommonDAL.ConnectionString, CommandType.StoredProcedure, "MeetingDocumentsDelProc", parameterValues);
		}

		public static void LoadFromReader(SqlDataReader reader, MeetingDocumentData target)
		{
			if (reader != null && !reader.IsClosed)
			{
				//target.Key = reader.GetInt32(0);
				if (!reader.IsDBNull(0)) target.MeetingDocument = reader.GetInt32(0);
				if (!reader.IsDBNull(1)) target.MeetingID = reader.GetInt32(1);
				if (!reader.IsDBNull(2)) target.IsDeleted = reader.GetBoolean(2);
			}
		}
		
	  
		public static int Insert( MeetingDocumentData source)
		{
			SqlParameter[] parameterValues = GetInsertParameterValues(source);
			SqlHelper.ExecuteNonQuery(CommonDAL.ConnectionString, CommandType.StoredProcedure, "MeetingDocumentsInsProc", parameterValues);
			source.MeetingDocument = (int) parameterValues[0].Value;
			return source.MeetingDocument;
		}

		public static int Update( MeetingDocumentData source)
		{
			SqlParameter[] parameterValues = GetUpdateParameterValues(source);
			SqlHelper.ExecuteNonQuery(CommonDAL.ConnectionString, CommandType.StoredProcedure,
				"MeetingDocumentsUpdProc", parameterValues);
			return source.MeetingDocument;
		}
		private static void UpdateCollection(MeetingDocumentsVector colOld,MeetingDocumentsVector col)
		{
			// Delete all old items
			foreach( MeetingDocumentData itemOld in colOld)
			{
				if(!ExistsKey(itemOld.MeetingDocument, col))
					Delete(itemOld.MeetingDocument);
			}
			foreach( MeetingDocumentData item in col)
			{
				if(item.MeetingDocument <= -1)
					Insert(item);
				else
					Update(item);
			}
		}
		private static bool ExistsKey(int Key, MeetingDocumentsVector col)
		{
			foreach( MeetingDocumentData item in col)
				if(item.MeetingDocument==Key)
					return true;
			return false;
		}

		private static SqlParameter GetSqlParameter(string name, ParameterDirection direction, object value)
		{
			SqlParameter p = new SqlParameter(name, value);
			p.Direction = direction;
			return p;
		}

		private static SqlParameter[] GetInsertParameterValues( MeetingDocumentData source)
		{
			SqlParameter[] prams = new SqlParameter[3];
			prams[0] = GetSqlParameter("@MeetingDocument", ParameterDirection.Output, source.MeetingDocument);
			prams[1] = GetSqlParameter("@MeetingID", ParameterDirection.Input, source.MeetingID);
			prams[2] = GetSqlParameter("@IsDeleted", ParameterDirection.Input, source.IsDeleted);
			return prams;
		}

		private static SqlParameter[] GetUpdateParameterValues( MeetingDocumentData source)
		{
			return new SqlParameter[] {
										  new SqlParameter("@MeetingDocument", source.MeetingDocument),
										  new SqlParameter("@MeetingID", source.MeetingID),
										  new SqlParameter("@IsDeleted", source.IsDeleted)
									  };
		}
	}
}   

