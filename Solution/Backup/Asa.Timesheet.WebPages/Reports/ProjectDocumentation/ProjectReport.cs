using System;
using GrapeCity.ActiveReports;

namespace Asa.Timesheet.WebPages.Reports
{
	/// <summary>
	/// Summary description for ProjectReport.
	/// </summary>
	public class ProjectReport
	{
        public static SectionReport CreateProjectReport(int projectID, bool fullReport, bool OnePage, bool hpr)
        {
            GrapeCity.ActiveReports.SectionReport rptMain = new GrapeCity.ActiveReports.SectionReport();
            int currentPage = 1;

            GrapeCity.ActiveReports.SectionReport rptTemp = null;

            rptTemp = new BasicInfoSubreport(projectID, fullReport, currentPage);
            rptTemp.Run();
            rptMain.Document.Pages.AddRange(rptTemp.Document.Pages);
            currentPage += rptTemp.Document.Pages.Count;

            if (!OnePage)
            {
                //project documentation
                rptTemp = new ProjectDocumentation(projectID, fullReport, currentPage);
                rptTemp.Run();
                rptMain.Document.Pages.AddRange(rptTemp.Document.Pages);
                currentPage += rptTemp.Document.Pages.Count;

                // add subcontracters
                rptTemp = new SubcontractersSubreport(projectID, fullReport, currentPage, hpr);
                rptTemp.Run();

                if (((SubcontractersSubreport)rptTemp).HasData)
                {
                    rptMain.Document.Pages.AddRange(rptTemp.Document.Pages);
                    currentPage += rptTemp.Document.Pages.Count;
                }

                //add subprojects
                rptTemp = new SubprojectsSubreport(projectID, fullReport, currentPage, hpr);
                rptTemp.Run();

                if (((SubprojectsSubreport)rptTemp).HasData)
                {
                    rptMain.Document.Pages.AddRange(rptTemp.Document.Pages);
                    currentPage += rptTemp.Document.Pages.Count;
                }
            }

            Logo logo = new Logo();
            logo.Run();

            for (int i = 0; i < rptMain.Document.Pages.Count; i++)
                rptMain.Document.Pages[i].Overlay(logo.Document.Pages[0]);

            return rptMain;
        }

        public static SectionReport CreateProjectInfoReport(int projectID, bool fullReport, bool hpr)
        {
            GrapeCity.ActiveReports.SectionReport rptMain = new GrapeCity.ActiveReports.SectionReport();
            int currentPage = 1;

            GrapeCity.ActiveReports.SectionReport rptTemp = null;

            rptTemp = new ProjectInfo(projectID, fullReport, currentPage);
            rptTemp.Run();
            rptMain.Document.Pages.AddRange(rptTemp.Document.Pages);
            currentPage += rptTemp.Document.Pages.Count;



            // add subcontracters

            rptTemp = new Reports.ProjectSubcontracters(projectID, "", true, false);
            rptTemp.Run();

            if (((Reports.ProjectSubcontracters)rptTemp).HasData)
            {
                rptMain.Document.Pages.AddRange(rptTemp.Document.Pages);
                currentPage += rptTemp.Document.Pages.Count;
            }



            Logo logo = new Logo();
            logo.Run();

            for (int i = 0; i < rptMain.Document.Pages.Count; i++)
                rptMain.Document.Pages[i].Overlay(logo.Document.Pages[0]);

            return rptMain;
        }
        public static SectionReport CreateTechReport(int projectID, bool fullReport, bool hpr)
        {
            GrapeCity.ActiveReports.SectionReport rptMain = new GrapeCity.ActiveReports.SectionReport();
            int currentPage = 1;

            GrapeCity.ActiveReports.SectionReport rptTemp = null;



            //project documentation
            rptTemp = new ProjectDocumentation(projectID, fullReport, currentPage);
            rptTemp.Run();
            rptMain.Document.Pages.AddRange(rptTemp.Document.Pages);
            currentPage += rptTemp.Document.Pages.Count;

            // add subcontracters
            rptTemp = new SubcontractersSubreport(projectID, fullReport, currentPage, hpr);
            rptTemp.Run();

            if (((SubcontractersSubreport)rptTemp).HasData)
            {
                rptMain.Document.Pages.AddRange(rptTemp.Document.Pages);
                currentPage += rptTemp.Document.Pages.Count;
            }



            Logo logo = new Logo();
            logo.Run();

            for (int i = 0; i < rptMain.Document.Pages.Count; i++)
                rptMain.Document.Pages[i].Overlay(logo.Document.Pages[0]);

            return rptMain;
        }
	}
}
