using System;
using System.Data;
using Asa.Timesheet.Data;
using GrapeCity.ActiveReports;
using GrapeCity.ActiveReports.Controls;
using GrapeCity.ActiveReports.SectionReportModel;
using GrapeCity.ActiveReports.Document.Section;
using GrapeCity.ActiveReports.Document;

namespace Asa.Timesheet.WebPages.Reports
{
    public class BuildingPercentFinishedRpt : GrapeCity.ActiveReports.SectionReport
	{
		private bool _forPDF=true;
		private string _createdBy; 
		private int _phase;
		private string _projectName = "";
		private string _clientName = string.Empty;
		private bool _IsData = true;
        public BuildingPercentFinishedRpt(DataView dv, bool forPDF, string createdBy, int phase, string projectName, string clientName)
        {
            _forPDF = forPDF;
            _createdBy = createdBy;
            _phase = phase;
            _projectName = projectName;
            _clientName = clientName;
            InitializeComponent();
            this.DataSource = dv;
            this.ReportEnd += new EventHandler(SubBuildingPercentFinishedRpt_ReportEnd);
            if (dv.Table.Rows.Count == 0)
            {
                lbNoData.Visible = true;
                _IsData = false;
            }
        }


        private void ReportFooter_Format(object sender, System.EventArgs eArgs)
        {
            txtCreatedBy.Text = _createdBy;
        }


        private void Detail_Format(object sender, System.EventArgs eArgs)
        {
            if (_IsData)
            {
                if (this.TextBox6.Text.Length == 0)
                {
                    this.Line16.Visible = true;
                    this.Line15.Visible = true;
                    this.Line21.Visible = true;
                    this.Line22.Visible = true;
                    this.Line23.Visible = true;
                    this.Line24.Visible = true;
                    this.TextBox2.Text = "";
                }
                else
                {
                    this.Line16.Visible = false;
                    this.Line15.Visible = false;
                    this.Line21.Visible = false;
                    this.Line22.Visible = false;
                    this.Line23.Visible = false;
                    this.Line24.Visible = false;
                }
                this.TextBox3.Text += "%";
                this.TextBox4.Text += "%";
                this.TextBox5.Text += "%";
                if (this.TextBox2.Text.Length != 0)
                    if (this.TextBox2.Text.LastIndexOf(".") == this.TextBox2.Text.Length - 1)
                        this.TextBox2.Text = this.TextBox2.Text.Substring(0, this.TextBox2.Text.Length - 1);
                if (this.TextBox1.Text.Length > 30)
                {
                    this.Detail.Height *= 2;
                    this.Line9.Y2 *= 2;
                    this.Line10.Y2 *= 2;
                    this.Line11.Y2 *= 2;
                    this.Line12.Y2 *= 2;
                    this.Line13.Y2 *= 2;
                    this.Line14.Y2 *= 2;
                    this.Line15.Y1 *= 2;
                    this.Line15.Y2 *= 2;
                    this.Line21.Y1 *= 2;
                    this.Line21.Y2 *= 2;
                    this.Line23.Y1 *= 2;
                    this.Line23.Y2 *= 2;
                }
                if (_phase != 0)
                    switch (_phase)
                    {
                        case 1:
                            {
                                this.TextBox4.Visible = false;
                                this.TextBox5.Visible = false;
                                this.Line13.Visible = false;
                                this.Line14.Visible = false;
                                this.Line21.Visible = false;
                                this.Line22.Visible = false;
                                this.Line23.Visible = false;
                                this.Line24.Visible = false;
                                break;
                            }
                        case 2:
                            {
                                this.TextBox3.Visible = false;
                                this.TextBox5.Visible = false;
                                this.Line11.Visible = false;
                                this.Line14.Visible = false;
                                this.Line21.Visible = false;
                                this.Line22.Visible = false;

                                break;
                            }
                        case 3:
                            {
                                this.TextBox4.Visible = false;
                                this.TextBox3.Visible = false;
                                this.Line11.Visible = false;
                                this.Line12.Visible = false;
                                break;
                            }
                    }
            }
            else
                this.Detail.Visible = false;
        }


        private void PageHeader_Format(object sender, System.EventArgs eArgs)
        {
            if (_IsData)
            {
                if (_forPDF || this.PageNumber == 1)
                {
                    if (_phase != 0)
                        switch (_phase)
                        {
                            case 1:
                                {
                                    this.Label4.Visible = false;
                                    this.Label5.Visible = false;
                                    this.Line5.Visible = false;
                                    this.Line6.Visible = false;
                                    this.Line17.Visible = false;
                                    this.Line18.Visible = false;
                                    this.Line19.Visible = false;
                                    this.Line20.Visible = false;
                                    break;
                                }
                            case 2:
                                {
                                    this.Label3.Visible = false;
                                    this.Label5.Visible = false;
                                    this.Line3.Visible = false;
                                    this.Line6.Visible = false;
                                    this.Line19.Visible = false;
                                    this.Line20.Visible = false;
                                    break;
                                }
                            case 3:
                                {
                                    this.Label4.Visible = false;
                                    this.Label3.Visible = false;
                                    this.Line3.Visible = false;
                                    this.Line4.Visible = false;
                                    break;
                                }
                        }
                }
                else
                    this.PageHeader.Visible = false;
            }
            else
                this.PageHeader.Visible = false;
        }


        private void ReportHeader_Format(object sender, System.EventArgs eArgs)
        {
            if (_projectName != "")
                txtProjectName.Text = _projectName;
            else
                lbProjectName.Visible = txtProjectName.Visible = false;
            if (_clientName != "")
                txtClientName.Text = _clientName;
            else
                lbClientName.Visible = txtClientName.Visible = false;
            DateTime dt = DateTime.Now;
            lbTitle.Text = string.Concat(lbTitle.Text, " ", dt.ToShortDateString());

        }

		
		#region ActiveReports Designer generated code


















































        public void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(BuildingPercentFinishedRpt));
            this.Detail = new GrapeCity.ActiveReports.SectionReportModel.Detail();
            this.ReportHeader = new GrapeCity.ActiveReports.SectionReportModel.ReportHeader();
            this.ReportFooter = new GrapeCity.ActiveReports.SectionReportModel.ReportFooter();
            this.PageHeader = new GrapeCity.ActiveReports.SectionReportModel.PageHeader();
            this.PageFooter = new GrapeCity.ActiveReports.SectionReportModel.PageFooter();
            this.txtClientName = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.Label10 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lbClientName = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lbTitle = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lbProjectName = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.txtProjectName = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.lbNoData = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label8 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label1 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label2 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label3 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label4 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label5 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Line1 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.Line2 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.Line3 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.Line4 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.Line5 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.Line6 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.Line7 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.Line8 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.Line17 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.Line18 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.Line19 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.Line20 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.TextBox1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.TextBox2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.TextBox3 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.TextBox4 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.TextBox5 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.TextBox6 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.Line9 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.Line10 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.Line11 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.Line12 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.Line13 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.Line14 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.Line16 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.Line15 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.Line21 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.Line22 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.Line23 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.Line24 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.txtCreatedBy = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.Label7 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            ((System.ComponentModel.ISupportInitialize)(this.txtClientName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbClientName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbTitle)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbProjectName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtProjectName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbNoData)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TextBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TextBox2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TextBox3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TextBox4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TextBox5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TextBox6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCreatedBy)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            // 
            // Detail
            // 
            this.Detail.ColumnSpacing = 0F;
            this.Detail.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
						this.TextBox1,
						this.TextBox2,
						this.TextBox3,
						this.TextBox4,
						this.TextBox5,
						this.TextBox6,
						this.Line9,
						this.Line10,
						this.Line11,
						this.Line12,
						this.Line13,
						this.Line14,
						this.Line16,
						this.Line15,
						this.Line21,
						this.Line22,
						this.Line23,
						this.Line24});
            this.Detail.Height = 0.2118056F;
            this.Detail.Name = "Detail";
            // 
            // ReportHeader
            // 
            this.ReportHeader.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
						this.txtClientName,
						this.Label10,
						this.lbClientName,
						this.lbTitle,
						this.lbProjectName,
						this.txtProjectName,
						this.lbNoData,
						this.Label8});
            this.ReportHeader.Height = 1.458333F;
            this.ReportHeader.Name = "ReportHeader";
            // 
            // ReportFooter
            // 
            this.ReportFooter.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
						this.txtCreatedBy,
						this.Label7});
            this.ReportFooter.Height = 0.4F;
            this.ReportFooter.Name = "ReportFooter";
            // 
            // PageHeader
            // 
            this.PageHeader.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
						this.Label1,
						this.Label2,
						this.Label3,
						this.Label4,
						this.Label5,
						this.Line1,
						this.Line2,
						this.Line3,
						this.Line4,
						this.Line5,
						this.Line6,
						this.Line7,
						this.Line8,
						this.Line17,
						this.Line18,
						this.Line19,
						this.Line20});
            this.PageHeader.Height = 0.2118056F;
            this.PageHeader.Name = "PageHeader";
            // 
            // PageFooter
            // 
            this.PageFooter.Height = 0F;
            this.PageFooter.Name = "PageFooter";
            // 
            // txtClientName
            // 
            this.txtClientName.Height = 0.2F;
            this.txtClientName.Left = 2F;
            this.txtClientName.Name = "txtClientName";
            this.txtClientName.Style = "font-weight: bold; text-align: left";
            this.txtClientName.Top = 0.8185F;
            this.txtClientName.Width = 5F;
            // 
            // Label10
            // 
            this.Label10.Height = 0.2F;
            this.Label10.HyperLink = null;
            this.Label10.Left = 2F;
            this.Label10.Name = "Label10";
            this.Label10.Style = "";
            this.Label10.Text = "";
            this.Label10.Top = 0.8185F;
            this.Label10.Visible = false;
            this.Label10.Width = 0.4375F;
            // 
            // lbClientName
            // 
            this.lbClientName.Height = 0.2F;
            this.lbClientName.HyperLink = null;
            this.lbClientName.Left = 1F;
            this.lbClientName.Name = "lbClientName";
            this.lbClientName.Style = "font-weight: bold; text-align: right";
            this.lbClientName.Text = "??????:";
            this.lbClientName.Top = 0.8125F;
            this.lbClientName.Width = 1F;
            // 
            // lbTitle
            // 
            this.lbTitle.Height = 0.2F;
            this.lbTitle.HyperLink = null;
            this.lbTitle.Left = 1F;
            this.lbTitle.Name = "lbTitle";
            this.lbTitle.Style = "font-size: 12pt; font-weight: bold; text-align: center";
            this.lbTitle.Text = "?????????? ?? ?????? ??? ????";
            this.lbTitle.Top = 0.2F;
            this.lbTitle.Width = 6F;
            // 
            // lbProjectName
            // 
            this.lbProjectName.Height = 0.2F;
            this.lbProjectName.HyperLink = null;
            this.lbProjectName.Left = 1F;
            this.lbProjectName.Name = "lbProjectName";
            this.lbProjectName.Style = "font-weight: bold; text-align: right";
            this.lbProjectName.Text = "??????:";
            this.lbProjectName.Top = 0.5F;
            this.lbProjectName.Width = 1F;
            // 
            // txtProjectName
            // 
            this.txtProjectName.Height = 0.2F;
            this.txtProjectName.Left = 2F;
            this.txtProjectName.Name = "txtProjectName";
            this.txtProjectName.Style = "font-weight: bold";
            this.txtProjectName.Top = 0.506F;
            this.txtProjectName.Width = 5F;
            // 
            // lbNoData
            // 
            this.lbNoData.Height = 0.2F;
            this.lbNoData.HyperLink = null;
            this.lbNoData.Left = 2F;
            this.lbNoData.Name = "lbNoData";
            this.lbNoData.Style = "text-align: center";
            this.lbNoData.Text = "?? ?? ???????? ????? ????????????? ?? ???????? ??????.";
            this.lbNoData.Top = 1.1875F;
            this.lbNoData.Visible = false;
            this.lbNoData.Width = 5F;
            // 
            // Label8
            // 
            this.Label8.Height = 0.2F;
            this.Label8.HyperLink = null;
            this.Label8.Left = 2F;
            this.Label8.Name = "Label8";
            this.Label8.Style = "";
            this.Label8.Text = "";
            this.Label8.Top = 0.506F;
            this.Label8.Visible = false;
            this.Label8.Width = 0.4375F;
            // 
            // Label1
            // 
            this.Label1.Height = 0.2F;
            this.Label1.HyperLink = null;
            this.Label1.Left = 1F;
            this.Label1.Name = "Label1";
            this.Label1.Style = "";
            this.Label1.Text = "??????????";
            this.Label1.Top = 0F;
            this.Label1.Width = 2.5F;
            // 
            // Label2
            // 
            this.Label2.Height = 0.2F;
            this.Label2.HyperLink = null;
            this.Label2.Left = 3.5F;
            this.Label2.Name = "Label2";
            this.Label2.Style = "text-align: justify";
            this.Label2.Text = "???";
            this.Label2.Top = 0F;
            this.Label2.Width = 1F;
            // 
            // Label3
            // 
            this.Label3.Height = 0.2F;
            this.Label3.HyperLink = null;
            this.Label3.Left = 4.5F;
            this.Label3.Name = "Label3";
            this.Label3.Style = "text-align: center";
            this.Label3.Text = "?????? ????";
            this.Label3.Top = 0F;
            this.Label3.Width = 1.05F;
            // 
            // Label4
            // 
            this.Label4.Height = 0.2F;
            this.Label4.HyperLink = null;
            this.Label4.Left = 5.55F;
            this.Label4.Name = "Label4";
            this.Label4.Style = "font-size: 9pt; text-align: center; ddo-char-set: 1";
            this.Label4.Text = "?????????? ????";
            this.Label4.Top = 0F;
            this.Label4.Width = 1.05F;
            // 
            // Label5
            // 
            this.Label5.Height = 0.2F;
            this.Label5.HyperLink = null;
            this.Label5.Left = 6.6F;
            this.Label5.Name = "Label5";
            this.Label5.Style = "text-align: center";
            this.Label5.Text = "??????? ????";
            this.Label5.Top = 0F;
            this.Label5.Width = 1.05F;
            // 
            // Line1
            // 
            this.Line1.Height = 0.21F;
            this.Line1.Left = 0.99F;
            this.Line1.LineWeight = 1F;
            this.Line1.Name = "Line1";
            this.Line1.Top = 0F;
            this.Line1.Width = 0F;
            this.Line1.X1 = 0.99F;
            this.Line1.X2 = 0.99F;
            this.Line1.Y1 = 0F;
            this.Line1.Y2 = 0.21F;
            // 
            // Line2
            // 
            this.Line2.Height = 0.21F;
            this.Line2.Left = 3.49F;
            this.Line2.LineWeight = 1F;
            this.Line2.Name = "Line2";
            this.Line2.Top = 0F;
            this.Line2.Width = 0F;
            this.Line2.X1 = 3.49F;
            this.Line2.X2 = 3.49F;
            this.Line2.Y1 = 0F;
            this.Line2.Y2 = 0.21F;
            // 
            // Line3
            // 
            this.Line3.Height = 0.21F;
            this.Line3.Left = 4.490001F;
            this.Line3.LineWeight = 1F;
            this.Line3.Name = "Line3";
            this.Line3.Top = 0F;
            this.Line3.Width = 0F;
            this.Line3.X1 = 4.490001F;
            this.Line3.X2 = 4.490001F;
            this.Line3.Y1 = 0F;
            this.Line3.Y2 = 0.21F;
            // 
            // Line4
            // 
            this.Line4.Height = 0.21F;
            this.Line4.Left = 5.54F;
            this.Line4.LineWeight = 1F;
            this.Line4.Name = "Line4";
            this.Line4.Top = 0F;
            this.Line4.Width = 0F;
            this.Line4.X1 = 5.54F;
            this.Line4.X2 = 5.54F;
            this.Line4.Y1 = 0F;
            this.Line4.Y2 = 0.21F;
            // 
            // Line5
            // 
            this.Line5.Height = 0.21F;
            this.Line5.Left = 6.590001F;
            this.Line5.LineWeight = 1F;
            this.Line5.Name = "Line5";
            this.Line5.Top = 0F;
            this.Line5.Width = 0F;
            this.Line5.X1 = 6.590001F;
            this.Line5.X2 = 6.590001F;
            this.Line5.Y1 = 0F;
            this.Line5.Y2 = 0.21F;
            // 
            // Line6
            // 
            this.Line6.Height = 0.21F;
            this.Line6.Left = 7.64F;
            this.Line6.LineWeight = 1F;
            this.Line6.Name = "Line6";
            this.Line6.Top = 0F;
            this.Line6.Width = 0F;
            this.Line6.X1 = 7.64F;
            this.Line6.X2 = 7.64F;
            this.Line6.Y1 = 0F;
            this.Line6.Y2 = 0.21F;
            // 
            // Line7
            // 
            this.Line7.Height = 0F;
            this.Line7.Left = 0.99F;
            this.Line7.LineWeight = 1F;
            this.Line7.Name = "Line7";
            this.Line7.Top = 0F;
            this.Line7.Width = 4.55F;
            this.Line7.X1 = 0.99F;
            this.Line7.X2 = 5.54F;
            this.Line7.Y1 = 0F;
            this.Line7.Y2 = 0F;
            // 
            // Line8
            // 
            this.Line8.Height = 0F;
            this.Line8.Left = 0.99F;
            this.Line8.LineWeight = 1F;
            this.Line8.Name = "Line8";
            this.Line8.Top = 0.21F;
            this.Line8.Width = 4.55F;
            this.Line8.X1 = 0.99F;
            this.Line8.X2 = 5.54F;
            this.Line8.Y1 = 0.21F;
            this.Line8.Y2 = 0.21F;
            // 
            // Line17
            // 
            this.Line17.Height = 0F;
            this.Line17.Left = 5.54F;
            this.Line17.LineWeight = 1F;
            this.Line17.Name = "Line17";
            this.Line17.Top = 0.21F;
            this.Line17.Width = 1.050001F;
            this.Line17.X1 = 5.54F;
            this.Line17.X2 = 6.590001F;
            this.Line17.Y1 = 0.21F;
            this.Line17.Y2 = 0.21F;
            // 
            // Line18
            // 
            this.Line18.Height = 0F;
            this.Line18.Left = 5.54F;
            this.Line18.LineWeight = 1F;
            this.Line18.Name = "Line18";
            this.Line18.Top = 0F;
            this.Line18.Width = 1.050001F;
            this.Line18.X1 = 5.54F;
            this.Line18.X2 = 6.590001F;
            this.Line18.Y1 = 0F;
            this.Line18.Y2 = 0F;
            // 
            // Line19
            // 
            this.Line19.Height = 0F;
            this.Line19.Left = 6.590001F;
            this.Line19.LineWeight = 1F;
            this.Line19.Name = "Line19";
            this.Line19.Top = 0.21F;
            this.Line19.Width = 1.049999F;
            this.Line19.X1 = 6.590001F;
            this.Line19.X2 = 7.64F;
            this.Line19.Y1 = 0.21F;
            this.Line19.Y2 = 0.21F;
            // 
            // Line20
            // 
            this.Line20.Height = 0F;
            this.Line20.Left = 6.590001F;
            this.Line20.LineWeight = 1F;
            this.Line20.Name = "Line20";
            this.Line20.Top = 0F;
            this.Line20.Width = 1.049999F;
            this.Line20.X1 = 6.590001F;
            this.Line20.X2 = 7.64F;
            this.Line20.Y1 = 0F;
            this.Line20.Y2 = 0F;
            // 
            // TextBox1
            // 
            this.TextBox1.DataField = "projectname";
            this.TextBox1.Height = 0.2F;
            this.TextBox1.Left = 1F;
            this.TextBox1.Name = "TextBox1";
            this.TextBox1.Text = "TextBox1";
            this.TextBox1.Top = 0F;
            this.TextBox1.Width = 2.5F;
            // 
            // TextBox2
            // 
            this.TextBox2.DataField = "ContentCode";
            this.TextBox2.Height = 0.2F;
            this.TextBox2.Left = 3.5F;
            this.TextBox2.Name = "TextBox2";
            this.TextBox2.Text = "TextBox2";
            this.TextBox2.Top = 0F;
            this.TextBox2.Width = 1F;
            // 
            // TextBox3
            // 
            this.TextBox3.DataField = "ContentFinishedPhase1";
            this.TextBox3.Height = 0.2F;
            this.TextBox3.Left = 4.5F;
            this.TextBox3.Name = "TextBox3";
            this.TextBox3.Style = "text-align: center";
            this.TextBox3.Text = "TextBox3";
            this.TextBox3.Top = 0F;
            this.TextBox3.Width = 1.05F;
            // 
            // TextBox4
            // 
            this.TextBox4.DataField = "ContentFinishedPhase2";
            this.TextBox4.Height = 0.2F;
            this.TextBox4.Left = 5.55F;
            this.TextBox4.Name = "TextBox4";
            this.TextBox4.Style = "text-align: center";
            this.TextBox4.Text = "TextBox4";
            this.TextBox4.Top = 0F;
            this.TextBox4.Width = 1.05F;
            // 
            // TextBox5
            // 
            this.TextBox5.DataField = "ContentFinishedPhase3";
            this.TextBox5.Height = 0.2F;
            this.TextBox5.Left = 6.6F;
            this.TextBox5.Name = "TextBox5";
            this.TextBox5.Style = "text-align: center";
            this.TextBox5.Text = "TextBox5";
            this.TextBox5.Top = 0F;
            this.TextBox5.Width = 1.05F;
            // 
            // TextBox6
            // 
            this.TextBox6.DataField = "ContentID";
            this.TextBox6.Height = 0.2F;
            this.TextBox6.Left = 3.302083F;
            this.TextBox6.Name = "TextBox6";
            this.TextBox6.Top = 0F;
            this.TextBox6.Visible = false;
            this.TextBox6.Width = 0.2F;
            // 
            // Line9
            // 
            this.Line9.Height = 0.21F;
            this.Line9.Left = 0.99F;
            this.Line9.LineWeight = 1F;
            this.Line9.Name = "Line9";
            this.Line9.Top = 0F;
            this.Line9.Width = 0F;
            this.Line9.X1 = 0.99F;
            this.Line9.X2 = 0.99F;
            this.Line9.Y1 = 0F;
            this.Line9.Y2 = 0.21F;
            // 
            // Line10
            // 
            this.Line10.Height = 0.21F;
            this.Line10.Left = 3.49F;
            this.Line10.LineWeight = 1F;
            this.Line10.Name = "Line10";
            this.Line10.Top = 0F;
            this.Line10.Width = 0F;
            this.Line10.X1 = 3.49F;
            this.Line10.X2 = 3.49F;
            this.Line10.Y1 = 0F;
            this.Line10.Y2 = 0.21F;
            // 
            // Line11
            // 
            this.Line11.Height = 0.21F;
            this.Line11.Left = 4.490001F;
            this.Line11.LineWeight = 1F;
            this.Line11.Name = "Line11";
            this.Line11.Top = 0F;
            this.Line11.Width = 0F;
            this.Line11.X1 = 4.490001F;
            this.Line11.X2 = 4.490001F;
            this.Line11.Y1 = 0F;
            this.Line11.Y2 = 0.21F;
            // 
            // Line12
            // 
            this.Line12.Height = 0.21F;
            this.Line12.Left = 5.54F;
            this.Line12.LineWeight = 1F;
            this.Line12.Name = "Line12";
            this.Line12.Top = 0F;
            this.Line12.Width = 0F;
            this.Line12.X1 = 5.54F;
            this.Line12.X2 = 5.54F;
            this.Line12.Y1 = 0F;
            this.Line12.Y2 = 0.21F;
            // 
            // Line13
            // 
            this.Line13.Height = 0.21F;
            this.Line13.Left = 6.590001F;
            this.Line13.LineWeight = 1F;
            this.Line13.Name = "Line13";
            this.Line13.Top = 0F;
            this.Line13.Width = 0F;
            this.Line13.X1 = 6.590001F;
            this.Line13.X2 = 6.590001F;
            this.Line13.Y1 = 0F;
            this.Line13.Y2 = 0.21F;
            // 
            // Line14
            // 
            this.Line14.Height = 0.21F;
            this.Line14.Left = 7.64F;
            this.Line14.LineWeight = 1F;
            this.Line14.Name = "Line14";
            this.Line14.Top = 0F;
            this.Line14.Width = 0F;
            this.Line14.X1 = 7.64F;
            this.Line14.X2 = 7.64F;
            this.Line14.Y1 = 0F;
            this.Line14.Y2 = 0.21F;
            // 
            // Line16
            // 
            this.Line16.Height = 0F;
            this.Line16.Left = 0.99F;
            this.Line16.LineWeight = 1F;
            this.Line16.Name = "Line16";
            this.Line16.Top = 0F;
            this.Line16.Visible = false;
            this.Line16.Width = 4.55F;
            this.Line16.X1 = 0.99F;
            this.Line16.X2 = 5.54F;
            this.Line16.Y1 = 0F;
            this.Line16.Y2 = 0F;
            // 
            // Line15
            // 
            this.Line15.Height = 0F;
            this.Line15.Left = 0.99F;
            this.Line15.LineWeight = 1F;
            this.Line15.Name = "Line15";
            this.Line15.Top = 0.21F;
            this.Line15.Visible = false;
            this.Line15.Width = 4.55F;
            this.Line15.X1 = 0.99F;
            this.Line15.X2 = 5.54F;
            this.Line15.Y1 = 0.21F;
            this.Line15.Y2 = 0.21F;
            // 
            // Line21
            // 
            this.Line21.Height = 0F;
            this.Line21.Left = 6.590001F;
            this.Line21.LineWeight = 1F;
            this.Line21.Name = "Line21";
            this.Line21.Top = 0.21F;
            this.Line21.Visible = false;
            this.Line21.Width = 1.049999F;
            this.Line21.X1 = 6.590001F;
            this.Line21.X2 = 7.64F;
            this.Line21.Y1 = 0.21F;
            this.Line21.Y2 = 0.21F;
            // 
            // Line22
            // 
            this.Line22.Height = 0F;
            this.Line22.Left = 6.590001F;
            this.Line22.LineWeight = 1F;
            this.Line22.Name = "Line22";
            this.Line22.Top = 0F;
            this.Line22.Visible = false;
            this.Line22.Width = 1.049999F;
            this.Line22.X1 = 6.590001F;
            this.Line22.X2 = 7.64F;
            this.Line22.Y1 = 0F;
            this.Line22.Y2 = 0F;
            // 
            // Line23
            // 
            this.Line23.Height = 0F;
            this.Line23.Left = 5.54F;
            this.Line23.LineWeight = 1F;
            this.Line23.Name = "Line23";
            this.Line23.Top = 0.21F;
            this.Line23.Visible = false;
            this.Line23.Width = 1.050001F;
            this.Line23.X1 = 5.54F;
            this.Line23.X2 = 6.590001F;
            this.Line23.Y1 = 0.21F;
            this.Line23.Y2 = 0.21F;
            // 
            // Line24
            // 
            this.Line24.Height = 0F;
            this.Line24.Left = 5.54F;
            this.Line24.LineWeight = 1F;
            this.Line24.Name = "Line24";
            this.Line24.Top = 0F;
            this.Line24.Visible = false;
            this.Line24.Width = 1.050001F;
            this.Line24.X1 = 5.54F;
            this.Line24.X2 = 6.590001F;
            this.Line24.Y1 = 0F;
            this.Line24.Y2 = 0F;
            // 
            // txtCreatedBy
            // 
            this.txtCreatedBy.Height = 0.2F;
            this.txtCreatedBy.Left = 2.25F;
            this.txtCreatedBy.Name = "txtCreatedBy";
            this.txtCreatedBy.Style = "font-size: 9pt; font-weight: normal; vertical-align: bottom";
            this.txtCreatedBy.Top = 0.112F;
            this.txtCreatedBy.Width = 4.2F;
            // 
            // Label7
            // 
            this.Label7.Height = 0.2F;
            this.Label7.HyperLink = null;
            this.Label7.Left = 1F;
            this.Label7.Name = "Label7";
            this.Label7.Style = "font-size: 9pt; font-weight: normal; vertical-align: bottom";
            this.Label7.Text = "???????? ?????????:";
            this.Label7.Top = 0.1F;
            this.Label7.Width = 1.25F;
            // 
            // ActiveReport1
            // 
            this.MasterReport = false;
            this.PageSettings.PaperHeight = 11F;
            this.PageSettings.PaperWidth = 8.5F;
            this.PrintWidth = 7.651F;
            this.Sections.Add(this.ReportHeader);
            this.Sections.Add(this.PageHeader);
            this.Sections.Add(this.Detail);
            this.Sections.Add(this.PageFooter);
            this.Sections.Add(this.ReportFooter);
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule(resources.GetString("$this.StyleSheet"), "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: inherit; font-style: inherit; font-variant: inherit; font-weight: bo" +
                        "ld; font-size: 16pt; font-size-adjust: inherit; font-stretch: inherit", "Heading1", "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: Times New Roman; font-style: italic; font-variant: inherit; font-wei" +
                        "ght: bold; font-size: 14pt; font-size-adjust: inherit; font-stretch: inherit", "Heading2", "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: inherit; font-style: inherit; font-variant: inherit; font-weight: bo" +
                        "ld; font-size: 13pt; font-size-adjust: inherit; font-stretch: inherit", "Heading3", "Normal"));
            ((System.ComponentModel.ISupportInitialize)(this.txtClientName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbClientName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbTitle)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbProjectName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtProjectName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbNoData)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TextBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TextBox2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TextBox3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TextBox4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TextBox5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TextBox6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCreatedBy)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();

            // Attach Report Events
            this.ReportFooter.Format += new System.EventHandler(this.ReportFooter_Format);
            this.Detail.Format += new System.EventHandler(this.Detail_Format);
            this.PageHeader.Format += new System.EventHandler(this.PageHeader_Format);
            this.ReportHeader.Format += new System.EventHandler(this.ReportHeader_Format);
        }

		#endregion

        private void SubBuildingPercentFinishedRpt_ReportEnd(object sender, EventArgs e)
        {
            if (!this._forPDF) return;

            Logo1 logo = new Logo1();
            logo.Run();

            for (int i = 0; i < this.Document.Pages.Count; i++)
                this.Document.Pages[i].Overlay(logo.Document.Pages[0]);
        }

        private ReportHeader ReportHeader;
        private TextBox txtClientName;
        private Label Label10;
        private Label lbClientName;
        private Label lbTitle;
        private Label lbProjectName;
        private TextBox txtProjectName;
        private Label lbNoData;
        private Label Label8;
        private PageHeader PageHeader;
        private Label Label1;
        private Label Label2;
        private Label Label3;
        private Label Label4;
        private Label Label5;
        private Line Line1;
        private Line Line2;
        private Line Line3;
        private Line Line4;
        private Line Line5;
        private Line Line6;
        private Line Line7;
        private Line Line8;
        private Line Line17;
        private Line Line18;
        private Line Line19;
        private Line Line20;
        private Detail Detail;
        private TextBox TextBox1;
        private TextBox TextBox2;
        private TextBox TextBox3;
        private TextBox TextBox4;
        private TextBox TextBox5;
        private TextBox TextBox6;
        private Line Line9;
        private Line Line10;
        private Line Line11;
        private Line Line12;
        private Line Line13;
        private Line Line14;
        private Line Line16;
        private Line Line15;
        private Line Line21;
        private Line Line22;
        private Line Line23;
        private Line Line24;
        private PageFooter PageFooter;
        private ReportFooter ReportFooter;
        private TextBox txtCreatedBy;
        private Label Label7;
	}
}
