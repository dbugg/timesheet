using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Data.SqlClient;
using log4net;
using Asa.Timesheet.Data;
using Asa.Timesheet.Data.Entities;
using Asa.Timesheet.Data.Util;
using GrapeCity.ActiveReports.Export.Pdf.Section;
using GrapeCity.ActiveReports.Export.Excel.Section;

namespace Asa.Timesheet.WebPages.Reports
{
	/// <summary>
	/// Summary description for Efficiency.
	/// </summary>
	public class Efficiency : TimesheetPageBase
	{
		protected System.Web.UI.WebControls.Label lblTitle;
		protected System.Web.UI.WebControls.Label lblUser;
		protected System.Web.UI.WebControls.PlaceHolder menuHolder;
		protected System.Web.UI.WebControls.Label lblError;
		protected System.Web.UI.WebControls.Label lblInfo;
		protected System.Web.UI.WebControls.CheckBox ckDates;
		protected System.Web.UI.WebControls.Label lblStartDay;
		protected System.Web.UI.WebControls.Label lblSelStartDate;
		protected System.Web.UI.WebControls.Calendar calStartDate;
		protected System.Web.UI.WebControls.Label lblEndDay;
		protected System.Web.UI.WebControls.Label lblSelEndDate;
		protected System.Web.UI.WebControls.Calendar calEndDate;
		protected System.Web.UI.WebControls.Label Label8;
		protected System.Web.UI.WebControls.Label lblSluj;
		protected System.Web.UI.WebControls.DropDownList ddlUser;
		protected System.Web.UI.WebControls.DropDownList ddlUserStatus;
		protected System.Web.UI.WebControls.ImageButton ibGenerate;
		protected System.Web.UI.WebControls.ImageButton ibPdfExport;
		protected System.Web.UI.WebControls.ImageButton ibXlsExport;
		protected System.Web.UI.WebControls.DataGrid grdWorkTime;
		protected System.Web.UI.WebControls.Label lblReportTitle;
		protected System.Web.UI.WebControls.Label Label10;
		protected System.Web.UI.WebControls.DropDownList ddlCompany;
		protected Asa.Timesheet.WebPages.UserControls.PageHeader header;

        private void Page_Load(object sender, System.EventArgs e)
        {

            if (!this.LoggedUser.IsLeader) ErrorRedirect(Resource.ResourceManager["noRightsAlert"]);
            if (!this.IsPostBack)
            {
                BindArchitects();
                header.PageTitle = lblReportTitle.Text;
                header.UserName = this.LoggedUser.UserName;






                calStartDate.SelectedDate = calStartDate.VisibleDate = new DateTime(DateTime.Today.Year, DateTime.Today.Month, 1);
                lblSelStartDate.Text = "(" + Resource.ResourceManager["reports_SelectDate"] + ")";
                calEndDate.SelectedDate = calEndDate.VisibleDate = DateTime.Today;
                lblSelEndDate.Text = "(" + Resource.ResourceManager["reports_SelectDate"] + ")";
                //UIHelpers.LoadProjectStatus(ddlProjectsStatus,(int)ProjectsData.ProjectsByStatus.Active);
                //UIHelpers.LoadBuildingTypes("",ddlBuildingTypes);
                //LoadProjects();
            }



            UIHelpers.CreateMenuReports(menuHolder, LoggedUser);

        }
//		private bool LoadUsers()
//		{
//			//if (this.LoggedUser.IsLeader)
//			//{
//			ddlUser.Visible = true;
//
//			SqlDataReader reader = null;
//			try
//			{
//				switch (ddlUserStatus.SelectedValue)
//				{
//					case "0":
//						reader = UsersData.SelectUserNames(true, true, (LoggedUser.HasPaymentRights||UIHelpers.GetIDParam()!=-1));break;
//					case "1":
//						reader = UsersData.SelectUserNames(false, true, (LoggedUser.HasPaymentRights||UIHelpers.GetIDParam()!=-1));break;
//					case "2":
//						reader = UsersData.SelectUserNames1();break;
//				}			
//				ddlUser.DataSource = reader;
//				ddlUser.DataValueField = "UserID";
//				ddlUser.DataTextField = "FullName";
//				ddlUser.DataBind();
//				ddlUser.Items.Insert(0,new ListItem("<"+Resource.ResourceManager["reports_ddlAllUsers"]+">","0"));
//				ddlUser.SelectedValue = "0";
//					
//			}
//			catch 
//			{
//				
//				return false;
//			}
//			finally
//			{
//				if (reader!=null) reader.Close();
//			}
//			//}
//
//			return true;
//		}
//		
		#region Web Form Designer generated code
        override protected void OnInit(EventArgs e)
        {
            //
            // CODEGEN: This call is required by the ASP.NET Web Form Designer.
            //
            InitializeComponent();
            base.OnInit(e);
        }
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
        private void InitializeComponent()
        {
            this.ckDates.CheckedChanged += new System.EventHandler(this.ckDates_CheckedChanged);
            this.calStartDate.SelectionChanged += new System.EventHandler(this.calStartDate_SelectionChanged);
            this.calEndDate.SelectionChanged += new System.EventHandler(this.calEndDate_SelectionChanged);
            this.ddlCompany.SelectedIndexChanged += new System.EventHandler(this.ddlCompany_SelectedIndexChanged);
            this.ddlUserStatus.SelectedIndexChanged += new System.EventHandler(this.ddlUserStatus_SelectedIndexChanged);
            this.ibGenerate.Click += new System.Web.UI.ImageClickEventHandler(this.ibGenerate_Click);
            this.Load += new System.EventHandler(this.Page_Load);

        }
		#endregion

        private void ddlUserStatus_SelectedIndexChanged(object sender, System.EventArgs e)
        {
            bool bASI = ddlCompany.SelectedValue == "1";
            if (bASI)
                BindIngeners();
            else
                BindArchitects();
        }

        private void ibGenerate_Click(object sender, System.Web.UI.ImageClickEventArgs e)
        {
            int nPersonID = int.Parse(ddlUser.SelectedValue);

            DateTime dtStart = calStartDate.SelectedDate;
            DateTime dtEnd = calEndDate.SelectedDate;
            if (ckDates.Checked)
                dtStart = dtEnd = Constants.DateMax;
            DataView dvExpenses = null;
            bool bASI = ddlCompany.SelectedValue == "1";
            if (nPersonID > 0)
                dvExpenses = EffUDL.ExecuteReportsWorkTimesHoursByUsersSummary(nPersonID, dtStart, dtEnd);
            else
            {
                foreach (ListItem li in ddlUser.Items)
                {
                    int nID = int.Parse(li.Value);
                    if (nID > 0)
                    {
                        DataView dv = EffUDL.ExecuteReportsWorkTimesHoursByUsersSummary(nID, dtStart, dtEnd);
                        if (dvExpenses == null)
                            dvExpenses = dv;
                        else
                            foreach (DataRow d in dv.Table.Rows)
                                dvExpenses.Table.Rows.Add(d.ItemArray);
                    }

                }
            }
            DataView dvTotals = EffUDL.ExecuteReportsWorkTimesHoursSummary(dtStart, dtEnd, bASI);
            DataView dvPayments = EffUDL.ExecuteReportsPayments(dtStart, dtEnd);
            DataTable dtResult = new DataTable();
            dtResult.Columns.Add(new DataColumn("Name", typeof(string)));
            dtResult.Columns.Add(new DataColumn("Project", typeof(string)));
            dtResult.Columns.Add(new DataColumn("Income", typeof(decimal)));
            dtResult.Columns.Add(new DataColumn("Hours", typeof(decimal)));
            dtResult.Columns.Add(new DataColumn("Expenses", typeof(decimal)));
            dtResult.Columns.Add(new DataColumn("Result", typeof(decimal)));
            string sName = "";
            decimal dMin = 0;
            decimal dExp = 0;
            decimal dInc = 0;
            decimal dMinUser = 0;
            decimal dExpUser = 0;
            decimal dIncUser = 0;
            int LastUserID = 0;
            string sLastName = "";
            if (dvExpenses.Count > 0)
                LastUserID = (int)dvExpenses[0]["UserID"];
            DataRow dr = null;
            foreach (DataRowView drv in dvExpenses)
            {
                dvPayments.RowFilter = "ProjectID=" + drv["ProjectID"];
                dvTotals.RowFilter = "ProjectID=" + drv["ProjectID"];
                int nUserID = (int)drv["UserID"];
                //				if(ddlUser.Items.FindByValue(nUserID.ToString())==null)
                //					continue;
                decimal dIncomes = 0;
                if (bASI)
                {
                    if (dvPayments.Count > 0 && dvPayments[0]["ASIPayments"] != DBNull.Value)
                        dIncomes += (decimal)dvPayments[0]["ASIPayments"];
                }
                else
                {
                    if (dvPayments.Count > 0 && dvPayments[0]["PaymentsNotPhase"] != DBNull.Value)
                        dIncomes += (decimal)dvPayments[0]["PaymentsNotPhase"];
                    if (dvPayments.Count > 0 && dvPayments[0]["PaymentsPhase"] != DBNull.Value)
                        dIncomes += (decimal)dvPayments[0]["PaymentsPhase"];
                }
                decimal dMinutesTotal = 0;
                decimal dMinutesPr = 0;
                if (drv["WorkedMinutes"] != DBNull.Value)
                    dMinutesPr = (decimal)(int)drv["WorkedMinutes"];
                if (dvTotals.Count > 0 && dvTotals[0]["WorkedMinutes"] != DBNull.Value)
                    dMinutesTotal += (decimal)(int)dvTotals[0]["WorkedMinutes"];
                if (dMinutesTotal == 0)
                    continue;

                dr = dtResult.NewRow();
                dr["Project"] = drv["ProjectName"];
                dr["Name"] = drv["FullName"];
                sName = (string)drv["FullName"];
                dr["Hours"] = dMinutesPr / 60;
                dr["Income"] = dIncomes * dMinutesPr / dMinutesTotal;
                decimal dExpenses = 0;
                if (drv["Expenses"] != DBNull.Value)
                {
                    dExpenses = (decimal)drv["Expenses"];

                }
                dr["Expenses"] = dExpenses;
                dr["Result"] = dIncomes * dMinutesPr / dMinutesTotal - dExpenses;


                if (nPersonID > 0)
                {
                    dtResult.Rows.Add(dr);
                }
                else
                {
                    if ((int)drv["UserID"] != LastUserID)
                    {
                        LastUserID = (int)drv["UserID"];
                        DataRow dr1 = dtResult.NewRow();
                        dr1["Project"] = "<b>" + Resource.ResourceManager["reports_Total"] + "</b>";
                        dr1["Name"] = sLastName;
                        dr1["Hours"] = dMinUser / 60;
                        dr1["Income"] = dIncUser;
                        dr1["Expenses"] = dExpUser;
                        dr1["Result"] = dIncUser - dExpUser;
                        dtResult.Rows.Add(dr1);
                        dMinUser = dExpUser = dIncUser = 0;
                    }
                }

                dMin += dMinutesPr;
                dExp += dExpenses;
                dInc += dIncomes * dMinutesPr / dMinutesTotal;

                dMinUser += dMinutesPr;
                dExpUser += dExpenses;
                dIncUser += dIncomes * dMinutesPr / dMinutesTotal;
                sLastName = sName;

            }
            if (nPersonID > 0)
            {
                DataRow dr2 = dtResult.NewRow();
                dr2["Project"] = "<b>" + Resource.ResourceManager["reports_Total"] + "</b>";

                dr2["Name"] = sName;

                dr2["Hours"] = dMin / 60;
                dr2["Income"] = dInc;
                dr2["Expenses"] = dExp;
                dr2["Result"] = dInc - dExp;
                dtResult.Rows.Add(dr2);
            }
            else
            {
                //if(dr!=null)
                {
                    DataRow dr1 = dtResult.NewRow();
                    dr1["Project"] = "<b>" + Resource.ResourceManager["reports_Total"] + "</b>";
                    dr1["Name"] = sLastName;
                    dr1["Hours"] = dMinUser / 60;
                    dr1["Income"] = dIncUser;
                    dr1["Expenses"] = dExpUser;
                    dr1["Result"] = dIncUser - dExpUser;
                    dtResult.Rows.Add(dr1);
                }
            }
            grdWorkTime.DataSource = dtResult;
            grdWorkTime.DataBind();


        }

        private void calStartDate_SelectionChanged(object sender, System.EventArgs e)
        {
            lblSelStartDate.Text = String.Concat("(", calStartDate.SelectedDates[0].ToString("d"), ")");
            if (calStartDate.SelectedDates.Count > 1)
            {
                calEndDate.SelectedDate = calStartDate.SelectedDates[calStartDate.SelectedDates.Count - 1];
                calEndDate.VisibleDate = calEndDate.SelectedDate;

                calStartDate.SelectedDate = calStartDate.SelectedDates[0];
                lblSelEndDate.Text = String.Concat("(", calEndDate.SelectedDate.ToString("d"), ")"); ;
            }
        }

        private void calEndDate_SelectionChanged(object sender, System.EventArgs e)
        {
            lblSelEndDate.Text = String.Concat("(", calEndDate.SelectedDates[0].ToString("d"), ")");
        }

        private void ckDates_CheckedChanged(object sender, System.EventArgs e)
        {
            bool bEnable = !ckDates.Checked;
            calStartDate.Enabled = calEndDate.Enabled = bEnable;

            if (bEnable)
            {
                lblSelStartDate.Text = String.Concat("(" + calStartDate.SelectedDate.ToString("d"), ")");

                lblSelEndDate.Text = String.Concat("(" + calEndDate.SelectedDate.ToString("d"), ")");
            }
            else
            {
                lblSelStartDate.Text = lblSelEndDate.Text = String.Concat("(", Resource.ResourceManager["reports_SelectDate"], ")");
            }

        }

        private void ddlCompany_SelectedIndexChanged(object sender, System.EventArgs e)
        {
            bool bASI = ddlCompany.SelectedValue == "1";
            if (bASI)
                BindIngeners();
            else
                BindArchitects();

        }
        private void BindArchitects()
        {
            SqlDataReader reader = null;
            try
            {
                string nameOfProcedure = "UsersListArcgitectProc1";
                reader = UsersData.SelectUsersSplit(int.Parse(ddlUserStatus.SelectedValue), nameOfProcedure);

                if (reader != null)
                {
                    ddlUser.DataSource = reader;
                    ddlUser.DataValueField = "UserID";
                    ddlUser.DataTextField = "FullName";
                    ddlUser.DataBind();
                    ddlUser.Items.Insert(0, new ListItem("<" + Resource.ResourceManager["reports_ddlAllUsers"] + ">", "0"));
                    ddlUser.SelectedValue = "0";
                }

            }
            catch
            {

                lblError.Text = Resource.ResourceManager["users_ErrorLoadUsers"];
                return;
            }
            finally
            {
                if (reader != null) reader.Close();
            }
        }

        private void BindIngeners()
        {
            SqlDataReader reader = null;
            try
            {
                string nameOfProcedure = "UsersListIngenerProc1";
                reader = UsersData.SelectUsersSplit(int.Parse(ddlUserStatus.SelectedValue), nameOfProcedure);

                if (reader != null)
                {
                    ddlUser.DataSource = reader;
                    ddlUser.DataValueField = "UserID";
                    ddlUser.DataTextField = "FullName";
                    ddlUser.DataBind();
                    ddlUser.Items.Insert(0, new ListItem("<" + Resource.ResourceManager["reports_ddlAllUsers"] + ">", "0"));
                    ddlUser.SelectedValue = "0";
                }

            }
            catch
            {

                lblError.Text = Resource.ResourceManager["users_ErrorLoadUsers"];
                return;
            }
            finally
            {
                if (reader != null) reader.Close();
            }
        }
	}
}
