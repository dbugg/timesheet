using System;
using System.Data;
using Asa.Timesheet.Data;
using Asa.Timesheet.Data.Reports;
using GrapeCity.ActiveReports;
using GrapeCity.ActiveReports.Controls;
using GrapeCity.ActiveReports.SectionReportModel;
using GrapeCity.ActiveReports.Document.Section;
using GrapeCity.ActiveReports.Document;

namespace Asa.Timesheet.WebPages.Reports
{
    public class MissingDataReport : GrapeCity.ActiveReports.SectionReport
	{

    private int _projectID = 0;
    private int _currentSubReport = 1;

    private string _projectName = String.Empty;
    bool _requiredDataOnly = true;

    public MissingDataReport(int projectID)
    {
        InitializeComponent();

        _projectID = projectID;


    }

    private void MissingDataReport_ReportStart(object sender, System.EventArgs eArgs)
    {
        ProjectsReportDS dsReport =
          ProjectsReportData.CreateMissingDataReportDataSource(_projectID);

        this._projectName = ((ProjectsReportDS.ProjectRow)dsReport.Project.Rows[0]).AdministrativeName;

        #region Izh danni

        for (int i = dsReport.InitialDataItems.Rows.Count - 1; i >= 0; i--)
        {
            ProjectsReportDS.InitialDataItemsRow row = (ProjectsReportDS.InitialDataItemsRow)dsReport.InitialDataItems.Rows[i];
            string missing = String.Empty;

            bool itemStatus = _requiredDataOnly ? !(row.IsDate1Null() || row.IsDate2Null()) :
              !(row.IsDate1Null() || row.IsDate2Null() || (!row.IsNumberLabelNull() && row.IsNumberNull()));

            if (!itemStatus)
            {
                if (row.IsDate1Null()) missing += row.Date1Label.Trim(':') + ", ";
                if (row.IsDate2Null()) missing += row.Date2Label.Trim(':') + ", ";

                if (!_requiredDataOnly && !row.IsNumberLabelNull() && row.IsNumberNull()) missing += row.NumberLabel.Trim(':');

                row.Date2Label = missing.Trim().Trim(','); ;
            }
            else dsReport.InitialDataItems.RemoveInitialDataItemsRow(row);

        }

        GrapeCity.ActiveReports.SectionReportModel.SubReport srTemp = null;

        if (dsReport.InitialDataItems.Rows.Count > 0)
        {
            srTemp = (GrapeCity.ActiveReports.SectionReportModel.SubReport)this.Detail.Controls["SubReport" + _currentSubReport.ToString()];
            srTemp.Report = new MissingIzhodDataSubeport();
            srTemp.Report.DataSource = dsReport.InitialDataItems;

            _currentSubReport++;
        }

        #endregion


        ProjectsReportDS.InfrastructureDataItemsDataTable tblInfra = dsReport.InfrastructureDataItems;
        for (int i = tblInfra.Rows.Count - 1; i >= 0; i--)
        {
            ProjectsReportDS.InfrastructureDataItemsRow rowInfra =
              (ProjectsReportDS.InfrastructureDataItemsRow)tblInfra.Rows[i];

            bool _requiredStatus = !(rowInfra.IsIzhDanniDate1Null() || rowInfra.IsIzhDanniDate2Null() ||
              (rowInfra.PismoShow && rowInfra.IsPismoDate1Null()) || (rowInfra.PrisDogovorShow && rowInfra.IsPrisDogovorDate1Null()) ||
              (rowInfra.ProjectShow && (rowInfra.IsProjectDate1Null() || rowInfra.IsProjectDate2Null())));

            if (!_requiredDataOnly)
            {
                bool otherDataStatus = !(rowInfra.IsNumberNull() || rowInfra.IsAmountNull());

                _requiredStatus &= otherDataStatus;
            }

            if (_requiredStatus) tblInfra.RemoveInfrastructureDataItemsRow(rowInfra);
        }

        int rc = tblInfra.Rows.Count;

        if (tblInfra.Rows.Count > 0)
        {
            srTemp = (GrapeCity.ActiveReports.SectionReportModel.SubReport)this.Detail.Controls["SubReport" + _currentSubReport.ToString()];
            srTemp.Report = new MissingInfraDataSubreport(_requiredDataOnly);
            srTemp.Report.DataSource = tblInfra;
            _currentSubReport++;
        }


        #region Build permission

        ProjectsReportDS.BuildPermissionDataDataTable tblBuild = dsReport.BuildPermissionData;

        DataTable dtTemp = new DataTable();
        dtTemp.Columns.Add("Title", System.Type.GetType("System.String"));
        dtTemp.Columns.Add("Missing", System.Type.GetType("System.String"));

        ProjectsReportDS.BuildPermissionDataRow buildRow = (ProjectsReportDS.BuildPermissionDataRow)tblBuild.Rows[0];

        #region Ideen proekt

        bool ideaStatus = _requiredDataOnly ? !(buildRow.IsIdeaProjectDoneNull() || buildRow.IsIdeaProjectAgreedNull()) :
          !(buildRow.IsIdeaProjectDoneNull() || buildRow.IsIdeaProjectAgreedNull() || buildRow.IsIdeaProjectMunWhereNull() || buildRow.IsIdeaProjectMunNull());

        if (!ideaStatus)
        {
            DataRow dr = dtTemp.NewRow();
            dr["Title"] = Resource.ResourceManager["Reports_IdeenProekt"];

            string missing = String.Empty;
            if (buildRow.IsIdeaProjectDoneNull()) missing += Resource.ResourceManager["ProjectsReport_Vnesen"].Trim(':') + ", ";
            if (buildRow.IsIdeaProjectAgreedNull()) missing += Resource.ResourceManager["ProjectsReport_Suglasuvan"].Trim(':') + ", ";

            if (!_requiredDataOnly)
            {
                if (buildRow.IsIdeaProjectMunWhereNull()) missing += Resource.ResourceManager["Reports_VhNomer"].Trim(':') + ", ";
                if (buildRow.IsIdeaProjectMunNull()) missing += Resource.ResourceManager["Reports_Obshtina"] + ", ";
            }

            dr["Missing"] = missing.Trim().Trim(',');

            dtTemp.Rows.Add(dr);

        }

        #endregion

        #region Tehnicheski proekt

        bool techStatus = _requiredDataOnly ? !(buildRow.IsTechProjectDoneNull() || buildRow.IsTechProjectAgreedNull()) :
          !(buildRow.IsTechProjectDoneNull() || buildRow.IsTechProjectAgreedNull() || buildRow.IsTechProjectMunWhereNull() || buildRow.IsTechProjectMunNull());

        if (!techStatus)
        {
            DataRow dr = dtTemp.NewRow();
            dr["Title"] = Resource.ResourceManager["Reports_TehnicheskiProekt"];

            string missing = String.Empty;
            if (buildRow.IsTechProjectDoneNull()) missing += Resource.ResourceManager["ProjectsReport_Vnesen"].Trim(':') + ", ";
            if (buildRow.IsTechProjectAgreedNull()) missing += Resource.ResourceManager["ProjectsReport_Suglasuvan"].Trim(':') + ", ";

            if (!_requiredDataOnly)
            {
                if (buildRow.IsTechProjectMunWhereNull()) missing += Resource.ResourceManager["Reports_VhNomer"] + ", ";
                if (buildRow.IsTechProjectMunNull()) missing += Resource.ResourceManager["Reports_Obshtina"] + ", ";
            }

            dr["Missing"] = missing.Trim().Trim(',');

            dtTemp.Rows.Add(dr);

        }

        #endregion

        #region Razreshenie za stroej

        bool buildPermissinStatus = _requiredDataOnly ? !buildRow.IsBuildPermissionDoneNull() :
                  !(buildRow.IsBuildPermissionDoneNull() || buildRow.IsBuildPermissionNumberNull());

        if (!buildPermissinStatus)
        {
            DataRow dr = dtTemp.NewRow();
            dr["Title"] = Resource.ResourceManager["Reports_BuildPermission"];

            string missing = String.Empty;
            if (buildRow.IsBuildPermissionDoneNull()) missing += Resource.ResourceManager["ProjectsReport_Polucheno"].Trim(':') + ", ";

            if (!_requiredDataOnly)
            {
                if (buildRow.IsBuildPermissionNumberNull()) missing += Resource.ResourceManager["Reports_IzhNomer"].Trim(':') + ", ";
            }

            dr["Missing"] = missing.Trim().Trim(',');

            dtTemp.Rows.Add(dr);

        }

        #endregion

        #region Drugi

        bool otherStatus = _requiredDataOnly ? !(buildRow.IsOtherNull() || buildRow.IsOtherDoneNull()) :
          !(buildRow.IsOtherNull() || buildRow.IsOtherDoneNull() || buildRow.IsOtherNumberNull() || buildRow.IsOtherExpensesNull());

        if (!otherStatus)
        {
            DataRow dr = dtTemp.NewRow();
            dr["Title"] = Resource.ResourceManager["Reports_Drugi"];

            string missing = String.Empty;

            if (buildRow.IsOtherNull()) missing += Resource.ResourceManager["ProjectsReport_Vnesen"].Trim(':') + ", ";
            if (buildRow.IsOtherDoneNull()) missing += Resource.ResourceManager["ProjectsReport_Poluchen"].Trim(':') + ", ";

            if (!_requiredDataOnly)
            {
                if (buildRow.IsOtherNumberNull()) missing += Resource.ResourceManager["Reports_IzhNomer"] + ", ";
                if (buildRow.IsOtherExpensesNull()) missing += Resource.ResourceManager["Reports_DrugiRazhodi"] + ", ";

            }

            dr["Missing"] = missing.Trim().Trim(',');

            dtTemp.Rows.Add(dr);

        }

        #endregion

        if (dtTemp.Rows.Count > 0)
        {
            srTemp = (GrapeCity.ActiveReports.SectionReportModel.SubReport)this.Detail.Controls["SubReport" + _currentSubReport.ToString()];
            srTemp.Report = new MissingDocumDataSubreport();
            srTemp.Report.DataSource = dtTemp;

            _currentSubReport++;
        }

        #endregion

        if (_currentSubReport == 1)
        {
            this.txtNoMissingData.Visible = true;
        }

    }

    private void MissingDataReport_ReportEnd(object sender, System.EventArgs eArgs)
    {
        Logo logo = new Logo();
        logo.Run();

        for (int i = 0; i < this.Document.Pages.Count; i++)
            this.Document.Pages[i].Overlay(logo.Document.Pages[0]);
    }

    private void PageFooter_Format(object sender, System.EventArgs eArgs)
    {

        this.lblPageFooter.Text = String.Concat(Resource.ResourceManager["Reports_MissingInformation"], " - ",
          this._projectName);

        if (this.PageNumber == 1) this.lblPage.Visible = false;
        else
        {
            this.lblPage.Visible = true;
            lblPage.Text = string.Concat(Resource.ResourceManager["rpt_Page"], " ", this.PageNumber.ToString());
        }
    }

    private void ReportHeader_Format(object sender, System.EventArgs eArgs)
    {
        this.txtProjectName.Text = _projectName;
    }

		#region ActiveReports Designer generated code














    public void InitializeComponent()
    {
        System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MissingDataReport));
        this.Detail = new GrapeCity.ActiveReports.SectionReportModel.Detail();
        this.ReportHeader = new GrapeCity.ActiveReports.SectionReportModel.ReportHeader();
        this.ReportFooter = new GrapeCity.ActiveReports.SectionReportModel.ReportFooter();
        this.PageHeader = new GrapeCity.ActiveReports.SectionReportModel.PageHeader();
        this.PageFooter = new GrapeCity.ActiveReports.SectionReportModel.PageFooter();
        this.txtReportTitle = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
        this.txtProjectName = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
        this.SubReport1 = new GrapeCity.ActiveReports.SectionReportModel.SubReport();
        this.SubReport2 = new GrapeCity.ActiveReports.SectionReportModel.SubReport();
        this.SubReport3 = new GrapeCity.ActiveReports.SectionReportModel.SubReport();
        this.txtNoMissingData = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
        this.lblPageFooter = new GrapeCity.ActiveReports.SectionReportModel.Label();
        this.lblPage = new GrapeCity.ActiveReports.SectionReportModel.Label();
        this.Line11 = new GrapeCity.ActiveReports.SectionReportModel.Line();
        ((System.ComponentModel.ISupportInitialize)(this.txtReportTitle)).BeginInit();
        ((System.ComponentModel.ISupportInitialize)(this.txtProjectName)).BeginInit();
        ((System.ComponentModel.ISupportInitialize)(this.txtNoMissingData)).BeginInit();
        ((System.ComponentModel.ISupportInitialize)(this.lblPageFooter)).BeginInit();
        ((System.ComponentModel.ISupportInitialize)(this.lblPage)).BeginInit();
        ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
        // 
        // Detail
        // 
        this.Detail.ColumnSpacing = 0F;
        this.Detail.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
						this.SubReport1,
						this.SubReport2,
						this.SubReport3,
						this.txtNoMissingData});
        this.Detail.Height = 1.427083F;
        this.Detail.Name = "Detail";
        // 
        // ReportHeader
        // 
        this.ReportHeader.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
						this.txtReportTitle,
						this.txtProjectName});
        this.ReportHeader.Height = 1F;
        this.ReportHeader.Name = "ReportHeader";
        // 
        // ReportFooter
        // 
        this.ReportFooter.Height = 0.2388889F;
        this.ReportFooter.Name = "ReportFooter";
        // 
        // PageHeader
        // 
        this.PageHeader.Height = 0.09375F;
        this.PageHeader.Name = "PageHeader";
        // 
        // PageFooter
        // 
        this.PageFooter.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
						this.lblPageFooter,
						this.lblPage,
						this.Line11});
        this.PageFooter.Height = 0.25F;
        this.PageFooter.Name = "PageFooter";
        // 
        // txtReportTitle
        // 
        this.txtReportTitle.Height = 0.2755906F;
        this.txtReportTitle.Left = 0F;
        this.txtReportTitle.Name = "txtReportTitle";
        this.txtReportTitle.Style = "font-family: Arial; font-size: 14pt; font-weight: bold; text-align: center";
        this.txtReportTitle.Text = "???????? ?????????? ?? ??????? ???????? ????????????";
        this.txtReportTitle.Top = 0F;
        this.txtReportTitle.Width = 6.692914F;
        // 
        // txtProjectName
        // 
        this.txtProjectName.Height = 0.2755906F;
        this.txtProjectName.Left = 0F;
        this.txtProjectName.Name = "txtProjectName";
        this.txtProjectName.Style = "font-family: Arial; font-size: 12pt; font-weight: bold; text-align: center";
        this.txtProjectName.Text = "txtProjectName";
        this.txtProjectName.Top = 0.3149606F;
        this.txtProjectName.Width = 6.692914F;
        // 
        // SubReport1
        // 
        this.SubReport1.CloseBorder = false;
        this.SubReport1.Height = 0.1968504F;
        this.SubReport1.Left = 0F;
        this.SubReport1.Name = "SubReport1";
        this.SubReport1.Report = null;
        this.SubReport1.Top = 0F;
        this.SubReport1.Width = 6.692914F;
        // 
        // SubReport2
        // 
        this.SubReport2.CloseBorder = false;
        this.SubReport2.Height = 0.1968504F;
        this.SubReport2.Left = 0F;
        this.SubReport2.Name = "SubReport2";
        this.SubReport2.Report = null;
        this.SubReport2.Top = 0.4966371F;
        this.SubReport2.Width = 6.692914F;
        // 
        // SubReport3
        // 
        this.SubReport3.CloseBorder = false;
        this.SubReport3.Height = 0.1968504F;
        this.SubReport3.Left = 0F;
        this.SubReport3.Name = "SubReport3";
        this.SubReport3.Report = null;
        this.SubReport3.Top = 1.005987F;
        this.SubReport3.Width = 6.692914F;
        // 
        // txtNoMissingData
        // 
        this.txtNoMissingData.Height = 0.2755906F;
        this.txtNoMissingData.Left = 0F;
        this.txtNoMissingData.Name = "txtNoMissingData";
        this.txtNoMissingData.Style = "font-family: Arial; font-size: 10pt; font-weight: bold; text-align: left";
        this.txtNoMissingData.Text = "???????? ???? ???????? ?????????? ?? ??????? ???????? ????????????.";
        this.txtNoMissingData.Top = 0F;
        this.txtNoMissingData.Visible = false;
        this.txtNoMissingData.Width = 6.692914F;
        // 
        // lblPageFooter
        // 
        this.lblPageFooter.Height = 0.1968504F;
        this.lblPageFooter.HyperLink = null;
        this.lblPageFooter.Left = 0F;
        this.lblPageFooter.Name = "lblPageFooter";
        this.lblPageFooter.Style = "font-size: 8pt; font-weight: bold; text-align: left";
        this.lblPageFooter.Text = "lblPageFooter";
        this.lblPageFooter.Top = 0.02460628F;
        this.lblPageFooter.Width = 5.51181F;
        // 
        // lblPage
        // 
        this.lblPage.Height = 0.1968504F;
        this.lblPage.HyperLink = null;
        this.lblPage.Left = 5.905513F;
        this.lblPage.Name = "lblPage";
        this.lblPage.Style = "font-size: 8pt; font-weight: bold; text-align: right";
        this.lblPage.Text = "lblPage";
        this.lblPage.Top = 0F;
        this.lblPage.Width = 0.7874014F;
        // 
        // Line11
        // 
        this.Line11.Height = 0F;
        this.Line11.Left = 0.006944444F;
        this.Line11.LineWeight = 1F;
        this.Line11.Name = "Line11";
        this.Line11.Top = 0.006944444F;
        this.Line11.Width = 7.086611F;
        this.Line11.X1 = 0.006944444F;
        this.Line11.X2 = 7.093555F;
        this.Line11.Y1 = 0.006944444F;
        this.Line11.Y2 = 0.006944444F;
        // 
        // ActiveReport1
        // 
        this.MasterReport = false;
        this.PageSettings.Margins.Bottom = 0.4097222F;
        this.PageSettings.Margins.Left = 1.377778F;
        this.PageSettings.Margins.Right = 0.5902778F;
        this.PageSettings.Margins.Top = 0.8034722F;
        this.PageSettings.PaperHeight = 11F;
        this.PageSettings.PaperWidth = 8.5F;
        this.PrintWidth = 6.692914F;
        this.Sections.Add(this.ReportHeader);
        this.Sections.Add(this.PageHeader);
        this.Sections.Add(this.Detail);
        this.Sections.Add(this.PageFooter);
        this.Sections.Add(this.ReportFooter);
        this.StyleSheet.Add(new DDCssLib.StyleSheetRule(resources.GetString("$this.StyleSheet"), "Normal"));
        this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: inherit; font-style: inherit; font-variant: inherit; font-weight: bo" +
                    "ld; font-size: 16pt; font-size-adjust: inherit; font-stretch: inherit", "Heading1", "Normal"));
        this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: Times New Roman; font-style: italic; font-variant: inherit; font-wei" +
                    "ght: bold; font-size: 14pt; font-size-adjust: inherit; font-stretch: inherit", "Heading2", "Normal"));
        this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: inherit; font-style: inherit; font-variant: inherit; font-weight: bo" +
                    "ld; font-size: 13pt; font-size-adjust: inherit; font-stretch: inherit", "Heading3", "Normal"));
        ((System.ComponentModel.ISupportInitialize)(this.txtReportTitle)).EndInit();
        ((System.ComponentModel.ISupportInitialize)(this.txtProjectName)).EndInit();
        ((System.ComponentModel.ISupportInitialize)(this.txtNoMissingData)).EndInit();
        ((System.ComponentModel.ISupportInitialize)(this.lblPageFooter)).EndInit();
        ((System.ComponentModel.ISupportInitialize)(this.lblPage)).EndInit();
        ((System.ComponentModel.ISupportInitialize)(this)).EndInit();

        // Attach Report Events
        this.ReportStart += new System.EventHandler(this.MissingDataReport_ReportStart);
        this.ReportEnd += new System.EventHandler(this.MissingDataReport_ReportEnd);
        this.PageFooter.Format += new System.EventHandler(this.PageFooter_Format);
        this.ReportHeader.Format += new System.EventHandler(this.ReportHeader_Format);
    }

		#endregion

        private ReportHeader ReportHeader;
        private TextBox txtReportTitle;
        private TextBox txtProjectName;
        private PageHeader PageHeader;
        private Detail Detail;
        private SubReport SubReport1;
        private SubReport SubReport2;
        private SubReport SubReport3;
        private TextBox txtNoMissingData;
        private PageFooter PageFooter;
        private Label lblPageFooter;
        private Label lblPage;
        private Line Line11;
        private ReportFooter ReportFooter;
	}
}
