using System;
using System.Web;
using Asa.Timesheet.Data.Entities;

namespace Asa.Timesheet.Data
{
	/// <summary>
	/// Summary description for SessionManager.
	/// </summary>
	public class SessionManager
	{
		public static string GetHelpLink()
		{
			return string.Concat("Help",LoggedUserInfo.UserRoleID,".html");
		}

		public static string GetReportsHelpLink()
		{
			return string.Concat("../Help",LoggedUserInfo.UserRoleID,".html");
		}

		public static UserInfo LoggedUserInfo
		{
			get
			{
				if (HttpContext.Current.Session["UserInfo"]!=null)
					return (UserInfo) HttpContext.Current.Session["UserInfo"];
				else
				{
					UserInfo ui;
					ui = UsersData.SelectUserByAccount(HttpContext.Current.User.Identity.Name);

					//HttpContext.Current.Session["UserInfo"] = ui;
					return ui;
				}
			}
		}
		public static ProjectsInfo CurrentProjectsInfo
		{
			get
			{
				if (HttpContext.Current.Session["ProjectsInfo"]!=null)
					return (ProjectsInfo) HttpContext.Current.Session["ProjectsInfo"];
				return null;
			}
			set 
			{
				HttpContext.Current.Session["ProjectsInfo"]=value;
			}
		}

		public static MailInfo CurrentMailInfo
		{
			get
			{
				if (HttpContext.Current.Session["MailInfo"]!=null)
					return (MailInfo) HttpContext.Current.Session["MailInfo"];
				return null;
			}
			set 
			{
				HttpContext.Current.Session["MailInfo"]=value;
			}
		}
	}
}
