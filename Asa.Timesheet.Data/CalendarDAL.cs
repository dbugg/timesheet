using System;
using System.Data;
using System.Data.SqlClient;

namespace Asa.Timesheet.Data
{
	/// <summary>
	/// Summary description for Calendar.
	/// </summary>
	public class CalendarDAL
	{
		private static SqlCommand Create_ProjectStartDate (int ProjectID)
		{
			SqlCommand cmd = new SqlCommand();
			cmd.CommandType = System.Data.CommandType.StoredProcedure;
			cmd.CommandText = "_ProjectStartDate";
			cmd.Parameters.Add(new System.Data.SqlClient.SqlParameter("@ProjectID", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.Input, true, 10, 0, "ProjectID", DataRowVersion.Current, null));
			cmd.Parameters["@ProjectID"].Value = ProjectID;
			cmd.Parameters.Add(new System.Data.SqlClient.SqlParameter("@Days", System.Data.SqlDbType.SmallDateTime, 8, System.Data.ParameterDirection.InputOutput, true, 0, 0, "Days", DataRowVersion.Current, null));
		

			return cmd;
		}
		public static DateTime Execute_ProjectStartDate(int ProjectID)
		{
			
			System.Data.SqlClient.SqlCommand cmd;
			cmd = Create_ProjectStartDate(ProjectID);
			SqlConnection cnDAL = new SqlConnection();

			try
			{
				cnDAL =  CommonDAL.OpenConnection();
				cmd.Connection = cnDAL;
				 cmd.ExecuteNonQuery();
				if(cmd.Parameters["@Days"].Value!=DBNull.Value)
					return (DateTime)cmd.Parameters["@Days"].Value;
				return Constants.DateMax;
		
			}
			catch(SqlException ex)
			{

				throw new ApplicationException("_ProjectStartDate",ex);
			}

			finally
			{
				CommonDAL.CloseConnection(cnDAL);
			}
		}
		private static SqlCommand CreateDaysUpdProc (int DaysID, int Days)
		{
			SqlCommand cmd = new SqlCommand();
			cmd.CommandType = System.Data.CommandType.StoredProcedure;
			cmd.CommandText = "DaysUpdProc";
			cmd.Parameters.Add(new System.Data.SqlClient.SqlParameter("@DaysID", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.Input, true, 10, 0, "DaysID", DataRowVersion.Current, null));
			cmd.Parameters["@DaysID"].Value = DaysID;
			cmd.Parameters.Add(new System.Data.SqlClient.SqlParameter("@Days", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.Input, true, 10, 0, "Days", DataRowVersion.Current, null));
			cmd.Parameters["@Days"].Value = Days;

			return cmd;
		}


		public static int ExecuteDaysUpdProc(int DaysID, int Days)
		{
			int RetObj;
			System.Data.SqlClient.SqlCommand cmd;
			cmd = CreateDaysUpdProc(DaysID, Days);
			SqlConnection cnDAL = new SqlConnection();

			try
			{
				cnDAL =  CommonDAL.OpenConnection();
				cmd.Connection = cnDAL;
				RetObj = cmd.ExecuteNonQuery();
				return RetObj;
		
			}
			catch(SqlException ex)
			{

				throw new ApplicationException("DaysUpdProc",ex);
			}

			finally
			{
				CommonDAL.CloseConnection(cnDAL);
			}
		}
		private static SqlCommand CreateDaysSelProc (int DaysID)
		{
			SqlCommand cmd = new SqlCommand();
			cmd.CommandType = System.Data.CommandType.StoredProcedure;
			cmd.CommandText = "DaysSelProc";
			cmd.Parameters.Add(new System.Data.SqlClient.SqlParameter("@DaysID", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.Input, true, 10, 0, "DaysID", DataRowVersion.Current, null));
			cmd.Parameters["@DaysID"].Value = DaysID;
			cmd.Parameters.Add(new System.Data.SqlClient.SqlParameter("@Days", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.InputOutput, true, 10, 0, "Days", DataRowVersion.Current, null));
			

			return cmd;
		}
		public static int ExecuteDaysSelProc(int DaysID)
		{
			
			System.Data.SqlClient.SqlCommand cmd;
			cmd = CreateDaysSelProc(DaysID);
			SqlConnection cnDAL = new SqlConnection();

			try
			{
				cnDAL =  CommonDAL.OpenConnection();
				cmd.Connection = cnDAL;
				cmd.ExecuteNonQuery();
				if(cmd.Parameters["@Days"].Value!=DBNull.Value)
					return (int)cmd.Parameters["@Days"].Value;
				return -1;
		
			}
			catch(SqlException ex)
			{

				throw new ApplicationException("DaysSelProc",ex);
			}

			finally
			{
				CommonDAL.CloseConnection(cnDAL);
			}
		}

		public static bool IsHoliday(DateTime dt)
		{
			bool bIsHoliday=(dt.DayOfWeek==DayOfWeek.Saturday ||
				dt.DayOfWeek==DayOfWeek.Sunday);
			bool bSavedIsHoliday;
			if(CalendarDAL.IsDaySaved(dt.Date, out bSavedIsHoliday))
				bIsHoliday=bSavedIsHoliday;
			return bIsHoliday;
		}

		private static SqlCommand CreateCalendarSelProc (DateTime CalendarDate)
		{
			SqlCommand cmd = new SqlCommand();
			cmd.CommandType = System.Data.CommandType.StoredProcedure;
			cmd.CommandText = "CalendarSelProc";
			cmd.Parameters.Add(new System.Data.SqlClient.SqlParameter("@CalendarDate", System.Data.SqlDbType.DateTime, 8, System.Data.ParameterDirection.Input, true, 0, 0, "CalendarDate", DataRowVersion.Current, null));
			cmd.Parameters["@CalendarDate"].Value = CalendarDate;
			cmd.Parameters.Add(new System.Data.SqlClient.SqlParameter("@IsHoliday", System.Data.SqlDbType.Bit, 1, System.Data.ParameterDirection.InputOutput, true, 0, 0, "IsHoliday", DataRowVersion.Current, null));
			//cmd.Parameters["@IsHoliday"].Value = IsHoliday;

			return cmd;
		}
		public static bool IsDaySaved(DateTime CalendarDate,out bool IsHoliday)
		{
			IsHoliday=false;
			System.Data.SqlClient.SqlCommand cmd;
			cmd = CreateCalendarSelProc(CalendarDate);
			SqlConnection cnDAL = new SqlConnection();

			try
			{
				cnDAL =  CommonDAL.OpenConnection();
				cmd.Connection = cnDAL;
				cmd.ExecuteNonQuery();
				if(cmd.Parameters["@IsHoliday"].Value ==DBNull.Value)
					return false;
				else
				{
					IsHoliday=(bool)cmd.Parameters["@IsHoliday"].Value;
					return true;
				}
		
			}
			catch(SqlException ex)
			{

				throw new ApplicationException("CalendarSelProc",ex);
			}

			finally
			{
				CommonDAL.CloseConnection(cnDAL);
			}
		}
		private static SqlCommand CreateCalendarUpdProc (DateTime CalendarDate, bool IsHoliday)
		{
			SqlCommand cmd = new SqlCommand();
			cmd.CommandType = System.Data.CommandType.StoredProcedure;
			cmd.CommandText = "CalendarUpdProc";
			cmd.Parameters.Add(new System.Data.SqlClient.SqlParameter("@CalendarDate", System.Data.SqlDbType.SmallDateTime, 8, System.Data.ParameterDirection.Input, true, 0, 0, "CalendarDate", DataRowVersion.Current, null));
			cmd.Parameters["@CalendarDate"].Value = CalendarDate;
			cmd.Parameters.Add(new System.Data.SqlClient.SqlParameter("@IsHoliday", System.Data.SqlDbType.Bit, 1, System.Data.ParameterDirection.Input, true, 0, 0, "IsHoliday", DataRowVersion.Current, null));
			cmd.Parameters["@IsHoliday"].Value = IsHoliday;

			return cmd;
		}
		public static int ExecuteCalendarUpdProc(DateTime CalendarDate, bool IsHoliday)
		{
			int RetObj;
			System.Data.SqlClient.SqlCommand cmd;
			cmd = CreateCalendarUpdProc(CalendarDate, IsHoliday);
			SqlConnection cnDAL = new SqlConnection();

			try
			{
				cnDAL =  CommonDAL.OpenConnection();
				cmd.Connection = cnDAL;
				RetObj = cmd.ExecuteNonQuery();
				return RetObj;
		
			}
			catch(SqlException ex)
			{

				throw new ApplicationException("CalendarUpdProc",ex);
			}

			finally
			{
				CommonDAL.CloseConnection(cnDAL);
			}
		}
	}
}
