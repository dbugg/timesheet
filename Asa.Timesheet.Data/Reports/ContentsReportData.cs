using System;
using System.Data;
using System.Data.SqlClient;
using Microsoft.ApplicationBlocks.Data;

namespace Asa.Timesheet.Data.Reports
{
	/// <summary>
	/// Summary description for ContentsReportsData.
	/// </summary>
	public class ContentsReportData
	{
    public static DataSet SelectContentsReportData(int projectID, int building)
    {
      string spName = "Projects_ContentsReport";

      SqlParameter[] parms = SqlHelperParameterCache.GetSpParameterSet(DBManager.GetConnectionString(), spName);
      if (projectID != 0) parms[0].Value = projectID;
      else parms[0].Value = null;
		
		if(building>0)
		parms[1].Value=building;
      DataSet ds = SqlHelper.ExecuteDataset(DBManager.GetConnectionString(), CommandType.StoredProcedure, spName, parms);

      return ds;
    }
	}
}
