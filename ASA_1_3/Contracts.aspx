﻿<%@ Register TagPrefix="uc1" TagName="PageHeader" Src="UserControls/PageHeader.ascx" %>
<%@ Page language="c#" Codebehind="Contracts.aspx.cs" AutoEventWireup="false" Inherits="Asa.Timesheet.WebPages.Contracts" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
	<head runat="server">
		<title>Contracts</title>
		<meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
		<meta content="C#" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<LINK href="styles/timesheet.css" type="text/css" rel="stylesheet">
	</HEAD>
	<body bottomMargin="0" leftMargin="0" topMargin="0" rightMargin="0">
		<form id="Form1" method="post" runat="server">
			<table height="100%" cellSpacing="0" cellPadding="0" width="100%" border="0">
				<tr height="1">
					<td>
						<uc1:PageHeader id="header" runat="server"></uc1:PageHeader>
					</td>
				</tr>
				<tr>
					<td>
						<table height="100%" cellSpacing="0" cellPadding="0" width="100%" border="0">
							<tr>
								<td class="menuLeft">
									<asp:PlaceHolder id="menuHolder" runat="server"></asp:PlaceHolder>
								</td>
								
								<td style="PADDING-RIGHT: 5px; PADDING-LEFT: 5px; PADDING-TOP: 3px" vAlign="top" noWrap><br>
									<asp:label id="lblError" runat="server" ForeColor="Red" EnableViewState="False" CssClass="ErrorLabel"></asp:label><asp:label id="lblInfo" runat="server" EnableViewState="False" CssClass="InfoLabel"></asp:label><br>
									<br>
									<asp:panel id="grid" 
										runat="server" Height="450px" Width="100%">
										<TABLE id="Table3" cellSpacing="0" cellPadding="0" width="880" border="0">
											<TR>
												<td style="WIDTH: 220px; HEIGHT: 20px">
													<asp:label id="Label1" runat="server" CssClass="enterDataLabel" Width="100%">Наименование на договора:</asp:label></TD>
												<td style="HEIGHT: 20px">
													<asp:TextBox id="txtContractName" runat="server" Width="168px"></asp:TextBox></TD>
											</TR>
											<TR>
												<td style="WIDTH: 220px; HEIGHT: 20px">
													<asp:label id="lbScanDocNew" runat="server" CssClass="enterDataLabel" Width="100%">Нов договор:</asp:label></TD>
												<td style="HEIGHT: 20px"><INPUT id="FileCtrl" style="WIDTH: 234px; HEIGHT: 20px" type="file" size="19" name="File1"
														runat="server">
													<asp:button id="btnScanDocAdd" runat="server" CssClass="ActionButton" Text="Добави"></asp:button></TD>
											</TR>
											<TR vAlign="top">
												<td style="WIDTH: 220px" vAlign="middle">
													<asp:label id="Label6" runat="server" CssClass="enterDataLabel" Width="100%">Сканирани договори:</asp:label></TD>
												<td>&nbsp;
													<asp:datalist id="dlDocs" runat="server" RepeatDirection="Horizontal">
														<ItemTemplate>
															<asp:HyperLink id="btnScan" runat="server" NavigateUrl='<%# GetURL((int)DataBinder.Eval(Container, "DataItem.ProjectContractID"))%>' Target=_blank>
																<img border="0" src="images/pdf.gif" /></asp:HyperLink>
															<br />
															<asp:ImageButton id="btnDel" runat="server" ToolTip="Изтрий" ImageUrl="images/delete.gif" Visible='<%# GetVis()%>' >
															</asp:ImageButton>
														</ItemTemplate>
													</asp:datalist>&nbsp;</TD>
											</TR>
										</TABLE>
									</asp:panel>
									<TABLE id="Table4" 
										cellSpacing="0" cellPadding="4" width="100%" border="0">
										<TR>
											<td>
												<asp:button id="btnProject" runat="server" CssClass="ActionButton" Width="216px" Text="Връщане към проекта"></asp:button></TD>
										</TR>
									</TABLE>
								</td>
							</tr>
						</table>
					</td>
				</tr>
			</table>
		</form>
	</body>
</HTML>
