﻿<%@ Register TagPrefix="uc1" TagName="PageHeader" Src="../UserControls/PageHeader.ascx" %>
<%@ Page language="c#" Codebehind="Efficiency.aspx.cs" AutoEventWireup="false" Inherits="Asa.Timesheet.WebPages.Reports.Efficiency" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
	<HEAD>
		<title>Efficiency</title>
		<meta name="GENERATOR" content="Microsoft Visual Studio .NET 7.1">
		<meta name="CODE_LANGUAGE" content="C#">
		<meta name="vs_defaultClientScript" content="JavaScript">
		<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5">
		<LINK rel="stylesheet" type="text/css" href="../styles/timesheet.css">
	</HEAD>
	<body bottomMargin="0" leftMargin="0" rightMargin="0" topMargin="0">
		<form id="Form1" method="post" runat="server">
			<table border="0" cellSpacing="0" cellPadding="0" width="100%" height="100%">
				<tr height="1">
					<td><uc1:pageheader id="header" runat="server"></uc1:pageheader></td>
				</tr>
				<tr style="DISPLAY: none" height="80">
					<td background="../images/copy1.gif">
						<TABLE id="Table2" width="100%" height="100%">
							<TR>
								<td style="WIDTH: 106px">&nbsp;&nbsp;&nbsp;&nbsp; <IMG alt="" src="../images/logo.jpg" width="64" height="80"></TD>
								<td noWrap><asp:label id="lblTitle" runat="server" Font-Size="X-Small" Font-Bold="True"></asp:label><br>
									<asp:label id="lblUser" runat="server" Font-Size="Medium" Font-Bold="True" ForeColor="SaddleBrown"></asp:label></TD>
							</TR>
						</TABLE>
					</td>
				</tr>
				<TR height="1">
					<td height="1" background="../images/line.gif" noWrap></td>
				</TR>
				<tr>
					<td>
						<table border="0" cellSpacing="0" cellPadding="0" width="100%" height="100%">
							<tr>
								<td style="BACKGROUND-IMAGE: url(../images/left110.gif)" vAlign="top" width="110" noWrap><asp:placeholder id="menuHolder" runat="server"></asp:placeholder></td>
								<td bgColor="lightgrey" width="1" noWrap></td>
								<td style="PADDING-LEFT: 5px; PADDING-RIGHT: 5px; PADDING-TOP: 3px" vAlign="top" noWrap>
									<TABLE id="tblInfo" border="0" cellSpacing="0" cellPadding="3" width="880" height="30">
										<TR>
											<td vAlign="middle" noWrap><asp:label id="lblError" runat="server" ForeColor="Red" EnableViewState="False" CssClass="ErrorLabel"></asp:label><asp:label id="lblInfo" runat="server" EnableViewState="False" CssClass="InfoLabel"></asp:label></TD>
										</TR>
									</TABLE>
									<TABLE id="Table3" border="0" cellSpacing="0" cellPadding="0" width="880">
										<TR>
											<td style="HEIGHT: 5px" colSpan="2"></TD>
										</TR>
										<TR style="DISPLAY: none">
											<td colSpan="2"><IMG src="../images/dot.gif" width="100%" height="1"></TD>
										</TR>
										<TR>
											<td height="3" colSpan="2"><asp:checkbox id="ckDates" runat="server" CssClass="EnterDataLabel" Width="144px" Text="За целия период"
													Checked="True" AutoPostBack="True"></asp:checkbox></td>
										</TR>
										<TR>
											<td>
												<TABLE id="Table4" border="0" cellSpacing="0" cellPadding="3">
													<TR id="SelectDateRow">
														<td style="WIDTH: 139px; HEIGHT: 4px" vAlign="top" align="right"><asp:label id="lblStartDay" runat="server" CssClass="EnterDataLabel">Ден, седмица, 
                        нач.дата:</asp:label><asp:label id="lblSelStartDate" runat="server" Font-Bold="True" ForeColor="SaddleBrown" Width="100px"></asp:label></TD>
														<td style="WIDTH: 72px" vAlign="top">
															<TABLE id="Table1" border="0" cellSpacing="0" cellPadding="0">
																<TR>
																	<td><asp:calendar id="calStartDate" runat="server" ForeColor="#222222" Width="240px" SelectionMode="DayWeek"
																			BorderColor="#999999" CellPadding="0" BorderWidth="1px" BackColor="White" Height="240px" NextMonthText=""
																			PrevMonthText="" DayNameFormat="FirstLetter" SelectWeekText="+"
																			FirstDayOfWeek="Monday" BorderStyle="Solid" CssClass="aspCalendar" NextPrevFormat="ShortMonth" Font-Bold="True" Font-Size="11pt">
																			<TodayDayStyle ForeColor="#C73B2E" BackColor="White" BorderColor="#C73B2E" 
                                                                                BorderStyle="Solid" BorderWidth="2px" Height="32px" Width="32px"></TodayDayStyle>
																			<SelectorStyle Font-Size="10pt" Font-Names="Verdana" ForeColor="SaddleBrown" 
                                                                                BorderStyle="None"></SelectorStyle>
																			<DayStyle Font-Size="10pt" Font-Bold="False" BorderStyle="Dotted" 
                                                                                BorderColor="#CCCCCC" BorderWidth="1px" Height="30px" Width="32px"></DayStyle>
																			<NextPrevStyle Font-Bold="True" Wrap="False" HorizontalAlign="Center" 
                                                                                Height="16px" BorderStyle="None" VerticalAlign="Middle" Font-Size="9pt" 
                                                                                Font-Underline="True"></NextPrevStyle>
																			<DayHeaderStyle Font-Size="9pt" Height="1px" ForeColor="SaddleBrown"></DayHeaderStyle>
																			<SelectedDayStyle Font-Bold="True" ForeColor="#C73B2E" BorderColor="#C73B2E" 
                                                                                BackColor="White" BorderStyle="Solid" BorderWidth="2px"
                                                                                Font-Size="11pt" Height="32px" Width="32px"></SelectedDayStyle>
																			<TitleStyle Font-Size="13pt" Font-Bold="True" ForeColor="#C73B2E" 
                                                                                BackColor="White" HorizontalAlign="Center"></TitleStyle>
																			<OtherMonthDayStyle ForeColor="Gray" BackColor="#FDFDFD" Font-Size="6pt"></OtherMonthDayStyle>
																		    <WeekendDayStyle Font-Bold="True" Font-Italic="False" 
                                                                                ForeColor="#C73B2E" />
																		</asp:calendar></TD>
																	<td vAlign="top" noWrap></TD>
																</TR>
															</TABLE>
														</TD>
														<td style="WIDTH: 117px" vAlign="top" align="right"><asp:label id="lblEndDay" runat="server" CssClass="EnterDataLabel">Крайна 
                        дата:</asp:label><asp:label id="lblSelEndDate" runat="server" Font-Bold="True" ForeColor="SaddleBrown" Width="100px"></asp:label></TD>
														<td style="WIDTH: 278px; HEIGHT: 4px" vAlign="top">
															<TABLE id="Table6" border="0" cellSpacing="0" cellPadding="0">
																<TR>
																	<td><asp:calendar id="calEndDate" runat="server" ForeColor="#222222" Width="240px" SelectionMode="DayWeek"
																			BorderColor="#999999" CellPadding="0" BorderWidth="1px" BackColor="White" Height="240px" NextMonthText=""
																			PrevMonthText="" DayNameFormat="FirstLetter" SelectWeekText="+"
																			FirstDayOfWeek="Monday" BorderStyle="Solid" CssClass="aspCalendar" NextPrevFormat="ShortMonth" Font-Bold="True" Font-Size="11pt">
																			<TodayDayStyle ForeColor="#C73B2E" BackColor="White" BorderColor="#C73B2E" 
                                                                                BorderStyle="Solid" BorderWidth="2px" Height="32px" Width="32px"></TodayDayStyle>
																			<SelectorStyle Font-Size="10pt" Font-Names="Verdana" ForeColor="SaddleBrown" 
                                                                                BorderStyle="None"></SelectorStyle>
																			<DayStyle Font-Size="10pt" Font-Bold="False" BorderStyle="Dotted" 
                                                                                BorderColor="#CCCCCC" BorderWidth="1px" Height="30px" Width="32px"></DayStyle>
																			<NextPrevStyle Font-Bold="True" Wrap="False" HorizontalAlign="Center" 
                                                                                Height="16px" BorderStyle="None" VerticalAlign="Middle" Font-Size="9pt" 
                                                                                Font-Underline="True"></NextPrevStyle>
																			<DayHeaderStyle Font-Size="9pt" Height="1px" ForeColor="SaddleBrown"></DayHeaderStyle>
																			<SelectedDayStyle Font-Bold="True" ForeColor="#C73B2E" BorderColor="#C73B2E" 
                                                                                BackColor="White" BorderStyle="Solid" BorderWidth="2px"
                                                                                Font-Size="11pt" Height="32px" Width="32px"></SelectedDayStyle>
																			<TitleStyle Font-Size="13pt" Font-Bold="True" ForeColor="#C73B2E" 
                                                                                BackColor="White" HorizontalAlign="Center"></TitleStyle>
																			<OtherMonthDayStyle ForeColor="Gray" BackColor="#FDFDFD" Font-Size="6pt"></OtherMonthDayStyle>
																		    <WeekendDayStyle Font-Bold="True" Font-Italic="False" 
                                                                                ForeColor="#C73B2E" />
																		</asp:calendar></TD>
																	<td vAlign="top" noWrap></TD>
																</TR>
															</TABLE>
														</TD>
													</TR>
												</TABLE>
											</TD>
										</TR>
										<TR>
											<td style="HEIGHT: 3px" colSpan="2"></TD>
										</TR>
										<TR>
											<td colSpan="2"><IMG src="../images/dot.gif" width="100%" height="1"></TD>
										</TR>
										<TR>
											<td height="3" colSpan="2"></td>
										</TR>
										<tr>
											<td>
												<table border="0" cellSpacing="0" cellPadding="3">
													<TR>
														<td style="WIDTH: 159px" vAlign="top" height="10">
															<asp:label id="Label10" runat="server" CssClass="enterDataLabel" Width="100%">Фирма за финансови показатели:</asp:label></TD>
														<td style="WIDTH: 243px" vAlign="top" height="10">
															<asp:dropdownlist id="ddlCompany" runat="server" CssClass="EnterDataBox" Width="250px" AutoPostBack="True">
																<asp:ListItem Value="0">АСА</asp:ListItem>
																<asp:ListItem Value="1">АСИ</asp:ListItem>
															</asp:dropdownlist></TD>
														<td vAlign="top" height="10"></TD>
													<TR>
														<td vAlign="top"><asp:label id="lblSluj" runat="server" CssClass="enterDataLabel" Width="100%">Служител:</asp:label></TD>
														<td vAlign="top" style="HEIGHT: 31px"><asp:dropdownlist id="ddlUser" runat="server" CssClass="EnterDataBox" Width="250px"></asp:dropdownlist>&nbsp;</TD>
														<td></TD>
														<td>
															<asp:dropdownlist id="ddlUserStatus" runat="server" CssClass="EnterDataBox" Width="200" AutoPostBack="True"
																style="Z-INDEX: 0">
																<asp:ListItem Value="-1">активни и пасивни</asp:ListItem>
																<asp:ListItem Value="1" Selected="True">активни служители</asp:ListItem>
																<asp:ListItem Value="0">пасивни служители</asp:ListItem>
															</asp:dropdownlist></TD>
													</TR>
													<tr>
														<td height="10"></TD>
														<td height="10"><asp:imagebutton style="Z-INDEX: 0" id="ibGenerate" runat="server" ImageUrl="../images/ie.gif"></asp:imagebutton><asp:imagebutton id="ibPdfExport" runat="server" ImageUrl="../images/pdf1.gif" Visible="False" ToolTip="Справка в PDF формат"
																style="Z-INDEX: 0"></asp:imagebutton></TD>
														<td style="WIDTH: 8px" height="10"></TD>
														<td style="WIDTH: 8px" height="10">
															<asp:imagebutton id="ibXlsExport" runat="server" ImageUrl="../images/xls.gif" Visible="False" ToolTip="Справка в Excel формат"
																style="Z-INDEX: 0"></asp:imagebutton></TD>
													</tr>
												</table>
											</td>
										</tr>
										<TR>
											<td style="HEIGHT: 5px" colSpan="2"></TD>
										</TR>
										<TR>
											<td colSpan="2"><IMG src="../images/dot.gif" width="100%" height="1"></TD>
										</TR>
										<TR>
											<td height="3" colSpan="2"></TD>
										</TR>
										<TR>
											<td colSpan="2"><IMG src="images/dot.gif" width="100%" height="1"></TD>
										</TR>
									</TABLE>
									<P>&nbsp;</P>
									<TABLE id="Table5" border="0" cellSpacing="0" cellPadding="0" width="100%">
										<TR>
											<td align="center"><asp:label id="lblReportTitle" runat="server" Font-Size="11pt" Font-Bold="True" EnableViewState="False"
													Visible="False"> СПРАВКА ЕФЕКТИВНОСТ</asp:label></TD>
										</TR>
									</TABLE>
									<P></P>
									<asp:datagrid style="Z-INDEX: 0" id="grdWorkTime" runat="server" ForeColor="DimGray" EnableViewState="False"
										CssClass="ReportGrid" Width="100%" CellPadding="4" AutoGenerateColumns="False">
										<HeaderStyle CssClass="reportGridHeader"></HeaderStyle>
										<Columns>
											<asp:BoundColumn DataField="Name" HeaderText="Служител"></asp:BoundColumn>
											<asp:BoundColumn DataField="Project" HeaderText="Проект"></asp:BoundColumn>
											<asp:BoundColumn DataField="Hours" HeaderText="Часове"></asp:BoundColumn>
											<asp:BoundColumn DataField="Income" HeaderText="Приходи(лв.)" DataFormatString="{0:n2}" ItemStyle-HorizontalAlign="Right"></asp:BoundColumn>
											<asp:BoundColumn DataField="Expenses" HeaderText="Разходи(лв.)" DataFormatString="{0:n2}" ItemStyle-HorizontalAlign="Right"></asp:BoundColumn>
											<asp:BoundColumn DataField="Result" HeaderText="Печалба(лв.)" DataFormatString="{0:n2}" ItemStyle-HorizontalAlign="Right"></asp:BoundColumn>
										</Columns>
									</asp:datagrid><BR>
									<BR>
									<BR>
								</td>
							</tr>
						</table>
					</td>
				</tr>
			</table>
		</form>
	</body>
</HTML>
