﻿<%@ Register TagPrefix="uc1" TagName="PageHeader" Src="UserControls/PageHeader.ascx" %>
<%@ Page language="c#" Codebehind="HoursPeriod.aspx.cs" AutoEventWireup="false" Inherits="Asa.Timesheet.WebPages.HoursPeriod" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
	<head runat="server">
		<title>Hours</title>
		<meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
		<meta content="C#" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<LINK href="styles/calendar.css" type="text/css" rel="stylesheet">
		<LINK href="styles/timesheet.css" type="text/css" rel="stylesheet">
		<script language="javascript" src="js/hours.js"></script>
		<script language="javascript" src="PopupCalendar.js"></script>
	</HEAD>
	<body bottomMargin="0" leftMargin="0" topMargin="0" onload="Init();" rightMargin="0">
		<form id="Form1" method="post" runat="server">
			<table height="100%" cellSpacing="0" cellPadding="0" width="100%" border="0">
				<tr height="1">
					<td><uc1:pageheader id="header" runat="server"></uc1:pageheader></td>
				</tr>
				<tr>
					<td>
						<table height="100%" cellSpacing="0" cellPadding="0" width="100%" border="0">
							<tr>
								<td class="menuLeft"><asp:placeholder id="menuHolder" runat="server"></asp:placeholder></td>
								
								<td style="PADDING-RIGHT: 3px; PADDING-LEFT: 5px; PADDING-TOP: 3px" vAlign="top" noWrap><INPUT id="txtVisibleRows" style="DISPLAY: none" type="text" name="txtVisibleRows" runat="server">
									<INPUT id="txtNoActivity" style="DISPLAY: none" type="text" runat="server">&nbsp;
									<INPUT id="txtIncorrectDataMsg" style="DISPLAY: none" type="text" name="txtIncorrectDataMsg"
										runat="server"> <INPUT id="txtMinGridRows" style="DISPLAY: none" type="text" name="txtMinGridRows" runat="server">
									<INPUT id="txtTimeAbbr" style="DISPLAY: none" type="text" name="Text1" runat="server">
									<INPUT id="txtDefaultWorkDayTimes" style="DISPLAY: none" type="text" name="Text1" runat="server">
									<INPUT id="txtDefaultHoursProjects" style="DISPLAY: none" type="text" name="Text1" runat="server">
									<TABLE id="tblInfo" height="40" cellSpacing="0" cellPadding="3" width="880" border="0">
										<TR>
											<td vAlign="middle" noWrap><asp:label id="lblInfo" runat="server" EnableViewState="False" CssClass="InfoLabel"></asp:label><br>
												<asp:label id="lblError" runat="server" EnableViewState="False" CssClass="ErrorLabel"></asp:label></TD>
										</TR>
									</TABLE>
									<div style="PADDING-BOTTOM: 5px" align="right"><asp:hyperlink id="HyperLink1" runat="server" NavigateUrl="Hours.aspx">Към режим "Въвеждане на ден"</asp:hyperlink></div>
									<TABLE id="Table3" 
										cellSpacing="0" cellPadding="4" width="100%" border="0">
										<TR>
											<td>&nbsp;<asp:label id="lblTop" runat="server" cssclass="PageCaption"></asp:label>&nbsp;<asp:dropdownlist id="ddlUsers" runat="server" CssClass="enterDataBox" AutoPostBack="True" Width="250px"></asp:dropdownlist>&nbsp;<asp:label id="lblPeriodLbl" runat="server" CssClass="PageCaption" Width="104px"> за периода от</asp:label><asp:textbox id="txtSD" runat="server" CssClass="enterDataBox" Width="88px" MaxLength="10"></asp:textbox>&nbsp;<IMG id="lkSD" style="CURSOR: hand" alt="Calendar" src="images/calendar.ico" width="18"
													align="middle" runat="server">&nbsp;
												<asp:label id="Label1" runat="server" CssClass="PageCaption">до</asp:label>&nbsp;<asp:textbox id="txtED" runat="server" CssClass="enterDataBox" Width="88px" MaxLength="10"></asp:textbox>&nbsp;<IMG id="lkED" style="CURSOR: hand" alt="Calendar" src="images/calendar.ico" width="18"
													align="middle" runat="server"></TD>
											<td align="right"><asp:label id="lblStatus" runat="server" CssClass="HoursGridStatusLabel"></asp:label>&nbsp;</td>
										</TR>
										<tr>
											<td colSpan="2"></td>
										</tr>
									</TABLE>
									<asp:panel id="divMain" Runat="server">
										<asp:panel id="grid"
											runat="server" Width="100%" Height="240px">
											<asp:datagrid id="GridHours" runat="server" CssClass="GridHours" BorderColor="SaddleBrown" CellPadding="4"
												BorderWidth="1px" BackColor="White" BorderStyle="Solid" PageSize="2" AutoGenerateColumns="False"
												GridLines="Horizontal" Width="100%">
												<AlternatingItemStyle BackColor="White"></AlternatingItemStyle>
												<ItemStyle BackColor="White"></ItemStyle>
												<HeaderStyle Height="5px" CssClass="HoursGridHeader"></HeaderStyle>
												<Columns>
													<asp:TemplateColumn>
														<HeaderStyle Wrap="False" Width="25px"></HeaderStyle>
														<ItemStyle Wrap="False" HorizontalAlign="Center" VerticalAlign="Top"></ItemStyle>
														<ItemTemplate>
															<asp:ImageButton id="btnDeleteItem" runat="server" ImageUrl="images/delete.gif"></asp:ImageButton><BR>
															<asp:Image id="imgWarning" style="DISPLAY: none" runat="server" EnableViewState="False" ImageUrl="images/error.gif"></asp:Image>
														</ItemTemplate>
														<FooterStyle Wrap="False"></FooterStyle>
													</asp:TemplateColumn>
													<asp:TemplateColumn HeaderText="&#160;&#160;Проект и дейност">
														<HeaderStyle Wrap="False" Width="40%"></HeaderStyle>
														<ItemStyle Wrap="False" VerticalAlign="Top"></ItemStyle>
														<ItemTemplate>
															<asp:DropDownList id="ddlProject" Width="100%" Runat="server" Font-Bold="True">
																<asp:ListItem Selected="True"></asp:ListItem>
															</asp:DropDownList><BR>
															<asp:DropDownList id="ddlActivity" Width="100%" Runat="server">
																<asp:ListItem></asp:ListItem>
															</asp:DropDownList><INPUT id="hdnPrevProjectID" style="DISPLAY: none" type="text" runat="server">
                                                            <INPUT id="hdnWorkTimeID" type="hidden" runat="server">
														</ItemTemplate>
													</asp:TemplateColumn>
													<asp:TemplateColumn HeaderText="Нач. час">
														<HeaderStyle Wrap="False"></HeaderStyle>
														<ItemStyle Wrap="False" VerticalAlign="Top"></ItemStyle>
														<ItemTemplate>
															<asp:DropDownList id="ddlStartTime" Width="65px" Runat="server">
																<asp:ListItem></asp:ListItem>
															</asp:DropDownList>
														</ItemTemplate>
													</asp:TemplateColumn>
													<asp:TemplateColumn HeaderText="Кр. час">
														<HeaderStyle Wrap="False"></HeaderStyle>
														<ItemStyle Wrap="False" VerticalAlign="Top"></ItemStyle>
														<ItemTemplate>
															<asp:DropDownList id="ddlEndTime" Width="65px" Runat="server">
																<asp:ListItem></asp:ListItem>
															</asp:DropDownList>
														</ItemTemplate>
													</asp:TemplateColumn>
													<asp:TemplateColumn HeaderText="&#160;Протокол">
														<HeaderStyle Wrap="False" Width="25%"></HeaderStyle>
														<ItemStyle Wrap="False" VerticalAlign="Top"></ItemStyle>
														<ItemTemplate>
															<asp:TextBox id="txtMinutes" runat="server" Width="100%" Height="58px" TextMode="MultiLine"></asp:TextBox>
														</ItemTemplate>
													</asp:TemplateColumn>
													<asp:TemplateColumn HeaderText="&#160;Админ. бележки">
														<HeaderStyle Width="25%"></HeaderStyle>
														<ItemStyle VerticalAlign="Top"></ItemStyle>
														<ItemTemplate>
															<asp:TextBox id="txtAdminMinutes" runat="server" Width="100%" Height="48px" TextMode="MultiLine"
																ForeColor="#C00000"></asp:TextBox>
														</ItemTemplate>
													</asp:TemplateColumn>
													<asp:TemplateColumn>
														<HeaderStyle Wrap="False" Width="25px"></HeaderStyle>
														<ItemStyle Wrap="False" HorizontalAlign="Center"></ItemStyle>
														<ItemTemplate>
															<asp:ImageButton id="btnMail" runat="server" ImageUrl="images/email.gif" ToolTip="Прати Е-мейл"></asp:ImageButton>
															<asp:ImageButton id="btnMeeting" runat="server" ImageUrl="images/word.gif" ToolTip="Протокол от среща"></asp:ImageButton>
														</ItemTemplate>
														<FooterStyle Wrap="False"></FooterStyle>
													</asp:TemplateColumn>
													<asp:BoundColumn Visible="False" DataField="WorkTimeID" HeaderText="WorkTimeID"></asp:BoundColumn>
												</Columns>
											</asp:datagrid>
										</asp:panel>
										<TABLE id="tblGridBottom" 
											borderColor="tan" cellSpacing="0" cellPadding="0" width="100%"  runat="server">
											<TR>
												<td class="Total">
													<TABLE cellSpacing="0" cellPadding="2" width="100%" border="0">
														<TR>
															<td>
																<asp:button id="btnNewProject" runat="server" CssClass="ActionButton" Text="Друг проект"></asp:button>&nbsp;&nbsp;
																<INPUT style="DISPLAY: none" onclick="ClearAll();" type="button" value="Нулирай всички">
																Протокол за авторски надзор:<INPUT id="fileProtokol" type="file" name="File1" runat="server"></TD>
															<td align="right">
																<asp:TextBox id="txtCopiedUsers" style="DISPLAY: none" runat="server"></asp:TextBox>
																<asp:Button id="btnCopySave" style="DISPLAY: none" runat="server" Text="Button"></asp:Button>
																<asp:button id="btnCopy" runat="server" CssClass="ActionButton" Width="172px" Text="Добави друг служител"></asp:button>&nbsp;
																<asp:button id="btnSave" runat="server" CssClass="ActionButton" Text="Запиши"></asp:button></TD>
														</TR>
													</TABLE>
												</TD>
											</TR>
											<TR>
												<td class="Total" width="100%" colSpan="2">ОБЩО ЧАСОВЕ НА ДЕН: <INPUT id="Total" style=" FONT-WEIGHT: bold; border: none;"
														readOnly type="text" size="4" name="Total" runat="server"></TD>
											</TR>
										</TABLE>
									</asp:panel>
									<P style="PADDING-LEFT: 5px"><asp:label id="lblSaveInfo" runat="server" Font-Size="12px"></asp:label></P>
								</td>
							</tr>
						</table>
					</td>
				</tr>
			</table>
		</form>
	</body>
</HTML>
