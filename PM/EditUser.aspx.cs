using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using Asa.Timesheet.Data;
using log4net;
using log4net.Config;
using System.Data.SqlClient;
using System.Text;

namespace PM
{
	/// <summary>
	/// Summary description for EditUser.
	/// </summary>
	public class EditUser : BasePage
	{

		#region WebControls
		
		protected System.Web.UI.WebControls.Label lblError;
		protected System.Web.UI.WebControls.Label lbFileMinutes;
		protected System.Web.UI.WebControls.Button btnSave;
		protected System.Web.UI.WebControls.Label lbAccountName;
		protected System.Web.UI.WebControls.TextBox txtAccountName;
		protected System.Web.UI.WebControls.TextBox txtAccountPass;
		protected System.Web.UI.HtmlControls.HtmlInputHidden hdnAccountPass;
		protected System.Web.UI.WebControls.Label lbFullName;
		protected PM.UserControls.PageHeader header;
		protected System.Web.UI.WebControls.Label lblInfo;
		protected System.Web.UI.WebControls.TextBox txtUserName;
		protected System.Web.UI.WebControls.Label txtFullName;
		protected System.Web.UI.HtmlControls.HtmlTable tblAccess;
		protected System.Web.UI.WebControls.DataGrid grdAccess;
		protected System.Web.UI.WebControls.Panel grid;
		protected System.Web.UI.WebControls.Label lbAccessName;
		protected System.Web.UI.WebControls.Label lbAccountPass;
		protected System.Web.UI.WebControls.Label lblName;
		protected System.Web.UI.WebControls.TextBox txtProject;
		protected System.Web.UI.WebControls.DropDownList ddlSearch;
		protected System.Web.UI.WebControls.DropDownList ddlBuildingTypes;
		protected System.Web.UI.WebControls.Button btnSearch;
		protected System.Web.UI.WebControls.Button btnClear;
		protected System.Web.UI.WebControls.Button btnAutoGenerate;
		protected System.Web.UI.WebControls.Label lbPMMail;
		protected System.Web.UI.WebControls.TextBox txtPMMail;
		protected System.Web.UI.WebControls.Label lblUserMail;
		protected System.Web.UI.WebControls.ImageButton imgBack;
		protected System.Web.UI.WebControls.LinkButton linkBack;
		protected System.Web.UI.WebControls.ImageButton imgSave;
		protected System.Web.UI.WebControls.LinkButton linkSave;
		protected System.Web.UI.HtmlControls.HtmlTable tblForm;
		protected System.Web.UI.WebControls.LinkButton linkEdit;
		protected System.Web.UI.WebControls.ImageButton imgEdit;
		protected System.Web.UI.WebControls.TextBox txtHdnAccountPass;
		
		private static readonly ILog log = LogManager.GetLogger(typeof(EditUser));
	
		#endregion

		#region enums
		private enum gridColomns
		{
			AccessID = 0,
			ProjectID,
			cbAccess,
			ProjectName,
			ProjectResponsible,
			ProjectMail,
		}

		#endregion

		#region Page_Load

        private void Page_Load(object sender, System.EventArgs e)
        {
            //			bool bb = (UIHelpers.GetIDParam() != -1||UIHelpers.GetPIDParam()==(int)PMUserType.PMUser);
            //			bb = LoggedUser.IsAuthenticated;
            //			bb= UIHelpers.GetPIDParam()!=-1;
            if (!(LoggedUser.IsAuthenticated && (UIHelpers.GetIDParam() != -1 || UIHelpers.GetPIDParam() == (int)PMUserType.PMUser) && UIHelpers.GetPIDParam() != -1))
                ErrorRedirect(UIHelpers.GetText(Language, "noRightsAlert"));

            if (!LoggedUser.IsUser/*&&(UIHelpers.GetIDParam() != LoggedUser.UserID&&UIHelpers.GetPIDParam()!=LoggedUser.UserType)*/) ErrorRedirect(UIHelpers.GetText(Language, "noRightsAlert"));
            LoadResourses();
            if (!this.IsPostBack)
            {

                //TODO
                //notes:if user is not autherized he can't use all function of the form
                //				bool b = (LoggedUser.UserRoleID >0);//== (int)Roles.Director);
                //				if(!b) 
                //				{
                //					//btnDelete.Visible = false;
                //				//	btnBack.Visible = false;
                //				}

                if (!LoadUser()) { lblError.Text = UIHelpers.GetText(Language, "editUser_ErrorLoadUser"); return; }

                //notes:load resourses always at the end
                //				if(LoggedUser.UserRoleID == (int)Roles.Director)
                //				{
                SortOrder = 1;
                UIHelpers.LoadProjectStatus(ddlSearch, LoggedUser.HasPaymentRights);
                UIHelpers.LoadBuildingTypes(ddlBuildingTypes);

                LoadAccess();
            }
            // Put user code to initialize the page here
        }


        private bool LoadUser()
        {

            int userType = UIHelpers.GetPIDParam();
            lblUserMail.Visible = false;
            switch (userType)
            {
                case ((int)PMUserType.Client):
                    {
                        ClientData ud = ClientDAL.Load(UIHelpers.GetIDParam());
                        if (ud == null) return false;
                        txtFullName.Text = ud.ClientName;
                        txtAccountName.Text = ud.AccountName;
                        OldAccName = ud.AccountName;
                        txtAccountPass.Text = ud.AccountPass;
                        txtHdnAccountPass.Text = txtAccountPass.Text;
                        hdnAccountPass.Value = "";
                        //for (int i=0; i<txtAccountPass.Text.Length; i++) hdnAccountPass.Value+="*";
                        OldAccPass = ud.AccountPass;
                        lblUserMail.Text = ud.Email;
                        txtPMMail.Text = (ud.PMMail.Length > 0) ? ud.PMMail : ud.Email;

                        break;
                    }
                case ((int)PMUserType.Builder):
                    {
                        ClientData ud = ClientDAL.Load(UIHelpers.GetIDParam());
                        if (ud == null) return false;
                        txtFullName.Text = ud.ClientName;
                        txtAccountName.Text = ud.AccountName;
                        OldAccName = ud.AccountName;
                        txtAccountPass.Text = ud.AccountPass;
                        txtHdnAccountPass.Text = txtAccountPass.Text;
                        hdnAccountPass.Value = "";
                        //for (int i=0; i<txtAccountPass.Text.Length; i++) hdnAccountPass.Value+="*";
                        OldAccPass = ud.AccountPass;
                        lblUserMail.Text = ud.Email;
                        txtPMMail.Text = (ud.PMMail.Length > 0) ? ud.PMMail : ud.Email;

                        break;
                    }
                case ((int)PMUserType.Subcontracter):
                    {
                        SubcontracterData ud = SubcontracterDAL.Load(UIHelpers.GetIDParam());
                        if (ud == null) return false;
                        txtFullName.Text = ud.SubcontracterType + " - " + ud.SubcontracterName;
                        txtAccountName.Text = ud.AccountName;
                        OldAccName = ud.AccountName;
                        txtAccountPass.Text = ud.AccountPass;
                        txtHdnAccountPass.Text = txtAccountPass.Text;
                        hdnAccountPass.Value = "";
                        //for (int i=0; i<txtAccountPass.Text.Length; i++) hdnAccountPass.Value+="*";
                        OldAccPass = ud.AccountPass;
                        txtPMMail.Text = (ud.PMMail.Length > 0) ? ud.PMMail : ud.Email;

                        //txtPMMail.Text = ud.PMMail;	
                        break;
                    }
                case ((int)PMUserType.PMUser):
                    {
                        int ClID = UIHelpers.GetIDParam();
                        txtFullName.Visible = false;
                        txtUserName.Visible = true;
                        if (ClID != -1)
                        {
                            ClientData ud = ClientDAL.Load(ClID);
                            if (ud == null) return false;
                            txtUserName.Text = ud.ClientName;
                            txtAccountName.Text = ud.AccountName;
                            OldAccName = ud.AccountName;
                            txtAccountPass.Text = ud.AccountPass;
                            txtHdnAccountPass.Text = txtAccountPass.Text;
                            hdnAccountPass.Value = "";
                            //for (int i=0; i<txtAccountPass.Text.Length; i++) hdnAccountPass.Value+="*";
                            OldAccPass = ud.AccountPass;
                            lblUserMail.Text = ud.Email;
                            txtPMMail.Text = ud.PMMail;

                        }


                        break;
                    }
                case ((int)PMUserType.Supervisors):
                    {
                        ClientData ud = ClientDAL.Load(UIHelpers.GetIDParam());
                        if (ud == null) return false;
                        txtFullName.Text = ud.ClientName;
                        txtAccountName.Text = ud.AccountName;
                        OldAccName = ud.AccountName;
                        txtAccountPass.Text = ud.AccountPass;
                        txtHdnAccountPass.Text = txtAccountPass.Text;
                        hdnAccountPass.Value = "";
                        //for (int i=0; i<txtAccountPass.Text.Length; i++) hdnAccountPass.Value+="*";
                        OldAccPass = ud.AccountPass;
                        lblUserMail.Text = ud.Email;
                        txtPMMail.Text = (ud.PMMail.Length > 0) ? ud.PMMail : ud.Email;

                        break;
                    }
                default: return false;
            }
            return true;
        }


        private void LoadResourses()
        {
            string lang = Language;
            lbFullName.Text = UIHelpers.GetText(lang, "Name_FullNameName");
            lbAccountName.Text = UIHelpers.GetText(lang, "Name_AccountNameName");
            lbAccountPass.Text = UIHelpers.GetText(lang, "Name_AccountPassName");
            //btnBack.Text = UIHelpers.GetText(lang,"Name_btnBackEditUserName");
            linkBack.Text = "<i class='fas fa-chevron-left'></i> " + UIHelpers.GetText(lang, "Name_btnBackEditUserName");
            //btnSave.Text = UIHelpers.GetText(lang,"Name_btnSaveName");
            linkSave.Text = UIHelpers.GetText(lang, "Name_btnSaveName");
            linkEdit.Text = UIHelpers.GetText(lang, "btnEdit_name");
            btnAutoGenerate.Text = UIHelpers.GetText(lang, "Name_btnGenerateNameColomn");
            if (!LoggedUser.IsUser)
                Header.PageTitle = UIHelpers.GetText(lang, "pmPageTitle_EditUserNotASA");
            else
                Header.PageTitle = UIHelpers.GetText(lang, "pmPageTitle_EditUser");
            //TODO
            //tblAccessible = (LoggedUser.UserRoleID == (int)Roles.Director);	
            //			if(tblAccess.Visis.Vible)
            //			{
            lbAccessName.Text = UIHelpers.GetText(lang, "Name_AccessToProjectsName");
            lblName.Text = UIHelpers.GetText(lang, "Name_ProjectName");
            btnSearch.Text = UIHelpers.GetText(lang, "Name_btnSearchName");
            btnClear.Text = UIHelpers.GetText(lang, "Name_btnCleanName");
            grdAccess.Columns[(int)gridColomns.ProjectName].HeaderText = UIHelpers.GetText(lang, "Name_ProjectNameColomn");
            grdAccess.Columns[(int)gridColomns.cbAccess].HeaderText = UIHelpers.GetText(lang, "Name_AccessNameColomn");
            grdAccess.Columns[(int)gridColomns.ProjectResponsible].HeaderText = UIHelpers.GetText(lang, "Name_ProjectResponsible");
            grdAccess.Columns[(int)gridColomns.ProjectMail].HeaderText = UIHelpers.GetText(lang, "Name_ProjectMail");
            //			}

            lbPMMail.Text = UIHelpers.GetText(lang, "Name_EditUser_Labels_PMMail");




        }


        private void LoadAccess()
        {
            SqlDataReader reader = null;
            int Active = int.Parse(ddlSearch.SelectedValue);
            int userID = UIHelpers.GetIDParam();
            int userType = UIHelpers.GetPIDParam();
            try
            {
                bool IsBuildingTypeGradoustrShow = false;
                if (ddlBuildingTypes.SelectedValue == System.Configuration.ConfigurationManager.AppSettings["BuildingTypeNotInPM"])
                    IsBuildingTypeGradoustrShow = true;

                reader = ProjectsData.SelectProjects(4 /*SortOrder*/, Active, txtProject.Text.Trim(), int.Parse(ddlBuildingTypes.SelectedValue)/*, ddlSearch.SelectedValue=="2"*/, true, -1, -1, IsBuildingTypeGradoustrShow);

                DataTable dt = CommonDAL.CreateTableFromReader(reader);

                PMProjectUserAccesssVector pmpavShow = new PMProjectUserAccesssVector();
                PMProjectUserAccesssVector pmpav = PMProjectUserAccessDAL.LoadCollectionForUser(userType, userID);
                foreach (DataRow dr in dt.Rows)
                {
                    int projectID = (int)dr[0];
                    string projectName1, projectCode1, administrativeName1, add;
                    decimal area2 = 0;
                    int clientID = 0;
                    bool bPrevPhases, isPUP;
                    ProjectsData.SelectProject(projectID, out projectName1, out  projectCode1, out administrativeName1, out area2, out add,
                        out clientID, out bPrevPhases, out isPUP);
                    //notes:algorith:ProjectCode,ProjectName
                    string projectName = string.Concat((string)dr[2], " ", (string)dr[1]);

                    int index = pmpav.IndexOfProject(projectID);
                    if (index != -1)
                    {
                        pmpav[index].ProjectName = projectName;
                        pmpavShow.Add(pmpav[index]);
                    }
                    else
                    {
                        bool userAccess = false;

                        switch (userType)
                        {
                            case (int)PMUserType.Subcontracter:
                                {
                                    ProjectSubcontractersVector vec = ProjectSubcontracterDAL.LoadCollectionByProjectAndSubc(projectID, userID);
                                    if (vec.Count > 0)
                                        userAccess = true;
                                    break;
                                }
                            case (int)PMUserType.Client:
                                {
                                    if (clientID == userID)
                                    {
                                        userAccess = true;
                                        break;
                                    }
                                    ProjectClientsVector vec = ProjectClientDAL.LoadCollectionByProjectAndClient(projectID, userID);
                                    if (vec.Count > 0)
                                        userAccess = true;

                                    break;
                                }
                            case (int)PMUserType.Builder:
                                {
                                    ProjectClientsVector vec = ProjectClientDAL.LoadCollectionByProjectAndClient(projectID, userID);
                                    if (vec.Count > 0)
                                        userAccess = true;
                                    break;
                                }
                            case (int)PMUserType.PMUser: { break; }
                            default: break;
                        }

                        PMProjectUserAccessData datNew = new PMProjectUserAccessData(-1, userType, userID, projectID, userAccess, string.Empty);
                        datNew.ProjectName = projectName;
                        pmpavShow.Add(datNew);
                    }

                }




                grdAccess.DataSource = pmpavShow;

                grdAccess.DataBind();
            }
            catch
            {
                lbAccessName.Visible = tblAccess.Visible = false;
            }
        }


		#endregion
		
		#region Web Form Designer generated code
        override protected void OnInit(EventArgs e)
        {
            //
            // CODEGEN: This call is required by the ASP.NET Web Form Designer.
            //
            InitializeComponent();
            base.OnInit(e);
        }
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
        private void InitializeComponent()
        {
            this.imgBack.Click += new System.Web.UI.ImageClickEventHandler(this.imgBack_Click);
            this.linkBack.Click += new System.EventHandler(this.link_Click);
            this.imgSave.Click += new System.Web.UI.ImageClickEventHandler(this.imgSave_Click);
            this.linkSave.Click += new System.EventHandler(this.linkSave_Click);
            this.btnAutoGenerate.Click += new System.EventHandler(this.btnAutoGenerate_Click);
            this.ddlSearch.SelectedIndexChanged += new System.EventHandler(this.ddlSearch_SelectedIndexChanged);
            this.ddlBuildingTypes.SelectedIndexChanged += new System.EventHandler(this.ddlBuildingTypes_SelectedIndexChanged);
            this.btnSearch.Click += new System.EventHandler(this.btnSearch_Click);
            this.btnClear.Click += new System.EventHandler(this.btnClear_Click);
            this.Load += new System.EventHandler(this.Page_Load);

        }
		#endregion

		#region EventHandlers
        private void save_click()
        {
            int userType = UIHelpers.GetPIDParam();
            int userID = UIHelpers.GetIDParam();
            if (UIHelpers.GetPIDParam() == (int)PMUserType.PMUser && txtUserName.Text.Length == 0)
            {
                AlertFieldNotEntered(lbFullName);
                Dirty = true;
                return;
            }
            if (txtAccountName.Text.Length == 0 || txtAccountPass.Text.Length == 0)
            {
                AlertFieldNotEntered(lbAccountName);
                AlertFieldNotEntered(lbAccountPass);
                Dirty = true;
                return;
            }
            System.Text.RegularExpressions.Regex mailRegex = new System.Text.RegularExpressions.Regex(@"\w+([-+.]\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*");
            if (txtPMMail.Text.Length > 0 && (!mailRegex.IsMatch(txtPMMail.Text)))
            {
                AlertFieldNotEntered(lbPMMail);
                Dirty = true;
                return;
            }


            string password = txtAccountPass.Text.Trim();
            if (password == hdnAccountPass.Value) password = txtHdnAccountPass.Text;

            if (txtAccountName.Text.Length > 0 && !(UIHelpers.CheckForUniqueNameAccount(txtAccountName.Text, OldAccName/*_accName*/)))
            {
                AlertFieldNotEntered(lbAccountName);
                lblError.Text = string.Format(UIHelpers.GetText(Language, "ErrorNotUniqueName"), txtAccountName.Text);
                return;
            }
            switch (userType)
            {
                case ((int)PMUserType.Client):
                    {
                        ClientData ud = ClientDAL.Load(UIHelpers.GetIDParam());
                        ud.AccountName = txtAccountName.Text;
                        ud.AccountPass = password;
                        ud.PMMail = txtPMMail.Text;

                        ClientDAL.Save(ud);
                        break;
                    }
                case ((int)PMUserType.Builder):
                    {
                        ClientData ud = ClientDAL.Load(UIHelpers.GetIDParam());
                        ud.AccountName = txtAccountName.Text;
                        ud.AccountPass = password;
                        ud.PMMail = txtPMMail.Text;

                        ClientDAL.Save(ud);
                        break;
                    }
                case ((int)PMUserType.Subcontracter):
                    {
                        SubcontracterData ud = SubcontracterDAL.Load(UIHelpers.GetIDParam());
                        ud.AccountName = txtAccountName.Text;
                        ud.AccountPass = password;
                        ud.PMMail = txtPMMail.Text;

                        SubcontracterDAL.Save(ud);
                        break;
                    }
                case ((int)PMUserType.PMUser):
                    {
                        if (UIHelpers.GetIDParam() != -1)
                        {
                            ClientData ud = ClientDAL.Load(UIHelpers.GetIDParam());
                            ud.ClientName = txtUserName.Text;
                            //ud.Email= txtUserEmail.Text;
                            ud.AccountName = txtAccountName.Text;
                            ud.AccountPass = password;
                            ud.PMMail = txtPMMail.Text;
                            ClientDAL.Save(ud);
                        }
                        else
                        {
                            ClientData ud = new ClientData();
                            ud.ClientName = txtUserName.Text;
                            //ud.Email= txtUserEmail.Text;
                            ud.AccountName = txtAccountName.Text;
                            ud.AccountPass = password;
                            ud.ClientType = (int)ClientTypes.PMUser;
                            ud.PMMail = txtPMMail.Text;
                            ClientDAL.Save(ud);
                            userID = ud.ClientID;
                        }
                        break;
                    }
                case ((int)PMUserType.Supervisors):
                    {
                        ClientData ud = ClientDAL.Load(UIHelpers.GetIDParam());
                        ud.AccountName = txtAccountName.Text;
                        ud.AccountPass = password;
                        ud.PMMail = txtPMMail.Text;
                        ClientDAL.Save(ud);
                        break;
                    }
                default: return;
            }
            if (txtAccountName.Text.Length > 0 && txtAccountPass.Text.Length > 0 && (txtAccountName.Text != OldAccName || password != OldAccPass) && LoggedUser.IsUser)
            {
                string mail = (txtPMMail.Text.Length > 0) ? txtPMMail.Text : lblUserMail.Text;
                MailBO.PMSendMailNewAccount(mail, txtAccountName.Text, password);
            }
            log.Info(string.Format(Resource.ResourceManager["log_info_PM_EditUser"], txtFullName.Text, LoggedUser.Account));

            //			if(LoggedUser.UserRoleID == (int)Roles.Director)
            //			{
            int userTypeID = UIHelpers.GetPIDParam();
            SaveAccess(userID, userTypeID);
            //			}


            //			if(LoggedUser.IsUser)
            Response.Redirect("Users.aspx");
            //			else
            //				Response.Redirect("Projects.aspx");
        }

//		private void btnSave_Click(object sender, System.EventArgs e)
//		{
//			int userType = UIHelpers.GetPIDParam();
//			int userID = UIHelpers.GetIDParam();
//			if(UIHelpers.GetPIDParam() == (int)PMUserType.PMUser&&txtUserName.Text.Length==0)
//			{
//				AlertFieldNotEntered(lbFullName);
//				Dirty=true;
//				return;
//			}
//			if(txtAccountName.Text.Length==0||txtAccountPass.Text.Length==0)
//			{
//				AlertFieldNotEntered(lbAccountName);
//				AlertFieldNotEntered(lbAccountPass);
//				Dirty=true;
//				return;
//			}
//			System.Text.RegularExpressions.Regex mailRegex = new System.Text.RegularExpressions.Regex(@"\w+([-+.]\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*");
//			if(txtPMMail.Text.Length>0&&(!mailRegex.IsMatch(txtPMMail.Text)))
//			{
//				AlertFieldNotEntered(lbPMMail);
//				Dirty=true;
//				return;
//			}
//
//
//			string password = txtAccountPass.Text.Trim();
//			if (password == hdnAccountPass.Value) password = txtHdnAccountPass.Text; 
//			
//			if(txtAccountName.Text.Length>0&& !(UIHelpers.CheckForUniqueNameAccount(txtAccountName.Text,OldAccName/*_accName*/)))
//			{
//				AlertFieldNotEntered(lbAccountName);
//				lblError.Text = string.Format(UIHelpers.GetText(Language,"ErrorNotUniqueName"),txtAccountName.Text);
//				return;
//			}
//			switch(userType)
//			{
//				case ((int)PMUserType.Client):
//				{
//					ClientData ud = ClientDAL.Load(UIHelpers.GetIDParam());
//					ud.AccountName = txtAccountName.Text;
//					ud.AccountPass = password;
//					ud.PMMail =txtPMMail.Text;
//					
//					ClientDAL.Save(ud);
//					break;
//				}
//				case ((int)PMUserType.Builder):
//				{
//					ClientData ud = ClientDAL.Load(UIHelpers.GetIDParam());
//					ud.AccountName = txtAccountName.Text;
//					ud.AccountPass = password;
//					ud.PMMail =txtPMMail.Text;
//					
//					ClientDAL.Save(ud);
//					break;
//				}
//				case ((int)PMUserType.Subcontracter):
//				{
//					SubcontracterData ud = SubcontracterDAL.Load(UIHelpers.GetIDParam());
//					ud.AccountName = txtAccountName.Text;
//					ud.AccountPass = password;
//					ud.PMMail =txtPMMail.Text;
//					
//					SubcontracterDAL.Save(ud);
//					break;
//				}
//				case ((int)PMUserType.PMUser):
//				{
//					if(UIHelpers.GetIDParam() != -1)
//					{
//						ClientData ud = ClientDAL.Load(UIHelpers.GetIDParam());
//						ud.ClientName = txtUserName.Text;
//						//ud.Email= txtUserEmail.Text;
//						ud.AccountName = txtAccountName.Text;
//						ud.AccountPass = password;
//						ud.PMMail =txtPMMail.Text;
//						ClientDAL.Save(ud);
//					}
//					else
//					{
//						ClientData ud = new ClientData();
//						ud.ClientName = txtUserName.Text;
//						//ud.Email= txtUserEmail.Text;
//						ud.AccountName = txtAccountName.Text;
//						ud.AccountPass = password;
//						ud.ClientType = (int)ClientTypes.PMUser;
//						ud.PMMail =txtPMMail.Text;
//						ClientDAL.Save(ud);
//						userID = ud.ClientID;
//					}
//					break;
//				}
//				case ((int)PMUserType.Supervisors):
//				{
//					ClientData ud = ClientDAL.Load(UIHelpers.GetIDParam());
//					ud.AccountName = txtAccountName.Text;
//					ud.AccountPass = password;
//					ud.PMMail =txtPMMail.Text;
//					ClientDAL.Save(ud);
//					break;
//				}
//				default:return;
//			}
//			if(txtAccountName.Text.Length>0&&txtAccountPass.Text.Length>0&&(txtAccountName.Text!=OldAccName||password!=OldAccPass)&&LoggedUser.IsUser)
//			{
//				string mail = (txtPMMail.Text.Length>0)?txtPMMail.Text:lblUserMail.Text;
//				MailBO.PMSendMailNewAccount(mail,txtAccountName.Text,password);
//			}
//			log.Info(string.Format(Resource.ResourceManager["log_info_PM_EditUser"],txtFullName.Text,LoggedUser.Account));
//
////			if(LoggedUser.UserRoleID == (int)Roles.Director)
////			{
//				int userTypeID = UIHelpers.GetPIDParam();
//				SaveAccess(userID,userTypeID);
////			}
//
//
////			if(LoggedUser.IsUser)
//				Response.Redirect("Users.aspx");
////			else
////				Response.Redirect("Projects.aspx");
//		}

        private void SaveAccess(int userID, int userType)
        {

            foreach (DataGridItem dgi in grdAccess.Items)
            {
                PMProjectUserAccessData dat = new PMProjectUserAccessData();
                dat.PMProjectUserAccessID = UIHelpers.ToInt(dgi.Cells[(int)gridColomns.AccessID].Text);
                dat.UserAccessID = userID;
                dat.UserType = userType;
                dat.ProjectID = UIHelpers.ToInt(dgi.Cells[(int)gridColomns.ProjectID].Text);
                CheckBox cb = (CheckBox)dgi.FindControl("cbAccess");
                dat.HasAccess = cb.Checked;
                TextBox txt = (TextBox)dgi.FindControl("txtProjectMail");
                dat.ProjectMail = txt.Text;
                txt = (TextBox)dgi.FindControl("txtResponsible");
                dat.Responsible = txt.Text;
                PMProjectUserAccessDAL.Save(dat);
            }
        }

        private void btnDelete_Click(object sender, System.EventArgs e)
        {
            int userType = UIHelpers.GetPIDParam();
            switch (userType)
            {
                case ((int)PMUserType.Client):
                    {
                        ClientData ud = ClientDAL.Load(UIHelpers.GetIDParam());
                        ud.AccountName = "";
                        ud.AccountPass = "";
                        ClientDAL.Save(ud);
                        break;
                    }
                case ((int)PMUserType.Builder):
                    {
                        ClientData ud = ClientDAL.Load(UIHelpers.GetIDParam());
                        ud.AccountName = "";
                        ud.AccountPass = "";
                        ClientDAL.Save(ud);
                        break;
                    }
                case ((int)PMUserType.Subcontracter):
                    {
                        SubcontracterData ud = SubcontracterDAL.Load(UIHelpers.GetIDParam());
                        ud.AccountName = "";
                        ud.AccountPass = "";
                        SubcontracterDAL.Save(ud);
                        break;
                    }
                default: return;
            }
            log.Info(string.Format(Resource.ResourceManager["log_info_PM_EditUser"], txtFullName.Text, LoggedUser.Account));
            LoadUser();
        }

		
//		private void btnBack_Click(object sender, System.EventArgs e)
//		{
//			Response.Redirect("Users.aspx");
//		}

        private void back_click()
        {
            Response.Redirect("Users.aspx");
        }


        private void ddlBuildingTypes_SelectedIndexChanged(object sender, System.EventArgs e)
        {
            LoadAccess();
        }

        private void ddlSearch_SelectedIndexChanged(object sender, System.EventArgs e)
        {
            LoadAccess();
        }

        private void btnSearch_Click(object sender, System.EventArgs e)
        {
            LoadAccess();
        }

        private void btnClear_Click(object sender, System.EventArgs e)
        {
            txtProject.Text = "";
            ddlSearch.SelectedIndex = 0;
            ddlBuildingTypes.SelectedIndex = 0;
            LoadAccess();
        }

        private void btnAutoGenerate_Click(object sender, System.EventArgs e)
        {
            txtAccountPass.Text = Generate();
            txtHdnAccountPass.Text = txtAccountPass.Text;
            hdnAccountPass.Value = "";
            for (int i = 0; i < txtAccountPass.Text.Length; i++) hdnAccountPass.Value += "*";
        }
        private void imgBack_Click(object sender, System.Web.UI.ImageClickEventArgs e)
        {
            back_click();
        }
        private void link_Click(object sender, System.EventArgs e)
        {
            back_click();
        }
        private void imgSave_Click(object sender, System.Web.UI.ImageClickEventArgs e)
        {
            save_click();
        }
        private void linkSave_Click(object sender, System.EventArgs e)
        {
            save_click();
        }
		
		#endregion


		#region GeneratePass

		private char[] characterArray;
		private int passwordLength = 6;
		Random randNum = new Random();

		//		public PasswordGenerator()
		//		{
		//			
		//		}

        private char GetRandomCharacter()
        {
            char c = this.characterArray[(int)((this.characterArray.GetUpperBound(0) + 1) * randNum.NextDouble())];
            return c;
        }

        public string Generate()
        {
            characterArray = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789".ToCharArray();
            StringBuilder sb = new StringBuilder();
            sb.Capacity = passwordLength;
            for (int count = 0; count <= passwordLength - 1; count++)
            {
                sb.Append(GetRandomCharacter());
            }
            if ((sb != null))
            {
                string returnValue = sb.ToString();
                return returnValue;
            }
            return string.Empty;
        }

		

		#endregion
	}
}
