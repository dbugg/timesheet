using System;
using System.Data;
using GrapeCity.ActiveReports;
using GrapeCity.ActiveReports.Controls;
using GrapeCity.ActiveReports.SectionReportModel;
using GrapeCity.ActiveReports.Document.Section;
using GrapeCity.ActiveReports.Document;

namespace Asa.Timesheet.WebPages.Reports
{
    public class ChronologyReport : GrapeCity.ActiveReports.SectionReport
	{
		bool _forPDF=false;

        public ChronologyReport(DataView dv, string type, string project, bool pdf)
        {
            _forPDF = pdf;
            InitializeComponent();
            lbZad.Text = type;
            foreach (DataRowView drv in dv)
            {
                if (drv["Item"] != DBNull.Value)
                    drv["Item"] = UIHelpers.StripHTML((string)drv["Item"]);
            }
            if (project.Length > 0 && !project.StartsWith("<"))
                lbProject.Text = project;
            else
                Label5.Visible = false;

            this.DataSource = dv;
            this.ReportEnd += new EventHandler(ChronologyReport_ReportEnd);
        }

		#region ActiveReports Designer generated code

















        public void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ChronologyReport));
            this.Detail = new GrapeCity.ActiveReports.SectionReportModel.Detail();
            this.ReportHeader = new GrapeCity.ActiveReports.SectionReportModel.ReportHeader();
            this.ReportFooter = new GrapeCity.ActiveReports.SectionReportModel.ReportFooter();
            this.PageHeader = new GrapeCity.ActiveReports.SectionReportModel.PageHeader();
            this.PageFooter = new GrapeCity.ActiveReports.SectionReportModel.PageFooter();
            this.lbTitle = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label4 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label5 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lbZad = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lbProject = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label1 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label2 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Line1 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.Label9 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.TextBox1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.TextBox2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.TextBox4 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            ((System.ComponentModel.ISupportInitialize)(this.lbTitle)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbZad)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbProject)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TextBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TextBox2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TextBox4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            // 
            // Detail
            // 
            this.Detail.ColumnSpacing = 0F;
            this.Detail.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
						this.TextBox1,
						this.TextBox2,
						this.TextBox4});
            this.Detail.Height = 0.21875F;
            this.Detail.Name = "Detail";
            // 
            // ReportHeader
            // 
            this.ReportHeader.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
						this.lbTitle,
						this.Label4,
						this.Label5,
						this.lbZad,
						this.lbProject,
						this.Label1,
						this.Label2,
						this.Line1,
						this.Label9});
            this.ReportHeader.Height = 1.552083F;
            this.ReportHeader.Name = "ReportHeader";
            // 
            // ReportFooter
            // 
            this.ReportFooter.Height = 0F;
            this.ReportFooter.Name = "ReportFooter";
            // 
            // PageHeader
            // 
            this.PageHeader.Height = 0F;
            this.PageHeader.Name = "PageHeader";
            // 
            // PageFooter
            // 
            this.PageFooter.Height = 0F;
            this.PageFooter.Name = "PageFooter";
            // 
            // lbTitle
            // 
            this.lbTitle.Height = 0.2F;
            this.lbTitle.HyperLink = null;
            this.lbTitle.Left = 1F;
            this.lbTitle.Name = "lbTitle";
            this.lbTitle.Style = "font-size: 15pt; font-weight: bold; text-align: center; ddo-char-set: 1";
            this.lbTitle.Text = "?????????? ?? ??????";
            this.lbTitle.Top = 0F;
            this.lbTitle.Width = 7.5625F;
            // 
            // Label4
            // 
            this.Label4.Height = 0.2F;
            this.Label4.HyperLink = null;
            this.Label4.Left = 1F;
            this.Label4.Name = "Label4";
            this.Label4.Style = "font-weight: bold";
            this.Label4.Text = "??????:";
            this.Label4.Top = 0.4375F;
            this.Label4.Width = 1.375F;
            // 
            // Label5
            // 
            this.Label5.Height = 0.2F;
            this.Label5.HyperLink = null;
            this.Label5.Left = 1F;
            this.Label5.Name = "Label5";
            this.Label5.Style = "font-weight: bold";
            this.Label5.Text = "??????:";
            this.Label5.Top = 0.625F;
            this.Label5.Width = 1.375F;
            // 
            // lbZad
            // 
            this.lbZad.Height = 0.2F;
            this.lbZad.HyperLink = null;
            this.lbZad.Left = 2.375F;
            this.lbZad.Name = "lbZad";
            this.lbZad.Style = "";
            this.lbZad.Text = "";
            this.lbZad.Top = 0.4375F;
            this.lbZad.Width = 6.1875F;
            // 
            // lbProject
            // 
            this.lbProject.Height = 0.2000001F;
            this.lbProject.HyperLink = null;
            this.lbProject.Left = 2.375F;
            this.lbProject.Name = "lbProject";
            this.lbProject.Style = "";
            this.lbProject.Text = "";
            this.lbProject.Top = 0.625F;
            this.lbProject.Width = 6.1875F;
            // 
            // Label1
            // 
            this.Label1.Height = 0.2F;
            this.Label1.HyperLink = null;
            this.Label1.Left = 1F;
            this.Label1.Name = "Label1";
            this.Label1.Style = "font-weight: bold";
            this.Label1.Text = "????";
            this.Label1.Top = 1.3125F;
            this.Label1.Width = 1.375F;
            // 
            // Label2
            // 
            this.Label2.Height = 0.2F;
            this.Label2.HyperLink = null;
            this.Label2.Left = 2.375F;
            this.Label2.Name = "Label2";
            this.Label2.Style = "font-weight: bold; text-align: justify";
            this.Label2.Text = "????????";
            this.Label2.Top = 1.3125F;
            this.Label2.Width = 3.9375F;
            // 
            // Line1
            // 
            this.Line1.Height = 0F;
            this.Line1.Left = 1F;
            this.Line1.LineWeight = 1F;
            this.Line1.Name = "Line1";
            this.Line1.Top = 1.5625F;
            this.Line1.Width = 9.1875F;
            this.Line1.X1 = 1F;
            this.Line1.X2 = 10.1875F;
            this.Line1.Y1 = 1.5625F;
            this.Line1.Y2 = 1.5625F;
            // 
            // Label9
            // 
            this.Label9.Height = 0.2F;
            this.Label9.HyperLink = null;
            this.Label9.Left = 6.3125F;
            this.Label9.Name = "Label9";
            this.Label9.Style = "font-weight: bold; text-align: justify";
            this.Label9.Text = "???????";
            this.Label9.Top = 1.3125F;
            this.Label9.Width = 2.25F;
            // 
            // TextBox1
            // 
            this.TextBox1.DataField = "Date";
            this.TextBox1.Height = 0.2F;
            this.TextBox1.Left = 1F;
            this.TextBox1.Name = "TextBox1";
            this.TextBox1.OutputFormat = resources.GetString("TextBox1.OutputFormat");
            this.TextBox1.Top = 0F;
            this.TextBox1.Width = 1.375F;
            // 
            // TextBox2
            // 
            this.TextBox2.DataField = "Item";
            this.TextBox2.Height = 0.2F;
            this.TextBox2.Left = 2.375F;
            this.TextBox2.Name = "TextBox2";
            this.TextBox2.Top = 0F;
            this.TextBox2.Width = 3.9375F;
            // 
            // TextBox4
            // 
            this.TextBox4.DataField = "Details";
            this.TextBox4.Height = 0.2F;
            this.TextBox4.Left = 6.3125F;
            this.TextBox4.Name = "TextBox4";
            this.TextBox4.OutputFormat = resources.GetString("TextBox4.OutputFormat");
            this.TextBox4.Style = "text-align: left";
            this.TextBox4.Top = 0F;
            this.TextBox4.Width = 2.25F;
            // 
            // ActiveReport1
            // 
            this.MasterReport = false;
            this.PageSettings.PaperHeight = 11F;
            this.PageSettings.PaperWidth = 8.5F;
            this.PrintWidth = 8.572917F;
            this.Sections.Add(this.ReportHeader);
            this.Sections.Add(this.PageHeader);
            this.Sections.Add(this.Detail);
            this.Sections.Add(this.PageFooter);
            this.Sections.Add(this.ReportFooter);
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule(resources.GetString("$this.StyleSheet"), "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: inherit; font-style: inherit; font-variant: inherit; font-weight: bo" +
                        "ld; font-size: 16pt; font-size-adjust: inherit; font-stretch: inherit", "Heading1", "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: Times New Roman; font-style: italic; font-variant: inherit; font-wei" +
                        "ght: bold; font-size: 14pt; font-size-adjust: inherit; font-stretch: inherit", "Heading2", "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: inherit; font-style: inherit; font-variant: inherit; font-weight: bo" +
                        "ld; font-size: 13pt; font-size-adjust: inherit; font-stretch: inherit", "Heading3", "Normal"));
            ((System.ComponentModel.ISupportInitialize)(this.lbTitle)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbZad)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbProject)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TextBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TextBox2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TextBox4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();
        }

		#endregion

        private void ChronologyReport_ReportEnd(object sender, EventArgs e)
        {
            if (!this._forPDF) return;

            Logo1 logo = new Logo1();
            logo.Run();

            for (int i = 0; i < this.Document.Pages.Count; i++)
                this.Document.Pages[i].Overlay(logo.Document.Pages[0]);
        }

        private ReportHeader ReportHeader;
        private Label lbTitle;
        private Label Label4;
        private Label Label5;
        private Label lbZad;
        private Label lbProject;
        private Label Label1;
        private Label Label2;
        private Line Line1;
        private Label Label9;
        private PageHeader PageHeader;
        private Detail Detail;
        private TextBox TextBox1;
        private TextBox TextBox2;
        private TextBox TextBox4;
        private PageFooter PageFooter;
        private ReportFooter ReportFooter;
	}
}
