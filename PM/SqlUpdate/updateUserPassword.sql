USE [asatimesheet]
GO
/****** Object:  StoredProcedure [dbo].[UsersUpdProc]    Script Date: 12/19/2017 4:12:31 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO








------------------------------------------------------------------------------------------------------------------------
-- Date Created: Sunday, May 22, 2005
-- Created By:   Generated by CodeSmith
------------------------------------------------------------------------------------------------------------------------

ALTER    PROCEDURE [dbo].[UsersUpdProc]
	@UserID int, 
	@RoleID int = NULL, 
	@FirstName nvarchar(50) = NULL, 
	@MiddleName nvarchar(50) = NULL, 
	@LastName nvarchar(50), 
	@Account varchar(50), 
	@Mail varchar(50),
	--@FullName nvarchar(154) = NULL, 
	--@IsActive bit 
	@Password varchar(50), 
	@Occupation nvarchar(100) = NULL, 
	@UserRole nvarchar(100) = NULL, 
	@Ext nvarchar(50) = NULL, 
	@Mobile nvarchar(50) = NULL, 
	@HomePhone nvarchar(50) = NULL, 
	@CV ntext = NULL ,
	@Salary decimal = null,
	@Bonus decimal = null,
	@IsTraine bit = null,
	@MobilePersonal nvarchar(50) = NULL,
	@IsASI bit = NULL,
	@HasWorkTime bit = NULL,
	@WorkStartedDate smalldatetime = null,
	@HasWorkTimeOver bit = null,
	@Manager int = null,
	@VacationLeft int = null,
	@UserNameEN nvarchar(100) =null,
	@PrivateEmail nvarchar(50) =null,
	@Coefficient float=1
AS

UPDATE [dbo].[Users] SET
	[RoleID] = @RoleID,
	[FirstName] = @FirstName,
	[MiddleName] = @MiddleName,
	[LastName] = @LastName,
	[Account] = @Account,
	[Mail] = @Mail,
	--[IsActive] = @IsActive
	[Password] = ISNULL(@Password, Password),
	[Occupation] = @Occupation,
	[UserRole] = @UserRole,
	[Ext] = @Ext,
	[Mobile] = @Mobile,
	[HomePhone] = @HomePhone,
	[CV] = @CV,
	Salary=@Salary,
	Bonus=@Bonus,
	IsTraine=@IsTraine,
	MobilePersonal=@MobilePersonal,
	IsASI=@IsASI,
	HasWorkTime = @HasWorkTime,
	WorkStartedDate = @WorkStartedDate,
	HasWorkTimeOver = @HasWorkTimeOver,
	Manager = @Manager,
	VacationLeft=@VacationLeft,
	UserNameEN=@UserNameEN,
	PrivateEmail=@PrivateEmail,
	Coefficient=@Coefficient
WHERE
	[UserID] = @UserID




