namespace Asa.Timesheet.WebPages.UserControls
{
	using System;
	using System.Data;
	using System.Drawing;
	using System.Web;
	using System.Web.UI.WebControls;
	using System.Web.UI.HtmlControls;
	using Asa.Timesheet.Data;
	/// <summary>
	///		Summary description for Header.
	/// </summary>
	public class PageHeader : System.Web.UI.UserControl
	{
		protected System.Web.UI.WebControls.Label lblTitle;
		protected System.Web.UI.WebControls.LinkButton lbLogOut;
		protected System.Web.UI.WebControls.Button btnLogOut;
		protected System.Web.UI.WebControls.Button btnEdit;
		protected System.Web.UI.WebControls.HyperLink HyperLink1;
		protected System.Web.UI.WebControls.HyperLink HyperLink2;
		protected System.Web.UI.WebControls.Label lblUser;
		public delegate void EditHandler(object sender, System.EventArgs e);
		public event EditHandler EditClicked;

		protected virtual void FireEditClicked(object sender,  System.EventArgs e)
		{
			if (EditClicked != null) 
			{
				EditClicked(this, e); 
			}

		}
		private void Page_Load(object sender, System.EventArgs e)
		{
				HyperLink1.Visible=SessionManager.LoggedUserInfo.UserID!=0;
		}

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		///		Required method for Designer support - do not modify
		///		the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.btnLogOut.Click += new System.EventHandler(this.btnLogOut_Click);
			this.lbLogOut.Click += new System.EventHandler(this.lbLogOut_Click);
			this.btnEdit.Click += new System.EventHandler(this.btnEdit_Click);
			this.Load += new System.EventHandler(this.Page_Load);

		}
		#endregion

		private void lbLogOut_Click(object sender, System.EventArgs e)
		{
			System.Web.Security.FormsAuthentication.SignOut();
			Session.Abandon();
			Response.Redirect(Request.ApplicationPath+"/Login.aspx");
		}

		private void btnLogOut_Click(object sender, System.EventArgs e)
		{
			System.Web.Security.FormsAuthentication.SignOut();
			Session.Abandon();
			Response.Redirect(Request.ApplicationPath);//+"/hours.aspx");//"/Login.aspx");
		}

		private void btnEdit_Click(object sender, System.EventArgs e)
		{
			btnEdit.Visible=!btnEdit.Visible;
			FireEditClicked(this,e);
		}
	
		public bool EditMode
		{
			set { btnEdit.Visible = !value; }
			get {return !btnEdit.Visible;}

		}
		public string PageTitle
		{
			set { lblTitle.Text = value; }
		}

		public string UserName
		{
			set { lblUser.Text = value; }
		}
	}
}
