using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Web.Security;
using log4net;
using Asa.Timesheet.Data;

namespace Asa.Timesheet.WebPages
{
	/// <summary>
	/// Summary description for Login.
	/// </summary>
	public class Login : System.Web.UI.Page
	{
		protected System.Web.UI.WebControls.PlaceHolder menuHolder;
		protected System.Web.UI.WebControls.Label lblError;
		protected System.Web.UI.WebControls.Label lblInfo;
		protected System.Web.UI.WebControls.Button btnEnter;
		protected System.Web.UI.WebControls.TextBox txtPassword;
		protected Asa.Timesheet.WebPages.UserControls.PageHeader header;
		protected System.Web.UI.WebControls.TextBox txtLoginName;
		protected System.Web.UI.WebControls.Label lblPassword;
		protected System.Web.UI.WebControls.Label lblLoginName;

		private static readonly ILog log = LogManager.GetLogger(typeof(Login));
	
		private void Page_Load(object sender, System.EventArgs e)
		{
			Page.RegisterHiddenField("__EVENTTARGET", "btnEnter");
			if (!this.IsPostBack)
			{
				header.PageTitle = Resource.ResourceManager["login_PageTitle"];
			}

     
     
		}

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
			this.btnEnter.Click += new System.EventHandler(this.btnEnter_Click);
			this.Load += new System.EventHandler(this.Page_Load);

		}
		#endregion

		private void btnEnter_Click(object sender, System.EventArgs e)
		{
			FormsAuthentication.SignOut();

			if (txtLoginName.Text.Trim()==String.Empty)
			{
				AlertFieldNotEntered(this.lblLoginName);
				return;
			}

			if (txtPassword.Text.Trim()==String.Empty)
			{
				AlertFieldNotEntered(this.lblPassword);
				return;
			}
			
			string account = String.Empty;

			try
			{
				account = DBManager.SelectUserAccount(txtLoginName.Text, txtPassword.Text);
			}
			catch (Exception ex)
			{
				log.Error(ex);
				lblError.Text = Resource.ResourceManager["login_ErrorCheckCredentials"];
				return;
			}
			
			if (account == null)
			{
				lblError.Text = Resource.ResourceManager["login_ErrorInvalidCredentials"];
				return;
			}

		
			FormsAuthentication.RedirectFromLoginPage(account, false);
		
		}

		protected static void AlertFieldNotEntered(Label lbl)
		{
			lbl.ForeColor = System.Drawing.Color.Red;
		}

	}
}
