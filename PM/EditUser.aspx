﻿<%@ Page Title="Редакция на потребител" Language="C#" MasterPageFile="~/PM.Master" AutoEventWireup="True" CodeBehind="EditUser.aspx.cs" Inherits="PM.EditUser" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
  
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
<table height="100%" cellSpacing="0" cellPadding="0" width="100%" border="0">
	<tr>
					<td>
						<table height="100%" cellSpacing="0" cellPadding="0" width="100%" border="0">
							<tr>
								<td style="PADDING-RIGHT: 5px; PADDING-LEFT: 5px; PADDING-TOP: 3px" vAlign="top" noWrap>
									<table>
										<tr>
											<td vAlign="middle"><asp:linkbutton id="linkBack" Runat="server" CssClass="Brown"></asp:linkbutton></td>
											<td>
                                            <asp:button id="btnSave" runat="server" CssClass="ActionButton" ></asp:button></td>
											<td><asp:imagebutton id="imgEdit" Runat="server" ImageUrl="images/7.gif" style="DISPLAY: none"></asp:imagebutton></td>
											<td vAlign="middle"><asp:linkbutton id="linkEdit" Runat="server" CssClass="Brown" style="DISPLAY: none"></asp:linkbutton></td>
										</tr>
									</table>
									<P><asp:label id="lblError" runat="server" CssClass="ErrorLabel" EnableViewState="False" ForeColor="Red"></asp:label><asp:label id="lblInfo" runat="server" CssClass="InfoLabel" EnableViewState="False"></asp:label><BR>
										<table id="tblForm" runat="server" cellSpacing="0" cellPadding="3" width="600" border="0">
											<tr>
												<td><asp:label id="lbFullName" runat="server" EnableViewState="False" Font-Bold="True" CssClass="enterDataLabel"></asp:label></td>
												<td style="WIDTH: 400px"><asp:label id="txtFullName" runat="server" CssClass="enterDataBox" Width="350px"></asp:label><asp:TextBox id="txtUserName" runat="server" CssClass="enterDataBox" Width="250px" Visible="False"></asp:TextBox></TD>
											</tr>
											<tr>
												<td style="WIDTH: 179px"><asp:label id="lbAccountName" runat="server" EnableViewState="False" Font-Bold="True" CssClass="enterDataLabel"></asp:label></td>
												<td style="WIDTH: 400px"><asp:TextBox id="txtAccountName" runat="server" CssClass="enterDataBox" Width="250px"></asp:TextBox></TD>
											</tr>
											<tr>
												<td style="WIDTH: 179px"><asp:label id="lbAccountPass" runat="server" EnableViewState="False" Font-Bold="True" CssClass="enterDataLabel"></asp:label></td>
												<td style="WIDTH: 400px"><asp:TextBox id="txtAccountPass" runat="server" CssClass="enterDataBox" Width="250px"></asp:TextBox>&nbsp;
													<asp:button id="btnAutoGenerate" runat="server" EnableViewState="False" CssClass="ActionButton"
														Width="100px" Visible="False"></asp:button><asp:textbox id="txtHdnAccountPass" runat="server" Visible="False"></asp:textbox><INPUT id="hdnAccountPass" type="hidden" runat="server" NAME="hdnPassword"></TD>
											</tr>
											<tr>
												<td style="WIDTH: 179px"><asp:label id="lbPMMail" runat="server" EnableViewState="False" Font-Bold="True" CssClass="enterDataLabel"></asp:label></td>
												<td style="WIDTH: 400px"><asp:TextBox id="txtPMMail" runat="server" CssClass="enterDataBox" Width="250px"></asp:TextBox>
													<asp:label id="lblUserMail" runat="server" EnableViewState="False" Font-Bold="True" Visible="False"></asp:label></TD>
											</tr>
										</table>
										<br>
										<TABLE id="tblAccess" runat="server" cellSpacing="0" cellPadding="0" border="0" width="100%">
											<tr>
												<td style="WIDTH: 677px">
													<TABLE id="Table1" style="WIDTH: 840px; HEIGHT: 83px" cellSpacing="1" cellPadding="1" width="840"
														border="0">
														<TR>
															<td style="WIDTH: 154px"></TD>
															<td style="WIDTH: 542px"><asp:Label id="lbAccessName" runat="server" EnableViewState="False" Font-Bold="True">test</asp:Label>
															</TD>
														</TR>
														<TR>
															<td align="right"><asp:label id="lblName" runat="server" EnableViewState="False" CssClass="enterDataLabel" Width="100%"
																	Font-Bold="True"></asp:label></TD>
															<td style="WIDTH: 542px"><asp:textbox id="txtProject" runat="server" CssClass="enterDataBox" Width="165px" MaxLength="30"></asp:textbox>&nbsp;<asp:dropdownlist id="ddlSearch" runat="server" CssClass="enterDataBox" Width="164px" AutoPostBack="True"></asp:dropdownlist>&nbsp;<asp:dropdownlist id="ddlBuildingTypes" runat="server" CssClass="enterDataBox" Width="190px" AutoPostBack="True"></asp:dropdownlist></TD>
														</TR>
														<TR>
															<td style="WIDTH: 154px"></TD>
															<td style="WIDTH: 535px">
																<P><asp:button id="btnSearch" runat="server" CssClass="ActionButton" Width="80px"></asp:button>&nbsp;<asp:button id="btnClear" runat="server" CssClass="ActionButton" Width="80px"></asp:button>&nbsp;&nbsp;&nbsp;</P>
															</TD>
														</TR>
													</TABLE>
												</td>
											</tr>
											<TR>
												<td>
													<asp:panel id="grid" 
														runat="server" Width="98%" Height="330px">
														<asp:datagrid id="grdAccess" runat="server" Width="100%" CssClass="Grid" AutoGenerateColumns="False"
															CellPadding="4" PageSize="2" AllowSorting="True">
															<ItemStyle CssClass="GridItem"></ItemStyle>
															<HeaderStyle Wrap="False" CssClass="GridHeader"></HeaderStyle>
															<Columns>
																<asp:BoundColumn DataField="PMProjectUserAccessID" Visible="False">
																	<HeaderStyle></HeaderStyle>
																</asp:BoundColumn>
																<asp:BoundColumn DataField="ProjectID" Visible="False">
																	<HeaderStyle></HeaderStyle>
																</asp:BoundColumn>
																<asp:TemplateColumn HeaderStyle-Width="45px" HeaderStyle-HorizontalAlign="Right" ItemStyle-HorizontalAlign="Right">
																	<ItemTemplate>
																		<asp:CheckBox ID="cbAccess" Runat="server" Checked='<%# DataBinder.Eval(Container, "DataItem.HasAccess") %>'>
																		</asp:CheckBox>
																	</ItemTemplate>
																</asp:TemplateColumn>
																<asp:BoundColumn DataField="ProjectName" HeaderText="Проект">
																	<HeaderStyle></HeaderStyle>
																</asp:BoundColumn>
																<asp:TemplateColumn HeaderStyle-HorizontalAlign="Left" ItemStyle-HorizontalAlign="Left" HeaderText="Отговорник по проекта">
																	<ItemTemplate>
																		<asp:TextBox ID="txtResponsible" Width="250px" Runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.Responsible") %>'>
																		</asp:TextBox>
																	</ItemTemplate>
																</asp:TemplateColumn>
																<asp:TemplateColumn HeaderStyle-HorizontalAlign="Left" ItemStyle-HorizontalAlign="Left" HeaderText="Е-мейл по проекта">
																	<ItemTemplate>
																		<asp:TextBox ID="txtProjectMail" Width="250px" Runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.ProjectMail") %>'>
																		</asp:TextBox>
																	</ItemTemplate>
																</asp:TemplateColumn>
															</Columns>
														</asp:datagrid>
													</asp:panel>
												</TD>
											</TR>
										</TABLE>

									</P>
								</td>
							</tr>
						</table>
					</td>
				</tr>
</table>
</asp:Content>