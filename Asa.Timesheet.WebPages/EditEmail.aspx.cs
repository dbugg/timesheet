using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Data.SqlClient;
using log4net;
using Asa.Timesheet.Data;
using Asa.Timesheet.Data.Entities;


namespace Asa.Timesheet.WebPages
{
	/// <summary>
	/// Summary description for EditEmail.
	/// </summary>
	public class EditEmail : TimesheetPageBase
	{
		#region Web controls

		protected System.Web.UI.WebControls.PlaceHolder menuHolder;
		protected System.Web.UI.WebControls.Label lblName;
		protected System.Web.UI.WebControls.TextBox txtName;
		protected System.Web.UI.WebControls.Button btnSave;
		protected System.Web.UI.WebControls.Button btnCancel;
		protected System.Web.UI.WebControls.Button btnDelete;
		protected System.Web.UI.WebControls.Label lblInfo;
		protected System.Web.UI.WebControls.TextBox txtEmail;
		protected System.Web.UI.WebControls.DropDownList ddlClients;
		protected System.Web.UI.WebControls.Label lblEmail;
		protected System.Web.UI.WebControls.Label lblClient;
		protected Asa.Timesheet.WebPages.UserControls.PageHeader header;

		#endregion
		protected System.Web.UI.WebControls.Label ErrorLabel;
		protected System.Web.UI.WebControls.Label lblError;
		

		private static readonly ILog log = LogManager.GetLogger(typeof(EditEmail));
	
		private void Page_Load(object sender, System.EventArgs e)
		{
			if (!(LoggedUser.HasRole)) ErrorRedirect(Resource.ResourceManager["noRightsAlert"]);
		
			lblInfo.Text = String.Empty;
			lblError.Text = String.Empty;

			UIHelpers.CreateMenu(menuHolder,LoggedUser);
			if (!this.IsPostBack)
			{
				header.UserName = LoggedUser.FullName;

				int emailID = EmailID;
				if (emailID > 0)
				{
					header.PageTitle = Resource.ResourceManager["editEmail_EditLabel"];
					if (!LoadEmail(emailID))
					{				
						ErrorRedirect(Resource.ResourceManager["editEmail_ErrorLoadEmail"]);
					}
					SetConfirmDelete(btnDelete, txtName.Text);
				}
				else
				{
					header.PageTitle = Resource.ResourceManager["editEmail_NewLabel"];
					btnDelete.Visible = false;

					//LoadClients("0");
				}
			}
		}

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
			this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
			this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
			this.btnDelete.Click += new System.EventHandler(this.btnDelete_Click);
			this.Load += new System.EventHandler(this.Page_Load);

		}
		#endregion

		private void btnSave_Click(object sender, System.EventArgs e)
		{
			string name = txtName.Text.Trim();
			string email = txtEmail.Text.Trim();

			//int clientID = int.Parse(ddlClients.SelectedValue);

			if (name == String.Empty) 
			{
				AlertFieldNotEntered(lblName);
				return;
			}

			if (email == String.Empty) 
			{
				AlertFieldNotEntered(lblEmail);
				return;
			}

			int emailID = EmailID;
			if (emailID == -1)
			{
				if (!InsertEmail(name, email))//, clientID))
				{
					lblError.Text = Resource.ResourceManager["editEmail_ErrorSave"];
					return;
				}
			}
			else
			{
				if (!UpdateEmail(emailID, name, email))//, clientID))
				{
					lblError.Text = Resource.ResourceManager["editEmail_ErrorSave"];
					return;
				}
			}

			Response.Redirect("Emails.aspx");
		}

		private void btnCancel_Click(object sender, System.EventArgs e)
		{
			Response.Redirect("Emails.aspx");
		}

		private void btnDelete_Click(object sender, System.EventArgs e)
		{
			if (!DeleteEmail(EmailID))
			{
				return;
			}

			Response.Redirect("Emails.aspx");
		}

		#region Database

		private bool LoadEmail(int emailID)
		{
			//int clientID = 0;
			SqlDataReader reader = null;
			try
			{
				reader = EmailsData.SelectEmail(emailID);
				if (reader.Read())
				{
					txtName.Text = reader.IsDBNull(1) ? String.Empty : reader.GetString(1);
					txtEmail.Text = reader.IsDBNull(2) ? String.Empty : reader.GetString(2);
					//clientID = reader.IsDBNull(3) ? 0 : reader.GetInt32(3);
				}
				else return false;
			}
			catch (Exception ex)
			{
				log.Error(ex);
				return false;
			}
			finally
			{
				if (reader!=null) reader.Close();
			}

			//if (!LoadClients(clientID.ToString())) return false;

			return true;
		}

		private bool LoadClients(string selectedValue)
		{
			SqlDataReader reader = null;
			try
			{
				reader = ClientsData.SelectClients(0);
				ddlClients.DataSource = reader;
				ddlClients.DataValueField = "ClientID";
				ddlClients.DataTextField = "ClientName";
				ddlClients.DataBind();

				ddlClients.Items.Insert(0, new ListItem("", "0"));
				ddlClients.SelectedValue = selectedValue;
			}
			catch (Exception ex)
			{
				log.Info(ex);
				return false;
			}
			finally 
			{
				if (reader!=null) reader.Close();
			}
			return true;
		}

		private bool DeleteEmail(int emailID)
		{
			try
			{
				EmailsData.DeleteEmail(emailID);
			}
			catch (Exception ex)
			{
				log.Error(ex);
				return false;
			}

			return true;
		}

		private bool InsertEmail(string name, string email) //, int clientID)
		{
			try
			{
				EmailsData.InsertEmail(name, email);//, clientID);
			}
			catch (Exception ex)
			{
				log.Error(ex);
				return false;
			}

			return true;
		}

		private bool UpdateEmail(int emailID, string name, string email)//, int clientID)
		{
			try
			{
				EmailsData.UpdateEmail(emailID, name, email);//, clientID);
			}
			catch (Exception ex)
			{
				log.Error(ex);
				return false;
			}

			return true;
		}

		#endregion

		#region private properties 
		private int EmailID
		{
			get 
			{
				try
				{
					return int.Parse(Request.Params["eid"]);
				}
				catch
				{
					return -1;
				}
			}		
		}
		#endregion

		#region Menu

//		private void CreateMenu()
//		{
//			UserControls.MenuTable menu = new UserControls.MenuTable();
//			menu.ID = "MenuTable";
//
//			// 'Links' group
//			ArrayList menuItems = new ArrayList();
//
//			menuItems.Add(new MenuItemInfo(Resource.ResourceManager["menuItem_Hours"], "Hours.aspx", true));
//			menuItems.Add(new MenuItemInfo(Resource.ResourceManager["menuItem_Projects"], "Projects.aspx"));
//
//			if (LoggedUser.IsLeader || LoggedUser.IsAssistant) 
//			{
//				menuItems.Add(new MenuItemInfo(Resource.ResourceManager["menuItem_Clients"], "Clients.aspx"));
//				menuItems.Add(new MenuItemInfo(Resource.ResourceManager["menuItem_Subcontracters"], "Subcontracters.aspx"));
//				if (LoggedUser.IsLeader)
//					menuItems.Add(new MenuItemInfo(Resource.ResourceManager["menuItem_Users"], "Users.aspx"));	
//			}
//			menuItems.Add(new MenuItemInfo(Resource.ResourceManager["menuItem_Emails"], "Emails.aspx"));
//			menu.AddMenuGroup(Resource.ResourceManager["menuHeader_Links"], 20, menuItems);
//			
//			// 'New' group
//			menuItems = new ArrayList();
//			menuItems.Add(new MenuItemInfo(Resource.ResourceManager["menuItem_NewProject"], "EditProject.aspx"));
//			if (LoggedUser.IsLeader || LoggedUser.IsAssistant)
//			{
//				menuItems.Add(new MenuItemInfo(Resource.ResourceManager["menuItem_NewClient"], "EditClient.aspx"));
//				if (LoggedUser.IsLeader)
//				menuItems.Add(new MenuItemInfo(Resource.ResourceManager["menuItem_NewUser"], "EditUser.aspx"));
//			}
//			menuItems.Add(new MenuItemInfo(Resource.ResourceManager["newcontracter_PageTitle"], "EditSubContracter.aspx"));
//
//			menu.AddMenuGroup(Resource.ResourceManager["menuHeader_New"], 30, menuItems);
//
//			menuItems = new ArrayList();
//			menuItems.Add(new MenuItemInfo(Resource.ResourceManager["menuItem_WorkTimeReport"], "reports/worktimes.aspx"));
//			menuItems.Add(new MenuItemInfo(Resource.ResourceManager["menuItem_MinutesReport"], "Reports/Minutes.aspx"));
//			if (LoggedUser.IsLeader || LoggedUser.IsAssistant)
//				menuItems.Add(new MenuItemInfo(Resource.ResourceManager["menuItem_MailReport"], "MailForm.aspx"));
//			if (LoggedUser.IsLeader)
//			{
//				menuItems.Add(new MenuItemInfo(Resource.ResourceManager["menuItem_NotEnteredWorkTime"], "Reports/EmptyHours.aspx"));
//				menuItems.Add(new MenuItemInfo(Resource.ResourceManager["menuItem_Calendar"], "reports/Calendar.aspx"));
//			}
//
//			menu.AddMenuGroup(Resource.ResourceManager["menuHeader_Reports"], 30, menuItems);
//
//			menuItems = new ArrayList();
//			menuItems.Add(new MenuItemInfo(Resource.ResourceManager["menuItem_Help"], SessionManager.GetHelpLink(),true,Pages.Emails));
//			menu.AddMenuGroup("", 10, menuItems);
//
//			menuHolder.Controls.Add(menu);
//		}

		#endregion
	}
}
