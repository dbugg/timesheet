using System;
using System.Data;
using System.Data.SqlClient; 
using System.Collections;
using Microsoft.ApplicationBlocks.Data;
using System.Collections.Generic;

namespace Asa.Timesheet.Data
{
	/// <summary>
	/// Summary description for ProjectLogDAL.
	/// </summary>
	public class ProjectLogDAL
	{
		/// <summary>
		/// Loads a ProjectLogData object from the database using the given ProjectLogID
		/// </summary>
		/// <param name="ProjectLogID">The primary key value</param>
		/// <returns>A ProjectLogData object</returns>
		public static ProjectLogData Load(int ProjectLogID)
		{
			SqlParameter[] parameterValues = new SqlParameter[] { new SqlParameter("@ProjectLogID", ProjectLogID) };
			//notes:not active for now
			using (SqlDataReader reader = SqlHelper.ExecuteReader(CommonDAL.ConnectionString, CommandType.StoredProcedure, "ProjectLogsSelProc", parameterValues))
			{
				if (reader.Read())
				{
					ProjectLogData result = new ProjectLogData();
					LoadFromReader(reader,result);
					return result;
				}
				else
					return null;
			}
		}

		/// <summary>
		/// Loads a ProjectLogData object from the database using the given where clause
		/// </summary>
		/// <param name="whereClause">The filter expression for the query</param>
		/// <returns>A ProjectLogData object</returns>
		public static ProjectLogData Load(string whereClause)
		{
			SqlParameter[] parameterValues = new SqlParameter[] { new SqlParameter("@WhereClause", whereClause) };
			using (SqlDataReader reader = SqlHelper.ExecuteReader(CommonDAL.ConnectionString, CommandType.StoredProcedure, "ProjectLogsListProc", parameterValues))
			{
				if (reader.Read())
				{
					ProjectLogData result = new ProjectLogData();
					LoadFromReader(reader,result);
					return result;
				}
				else
					return null;
			}
		}
		public static int Save( ProjectLogData source)
		{
			//notes:update not active for now
			if (source.ProjectLogID == -1)
				return Insert(source);
			else return -1;
//			else
//				return Update(source);	
		}

		public static void Delete( ProjectLogData source)
		{
			//notes:not active for now
			Delete(source.ProjectLogID);
		}

        /// <summary>
        /// Loads a collection of ProjectLogData objects from the database.
        /// </summary>
        /// <returns>A collection containing all of the ProjectLogData objects in the database.</returns>
        public static List<int> LoadDonwloaded(int CategoryID)
        {
            SqlParameter[] parms = new SqlParameter[1];
            parms[0] = new SqlParameter("@CategoryID", CategoryID);
            List<int> _list = new List<int>();
            using (SqlDataReader reader = SqlHelper.ExecuteReader(CommonDAL.ConnectionString, CommandType.StoredProcedure, "ProjectLogsGetDownloaded", parms))
            {
                while (reader.Read())
                {
                    if (!reader.IsDBNull(0)) _list.Add(reader.GetInt32(0));
                }
            }
            return _list;
            
        }

		/// <summary>
		/// Loads a collection of ProjectLogData objects from the database.
		/// </summary>
		/// <returns>A collection containing all of the ProjectLogData objects in the database.</returns>
		public static ProjectLogsVector LoadCollection(int ProjectID,int CategoryID,int FileID,int SortOrder,int nStatus, int nType, string sUser, int nLogType)
		{
			SqlParameter[] parms = new SqlParameter[8];
			
			if(ProjectID != -1)
			{
				parms[0] = new SqlParameter("@ProjectID", ProjectID);

			}
			if(CategoryID!=-1)
			{
				parms[1] = new SqlParameter("@CategoryID", CategoryID);
			}
			if(FileID!=-1)
			{
				parms[2] = new SqlParameter("@FileID", FileID);
			}
			parms[3] = new SqlParameter("@SortOrder", SortOrder);
			
			if(nStatus>0)
			{
				parms[4] = new SqlParameter("@Status", nStatus);
			}
			if(nLogType!=-1)
			{
				parms[5] = new SqlParameter("@logtype", nLogType==1?true:false);
			}
			if(sUser!=null)
			{
				parms[6] = new SqlParameter("@user", sUser);
			}
			return LoadCollection("ProjectLogsListProc", parms);
		}

		public static ProjectLogsVector LoadCollection()
		{
			SqlParameter[] parms = new SqlParameter[] {};
			return LoadCollection("ProjectLogsListProc", parms);
		}
	  
		/// <summary>
		/// Saves a collection of ProjectLogData objects to the database.
		/// </summary>
		/// <returns>A collection containing all of the ProjectLogData objects in the database.</returns>
		public static  void  SaveCollection(ProjectLogsVector col,string spName, SqlParameter[] parms)
		{
			ProjectLogsVector colOld = LoadCollection(spName,parms);
			UpdateCollection(colOld,col);
		}
		public static  void  SaveCollection(ProjectLogsVector col)
		{
			ProjectLogsVector colOld = LoadCollection();
			UpdateCollection(colOld,col);
		}
		public static  void  SaveCollection(ProjectLogsVector col, string whereClause )
		{
			ProjectLogsVector colOld = LoadCollection(whereClause);
			UpdateCollection(colOld,col);
		}
		/// <summary>
		/// Loads a collection of ProjectLogData objects from the database.
		/// </summary>
		/// <returns>A collection containing all of the ProjectLogData objects in the database.</returns>
		public static ProjectLogsVector LoadCollection(string whereClause)
		{
			SqlParameter[] parms = new SqlParameter[] { new SqlParameter("@WhereClause", whereClause) };
			return LoadCollection("ProjectLogsListProc", parms);
		}

		/// <summary>
		/// Loads a collection of ProjectLogData objects from the database.
		/// </summary>
		/// <returns>A collection containing all of the ProjectLogData objects in the database.</returns>
		public static ProjectLogsVector LoadCollection(string spName, SqlParameter[] parms)
		{
			ProjectLogsVector result = new ProjectLogsVector();
			using (SqlDataReader reader = SqlHelper.ExecuteReader(CommonDAL.ConnectionString, CommandType.StoredProcedure, spName, parms))
			{
				while (reader.Read())
				{
					ProjectLogData tmp = new ProjectLogData();
					LoadFromReader(reader, tmp);
					result.Add(tmp);
				}
			}
			return result;
		}
		/// <summary>
		/// Deletes a ProjectLogData object from the database.
		/// </summary>
		/// <param name="ProjectLogID">The primary key value</param>
		public static void Delete(int ProjectLogID)
		{
			SqlParameter[] parameterValues = new SqlParameter[] { new SqlParameter("@ProjectLogID", ProjectLogID) };
			SqlHelper.ExecuteNonQuery(CommonDAL.ConnectionString, CommandType.StoredProcedure, "ProjectLogsDelProc", parameterValues);
		}

		public static void LoadFromReader(SqlDataReader reader, ProjectLogData target)
		{
			if (reader != null && !reader.IsClosed)
			{
				//target.Key = reader.GetInt32(0);
				if (!reader.IsDBNull(0)) target.ProjectLogID = reader.GetInt32(0);
				if (!reader.IsDBNull(1)) target.FileID = reader.GetInt32(1);
				if (!reader.IsDBNull(2)) target.ActionDate = reader.GetDateTime(2);
				if (!reader.IsDBNull(3)) target.CreateOrDownload = reader.GetBoolean(3);
				if (!reader.IsDBNull(4)) target.ActionAccount = reader.GetString(4);
				if (!reader.IsDBNull(5)) target.FileSubject = reader.GetString(5);
				if (!reader.IsDBNull(6)) target.FileMinutes = reader.GetString(6);
				if (!reader.IsDBNull(7)) target.CategoryName = reader.GetString(7);
				if (!reader.IsDBNull(8)) target.ProjectName = reader.GetString(8);
				if (!reader.IsDBNull(9)) target.Deleted = reader.GetBoolean(9);

				if(reader.FieldCount>10)
					if (!reader.IsDBNull(10)) target.CreatedDate = reader.GetDateTime(10);
		
			}
		}
		
	  
		public static int Insert( ProjectLogData source)
		{
			SqlParameter[] parameterValues = GetInsertParameterValues(source);
			SqlHelper.ExecuteNonQuery(CommonDAL.ConnectionString, CommandType.StoredProcedure, "ProjectLogsInsProc", parameterValues);
			source.ProjectLogID = (int) parameterValues[0].Value;
			return source.ProjectLogID;
		}

//		public static int Update( ProjectLogData source)
//		{
////			SqlParameter[] parameterValues = GetUpdateParameterValues(source);
////			SqlHelper.ExecuteNonQuery(CommonDAL.ConnectionString, CommandType.StoredProcedure,
////				"ProjectLogsUpdProc", parameterValues);
////			return source.ProjectLogID;
//		}
		private static void UpdateCollection(ProjectLogsVector colOld,ProjectLogsVector col)
		{
			// Delete all old items
			foreach( ProjectLogData itemOld in colOld)
			{
				if(!ExistsKey(itemOld.ProjectLogID, col))
					Delete(itemOld.ProjectLogID);
			}
			foreach( ProjectLogData item in col)
			{
				if(item.ProjectLogID <= -1)
					Insert(item);
//				else
//					Update(item);
			}
		}
		private static bool ExistsKey(int Key, ProjectLogsVector col)
		{
			foreach( ProjectLogData item in col)
				if(item.ProjectLogID==Key)
					return true;
			return false;
		}

		private static SqlParameter GetSqlParameter(string name, ParameterDirection direction, object value)
		{
			SqlParameter p = new SqlParameter(name, value);
			p.Direction = direction;
			return p;
		}

		private static SqlParameter[] GetInsertParameterValues( ProjectLogData source)
		{
			SqlParameter[] prams = new SqlParameter[6];
			prams[0] = GetSqlParameter("@ProjectLogID", ParameterDirection.Output, source.ProjectLogID);
			prams[1] = GetSqlParameter("@FileID", ParameterDirection.Input, source.FileID);
			prams[2] = GetSqlParameter("@ActionDate", ParameterDirection.Input, source.ActionDate);
			prams[3] = GetSqlParameter("@CreateOrDownload", ParameterDirection.Input, source.CreateOrDownload);
			prams[4] = GetSqlParameter("@ActionAccount", ParameterDirection.Input, source.ActionAccount);
			prams[5] = GetSqlParameter("@Deleted", ParameterDirection.Input, source.Deleted);
			return prams;
		
		}

//		private static SqlParameter[] GetUpdateParameterValues( ProjectLogData source)
//		{
////			return new SqlParameter[] {
////										  new SqlParameter("@ProjectLogID", source.ProjectLogID),
////										  new SqlParameter("@LogID", source.LogID),
////										  new SqlParameter("@IsDeleted", source.IsDeleted)
////									  };
//		}
	}
}
