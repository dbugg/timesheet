<%@ Register TagPrefix="uc1" TagName="PageHeader" Src="UserControls/PageHeader.ascx" %>
<%@ Register TagPrefix="uc1" TagName="EditForm" Src="UserControls/EditForm.ascx" %>
<%@ Page language="c#" Codebehind="EditProtokols.aspx.cs" AutoEventWireup="false" Inherits="Asa.Timesheet.WebPages.EditProtokols" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
	<HEAD>
		<title>Protokoly</title>
		<meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
		<meta content="C#" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<LINK href="styles/timesheet.css" type="text/css" rel="stylesheet">
		<script language="javascript" src="PopupCalendar.js"></script>
	</HEAD>
	<body bottomMargin="0" leftMargin="0" topMargin="0" rightMargin="0">
		<form id="Form1" method="post" runat="server">
			<table height="100%" cellSpacing="0" cellPadding="0" width="100%" border="0">
				<tr height="1">
					<td><uc1:pageheader id="header" runat="server"></uc1:pageheader><uc1:editform id="editCtrl" runat="server"></uc1:editform></td>
				</tr>
				<TR height="1">
					<td noWrap background="images/line.gif" height="1"></td>
				</TR>
				<tr>
					<td>
						<table height="100%" cellSpacing="0" cellPadding="0" width="100%" border="0">
							<tr>
								<td style="BACKGROUND-IMAGE: url(images/left110.gif)" vAlign="top" noWrap width="110"><asp:placeholder id="menuHolder" runat="server"></asp:placeholder></td>
								<td noWrap width="1" bgColor="lightgrey"></td>
								<td style="PADDING-LEFT: 5px; PADDING-TOP: 3px" vAlign="top" noWrap>
									<TABLE id="Table3" cellSpacing="0" cellPadding="0" width="880" border="0">
										<TR>
											<TD style="WIDTH: 4px"></TD>
											<TD>
												<TABLE id="tblForm" cellSpacing="0" cellPadding="3" width="100%" border="0">
													<TBODY>
														<TR>
															<TD style="WIDTH: 168px; HEIGHT: 36px"><asp:label id="Label8" runat="server" Width="100%" CssClass="enterDataLabel">Сграда:</asp:label></TD>
															<TD style="HEIGHT: 34px"><asp:dropdownlist id="ddlBuildingTypes" runat="server" Width="250px" CssClass="enterDataBox" AutoPostBack="True"></asp:dropdownlist><asp:dropdownlist id="ddlProjectsStatus" runat="server" Width="250px" CssClass="EnterDataBox" AutoPostBack="True">
																	<asp:ListItem Value="0">всички проекти</asp:ListItem>
																	<asp:ListItem Value="1">активни проекти</asp:ListItem>
																	<asp:ListItem Value="2">пасивни проекти</asp:ListItem>
																	<asp:ListItem Value="3">приключени проекти</asp:ListItem>
																</asp:dropdownlist></TD>
														</TR>
														<TR>
															<TD style="WIDTH: 168px; HEIGHT: 29px"><asp:label id="lblName" runat="server" Width="100%" CssClass="enterDataLabel" EnableViewState="False"
																	Font-Bold="True">Проект:</asp:label></TD>
															<TD style="HEIGHT: 30px"><asp:dropdownlist id="ddlProject" runat="server" Width="250px" CssClass="EnterDataBox" AutoPostBack="True"></asp:dropdownlist><IMG height="16" alt="" src="images/required1.gif" width="16">
															</TD>
														</TR>
														<TR>
															<TD style="WIDTH: 168px"><asp:label id="lbDate" runat="server" Width="100%" CssClass="enterDataLabel" Font-Bold="True">Дата:</asp:label></TD>
															<TD><asp:textbox id="txtStartDate" runat="server" Width="88px" CssClass="enterDataBox" MaxLength="10"></asp:textbox>&nbsp;<IMG id="lkCalendar1" alt="Calendar" src="images/calendar.ico" width="18" align="absBottom"
																	runat="server">&nbsp;<IMG height="16" alt="" src="images/required1.gif" width="16"></TD>
														</TR>
														<TR>
															<TD style="WIDTH: 168px"><asp:label id="Label9" runat="server" Width="100%" CssClass="enterDataLabel">Бележки:</asp:label></TD>
															<TD><asp:textbox id="txtNotes" runat="server" Width="368px" CssClass="enterDataBox" Height="50px"
																	MaxLength="10" TextMode="MultiLine"></asp:textbox></TD>
														</TR>
														<TR>
															<TD style="WIDTH: 168px"><asp:label id="lbScanDocNew" runat="server" Width="100%" CssClass="enterDataLabel">Нов сканиран документ:</asp:label></TD>
															<TD><INPUT id="FileCtrl" type="file" name="File1" runat="server"><asp:button id="btnScanDocAdd" runat="server" CssClass="ActionButton" Text="Добави"></asp:button></TD>
														</TR>
														<TR>
															<TD style="WIDTH: 168px"><asp:label id="Label6" runat="server" Width="100%" CssClass="enterDataLabel">Сканирани документи:</asp:label></TD>
															<TD><asp:datalist id="dlDocs" runat="server" RepeatDirection="Horizontal">
																	<ItemTemplate>
																		<asp:HyperLink id="btnScan" runat="server" ToolTip="Сканиран документ" NavigateUrl='<%# GetURL((int)DataBinder.Eval(Container, "DataItem.ProjectProtokolID"))%>' Target=_blank>
																			<img border="0" src="images/pdf.gif" /></asp:HyperLink>
																		<br />
																		<asp:ImageButton id="btnDel" runat="server" ToolTip="Изтрий" ImageUrl="images/delete.gif" Visible='<%# GetVis()%>' >
																		</asp:ImageButton>
																	</ItemTemplate>
																</asp:datalist></TD>
														</TR>
													</TBODY>
												</TABLE>
											</TD>
										</TR>
										<TR>
											<TD style="HEIGHT: 5px" colSpan="2"></TD>
										</TR>
										<TR>
											<TD colSpan="2"><IMG height="1" src="images/dot.gif" width="100%"></TD>
										</TR>
										<TR>
											<td colSpan="2" height="3"></td>
										</TR>
										<TR>
											<TD style="WIDTH: 4px"></TD>
											<TD>
												<TABLE id="Table5" cellSpacing="0" cellPadding="0" border="0">
													<TR>
														<TD style="WIDTH: 97px"><asp:button id="btnSave" runat="server" CssClass="ActionButton" EnableViewState="False" Text="Запиши"></asp:button><asp:button id="btnEdit" style="DISPLAY: none" runat="server" CssClass="ActionButton" EnableViewState="False"
																Text="Редактирай"></asp:button></TD>
														<TD style="WIDTH: 245px"><asp:button id="btnCancel" runat="server" CssClass="ActionButton" EnableViewState="False" Text="Откажи"></asp:button></TD>
														<TD><asp:button id="btnDelete" runat="server" CssClass="ActionButton" EnableViewState="False" Text="Изтрий"
																Visible="False"></asp:button></TD>
													</TR>
												</TABLE>
											</TD>
										</TR>
										<TR>
											<TD height="3"></TD>
										</TR>
										<TR>
											<TD colSpan="2"><IMG height="1" src="images/dot.gif" width="100%"></TD>
										</TR>
									</TABLE>
									<br>
									&nbsp;
									<asp:label id="lblInfo" runat="server" CssClass="InfoLabel" EnableViewState="False"></asp:label><asp:label id="lblError" runat="server" CssClass="ErrorLabel" EnableViewState="False" ForeColor="Red"></asp:label></td>
							</tr>
						</table>
					</td>
				</tr>
			</table>
		</form>
	</body>
</HTML>
