
using System;
using System.Data;
using System.Collections;
using System.Data.SqlClient;
using Microsoft.ApplicationBlocks.Data;
namespace Asa.Timesheet.Data
{
	/// <summary>
	/// Summary description for SubprojectsData.
	/// </summary>
	public class SubprojectsUDL
	{
		public static DataSet SelectContentsPrefix()
		{
			string spName = "ContentTypesPrefixProc";

			
			DataSet ds = SqlHelper.ExecuteDataset(DBManager.GetConnectionString(), CommandType.StoredProcedure, spName);

			return ds;
		}
		private static SqlCommand CreateContentsDelByProjectProc (int ProjectID)
		{
			SqlCommand cmd = new SqlCommand();
			cmd.CommandType = System.Data.CommandType.StoredProcedure;
			cmd.CommandText = "ContentsDelByProjectProc";
			cmd.Parameters.Add(new System.Data.SqlClient.SqlParameter("@ProjectID", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.Input, true, 10, 0, "ProjectID", DataRowVersion.Current, null));
			cmd.Parameters["@ProjectID"].Value = ProjectID;

			return cmd;
		}

		public static int ExecuteContentsDelByProjectProc(int ProjectID)
		{
			int RetObj;
			System.Data.SqlClient.SqlCommand cmd;
			cmd = CreateContentsDelByProjectProc(ProjectID);
			SqlConnection cnDAL = new SqlConnection();

			try
			{
				cnDAL =  CommonDAL.OpenConnection();
				cmd.Connection = cnDAL;
				RetObj = cmd.ExecuteNonQuery();
				return RetObj;
		
			}
			catch(SqlException ex)
			{

				throw new ApplicationException("ContentsDelByProjectProc",ex);
			}

			finally
			{
				CommonDAL.CloseConnection(cnDAL);
			}
		}

		public static SqlDataReader SelectSubprojects(int projectID)
		{
			string spName = "SubprojectsListProc";

			SqlParameter[] storedParams = SqlHelperParameterCache.GetSpParameterSet(DBManager.GetConnectionString(), spName);
			storedParams[0].Value = projectID;

			return SqlHelper.ExecuteReader(DBManager.GetConnectionString(), CommandType.StoredProcedure, spName, storedParams);
		}
		public static SqlDataReader SelectContentTypes(int Filter)
		{
			string spName = "ContentTypesListProc";

			SqlParameter[] storedParams = SqlHelperParameterCache.GetSpParameterSet(DBManager.GetConnectionString(), spName);
			if(Filter>0)
				storedParams[0].Value = Filter;

			return SqlHelper.ExecuteReader(DBManager.GetConnectionString(), CommandType.StoredProcedure, spName, storedParams);
		}
		public static SqlDataReader SelectContentTypes1(bool Filter0, bool Filter1, bool Filter2, bool Filter3, bool Filter4, bool Filter5, bool Filter6,bool Filter7)
		{
			string spName = "ContentTypesListPlusMinusProc";

			SqlParameter[] storedParams = SqlHelperParameterCache.GetSpParameterSet(DBManager.GetConnectionString(), spName);
				System.Text.StringBuilder sbWhere = new System.Text.StringBuilder();
				string where = "IsActive<>0 AND Filter<>8 ";
			sbWhere.Append(where);
			if(!Filter0)
			{
				where =" AND Filter<>9 ";
				sbWhere.Append(where);
			}
			if(!Filter1)
			{
				where = " AND Filter<>0 AND Filter<>1 ";
				sbWhere.Append(where);
			}
			if(!Filter2)
			{
				where =" AND Filter<>2 ";
				sbWhere.Append(where);
			}
			if(!Filter3)
			{
				where =" AND Filter<>3 ";
				sbWhere.Append(where);
			}
			if(!Filter4)
			{
				where = " AND Filter<>4 ";
				sbWhere.Append(where);
			}
			if(!Filter5)
			{
				where = " AND Filter<>5 ";
				sbWhere.Append(where);
			}
			if(!Filter6)
			{	
				where = " AND Filter<>6 ";
				sbWhere.Append(where);
			}
			if(!Filter7)
			{	
				where =" AND Filter<>7 ";
				sbWhere.Append(where);
			}
				storedParams[0].Value = sbWhere.ToString();
			return SqlHelper.ExecuteReader(DBManager.GetConnectionString(), CommandType.StoredProcedure, spName, storedParams);
		}
		public static SqlDataReader SelectContentTypesConstructors(int Filter)
		{
			string spName = "ContentTypesListForConstructorsProc";

			SqlParameter[] storedParams = SqlHelperParameterCache.GetSpParameterSet(DBManager.GetConnectionString(), spName);
			if(Filter>0)
				storedParams[0].Value = Filter;

			return SqlHelper.ExecuteReader(DBManager.GetConnectionString(), CommandType.StoredProcedure, spName, storedParams);
		}
		public static SqlDataReader SelectOneContentType(int ID)
		{
			string spName = "ContentTypesSelProc";

			SqlParameter[] storedParams = SqlHelperParameterCache.GetSpParameterSet(DBManager.GetConnectionString(), spName);
			storedParams[0].Value = ID;

			return SqlHelper.ExecuteReader(DBManager.GetConnectionString(), CommandType.StoredProcedure, spName, storedParams);
		}
		
		public static SqlDataReader SelectSubactivities(int typeID)
		{
			string spName = "SubactivitiesListProc";

			SqlParameter[] storedParams = SqlHelperParameterCache.GetSpParameterSet(DBManager.GetConnectionString(), spName);
			if(typeID>0)
				storedParams[0].Value = typeID;

			return SqlHelper.ExecuteReader(DBManager.GetConnectionString(), CommandType.StoredProcedure, spName, storedParams);
		}
		public static SqlDataReader SelectSubcontracters(int typeID, int sort, bool HasPaymentRight)
		{
			string spName = "SubcontractersListProc1";

			SqlParameter[] storedParams = SqlHelperParameterCache.GetSpParameterSet(DBManager.GetConnectionString(), spName);
			if(typeID>0)
				storedParams[0].Value = typeID;
			if(sort!=0)
				storedParams[1].Value = sort;
			storedParams[2].Value = HasPaymentRight;
			return SqlHelper.ExecuteReader(DBManager.GetConnectionString(), CommandType.StoredProcedure, spName, storedParams);
		}
		
		public static SqlDataReader SelectSubcontracters(int typeID, int sort)
		{
			string spName = "SubcontractersListProc";

			SqlParameter[] storedParams = SqlHelperParameterCache.GetSpParameterSet(DBManager.GetConnectionString(), spName);
			if(typeID>0)
				storedParams[0].Value = typeID;
			if(sort!=0)
				storedParams[1].Value = sort;
			return SqlHelper.ExecuteReader(DBManager.GetConnectionString(), CommandType.StoredProcedure, spName, storedParams);
		}
		
		public static SqlDataReader SelectSubcontracterTypes()
		{
			string spName = "SubcontracterTypesListProc";


			return SqlHelper.ExecuteReader(DBManager.GetConnectionString(), CommandType.StoredProcedure, spName, null);
		}
		public static SqlDataReader SelectSchemes()
		{
			string spName = "PaymentSchemesListProc";

			//SqlParameter[] storedParams = SqlHelperParameterCache.GetSpParameterSet(DBManager.GetConnectionString(), spName);

			return SqlHelper.ExecuteReader(DBManager.GetConnectionString(), CommandType.StoredProcedure, spName, null);
		}
		public static SqlDataReader SelectSubprojectTypes()
		{
			string spName = "SubprojectTypesListProc";

			SqlParameter[] storedParams = SqlHelperParameterCache.GetSpParameterSet(DBManager.GetConnectionString(), spName);

			return SqlHelper.ExecuteReader(DBManager.GetConnectionString(), CommandType.StoredProcedure, spName, storedParams);
		}
		private static SqlCommand CreateProjectSubactivitiesDelBySubcontracterProc (int SubcontracterID)
		{
			SqlCommand cmd = new SqlCommand();
			cmd.CommandType = System.Data.CommandType.StoredProcedure;
			cmd.CommandText = "ProjectSubactivitiesDelBySubcontracterProc";
			cmd.Parameters.Add(new System.Data.SqlClient.SqlParameter("@SubcontracterID", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.Input, true, 10, 0, "SubcontracterID", DataRowVersion.Current, null));
			cmd.Parameters["@SubcontracterID"].Value = SubcontracterID;

			return cmd;
		}

		public static int ExecutePaymentsDelBySubcontracterProc(int SubcontracterID)
		{
			int RetObj;
			System.Data.SqlClient.SqlCommand cmd;
			cmd = CreatePaymentsDelBySubcontracterProc(SubcontracterID);
			SqlConnection cnDAL = new SqlConnection();

			try
			{
				cnDAL =  CommonDAL.OpenConnection();
				cmd.Connection = cnDAL;
				RetObj = cmd.ExecuteNonQuery();
				return RetObj;
		
			}
			catch(SqlException ex)
			{

				throw new ApplicationException("PaymentsDelBySubcontracterProc",ex);
			}

			finally
			{
				CommonDAL.CloseConnection(cnDAL);
			}
		}

		private static SqlCommand CreatePaymentsDelBySubcontracterProc (int SubcontracterID)
		{
			SqlCommand cmd = new SqlCommand();
			cmd.CommandType = System.Data.CommandType.StoredProcedure;
			cmd.CommandText = "PaymentsDelBySubcontracterProc";
			cmd.Parameters.Add(new System.Data.SqlClient.SqlParameter("@SubcontracterID", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.Input, true, 10, 0, "SubcontracterID", DataRowVersion.Current, null));
			cmd.Parameters["@SubcontracterID"].Value = SubcontracterID;

			return cmd;
		}

		public static int ExecuteProjectSubactivitiesDelBySubcontracterProc (int SubcontracterID)
		{
			int RetObj;
			System.Data.SqlClient.SqlCommand cmd;
			cmd = CreateProjectSubactivitiesDelBySubcontracterProc (SubcontracterID);
			SqlConnection cnDAL = new SqlConnection();

			try
			{
				cnDAL =  CommonDAL.OpenConnection();
				cmd.Connection = cnDAL;
				RetObj = cmd.ExecuteNonQuery();
				return RetObj;
		
			}
			catch(SqlException ex)
			{

				throw new ApplicationException("ProjectSubactivitiesDelBySubcontracterProc ",ex);
			}

			finally
			{
				CommonDAL.CloseConnection(cnDAL);
			}
		}
		private static SqlCommand CreatePaymentsDelByAuthorsProc (int AuthorsID)
		{
			SqlCommand cmd = new SqlCommand();
			cmd.CommandType = System.Data.CommandType.StoredProcedure;
			cmd.CommandText = "PaymentsDelByAuthorsProc";
			cmd.Parameters.Add(new System.Data.SqlClient.SqlParameter("@AuthorsID", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.Input, true, 10, 0, "AuthorsID", DataRowVersion.Current, null));
			cmd.Parameters["@AuthorsID"].Value = AuthorsID;

			return cmd;
		}
		public static int ExecutePaymentsDelByAuthorsProc (int AuthorsID)
		{
			int RetObj;
			System.Data.SqlClient.SqlCommand cmd;
			cmd = CreatePaymentsDelByAuthorsProc (AuthorsID);
			SqlConnection cnDAL = new SqlConnection();

			try
			{
				cnDAL =  CommonDAL.OpenConnection();
				cmd.Connection = cnDAL;
				RetObj = cmd.ExecuteNonQuery();
				return RetObj;
		
			}
			catch(SqlException ex)
			{

				throw new ApplicationException("PaymentsDelByAuthorsProc ",ex);
			}

			finally
			{
				CommonDAL.CloseConnection(cnDAL);
			}
		}

		private static SqlCommand CreatePaymentsDelBySubprojectProc (int SubprojectID)
		{
			SqlCommand cmd = new SqlCommand();
			cmd.CommandType = System.Data.CommandType.StoredProcedure;
			cmd.CommandText = "PaymentsDelBySubprojectProc";
			cmd.Parameters.Add(new System.Data.SqlClient.SqlParameter("@SubprojectID", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.Input, true, 10, 0, "SubprojectID", DataRowVersion.Current, null));
			cmd.Parameters["@SubprojectID"].Value = SubprojectID;

			return cmd;
		}
		private static SqlCommand CreatePaymentsSwitchProc (int ProjectID, int SubprojectID)
		{
			SqlCommand cmd = new SqlCommand();
			cmd.CommandType = System.Data.CommandType.StoredProcedure;
			cmd.CommandText = "PaymentsSwitchProc";
			cmd.Parameters.Add(new System.Data.SqlClient.SqlParameter("@ProjectID", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.Input, true, 10, 0, "ProjectID", DataRowVersion.Current, null));
			
				cmd.Parameters["@ProjectID"].Value = ProjectID;
			cmd.Parameters.Add(new System.Data.SqlClient.SqlParameter("@SubprojectID", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.Input, true, 10, 0, "SubprojectID", DataRowVersion.Current, null));
			
				cmd.Parameters["@SubprojectID"].Value = SubprojectID;

			return cmd;
		}
		public static int ExecutePaymentsSwitchProc(int ProjectID, int SubprojectID)
		{
			int RetObj;
			System.Data.SqlClient.SqlCommand cmd;
			cmd = CreatePaymentsSwitchProc(ProjectID, SubprojectID);
			SqlConnection cnDAL = new SqlConnection();

			try
			{
				cnDAL =  CommonDAL.OpenConnection();
				cmd.Connection = cnDAL;
				RetObj = cmd.ExecuteNonQuery();
				return RetObj;
		
			}
			catch(SqlException ex)
			{

				throw new ApplicationException("PaymentsSwitchProc",ex);
			}

			finally
			{
				CommonDAL.CloseConnection(cnDAL);
			}
		}

		private static SqlCommand CreatePaymentsDelByProjectProc (int SubprojectID)
		{
			SqlCommand cmd = new SqlCommand();
			cmd.CommandType = System.Data.CommandType.StoredProcedure;
			cmd.CommandText = "PaymentsDelByProjectProc";
			cmd.Parameters.Add(new System.Data.SqlClient.SqlParameter("@SubprojectID", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.Input, true, 10, 0, "SubprojectID", DataRowVersion.Current, null));
			cmd.Parameters["@SubprojectID"].Value = SubprojectID;

			return cmd;
		}
		public static int ExecutePaymentsDelByProjectProc(int SubprojectID)
		{
			int RetObj;
			System.Data.SqlClient.SqlCommand cmd;
			cmd = CreatePaymentsDelByProjectProc(SubprojectID);
			SqlConnection cnDAL = new SqlConnection();

			try
			{
				cnDAL =  CommonDAL.OpenConnection();
				cmd.Connection = cnDAL;
				RetObj = cmd.ExecuteNonQuery();
				return RetObj;
		
			}
			catch(SqlException ex)
			{

				throw new ApplicationException("PaymentsDelBySubprojectProc",ex);
			}

			finally
			{
				CommonDAL.CloseConnection(cnDAL);
			}
		}
		public static int ExecutePaymentsDelBySubprojectProc(int SubprojectID)
		{
			int RetObj;
			System.Data.SqlClient.SqlCommand cmd;
			cmd = CreatePaymentsDelBySubprojectProc(SubprojectID);
			SqlConnection cnDAL = new SqlConnection();

			try
			{
				cnDAL =  CommonDAL.OpenConnection();
				cmd.Connection = cnDAL;
				RetObj = cmd.ExecuteNonQuery();
				return RetObj;
		
			}
			catch(SqlException ex)
			{

				throw new ApplicationException("PaymentsDelBySubprojectProc",ex);
			}

			finally
			{
				CommonDAL.CloseConnection(cnDAL);
			}
		}
	}
}
