using System;
using System.Data;
using GrapeCity.ActiveReports;
using GrapeCity.ActiveReports.Controls;
using GrapeCity.ActiveReports.SectionReportModel;
using GrapeCity.ActiveReports.Document.Section;
using GrapeCity.ActiveReports.Document;

namespace Asa.Timesheet.WebPages.Reports
{
    public class rptTefter : GrapeCity.ActiveReports.SectionReport
	{
        public rptTefter(string sObekt, string sDate, bool client, bool sub, string Names)
        {
            InitializeComponent();
            this.PageSettings.Margins.Top = SectionReport.CmToInch(.2f);
            this.PageSettings.Margins.Bottom = SectionReport.CmToInch(0);
            this.PageSettings.Gutter = 0F;
            this.PageSettings.Margins.Left = 0.15f;
            this.PageSettings.Margins.Right = 0;
            txtObekt.Text = sObekt;
            txtDate.Text = sDate;
            if (!client)
                checkClient.Visible = false;
            if (!sub)
                checkSub.Visible = false;
            DataTable dt = new DataTable();
            dt.Columns.Add("Names");
            string[] arr = Names.Split(';');
            for (int i = 0; i < arr.Length; i++)
            {

                DataRow dr = dt.NewRow();
                dr[0] = arr[i];
                dt.Rows.Add(dr);
            }
            DataView dv = new DataView(dt);
            this.SubReport1.Report = new rtfTefterSubrep(dv);
        }

		#region ActiveReports Designer generated code










        public void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(rptTefter));
            this.Detail = new GrapeCity.ActiveReports.SectionReportModel.Detail();
            this.PageHeader = new GrapeCity.ActiveReports.SectionReportModel.PageHeader();
            this.PageFooter = new GrapeCity.ActiveReports.SectionReportModel.PageFooter();
            this.Picture1 = new GrapeCity.ActiveReports.SectionReportModel.Picture();
            this.checkMeet = new GrapeCity.ActiveReports.SectionReportModel.Picture();
            this.txtObekt = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtDate = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.checkClient = new GrapeCity.ActiveReports.SectionReportModel.Picture();
            this.checkSub = new GrapeCity.ActiveReports.SectionReportModel.Picture();
            this.SubReport1 = new GrapeCity.ActiveReports.SectionReportModel.SubReport();
            ((System.ComponentModel.ISupportInitialize)(this.Picture1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkMeet)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtObekt)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkClient)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkSub)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            // 
            // Detail
            // 
            this.Detail.ColumnSpacing = 0F;
            this.Detail.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
						this.Picture1,
						this.checkMeet,
						this.txtObekt,
						this.txtDate,
						this.checkClient,
						this.checkSub,
						this.SubReport1});
            this.Detail.Height = 10.8125F;
            this.Detail.Name = "Detail";
            // 
            // PageHeader
            // 
            this.PageHeader.Height = 0F;
            this.PageHeader.Name = "PageHeader";
            // 
            // PageFooter
            // 
            this.PageFooter.Height = 0F;
            this.PageFooter.Name = "PageFooter";
            // 
            // Picture1
            // 
            this.Picture1.Height = 10.9375F;
            this.Picture1.ImageData = ((System.IO.Stream)(resources.GetObject("Picture1.ImageData")));
            this.Picture1.Left = 0F;
            this.Picture1.LineColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.Picture1.Name = "Picture1";
            this.Picture1.SizeMode = GrapeCity.ActiveReports.SectionReportModel.SizeModes.Zoom;
            this.Picture1.Top = -0.125F;
            this.Picture1.Width = 8.1875F;
            // 
            // checkMeet
            // 
            this.checkMeet.Height = 0.25F;
            this.checkMeet.ImageData = ((System.IO.Stream)(resources.GetObject("checkMeet.ImageData")));
            this.checkMeet.Left = 4F;
            this.checkMeet.LineColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.checkMeet.Name = "checkMeet";
            this.checkMeet.Top = 0.1875F;
            this.checkMeet.Width = 0.25F;
            // 
            // txtObekt
            // 
            this.txtObekt.Height = 0.2F;
            this.txtObekt.Left = 2.5625F;
            this.txtObekt.Name = "txtObekt";
            this.txtObekt.Style = "font-size: 8pt";
            this.txtObekt.Top = 0.6875F;
            this.txtObekt.Width = 3.374499F;
            // 
            // txtDate
            // 
            this.txtDate.Height = 0.2F;
            this.txtDate.Left = 6.3125F;
            this.txtDate.Name = "txtDate";
            this.txtDate.Style = "font-size: 7pt; text-align: center; ddo-char-set: 1";
            this.txtDate.Top = 0.6875F;
            this.txtDate.Width = 1.125F;
            // 
            // checkClient
            // 
            this.checkClient.Height = 0.25F;
            this.checkClient.ImageData = ((System.IO.Stream)(resources.GetObject("checkClient.ImageData")));
            this.checkClient.Left = 5.375F;
            this.checkClient.LineColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.checkClient.Name = "checkClient";
            this.checkClient.Top = 0.1875F;
            this.checkClient.Width = 0.25F;
            // 
            // checkSub
            // 
            this.checkSub.Height = 0.25F;
            this.checkSub.ImageData = ((System.IO.Stream)(resources.GetObject("checkSub.ImageData")));
            this.checkSub.Left = 7.1875F;
            this.checkSub.LineColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.checkSub.Name = "checkSub";
            this.checkSub.Top = 0.1875F;
            this.checkSub.Width = 0.25F;
            // 
            // SubReport1
            // 
            this.SubReport1.CloseBorder = false;
            this.SubReport1.Height = 0.29F;
            this.SubReport1.Left = 2.17F;
            this.SubReport1.Name = "SubReport1";
            this.SubReport1.Report = null;
            this.SubReport1.Top = 1.24F;
            this.SubReport1.Width = 5.2F;
            // 
            // ActiveReport1
            // 
            this.MasterReport = false;
            this.PageSettings.PaperHeight = 11F;
            this.PageSettings.PaperWidth = 8.5F;
            this.PrintWidth = 8.364583F;
            this.Sections.Add(this.PageHeader);
            this.Sections.Add(this.Detail);
            this.Sections.Add(this.PageFooter);
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule(resources.GetString("$this.StyleSheet"), "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: inherit; font-style: inherit; font-variant: inherit; font-weight: bo" +
                        "ld; font-size: 16pt; font-size-adjust: inherit; font-stretch: inherit", "Heading1", "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: Times New Roman; font-style: italic; font-variant: inherit; font-wei" +
                        "ght: bold; font-size: 14pt; font-size-adjust: inherit; font-stretch: inherit", "Heading2", "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: inherit; font-style: inherit; font-variant: inherit; font-weight: bo" +
                        "ld; font-size: 13pt; font-size-adjust: inherit; font-stretch: inherit", "Heading3", "Normal"));
            ((System.ComponentModel.ISupportInitialize)(this.Picture1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkMeet)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtObekt)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkClient)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkSub)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();
        }

		#endregion

        private PageHeader PageHeader;
        private Detail Detail;
        private Picture Picture1;
        private Picture checkMeet;
        private TextBox txtObekt;
        private TextBox txtDate;
        private Picture checkClient;
        private Picture checkSub;
        private SubReport SubReport1;
        private PageFooter PageFooter;
	}
}
