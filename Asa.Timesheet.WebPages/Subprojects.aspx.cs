using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Data.SqlClient;
using Asa.Timesheet.Data;
using Asa.Timesheet.Data.Entities;
using log4net;
using Asa.Timesheet.WebPages.UserControls;

namespace Asa.Timesheet.WebPages
{
	/// <summary>
	/// Summary description for Subprojects.
	/// </summary>
	public class Subprojects  : TimesheetPageBase 
	{
		protected System.Web.UI.WebControls.PlaceHolder menuHolder;
		protected System.Web.UI.WebControls.Label lblError;
		protected System.Web.UI.WebControls.Label lblInfo;
		protected System.Web.UI.WebControls.DataGrid grdProjects;
		protected System.Web.UI.WebControls.Panel grid;
		protected System.Web.UI.WebControls.Button btnNewProject;
		protected Asa.Timesheet.WebPages.UserControls.PageHeader header;
		protected System.Web.UI.WebControls.Button btnProject;
		protected System.Web.UI.WebControls.Button btnSubc;
		private static readonly ILog log = LogManager.GetLogger(typeof(Subprojects));

		private enum GridColumns
		{
			Number=0,
			ProjectName,
			ProjectCode,
			Subproject,
			StartDate,
			EndDate,
			
			EditItem,
			DeleteItem
		}


		private void Page_Load(object sender, System.EventArgs e)
		{
			if (!LoggedUser.HasRole) ErrorRedirect(Resource.ResourceManager["noRightsAlert"]);
		
			UIHelpers.CreateMenu(menuHolder,LoggedUser);
			string projectName, projectCode, administrativeName,add;
			decimal area=0;int clientID=0;
			int PID=UIHelpers.GetPIDParam();
			bool phases;
			if (!ProjectsData.SelectProject(PID, out projectName, out  projectCode, out administrativeName, out area, out add, out clientID,out phases))
			{
				log.Error("Project not found " + PID);
				
			}
			if (!this.IsPostBack)
			{
				header.PageTitle = Resource.ResourceManager["subprojects_PageTitle"]+" "+ projectName;//projectCode+ " "+administrativeName;
				header.UserName = LoggedUser.UserName;
				btnNewProject.Text = Resource.ResourceManager["subprojects_btnNewProject"];

				if (!LoggedUser.HasPaymentRights)
					((TemplateColumn)grdProjects.Columns[(int)GridColumns.DeleteItem]).Visible = false;
				SortOrder = 1;
				BindGrid();
				if(phases && LoggedUser.HasPaymentRights)
				{

					CheckForError();
				}
			}
		}

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
			this.grdProjects.ItemCommand += new System.Web.UI.WebControls.DataGridCommandEventHandler(this.grdProjects_ItemCommand);
			this.btnNewProject.Click += new System.EventHandler(this.btnNewProject_Click);
			this.btnProject.Click += new System.EventHandler(this.btnProject_Click);
			this.btnSubc.Click += new System.EventHandler(this.btnSubc_Click);
			this.Load += new System.EventHandler(this.Page_Load);

		}
		#endregion

		#region Database

		private void BindGrid()
		{
			SqlDataReader reader = null;

			try
			{
				
				grdProjects.DataSource= SubprojectDAL.LoadCollection("SubprojectsListProc",SQLParms.CreateSubprojectsListProc(UIHelpers.GetPIDParam()));
				grdProjects.DataKeyField="SubprojectID";
				grdProjects.DataBind();
			}
			catch (Exception ex)
			{
				log.Info(ex);
				lblError.Text = Resource.ResourceManager["subprojects_ErrorLoadProjects"];
				return;
			}
			finally
			{
				if (reader!=null) reader.Close();
			}

			if (grdProjects.Items.Count == 0) 
			{
				grid.Visible = false;
				lblInfo.Text = Resource.ResourceManager["subprojects_NoActiveProjects"];
			}
			else grid.Visible = true;

			try
			{
				SetConfirmDelete(grdProjects, "btnDelete", 1);
			}
			catch (Exception ex)
			{
				log.Error(ex);
			}
		}

		private bool DeleteProject(int projectID)
		{
			try
			{
				ProjectsData.InactiveProject(projectID);
			}
			catch (Exception ex)
			{
				log.Info(ex);
				return false;
			}

			return true;
		}

		#endregion

		#region Menu
//		private void CreateMenu()
//		{
//			UserControls.MenuTable menu = new UserControls.MenuTable();
//			menu.ID = "MenuTable";
//
//			ArrayList menuItems = new ArrayList();
//			menuItems.Add(new MenuItemInfo(Resource.ResourceManager["menuItem_Hours"], "Hours.aspx", true));
//			menuItems.Add(new MenuItemInfo(Resource.ResourceManager["menuItem_Projects"], "Projects.aspx"));
//			if ((LoggedUser.IsLeader) || (LoggedUser.IsAssistant)) 
//			{
//				menuItems.Add(new MenuItemInfo(Resource.ResourceManager["menuItem_Clients"], "Clients.aspx"));
//								menuItems.Add(new MenuItemInfo(Resource.ResourceManager["menuItem_Subcontracters"], "Subcontracters.aspx"));
//				if (LoggedUser.IsLeader)
//				menuItems.Add(new MenuItemInfo(Resource.ResourceManager["menuItem_Users"], "Users.aspx"));			
//			}
//			menuItems.Add(new MenuItemInfo(Resource.ResourceManager["menuItem_Emails"], "Emails.aspx"));
//			menu.AddMenuGroup(Resource.ResourceManager["menuHeader_Links"], 20, menuItems);
//			
//			//2 group
//			menuItems = new ArrayList();
//			menuItems.Add(new MenuItemInfo(Resource.ResourceManager["menuItem_NewProject"], "EditProject.aspx"));
//			if ((LoggedUser.IsLeader) || (LoggedUser.IsAssistant)) 
//			{
//				menuItems.Add(new MenuItemInfo(Resource.ResourceManager["menuItem_NewClient"], "EditClient.aspx"));
//				if (LoggedUser.IsLeader)
//				menuItems.Add(new MenuItemInfo(Resource.ResourceManager["menuItem_NewUser"], "EditUser.aspx"));		
//			}
//			menuItems.Add(new MenuItemInfo(Resource.ResourceManager["menuItem_NewEmail"], "EditEmail.aspx"));
//			menuItems.Add(new MenuItemInfo(Resource.ResourceManager["newcontracter_PageTitle"], "EditSubContracter.aspx"));
//
//			menu.AddMenuGroup(Resource.ResourceManager["menuHeader_New"], 30, menuItems);
//
//			menuItems = new ArrayList();
//			menuItems.Add(new MenuItemInfo(Resource.ResourceManager["menuItem_WorkTimeReport"], "reports/worktimes.aspx"));
//			menuItems.Add(new MenuItemInfo(Resource.ResourceManager["menuItem_MinutesReport"], "Reports/Minutes.aspx"));
//			if (LoggedUser.IsLeader || LoggedUser.IsAssistant)
//				menuItems.Add(new MenuItemInfo(Resource.ResourceManager["menuItem_MailReport"], "MailForm.aspx"));
//			if (LoggedUser.IsLeader)
//			{
//				menuItems.Add(new MenuItemInfo(Resource.ResourceManager["menuItem_NotEnteredWorkTime"], "reports/EmptyHours.aspx"));
//				menuItems.Add(new MenuItemInfo(Resource.ResourceManager["menuItem_Calendar"], "reports/Calendar.aspx"));
//			}
//
//			menu.AddMenuGroup(Resource.ResourceManager["menuHeader_Reports"], 30, menuItems);
//			
//			menuItems = new ArrayList();
//			menuItems.Add(new MenuItemInfo(Resource.ResourceManager["menuItem_Help"], SessionManager.GetHelpLink(),true,Pages.Subprojects));
//			menu.AddMenuGroup("", 10, menuItems);
//
//			menuHolder.Controls.Add(menu);
//		}
		#endregion


	
		private void grdProjects_ItemCommand(object source, System.Web.UI.WebControls.DataGridCommandEventArgs e)
		{
			if(e.CommandName=="Edit")
			{
					string sProjectID = ((DataGrid)source).DataKeys[e.Item.ItemIndex].ToString();
				Response.Redirect("EditSubproject.aspx"+"?sid="+sProjectID+"&pid="+UIHelpers.GetPIDParam());
				return;
			}
			ImageButton ib = (ImageButton)e.CommandSource;
			string s = ib.ID;
			switch (ib.ID)
			{
				case "btnDelete": int projectID = (int)((DataGrid)source).DataKeys[e.Item.ItemIndex];
					SubprojectDAL.Delete(projectID);
					
					BindGrid();
					break;
				case "btnEdit": string sProjectID = ((DataGrid)source).DataKeys[e.Item.ItemIndex].ToString();
					Response.Redirect("EditSubproject.aspx"+"?sid="+sProjectID+"&pid="+UIHelpers.GetPIDParam()); break;
			}
		}

		private void btnNewProject_Click(object sender, System.EventArgs e)
		{
			Response.Redirect("EditSubproject.aspx?pid="+UIHelpers.GetPIDParam());
		}

		private void btnProject_Click(object sender, System.EventArgs e)
		{
			Response.Redirect("EditProject.aspx?pid="+UIHelpers.GetPIDParam());
		}

		private void btnSubc_Click(object sender, System.EventArgs e)
		{
			Response.Redirect("ProjectSubcontracters.aspx?pid="+UIHelpers.GetPIDParam());
		}
		private void CheckForError()
		{
			for(int i=0;i<grdProjects.Items.Count;i++)
			{
				int psID = (int) grdProjects.DataKeys[grdProjects.Items[i].ItemIndex];
			SubprojectData sd = SubprojectDAL.Load(psID);
			if(sd!=null)
			{
				
				PaymentsVector pv = PaymentDAL.LoadCollection("PaymentsListBySubprojectProc", SQLParms.CreatePaymentsListBySubprojectProc(psID));
				PaymentSchemes ps =(PaymentSchemes)sd.PaymentSchemeID;
				if(ps==PaymentSchemes.Fixed)
					sd.TotalAmount=sd.Rate;
				else if(ps==PaymentSchemes.Area)
					sd.TotalAmount=sd.Rate*(decimal)sd.Area;
				decimal dEUR=0;
				
				for(int j=0;j<pv.Count;j++)
				{
					
						
					//							am = SetRec();
					dEUR+=pv[j].Amount;
				}
					if(dEUR!=sd.TotalAmount )
					{
						lblError.Text=string.Concat(Resource.ResourceManager["editsubprojects_errorTotal"]," ",i+1);
					
						break;
					}
				}
			}
//			}

		}
//		private decimal SetRec()
//		{
//
//		}
	}
}
