using System;
using System.Collections;
using System.Text;

namespace Asa.Timesheet.Data
{
	/// <summary>
	/// Summary description for ExpencesMontlyData.
	/// </summary>
	public class ExpencesMontlyData
	{
		private int _expenceID;
		private DateTime _monthOfYear;
		private int _expences;
		private int _expencesASI;
		

		/// <summary>
		/// Initialize an new empty Login object.
		/// </summary>
		public ExpencesMontlyData()
		{
			
		}

		public ExpencesMontlyData(int expenceID,DateTime monthOfYear,int expences)
		{
			_expenceID = expenceID;
			_monthOfYear = monthOfYear;
			_expences = expences;
		}
		public override string ToString()
		{
			StringBuilder builder = new StringBuilder();
			builder.Append("_expenceID: " + _expenceID.ToString()).Append("\r\n");
			builder.Append("_monthOfYear: " + _monthOfYear.ToString()).Append("\r\n");
			builder.Append("_expences: " + _expences.ToString()).Append("\r\n");
			builder.Append("\r\n");
			return builder.ToString();
		}	
		public object GetByIndex(int nIndex)
		{
			switch(nIndex)
			{	
				case 0:
					return _expenceID;
				case 1:
					return _monthOfYear;
				case 2:
					return _expences;
			}
			return null;
		}
		#region Properties
		public int ExpencesASI
		{
			get { return _expencesASI; }
			set { _expencesASI = value; }
		}

		public int ExpenceID
		{
			get { return _expenceID; }
			set { _expenceID = value; }
		}
		public DateTime MonthOfYear
		{
			get { return _monthOfYear; }
			set { _monthOfYear = value; }
		}
		public int Expences
		{
			get { return _expences; }
			set { _expences = value; }
		}
		

		#endregion
	}
}
