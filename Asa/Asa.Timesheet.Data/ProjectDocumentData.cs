using System;
using System.Collections;
using System.Text;

namespace Asa.Timesheet.Data
{
	public class ProjectDocumentData
	{
		private int _projectDocumentID = -1;
		private int _projectID = -1;
		private int _documentType = -1;
		private string _documentDiscription = string.Empty;

		/// <summary>
		/// Initialize an new empty Login object.
		/// </summary>
		public ProjectDocumentData()
		{
		}
		
		/// <summary>
		/// Initialize a new Login object with the given parameters.
		/// </summary>
		public ProjectDocumentData(int projectDocumentID, int projectID, int documentType, string documentDiscription)
		{
			this._projectDocumentID = projectDocumentID;
			this._projectID = projectID;
			this._documentType = documentType;
			this._documentDiscription = documentDiscription;
		}	
		
	  
		public override string ToString()
		{
			StringBuilder builder = new StringBuilder();
			builder.Append("_projectDocumentID: " + _projectDocumentID.ToString()).Append("\r\n");
			builder.Append("_projectID: " + _projectID.ToString()).Append("\r\n");
			builder.Append("_documentType: " + _documentType.ToString()).Append("\r\n");
			builder.Append("\r\n");
			return builder.ToString();
		}	
		public object GetByIndex(int nIndex)
		{
			switch(nIndex)
			{
				case 0:
					return _projectDocumentID;
				case 1:
					return _projectID;
				case 2:
					return _documentType;
				case 3:
					return _documentDiscription;
			}
			return null;
		}
		#region Properties
	
		public int ProjectDocumentID
		{
			get { return _projectDocumentID; }
			set { _projectDocumentID = value; }
		}
	
		public int ProjectID
		{
			get { return _projectID; }
			set { _projectID = value; }
		}
	
		public int DocumentType
		{
			get { return _documentType; }
			set { _documentType = value; }
		}
		
		public string DocumentDiscription
		{
			get { return _documentDiscription; }
			set { _documentDiscription = value; }
		}
		#endregion
	}
}


