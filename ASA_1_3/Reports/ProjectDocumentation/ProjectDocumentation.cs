using System;
using Asa.Timesheet.Data;
using Asa.Timesheet.Data.Reports;
using GrapeCity.ActiveReports;
using GrapeCity.ActiveReports.Controls;
using GrapeCity.ActiveReports.SectionReportModel;
using GrapeCity.ActiveReports.Document.Section;
using GrapeCity.ActiveReports.Document;

namespace Asa.Timesheet.WebPages.Reports
{
    public class ProjectDocumentation : GrapeCity.ActiveReports.SectionReport
	{

		int _projectID;
		int _startPageNumber;
    bool _fullReport;

    public ProjectDocumentation(int projectID, bool fullReport, int startPageNumber)
    {
        InitializeComponent();
        _projectID = projectID;
        _fullReport = fullReport;
        _startPageNumber = startPageNumber;
    }

    private void ProjectDocumentation_ReportStart(object sender, System.EventArgs eArgs)
    {
        int currentPage = _startPageNumber;

        ProjectsReportDS dsReport =
            ProjectsReportData.CreateReportDataSource(_projectID);

        string projectName = dsReport.Project.Rows[0]["ProjectName"].ToString();

        SectionReport rep = new IzhodniDanniSubreport(projectName, currentPage);
        rep.DataSource = dsReport.InitialDataItems;

        rep.Run();

        this.Document.Pages.AddRange(rep.Document.Pages);
        currentPage += rep.Document.Pages.Count;


        rep = new InfrastrukturniDanniSubreport(projectName, _fullReport, currentPage);
        rep.DataSource = dsReport.InfrastructureDataItems;

        rep.Run();

        this.Document.Pages.AddRange(rep.Document.Pages);
        currentPage += rep.Document.Pages.Count;

        rep = new BuildPermissionSubreport(projectName, _fullReport, currentPage);
        rep.DataSource = dsReport.BuildPermissionData;

        rep.Run();

        this.Document.Pages.AddRange(rep.Document.Pages);
        if (_startPageNumber == 1)
        {
            Logo logo = new Logo();
            logo.Run();

            for (int i = 0; i < this.Document.Pages.Count; i++)
                this.Document.Pages[i].Overlay(logo.Document.Pages[0]);
        }

    }

    private void ProjectDocumentation_DataInitialize(object sender, System.EventArgs eArgs)
    {

    }

    private void ProjectDocumentation_ReportEnd(object sender, System.EventArgs eArgs)
    {
        if (this.Document.Pages.Count > 0) this.Document.Pages.RemoveAt(this.Document.Pages.Count - 1);
    }

		#region ActiveReports Designer generated code



    public void InitializeComponent()
    {
        System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ProjectDocumentation));
        this.Detail = new GrapeCity.ActiveReports.SectionReportModel.Detail();
        this.ReportHeader = new GrapeCity.ActiveReports.SectionReportModel.ReportHeader();
        this.ReportFooter = new GrapeCity.ActiveReports.SectionReportModel.ReportFooter();
        ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
        // 
        // Detail
        // 
        this.Detail.ColumnSpacing = 0F;
        this.Detail.Height = 0.08333334F;
        this.Detail.Name = "Detail";
        // 
        // ReportHeader
        // 
        this.ReportHeader.Height = 0.04166667F;
        this.ReportHeader.Name = "ReportHeader";
        // 
        // ReportFooter
        // 
        this.ReportFooter.Height = 0.04166667F;
        this.ReportFooter.Name = "ReportFooter";
        // 
        // ActiveReport1
        // 
        this.MasterReport = false;
        this.PageSettings.DefaultPaperSize = false;
        this.PageSettings.Margins.Bottom = 0.7875F;
        this.PageSettings.Margins.Left = 1.377778F;
        this.PageSettings.Margins.Right = 0.39375F;
        this.PageSettings.Margins.Top = 0.7875F;
        this.PageSettings.PaperHeight = 11.69306F;
        this.PageSettings.PaperKind = System.Drawing.Printing.PaperKind.A4;
        this.PageSettings.PaperWidth = 8.268056F;
        this.PrintWidth = 7.086611F;
        this.Sections.Add(this.ReportHeader);
        this.Sections.Add(this.Detail);
        this.Sections.Add(this.ReportFooter);
        this.StyleSheet.Add(new DDCssLib.StyleSheetRule(resources.GetString("$this.StyleSheet"), "Normal"));
        this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: inherit; font-style: inherit; font-variant: inherit; font-weight: bo" +
                    "ld; font-size: 16pt; font-size-adjust: inherit; font-stretch: inherit", "Heading1", "Normal"));
        this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: Times New Roman; font-style: italic; font-variant: inherit; font-wei" +
                    "ght: bold; font-size: 14pt; font-size-adjust: inherit; font-stretch: inherit", "Heading2", "Normal"));
        this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: inherit; font-style: inherit; font-variant: inherit; font-weight: bo" +
                    "ld; font-size: 13pt; font-size-adjust: inherit; font-stretch: inherit", "Heading3", "Normal"));
        ((System.ComponentModel.ISupportInitialize)(this)).EndInit();

        // Attach Report Events
        this.ReportStart += new System.EventHandler(this.ProjectDocumentation_ReportStart);
        this.DataInitialize += new System.EventHandler(this.ProjectDocumentation_DataInitialize);
        this.ReportEnd += new System.EventHandler(this.ProjectDocumentation_ReportEnd);

    }

		#endregion

        private ReportHeader ReportHeader;
        private Detail Detail;
        private ReportFooter ReportFooter;
	}
}
