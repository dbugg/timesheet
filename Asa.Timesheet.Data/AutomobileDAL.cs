using System;
using System.Data;
using System.Data.SqlClient; 
using System.Collections;
using Microsoft.ApplicationBlocks.Data;

namespace Asa.Timesheet.Data
{
	public class AutomobileDAL
	{
		/// <summary>
		/// Loads a AutomobileData object from the database using the given AutomobileID
		/// </summary>
		/// <param name="automobileID">The primary key value</param>
		/// <returns>A AutomobileData object</returns>
		public static AutomobileData Load(int automobileID)
		{
			SqlParameter[] parameterValues = new SqlParameter[] { new SqlParameter("@AutomobileID", automobileID) };
			using (SqlDataReader reader = SqlHelper.ExecuteReader(CommonDAL.ConnectionString, CommandType.StoredProcedure, "AutomobilesSelProc", parameterValues))
			{
				if (reader.Read())
				{
					AutomobileData result = new AutomobileData();
					LoadFromReader(reader,result);
					return result;
				}
				else
					return null;
			}
		}

		/// <summary>
		/// Loads a AutomobileData object from the database using the given where clause
		/// </summary>
		/// <param name="whereClause">The filter expression for the query</param>
		/// <returns>A AutomobileData object</returns>
		public static AutomobileData Load(string whereClause)
		{
			SqlParameter[] parameterValues = new SqlParameter[] { new SqlParameter("@WhereClause", whereClause) };
			using (SqlDataReader reader = SqlHelper.ExecuteReader(CommonDAL.ConnectionString, CommandType.StoredProcedure, "AutomobilesListProc", parameterValues))
			{
				if (reader.Read())
				{
					AutomobileData result = new AutomobileData();
					LoadFromReader(reader,result);
					return result;
				}
				else
					return null;
			}
		}
		public static int Save( AutomobileData source)
		{
			if (source.AutomobileID == -1)
				return Insert(source);
			else
				return Update(source);	
		}

		public static void Delete( AutomobileData source)
		{
			Delete(source.AutomobileID);
		}
		/// <summary>
		/// Loads a collection of AutomobileData objects from the database.
		/// </summary>
		/// <returns>A collection containing all of the AutomobileData objects in the database.</returns>
		public static AutomobilesVector LoadCollection()
		{
			SqlParameter[] parms = new SqlParameter[] {};
			return LoadCollection("AutomobilesListProc", parms);
		}
		public static AutomobilesVector LoadCollectionCheckAnnual()
		{
			SqlParameter[] parms = new SqlParameter[] {};
			return LoadCollection("AutomobilesListCheckSoonYearProc", parms);
		}
		public static AutomobilesVector LoadCollectionCheckCivil()
		{
			SqlParameter[] parms = new SqlParameter[] {};
			return LoadCollection("AutomobilesListCheckSoonCivilProc", parms);
		}
		public static AutomobilesVector LoadCollectionCheckKasko()
		{
			SqlParameter[] parms = new SqlParameter[] {};
			return LoadCollection("AutomobilesListCheckSoonKaskoProc", parms);
		}
		public static AutomobilesVector LoadCollectionCheckGreen()
		{
			SqlParameter[] parms = new SqlParameter[] {};
			return LoadCollection("AutomobilesListCheckSoonGreenProc", parms);
		}
		public static AutomobilesVector LoadCollectionCheckOil()
		{
			SqlParameter[] parms = new SqlParameter[] {};
			return LoadCollection("AutomobilesListCheckSoonOilProc", parms);
		}
		
		/// <summary>
		/// Saves a collection of AutomobileData objects to the database.
		/// </summary>
		/// <returns>A collection containing all of the AutomobileData objects in the database.</returns>
		public static  void  SaveCollection(AutomobilesVector col,string spName, SqlParameter[] parms)
		{
			AutomobilesVector colOld = LoadCollection(spName,parms);
			UpdateCollection(colOld,col);
		}
		public static  void  SaveCollection(AutomobilesVector col)
		{
			AutomobilesVector colOld = LoadCollection();
			UpdateCollection(colOld,col);
		}
		public static  void  SaveCollection(AutomobilesVector col, string whereClause )
		{
			AutomobilesVector colOld = LoadCollection(whereClause);
			UpdateCollection(colOld,col);
		}
		/// <summary>
		/// Loads a collection of AutomobileData objects from the database.
		/// </summary>
		/// <returns>A collection containing all of the AutomobileData objects in the database.</returns>
		public static AutomobilesVector LoadCollection(string whereClause)
		{
			SqlParameter[] parms = new SqlParameter[] { new SqlParameter("@WhereClause", whereClause) };
			return LoadCollection("AutomobilesListProc", parms);
		}

		/// <summary>
		/// Loads a collection of AutomobileData objects from the database.
		/// </summary>
		/// <returns>A collection containing all of the AutomobileData objects in the database.</returns>
		public static AutomobilesVector LoadCollection(string spName, SqlParameter[] parms)
		{
			AutomobilesVector result = new AutomobilesVector();
			using (SqlDataReader reader = SqlHelper.ExecuteReader(CommonDAL.ConnectionString, CommandType.StoredProcedure, spName, parms))
			{
				while (reader.Read())
				{
					AutomobileData tmp = new AutomobileData();
					LoadFromReader(reader, tmp);
					result.Add(tmp);
				}
			}
			return result;
		}
		/// <summary>
		/// Deletes a AutomobileData object from the database.
		/// </summary>
		/// <param name="automobileID">The primary key value</param>
		public static void Delete(int automobileID)
		{
			SqlParameter[] parameterValues = new SqlParameter[] { new SqlParameter("@AutomobileID", automobileID) };
			SqlHelper.ExecuteNonQuery(CommonDAL.ConnectionString, CommandType.StoredProcedure, "AutomobilesInactiveProc", parameterValues);
		}

		public static void LoadFromReader(SqlDataReader reader, AutomobileData target)
		{
			if (reader != null && !reader.IsClosed)
			{
				//target.Key = reader.GetInt32(0);
				if (!reader.IsDBNull(0)) target.AutomobileID = reader.GetInt32(0);
				if (!reader.IsDBNull(1)) target.UserID = reader.GetInt32(1);
				if (!reader.IsDBNull(2)) target.Name = reader.GetString(2);
				if (!reader.IsDBNull(3)) target.RegNumber = reader.GetString(3);
				if (!reader.IsDBNull(4)) target.CivilDate = reader.GetDateTime(4);
				if (!reader.IsDBNull(5)) target.CivilInsuranceAgentID = reader.GetInt32(5);
				if (!reader.IsDBNull(6)) target.KaskoDate = reader.GetDateTime(6);
				if (!reader.IsDBNull(7)) target.KaskoInsuranceAgentID = reader.GetInt32(7);
				if (!reader.IsDBNull(8)) target.GreencardDate = reader.GetDateTime(8);
				if (!reader.IsDBNull(9)) target.GreencardInsuranceAgentID = reader.GetInt32(9);
				if (!reader.IsDBNull(10)) target.AnnualCheckDate = reader.GetDateTime(10);
				if (!reader.IsDBNull(11)) target.OilChangeDate = reader.GetDateTime(11);
				if (!reader.IsDBNull(12)) target.OilChangeKm = reader.GetString(12);
				if (!reader.IsDBNull(13)) target.NextChangeDate = reader.GetDateTime(13);
				if (!reader.IsDBNull(14)) target.NextChangeKm = reader.GetString(14);
				if (!reader.IsDBNull(15)) target.AutoServiceID = reader.GetInt32(15);
				if (!reader.IsDBNull(16)) target.IsActive = reader.GetBoolean(16);
				if(reader.FieldCount>17)
				{
					if (!reader.IsDBNull(17)) target.CivilIns = reader.GetString(17);
					if (!reader.IsDBNull(18)) target.KaskoIns = reader.GetString(18);
					if (!reader.IsDBNull(19)) target.GreenIns = reader.GetString(19);
					if (!reader.IsDBNull(20)) target.AutoService = reader.GetString(20);
				}
			}
		}
		
	  
		public static int Insert( AutomobileData source)
		{
			SqlParameter[] parameterValues = GetInsertParameterValues(source);
			SqlHelper.ExecuteNonQuery(CommonDAL.ConnectionString, CommandType.StoredProcedure, "AutomobilesInsProc", parameterValues);
			source.AutomobileID = (int) parameterValues[0].Value;
			return source.AutomobileID;
		}

		public static int Update( AutomobileData source)
		{
			SqlParameter[] parameterValues = GetUpdateParameterValues(source);
			SqlHelper.ExecuteNonQuery(CommonDAL.ConnectionString, CommandType.StoredProcedure,
				"AutomobilesUpdProc", parameterValues);
			return source.AutomobileID;
		}
		private static void UpdateCollection(AutomobilesVector colOld,AutomobilesVector col)
		{
			// Delete all old items
			foreach( AutomobileData itemOld in colOld)
			{
				if(!ExistsKey(itemOld.AutomobileID, col))
					Delete(itemOld.AutomobileID);
			}
			foreach( AutomobileData item in col)
			{
				if(item.AutomobileID <= -1)
					Insert(item);
				else
					Update(item);
			}
		}
		private static bool ExistsKey(int Key, AutomobilesVector col)
		{
			foreach( AutomobileData item in col)
				if(item.AutomobileID==Key)
					return true;
			return false;
		}

		private static SqlParameter GetSqlParameter(string name, ParameterDirection direction, object value)
		{
			SqlParameter p = new SqlParameter(name, value);
			p.Direction = direction;
			return p;
		}

		private static SqlParameter[] GetInsertParameterValues( AutomobileData source)
		{
			SqlParameter[] prams = new SqlParameter[17];
			prams[0] = GetSqlParameter("@AutomobileID", ParameterDirection.Output, source.AutomobileID);
			if(source.UserID<=0)
				prams[1] = GetSqlParameter("@UserID", ParameterDirection.Input, DBNull.Value);
			else
			prams[1] = GetSqlParameter("@UserID", ParameterDirection.Input, source.UserID);
			prams[2] = GetSqlParameter("@Name", ParameterDirection.Input, source.Name);
			prams[3] = GetSqlParameter("@RegNumber", ParameterDirection.Input, source.RegNumber);
			if(source.CivilDate==Constants.DateMax)
				prams[4] = GetSqlParameter("@CivilDate", ParameterDirection.Input, DBNull.Value);
			else
				prams[4] = GetSqlParameter("@CivilDate", ParameterDirection.Input, source.CivilDate);
			if(source.CivilInsuranceAgentID<=0)
				prams[5] = GetSqlParameter("@CivilInsuranceAgentID", ParameterDirection.Input, DBNull.Value);
			else
			prams[5] = GetSqlParameter("@CivilInsuranceAgentID", ParameterDirection.Input, source.CivilInsuranceAgentID);
			if(source.KaskoDate==Constants.DateMax)
				prams[6] = GetSqlParameter("@KaskoDate", ParameterDirection.Input, DBNull.Value);
			else
			prams[6] = GetSqlParameter("@KaskoDate", ParameterDirection.Input, source.KaskoDate);
			if(source.KaskoInsuranceAgentID<=0)
				prams[7] = GetSqlParameter("@KaskoInsuranceAgentID", ParameterDirection.Input, DBNull.Value);
			else
			prams[7] = GetSqlParameter("@KaskoInsuranceAgentID", ParameterDirection.Input, source.KaskoInsuranceAgentID);
			if(source.GreencardDate==Constants.DateMax)
				prams[8] = GetSqlParameter("@GreencardDate", ParameterDirection.Input, DBNull.Value);
			else
			prams[8] = GetSqlParameter("@GreencardDate", ParameterDirection.Input, source.GreencardDate);
			if(source.GreencardInsuranceAgentID<=0)
				prams[9] = GetSqlParameter("@GreencardInsuranceAgentID", ParameterDirection.Input, DBNull.Value);
			else
			prams[9] = GetSqlParameter("@GreencardInsuranceAgentID", ParameterDirection.Input, source.GreencardInsuranceAgentID);
			if(source.AnnualCheckDate==Constants.DateMax)
				prams[10] = GetSqlParameter("@AnnualCheckDate", ParameterDirection.Input, DBNull.Value);
			else
			prams[10] = GetSqlParameter("@AnnualCheckDate", ParameterDirection.Input, source.AnnualCheckDate);
			if(source.OilChangeDate==Constants.DateMax)
				prams[11] = GetSqlParameter("@OilChangeDate", ParameterDirection.Input, DBNull.Value);
			else
			prams[11] = GetSqlParameter("@OilChangeDate", ParameterDirection.Input, source.OilChangeDate);
			prams[12] = GetSqlParameter("@OilChangeKm", ParameterDirection.Input, source.OilChangeKm);
			if(source.NextChangeDate==Constants.DateMax)
				prams[13] = GetSqlParameter("@NextChangeDate", ParameterDirection.Input, DBNull.Value);
			else
				prams[13] = GetSqlParameter("@NextChangeDate", ParameterDirection.Input, source.NextChangeDate);
			prams[14] = GetSqlParameter("@NextChangeKm", ParameterDirection.Input, source.NextChangeKm);
			if(source.AutoServiceID<=0)
				prams[15] = GetSqlParameter("@AutoServiceID", ParameterDirection.Input, DBNull.Value);
			else
			prams[15] = GetSqlParameter("@AutoServiceID", ParameterDirection.Input, source.AutoServiceID);
			prams[16] = GetSqlParameter("@IsActive", ParameterDirection.Input, source.IsActive);
			return prams;
		}

		private static SqlParameter[] GetUpdateParameterValues( AutomobileData source)
		{
			SqlParameter[] prm = GetInsertParameterValues(source);
			prm[0].Direction=  ParameterDirection.Input;
			return prm;

		}
	}
}   

