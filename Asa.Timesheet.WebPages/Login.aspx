﻿<%@ Register TagPrefix="uc1" TagName="PageHeader" Src="UserControls/PageHeader.ascx" %>
<%@ Page language="c#" Codebehind="Login.aspx.cs" AutoEventWireup="false" Inherits="Asa.Timesheet.WebPages.Login" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
	<HEAD>
		<title>Login</title>
		<meta name="GENERATOR" Content="Microsoft Visual Studio .NET 7.1">
		<meta name="CODE_LANGUAGE" Content="C#">
		<meta name="vs_defaultClientScript" content="JavaScript">
		<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5">
		<LINK href="styles/timesheet.css" type="text/css" rel="stylesheet">
		<script>
		function testForEnterR(field) 
{    

	if (event.keyCode == 13 ) 
	{        
		
		document.getElementById('btnEnter').focus();
     }
} 
		</script>
	</HEAD>
	<body bottomMargin="0" leftMargin="0" topMargin="0" rightMargin="0" onkeydown="testForEnterR()">
		<form id="Form1" method="post" runat="server">
			<table height="100%" cellSpacing="0" cellPadding="0" width="100%" border="0">
				<tr height="1">
					<td>
						<uc1:PageHeader id="header" runat="server"></uc1:PageHeader>
					</td>
				</tr>
				<TR height="1">
					<td noWrap background="images/line.gif" height="1"></td>
				</TR>
				<tr>
					<td>
						<table height="100%" cellSpacing="0" cellPadding="0" width="100%" border="0">
							<tr>
								<td style="BACKGROUND-IMAGE: url(images/left.gif)" vAlign="top" noWrap width="100">
									<asp:PlaceHolder id="menuHolder" runat="server"></asp:PlaceHolder>
								</td>
								<td noWrap width="1" bgColor="lightgrey"></td>
								<td style="PADDING-LEFT: 5px; PADDING-TOP: 3px" vAlign="top" noWrap>
									<TABLE id="Table3" cellSpacing="0" width="890" border="0">
										<TR>
											<TD style="WIDTH: 4px"></TD>
											<TD>
												<TABLE id="Table4" cellSpacing="0" cellPadding="3" border="0">
													<TR>
														<TD style="WIDTH: 141px" colSpan="1" rowSpan="1">
															<asp:label id="lblLoginName" runat="server" Font-Bold="True" CssClass="enterDataLabel" EnableViewState="False">Потребителско име:</asp:label></TD>
														<TD style="WIDTH: 207px">
															<asp:textbox id="txtLoginName" onkeydown="testForEnterR()"   runat="server" Width="200px" CssClass="enterDataBox"></asp:textbox>&nbsp;</TD>
														<TD style="WIDTH: 479px"></TD>
													</TR>
													<TR>
														<TD style="WIDTH: 141px" colSpan="1" rowSpan="1">
															<asp:label id="lblPassword" runat="server" CssClass="enterDataLabel" Font-Bold="True"> Парола:</asp:label></TD>
														<TD style="WIDTH: 207px">
															<asp:textbox id="txtPassword" onkeydown=  "testForEnterR()" runat="server" Width="200px" CssClass="enterDataBox" TextMode="Password"></asp:textbox>&nbsp;</TD>
														<TD style="WIDTH: 479px">
															<asp:button id="btnEnter" runat="server" CssClass="ActionButton" Text="Вход"></asp:button></TD>
													</TR>
												</TABLE>
											</TD>
										</TR>
										<TR>
											<TD style="HEIGHT: 5px" colSpan="2"></TD>
										</TR>
										<TR>
											<TD colSpan="2"><IMG height="1" src="images/dot.gif" width="100%"></TD>
										</TR>
										<TR>
											<TD style="WIDTH: 4px" height="3" colspan="2"></TD>
										</TR>
										<TR>
											<TD style="WIDTH: 4px" colSpan="1" height="3"></TD>
											<td>&nbsp;
												<asp:label id="lblError" runat="server" ForeColor="Red" EnableViewState="False"></asp:label>
												<asp:label id="lblInfo" runat="server" EnableViewState="False"></asp:label></td>
										</TR>
									</TABLE>
									<BR>
									<BR>
									<BR>
								</td>
							</tr>
						</table>
					</td>
				</tr>
			</table>
		</form>
	</body>
</HTML>
