﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web.SessionState;
using System.Web.UI.HtmlControls;
using System.Data.SqlClient;
using log4net;
using Asa.Timesheet.Data;
using Asa.Timesheet.Data.Entities;
using Asa.Timesheet.Data.Util;
using Asa.Timesheet.Data.Reports;
using GrapeCity.ActiveReports.Export.Pdf.Section;
using Asa.Timesheet.WebPages.Reports;

using Microsoft.ApplicationBlocks.Data;

namespace Asa.Timesheet.WebPages
{
    public partial class Tasks : TimesheetPageBase
    {
        int nProjectID;
        private SqlConnection conn = new SqlConnection(CommonDAL.ConnectionString); 

        private static readonly ILog log = LogManager.GetLogger(typeof(HotIssues));

        protected void Page_Load(object sender, EventArgs e)
        {
            nProjectID = UIHelpers.ToInt(ddlProject.SelectedValue);
            LoadProjects();
            //UIHelpers.LoadHotIssueAssigneds(ddlAssignedToNew, nProjectID);
            //BindStatusDropDown(ddlStatusNew);
            //UIHelpers.LoadHotIssueStatuses(ddlStatusNew);
            if (!this.IsPostBack)
            {
                if (SessionManager.CurrentHotIssueInfo != null)
                {
                    if (ddlProject.Items.FindByValue(SessionManager.CurrentHotIssueInfo.ProjectID.ToString()) != null)
                        ddlProject.SelectedValue = SessionManager.CurrentHotIssueInfo.ProjectID.ToString();
                }
                var culture = new System.Globalization.CultureInfo("bg-BG");
                this.Master.Title = ddlProject.SelectedItem.ToString();
                BindGrid();
            }
            
        }


        protected void LoadProjects() {
            SqlDataReader reader = null;
            reader = ProjectsData.SelectProjectNamesRecent();
			
				while (reader.Read())
				{
					int projectID = reader.GetInt32(0);
					ddlProject.Items.Add(new ListItem(reader.GetString(1), projectID.ToString()));
					
				}
        }

        protected void GridView1_RowDeleting(object sender, GridViewDeleteEventArgs e)
        {
            GridViewRow row = (GridViewRow)GridView1.Rows[e.RowIndex];
            
            HiddenField HotIssueID = (HiddenField)row.FindControl("HotIssueID");
            conn.Open();
            SqlCommand cmd = new SqlCommand("delete FROM HotIssue where HotIssueID='" + HotIssueID.Value.ToString() + "'", conn);
            cmd.ExecuteNonQuery();
            conn.Close();
            BindGrid();
        }
        protected void GridView1_RowEditing(object sender, GridViewEditEventArgs e)
        {
            GridView1.EditIndex = e.NewEditIndex;
            BindGrid();
        }
        protected void GridView1_RowUpdating(object sender, GridViewUpdateEventArgs e)
        {
            GridViewRow Row = (GridViewRow)GridView1.Rows[e.RowIndex];
            HiddenField HotIssueID = (HiddenField)Row.FindControl("HotIssueID");
            TextBox txtDeadline = (TextBox)Row.FindControl("txtDeadline");
            TextBox txtName = (TextBox)Row.FindControl("HotIssueName");
            //TextBox txtname=(TextBox)gr.cell[].control[];  
            DropDownList ddlAssigned = (Row.FindControl("ddlAssigned") as DropDownList);
            DropDownList ddlStatus = (Row.FindControl("ddlStatus") as DropDownList);
            //TextBox textadd = (TextBox)row.FindControl("txtadd");  
            //TextBox textc = (TextBox)row.FindControl("txtc");  
            GridView1.EditIndex = -1;
            conn.Open();
            //SqlCommand cmd = new SqlCommand("SELECT * FROM detail", conn);  
            SqlCommand cmd = new SqlCommand("update HotIssue set HotIssueName='" + txtName.Text + "' where HotIssueID = '" + HotIssueID.Value + "'", conn);
            cmd.ExecuteNonQuery();
            conn.Close();
            BindGrid();
            //GridView1.DataBind();  
        }
        protected void GridView1_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            GridView1.PageIndex = e.NewPageIndex;
            BindGrid();
        }
        protected void GridView1_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
        {
            GridView1.EditIndex = -1;
            BindGrid();
        }
        protected void GridView1_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                
                DropDownList ddlAssigned = (e.Row.FindControl("ddlAssigned") as DropDownList);
                DropDownList ddlStatus = (e.Row.FindControl("ddlStatus") as DropDownList);

                HiddenField HotIssueID = (e.Row.FindControl("HotIssueID") as HiddenField);
                HiddenField AssignedTo = (e.Row.FindControl("AssignedTo") as HiddenField);

                UIHelpers.LoadHotIssueAssigneds(ddlAssigned, nProjectID);
                UIHelpers.LoadHotIssueStatuses(ddlStatus);
                /*
                conn.Open();
                SqlCommand cmd = new SqlCommand("select HotIssueStatusID, HotIssueStatusName from HotIssueStatus", conn);
                SqlDataAdapter sda = new SqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                sda.Fill(dt);
                conn.Close();
                ddlStatus.DataSource = dt;

                ddlStatus.DataTextField = "HotIssueStatusName";
                ddlStatus.DataValueField = "HotIssueStatusID";
                ddlStatus.DataBind();
                ddlStatus.Items.Insert(0, new ListItem("--Select Qualification--", "0"));
                */

            }

        }

        protected void BindStatusDropDown(DropDownList ddlStatus) {
            conn.Open();
            SqlCommand cmd = new SqlCommand("select HotIssueStatusID, HotIssueStatusName from HotIssueStatus", conn);
            SqlDataAdapter sda = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            sda.Fill(dt);
            conn.Close();
            ddlStatus.DataSource = dt;

            ddlStatus.DataTextField = "HotIssueStatusName";
            ddlStatus.DataValueField = "HotIssueStatusID";
            ddlStatus.DataBind();
            ddlStatus.Items.Insert(0, new ListItem("--Select Qualification--", "0"));
        }

        private void BindGrid()
        {
            int nProjectID = UIHelpers.ToInt(ddlProject.SelectedValue);
            plcTeam.Items.Clear();
            plcClients.Items.Clear();
            plcBuilders.Items.Clear();
            string Builders, Subs, Team, Clients;
            UIHelpers.LoadItems(nProjectID, out Clients, out Team, out Subs, out Builders);

            if (nProjectID != 0)
            {
                string[] teamList = Team.TrimEnd(new char[] { ' ', ',' }).Split(',');
                int u = 0;
                foreach (string member in teamList)
                {
                    if (!string.IsNullOrEmpty(member))
                    {
                        plcTeam.Items.Add(new ListItem(member.Trim(), "cb_" + u.ToString()));
                        u++;
                    }
                }
                string[] buildList = Builders.TrimEnd(new char[] { ' ', ',' }).Split(',');
                foreach (string builder in buildList)
                {
                    if (!string.IsNullOrEmpty(builder))
                    {
                        plcClients.Items.Add(new ListItem(builder.Trim(), "cb_" + u.ToString()));
                        u++;
                    }
                }
                string[] clientList = Clients.TrimEnd(new char[] { ' ', ',' }).Split(',');
                foreach (string client in clientList)
                {
                    if (!string.IsNullOrEmpty(client))
                    {
                        plcBuilders.Items.Add(new ListItem(client.Trim(), "cb_" + u.ToString()));
                        u++;
                    }
                }
                string[] subContrList = Subs.TrimEnd(new char[] { ' ', ',' }).Split(',');
                foreach (string subContr in subContrList)
                {
                    if (!string.IsNullOrEmpty(subContr))
                    {
                        plcBuilders.Items.Add(new ListItem(subContr.Trim(), "cb_" + u.ToString()));
                        u++;
                    }
                }
            }

            lbSubsData.Text = Subs.TrimEnd(new char[] { ' ', ',' });
            lbSubs.Visible = lbSubsData.Visible = nProjectID > 0;
            conn.Open();
            String spName = "HotIssueProjectSelect";
            SqlCommand cmd = new SqlCommand(spName, conn);  
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.Add("@ProjectID", SqlDbType.VarChar).Value = nProjectID.ToString();
            SqlDataAdapter da = new SqlDataAdapter(cmd);  
            DataSet ds = new DataSet();  
            da.Fill(ds);  
            conn.Close();
            if (ds.Tables[0].Rows.Count > 0)
            {
                GridView1.DataSource = ds;
                GridView1.DataBind();
            }
            else
            {
                ds.Tables[0].Rows.Add(ds.Tables[0].NewRow());
                GridView1.DataSource = ds;
                GridView1.DataBind();
                int columncount = GridView1.Rows[0].Cells.Count;
                GridView1.Rows[0].Cells.Clear();
                GridView1.Rows[0].Cells.Add(new TableCell());
                GridView1.Rows[0].Cells[0].ColumnSpan = columncount;
                GridView1.Rows[0].Cells[0].Text = "No Records Found";
            }  

            //GridView1.Columns[(int)gridColomns.ProjectName].Visible = nProjectID <= 0;


            //hi.TypeID = int.Parse(ddlProjectsStatus.SelectedValue);
            //hi.BuildingType = ddlBuildingTypes.SelectedValue;
            HotIssueInfo hi = new HotIssueInfo();
            hi.ProjectID = int.Parse(ddlProject.SelectedValue);

            //hi.ThemeID = int.Parse(ddlCategory.SelectedValue);
            //hi.StatusID = int.Parse(ddlStatus.SelectedValue);
            //hi.StartDate = txtStartDate.Text;
            //hi.EndDate = txtEndDate.Text;
            SessionManager.CurrentHotIssueInfo = hi;

        }

        private void ddlProject_SelectedIndexChanged(object sender, System.EventArgs e)
        {
            
            //UIHelpers.LoadHotIssueCategories(ddlCategory, UIHelpers.ToInt(ddlProject.SelectedValue), UIHelpers.ToInt(ddlBuildingTypes.SelectedValue), UIHelpers.ToInt(ddlProjectsStatus.SelectedValue));
            BindGrid();
        }
        private void ibPdfExport_Click(object sender, System.EventArgs e)
        {
            ExportMinutes();
        }

        private void ExportMinutes()
        {

            //string PathOfPDF = string.Concat(System.Configuration.ConfigurationManager.AppSettings["MeetingHotIssuesFilesPath"],IDD,System.Configuration.ConfigurationManager.AppSettings["Extension"]);

            int nProjectID = UIHelpers.ToInt(ddlProject.SelectedValue);
            //Manual data entry for the current protocol DBUGG
            string[] DocumentData = new string[3];
            DocumentData[0] = txtID.Text;
            DocumentData[1] = txtDate.Text;
            DocumentData[2] = txtDocuments.Text;
            ////
            DataView dvProjects = UIHelpers.HotIssueCreateDVprojects(nProjectID, false);
            DataTable dtUsers = UIHelpers.HotIssueCreateDVusers(nProjectID);

            string selectedNames = "";

            foreach (ListItem c in plcTeam.Items)
            {
                if (((ListItem)c).Selected)
                {
                    selectedNames += c;
                }
            }
            foreach (ListItem c in plcBuilders.Items)
            {
                if (((ListItem)c).Selected)
                {
                    selectedNames += c;
                }
            }
            foreach (ListItem c in plcClients.Items)
            {
                if (((ListItem)c).Selected)
                {
                    selectedNames += c;
                }
            }
            DataTable tempUsers = dtUsers.Clone();

            string name = "";
            string from = "";
            foreach (DataRow r in dtUsers.Rows)
            {
                name = r.Field<string>("UserName");
                from = r.Field<string>("UserFrom");
                if (selectedNames.Contains(name) || selectedNames.Contains(from))
                    tempUsers.Rows.Add(r.ItemArray);
            }

            DataView dvUsers = new DataView(tempUsers);

            HotIssuesVector hiv = UIHelpers.HotIssueCreateHotIssuesVector(nProjectID, Page.Request.Params["ASA"] == "1", 0, 0, TimeHelper.GetDate(DateTime.Now.ToString()), TimeHelper.GetDate(DateTime.Now.Date.AddDays(365).ToString()), true, false);

            string meetingNumber = MeetingDAL.SelectMeetingNumber(nProjectID, Constants.DateMax);
            string projectName, projectCode, administrativeName, add;
            decimal area = 0;
            int clientID = 0;
            bool phases; bool isPUP;
            ProjectsData.SelectProject(nProjectID, out projectName, out  projectCode, out administrativeName, out area, out add,
                out clientID, out phases, out isPUP);
            string sFileName = DateTime.Now.ToString("yyMMdd") + "_" + projectCode + "_PCM" + meetingNumber;
            GrapeCity.ActiveReports.SectionReport report = new ReportMeetingHotIssuesAll(DateTime.Now.Date, "", "", "", "", dvProjects, dvUsers, hiv, "", true, "", "", nProjectID, LoggedUser.FullName, 0, false, DocumentData);
            report.PageSettings.Orientation = GrapeCity.ActiveReports.Document.Section.PageOrientation.Default;
            report.Run(false);
            report = new ReportMeetingHotIssuesAll(DateTime.Now.Date, "", "", "", "", dvProjects, dvUsers, hiv, "", true, "", "", nProjectID, LoggedUser.FullName, report.Document.Pages.Count, false, DocumentData);
            report.PageSettings.Orientation = GrapeCity.ActiveReports.Document.Section.PageOrientation.Default;
            report.Run();

            PdfExport pdf = new PdfExport();

            pdf.NeverEmbedFonts = "";
            System.IO.MemoryStream memoryFile = new System.IO.MemoryStream();

            pdf.Export(report.Document, memoryFile);

            Response.Clear();
            Response.AppendHeader("content-disposition"
                , "attachment; filename=" + sFileName + ".pdf");
            Response.ContentType = "application/pdf";

            memoryFile.WriteTo(Response.OutputStream);
            Response.End();
            

        }
        #region Web Form Designer generated code
        override protected void OnInit(EventArgs e)
        {
            //
            // CODEGEN: This call is required by the ASP.NET Web Form Designer.
            //
            
            base.OnInit(e);
            if (!this.DesignMode) {
                InitializeComponent();
            }
        }

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {

            this.ddlProject.SelectedIndexChanged += new System.EventHandler(this.ddlProject_SelectedIndexChanged);
            this.ibPdfExport.Click += new System.EventHandler(this.ibPdfExport_Click);


        }
        #endregion
    }

}