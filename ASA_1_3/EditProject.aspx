﻿<%@ Page language="c#" Codebehind="EditProject.aspx.cs" AutoEventWireup="false" Inherits="Asa.Timesheet.WebPages.EditProject" smartNavigation="True"%>
<%@ Register TagPrefix="uc1" TagName="EditForm" Src="UserControls/EditForm.ascx" %>
<%@ Register TagPrefix="radg" Namespace="Telerik.WebControls" Assembly="RadGrid" %>
<%@ Register TagPrefix="uc1" TagName="PageHeader" Src="UserControls/PageHeader.ascx" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
	<head runat="server">
		<title>Edit Project</title>
		<script language="javascript" src="color_conv.js"></script>
		<script language="javascript">
/* Ivailo */
function ArrowButtonClick()
{
	var btn = event.srcElement;
	var src, dest;
	switch (btn.id)
	{
		case "lbSelectedUsersMails" :					  
		case "inputUsersToLeft" :   dest = document.getElementById("lbUsersMails");
									src = document.getElementById("lbSelectedUsersMails");
									break;
		case "lbUsersMails":
		case "inputUsersToRight" :  dest = document.getElementById("lbSelectedUsersMails");
									src = document.getElementById("lbUsersMails");
									break;
		case "lbSelectedClients" :					  
		case "inputClientsToLeft" :   dest = document.getElementById("lbClients");
									src = document.getElementById("lbSelectedClients");
									break;
		case "lbClients":
		case "inputClientsToRight" :  dest = document.getElementById("lbSelectedClients");
									src = document.getElementById("lbClients");
									break;
		case "lbSelectedBuilders" :					  
		case "inputBuildersToLeft" :   dest = document.getElementById("lbBuilders");
									src = document.getElementById("lbSelectedBuilders");
									break;
		case "lbBuilders":
		case "inputBuildersToRight" :  dest = document.getElementById("lbSelectedBuilders");
									src = document.getElementById("lbBuilders");
									break;
		case "lbSelectedSupervisors" :					  
		case "inputSupervisorsToLeft" :   dest = document.getElementById("lbSupervisors");
									src = document.getElementById("lbSelectedSupervisors");
									break;
		case "lbSupervisors":
		case "inputSupervisorsToRight" :  dest = document.getElementById("lbSelectedSupervisors");
									src = document.getElementById("lbSupervisors");
									break;					
	}					
	//if(src == null) return;
	for (i=0; i<src.options.length; i++)
	if (src.options[i].selected)
	{ 	
	    var email = src.options[i].innerText;			
		var value = src.options[i].value;
		var opt = document.createElement("OPTION");
		dest.options.add(opt); 	
		opt.innerText = email;		
		opt.value = value;
	}	
	for (i=src.options.length-1;i>=0 ;i--)
	if (src.options[i].selected) src.options.remove(i);			
	UpdateContainer(dest);
	UpdateContainer(src);
}
/* Ivailo */
function UpdateContainer(listBox)
		{
			var container = document.getElementById("hdn"+listBox.id);
			
			var arr = new Array();
			for (i=0; i<listBox.options.length; i++) arr.push(listBox.options[i].value);
			container.value = arr.join(";");
			
		}
		
function OnBtnColorClick(color,param){
	fnShowChooseColorDlg(color,param,"");
}

function OnChangeColor(color,param){
	if(color){
		if(param==1){
			document.getElementById('txtColor').value = color;
			document.getElementById('txtColor').style.backgroundColor = color;
		}
	}
}
function setColor()
{
if(document.getElementById('txtColor').value !="")
			document.getElementById('txtColor').style.backgroundColor = document.getElementById('txtColor').value;
}

		
function Print()
{
	var r= location.toString();

	if(r.substr(r.length-1,1)=="#")
		window.open(r.substr(0,r.length-1)+'&print=1');
	else
	window.open(location+'&print=1');
}
function Header()
{
	var r= location.toString();

	if(r.substr(r.length-1,1)=="#")
		window.open(r.substr(0,r.length-1)+'&print=header');
	else
	window.open(location+'&print=header');
}
function Fax()
{
var r= location.toString();

	if(r.substr(r.length-1,1)=="#")
		window.open(r.substr(0,r.length-1)+'&print=fax');
	else
	window.open(location+'&print=fax');
}
function Reg()
{
var r= location.toString();

	if(r.substr(r.length-1,1)=="#")
		window.open(r.substr(0,r.length-1)+'&print=reg');
	else
	window.open(location+'&print=reg');
}
function Print(what)
{
var r= location.toString();

	if(r.substr(r.length-1,1)=="#")
		window.open(r.substr(0,r.length-1)+'&print='+what);
	else
	window.open(location+'&print='+what);
}
function Print1()
{var r= location.toString();

	if(r.substr(r.length-1,1)=="#")
		window.open(r.substr(0,r.length-1)+'&print=1');
	else
	window.open(location+'&print=1');
}
function Buildings()
{
	LeftPosition = (screen.width) ? (screen.width-800)/2 : 0;
	TopPosition = (screen.height) ? (screen.height-800)/2 : 0;		
	varFeature = "height="+800+",width="+600+",top="+TopPosition+",left="+LeftPosition+",resizable=no,scrollbars=yes";
	
	var wpopup = window.open('Buildings.aspx?pid='+document.forms[0].hidID.value,"Buildings",varFeature);

}


		</script>
		<script language="javascript" src="PopupCalendar.js"></script>
		<meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
		<meta content="C#" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<script language="javascript" src="js/EditForm.js"></script>
		<LINK href="styles/timesheet.css" type="text/css" rel="stylesheet">
	</HEAD>
	<body bottomMargin="0" leftMargin="0" topMargin="0" onload="setColor()" rightMargin="0">
		<form id="Form1" method="post" runat="server">
			<table height="100%" cellSpacing="0" cellPadding="0" width="100%" border="0">
				<tr height="1">
					<td><uc1:pageheader id="header" runat="server"></uc1:pageheader><uc1:editform id="editCtrl" runat="server"></uc1:editform><INPUT id="hidID" type="hidden" name="hidID" runat="server"></td>
				</tr>
				<tr>
					<td>
						<table height="100%" cellSpacing="0" cellPadding="0" width="100%" border="0">
							<tr>
								<td class="menuLeft"><asp:placeholder id="menuHolder" runat="server"></asp:placeholder></td>
								
								<td style="PADDING-LEFT: 5px; PADDING-TOP: 3px" vAlign="top" noWrap>
									<TABLE id="Table3" cellSpacing="0" width="880" border="0" runat="server">
										<TR>
											<td style="HEIGHT: 5px" colSpan="2"></TD>
										</TR>
										<TR>
											<td colSpan="2"><IMG height="1" src="images/dot.gif" width="100%"></TD>
										</TR>
										<TR>
											<td colSpan="2" height="3"></td>
										</TR>
										<TR>
											<td style="WIDTH: 4px"></TD>
											<td>
												<TABLE id="Table15" cellSpacing="0" cellPadding="0" border="0">
													<TR>
														<td style="WIDTH: 184px" noWrap><asp:button id="btnSaveUP" runat="server" CssClass="ActionButton" Text="Запиши"></asp:button><asp:button id="btnEditUP" style="DISPLAY: none" runat="server" CssClass="ActionButton" Text="Редактирай"></asp:button></TD>
														<td style="WIDTH: 321px" noWrap><asp:button id="btnCancelUP" runat="server" CssClass="ActionButton" Width="168px" Text="Обратно към проекти"></asp:button></TD>
														<td></TD>
													</TR>
												</TABLE>
											</TD>
										</TR>
										<TR>
											<td style="WIDTH: 4px" height="3"></TD>
										</TR>
										<TR>
											<td colSpan="2"><IMG height="1" src="images/dot.gif" width="100%"></TD>
										</TR>
										<TR>
											<td style="WIDTH: 4px"></TD>
											<td>
												<TABLE id="Table4" cellSpacing="0" cellPadding="3" width="100%" border="0">
													<TR>
														<td id="formCell" style="WIDTH: 570px">
															<table id="tblForm" cellSpacing="0" cellPadding="3" width="100%" border="0">
																<tr>
																	<td style="WIDTH: 168px"><asp:label id="lblName" runat="server" EnableViewState="False" Font-Bold="True" CssClass="enterDataLabel">Име на проект:</asp:label></td>
																	<td style="WIDTH: 378px"><asp:textbox id="txtProjectName" runat="server" CssClass="enterDataBox" Width="350px"></asp:textbox>&nbsp;<IMG height="16" alt="" src="images/required1.gif" width="16"></TD>
																<TR>
																	<td style="WIDTH: 168px"><asp:label id="lblOwner" runat="server" CssClass="enterDataLabel">Собственик:</asp:label></TD>
																	<td style="WIDTH: 378px"><asp:textbox id="txtOwner" runat="server" CssClass="enterDataBox" Width="350px"></asp:textbox></TD>
																</TR>
																<TR>
																	<td style="WIDTH: 168px" vAlign="top"><asp:label id="Label11" runat="server" CssClass="enterDataLabel">Инвеститор:</asp:label></TD>
																	<td style="WIDTH: 378px"><asp:textbox id="txtInvestor" runat="server" CssClass="enterDataBox" Width="350px" Rows="3" TextMode="MultiLine"></asp:textbox></TD>
																</TR>
																<TR>
																	<td style="WIDTH: 168px"><asp:label id="lblBuildingType" runat="server" CssClass="enterDataLabel">Тип сграда:</asp:label></TD>
																	<td style="WIDTH: 378px"><asp:dropdownlist id="ddlBuildingTypes" runat="server" CssClass="enterDataBox" Width="350px" AutoPostBack="True"></asp:dropdownlist></TD>
																</TR>
																<TR>
																	<td style="WIDTH: 168px"><asp:label id="lblProjectCode" runat="server" Font-Bold="True" CssClass="enterDataLabel">Код на проект:</asp:label></TD>
																	<td style="WIDTH: 378px">
																		<TABLE id="Table2" cellSpacing="0" cellPadding="0" width="350" border="0">
																			<TR>
																				<td class="projectCodeBox" noWrap>&nbsp;<asp:label id="lblBTCode" runat="server" Font-Bold="True"></asp:label></TD>
																				<td noWrap width="5"></td>
																				<td class="projectCodeBox" noWrap>&nbsp;<asp:label id="lblProjectOrder" runat="server" Font-Bold="True"></asp:label></TD>
																				<td noWrap width="5"></td>
																				<td noWrap><asp:textbox id="txtProjectCode" runat="server" CssClass="enterDataBox" Width="165px" MaxLength="2"></asp:textbox><IMG height="16" alt="" src="images/required1.gif" width="16"></TD>
																			</TR>
																		</TABLE>
																	</TD>
																</TR>
																<TR>
																	<td style="WIDTH: 168px"><asp:label id="Label18" runat="server" CssClass="enterDataLabel">Връзка към папката:</asp:label></TD>
																	<td style="WIDTH: 378px"><asp:hyperlink id="hlProjectFolder" runat="server" CssClass="projectFolderLink" Target="_blank"
																			Visible="False">hlProjectFolder</asp:hyperlink><BR>
																		<asp:button id="btnCreateFolder" runat="server" CssClass="ActionButton" Width="112px" Visible="False"
																			Text="Създай папка"></asp:button></TD>
																</TR>
																<TR>
																	<td style="WIDTH: 168px"><asp:label id="lblStatus" runat="server" Font-Bold="True" CssClass="enterDataLabel" Visible="False">Статус:</asp:label></TD>
																	<td style="WIDTH: 378px"><asp:checkbox id="ckActive" runat="server" Visible="False" Text="Активен " Checked="True"></asp:checkbox>&nbsp;
																		<asp:checkbox id="ckBlack" runat="server" Visible="False" Text="Без ДДС"></asp:checkbox><asp:checkbox id="ckConcluded" runat="server" AutoPostBack="True" Visible="False" Text="Приключен"></asp:checkbox></TD>
																</TR>
																<TR>
																	<td style="WIDTH: 172px"><asp:Label ID="lbRegisterCameraArchitects" Runat="server" CssClass="enterDataLabel">Регистриран в камерата на архитектите:</asp:Label></TD>
																	<td><asp:CheckBox id="cbRegisterCameraArchitects" runat="server" CssClass="enterDataBox"></asp:CheckBox>
																	</TD>
																</TR>
																<TR>
																	<td style="WIDTH: 168px" vAlign="top"><asp:label id="lblAdministrativeName" runat="server" CssClass="enterDataLabel">Административно име:</asp:label></TD>
																	<td style="WIDTH: 378px"><asp:textbox id="txtAdministrativeName" runat="server" CssClass="enterDataBox" Width="350px"
																			Rows="3" TextMode="MultiLine"></asp:textbox></TD>
																</TR>
																<TR>
																	<td style="WIDTH: 168px" vAlign="top">
																		<asp:label id="Label33" runat="server" CssClass="enterDataLabel">Административно име(английски):</asp:label></TD>
																	<td style="WIDTH: 378px">
																		<asp:textbox id="txtNameEN" runat="server" CssClass="enterDataBox" Width="350px" TextMode="MultiLine"
																			Rows="3"></asp:textbox></TD>
																</TR>
																<TR>
																	<td style="WIDTH: 168px" vAlign="top"><asp:label id="Label12" runat="server" CssClass="enterDataLabel">Адрес - пл. №:</asp:label></TD>
																	<td style="WIDTH: 378px"><asp:textbox id="txtAddInfo" runat="server" CssClass="enterDataBox" Width="350px" Rows="3" TextMode="MultiLine"></asp:textbox></TD>
																</TR>
																<TR>
																	<td style="HEIGHT: 23px" vAlign="top"></TD>
																	<td style="HEIGHT: 23px"><asp:dropdownlist id="ddlContents" runat="server" CssClass="enterDataBox" Width="184px"></asp:dropdownlist><asp:button id="btnAdd" runat="server" CssClass="ActionButton" Width="80px" Text="Добави"></asp:button></TD>
																</TR>
																<TR>
																	<td vAlign="top"></TD>
																	<td><asp:datagrid id="grd" runat="server" CssClass="Grid" Width="350px" CellPadding="4" AutoGenerateColumns="False">
																			<HeaderStyle CssClass="GridHeader1"></HeaderStyle>
																			<Columns>
																				<asp:BoundColumn Visible="False" DataField="ContentTypeID">
																					<HeaderStyle Width="0"></HeaderStyle>
																				</asp:BoundColumn>
																				<asp:BoundColumn DataField="ContentType" HeaderText="Съдържание">
																					<HeaderStyle Width="150px"></HeaderStyle>
																				</asp:BoundColumn>
																				<asp:TemplateColumn HeaderText="Текст">
																					<HeaderStyle Width="100px"></HeaderStyle>
																					<ItemTemplate>
																						<asp:TextBox id=txtT runat="server" Width="130px" Text='<%# DataBinder.Eval(Container, "DataItem.ContentText") %>'>
																						</asp:TextBox>
																					</ItemTemplate>
																				</asp:TemplateColumn>
																				<asp:TemplateColumn HeaderText="Мащаб">
																					<ItemTemplate>
																						<asp:TextBox id=txtMa runat="server" Width="61px" Text='<%# DataBinder.Eval(Container, "DataItem.Scale") %>'>
																						</asp:TextBox>
																					</ItemTemplate>
																				</asp:TemplateColumn>
																				<asp:ButtonColumn Text="&lt;img border=0 alt='Изтрий' src='images/delete.gif'&gt;" CommandName="Delete"></asp:ButtonColumn>
																			</Columns>
																		</asp:datagrid><asp:datagrid id="grdDistr" runat="server" CssClass="Grid" Width="350px" CellPadding="4" AutoGenerateColumns="False">
																			<HeaderStyle CssClass="GridHeader1"></HeaderStyle>
																			<Columns>
																				<asp:BoundColumn Visible="False" DataField="ContentTypeID">
																					<HeaderStyle Width="15%"></HeaderStyle>
																				</asp:BoundColumn>
																				<asp:BoundColumn DataField="ContentType" HeaderText="Разпределение">
																					<HeaderStyle Width="100px"></HeaderStyle>
																				</asp:BoundColumn>
																				<asp:TemplateColumn HeaderText="Кота">
																					<ItemTemplate>
																						<asp:TextBox id=txtKota runat="server" Width="61px" Text='<%# Asa.Timesheet.WebPages.UIHelpers.FormatDecimal2((decimal)DataBinder.Eval(Container, "DataItem.Kota")) %>'>
																						</asp:TextBox>
																					</ItemTemplate>
																				</asp:TemplateColumn>
																				<asp:TemplateColumn HeaderText="ЗП Кв.метра">
																					<ItemTemplate>
																						<asp:TextBox id=txtA runat="server" Width="61px" Text='<%# Asa.Timesheet.WebPages.UIHelpers.FormatDecimal2((decimal)DataBinder.Eval(Container, "DataItem.Area")) %>' >
																						</asp:TextBox>
																					</ItemTemplate>
																				</asp:TemplateColumn>
																				<asp:TemplateColumn HeaderText="Мащаб">
																					<ItemTemplate>
																						<asp:TextBox id=txtMa runat="server" Width="61px" Text='<%# DataBinder.Eval(Container, "DataItem.Scale") %>'>
																						</asp:TextBox>
																					</ItemTemplate>
																				</asp:TemplateColumn>
																				<asp:ButtonColumn Text="&lt;img border=0 alt='Изтрий' src='images/delete.gif'&gt;" CommandName="Delete"></asp:ButtonColumn>
																			</Columns>
																		</asp:datagrid></TD>
																</TR>
																<TR>
																	<td style="WIDTH: 168px" vAlign="top"></TD>
																	<td style="WIDTH: 378px"><asp:textbox id="txtProjectPhase" runat="server" Width="20px" MaxLength="1" Visible="False" ReadOnly="True"></asp:textbox></TD>
																</TR>
																<TR>
																	<td style="WIDTH: 168px"><asp:label id="Label14" runat="server" CssClass="enterDataLabel">Динамична РЗП:</asp:label></TD>
																	<td style="WIDTH: 378px"><asp:textbox id="txtArea1" runat="server" CssClass="enterDataBox" Width="165px"></asp:textbox><asp:button id="btnCalculate" runat="server" CssClass="ActionButton" Width="136px" Text="Преизчисли площ"></asp:button></TD>
																</TR>
																<TR>
																	<td style="WIDTH: 168px"><asp:label id="lblArea" runat="server" CssClass="enterDataLabel">Статична РЗП:</asp:label></TD>
																	<td style="WIDTH: 378px"><asp:textbox id="txtArea" runat="server" CssClass="enterDataBox" Width="165px"></asp:textbox></TD>
																</TR>
																<TR>
																	<td style="WIDTH: 168px; HEIGHT: 25px" vAlign="top"><asp:label id="lblStartDate" runat="server" CssClass="enterDataLabel">Начална дата:</asp:label></TD>
																	<td style="WIDTH: 378px; HEIGHT: 25px"><asp:textbox id="txtStartDate" runat="server" CssClass="enterDataBox" Width="88px" MaxLength="10"></asp:textbox>&nbsp;<IMG id="lkCalendar1" alt="Calendar" src="images/calendar.ico" width="18" align="absBottom"
																			runat="server"></TD>
																</TR>
																<TR>
																	<td style="WIDTH: 168px" vAlign="top"><asp:label id="Label7" runat="server" CssClass="enterDataLabel">Крайна дата по договор:</asp:label></TD>
																	<td style="WIDTH: 378px"><asp:textbox id="txtEndDateContract" runat="server" CssClass="enterDataBox" Width="88px" MaxLength="10"></asp:textbox>&nbsp;<IMG id="lkCalendar3" alt="Calendar" src="images/calendar.ico" width="18" align="absBottom"
																			runat="server"></TD>
																</TR>
																<TR>
																	<td style="WIDTH: 168px" vAlign="top"><asp:label id="Label1" runat="server" CssClass="enterDataLabel">Крайна дата на строеж:</asp:label></TD>
																	<td style="WIDTH: 378px"><asp:textbox id="txtEndDate" runat="server" CssClass="enterDataBox" Width="88px" MaxLength="10"></asp:textbox>&nbsp;<IMG id="lkCalendar2" alt="Calendar" src="images/calendar.ico" width="18" align="absBottom"
																			runat="server"></TD>
																</TR>
																<TR>
																	<td style="WIDTH: 168px"></TD>
																	<td style="WIDTH: 378px"></TD>
																</TR>
																<TR>
																	<td style="WIDTH: 168px"><asp:label id="lblAddress" runat="server" CssClass="enterDataLabel">Адрес:</asp:label></TD>
																	<td style="WIDTH: 378px"><asp:textbox id="txtAddress" runat="server" CssClass="enterDataBox" Width="350px"></asp:textbox></TD>
																</TR>
																<TR>
																	<td style="WIDTH: 168px"><asp:label id="lblClient" runat="server" CssClass="enterDataLabel">Клиент: </asp:label></TD>
																	<td style="WIDTH: 378px"><asp:dropdownlist id="ddlClients" runat="server" CssClass="enterDataBox" Width="350px" AutoPostBack="True"></asp:dropdownlist></TD>
																</TR>
																<TR>
																	<td style="WIDTH: 168px"><asp:label id="lblMoreClients" runat="server" Font-Bold="true" CssClass="enterDataLabel">Други клиенти:</asp:label></TD>
																	<td style="WIDTH: 378px">
																		<TABLE id="TableMoreClients" cellSpacing="0" cellPadding="0" width="100%" border="0" runat="server">
																			<TR>
																				<td style="WIDTH: 158px"><asp:listbox id="lbClients" runat="server" CssClass="enterDataBox" Width="157px" Height="106px"
																						SelectionMode="Multiple"></asp:listbox></TD>
																				<td style="WIDTH: 34px">
																					<TABLE id="Table11" border="0">
																						<TR>
																							<td style="HEIGHT: 19px"><asp:button id="btnClientRight" runat="server" CssClass="ActionButton" Width="26px" Text=">"
																									Height="18px"></asp:button></TD>
																						</TR>
																						<TR>
																							<td><asp:button id="btnClientLeft" runat="server" CssClass="ActionButton" Width="26px" Text="<"
																									Height="18px"></asp:button></TD>
																						</TR>
																					</TABLE>
																				</TD>
																				<td><asp:listbox id="lbSelectedClients" runat="server" CssClass="enterDataBox" Width="157px" Height="106px"
																						SelectionMode="Multiple"></asp:listbox><INPUT id="hdnlbClients" style="WIDTH: 19px; DISPLAY: none; HEIGHT: 19px" size="1" name="Text1"
																						runat="server"><INPUT id="hdnlbSelectedClients" style="WIDTH: 19px; DISPLAY: none; HEIGHT: 19px" size="1"
																						name="Text2" runat="server"></TD>
																			</TR>
																		</TABLE>
																	</TD>
																</TR>
																<TR>
																	<td style="WIDTH: 168px"><asp:label id="lbMoreBuilders" runat="server" Font-Bold="true" CssClass="enterDataLabel">Строители/Проектни мениджъри/ Консултанти/Брокери:</asp:label></TD>
																	<td style="WIDTH: 378px">
																		<TABLE id="Table6" cellSpacing="0" cellPadding="0" width="100%" border="0" runat="server">
																			<TR>
																				<td style="WIDTH: 158px"><asp:listbox id="lbBuilders" ondblclick="ArrowButtonClick();" runat="server" CssClass="enterDataBox"
																						Width="157px" Height="106px" SelectionMode="Multiple"></asp:listbox></TD>
																				<td style="WIDTH: 34px">
																					<TABLE id="Table12" border="0">
																						<TR>
																							<td><INPUT id="inputBuildersToRight" onclick="ArrowButtonClick();" Class="ActionButton" style="WIDTH: 26px; HEIGHT: 18px"
																									type="button" Height="18" value=">" name="btnBuilderRight"></TD>
																						</TR>
																						<TR>
																							<td><INPUT id="inputBuildersToLeft" onclick="ArrowButtonClick();" Class="ActionButton" style="WIDTH: 26px; HEIGHT: 18px"
																									type="button" value="<" name="btnBuilderLeft"></TD>
																						</TR>
																					</TABLE>
																				</TD>
																				<td><asp:listbox id="lbSelectedBuilders" ondblclick="ArrowButtonClick();" runat="server" CssClass="enterDataBox"
																						Width="157px" Height="106px" SelectionMode="Multiple"></asp:listbox><INPUT id="hdnlbBuilders" style="WIDTH: 19px; DISPLAY: none; HEIGHT: 19px" size="1" name="Text1"
																						runat="server"><INPUT id="hdnlbSelectedBuilders" style="WIDTH: 19px; DISPLAY: none; HEIGHT: 19px" size="1"
																						name="Text2" runat="server"></TD>
																			</TR>
																		</TABLE>
																	</TD>
																</TR>
																<TR>
																	<td style="WIDTH: 168px"></TD>
																	<td style="WIDTH: 378px"><asp:label id="Label32" runat="server" Font-Bold="True" CssClass="enterDataLabel" Width="175px">Технически ръководител</asp:label></TD>
																</TR>
																<TR>
																	<td style="WIDTH: 168px"><asp:label id="Label29" runat="server" Font-Bold="True" CssClass="enterDataLabel">Име:</asp:label></TD>
																	<td style="WIDTH: 378px"><asp:textbox id="txtProjectTechnicalName" runat="server" CssClass="enterDataBox" Width="350px"></asp:textbox></TD>
																</TR>
																<TR>
																	<td style="WIDTH: 168px"><asp:label id="Label30" runat="server" Font-Bold="True" CssClass="enterDataLabel"> Телефон:</asp:label></TD>
																	<td style="WIDTH: 378px"><asp:textbox id="txtProjectTechnicalPhone" runat="server" CssClass="enterDataBox" Width="350px"></asp:textbox></TD>
																</TR>
																<TR>
																	<td style="WIDTH: 168px"><asp:label id="Label31" runat="server" Font-Bold="True" CssClass="enterDataLabel"> e-mail:</asp:label></TD>
																	<td style="WIDTH: 378px"><asp:textbox id="txtProjectTechnicalEmail" runat="server" CssClass="enterDataBox" Width="350px"></asp:textbox></TD>
																</TR>
																<TR>
																	<td style="WIDTH: 168px"></TD>
																	<td style="WIDTH: 378px"></TD>
																</TR>
																<TR>
																	<td style="WIDTH: 168px"><asp:label id="lbMoreSupervisors" runat="server" Font-Bold="true" CssClass="enterDataLabel">Надзорници:</asp:label></TD>
																	<td style="WIDTH: 378px">
																		<TABLE id="Table7" cellSpacing="0" cellPadding="0" width="100%" border="0" runat="server">
																			<TR>
																				<td style="WIDTH: 158px"><asp:listbox id="lbSupervisors" ondblclick="ArrowButtonClick();" runat="server" CssClass="enterDataBox"
																						Width="157px" Height="106px" SelectionMode="Multiple"></asp:listbox></TD>
																				<td style="WIDTH: 34px">
																					<TABLE id="Table13" border="0">
																						<TR>
																							<td><INPUT id="inputSupervisorsToRight" onclick="ArrowButtonClick();" Class="ActionButton"
																									style="WIDTH: 26px; HEIGHT: 18px" type="button" Height="18" value=">" name="btnSupervisorRight"></TD>
																						</TR>
																						<TR>
																							<td><INPUT id="inputSupervisorsToLeft" onclick="ArrowButtonClick();" Class="ActionButton" style="WIDTH: 26px; HEIGHT: 18px"
																									type="button" value="<" name="btnSupervisorLeft"></TD>
																						</TR>
																					</TABLE>
																				</TD>
																				<td><asp:listbox id="lbSelectedSupervisors" ondblclick="ArrowButtonClick();" runat="server" CssClass="enterDataBox"
																						Width="157px" Height="106px" SelectionMode="Multiple"></asp:listbox><INPUT id="hdnlbSupervisors" style="WIDTH: 19px; DISPLAY: none; HEIGHT: 19px" size="1"
																						name="Text1" runat="server"><INPUT id="hdnlbSelectedSupervisors" style="WIDTH: 19px; DISPLAY: none; HEIGHT: 19px" size="1"
																						name="Text2" runat="server"></TD>
																			</TR>
																		</TABLE>
																	</TD>
																</TR>
																<TR>
																	<td style="WIDTH: 168px"><asp:label id="Label16" runat="server" CssClass="enterDataLabel">Предадени папки на инвеститора:</asp:label></TD>
																	<td style="WIDTH: 378px"><asp:textbox id="txtFoldersGiven" runat="server" CssClass="enterDataBox" Width="40px"></asp:textbox></TD>
																</TR>
																<TR>
																	<td style="WIDTH: 168px"><asp:label id="Label17" runat="server" CssClass="enterDataLabel">Останали папки в архива:</asp:label></TD>
																	<td style="WIDTH: 378px"><asp:textbox id="txtFoldersArchive" runat="server" CssClass="enterDataBox" Width="40px"></asp:textbox></TD>
																</TR>
																<TR>
																	<td style="WIDTH: 168px"><asp:label id="Label22" runat="server" CssClass="enterDataLabel">Цвят:</asp:label></TD>
																	<td style="WIDTH: 378px"><asp:textbox id="txtColor" runat="server" CssClass="enterDataBox" Width="88px" MaxLength="10"></asp:textbox>&nbsp;<INPUT class="ActionButton" id="btnChoose" type="button" value="Избери цвят" name="Button3"
																			runat="server"></TD>
																</TR>
																<TR>
																	<td style="WIDTH: 168px; HEIGHT: 17px"><asp:label id="lblManager" runat="server" EnableViewState="False" Font-Bold="True" CssClass="enterDataLabel">Ръководител:</asp:label></TD>
																	<td style="WIDTH: 378px; HEIGHT: 17px"><asp:dropdownlist id="ddlManager" runat="server" CssClass="enterDataBox" Width="350px"></asp:dropdownlist>&nbsp;<IMG height="16" alt="" src="images/required1.gif" width="16"></TD>
																</TR>
																<TR style="DISPLAY: none">
																	<td style="WIDTH: 168px"><asp:label id="lblHasActivity" runat="server" EnableViewState="False" Font-Bold="True" CssClass="enterDataLabel"
																			Visible="False">Изисква вид дейност:</asp:label></TD>
																	<td style="WIDTH: 378px"><asp:checkbox id="cbHasActivity" style="POSITION: relative; LEFT: -4px" runat="server" Visible="False"
																			Checked="True"></asp:checkbox>&nbsp;<IMG style="DISPLAY: none" height="16" alt="" src="images/required1.gif" width="16"></TD>
																</TR>
																<TR>
																	<td style="WIDTH: 168px"><asp:label id="Label23" runat="server" Font-Bold="true" CssClass="enterDataLabel">Екип:</asp:label></TD>
																	<td style="WIDTH: 378px">
																		<TABLE id="Table8" cellSpacing="0" cellPadding="0" width="100%" border="0">
																			<TR>
																				<td style="WIDTH: 158px"><asp:listbox id="lbUsersMails" ondblclick="ArrowButtonClick();" runat="server" CssClass="enterDataBox"
																						Width="157px" Height="106px" SelectionMode="Multiple"></asp:listbox></TD>
																				<td style="WIDTH: 34px">
																					<TABLE id="Table9" border="0">
																						<TR>
																							<td><INPUT class="ActionButton" id="inputUsersToRight" style="WIDTH: 26px; HEIGHT: 18px" onclick="ArrowButtonClick();"
																									type="button" value=">" name="Button2"></TD>
																						</TR>
																						<TR>
																							<td><INPUT class="ActionButton" id="inputUsersToLeft" style="WIDTH: 26px; HEIGHT: 18px" onclick="ArrowButtonClick();"
																									type="button" value="<" name="Button1"></TD>
																						</TR>
																					</TABLE>
																				</TD>
																				<td><asp:listbox id="lbSelectedUsersMails" ondblclick="ArrowButtonClick();" runat="server" CssClass="enterDataBox"
																						Width="157px" Height="106px" SelectionMode="Multiple"></asp:listbox><INPUT id="hdnlbUsersMails" style="WIDTH: 19px; DISPLAY: none; HEIGHT: 19px" size="1" name="Text1"
																						runat="server"><INPUT id="hdnlbSelectedUsersMails" style="WIDTH: 19px; DISPLAY: none; HEIGHT: 19px" size="1"
																						name="Text2" runat="server"></TD>
																			</TR>
																		</TABLE>
																	</TD>
																</TR>
															</table>
															<table id="tbl" cellSpacing="0" cellPadding="3" border="0" runat="server">
																<TR>
																	<td style="WIDTH: 172px; HEIGHT: 15px"><asp:label id="Label2" runat="server" Font-Bold="True" CssClass="enterDataLabel">Схема на плащане: </asp:label></TD>
																	<td style="HEIGHT: 15px"><asp:dropdownlist id="ddlScheme" runat="server" CssClass="enterDataBox" Width="350px"></asp:dropdownlist>&nbsp;<IMG height="16" alt="" src="images/required1.gif" width="16"></TD>
																</TR>
																<TR>
																	<td style="WIDTH: 172px"><asp:label id="Label3" runat="server" CssClass="enterDataLabel">Договорена цена:</asp:label></TD>
																	<td><asp:textbox id="txtRate" runat="server" CssClass="enterDataBox" Width="184px"></asp:textbox></TD>
																</TR>
																<TR>
																	<td style="WIDTH: 172px"></TD>
																	<td><asp:button id="btn" runat="server" CssClass="ActionButton" Text="Преизчисли"></asp:button></TD>
																</TR>
																<TR>
																	<td style="WIDTH: 172px; HEIGHT: 19px"><asp:label id="Label4" runat="server" EnableViewState="False" CssClass="enterDataLabel">Обща сума(EUR):</asp:label></TD>
																	<td style="HEIGHT: 19px"><asp:label id="lbEUR" runat="server" EnableViewState="False" CssClass="enterDataBox"></asp:label></TD>
																</TR>
																<TR>
																	<td style="WIDTH: 172px; HEIGHT: 19px"><asp:label id="Label5" runat="server" EnableViewState="False" CssClass="enterDataLabel">Обща сума(BGN):</asp:label></TD>
																	<td style="HEIGHT: 19px"><asp:label id="lbBGN" runat="server" EnableViewState="False" CssClass="enterDataBox"></asp:label></TD>
																</TR>
																<TR>
																	<td style="WIDTH: 172px">
																		<asp:label style="Z-INDEX: 0" id="Label34" runat="server" CssClass="enterDataLabel" Font-Bold="True"
																			EnableViewState="False">Процент за оценка на разходи:</asp:label></TD>
																	<td>
																		<asp:textbox style="Z-INDEX: 0" id="txtPercent" runat="server" CssClass="enterDataBox" Width="40px"></asp:textbox>&nbsp;</TD>
																</TR>
																<TR>
																	<td style="WIDTH: 172px">
																		<asp:label style="Z-INDEX: 0" id="Label35" runat="server" CssClass="enterDataLabel" EnableViewState="False">Текущи разходи:</asp:label></TD>
																	<td>
																		<asp:label style="Z-INDEX: 0" id="lbCurrentPercent" runat="server" CssClass="enterDataBox"
																			EnableViewState="False"></asp:label></TD>
																</TR>
																<TR>
																	<td style="WIDTH: 172px"><asp:label id="Label20" runat="server" CssClass="enterDataLabel">Цена за авт. надзор<BR> (EUR за час):</asp:label></TD>
																	<td><asp:textbox id="txtAuthor" runat="server" CssClass="enterDataBox" Width="40px"></asp:textbox></TD>
																</TR>
																<TR>
																	<td style="WIDTH: 172px"><asp:label id="Label21" runat="server" CssClass="enterDataLabel">Срок за фактуриране за авт. надзор:</asp:label></TD>
																	<td><asp:dropdownlist id="ddlAuth" runat="server" CssClass="enterDataBox" Width="112px">
																			<asp:ListItem Value="0" Selected="True">Няма</asp:ListItem>
																			<asp:ListItem Value="1">1 месец</asp:ListItem>
																			<asp:ListItem Value="2">2 месеца</asp:ListItem>
																			<asp:ListItem Value="3">3 месеца</asp:ListItem>
																		</asp:dropdownlist></TD>
																</TR>
																<TR>
																	<td style="WIDTH: 172px"></TD>
																	<td><asp:label id="lbPayments" runat="server" EnableViewState="False" CssClass="enterDataBox">Разпределение на плащанията:</asp:label><asp:checkbox id="ckPhases" runat="server" CssClass="enterDataBox" AutoPostBack="True" Text="по фази"></asp:checkbox><asp:checkbox id="cbPUP" runat="server" CssClass="enterDataBox" AutoPostBack="True" Text="Тип ПУП"></asp:checkbox></TD>
																</TR>
															</table>
														</TD>
														<td vAlign="top">
															<TABLE class="menuTable" id="Table1" borderColor="#ffffff" cellSpacing="3" cellPadding="0"
																width="100%" border="0" runat="server">
																<!--	<tr>  
																	<td width="1" bgColor="gainsboro" rowSpan="0"></TD>
																</tr> -->
																<tr>
																	<td style="HEIGHT: 6px"></td>
																</tr>
																<TR>
																	<td style="FONT-FAMILY: Arial; HEIGHT: 6px; COLOR: black; FONT-SIZE: 9pt; FONT-WEIGHT: bold">&nbsp;Бързи 
																		връзки за проекта</td>
																</TR>
																<tr>
																	<td style="PADDING-LEFT: 5px"><IMG style="WIDTH: 188px" height="1" alt="" src="/asa/images/menuline.bmp" width="188"></td>
																</tr>
																<TR>
																	<td><asp:linkbutton id="lkProfile" runat="server" cssclass="menuItem">Изходна проектна документация</asp:linkbutton></TD>
																</TR>
																<TR>
																	<td><asp:linkbutton id="lkSubpr" runat="server" CssClass="menuItem">Фази</asp:linkbutton></TD>
																</TR>
																<TR>
																	<td><asp:linkbutton id="lkSubcontracters" runat="server" CssClass="menuItem">Подизпълнители</asp:linkbutton></TD>
																</TR>
																<TR>
																	<td><asp:linkbutton id="Linkbutton1" runat="server" CssClass="menuItem">Авторски надзор</asp:linkbutton></TD>
																</TR>
																<TR>
																	<td><asp:linkbutton id="lbProjectContract" runat="server" Font-Bold="True" CssClass="menuItem">Договори</asp:linkbutton></TD>
																</TR>
																<TR>
																	<td><asp:linkbutton id="lbProjectBuilding" runat="server" Font-Bold="True" CssClass="menuItem">ОБЕКТ</asp:linkbutton></TD>
																</TR>
																<TR>
																	<td><asp:linkbutton id="lbMailing" runat="server" Font-Bold="True" CssClass="menuItem">Кореспонденция</asp:linkbutton><br>
																	</TD>
																</TR>
																<TR>
																	<td><asp:linkbutton id="lbHotIssues" runat="server" Font-Bold="True" CssClass="menuItem">Задачи</asp:linkbutton><br>
																	</TD>
																</TR>
																<tr>
																	<td style="HEIGHT: 6px"></td>
																</tr>
																<TR>
																	<td style="FONT-FAMILY: Arial; HEIGHT: 6px; COLOR: black; FONT-SIZE: 9pt; FONT-WEIGHT: bold">
																		&nbsp;<asp:Label ID="commonInfo" Runat="server"></asp:Label>
																	</td>
																</TR>
																<tr>
																	<td style="PADDING-LEFT: 5px"><IMG style="WIDTH: 188px" height="1" alt="" src="/asa/images/menuline.bmp" width="188"></td>
																</tr>
																<TR>
																	<td><asp:linkbutton id="lkFull" runat="server" CssClass="menuitem">Пълна документация</asp:linkbutton></TD>
																</TR>
																<TR>
																	<td><asp:linkbutton id="lbShortDocumentation" runat="server" CssClass="menuitem">Кратка документация</asp:linkbutton></TD>
																</TR>
																<TR>
																	<td><asp:linkbutton id="Linkbutton2" runat="server" CssClass="menuitem"> Заглавна страница</asp:linkbutton></TD>
																</TR>
																<TR>
																	<td><asp:linkbutton id="lkFullProjectInfo" runat="server" CssClass="menuitem">Информация за подизпълнители</asp:linkbutton></TD>
																</TR>
																<tr>
																	<td style="HEIGHT: 6px"></td>
																</tr>
																<TR>
																	<td style="FONT-FAMILY: Arial; HEIGHT: 6px; COLOR: black; FONT-SIZE: 9pt; FONT-WEIGHT: bold">&nbsp;Tехническа 
																		документация</td>
																</TR>
																<tr>
																	<td style="PADDING-LEFT: 5px"><IMG style="WIDTH: 188px" height="1" alt="" src="/asa/images/menuline.bmp" width="188"></td>
																</tr>
																<TR>
																	<td><asp:linkbutton id="btnContents" runat="server" CssClass="menuitem">Съдържание</asp:linkbutton>&nbsp;
																		<asp:imagebutton id="img" runat="server" ToolTip="Съдържание" ImageUrl="images/word.gif"></asp:imagebutton>&nbsp;
																		<asp:imagebutton id="btnContentExcel" runat="server" ToolTip="Съдържание" ImageUrl="images/excel_ico2.gif"></asp:imagebutton></TD>
																</TR>
																<TR>
																	<td><asp:linkbutton id="btnTech" runat="server" CssClass="menuitem" Width="152px">Техн.-икономически. показатели</asp:linkbutton><asp:imagebutton id="imgTechWord" runat="server" ToolTip="Съдържание" ImageUrl="images/word.gif"></asp:imagebutton><asp:imagebutton id="imgTechExcel" runat="server" ToolTip="Съдържание" ImageUrl="images/excel_ico2.gif"></asp:imagebutton></TD>
																</TR>
																<TR>
																	<td><asp:linkbutton id="lkMissingData" runat="server" CssClass="menuitem">Липсваща информация</asp:linkbutton></TD>
																</TR>
																<TR>
																	<td><A class="menuitem" onclick="Header()" href="#">Челен лист</A>
																	</TD>
																</TR>
																<TR>
																	<td><A class="menuitem" onclick="Fax()" href="#">Факс</A></TD>
																</TR>
																<TR>
																	<td><A runat="server" class="menuitem" onclick="Reg()" href="#" id="docReg">Договор за 
																			регистрация</A></TD>
																</TR>
																<TR>
																	<td><A class="menuitem" onclick="Print('udo')" href="#" runat="server" id="udReg">Удостоверение 
																			за регистрация</A></TD>
																</TR>
																<TR>
																	<td><A class="menuItem" onclick="Print('protokol')" href="#">Приемо-предавателен 
																			протокол</A>&nbsp;
																	</TD>
																</TR>
																<TR>
																	<td><asp:linkbutton id="lkProtPlus" runat="server" cssclass="menuitem">Приемо-предавателен 
																				протокол със съдържание</asp:linkbutton></TD>
																</TR>
																<TR>
																	<td><asp:linkbutton id="lkFolder1" runat="server" cssclass="menuitem">Печат за папки 1</asp:linkbutton></TD>
																</TR>
																<TR>
																	<td><asp:linkbutton id="lkFolder2" runat="server" cssclass="menuitem">Печат за папки 2</asp:linkbutton></TD>
																</TR>
																<TR>
																	<td class="menuitem">
																		<%= GetMails() %>
																	</TD>
																</TR>
																<tr>
																	<td><A class="menuitem" onclick="Print('Letter')" href="#">Писмо до фирма към обект</A>
																	</td>
																</tr>
															</TABLE>
														</td>
													</TR>
												</TABLE>
												<table id="tblP" runat="server">
													<TR>
														<td style="WIDTH: 175px" vAlign="top"><asp:label id="lb1" runat="server" CssClass="enterDataLabel" Height="24px">Процент:</asp:label><asp:label id="Label8" runat="server" CssClass="enterDataLabel" Height="26px"> Сума(EUR):</asp:label><asp:label id="Label9" runat="server" CssClass="enterDataLabel" Height="22px"> Платено:</asp:label><asp:label id="Label10" runat="server" CssClass="enterDataLabel" Height="22px">Дата на плащане:</asp:label><asp:label id="Label19" runat="server" CssClass="enterDataLabel" Height="24px">Забележка:</asp:label></TD>
														<td><asp:datalist id="dlPayments" runat="server" RepeatDirection="Horizontal">
																<ItemTemplate>
																	<asp:textbox id=txtPer runat="server" CssClass="enterDataBox" Width="100px" Height="21px" Text='<%# Asa.Timesheet.WebPages.UIHelpers.FormatDecimaln((decimal) DataBinder.Eval(Container, "DataItem.PaymentPercent")) %>' BackColor="#FFFF99">
																	</asp:textbox><BR>
																	<asp:textbox id=txtAmount runat="server" CssClass="enterDataBox" Width="100px" Height="20px" Text='<%# Asa.Timesheet.WebPages.UIHelpers.FormatDecimal2((decimal)DataBinder.Eval(Container, "DataItem.Amount")) %>'>
																	</asp:textbox><BR>
																	<asp:CheckBox id="ckPaid" runat="server" Text="ДА" Checked='<%# DataBinder.Eval(Container, "DataItem.Done") %>'>
																	</asp:CheckBox><BR>
																	<asp:textbox id=txtDate runat="server" CssClass="enterDataBox" Width="80px" Height="20px" Text='<%#  Asa.Timesheet.Data.Util.TimeHelper.FormatDate((DateTime)DataBinder.Eval(Container, "DataItem.PaymentDate")) %>'>
																	</asp:textbox><IMG id="lkCal" alt="Calendar" src="images/calendar.ico" width="18" align="absBottom"
																		runat="server"><br>
																	<asp:textbox id="txtNotes" runat="server" CssClass="enterDataBox" TextMode=MultiLine Width="100px" Height="60px" Text='<%#  DataBinder.Eval(Container, "DataItem.Notes") %>'>
																	</asp:textbox>
																</ItemTemplate>
															</asp:datalist><asp:button id="btnNewPayment" runat="server" CssClass="ActionButton" Width="144px"></asp:button>&nbsp;
															<asp:button id="btnExportPay" runat="server" CssClass="ActionButton" Width="80px" Text="Експорт"></asp:button></TD>
														<td></TD>
													</TR>
												</table>
												<table id="tblddlCP" cellSpacing="0" cellPadding="3" border="0" runat="server">
													<TR>
														<td style="WIDTH: 172px; HEIGHT: 23px"></TD>
														<td style="HEIGHT: 23px"><asp:label id="Label28" runat="server" CssClass="enterDataBox">Плащанията на всички клиети са </asp:label><asp:label id="lbAllClientPayments" runat="server"></asp:label><asp:button id="btnAllClientPaymentsCalc" runat="server" CssClass="ActionButton" Width="100px"
																Text="Преизчисли"></asp:button></TD>
													</TR>
													<TR>
														<td style="WIDTH: 172px; HEIGHT: 15px"><asp:label id="Label13" runat="server" Font-Bold="True" CssClass="enterDataLabel" Visible="False">Клиент: </asp:label></TD>
														<td style="HEIGHT: 15px"><asp:dropdownlist id="ddlClientsPayment" CssClass="enterDataBox" Width="350px" AutoPostBack="true"
																Visible="False" Runat="server"></asp:dropdownlist></TD>
													</TR>
												</table>
												<table id="tblCP" runat="server">
													<TR>
														<td style="WIDTH: 175px" vAlign="top"><asp:label id="lb1C" runat="server" CssClass="enterDataLabel" Height="24px">Процент:</asp:label><asp:label id="Label24" runat="server" CssClass="enterDataLabel" Height="26px"> Сума(EUR):</asp:label><asp:label id="Label25" runat="server" CssClass="enterDataLabel" Height="22px"> Платено:</asp:label><asp:label id="Label26" runat="server" CssClass="enterDataLabel" Height="22px">Дата на плащане:</asp:label><asp:label id="Label27" runat="server" CssClass="enterDataLabel" Height="24px">Забележка:</asp:label></TD>
														<td><asp:datalist id="dlPaymentsC" runat="server" RepeatDirection="Horizontal">
																<ItemTemplate>
																	<asp:textbox id="txtPerC" runat="server" CssClass="enterDataBox" Width="100px" Height="21px" Text='<%# Asa.Timesheet.WebPages.UIHelpers.FormatDecimaln((decimal) DataBinder.Eval(Container, "DataItem.PaymentPercent")) %>' BackColor="#FFFF99">
																	</asp:textbox><BR>
																	<asp:textbox id="txtAmountC" runat="server" CssClass="enterDataBox" Width="100px" Height="20px" Text='<%# Asa.Timesheet.WebPages.UIHelpers.FormatDecimal2((decimal)DataBinder.Eval(Container, "DataItem.Amount")) %>'>
																	</asp:textbox><BR>
																	<asp:CheckBox id="ckPaidC" runat="server" Text="ДА" Checked='<%# DataBinder.Eval(Container, "DataItem.Done") %>'>
																	</asp:CheckBox><BR>
																	<asp:textbox id="txtDateC" runat="server" CssClass="enterDataBox" Width="80px" Height="20px" Text='<%#  Asa.Timesheet.Data.Util.TimeHelper.FormatDate((DateTime)DataBinder.Eval(Container, "DataItem.PaymentDate")) %>'>
																	</asp:textbox><IMG id="lkCalC" alt="Calendar" src="images/calendar.ico" width="18" align="absBottom"
																		runat="server"><br>
																	<asp:textbox id="txtNotesC" runat="server" CssClass="enterDataBox" TextMode=MultiLine Width="100px" Height="60px" Text='<%#  DataBinder.Eval(Container, "DataItem.Notes") %>'>
																	</asp:textbox>
																</ItemTemplate>
															</asp:datalist><asp:button id="btnNewPaymentC" runat="server" CssClass="ActionButton" Width="144px"></asp:button>&nbsp;
															<asp:button id="btnExportPayC" runat="server" CssClass="ActionButton" Width="80px" Text="Експорт"></asp:button></TD>
														<td></TD>
													</TR>
												</table>
											</TD>
										</TR>
										<TR>
											<td style="HEIGHT: 5px" colSpan="2"></TD>
										</TR>
										<TR>
											<td colSpan="2"><IMG height="1" src="images/dot.gif" width="100%"></TD>
										</TR>
										<TR>
											<td colSpan="2" height="3"></td>
										</TR>
										<TR>
											<td style="WIDTH: 4px"></TD>
											<td>
												<TABLE id="Table5" cellSpacing="0" cellPadding="0" border="0">
													<TR>
														<td style="WIDTH: 184px" noWrap><asp:button id="btnSave" runat="server" CssClass="ActionButton" Text="Запиши"></asp:button><asp:button id="btnEdit" style="DISPLAY: none" runat="server" CssClass="ActionButton" Text="Редактирай"></asp:button></TD>
														<td style="WIDTH: 321px" noWrap><asp:button id="btnCancel" runat="server" CssClass="ActionButton" Width="168px" Text="Обратно към проекти"></asp:button>&nbsp;<INPUT class="ActionButton" id="btnPrint" onclick="Print1();" type="button" value="Печат"
																name="Button1" runat="server"></TD>
														<td><asp:button id="btnDelete" runat="server" CssClass="ActionButton" Text="Неактивен"></asp:button>&nbsp;
															<asp:button id="btnDelete1" runat="server" CssClass="ActionButton" Width="128px" Text="Изтрий напълно"></asp:button></TD>
													</TR>
												</TABLE>
											</TD>
										</TR>
										<TR>
											<td style="WIDTH: 4px" height="3"></TD>
										</TR>
										<TR>
											<td colSpan="2"><IMG height="1" src="images/dot.gif" width="100%"></TD>
										</TR>
									</TABLE>
									&nbsp;
									<asp:label id="lblInfo" runat="server" CssClass="InfoLabel" EnableViewState="False"></asp:label><asp:label id="lblError" runat="server" CssClass="ErrorLabel" EnableViewState="False" ForeColor="Red"></asp:label></td>
							</tr>
						</table>
					</td>
				</tr>
			</table>
			<radg:radgrid id="gridCalls" runat="server" CssClass="RadGrid" Width="100%" AutoGenerateColumns="False"
				GridLines="Horizontal">
				<PagerStyle CssClass="GridHeader" Mode="NumericPages"></PagerStyle>
				<ItemStyle HorizontalAlign="Center" CssClass="GridItem"></ItemStyle>
				<GroupPanel Visible="False"></GroupPanel>
				<HeaderStyle HorizontalAlign="Center" Wrap="False" CssClass="GridHeader"></HeaderStyle>
				<AlternatingItemStyle HorizontalAlign="Center" CssClass="GridItem"></AlternatingItemStyle>
				<GroupHeaderItemStyle BorderColor="Black" BackColor="Silver"></GroupHeaderItemStyle>
				<MasterTableView DataSourcePersistenceMode="NoPersistence" AllowCustomPaging="False" AllowSorting="True"
					PageSize="15" GridLines="Horizontal" AllowPaging="False" Visible="True">
					<Columns>
						<radg:GridBoundColumn UniqueName="PaymentPercent" HeaderButtonType="TextButton" HeaderText="Процент" DataField="PaymentPercent"
							DataFormatString="{0:n2}"></radg:GridBoundColumn>
						<radg:GridBoundColumn UniqueName="Amount" HeaderButtonType="TextButton" HeaderText="Сума(EUR)" DataField="Amount"
							DataFormatString="{0:n2}"></radg:GridBoundColumn>
						<radg:GridTemplateColumn UniqueName="Done" HeaderText="Платено">
							<ItemTemplate>
								<asp:label id="Label6" runat="server" Text='<%#  IsPaid((bool)DataBinder.Eval(Container, "DataItem.Done")) %>'>
								</asp:label>
							</ItemTemplate>
						</radg:GridTemplateColumn>
						<radg:GridTemplateColumn UniqueName="PaymentDate" HeaderText="Дата на плащане">
							<ItemTemplate>
								<asp:label id="Label15" runat="server" Text='<%#  Asa.Timesheet.Data.Util.TimeHelper.FormatDate((DateTime)DataBinder.Eval(Container, "DataItem.PaymentDate")) %>'>
								</asp:label>
							</ItemTemplate>
						</radg:GridTemplateColumn>
					</Columns>
					<RowIndicatorColumn Visible="False" UniqueName="RowIndicator">
						<HeaderStyle Width="20px"></HeaderStyle>
					</RowIndicatorColumn>
					<EditFormSettings>
						<EditColumn UniqueName="EditCommandColumn"></EditColumn>
					</EditFormSettings>
					<ExpandCollapseColumn ButtonType="ImageButton" Visible="False" UniqueName="ExpandColumn">
						<HeaderStyle Width="19px"></HeaderStyle>
					</ExpandCollapseColumn>
				</MasterTableView>
			</radg:radgrid></form>
	</body>
</HTML>
