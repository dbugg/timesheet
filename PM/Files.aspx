﻿<%@ Page Title="Проекти" Language="C#" MasterPageFile="~/PM.Master" AutoEventWireup="True" CodeBehind="Files.aspx.cs" Inherits="PM.Files" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
  
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
<table height="100%" cellSpacing="0" cellPadding="0" width="100%" border="0">
	<tr>
		<td style="PADDING-RIGHT: 5px; PADDING-LEFT: 5px; PADDING-TOP: 3px" vAlign="top" noWrap>
        <asp:linkbutton id="linkBack" Runat="server" CssClass="Brown" onclick="link_Click"></asp:linkbutton>

			<P><asp:label id="lblError" runat="server" CssClass="ErrorLabel" EnableViewState="False" ForeColor="Red"></asp:label><asp:label id="lblInfo" runat="server" CssClass="InfoLabel" EnableViewState="False"></asp:label><BR>
				<table id="tblForm" cellSpacing="0" cellPadding="3" width="646" border="0">
					<tr>
						<td style="WIDTH: 208px"><asp:label id="lbProjectName" runat="server" CssClass="enterDataLabel" EnableViewState="False"
								Font-Bold="True"></asp:label></td>
						<td style="WIDTH: 438px"><asp:label id="txtProjectName" runat="server" CssClass="enterDataBox" Width="350px"></asp:label></TD>
					</tr>
					<tr>
						<td style="WIDTH: 208px"><asp:label id="lbCategoryName" runat="server" CssClass="enterDataLabel" EnableViewState="False"
								Font-Bold="True"></asp:label></td>
						<td style="WIDTH: 438px"><asp:label id="lblCategoryName" runat="server" CssClass="enterDataBox" Width="350px"></asp:label><asp:textbox id="txtCategoryName" runat="server" CssClass="enterDataBox" Width="405px"></asp:textbox>&nbsp;<IMG id="img" height="16" alt="images/required1.gif" src="images/required1.gif" width="16"
								runat="server">
						</TD>
					</tr>
					<tr>
						<td style="WIDTH: 208px"><asp:label id="lbSubCategories" runat="server" CssClass="enterDataLabel" EnableViewState="False"
								Font-Bold="True"></asp:label></td>
						<td style="WIDTH: 438px"><asp:DropDownList id="ddlSubCategories" runat="server" CssClass="enterDataBox" Width="350px"></asp:DropDownList></TD>
					</tr>
                    
					<TR>
						<td >
							<asp:label id="lbVisibles" runat="server" CssClass="enterDataLabel" EnableViewState="True" Visible="false"
								Font-Bold="True"></asp:label></TD>
						<td>
							<asp:checkboxlist id="cblVisibles" runat="server" CssClass="enterDataBox" Width="350px" RepeatDirection="Horizontal"></asp:checkboxlist></TD>
					</TR>
				</table>
				<table id="tblForm1" cellSpacing="0" cellPadding="3" width="100%" border="0">
					<tr>
						<td><asp:button id="btnSave" runat="server" CssClass="ActionButton Default" onclick="btnSave_Click"></asp:button>&nbsp;<asp:button id="btnNew" runat="server" CssClass="ActionButton" onclick="btnNew_Click"></asp:button></td>
					</tr>
				</table>
				<asp:panel id="grid" 
					runat="server" Width="98%" Height="500px">
					<asp:datagrid id="grdFiles" runat="server" CssClass="Grid" Width="100%" AutoGenerateColumns="False"
						CellPadding="4" PageSize="2" AllowSorting="True">
						<ItemStyle CssClass="GridItem"></ItemStyle>
						<HeaderStyle Wrap="False" CssClass="GridHeader"></HeaderStyle>
						<Columns>
							<asp:TemplateColumn>
								<HeaderStyle Wrap="False" Width="80px"></HeaderStyle>
								<ItemStyle Wrap="False" HorizontalAlign="Center"></ItemStyle>
								<ItemTemplate>
									<asp:LinkButton id="btnEdit" runat="server" text='<%# getTextLink() %>' CommandName="editDB" Font-Size="8" ForeColor="#804000" Font-Underline="false" Font-Bold="true">
									</asp:LinkButton>
								</ItemTemplate>
							</asp:TemplateColumn>
							<asp:TemplateColumn SortExpression="FileNameSave">
								<ItemStyle Width="40%"></ItemStyle>
								<ItemTemplate>
									<asp:LinkButton id="lnk" runat="server" CssClass="menuTable" ForeColor="#804000" CommandName="Edit">
										<span Class="menuTable">
											<%# DataBinder.Eval(Container, "DataItem.FileNameSave") %>
											<%# DataBinder.Eval(Container, "DataItem.FileType") %>
										</span>
									</asp:LinkButton>
								</ItemTemplate>
							</asp:TemplateColumn>
							<asp:BoundColumn Visible="False" DataField="FileNameSave"></asp:BoundColumn>
							<asp:BoundColumn DataField="FileSubject" SortExpression="FileSubject"></asp:BoundColumn>
							<asp:BoundColumn DataField="FileVersion" SortExpression="FileVersion">
								<HeaderStyle Width="45px"></HeaderStyle>
							</asp:BoundColumn>
							<asp:BoundColumn Visible="False" DataField="FileType"></asp:BoundColumn>
							<asp:BoundColumn DataField="CreatedBy" SortExpression="CreatedBy"></asp:BoundColumn>
							<asp:BoundColumn DataField="DateCreated" SortExpression="DateCreated" DataFormatString="{0:dd.MM.yyyy}"></asp:BoundColumn>
							<asp:TemplateColumn>
								<HeaderStyle Wrap="False" Width="20px"></HeaderStyle>
								<ItemStyle Wrap="False" HorizontalAlign="Center"></ItemStyle>
								<ItemTemplate>
									<asp:ImageButton id="btnDelete" runat="server" Width="16px" Height="16px" ToolTip="Изтрий" ImageUrl="images/delete.gif"></asp:ImageButton>
								</ItemTemplate>
							</asp:TemplateColumn>
                                                    
                            <asp:TemplateColumn>
								<HeaderStyle Wrap="False" Width="20px"></HeaderStyle>
								<ItemStyle Wrap="False" HorizontalAlign="Center"></ItemStyle>
								<ItemTemplate>
									<%#  getDownloaded((int)DataBinder.Eval(Container, "DataItem.FileID")).ToString() %>
								</ItemTemplate>
							</asp:TemplateColumn>
						</Columns>
					</asp:datagrid>
				</asp:panel></P>
			<table>
				<tr>
					<td>
						<%= GetPDFdownload() %>
					</td>
				</tr>
			</table>
		</td>
	</tr>
</table>
</asp:Content>