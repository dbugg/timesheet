﻿<%@ Page language="c#" Codebehind="Corespondency.aspx.cs" AutoEventWireup="false" Inherits="Asa.Timesheet.WebPages.Corespondency" %>
<%@ Register TagPrefix="uc1" TagName="PageHeader" Src="UserControls/PageHeader.ascx" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
	<head runat="server">
		<title>Corespondency</title>
		<meta name="GENERATOR" Content="Microsoft Visual Studio .NET 7.1">
		<meta name="CODE_LANGUAGE" Content="C#">
		<meta name="vs_defaultClientScript" content="JavaScript">
		<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5">
		<LINK href="styles/timesheet.css" type="text/css" rel="stylesheet">
		<script language="javascript" src="PopupCalendar.js"></script>
	</HEAD>
	<body bottomMargin="0" leftMargin="0" topMargin="0" rightMargin="0">
		<form id="Form1" method="post" runat="server">
			<table height="100%" cellSpacing="0" cellPadding="0" width="100%" border="0">
				<tr height="1">
					<td>
						<uc1:PageHeader id="header" runat="server"></uc1:PageHeader>
					</td>
				</tr>
				<tr>
					<td>
						<table height="100%" cellSpacing="0" cellPadding="0" width="100%" border="0">
							<tr>
								<td class="menuLeft"><asp:placeholder id="menuHolder" runat="server"></asp:placeholder></td>
								
								<td style="PADDING-RIGHT: 5px; PADDING-LEFT: 5px; PADDING-TOP: 3px" vAlign="top" noWrap><br>
									<asp:label id="lblError" runat="server" ForeColor="Red" Visible="False" EnableViewState="False"
										CssClass="ErrorLabel"></asp:label><asp:label id="lblInfo" runat="server" Visible="False" EnableViewState="False" CssClass="InfoLabel"></asp:label><br>
									<TABLE id="Table1" style="WIDTH: 848px; HEIGHT: 26px" cellSpacing="1" cellPadding="1" width="848"
										border="0">
										<TR>
											<td style="WIDTH: 156px" vAlign="top"><asp:label id="Label4" runat="server" CssClass="enterDataLabel" Width="100%">Период:</asp:label></TD>
											<td style="WIDTH: 252px" vAlign="top" noWrap><asp:textbox id="txtStartDate" runat="server" CssClass="enterDataBox" Width="88px" MaxLength="10"></asp:textbox>&nbsp;<IMG id="lkCalendar1" alt="Calendar" src="images/calendar.ico" width="18" align="absBottom"
													runat="server"> до&nbsp;
												<asp:textbox id="txtEndDate" runat="server" CssClass="enterDataBox" Width="88px" MaxLength="10"></asp:textbox>&nbsp;<IMG id="lkCalendar2" alt="Calendar" src="images/calendar.ico" width="18" align="absBottom"
													runat="server"></TD>
											<td>
											</TD>
											<td></TD>
										</TR>
										<TR>
											<td vAlign="top" style="WIDTH: 156px"><asp:label id="lblProject" runat="server" CssClass="enterDataLabel" Width="100%">Тип:</asp:label></TD>
											<td style="WIDTH: 252px" vAlign="top">
												<asp:dropdownlist id="ddlProjectsStatus" runat="server" CssClass="EnterDataBox" Width="250px" AutoPostBack="True"
													></asp:dropdownlist></TD>
											<td></TD>
											<td></TD>
										</TR>
										<TR>
											<td style="WIDTH: 156px" vAlign="top">
												<asp:label id="Label1" runat="server" CssClass="enterDataLabel" Width="100%">Сграда:</asp:label></TD>
											<td style="WIDTH: 252px" vAlign="top">
												<asp:dropdownlist id="ddlBuildingTypes" runat="server" CssClass="enterDataBox" Width="250px" AutoPostBack="True"></asp:dropdownlist></TD>
											<td></TD>
											<td></TD>
										</TR>
										<TR>
											<td style="WIDTH: 156px" vAlign="top">
												<asp:label id="Label2" runat="server" CssClass="enterDataLabel" Width="100%">Проект:</asp:label></TD>
											<td style="WIDTH: 252px" vAlign="top">
												<asp:dropdownlist id="ddlProject" runat="server" CssClass="EnterDataBox" Width="250px"></asp:dropdownlist></TD>
											<td></TD>
											<td></TD>
										</TR>
										<TR>
											<td vAlign="top" style="WIDTH: 156px"><asp:label id="lbClient" runat="server" CssClass="enterDataLabel" Width="100%">Клиент:</asp:label></TD>
											<td style="WIDTH: 252px" vAlign="top"><asp:dropdownlist id="ddlClients" runat="server" CssClass="EnterDataBox" Width="250px"></asp:dropdownlist></TD>
											<td></TD>
											<td></TD>
										</TR>
										<TR>
											<td style="WIDTH: 156px" vAlign="top"><asp:label id="lbSendReseve" runat="server" CssClass="enterDataLabel" Width="100%">Получени/Изпратени:</asp:label></TD>
											<td style="WIDTH: 252px" vAlign="top"><asp:DropDownList id="ddlSendReseve" runat="server" CssClass="EnterDataBox" Width="250px"></asp:DropDownList></TD>
											<td></TD>
											<td></TD>
										</TR>
										<TR>
											<td style="WIDTH: 156px" vAlign="top"></TD>
											<td style="WIDTH: 252px" vAlign="top"><asp:button id="btnSearch" runat="server" CssClass="ActionButton" Text="Филтър"></asp:button></TD>
											<td></TD>
											<td></TD>
										</TR>
									</TABLE>
									<BR>
									<asp:panel id="grid" 
										runat="server" Width="98%" Height="450px">
										<asp:datagrid id="grdMain" runat="server" CssClass="Grid" Width="100%" AllowSorting="True" PageSize="2"
											CellPadding="4" AutoGenerateColumns="False">
											<ItemStyle CssClass="GridItem"></ItemStyle>
											<HeaderStyle Wrap="False" CssClass="GridHeader"></HeaderStyle>
											<Columns>
												<asp:BoundColumn DataField="MailingNumber" HeaderText="Входящ номер" SortExpression="MailingNumber">
													<HeaderStyle Wrap="False" Width="70px"></HeaderStyle>
													<ItemStyle ForeColor="DimGray"></ItemStyle>
												</asp:BoundColumn>
												<asp:TemplateColumn HeaderText="Получени/Изпратени" SortExpression="Send">
													<HeaderStyle Wrap="False" Width="30px"></HeaderStyle>
													<ItemStyle ForeColor="DimGray"></ItemStyle>
													<ItemTemplate>
														<asp:LinkButton id="lnk" runat="server" CssClass="menuTable"  CommandName="Edit">
															<span Class="menuTable">
																<%# GetSendReseve(DataBinder.Eval(Container, "DataItem.Send"))%>
															</span>
														</asp:LinkButton>
													</ItemTemplate>
												</asp:TemplateColumn>
												<asp:BoundColumn DataField="Send" Visible="False">
													<HeaderStyle Wrap="False" Width="30px"></HeaderStyle>
													<ItemStyle ForeColor="DimGray"></ItemStyle>
												</asp:BoundColumn>
												<asp:BoundColumn DataField="MailingDate" HeaderText="Дата" DataFormatString="{0:dd.MM.yyyy}" SortExpression="MailingDate">
													<HeaderStyle Wrap="False" Width="70px"></HeaderStyle>
													<ItemStyle ForeColor="DimGray"></ItemStyle>
												</asp:BoundColumn>
												<asp:BoundColumn DataField="ProjectName" HeaderText="Проект" SortExpression="ProjectName">
													<ItemStyle Width="20%"></ItemStyle>
												</asp:BoundColumn>
												<asp:BoundColumn DataField="Person" HeaderText="Изпратено до/Получено от" SortExpression="Person">
													<ItemStyle Width="20%"></ItemStyle>
												</asp:BoundColumn>
												<asp:BoundColumn DataField="MailingTypes" HeaderText="Tип" SortExpression="MailingTypes">
													<HeaderStyle Wrap="False" Width="70px"></HeaderStyle>
													<ItemStyle ForeColor="DimGray"></ItemStyle>
												</asp:BoundColumn>
												<asp:BoundColumn DataField="Notes" HeaderText="Забележки" SortExpression="Notes">
													<HeaderStyle Wrap="False" Width="70px"></HeaderStyle>
													<ItemStyle ForeColor="DimGray"></ItemStyle>
												</asp:BoundColumn>
												<asp:TemplateColumn Visible="False">
													<HeaderStyle Wrap="False" Width="20px"></HeaderStyle>
													<ItemStyle Wrap="False" HorizontalAlign="Center"></ItemStyle>
													<ItemTemplate>
														<asp:HyperLink id="btnScan" runat="server" ToolTip="Сканиран документ" NavigateUrl='<%# GetURL((int)DataBinder.Eval(Container, "DataItem.MailingID"))%>' Target=_blank>
															<img border="0" src="images/pdf.gif" /></asp:HyperLink>
													</ItemTemplate>
												</asp:TemplateColumn>
												<asp:TemplateColumn>
													<HeaderStyle Wrap="False" Width="50px"></HeaderStyle>
													<ItemStyle Wrap="False" HorizontalAlign="Center"></ItemStyle>
													<ItemTemplate>
														<asp:ImageButton id="btnFile" runat="server" Width="12px" Height="12px" ToolTip="Изтегли" ImageUrl="images/pdf.gif" Visible='<%# GetVisible(DataBinder.Eval(Container, "DataItem.HasScan"))%>'>
														</asp:ImageButton>
													</ItemTemplate>
												</asp:TemplateColumn>
												<asp:TemplateColumn>
													<HeaderStyle Wrap="False" Width="80px"></HeaderStyle>
													<ItemStyle Wrap="False" HorizontalAlign="Center"></ItemStyle>
													<ItemTemplate>
														<asp:ImageButton id="btnDelete" runat="server" Width="12px" Height="12px" ToolTip="Изтрий" ImageUrl="images/delete.gif"></asp:ImageButton>
													</ItemTemplate>
												</asp:TemplateColumn>
												<asp:BoundColumn DataField="ext" Visible="False">
													<HeaderStyle Wrap="False" Width="70px"></HeaderStyle>
													<ItemStyle ForeColor="DimGray"></ItemStyle>
												</asp:BoundColumn>
											</Columns>
										</asp:datagrid>
									</asp:panel>
									<TABLE id="Table4"
										cellSpacing="0" cellPadding="4" width="100%" border="0">
										<TBODY>
											<TR>
												<td><asp:button id="btnNew" runat="server" CssClass="ActionButton" Text="Новa кореспонденция" Width="165px"></asp:button>&nbsp;
													<asp:button id="btnExport" runat="server" CssClass="ActionButton" Text="Експорт" Visible="False"></asp:button>&nbsp;
													<asp:button id="btnBackToProject" runat="server" CssClass="ActionButton" Text="Връщане към проекта"
														Width="165px"></asp:button>&nbsp;</TD>
											</TR>
										</TBODY>
									</TABLE>
								</td>
							</tr>
						</table>
					</td>
				</tr>
			</table>
		</form>
	</body>
</HTML>
