using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Data.SqlClient;
using log4net;
using Asa.Timesheet.Data;
using Asa.Timesheet.Data.Entities;
using Asa.Timesheet.Data.Util;
using GrapeCity.ActiveReports.Export.Pdf.Section;
using GrapeCity.ActiveReports.Export.Excel.Section;
using Asa.Timesheet.WebPages.Reports;

using System.Collections.Specialized;
using System.IO;

namespace Asa.Timesheet.WebPages
{
	/// <summary>
	/// Summary description for Protokoly.
	/// </summary>
	public class EditProtokols : TimesheetPageBase
	{
		#region WebControls
		private static readonly ILog log = LogManager.GetLogger(typeof(EditProtokols));
		protected System.Web.UI.WebControls.PlaceHolder menuHolder;
		protected System.Web.UI.WebControls.Label Label8;
		protected System.Web.UI.WebControls.DropDownList ddlBuildingTypes;
		protected System.Web.UI.WebControls.DropDownList ddlProjectsStatus;
		protected System.Web.UI.WebControls.Label lblName;
		protected System.Web.UI.WebControls.DropDownList ddlProject;
		protected System.Web.UI.WebControls.Label lbProjects;
		protected System.Web.UI.WebControls.Label lbDate;
		protected System.Web.UI.WebControls.TextBox txtStartDate;
		protected System.Web.UI.WebControls.Label Label9;
		protected System.Web.UI.WebControls.TextBox txtNotes;
		protected System.Web.UI.WebControls.Label Label6;
		protected System.Web.UI.WebControls.Button btnScanDocAdd;
		protected System.Web.UI.WebControls.Button btnDelete;
		protected System.Web.UI.WebControls.Label lblInfo;
		protected System.Web.UI.WebControls.Label lblError;
		protected System.Web.UI.HtmlControls.HtmlImage lkCalendar1;
		protected System.Web.UI.HtmlControls.HtmlInputFile FileCtrl;
		protected Asa.Timesheet.WebPages.UserControls.PageHeader header;
		protected System.Web.UI.WebControls.Label lbScanDocNew;
		protected System.Web.UI.WebControls.DataList dlDocs;
		protected System.Web.UI.WebControls.Label Label1;
		protected System.Web.UI.WebControls.Button btnSaveUP;
		protected System.Web.UI.WebControls.Button btnEditUP;
		protected System.Web.UI.WebControls.Button btnCancelUP;
		protected System.Web.UI.WebControls.Button btnSave;
		protected System.Web.UI.WebControls.Button btnEdit;
		protected System.Web.UI.WebControls.Button btnCancel;
		protected System.Web.UI.WebControls.Label Label2;
		protected System.Web.UI.WebControls.RadioButtonList rbb;
		protected Asa.Timesheet.WebPages.UserControls.EditForm editCtrl;
		#endregion
	
		#region Page_Load

        private void Page_Load(object sender, System.EventArgs e)
        {

            lkCalendar1.Attributes["onclick"] = TimeHelper.InvokePopupCal(txtStartDate);
            if (!this.IsPostBack)
            {
                if (Page.IsPostBack == false)
                {
                    // Set up form for data change checking when
                    // first loaded.
                    this.CheckForDataChanges = true;
                    this.BypassPromptIds =
                        new string[] { "dlDocs+", "ibPdfExport", "btnDel", "btnScanDocAdd", "btnSave", "btnSaveUP", "btnEdit", "btnEditUP", "btnDelete", "ddlProject", "ddlBuildingTypes", "ddlProjectsStatus" };
                }
                if (UIHelpers.GetIDParam() <= 0)
                {
                    //ddlProjectsStatus.SelectedIndex=1;
                    header.PageTitle = Resource.ResourceManager["newProtocol_PageTitle"];
                }
                else
                    header.PageTitle = Resource.ResourceManager["editProtocol_PageTitle"];

                header.UserName = this.LoggedUser.UserName;
                int statusIndex = (UIHelpers.GetIDParam() > 0) ? (int)ProjectsData.ProjectsByStatus.AllProjects : (int)ProjectsData.ProjectsByStatus.Active;
                UIHelpers.LoadProjectStatus(ddlProjectsStatus, statusIndex);
                UIHelpers.LoadBuildingTypes("", ddlBuildingTypes);
                LoadProjects();
                InitEdit();
                if (UIHelpers.GetIDParam() > 0)
                {
                    LoadFromID(UIHelpers.GetIDParam());
                    BindGrid();
                }
                else
                {
                    btnSave.Text = Resource.ResourceManager["btnNewMeeting"];
                    btnSaveUP.Text = Resource.ResourceManager["btnNewMeeting"];
                    btnScanDocAdd.Visible = false;
                }

            }
            UIHelpers.CreateMenu(menuHolder, LoggedUser);
        }
        private void LoadFromID(int ID)
        {
            ProtokolData pd = ProtokolDAL.Load(ID);
            if (pd == null)
                ErrorRedirect("Error load meeting.");
            ddlProject.SelectedValue = pd.ProjectID.ToString();
            txtStartDate.Text = TimeHelper.FormatDate(pd.ProtokolDate);
            txtNotes.Text = pd.Notes;
            rbb.SelectedIndex = pd.Accepted;


        }
	
		#endregion

		public ProjectsData.ProjectsByStatus ProjectsStatus
		{
			get
			{
				try
				{
					return  (ProjectsData.ProjectsByStatus)int.Parse(ddlProjectsStatus.SelectedValue);
				}
				catch { return ProjectsData.ProjectsByStatus.AllProjects; }
			}
		}


        private void InitEdit()
        {
            if (UIHelpers.GetIDParam() <= 0) return;

            string vmButtons = this.btnEditUP.ClientID + ";" + this.btnEdit.ClientID;
            string emButtons = this.btnSave.ClientID + ";" + this.btnSaveUP.ClientID + ";" + this.btnDelete.ClientID;

            string exl = string.Empty;

            editCtrl.InitEdit("tblForm", vmButtons, emButtons, exl);
        }
	
	
		#region BindGrid
        private void BindGrid()
        {
            int ID = UIHelpers.GetIDParam();
            ProjectProtocolsVector ppv = ProjectProtokolDAL.LoadCollection("ProjectProtokolsSelByProtocolProc", SQLParms.CreateProjectProtokolsSelByProtocolProc(ID));
            if (!(ppv.Count > 0))
            {
                ProtokolData pd = ProtokolDAL.Load(ID);
                pd.HasScan = false;
                ProtokolDAL.Save(pd);
            }
            dlDocs.DataSource = ppv;
            dlDocs.DataKeyField = "ProjectProtokolID";
            dlDocs.DataBind();

            //			if (!LoggedUser.HasPaymentRights)
            ////				grdMeetings.Columns[(int)GridColumns.DeleteItem].Visible = false;
            //				dlDocs.GridLines[GridLines.Horizontal].Visible = false;
            try
            {
                for (int i = 0; i < dlDocs.Items.Count; i++)
                {
                    string js = string.Concat("return confirm('", Resource.ResourceManager["grid_ConfirmDeleteForProtokols"], " ?');");
                    System.Web.UI.WebControls.WebControl btn = (System.Web.UI.WebControls.WebControl)dlDocs.Items[i].FindControl("btnDel");
                    btn.Attributes["onclick"] = js;
                }
            }
            catch
            {

            }

        }

        private bool LoadProjects()
        {
            return UIHelpers.LoadProjects(ddlProject, ddlProjectsStatus, ddlBuildingTypes);
            //			SqlDataReader reader = null;
            //			
            //			try
            //			{
            //				reader = ProjectsData.SelectProjectNamesClear(this.ProjectsStatus, int.Parse(ddlBuildingTypes.SelectedValue));
            //			
            //				ddlProject.DataSource = reader;
            //				ddlProject.DataValueField = "ProjectID";
            //				ddlProject.DataTextField = "ProjectName";
            //				ddlProject.DataBind();
            //
            //				ddlProject.Items.Insert(0, new ListItem("<"+Resource.ResourceManager["reports_ddlAllProjects"]+">", "-1"));
            //				ddlProject.SelectedValue = "-1";
            //			}
            //			catch (Exception ex)
            //			{
            //				log.Error(ex);
            //				return false;
            //			}
            //
            //			finally
            //			{
            //				if (reader!=null) reader.Close();
            //			}
            //			return true;
        }

		#endregion
	
		#region Save Pdf documents
        public void SaveMeetingDocuments()
        {
            int ID = UIHelpers.GetIDParam();
            if (FileCtrl.PostedFile.FileName.Length != 0)
            {
                string name = FileCtrl.PostedFile.FileName;
                int n = name.LastIndexOf(".");
                if (n != -1)
                {
                    string ext = name.Substring(n);
                    if (ext == System.Configuration.ConfigurationManager.AppSettings["Extension"])
                    {
                        ProjectProtokolData mdd = new ProjectProtokolData(-1, ID, false);
                        ProjectProtokolDAL.Save(mdd);
                        log.Info(string.Format("ScanDoc {0} of Protocol {2} has been INSERTED by {1}", mdd.ProjectProtokolID, string.Concat(LoggedUser.FullName, " ( userID ", LoggedUser.UserID, " )"), ID));
                        string path = string.Concat(System.Configuration.ConfigurationManager.AppSettings["ScanPathProtocol"], mdd.ProjectProtokolID, ext);
                        string mapped = Request.MapPath(path);
                        FileCtrl.PostedFile.SaveAs(mapped);
                        ProtokolData md = ProtokolDAL.Load(ID);
                        md.HasScan = true;
                        ProtokolDAL.Save(md);
                        string sc = string.Empty;//new StringCollection();
                        SqlDataReader reader = UsersData.SelectUsersMainAccMailListProc();
                        while (reader.Read())
                        {
                            string email = reader.GetString(1);
                            string emailName = reader.GetString(0);
                            //					sc.Add(String.Concat(email, ":", emailName)); 
                            sc = string.Concat(sc, ";", email);
                        }
                        sc = sc.Substring(1);
                       // MailBO.SendMailOfficer(sc, ddlProject.SelectedItem.Text, txtStartDate.Text, mapped, true, rbb.SelectedItem.Value);
                        UserInfo Director = UsersData.UsersListByRoleProc(1);

                        MailBO.SendMailMinutes(Director.Mail, mapped, ddlProject.SelectedItem.Text, txtStartDate.Text, rbb.SelectedItem.Value);
                    }
                }
            }
        }
        protected string GetURL(int ID)
        {
            return string.Concat(System.Configuration.ConfigurationManager.AppSettings["ScanPathProtocol"], ID, System.Configuration.ConfigurationManager.AppSettings["Extension"]);
        }

        protected bool GetVis()
        {
            if (LoggedUser.HasPaymentRights || LoggedUser.IsAssistant || LoggedUser.IsSecretary)
                return true;
            else
                return false;
        }
			
		#endregion
	
		#region Web Form Designer generated code
        override protected void OnInit(EventArgs e)
        {
            //
            // CODEGEN: This call is required by the ASP.NET Web Form Designer.
            //
            InitializeComponent();
            base.OnInit(e);
        }
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
        private void InitializeComponent()
        {
            this.btnSaveUP.Click += new System.EventHandler(this.btnSave_Click);
            this.btnCancelUP.Click += new System.EventHandler(this.btnCancel_Click);
            this.ddlProjectsStatus.SelectedIndexChanged += new System.EventHandler(this.ddlProjectsStatus_SelectedIndexChanged);
            this.ddlBuildingTypes.SelectedIndexChanged += new System.EventHandler(this.ddlProject_SelectedIndexChanged);
            this.ddlProject.SelectedIndexChanged += new System.EventHandler(this.ddlProject_SelectedIndexChanged);
            this.btnScanDocAdd.Click += new System.EventHandler(this.btnScanDocAdd_Click);
            this.dlDocs.ItemCommand += new System.Web.UI.WebControls.DataListCommandEventHandler(this.dlDocs_ItemCommand);
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            this.Load += new System.EventHandler(this.Page_Load);

        }
		#endregion
	
		#region Event handlers
        private void ddlProjectsStatus_SelectedIndexChanged(object sender, System.EventArgs e)
        {
            LoadProjects();
        }

        private void ddlBuildingTypes_SelectedIndexChanged(object sender, System.EventArgs e)
        {
            LoadProjects();
        }

        private void btnSave_Click(object sender, System.EventArgs e)
        {
            if (ddlProject.SelectedIndex == 0)
            {
                AlertFieldNotEntered(lblName);
                Dirty = true;
                return;
            }
            if (txtStartDate.Text.Trim() == "" || UIHelpers.GetDate(txtStartDate) == Constants.DateMax)
            {
                AlertFieldNotEntered(lbDate);
                Dirty = true;
                return;
            }
            int ID = UIHelpers.GetIDParam();
            bool bHasScanBefore = false;

            DateTime dtMeet = UIHelpers.GetDate(txtStartDate);
            if (ID > 0)
            {
                ProtokolData mdd = ProtokolDAL.Load(ID);
                bHasScanBefore = mdd.HasScan;

            }
            else
            {


            }
            bool InsertOrUpdate = (ID < 1);
            ProtokolData md = new ProtokolData(ID, dtMeet, int.Parse(ddlProject.SelectedValue), LoggedUser.UserID, bHasScanBefore, txtNotes.Text);
            md.Accepted = rbb.SelectedIndex;
            ProtokolDAL.Save(md);
            if (InsertOrUpdate)
                log.Info(string.Format("Protocol {0} has been INSERTED by {1}", md.ProtokolID, string.Concat(LoggedUser.FullName, " ( userID ", LoggedUser.UserID, " )")));
            else
                log.Info(string.Format("Protocol {0} has been UPDATED by {1}", md.ProtokolID, string.Concat(LoggedUser.FullName, " ( userID ", LoggedUser.UserID, " )")));
            if (FileCtrl.PostedFile.FileName.Length != 0)
            {
                string name = FileCtrl.PostedFile.FileName;
                int n = name.LastIndexOf(".");
                if (n != -1)
                {
                    string ext = name.Substring(n);
                    if (ext == System.Configuration.ConfigurationManager.AppSettings["Extension"])
                    {
                        ProjectProtokolData mdd = new ProjectProtokolData(-1, md.ProtokolID, false);
                        ProjectProtokolDAL.Save(mdd);
                        log.Info(string.Format("ScanDoc {0} of Protocol {2} has been INSERTED by {1}", mdd.ProjectProtokolID, string.Concat(LoggedUser.FullName, " ( userID ", LoggedUser.UserID, " )"), md.ProtokolID));
                        string path = string.Concat(System.Configuration.ConfigurationManager.AppSettings["ScanPathProtocol"], mdd.ProjectProtokolID, ext);
                        string mapped = Request.MapPath(path);
                        FileCtrl.PostedFile.SaveAs(mapped);
                        md.HasScan = true;
                        ProtokolDAL.Save(md);
                        string sc = string.Empty;//new StringCollection();
                        SqlDataReader reader = UsersData.SelectUsersMainAccMailListProc();
                        while (reader.Read())
                        {
                            string email = reader.GetString(1);
                            string emailName = reader.GetString(0);
                            //					sc.Add(String.Concat(email, ":", emailName)); 
                            sc = string.Concat(sc, ";", email);
                        }
                        if (sc.Length > 0)
                            sc = sc.Substring(1);

                        MailBO.SendMailOfficer(sc, ddlProject.SelectedItem.Text, txtStartDate.Text, mapped, true, rbb.SelectedItem.Text);
                        UserInfo Director = UsersData.UsersListByRoleProc(1);

                        MailBO.SendMailMinutes(Director.Mail, mapped, ddlProject.SelectedItem.Text, txtStartDate.Text, rbb.SelectedItem.Text);
                    }
                }
            }
            Response.Redirect("Protokols.aspx");
        }

        private void btnCancel_Click(object sender, System.EventArgs e)
        {
            Response.Redirect("Protokols.aspx");
        }

        private void ddlProject_SelectedIndexChanged(object sender, System.EventArgs e)
        {

        }
        private void dlDocs_ItemCommand(object source, System.Web.UI.WebControls.DataListCommandEventArgs e)
        {
            ImageButton ib = (ImageButton)e.CommandSource;
            string s = ib.ID;
            switch (ib.ID)
            {
                case "btnDel":
                    int ID = (int)dlDocs.DataKeys[e.Item.ItemIndex];
                    ProjectProtokolData mdd = ProjectProtokolDAL.Load(ID);
                    mdd.IsDeleted = true;
                    ProjectProtokolDAL.Save(mdd);
                    log.Info(string.Format("ScanDoc {0} of Protocol {2} has been DELETED by {1}", mdd.ProjectProtokolID, string.Concat(LoggedUser.FullName, " ( userID ", LoggedUser.UserID, " )"), UIHelpers.GetIDParam()));
                    BindGrid();
                    break;
            }
        }

        private void btnScanDocAdd_Click(object sender, System.EventArgs e)
        {
            SaveMeetingDocuments();
            BindGrid();
        }
	
		#endregion
	
	}
}
