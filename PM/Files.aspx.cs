using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using Asa.Timesheet.Data;
using Asa.Timesheet.Data.Entities;
using System.Data.SqlClient;
using System.IO;
using System.Configuration;
using log4net;
using log4net.Config;
using System.Collections.Generic;

namespace PM
{
	/// <summary>
	/// Summary description for Files.
	/// </summary>
	public partial class Files : BasePage
	{
		#region WebControls
	
		protected System.Web.UI.WebControls.HyperLink Hyperlink1;
		protected PM.UserControls.PageHeader header;
		private static readonly ILog log = LogManager.GetLogger(typeof(Files));
		
		#endregion

		#region enums

		private enum gridColomns
		{
			Edit= 0,
			FileName,
			FileNameUnvisinle,
			Subject,
			//Minutes,
			Version,
			Type,
			Creator,
			Date,
			Delete,
            Downloaded
		}


		#endregion

		#region Page_Load
        List<int> _downloaded = new List<int>();

        protected void Page_Load(object sender, System.EventArgs e)
        {
            if ((!LoggedUser.IsAuthenticated) || (!UIHelpers.HasAccess(LoggedUser, UIHelpers.GetPIDParam())) || (!UIHelpers.HasAccessToCategory(LoggedUser, UIHelpers.GetCIDParam())))
                ErrorRedirect(UIHelpers.GetText(Language, "noRightsAlert"));
            if (UIHelpers.GetPIDParam() == -1) ErrorRedirect(UIHelpers.GetText(Language, "noRightsAlert"));

            LoadResourses();
            bool b = (LoggedUser.UserRoleID > 0);//== (int)Roles.Director);
            

            if (!this.IsPostBack)
            {

                //TODO


                int CategoryID = UIHelpers.GetCIDParam();
                _downloaded = ProjectLogDAL.LoadDonwloaded(CategoryID);
                if (CategoryID > 0)
                {
                    if (!LoadCategory(CategoryID)) { lblError.Text = UIHelpers.GetText(Language, "ErrorLoadCategories"); return; }
                    BindGrid();
                    grid.Visible = true;
                    ddlSubCategories.Visible = false;
                    lbSubCategories.Visible = false;
                    if (!(LoggedUser.IsUser || LoggedUser.IsSubcontracter || LoggedUser.IsClient))
                    {
                        grdFiles.Columns[(int)gridColomns.Edit].Visible = false;
                        //grdFiles.Columns[(int)gridColomns.Creator].Visible  = false;
                        grdFiles.Columns[(int)gridColomns.Delete].Visible = false;
                        //grdFiles.Columns[(int)gridColomns.Subject].Visible  = false;
                        //grdFiles.Columns[(int)gridColomns.Type].Visible  = false;
                        //grdFiles.Columns[(int)gridColomns.Minutes].Visible  = false;


                        btnNew.Visible = false;
                    }
                    if (!(LoggedUser.IsUser))
                    {
                        grdFiles.Columns[(int)gridColomns.Date].Visible = false;
                    }
                    if (LoggedUser.IsSubcontracter)
                    {
                        ProjectCategoryData datcategory = ProjectCategoryDAL.Load(CategoryID);
                        grdFiles.Columns[(int)gridColomns.Downloaded].Visible = false;
                        if (datcategory.SubcontracterID != LoggedUser.UserID)
                        {
                            btnNew.Visible = false;
                            grdFiles.Columns[(int)gridColomns.Edit].Visible = false;
                            grdFiles.Columns[(int)gridColomns.Delete].Visible = false;

                        }
                    }
                    if (LoggedUser.IsClient || LoggedUser.IsBuilder)
                    {
                        ProjectCategoryData datcategory = ProjectCategoryDAL.Load(CategoryID);
                        if (datcategory.ClientID != LoggedUser.UserID)
                        {
                            btnNew.Visible = false;
                            grdFiles.Columns[(int)gridColomns.Edit].Visible = false;
                            grdFiles.Columns[(int)gridColomns.Delete].Visible = false;

                        }
                        grdFiles.Columns[(int)gridColomns.Downloaded].Visible = false;
                    }
                }
                else
                {
                    int projectID = UIHelpers.GetPIDParam();
                    if (!LoadProject(projectID)) { lblError.Text = UIHelpers.GetText(Language, "noRightsAlert"); return; }
                    //LoadVisibles(false,false,false,false,false);
                    grid.Visible = btnNew.Visible = false;
                    LoadSubCategories();
                }
                lblError.Text = "";


            }
            if (ddlSubCategories.Visible && (!LoggedUser.IsUser))
            {
                ddlSubCategories.Visible = false;
                lbSubCategories.Visible = false;
            }
            if ((!(LoggedUser.IsUser || LoggedUser.IsSubcontracter)))
            {
                btnSave.Visible = false;
                lbVisibles.Visible = cblVisibles.Visible = false;
            }
            if (LoggedUser.IsSubcontracter)
            {
                lbVisibles.Visible = cblVisibles.Visible = false;
            }
            
            if (UIHelpers.GetCIDParam() > 0)
                Header.PageTitle = string.Format(UIHelpers.GetText(Language, "pmPageTitle_EditCategory"), txtCategoryName.Text, ProjectsData.SelectProjectNameForPMmails(UIHelpers.GetPIDParam()));
            else
                Header.PageTitle = string.Format(UIHelpers.GetText(Language, "pmPageTitle_NewCategory"), txtCategoryName.Text, ProjectsData.SelectProjectNameForPMmails(UIHelpers.GetPIDParam()));
            //TODO

        }

        private void LoadVisibles(bool IsClientVisible, bool IsBuilderVisible, bool IsSubcontracterVisible, bool IsNadzorVisible, bool IsOtherVisible)
        {
            cblVisibles.Items.Clear();
            string lang = Language;
            ListItem li = new ListItem(UIHelpers.GetText(lang, "CategoryVisibleSubcontracters"));
            cblVisibles.Items.Add(li);

            cblVisibles.Items[0].Selected = IsSubcontracterVisible;
            li = new ListItem(UIHelpers.GetText(lang, "CategoryVisibleClients"));
            cblVisibles.Items.Add(li);
            cblVisibles.Items[1].Selected = IsClientVisible;
            li = new ListItem(UIHelpers.GetText(lang, "CategoryVisibleBuilders"));
            cblVisibles.Items.Add(li);
            cblVisibles.Items[2].Selected = IsBuilderVisible;

            li = new ListItem(UIHelpers.GetText(lang, "CategoryVisibleNadzor"));
            cblVisibles.Items.Add(li);
            cblVisibles.Items[3].Selected = IsNadzorVisible;

            li = new ListItem(UIHelpers.GetText(lang, "CategoryVisibleOther"));
            cblVisibles.Items.Add(li);
            cblVisibles.Items[4].Selected = IsOtherVisible;
            lbVisibles.Visible = true;
        }
        private void LoadResourses()
        {
            string lang = Language;
            lbProjectName.Text = UIHelpers.GetText(lang, "Name_ProjectName");
            lbCategoryName.Text = UIHelpers.GetText(lang, "Name_CategoryName");
            btnNew.Text = UIHelpers.GetText(lang, "Name_btnNewFileName");
            //btnBack.Text = UIHelpers.GetText(lang,"Name_btnBackCategoryName");
            btnSave.Text = UIHelpers.GetText(lang, "Name_btnSaveName");
            linkBack.Text = "<i class='fas fa-chevron-left'></i> " + UIHelpers.GetText(lang, "Name_btnBackCategoryName");
            grdFiles.Columns[(int)gridColomns.Subject].HeaderText = UIHelpers.GetText(lang, "Name_FileSubjectColomn");
            grdFiles.Columns[(int)gridColomns.Version].HeaderText = UIHelpers.GetText(lang, "Name_VersionColomn");
            grdFiles.Columns[(int)gridColomns.Creator].HeaderText = UIHelpers.GetText(lang, "Name_CreatorColomn");
            //grdFiles.Columns[(int)gridColomns.Minutes].HeaderText  = UIHelpers.GetText(lang,"Name_FileMinutesColomn");
            grdFiles.Columns[(int)gridColomns.FileName].HeaderText = UIHelpers.GetText(lang, "Name_SubjectColomn");
            grdFiles.Columns[(int)gridColomns.Date].HeaderText = UIHelpers.GetText(lang, "Name_DateColomn");
            lbSubCategories.Text = UIHelpers.GetText(lang, "Name_lbSubCategories");
            lbVisibles.Text = UIHelpers.GetText(lang, "Name_VisiblesName");
            lbVisibles.Visible = false;
        }


        protected string getTextLink()
        {
            return UIHelpers.GetText(Language, "tooltip_gridEdit");
        }

        public string getDownloaded(int id)
        {

            if (_downloaded.Contains(id))
            {
                return "<i class='fas fa-check-circle' style='color: green;'></i>";

            }
            else
            {
                return "<i class='fas fa-arrow-alt-circle-down'></i>";
            }
        }
		#endregion

		#region Web Form Designer generated code
        override protected void OnInit(EventArgs e)
        {
            //
            // CODEGEN: This call is required by the ASP.NET Web Form Designer.
            //
            InitializeComponent();
            base.OnInit(e);
        }
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
        private void InitializeComponent()
        {
            this.grdFiles.ItemCommand += new System.Web.UI.WebControls.DataGridCommandEventHandler(this.grdFiles_ItemCommand);

        }
		#endregion

		#region DB

        protected string GetURL(int ID, string ext)
        {
            string returnStirng = string.Concat(System.Configuration.ConfigurationManager.AppSettings["FilePath"], ID, ext);
            return returnStirng;
        }


        private bool LoadCategory(int CategoryID)
        {
            try
            {
                ProjectCategoryData dat = ProjectCategoryDAL.Load(CategoryID);
                if (dat == null)
                    return false;
                txtProjectName.Text = dat.ProjectName;
                txtCategoryName.Text = dat.CategoryName;
                lblCategoryName.Text = dat.CategoryName;
                //LoadVisibles(dat.IsClientVisible,dat.IsBuilderVisible,dat.IsSubcontractorVisible);
                if (!LoggedUser.IsUser)//||dat.ClientID>0||dat.SubcontracterID>0)
                {
                    txtCategoryName.Visible = false;
                    img.Visible = false;
                    lblCategoryName.Visible = true;
                    //txtCategoryName.Enabled = false;
                }
                else
                {
                    txtCategoryName.Visible = true;
                    img.Visible = true;
                    lblCategoryName.Visible = false;
                }
            }
            catch (Exception ex)
            {
                log.Error(UIHelpers.GetLogErrorMessage(ex));
                return false;
            }
            finally
            {
                //if (reader!=null) reader.Close(); 
            }
            return true;
        }


        private void LoadSubCategories()
        {
            ProjectCategoriesVector pcv = ProjectCategoryDAL.LoadCollection(UIHelpers.GetPIDParam(), -1);

            SubcontractersVector vec = SubcontracterDAL.LoadCollection("SubcontractersListProc1", SQLParms.CreateSubcontractersListProc1(-1, LoggedUser.HasPaymentRights));

            //SubcontractersVector psv = SubcontracterDAL.LoadCollection("ProjectSubcontractersListProc1",SQLParms.CreateProjectSubcontractersListProc1(UIHelpers.GetPIDParam(),false));
            foreach (SubcontracterData subdat in vec)
            {
                bool _contain = false;
                foreach (ProjectCategoryData dat in pcv)
                {
                    if (dat.CategoryName == subdat.SubcontracterType + " - " + subdat.SubcontracterName)
                        _contain = true;
                }
                if (!_contain)
                {
                    ListItem li = new ListItem(subdat.SubcontracterType + " - " + subdat.SubcontracterName, subdat.SubcontracterID.ToString());
                    ddlSubCategories.Items.Add(li);
                }
            }
            ddlSubCategories.Items.Insert(0, new ListItem("", "0"));
            if (ddlSubCategories.Items.Count == 1)
            {
                ddlSubCategories.Visible = false;
                lbSubCategories.Visible = false;
            }
            else
                ddlSubCategories.Visible = true;
        }


        private bool LoadProject(int projectID)
        {
            SqlDataReader reader = null;
            try
            {
                reader = ProjectsData.SelectProject(projectID);
                if (reader.Read())
                {
                    txtProjectName.Text = reader.GetString(1);
                }
            }
            catch (Exception ex)
            {
                log.Error(UIHelpers.GetLogErrorMessage(ex));
                return false;
            }
            finally
            {
                if (reader != null) reader.Close();
            }
            lblCategoryName.Visible = false;
            txtCategoryName.Visible = true;
            img.Visible = true;
            return true;
        }
        private bool Exists(string name, int id)
        {
            ProjectCategoriesVector pcv = ProjectCategoryDAL.LoadCollection(UIHelpers.GetPIDParam(), -1);
            foreach (ProjectCategoryData pcd in pcv)
                if (pcd.CategoryName == name && (id < 0 || id > 0 && pcd.CategoryID != id))
                    return true;
            return false;
        }

        private void BindGrid()
        {
            FilesPMVector fPMv = FilePMDAL.LoadCollectionByCategory(UIHelpers.GetCIDParam(), LoggedUser.UserType, SortOrder);
            if (SessionManager.Lang == Languages.EN)
                grdFiles.Columns[(int)gridColomns.Creator].Visible = false;


            grdFiles.DataSource = fPMv;
            grdFiles.DataKeyField = "FileID";
            grdFiles.DataBind();
            try
            {
                SetConfirmDelete(grdFiles, "btnDelete", (int)gridColomns.FileName);
            }
            catch (Exception ex)
            {
                log.Error(UIHelpers.GetLogErrorMessage(ex));
            }
            bool IsClientVisible = true;
            bool IsBuilderVisible = true;
            bool IsSubcontracterVisible = true;
            bool IsNadzorVisible = true;
            bool IsOtherVisible = true;
            if (fPMv.Count == 0)
                LoadVisibles(false, false, false, false, false);
            else
            {
                foreach (FilePMData d in fPMv)
                {
                    if (!d.IsBuilderVisible)
                        IsBuilderVisible = false;
                    if (!d.IsClientVisible)
                        IsClientVisible = false;
                    if (!d.IsSubcontractorVisible)
                        IsSubcontracterVisible = false;
                    if (!d.IsNadzorVisible)
                        IsNadzorVisible = false;
                    if (!d.IsOtherVisible)
                        IsOtherVisible = false;


                }
                LoadVisibles(IsClientVisible, IsBuilderVisible, IsSubcontracterVisible, IsNadzorVisible, IsOtherVisible);
            }
        }
		
	
		#endregion

		#region EventHandlers



        private void back_click()
        {
            Response.Redirect("EditProject.aspx" + "?pid=" + UIHelpers.GetPIDParam());
        }

        protected void btnSave_Click(object sender, System.EventArgs e)
        {
            SaveCategory();

        }


        private void SaveCategory()
        {
            if (txtCategoryName.Text.Length == 0 && ddlSubCategories.SelectedValue == "0")
            {
                AlertFieldNotEntered(lbCategoryName);
                Dirty = true;
                return;
            }
            string name = "";
            int categoryID = UIHelpers.GetCIDParam();
            if (categoryID > 0)
            {
                name = UIHelpers.CleanFolder(txtCategoryName.Text);
            }
            else
            {
                if (ddlSubCategories.SelectedValue != "0" && ddlSubCategories.SelectedItem != null)
                {
                    name = UIHelpers.CleanFolder(ddlSubCategories.SelectedItem.Text);
                }
                else
                {
                    if (LoggedUser.IsUser)
                    {
                        string StartWith = SubcontracterDAL.LoadSubcontracterTypeSymbol(int.Parse(System.Configuration.ConfigurationManager.AppSettings["AchitectureID"]));
                        name = string.Concat(StartWith, " - ", txtCategoryName.Text);
                    }
                    if (LoggedUser.IsSubcontracter)
                    {
                        SubcontracterData sdat = SubcontracterDAL.Load(LoggedUser.UserID);
                        string StartWith = SubcontracterDAL.LoadSubcontracterTypeSymbol(sdat.SubcontracterTypeID);
                        name = string.Concat(StartWith, " - ", txtCategoryName.Text);
                    }
                }
            }

            if (Exists(UIHelpers.CleanFolder(name), categoryID))
            {
                lblError.Text = Resource.ResourceManager["Category_Error_Exists"];
                return;
            }
            //			//notes:here set visibility
            //			bool isClientVisible = true;
            //			bool isBuilderVisible = true;	
            //			bool isSubcontracterVisible = true;
            //			ListItem li = cblVisibles.Items.FindByText(UIHelpers.GetText(Language,"CategoryVisibleSubcontracters"));
            //			if(li != null)
            //				isSubcontracterVisible = li.Selected;
            //			li = cblVisibles.Items.FindByText(UIHelpers.GetText(Language,"CategoryVisibleClients"));
            //			if(li != null)
            //				isClientVisible = li.Selected;
            //			li = cblVisibles.Items.FindByText(UIHelpers.GetText(Language,"CategoryVisibleBuilders"));
            //			if(li != null)
            //				isBuilderVisible = li.Selected;



            ProjectCategoryData pcd = new ProjectCategoryData();
            string sCategoryName = UIHelpers.CleanFolder(txtCategoryName.Text);
            if (categoryID > 0)
            {
                pcd = ProjectCategoryDAL.Load(categoryID);

                if (pcd == null)
                {
                    lblError.Text = UIHelpers.GetText(Language, "ErrorSaveCategory");
                    return;
                }
                if (sCategoryName != pcd.CategoryName)
                {
                    DirectoryInfo directory = new DirectoryInfo(string.Concat(System.Configuration.ConfigurationManager.AppSettings["FilePath"], @"\", txtProjectName.Text, @"\", pcd.CategoryName));

                    if (directory.Exists)
                        directory.MoveTo(string.Concat(System.Configuration.ConfigurationManager.AppSettings["FilePath"], @"\", txtProjectName.Text, @"\", sCategoryName));

                }
                pcd.CategoryName = sCategoryName;
                //				pcd.IsClientVisible = isClientVisible;
                //				pcd.IsBuilderVisible = isBuilderVisible;
                //				pcd.IsSubcontractorVisible = isSubcontracterVisible;
            }
            else
            {
                if (ddlSubCategories.SelectedValue != "0")
                {
                    pcd = new ProjectCategoryData(-1, UIHelpers.GetPIDParam(), UIHelpers.CleanFolder(ddlSubCategories.SelectedItem.Text)/*,isClientVisible,isSubcontracterVisible,isBuilderVisible*/, true, -1, UIHelpers.ToInt(ddlSubCategories.SelectedValue), false);
                    //access
                    PMProjectUserAccesssVector pmpav = PMProjectUserAccessDAL.LoadCollectionForUser((int)PMUserType.Subcontracter, UIHelpers.ToInt(ddlSubCategories.SelectedValue), UIHelpers.GetPIDParam());
                    if (pmpav != null && pmpav.Count > 0)
                    {
                        pmpav[0].HasAccess = true;
                        PMProjectUserAccessDAL.Save(pmpav[0]);
                    }
                    else
                    {
                        PMProjectUserAccessData dat = new PMProjectUserAccessData(-1, (int)PMUserType.Subcontracter, UIHelpers.ToInt(ddlSubCategories.SelectedValue), UIHelpers.GetPIDParam(), true, string.Empty);
                        PMProjectUserAccessDAL.Save(dat);
                    }
                    //here send mail to account
                    MailBO.PMSendMailSubcontracterDirectoryAdd(UIHelpers.CleanFolder(ddlSubCategories.SelectedItem.Text), UIHelpers.GetPIDParam(), LoggedUser);
                }
                else
                {
                    if (LoggedUser.IsUser)
                    {
                        string StartWith = SubcontracterDAL.LoadSubcontracterTypeSymbol(int.Parse(System.Configuration.ConfigurationManager.AppSettings["AchitectureID"]));
                        pcd = new ProjectCategoryData(-1, UIHelpers.GetPIDParam(), string.Concat(StartWith, " - ", sCategoryName)/*,isClientVisible,isSubcontracterVisible,isBuilderVisible*/, true, -1, -1, false);
                    }
                    if (LoggedUser.IsSubcontracter)
                    {
                        SubcontracterData sdat = SubcontracterDAL.Load(LoggedUser.UserID);
                        string StartWith = SubcontracterDAL.LoadSubcontracterTypeSymbol(sdat.SubcontracterTypeID);
                        pcd = new ProjectCategoryData(-1, UIHelpers.GetPIDParam(), string.Concat(StartWith, " - ", sCategoryName)/*,isClientVisible,isSubcontracterVisible,isBuilderVisible*/, true, -1, sdat.SubcontracterID, false);
                    }
                }
            }
            if (LoggedUser.IsUser)
            {
                FilesPMVector fPMv = FilePMDAL.LoadCollectionByCategory(UIHelpers.GetCIDParam(), LoggedUser.UserType, SortOrder);
                foreach (FilePMData d in fPMv)
                {
                    d.IsSubcontractorVisible = cblVisibles.Items[0].Selected;
                    d.IsClientVisible = cblVisibles.Items[1].Selected;
                    d.IsBuilderVisible = cblVisibles.Items[2].Selected;
                    d.IsNadzorVisible = cblVisibles.Items[3].Selected;
                    d.IsOtherVisible = cblVisibles.Items[4].Selected;
                    FilePMDAL.Save(d);
                }
            }
            lbSubCategories.Visible = false;
            ProjectCategoryDAL.Save(pcd);
            log.Info(string.Format(Resource.ResourceManager["log_info_PM_CreateCategory"], txtCategoryName.Text, txtProjectName.Text, LoggedUser.Account));

            if (UIHelpers.GetCIDParam() == -1)
                Response.Redirect("Files.aspx" + "?cid=" + pcd.CategoryID + "&pid=" + UIHelpers.GetPIDParam());
        }


        protected void btnNew_Click(object sender, System.EventArgs e)
        {
            Response.Redirect("EditFile.aspx" + "?cid=" + UIHelpers.GetCIDParam() + "&pid=" + UIHelpers.GetPIDParam());
        }


        private void grdFiles_ItemCommand(object source, System.Web.UI.WebControls.DataGridCommandEventArgs e)
        {
            if (e.CommandName == "Sort")
            {
                int sortOrder = SortOrder;
                switch ((string)e.CommandArgument)
                {
                    case "FileNameSave": if (sortOrder == 1) sortOrder = -1; else sortOrder = 1;
                        SortOrder = sortOrder;
                        BindGrid(); break;
                    case "FileSubject": if (sortOrder == 2) sortOrder = -2; else sortOrder = 2;
                        SortOrder = sortOrder;
                        BindGrid(); break;
                    case "FileVersion": if (sortOrder == 3) sortOrder = -3; else sortOrder = 3;
                        SortOrder = sortOrder;
                        BindGrid(); break;
                    case "CreatedBy": if (sortOrder == 4) sortOrder = -4; else sortOrder = 4;
                        SortOrder = sortOrder;
                        BindGrid(); break;
                    case "DateCreated": if (sortOrder == 5) sortOrder = -5; else sortOrder = 5;
                        SortOrder = sortOrder;
                        BindGrid(); break;
                }

                return;
            }
            if (e.CommandName == "Edit")
            {
                int ID = (int)((DataGrid)source).DataKeys[e.Item.ItemIndex];
                string filePath = /*Server.MapPath(*/string.Concat(System.Configuration.ConfigurationManager.AppSettings["FilePath"], @"\", txtProjectName.Text, @"\", txtCategoryName.Text, @"\", e.Item.Cells[(int)gridColomns.FileNameUnvisinle].Text, e.Item.Cells[(int)gridColomns.Type].Text);//);

                Response.Clear();

                Response.Cache.SetExpires(DateTime.Now.AddSeconds(-1));
                Response.Cache.SetCacheability(HttpCacheability.Private);
                string fileName = e.Item.Cells[(int)gridColomns.FileNameUnvisinle].Text + e.Item.Cells[(int)gridColomns.Type].Text;

                Response.ContentType = "application/octet-stream";
                Response.ContentEncoding = System.Text.Encoding.UTF8;
                fileName.Trim();
                fileName = fileName.Replace(" ", ""); //replace commas because chrome breaks

                //Response.AppendHeader("Content-Disposition", "attachment; filename=\"" + HttpUtility.UrlEncode(fileName, System.Text.Encoding.UTF8) + "\"");
                Response.AppendHeader("Content-Disposition", "attachment; filename=\"" + fileName + "\"");

                Response.WriteFile(filePath);
                //notes:here send message if file is tracking
                FilePMData fPMd = FilePMDAL.Load(ID);
                if (fPMd.MailForDownload)
                {
                    UserInfoPM MailToUser = UsersData.SelectUserPMDownLoadByName(fPMd.CreatedBy);
                    MailBO.PMSendMailDownload(LoggedUser.Account, fPMd, MailToUser.Mail);
                }

                //notes:Save Log in DateBase if not asa
                if (LoggedUser.IsClient || LoggedUser.IsBuilder || LoggedUser.IsSubcontracter)
                {
                    ProjectLogData pld = new ProjectLogData(-1, ID, DateTime.Now, false, LoggedUser.FullName);
                    ProjectLogDAL.Save(pld);
                }
                Response.End();
                return;
            }
            if (e.CommandName == "editDB")
            {
                string sFileID = ((DataGrid)source).DataKeys[e.Item.ItemIndex].ToString();
                Response.Redirect("EditFile.aspx" + "?id=" + sFileID + "&pid=" + UIHelpers.GetPIDParam() + "&cid=" + UIHelpers.GetCIDParam());
                return;
            }
            if (!(e.CommandSource is ImageButton))
                return;
            ImageButton ib = (ImageButton)e.CommandSource;
            string s = ib.ID;
            switch (ib.ID)
            {
                //				case "btnFileLoad":
                //				{
                //					FilePMData fPMd = FilePMDAL.Load(ID);
                //					if(fPMd.MailForDownload&&LoggedUser.Account!=fPMd.CreatedBy)
                //					{
                //							UserInfoPM MailToUser = UsersData.SelectUserPMByAccount(fPMd.CreatedBy);
                //							MailBO.PMSendMailDownload(LoggedUser.Account,fPMd,MailToUser.Mail);
                //					}
                //		}
                case "btnDelete":
                    {
                        int ID = (int)((DataGrid)source).DataKeys[e.Item.ItemIndex];
                        FilePMDAL.Delete(ID);
                        log.Info(string.Format(Resource.ResourceManager["log_info_PM_DeleteFile"], e.Item.Cells[(int)gridColomns.Subject].Text, txtCategoryName.Text, txtProjectName.Text, LoggedUser.Account));
                        ProjectLogData pld = new ProjectLogData(-1, ID, DateTime.Now, true, LoggedUser.FullName);
                        pld.Deleted = true;
                        ProjectLogDAL.Save(pld);
                        BindGrid();
                        break;
                    }
                //				case "btnEdit":
                //				{
                //					string sFileID = ((DataGrid)source).DataKeys[e.Item.ItemIndex].ToString();
                //					Response.Redirect("EditFile.aspx"+"?id="+sFileID+"&pid="+UIHelpers.GetPIDParam()+"&cid="+UIHelpers.GetCIDParam()); 
                //					return;
                //				}
            }
        }


        private void btnLogs_Click(object sender, System.EventArgs e)
        {
            Response.Redirect("Logs.aspx" + "?cid=" + UIHelpers.GetCIDParam());
        }

        protected string GetPDFdownload()
        {

            return UIHelpers.GetText(Language, "Name_PDFloadName");
        }

        private void ddlSubCategories_SelectedIndexChanged(object sender, System.EventArgs e)
        {
            if (ddlSubCategories.SelectedValue != "0")
                img.Visible = false;
            else
                img.Visible = true;

        }

        protected void imgBack_Click(object sender, System.Web.UI.ImageClickEventArgs e)
        {
            back_click();
        }
        protected void link_Click(object sender, System.EventArgs e)
        {
            back_click();
        }
		#endregion

	
		
			
	}
}
