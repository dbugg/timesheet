﻿<%@ Page Title="" Language="C#" MasterPageFile="~/timesheet.Master" AutoEventWireup="true" CodeBehind="Login.aspx.cs" Inherits="Asa.Timesheet.WebPages.Login" %>

<%@ Register TagPrefix="recaptcha" Namespace="Recaptcha" Assembly="Recaptcha" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style>
        body {
            background: url('images/background.jpg');
            background-size: cover;
        }

        .quote {
            font-family: Times New Roman;
            font-weight: 100;
            position: relative;
            line-height: 28pt;
            font-size: 20pt;
            color: rgba(255,255,255, 0.9);
            text-shadow: 1px 1px rgba(0,0,0, 0.5);
        }

            .quote:before {
                font-family: Times New Roman;
                content: "\"";
                font-size: 80pt;
                position: absolute;
                top: 0px;
                left: -60px;
                opacity: .3;
            }

            .quote:after {
                font-family: Times New Roman;
                content: "\"";
                font-size: 80pt;
                position: absolute;
                bottom: 0px;
                right: -60px;
                opacity: .3;
            }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="container d-flex ">
        <div class="row justify-content-center align-self-center">
            <div class="col-md-6">
                <div class="card" style="background: rgba(255,255,255, 0.7)">
                    <div class="card-body">
                        <a href="/timesheet">
                            <img style="padding-bottom: 15px" id="IMG1" alt="" src="~/images/logo.png" width="320" runat="server"></a>
                        <div class="form-group">
                            <asp:Label for="txtLoginName" ID="lblLoginName" runat="server" Font-Bold="True" CssClass="form-label"
                                EnableViewState="False">Поща:</asp:Label>
                            <asp:TextBox ID="txtLoginName" onkeydown="testForEnterR()" runat="server"
                                CssClass="form-control" placeholder="Електронен адрес"></asp:TextBox>
                            <asp:Label ID="lblPassword" runat="server" CssClass="enterDataLabel" Font-Bold="True">Парола:</asp:Label>
                            <asp:TextBox ID="txtPassword" onkeydown="testForEnterR()" runat="server"
                                CssClass="form-control" TextMode="Password" placeholder="Парола"></asp:TextBox><asp:Label ID="lblError" runat="server" ForeColor="Red" EnableViewState="False"></asp:Label>
                        </div>
                        <asp:Button ID="btnEnter" runat="server" CssClass="btn btn-primary btn-block" Text="Вход" OnClick="btnEnter_Click"></asp:Button>
                        <span runat="server" id="rowCaptcha" visible="false">
                            <recaptcha:RecaptchaControl ID="recaptcha" runat="server" PublicKey="6Ld2e1YUAAAAAGnmKVDupVv9ViOhz" PrivateKey="6Ld2e1YUAAAAAMBvAvjnaZt36JlY9suyaeZQPrqJ" />
                        </span>
                    </div>
                </div>
                <br />
                <hr />
                <i class="quote">Every great architect is - necessarily - a great poet. He must be a great original interpreter of his time, his day, his age. 
                </i>
                <br />
                <p class="text-right">~ Frank Lloyd Wright</strong></p>
            </div>

        </div>
    </div>
</asp:Content>
