using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Data.SqlClient;
using log4net;
using Asa.Timesheet.Data;
using Asa.Timesheet.Data.Entities;
using System.Text;

namespace Asa.Timesheet.WebPages
{
	/// <summary>
	/// Summary description for EditClient.
	/// </summary>
	public class EditClient : TimesheetPageBase
	{
		#region WebControls

		protected System.Web.UI.WebControls.Label lblInfo;
		protected System.Web.UI.WebControls.Label lblName;
		protected System.Web.UI.WebControls.Label lblCity;
		protected System.Web.UI.WebControls.TextBox txtName;
		protected System.Web.UI.WebControls.TextBox txtCity;
		protected System.Web.UI.WebControls.TextBox txtAddress;
		protected System.Web.UI.WebControls.Button btnDelete;
		protected System.Web.UI.WebControls.PlaceHolder menuHolder;
		protected System.Web.UI.WebControls.Label lblAddress;
		protected System.Web.UI.WebControls.Button btnCancel;
		protected System.Web.UI.WebControls.Label lblError;
		protected Asa.Timesheet.WebPages.UserControls.PageHeader header;
		protected System.Web.UI.WebControls.Label lblEmail;
		protected System.Web.UI.WebControls.TextBox txtEmail;
		protected System.Web.UI.WebControls.Label Label1;
		protected System.Web.UI.WebControls.Label Label2;
		protected System.Web.UI.WebControls.Label Label3;
		protected System.Web.UI.WebControls.Label Label4;
		protected System.Web.UI.WebControls.TextBox txtFax;
		protected System.Web.UI.WebControls.TextBox txtManager;
		protected System.Web.UI.WebControls.TextBox txtRepr1;
		protected System.Web.UI.WebControls.TextBox txtRepr2;
		protected System.Web.UI.WebControls.Label Label5;
		protected System.Web.UI.WebControls.TextBox TextBox1;
		protected System.Web.UI.WebControls.Label Label6;
		protected System.Web.UI.WebControls.TextBox TextBox2;
		protected System.Web.UI.WebControls.Label Label7;
		protected System.Web.UI.WebControls.TextBox TextBox3;
		protected System.Web.UI.WebControls.Label Label8;
		protected System.Web.UI.WebControls.TextBox Textbox4;
		protected System.Web.UI.WebControls.Label Label9;
		protected System.Web.UI.WebControls.Label Label10;
		protected System.Web.UI.WebControls.TextBox Textbox5;
		protected System.Web.UI.WebControls.Label Label11;
		protected System.Web.UI.WebControls.TextBox Textbox6;
		protected System.Web.UI.WebControls.TextBox txtDelo;
		protected System.Web.UI.WebControls.TextBox txtBulstat;
		protected System.Web.UI.WebControls.TextBox txtNDR;
		protected System.Web.UI.WebControls.TextBox txtPhone;
		protected System.Web.UI.WebControls.TextBox txtPhone1;
		protected System.Web.UI.WebControls.TextBox txtEmail1;
		protected System.Web.UI.WebControls.Label Label12;
		protected System.Web.UI.WebControls.Label Label13;
		protected System.Web.UI.WebControls.TextBox txtPhone2;
		protected System.Web.UI.WebControls.Label Label14;
		protected System.Web.UI.WebControls.TextBox txtEmail2;
		protected System.Web.UI.WebControls.Label Label15;
		protected System.Web.UI.WebControls.TextBox txtWebsite;
		protected Asa.Timesheet.WebPages.UserControls.EditForm editCtrl;
		protected System.Web.UI.WebControls.Label lbAccountName;
		protected System.Web.UI.WebControls.TextBox txtAccountName;
		protected System.Web.UI.WebControls.Label lbAccountPass;
		protected System.Web.UI.WebControls.TextBox txtAccountPass;

   
		protected System.Web.UI.WebControls.Button btnAutoGenerate;
		protected System.Web.UI.WebControls.TextBox txtHdnAccountPass;
		protected System.Web.UI.HtmlControls.HtmlInputHidden hdnAccountPass;
		protected System.Web.UI.WebControls.Label lbClientType;
		protected System.Web.UI.WebControls.DropDownList ddlClientType; 
		protected System.Web.UI.HtmlControls.HtmlImage requiredClientType;

		 #endregion
		protected System.Web.UI.WebControls.Button btnSaveUP;
		protected System.Web.UI.WebControls.Button btnEditUP;
		protected System.Web.UI.WebControls.Button btnCancelUP;
		protected System.Web.UI.WebControls.Button btnSave;
		protected System.Web.UI.WebControls.Button btnEdit;
		protected System.Web.UI.WebControls.Label Label16;
		protected System.Web.UI.WebControls.TextBox txtNameEN;
		protected System.Web.UI.WebControls.Label Label17;
		protected System.Web.UI.WebControls.TextBox txtReprEN;
		

//		//notes:here is set name and pass at the load of the form
//		private string _accName = string.Empty;
//		private string _accPass = string.Empty;


		private static readonly ILog log = LogManager.GetLogger(typeof(EditClient));

        private void Page_Load(object sender, System.EventArgs e)
        {
            //if (! (LoggedUser.IsLeader || LoggedUser.IsAssistant)) ErrorRedirect(Resource.ResourceManager["noRightsAlert"]);
            if (Page.IsPostBack == false)
            {
                //btnAutoGenerate.Attributes["onclick"] = "if(document.getElementById('txtAccountPass')!=null) document.getElementById('txtAccountPass').value=document.getElementById('hdnAccountPass').value;";

                // Set up form for data change checking when
                // first loaded.
                this.CheckForDataChanges = true;
                this.BypassPromptIds =
                   new string[] { "btnSave","btnSaveUP", "btnDelete",
                           "btnEdit","btnEditUP","btnAutoGenerate","ddlClientType" };
            }
            UIHelpers.CreateMenu(menuHolder, LoggedUser);

            if (!this.IsPostBack)
            {
                header.UserName = LoggedUser.FullName;
                LoadClientTypes();
                int clientID = this.ClientID;
                if (clientID > 0)
                {
                    switch (UIHelpers.GetIDParam())
                    {
                        case ((int)ClientTypes.Client): { header.PageTitle = Resource.ResourceManager["editClient_EditLabel"]; break; }
                        case ((int)ClientTypes.Builder): { header.PageTitle = Resource.ResourceManager["editBuilder_EditLabel"]; break; }
                        case ((int)ClientTypes.Producer): { header.PageTitle = Resource.ResourceManager["editProducer_EditLabel"]; break; }
                        case ((int)ClientTypes.All): { header.PageTitle = Resource.ResourceManager["editContact_EditLabel"]; break; }
                        default: { header.PageTitle = Resource.ResourceManager["editContact_EditLabel"]; break; }
                    }
                    //					if(UIHelpers.GetIDParam() == (int)ClientTypes.Client)
                    //						header.PageTitle = Resource.ResourceManager["editClient_EditLabel"];
                    //					else
                    //						header.PageTitle = Resource.ResourceManager["editBuilder_EditLabel"];

                    if (!LoadClient(clientID)) ErrorRedirect(Resource.ResourceManager["editClient_ErrorLoadClient"]);

                    SetConfirmDelete(btnDelete, txtName.Text);
                }
                else
                {
                    switch (UIHelpers.GetIDParam())
                    {
                        case ((int)ClientTypes.Client): { header.PageTitle = Resource.ResourceManager["editClient_NewLabel"]; break; }
                        case ((int)ClientTypes.Builder): { header.PageTitle = Resource.ResourceManager["editBuilder_NewLabel"]; break; }
                        case ((int)ClientTypes.Producer): { header.PageTitle = Resource.ResourceManager["editProducer_NewLabel"]; break; }
                        case ((int)ClientTypes.All): { header.PageTitle = Resource.ResourceManager["editContact_NewLabel"]; break; }
                        default: { header.PageTitle = Resource.ResourceManager["editContact_NewLabel"]; break; }
                    }
                    //					if(UIHelpers.GetIDParam() == (int)ClientTypes.Client)
                    //						header.PageTitle = Resource.ResourceManager["editClient_NewLabel"];
                    //					else
                    //						header.PageTitle = Resource.ResourceManager["editBuilder_NewLabel"];
                    btnDelete.Visible = false;
                    if (UIHelpers.GetIDParam() != (int)ClientTypes.Client)
                    {
                        //						Label5.Visible = Label6.Visible = Label7.Visible = lblCity.Visible = 
                        //							lblAddress.Visible = Label1.Visible = Label2.Visible = Label8.Visible = 
                        //							Label15.Visible = Label9.Visible = Label3.Visible = 
                        //							Label10.Visible = Label11.Visible = Label12.Visible = Label4.Visible = 
                        //							Label14.Visible = Label13.Visible = 
                        //							txtCity.Visible = txtAddress.Visible = txtFax.Visible = txtManager.Visible = 
                        //							txtRepr1.Visible = txtRepr2.Visible = txtDelo.Visible = txtBulstat.Visible = 
                        //							txtNDR.Visible = txtPhone.Visible = txtWebsite.Visible = txtPhone1.Visible = 
                        //							txtEmail1.Visible = txtPhone2.Visible = txtEmail2.Visible = false;
                    }
                }
                requiredClientType.Visible = lbClientType.Visible = ddlClientType.Visible = (UIHelpers.GetIDParam() == (int)ClientTypes.All);
                lblError.Text = "";
                InitEdit();
            }

            if (!(LoggedUser.HasPaymentRights || LoggedUser.IsSecretary))
            {
                btnEdit.Visible = false;
                btnEditUP.Visible = false;
                if (ClientID <= 0)
                    ErrorRedirect(Resource.ResourceManager["noRightsAlert"]);
            }

        }

	
		#region Web Form Designer generated code
        override protected void OnInit(EventArgs e)
        {
            //
            // CODEGEN: This call is required by the ASP.NET Web Form Designer.
            //
            InitializeComponent();
            base.OnInit(e);
        }
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
        private void InitializeComponent()
        {
            this.btnSaveUP.Click += new System.EventHandler(this.btnSave_Click);
            this.btnCancelUP.Click += new System.EventHandler(this.btnCancel_Click);
            this.ddlClientType.SelectedIndexChanged += new System.EventHandler(this.ddlClientType_SelectedIndexChanged);
            this.btnAutoGenerate.Click += new System.EventHandler(this.btnAutoGenerate_Click);
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            this.btnDelete.Click += new System.EventHandler(this.btnDelete_Click);
            this.Load += new System.EventHandler(this.Page_Load);

        }
		#endregion

		#region Event handlers 

        private void btnCancel_Click(object sender, System.EventArgs e)
        {
            Response.Redirect("Clients.aspx" + "?id=" + UIHelpers.GetIDParam().ToString());
        }

        private void btnSave_Click(object sender, System.EventArgs e)
        {
            string clientName = txtName.Text.Trim();
            if (clientName == String.Empty)
            {
                AlertFieldNotEntered(lblName);
                return;
            }
            if (UIHelpers.GetIDParam() == (int)ClientTypes.All && (UIHelpers.ToInt(ddlClientType.SelectedValue) <= 0))
            {
                AlertFieldNotEntered(lbClientType);
                return;
            }


            string password = txtAccountPass.Text.Trim();
            if (password == hdnAccountPass.Value) password = txtHdnAccountPass.Text;
            if (!txtAccountName.Visible)
            {
                password = "";
                txtAccountName.Text = "";
            }

            if (txtAccountName.Text.Length > 0 && !(UIHelpers.CheckForUniqueNameAccount(txtAccountName.Text, OldAccName/*_accName*/)))
            {
                lblError.Text = string.Format(Resource.ResourceManager["ErrorNotUniqueName"], txtAccountName.Text);
                return;
            }
            if (txtAccountName.Text.Length == 0 || password.Length == 0)
            {
                txtAccountName.Text = "";
                txtAccountPass.Text = "";
                password = "";
            }

            string city = txtCity.Text.Trim();
            string address = txtAddress.Text.Trim();
            string email = txtEmail.Text.Trim();

            int clientType = UIHelpers.ToInt(ddlClientType.SelectedValue);
            if (UIHelpers.GetIDParam() != (int)ClientTypes.All)
                clientType = UIHelpers.GetIDParam();

            int clientID = this.ClientID;
            if (clientID > 0)
            {
                if (!UpdateClient(clientID, clientName, city, address, email, txtFax.Text, txtManager.Text, txtRepr1.Text, txtRepr2.Text
                    , txtDelo.Text, txtBulstat.Text, txtNDR.Text, txtPhone.Text, txtWebsite.Text, txtPhone1.Text, txtEmail1.Text, txtPhone2.Text, txtEmail2.Text,
                    clientType, txtAccountName.Text, password, txtNameEN.Text, txtReprEN.Text))
                {
                    lblError.Text = Resource.ResourceManager["editClient_ErrorSave"];
                    return;
                }
                else
                {
                    if (txtAccountName.Text.Length > 0 && txtAccountPass.Text.Length > 0)
                        MailBO.SendMailNewAccount(txtEmail.Text, txtAccountName.Text, password);
                    log.Info(string.Format("Client {0} has been UPDATED by {1}", clientID, string.Concat(LoggedUser.FullName, " ( userID ", LoggedUser.UserID, " )")));
                }
            }
            else
            {
                if (!InsertClient(clientName, city, address, email, txtFax.Text, txtManager.Text, txtRepr1.Text, txtRepr2.Text
                    , txtDelo.Text, txtBulstat.Text, txtNDR.Text, txtPhone.Text, txtWebsite.Text, txtPhone1.Text, txtEmail1.Text, txtPhone2.Text, txtEmail2.Text,
                    clientType, txtAccountName.Text, password, txtNameEN.Text, txtReprEN.Text))
                {
                    lblError.Text = Resource.ResourceManager["editClient_ErrorSave"];
                    return;
                }
                else
                {
                    if (txtAccountName.Text.Length > 0 && txtAccountPass.Text.Length > 0)
                        MailBO.SendMailNewAccount(txtEmail.Text, txtAccountName.Text, password);
                    log.Info(string.Format("Client {0} has been INSERTED by {1}", clientID, string.Concat(LoggedUser.FullName, " ( userID ", LoggedUser.UserID, " )")));
                }
            }
            Response.Redirect("Clients.aspx" + "?id=" + UIHelpers.GetIDParam().ToString() + "&all=1");
        }

        private void btnDelete_Click(object sender, System.EventArgs e)
        {
            int clientID = this.ClientID;

            if (!DeleteClient(clientID))
            {
                lblError.Text = Resource.ResourceManager["editClient_ErrorDelete"];
                return;
            }
            else
                log.Info(string.Format("Client {0} has been DELETED by {1}", clientID, string.Concat(LoggedUser.FullName, " ( userID ", LoggedUser.UserID, " )")));

            Response.Redirect("Clients.aspx" + "?id=" + UIHelpers.GetIDParam().ToString());
        }

        private void btnAutoGenerate_Click(object sender, System.EventArgs e)
        {
            txtAccountPass.Text = Generate();
            txtHdnAccountPass.Text = txtAccountPass.Text;
            hdnAccountPass.Value = "";
            for (int i = 0; i < txtAccountPass.Text.Length; i++) hdnAccountPass.Value += "*";
        }

        private void ddlClientType_SelectedIndexChanged(object sender, System.EventArgs e)
        {
            SetVisibleNameAndPass();
        }

		#endregion

		#region Database
        private void LoadClientTypes()
        {
            ListItem li = new ListItem(Resource.ResourceManager["ListItem_NotSelected"], "-1");
            ddlClientType.Items.Add(li);
            li = new ListItem(Resource.ResourceManager["ListItem_Client"], ((int)ClientTypes.Client).ToString());
            ddlClientType.Items.Add(li);
            li = new ListItem(Resource.ResourceManager["ListItem_Builder"], ((int)ClientTypes.Builder).ToString());
            ddlClientType.Items.Add(li);
            li = new ListItem(Resource.ResourceManager["ListItem_Producer"], ((int)ClientTypes.Producer).ToString());
            ddlClientType.Items.Add(li);
            li = new ListItem(Resource.ResourceManager["ListItem_SuperVisor"], ((int)ClientTypes.Supervisors).ToString());
            ddlClientType.Items.Add(li);
            ddlClientType.SelectedIndex = 0;
        }
        private bool LoadClient(int clientID)
        {
            SqlDataReader reader = null;
            try
            {
                reader = ClientsData.SelectClient(clientID);
                if (reader.Read())
                {
                    txtName.Text = reader.GetString(1);
                    txtCity.Text = reader.IsDBNull(2) ? String.Empty : reader.GetString(2);
                    txtAddress.Text = reader.IsDBNull(3) ? String.Empty : reader.GetString(3);
                    txtEmail.Text = reader.IsDBNull(4) ? String.Empty : reader.GetString(4);
                    txtFax.Text = reader.IsDBNull(6) ? String.Empty : reader.GetString(6);
                    txtManager.Text = reader.IsDBNull(7) ? String.Empty : reader.GetString(7);
                    txtRepr1.Text = reader.IsDBNull(8) ? String.Empty : reader.GetString(8);
                    txtRepr2.Text = reader.IsDBNull(9) ? String.Empty : reader.GetString(9);
                    txtDelo.Text = reader.IsDBNull(10) ? String.Empty : reader.GetString(10);
                    txtBulstat.Text = reader.IsDBNull(11) ? String.Empty : reader.GetString(11);
                    txtNDR.Text = reader.IsDBNull(12) ? String.Empty : reader.GetString(12);
                    txtPhone.Text = reader.IsDBNull(13) ? String.Empty : reader.GetString(13);
                    txtWebsite.Text = reader.IsDBNull(14) ? String.Empty : reader.GetString(14);
                    txtPhone1.Text = reader.IsDBNull(15) ? String.Empty : reader.GetString(15);
                    txtEmail1.Text = reader.IsDBNull(16) ? String.Empty : reader.GetString(16);
                    txtPhone2.Text = reader.IsDBNull(17) ? String.Empty : reader.GetString(17);
                    txtEmail2.Text = reader.IsDBNull(18) ? String.Empty : reader.GetString(18);
                    txtAccountName.Text = reader.IsDBNull(21) ? String.Empty : reader.GetString(21);
                    txtAccountPass.Text = reader.IsDBNull(22) ? String.Empty : reader.GetString(22);
                    txtNameEN.Text = reader.IsDBNull(24) ? String.Empty : reader.GetString(24);
                    txtReprEN.Text = reader.IsDBNull(25) ? String.Empty : reader.GetString(25);
                    txtHdnAccountPass.Text = txtAccountPass.Text;
                    hdnAccountPass.Value = "";
                    for (int i = 0; i < txtAccountPass.Text.Length; i++) hdnAccountPass.Value += "*";
                    OldAccName = txtAccountName.Text;
                    OldAccPass = txtAccountPass.Text;
                    //_accName = txtAccountName.Text;
                    //_accPass = txtAccountPass.Text;


                    if (UIHelpers.GetIDParam() == (int)ClientTypes.Builder)
                    {

                        //						txtEmail2.Visible = Label14.Visible = txtPhone2.Visible = Label13.Visible = 
                        //						txtRepr2.Visible = Label4.Visible = Label12.Visible = txtEmail1.Visible = 
                        //						Label11.Visible = txtRepr1.Visible = Label3.Visible = Label9.Visible = false;



                    }
                    if (UIHelpers.GetIDParam() == (int)ClientTypes.All)
                    {
                        //						Label5.Visible = Label6.Visible = Label7.Visible = lblCity.Visible = 
                        //							lblAddress.Visible = Label1.Visible = Label2.Visible = Label8.Visible = 
                        //							Label15.Visible = Label9.Visible = Label3.Visible = 
                        //							Label10.Visible = Label11.Visible = Label12.Visible = Label4.Visible = 
                        //							Label14.Visible = Label13.Visible = 
                        //							txtCity.Visible = txtAddress.Visible = txtFax.Visible = txtManager.Visible = 
                        //							txtRepr1.Visible = txtRepr2.Visible = txtDelo.Visible = txtBulstat.Visible = 
                        //							txtNDR.Visible = txtPhone.Visible = txtWebsite.Visible = txtPhone1.Visible = 
                        //							txtEmail1.Visible = txtPhone2.Visible = txtEmail2.Visible = false;

                        if (ddlClientType.Items.FindByValue(reader.GetInt32(20).ToString()) != null)
                            ddlClientType.SelectedValue = reader.GetInt32(20).ToString();
                        SetVisibleNameAndPass();
                    }

                }
                else return false;

            }
            catch (Exception ex)
            {
                log.Error(ex);
                return false;
            }
            finally
            {
                if (reader != null) reader.Close();
            }

            return true;
        }

        private bool DeleteClient(int clientID)
        {
            try
            {
                ClientsData.InactiveClient(clientID);
            }
            catch (Exception ex)
            {
                log.Error(ex);
                return false;
            }

            return true;
        }

        private bool UpdateClient(int clientID, string clientName, string city, string address, string email, string fax, string manager, string repr1, string repr2, string delo, string bulstat, string ndr, string phone, string webpage, string phone1, string email1,
            string phone2, string email2, int clientType, string accountName, string accountPass, string sNameEN, string sReprEN)
        {
            try
            {
                ClientsData.UpdateClient(clientID, clientName, city, address, email, fax, manager, repr1, repr2, delo, bulstat, ndr, phone, webpage, phone1, email1,
             phone2, email2, clientType, accountName, accountPass, sNameEN, sReprEN);
            }
            catch (Exception ex)
            {
                log.Error(ex);
                return false;
            }

            return true;
        }

        private bool InsertClient(string clientName, string city, string address, string email, string fax, string manager, string repr1, string repr2, string delo, string bulstat, string ndr, string phone, string webpage, string phone1, string email1,
            string phone2, string email2, int clientType, string accountName, string accountPass, string sNameEN, string sReprEN)
        {
            try
            {
                ClientsData.InsertClient(clientName, city, address, email, true, fax, manager, repr1, repr2, delo, bulstat, ndr, phone, webpage, phone1, email1,
             phone2, email2, clientType, accountName, accountPass, sNameEN, sReprEN);
            }
            catch (Exception ex)
            {
                log.Error(ex);
                return false;
            }

            return true;
        }

		#endregion

		#region private properties 
		new private int ClientID
		{
			get 
			{
				try
				{
					return int.Parse(Request.Params["cid"]);
				}
				catch
				{
					return -1;
				}
			}		
		}
		#endregion

		#region UI

        private void InitEdit()
        {
            if (this.ClientID <= 0) return;

            string cont = "tblForm";
            string vmButtons = this.btnEdit.ClientID + ";" + this.btnEditUP.ClientID;
            string emButtons = this.btnSave.ClientID + ";" + this.btnSaveUP.ClientID + ";" + this.btnDelete.ClientID;

            string exl = String.Empty;

            editCtrl.InitEdit(cont, vmButtons, emButtons, exl);
        }

    #endregion

		#region Menu

//		private void CreateMenu()
//		{
//			UserControls.MenuTable menu = new UserControls.MenuTable();
//			menu.ID = "MenuTable";
//
//			ArrayList menuItems = new ArrayList();
//
//			menuItems.Add(new MenuItemInfo(Resource.ResourceManager["menuItem_Hours"], "Hours.aspx", true));
//			menuItems.Add(new MenuItemInfo(Resource.ResourceManager["menuItem_Projects"], "Projects.aspx"));
//			menuItems.Add(new MenuItemInfo(Resource.ResourceManager["menuItem_Clients"], "Clients.aspx"));
//			menuItems.Add(new MenuItemInfo(Resource.ResourceManager["menuItem_Subcontracters"], "Subcontracters.aspx"));
//
//			if (LoggedUser.IsLeader)
//			menuItems.Add(new MenuItemInfo(Resource.ResourceManager["menuItem_Users"], "Users.aspx"));
//			menuItems.Add(new MenuItemInfo(Resource.ResourceManager["menuItem_Emails"], "Emails.aspx"));
//			
//			menu.AddMenuGroup(Resource.ResourceManager["menuHeader_Links"], 20, menuItems);
//			
//			menuItems = new ArrayList();
//			menuItems.Add(new MenuItemInfo(Resource.ResourceManager["menuItem_NewProject"], "EditProject.aspx"));
//			if (LoggedUser.IsLeader)
//			menuItems.Add(new MenuItemInfo(Resource.ResourceManager["menuItem_NewUser"], "EditUser.aspx"));
//			menuItems.Add(new MenuItemInfo(Resource.ResourceManager["menuItem_NewEmail"], "EditEmail.aspx"));
//			menuItems.Add(new MenuItemInfo(Resource.ResourceManager["newcontracter_PageTitle"], "EditSubContracter.aspx"));
//
//			menu.AddMenuGroup(Resource.ResourceManager["menuHeader_New"], 30, menuItems);
//
//			menuItems = new ArrayList();
//			menuItems.Add(new MenuItemInfo(Resource.ResourceManager["menuItem_WorkTimeReport"], "reports/worktimes.aspx"));
//			menuItems.Add(new MenuItemInfo(Resource.ResourceManager["menuItem_MinutesReport"], "Reports/Minutes.aspx"));
//			if (LoggedUser.IsLeader || LoggedUser.IsAssistant)
//				menuItems.Add(new MenuItemInfo(Resource.ResourceManager["menuItem_MailReport"], "MailForm.aspx"));
//			if (LoggedUser.IsLeader)
//			{
//				menuItems.Add(new MenuItemInfo(Resource.ResourceManager["menuItem_NotEnteredWorkTime"], "reports/EmptyHours.aspx"));
//				menuItems.Add(new MenuItemInfo(Resource.ResourceManager["menuItem_Calendar"], "reports/Calendar.aspx"));
//			}
//
//			menu.AddMenuGroup(Resource.ResourceManager["menuHeader_Reports"], 30, menuItems);
//
//			menuItems = new ArrayList();
//			menuItems.Add(new MenuItemInfo(Resource.ResourceManager["menuItem_Help"], SessionManager.GetHelpLink(),true,Pages.Clients));
//			menu.AddMenuGroup("", 10, menuItems);
//
//			menuHolder.Controls.Add(menu);
//		}

		#endregion

        private void SetVisibleNameAndPass()
        {
            if (ddlClientType.SelectedValue == ((int)ClientTypes.Producer).ToString())
            {
                txtAccountName.Text = txtAccountPass.Text = "";
                lbAccountName.Visible = txtAccountName.Visible = lbAccountPass.Visible = txtAccountPass.Visible = false;
            }
            else
            {
                lbAccountName.Visible = txtAccountName.Visible = lbAccountPass.Visible = txtAccountPass.Visible = true;
            }


        }

	
		#region GeneratePass

		private char[] characterArray;
		private int passwordLength = 6;
		Random randNum = new Random();

		//		public PasswordGenerator()
		//		{
		//			
		//		}

        private char GetRandomCharacter()
        {
            char c = this.characterArray[(int)((this.characterArray.GetUpperBound(0) + 1) * randNum.NextDouble())];
            return c;
        }

        public string Generate()
        {
            characterArray = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789".ToCharArray();
            StringBuilder sb = new StringBuilder();
            sb.Capacity = passwordLength;
            for (int count = 0; count <= passwordLength - 1; count++)
            {
                sb.Append(GetRandomCharacter());
            }
            if ((sb != null))
            {
                string returnValue = sb.ToString();
                return returnValue;
            }
            return string.Empty;
        }

		

		#endregion

		
	}
}
