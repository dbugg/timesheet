using System;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using Asa.Timesheet.Data;

namespace Asa.Timesheet.WebPages.UserControls
{
	
	/// <summary>
	///		Summary description for Documents.
	/// </summary>
	public class Documents : System.Web.UI.UserControl
	{
		protected System.Web.UI.WebControls.DataList dlDocs;
		protected ProfileItems _itemType;
		protected System.Web.UI.HtmlControls.HtmlInputFile FileCtrl;
		protected System.Web.UI.WebControls.Label Label58;
		protected System.Web.UI.WebControls.Label lbDescription;
		protected System.Web.UI.WebControls.TextBox txtDescription;
		protected int _projectID=0;

		private void Page_Load(object sender, System.EventArgs e)
		{
			BindGrid();
		}
		private void BindGrid()
		{
			dlDocs.DataSource=ProjectDocumentDAL.LoadCollection("ProjectDocumentsListProc",SQLParms.CreateProjectDocumentsListProc(_projectID,(int)_itemType));
			dlDocs.DataBind();

		}
		protected string GetURL(int ID)
		{
			return string.Concat("../"+System.Configuration.ConfigurationSettings.AppSettings["ProjectDocs"],ID,System.Configuration.ConfigurationSettings.AppSettings["Extension"]);
		}
		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		///		Required method for Designer support - do not modify
		///		the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.Load += new System.EventHandler(this.Page_Load);

		}
		#endregion
		public ProfileItems ItemType
		{
			get{return _itemType;}
			set {_itemType=value;}
		}
		public int ProjectID
		{
			get{return _projectID;}
			set {_projectID=value;}
		}
		public void Save()
		{
			if(FileCtrl.PostedFile.FileName!=null
				&&FileCtrl.PostedFile.FileName!="")
			{
				
			
				string name = FileCtrl.PostedFile.FileName;
				int n = name.LastIndexOf(".");
				if(n!=-1)
				{
					string ext = name.Substring(n);
					if(ext==System.Configuration.ConfigurationSettings.AppSettings["Extension"])
					{
						ProjectDocumentData pd= new ProjectDocumentData(-1,_projectID,(int)_itemType,txtDescription.Text);
						ProjectDocumentDAL.Save(pd);
						string path = string.Concat(System.Configuration.ConfigurationSettings.AppSettings["ProjectDocs"],pd.ProjectDocumentID,ext);
						string mapped = Request.MapPath(path);

						FileCtrl.PostedFile.SaveAs(mapped);
						
						
					}
				}
			}
		}
	}
	


}
