using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Data.SqlClient;
using Asa.Timesheet.Data;
using Asa.Timesheet.Data.Entities;
using log4net;
using Asa.Timesheet.WebPages.UserControls;
using Asa.Timesheet.Data.Util;
using System.Globalization;
using System.Text;
using System.Configuration;
namespace Asa.Timesheet.WebPages
{
	/// <summary>
	/// Summary description for UIHelpers.
	/// </summary>
	public class UIHelpers
	{
		public static readonly int N_TEXT=10;
		public static readonly int N_FIXED=250;
		public static decimal[] payments  = new decimal[]{0.3M, 0.3M,0.3M,0.1M};
		public static string Get4Replace(string sWhat, string sWith)
		{
			//string[] arrs = sWith.Split('\r');
			sWith=sWith.Replace("\r\n","\\r");
			sWith=sWith.Replace("'","`");
			return string.Concat("docRange.Find.Execute( '",sWhat,"', false, false, false, false, false, true,1, false, '", sWith,"', 2); ");
		}
		public static DateTime GetDate(TextBox t)
		{
			DateTime dt = Constants.DateMax;
			string s= t.Text;
			try
			{
				if(s!="")
					dt=DateTime.ParseExact(s,"dd.MM.yyyy",System.Globalization.NumberFormatInfo.InvariantInfo);
				return dt;
			}
			catch
			{
				
				return dt;
			}
		}
		public static bool LoadBuildingTypes(DropDownList ddlBuildingTypes, string selectedValue)
		{
			SqlDataReader reader = null;
			try
			{
				reader = DBManager.SelectBuildingTypes();
				ddlBuildingTypes.DataSource = reader;
				ddlBuildingTypes.DataValueField = "BuildingTypeID";
				ddlBuildingTypes.DataTextField = "BuildingType";
				ddlBuildingTypes.DataBind();

				ddlBuildingTypes.Items.Insert(0, new ListItem(Resource.ResourceManager["reports_typeBuilding"], "0"));
				ddlBuildingTypes.SelectedValue = selectedValue;
			}
			catch
			{
				
				return false;
			}
			finally 
			{
				if (reader!=null) reader.Close();
			}

			return true;
		}
		public static void CreateMenuReports(PlaceHolder menuHolder, UserInfo LoggedUser)
		{
			UserControls.MenuTable menu = new UserControls.MenuTable();
			menu.ID = "MenuTable";

			// 'Links' group
			ArrayList menuItems = new ArrayList();
			if(LoggedUser.HasWorkTime || LoggedUser.IsAssistantOnly || LoggedUser.HasPaymentRights ||LoggedUser.IsSecretary  )
				menuItems.Add(new MenuItemInfo(Resource.ResourceManager["menuItem_Hours"], "../Hours.aspx", true));
			menuItems.Add(new MenuItemInfo(Resource.ResourceManager["menuItem_Projects"], "../Projects.aspx"));
			//if (LoggedUser.IsLeader || LoggedUser.IsAssistant) 
		{
			menuItems.Add(new MenuItemInfo(Resource.ResourceManager["menuItem_Clients"], "../Clients.aspx"));
			menuItems.Add(new MenuItemInfo(Resource.ResourceManager["menuItem_Subcontracters"], "../Subcontracters.aspx"));
			menuItems.Add(new MenuItemInfo(Resource.ResourceManager["menuItem_Users"], "../Users.aspx"));	
		}
			menuItems.Add(new MenuItemInfo(Resource.ResourceManager["menuItem_Emails"], "../Emails.aspx"));


			if(LoggedUser.HasWorkTime || LoggedUser.IsAssistantOnly || LoggedUser.HasPaymentRights ||LoggedUser.IsSecretary  )
			{
				menuItems.Add(new MenuItemInfo(Resource.ResourceManager["menuItem_Meetings"], "../Meetings.aspx",true));
				menuItems.Add(new MenuItemInfo(Resource.ResourceManager["menuItem_Protokols"], "../Protokols.aspx",true));
			}
			menu.AddMenuGroup(Resource.ResourceManager["menuHeader_Links"], 20, menuItems);
			
			// 'New' group
			menuItems = new ArrayList();
			menuItems.Add(new MenuItemInfo(Resource.ResourceManager["menuItem_NewProject"], "../EditProject.aspx"));
			//if (LoggedUser.IsLeader || LoggedUser.IsAssistant)
		{
			menuItems.Add(new MenuItemInfo(Resource.ResourceManager["menuItem_NewClient"], "../EditClient.aspx"));
			if (LoggedUser.IsLeader || LoggedUser.IsAdmin || LoggedUser.IsAccountant )
				menuItems.Add(new MenuItemInfo(Resource.ResourceManager["menuItem_NewUser"], "../EditUser.aspx"));
		}
			menuItems.Add(new MenuItemInfo(Resource.ResourceManager["menuItem_NewEmail"], "../EditEmail.aspx"));
			menuItems.Add(new MenuItemInfo(Resource.ResourceManager["newcontracter_PageTitle"], "../EditSubContracter.aspx"));

			if(LoggedUser.IsAssistantOnly || LoggedUser.HasPaymentRights ||LoggedUser.IsSecretary)
				menu.AddMenuGroup(Resource.ResourceManager["menuHeader_New"], 30, menuItems);

			// "Reports' group
			menuItems = new ArrayList();
			if(LoggedUser.HasWorkTime || LoggedUser.IsAssistantOnly || LoggedUser.HasPaymentRights ||LoggedUser.IsSecretary  )
			{
				menuItems.Add(new MenuItemInfo(Resource.ResourceManager["menuItem_WorkTimeReport"], "Worktimes.aspx"+"?id=1"));
				menuItems.Add(new MenuItemInfo(Resource.ResourceManager["menuItem_WorkTimeOvertimeReport"], "Worktimes.aspx"+"?id=2",true));
			}
			if (LoggedUser.IsAccountant || LoggedUser.IsLeader) 
				menuItems.Add(new MenuItemInfo(Resource.ResourceManager["menuItem_Absence"], "Absence.aspx"));

			menuItems.Add(new MenuItemInfo(Resource.ResourceManager["menuItem_MinutesReport"], "Minutes.aspx"));

		
			//if (LoggedUser.IsLeader || LoggedUser.IsAssistant)
			menuItems.Add(new MenuItemInfo(Resource.ResourceManager["menuItem_MailReport"], "../MailForm.aspx"));
			
			if (LoggedUser.IsLeader || LoggedUser.IsAdmin || LoggedUser.IsAccountant)
			{
				menuItems.Add(new MenuItemInfo(Resource.ResourceManager["menuItem_NotEnteredWorkTime"], "EmptyHours.aspx"));
				menuItems.Add(new MenuItemInfo(Resource.ResourceManager["menuItem_Calendar"], "Calendar.aspx"));
			}
			menuItems.Add(new MenuItemInfo(Resource.ResourceManager["reports_Folders_Title"], "Folders.aspx"));
			menuItems.Add(new MenuItemInfo(Resource.ResourceManager["reports_ContentFinished_Title"], "BuildingPercentFinished.aspx",true));
			menu.AddMenuGroup(Resource.ResourceManager["menuHeader_Reports"], 30, menuItems);
			if(LoggedUser.HasPaymentRights)
			{
				menuItems = new ArrayList();
				menuItems.Add(new MenuItemInfo(Resource.ResourceManager["menuItem_ProjectAnalysis"], "ProjectAnalysis.aspx"));
				menuItems.Add(new MenuItemInfo(Resource.ResourceManager["menuItem_ClientAnalysis"], "ClientAnalysis.aspx"));
				menuItems.Add(new MenuItemInfo(Resource.ResourceManager["menuItem_SubcontracterAnalysis"], "SubcontracterAnalysis.aspx"));
				menuItems.Add(new MenuItemInfo(Resource.ResourceManager["menuItem_DDSAnalysis"], "DDSAnalysis.aspx"));
				menuItems.Add(new MenuItemInfo(Resource.ResourceManager["menuItem_ASI"], "SubcontracterAnalysis.aspx?Type=ASI"));
				menuItems.Add(new MenuItemInfo(Resource.ResourceManager["menuItem_AN"], "Minutes.aspx?Type=AN"));
				menuItems.Add(new MenuItemInfo(Resource.ResourceManager["menuItem_FeeAccount"], "FeeAccount.aspx",true));

				menu.AddMenuGroup(Resource.ResourceManager["menuHeader_Analysis"], 30, menuItems);
			}	
			if(LoggedUser.IsASI)
			{
				menuItems = new ArrayList();
				menuItems.Add(new MenuItemInfo(Resource.ResourceManager["menuItem_ASI"], "SubcontracterAnalysis.aspx?Type=ASI"));
				menu.AddMenuGroup(Resource.ResourceManager["menuHeader_Analysis"], 30, menuItems);
	
			}
			menuItems = new ArrayList();
			//menuItems.Add(new MenuItemInfo(Resource.ResourceManager["menuItem_Docs"], "images/ASA_ISO.pdf",true,Pages.Projects));
			menuItems.Add(new MenuItemInfo(Resource.ResourceManager["menuItem_Help"], SessionManager.GetReportsHelpLink(),true,GetPageFromRequest()));
			menu.AddMenuGroup("", 10, menuItems);


			menuHolder.Controls.Add(menu);
		}
		public static string GetPageFromRequest()
		{
			string sPath = HttpContext.Current.Request.Path;
			string sAppPath = HttpContext.Current.Request.ApplicationPath;
			string sPage  = sPath.Remove(0, sAppPath.Length+1);
			int nTill = sPage.IndexOf(".aspx");
			sPage = sPage.Substring(0,nTill);
			
			return sPage;
			
		}
		public static void CreateMenu(PlaceHolder menuHolder, UserInfo LoggedUser)
		{
			UserControls.MenuTable menu = new UserControls.MenuTable();
			menu.ID = "MenuTable";

			// 'Links' group
			ArrayList menuItems = new ArrayList();
			if(LoggedUser.HasWorkTime || LoggedUser.IsAssistantOnly || LoggedUser.HasPaymentRights ||LoggedUser.IsSecretary  )
				menuItems.Add(new MenuItemInfo(Resource.ResourceManager["menuItem_Hours"], "Hours.aspx", true));
			menuItems.Add(new MenuItemInfo(Resource.ResourceManager["menuItem_Projects"], "Projects.aspx"));
			menuItems.Add(new MenuItemInfo(Resource.ResourceManager["menuItem_Clients"], "Clients.aspx"));
			menuItems.Add(new MenuItemInfo(Resource.ResourceManager["menuItem_Subcontracters"], "Subcontracters.aspx"));
			
			//if (LoggedUser.IsLeader || LoggedUser.IsAdmin)
			menuItems.Add(new MenuItemInfo(Resource.ResourceManager["menuItem_Users"], "Users.aspx"));
			menuItems.Add(new MenuItemInfo(Resource.ResourceManager["menuItem_Emails"], "Emails.aspx"));
			if(LoggedUser.HasWorkTime || LoggedUser.IsAssistantOnly || LoggedUser.HasPaymentRights ||LoggedUser.IsSecretary  )
			{
				menuItems.Add(new MenuItemInfo(Resource.ResourceManager["menuItem_Meetings"], "Meetings.aspx",true));
				menuItems.Add(new MenuItemInfo(Resource.ResourceManager["menuItem_Protokols"], "Protokols.aspx",true));
			}

			menu.AddMenuGroup(Resource.ResourceManager["menuHeader_Links"], 20, menuItems);
			
			// 'New' grop
			menuItems = new ArrayList();
			menuItems.Add(new MenuItemInfo(Resource.ResourceManager["menuItem_NewProject"], "editproject.aspx"));
			menuItems.Add(new MenuItemInfo(Resource.ResourceManager["menuItem_NewClient"], "editclient.aspx"));
			if (LoggedUser.IsLeader || LoggedUser.IsAdmin || LoggedUser.IsAccountant )
				menuItems.Add(new MenuItemInfo(Resource.ResourceManager["menuItem_NewUser"], "EditUser.aspx"));
			menuItems.Add(new MenuItemInfo(Resource.ResourceManager["menuItem_NewEmail"], "editemail.aspx"));
			menuItems.Add(new MenuItemInfo(Resource.ResourceManager["newcontracter_PageTitle"], "EditSubContracter.aspx"));

			if(LoggedUser.IsAssistantOnly || LoggedUser.HasPaymentRights ||LoggedUser.IsSecretary)
				menu.AddMenuGroup(Resource.ResourceManager["menuHeader_New"], 30, menuItems);

			// 
			menuItems = new ArrayList();
			if(LoggedUser.HasWorkTime || LoggedUser.IsAssistantOnly || LoggedUser.HasPaymentRights ||LoggedUser.IsSecretary  )
			{
				menuItems.Add(new MenuItemInfo(Resource.ResourceManager["menuItem_WorkTimeReport"], "reports/worktimes.aspx" + "?id=1"));
				menuItems.Add(new MenuItemInfo(Resource.ResourceManager["menuItem_WorkTimeOvertimeReport"], "reports/worktimes.aspx"+"?id=2",true));
			}
			if (LoggedUser.IsAccountant || LoggedUser.IsLeader) 
				menuItems.Add(new MenuItemInfo(Resource.ResourceManager["menuItem_Absence"], "Reports/Absence.aspx"));
			
			menuItems.Add(new MenuItemInfo(Resource.ResourceManager["menuItem_MinutesReport"], "Reports/Minutes.aspx"));
			
			//if (LoggedUser.IsLeader || LoggedUser.IsAssistant)
			menuItems.Add(new MenuItemInfo(Resource.ResourceManager["menuItem_MailReport"], "MailForm.aspx"));
			if (LoggedUser.IsLeader || LoggedUser.IsAdmin || LoggedUser.IsAccountant)
			{
				menuItems.Add(new MenuItemInfo(Resource.ResourceManager["menuItem_NotEnteredWorkTime"], "reports/EmptyHours.aspx"));
				menuItems.Add(new MenuItemInfo(Resource.ResourceManager["menuItem_Calendar"], "reports/Calendar.aspx"));

			}
			menuItems.Add(new MenuItemInfo(Resource.ResourceManager["reports_Folders_Title"], "Reports/Folders.aspx"));;
			menuItems.Add(new MenuItemInfo(Resource.ResourceManager["reports_ContentFinished_Title"], "Reports/BuildingPercentFinished.aspx",true));
			

			menu.AddMenuGroup(Resource.ResourceManager["menuHeader_Reports"], 30, menuItems);
			if(LoggedUser.HasPaymentRights)
				UIHelpers.AddMenuAnalysis(menu);
			if(LoggedUser.IsASI)
			{
				menuItems = new ArrayList();
				menuItems.Add(new MenuItemInfo(Resource.ResourceManager["menuItem_ASI"], "Reports/SubcontracterAnalysis.aspx?Type=ASI"));
				menu.AddMenuGroup(Resource.ResourceManager["menuHeader_Analysis"], 30, menuItems);
			}
			menuItems = new ArrayList();
		//	menuItems.Add(new MenuItemInfo(Resource.ResourceManager["menuItem_Docs"], "images/ASA_ISO.pdf",true,Pages.Projects));
			menuItems.Add(new MenuItemInfo(Resource.ResourceManager["menuItem_Help"], SessionManager.GetHelpLink(),true,GetPageFromRequest()));
			menu.AddMenuGroup("", 10, menuItems);

			menuHolder.Controls.Add(menu);
		}
		public static void AddMenuAnalysis(MenuTable menu)
		{
			ArrayList menuItems = new ArrayList();
			menuItems.Add(new MenuItemInfo(Resource.ResourceManager["menuItem_ProjectAnalysis"], "Reports/ProjectAnalysis.aspx"));
			menuItems.Add(new MenuItemInfo(Resource.ResourceManager["menuItem_ClientAnalysis"], "Reports/ClientAnalysis.aspx"));
			menuItems.Add(new MenuItemInfo(Resource.ResourceManager["menuItem_SubcontracterAnalysis"], "Reports/SubcontracterAnalysis.aspx"));
			menuItems.Add(new MenuItemInfo(Resource.ResourceManager["menuItem_DDSAnalysis"], "Reports/DDSAnalysis.aspx"));
			menuItems.Add(new MenuItemInfo(Resource.ResourceManager["menuItem_ASI"], "Reports/SubcontracterAnalysis.aspx?Type=ASI"));
			menuItems.Add(new MenuItemInfo(Resource.ResourceManager["menuItem_AN"], "Reports/Minutes.aspx?Type=AN"));
			menuItems.Add(new MenuItemInfo(Resource.ResourceManager["menuItem_FeeAccount"], "Reports/FeeAccount.aspx",true));

			menu.AddMenuGroup(Resource.ResourceManager["menuHeader_Analysis"], 30, menuItems);
			
		}
		public static decimal ParseDecimal(string s)
		{
			if(s==null || s=="")
				return 0;
			s=s.Replace(".",",");
			decimal d=0;
			try
			{
				d=decimal.Parse(s);
			}
			catch
			{
				TimesheetPageBase.ErrorRedirect(Resource.ResourceManager["errorParse"]);
			}
			return d;
		}
		public static string Get4ReplacePlus(string sWhat, string sWith)
		{
			//string[] arrs = sWith.Split('\r');
			
			
			string[] s = new string[N_TEXT+1];
			int n=sWith.Length/N_FIXED;
			int nCur=0;
			sWith=sWith.Replace("'","`");
			sWith=sWith.Replace("\r\n","|");
			for(int i=0;i<n;i++)
			{
				if(nCur+N_FIXED>=sWith.Length)
				{
					s[i]=sWith.Substring(nCur);
					nCur=sWith.Length;
					s[i]=s[i].Replace("|","\\r");
					break;
				}
				else
				{
					s[i] = sWith.Substring(nCur,N_FIXED);
					nCur+=N_FIXED;
				}
				s[i]=s[i].Replace("|","\\r");
				
			}
			if(nCur<sWith.Length-1)
			{
				s[n]=sWith.Substring(nCur).Replace("|","\\r");
			}
			string sRet="";
			for(int i=0;i<s.Length;i++)
			{
				sRet+=string.Concat("docRange.Find.Execute( '",sWhat+i.ToString()+"}","', false, false, false, false, false, true,1, false, '", s[i],"', 2); ");
			}
			return sRet;
		}
		
		public static void FrontPage(System.Web.UI.Page page,string name, string investor,string addinfo,string code)
		{
			
			
			string DocumentPath= ConfigurationSettings.AppSettings["DocumentPath"];
			//string sPath = HttpContext.Current.Request.MapPath(DocumentPath + Constants._ORDERADD +((Statuses)md.StatusID).ToString()+Constants._DOT);
			string oTemplate = DocumentPath + Constants.FRONT_PAGE ;

			StringBuilder scr =new StringBuilder();
			scr.Append("<script language=\"javascript\">");
			scr.Append("onload=showdoc;");
			scr.Append("function showdoc(){");
			scr.Append("var word = new ActiveXObject('Word.Application');");
			scr.Append("word.Visible = true;");
			scr.Append("var t=word.Documents.Add('");
			scr.Append(oTemplate);
			scr.Append("', false, 0);   ");
			scr.Append("var docRange = t.Range();");
			scr.Append(UIHelpers.Get4Replace(Constants._PROJECT,name));
			scr.Append(UIHelpers.Get4Replace(Constants._INVESTOR,investor));
			scr.Append(UIHelpers.Get4Replace(Constants._ADDINFO,addinfo));
			scr.Append(UIHelpers.Get4Replace(Constants._CODE,code));
			
			
			
			scr.Append("}");
			scr.Append("</" + "script>");
			
			page.RegisterClientScriptBlock("showdoc", scr.ToString());

			
			
		}
		public static void Meeting(System.Web.UI.Page page,string project, string investor,string date,string manager,string user,string text,string sClients,string sSubs,string sUsers,string address)
		{
			
			
			string DocumentPath= ConfigurationSettings.AppSettings["DocumentPath"];
			//string sPath = HttpContext.Current.Request.MapPath(DocumentPath + Constants._ORDERADD +((Statuses)md.StatusID).ToString()+Constants._DOT);
			string oTemplate = DocumentPath + Constants.MEETING ;

			StringBuilder scr =new StringBuilder();
			scr.Append("<script language=\"javascript\">");
			scr.Append("onload=showdoc;");
			scr.Append("function showdoc(){");
			scr.Append("var word = new ActiveXObject('Word.Application');");
			scr.Append("word.Visible = true;");
			scr.Append("var t=word.Documents.Add('");
			scr.Append(oTemplate);
			scr.Append("', false, 0);   ");
			scr.Append("var docRange = t.Range();");
			scr.Append(UIHelpers.Get4Replace(Constants._PROJECT,project));
			scr.Append(UIHelpers.Get4Replace(Constants._INVESTOR,investor));
			scr.Append(UIHelpers.Get4Replace(Constants._DATE,date));
			scr.Append(UIHelpers.Get4Replace(Constants._MANAGER,manager));
			scr.Append(UIHelpers.Get4Replace(Constants._USER,user));
			scr.Append(UIHelpers.Get4Replace(Constants._USERS,sUsers));
			scr.Append(UIHelpers.Get4Replace(Constants._SUBCONTRACTERS,sSubs));
			scr.Append(UIHelpers.Get4Replace(Constants._CLIENTS,sClients));
			scr.Append(UIHelpers.Get4Replace(Constants._ADDRESS,address));
			scr.Append(UIHelpers.Get4ReplacePlus(Constants._TEXT,text));
			
			scr.Append("}");
			scr.Append("</" + "script>");
			
			page.RegisterClientScriptBlock("showdoc", scr.ToString());

			
			
		}
		public static void Subcontracter(System.Web.UI.Page page,string project, string subcontracter,string manager,string address,string lastname)
		{
			
			
			string DocumentPath= ConfigurationSettings.AppSettings["DocumentPath"];
			//string sPath = HttpContext.Current.Request.MapPath(DocumentPath + Constants._ORDERADD +((Statuses)md.StatusID).ToString()+Constants._DOT);
			string oTemplate = DocumentPath + Constants.BLANKAS ;

			StringBuilder scr =new StringBuilder();
			scr.Append("<script language=\"javascript\">");
			scr.Append("onload=showdoc;");
			scr.Append("function showdoc(){");
			scr.Append("var word = new ActiveXObject('Word.Application');");
			scr.Append("word.Visible = true;");
			scr.Append("var t=word.Documents.Add('");
			scr.Append(oTemplate);
			scr.Append("', false, 0);   ");
			scr.Append("var docRange = t.Range();");
			scr.Append(UIHelpers.Get4Replace(Constants._PROJECT,project));
			scr.Append(UIHelpers.Get4Replace(Constants._SUBCONTRACTER,subcontracter));
			
			scr.Append(UIHelpers.Get4Replace(Constants._ADDRESS,address));
			scr.Append(UIHelpers.Get4Replace(Constants._LASTNAME,lastname));
			scr.Append(UIHelpers.Get4Replace(Constants._MANAGER,manager));
			
			scr.Append("}");
			scr.Append("</" + "script>");
			
			page.RegisterClientScriptBlock("showdoc", scr.ToString());

			
			
		}
		public static void Udo(System.Web.UI.Page page,string name,string addinfo)
		{
			
			
			string DocumentPath= ConfigurationSettings.AppSettings["DocumentPath"];
			//string sPath = HttpContext.Current.Request.MapPath(DocumentPath + Constants._ORDERADD +((Statuses)md.StatusID).ToString()+Constants._DOT);
			string oTemplate = DocumentPath + Constants.UDO ;

			StringBuilder scr =new StringBuilder();
			scr.Append("<script language=\"javascript\">");
			scr.Append("onload=showdoc;");
			scr.Append("function showdoc(){");
			scr.Append("var word = new ActiveXObject('Word.Application');");
			scr.Append("word.Visible = true;");
			scr.Append("var t=word.Documents.Add('");
			scr.Append(oTemplate);
			scr.Append("', false, 0);   ");
			scr.Append("var docRange = t.Range();");
			scr.Append(UIHelpers.Get4Replace(Constants._PROJECT,name));
			scr.Append(UIHelpers.Get4Replace(Constants._ADDRESS,addinfo));
			
			
			
			scr.Append("}");
			scr.Append("</" + "script>");
			
			page.RegisterClientScriptBlock("showdoc", scr.ToString());

			
			
		}

		public static void Protokol(bool first, System.Web.UI.Page page,string name, string investor,string manager, string area,string addinfo, string Papki)
		{
			
			
			string DocumentPath= ConfigurationSettings.AppSettings["DocumentPath"];
			//string sPath = HttpContext.Current.Request.MapPath(DocumentPath + Constants._ORDERADD +((Statuses)md.StatusID).ToString()+Constants._DOT);
			string oTemplate = DocumentPath + Constants.PROTOKOL1 ;
			if(!first)
				oTemplate = DocumentPath + Constants.PROTOKOL2 ;
			StringBuilder scr =new StringBuilder();
			scr.Append("<script language=\"javascript\">");
			scr.Append("onload=showdoc;");
			scr.Append("function showdoc(){");
			scr.Append("var word = new ActiveXObject('Word.Application');");
			scr.Append("word.Visible = true;");
			scr.Append("var t=word.Documents.Add('");
			scr.Append(oTemplate);
			scr.Append("', false, 0);   ");
			scr.Append("var docRange = t.Range();");
			scr.Append(UIHelpers.Get4Replace(Constants._PROJECT,name));
			scr.Append(UIHelpers.Get4Replace(Constants._INVESTOR,investor));
			scr.Append(UIHelpers.Get4Replace(Constants._MANAGER,manager));
			scr.Append(UIHelpers.Get4Replace(Constants._AREA,area));
			scr.Append(UIHelpers.Get4Replace(Constants._ADDRESS,addinfo));
			scr.Append(UIHelpers.Get4Replace(Constants._PAPKI,Papki));
			
			
			
			scr.Append("}");
			scr.Append("</" + "script>");
			
			page.RegisterClientScriptBlock("showdoc", scr.ToString());

			
			
		}
		public static void LetterToClient(System.Web.UI.Page page,string name, string investor, string address, string user)
		{
			
			
			string DocumentPath= ConfigurationSettings.AppSettings["DocumentPath"];
			//string sPath = HttpContext.Current.Request.MapPath(DocumentPath + Constants._ORDERADD +((Statuses)md.StatusID).ToString()+Constants._DOT);
			string oTemplate = DocumentPath + Constants.LetterToClient ;

			StringBuilder scr =new StringBuilder();
			scr.Append("<script language=\"javascript\">");
			scr.Append("onload=showdoc;");
			scr.Append("function showdoc(){");
			scr.Append("var word = new ActiveXObject('Word.Application');");
			scr.Append("word.Visible = true;");
			scr.Append("var t=word.Documents.Add('");
			scr.Append(oTemplate);
			scr.Append("', false, 0);   ");
			scr.Append("var docRange = t.Range();");
			scr.Append(UIHelpers.Get4Replace(Constants._PROJECT,name));
			scr.Append(UIHelpers.Get4Replace(Constants._INVESTOR,investor));
			scr.Append(UIHelpers.Get4Replace(Constants._ADDRESS,address));
			scr.Append(UIHelpers.Get4Replace(Constants._USER,user));
			
			
			
			scr.Append("}");
			scr.Append("</" + "script>");
			
			page.RegisterClientScriptBlock("showdoc", scr.ToString());

			
			
		}
		public static void Reg(System.Web.UI.Page page,string name, string client, string address, string user)
		{
			
			
			string DocumentPath= ConfigurationSettings.AppSettings["DocumentPath"];
			//string sPath = HttpContext.Current.Request.MapPath(DocumentPath + Constants._ORDERADD +((Statuses)md.StatusID).ToString()+Constants._DOT);
			string oTemplate = DocumentPath + Constants.DOGOVOR ;

			StringBuilder scr =new StringBuilder();
			scr.Append("<script language=\"javascript\">");
			scr.Append("onload=showdoc;");
			scr.Append("function showdoc(){");
			scr.Append("var word = new ActiveXObject('Word.Application');");
			scr.Append("word.Visible = true;");
			scr.Append("var t=word.Documents.Add('");
			scr.Append(oTemplate);
			scr.Append("', false, 0);   ");
			scr.Append("var docRange = t.Range();");
			scr.Append(UIHelpers.Get4Replace(Constants._PROJECT,name));
			scr.Append(UIHelpers.Get4Replace(Constants._CLIENT,client));
			scr.Append(UIHelpers.Get4Replace(Constants._ADDRESS,address));
			scr.Append(UIHelpers.Get4Replace(Constants._USER,user));
			
			
			
			scr.Append("}");
			scr.Append("</" + "script>");
			
			page.RegisterClientScriptBlock("showdoc", scr.ToString());

			
			
		}
		//		public static void Clear(System.Web.UI.Page page)
		//		{
		//			StringBuilder scr =new StringBuilder();
		//			scr.Append("<script language=\"javascript\">");
		//			scr.Append("onload=showdoc;");
		//			scr.Append("function close(){document.close();");
		//			
		//			
		//			
		//			scr.Append("}");
		//			scr.Append("</" + "script>");
		//			
		//			page.RegisterClientScriptBlock("showdoc", scr.ToString());
		//		}
		public static void Note(System.Web.UI.Page page,string name, string addinfo,string text, string user)
		{
			
			
			string DocumentPath= ConfigurationSettings.AppSettings["DocumentPath"];
			//string sPath = HttpContext.Current.Request.MapPath(DocumentPath + Constants._ORDERADD +((Statuses)md.StatusID).ToString()+Constants._DOT);
			string oTemplate = DocumentPath + Constants.NOTES ;

			StringBuilder scr =new StringBuilder();
			scr.Append("<script language=\"javascript\">");
			scr.Append("onload=showdoc;");
			scr.Append("function showdoc(){");
			scr.Append("var word = new ActiveXObject('Word.Application');");
			scr.Append("word.Visible = true;");
			scr.Append("var t=word.Documents.Add('");
			scr.Append(oTemplate);
			scr.Append("', false, 0);   ");
			scr.Append("var docRange = t.Range();");
			scr.Append(UIHelpers.Get4Replace(Constants._PROJECT,name));
			
		
			scr.Append(UIHelpers.Get4ReplacePlus(Constants._TEXT,text));
			scr.Append(UIHelpers.Get4Replace(Constants._USER,user));
			scr.Append(UIHelpers.Get4Replace(Constants._ADDINFO,addinfo));
			scr.Append("}");
			scr.Append("</" + "script>");
			
			page.RegisterClientScriptBlock("showdoc", scr.ToString());

			
			
		}
		
		public static void Fax(System.Web.UI.Page page,string name, string addinfo,string client, string fax,string user)
		{
			
			
			string DocumentPath= ConfigurationSettings.AppSettings["DocumentPath"];
			//string sPath = HttpContext.Current.Request.MapPath(DocumentPath + Constants._ORDERADD +((Statuses)md.StatusID).ToString()+Constants._DOT);
			string oTemplate = DocumentPath + Constants.FAX_COVER ;

			StringBuilder scr =new StringBuilder();
			scr.Append("<script language=\"javascript\">");
			scr.Append("onload=showdoc;");
			scr.Append("function showdoc(){");
			scr.Append("var word = new ActiveXObject('Word.Application');");
			scr.Append("word.Visible = true;");
			scr.Append("var t=word.Documents.Add('");
			scr.Append(oTemplate);
			scr.Append("', false, 0);   ");
			scr.Append("var docRange = t.Range();");
			scr.Append(UIHelpers.Get4Replace(Constants._PROJECT,name));
			scr.Append(UIHelpers.Get4Replace(Constants._DATE,TimeHelper.FormatDate(DateTime.Now)));
			scr.Append(UIHelpers.Get4Replace(Constants._CLIENT,client));
			scr.Append(UIHelpers.Get4Replace(Constants._FAX,fax));
			scr.Append(UIHelpers.Get4Replace(Constants._USER,user));
			scr.Append(UIHelpers.Get4Replace(Constants._ADDINFO,addinfo));
			scr.Append("}");
			scr.Append("</" + "script>");
			
			page.RegisterClientScriptBlock("showdoc", scr.ToString());

			
			
		}
		public static int GetSIDParam()
		{
			int nID=-1;
			string sID= HttpContext.Current.Request.Params["sid"];
			if(sID!=null && sID!="")
			{
				nID= ToInt(sID);
			}
			return nID;
		}
		public static int GetPIDParam()
		{
			int nID=-1;
			string sID= HttpContext.Current.Request.Params["pid"];
			if(sID!=null && sID!="")
			{
				nID= ToInt(sID);
			}
			return nID;
		}
		public static int GetIDParam()
		{
			int nID=-1;
			string sID= HttpContext.Current.Request.Params["id"];
			if(sID!=null && sID!="")
			{
				nID= ToInt(sID);
			}
			return nID;
		}
		/// <summary>
		/// Converts to int without throwing excpetion if unsuccess
		/// </summary>
		/// <param name="sID">String to parse</param>
		/// <returns>The parsed integer or -1 if unsuccess</returns>
		public static int ToInt(string sID)
		{
			double resNum;
			if(!Double.TryParse(sID, NumberStyles.Integer,NumberFormatInfo.InvariantInfo,out resNum))
				return -1;
			return (int) resNum;
		}
		/// <summary>
		/// Converts to double without throwing excpetion if unsuccess
		/// </summary>
		/// <param name="sID">String to parse</param>
		/// <returns>The parsed double or -1 if unsuccess</returns>
		public static double ToDouble(string sID)
		{
			double resNum;
			if(!Double.TryParse(sID, NumberStyles.Float,NumberFormatInfo.InvariantInfo,out resNum))
				return 0;
			return resNum;
		}
		public static PaymentsVector GetNewPaymentsTable(decimal total)
		{
			//			DataTable dt = new DataTable();
			//			dt.Columns.Add(new DataColumn("PaymentPercent", typeof(decimal)));
			//			dt.Columns.Add(new DataColumn("Amount", typeof(decimal)));
			//			dt.Columns.Add(new DataColumn("PaymentDate", typeof(DateTime)));
			PaymentsVector pv= new PaymentsVector();
			foreach(decimal d in payments)
			{
				PaymentData pd = new PaymentData(-1,-1,-1,d*100,d*total,false,new DateTime(),0,false);
				pv.Add(pd);
			}
			return pv;
			//				DataRow dr = dt.NewRow();	
			//				dr[0]=(d*100);
			//				dr[1]=d*total;
			//				dt.Rows.Add(dr);
			//			}
			//
			//			dt.AcceptChanges();
			//			return dt;
		}
		public static string FormatDecimal(decimal d)
		{
			NumberFormatInfo nfi = new CultureInfo( "bg-BG", false ).NumberFormat;
			nfi.NumberDecimalDigits = 0;
			return d.ToString("n",nfi);
		}
		public static string FormatDecimal2(decimal d)
		{
			NumberFormatInfo nfi = new CultureInfo( "bg-BG", false ).NumberFormat;
			nfi.NumberDecimalDigits = 2;
			return d.ToString("n",nfi);
		}
		public static string FormatDecimal4(decimal d)
		{
			NumberFormatInfo nfi = new CultureInfo( "bg-BG", false ).NumberFormat;
			nfi.NumberDecimalDigits = 8;
			return d.ToString("n",nfi);
		}
		public static string FormatDecimaln(decimal d)
		{
			NumberFormatInfo nfi = new CultureInfo( "bg-BG", false ).NumberFormat;
			nfi.NumberDecimalDigits = 8;
			return d.ToString("n",nfi).TrimEnd('0').TrimEnd(',');
		}
		
		public static void AddValueByID(string sID,ListBox lsFrom,ListBox lsTo)
		{
			ListItem li;
			for(int i=0;i<lsFrom.Items.Count;i++)
				if(lsFrom.Items[i].Value==sID)
				{
					li = lsFrom.Items[i];
					lsTo.Items.Add(li);
					lsFrom.Items.Remove(li);
					return;
				}
		}
		public static bool FirstSelect(ListBox ls, Label lb)
		{
			if(ls.SelectedItem==null)
			{
				lb.Text="Please select an item first.";
				return true;
			}
			lb.Text="";
			return false;
		}
		public static void SetSelected(string sel,ListBox lsPersons,ListBox lsPersonsSel)
		{
			string[] ss = sel.Split(';');
			foreach(string s in ss)
				AddValueByID(s,lsPersons,lsPersonsSel);
			
		}
		public static string GetSelected(ListBox lsProjectsSel)
		{
			StringBuilder sb = new StringBuilder();
			for(int i=0;i<lsProjectsSel.Items.Count;i++)
			{
				sb.Append(lsProjectsSel.Items[i].Value);
				sb.Append(',');
			}
			string ret = sb.ToString();
			ret = ret.TrimEnd(new char[] {';'});
			return ret;
		}
		public static ArrayList AddSelectedListBoxValues(ListBox lsFrom,ListBox lsTo)
		{
			ArrayList al = new ArrayList();
			for(int i=0;i<lsFrom.Items.Count;i++)
				if(lsFrom.Items[i].Selected==true)
				{
					lsTo.Items.Add(lsFrom.Items[i]);
					al.Add(lsFrom.Items[i]);
				}
			for(int i=0;i<al.Count;i++)
				lsFrom.Items.Remove((ListItem)al[i]);
			lsFrom.SelectedIndex=-1;
			lsTo.SelectedIndex=-1;
			return al;
		}
		
	}
	public enum BuildingGridTitleNames1
	{
		A,
		B,
		C,
		D,
		E,
		F,
		G,
		H,
		I,
		J,
		K
	}
}
