using System;
using System.Data;
using Asa.Timesheet.Data.Reports;
using Asa.Timesheet.Data;
using System.Collections;
using GrapeCity.ActiveReports;
using GrapeCity.ActiveReports.Controls;
using GrapeCity.ActiveReports.SectionReportModel;
using GrapeCity.ActiveReports.Document.Section;
using GrapeCity.ActiveReports.Document;

namespace Asa.Timesheet.WebPages.Reports
{
    public class Pokazateli : GrapeCity.ActiveReports.SectionReport
	{
		int _projectID = 0;
		int _building = 0;
		int _pos = 0;
		string _userName = String.Empty;
		string _projectName = String.Empty;
		bool _isRtf = false;
		ChecksVector _checks = null;
		string _phase = "";
        public Pokazateli(int projectID, string userName, int building, bool isRtf, ChecksVector checks, string phase)
        {
            _projectID = projectID;
            _userName = userName;
            _building = building;
            _isRtf = isRtf;
            _checks = checks;
            _phase = phase;
            InitializeComponent();
            ReportFooter.KeepTogether = true;
            if (isRtf)
                lblPageFooter.Visible = txtPageHeader.Visible = false;
        }


        private void Pokazateli_ReportStart(object sender, System.EventArgs eArgs)
        {
            DataSet dsReport = PokazateliReportData.SelectPokazateliReportData(_projectID, _building);
            if (_phase != "")
            {
                for (int i = 0; i < dsReport.Tables[1].Rows.Count; i++)
                {
                    string code = dsReport.Tables[1].Rows[i][8].ToString();
                    if (code.Length != 0)
                    {
                        code = string.Concat(_phase, ".", code);
                        dsReport.Tables[1].Rows[i][8] = code;
                    }
                }

            }

            this.DataSource = dsReport.Tables[0];
            bool IsCode = false;
            foreach (DataRow dr in dsReport.Tables[1].Rows)
            {
                if (dr[8].ToString() != "")
                {
                    int _cid = int.Parse(dr[1].ToString());
                    int index = _checks.IndexOf(_cid, true);
                    if (index == -1)
                    { IsCode = true; break; }
                    bool ischecked = _checks[index].IsChecked;
                    if (ischecked)
                    { IsCode = true; break; }
                }
            }
            if (_building <= 0)
                this.SubReport1.Report = new PokazateliSubreport1(IsCode, _checks);
            else
                this.SubReport1.Report = new PokazateliSubreport(IsCode, _checks);
            //			string sign="";
            //			if (dsReport.Tables[1].Rows.Count > 0)
            //			{
            //				if(dsReport.Tables[1].Rows[0]["Signature"]!=DBNull.Value)
            //					sign=(string)dsReport.Tables[1].Rows[0]["Signature"];
            //			}
            //			if(sign!="")
            //				lbSub.Text=sign;
            //			else
            //				lbSub.Visible=lbSubLabel.Visible=false;
        }

        private void Pokazateli_FetchData(object sender, FetchEventArgs eArgs)
        {
            if (eArgs.EOF) return;
            DataTable dt = (DataTable)this.DataSource;

            decimal area = dt.Rows[_pos].IsNull("Area") ? 0 : (decimal)dt.Rows[_pos]["Area"];

            DataRow[] alrows = dt.Rows[_pos].GetChildRows("Rel1");
            if (_building <= 0)
            {
                ArrayList al = new ArrayList();
                foreach (DataRow ff in alrows)
                {
                    int contentType = (int)ff["ContentTypeID"];

                    if (contentType != (int)PokazateliReportData.ContentTypes.KvadraturaNaChertej)

                        al.Add(ff);
                }
                alrows = (DataRow[])al.ToArray(typeof(DataRow));

            }
            this.SubReport1.Report.DataSource = alrows;

            DataSet dsReport = PokazateliReportData.SelectPokazateliReportData(_projectID, -1);

            DataRow[] drs = dsReport.Tables[0].Rows[_pos].GetChildRows("Rel1");
            decimal kv = 0;
            decimal parterArea = 0;
            decimal rzpAbove = 0;
            decimal area1 = 0;

            //DataSet dsReport = PokazateliReportData.SelectPokazateliReportData(_projectID,-1);
            bool IsParter = isParter(drs);
            foreach (DataRow ff in drs)
            {
                decimal arr = (decimal)ff["Area"];
                int contentType = (int)ff["ContentTypeID"];
                if (IsParter)
                { if (contentType == (int)PokazateliReportData.ContentTypes.Parter) parterArea += arr; }
                else
                { if (contentType == (int)PokazateliReportData.ContentTypes.Etaj) parterArea += arr; }
                if ((contentType != (int)PokazateliReportData.ContentTypes.Suteren) &&
                    (contentType != (int)PokazateliReportData.ContentTypes.KvadraturaNaChertej)) rzpAbove += arr;

                if (contentType == (int)PokazateliReportData.ContentTypes.KvadraturaNaChertej &&
                    (int)ff["BuildingNumber"] == 1)
                    kv = arr;
                else
                    area1 += arr;
            }

            ContentsVector cv1 = ContentDAL.LoadCollection("ContentsList0Proc", SQLParms.CreateContentsList0Proc(_projectID));

            //DistrTable=cv1;
            int nMax = cv1.GetMaxBuilding();
            if (_building <= 0)
            {
                ((PokazateliSubreport1)this.SubReport1.Report).Reset(area1, kv, parterArea, rzpAbove, nMax);
            }
            else
                ((PokazateliSubreport)this.SubReport1.Report).Reset(area, kv, parterArea, rzpAbove, nMax);

            this._projectName = dt.Rows[_pos].IsNull("ProjectName") ? String.Empty : (string)dt.Rows[_pos]["ProjectName"];

            if (_pos == dt.Rows.Count - 1) this.Detail.NewPage = NewPage.None;

            _pos++;
        }

        private void Pokazateli_ReportEnd(object sender, System.EventArgs eArgs)
        {
            if (_isRtf)
                return;
            Logo logo = new Logo();
            logo.Run();

            for (int i = 0; i < this.Document.Pages.Count; i++)
                this.Document.Pages[i].Overlay(logo.Document.Pages[0]);

        }

        private void ReportHeader_Format(object sender, System.EventArgs eArgs)
        {
            this.txtDate.Text = DateTime.Today.Date.ToShortDateString();
        }

        private void ReportFooter_Format(object sender, System.EventArgs eArgs)
        {
            this.txtCreatedBy.Text = this._userName;
        }

        private void PageFooter_Format(object sender, System.EventArgs eArgs)
        {
            if (this.PageNumber == 1)
            {
                this.PageFooter.Visible = false;
                return;
            }

            this.PageFooter.Visible = true;

            this.lblPageFooter.Text = this._projectName;
            lblPage.Text = string.Concat(Resource.ResourceManager["rpt_Page"], " ", this.PageNumber.ToString());
        }

        private void PageHeader_Format(object sender, System.EventArgs eArgs)
        {
            if (this.PageNumber == 1)
            {
                this.PageHeader.Visible = false;
                return;
            }

            this.PageHeader.Visible = true;
            this.txtPageHeader.Text =
                String.Concat(Resource.ResourceManager["Reports_PokazateliTitle"], " - ", this._projectName);
        }

		#region ActiveReports Designer generated code




















        public void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Pokazateli));
            this.Detail = new GrapeCity.ActiveReports.SectionReportModel.Detail();
            this.ReportHeader = new GrapeCity.ActiveReports.SectionReportModel.ReportHeader();
            this.ReportFooter = new GrapeCity.ActiveReports.SectionReportModel.ReportFooter();
            this.PageHeader = new GrapeCity.ActiveReports.SectionReportModel.PageHeader();
            this.PageFooter = new GrapeCity.ActiveReports.SectionReportModel.PageFooter();
            this.txtDate = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtPageHeader = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.Line2 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.TextBox23 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtObekt = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtAddress = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtAdminNameLbl = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtChast = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtReportTitle = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.SubReport1 = new GrapeCity.ActiveReports.SectionReportModel.SubReport();
            this.lblPageFooter = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Line11 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.lblPage = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label1 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.txtCreatedBy = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            ((System.ComponentModel.ISupportInitialize)(this.txtDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPageHeader)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TextBox23)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtObekt)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAddress)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAdminNameLbl)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtChast)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtReportTitle)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblPageFooter)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblPage)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCreatedBy)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            // 
            // Detail
            // 
            this.Detail.ColumnSpacing = 0F;
            this.Detail.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
						this.TextBox23,
						this.txtObekt,
						this.txtAddress,
						this.txtAdminNameLbl,
						this.txtChast,
						this.txtReportTitle,
						this.SubReport1});
            this.Detail.Height = 2.125F;
            this.Detail.Name = "Detail";
            this.Detail.NewPage = GrapeCity.ActiveReports.SectionReportModel.NewPage.After;
            // 
            // ReportHeader
            // 
            this.ReportHeader.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
						this.txtDate});
            this.ReportHeader.Height = 0.71875F;
            this.ReportHeader.Name = "ReportHeader";
            // 
            // ReportFooter
            // 
            this.ReportFooter.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
						this.Label1,
						this.txtCreatedBy});
            this.ReportFooter.Height = 1.364583F;
            this.ReportFooter.Name = "ReportFooter";
            // 
            // PageHeader
            // 
            this.PageHeader.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
						this.txtPageHeader,
						this.Line2});
            this.PageHeader.Height = 0.4270833F;
            this.PageHeader.Name = "PageHeader";
            // 
            // PageFooter
            // 
            this.PageFooter.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
						this.lblPageFooter,
						this.Line11,
						this.lblPage});
            this.PageFooter.Height = 0.25F;
            this.PageFooter.Name = "PageFooter";
            // 
            // txtDate
            // 
            this.txtDate.Height = 0.2F;
            this.txtDate.Left = 4.625F;
            this.txtDate.Name = "txtDate";
            this.txtDate.Style = "font-size: 11pt; text-align: right";
            this.txtDate.Text = "txtDate";
            this.txtDate.Top = 0.0625F;
            this.txtDate.Width = 1.7F;
            // 
            // txtPageHeader
            // 
            this.txtPageHeader.Height = 0.236F;
            this.txtPageHeader.Left = 0F;
            this.txtPageHeader.Name = "txtPageHeader";
            this.txtPageHeader.Style = "font-size: 11pt; font-weight: bold";
            this.txtPageHeader.Text = "txtPageHeader";
            this.txtPageHeader.Top = 0F;
            this.txtPageHeader.Width = 6.3125F;
            // 
            // Line2
            // 
            this.Line2.Height = 0F;
            this.Line2.Left = 0F;
            this.Line2.LineWeight = 2F;
            this.Line2.Name = "Line2";
            this.Line2.Top = 0.243493F;
            this.Line2.Width = 6.313001F;
            this.Line2.X1 = 0F;
            this.Line2.X2 = 6.313001F;
            this.Line2.Y1 = 0.243493F;
            this.Line2.Y2 = 0.243493F;
            // 
            // TextBox23
            // 
            this.TextBox23.Height = 0.2F;
            this.TextBox23.Left = 0.6186021F;
            this.TextBox23.Name = "TextBox23";
            this.TextBox23.Style = "font-size: 12pt";
            this.TextBox23.Text = "?????:";
            this.TextBox23.Top = 0.7701771F;
            this.TextBox23.Width = 1.006398F;
            // 
            // txtObekt
            // 
            this.txtObekt.DataField = "AdministrativeName";
            this.txtObekt.Height = 0.2F;
            this.txtObekt.Left = 1.612205F;
            this.txtObekt.Name = "txtObekt";
            this.txtObekt.Style = "font-size: 11pt; font-weight: bold; ddo-char-set: 204";
            this.txtObekt.Text = "txtObekt";
            this.txtObekt.Top = 0.7701771F;
            this.txtObekt.Width = 4.711F;
            // 
            // txtAddress
            // 
            this.txtAddress.DataField = "AddInfo";
            this.txtAddress.Height = 0.2F;
            this.txtAddress.Left = 1.612205F;
            this.txtAddress.Name = "txtAddress";
            this.txtAddress.Style = "font-size: 11pt; font-weight: normal";
            this.txtAddress.Text = "txtCode";
            this.txtAddress.Top = 0.991634F;
            this.txtAddress.Width = 4.711F;
            // 
            // txtAdminNameLbl
            // 
            this.txtAdminNameLbl.Height = 0.2F;
            this.txtAdminNameLbl.Left = 0.6186021F;
            this.txtAdminNameLbl.Name = "txtAdminNameLbl";
            this.txtAdminNameLbl.Style = "font-size: 12pt";
            this.txtAdminNameLbl.Text = "????:";
            this.txtAdminNameLbl.Top = 1.47441F;
            this.txtAdminNameLbl.Width = 1.006398F;
            // 
            // txtChast
            // 
            this.txtChast.Height = 0.2F;
            this.txtChast.Left = 1.612205F;
            this.txtChast.Name = "txtChast";
            this.txtChast.Style = "font-size: 12pt; font-weight: bold";
            this.txtChast.Text = "????????? ?????";
            this.txtChast.Top = 1.47441F;
            this.txtChast.Width = 4.711F;
            // 
            // txtReportTitle
            // 
            this.txtReportTitle.Height = 0.276F;
            this.txtReportTitle.Left = 0F;
            this.txtReportTitle.Name = "txtReportTitle";
            this.txtReportTitle.Style = "font-family: Microsoft Sans Serif; font-size: 15pt; font-weight: bold; text-align" +
                ": center";
            this.txtReportTitle.Text = "???????-???????????? ??????????";
            this.txtReportTitle.Top = 0F;
            this.txtReportTitle.Width = 6.3125F;
            // 
            // SubReport1
            // 
            this.SubReport1.CloseBorder = false;
            this.SubReport1.Height = 0.197F;
            this.SubReport1.Left = 0F;
            this.SubReport1.Name = "SubReport1";
            this.SubReport1.Report = null;
            this.SubReport1.Top = 1.90543F;
            this.SubReport1.Width = 6.335F;
            // 
            // lblPageFooter
            // 
            this.lblPageFooter.Height = 0.197F;
            this.lblPageFooter.HyperLink = null;
            this.lblPageFooter.Left = 0F;
            this.lblPageFooter.Name = "lblPageFooter";
            this.lblPageFooter.Style = "font-size: 8pt; font-weight: bold; text-align: left";
            this.lblPageFooter.Text = "lblPageFooter";
            this.lblPageFooter.Top = 0.02460628F;
            this.lblPageFooter.Width = 5.625F;
            // 
            // Line11
            // 
            this.Line11.Height = 0F;
            this.Line11.Left = 0.006944444F;
            this.Line11.LineWeight = 1F;
            this.Line11.Name = "Line11";
            this.Line11.Top = 0.006944444F;
            this.Line11.Width = 6.306056F;
            this.Line11.X1 = 0.006944444F;
            this.Line11.X2 = 6.313001F;
            this.Line11.Y1 = 0.006944444F;
            this.Line11.Y2 = 0.006944444F;
            // 
            // lblPage
            // 
            this.lblPage.Height = 0.197F;
            this.lblPage.HyperLink = null;
            this.lblPage.Left = 5.65F;
            this.lblPage.Name = "lblPage";
            this.lblPage.Style = "font-size: 8pt; font-weight: bold; text-align: center";
            this.lblPage.Text = "lblPage";
            this.lblPage.Top = 0F;
            this.lblPage.Width = 0.673F;
            // 
            // Label1
            // 
            this.Label1.Height = 0.2F;
            this.Label1.HyperLink = null;
            this.Label1.Left = 0.625F;
            this.Label1.Name = "Label1";
            this.Label1.Style = "font-size: 11pt";
            this.Label1.Text = "????????: ......................................";
            this.Label1.Top = 0.8125F;
            this.Label1.Width = 1.006F;
            // 
            // txtCreatedBy
            // 
            this.txtCreatedBy.Height = 0.2F;
            this.txtCreatedBy.Left = 1.612F;
            this.txtCreatedBy.Name = "txtCreatedBy";
            this.txtCreatedBy.Style = "font-size: 10pt; font-weight: normal; text-align: center";
            this.txtCreatedBy.Text = "txtCreatedBy";
            this.txtCreatedBy.Top = 1.0625F;
            this.txtCreatedBy.Width = 2F;
            // 
            // ActiveReport1
            // 
            this.MasterReport = false;
            this.PageSettings.DefaultPaperSize = false;
            this.PageSettings.Margins.Bottom = 0.8034722F;
            this.PageSettings.Margins.Left = 1.377778F;
            this.PageSettings.Margins.Right = 0.8034722F;
            this.PageSettings.Margins.Top = 0.8034722F;
            this.PageSettings.PaperHeight = 11.69306F;
            this.PageSettings.PaperKind = System.Drawing.Printing.PaperKind.A4;
            this.PageSettings.PaperWidth = 8.268056F;
            this.PrintWidth = 6.4F;
            this.Sections.Add(this.ReportHeader);
            this.Sections.Add(this.PageHeader);
            this.Sections.Add(this.Detail);
            this.Sections.Add(this.PageFooter);
            this.Sections.Add(this.ReportFooter);
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule(resources.GetString("$this.StyleSheet"), "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: inherit; font-style: inherit; font-variant: inherit; font-weight: bo" +
                        "ld; font-size: 16pt; font-size-adjust: inherit; font-stretch: inherit", "Heading1", "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: Times New Roman; font-style: italic; font-variant: inherit; font-wei" +
                        "ght: bold; font-size: 14pt; font-size-adjust: inherit; font-stretch: inherit", "Heading2", "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: inherit; font-style: inherit; font-variant: inherit; font-weight: bo" +
                        "ld; font-size: 13pt; font-size-adjust: inherit; font-stretch: inherit", "Heading3", "Normal"));
            ((System.ComponentModel.ISupportInitialize)(this.txtDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPageHeader)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TextBox23)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtObekt)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAddress)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAdminNameLbl)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtChast)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtReportTitle)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblPageFooter)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblPage)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCreatedBy)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();

            // Attach Report Events
            this.ReportStart += new System.EventHandler(this.Pokazateli_ReportStart);
            this.FetchData += new GrapeCity.ActiveReports.SectionReport.FetchEventHandler(this.Pokazateli_FetchData);
            this.ReportEnd += new System.EventHandler(this.Pokazateli_ReportEnd);
            this.ReportHeader.Format += new System.EventHandler(this.ReportHeader_Format);
            this.ReportFooter.Format += new System.EventHandler(this.ReportFooter_Format);
            this.PageFooter.Format += new System.EventHandler(this.PageFooter_Format);
            this.PageHeader.Format += new System.EventHandler(this.PageHeader_Format);
        }

		#endregion

        private bool isParter(DataRow[] drs)
        {
            foreach (DataRow ff in drs)
            {
                int contentType = (int)ff["ContentTypeID"];
                if (contentType == (int)PokazateliReportData.ContentTypes.Parter)
                    return true;
            }
            return false;
        }

        private ReportHeader ReportHeader;
        private TextBox txtDate;
        private PageHeader PageHeader;
        private TextBox txtPageHeader;
        private Line Line2;
        private Detail Detail;
        private TextBox TextBox23;
        private TextBox txtObekt;
        private TextBox txtAddress;
        private TextBox txtAdminNameLbl;
        private TextBox txtChast;
        private TextBox txtReportTitle;
        private SubReport SubReport1;
        private PageFooter PageFooter;
        private Label lblPageFooter;
        private Line Line11;
        private Label lblPage;
        private ReportFooter ReportFooter;
        private Label Label1;
        private TextBox txtCreatedBy;
	}
}
