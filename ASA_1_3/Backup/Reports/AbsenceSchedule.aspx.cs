﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web.SessionState;
using System.Web.UI.HtmlControls;
using System.Data.SqlClient;
using log4net;
using Asa.Timesheet.Data;
using Asa.Timesheet.Data.Entities;
using Asa.Timesheet.Data.Util;
using Asa.Timesheet.Data.Reports;
using GrapeCity.ActiveReports.Export.Pdf.Section;
using GrapeCity.ActiveReports.Export.Excel.Section;
using GrapeCity.ActiveReports.Export.Html.Section;
namespace Asa.Timesheet.WebPages.Reports
{
    public partial class AbsenceSchedule : System.Web.UI.Page
    {

        DataTable dtPeopleList = new DataTable();
        DataTable dtDates = new DataTable();
        int rowsCount;
        int currMonth = DateTime.DaysInMonth(DateTime.Now.Year, DateTime.Now.Month) - DateTime.Now.Day + 1;
        int nextMonth = DateTime.DaysInMonth(DateTime.Now.Year, DateTime.Now.Month + 1);

        protected void Page_Load(object sender, EventArgs e)
        {

            var culture = new System.Globalization.CultureInfo("bg-BG");

            SqlDataReader readerDates = AbsenceRequestData.SelectAbsenceScheduleAll();
            SqlDataReader readerPeopleList = AbsenceRequestData.SelectAbsenceSchedulePeopleList();

            dtPeopleList.Load(readerPeopleList);
            dtDates.Load(readerDates);


            int currMonth = DateTime.DaysInMonth(DateTime.Now.Year, DateTime.Now.Month);
            int nextMonth = DateTime.DaysInMonth(DateTime.Now.Year, DateTime.Now.Month + 1);

            for (int i = 0; i < currMonth + nextMonth; i++)
            {
                BoundField day = new BoundField();
                day.HeaderText = DateTime.Today.AddDays(i).Day.ToString();// +"<br />" + culture.DateTimeFormat.GetAbbreviatedDayName(DateTime.Today.AddDays(i).DayOfWeek);
                if (DateTime.Today.AddDays(i).DayOfWeek == DayOfWeek.Saturday)
                {
                    day.HeaderStyle.ForeColor = System.Drawing.Color.Gray;
                    day.HeaderStyle.BackColor = System.Drawing.Color.FromArgb(100, 240, 240, 240);
                    day.ItemStyle.BackColor = System.Drawing.Color.FromArgb(100, 240, 240, 240);
                }
                if (DateTime.Today.AddDays(i).DayOfWeek == DayOfWeek.Sunday)
                {
                    day.HeaderStyle.ForeColor = System.Drawing.Color.Red;
                    day.HeaderStyle.BackColor = System.Drawing.Color.FromArgb(100, 240, 240, 240);
                    day.ItemStyle.BackColor = System.Drawing.Color.FromArgb(100,240,240,240);
                    
                }
                day.HeaderStyle.Width = 32;
                day.HeaderStyle.Wrap = false;
                day.ControlStyle.BorderStyle = BorderStyle.None;
                GridView1.Columns.Add(day);

            }
            DataRow r = dtPeopleList.NewRow();
            r[0] = "0"; //Error message occured here
            r[1] = "12-12-12";
            r[2] = "12-12-12";
            r[3] = false;
            r[4] = 0;
            r[5] = 0;
            r[6] = "";
            r[7] = false;

            dtPeopleList.Rows.Add(r);
            rowsCount = dtPeopleList.Rows.Count;
            GridView1.DataSource = dtPeopleList;
            GridView1.DataBind();
            
        }
        protected void VacationsGridView_RowCreated(object sender, GridViewRowEventArgs e)
        {
            e.Row.Cells[1].CssClass = "sticky";
            if (e.Row.RowIndex == rowsCount - 1 )
            {
                var culture = new System.Globalization.CultureInfo("bg-BG");

                
                e.Row.Cells[2].Attributes.Add("colspan", currMonth.ToString());
                e.Row.Cells[2].Text = culture.DateTimeFormat.GetMonthName(DateTime.Now.Month);
                e.Row.Cells[2].CssClass = "monthName";
                e.Row.Cells[3].Attributes.Add("colspan", nextMonth.ToString());
                e.Row.Cells[3].Text = culture.DateTimeFormat.GetMonthName(DateTime.Now.Month + 1);
                e.Row.Cells[3].CssClass = "monthName";
                for (int i = 4; i < currMonth + nextMonth + 4; i++)
                {
                    e.Row.Cells[i].Visible = false;
                }
            }
        }
        protected void btnSeeVacations_Click(object sender, EventArgs e)
        {

        }

        protected void VacationsGridView_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if(e.Row.RowIndex > 0 && e.Row.RowIndex < rowsCount - 1){
                foreach (DataRow dtRow in dtDates.Rows)
                {
                    if (Convert.ToString(dtRow[6]) == e.Row.Cells[1].Text) {
                         
                        DateTime startDate = Convert.ToDateTime(dtRow[1]);
                        DateTime endDate = Convert.ToDateTime(dtRow[2]);

                        DateTime testToday = DateTime.Now;
                        int startDateOffset = (int)(startDate - testToday).TotalDays;
                        int endDateOffset = (int)(endDate - testToday).TotalDays;
                        for (int i = startDateOffset; i <= endDateOffset; i++)
                        {
                            e.Row.Cells[i + 3].Text = "<i class=\"fas fa-suitcase\"></i>";
                        }
                    }
                }
            }     
        }
    }

}