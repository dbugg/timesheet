using System;
using DataDynamics.ActiveReports;
using DataDynamics.ActiveReports.Document;

namespace Asa.Timesheet.Windows.Mail
{
	public class Logo1 : ActiveReport
	{
		public Logo1()
		{
			InitializeReport();
		}

		#region ActiveReports Designer generated code
		private DataDynamics.ActiveReports.ReportHeader ReportHeader = null;
		private DataDynamics.ActiveReports.Picture Picture1 = null;
		private DataDynamics.ActiveReports.TextBox TextBox3 = null;
		private DataDynamics.ActiveReports.TextBox txtTest = null;
		private DataDynamics.ActiveReports.Detail Detail = null;
		private DataDynamics.ActiveReports.ReportFooter ReportFooter = null;
		public void InitializeReport()
		{
			this.LoadLayout(this.GetType(), "Mail.Logo1.rpx");
			this.ReportHeader = ((DataDynamics.ActiveReports.ReportHeader)(this.Sections["ReportHeader"]));
			this.Detail = ((DataDynamics.ActiveReports.Detail)(this.Sections["Detail"]));
			this.ReportFooter = ((DataDynamics.ActiveReports.ReportFooter)(this.Sections["ReportFooter"]));
			this.Picture1 = ((DataDynamics.ActiveReports.Picture)(this.ReportHeader.Controls[0]));
			this.TextBox3 = ((DataDynamics.ActiveReports.TextBox)(this.ReportHeader.Controls[1]));
			this.txtTest = ((DataDynamics.ActiveReports.TextBox)(this.ReportHeader.Controls[2]));
		}

		#endregion
	}
}
