﻿<%@ Page language="c#" Codebehind="EditUser.aspx.cs" AutoEventWireup="True" Inherits="Asa.Timesheet.WebPages.EditUser" smartNavigation="True"%>
<%@ Register TagPrefix="uc1" TagName="PageHeader" Src="UserControls/PageHeader.ascx" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
	<head runat="server">
		<TITLE>EditUser</TITLE>
		<script language="javascript" src="PopupCalendar.js"></script>
		<meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
		<meta content="C#" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<LINK href="styles/timesheet.css" type="text/css" rel="stylesheet">
	</HEAD>
	<body bottomMargin="0" leftMargin="0" topMargin="0" rightMargin="0">
		<form id="Form1" method="post" runat="server">
			<table height="100%" cellSpacing="0" cellPadding="0" width="100%" border="0">
				<tr height="1">
					<td><uc1:pageheader id="header" runat="server"></uc1:pageheader></td>
				</tr>
				<tr>
					<td>
						<table height="100%" cellSpacing="0" cellPadding="0" width="80%" border="0">
							<tr>
								<td class="menuLeft"><asp:placeholder id="menuHolder" runat="server"></asp:placeholder></td>
								
								<td>
									<TABLE id="Table3" cellSpacing="0" width="80%" border="0">
                                		<TR>
											<td colspan="3">    
                                            <asp:button id="btnSaveUP" runat="server" CssClass="ActionButton" Text="Запиши" onclick="btnSave_Click"></asp:button>
                                            <asp:button id="btnCancelUP" runat="server" CssClass="ActionButton" Text="Обратно" onclick="btnCancel_Click"></asp:button>
                                            <asp:label id="lblInfo" runat="server" CssClass="InfoLabel" EnableViewState="False"></asp:label><asp:label id="lblError" runat="server" CssClass="ErrorLabel" EnableViewState="False" ForeColor="Red"></asp:label></td>

										</TR>
										<TR style="background: #fdfdfd">
											<td width="33%"><asp:label id="lblLastName" runat="server" CssClass="enterDataLabel" Font-Bold="True" EnableViewState="False"> Фамилия:</asp:label><asp:textbox id="txtLastName" runat="server" CssClass="enterDataBox"></asp:textbox>&nbsp;<IMG height="16" alt="" src="images/required1.gif" width="16"></TD>
											<td  width="33%"><asp:label id="lblEmail" runat="server" CssClass="enterDataLabel" Font-Bold="True" EnableViewState="False" autocomplete="off">Електронен адрес:</asp:label><asp:textbox id="txtEmail" runat="server" CssClass="enterDataBox" ></asp:textbox>&nbsp;<IMG height="16" alt="" src="images/required1.gif" width="16"></TD>
                                                        									<td width="33%" ><asp:label id="lblPassword" runat="server" CssClass="enterDataLabel" Font-Bold="True">Парола:</asp:label>
											<asp:textbox id="txtPassword" runat="server" CssClass="enterDataBox" autocomplete="off"></asp:textbox>&nbsp;<IMG height="16" alt="" src="images/required1.gif" width="16">
												<asp:textbox id="txtHdnPassword" runat="server" Visible="False"></asp:textbox><INPUT id="hdnPassword" type="hidden" runat="server"></TD>
													
                                        <TR style="background: #fdfdfd;">
														<td><asp:label id="lblRole" runat="server" CssClass="enterDataLabel" Font-Bold="True">Роля в системата: </asp:label><asp:dropdownlist id="ddlRole" runat="server" CssClass="enterDataBox" ></asp:dropdownlist>&nbsp;<IMG height="16" alt="" src="images/required1.gif" width="16"></TD>
														<td><asp:label id="Label2" runat="server" CssClass="enterDataLabel">Длъжност:</asp:label><asp:dropdownlist id="ddlOccupation" runat="server" CssClass="enterDataBox" ></asp:dropdownlist>&nbsp;<IMG height="16" alt="" src="images/required1.gif" width="16"></TD>
														<td>
															<asp:label id="Label14" runat="server" CssClass="enterDataLabel">Коефициент:</asp:label>
															<asp:textbox id="txtCoeff" runat="server" CssClass="enterDataBox"></asp:textbox></TD>
													</TR>
                                                    <tr style="background: #fdfdfd;"><td colspan=3"">&nbsp</td></tr>

                            <TR>
								<td><asp:label id="lblFirstName" runat="server" CssClass="enterDataLabel">Име:</asp:label>
                                <asp:textbox id="txtFirstName" runat="server" CssClass="enterDataBox"></asp:textbox></TD>

								<td><asp:label id="lblSecondName" runat="server" CssClass="enterDataLabel"> Презиме:</asp:label>
                                <asp:textbox id="txtSecondName" runat="server" CssClass="enterDataBox"></asp:textbox></TD>

								<td><asp:label id="lblNameEN" runat="server" CssClass="enterDataLabel" Font-Bold="False" EnableViewState="False"> Пълно име - английски:</asp:label>
                                <asp:textbox id="txtNameEN" runat="server" CssClass="enterDataBox"></asp:textbox></TD>
							</TR>
							<!--<TR>
								<td><asp:label id="lblAccount" runat="server" CssClass="enterDataLabel" Font-Bold="True" EnableViewState="False" type="hidden"> Windows Акаунт:</asp:label></TD>
								<td><asp:textbox id="txtAccount" runat="server" CssClass="enterDataBox"  type="hidden"></asp:textbox>&nbsp;<IMG height="16" alt="" src="images/required1.gif" width="16"></TD>
							</TR>-->

							<TR>
								<td><asp:label id="Label13" runat="server" CssClass="enterDataLabel" Font-Bold="False" EnableViewState="False">Личен електронен адрес:</asp:label><asp:textbox id="txtPrivate" runat="server" CssClass="enterDataBox"></asp:textbox></TD>

								<td>
                                <asp:label id="Label10" runat="server" CssClass="enterDataLabel">Личен мобилен:</asp:label>
                                <asp:textbox id="txtMobilePersonal" runat="server" CssClass="enterDataBox" ></asp:textbox></TD>

								<td><asp:label id="Label6" runat="server" CssClass="enterDataLabel">Домашен телефон:</asp:label>
                                <asp:textbox id="txtHome" runat="server" CssClass="enterDataBox" ></asp:textbox>
</TD>
							</TR>
                            <tr>
                                <td><asp:label id="Label5" runat="server" CssClass="enterDataLabel">Мобилен телефон:</asp:label>
                                    <asp:textbox id="txtMobile" runat="server" CssClass="enterDataBox" ></asp:textbox></td>
                                <td>                                <asp:label id="Label4" runat="server" CssClass="enterDataLabel">Вътрешен телефон:</asp:label>
                                    <asp:textbox id="txtExt" runat="server" CssClass="enterDataBox" ></asp:textbox></td>
                                    <td><asp:label id="Label12" runat="server" CssClass="enterDataLabel">Дата на започване на работа:</asp:label>
                                    <asp:textbox id="txtWorkDateStart" runat="server" CssClass="enterDataBox" ></asp:textbox>&nbsp;<IMG id="lkCalendar1" alt="Calendar" src="images/calendar.ico"  align="absBottom" runat="server"></td>
                             </tr>
							<TR>
                                <td><asp:label id="Label3" runat="server" CssClass="enterDataLabel">Част:</asp:label><asp:dropdownlist id="ddlUserRole" runat="server" CssClass="enterDataBox">
										<asp:ListItem></asp:ListItem>
										<asp:ListItem Value="Архитектура">Архитектура</asp:ListItem>
										<asp:ListItem Value="ПСД">ПСД</asp:ListItem>
										<asp:ListItem Value="Конструктор">Конструктор</asp:ListItem>
									</asp:dropdownlist></td>
								<td><asp:label id="lbManager" runat="server" CssClass="enterDataLabel">Ръководител: </asp:label>
                                <asp:dropdownlist id="ddlManager" runat="server" CssClass="enterDataBox" ></asp:dropdownlist></TD>

								<td>
									<asp:label id="lblVacation" runat="server" CssClass="enterDataLabel">Отпуск от минали години:</asp:label>
									<asp:textbox id="txtVacation" runat="server" CssClass="enterDataBox"></asp:textbox></TD>
							</TR>
                            <TR>
								<td><asp:label id="Label11" runat="server" CssClass="enterDataLabel">АСИ:</asp:label>
                                <asp:checkbox id="cbIsASI" runat="server" oncheckedchanged="cbIsTraine_CheckedChanged"></asp:checkbox></TD>

								<td>
                                <asp:label id="Label9" runat="server" CssClass="enterDataLabel">Стажант:</asp:label><asp:checkbox id="cbIsTraine" runat="server"></asp:checkbox></TD>

							</TR>
							<TR>

								<td colspan="3"><asp:label id="Label1" runat="server" CssClass="enterDataLabel">Кратко CV:</asp:label>
                                <asp:textbox id="txtCV" runat="server" CssClass="enterDataBox" TextMode="MultiLine"
										Height="96px" Width="80%"></asp:textbox></TD>
							</TR>
							<TR>
								<td><asp:label id="lbHasWorkTime" runat="server" CssClass="enterDataLabel">Въвежда ли раб. време:</asp:label>
                                <asp:checkbox id="cbHasWorkTime" runat="server" AutoPostBack="True" oncheckedchanged="cbHasWorkTime_CheckedChanged"></asp:checkbox></TD>

								<td><asp:label id="lbHasWorkTimeOver" runat="server" CssClass="enterDataLabel">Въвежда ли извънраб. време:</asp:label>
								<asp:checkbox id="cbHasWorkTimeOver" runat="server"></asp:checkbox></TD>
							</TR>

                            				<TR>
					<td><asp:button id="btnSave" runat="server" CssClass="ActionButton" Text="Запиши" onclick="btnSave_Click"></asp:button>
					<asp:button id="btnCancel" runat="server" CssClass="ActionButton" Text="Обратно" onclick="btnCancel_Click"></asp:button>
					<asp:button id="btnExport" runat="server" CssClass="ActionButton" Text="Експорт" onclick="btnExport_Click"></asp:button>
					<asp:button id="btnDelete" runat="server" CssClass="ActionButton" Text="Изтрий" onclick="btnDelete_Click"></asp:button></TD>

				</TR>
							<TR>
								<td><asp:label id="lbSalary" runat="server" CssClass="enterDataLabel" Visible="False"> Заплата (BGN):</asp:label></TD>
								<td></TD>
							</TR>
							<TR>
								<td colSpan="4"><asp:datagrid id="grdSalaries" runat="server" CssClass="Grid" Width="100%" AllowSorting="True"
										PageSize="2" CellPadding="4" AutoGenerateColumns="False">
										<ItemStyle CssClass="GridItem"></ItemStyle>
										<HeaderStyle Wrap="False" CssClass="GridHeader1"></HeaderStyle>
										<Columns>
											<asp:TemplateColumn HeaderText="Година">
												<ItemTemplate>
													<asp:Label ID="lbYear" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.SalaryYear").ToString()+ " - " + GetSalaryType((int)DataBinder.Eval(Container, "DataItem.SalaryType")) %>'>
													</asp:Label>
												</ItemTemplate>
											</asp:TemplateColumn>
											<asp:TemplateColumn HeaderText="Яну">
												<ItemStyle ForeColor="DimGray" Width="10px"></ItemStyle>
												<ItemTemplate>
													<asp:Textbox CssClass="textboxSalary" runat="server" Text='<%# GetSalary((int)DataBinder.Eval(Container, "DataItem.SalaryYear"),(int)DataBinder.Eval(Container, "DataItem.SalaryType"),1)  %>' ID="txtJan" >
													</asp:Textbox>
												</ItemTemplate>
											</asp:TemplateColumn>
											<asp:TemplateColumn HeaderText="Фев">
												<ItemStyle ForeColor="DimGray" Width="10px"></ItemStyle>
												<ItemTemplate>
													<asp:Textbox CssClass="textboxSalary" runat="server" Text='<%# GetSalary((int)DataBinder.Eval(Container, "DataItem.SalaryYear"),(int)DataBinder.Eval(Container, "DataItem.SalaryType"),2)  %>' ID="txtFeb" >
													</asp:Textbox>
												</ItemTemplate>
											</asp:TemplateColumn>
											<asp:TemplateColumn HeaderText="Март">
												<ItemStyle ForeColor="DimGray" Width="10px"></ItemStyle>
												<ItemTemplate>
													<asp:Textbox CssClass="textboxSalary" runat="server" Text='<%# GetSalary((int)DataBinder.Eval(Container, "DataItem.SalaryYear"),(int)DataBinder.Eval(Container, "DataItem.SalaryType"),3)  %>' ID="txtMarch" >
													</asp:Textbox>
												</ItemTemplate>
											</asp:TemplateColumn>
											<asp:TemplateColumn HeaderText="Апр">
												<ItemStyle ForeColor="DimGray" Width="10px"></ItemStyle>
												<ItemTemplate>
													<asp:Textbox CssClass="textboxSalary" runat="server" Text='<%# GetSalary((int)DataBinder.Eval(Container, "DataItem.SalaryYear"),(int)DataBinder.Eval(Container, "DataItem.SalaryType"),4)  %>' ID="txtApril" >
													</asp:Textbox>
												</ItemTemplate>
											</asp:TemplateColumn>
											<asp:TemplateColumn HeaderText="Май">
												<ItemStyle ForeColor="DimGray" Width="10px"></ItemStyle>
												<ItemTemplate>
													<asp:Textbox CssClass="textboxSalary" runat="server" Text='<%# GetSalary((int)DataBinder.Eval(Container, "DataItem.SalaryYear"),(int)DataBinder.Eval(Container, "DataItem.SalaryType"),5)  %>' ID="txtMay" >
													</asp:Textbox>
												</ItemTemplate>
											</asp:TemplateColumn>
											<asp:TemplateColumn HeaderText="Юни">
												<ItemStyle ForeColor="DimGray" Width="10px"></ItemStyle>
												<ItemTemplate>
													<asp:Textbox CssClass="textboxSalary" runat="server" Text='<%# GetSalary((int)DataBinder.Eval(Container, "DataItem.SalaryYear"),(int)DataBinder.Eval(Container, "DataItem.SalaryType"),6)  %>' ID="txtJune" >
													</asp:Textbox>
												</ItemTemplate>
											</asp:TemplateColumn>
											<asp:TemplateColumn HeaderText="Юли">
												<ItemStyle ForeColor="DimGray" Width="10px"></ItemStyle>
												<ItemTemplate>
													<asp:Textbox CssClass="textboxSalary" runat="server" Text='<%# GetSalary((int)DataBinder.Eval(Container, "DataItem.SalaryYear"),(int)DataBinder.Eval(Container, "DataItem.SalaryType"),7)  %>' ID="txtJuly" >
													</asp:Textbox>
												</ItemTemplate>
											</asp:TemplateColumn>
											<asp:TemplateColumn HeaderText="Авг">
												<ItemStyle ForeColor="DimGray" Width="10px"></ItemStyle>
												<ItemTemplate>
													<asp:Textbox CssClass="textboxSalary" runat="server" Text='<%# GetSalary((int)DataBinder.Eval(Container, "DataItem.SalaryYear"),(int)DataBinder.Eval(Container, "DataItem.SalaryType"),8)  %>' ID="txtAug" >
													</asp:Textbox>
												</ItemTemplate>
											</asp:TemplateColumn>
											<asp:TemplateColumn HeaderText="Септ">
												<ItemStyle ForeColor="DimGray" Width="10px"></ItemStyle>
												<ItemTemplate>
													<asp:Textbox CssClass="textboxSalary" runat="server" Text='<%# GetSalary((int)DataBinder.Eval(Container, "DataItem.SalaryYear"),(int)DataBinder.Eval(Container, "DataItem.SalaryType"),9)  %>' ID="txtSept" >
													</asp:Textbox>
												</ItemTemplate>
											</asp:TemplateColumn>
											<asp:TemplateColumn HeaderText="Окт">
												<ItemStyle ForeColor="DimGray" Width="10px"></ItemStyle>
												<ItemTemplate>
													<asp:Textbox CssClass="textboxSalary" runat="server" Text='<%# GetSalary((int)DataBinder.Eval(Container, "DataItem.SalaryYear"),(int)DataBinder.Eval(Container, "DataItem.SalaryType"),10)  %>' ID="txtOct" >
													</asp:Textbox>
												</ItemTemplate>
											</asp:TemplateColumn>
											<asp:TemplateColumn HeaderText="Ное">
												<ItemStyle ForeColor="DimGray" Width="10px"></ItemStyle>
												<ItemTemplate>
													<asp:Textbox CssClass="textboxSalary" runat="server" Text='<%# GetSalary((int)DataBinder.Eval(Container, "DataItem.SalaryYear"),(int)DataBinder.Eval(Container, "DataItem.SalaryType"),11)  %>' ID="txtNov" >
													</asp:Textbox>
												</ItemTemplate>
											</asp:TemplateColumn>
											<asp:TemplateColumn HeaderText="Дек">
												<ItemStyle ForeColor="DimGray" Width="10px"></ItemStyle>
												<ItemTemplate>
													<asp:Textbox CssClass="textboxSalary" runat="server" Text='<%# GetSalary((int)DataBinder.Eval(Container, "DataItem.SalaryYear"),(int)DataBinder.Eval(Container, "DataItem.SalaryType"),12)  %>' ID="txtDec" >
													</asp:Textbox>
												</ItemTemplate>
											</asp:TemplateColumn>
											<asp:TemplateColumn HeaderText="Средно/Общо">
												<ItemStyle ForeColor="DimGray" Width="10px"></ItemStyle>
												<ItemTemplate>
													<asp:Textbox CssClass="textboxSalary" runat="server" ID="Textbox12" Text='<%# GetTotal((int)DataBinder.Eval(Container, "DataItem.SalaryYear"),(int)DataBinder.Eval(Container, "DataItem.SalaryType"))  %>'>
													</asp:Textbox>
												</ItemTemplate>
											</asp:TemplateColumn>
										</Columns>
									</asp:datagrid></TD>
							</TR>
						</TABLE>
						<table id="tbl" cellSpacing="0" cellPadding="3" runat="server">
							<TR>
								<td><asp:label id="Label7" runat="server" CssClass="enterDataLabel" Visible="False"> Заплата (BGN):</asp:label></TD>
								<td><asp:textbox id="txtSalary" runat="server" CssClass="enterDataBox" Width="100px" Visible="False"></asp:textbox></TD>
							</TR>
							<TR>
								<td><asp:label id="Label8" runat="server" CssClass="enterDataLabel" Visible="False">Бонус:</asp:label></TD>
								<td><asp:textbox id="txtBonus" runat="server" CssClass="enterDataBox" Width="100px" Visible="False"></asp:textbox></TD>
							</TR>
						</table>
					</TD>
				</TR>



			</table>
		</form>
	</body>
</HTML>
