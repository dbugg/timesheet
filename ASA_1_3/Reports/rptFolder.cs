using System;
using System.Drawing;
using GrapeCity.ActiveReports;
using GrapeCity.ActiveReports.Controls;
using GrapeCity.ActiveReports.SectionReportModel;
using GrapeCity.ActiveReports.Document.Section;
using GrapeCity.ActiveReports.Document;

namespace Asa.Timesheet.WebPages.Reports
{
    public class rptFolder : GrapeCity.ActiveReports.SectionReport
	{
        public rptFolder(string sColor, string name, string address, string code, string innername)
        {
            InitializeComponent();
            this.PageSettings.Margins.Top = 0.3f;
            this.PageSettings.Margins.Bottom = .6f;
            //			this.PageSettings.Gutter = 0F;
            //			this.PageSettings.Margins.Left = 0;
            //			this.PageSettings.Margins.Right = 0;
            shColor.BackColor = shColor1.BackColor = ColorTranslator.FromHtml(sColor);
            lbObekt.Text = lbObekt1.Text = lbObekt2.Text = lbObekt3.Text = lbObekt4.Text = name;
            lbAddress.Text = lbAddress1.Text = lbAddress2.Text = lbAddress3.Text = lbAddress4.Text = address;
            lbCode.Text = lbCode1.Text = lbCode2.Text = lbCode3.Text = lbCode4.Text = code;
            if (innername != null && innername.Length > 26)
                innername = innername.Substring(0, 26);
            lbInnerName.Text = lbInnerName1.Text = lbInnerName2.Text = lbInnerName3.Text = lbInnerName4.Text = innername;
        }

		#region ActiveReports Designer generated code















































        public void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(rptFolder));
            this.Detail = new GrapeCity.ActiveReports.SectionReportModel.Detail();
            this.PageHeader = new GrapeCity.ActiveReports.SectionReportModel.PageHeader();
            this.PageFooter = new GrapeCity.ActiveReports.SectionReportModel.PageFooter();
            this.Shape3 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
            this.Picture5 = new GrapeCity.ActiveReports.SectionReportModel.Picture();
            this.Picture4 = new GrapeCity.ActiveReports.SectionReportModel.Picture();
            this.Picture3 = new GrapeCity.ActiveReports.SectionReportModel.Picture();
            this.Picture2 = new GrapeCity.ActiveReports.SectionReportModel.Picture();
            this.Picture1 = new GrapeCity.ActiveReports.SectionReportModel.Picture();
            this.shColor1 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
            this.Label10 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.shColor = new GrapeCity.ActiveReports.SectionReportModel.Shape();
            this.Line5 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.Line4 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.Line3 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.Line2 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.Label4 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Line1 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.lbAddress = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lbObekt = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label6 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lbInnerName = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lbCode = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lbInnerName1 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lbInnerName2 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lbInnerName3 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lbCode1 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lbCode2 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lbCode3 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lbCode4 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label15 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label16 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label17 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label18 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lbObekt1 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lbObekt2 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lbObekt3 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lbObekt4 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lbAddress1 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lbAddress2 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lbAddress3 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lbAddress4 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label27 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label28 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label29 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label30 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lbInnerName4 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            ((System.ComponentModel.ISupportInitialize)(this.Picture5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Picture4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Picture3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Picture2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Picture1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbAddress)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbObekt)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbInnerName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbCode)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbInnerName1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbInnerName2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbInnerName3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbCode1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbCode2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbCode3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbCode4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label15)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label16)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label17)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label18)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbObekt1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbObekt2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbObekt3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbObekt4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbAddress1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbAddress2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbAddress3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbAddress4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label27)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label28)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label29)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label30)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbInnerName4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            // 
            // Detail
            // 
            this.Detail.ColumnSpacing = 0F;
            this.Detail.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
						this.Shape3,
						this.Picture5,
						this.Picture4,
						this.Picture3,
						this.Picture2,
						this.Picture1,
						this.shColor1,
						this.Label10,
						this.shColor,
						this.Line5,
						this.Line4,
						this.Line3,
						this.Line2,
						this.Label4,
						this.Line1,
						this.lbAddress,
						this.lbObekt,
						this.Label6,
						this.lbInnerName,
						this.lbCode,
						this.lbInnerName1,
						this.lbInnerName2,
						this.lbInnerName3,
						this.lbCode1,
						this.lbCode2,
						this.lbCode3,
						this.lbCode4,
						this.Label15,
						this.Label16,
						this.Label17,
						this.Label18,
						this.lbObekt1,
						this.lbObekt2,
						this.lbObekt3,
						this.lbObekt4,
						this.lbAddress1,
						this.lbAddress2,
						this.lbAddress3,
						this.lbAddress4,
						this.Label27,
						this.Label28,
						this.Label29,
						this.Label30,
						this.lbInnerName4});
            this.Detail.Height = 7.0625F;
            this.Detail.Name = "Detail";
            // 
            // PageHeader
            // 
            this.PageHeader.Height = 0F;
            this.PageHeader.Name = "PageHeader";
            // 
            // PageFooter
            // 
            this.PageFooter.Height = 0F;
            this.PageFooter.Name = "PageFooter";
            // 
            // Shape3
            // 
            this.Shape3.Height = 7.0625F;
            this.Shape3.Left = 0F;
            this.Shape3.Name = "Shape3";
            this.Shape3.RoundingRadius = 9.999999F;
            this.Shape3.Top = 0F;
            this.Shape3.Width = 9.84F;
            // 
            // Picture5
            // 
            this.Picture5.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.Picture5.Height = 1.181102F;
            this.Picture5.ImageData = ((System.IO.Stream)(resources.GetObject("Picture5.ImageData")));
            this.Picture5.Left = 8.4375F;
            this.Picture5.LineColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.Picture5.Name = "Picture5";
            this.Picture5.PictureAlignment = GrapeCity.ActiveReports.SectionReportModel.PictureAlignment.TopRight;
            this.Picture5.SizeMode = GrapeCity.ActiveReports.SectionReportModel.SizeModes.Zoom;
            this.Picture5.Top = 0.3125F;
            this.Picture5.Width = 0.7238354F;
            // 
            // Picture4
            // 
            this.Picture4.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.Picture4.Height = 1.181102F;
            this.Picture4.ImageData = ((System.IO.Stream)(resources.GetObject("Picture4.ImageData")));
            this.Picture4.Left = 6.5F;
            this.Picture4.LineColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.Picture4.Name = "Picture4";
            this.Picture4.PictureAlignment = GrapeCity.ActiveReports.SectionReportModel.PictureAlignment.TopRight;
            this.Picture4.SizeMode = GrapeCity.ActiveReports.SectionReportModel.SizeModes.Zoom;
            this.Picture4.Top = 0.3125F;
            this.Picture4.Width = 0.7238354F;
            // 
            // Picture3
            // 
            this.Picture3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.Picture3.Height = 1.181102F;
            this.Picture3.ImageData = ((System.IO.Stream)(resources.GetObject("Picture3.ImageData")));
            this.Picture3.Left = 4.5625F;
            this.Picture3.LineColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.Picture3.Name = "Picture3";
            this.Picture3.PictureAlignment = GrapeCity.ActiveReports.SectionReportModel.PictureAlignment.TopRight;
            this.Picture3.SizeMode = GrapeCity.ActiveReports.SectionReportModel.SizeModes.Zoom;
            this.Picture3.Top = 0.3125F;
            this.Picture3.Width = 0.7238354F;
            // 
            // Picture2
            // 
            this.Picture2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.Picture2.Height = 1.181102F;
            this.Picture2.ImageData = ((System.IO.Stream)(resources.GetObject("Picture2.ImageData")));
            this.Picture2.Left = 2.5625F;
            this.Picture2.LineColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.Picture2.Name = "Picture2";
            this.Picture2.PictureAlignment = GrapeCity.ActiveReports.SectionReportModel.PictureAlignment.TopRight;
            this.Picture2.SizeMode = GrapeCity.ActiveReports.SectionReportModel.SizeModes.Zoom;
            this.Picture2.Top = 0.3125F;
            this.Picture2.Width = 0.7238354F;
            // 
            // Picture1
            // 
            this.Picture1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.Picture1.Height = 1.181102F;
            this.Picture1.ImageData = ((System.IO.Stream)(resources.GetObject("Picture1.ImageData")));
            this.Picture1.Left = 0.5625F;
            this.Picture1.LineColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.Picture1.Name = "Picture1";
            this.Picture1.PictureAlignment = GrapeCity.ActiveReports.SectionReportModel.PictureAlignment.TopRight;
            this.Picture1.SizeMode = GrapeCity.ActiveReports.SectionReportModel.SizeModes.Zoom;
            this.Picture1.Top = 0.3125F;
            this.Picture1.Width = 0.7238354F;
            // 
            // shColor1
            // 
            this.shColor1.Height = 0.25F;
            this.shColor1.Left = 0F;
            this.shColor1.Name = "shColor1";
            this.shColor1.RoundingRadius = 9.999999F;
            this.shColor1.Top = 0F;
            this.shColor1.Width = 9.84F;
            // 
            // Label10
            // 
            this.Label10.Height = 0.25F;
            this.Label10.HyperLink = null;
            this.Label10.Left = 7.875F;
            this.Label10.Name = "Label10";
            this.Label10.Style = "color: White; font-size: 9pt; font-weight: bold; text-align: center; vertical-ali" +
                "gn: middle; ddo-char-set: 1";
            this.Label10.Text = "";
            this.Label10.Top = 0F;
            this.Label10.Width = 1.875F;
            // 
            // shColor
            // 
            this.shColor.Height = 0.5F;
            this.shColor.Left = 0F;
            this.shColor.Name = "shColor";
            this.shColor.RoundingRadius = 9.999999F;
            this.shColor.Top = 6.5625F;
            this.shColor.Width = 9.84F;
            // 
            // Line5
            // 
            this.Line5.Height = 0F;
            this.Line5.Left = 0F;
            this.Line5.LineWeight = 1F;
            this.Line5.Name = "Line5";
            this.Line5.Top = 6.5625F;
            this.Line5.Width = 9.84F;
            this.Line5.X1 = 9.84F;
            this.Line5.X2 = 0F;
            this.Line5.Y1 = 6.5625F;
            this.Line5.Y2 = 6.5625F;
            // 
            // Line4
            // 
            this.Line4.Height = 7.055555F;
            this.Line4.Left = 7.87F;
            this.Line4.LineWeight = 1F;
            this.Line4.Name = "Line4";
            this.Line4.Top = 0.006944444F;
            this.Line4.Width = 0F;
            this.Line4.X1 = 7.87F;
            this.Line4.X2 = 7.87F;
            this.Line4.Y1 = 0.006944444F;
            this.Line4.Y2 = 7.0625F;
            // 
            // Line3
            // 
            this.Line3.Height = 7.055555F;
            this.Line3.Left = 5.909999F;
            this.Line3.LineWeight = 1F;
            this.Line3.Name = "Line3";
            this.Line3.Top = 0.006944444F;
            this.Line3.Width = 0F;
            this.Line3.X1 = 5.909999F;
            this.Line3.X2 = 5.909999F;
            this.Line3.Y1 = 0.006944444F;
            this.Line3.Y2 = 7.0625F;
            // 
            // Line2
            // 
            this.Line2.Height = 7.055555F;
            this.Line2.Left = 3.94F;
            this.Line2.LineWeight = 1F;
            this.Line2.Name = "Line2";
            this.Line2.Top = 0.006944444F;
            this.Line2.Width = 0F;
            this.Line2.X1 = 3.94F;
            this.Line2.X2 = 3.94F;
            this.Line2.Y1 = 0.006944444F;
            this.Line2.Y2 = 7.0625F;
            // 
            // Label4
            // 
            this.Label4.Angle = 900;
            this.Label4.Height = 1F;
            this.Label4.HyperLink = null;
            this.Label4.Left = 0.4375F;
            this.Label4.Name = "Label4";
            this.Label4.Style = "font-family: Arial; font-size: 12pt; font-style: normal; font-weight: bold; verti" +
                "cal-align: bottom; ddo-char-set: 0";
            this.Label4.Text = "?????:";
            this.Label4.Top = 5.3125F;
            this.Label4.Width = 0.25F;
            // 
            // Line1
            // 
            this.Line1.Height = 7.055555F;
            this.Line1.Left = 1.97F;
            this.Line1.LineWeight = 1F;
            this.Line1.Name = "Line1";
            this.Line1.Top = 0.006944444F;
            this.Line1.Width = 0F;
            this.Line1.X1 = 1.97F;
            this.Line1.X2 = 1.97F;
            this.Line1.Y1 = 0.006944444F;
            this.Line1.Y2 = 7.0625F;
            // 
            // lbAddress
            // 
            this.lbAddress.Angle = 900;
            this.lbAddress.Height = 4.75F;
            this.lbAddress.HyperLink = null;
            this.lbAddress.Left = 1.0625F;
            this.lbAddress.Name = "lbAddress";
            this.lbAddress.Style = "font-family: Arial; font-size: 12pt; font-style: normal; font-weight: normal; tex" +
                "t-align: left; vertical-align: bottom; ddo-char-set: 0";
            this.lbAddress.Text = "";
            this.lbAddress.Top = 1.5625F;
            this.lbAddress.Width = 0.25F;
            // 
            // lbObekt
            // 
            this.lbObekt.Angle = 900;
            this.lbObekt.Height = 4.75F;
            this.lbObekt.HyperLink = null;
            this.lbObekt.Left = 0.6875F;
            this.lbObekt.Name = "lbObekt";
            this.lbObekt.Style = "font-family: Arial; font-size: 12pt; font-style: normal; font-weight: bold; text-" +
                "align: left; vertical-align: bottom; ddo-char-set: 0";
            this.lbObekt.Text = "";
            this.lbObekt.Top = 1.5625F;
            this.lbObekt.Width = 0.25F;
            // 
            // Label6
            // 
            this.Label6.Angle = 900;
            this.Label6.Height = 3.125F;
            this.Label6.HyperLink = null;
            this.Label6.Left = 1.4375F;
            this.Label6.Name = "Label6";
            this.Label6.Style = "font-family: Arial; font-size: 12pt; font-style: normal; font-weight: bold; verti" +
                "cal-align: bottom; ddo-char-set: 0";
            this.Label6.Text = "????: ???????????";
            this.Label6.Top = 3.1875F;
            this.Label6.Width = 0.3125F;
            // 
            // lbInnerName
            // 
            this.lbInnerName.Height = 0.2F;
            this.lbInnerName.HyperLink = null;
            this.lbInnerName.Left = 0.0625F;
            this.lbInnerName.Name = "lbInnerName";
            this.lbInnerName.Style = "color: White; font-size: 9pt; font-weight: bold; text-align: center; vertical-ali" +
                "gn: middle; ddo-char-set: 1";
            this.lbInnerName.Text = "Label7";
            this.lbInnerName.Top = 0F;
            this.lbInnerName.Width = 1.875F;
            // 
            // lbCode
            // 
            this.lbCode.Height = 0.3125F;
            this.lbCode.HyperLink = null;
            this.lbCode.Left = 0.4375F;
            this.lbCode.Name = "lbCode";
            this.lbCode.Style = "color: White; font-weight: bold; text-align: center";
            this.lbCode.Text = "Label7";
            this.lbCode.Top = 6.625F;
            this.lbCode.Width = 1.25F;
            // 
            // lbInnerName1
            // 
            this.lbInnerName1.Height = 0.2F;
            this.lbInnerName1.HyperLink = null;
            this.lbInnerName1.Left = 2F;
            this.lbInnerName1.Name = "lbInnerName1";
            this.lbInnerName1.Style = "color: White; font-size: 9pt; font-weight: bold; text-align: center; vertical-ali" +
                "gn: middle; ddo-char-set: 1";
            this.lbInnerName1.Text = "";
            this.lbInnerName1.Top = 0F;
            this.lbInnerName1.Width = 1.875F;
            // 
            // lbInnerName2
            // 
            this.lbInnerName2.Height = 0.2F;
            this.lbInnerName2.HyperLink = null;
            this.lbInnerName2.Left = 4F;
            this.lbInnerName2.Name = "lbInnerName2";
            this.lbInnerName2.Style = "color: White; font-size: 9pt; font-weight: bold; text-align: center; vertical-ali" +
                "gn: middle; ddo-char-set: 1";
            this.lbInnerName2.Text = "";
            this.lbInnerName2.Top = 0F;
            this.lbInnerName2.Width = 1.875F;
            // 
            // lbInnerName3
            // 
            this.lbInnerName3.Height = 0.2F;
            this.lbInnerName3.HyperLink = null;
            this.lbInnerName3.Left = 5.9375F;
            this.lbInnerName3.Name = "lbInnerName3";
            this.lbInnerName3.Style = "color: White; font-size: 9pt; font-weight: bold; text-align: center; vertical-ali" +
                "gn: middle; ddo-char-set: 1";
            this.lbInnerName3.Text = "";
            this.lbInnerName3.Top = 0F;
            this.lbInnerName3.Width = 1.875F;
            // 
            // lbCode1
            // 
            this.lbCode1.Height = 0.3125F;
            this.lbCode1.HyperLink = null;
            this.lbCode1.Left = 2.4375F;
            this.lbCode1.Name = "lbCode1";
            this.lbCode1.Style = "color: White; font-weight: bold; text-align: center";
            this.lbCode1.Text = "Label7";
            this.lbCode1.Top = 6.625F;
            this.lbCode1.Width = 1.25F;
            // 
            // lbCode2
            // 
            this.lbCode2.Height = 0.3125F;
            this.lbCode2.HyperLink = null;
            this.lbCode2.Left = 4.375F;
            this.lbCode2.Name = "lbCode2";
            this.lbCode2.Style = "color: White; font-weight: bold; text-align: center";
            this.lbCode2.Text = "Label7";
            this.lbCode2.Top = 6.625F;
            this.lbCode2.Width = 1.25F;
            // 
            // lbCode3
            // 
            this.lbCode3.Height = 0.3125F;
            this.lbCode3.HyperLink = null;
            this.lbCode3.Left = 6.3125F;
            this.lbCode3.Name = "lbCode3";
            this.lbCode3.Style = "color: White; font-weight: bold; text-align: center";
            this.lbCode3.Text = "Label7";
            this.lbCode3.Top = 6.625F;
            this.lbCode3.Width = 1.25F;
            // 
            // lbCode4
            // 
            this.lbCode4.Height = 0.3125F;
            this.lbCode4.HyperLink = null;
            this.lbCode4.Left = 8.3125F;
            this.lbCode4.Name = "lbCode4";
            this.lbCode4.Style = "color: White; font-weight: bold; text-align: center";
            this.lbCode4.Text = "Label7";
            this.lbCode4.Top = 6.625F;
            this.lbCode4.Width = 1.25F;
            // 
            // Label15
            // 
            this.Label15.Angle = 900;
            this.Label15.Height = 1F;
            this.Label15.HyperLink = null;
            this.Label15.Left = 2.4375F;
            this.Label15.Name = "Label15";
            this.Label15.Style = "font-family: Arial; font-size: 12pt; font-style: normal; font-weight: bold; verti" +
                "cal-align: bottom; ddo-char-set: 0";
            this.Label15.Text = "?????:";
            this.Label15.Top = 5.3125F;
            this.Label15.Width = 0.25F;
            // 
            // Label16
            // 
            this.Label16.Angle = 900;
            this.Label16.Height = 1F;
            this.Label16.HyperLink = null;
            this.Label16.Left = 4.4375F;
            this.Label16.Name = "Label16";
            this.Label16.Style = "font-family: Arial; font-size: 12pt; font-style: normal; font-weight: bold; verti" +
                "cal-align: bottom; ddo-char-set: 0";
            this.Label16.Text = "?????:";
            this.Label16.Top = 5.3125F;
            this.Label16.Width = 0.25F;
            // 
            // Label17
            // 
            this.Label17.Angle = 900;
            this.Label17.Height = 1F;
            this.Label17.HyperLink = null;
            this.Label17.Left = 6.375F;
            this.Label17.Name = "Label17";
            this.Label17.Style = "font-family: Arial; font-size: 12pt; font-style: normal; font-weight: bold; verti" +
                "cal-align: bottom; ddo-char-set: 0";
            this.Label17.Text = "?????:";
            this.Label17.Top = 5.3125F;
            this.Label17.Width = 0.25F;
            // 
            // Label18
            // 
            this.Label18.Angle = 900;
            this.Label18.Height = 1F;
            this.Label18.HyperLink = null;
            this.Label18.Left = 8.3125F;
            this.Label18.Name = "Label18";
            this.Label18.Style = "font-family: Arial; font-size: 12pt; font-style: normal; font-weight: bold; verti" +
                "cal-align: bottom; ddo-char-set: 0";
            this.Label18.Text = "?????:";
            this.Label18.Top = 5.3125F;
            this.Label18.Width = 0.25F;
            // 
            // lbObekt1
            // 
            this.lbObekt1.Angle = 900;
            this.lbObekt1.Height = 4.75F;
            this.lbObekt1.HyperLink = null;
            this.lbObekt1.Left = 2.6875F;
            this.lbObekt1.Name = "lbObekt1";
            this.lbObekt1.Style = "font-family: Arial; font-size: 12pt; font-style: normal; font-weight: bold; text-" +
                "align: left; vertical-align: bottom; ddo-char-set: 0";
            this.lbObekt1.Text = "";
            this.lbObekt1.Top = 1.5625F;
            this.lbObekt1.Width = 0.25F;
            // 
            // lbObekt2
            // 
            this.lbObekt2.Angle = 900;
            this.lbObekt2.Height = 4.75F;
            this.lbObekt2.HyperLink = null;
            this.lbObekt2.Left = 4.6875F;
            this.lbObekt2.Name = "lbObekt2";
            this.lbObekt2.Style = "font-family: Arial; font-size: 12pt; font-style: normal; font-weight: bold; text-" +
                "align: left; vertical-align: bottom; ddo-char-set: 0";
            this.lbObekt2.Text = "";
            this.lbObekt2.Top = 1.5625F;
            this.lbObekt2.Width = 0.25F;
            // 
            // lbObekt3
            // 
            this.lbObekt3.Angle = 900;
            this.lbObekt3.Height = 4.75F;
            this.lbObekt3.HyperLink = null;
            this.lbObekt3.Left = 6.625F;
            this.lbObekt3.Name = "lbObekt3";
            this.lbObekt3.Style = "font-family: Arial; font-size: 12pt; font-style: normal; font-weight: bold; text-" +
                "align: left; vertical-align: bottom; ddo-char-set: 0";
            this.lbObekt3.Text = "";
            this.lbObekt3.Top = 1.5625F;
            this.lbObekt3.Width = 0.25F;
            // 
            // lbObekt4
            // 
            this.lbObekt4.Angle = 900;
            this.lbObekt4.Height = 4.75F;
            this.lbObekt4.HyperLink = null;
            this.lbObekt4.Left = 8.5625F;
            this.lbObekt4.Name = "lbObekt4";
            this.lbObekt4.Style = "font-family: Arial; font-size: 12pt; font-style: normal; font-weight: bold; text-" +
                "align: left; vertical-align: bottom; ddo-char-set: 0";
            this.lbObekt4.Text = "";
            this.lbObekt4.Top = 1.5625F;
            this.lbObekt4.Width = 0.25F;
            // 
            // lbAddress1
            // 
            this.lbAddress1.Angle = 900;
            this.lbAddress1.Height = 4.75F;
            this.lbAddress1.HyperLink = null;
            this.lbAddress1.Left = 3.0625F;
            this.lbAddress1.Name = "lbAddress1";
            this.lbAddress1.Style = "font-family: Arial; font-size: 12pt; font-style: normal; font-weight: normal; tex" +
                "t-align: left; vertical-align: bottom; ddo-char-set: 0";
            this.lbAddress1.Text = "";
            this.lbAddress1.Top = 1.5625F;
            this.lbAddress1.Width = 0.25F;
            // 
            // lbAddress2
            // 
            this.lbAddress2.Angle = 900;
            this.lbAddress2.Height = 4.75F;
            this.lbAddress2.HyperLink = null;
            this.lbAddress2.Left = 5.0625F;
            this.lbAddress2.Name = "lbAddress2";
            this.lbAddress2.Style = "font-family: Arial; font-size: 12pt; font-style: normal; font-weight: normal; tex" +
                "t-align: left; vertical-align: bottom; ddo-char-set: 0";
            this.lbAddress2.Text = "";
            this.lbAddress2.Top = 1.5625F;
            this.lbAddress2.Width = 0.25F;
            // 
            // lbAddress3
            // 
            this.lbAddress3.Angle = 900;
            this.lbAddress3.Height = 4.75F;
            this.lbAddress3.HyperLink = null;
            this.lbAddress3.Left = 7F;
            this.lbAddress3.Name = "lbAddress3";
            this.lbAddress3.Style = "font-family: Arial; font-size: 12pt; font-style: normal; font-weight: normal; tex" +
                "t-align: left; vertical-align: bottom; ddo-char-set: 0";
            this.lbAddress3.Text = "";
            this.lbAddress3.Top = 1.5625F;
            this.lbAddress3.Width = 0.25F;
            // 
            // lbAddress4
            // 
            this.lbAddress4.Angle = 900;
            this.lbAddress4.Height = 4.75F;
            this.lbAddress4.HyperLink = null;
            this.lbAddress4.Left = 8.9375F;
            this.lbAddress4.Name = "lbAddress4";
            this.lbAddress4.Style = "font-family: Arial; font-size: 12pt; font-style: normal; font-weight: normal; tex" +
                "t-align: left; vertical-align: bottom; ddo-char-set: 0";
            this.lbAddress4.Text = "";
            this.lbAddress4.Top = 1.5625F;
            this.lbAddress4.Width = 0.25F;
            // 
            // Label27
            // 
            this.Label27.Angle = 900;
            this.Label27.Height = 3.125F;
            this.Label27.HyperLink = null;
            this.Label27.Left = 3.4375F;
            this.Label27.Name = "Label27";
            this.Label27.Style = "font-family: Arial; font-size: 12pt; font-style: normal; font-weight: bold; verti" +
                "cal-align: bottom; ddo-char-set: 0";
            this.Label27.Text = "????: ???????????";
            this.Label27.Top = 3.1875F;
            this.Label27.Width = 0.3125F;
            // 
            // Label28
            // 
            this.Label28.Angle = 900;
            this.Label28.Height = 3.125F;
            this.Label28.HyperLink = null;
            this.Label28.Left = 5.4375F;
            this.Label28.Name = "Label28";
            this.Label28.Style = "font-family: Arial; font-size: 12pt; font-style: normal; font-weight: bold; verti" +
                "cal-align: bottom; ddo-char-set: 0";
            this.Label28.Text = "????: ???????????";
            this.Label28.Top = 3.1875F;
            this.Label28.Width = 0.3125F;
            // 
            // Label29
            // 
            this.Label29.Angle = 900;
            this.Label29.Height = 3.125F;
            this.Label29.HyperLink = null;
            this.Label29.Left = 7.375F;
            this.Label29.Name = "Label29";
            this.Label29.Style = "font-family: Arial; font-size: 12pt; font-style: normal; font-weight: bold; verti" +
                "cal-align: bottom; ddo-char-set: 0";
            this.Label29.Text = "????: ???????????";
            this.Label29.Top = 3.1875F;
            this.Label29.Width = 0.3125F;
            // 
            // Label30
            // 
            this.Label30.Angle = 900;
            this.Label30.Height = 3.125F;
            this.Label30.HyperLink = null;
            this.Label30.Left = 9.3125F;
            this.Label30.Name = "Label30";
            this.Label30.Style = "font-family: Arial; font-size: 12pt; font-style: normal; font-weight: bold; verti" +
                "cal-align: bottom; ddo-char-set: 0";
            this.Label30.Text = "????: ???????????";
            this.Label30.Top = 3.1875F;
            this.Label30.Width = 0.3125F;
            // 
            // lbInnerName4
            // 
            this.lbInnerName4.Height = 0.2F;
            this.lbInnerName4.HyperLink = null;
            this.lbInnerName4.Left = 7.9375F;
            this.lbInnerName4.Name = "lbInnerName4";
            this.lbInnerName4.Style = "color: White; font-size: 9pt; font-weight: bold; text-align: center; vertical-ali" +
                "gn: middle; ddo-char-set: 1";
            this.lbInnerName4.Text = "Label31";
            this.lbInnerName4.Top = 0F;
            this.lbInnerName4.Width = 1.875F;
            // 
            // ActiveReport1
            // 
            this.MasterReport = false;
            this.PageSettings.PaperHeight = 11F;
            this.PageSettings.PaperWidth = 8.5F;
            this.PrintWidth = 9.96875F;
            this.Sections.Add(this.PageHeader);
            this.Sections.Add(this.Detail);
            this.Sections.Add(this.PageFooter);
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule(resources.GetString("$this.StyleSheet"), "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: inherit; font-style: inherit; font-variant: inherit; font-weight: bo" +
                        "ld; font-size: 16pt; font-size-adjust: inherit; font-stretch: inherit", "Heading1", "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: Times New Roman; font-style: italic; font-variant: inherit; font-wei" +
                        "ght: bold; font-size: 14pt; font-size-adjust: inherit; font-stretch: inherit", "Heading2", "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: inherit; font-style: inherit; font-variant: inherit; font-weight: bo" +
                        "ld; font-size: 13pt; font-size-adjust: inherit; font-stretch: inherit", "Heading3", "Normal"));
            ((System.ComponentModel.ISupportInitialize)(this.Picture5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Picture4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Picture3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Picture2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Picture1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbAddress)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbObekt)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbInnerName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbCode)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbInnerName1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbInnerName2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbInnerName3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbCode1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbCode2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbCode3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbCode4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label15)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label16)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label17)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label18)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbObekt1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbObekt2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbObekt3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbObekt4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbAddress1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbAddress2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbAddress3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbAddress4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label27)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label28)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label29)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label30)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbInnerName4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();
        }

		#endregion

        private PageHeader PageHeader;
        private Detail Detail;
        private Shape Shape3;
        private Picture Picture5;
        private Picture Picture4;
        private Picture Picture3;
        private Picture Picture2;
        private Picture Picture1;
        private Shape shColor1;
        private Label Label10;
        private Shape shColor;
        private Line Line5;
        private Line Line4;
        private Line Line3;
        private Line Line2;
        private Label Label4;
        private Line Line1;
        private Label lbAddress;
        private Label lbObekt;
        private Label Label6;
        private Label lbInnerName;
        private Label lbCode;
        private Label lbInnerName1;
        private Label lbInnerName2;
        private Label lbInnerName3;
        private Label lbCode1;
        private Label lbCode2;
        private Label lbCode3;
        private Label lbCode4;
        private Label Label15;
        private Label Label16;
        private Label Label17;
        private Label Label18;
        private Label lbObekt1;
        private Label lbObekt2;
        private Label lbObekt3;
        private Label lbObekt4;
        private Label lbAddress1;
        private Label lbAddress2;
        private Label lbAddress3;
        private Label lbAddress4;
        private Label Label27;
        private Label Label28;
        private Label Label29;
        private Label Label30;
        private Label lbInnerName4;
        private PageFooter PageFooter;
	}
}
