﻿<%@ Register TagPrefix="uc1" TagName="PageHeader" Src="UserControls/PageHeader.ascx" %>
<%@ Page language="c#" Codebehind="AbsenceRequest.aspx.cs" AutoEventWireup="false" Inherits="Asa.Timesheet.WebPages.AbsenceRequest" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
<head runat="server">
	<title>Заяви отпуск</title>
	<meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
	<meta content="C#" name="CODE_LANGUAGE">
	<meta content="JavaScript" name="vs_defaultClientScript">
	<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
	<LINK href="styles/timesheet.css" type="text/css" rel="stylesheet">
</HEAD>
<body bottomMargin="0" leftMargin="0" topMargin="0" rightMargin="0">
	<form id="Form1" method="post" runat="server">
	<table  cellSpacing="0" cellPadding="0"  border="0" width="100%">
			<tr >
				<td><uc1:pageheader id="header" runat="server"></uc1:pageheader></td>
			</tr>

			<tr>
				<td>
					<table  cellSpacing="0" cellPadding="0" border="0" height="50%">
						<tr>
							<td class="menuLeft"><asp:placeholder id="menuHolder" runat="server"></asp:placeholder></td>
								
							<td  vAlign="top" noWrap>
                    
                            <asp:label id="lblError" runat="server" ForeColor="Red" EnableViewState="False" CssClass="ErrorLabel"></asp:label>
                            <asp:label id="lblInfo" runat="server" EnableViewState="False" CssClass="SuccessAlert"></asp:label>
                
								<TABLE id="tblRequestAbsence" runat="server" cellSpacing="0" cellPadding="0"  border="0"  width="100%" >
                                    <tr>
                                        <td><asp:label id="lbStt" runat="server" CssClass="EnterDataLabel" style="color: White;">Начална дата:</asp:label><asp:label id="lbSt" runat="server" ForeColor="white" style="font-size: 24pt;  width: 90%; font-weight: 100;background: gray; display: block; padding-left: 10%;"></asp:label></td>
                                        <td><asp:label id="lbEnt" runat="server" CssClass="EnterDataLabel"  style="color: White;">Крайна дата:</asp:label><asp:label id="lbEn" runat="server" ForeColor="white"  style="font-size: 24pt; width: 90%; font-weight: 300; background: dimgray;  display: block;padding-left: 27px"></asp:label></td>
                                        <td style="padding: 0 15px; padding-bottom: 0px;" rowspan="3">                                            
                                            <asp:label id="Label1" runat="server" CssClass="enterDataLabel" Width="100%">Тип:</asp:label>
                                            <asp:dropdownlist id="ddlProject" runat="server" CssClass="EnterDataBox" Width="100%"></asp:dropdownlist>
                                            <asp:label id="Label6" runat="server" CssClass="enterDataLabel" Width="100%" Visible=false>Ще бъда заместван от:</asp:label>
                                            <asp:dropdownlist id="ddlUser" runat="server" CssClass="EnterDataBox" Width="100%" Visible=false></asp:dropdownlist></asp:dropdownlist>
                                            <asp:label id="Label2" runat="server" CssClass="enterDataLabel" Width="100%">Забележка:</asp:label>
                                        <asp:textbox id="txtReason" CssClass="EnterDataBox " Width="100%" Runat="server" TextMode="MultiLine"
															Height="165px"></asp:textbox>
                                                            <asp:RequiredFieldValidator id="RequiredFieldValidator1" runat="server" ErrorMessage="Моля въведете причина!"
															ControlToValidate="txtReason"></asp:RequiredFieldValidator>
                                        <asp:button id="btnSendRequestAbsence" CssClass="ActionButton ActionButtonBlock"  Text="Подай молба за отпуск"
															Runat="server"></asp:button></td>
                                    </tr>
                                    <tr>
                                        <td><asp:calendar id="calSt" runat="server" ForeColor="#222222" SelectionMode="DayWeek"
																		BorderColor="#999999" CellPadding="0" BorderWidth="1px" BackColor="White" Height="240px" NextMonthText=""
																		PrevMonthText="" DayNameFormat="FirstLetter" SelectWeekText="+"
																		FirstDayOfWeek="Monday" BorderStyle="Solid" CssClass="aspCalendar" NextPrevFormat="ShortMonth" Font-Bold="True" Font-Size="11pt">
																		<TodayDayStyle ForeColor="#C73B2E" BackColor="White" BorderColor="#C73B2E" 
                                                                            BorderStyle="Solid" BorderWidth="2px" Height="32px" Width="32px"></TodayDayStyle>
																		<SelectorStyle Font-Size="10pt" Font-Names="Verdana" ForeColor="SaddleBrown" 
                                                                            BorderStyle="None"></SelectorStyle>
																		<DayStyle Font-Size="10pt" Font-Bold="False" BorderStyle="Dotted" 
                                                                            BorderColor="#CCCCCC" BorderWidth="1px" Height="30px" Width="32px"></DayStyle>
																		<NextPrevStyle Font-Bold="True" Wrap="False" HorizontalAlign="Center" 
                                                                            Height="16px" BorderStyle="None" VerticalAlign="Middle" Font-Size="9pt" 
                                                                            Font-Underline="True"></NextPrevStyle>
																		<DayHeaderStyle Font-Size="9pt" Height="1px" ForeColor="SaddleBrown"></DayHeaderStyle>
																		<SelectedDayStyle Font-Bold="True" ForeColor="#C73B2E" BorderColor="#C73B2E" 
                                                                            BackColor="White" BorderStyle="Solid" BorderWidth="2px"
                                                                            Font-Size="11pt" Height="32px" Width="32px"></SelectedDayStyle>
																		<TitleStyle Font-Size="13pt" Font-Bold="True" ForeColor="#C73B2E" 
                                                                            BackColor="White" HorizontalAlign="Center"></TitleStyle>
																		<OtherMonthDayStyle ForeColor="Gray" BackColor="#FDFDFD" Font-Size="6pt"></OtherMonthDayStyle>
																		<WeekendDayStyle Font-Bold="True" Font-Italic="False" 
                                                                            ForeColor="#C73B2E" />
																	</asp:calendar></td>
                                        <td><asp:calendar id="calEn" runat="server" ForeColor="#222222"  SelectionMode="DayWeek"
																		BorderColor="#999999" CellPadding="0" BorderWidth="1px" BackColor="White" Height="240px" NextMonthText=""
																		PrevMonthText="" DayNameFormat="FirstLetter" SelectWeekText="+"
																		FirstDayOfWeek="Monday" BorderStyle="Solid" CssClass="aspCalendar" NextPrevFormat="ShortMonth" Font-Bold="True" Font-Size="11pt">
																		<TodayDayStyle ForeColor="#C73B2E" BackColor="White" BorderColor="#C73B2E" 
                                                                            BorderStyle="Solid" BorderWidth="2px" Height="32px" Width="32px"></TodayDayStyle>
																		<SelectorStyle Font-Size="10pt" Font-Names="Verdana" ForeColor="SaddleBrown" 
                                                                            BorderStyle="None"></SelectorStyle>
																		<DayStyle Font-Size="10pt" Font-Bold="False" BorderStyle="Dotted" 
                                                                            BorderColor="#CCCCCC" BorderWidth="1px" Height="30px" Width="32px"></DayStyle>
																		<NextPrevStyle Font-Bold="True" Wrap="False" HorizontalAlign="Center" 
                                                                            Height="16px" BorderStyle="None" VerticalAlign="Middle" Font-Size="9pt" 
                                                                            Font-Underline="True"></NextPrevStyle>
																		<DayHeaderStyle Font-Size="9pt" Height="1px" ForeColor="SaddleBrown"></DayHeaderStyle>
																		<SelectedDayStyle Font-Bold="True" ForeColor="#C73B2E" BorderColor="#C73B2E" 
                                                                            BackColor="White" BorderStyle="Solid" BorderWidth="2px"
                                                                            Font-Size="11pt" Height="32px" Width="32px"></SelectedDayStyle>
																		<TitleStyle Font-Size="13pt" Font-Bold="True" ForeColor="#C73B2E" 
                                                                            BackColor="White" HorizontalAlign="Center"></TitleStyle>
																		<OtherMonthDayStyle ForeColor="Gray" BackColor="#FDFDFD" Font-Size="6pt"></OtherMonthDayStyle>
																		<WeekendDayStyle Font-Bold="True" Font-Italic="False" 
                                                                            ForeColor="#C73B2E" />
																	</asp:calendar></td>

                                        <td width="10%"></td>
                                    </tr>
                                    <tr>


                                    </tr>
                                    <tr style="background: #fefefe;">
                                        <td><asp:label id="Label3" runat="server" CssClass="enterDataLabel" Width="100%">Отпуск от минали години:</asp:label>
                                        <asp:Label id="lbVacationPast" runat="server" CssClass="InfoLabel" ></asp:Label></td>
                                        <td><asp:label id="Label4" runat="server" CssClass="enterDataLabel" Width="100%" >Използван платен отпуск:</asp:label>
                                        <asp:Label id="lbUsedVacation" runat="server" CssClass="InfoLabel" style="color: Red"></asp:Label></td>
                                        <td><asp:label id="Label5" runat="server" CssClass="enterDataLabel" Width="100%" > Полагаем платен отпуск :</asp:label>
                                        <asp:Label id="lbVacationRest" runat="server" CssClass="InfoLabel" style="color: Teal"></asp:Label></td>
                                    </tr>
									<tr>
										<td>
                                        <asp:datagrid id="grdAbsence" runat="server" EnableViewState="False" AutoGenerateColumns="False" Visible=false
												BorderStyle="None">
												<Columns>
													<asp:TemplateColumn>
														<ItemTemplate>
															<asp:datagrid id="grdReport" runat="server" ForeColor="DimGray" EnableViewState="False" CssClass="ReportGrid"
																CellPadding="4" AutoGenerateColumns="False" Border="0">
																<HeaderStyle CssClass="reportGridHeader"></HeaderStyle>
																<FooterStyle Font-Bold="True" ForeColor="Black"></FooterStyle>
																<Columns>
																	<asp:BoundColumn DataField="Name" HeaderText="Отсъствие" FooterText="Общо за проекта:">
																		<ItemStyle Width="250px"></ItemStyle>
																	</asp:BoundColumn>
																	<asp:BoundColumn DataField="DaysString" HeaderText="Дати">
																		<ItemStyle Wrap="False" Width="400px"></ItemStyle>
																	</asp:BoundColumn>
																	<asp:BoundColumn DataField="DaysCount" HeaderText="Общо дни">
																		<ItemStyle HorizontalAlign="Center"></ItemStyle>
																	</asp:BoundColumn>
																</Columns>
															</asp:datagrid>
														</ItemTemplate>
													</asp:TemplateColumn>
												</Columns>
											</asp:datagrid>
										</td>
									</tr>
                                    <tr>
                                    <td colspan="4">
                                        <asp:Label ID="lbInformation" Runat="server" style="font-style=italic">* Може да се подаде молба за отпуск не по-късно от три дни преди желаната начална дата.</asp:Label>
                                        </td>
                                    </tr>
								</TABLE>
							</td>
						</tr>
					</table>

	</form>
     <div id="blockScreen" class="blockScreen" style="display:none"><p class="SuccessAlert" ><strong>Моля, изчакайте! </strong>Молбата се обработва...</p></div>
    <script type="text/javascript">
        function ShowLoading(e) {
            // var divBg = document.createElement('div');


            document.getElementById('blockScreen').style.display = 'block';

            // These 2 lines cancel form submission, so only use if needed.
            //window.event.cancelBubble= true;
            //e.stopPropagation();
        }

        function HideLoading() {
            //alert('hideloading');
            document.getElementById("form1").onsubmit = null;
            document.getElementById('blockScreen').style.display = 'none';
            //alert('done');
        }
    </script>
   
</body>
</HTML>
