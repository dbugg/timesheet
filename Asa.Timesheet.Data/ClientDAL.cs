using System;
using System.Data;
using System.Data.SqlClient; 
using System.Collections;
using System.Collections.Specialized;
using Microsoft.ApplicationBlocks.Data;

namespace Asa.Timesheet.Data
{
	public class ClientDAL
	{
		/// <summary>
		/// Loads a ClientData object from the database using the given ClientID
		/// </summary>
		/// <param name="clientID">The primary key value</param>
		/// <returns>A ClientData object</returns>
		public static ClientData Load(int clientID)
		{
			SqlParameter[] parameterValues = new SqlParameter[] { new SqlParameter("@ClientID", clientID) };
			using (SqlDataReader reader = SqlHelper.ExecuteReader(CommonDAL.ConnectionString, CommandType.StoredProcedure, "ClientsSelProc", parameterValues))
			{
				if (reader.Read())
				{
					ClientData result = new ClientData();
					LoadFromReader(reader,result);
					return result;
				}
				else
					return null;
			}
		}

		/// <summary>
		/// Loads a ClientData object from the database using the given where clause
		/// </summary>
		/// <param name="whereClause">The filter expression for the query</param>
		/// <returns>A ClientData object</returns>
		public static ClientData Load(string whereClause)
		{
			SqlParameter[] parameterValues = new SqlParameter[] { new SqlParameter("@WhereClause", whereClause) };
			using (SqlDataReader reader = SqlHelper.ExecuteReader(CommonDAL.ConnectionString, CommandType.StoredProcedure, "ClientsListProc", parameterValues))
			{
				if (reader.Read())
				{
					ClientData result = new ClientData();
					LoadFromReader(reader,result);
					return result;
				}
				else
					return null;
			}
		}
		public static int Save( ClientData source)
		{
			if (source.ClientID == -1)
				return Insert(source);
			else
				return Update(source);	
		}

		public static void Delete( ClientData source)
		{
			Delete(source.ClientID);
		}
		/// <summary>
		/// Loads a collection of ClientData objects from the database.
		/// </summary>
		/// <returns>A collection containing all of the ClientData objects in the database.</returns>
		public static ClientsVector LoadCollection(int ClientTypeID)
		{
			SqlParameter[] parms;
			if(ClientTypeID>0)
				 parms = new SqlParameter[] { new SqlParameter("@ClientTypeID", ClientTypeID) };
			else
				parms = new SqlParameter[] { };
			return LoadCollection("ClientsListProc", parms);
		}
	  
		/// <summary>
		/// Saves a collection of ClientData objects to the database.
		/// </summary>
		/// <returns>A collection containing all of the ClientData objects in the database.</returns>
		public static  void  SaveCollection(ClientsVector col,string spName, SqlParameter[] parms)
		{
			ClientsVector colOld = LoadCollection(spName,parms);
			UpdateCollection(colOld,col);
		}
		public static  void  SaveCollection(ClientsVector col)
		{
			ClientsVector colOld = LoadCollection(-1);
			UpdateCollection(colOld,col);
		}
		public static  void  SaveCollection(ClientsVector col, string whereClause )
		{
			ClientsVector colOld = LoadCollection(whereClause);
			UpdateCollection(colOld,col);
		}
		/// <summary>
		/// Loads a collection of ClientData objects from the database.
		/// </summary>
		/// <returns>A collection containing all of the ClientData objects in the database.</returns>
		public static ClientsVector LoadCollection(string whereClause)
		{
			SqlParameter[] parms = new SqlParameter[] { new SqlParameter("@WhereClause", whereClause) };
			return LoadCollection("ClientsListProc", parms);
		}
		public static ClientsVector LoadCollectionByProject(int nProjectID)
		{
			SqlParameter[] parms = new SqlParameter[] { new SqlParameter("@ProjectID", nProjectID) };
			return LoadCollection("ClientsListForProjectsProc", parms);
		}
		/// <summary>
		/// Loads a collection of ClientData objects from the database.
		/// </summary>
		/// <returns>A collection containing all of the ClientData objects in the database.</returns>
		public static ClientsVector LoadCollection(string spName, SqlParameter[] parms)
		{
			ClientsVector result = new ClientsVector();
			using (SqlDataReader reader = SqlHelper.ExecuteReader(CommonDAL.ConnectionString, CommandType.StoredProcedure, spName, parms))
			{
				while (reader.Read())
				{
					ClientData tmp = new ClientData();
					LoadFromReader(reader, tmp);
					result.Add(tmp);
				}
			}
			return result;
		}
		public static StringCollection LoadMailsByProject(int ProjectID,int ClientTypeID)
		{
			string spName = "ProjectClientsListMailsProc";
			SqlParameter[] parms = new SqlParameter[2];
			parms[0]= new System.Data.SqlClient.SqlParameter("@ProjectID", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.Input, true, 10, 0, "ProjectID", DataRowVersion.Current, null);
			parms[0].Value = ProjectID;
			parms[1]= new System.Data.SqlClient.SqlParameter("@ClientType", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.Input, true, 10, 0, "ClientType", DataRowVersion.Current, null);
			parms[1].Value = ClientTypeID;
			StringCollection result = new StringCollection();
			using (SqlDataReader reader = SqlHelper.ExecuteReader(CommonDAL.ConnectionString, CommandType.StoredProcedure, spName, parms))
			{
				while (reader.Read())
				{
					string tmp = "";
					if (reader != null && !reader.IsClosed)
					{
						if (!reader.IsDBNull(0)) tmp = reader.GetString(0);
					}
					if(tmp.Length>0)
						result.Add(tmp);
				}
			}
			return result;
		}
		/// <summary>
		/// Deletes a ClientData object from the database.
		/// </summary>
		/// <param name="clientID">The primary key value</param>
		public static void Delete(int clientID)
		{
			SqlParameter[] parameterValues = new SqlParameter[] { new SqlParameter("@ClientID", clientID) };
			SqlHelper.ExecuteNonQuery(CommonDAL.ConnectionString, CommandType.StoredProcedure, "ClientsInactiveProc", parameterValues);
		}

		public static void LoadFromReader(SqlDataReader reader, ClientData target)
		{
			if (reader != null && !reader.IsClosed)
			{
				//target.Key = reader.GetInt32(0);
				if (!reader.IsDBNull(0)) target.ClientID = reader.GetInt32(0);
				if (!reader.IsDBNull(1)) target.ClientName = reader.GetString(1);
				if (!reader.IsDBNull(2)) target.City = reader.GetString(2);
				if (!reader.IsDBNull(3)) target.Address = reader.GetString(3);
				if (!reader.IsDBNull(4)) target.Email = reader.GetString(4);
				if (!reader.IsDBNull(5)) target.IsActive = reader.GetBoolean(5);
				if (!reader.IsDBNull(6)) target.Fax = reader.GetString(6);
				
				if (!reader.IsDBNull(7)) target.Manager = reader.GetString(7);
				//if (!reader.IsDBNull(8)) target.ManagerTitleID = reader.GetInt32(8);
				if (!reader.IsDBNull(8)) target.Representative1 = reader.GetString(8);
				if (!reader.IsDBNull(9)) target.Representative2 = reader.GetString(9);
				if (!reader.IsDBNull(10)) target.Delo = reader.GetString(10);
				if (!reader.IsDBNull(11)) target.Bulstat = reader.GetString(11);
				if (!reader.IsDBNull(12)) target.NDR = reader.GetString(12);
				if (!reader.IsDBNull(13)) target.Phone = reader.GetString(13);
				if (!reader.IsDBNull(14)) target.Webpage = reader.GetString(14);
				if (!reader.IsDBNull(15)) target.Phone1 = reader.GetString(15);
				if (!reader.IsDBNull(16)) target.Email1 = reader.GetString(16);
				if (!reader.IsDBNull(17)) target.Phone2 = reader.GetString(17);
				if (!reader.IsDBNull(18)) target.Email2 = reader.GetString(18);
				if (!reader.IsDBNull(19)) target.Status = reader.GetInt32(19);
				if (!reader.IsDBNull(20)) target.ClientType = reader.GetInt32(20);
				if (!reader.IsDBNull(21)) target.AccountName = reader.GetString(21);
				if (!reader.IsDBNull(22)) target.AccountPass = reader.GetString(22);
				if (!reader.IsDBNull(23)) target.PMMail = reader.GetString(23);
				if(reader.FieldCount>25)
				{
					if (!reader.IsDBNull(24)) target.ClientNameEN = reader.GetString(24);
					if (!reader.IsDBNull(25)) target.RepresentativeEN = reader.GetString(25);
				}
			}
		}
		
	  
		public static int Insert( ClientData source)
		{
			SqlParameter[] parameterValues = GetInsertParameterValues(source);
			SqlHelper.ExecuteNonQuery(CommonDAL.ConnectionString, CommandType.StoredProcedure, "ClientsInsProc", parameterValues);
			source.ClientID = (int) parameterValues[0].Value;
			return source.ClientID;
		}

		public static int Update( ClientData source)
		{
			SqlParameter[] parameterValues = GetUpdateParameterValues(source);
			SqlHelper.ExecuteNonQuery(CommonDAL.ConnectionString, CommandType.StoredProcedure,
				"ClientsUpdProc", parameterValues);
			return source.ClientID;
		}
		private static void UpdateCollection(ClientsVector colOld,ClientsVector col)
		{
			// Delete all old items
			foreach( ClientData itemOld in colOld)
			{
				if(!ExistsKey(itemOld.ClientID, col))
					Delete(itemOld.ClientID);
			}
			foreach( ClientData item in col)
			{
				if(item.ClientID <= -1)
					Insert(item);
				else
					Update(item);
			}
		}
		private static bool ExistsKey(int Key, ClientsVector col)
		{
			foreach( ClientData item in col)
				if(item.ClientID==Key)
					return true;
			return false;
		}

		private static SqlParameter GetSqlParameter(string name, ParameterDirection direction, object value)
		{
			SqlParameter p = new SqlParameter(name, value);
			p.Direction = direction;
			return p;
		}

		private static SqlParameter[] GetInsertParameterValues( ClientData source)
		{
			SqlParameter[] prams = new SqlParameter[25];
			prams[0] = GetSqlParameter("@ClientID", ParameterDirection.Output, source.ClientID);
			prams[1] = GetSqlParameter("@ClientName", ParameterDirection.Input, source.ClientName);
			prams[2] = GetSqlParameter("@City", ParameterDirection.Input, source.City);
			prams[3] = GetSqlParameter("@Address", ParameterDirection.Input, source.Address);
			prams[4] = GetSqlParameter("@Email", ParameterDirection.Input, source.Email);
			prams[6] = GetSqlParameter("@Fax", ParameterDirection.Input, source.Fax);
			prams[5] = GetSqlParameter("@IsActive", ParameterDirection.Input, source.IsActive);
			prams[7] = GetSqlParameter("@Manager", ParameterDirection.Input, source.Manager);
			//prams[8] = GetSqlParameter("@ManagerTitleID", ParameterDirection.Input, source.ManagerTitleID);
			prams[8] = GetSqlParameter("@Representative1", ParameterDirection.Input, source.Representative1);
			prams[9] = GetSqlParameter("@Representative2", ParameterDirection.Input, source.Representative2);
			prams[10] = GetSqlParameter("@Delo", ParameterDirection.Input, source.Delo);
			prams[11] = GetSqlParameter("@Bulstat", ParameterDirection.Input, source.Bulstat);
			prams[12] = GetSqlParameter("@NDR", ParameterDirection.Input, source.NDR);
			prams[13] = GetSqlParameter("@Phone", ParameterDirection.Input, source.Phone);
			prams[14] = GetSqlParameter("@Webpage", ParameterDirection.Input, source.Webpage);
			prams[15] = GetSqlParameter("@Phone1", ParameterDirection.Input, source.Phone1);
			prams[16] = GetSqlParameter("@Email1", ParameterDirection.Input, source.Email1);
			prams[17] = GetSqlParameter("@Phone2", ParameterDirection.Input, source.Phone2);
			prams[18] = GetSqlParameter("@Email2", ParameterDirection.Input, source.Email2);
			//prams[19] = GetSqlParameter("@Status", ParameterDirection.Input, source.Status);
			
			prams[19] = GetSqlParameter("@ClientTypeID", ParameterDirection.Input, source.ClientType);
			prams[20] = GetSqlParameter("@AccountName", ParameterDirection.Input, source.AccountName);
			prams[21] = GetSqlParameter("@AccountPass", ParameterDirection.Input, source.AccountPass);
			prams[22] = GetSqlParameter("@PMMail", ParameterDirection.Input, (source.PMMail.Length>0)? (object)source.PMMail:DBNull.Value);
			prams[23] = GetSqlParameter("@ClientNameEN", ParameterDirection.Input, source.ClientNameEN);
			prams[24] = GetSqlParameter("@RepresentativeEN", ParameterDirection.Input, source.RepresentativeEN);

			return prams;
		}

		private static SqlParameter[] GetUpdateParameterValues( ClientData source)
		{
			return new SqlParameter[] {
										  new SqlParameter("@ClientID", source.ClientID),
										  new SqlParameter("@ClientName", source.ClientName),
										  new SqlParameter("@City", source.City),
										  new SqlParameter("@Address", source.Address),
										  new SqlParameter("@Email", source.Email),
										  new SqlParameter("@Fax", source.Fax),
										  //new SqlParameter("@IsActive", source.IsActive),
										  new SqlParameter("@Manager", source.Manager),
										  //new SqlParameter("@ManagerTitleID", source.ManagerTitleID),
										  new SqlParameter("@Representative1", source.Representative1),
										  new SqlParameter("@Representative2", source.Representative2),
										  new SqlParameter("@Delo", source.Delo),
										  new SqlParameter("@Bulstat", source.Bulstat),
										  new SqlParameter("@NDR", source.NDR),
										  new SqlParameter("@Phone", source.Phone),
										  new SqlParameter("@Webpage", source.Webpage),
										  new SqlParameter("@Phone1", source.Phone1),
										  new SqlParameter("@Email1", source.Email1),
										  new SqlParameter("@Phone2", source.Phone2),
										  new SqlParameter("@Email2", source.Email2),
										  //new SqlParameter("@Status", source.Status),
										  new SqlParameter("@ClientTypeID", source.ClientType),
										  new SqlParameter("@AccountName", source.AccountName),
										  new SqlParameter("@AccountPass", source.AccountPass),
										  new SqlParameter("@PMMail",  (source.PMMail.Length>0)?(object) source.PMMail:DBNull.Value),
new SqlParameter("@ClientNameEN", source.ClientNameEN),
										 new SqlParameter("@RepresentativeEN", source.RepresentativeEN),
									  };
		}
	}
}   

