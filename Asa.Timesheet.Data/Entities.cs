using System;

namespace Asa.Timesheet.Data.Entities
{
	#region Pages
	public enum Pages
	{
		Undefined=0,
		Clients,
		Emails,
		Hours,
		MailForm,
		Projects,
		Users,
		Minutes,
		WorkTimes,
		Subcontracters,
		ProjectSubcontracters,
		Profile,
		Subprojects,
		Meetings
	}
	#endregion

	#region UserInfo

	public class UserInfo
	{
		private int _userID;
		private int _userRoleID;
		private string _account;
		private string _firstName;
		private string _middleName;
		private string _lastName;
		private string _fullName;
		private string _mail;
		private string _occupation = String.Empty;
		private string _userRole = String.Empty;
		private string _ext = String.Empty;
		private string _mobile = String.Empty;
		private string _homePhone = String.Empty;
		private string _cV = String.Empty;
		private decimal _salary=0;
		private decimal _bonus=0;
		private bool _hasWorkTime = true;
		private DateTime _workStartedDate = Constants.DateMax;
		private bool _hasWorkTimeOver = false;
		private int _manager;
		private bool _isTraine=false;
		private string _userNameEN="";
		private string _privateEmail="";
        private decimal _coefficient = 1;

     
		public UserInfo()
		{
			_userID = 0;
			_userRoleID = 0;
			_account = String.Empty;
			_firstName = String.Empty;
			_middleName = String.Empty;
			_lastName = String.Empty;
			_fullName = String.Empty;
			_mail = String.Empty;
		}
		public decimal Salary
		{
			get { return _salary; }
			set { _salary = value; }
		}
		public bool IsTraine
		{
			get { return _isTraine; }
			set { _isTraine = value; }
		}
		
		public decimal Bonus
		{
			get { return _bonus; }
			set { _bonus = value; }
		}
		public int UserID
		{
			get { return _userID; }
			set { _userID = value; }
		}

		public int UserRoleID
		{
			get { return _userRoleID; }
			set { _userRoleID = value; }
		}

		public string Account
		{
			get { return _account; }
			set { _account = value; }
		}
		public string PrivateEmail
		{
			get { return _privateEmail; }
			set { _privateEmail = value; }
		}

		public string UserName
		{
			get { return (_firstName + " " + _lastName).TrimStart(null); }
		}

		public string FullName
		{
			get { return _fullName; }
			set { _fullName = value; }
		}


		public string FirstName
		{
			get { return _firstName; }
			set { _firstName = value; }
		}

		public string MiddleName
		{
			get { return _middleName; }
			set { _middleName = value; }
		}

		public string LastName
		{
			get { return _lastName; }
			set { _lastName = value; }
		}

		public string Mail
		{
			get { return _mail; }
			set { _mail = value; }
		}
		public bool HasPaymentRights
		{
			get { return (_userRoleID==(int)Roles.Director || _userRoleID==(int)Roles.MainAccount || _userRoleID==(int)Roles.Account); }
		}
		public bool IsLeader
		{
			get { return (_userRoleID==(int)Roles.Director  || _userRoleID==(int)Roles.MainAccount ); }
		}
		public bool IsAdmin
		{
			get { return (_userRoleID==(int)Roles.Admin); }
		}
		public bool IsAccountant
		{
			get { return (_userRoleID==(int)Roles.MainAccount || _userRoleID==(int)Roles.Account); }
		}
		public bool IsAccountantLow
		{
			get { return (_userRoleID==(int)Roles.Account); }
		}
		public bool IsASI
		{
			get { return (_userRoleID==(int)Roles.Ingener); }
		}
		public bool IsAssistantOnly
		{
			get { return (_userRoleID==(int)Roles.Assistant ) ; }
		}
		public bool IsAssistant
		{
			get { return (_userRoleID==(int)Roles.Assistant||_userRoleID==(int)Roles.Employ|| _userRoleID==(int)Roles.MainAccount|| _userRoleID==(int)Roles.Technical || _userRoleID==(int)Roles.Ingener || _userRoleID==(int)Roles.Secretary || _userRoleID==(int)Roles.Account ||_userRoleID==(int)Roles.Cleaner||_userRoleID==(int)Roles.Layer||_userRoleID==(int)Roles.Driver) ; }
		}
		public bool IsAchitect
		{
			get{return _occupation == Resource.ResourceManager["EditUserOccupationAchitekt"];}
		}
		public bool IsAuthenticated
		{
			get { return (_userID!=0); }
		}

		public bool IsEmployee
		{
			get { return (_userRoleID==(int)Roles.Employ); }
		}
		public bool IsSecretary
		{
			get { return (_userRoleID==(int)Roles.Secretary); }
		}

		public bool IsLayer
		{
			get { return (_userRoleID==(int)Roles.Layer); }
		}
		public bool HasRole
		{
			get { return (_userRoleID>0); }
		}
		public string Occupation
		{
			get { return _occupation; }
			set { _occupation = value; }
		}
	
		public string UserRole
		{
			get { return _userRole; }
			set { _userRole = value; }
		}
	
		public string Ext
		{
			get { return _ext; }
			set { _ext = value; }
		}
	
		public string Mobile
		{
			get { return _mobile; }
			set { _mobile = value; }
		}
	
		public string HomePhone
		{
			get { return _homePhone; }
			set { _homePhone = value; }
		}
	
		public string CV
		{
			get { return _cV; }
			set { _cV = value; }
		}
		public string UserNameEN
		{
			get { return _userNameEN; }
			set { _userNameEN = value; }
		}
		public bool HasWorkTime
		{
			get { return _hasWorkTime; }
			set { _hasWorkTime = value; }
		}
		public DateTime WorkStartedDate
		{
			get { return _workStartedDate; }
			set { _workStartedDate = value; }
		}
		//Add by Ivailo date: 04.12.2007
		public bool HasWorkTimeOver
		{
			get { return _hasWorkTimeOver; }
			set { _hasWorkTimeOver = value; }
		}
		public int Manager
		{
			get { return _manager; }
			set { _manager = value; }
		}

        public decimal Coefficient
        {
            get { return _coefficient; }
            set { _coefficient = value; }
        }
	}

	#endregion

	#region ShortProjectInfo

	public class ShortProjectInfo
	{
		int _projectID;
		string _projectName;


		public ShortProjectInfo(int projectID, string projectName)
		{
			_projectID = projectID;
			_projectName = projectName;
		}

		public int ProjectID
		{
			get { return _projectID; }
		}

		public string ProjectName
		{
			get { return _projectName; }
		}

	}

	#endregion

	#region ShortActivityInfo

	public class ShortActivityInfo
	{
		int _activityID;
		string _activityName;

		public ShortActivityInfo(int activityID, string activityName)
		{
			_activityID = activityID;
			_activityName = activityName;
		}

		public int ActivityID
		{
			get { return _activityID; }
		}

		public string ActivityName
		{
			get { return _activityName; }
		}
	}

	#endregion

	#region TimeInfo

	public class TimeInfo
	{
		int _timeID;
		string _timeString;

		public TimeInfo(int timeID, string timeString)
		{
			_timeID = timeID;
			_timeString = timeString;
		}

		public int TimeID
		{
			get { return _timeID; }
		}

		public string TimeString
		{
			get { return _timeString; }
		}
	}

	#endregion

	#region WorkTimeInfo

	public class WorkTimeInfo
	{
		private int _workTimeID;
		private DateTime _workDate;
		private int _userID;
		private int _projectID;
		private int _activityID;
		private int _startTimeID;
		private int _endTimeID;
		private string _minutes;
		private string _adminMinutes;
		private DateTime _dateEntered;
        private int _meetingId;

        public int MeetingId
        {
            get { return _meetingId; }
            set { _meetingId = value; }
        }
		private int _status = 0;

		public WorkTimeInfo()
		{
		
		}

		public int Status
		{
			get { return _status; }
			set { _status = value; }
		}

		public int WorkTimeID
		{
			get { return _workTimeID; }
			set { _workTimeID = value; }
		}

		public int UserID
		{
			get { return _userID; }
			set { _userID = value; }
		}

		public DateTime WorkDate
		{
			get { return _workDate; }
			set { _workDate = value; }
		}

		public int ActivityID
		{
			get { return _activityID; }
			set { _activityID = value; }
		}

		public int ProjectID
		{
			get { return _projectID; }
			set { _projectID = value; }
		}

		public int StartTimeID
		{
			get { return _startTimeID; }
			set {  _startTimeID = value; }
		}

		public int EndTimeID
		{
			get { return _endTimeID; }
			set { _endTimeID = value; }
		}

		public string Minutes
		{
			get { return _minutes; }
			set { _minutes = value; }
		}

		public string AdminMinutes
		{
			get { return _adminMinutes; }
			set { _adminMinutes = value; }
		}

		public DateTime DateEntered
		{
			get { return _dateEntered; }
			set { _dateEntered = value; }
		}
	}

	#endregion

	#region TimeInterval

	public class TimeInterval
	{
		int _startTime;
		int _endTime;

		public TimeInterval()
		{
			_startTime = 0;
			_endTime = 0;
		}

		public TimeInterval(int startTime, int endTime)
		{
			_startTime = startTime;
			_endTime = endTime;
		}

		public TimeInterval(string sStartTime, string sEndTime)
		{

		}

		public int StartTime
		{
			get { return _startTime; }
			set { _startTime = value; }
		}

		public int EndTime
		{
			get { return _endTime; }
			set { _endTime = value; }
		}
	}

	#endregion

	#region MenuItem info
	
	public class MenuItemInfo
	{
		private string _title;
		private string _navigateUrl;
		private bool _newWindow=false;
		private string _previousPage;
		private bool _highlighted=false;

		public MenuItemInfo(string title, string navigateUrl)
		{
			_title = title;
			_navigateUrl = navigateUrl;
		}
		public MenuItemInfo(string title, string navigateUrl, bool highlighted)
		{
			_title = title;
			_navigateUrl = navigateUrl;
			_highlighted = highlighted;
		}
		public MenuItemInfo(string title, string navigateUrl, bool newWindow, string prev)
		{
			_title = title;
			_navigateUrl = navigateUrl;
			_newWindow=newWindow;
			_previousPage=prev;
		}
		public string Title
		{
			get { return _title; }
			set { _title = value; }
		}

		public string NavigateUrl
		{
			get { return _navigateUrl; }
			set { _navigateUrl = value; }
		}

		public bool Highlighted
		{
			get { return _highlighted; }
			set { _highlighted = value; }
		}

		public bool NewWindow
		{
			get { return _newWindow; }
			set { _newWindow = value; }
		}
		public string PreviousPage
		{
			get { return _previousPage; }
			set { _previousPage = value; }
		}
		
	}

	#endregion

	#region MailInfo
/*
	public class MailInfo
	{
		public int UserID;
		public DateTime StartDate;
		public DateTime EndDate;
		public int ProjectID;
		public System.Collections.Specialized.StringCollection Minutes;
		public System.Collections.Specialized.StringCollection AdminMinutes;
		public bool IsFromHours=false;
		public MailInfo()
		{
			Minutes = new System.Collections.Specialized.StringCollection();
			AdminMinutes= new System.Collections.Specialized.StringCollection();
		}
	}
*/
	#endregion

	#region ProjectsInfo
	
	public class ProjectsInfo
	{
		public string SearchText;
		public string BuildingType;	
		public int TypeID;
	
	
	}
	#endregion

	#region DaysListInfo

	public class DaysListInfo
	{
		private int _userID;
		private string _userName;
		private string _daysString;
		private int _days;

		public DaysListInfo(int userID, string userName, string daysString)
		{
			_userID = userID;
			_userName = userName;
			_daysString = daysString;
		}

		public DaysListInfo(int userID, string userName, System.Collections.ArrayList daysList, bool includeWeekends)
		{
			_userID = userID;
			_userName = userName;
			_daysString = GetDaysString(daysList, includeWeekends, "dd.MM.yy");
		}
		
		public static string GetDaysString(System.Collections.ArrayList daysList, bool weekendsInEndOfPeriod, string format)
		{
			string daysString = String.Empty;

			if (daysList.Count == 0) return daysString;

			DateTime startDate = (DateTime)daysList[0];
			DateTime endDate = (DateTime)daysList[0];
				
			for (int i=0; i<daysList.Count; i++)
			{
				DateTime tempDate = (i+1<daysList.Count) ? (DateTime)daysList[i+1] : (DateTime)daysList[i];

				if (endDate.Date.AddDays(1) == tempDate.Date) endDate = tempDate;
				else
				{
					if (!weekendsInEndOfPeriod)
					{
						while(CalendarDAL.IsHoliday(startDate) && startDate<endDate)
							startDate=startDate.AddDays(1);
//						if (startDate.DayOfWeek == DayOfWeek.Sunday) startDate = startDate.AddDays(1);
//						else if (startDate.DayOfWeek == DayOfWeek.Saturday) startDate = startDate.AddDays(2);

						while(CalendarDAL.IsHoliday(endDate) && startDate<endDate)
							endDate=endDate.AddDays(-1);
//						if (endDate.DayOfWeek == DayOfWeek.Sunday) endDate = endDate.AddDays(-2);
//						else if (endDate.DayOfWeek == DayOfWeek.Saturday) endDate = endDate.AddDays(-1);
					}
					if (!((startDate.DayOfWeek == DayOfWeek.Saturday && CalendarDAL.IsHoliday(startDate)) && (startDate.AddDays(1)==endDate) && CalendarDAL.IsHoliday(endDate)))
					{
						if (startDate.Date == endDate.Date && !CalendarDAL.IsHoliday(endDate))
						{
							daysString+=startDate.ToString(format)+"; ";
						} 
						else
							if (startDate<endDate) 
								daysString+= startDate.ToString(format)+"-"+endDate.ToString(format)+"; ";
					}
					startDate = tempDate; endDate = tempDate;
				}
			}

			return daysString;
		}

		#region properties

		public int UserID
		{
			get { return _userID; }
		}

		public string UserName
		{
			get { return _userName; }
		}

		public string DaysString
		{
			get { return _daysString; }
			set { _daysString = value; }
		}
		public int Days
		{
			get { return _days; }
			set { _days = value; }
		}

		#endregion

	}

	#endregion

	#region UserInfoPM
	public class UserInfoPM
	{
		private int _userID;
		private int _userRoleID;
		private string _account;
		//private string _firstName;
		//private string _middleName;
		//private string _lastName;
		private string _fullName;
		private string _mail;
		//private string _occupation = String.Empty;
		//private string _userRole = String.Empty;
		//private string _ext = String.Empty;
		//private string _mobile = String.Empty;
		//private string _homePhone = String.Empty;
		//private string _cV = String.Empty;
		//private decimal _salary=0;
		//private decimal _bonus=0;
		//private bool _hasWorkTime = true;
		//private DateTime _workStartedDate = Constants.DateMax;
		//private bool _hasWorkTimeOver = false;
		private int _userType;
		public UserInfoPM()
		{
			_userID = 0;
			//_userRoleID = 0;
			_account = String.Empty;
			//_firstName = String.Empty;
			//_middleName = String.Empty;
			//_lastName = String.Empty;
			_fullName = String.Empty;
			_mail = String.Empty;
		}
//		public decimal Salary
//		{
//			get { return _salary; }
//			set { _salary = value; }
//		}
//		public decimal Bonus
//		{
//			get { return _bonus; }
//			set { _bonus = value; }
//		}
		public int UserID
		{
			get { return _userID; }
			set { _userID = value; }
		}

		public int UserRoleID
		{
			get { return _userRoleID; }
			set { _userRoleID = value; }
		}

		public string Account
		{
			get { return _account; }
			set { _account = value; }
		}

//		public string UserName
//		{
//			get { return (_firstName + " " + _lastName).TrimStart(null); }
//		}

		public string FullName
		{
			get { return _fullName; }
			set { _fullName = value; }
		}


//		public string FirstName
//		{
//			get { return _firstName; }
//			set { _firstName = value; }
//		}
//
//		public string MiddleName
//		{
//			get { return _middleName; }
//			set { _middleName = value; }
//		}
//
//		public string LastName
//		{
//			get { return _lastName; }
//			set { _lastName = value; }
//		}

		public string Mail
		{
			get { return _mail; }
			set { _mail = value; }
		}
		public bool HasPaymentRights
		{
			get { return (_userRoleID==(int)Roles.Director || _userRoleID==(int)Roles.MainAccount || _userRoleID==(int)Roles.Account); }
		}
//		public bool IsLeader
//		{
//			get { return (_userRoleID==(int)Roles.Director); }
//		}
//		public bool IsAdmin
//		{
//			get { return (_userRoleID==(int)Roles.Admin); }
//		}
//		public bool IsAccountant
//		{
//			get { return (_userRoleID==(int)Roles.MainAccount || _userRoleID==(int)Roles.Account); }
//		}
//		public bool IsAccountantLow
//		{
//			get { return (_userRoleID==(int)Roles.Account); }
//		}
//		public bool IsASI
//		{
//			get { return (_userRoleID==(int)Roles.Ingener); }
//		}
//		public bool IsAssistantOnly
//		{
//			get { return (_userRoleID==(int)Roles.Assistant ) ; }
//		}
//		public bool IsAssistant
//		{
//			get { return (_userRoleID==(int)Roles.Assistant||_userRoleID==(int)Roles.Employ|| _userRoleID==(int)Roles.MainAccount|| _userRoleID==(int)Roles.Technical || _userRoleID==(int)Roles.Ingener || _userRoleID==(int)Roles.Secretary || _userRoleID==(int)Roles.Account ||_userRoleID==(int)Roles.Cleaner||_userRoleID==(int)Roles.Layer||_userRoleID==(int)Roles.Driver) ; }
//		}

		public bool IsAuthenticated
		{
			get { return (_userID!=0); }
		}

//		public bool IsEmployee
//		{
//			get { return (_userRoleID==(int)Roles.Employ); }
//		}
//		public bool IsSecretary
//		{
//			get { return (_userRoleID==(int)Roles.Technical); }
//		}
//		public bool IsSecretaryReally
//		{
//			get { return (_userRoleID==(int)Roles.Secretary); }
//		}
//		public bool IsLayer
//		{
//			get { return (_userRoleID==(int)Roles.Layer); }
//		}
//		public bool HasRole
//		{
//			get { return (_userRoleID>0); }
//		}
//		public string Occupation
//		{
//			get { return _occupation; }
//			set { _occupation = value; }
//		}
//	
//		public string UserRole
//		{
//			get { return _userRole; }
//			set { _userRole = value; }
//		}
//	
//		public string Ext
//		{
//			get { return _ext; }
//			set { _ext = value; }
//		}
//	
//		public string Mobile
//		{
//			get { return _mobile; }
//			set { _mobile = value; }
//		}
//	
//		public string HomePhone
//		{
//			get { return _homePhone; }
//			set { _homePhone = value; }
//		}
//	
//		public string CV
//		{
//			get { return _cV; }
//			set { _cV = value; }
//		}
//		public bool HasWorkTime
//		{
//			get { return _hasWorkTime; }
//			set { _hasWorkTime = value; }
//		}
//		public DateTime WorkStartedDate
//		{
//			get { return _workStartedDate; }
//			set { _workStartedDate = value; }
//		}
//		//Add by Ivailo date: 04.12.2007
//		public bool HasWorkTimeOver
//		{
//			get { return _hasWorkTimeOver; }
//			set { _hasWorkTimeOver = value; }
//		}
				public int UserType
				{
					get { return _userType; }
					set { _userType = value; }
				}
				public bool IsUser
				{
					get { return (_userType == (int)PMUserType.User); }
				}
				public bool IsClient
				{
					get { return  (_userType == (int)PMUserType.Client); }
				}
				public bool IsBuilder
				{
					get { return  (_userType == (int)PMUserType.Builder); }
				}
				public bool IsSubcontracter
				{
					get { return  (_userType == (int)PMUserType.Subcontracter); }
				}
	}

	#endregion

	public class HotIssueInfo
	{
		public string SearchText;
		public string BuildingType;	
		public int TypeID;
		public int ProjectID;
		public int ThemeID;
		public int StatusID;
		public string StartDate;
		public string EndDate;
	
	}
}
