using System;
using System.Collections;
using System.Text;


namespace Asa.Timesheet.Data
{
	/// <summary>
	/// Summary description for ProjectCategoryData.
	/// </summary>
	public class ProjectCategoryData
	{
		private int _categoryID = -1;
		private int _projectID = -1;
		private string _categoryName = string.Empty;
//		private bool _isClientVisible;
//		private bool _isSubcontractorVisible;
//		private bool _isBuilderVisible;
		private bool _IsActive;
		private int _clientID = -1;
		private int _subcontracterID = -1;
		private bool _isProjectMain = false;
		private string _projectName = string.Empty;
		/// <summary>
		/// Initialize an new empty Login object.
		/// </summary>
		public ProjectCategoryData()
		{
		}
		
		/// <summary>
		/// Initialize a new Login object with the given parameters.
		/// </summary>

		public ProjectCategoryData(int categoryID, int projectID,string categoryName/*,bool isClientVisible,bool isSubcontractorVisible,bool isBuilderVisible*/,bool IsActive,int clientID,int subcontracterID,bool isProjectMain)
		{
			this._categoryID = categoryID;
			this._projectID = projectID;
			this._categoryName = categoryName;
//			this._isClientVisible = isClientVisible;
//			this._isSubcontractorVisible = isSubcontractorVisible;
//			this._isBuilderVisible = isBuilderVisible;
			this._IsActive = IsActive;
			this._clientID = clientID;
			this._subcontracterID = subcontracterID;
			this._isProjectMain = isProjectMain;
		}	
		
	  
		public override string ToString()
		{
			StringBuilder builder = new StringBuilder();
			builder.Append("_categoryID: " + _categoryID.ToString()).Append("\r\n");
			builder.Append("_projectID: " + _projectID.ToString()).Append("\r\n");
			builder.Append("_categoryName: " + _categoryName.ToString()).Append("\r\n");
//			builder.Append("_isClientVisible: " + _isClientVisible.ToString()).Append("\r\n");
//			builder.Append("_isSubcontractorVisible: " + _isSubcontractorVisible.ToString()).Append("\r\n");
//			builder.Append("_isBuilderVisible: " + _isBuilderVisible.ToString()).Append("\r\n");
			builder.Append("_IsActive: " + _IsActive.ToString()).Append("\r\n");
			builder.Append("_clientID: " + _clientID.ToString()).Append("\r\n");
			builder.Append("_subcontracterID: " + _subcontracterID.ToString()).Append("\r\n");
			builder.Append("_isProjectMain: " + _isProjectMain.ToString()).Append("\r\n");
			builder.Append("\r\n");
			return builder.ToString();
		}	
		public object GetByIndex(int nIndex)
		{
			switch(nIndex)
			{
				case 0:
					return _categoryID;
				case 1:
					return _projectID;
				case 2:
					return _categoryName;
//				case 3:
//					return _isClientVisible;
//				case 4:
//					return _isSubcontractorVisible;
//				case 5:
//					return _isBuilderVisible;
				case 3:
					return _IsActive;
				case 4:
					return _clientID;
				case 5:
					return _subcontracterID;
				case 6:
					return _isProjectMain;
				case 7:
					return _projectName;
			}
			return null;
		}

		#region Properties
	
		public int CategoryID
		{
			get { return _categoryID; }
			set { _categoryID = value; }
		}
	
		public int ProjectID
		{
			get { return _projectID; }
			set { _projectID = value; }
		}
	
		public string CategoryName
		{
			get { return _categoryName; }
			set { _categoryName = value; }
		}

//		public bool IsClientVisible
//		{
//			get { return _isClientVisible; }
//			set { _isClientVisible = value; }
//		}
//
//		public bool IsSubcontractorVisible
//		{
//			get { return _isSubcontractorVisible; }
//			set { _isSubcontractorVisible = value; }
//		}
//
//		public bool IsBuilderVisible
//		{
//			get { return _isBuilderVisible; }
//			set { _isBuilderVisible = value; }
//		}

		public bool IsActive
		{
			get { return _IsActive; }
			set { _IsActive = value; }
		}

		public int ClientID
		{
			get { return _clientID; }
			set { _clientID = value; }
		}

		public int SubcontracterID
		{
			get { return _subcontracterID; }
			set { _subcontracterID = value; }
		}

		public bool IsProjectMain
		{
			get { return _isProjectMain; }
			set { _isProjectMain = value; }
		}

		public string ProjectName
		{
			get { return _projectName; }
			set { _projectName = value; }
		}

		#endregion
	}
}

