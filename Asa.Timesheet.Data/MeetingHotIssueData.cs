using System;
using System.Collections;
using System.Text;

namespace Asa.Timesheet.Data
{
	public class MeetingHotIssueData
	{
		private int _meetingHotIssuesID = -1;
		private int _meetingID = -1;
		private int _hotIssueID = -1;

		/// <summary>
		/// Initialize an new empty Login object.
		/// </summary>
		public MeetingHotIssueData()
		{
		}
		
		/// <summary>
		/// Initialize a new Login object with the given parameters.
		/// </summary>
		public MeetingHotIssueData(int meetingHotIssuesID, int meetingID, int hotIssueID)
		{
			this._meetingHotIssuesID = meetingHotIssuesID;
			this._meetingID = meetingID;
			this._hotIssueID = hotIssueID;
		}	
		
	  
		public override string ToString()
		{
			StringBuilder builder = new StringBuilder();
			builder.Append("_meetingHotIssuesID: " + _meetingHotIssuesID.ToString()).Append("\r\n");
			builder.Append("_meetingID: " + _meetingID.ToString()).Append("\r\n");
			builder.Append("_hotIssueID: " + _hotIssueID.ToString()).Append("\r\n");
			builder.Append("\r\n");
			return builder.ToString();
		}	
		public object GetByIndex(int nIndex)
		{
			switch(nIndex)
			{
				case 0:
					return _meetingHotIssuesID;
				case 1:
					return _meetingID;
				case 2:
					return _hotIssueID;
			}
			return null;
		}
		#region Properties
	
		public int MeetingHotIssuesID
		{
			get { return _meetingHotIssuesID; }
			set { _meetingHotIssuesID = value; }
		}
	
		public int MeetingID
		{
			get { return _meetingID; }
			set { _meetingID = value; }
		}
	
		public int HotIssueID
		{
			get { return _hotIssueID; }
			set { _hotIssueID = value; }
		}
	
		#endregion
	}
}


