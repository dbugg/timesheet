﻿<%@ Page language="c#" Codebehind="Minutes.aspx.cs" AutoEventWireup="false" Inherits="Asa.Timesheet.WebPages.Reports.Minutes" %>
<%@ Register TagPrefix="uc1" TagName="PageHeader" Src="../UserControls/PageHeader.ascx" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
	<head runat="server">
		<title>Minutes</title>
		<meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
		<meta content="C#" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<LINK href="../styles/timesheet.css" type="text/css" rel="stylesheet">
		<script language="javascript">
		function DisableActivity()
		{
			var noActivityProjects = document.getElementById('txtNoActivity').value;

			var ddlActivity = document.getElementById('ddlActivity');
			
			if (noActivityProjects.indexOf("."+event.srcElement.value+".")>=0)
			{
				ddlActivity.value = "0";
				ddlActivity.disabled = true;
			}
			else ddlActivity.disabled = false;
		}
		</script>
	</HEAD>
	<body bottomMargin="0" leftMargin="0" topMargin="0" rightMargin="0">
		<form id="Form1" method="post" runat="server">
			<table height="100%" cellSpacing="0" cellPadding="0" width="100%" border="0">
				<tr height="1">
					<td><uc1:pageheader id="header" runat="server"></uc1:pageheader></td>
				</tr>
				<tr>
					<td>
						<table height="100%" cellSpacing="0" cellPadding="0" width="100%" border="0">
							<tr>
								<td class="menuLeft"><asp:placeholder id="menuHolder" runat="server"></asp:placeholder></td>
								
								<td style="PADDING-LEFT: 5px; PADDING-RIGHT: 5px; PADDING-TOP: 3px" vAlign="top" noWrap>
									<TABLE id="tblInfo" height="30" cellSpacing="0" cellPadding="3" width="880" border="0">
										<TR>
											<td vAlign="middle" noWrap><asp:label id="lblInfo" runat="server" EnableViewState="False" CssClass="InfoLabel"></asp:label><asp:label id="lblError" runat="server" EnableViewState="False" CssClass="ErrorLabel" ForeColor="Red"></asp:label><INPUT id="txtNoActivity" style="DISPLAY: none" name="Text1" runat="server"></TD>
										</TR>
									</TABLE>
									<TABLE id="Table3" cellSpacing="0" cellPadding="0" width="100%" border="0">
										<TR>
											<td style="HEIGHT: 5px" colSpan="2"></TD>
										</TR>
										<TR>
											<td colSpan="2"><IMG height="1" src="../images/dot.gif" width="100%"></TD>
										</TR>
										<TR>
											<td colSpan="2" height="3"></td>
										</TR>
										<TR>
											<td style="WIDTH: 4px" colSpan="2">
												<TABLE id="Table4" cellSpacing="0" cellPadding="3" border="0">
													<TR>
														<td style="WIDTH: 139px" vAlign="top">
															<asp:label id="Label4" runat="server" CssClass="enterDataLabel" Width="100%">Тип:</asp:label></TD>
														<td style="WIDTH: 243px" vAlign="top"><asp:dropdownlist id="ddlProjectStatus" runat="server" CssClass="EnterDataBox" Width="300px" AutoPostBack="True"></asp:dropdownlist></TD>
														<td vAlign="top"></TD>
														<td></TD>
														<td></TD>
														<td style="WIDTH: 8px"></TD>
														<td style="WIDTH: 8px"></TD>
													</TR>
													<TR>
														<td style="WIDTH: 139px" vAlign="top"><asp:label id="Label7" runat="server" CssClass="enterDataLabel" Width="100%">Сграда:</asp:label></TD>
														<td style="WIDTH: 243px" vAlign="top"><asp:dropdownlist id="ddlBuildingTypes" runat="server" CssClass="enterDataBox" Width="300px" AutoPostBack="True"></asp:dropdownlist></TD>
														<td vAlign="top"></TD>
														<td></TD>
														<td></TD>
														<td style="WIDTH: 8px"></TD>
														<td style="WIDTH: 8px"></TD>
													</TR>
													<TR>
														<td style="HEIGHT: 23px"><asp:label id="lblProject" runat="server" CssClass="enterDataLabel" Width="100%">Проект:</asp:label></TD>
														<td style="HEIGHT: 23px"><asp:dropdownlist id="ddlProject" runat="server" CssClass="enterDataBox" Width="300px"></asp:dropdownlist></TD>
														<td style="HEIGHT: 23px">
														</TD>
														<td style="HEIGHT: 23px"></TD>
														<td style="WIDTH: 20px; HEIGHT: 23px"></TD>
														<td style="HEIGHT: 23px"></TD>
														<td style="HEIGHT: 23px"></TD>
													</TR>
													<TR>
														<td style="WIDTH: 83px; HEIGHT: 22px"><asp:label id="lblPotr" runat="server" CssClass="enterDataLabel" Width="100%">Служител:</asp:label></TD>
														<td style="HEIGHT: 22px"><asp:dropdownlist id="ddlUser" runat="server" CssClass="enterDataBox" Width="300px"></asp:dropdownlist></TD>
														<td style="HEIGHT: 22px"><asp:dropdownlist id="ddlUserStatus" runat="server" CssClass="EnterDataBox" Width="300px" AutoPostBack="True">
																<asp:ListItem Value="0">&lt;активни и пасивни&gt;</asp:ListItem>
																<asp:ListItem Value="1" Selected="True">&lt;активни служители&gt;</asp:ListItem>
																<asp:ListItem Value="2">&lt;пасивни служители&gt;</asp:ListItem>
															</asp:dropdownlist></TD>
														<td style="HEIGHT: 22px"></TD>
														<td style="WIDTH: 20px; HEIGHT: 22px"></TD>
														<td style="HEIGHT: 22px"></TD>
														<td style="HEIGHT: 22px"></TD>
													</TR>
													<TR>
														<td><asp:label id="lblActivity" runat="server" CssClass="enterDataLabel" Width="100%">Дейност:</asp:label></TD>
														<td><asp:dropdownlist id="ddlActivity" runat="server" CssClass="enterDataBox" Width="300px"></asp:dropdownlist></TD>
														<td><asp:button id="btnGenerateReport" runat="server" CssClass="ActionButton" Visible="False" Text="Генерирай"></asp:button></TD>
														<td></TD>
														<td style="WIDTH: 20px"><asp:imagebutton id="ibGenerate" runat="server" ImageUrl="../images/ie.gif"></asp:imagebutton></TD>
														<td><asp:imagebutton id="ibPdfExport" runat="server" ImageUrl="../images/pdf1.gif"></asp:imagebutton></TD>
														<td><asp:imagebutton id="ibXlsExport" runat="server" ImageUrl="../images/xls.gif"></asp:imagebutton></TD>
													</TR>
												</TABLE>
											</TD>
										</TR>
										<TR>
											<td height="3"></TD>
										</TR>
										<TR>
											<td colSpan="2"><IMG height="1" src="../images/dot.gif" width="100%"></TD>
										</TR>
									</TABLE>
									<br>
									<br>
									<br>
									<TABLE id="tblHeader" cellSpacing="0" cellPadding="2" border="0" runat="server">
										<TR>
											<td><asp:label id="Label1" runat="server" Font-Bold="True" Font-Size="9pt">ОБЕКТ:</asp:label></TD>
											<td><asp:label id="lblObekt" runat="server" Font-Size="9pt"></asp:label></TD>
										</TR>
										<TR>
											<td><asp:label id="Label3" runat="server" Font-Bold="True" Font-Size="9pt">ИНВЕСТИТОР:</asp:label></TD>
											<td><asp:label id="lblClient" runat="server" Font-Size="9pt"></asp:label></TD>
										</TR>
										<TR>
											<td></TD>
											<td><asp:label id="lblClientAddress" runat="server" Font-Size="9pt"></asp:label></TD>
										</TR>
										<TR>
											<td noWrap><asp:label id="Label6" runat="server" Font-Bold="True" Font-Size="9pt">ГЛАВЕН ПРОЕКТАНТ:</asp:label></TD>
											<td><asp:label id="lblProektant" runat="server" Font-Size="9pt"></asp:label></TD>
										</TR>
										<TR>
											<td noWrap><asp:label id="Label2" runat="server" Font-Bold="True" Font-Size="9pt">СЛУЖИТЕЛ:</asp:label></TD>
											<td><asp:label id="lblReportUser" runat="server" Font-Size="9pt"></asp:label></TD>
										</TR>
										<TR>
											<td noWrap><asp:label id="Label5" runat="server" Font-Bold="True" Font-Size="9pt">ДЕЙНОСТ:</asp:label></TD>
											<td><asp:label id="lblReportActivity" runat="server" Font-Size="9pt"></asp:label></TD>
										</TR>
									</TABLE>
									<br>
									<br>
									<br>
									<TABLE id="Table1" cellSpacing="0" cellPadding="0" width="100%" border="0">
										<TR>
											<td align="center"><asp:label id="lblReportTitle" runat="server" Visible="False" Font-Bold="True" Font-Size="11pt">СПРАВКА ПРОТОКОЛ</asp:label></TD>
										</TR>
									</TABLE>
									<br>
									<asp:label id="lbTotal" runat="server" Font-Size="9pt" Font-Bold="True"></asp:label>
									<br>
									<br>
									<asp:panel id="pnlGrid" runat="server" Width="100%">
										<asp:DataGrid id="grdReport" runat="server" CssClass="ReportGrid" EnableViewState="False" ForeColor="DimGray"
											Width="100%" CellPadding="4" AutoGenerateColumns="False" GridLines="Horizontal">
											<HeaderStyle Font-Bold="True" CssClass="reportGridHeader"></HeaderStyle>
											<Columns>
												<asp:BoundColumn DataField="UserName" HeaderText="Служител">
													<HeaderStyle Wrap="False" Width="250px"></HeaderStyle>
													<ItemStyle Wrap="False" VerticalAlign="Top"></ItemStyle>
												</asp:BoundColumn>
												<asp:BoundColumn DataField="WorkDate" HeaderText="Дата" DataFormatString="{0:dd.MM.yyyy}">
													<HeaderStyle Wrap="False" Width="50px"></HeaderStyle>
													<ItemStyle Wrap="False" VerticalAlign="Top"></ItemStyle>
												</asp:BoundColumn>
												<asp:BoundColumn DataField="StartHour" HeaderText="Нач.час">
													<HeaderStyle Wrap="False" Width="50px"></HeaderStyle>
													<ItemStyle Wrap="False" HorizontalAlign="Right" VerticalAlign="Top"></ItemStyle>
												</asp:BoundColumn>
												<asp:BoundColumn DataField="EndHour" HeaderText="Кр.час">
													<HeaderStyle Wrap="False" Width="50px"></HeaderStyle>
													<ItemStyle Wrap="False" HorizontalAlign="Right" VerticalAlign="Top"></ItemStyle>
												</asp:BoundColumn>
												<asp:BoundColumn DataField="Minutes" HeaderText="Протокол">
													<HeaderStyle Wrap="False" Width="200px"></HeaderStyle>
													<ItemStyle VerticalAlign="Top"></ItemStyle>
												</asp:BoundColumn>
												<asp:BoundColumn DataField="AdminMinutes" HeaderText="Админ.бележки">
													<HeaderStyle Wrap="False" Width="200px"></HeaderStyle>
													<ItemStyle VerticalAlign="Top"></ItemStyle>
												</asp:BoundColumn>
											</Columns>
										</asp:DataGrid>
									</asp:panel><asp:label id="lblNoDataFound" runat="server" EnableViewState="False" CssClass="InfoLabel"
										Visible="False"></asp:label><br>
									<br>
									<asp:label id="lbPayments" runat="server" Visible="False" Font-Size="9pt" Font-Bold="True">ПЛАЩАНИЯ</asp:label><BR>
									<asp:datagrid id="grd" runat="server" CssClass="ReportGrid" EnableViewState="False" ForeColor="DimGray"
										Width="100%" CellPadding="4" AutoGenerateColumns="False" ShowHeader="True">
										<HeaderStyle CssClass="reportGridHeader"></HeaderStyle>
										<Columns>
											<asp:BoundColumn Visible="False" DataField="PaymentID"></asp:BoundColumn>
											<asp:BoundColumn DataField="projectname" HeaderText="Проект"></asp:BoundColumn>
											<asp:BoundColumn DataField="Amount" HeaderText="Обща сума (EUR)" DataFormatString="{0:n2}">
												<HeaderStyle HorizontalAlign="Right"></HeaderStyle>
												<ItemStyle HorizontalAlign="Right" Width="80px"></ItemStyle>
											</asp:BoundColumn>
											<asp:BoundColumn DataField="AmountBGN" HeaderText="Обща сума (BGN)" DataFormatString="{0:n2}">
												<HeaderStyle HorizontalAlign="Right"></HeaderStyle>
												<ItemStyle HorizontalAlign="Right" Width="80px"></ItemStyle>
											</asp:BoundColumn>
											<asp:TemplateColumn HeaderText="Дата на плащане">
												<ItemStyle HorizontalAlign="Center" Width="60px"></ItemStyle>
												<ItemTemplate>
													<asp:Label runat="server" Text='<%# Asa.Timesheet.Data.Util.TimeHelper.FormatDateO(DataBinder.Eval(Container, "DataItem.PaymentDate")) %>' ID="Label1" >
													</asp:Label>
												</ItemTemplate>
											</asp:TemplateColumn>
											<asp:TemplateColumn HeaderText="Платено">
												<HeaderStyle HorizontalAlign="Center"></HeaderStyle>
												<ItemStyle HorizontalAlign="Center" Width="40px"></ItemStyle>
												<ItemTemplate>
													<asp:Image id=imgDone ImageUrl='<%# GetImage(DataBinder.Eval(Container, "DataItem.Done")) %>' Runat=server>
													</asp:Image>
												</ItemTemplate>
											</asp:TemplateColumn>
										</Columns>
									</asp:datagrid>
									<asp:label id="lblNoDataFound1" runat="server" CssClass="InfoLabel" EnableViewState="False"
										Visible="False"></asp:label><BR>
									<br>
									<asp:label id="lblCreatedByLbl" runat="server" EnableViewState="False" Visible="False" Font-Bold="True"
										Font-Size="9pt">ИЗГОТВИЛ СПРАВКАТА:</asp:label><asp:label id="lblCreatedBy" runat="server" EnableViewState="False" Visible="False" Font-Size="9pt"></asp:label><BR>
									<br>
								</td>
							</tr>
						</table>
					</td>
				</tr>
			</table>
		</form>
	</body>
</HTML>
