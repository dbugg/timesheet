using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using Asa.Timesheet.Data;

namespace Asa.Timesheet.WebPages.UserControls
{
	/// <summary>
	/// Summary description for DocumentsForMeeting.
	/// </summary>
	public class DocumentsForMeeting : System.Web.UI.Page
	{
		#region WebControls
		protected System.Web.UI.WebControls.DataList dlDocs;
		protected System.Web.UI.WebControls.Label Label58;
		protected System.Web.UI.HtmlControls.HtmlInputFile FileCtrl;
		protected System.Web.UI.WebControls.Button btnScanDocAdd;
		protected int _meetingID=0;
		#endregion

		private void Page_Load(object sender, System.EventArgs e)
		{
			BindGrid();
		}
		private void BindGrid()
		{
			dlDocs.DataSource = MeetingDocumentDAL.LoadCollection("MeetingDocumentsSelByMeetingProc",SQLParms.CreateMeetingDocumentsSelByMeetingProc(UIHelpers.GetIDParam()));
			dlDocs.DataKeyField="MeetingDocument";
			dlDocs.DataBind();
		}
		protected string GetURL(int ID)
		{
			return string.Concat(System.Configuration.ConfigurationSettings.AppSettings["ScanPath"],ID,System.Configuration.ConfigurationSettings.AppSettings["Extension"]);
		}
		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
			this.dlDocs.ItemCommand += new System.Web.UI.WebControls.DataListCommandEventHandler(this.dlDocs_ItemCommand);
			this.btnScanDocAdd.Click += new System.EventHandler(this.btnScanDocAdd_Click);
			this.Load += new System.EventHandler(this.Page_Load);

		}
		#endregion
		
		public int MeetingID
		{
			get{return _meetingID;}
			set {_meetingID=value;}
		}
		public void SaveMeetingDocuments()
		{
			int ID = UIHelpers.GetIDParam();
			if(FileCtrl.PostedFile.FileName!=null
				&&FileCtrl.PostedFile.FileName!="")
			{
				string name = FileCtrl.PostedFile.FileName;
				int n = name.LastIndexOf(".");
				if(n!=-1)
				{
					string ext = name.Substring(n);
					if(ext==System.Configuration.ConfigurationSettings.AppSettings["Extension"])
					{
						MeetingDocumentData mdd = new MeetingDocumentData(-1,ID,false);
						MeetingDocumentDAL.Save(mdd);
						string path = string.Concat(System.Configuration.ConfigurationSettings.AppSettings["ScanPath"],mdd.MeetingDocument,ext);
						string mapped = Request.MapPath(path);
						FileCtrl.PostedFile.SaveAs(mapped);
					}
				}
			}
		}

		private void dlDocs_ItemCommand(object source, System.Web.UI.WebControls.DataListCommandEventArgs e)
		{
			ImageButton ib = (ImageButton)e.CommandSource;
			string s = ib.ID;
			switch (ib.ID)
			{
				case "btnDel": int ID = (int)dlDocs.DataKeys[e.Item.ItemIndex];
					MeetingDocumentData mdd = MeetingDocumentDAL.Load(ID);
					mdd.IsDeleted = true;
					MeetingDocumentDAL.Save(mdd);
					BindGrid();
					break;
			}
		}

		private void btnScanDocAdd_Click(object sender, System.EventArgs e)
		{
			SaveMeetingDocuments();
			BindGrid();
		}
	


	}
}
