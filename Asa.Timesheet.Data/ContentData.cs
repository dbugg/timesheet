using System;
using System.Collections;
using System.Text;

namespace Asa.Timesheet.Data
{
	public class ContentData
	{
		private int _contentID = -1;
		private int _projectID = -1;
		private int _contentTypeID = -1;
		private decimal _kota;
		private decimal _area = -1;
		private string _contentText = String.Empty;
		private string _contentType="";
		private string _scale = String.Empty;
		private int _buildingNumber = -1;
		private string _signature = String.Empty;
		private string _contentCode = String.Empty;
		private string _contentRevision = String.Empty;
		private int _contentOrder = -1;
		private bool _isChecked = true;
		private int _contentFinishedPhase1 = 0;
		private int _contentFinishedPhase2 = 0;
		private int _contentFinishedPhase3 = 0;
		private int _contentFinishedPhase4 = 0;
		private string _otherInfo="";
		private string _contentLevel="";
		/// <summary>
		/// Initialize an new empty Login object.
		/// </summary>
		public ContentData()
		{
		}
		
		/// <summary>
		/// Initialize a new Login object with the given parameters.
		/// </summary>
		public ContentData(int contentID, int projectID, int contentTypeID, decimal kota, decimal area,
			string contentText, string scale, int number, string signature, string contentCode,
			string contentRevision, int contentOrder, int contentFinishedPhase1, int contentFinishedPhase2, 
			int contentFinishedPhase3, int contentFinishedPhase4)
		{
			this._contentID = contentID;
			this._projectID = projectID;
			this._contentTypeID = contentTypeID;
			this._kota = kota;
			this._area = area;
			this._contentText = contentText;
		    this._scale = scale;
			_buildingNumber=number;
			_signature=signature;
			_contentCode = contentCode;
			_contentRevision = contentRevision;
			_contentOrder=contentOrder;
			_contentFinishedPhase1 = contentFinishedPhase1;
			_contentFinishedPhase2 = contentFinishedPhase2;
			_contentFinishedPhase3 = contentFinishedPhase3;
			_contentFinishedPhase4 = contentFinishedPhase4;
		}	
		
	  
		public override string ToString()
		{
			StringBuilder builder = new StringBuilder();
			builder.Append("_contentID: " + _contentID.ToString()).Append("\r\n");
			builder.Append("_projectID: " + _projectID.ToString()).Append("\r\n");
			builder.Append("_contentTypeID: " + _contentTypeID.ToString()).Append("\r\n");
			builder.Append("_kota: " + _kota.ToString()).Append("\r\n");
			builder.Append("_area: " + _area.ToString()).Append("\r\n");
			builder.Append("_contentText: " + _contentText.ToString()).Append("\r\n");
			builder.Append("_scale: " + _scale.ToString()).Append("\r\n");
			builder.Append("_contentOrder: " + _contentOrder.ToString()).Append("\r\n");
			builder.Append("\r\n");
			return builder.ToString();
		}	
		public object GetByIndex(int nIndex)
		{
			switch(nIndex)
			{
				case 0:
					return _contentID;
				case 1:
					return _projectID;
				case 2:
					return _contentTypeID;
				case 3:
					return _kota;
				case 4:
					return _area;
				case 5:
					return _contentText;
				case 6:
					return _scale;
				case 7:
					return _contentCode;
				case 8:
				    return _contentRevision;
				case 9:
					return _contentOrder;
				case 10: 
					return _contentFinishedPhase1;
				case 11: 
					return _contentFinishedPhase2;
				case 12: 
					return _contentFinishedPhase3;
				case 13: 
					return _contentFinishedPhase4;
			}
			return null;
		}
		#region Properties
	
		public int ContentID
		{
			get { return _contentID; }
			set { _contentID = value; }
		}
		public string Signature
		{
			get { return _signature; }
			set { _signature = value; }
		}
		public string ContentLevel
		{
			get { return _contentLevel; }
			set { _contentLevel = value; }
		}
		
		public int BuildingNumber
		{
			get { return _buildingNumber; }
			set { _buildingNumber = value; }
		}
		public string OtherInfo
		{
			get { return _otherInfo; }
			set { _otherInfo = value; }
		}
	
		public int ProjectID
		{
			get { return _projectID; }
			set { _projectID = value; }
		}
	
		public int ContentTypeID
		{
			get { return _contentTypeID; }
			set { _contentTypeID = value; }
		}
		public string ContentType
		{
			get { return _contentType; }
			set { _contentType = value; }
		}
	
		public decimal Kota
		{
			get { return _kota; }
			set { _kota = value; }
		}
	
		public decimal Area
		{
			get { return _area; }
			set { _area = value; }
		}
	
		public string ContentText
		{
			get { return _contentText; }
			set { _contentText = value; }
		}

    public string Scale
    {
      get { return _scale; }
      set { _scale = value; }
    }
		public string ContentCode
		{
			get { return _contentCode; }
			set { _contentCode = value; }
		}
		public string ContentRevision
		{
			get { return _contentRevision; }
			set { _contentRevision = value; }
		}
		public int ContentOrder
		{
			get { return _contentOrder; }
			set { _contentOrder = value; }
		}
		public bool IsChecked
		{
			get { return _isChecked; }
			set { _isChecked = value; }
		}
		public int ContentFinishedPhase1
		{
			get { return _contentFinishedPhase1; }
			set { _contentFinishedPhase1 = value; }
		}
		public int ContentFinishedPhase2
		{
			get { return _contentFinishedPhase2; }
			set { _contentFinishedPhase2 = value; }
		}
		public int ContentFinishedPhase3
		{
			get { return _contentFinishedPhase3; }
			set { _contentFinishedPhase3 = value; }
		}
		public int ContentFinishedPhase4
		{
			get { return _contentFinishedPhase4; }
			set { _contentFinishedPhase4 = value; }
		}
		#endregion
	}
}


