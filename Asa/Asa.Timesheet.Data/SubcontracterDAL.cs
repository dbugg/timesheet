using System;
using System.Data;
using System.Data.SqlClient; 
using System.Collections;
using Microsoft.ApplicationBlocks.Data;

namespace Asa.Timesheet.Data
{
	public class SubcontracterDAL
	{
		/// <summary>
		/// Loads a SubcontracterData object from the database using the given SubcontracterID
		/// </summary>
		/// <param name="subcontracterID">The primary key value</param>
		/// <returns>A SubcontracterData object</returns>
		public static SubcontracterData Load(int subcontracterID)
		{
			SqlParameter[] parameterValues = new SqlParameter[] { new SqlParameter("@SubcontracterID", subcontracterID) };
			using (SqlDataReader reader = SqlHelper.ExecuteReader(CommonDAL.ConnectionString, CommandType.StoredProcedure, "SubcontractersSelProc", parameterValues))
			{
				if (reader.Read())
				{
					SubcontracterData result = new SubcontracterData();
					LoadFromReader(reader,result);
					return result;
				}
				else
					return null;
			}
		}

		/// <summary>
		/// Loads a SubcontracterData object from the database using the given where clause
		/// </summary>
		/// <param name="whereClause">The filter expression for the query</param>
		/// <returns>A SubcontracterData object</returns>
		public static SubcontracterData Load(string whereClause)
		{
			SqlParameter[] parameterValues = new SqlParameter[] { new SqlParameter("@WhereClause", whereClause) };
			using (SqlDataReader reader = SqlHelper.ExecuteReader(CommonDAL.ConnectionString, CommandType.StoredProcedure, "SubcontractersListProc", parameterValues))
			{
				if (reader.Read())
				{
					SubcontracterData result = new SubcontracterData();
					LoadFromReader(reader,result);
					return result;
				}
				else
					return null;
			}
		}
		public static int Save( SubcontracterData source)
		{
			if (source.SubcontracterID == -1)
				return Insert(source);
			else
				return Update(source);	
		}

		public static void Delete( SubcontracterData source)
		{
			Delete(source.SubcontracterID);
		}
		/// <summary>
		/// Loads a collection of SubcontracterData objects from the database.
		/// </summary>
		/// <returns>A collection containing all of the SubcontracterData objects in the database.</returns>
		public static SubcontractersVector LoadCollection()
		{
			SqlParameter[] parms = new SqlParameter[] {};
			return LoadCollection("SubcontractersListProc", parms);
		}
	  
		/// <summary>
		/// Saves a collection of SubcontracterData objects to the database.
		/// </summary>
		/// <returns>A collection containing all of the SubcontracterData objects in the database.</returns>
		public static  void  SaveCollection(SubcontractersVector col,string spName, SqlParameter[] parms)
		{
			SubcontractersVector colOld = LoadCollection(spName,parms);
			UpdateCollection(colOld,col);
		}
		public static  void  SaveCollection(SubcontractersVector col)
		{
			SubcontractersVector colOld = LoadCollection();
			UpdateCollection(colOld,col);
		}
		public static  void  SaveCollection(SubcontractersVector col, string whereClause )
		{
			SubcontractersVector colOld = LoadCollection(whereClause);
			UpdateCollection(colOld,col);
		}
		/// <summary>
		/// Loads a collection of SubcontracterData objects from the database.
		/// </summary>
		/// <returns>A collection containing all of the SubcontracterData objects in the database.</returns>
		public static SubcontractersVector LoadCollection(string whereClause)
		{
			SqlParameter[] parms = new SqlParameter[] { new SqlParameter("@WhereClause", whereClause) };
			return LoadCollection("SubcontractersListProc", parms);
		}

		/// <summary>
		/// Loads a collection of SubcontracterData objects from the database.
		/// </summary>
		/// <returns>A collection containing all of the SubcontracterData objects in the database.</returns>
		public static SubcontractersVector LoadCollection(string spName, SqlParameter[] parms)
		{
			SubcontractersVector result = new SubcontractersVector();
			using (SqlDataReader reader = SqlHelper.ExecuteReader(CommonDAL.ConnectionString, CommandType.StoredProcedure, spName, parms))
			{
				while (reader.Read())
				{
					SubcontracterData tmp = new SubcontracterData();
					LoadFromReader(reader, tmp);
					result.Add(tmp);
				}
			}
			return result;
		}
		/// <summary>
		/// Deletes a SubcontracterData object from the database.
		/// </summary>
		/// <param name="subcontracterID">The primary key value</param>
		public static void Delete(int subcontracterID)
		{
			SqlParameter[] parameterValues = new SqlParameter[] { new SqlParameter("@SubcontracterID", subcontracterID) };
			SqlHelper.ExecuteNonQuery(CommonDAL.ConnectionString, CommandType.StoredProcedure, "SubcontractersInactiveProc", parameterValues);
		}

		public static void LoadFromReader(SqlDataReader reader, SubcontracterData target)
		{
			if (reader != null && !reader.IsClosed)
			{
				//target.Key = reader.GetInt32(0);
				if (!reader.IsDBNull(0)) target.SubcontracterID = reader.GetInt32(0);
				if (!reader.IsDBNull(1)) target.SubcontracterName = reader.GetString(1);
				if (!reader.IsDBNull(2)) target.Address = reader.GetString(2);
				if (!reader.IsDBNull(3)) target.Phone = reader.GetString(3);
				if (!reader.IsDBNull(4)) target.SubcontracterTypeID = reader.GetInt32(4);
				if (!reader.IsDBNull(5)) target.Name1 = reader.GetString(5);
				if (!reader.IsDBNull(6)) target.Name2 = reader.GetString(6);
				if (!reader.IsDBNull(7)) target.DefaultPaymentSchemeID = reader.GetInt32(7);
				if (!reader.IsDBNull(8)) target.DefaultRate = reader.GetDecimal(8);
				if (!reader.IsDBNull(9)) target.IsActive = reader.GetBoolean(9);
                if (!reader.IsDBNull(10)) target.Email = reader.GetString(10);
				if (!reader.IsDBNull(11)) target.Fax = reader.GetString(11);
				if (!reader.IsDBNull(12)) target.Manager = reader.GetString(12);
				if (!reader.IsDBNull(13)) target.Delo = reader.GetString(13);
				if (!reader.IsDBNull(14)) target.Bulstat = reader.GetString(14);
				if (!reader.IsDBNull(15)) target.NDR = reader.GetString(15);
				if (!reader.IsDBNull(16)) target.Webpage = reader.GetString(16);
				if (!reader.IsDBNull(17)) target.Phone1 = reader.GetString(17);
				if (!reader.IsDBNull(18)) target.Email1 = reader.GetString(18);
				if (!reader.IsDBNull(19)) target.Phone2 = reader.GetString(19);
				if (!reader.IsDBNull(20)) target.Email2 = reader.GetString(20);
				if (!reader.IsDBNull(22)) target.SubcontracterType = reader.GetString(22);
				if (!reader.IsDBNull(21)) target.Hide = reader.GetBoolean(21);
			}
		}
		
	  
		public static int Insert( SubcontracterData source)
		{
			SqlParameter[] parameterValues = GetInsertParameterValues(source);
			SqlHelper.ExecuteNonQuery(CommonDAL.ConnectionString, CommandType.StoredProcedure, "SubcontractersInsProc", parameterValues);
			source.SubcontracterID = (int) parameterValues[0].Value;
			return source.SubcontracterID;
		}

		public static int Update( SubcontracterData source)
		{
			SqlParameter[] parameterValues = GetUpdateParameterValues(source);
			SqlHelper.ExecuteNonQuery(CommonDAL.ConnectionString, CommandType.StoredProcedure,
				"SubcontractersUpdProc", parameterValues);
			return source.SubcontracterID;
		}
		private static void UpdateCollection(SubcontractersVector colOld,SubcontractersVector col)
		{
			// Delete all old items
			foreach( SubcontracterData itemOld in colOld)
			{
				if(!ExistsKey(itemOld.SubcontracterID, col))
					Delete(itemOld.SubcontracterID);
			}
			foreach( SubcontracterData item in col)
			{
				if(item.SubcontracterID <= -1)
					Insert(item);
				else
					Update(item);
			}
		}
		private static bool ExistsKey(int Key, SubcontractersVector col)
		{
			foreach( SubcontracterData item in col)
				if(item.SubcontracterID==Key)
					return true;
			return false;
		}

		private static SqlParameter GetSqlParameter(string name, ParameterDirection direction, object value)
		{
			SqlParameter p = new SqlParameter(name, value);
			p.Direction = direction;
			return p;
		}

		private static SqlParameter[] GetInsertParameterValues( SubcontracterData source)
		{
			SqlParameter[] prams = new SqlParameter[22];
			prams[0] = GetSqlParameter("@SubcontracterID", ParameterDirection.Output, source.SubcontracterID);
			prams[1] = GetSqlParameter("@SubcontracterName", ParameterDirection.Input, source.SubcontracterName);
			prams[2] = GetSqlParameter("@Address", ParameterDirection.Input, source.Address);
			prams[3] = GetSqlParameter("@Phone", ParameterDirection.Input, source.Phone);
			prams[4] = GetSqlParameter("@SubcontracterTypeID", ParameterDirection.Input, source.SubcontracterTypeID);
			prams[5] = GetSqlParameter("@Name1", ParameterDirection.Input, source.Name1);
			prams[6] = GetSqlParameter("@Name2", ParameterDirection.Input, source.Name2);
			if(source.DefaultPaymentSchemeID<=0)
				prams[7] =GetSqlParameter("@DefaultPaymentSchemeID", ParameterDirection.Input, System.DBNull.Value);
			else
				prams[7] = GetSqlParameter("@DefaultPaymentSchemeID", ParameterDirection.Input, source.DefaultPaymentSchemeID);
			prams[8] = GetSqlParameter("@DefaultRate", ParameterDirection.Input, source.DefaultRate);
			prams[9] = GetSqlParameter("@IsActive", ParameterDirection.Input, source.IsActive);
			prams[10] = GetSqlParameter("@Email", ParameterDirection.Input, source.Email);
			prams[11] = GetSqlParameter("@Fax", ParameterDirection.Input, source.Fax);
			prams[12] = GetSqlParameter("@Manager", ParameterDirection.Input, source.Manager);
			prams[13] = GetSqlParameter("@Delo", ParameterDirection.Input, source.Delo);
			prams[14] = GetSqlParameter("@Bulstat", ParameterDirection.Input, source.Bulstat);
			prams[15] = GetSqlParameter("@NDR", ParameterDirection.Input, source.NDR);
			prams[16] = GetSqlParameter("@Webpage", ParameterDirection.Input, source.Webpage);
			prams[17] = GetSqlParameter("@Phone1", ParameterDirection.Input, source.Phone1);
			prams[18] = GetSqlParameter("@Email1", ParameterDirection.Input, source.Email1);
			prams[19] = GetSqlParameter("@Phone2", ParameterDirection.Input, source.Phone2);
			prams[20] = GetSqlParameter("@Email2", ParameterDirection.Input, source.Email2);
			prams[21] = GetSqlParameter("@Hide", ParameterDirection.Input, source.Hide);
		
			return prams;
		}

		private static SqlParameter[] GetUpdateParameterValues( SubcontracterData source)
		{
			SqlParameter[] prm = GetInsertParameterValues(source);
			prm[0].Direction=  ParameterDirection.Input;
			return prm;
//			return new SqlParameter[] {
//										  new SqlParameter("@SubcontracterID", source.SubcontracterID),
//										  new SqlParameter("@SubcontracterName", source.SubcontracterName),
//										  new SqlParameter("@Address", source.Address),
//										  new SqlParameter("@Phone", source.Phone),
//										  new SqlParameter("@SubcontracterTypeID", source.SubcontracterTypeID),
//										  new SqlParameter("@Name1", source.Name1),
//										  new SqlParameter("@Name2", source.Name2),
//										  new SqlParameter("@DefaultPaymentSchemeID", source.DefaultPaymentSchemeID),
//										  new SqlParameter("@DefaultRate", source.DefaultRate),
//										  new SqlParameter("@IsActive", source.IsActive)
//									  };
		}
	}
}   

