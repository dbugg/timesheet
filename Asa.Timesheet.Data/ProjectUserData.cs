using System;
using System.Collections;
using System.Text;

namespace Asa.Timesheet.Data
{
	public class ProjectUserData
	{
		private int _projectUserID = -1;
		private int _projectID = -1;
		private int _userID = -1;

		/// <summary>
		/// Initialize an new empty Login object.
		/// </summary>
		public ProjectUserData()
		{
		}
		
		/// <summary>
		/// Initialize a new Login object with the given parameters.
		/// </summary>
		public ProjectUserData(int projectUserID, int projectID, int userID)
		{
			this._projectUserID = projectUserID;
			this._projectID = projectID;
			this._userID = userID;
		}	
		
	  
		public override string ToString()
		{
			StringBuilder builder = new StringBuilder();
			builder.Append("_projectUserID: " + _projectUserID.ToString()).Append("\r\n");
			builder.Append("_projectID: " + _projectID.ToString()).Append("\r\n");
			builder.Append("_userID: " + _userID.ToString()).Append("\r\n");
			builder.Append("\r\n");
			return builder.ToString();
		}	
		public object GetByIndex(int nIndex)
		{
			switch(nIndex)
			{
				case 0:
					return _projectUserID;
				case 1:
					return _projectID;
				case 2:
					return _userID;
			}
			return null;
		}
		#region Properties
	
		public int ProjectUserID
		{
			get { return _projectUserID; }
			set { _projectUserID = value; }
		}
	
		public int ProjectID
		{
			get { return _projectID; }
			set { _projectID = value; }
		}
	
		public int UserID
		{
			get { return _userID; }
			set { _userID = value; }
		}
      
		#endregion
	}

    public class ProjectUserData2 : ProjectUserData
    {
        string _name;

        public string Name
        {
            get { return _name; }
            set { _name = value; }
        }

        string _email;

        public string Email
        {
            get { return _email; }
            set { _email = value; }
        }
    }
}


