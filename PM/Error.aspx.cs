using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using log4net;
using log4net.Config;
using Asa.Timesheet.Data;

namespace PM
{
	/// <summary>
	/// Summary description for Error.
	/// </summary>
	public class Error : System.Web.UI.Page
	{
		protected System.Web.UI.WebControls.Label lblUser;
		protected System.Web.UI.WebControls.Label lblInfo;
		protected PM.UserControls.PageHeader header;

        private void Page_Load(object sender, System.EventArgs e)
        {
            if (this.Session["ErrorRedirectMessage"] != null)
            {
                //header.UserName = this.LoggedUser.UserName;

                lblInfo.Text = this.Session["ErrorRedirectMessage"].ToString();
                Session.Remove("ErrorRedirectMessage");
                string lang = (Session["lang"] == null) ? Languages.BG.ToString() : (string)Session["lang"];
                lblUser.Text = UIHelpers.GetText(lang, "Name_ErrorLabelName");
                header.PageTitle = UIHelpers.GetText(lang, "pmPageTitle_Error");

            }
        }

		#region Web Form Designer generated code
        override protected void OnInit(EventArgs e)
        {
            //
            // CODEGEN: This call is required by the ASP.NET Web Form Designer.
            //
            InitializeComponent();
            base.OnInit(e);
        }
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
        private void InitializeComponent()
        {
            this.Load += new System.EventHandler(this.Page_Load);

        }
		#endregion
	}
}
