﻿<%@ Page language="c#" Codebehind="Error.aspx.cs" AutoEventWireup="false" Inherits="Asa.Timesheet.WebPages.Error" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
	<head runat="server">
		<title>Timesheet Error</title>
		<meta name="GENERATOR" Content="Microsoft Visual Studio .NET 7.1">
		<meta name="CODE_LANGUAGE" Content="C#">
		<meta name="vs_defaultClientScript" content="JavaScript">
		<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5">
		<LINK href="styles/timesheet.css" type="text/css" rel="stylesheet">
        <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i,800,800i&amp;subset=cyrillic" rel="stylesheet"> 
        <link href="https://use.fontawesome.com/releases/v5.0.6/css/all.css" rel="stylesheet">
		<Link href="<%=Request.ApplicationPath%>/styles/timesheet.css" type="text/css" rel="stylesheet">
	</HEAD>
	<body bottomMargin="0" leftMargin="0" topMargin="0" rightMargin="0">
		<form id="Form1" method="post" runat="server">
			<table height="100%" cellSpacing="0" cellPadding="0" width="100%" border="0">
				<tr height="80">
					<td >
						<TABLE id="Table2" height="100%" width="100%">
							<TR>
								<td > <IMG id="IMG1"  alt="" src="~/images/logo.png" width="320" runat="server"></TD>
								<td noWrap>
									<asp:label id="lblUser" runat="server" CssClass="ErrorLabel">Грешка! :(</asp:label>
									<br>
									<asp:label id="lblInfo" runat="server"></asp:label>
								</TD>
							</TR>
						</TABLE>
					</td>
				</tr>
			</table>
		</form>
	</body>
</HTML>
