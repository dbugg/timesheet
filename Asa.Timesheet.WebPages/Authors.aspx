<%@ Page language="c#" Codebehind="Authors.aspx.cs" AutoEventWireup="false" Inherits="Asa.Timesheet.WebPages.Authors" %>
<%@ Register TagPrefix="uc1" TagName="PageHeader" Src="UserControls/PageHeader.ascx" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
	<HEAD>
		<title></title>
		<meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
		<meta content="C#" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<LINK href="styles/timesheet.css" type="text/css" rel="stylesheet">
		<script language="javascript" src="PopupCalendar.js"></script>
	</HEAD>
	<body bottomMargin="0" leftMargin="0" topMargin="0" rightMargin="0">
		<form id="Form1" method="post" runat="server">
			<table height="100%" cellSpacing="0" cellPadding="0" width="100%" border="0">
				<tr height="1">
					<td>
						<uc1:PageHeader id="header" runat="server"></uc1:PageHeader>
					</td>
				</tr>
				<TR height="1">
					<td noWrap background="images/line.gif" height="1"></td>
				</TR>
				<tr>
					<td>
						<table height="100%" cellSpacing="0" cellPadding="0" width="100%" border="0">
							<tr>
								<td style="BACKGROUND-IMAGE: url(images/left110.gif)" vAlign="top" noWrap width="110">
									<asp:PlaceHolder id="menuHolder" runat="server"></asp:PlaceHolder>
								</td>
								<td noWrap width="1" bgColor="lightgrey"></td>
								<td style="PADDING-RIGHT: 5px; PADDING-LEFT: 5px; PADDING-TOP: 3px" vAlign="top" noWrap><br>
									<asp:label id="lblError" runat="server" ForeColor="Red" EnableViewState="False" CssClass="ErrorLabel"></asp:label><asp:label id="lblInfo" runat="server" EnableViewState="False" CssClass="InfoLabel"></asp:label><br>
									<br>
									<asp:panel id="grid" style="BORDER-RIGHT: #d2b48c 2px solid; PADDING-RIGHT: 2px; BORDER-TOP: #d2b48c 2px solid; OVERFLOW-Y: scroll; PADDING-LEFT: 2px; OVERFLOW-X: auto; BORDER-LEFT: #d2b48c 2px solid; PADDING-TOP: 2px; BORDER-BOTTOM: #d2b48c 2px solid"
										runat="server" Height="450px" Width="100%">
										<TABLE id="tbl" runat="server">
											<TR>
												<TD style="WIDTH: 174px" vAlign="top">
													<asp:label id="Label8" runat="server" CssClass="enterDataLabel" Height="26px"> Сума(EUR):</asp:label>
													<asp:label id="Label9" runat="server" CssClass="enterDataLabel" Height="22px"> Платено:</asp:label>
													<asp:label id="Label10" runat="server" CssClass="enterDataLabel" Height="22px">Дата на плащане:</asp:label>
													<asp:label id="Label1" runat="server" CssClass="enterDataLabel" Height="22px">Период от:</asp:label>
													<asp:label id="Label2" runat="server" CssClass="enterDataLabel" Height="22px">Период до:</asp:label>
													<asp:label id="Label3" runat="server" CssClass="enterDataLabel" Height="22px">Забележка:</asp:label></TD>
												<TD>
													<asp:datalist id="dlPayments" runat="server" RepeatDirection="Horizontal">
														<ItemTemplate>
															<asp:textbox id=txtAmount runat="server" CssClass="enterDataBox" Width="100px" Height="20px" Text='<%# Asa.Timesheet.WebPages.UIHelpers.FormatDecimal2((decimal)DataBinder.Eval(Container, "DataItem.Amount")) %>'>
															</asp:textbox><BR>
															<asp:CheckBox id="ckPaid" runat="server" Text="ДА" Checked='<%# DataBinder.Eval(Container, "DataItem.Done") %>'>
															</asp:CheckBox><BR>
															<asp:textbox id=txtDate runat="server" CssClass="enterDataBox" Width="80px" Height="20px" Text='<%#  Asa.Timesheet.Data.Util.TimeHelper.FormatDate((DateTime)DataBinder.Eval(Container, "DataItem.PaymentDate")) %>'>
															</asp:textbox><IMG id="lkCal" alt="Calendar" src="images/calendar.ico" width="18" align="absBottom"
																runat="server"><br>
															<asp:Label id="txtStartDate" runat="server" CssClass="enterDataBox" Width="80px" Height="20px" Text='<%#  Asa.Timesheet.Data.Util.TimeHelper.FormatDate((DateTime)DataBinder.Eval(Container, "DataItem.AuthorsStartDate")) %>'>
															</asp:Label><br>
															<asp:Label id="txtEndDate" runat="server" CssClass="enterDataBox" Width="80px" Height="20px" Text='<%#  Asa.Timesheet.Data.Util.TimeHelper.FormatDate((DateTime)DataBinder.Eval(Container, "DataItem.AuthorsEndDate")) %>'>
															</asp:Label><br>
															<asp:textbox id="txtNotes" runat="server" CssClass="enterDataBox" TextMode=MultiLine Width="100px" Height="60px" Text='<%#  DataBinder.Eval(Container, "DataItem.Notes") %>'>
															</asp:textbox>
														</ItemTemplate>
													</asp:datalist></TD>
												<TD></TD>
											</TR>
											<TR>
												<TD style="WIDTH: 174px" vAlign="top"></TD>
												<TD>
													<asp:button id="btnSave" runat="server" CssClass="ActionButton" Width="128px" Text="Запиши"></asp:button>&nbsp;
													<asp:button id="btnNewPayment" runat="server" CssClass="ActionButton" Width="144px" Text="Ново плащане"></asp:button></TD>
												<TD></TD>
											</TR>
										</TABLE>
									</asp:panel>
									<TABLE id="Table4" style="BORDER-RIGHT: tan thin solid; BORDER-TOP: tan thin solid; BORDER-LEFT: tan thin solid; BORDER-BOTTOM: tan thin solid"
										cellSpacing="0" cellPadding="4" width="100%" border="0">
										<TR>
											<TD>
												<asp:button id="btnProject" runat="server" CssClass="ActionButton" Width="216px" Text="Връщане към проекта"></asp:button></TD>
										</TR>
									</TABLE>
								</td>
							</tr>
						</table>
					</td>
				</tr>
			</table>
		</form>
	</body>
</HTML>
