using System;
using System.Data;
using DataDynamics.ActiveReports;
using DataDynamics.ActiveReports.Document;
using Asa.Timesheet.Data;
using Asa.Timesheet.Data.Util;

namespace Asa.Timesheet.Windows.Mail
{
	public class LateReport : ActiveReport
	{
		private bool _pdf;
		private string _createdBy= string.Empty;
		public LateReport(HeaderInfo hi,bool pdf,string createdBy, DataSet ds, WorkedHoursInfo workedHoursInfo, bool IsTraine)
		{
			_pdf=pdf;
			_createdBy = createdBy;
			
			InitializeReport();
			SetFilterInfo(hi);
			this.DataSource = ds.Tables[0];
			this.ReportEnd += new EventHandler(SubAnalysisRpt1_ReportEnd);
			this.Detail.Format+=new EventHandler(Detail_Format);
			this.GroupFooter1.Format+=new EventHandler(GroupFooter1_Format);
			if (workedHoursInfo!=null)
			{
				this.SummarySubReport.Report = new WorkTimesFooter(workedHoursInfo,false,IsTraine);
				lbUser.Visible=txtUser.Visible=true;
				txtUser.Text=hi.User;
			}
			txtCreatedBy.Text = _createdBy;
			if(IsTraine)
			{
				Label9.Visible=txtUnder.Visible=txtUnderT.Visible=false;
			}
			
		}

		#region ActiveReports Designer generated code
		private DataDynamics.ActiveReports.ReportHeader ReportHeader = null;
		private DataDynamics.ActiveReports.TextBox txtHeader = null;
		private DataDynamics.ActiveReports.PageHeader PageHeader = null;
		private DataDynamics.ActiveReports.GroupHeader GroupHeader1 = null;
		private DataDynamics.ActiveReports.TextBox txtFilter4 = null;
		private DataDynamics.ActiveReports.Label lblFilter4 = null;
		private DataDynamics.ActiveReports.Label lblFilter3 = null;
		private DataDynamics.ActiveReports.TextBox txtFilter2 = null;
		private DataDynamics.ActiveReports.Label lblFilter2 = null;
		private DataDynamics.ActiveReports.TextBox txtFilter1 = null;
		private DataDynamics.ActiveReports.Label lblFilter1 = null;
		private DataDynamics.ActiveReports.TextBox txtFilter3 = null;
		private DataDynamics.ActiveReports.Line Line10 = null;
		private DataDynamics.ActiveReports.Label Label4 = null;
		private DataDynamics.ActiveReports.Label Label5 = null;
		private DataDynamics.ActiveReports.Label Label6 = null;
		private DataDynamics.ActiveReports.TextBox TextBox2 = null;
		private DataDynamics.ActiveReports.Label Label8 = null;
		private DataDynamics.ActiveReports.Label Label9 = null;
		private DataDynamics.ActiveReports.Label Label11 = null;
		private DataDynamics.ActiveReports.Detail Detail = null;
		private DataDynamics.ActiveReports.TextBox txtProjectName = null;
		private DataDynamics.ActiveReports.TextBox txtTotalTime = null;
		private DataDynamics.ActiveReports.TextBox txtOvertime = null;
		private DataDynamics.ActiveReports.TextBox txtTotal = null;
		private DataDynamics.ActiveReports.TextBox txtNormativ = null;
		private DataDynamics.ActiveReports.TextBox txtUnder = null;
		private DataDynamics.ActiveReports.TextBox TextBox3 = null;
		private DataDynamics.ActiveReports.GroupFooter GroupFooter1 = null;
		private DataDynamics.ActiveReports.Line Line12 = null;
		private DataDynamics.ActiveReports.TextBox txtUnderT = null;
		private DataDynamics.ActiveReports.TextBox txtTotalTimeT = null;
		private DataDynamics.ActiveReports.Label Label10 = null;
		private DataDynamics.ActiveReports.PageFooter PageFooter = null;
		private DataDynamics.ActiveReports.ReportFooter ReportFooter = null;
		private DataDynamics.ActiveReports.TextBox txtUser = null;
		private DataDynamics.ActiveReports.Label lbUser = null;
		private DataDynamics.ActiveReports.TextBox txtCreatedBy = null;
		private DataDynamics.ActiveReports.Label Label7 = null;
		private DataDynamics.ActiveReports.SubReport SummarySubReport = null;
		public void InitializeReport()
		{
			this.LoadLayout(this.GetType(), "Mail.LateReport.rpx");
			this.ReportHeader = ((DataDynamics.ActiveReports.ReportHeader)(this.Sections["ReportHeader"]));
			this.PageHeader = ((DataDynamics.ActiveReports.PageHeader)(this.Sections["PageHeader"]));
			this.GroupHeader1 = ((DataDynamics.ActiveReports.GroupHeader)(this.Sections["GroupHeader1"]));
			this.Detail = ((DataDynamics.ActiveReports.Detail)(this.Sections["Detail"]));
			this.GroupFooter1 = ((DataDynamics.ActiveReports.GroupFooter)(this.Sections["GroupFooter1"]));
			this.PageFooter = ((DataDynamics.ActiveReports.PageFooter)(this.Sections["PageFooter"]));
			this.ReportFooter = ((DataDynamics.ActiveReports.ReportFooter)(this.Sections["ReportFooter"]));
			this.txtHeader = ((DataDynamics.ActiveReports.TextBox)(this.ReportHeader.Controls[0]));
			this.txtFilter4 = ((DataDynamics.ActiveReports.TextBox)(this.GroupHeader1.Controls[0]));
			this.lblFilter4 = ((DataDynamics.ActiveReports.Label)(this.GroupHeader1.Controls[1]));
			this.lblFilter3 = ((DataDynamics.ActiveReports.Label)(this.GroupHeader1.Controls[2]));
			this.txtFilter2 = ((DataDynamics.ActiveReports.TextBox)(this.GroupHeader1.Controls[3]));
			this.lblFilter2 = ((DataDynamics.ActiveReports.Label)(this.GroupHeader1.Controls[4]));
			this.txtFilter1 = ((DataDynamics.ActiveReports.TextBox)(this.GroupHeader1.Controls[5]));
			this.lblFilter1 = ((DataDynamics.ActiveReports.Label)(this.GroupHeader1.Controls[6]));
			this.txtFilter3 = ((DataDynamics.ActiveReports.TextBox)(this.GroupHeader1.Controls[7]));
			this.Line10 = ((DataDynamics.ActiveReports.Line)(this.GroupHeader1.Controls[8]));
			this.Label4 = ((DataDynamics.ActiveReports.Label)(this.GroupHeader1.Controls[9]));
			this.Label5 = ((DataDynamics.ActiveReports.Label)(this.GroupHeader1.Controls[10]));
			this.Label6 = ((DataDynamics.ActiveReports.Label)(this.GroupHeader1.Controls[11]));
			this.TextBox2 = ((DataDynamics.ActiveReports.TextBox)(this.GroupHeader1.Controls[12]));
			this.Label8 = ((DataDynamics.ActiveReports.Label)(this.GroupHeader1.Controls[13]));
			this.Label9 = ((DataDynamics.ActiveReports.Label)(this.GroupHeader1.Controls[14]));
			this.Label11 = ((DataDynamics.ActiveReports.Label)(this.GroupHeader1.Controls[15]));
			this.txtProjectName = ((DataDynamics.ActiveReports.TextBox)(this.Detail.Controls[0]));
			this.txtTotalTime = ((DataDynamics.ActiveReports.TextBox)(this.Detail.Controls[1]));
			this.txtOvertime = ((DataDynamics.ActiveReports.TextBox)(this.Detail.Controls[2]));
			this.txtTotal = ((DataDynamics.ActiveReports.TextBox)(this.Detail.Controls[3]));
			this.txtNormativ = ((DataDynamics.ActiveReports.TextBox)(this.Detail.Controls[4]));
			this.txtUnder = ((DataDynamics.ActiveReports.TextBox)(this.Detail.Controls[5]));
			this.TextBox3 = ((DataDynamics.ActiveReports.TextBox)(this.Detail.Controls[6]));
			this.Line12 = ((DataDynamics.ActiveReports.Line)(this.GroupFooter1.Controls[0]));
			this.txtUnderT = ((DataDynamics.ActiveReports.TextBox)(this.GroupFooter1.Controls[1]));
			this.txtTotalTimeT = ((DataDynamics.ActiveReports.TextBox)(this.GroupFooter1.Controls[2]));
			this.Label10 = ((DataDynamics.ActiveReports.Label)(this.GroupFooter1.Controls[3]));
			this.txtUser = ((DataDynamics.ActiveReports.TextBox)(this.ReportFooter.Controls[0]));
			this.lbUser = ((DataDynamics.ActiveReports.Label)(this.ReportFooter.Controls[1]));
			this.txtCreatedBy = ((DataDynamics.ActiveReports.TextBox)(this.ReportFooter.Controls[2]));
			this.Label7 = ((DataDynamics.ActiveReports.Label)(this.ReportFooter.Controls[3]));
			this.SummarySubReport = ((DataDynamics.ActiveReports.SubReport)(this.ReportFooter.Controls[4]));
		}

		#endregion

		private void SetFilterInfo(HeaderInfo hi)
		{
			int currentLabel = 1;

			if (hi.StartDate > Constants.DateMin)
			{
				Label lbl = (Label)this.GroupHeader1.Controls["lblFilter"+currentLabel.ToString()];
				lbl.Visible = true;
				lbl.Text = Resource.ResourceManager["reports_Period"];
				TextBox txt = (TextBox)this.GroupHeader1.Controls["txtFilter"+currentLabel.ToString()];
				txt.Visible = true;
				txt.Text = String.Concat(hi.StartDate.ToString("d MMM yyyy")," ", 
					Resource.ResourceManager["reports_YearAbbr"], " - ", 
					hi.EndDate.ToString("d MMM yyyy"), " ", 
					Resource.ResourceManager["reports_YearAbbr"]);
				currentLabel++;
			}
			
			if (hi.User != String.Empty)
			{
				Label lbl = (Label)this.GroupHeader1.Controls["lblFilter"+currentLabel.ToString()];
				lbl.Visible = true;
				lbl.Text = Resource.ResourceManager["reports_User"];
				TextBox txt = (TextBox)this.GroupHeader1.Controls["txtFilter"+currentLabel.ToString()];
				txt.Visible = true;
				txt.Text = hi.User;
				currentLabel++;
			}

			if (hi.Activity != String.Empty)
			{
				Label lbl = (Label)this.GroupHeader1.Controls["lblFilter"+currentLabel.ToString()];
				lbl.Visible = true;
				lbl.Text = Resource.ResourceManager["reports_Activity"];
				TextBox txt = (TextBox)this.GroupHeader1.Controls["txtFilter"+currentLabel.ToString()];
				txt.Visible = true;
				txt.Text = hi.Activity;
				currentLabel++;
			}

			if (hi.BuildingType != String.Empty)
			{
				Label lbl = (Label)this.GroupHeader1.Controls["lblFilter"+currentLabel.ToString()];
				lbl.Visible = true;
				lbl.Text = Resource.ResourceManager["reports_BuildingType"];
				TextBox txt = (TextBox)this.GroupHeader1.Controls["txtFilter"+currentLabel.ToString()];
				txt.Visible = true;
				txt.Text = hi.BuildingType;
				currentLabel++;
			}
			if(SessionManager.LoggedUserInfo!=null && !SessionManager.LoggedUserInfo.HasPaymentRights)
			{
				Label5.Visible = false;
			}

		}
		private void ReportFooter_Format(object sender, System.EventArgs eArgs)
		{
			
		}

		private void PageHeader_Format(object sender, System.EventArgs eArgs)
		{
			if(!_pdf)
				if(this.PageNumber >1)
					this.PageHeader.Visible= false;
		}

		private void Detail_Format(object sender, System.EventArgs eArgs)
		{

			if (!_pdf)
			{
				
				if(txtTotalTime.Text!=null && txtTotalTime.Text!="")
					txtTotalTime.Text = TimeHelper.HoursFromMinutes((int)decimal.Parse(txtTotalTime.Text)).ToString("0.00");
				if(txtUnder.Text!=null && txtUnder.Text!="")
					txtUnder.Text = TimeHelper.HoursFromMinutes((int)decimal.Parse(txtUnder.Text)).ToString("0.00");
			}
			else
			{

				if(txtTotalTime.Text!=null && txtTotalTime.Text!="")
					txtTotalTime.Text = TimeHelper.HoursStringFromMinutes((int)decimal.Parse(txtTotalTime.Text), false);
				if(txtUnder.Text!=null && txtUnder.Text!="")
					txtUnder.Text = TimeHelper.HoursStringFromMinutes((int)decimal.Parse(txtUnder.Text), false);
			}
		}
		private void SubAnalysisRpt1_ReportEnd(object sender, EventArgs e)
		{
			if (!this._pdf) return;

			Logo1 logo = new Logo1();
			logo.Run();

			for (int i=0; i<this.Document.Pages.Count; i++)
				this.Document.Pages[i].Overlay(logo.Document.Pages[0]);
		}
		private void GroupFooter1_Format(object sender, EventArgs e)
		{
			if (!_pdf)
			{
				
				if(txtTotalTimeT.Text!=null)
					txtTotalTimeT.Text = TimeHelper.HoursFromMinutes((int)decimal.Parse(txtTotalTimeT.Text)).ToString("0.00");
				if(txtUnderT.Text!=null)
					txtUnderT.Text = TimeHelper.HoursFromMinutes((int)decimal.Parse(txtUnderT.Text)).ToString("0.00");
			}
			else
			{
				
				if(txtTotalTimeT.Text!=null)
					txtTotalTimeT.Text = TimeHelper.HoursStringFromMinutes((int)decimal.Parse(txtTotalTimeT.Text), false);
				if(txtUnderT.Text!=null)
					txtUnderT.Text = TimeHelper.HoursStringFromMinutes((int)decimal.Parse(txtUnderT.Text), false);
			}
		}
	}

}
