using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Data.SqlClient;
using log4net;
using Asa.Timesheet.Data;
using Asa.Timesheet.Data.Entities;
using System.Text;
using Asa.Timesheet.Data.Util;

namespace Asa.Timesheet.WebPages
{
	/// <summary>
	/// Summary description for EditAutomobile.
	/// </summary>
	public class EditAutomobile : TimesheetPageBase
	{
		protected System.Web.UI.WebControls.PlaceHolder menuHolder;
		protected System.Web.UI.WebControls.Button btnSaveUP;
		protected System.Web.UI.WebControls.Button btnEditUP;
		protected System.Web.UI.WebControls.Button btnCancelUP;
		protected System.Web.UI.WebControls.Label lblName;
		protected System.Web.UI.WebControls.Label Label16;
		protected System.Web.UI.WebControls.Label Label5;
		protected System.Web.UI.WebControls.Button btnSave;
		protected System.Web.UI.WebControls.Button btnEdit;
		protected System.Web.UI.WebControls.Button btnCancel;
		protected System.Web.UI.WebControls.Label lblInfo;
		protected System.Web.UI.WebControls.TextBox txtMarka;
		protected System.Web.UI.WebControls.TextBox txtNomer;
		protected System.Web.UI.WebControls.Label lbDate;
		protected System.Web.UI.WebControls.TextBox txtCivilDate;
		protected System.Web.UI.WebControls.DropDownList ddlCivil;
		protected System.Web.UI.WebControls.Label Label3;
		protected System.Web.UI.WebControls.Label Label4;
		protected System.Web.UI.WebControls.TextBox txtKaskoDate;
		protected System.Web.UI.WebControls.DropDownList ddlKasko;
		protected System.Web.UI.HtmlControls.HtmlImage imgCivil;
		protected System.Web.UI.HtmlControls.HtmlImage imgKasko;
		protected System.Web.UI.WebControls.Label Label1;
		protected System.Web.UI.WebControls.TextBox txtGreencardDate;
		protected System.Web.UI.WebControls.Label Label2;
		protected System.Web.UI.WebControls.DropDownList ddlGreencard;
		protected System.Web.UI.WebControls.Label Label6;
		protected System.Web.UI.WebControls.TextBox txtAnnualCheck;
		protected System.Web.UI.WebControls.Label Label7;
		protected System.Web.UI.WebControls.TextBox txtDateOil;
		protected System.Web.UI.WebControls.Label Label8;
		protected System.Web.UI.WebControls.TextBox txtOilKM;
		protected System.Web.UI.WebControls.Label Label9;
		protected System.Web.UI.WebControls.TextBox txtDateOilNext;
		protected System.Web.UI.WebControls.Label Label10;
		protected System.Web.UI.WebControls.TextBox txtOilNextKM;
		protected System.Web.UI.WebControls.Label Label11;
		protected System.Web.UI.WebControls.DropDownList ddlService;
		protected System.Web.UI.HtmlControls.HtmlImage imgGreencard;
		protected System.Web.UI.HtmlControls.HtmlImage imgCheck;
		protected System.Web.UI.HtmlControls.HtmlImage imgOil;
		protected System.Web.UI.HtmlControls.HtmlImage imgOilNext;
		protected System.Web.UI.WebControls.Label lblError;
		protected System.Web.UI.WebControls.Label Label12;
		protected System.Web.UI.WebControls.DropDownList ddlUser;
		protected System.Web.UI.WebControls.DataList dlOldIssues;
		protected System.Web.UI.WebControls.Button btnAdd;
		protected System.Web.UI.WebControls.TextBox txtNewRepair;
		protected System.Web.UI.WebControls.Label lblNew;
		protected System.Web.UI.WebControls.Label Label17;
		protected System.Web.UI.WebControls.TextBox txtDateRepair;
		protected System.Web.UI.HtmlControls.HtmlImage imgRepair;
		protected Asa.Timesheet.WebPages.UserControls.PageHeader header;
        private static readonly ILog log = LogManager.GetLogger(typeof(Automobiles));

        private void Page_Load(object sender, System.EventArgs e)
        {
            UIHelpers.CreateMenu(menuHolder, LoggedUser);

            if (!this.IsPostBack)
            {
                header.UserName = LoggedUser.FullName;
                header.PageTitle = Resource.ResourceManager["editService_EditLabel"];
                LoadFirst();

                if (UIHelpers.GetIDParam() > 0)
                {
                    LoadFromID();

                }
                else
                {
                    txtNewRepair.Visible = btnAdd.Visible = lblNew.Visible = false;
                }
            }
            if (!( LoggedUser.HasPaymentRights || LoggedUser.IsSecretary || LoggedUser.IsAssistant))
            {
                ErrorRedirect(Resource.ResourceManager["noRightsAlert"]);
            }
            imgCheck.Attributes["onclick"] = TimeHelper.InvokePopupCal(txtAnnualCheck);
            imgCivil.Attributes["onclick"] = TimeHelper.InvokePopupCal(txtCivilDate);
            imgGreencard.Attributes["onclick"] = TimeHelper.InvokePopupCal(txtGreencardDate);
            imgKasko.Attributes["onclick"] = TimeHelper.InvokePopupCal(txtKaskoDate);
            imgOil.Attributes["onclick"] = TimeHelper.InvokePopupCal(txtDateOil);
            imgOilNext.Attributes["onclick"] = TimeHelper.InvokePopupCal(txtDateOilNext);
            imgRepair.Attributes["onclick"] = TimeHelper.InvokePopupCal(txtDateRepair);

        }

		#region Web Form Designer generated code
        override protected void OnInit(EventArgs e)
        {
            //
            // CODEGEN: This call is required by the ASP.NET Web Form Designer.
            //
            InitializeComponent();
            base.OnInit(e);
        }
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
        private void InitializeComponent()
        {
            this.btnSaveUP.Click += new System.EventHandler(this.btnSave_Click);
            this.btnCancelUP.Click += new System.EventHandler(this.btnCancel_Click);
            this.btnAdd.Click += new System.EventHandler(this.btnAdd_Click);
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            this.Load += new System.EventHandler(this.Page_Load);

        }
		#endregion
        private void LoadFromID()
        {
            int nID = UIHelpers.GetIDParam();
            dlOldIssues.DataSource = AutoRepairDAL.LoadCollection(nID);
            dlOldIssues.DataBind();
            AutomobileData ad = AutomobileDAL.Load(nID);
            if (ad != null)
            {
                txtAnnualCheck.Text = TimeHelper.FormatDate(ad.AnnualCheckDate);
                txtCivilDate.Text = TimeHelper.FormatDate(ad.CivilDate);
                txtDateOil.Text = TimeHelper.FormatDate(ad.OilChangeDate);
                txtDateOilNext.Text = TimeHelper.FormatDate(ad.NextChangeDate);
                txtGreencardDate.Text = TimeHelper.FormatDate(ad.GreencardDate);
                txtKaskoDate.Text = TimeHelper.FormatDate(ad.KaskoDate);
                txtMarka.Text = ad.Name;
                txtNomer.Text = ad.RegNumber;
                txtOilKM.Text = ad.OilChangeKm;
                txtOilNextKM.Text = ad.NextChangeKm;
                if (ddlCivil.Items.FindByValue(ad.CivilInsuranceAgentID.ToString()) != null)
                    ddlCivil.SelectedValue = ad.CivilInsuranceAgentID.ToString();
                if (ddlGreencard.Items.FindByValue(ad.GreencardInsuranceAgentID.ToString()) != null)
                    ddlGreencard.SelectedValue = ad.GreencardInsuranceAgentID.ToString();
                if (ddlKasko.Items.FindByValue(ad.KaskoInsuranceAgentID.ToString()) != null)
                    ddlKasko.SelectedValue = ad.KaskoInsuranceAgentID.ToString();
                if (ddlService.Items.FindByValue(ad.AutoServiceID.ToString()) != null)
                    ddlService.SelectedValue = ad.AutoServiceID.ToString();
                if (ddlUser.Items.FindByValue(ad.UserID.ToString()) != null)
                    ddlUser.SelectedValue = ad.UserID.ToString();
            }
        }
        private bool LoadUsers()
        {
            //if (this.LoggedUser.IsLeader)
            {
                ddlUser.Visible = true;

                SqlDataReader reader = null;
                try
                {
                    reader = UsersData.SelectUserNames(true);

                    ddlUser.DataSource = reader;
                    ddlUser.DataValueField = "UserID";
                    ddlUser.DataTextField = "FullName";
                    ddlUser.DataBind();
                    ddlUser.Items.Insert(0, new ListItem("<" + Resource.ResourceManager["btnChooseNew_New"] + ">", "0"));
                    ddlUser.SelectedValue = "0";
                }
                catch (Exception ex)
                {
                    log.Error(ex);
                    return false;
                }
                finally
                {
                    if (reader != null) reader.Close();
                }
            }

            return true;
        }

        private void btnSave_Click(object sender, System.EventArgs e)
        {
            int nID = UIHelpers.GetIDParam();
            AutomobileData ad = new AutomobileData();
            ad.AutomobileID = nID;
            ad.AnnualCheckDate = TimeHelper.GetDate(txtAnnualCheck.Text);
            ad.CivilDate = TimeHelper.GetDate(txtCivilDate.Text);
            ad.OilChangeDate = TimeHelper.GetDate(txtDateOil.Text);
            ad.NextChangeDate = TimeHelper.GetDate(txtDateOilNext.Text);
            ad.GreencardDate = TimeHelper.GetDate(txtGreencardDate.Text);
            ad.KaskoDate = TimeHelper.GetDate(txtKaskoDate.Text);
            ad.Name = txtMarka.Text;
            ad.RegNumber = txtNomer.Text;
            ad.OilChangeKm = txtOilKM.Text;
            ad.NextChangeKm = txtOilNextKM.Text;
            ad.GreencardInsuranceAgentID = UIHelpers.ToInt(ddlGreencard.SelectedValue);
            ad.KaskoInsuranceAgentID = UIHelpers.ToInt(ddlKasko.SelectedValue);
            ad.CivilInsuranceAgentID = UIHelpers.ToInt(ddlCivil.SelectedValue);
            ad.AutoServiceID = UIHelpers.ToInt(ddlService.SelectedValue);
            ad.UserID = UIHelpers.ToInt(ddlUser.SelectedValue);

            AutomobileDAL.Save(ad);
            Response.Redirect("Automobiles.aspx");
        }
        private void LoadFirst()
        {
            LoadUsers();
            ddlCivil.DataSource = InsuranceAgentDAL.LoadCollection();
            ddlCivil.DataValueField = "InsuranceAgentID";
            ddlCivil.DataTextField = "InsuranceAgentName";
            ddlCivil.DataBind();
            ddlCivil.Items.Insert(0, new ListItem(Resource.ResourceManager["btnChooseNew_New"], "-1"));


            ddlKasko.DataSource = InsuranceAgentDAL.LoadCollection();
            ddlKasko.DataValueField = "InsuranceAgentID";
            ddlKasko.DataTextField = "InsuranceAgentName";
            ddlKasko.DataBind();
            ddlKasko.Items.Insert(0, new ListItem(Resource.ResourceManager["btnChooseNew_New"], "-1"));

            ddlGreencard.DataSource = InsuranceAgentDAL.LoadCollection();
            ddlGreencard.DataValueField = "InsuranceAgentID";
            ddlGreencard.DataTextField = "InsuranceAgentName";
            ddlGreencard.DataBind();
            ddlGreencard.Items.Insert(0, new ListItem(Resource.ResourceManager["btnChooseNew_New"], "-1"));

            ddlService.DataSource = AutoServiceDAL.LoadCollection();
            ddlService.DataValueField = "AutoServiceID";
            ddlService.DataTextField = "AutoServiceName";
            ddlService.DataBind();
            ddlService.Items.Insert(0, new ListItem(Resource.ResourceManager["btnChooseNew_New"], "-1"));
        }
        private void btnCancel_Click(object sender, System.EventArgs e)
        {
            Response.Redirect("Automobiles.aspx");
        }

        private void btnAdd_Click(object sender, System.EventArgs e)
        {
            AutoRepairData apr = new AutoRepairData();
            apr.AutomobileID = UIHelpers.GetIDParam();
            apr.AutoRepairDescription = txtNewRepair.Text;
            apr.AutoServiceID = int.Parse(ddlService.SelectedValue);
            apr.DateAdded = TimeHelper.GetDate(txtDateRepair.Text);

            AutoRepairDAL.Save(apr);
            txtNewRepair.Text = "";
            txtDateRepair.Text = "";
            LoadFromID();
        }
	}
}
