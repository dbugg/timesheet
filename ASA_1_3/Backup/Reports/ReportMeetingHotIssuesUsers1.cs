using System;
using System.Data;
using GrapeCity.ActiveReports;
using GrapeCity.ActiveReports.Controls;
using GrapeCity.ActiveReports.SectionReportModel;
using GrapeCity.ActiveReports.Document.Section;
using GrapeCity.ActiveReports.Document;

namespace Asa.Timesheet.WebPages.Reports
{
    public class ReportMeetingHotIssuesUsers : GrapeCity.ActiveReports.SectionReport
	{
        public ReportMeetingHotIssuesUsers(DataView dv)
        {
            InitializeComponent();
            this.DataSource = dv;
        }

		#region ActiveReports Designer generated code








        public void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ReportMeetingHotIssuesUsers));
            this.Detail = new GrapeCity.ActiveReports.SectionReportModel.Detail();
            this.ReportHeader = new GrapeCity.ActiveReports.SectionReportModel.ReportHeader();
            this.ReportFooter = new GrapeCity.ActiveReports.SectionReportModel.ReportFooter();
            this.TextBox4 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.TextBox6 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtMail = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtName = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtFrom = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            ((System.ComponentModel.ISupportInitialize)(this.TextBox4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TextBox6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtMail)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtFrom)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            // 
            // Detail
            // 
            this.Detail.ColumnSpacing = 0F;
            this.Detail.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
						this.txtMail,
						this.txtName,
						this.txtFrom});
            this.Detail.Height = 0.25F;
            this.Detail.Name = "Detail";
            // 
            // ReportHeader
            // 
            this.ReportHeader.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
						this.TextBox4,
						this.TextBox6});
            this.ReportHeader.Height = 0F;
            this.ReportHeader.Name = "ReportHeader";
            // 
            // ReportFooter
            // 
            this.ReportFooter.Height = 0F;
            this.ReportFooter.Name = "ReportFooter";
            // 
            // TextBox4
            // 
            this.TextBox4.Height = 0.2F;
            this.TextBox4.Left = 4F;
            this.TextBox4.Name = "TextBox4";
            this.TextBox4.Style = "font-size: 12pt; font-weight: bold";
            this.TextBox4.Text = "????";
            this.TextBox4.Top = 0F;
            this.TextBox4.Visible = false;
            this.TextBox4.Width = 2F;
            // 
            // TextBox6
            // 
            this.TextBox6.Height = 0.2F;
            this.TextBox6.Left = 0F;
            this.TextBox6.Name = "TextBox6";
            this.TextBox6.Style = "font-size: 12pt; font-weight: bold; vertical-align: top";
            this.TextBox6.Text = "???";
            this.TextBox6.Top = 0F;
            this.TextBox6.Visible = false;
            this.TextBox6.Width = 4F;
            // 
            // txtMail
            // 
            this.txtMail.DataField = "UserMail";
            this.txtMail.Height = 0.2F;
            this.txtMail.Left = 5.375F;
            this.txtMail.Name = "txtMail";
            this.txtMail.Style = "font-family: Verdana; font-size: 9pt; font-weight: normal; text-align: right; ddo" +
                "-char-set: 1";
            this.txtMail.Top = 0F;
            this.txtMail.Width = 2.5F;
            // 
            // txtName
            // 
            this.txtName.DataField = "UserName";
            this.txtName.Height = 0.2F;
            this.txtName.Left = 0F;
            this.txtName.Name = "txtName";
            this.txtName.Style = "font-family: Verdana; font-size: 9pt; font-weight: normal; ddo-char-set: 1";
            this.txtName.Top = 0F;
            this.txtName.Width = 2.5F;
            // 
            // txtFrom
            // 
            this.txtFrom.DataField = "UserFrom";
            this.txtFrom.Height = 0.2F;
            this.txtFrom.Left = 2.5625F;
            this.txtFrom.Name = "txtFrom";
            this.txtFrom.Style = "font-family: Verdana; font-size: 9pt; font-weight: normal; ddo-char-set: 1";
            this.txtFrom.Top = 0F;
            this.txtFrom.Width = 2.8125F;
            // 
            // ActiveReport1
            // 
            this.MasterReport = false;
            this.PageSettings.PaperHeight = 11F;
            this.PageSettings.PaperWidth = 8.5F;
            this.PrintWidth = 7.96875F;
            this.Sections.Add(this.ReportHeader);
            this.Sections.Add(this.Detail);
            this.Sections.Add(this.ReportFooter);
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule(resources.GetString("$this.StyleSheet"), "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: inherit; font-style: inherit; font-variant: inherit; font-weight: bo" +
                        "ld; font-size: 16pt; font-size-adjust: inherit; font-stretch: inherit", "Heading1", "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: Times New Roman; font-style: italic; font-variant: inherit; font-wei" +
                        "ght: bold; font-size: 14pt; font-size-adjust: inherit; font-stretch: inherit", "Heading2", "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: inherit; font-style: inherit; font-variant: inherit; font-weight: bo" +
                        "ld; font-size: 13pt; font-size-adjust: inherit; font-stretch: inherit", "Heading3", "Normal"));
            ((System.ComponentModel.ISupportInitialize)(this.TextBox4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TextBox6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtMail)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtFrom)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();
        }

		#endregion

        private ReportHeader ReportHeader;
        private TextBox TextBox4;
        private TextBox TextBox6;
        private Detail Detail;
        private TextBox txtMail;
        private TextBox txtName;
        private TextBox txtFrom;
        private ReportFooter ReportFooter;
	}
}
