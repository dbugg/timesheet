<%@ Page language="c#" Codebehind="CopyHours.aspx.cs" AutoEventWireup="false" Inherits="Asa.Timesheet.WebPages.CopyHours" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
	<HEAD>
		<title>CopyHours</title>
		<meta name="GENERATOR" Content="Microsoft Visual Studio .NET 7.1">
		<meta name="CODE_LANGUAGE" Content="C#">
		<meta name="vs_defaultClientScript" content="JavaScript">
		<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5">
		<LINK href="styles/timesheet.css" type="text/css" rel="stylesheet">
		<script language="javascript">
		
		function UserDblClick()
		{
			var src, dest;
			
			src = event.srcElement;
			switch (src.id)
			{
				case "lbUsers" : dest = document.getElementById("lbSelectedUsers");
								 break;
				case "lbSelectedUsers" : dest = document.getElementById("lbUsers");
								break;
			}
			
			index = src.selectedIndex;
			if (index==-1) return;
			
			var user = src.options[index].innerText;
			var value = src.options[index].value;
			
			src.options.remove(index);
			var opt = document.createElement("OPTION");
			dest.options.add(opt); 
			opt.innerText = user;
			opt.value = value;
			
			UpdateContainer(src);
			UpdateContainer(dest);
		} 
		
		function ArrowButtonClick()
		{
			var btn = event.srcElement;
			var src, dest;
			
			switch (btn.id)
			{
				case "inputUsersToRight" : src = document.getElementById("lbUsers");
									  dest = document.getElementById("lbSelectedUsers");
									  break;
				case "inputUsersToLeft" :  dest = document.getElementById("lbUsers");
									  src = document.getElementById("lbSelectedUsers");
									  break;
			}
			
			//for (i=src.options.length-1;i>=0 ;i--)
			
			for (i=0; i<src.options.length; i++)
			if (src.options[i].selected)
			{ 
				var email = src.options[i].innerText;
				var value = src.options[i].value;
				var opt = document.createElement("OPTION");
				dest.options.add(opt); 
				opt.innerText = email;
				opt.value = value;
			}
			
			for (i=src.options.length-1;i>=0 ;i--)
			if (src.options[i].selected) src.options.remove(i);
			
			UpdateContainer(dest);
			UpdateContainer(src);
			
		}
		
		function UpdateContainer(listBox)
		{
			var container = document.getElementById("hdn"+listBox.id);
			var arr = new Array();
			for (i=0; i<listBox.options.length; i++) arr.push(listBox.options[i].value+":"+listBox.options[i].innerText);
			container.value = arr.join(";");
		}
		
		function CheckSelected()
		{
			
			var project = document.getElementById("ddlProject");
			var errorMessages = Form1.hdnErrorMessages.value.split(';');
			
			if (project.value=="0")
			{
				document.getElementById("lblError").innerText = errorMessages[0];
				document.getElementById("lblProject").style.color = "red";
				return false;
			}
			else 
			{
				document.getElementById("lblError").innerText = "";
				document.getElementById("lblProject").style.color = "";
			}
			var user = document.getElementById("ddlUsers");
			if (user.value=="-1")
			{
				document.getElementById("lblError").innerText = errorMessages[1];
				document.getElementById("lblSelectUser").style.color = "red";
				return false;
			}
			else 
			{
				document.getElementById("lblError").innerText = "";
				document.getElementById("lblSelectUser").style.color = "";
			}
			
			return true;
		}
		
		function RemoveCurrent()
		{
		  listBox = document.getElementById("lbUsers");
		  
		  for (i=listBox.options.length-1; i>=0; i--) 
		  {
		    if (listBox.options[i].value == window.cu) 
		    {
		      listBox.options.remove(i); return;
		    }
		  }
		}
		
		function SaveClick()
		{
		  window.close();
		  window.opener.document.getElementById("txtCopiedUsers").value = document.getElementById("hdnlbSelectedUsers").value;
		  window.opener.document.getElementById("btnCopySave").click();
		}
		
		function CancelClick()
		{
		  window.close();
		}
		
		</script>
	</HEAD>
	<body>
		<form id="Form1" method="post" runat="server">
			<TABLE id="Table1" cellSpacing="0" cellPadding="0" width="100%" border="0">
				<TR>
					<TD>
						<asp:Label id="lblTitle" runat="server" CssClass="PageCaption" Font-Size="16px" ForeColor="SaddleBrown">Добавяне на служители със същата дейност за периода</asp:Label></TD>
				</TR>
				<TR height="5">
					<TD></TD>
				</TR>
				<TR>
					<TD>
						<asp:Label id="lblSaveInfo" runat="server" Font-Size="11px"></asp:Label></TD>
				</TR>
				<TR height="10">
					<TD></TD>
				</TR>
				<TR>
					<TD>
						<TABLE id="Table8" cellSpacing="0" cellPadding="0" border="0">
							<TR>
								<TD style="WIDTH: 179px">
									<asp:listbox id="lbUsers" runat="server" CssClass="enterDataBox" SelectionMode="Multiple" Height="300px"
										Width="230px"></asp:listbox></TD>
								<TD style="WIDTH: 34px">
									<TABLE id="Table9" border="0">
										<TR>
											<TD><INPUT class="ActionButton" id="inputUsersToRight" style="WIDTH: 26px; HEIGHT: 18px" onclick="ArrowButtonClick();"
													type="button" size="20" value=">" name="Button2"></TD>
										</TR>
										<TR>
											<TD><INPUT class="ActionButton" id="inputUsersToLeft" style="WIDTH: 26px; HEIGHT: 18px" onclick="ArrowButtonClick();"
													type="button" size="20" value="<" name="Button1"></TD>
										</TR>
									</TABLE>
								</TD>
								<TD>
									<asp:listbox id="lbSelectedUsers" runat="server" CssClass="enterDataBox" SelectionMode="Multiple"
										Height="300px" Width="230px"></asp:listbox><INPUT id="hdnlbUsers" style="DISPLAY: none; WIDTH: 42px; HEIGHT: 19px" type="text" size="1"
										name="Text1" runat="server"><INPUT id="hdnlbSelectedUsers" style="DISPLAY: none; WIDTH: 34px; HEIGHT: 19px" type="text"
										size="1" name="Text2" runat="server"></TD>
							</TR>
						</TABLE>
					</TD>
				</TR>
				<TR height="10">
					<TD></TD>
				</TR>
				<TR>
					<TD>
						<TABLE id="Table5" cellSpacing="0" cellPadding="0" border="0" style="WIDTH: 496px; HEIGHT: 18px">
							<TR>
								<TD align="center">
									<asp:Button id="btnSave" runat="server" CssClass="ActionButton" EnableViewState="False" Text="Запиши"></asp:Button>&nbsp;
									<asp:Button id="btnCancel" runat="server" CssClass="ActionButton" EnableViewState="False" Text="Откажи"></asp:Button></TD>
							</TR>
						</TABLE>
					</TD>
				</TR>
			</TABLE>
		</form>
	</body>
</HTML>
