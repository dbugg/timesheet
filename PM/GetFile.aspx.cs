﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Asa.Timesheet.Data;

namespace PM
{
    public partial class GetFile : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                string fID = HttpContext.Current.Request.Params["fid"];
                string tID = HttpContext.Current.Request.Params["tid"];
                string uID = HttpContext.Current.Request.Params["uid"];
                if (String.IsNullOrEmpty(fID) || String.IsNullOrEmpty(tID) || String.IsNullOrEmpty(uID))
                {
                    //Wrong link
                    return;
                }
                FilePMData file = FilePMDAL.LoadLink(fID);
                string filePath = /*Server.MapPath(*/string.Concat(System.Configuration.ConfigurationManager.AppSettings["FilePath"], @"\", file.ProjectName, @"\", file.CategoryName, @"\", file.FileNameSave, file.FileType);//);
                string name = "";
                int uid = int.Parse(uID);
                //try to get user
                if (tID == "1")
                {
                    name = SubcontracterDAL.Load(uid).SubcontracterName;
                }
                else if (tID == "3")
                {
                    name = ClientDAL.Load(uid).ClientName;
                }
                else if (tID != "2")
                {
                    //wrong type
                    return;
                }

                Response.Cache.SetExpires(DateTime.Now.AddSeconds(-1));
                Response.Cache.SetCacheability(HttpCacheability.Private);

                Response.ContentType = "application/octet-stream";
                Response.ContentEncoding = System.Text.Encoding.UTF8;
                file.FileNameSave.Trim();
                file.FileType.Trim();
                string fileName = file.FileNameSave.Replace(" ", ""); //replace commas because chrome breaks
                string fileType = file.FileType.Replace(" ", "");
                //Response.AppendHeader("Content-Disposition", "attachment; filename=\"" + HttpUtility.UrlEncode(fileName, System.Text.Encoding.UTF8) + "\"");
                Response.AppendHeader("Content-Disposition", "attachment; filename=\"" + fileName + fileType + "\"");


                
                Response.TransmitFile(filePath);
                //notes:here send message if file is tracking

                Response.End();
                if (tID != "2")
                {
                    ProjectLogData pld = new ProjectLogData(-1, file.FileID, DateTime.Now, false, name);
                    ProjectLogDAL.Save(pld);
                }

                return;
            }
            catch
            {
                //wrong link
            }
        }
    }
}