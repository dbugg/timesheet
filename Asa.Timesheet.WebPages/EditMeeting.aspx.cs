using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Data.SqlClient;
using log4net;
using Asa.Timesheet.Data;
using Asa.Timesheet.Data.Entities;
using Asa.Timesheet.Data.Util;
using DataDynamics.ActiveReports.Export.Pdf;
using DataDynamics.ActiveReports.Export.Xls;
using Asa.Timesheet.WebPages.Reports;
using DataDynamics.ActiveReports.Export.Rtf;
using System.IO;

namespace Asa.Timesheet.WebPages
{
	/// <summary>
	/// Summary description for EditMeeting.
	/// </summary>
	public class EditMeeting : TimesheetPageBase
	{
		#region WebControls
		private static readonly ILog log = LogManager.GetLogger(typeof(EditMeeting));
		protected System.Web.UI.WebControls.PlaceHolder menuHolder;
		protected System.Web.UI.WebControls.Label lblName;
		protected System.Web.UI.WebControls.Button btnSave;
		protected System.Web.UI.WebControls.Button btnEdit;
		protected System.Web.UI.WebControls.Button btnCancel;
		protected System.Web.UI.WebControls.Label lblInfo;
		protected System.Web.UI.WebControls.TextBox txtStartDate;
		protected System.Web.UI.HtmlControls.HtmlImage lkCalendar1;
		protected System.Web.UI.WebControls.DropDownList ddlStartTime;
		protected System.Web.UI.WebControls.Label Label1;
		protected System.Web.UI.WebControls.Label Label2;
		protected System.Web.UI.WebControls.Label Label3;
		protected System.Web.UI.WebControls.ListBox lbUsers;
		protected System.Web.UI.WebControls.ListBox lbSelectedUsers;
		protected System.Web.UI.HtmlControls.HtmlInputText hdnlbUsers;
		protected System.Web.UI.HtmlControls.HtmlInputText hdnlbSelectedUsers;
		protected System.Web.UI.HtmlControls.HtmlInputText hdn1lbSelectedUsers;
		protected System.Web.UI.WebControls.ListBox lbClients;
		protected System.Web.UI.WebControls.ListBox lbSelectedClients;
		protected System.Web.UI.HtmlControls.HtmlInputText hdnlbClients;
		protected System.Web.UI.HtmlControls.HtmlInputText hdnlbSelectedClients;
		protected System.Web.UI.HtmlControls.HtmlInputText hdn1lbSelectedClients;
		protected System.Web.UI.WebControls.DropDownList ddlProject;
		protected System.Web.UI.WebControls.DropDownList ddlProjectsStatus;
		protected System.Web.UI.WebControls.DropDownList ddlBuildingTypes;
		protected System.Web.UI.WebControls.Label Label8;
		protected System.Web.UI.WebControls.Label lblError;
		protected System.Web.UI.WebControls.ListBox lbUsersMails;
		protected System.Web.UI.WebControls.ListBox lbSelectedUsersMails;
		protected System.Web.UI.HtmlControls.HtmlInputText hdnlbUsersMails;
		protected System.Web.UI.HtmlControls.HtmlInputText hdnlbSelectedUsersMails;
		protected System.Web.UI.WebControls.DropDownList ddlEndTime;
		protected System.Web.UI.WebControls.Label Label5;
		protected System.Web.UI.WebControls.Label lbDate;
		protected System.Web.UI.WebControls.Label lbStart;
		protected System.Web.UI.WebControls.Label lbEnd;
		protected System.Web.UI.WebControls.CheckBox ckApproved;
		protected Asa.Timesheet.WebPages.UserControls.PageHeader header;
		protected System.Web.UI.HtmlControls.HtmlInputFile FileCtrl;
		protected System.Web.UI.WebControls.HyperLink btnScan;
		protected System.Web.UI.WebControls.Label Label7;
		protected System.Web.UI.WebControls.Label Label9;
		protected System.Web.UI.WebControls.TextBox txtPlace;
		protected System.Web.UI.WebControls.TextBox txtOtherPeople;
		protected System.Web.UI.WebControls.TextBox txtNotes;
		protected System.Web.UI.WebControls.Label lbPlace;
		protected System.Web.UI.WebControls.ListBox lbSelectedMeetingProjects;
		protected System.Web.UI.WebControls.ListBox lbMeetingProjects;
		protected System.Web.UI.HtmlControls.HtmlInputText hdnlbMeetingProjects;
		protected System.Web.UI.HtmlControls.HtmlInputText hdnlbSelectedMeetingProjects;
		protected System.Web.UI.HtmlControls.HtmlInputText hdnlbMeetingProjects1;
		protected System.Web.UI.WebControls.ListBox lbMeetingProjects1;
		protected System.Web.UI.WebControls.Label lbProjects;
		protected System.Web.UI.WebControls.Button btnReload;
		protected System.Web.UI.WebControls.Label Label6;
		protected System.Web.UI.WebControls.DataList dlDocs;
		protected System.Web.UI.WebControls.Button btnScanDocAdd;
		protected System.Web.UI.WebControls.Button btnDel;
		protected System.Web.UI.WebControls.Button btnDelete;
		protected System.Web.UI.WebControls.ImageButton ibPdfExport;
		protected Asa.Timesheet.WebPages.UserControls.EditForm editCtrl;
		#endregion

		#region Page_Load

		private void Page_Load(object sender, System.EventArgs e)
		{

			lkCalendar1.Attributes["onclick"] = TimeHelper.InvokePopupCal(txtStartDate);
			if (!this.IsPostBack)
			{
				if(Page.IsPostBack == false)
				{
					// Set up form for data change checking when
					// first loaded.
					this.CheckForDataChanges= true;
					this.BypassPromptIds =
						new string[] {"ibPdfExport", "btnDel", "btnScanDocAdd", "btnReload", "btnSave", "btnEdit",  "btnDelete",  "ddlProject",  "ddlBuildingTypes",  "ddlProjectsStatus"};
				}
				if(UIHelpers.GetIDParam()<=0)
				{
					ddlProjectsStatus.SelectedIndex=1;
					header.PageTitle = Resource.ResourceManager["newMeeting_PageTitle"];
				}
				else
					header.PageTitle = Resource.ResourceManager["editMeeting_PageTitle"];
				header.UserName = this.LoggedUser.UserName;
				UIHelpers.LoadBuildingTypes(ddlBuildingTypes, "");
				LoadProjects();
				ArrayList times=LoadTimes();
				
				ddlStartTime.DataSource = times;
				ddlStartTime.DataValueField = "TimeID";
				ddlStartTime.DataTextField = "TimeString";
				ddlStartTime.DataBind();
				ddlEndTime.DataSource = times;
				ddlEndTime.DataValueField = "TimeID";
				ddlEndTime.DataTextField = "TimeString";
				ddlEndTime.DataBind();

				
				InitEdit();
				LoadUsers();
				LoadMeetingProjects(true);
				ReloadListsFromProjects(true);
				
				//ReloadLists();
				//BindGrid();
				
				if(UIHelpers.GetIDParam()>0)
				{
					LoadFromID(UIHelpers.GetIDParam());
					//ReloadLists();
					BindGrid();				
				}
				else
				{
					btnSave.Text=Resource.ResourceManager["btnNewMeeting"];
					UIHelpers.SetSelected(LoggedUser.UserID.ToString(), lbUsersMails, lbSelectedUsersMails);									
					hdnlbSelectedUsersMails.Value=LoggedUser.UserID.ToString();
					btnScanDocAdd.Visible=false;
					
				}
				
			}

			

			UIHelpers.CreateMenu(menuHolder,LoggedUser);
		}		
		private void LoadFromID(int ID)
		{
			string s="";
			MeetingProjectsVector mpv = MeetingProjectDAL.LoadCollection("MeetingProjectsSelByMeetingProc",SQLParms.CreateMeetingProjectsSelByMeetingProc(ID));
			foreach(MeetingProjectData mpd in mpv)
			{
				s+=";";
				s+=mpd.ProjectID.ToString();
			}   
			lbSelectedMeetingProjects.Items.Clear();
			UIHelpers.SetSelected(s, lbMeetingProjects1, lbSelectedMeetingProjects);
			hdnlbSelectedMeetingProjects.Value = s;
			IsMainProjectSelected(true);
			lbMeetingProjects.Items.Clear();
			ListItem li;
			for(int i=0;i<lbMeetingProjects1.Items.Count;i++)
			{
				li = lbMeetingProjects1.Items[i];
				lbMeetingProjects.Items.Add(li);
			}
			hdnlbMeetingProjects.Value = hdnlbMeetingProjects1.Value;
			ReloadListsFromProjects(false);
			RemoveMainProjectFromProjects(true);
			
			MeetingData md = MeetingDAL.Load(ID);
			if(md==null)
				ErrorRedirect("Error load meeting.");

			ddlProject.SelectedValue=md.ProjectID.ToString();
			txtStartDate.Text=TimeHelper.FormatDate(md.MeetingDate);
			if(ddlStartTime.Items.FindByValue(md.StartTimeID.ToString())!=null)
				ddlStartTime.SelectedValue=md.StartTimeID.ToString();
			if(ddlEndTime.Items.FindByValue(md.EndTimeID.ToString())!=null)
				ddlEndTime.SelectedValue=md.EndTimeID.ToString();
			ckApproved.Checked=md.Approved;

			txtPlace.Text=md.Place;
			txtNotes.Text=md.Notes;
			txtOtherPeople.Text=md.OtherPeople;
//				if(md.HasScan)
//				{
//					if(!LoggedUser.IsLeader)
//					{
//							FileCtrl.Visible=false;
//							btnScanDocAdd.Visible=false;
//					}	
//									btnScan.NavigateUrl=
//										string.Concat(System.Configuration.ConfigurationSettings.AppSettings["ScanPath"],ID,System.Configuration.ConfigurationSettings.AppSettings["Extension"]);
//									btnScan.Visible=true;
//				}
			
			lbSelectedClients.Items.Clear();
			lbSelectedUsers.Items.Clear();
			lbSelectedUsersMails.Items.Clear();			
			UIHelpers.SetSelected(md.Clients, lbClients, lbSelectedClients);
			UIHelpers.SetSelected(md.Subcontracters, lbUsers, lbSelectedUsers);
			UIHelpers.SetSelected(md.Users, lbUsersMails, lbSelectedUsersMails);
			hdnlbSelectedClients.Value=md.Clients;
			hdnlbSelectedUsers.Value=md.Subcontracters;
			hdnlbSelectedUsersMails.Value=md.Users;		
			
		}
		#endregion

		#region //ReloadListFromOneProject
//		private void ReloadLists()
//		{
//			int nProjectID=int.Parse(ddlProject.SelectedValue);
//			SubcontractersVector sv=new SubcontractersVector(); 
//			ClientsVector cv = new ClientsVector();
//			if(nProjectID>0)
//			{
//				SubcontractersVector ssv= SubcontracterDAL.LoadCollection();	
//				
//				ProjectSubcontractersVector psv= ProjectSubcontracterDAL.LoadCollection("ProjectSubcontractersListProc",SQLParms.CreateProjectSubcontractersListProc (nProjectID));
//
//				foreach(ProjectSubcontracterData psd in psv)
//				{
//					sv.Add(ssv.GetByID(psd.SubcontracterID));
//				}
//				string projectName, projectCode, administrativeName,add;
//				decimal area=0;
//				
//				int clientID=0;
//				bool phases;
//				if (!ProjectsData.SelectProject(nProjectID, out projectName, out  projectCode, out administrativeName, out area, out add, 
//					out clientID,out phases))
//				{
//					log.Error("Project not found " + nProjectID);
//					return;
//				
//				}
//				ClientData cd = ClientDAL.Load(clientID);
//				if(cd!=null)
//					cv.Add(cd);
//			}
//			else
//			{
//				sv=SubcontracterDAL.LoadCollection();
//				cv=ClientDAL.LoadCollection();
//			}
//			lbUsers.DataSource = sv;
//			lbUsers.DataValueField = "SubcontracterID";
//			lbUsers.DataTextField = "SubcontracterNameAndAll";
//			lbUsers.DataBind();
//			lbClients.DataSource = cv;
//			lbClients.DataValueField = "ClientID";
//			lbClients.DataTextField = "ClientNameAndAll";
//			lbClients.DataBind();
//		}
		#endregion
		
		#region //BindGrid
//		private void BindGrid()
//		{
//			SqlDataReader reader = null;
//
//			try
//			{
//				reader = ClientsData.SelectClients(SortOrder);
//				if (reader!=null)
//				{
//					lbClients.DataSource = reader;
//					lbClients.DataValueField = "ClientID";
//					lbClients.DataTextField = "ClientName";
//					lbClients.DataBind();
//				
//				}
//			}
//			catch (Exception ex)
//			{
//				log.Error(ex);
//				
//				return;
//			}
//
//			finally
//			{
//				if (reader!=null) reader.Close();
//			}
//
//			
//			
//		}
		#endregion
		
		private bool LoadUsers()
		{
			//if (this.LoggedUser.IsLeader)
		{
			

			SqlDataReader reader = null;
			try
			{
				reader = UsersData.SelectUserNames(false, true);
				lbUsersMails.DataSource = reader;
				lbUsersMails.DataValueField = "UserID";
				lbUsersMails.DataTextField = "FullName";
				lbUsersMails.DataBind();
			}
			catch (Exception ex)
			{
				log.Error(ex);
				return false;
			}
			finally
			{
				if (reader!=null) reader.Close();
			}
		}

			return true;
		}
		public ProjectsData.ProjectsByStatus ProjectsStatus
		{
			get
			{
				try
				{
					return  (ProjectsData.ProjectsByStatus)int.Parse(ddlProjectsStatus.SelectedValue);
				}
				catch { return ProjectsData.ProjectsByStatus.AllProjects; }
			}
		}
		private ArrayList LoadTimes()
		{
			ArrayList times = new ArrayList();
		
			SqlDataReader reader = null;
			
			try
			{
				reader = DBManager.SelectTimes();
			
				while (reader.Read())
				{
					TimeInfo ti = new TimeInfo(reader.GetInt32(0), reader.GetString(1));
					times.Add(ti);
				}
			}
			catch (Exception ex)
			{
				log.Error(ex);
				ErrorRedirect("Error load meeting.");
			}
			finally
			{
				if (reader!=null) reader.Close();
			}

			times.Insert(0, new TimeInfo(0, ""));
			return times;
		}
		private bool LoadProjects()
		{
			SqlDataReader reader = null;
			
			try
			{
				reader = ProjectsData.SelectProjectNamesClear(this.ProjectsStatus, int.Parse(ddlBuildingTypes.SelectedValue));
			
				ddlProject.DataSource = reader;
				ddlProject.DataValueField = "ProjectID";
				ddlProject.DataTextField = "ProjectName";
				ddlProject.DataBind();

				ddlProject.Items.Insert(0, new ListItem("<"+Resource.ResourceManager["reports_ddlAllProjects"]+">", "-1"));
				ddlProject.SelectedValue = "-1";
			}
			catch (Exception ex)
			{
				log.Error(ex);
				return false;
			}

			finally
			{
				if (reader!=null) reader.Close();
			}
			return true;
		}

		private bool IsWorkDayEntered(int User, DateTime dt)
		{
			try
			{
				return  WorkTimesData.IsWorkDayEntered(User, 
					dt,
					false);
			}		
			catch (Exception ex)
			{
				log.Error(ex);
				ErrorRedirect("Error.");
				return true;
			}
		}
		private void InitEdit()
		{
			

			if (UIHelpers.GetIDParam()<=0) return;

			string vmButtons = this.btnEdit.ClientID;
			string emButtons = this.btnSave.ClientID+";"+this.btnDelete.ClientID;

			string exl = string.Empty;

			editCtrl.InitEdit("tblForm", vmButtons, emButtons, exl);
		}
		//Show Subcontractors and Clients from selected projects 
		private void ReloadListsFromProjects(bool FromPageLoad)
		{
			LoadMeetingProjectsChanges();
			string[] arr = hdnlbSelectedMeetingProjects.Value.Split(';');
			SubcontractersVector sv=new SubcontractersVector(); 
			ClientsVector cv = new ClientsVector();
			
			if((hdnlbSelectedMeetingProjects.Value!=null)&&(hdnlbSelectedMeetingProjects.Value!=""))
			{
				SubcontractersVector ssv = SubcontracterDAL.LoadCollection();
				ProjectSubcontractersVector psv = new ProjectSubcontractersVector();
				foreach (string s in arr)
				{					
					int nProjectID = UIHelpers.ToInt(s);
					psv = ProjectSubcontracterDAL.LoadCollection("ProjectSubcontractersListProc",SQLParms.CreateProjectSubcontractersListProc (nProjectID));
					foreach(ProjectSubcontracterData psd in psv)
					{
						bool Ispsd = true;
						foreach (SubcontracterData psd1 in sv)
						{
							if (psd.SubcontracterID==psd1.SubcontracterID) Ispsd = false;
						}
						if (Ispsd) sv.Add(ssv.GetByID(psd.SubcontracterID));
					}
				}

				string projectName, projectCode, administrativeName,add;
				decimal area=0;
				int n = arr.Length;
				int[] clientID = new int[n];
				bool phases;
				int i=0;
				foreach (string s in arr)
				{
					int nProjectID = UIHelpers.ToInt(s);
					if (!ProjectsData.SelectProject(nProjectID, out projectName, out  projectCode, out administrativeName, out area, out add, 
						out clientID[i],out phases))
					{
						log.Error("Project not found " + nProjectID);
										
					}
					i++;
				}
				for(i = 0;i<clientID.Length;i++)
				{
					for(int j=i+1;j<clientID.Length;j++)
					{
						if (clientID[i]==clientID[j]) clientID[i]=-1;
					}
				}
				foreach(int index in clientID)
					if(index!=-1) 
					{
						ClientData cd = ClientDAL.Load(index);
						if(cd!=null)
							cv.Add(cd);
					}
			}
			else
			{
				if (FromPageLoad)
				{
					sv=SubcontracterDAL.LoadCollection();
					cv=ClientDAL.LoadCollection();
				}
			}
			lbUsers.DataSource = sv;
			lbUsers.DataValueField = "SubcontracterID";
			lbUsers.DataTextField = "SubcontracterNameAndAll";
			lbUsers.DataBind();
			foreach(ListItem li in lbSelectedUsers.Items)
			{lbUsers.Items.Remove(li);}
			lbClients.DataSource = cv;
			lbClients.DataValueField = "ClientID";
			lbClients.DataTextField = "ClientNameAndAll";
			lbClients.DataBind();
			foreach(ListItem li in lbSelectedClients.Items)
			{lbClients.Items.Remove(li);}
		}
		//5 new, for binding MeetingProjects Database
		private bool LoadMeetingProjects(bool FromPageLoad)
		{
			SqlDataReader reader = null;
			try
			{
				reader = ProjectsData.SelectProjectNamesClear(this.ProjectsStatus, int.Parse(ddlBuildingTypes.SelectedValue));
				lbMeetingProjects.DataSource = reader;
				lbMeetingProjects.DataValueField = "ProjectID";
				lbMeetingProjects.DataTextField = "ProjectName";
				lbMeetingProjects.DataBind();
				if (FromPageLoad)
					LoadMeetingProjects1();
				else
					LoadMeetingProjectsChanges();								
			}
			catch (Exception ex)
			{
				log.Error(ex);
				return false;
			}
			finally
			{
				if (reader!=null) reader.Close();
			}
			return true;
		}
		
		private bool LoadMeetingProjects1()
		{
			SqlDataReader reader = null;
			try
			{
				reader = ProjectsData.SelectProjectNamesClear(0, 0);
				lbMeetingProjects1.DataSource = reader;
				lbMeetingProjects1.DataValueField = "ProjectID";
				lbMeetingProjects1.DataTextField = "ProjectName";
				lbMeetingProjects1.DataBind();	
				
			}
			catch (Exception ex)
			{
				log.Error(ex);
				return false;
			}
			finally
			{
				if (reader!=null) reader.Close();
			}
			return true;
		}
		
		private void LoadMeetingProjectsChanges()
		{
			LoadMeetingProjects1();
			string str = hdnlbSelectedMeetingProjects.Value;
			lbSelectedMeetingProjects.Items.Clear();
			UIHelpers.SetSelected(str, lbMeetingProjects1, lbSelectedMeetingProjects);	
			foreach(ListItem li in lbSelectedMeetingProjects.Items)
			{
				lbMeetingProjects.Items.Remove(li);					
			}
		}
		private void IsMainProjectSelected(bool FromPageLoad)
		{
			if (FromPageLoad)
			{
				MeetingData md=MeetingDAL.Load(UIHelpers.GetIDParam());
				String MainProject = md.ProjectID.ToString();
				bool IsSelected = false;
				string[] arr = hdnlbSelectedMeetingProjects.Value.Split(';');
				foreach(string s in arr)
				{
					if (MainProject == s) IsSelected = true;
				}
				if (!IsSelected)
				{
					hdnlbSelectedMeetingProjects.Value += ";";
					hdnlbSelectedMeetingProjects.Value += MainProject;
					LoadMeetingProjectsChanges();
				}		
				 
			}
			else
			{
				if (ddlProject.SelectedIndex!=0)
				{
					bool IsSelected = false;
					string MainProject = ddlProject.SelectedValue.ToString();
					string[] arr = hdnlbSelectedMeetingProjects.Value.Split(';');
					foreach(string s in arr)
					{
						if (MainProject == s) IsSelected = true;
					}
					if (!IsSelected)
					{
						hdnlbSelectedMeetingProjects.Value += ";";
						hdnlbSelectedMeetingProjects.Value += MainProject;
						LoadMeetingProjectsChanges();
					}				
				}
			}
		}

		private void RemoveMainProjectFromProjects(bool FromPageLoad)
		{
			if (FromPageLoad)
			{
				MeetingData md=MeetingDAL.Load(UIHelpers.GetIDParam());
				string MainProjectID = md.ProjectID.ToString();
				ListItem li = lbSelectedMeetingProjects.Items.FindByValue(MainProjectID);
				lbSelectedMeetingProjects.Items.Remove(li);
				
			}
			else 
			{
				if (ddlProject.SelectedIndex!=0)
				{	
					string MainProjectID = ddlProject.SelectedValue.ToString();
					ListItem li = lbSelectedMeetingProjects.Items.FindByValue(MainProjectID);
					lbSelectedMeetingProjects.Items.Remove(li);
				}
			}
		}

		//4 new,For more ScanDocuments
		private void BindGrid()
		{
			int ID = UIHelpers.GetIDParam();
			MeetingDocumentsVector mdv = MeetingDocumentDAL.LoadCollection("MeetingDocumentsSelByMeetingProc",SQLParms.CreateMeetingDocumentsSelByMeetingProc(ID));
			if (!(mdv.Count>0))
			{
				MeetingData md = MeetingDAL.Load(ID);
				md.HasScan = false;
				MeetingDAL.Save(md);
			}
			dlDocs.DataSource = mdv;
			dlDocs.DataKeyField="MeetingDocument";
			dlDocs.DataBind();
			
//			if (!LoggedUser.HasPaymentRights)
////				grdMeetings.Columns[(int)GridColumns.DeleteItem].Visible = false;
//				dlDocs.GridLines[GridLines.Horizontal].Visible = false;
			try
			{
				for (int i=0; i<dlDocs.Items.Count; i++)
				{
					string path = string.Concat(ID,System.Configuration.ConfigurationSettings.AppSettings["Extension"]);
					string js = string.Concat("return confirm('",Resource.ResourceManager["grid_ConfirmDelete"]," ",path,"?');");
					System.Web.UI.WebControls.WebControl btn = (System.Web.UI.WebControls.WebControl)dlDocs.Items[i].FindControl("btnDel");
					btn.Attributes["onclick"] = js;
				}
			}
			catch (Exception ex)
			{
				
			}
			
		}
		public void SaveMeetingDocuments()
		{
			
			int ID = UIHelpers.GetIDParam();
			if(FileCtrl.PostedFile.FileName.Length!=0)
			{
				string name = FileCtrl.PostedFile.FileName;
				int n = name.LastIndexOf(".");
				if(n!=-1)
				{
					string ext = name.Substring(n);
					if(ext==System.Configuration.ConfigurationSettings.AppSettings["Extension"])
					{					
						MeetingDocumentData mdd = new MeetingDocumentData(-1,ID,false);
						MeetingDocumentDAL.Save(mdd);
						string path = string.Concat(System.Configuration.ConfigurationSettings.AppSettings["ScanPath"],mdd.MeetingDocument,ext);
						string mapped = Request.MapPath(path);
						FileCtrl.PostedFile.SaveAs(mapped);											
						MeetingData md=MeetingDAL.Load(ID);
						md.HasScan=true;
						MeetingDAL.Save(md);
						//Check if the Director is on the meeting
						UserInfo Director = UsersData.UsersListByRoleProc(1);
						
						bool IsDirectorThere =false;
						string[] arr = hdnlbSelectedUsersMails.Value.Split(';');
						foreach(string s in arr)
						{
							int currentID = UIHelpers.ToInt(s);
							if(currentID == Director.UserID)
								IsDirectorThere = true;
						}
						if(!IsDirectorThere)
						{hdnlbSelectedUsersMails.Value = string.Concat(hdnlbSelectedUsersMails.Value,";",Director.UserID);IsDirectorThere= false;}
						MailBO.SendMailMinutes(hdnlbSelectedUsersMails.Value, mapped,ddlProject.SelectedItem.Text,txtStartDate.Text,ddlStartTime.SelectedValue, ddlEndTime.SelectedValue, hdnlbSelectedUsers.Value,hdnlbSelectedClients.Value,txtPlace.Text, txtOtherPeople.Text,-1,txtNotes.Text,TimeHelper.FormatDate(DateTime.Parse(txtStartDate.Text)));
						if(!IsDirectorThere)
						{
							int index = hdnlbSelectedUsersMails.Value.LastIndexOf(";");
							hdnlbSelectedUsersMails.Value = hdnlbSelectedUsersMails.Value.Substring(0,index);
						}
					}
				}
				
			}
		}
		protected string GetURL(int ID)
		{
			return string.Concat(System.Configuration.ConfigurationSettings.AppSettings["ScanPath"],ID,System.Configuration.ConfigurationSettings.AppSettings["Extension"]);
		}
		
		protected bool GetVis()
		{
			if (LoggedUser.HasPaymentRights)
				return true;
			else
				return false;
		}
			
		protected string WhoGoToMeeting()
		{	
			string stringAll = string.Empty;
			string[] arr = hdnlbSelectedUsersMails.Value.Split(';');
			for(int i=0;i<arr.Length;i++)
			{
				int Nomer = UIHelpers.ToInt(arr[i]);
				if(Nomer!=-1)
				{
					UserInfo ui = UsersData.SelectUserByID(Nomer);
					string New = ui.FullName;
					stringAll = string.Concat(stringAll,New,';');
				}
			}
			string[] arr1 = hdnlbSelectedUsers.Value.Split(';');
			for(int i=0;i<arr1.Length;i++)
			{
				int Nomer = UIHelpers.ToInt(arr1[i]);
				if(Nomer!=-1)
				{
					SubcontracterData sd = SubcontracterDAL.Load(Nomer);
					string New = sd.SubcontracterName;
					stringAll = string.Concat(stringAll,New,';');
				}
			}
			string[] arr2 = hdnlbSelectedClients.Value.Split(';');
			for(int i=0;i<arr2.Length;i++)
			{
				int Nomer = UIHelpers.ToInt(arr2[i]);
				if(Nomer!=-1)
				{
					ClientData cd = ClientDAL.Load(Nomer);
					string New = cd.ClientName;
					stringAll = string.Concat(stringAll,New,';');
				}
			}
			int index = stringAll.Length - 1;
			return stringAll.Substring(0,index);
		}

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
			this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
			this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
			this.ddlBuildingTypes.SelectedIndexChanged += new System.EventHandler(this.ddlBuildingTypes_SelectedIndexChanged);
			this.ddlProjectsStatus.SelectedIndexChanged += new System.EventHandler(this.ddlProjectsStatus_SelectedIndexChanged);
			this.ddlProject.SelectedIndexChanged += new System.EventHandler(this.ddlProject_SelectedIndexChanged);
			this.btnReload.Click += new System.EventHandler(this.btnReload_Click);
			this.dlDocs.ItemCommand += new System.Web.UI.WebControls.DataListCommandEventHandler(this.dlDocs_ItemCommand);
			this.btnScanDocAdd.Click += new System.EventHandler(this.btnScanDocAdd_Click);
			this.Load += new System.EventHandler(this.Page_Load);

		}
		#endregion

		#region Event handlers
		private void ddlProjectsStatus_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			LoadProjects();			
			LoadMeetingProjects(false);
			IsMainProjectSelected(false);
			RemoveMainProjectFromProjects(false);
		}

		private void ddlBuildingTypes_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			LoadProjects();		
			LoadMeetingProjects(false);
			IsMainProjectSelected(false);
			RemoveMainProjectFromProjects(false);
		}

		private void btnSave_Click(object sender, System.EventArgs e)
		{
			if (ddlProject.SelectedIndex==0) 
			{
				AlertFieldNotEntered(lblName);
				Dirty=true;
				return;
			}
			
			if (txtStartDate.Text.Trim()=="" ||UIHelpers.GetDate(txtStartDate)==Constants.DateMax) 
			{
				AlertFieldNotEntered(lbDate);
				Dirty=true;
				return;
			}

			
			if (ddlStartTime.SelectedIndex==0) 
			{
				AlertFieldNotEntered(lbStart);
				Dirty=true;
				return;
			}
			if (txtPlace.Text=="") 
			{
				AlertFieldNotEntered(lbPlace);
				Dirty=true;
				return;
			}
			if (ddlEndTime.SelectedIndex==0 || ddlStartTime.SelectedIndex>=ddlEndTime.SelectedIndex) 
			{
				AlertFieldNotEntered(lbEnd);
				Dirty=true;
				return;
			}
			string stringNames = WhoGoToMeeting();
			int ID = UIHelpers.GetIDParam();
			bool bAprovedPrev=false;
			bool bInsert=ID<=0;
			bool bDelete = false;
			bool bHasScanBefore = false;
			if(ID>0)
			{
				MeetingData mdd=MeetingDAL.Load(ID);
				bAprovedPrev=mdd.Approved;
				bHasScanBefore=mdd.HasScan;
				if(UIHelpers.GetDate(txtStartDate)!=mdd.MeetingDate
					|| int.Parse(ddlStartTime.SelectedValue)!=mdd.StartTimeID
					|| int.Parse(ddlEndTime.SelectedValue)!=mdd.EndTimeID
					|| mdd.Users!=hdnlbSelectedUsersMails.Value)
				{
					bInsert=true;
					bDelete=true;
				}
			}
			DateTime dtMeet=UIHelpers.GetDate(txtStartDate);
			
			MeetingData md = new MeetingData(ID,dtMeet,int.Parse(ddlProject.SelectedValue),-1,
				int.Parse(ddlStartTime.SelectedValue),int.Parse(ddlEndTime.SelectedValue),
				hdnlbSelectedClients.Value,hdnlbSelectedUsers.Value,
				hdnlbSelectedUsersMails.Value,LoggedUser.UserID,ckApproved.Checked,LoggedUser.UserID);
			
			md.HasScan=bHasScanBefore;
			md.Place=txtPlace.Text;
			md.Notes=txtNotes.Text;
			md.OtherPeople=txtOtherPeople.Text;
			MeetingDAL.Save(md);
			//MeetingProjects , save
			ID = md.MeetingID;
			string[] arr = hdnlbSelectedMeetingProjects.Value.Split(';');
			MeetingProjectUDL.ExecuteMeetingProjectsDelByMeetingProc(ID);
			foreach(string s in arr)
			{

				int nID=UIHelpers.ToInt(s);
				if(nID>0)
				{
					MeetingProjectData mpd = new MeetingProjectData(-1,ID,nID);
					MeetingProjectDAL.Save(mpd);
				}
			}
			//end
			if(bDelete)
			{
				WorkTimesData.ExecuteWorkTimesDelByMeetingProc(md.MeetingID);
			}
			if(ckApproved.Checked)
			{
				string [] susers=hdnlbSelectedUsersMails.Value.Split(';');
				foreach(string s in susers)
				{
					if(s!=null && s.Length>0)
					{
						try
						{
							int i = int.Parse(s);
							if(!IsWorkDayEntered(i,dtMeet))
							{
								WorkTimesData.InsertWorkTime(null,dtMeet,i,int.Parse(ddlProject.SelectedValue),
									int.Parse(System.Configuration.ConfigurationSettings.AppSettings["MeetingID"]),
									int.Parse(ddlStartTime.SelectedValue),int.Parse(ddlEndTime.SelectedValue),"","",
									DateTime.Now,LoggedUser.UserID,md.MeetingID);

							}
						}
						catch{}
					}
				}
			}
			#region Save When There Was One Meeting Document
//			if(FileCtrl.PostedFile.FileName!=null
//				&&FileCtrl.PostedFile.FileName!="")
//			{
//				
//			
//				string name = FileCtrl.PostedFile.FileName;
//				int n = name.LastIndexOf(".");
//				if(n!=-1)
//				{
//					string ext = name.Substring(n);
//					if(ext==System.Configuration.ConfigurationSettings.AppSettings["Extension"])
//					{
//						string path = string.Concat(System.Configuration.ConfigurationSettings.AppSettings["ScanPath"],md.MeetingID,ext);
//						string mapped = Request.MapPath(path);
//
//						FileCtrl.PostedFile.SaveAs(mapped);
//						md.HasScan=true;
//						MeetingDAL.Save(md);
//						md=MeetingDAL.Load(md.MeetingID);
//						//if(!bHasScanBefore)
//						{
//							if(hdnlbSelectedUsersMails.Value!="" || hdnlbSelectedClients.Value!="" || hdnlbSelectedUsers.Value!="")
//								MailBO.SendMailMinutes(hdnlbSelectedUsersMails.Value, mapped,md.ProjectName, md.MeetingDate.ToShortDateString(),md.StartHour, md.EndHour, hdnlbSelectedUsers.Value,hdnlbSelectedClients.Value,txtPlace.Text, txtOtherPeople.Text,md.ClientID,txtNotes.Text,TimeHelper.FormatDate(md.MeetingDate));
//
//						}
//					}
//				}
//			}
			#endregion
			md=MeetingDAL.Load(md.MeetingID);
			PdfExport pdf = new PdfExport();
			pdf.NeverEmbedFonts = "";
			bool bClient= hdnlbSelectedClients.Value!="";
			bool bSub= hdnlbSelectedUsers.Value!="";			
			DataDynamics.ActiveReports.ActiveReport report =new rptTefter(md.ProjectName, md.MeetingDate.ToShortDateString(),bClient,bSub,stringNames);
			string path1 = string.Concat(System.Configuration.ConfigurationSettings.AppSettings["ScanPath"],"NoteBook",md.MeetingID,".pdf");
			string mapped1 = Request.MapPath(path1);
			report.Run();
			pdf.Export(report.Document,mapped1);
			if(bAprovedPrev==false && ckApproved.Checked)
			{
				if(hdnlbSelectedUsersMails.Value!="" || hdnlbSelectedClients.Value!="" || hdnlbSelectedUsers.Value!="")
					if(bInsert)
					{
						MailBO.SendMailFirst(hdnlbSelectedUsersMails.Value, md.ProjectName, md.MeetingDate.ToShortDateString(),md.StartHour, md.EndHour, hdnlbSelectedUsers.Value,hdnlbSelectedClients.Value,txtPlace.Text, txtOtherPeople.Text,md.ClientID,txtNotes.Text,TimeHelper.FormatDate(md.MeetingDate));
						MailBO.SendMailApproved(hdnlbSelectedUsersMails.Value, mapped1,md.ProjectName, md.MeetingDate.ToShortDateString(),md.StartHour, md.EndHour, hdnlbSelectedUsers.Value,hdnlbSelectedClients.Value,txtPlace.Text, txtOtherPeople.Text,md.ClientID,txtNotes.Text,TimeHelper.FormatDate(md.MeetingDate));
					}
					else
						MailBO.SendMailApproved(hdnlbSelectedUsersMails.Value, mapped1,md.ProjectName, md.MeetingDate.ToShortDateString(),md.StartHour, md.EndHour, hdnlbSelectedUsers.Value,hdnlbSelectedClients.Value,txtPlace.Text, txtOtherPeople.Text,md.ClientID,txtNotes.Text,TimeHelper.FormatDate(md.MeetingDate));
			}
			if(bInsert && !ckApproved.Checked)
			{
				MailBO.SendMailOfficer(md.ProjectName, TimeHelper.FormatDate(md.MeetingDate),md.StartHour, md.EndHour);
				if(hdnlbSelectedUsersMails.Value!="" || hdnlbSelectedClients.Value!="" || hdnlbSelectedUsers.Value!="")
					MailBO.SendMailFirst(hdnlbSelectedUsersMails.Value, md.ProjectName, md.MeetingDate.ToShortDateString(),md.StartHour, md.EndHour, hdnlbSelectedUsers.Value,hdnlbSelectedClients.Value,txtPlace.Text, txtOtherPeople.Text,md.ClientID,txtNotes.Text,TimeHelper.FormatDate(md.MeetingDate));

			}
			if(FileCtrl.PostedFile.FileName.Length!=0)
			{
				string name = FileCtrl.PostedFile.FileName;
				int n = name.LastIndexOf(".");
				if(n!=-1)
				{
					string ext = name.Substring(n);
					if(ext==System.Configuration.ConfigurationSettings.AppSettings["Extension"])
					{					
						MeetingDocumentData mdd = new MeetingDocumentData(-1,md.MeetingID,false);
						MeetingDocumentDAL.Save(mdd);
						string path = string.Concat(System.Configuration.ConfigurationSettings.AppSettings["ScanPath"],mdd.MeetingDocument,ext);
						string mapped = Request.MapPath(path);
						FileCtrl.PostedFile.SaveAs(mapped);											
						md.HasScan=true;
						MeetingDAL.Save(md);			
						if(hdnlbSelectedUsersMails.Value!="" || hdnlbSelectedClients.Value!="" || hdnlbSelectedUsers.Value!="")
						{
							//Check if the Director is on the meeting
							UserInfo Director = UsersData.UsersListByRoleProc(1);
						
							bool IsDirectorThere =false;
							string[] arr1 = hdnlbSelectedUsersMails.Value.Split(';');
							foreach(string s in arr1)
							{
								int currentID = UIHelpers.ToInt(s);
								if(currentID == Director.UserID)
									IsDirectorThere = true;
							}
							if(!IsDirectorThere)
							{hdnlbSelectedUsersMails.Value = string.Concat(hdnlbSelectedUsersMails.Value,";",Director.UserID);IsDirectorThere= false;}
							MailBO.SendMailMinutes(hdnlbSelectedUsersMails.Value, mapped,md.ProjectName, md.MeetingDate.ToShortDateString(),md.StartHour, md.EndHour, hdnlbSelectedUsers.Value,hdnlbSelectedClients.Value,txtPlace.Text, txtOtherPeople.Text,md.ClientID,txtNotes.Text,TimeHelper.FormatDate(md.MeetingDate));
							if(!IsDirectorThere)
							{
								int index = hdnlbSelectedUsersMails.Value.LastIndexOf(";");
								hdnlbSelectedUsersMails.Value = hdnlbSelectedUsersMails.Value.Substring(0,index);
							}
						}
					}
				}
			}
			

			Response.Redirect("Meetings.aspx");

		}
		
		private void btnCancel_Click(object sender, System.EventArgs e)
		{
			Response.Redirect("Meetings.aspx");
		}
		
		private void ddlProject_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			LoadMeetingProjects(false);
			IsMainProjectSelected(false);
			ReloadListsFromProjects(false);
			RemoveMainProjectFromProjects(false);
		}
	
		private void btnReload_Click(object sender, System.EventArgs e)
		{
			LoadMeetingProjects(false);
			IsMainProjectSelected(false);
			ReloadListsFromProjects(false);
			RemoveMainProjectFromProjects(false);
		}

		private void dlDocs_ItemCommand(object source, System.Web.UI.WebControls.DataListCommandEventArgs e)
		{
			ImageButton ib = (ImageButton)e.CommandSource;
			string s = ib.ID;
			switch (ib.ID)
			{
				case "btnDel": 
					int ID = (int)dlDocs.DataKeys[e.Item.ItemIndex];			
					MeetingDocumentData mdd = MeetingDocumentDAL.Load(ID);
					mdd.IsDeleted = true;
					MeetingDocumentDAL.Save(mdd);
					BindGrid();
					LoadMeetingProjects(false);
					IsMainProjectSelected(false);
					ReloadListsFromProjects(false);
					RemoveMainProjectFromProjects(false);
					break;
			}
		}

		private void btnScanDocAdd_Click(object sender, System.EventArgs e)
		{
			SaveMeetingDocuments();
			BindGrid();
			LoadMeetingProjects(false);
			IsMainProjectSelected(false);
			ReloadListsFromProjects(false);
			RemoveMainProjectFromProjects(false);
		}
	
		#endregion
	}
}
