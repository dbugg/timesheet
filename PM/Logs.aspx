﻿<%@ Page Title="Логове" Language="C#" MasterPageFile="~/PM.Master" AutoEventWireup="True" CodeBehind="Logs.aspx.cs" Inherits="PM.Logs" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
<table height="100%" cellSpacing="0" cellPadding="0" width="100%" border="0">
    <tr>
					<td>
						<table height="100%" cellSpacing="0" cellPadding="0" width="100%" border="0">
							<tr>
								<td style="PADDING-RIGHT: 5px; PADDING-LEFT: 5px; PADDING-TOP: 3px" vAlign="top" noWrap>
									<TABLE id="Table1" style="WIDTH: 558px; HEIGHT: 26px" cellSpacing="1" cellPadding="1" width="558"
										border="0">
										<TR>
											<td style="WIDTH: 223px"><asp:label id="lbProjectType" runat="server" EnableViewState="False" CssClass="enterDataLabel"
													Width="222px" Font-Bold="True"></asp:label></TD>
											<td style="WIDTH: 335px"><asp:DropDownList id="ddlProjectsStatus" runat="server" CssClass="EnterDataBox" Width="250px" AutoPostBack="True"
													DESIGNTIMEDRAGDROP="46" onselectedindexchanged="ddlProjectsStatus_SelectedIndexChanged"></asp:DropDownList>
											</TD>
										</TR>
										<TR>
											<td style="WIDTH: 223px"><asp:label id="lbBuildingType" runat="server" EnableViewState="False" CssClass="enterDataLabel"
													Width="222px" Font-Bold="True"></asp:label></TD>
											<td style="WIDTH: 335px"><asp:DropDownList id="ddlBuildingTypes" runat="server" CssClass="enterDataBox" Width="250px" AutoPostBack="True" onselectedindexchanged="ddlBuildingTypes_SelectedIndexChanged"></asp:DropDownList>
											</TD>
										</TR>
										<TR>
											<td style="WIDTH: 223px"><asp:label id="lblName" runat="server" EnableViewState="False" CssClass="enterDataLabel" Width="222px"
													Font-Bold="True"></asp:label></TD>
											<td style="WIDTH: 335px"><asp:DropDownList ID="ddlProjects" runat="server" CssClass="enterDataBox" Width="250px" AutoPostBack="True" onselectedindexchanged="ddlProjects_SelectedIndexChanged"></asp:DropDownList>
											</TD>
										</TR>
										<TR>
											<td style="WIDTH: 223px"><asp:label id="Label1" runat="server" EnableViewState="False" CssClass="enterDataLabel" Width="222px"
													Font-Bold="True"></asp:label></TD>
											<td style="WIDTH: 335px"><asp:DropDownList ID="ddlCategories" runat="server" CssClass="enterDataBox" Width="250px" AutoPostBack="True" onselectedindexchanged="ddlCategories_SelectedIndexChanged"></asp:DropDownList>
											</TD>
										</TR>
										<TR>
											<td style="WIDTH: 223px"><asp:label id="Label2" runat="server" EnableViewState="False" CssClass="enterDataLabel" Width="222px"
													Font-Bold="True"></asp:label></TD>
											<td style="WIDTH: 335px"><asp:DropDownList ID="ddlFiles" runat="server" CssClass="enterDataBox" Width="250px"></asp:DropDownList>
											</TD>
										</TR>
										<TR>
											<td style="WIDTH: 223px">
												<asp:label id="lbUser" runat="server" Font-Bold="True" Width="222px" CssClass="enterDataLabel"
													EnableViewState="False"></asp:label></TD>
											<td style="WIDTH: 335px">
												<asp:DropDownList id="ddlUser" runat="server" Width="250px" CssClass="enterDataBox"></asp:DropDownList></TD>
										</TR>
										<TR>
											<td style="WIDTH: 223px">
												<asp:label id="lbType" runat="server" Font-Bold="True" Width="222px" CssClass="enterDataLabel"
													EnableViewState="False"></asp:label></TD>
											<td style="WIDTH: 335px">
												<asp:DropDownList id="ddlType" runat="server" Width="250px" CssClass="enterDataBox"></asp:DropDownList></TD>
										</TR>
										<TR>
											<td style="WIDTH: 223px"></TD>
											<td style="WIDTH: 335px">
												<asp:button id="btnSearch" runat="server" CssClass="ActionButton" onclick="btnSearch_Click"></asp:button>
												<asp:imagebutton id="ibPdfExport" runat="server" ToolTip="Справка в PDF формат" ImageUrl="images/pdf1.gif" onclick="ibPdfExport_Click"></asp:imagebutton>
												<asp:imagebutton id="ibXlsExport" runat="server" ToolTip="Справка в Excel формат" ImageUrl="images/xls.gif" onclick="ibXlsExport_Click"></asp:imagebutton>
											</TD>
										</TR>
									</TABLE>
									<P></P>
									<asp:label id="lblError" runat="server" CssClass="ErrorLabel" EnableViewState="False" ForeColor="Red"></asp:label><asp:label id="lblInfo" runat="server" CssClass="InfoLabel" EnableViewState="False"></asp:label><BR>
									<asp:panel id="grid" 
										runat="server" Width="98%" Height="544px">
										<asp:datagrid id="grdLogs" runat="server" Width="100%" CssClass="Grid" AllowSorting="True" AutoGenerateColumns="False"
											CellPadding="4" PageSize="20" AllowPaging="True">
											<ItemStyle CssClass="GridItem"></ItemStyle>
											<HeaderStyle Wrap="False" CssClass="GridHeader"></HeaderStyle>
											<Columns>
												<asp:BoundColumn DataField="ActionDate" SortExpression="ActionDate" DataFormatString="{0:dd.MM.yyyy HH:mm}"></asp:BoundColumn>
												<asp:BoundColumn DataField="CreateOrDownload" SortExpression="CreateOrDownload"></asp:BoundColumn>
												<asp:BoundColumn DataField="ActionAccount" SortExpression="ActionAccount"></asp:BoundColumn>
												<asp:BoundColumn DataField="FileSubject" SortExpression="FileSubject"></asp:BoundColumn>
												<asp:BoundColumn DataField="FileMinutes" SortExpression="FileMinutes"></asp:BoundColumn>
												<asp:BoundColumn DataField="CreatedDate" SortExpression="CreatedDate"></asp:BoundColumn>
												<asp:BoundColumn DataField="CategoryName" SortExpression="CategoryName"></asp:BoundColumn>
												<asp:BoundColumn DataField="ProjectName" SortExpression="ProjectName"></asp:BoundColumn>
												<asp:TemplateColumn ItemStyle-Width="40%">
													<ItemTemplate>
														<asp:LinkButton id="lnk" runat="server" CssClass="menuTable" ForeColor="#804000" CommandName="Edit"></asp:LinkButton>
													</ItemTemplate>
												</asp:TemplateColumn>
												<asp:BoundColumn DataField="FileID" Visible="False"></asp:BoundColumn>
												<asp:BoundColumn DataField="Deleted" Visible="False"></asp:BoundColumn>
											</Columns>
											<PagerStyle HorizontalAlign="Left" CssClass="PagingClass" Mode="NumericPages"></PagerStyle>
										</asp:datagrid>
									</asp:panel>
									<P></P>
								</td>
							</tr>
						</table>
					</td>
				</tr>
</table>
</asp:Content>