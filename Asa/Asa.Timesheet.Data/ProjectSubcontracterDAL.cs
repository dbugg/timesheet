using System;
using System.Data;
using System.Data.SqlClient; 
using System.Collections;
using Microsoft.ApplicationBlocks.Data;

namespace Asa.Timesheet.Data
{
	public class ProjectSubcontracterDAL
	{
		/// <summary>
		/// Loads a ProjectSubcontracterData object from the database using the given ProjectSubcontracterID
		/// </summary>
		/// <param name="projectSubcontracterID">The primary key value</param>
		/// <returns>A ProjectSubcontracterData object</returns>
		public static ProjectSubcontracterData Load(int projectSubcontracterID)
		{
			SqlParameter[] parameterValues = new SqlParameter[] { new SqlParameter("@ProjectSubcontracterID", projectSubcontracterID) };
			using (SqlDataReader reader = SqlHelper.ExecuteReader(CommonDAL.ConnectionString, CommandType.StoredProcedure, "ProjectSubcontractersSelProc", parameterValues))
			{
				if (reader.Read())
				{
					ProjectSubcontracterData result = new ProjectSubcontracterData();
					LoadFromReader(reader,result);
					return result;
				}
				else
					return null;
			}
		}

		/// <summary>
		/// Loads a ProjectSubcontracterData object from the database using the given where clause
		/// </summary>
		/// <param name="whereClause">The filter expression for the query</param>
		/// <returns>A ProjectSubcontracterData object</returns>
		public static ProjectSubcontracterData Load(string whereClause)
		{
			SqlParameter[] parameterValues = new SqlParameter[] { new SqlParameter("@WhereClause", whereClause) };
			using (SqlDataReader reader = SqlHelper.ExecuteReader(CommonDAL.ConnectionString, CommandType.StoredProcedure, "ProjectSubcontractersListProc", parameterValues))
			{
				if (reader.Read())
				{
					ProjectSubcontracterData result = new ProjectSubcontracterData();
					LoadFromReader(reader,result);
					return result;
				}
				else
					return null;
			}
		}
		public static int Save( ProjectSubcontracterData source)
		{
			if (source.ProjectSubcontracterID == -1)
				return Insert(source);
			else
				return Update(source);	
		}

		public static void Delete( ProjectSubcontracterData source)
		{
			Delete(source.ProjectSubcontracterID);
		}
		/// <summary>
		/// Loads a collection of ProjectSubcontracterData objects from the database.
		/// </summary>
		/// <returns>A collection containing all of the ProjectSubcontracterData objects in the database.</returns>
		public static ProjectSubcontractersVector LoadCollection()
		{
			SqlParameter[] parms = new SqlParameter[] {};
			return LoadCollection("ProjectSubcontractersListProc", parms);
		}
	  
		/// <summary>
		/// Saves a collection of ProjectSubcontracterData objects to the database.
		/// </summary>
		/// <returns>A collection containing all of the ProjectSubcontracterData objects in the database.</returns>
		public static  void  SaveCollection(ProjectSubcontractersVector col,string spName, SqlParameter[] parms)
		{
			ProjectSubcontractersVector colOld = LoadCollection(spName,parms);
			UpdateCollection(colOld,col);
		}
		public static  void  SaveCollection(ProjectSubcontractersVector col)
		{
			ProjectSubcontractersVector colOld = LoadCollection();
			UpdateCollection(colOld,col);
		}
		public static  void  SaveCollection(ProjectSubcontractersVector col, string whereClause )
		{
			ProjectSubcontractersVector colOld = LoadCollection(whereClause);
			UpdateCollection(colOld,col);
		}
		/// <summary>
		/// Loads a collection of ProjectSubcontracterData objects from the database.
		/// </summary>
		/// <returns>A collection containing all of the ProjectSubcontracterData objects in the database.</returns>
		public static ProjectSubcontractersVector LoadCollection(string whereClause)
		{
			SqlParameter[] parms = new SqlParameter[] { new SqlParameter("@WhereClause", whereClause) };
			return LoadCollection("ProjectSubcontractersListProc", parms);
		}

		/// <summary>
		/// Loads a collection of ProjectSubcontracterData objects from the database.
		/// </summary>
		/// <returns>A collection containing all of the ProjectSubcontracterData objects in the database.</returns>
		public static ProjectSubcontractersVector LoadCollection(string spName, SqlParameter[] parms)
		{
			ProjectSubcontractersVector result = new ProjectSubcontractersVector();
			using (SqlDataReader reader = SqlHelper.ExecuteReader(CommonDAL.ConnectionString, CommandType.StoredProcedure, spName, parms))
			{
				while (reader.Read())
				{
					ProjectSubcontracterData tmp = new ProjectSubcontracterData();
					LoadFromReader(reader, tmp);
					result.Add(tmp);
				}
			}
			return result;
		}
		/// <summary>
		/// Deletes a ProjectSubcontracterData object from the database.
		/// </summary>
		/// <param name="projectSubcontracterID">The primary key value</param>
		public static void Delete(int projectSubcontracterID)
		{
			SqlParameter[] parameterValues = new SqlParameter[] { new SqlParameter("@ProjectSubcontracterID", projectSubcontracterID) };
			SqlHelper.ExecuteNonQuery(CommonDAL.ConnectionString, CommandType.StoredProcedure, "ProjectSubcontractersDelProc", parameterValues);
		}

		public static void LoadFromReader(SqlDataReader reader, ProjectSubcontracterData target)
		{
			if (reader != null && !reader.IsClosed)
			{
				//target.Key = reader.GetInt32(0);
				if (!reader.IsDBNull(0)) target.ProjectSubcontracterID = reader.GetInt32(0);
				if (!reader.IsDBNull(1)) target.ProjectID = reader.GetInt32(1);
				if (!reader.IsDBNull(2)) target.SubcontracterID = reader.GetInt32(2);
				if (!reader.IsDBNull(3)) target.PaymentSchemeID = reader.GetInt32(3);
				if (!reader.IsDBNull(4)) target.Area = reader.GetDecimal(4);
				if (!reader.IsDBNull(5)) target.Rate = reader.GetDecimal(5);
				if (!reader.IsDBNull(6)) target.TotalAmount = reader.GetDecimal(6);
				if (!reader.IsDBNull(7)) target.StartDate = reader.GetDateTime(7);
				if (!reader.IsDBNull(8)) target.EndDate = reader.GetDateTime(8);
				if (!reader.IsDBNull(9)) target.Note = reader.GetString(9);
				if (!reader.IsDBNull(10)) target.Done = reader.GetBoolean(10);
				if (!reader.IsDBNull(11)) target.FoldersGiven = reader.GetInt32(11);
				if (!reader.IsDBNull(12)) target.FordersArchive = reader.GetInt32(12);
				if (!reader.IsDBNull(13)) target.SubcontracterType = reader.GetString(13);
				if (!reader.IsDBNull(14)) target.SubcontracterName = reader.GetString(14);
				if (!reader.IsDBNull(15)) target.SubprojectID = reader.GetInt32(15);
				
			}
		}
		
	  
		public static int Insert( ProjectSubcontracterData source)
		{
			SqlParameter[] parameterValues = GetInsertParameterValues(source);
			SqlHelper.ExecuteNonQuery(CommonDAL.ConnectionString, CommandType.StoredProcedure, "ProjectSubcontractersInsProc", parameterValues);
			source.ProjectSubcontracterID = (int) parameterValues[0].Value;
			return source.ProjectSubcontracterID;
		}

		public static int Update( ProjectSubcontracterData source)
		{
			SqlParameter[] parameterValues = GetUpdateParameterValues(source);
			SqlHelper.ExecuteNonQuery(CommonDAL.ConnectionString, CommandType.StoredProcedure,
				"ProjectSubcontractersUpdProc", parameterValues);
			return source.ProjectSubcontracterID;
		}
		private static void UpdateCollection(ProjectSubcontractersVector colOld,ProjectSubcontractersVector col)
		{
			// Delete all old items
			foreach( ProjectSubcontracterData itemOld in colOld)
			{
				if(!ExistsKey(itemOld.ProjectSubcontracterID, col))
					Delete(itemOld.ProjectSubcontracterID);
			}
			foreach( ProjectSubcontracterData item in col)
			{
				if(item.ProjectSubcontracterID <= -1)
					Insert(item);
				else
					Update(item);
			}
		}
		private static bool ExistsKey(int Key, ProjectSubcontractersVector col)
		{
			foreach( ProjectSubcontracterData item in col)
				if(item.ProjectSubcontracterID==Key)
					return true;
			return false;
		}

		private static SqlParameter GetSqlParameter(string name, ParameterDirection direction, object value)
		{
			SqlParameter p = new SqlParameter(name, value);
			p.Direction = direction;
			return p;
		}

		private static SqlParameter[] GetInsertParameterValues( ProjectSubcontracterData source)
		{
			SqlParameter[] prams = new SqlParameter[14];
			prams[0] = GetSqlParameter("@ProjectSubcontracterID", ParameterDirection.Output, source.ProjectSubcontracterID);
			prams[1] = GetSqlParameter("@ProjectID", ParameterDirection.Input, source.ProjectID);
			prams[2] = GetSqlParameter("@SubcontracterID", ParameterDirection.Input, source.SubcontracterID);
			prams[3] = GetSqlParameter("@PaymentSchemeID", ParameterDirection.Input, source.PaymentSchemeID);
			prams[4] = GetSqlParameter("@Area", ParameterDirection.Input, source.Area);
			prams[5] = GetSqlParameter("@Rate", ParameterDirection.Input, source.Rate);
			prams[6] = GetSqlParameter("@TotalAmount", ParameterDirection.Input, source.TotalAmount);
			if(source.StartDate!=Constants.DateMax && source.StartDate!=new DateTime())
				prams[7] = GetSqlParameter("@StartDate", ParameterDirection.Input, source.StartDate);
			else
				prams[7] = GetSqlParameter("@StartDate", ParameterDirection.Input, DBNull.Value);
			if(source.EndDate!=Constants.DateMax && source.EndDate!=new DateTime())
				prams[8] = GetSqlParameter("@EndDate", ParameterDirection.Input, source.EndDate);
			else
				prams[8] = GetSqlParameter("@EndDate", ParameterDirection.Input, DBNull.Value);

			prams[9] = GetSqlParameter("@Note", ParameterDirection.Input, source.Note);
			prams[10] = GetSqlParameter("@Done", ParameterDirection.Input, source.Done);
			if(source.FoldersGiven>0)
				prams[11] = GetSqlParameter("@FoldersGiven", ParameterDirection.Input, source.FoldersGiven);
			else
				prams[11] = GetSqlParameter("@FoldersGiven", ParameterDirection.Input,DBNull.Value);
			if(source.FordersArchive>0)
				prams[12] = GetSqlParameter("@FordersArchive", ParameterDirection.Input, source.FordersArchive);
			else
				prams[12] = GetSqlParameter("@FordersArchive", ParameterDirection.Input, DBNull.Value);
			prams[13] = GetSqlParameter("@SubprojectID", ParameterDirection.Input, source.SubprojectID);
			return prams;
		}

		private static SqlParameter[] GetUpdateParameterValues( ProjectSubcontracterData source)
		{
			SqlParameter[] prm = GetInsertParameterValues(source);
			prm[0].Direction=  ParameterDirection.Input;
			return prm;
//			return new SqlParameter[] {
//										  new SqlParameter("@ProjectSubcontracterID", source.ProjectSubcontracterID),
//										  new SqlParameter("@ProjectID", source.ProjectID),
//										  new SqlParameter("@SubcontracterID", source.SubcontracterID),
//										  new SqlParameter("@PaymentSchemeID", source.PaymentSchemeID),
//										  new SqlParameter("@Area", source.Area),
//										  new SqlParameter("@Rate", source.Rate),
//										  new SqlParameter("@TotalAmount", source.TotalAmount),
//										  new SqlParameter("@StartDate", source.StartDate),
//										  new SqlParameter("@EndDate", source.EndDate),
//										  new SqlParameter("@Note", source.Note),
//										  new SqlParameter("@Done", source.Done),
//										  new SqlParameter("@FoldersGiven", source.FoldersGiven),
//										  new SqlParameter("@FordersArchive", source.FordersArchive),
//										  new SqlParameter("@SubprojectID", source.SubprojectID)
//									  };
		}
	}
}   

