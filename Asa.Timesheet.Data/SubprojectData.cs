using System;
using System.Collections;
using System.Text;

namespace Asa.Timesheet.Data
{
	public class SubprojectData
	{
		private int _subprojectID = -1;
		private int _projectID = -1;
		private int _subprojectTypeID = -1;
		private DateTime _startDate;
		private DateTime _endDate;
		private string _notes = String.Empty;
		private int _paymentSchemeID = -1;
		private decimal _area = -1;
		private decimal _rate;
		private decimal _totalAmount;
		private string _projectName = String.Empty;
		private string _projectCode = String.Empty;
		private string _subprojectType="";
		/// <summary>
		/// Initialize an new empty Login object.
		/// </summary>
		public SubprojectData()
		{
		}
		
		/// <summary>
		/// Initialize a new Login object with the given parameters.
		/// </summary>
		public SubprojectData(int subprojectID, int projectID, int subprojectTypeID, DateTime startDate, DateTime endDate, string notes, int paymentSchemeID, int area, decimal rate, decimal totalAmount)
		{
			this._subprojectID = subprojectID;
			this._projectID = projectID;
			this._subprojectTypeID = subprojectTypeID;
			this._startDate = startDate;
			this._endDate = endDate;
			this._notes = notes;
			this._paymentSchemeID = paymentSchemeID;
			this._area = area;
			this._rate = rate;
			this._totalAmount = totalAmount;
		}	
		
	  
		public override string ToString()
		{
			StringBuilder builder = new StringBuilder();
			builder.Append("_subprojectID: " + _subprojectID.ToString()).Append("\r\n");
			builder.Append("_projectID: " + _projectID.ToString()).Append("\r\n");
			builder.Append("_subprojectTypeID: " + _subprojectTypeID.ToString()).Append("\r\n");
			builder.Append("_startDate: " + _startDate.ToString()).Append("\r\n");
			builder.Append("_endDate: " + _endDate.ToString()).Append("\r\n");
			builder.Append("_notes: " + _notes.ToString()).Append("\r\n");
			builder.Append("_paymentSchemeID: " + _paymentSchemeID.ToString()).Append("\r\n");
			builder.Append("_area: " + _area.ToString()).Append("\r\n");
			builder.Append("_rate: " + _rate.ToString()).Append("\r\n");
			builder.Append("_totalAmount: " + _totalAmount.ToString()).Append("\r\n");
			builder.Append("\r\n");
			return builder.ToString();
		}	
		public object GetByIndex(int nIndex)
		{
			switch(nIndex)
			{
				case 0:
					return _subprojectID;
				case 1:
					return _projectID;
				case 2:
					return _subprojectTypeID;
				case 3:
					return _startDate;
				case 4:
					return _endDate;
				case 5:
					return _notes;
				case 6:
					return _paymentSchemeID;
				case 7:
					return _area;
				case 8:
					return _rate;
				case 9:
					return _totalAmount;
			}
			return null;
		}
		#region Properties
		
		
		public string ProjectName
		{
			get { return _projectName; }
			set { _projectName = value; }
		}
		public string SubprojectType
		{
			get { return _subprojectType; }
			set { _subprojectType = value; }
		}
		
		public string ProjectCode
		{
			get { return _projectCode; }
			set { _projectCode = value; }
		}
	
		public int SubprojectID
		{
			get { return _subprojectID; }
			set { _subprojectID = value; }
		}
	
		public int ProjectID
		{
			get { return _projectID; }
			set { _projectID = value; }
		}
	
		public int SubprojectTypeID
		{
			get { return _subprojectTypeID; }
			set { _subprojectTypeID = value; }
		}
	
		public DateTime StartDate
		{
			get { return _startDate; }
			set { _startDate = value; }
		}
	
		public DateTime EndDate
		{
			get { return _endDate; }
			set { _endDate = value; }
		}
	
		public string Notes
		{
			get { return _notes; }
			set { _notes = value; }
		}
	
		public int PaymentSchemeID
		{
			get { return _paymentSchemeID; }
			set { _paymentSchemeID = value; }
		}
	
		public decimal Area
		{
			get { return _area; }
			set { _area = value; }
		}
	
		public decimal Rate
		{
			get { return _rate; }
			set { _rate = value; }
		}
	
		public decimal TotalAmount
		{
			get { return _totalAmount; }
			set { _totalAmount = value; }
		}
	
		#endregion
	}
}


