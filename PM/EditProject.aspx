﻿<%@ Page Title="Редакция на проект" Language="C#" MasterPageFile="~/PM.Master" AutoEventWireup="True" CodeBehind="EditProject.aspx.cs" Inherits="PM.EditProject" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
  
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

<table>
	<tr>
		<td vAlign="middle"><asp:linkbutton id="linkBack" Runat="server" CssClass="Brown"><i class="fas fa-2x fa-chevron-left"></i></asp:linkbutton></td>
	</tr>
</table>
<P><asp:label id="lblError" runat="server" CssClass="ErrorLabel" ForeColor="Red" EnableViewState="False"></asp:label><asp:label id="lblInfo" runat="server" CssClass="InfoLabel" EnableViewState="False"></asp:label><BR>
<table width="100%">
	<tr>
		<td vAlign="top" width="50%">
			<table id="tblForm" cellSpacing="0" cellPadding="3" width="100%" border="0">

				<TR>
					<td style="WIDTH: 30%"><asp:label id="lblBuildingType" runat="server" CssClass="enterDataLabel" Width="100%" Font-Bold="True"></asp:label></TD>
					<td style="WIDTH: 70%"><asp:dropdownlist id="ddlBuildingTypes" runat="server" CssClass="enterDataBox" Width="350px" Visible="False"></asp:dropdownlist><asp:label id="txtBuildingTypes" runat="server" CssClass="enterDataBox" Width="100%" MaxLength="10"></asp:label></TD>
				</TR>
				<TR>
					<td style="WIDTH: 30%; HEIGHT: 21px" colSpan="1" rowSpan="1"><asp:label id="lblProjectCode" runat="server" CssClass="enterDataLabel" Width="100%" Font-Bold="True"></asp:label></TD>
					<td style="WIDTH: 70%; HEIGHT: 21px"><TABLE id="Table2" cellSpacing="0" cellPadding="0" width="350" border="0" runat="server"
							Visible="false">
							<TR>
								<td class="projectCodeBox" noWrap>&nbsp;</TD>
								<td style="WIDTH: 54px" noWrap width="49"></td>
								<td class="projectCodeBox" noWrap>&nbsp;</TD>
								<td noWrap width="5"></td>
								<td noWrap><asp:textbox id="txtProjectCode" runat="server" CssClass="enterDataBox" Width="165px" MaxLength="2"></asp:textbox><asp:label id="lblBTCode" runat="server" Font-Bold="True"></asp:label>&nbsp;<asp:label id="lblProjectOrder" runat="server" Font-Bold="True"></asp:label></TD>
							</TR>
						</TABLE>
						<asp:label id="txtProjCode" runat="server" CssClass="enterDataBox" MaxLength="10"></asp:label></TD>
				</TR>
				<TR>
					<td style="WIDTH: 30%" vAlign="top"><asp:label id="lblStartDate" runat="server" CssClass="enterDataLabel" Width="100%" Font-Bold="True"></asp:label></TD>
					<td style="WIDTH: 70%"><asp:label id="txtStartDate" runat="server" CssClass="enterDataBox" Width="100%" MaxLength="10"></asp:label></TD>
				</TR>
				<TR>
					<td style="WIDTH: 30%"><asp:label id="Label7" runat="server" CssClass="enterDataLabel" Width="100%" Font-Bold="True"></asp:label></TD>
					<td style="WIDTH: 70%"><asp:label id="txtEndDateContract" runat="server" CssClass="enterDataBox" Width="100%" MaxLength="10"></asp:label></TD>
				</TR>
				<TR>
					<td style="WIDTH: 30%; HEIGHT: 17px"><asp:label id="lblManager" runat="server" CssClass="enterDataLabel" EnableViewState="False"
							Width="100%" Font-Bold="True"></asp:label></TD>
					<td style="WIDTH: 70%; HEIGHT: 17px"><asp:dropdownlist id="ddlManager" runat="server" CssClass="enterDataBox" Width="350px" Visible="False"></asp:dropdownlist><asp:label id="txtManager" runat="server" CssClass="enterDataBox" Width="100%"></asp:label></TD>
				</TR>
			</table>
		</td>
		<td vAlign="top" width="50%">
			<table>
				<tr>
					<td>
						<asp:datagrid id="grdProfile" runat="server" CssClass="Grid" Width="100%" HorizontalAlign="Right"
							AutoGenerateColumns="False" CellPadding="4" PageSize="2" AllowSorting="True">
							<ItemStyle CssClass="GridItem"></ItemStyle>
							<HeaderStyle Wrap="False" CssClass="GridHeader"></HeaderStyle>
							<Columns>
								<asp:BoundColumn DataField="ProjectDocumentID" Visible="false"></asp:BoundColumn>
								<asp:TemplateColumn>
									<HeaderStyle Wrap="False" Width="20px"></HeaderStyle>
									<ItemStyle Wrap="False" HorizontalAlign="Center"></ItemStyle>
									<ItemTemplate>
										<asp:ImageButton id="btnScan" runat="server" ImageUrl="images/pdf.gif" ToolTip='<%# DataBinder.Eval(Container, "DataItem.DocumentDiscription") %>' CommandName="pdfShow">
										</asp:ImageButton>
									</ItemTemplate>
								</asp:TemplateColumn>
								<asp:BoundColumn DataField="DocumentType" Visible="false"></asp:BoundColumn>
								<asp:BoundColumn Visible="true"></asp:BoundColumn>
								<asp:BoundColumn DataField="DocumentDiscription" Visible="true"></asp:BoundColumn>
								<asp:TemplateColumn HeaderText="Подизпълнители">
									<HeaderStyle Wrap="False" Width="20px"></HeaderStyle>
									<ItemStyle Wrap="False" HorizontalAlign="Center"></ItemStyle>
									<ItemTemplate>
										<asp:CheckBox id="imgSubcontracter" runat="server" Checked='<%# DataBinder.Eval(Container, "DataItem.IsSubcontracterVisible") %>' AutoPostBack=True>
										</asp:CheckBox>
									</ItemTemplate>
								</asp:TemplateColumn>
								<asp:TemplateColumn HeaderText="Клиенти">
									<HeaderStyle Wrap="False" Width="20px"></HeaderStyle>
									<ItemStyle Wrap="False" HorizontalAlign="Center"></ItemStyle>
									<ItemTemplate>
										<asp:CheckBox id="imgClient" runat="server" Checked='<%# DataBinder.Eval(Container, "DataItem.IsClientVisible") %>' AutoPostBack=True>
										</asp:CheckBox>
									</ItemTemplate>
								</asp:TemplateColumn>
								<asp:TemplateColumn HeaderText="Строители">
									<HeaderStyle Wrap="False" Width="20px"></HeaderStyle>
									<ItemStyle Wrap="False" HorizontalAlign="Center"></ItemStyle>
									<ItemTemplate>
										<asp:CheckBox id="imgBuilder" runat="server" Checked='<%# DataBinder.Eval(Container, "DataItem.IsBuilderVisible") %>' AutoPostBack=True>
										</asp:CheckBox>
									</ItemTemplate>
								</asp:TemplateColumn>
							</Columns>
						</asp:datagrid>
					</td>
				</tr>
				<tr>
					<td>
						<asp:label id="lbProfileNote" Runat="server" CssClass="enterDataBox" Font-Bold="True" valign="bottom"
							Visible="false"></asp:label>
						<P></P>
					</td>
				</tr>
			</table>
			<P></P>
		</td>
	</tr>
</table>
<hr />
<TABLE id="Table5"  width="100%">
	<TR>
		<td width="40%">
			<P>&nbsp;<asp:button id="btnNew" runat="server" CssClass="ActionButton Default"></asp:button></P></TD>
        <td align=right>
			<P>&nbsp;<asp:dropdownlist id="ddlSubCategories" Runat="server" CssClass="enterDataBox" Width="400px"></asp:dropdownlist>&nbsp;
            <asp:button id="btnCreateCategory" runat="server" CssClass="ActionButton" ></asp:button>&nbsp;</P>
		</TD>
	</TR>
</TABLE>

<table id="tblCat" style="WIDTH: 100%" runat="server">
	<TBODY>
		<tr>
			<td style="WIDTH: 50%" vAlign="top">
            <asp:label id="lbInsideUse" Runat="server" CssClass="lblTitle" ></asp:label><small>  (.dwg)</small><br />
            <asp:Button id="btnPlusAll" Runat="server" CssClass="btnCircle Plus"  Text="+"></asp:Button>
            <asp:Button id="btnMinusAll" Runat="server" CssClass="btnCircle Minus"  Text="-"></asp:Button>
            <BR>
				<BR>
				<asp:panel id="grid" 
					runat="server" Width="99%">

					<asp:datalist id="dlCategories" runat="server" Width="100%" RepeatColumns="1" RepeatDirection="Horizontal">
						<EditItemStyle VerticalAlign="Top"></EditItemStyle>
						<ItemStyle VerticalAlign="Top"></ItemStyle>
						<ItemTemplate>
							<table style="Width =100%">
								<tr>
									<td style="width=25px">
										<asp:Button ID="btnPlus" Runat="server" CssClass="btnCircle Plus" Text="+" ></asp:Button>
										<asp:Button ID="btnMinus" Runat="server" CssClass="btnCircle Minus" Text="-"  Visible="False"></asp:Button>
									</td>
									<td>
										<asp:LinkButton ID="linkCategoryName" Runat="server" CssClass="menuTable" ForeColor="#804000" CommandName="Edit">
											<span Class="menuTable">
												<%# GetTextEN((string)DataBinder.Eval(Container, "DataItem.CategoryName")) %>
											</span>
										</asp:LinkButton>
										<asp:Label ID="lbCategoryName" Runat=server text='<%# DataBinder.Eval(Container, "DataItem.CategoryName") %>' Visible=False>
										</asp:Label>
										<asp:Label ID="lbID" Runat=server text='<%# DataBinder.Eval(Container, "DataItem.CategoryID") %>' Visible=False>
										</asp:Label>
										<asp:Label ID="lbSubID" Runat=server text='<%# DataBinder.Eval(Container, "DataItem.SubcontracterID") %>' Visible=False>
										</asp:Label>
									</td>
									<td width="20px">
										<asp:ImageButton id="imgActiveGroupYes" runat="server" Width="16px" Height="16px" ToolTip="" ImageUrl="images/1_modify.gif" Visible='<%# CheckForActiveGroupYes((int)DataBinder.Eval(Container, "DataItem.CategoryID")) %>' align=right>
										</asp:ImageButton>
										<asp:ImageButton id="imgActiveGroupNo" runat="server" Width="16px" Height="16px" ImageUrl="images/1.gif" Visible='<%# CheckForActiveGroupNo((int)DataBinder.Eval(Container, "DataItem.CategoryID")) %>' align=right>
										</asp:ImageButton>
									</td>
									<td width="20px">
										<asp:ImageButton id="imgDelete" runat="server" Width="16px" Height="16px" ToolTip="Изтрий" ImageUrl="images/delete.gif" Visible='<%# GetVisible(DataBinder.Eval(Container, "DataItem.IsProjectMain"),DataBinder.Eval(Container, "DataItem.ClientID"),DataBinder.Eval(Container, "DataItem.SubcontracterID"))%>' align=right>
										</asp:ImageButton>
									</td>
								</tr>
							</table>
							<table style="width=100%">
								<tr>
									<td>
										<asp:datagrid id="grdFiles" runat="server" CssClass="Grid" Width="100%" AllowSorting="True" PageSize="2"
											CellPadding="4" AutoGenerateColumns="False" Visible="False">
											<ItemStyle CssClass="GridItem"></ItemStyle>
											<HeaderStyle Wrap="False" CssClass="GridHeader"></HeaderStyle>
											<Columns>
												<asp:TemplateColumn Visible="False">
													<HeaderStyle Wrap="False" Width="20px"></HeaderStyle>
													<ItemStyle Wrap="False" HorizontalAlign="Center"></ItemStyle>
													<ItemTemplate>
														<asp:ImageButton id="btnEdit" runat="server" ImageUrl="images/edit1.gif" ToolTip="Редактирай"></asp:ImageButton>
													</ItemTemplate>
												</asp:TemplateColumn>
												<asp:TemplateColumn SortExpression="FileNameSave">
													<ItemTemplate>
														<asp:LinkButton id="lnk" runat="server" CssClass="menuTable" ForeColor="#804000" CommandName="Edit">
															<span Class="menuTable">
																<%# DataBinder.Eval(Container, "DataItem.FileNameSave") %>
																<%# DataBinder.Eval(Container, "DataItem.FileType") %>
															</span>
														</asp:LinkButton>
													</ItemTemplate>
												</asp:TemplateColumn>
												<asp:BoundColumn DataField="FileNameSave" Visible="False">
													<HeaderStyle></HeaderStyle>
												</asp:BoundColumn>
												<asp:BoundColumn DataField="FileSubject" SortExpression="FileSubject">
													<HeaderStyle></HeaderStyle>
												</asp:BoundColumn>
												<asp:BoundColumn DataField="FileVersion" HeaderStyle-Width="45px" SortExpression="FileVersion">
													<HeaderStyle></HeaderStyle>
												</asp:BoundColumn>
												<asp:BoundColumn DataField="FileType" Visible="false">
													<HeaderStyle></HeaderStyle>
												</asp:BoundColumn>
												<asp:BoundColumn DataField="CreatedBy" SortExpression="CreatedBy">
													<HeaderStyle></HeaderStyle>
												</asp:BoundColumn>
												<asp:BoundColumn DataField="DateCreated" DataFormatString="{0:dd.MM.yyyy}" SortExpression="DateCreated">
													<HeaderStyle></HeaderStyle>
												</asp:BoundColumn>
												<asp:TemplateColumn Visible="False">
													<HeaderStyle Wrap="False" Width="20px"></HeaderStyle>
													<ItemStyle Wrap="False" HorizontalAlign="Center"></ItemStyle>
													<ItemTemplate>
														<asp:ImageButton id="btnDelete" runat="server" Width="16px" Height="16px" ToolTip="Изтрий" ImageUrl="images/delete.gif"></asp:ImageButton>
													</ItemTemplate>
												</asp:TemplateColumn>
											</Columns>
										</asp:datagrid>
									</td>
								</tr>
							</table>
						</ItemTemplate>
					</asp:datalist>
				</asp:panel></td>

			<td style="WIDTH: 50%; box-shadow: 0px 0px 2px black;background: #fdfdfd; padding-left: 20px;" vAlign="top" s>
            <asp:label id="lbOutsideUse" Runat="server" CssClass="lblTitle" ></asp:label><small>  (.pdf)</small><br />
            <asp:button id="btnPlusAllCopy" Runat="server" CssClass="btnCircle"  Text="+"></asp:button>
            <asp:button id="btnMinusAllCopy" Runat="server" CssClass="btnCircle" Text="-"></asp:button><BR>
				<BR>
				<asp:panel id="gridCopy" 
					runat="server" Width="99%">

					<asp:datalist id="dlCategoriesCopy" runat="server" Width="100%" RepeatColumns="1" RepeatDirection="Horizontal">
						<EditItemStyle VerticalAlign="Top"></EditItemStyle>
						<ItemStyle VerticalAlign="Top"></ItemStyle>
						<ItemTemplate>
							<table style="Width =100%">
								<tr>
									<td style="width=25px">
										<asp:Button ID="btnPlusCopy" Runat="server" CssClass="btnCircle Plus" Text="+"></asp:Button>
										<asp:Button ID="btnMinusCopy" Runat="server" CssClass="btnCircle Minus" Text="-"  Visible="False"></asp:Button>
									</td>
									<td>
										<asp:LinkButton ID="linkCategoryNameCopy" Runat="server" CssClass="menuTable" ForeColor="#804000"
											CommandName="Edit">
											<span Class="menuTable">
												<%#  GetTextEN((string)DataBinder.Eval(Container, "DataItem.CategoryName")) %>
											</span>
										</asp:LinkButton>
										<asp:Label ID="lbCategoryNameCopy" Runat=server text='<%# DataBinder.Eval(Container, "DataItem.CategoryName") %>' Visible=False>
										</asp:Label>
										<asp:Label ID="lbIDCopy" Runat=server text='<%# DataBinder.Eval(Container, "DataItem.CategoryID") %>' Visible=False>
										</asp:Label>
										<asp:Label ID="lbSubIDCopy" Runat=server text='<%# DataBinder.Eval(Container, "DataItem.SubcontracterID") %>' Visible=False>
										</asp:Label>
									</td>
									<td width="20px">
										<asp:ImageButton id="imgActiveGroupYesCopy" runat="server" Width="16px" Height="16px" ToolTip="отмини" ImageUrl="images/1_modify.gif" Visible='<%# CheckForActiveGroupYes((int)DataBinder.Eval(Container, "DataItem.CategoryID")) %>' align=right>
										</asp:ImageButton>
										<asp:ImageButton id="imgActiveGroupNoCopy" runat="server" Width="16px" Height="16px" ToolTip="дай" ImageUrl="images/1.gif" Visible='<%# CheckForActiveGroupNo((int)DataBinder.Eval(Container, "DataItem.CategoryID")) %>' align=right>
										</asp:ImageButton>
									</td>
									<td width="20px">
										<asp:ImageButton id="imgDeleteCopy" runat="server" Width="16px" Height="16px" ToolTip="Изтрий" ImageUrl="images/delete.gif" Visible='<%# GetVisible(DataBinder.Eval(Container, "DataItem.IsProjectMain"),DataBinder.Eval(Container, "DataItem.ClientID"),DataBinder.Eval(Container, "DataItem.SubcontracterID"))%>' align=right>
										</asp:ImageButton>
									</td>
								</tr>
							</table>
							<table style="width=100%">
								<tr>
									<td>
										<asp:datagrid id="grdFilesCopy" runat="server" CssClass="Grid" Width="100%" AllowSorting="True"
											PageSize="2" CellPadding="4" AutoGenerateColumns="False" Visible="False">
											<ItemStyle CssClass="GridItem"></ItemStyle>
											<HeaderStyle Wrap="False" CssClass="GridHeader"></HeaderStyle>
											<Columns>
												<asp:TemplateColumn Visible="False">
													<HeaderStyle Wrap="False" Width="20px"></HeaderStyle>
													<ItemStyle Wrap="False" HorizontalAlign="Center"></ItemStyle>
													<ItemTemplate>
														<asp:ImageButton id="btnEdit" runat="server" ImageUrl="images/edit1.gif" ToolTip="Редактирай"></asp:ImageButton>
													</ItemTemplate>
												</asp:TemplateColumn>
												<asp:TemplateColumn SortExpression="FileNameSave">
													<ItemTemplate>
														<asp:LinkButton id="Linkbutton1" runat="server" CssClass="menuTable" ForeColor="#804000" CommandName="Edit">
															<span Class="menuTable">
																<%# DataBinder.Eval(Container, "DataItem.FileNameSave") %>
																<%# DataBinder.Eval(Container, "DataItem.FileType") %>
															</span>
														</asp:LinkButton>
													</ItemTemplate>
												</asp:TemplateColumn>
												<asp:BoundColumn DataField="FileNameSave" Visible="False">
													<HeaderStyle></HeaderStyle>
												</asp:BoundColumn>
												<asp:BoundColumn DataField="FileSubject" SortExpression="FileSubject">
													<HeaderStyle></HeaderStyle>
												</asp:BoundColumn>
												<asp:BoundColumn DataField="FileVersion" HeaderStyle-Width="45px" SortExpression="FileVersion">
													<HeaderStyle></HeaderStyle>
												</asp:BoundColumn>
												<asp:BoundColumn DataField="FileType" Visible="false">
													<HeaderStyle></HeaderStyle>
												</asp:BoundColumn>
												<asp:BoundColumn DataField="CreatedBy" SortExpression="CreatedBy">
													<HeaderStyle></HeaderStyle>
												</asp:BoundColumn>
												<asp:BoundColumn DataField="DateCreated" DataFormatString="{0:dd.MM.yyyy}" SortExpression="DateCreated">
													<HeaderStyle></HeaderStyle>
												</asp:BoundColumn>
												<asp:TemplateColumn Visible="False">
													<HeaderStyle Wrap="False" Width="20px"></HeaderStyle>
													<ItemStyle Wrap="False" HorizontalAlign="Center"></ItemStyle>
													<ItemTemplate>
														<asp:ImageButton id="btnDelete" runat="server" Width="12px" Height="12px" ToolTip="Изтрий" ImageUrl="images/delete.gif"></asp:ImageButton>
													</ItemTemplate>
												</asp:TemplateColumn>
											</Columns>
										</asp:datagrid>
									</td>
								</tr>
							</table>
						</ItemTemplate>
					</asp:datalist>
				</asp:panel></td>
		</tr>
	</TBODY>
</table>

&nbsp;&nbsp;&nbsp;
<asp:Label id="lbNoCategory" Runat="server" Visible="False"></asp:Label><BR>
</asp:Content>
