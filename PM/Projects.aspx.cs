using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using Asa.Timesheet.Data;
using System.Data.SqlClient;
using log4net;
using log4net.Config;

namespace PM
{
	/// <summary>
	/// Summary description for PMProjects.
	/// </summary>
	public class Projects : BasePage
	{
		#region Web controls

		protected System.Web.UI.WebControls.Panel grid;
		protected System.Web.UI.WebControls.Label lblInfo;
		protected System.Web.UI.WebControls.DataGrid grdProjects;
		protected System.Web.UI.WebControls.Label lblError;
		protected System.Web.UI.WebControls.Label lblName;
		protected System.Web.UI.WebControls.Button btnSearch;
		protected System.Web.UI.WebControls.DropDownList ddlSearch;
		protected System.Web.UI.WebControls.TextBox txtProject;
		protected System.Web.UI.WebControls.Button btnClear;
		protected System.Web.UI.WebControls.DropDownList ddlBuildingTypes;
        
		
		private static readonly ILog log = LogManager.GetLogger(typeof(Projects));
		
		#endregion

		#region enums

		private enum GridColumns
		{
			Number=0,
			ProjectName,
			ProjectCode,
			ProjectManager,
			BuildingType,
			Active,
			Black,
			Concluded
		}

		#endregion

		#region Page_Load


        private void Page_Load(object sender, System.EventArgs e)
        {
            if (!LoggedUser.IsAuthenticated) ErrorRedirect(Resource.ResourceManager["noRightsAlert"]);

            Page.RegisterHiddenField("__EVENTTARGET", "btnSearch");

            LoadResourses();
            if (!this.IsPostBack)
            {
                string lang = Language;

                //TODO

                txtProject.Attributes.Add("placeholder", UIHelpers.GetText(lang, "Name_ProjectName"));
                SortOrder = 1;
                UIHelpers.LoadProjectStatus(ddlSearch, LoggedUser.HasPaymentRights);
                UIHelpers.LoadBuildingTypes("0", ddlBuildingTypes);
                BindGrid();
            }

        }

		
//		private bool LoadBuildingTypes(string selectedValue)
//		{
//			SqlDataReader reader = null;
//			try
//			{
//				reader = DBManager.SelectBuildingTypes();
//				ddlBuildingTypes.DataSource = reader;
//				ddlBuildingTypes.DataValueField = "BuildingTypeID";
//				ddlBuildingTypes.DataTextField = "BuildingType";
//				ddlBuildingTypes.DataBind();
//				if(ddlBuildingTypes.Items.FindByValue(System.Configuration.ConfigurationManager.AppSettings["BuildingTypeNotInPM"])!=null)
//					ddlBuildingTypes.Items.Remove(ddlBuildingTypes.Items.FindByValue(System.Configuration.ConfigurationManager.AppSettings["BuildingTypeNotInPM"]));
//				
//				ddlBuildingTypes.Items.Insert(0, new ListItem(Resource.ResourceManager["reports_typeBuilding"], "0"));
//				ddlBuildingTypes.SelectedValue = selectedValue;
//			}
//			catch (Exception ex)
//			{
//				log.Error(UIHelpers.GetLogErrorMessage(ex));
//				return false;
//			}
//			finally 
//			{
//				if (reader!=null) reader.Close();
//			}
//
//			return true;
//		}


        private void LoadResourses()
        {
            string lang = Language;
            btnSearch.Text = UIHelpers.GetText(lang, "Name_btnSearchName");

            btnClear.Text = UIHelpers.GetText(lang, "Name_btnCleanName");

            grdProjects.Columns[(int)GridColumns.BuildingType].HeaderText = UIHelpers.GetText(lang, "Name_BuildingTypeColomn");
            grdProjects.Columns[(int)GridColumns.ProjectName].HeaderText = UIHelpers.GetText(lang, "Name_ProjectNameColomn");
            grdProjects.Columns[(int)GridColumns.ProjectManager].HeaderText = UIHelpers.GetText(lang, "Name_ProjectManagerColomn");
            grdProjects.Columns[(int)GridColumns.ProjectCode].HeaderText = UIHelpers.GetText(lang, "Name_ProjectCodeColomn");





        }


		#endregion
	
		#region Web Form Designer generated code
        override protected void OnInit(EventArgs e)
        {
            //
            // CODEGEN: This call is required by the ASP.NET Web Form Designer.
            //
            InitializeComponent();
            base.OnInit(e);
        }
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
        private void InitializeComponent()
        {
            this.ddlSearch.SelectedIndexChanged += new System.EventHandler(this.ddlSearch_SelectedIndexChanged);
            this.ddlBuildingTypes.SelectedIndexChanged += new System.EventHandler(this.ddlBuildingTypes_SelectedIndexChanged);
            this.btnSearch.Click += new System.EventHandler(this.btnSearch_Click);
            this.btnClear.Click += new System.EventHandler(this.btnClear_Click);
            this.grdProjects.ItemCommand += new System.Web.UI.WebControls.DataGridCommandEventHandler(this.grdProjects_ItemCommand);
            this.grdProjects.ItemDataBound += new System.Web.UI.WebControls.DataGridItemEventHandler(this.grdProjects_ItemDataBound);
            //this.Load += new System.EventHandler(this.Page_Load); DBUGG: Loads page Twice

        }
		#endregion

		#region Database

        protected Color GetColor(object o)
        {
            if (DBNull.Value == o)
                return Color.White;
            string sColor = (string)o;
            return ColorTranslator.FromHtml(sColor);
        }


        protected Color GetForeColor(object o)
        {
            if (DBNull.Value == o)
                return Color.Black;
            else
                return Color.White;
        }


        private void BindGrid()
        {
            SqlDataReader reader = null;
            int Active = int.Parse(ddlSearch.SelectedValue);
            bool IsBuildingTypeGradoustrShow = false;
            if (ddlBuildingTypes.SelectedValue == System.Configuration.ConfigurationManager.AppSettings["BuildingTypeNotInPM"])
                IsBuildingTypeGradoustrShow = true;

            try
            {
                switch (LoggedUser.UserType)
                {
                    case ((int)PMUserType.User): { reader = ProjectsData.SelectProjects(SortOrder, Active, txtProject.Text.Trim(), int.Parse(ddlBuildingTypes.SelectedValue)/*, ddlSearch.SelectedValue=="2"*/, true, -1, -1, IsBuildingTypeGradoustrShow); break; }
                    case ((int)PMUserType.Client): { reader = ProjectsData.SelectProjects(SortOrder, Active, txtProject.Text.Trim(), int.Parse(ddlBuildingTypes.SelectedValue)/*, ddlSearch.SelectedValue=="2"*/, true, LoggedUser.UserID, -1, IsBuildingTypeGradoustrShow); break; }
                    case ((int)PMUserType.Builder): { reader = ProjectsData.SelectProjects(SortOrder, Active, txtProject.Text.Trim(), int.Parse(ddlBuildingTypes.SelectedValue)/*, ddlSearch.SelectedValue=="2"*/, true, LoggedUser.UserID, -1, IsBuildingTypeGradoustrShow); break; }
                    case ((int)PMUserType.Subcontracter): { reader = ProjectsData.SelectProjects(SortOrder, Active, txtProject.Text.Trim(), int.Parse(ddlBuildingTypes.SelectedValue)/*, ddlSearch.SelectedValue=="2"*/, true, -1, LoggedUser.UserID, IsBuildingTypeGradoustrShow); break; }
                    case ((int)PMUserType.PMUser): { reader = ProjectsData.SelectProjects(SortOrder, Active, txtProject.Text.Trim(), int.Parse(ddlBuildingTypes.SelectedValue)/*, ddlSearch.SelectedValue=="2"*/, true, LoggedUser.UserID, -1, IsBuildingTypeGradoustrShow); break; }
                    case ((int)PMUserType.Supervisors): { reader = ProjectsData.SelectProjects(SortOrder, Active, txtProject.Text.Trim(), int.Parse(ddlBuildingTypes.SelectedValue)/*, ddlSearch.SelectedValue=="2"*/, true, LoggedUser.UserID, -1, IsBuildingTypeGradoustrShow); break; }
                    default: { reader = ProjectsData.SelectProjects(SortOrder, Active, txtProject.Text.Trim(), int.Parse(ddlBuildingTypes.SelectedValue)/*, ddlSearch.SelectedValue=="2"*/, true, -1, -1, IsBuildingTypeGradoustrShow); break; }

                }

                //				if(LoggedUser.IsUser)
                //					reader = ProjectsData.SelectProjects(1/*SortOrder*/,  Active,  txtProject.Text.Trim(), int.Parse(ddlBuildingTypes.SelectedValue), ddlSearch.SelectedValue=="2",true,-1,-1);
                if (reader != null)
                {
                    grdProjects.DataSource = reader;
                    if (SessionManager.Lang == Languages.EN)
                        ((BoundColumn)grdProjects.Columns[(int)GridColumns.ProjectManager]).DataField = "UserNameEN";

                    grdProjects.DataKeyField = "ProjectID";
                    grdProjects.DataBind();
                    //else
                    //	((BoundColumn)grdProjects.Columns[(int)GridColumns.ProjectManager]).DataField="FullName";
                    grdProjects.Columns[(int)GridColumns.BuildingType].Visible = SessionManager.Lang == Languages.BG;
                }
            }
            catch (Exception ex)
            {
                log.Error(UIHelpers.GetLogErrorMessage(ex));
                lblError.Text = UIHelpers.GetText(Language, "projects_ErrorLoadProjects");
                return;
            }
            finally
            {
                if (reader != null) reader.Close();
            }

            if (grdProjects.Items.Count == 0)
            {
                grid.Visible = false;
                lblInfo.Text = UIHelpers.GetText(Language, "projects_NoActiveProjects");
            }
            else grid.Visible = true;

            //			try
            //			{
            //				//SetConfirmDelete(grdProjects, "btnDelete", 1);
            //			}
            //			catch (Exception ex)
            //			{
            //				//log.Error(ex);
            //			}

        }

		
		#endregion
		
		#region Event handlers

        private void btnSearch_Click(object sender, System.EventArgs e)
        {
            BindGrid();
        }


        private void grdProjects_ItemDataBound(object sender, System.Web.UI.WebControls.DataGridItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Item
                || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                if (e.Item.Cells[(int)GridColumns.Active].Text == "0")
                {
                    e.Item.CssClass = "Inactive";

                }
                else if (e.Item.Cells[(int)GridColumns.Concluded].Text == "1")
                {
                    e.Item.CssClass = "Concluded";

                }

                //else if(LoggedUser.HasPaymentRights && e.Item.Cells[(int)GridColumns.Black].Text=="1")
                //{
                //	e.Item.CssClass="Black";

                //}
            }
        }


        private void btnClear_Click(object sender, System.EventArgs e)
        {
            txtProject.Text = "";
            ddlSearch.SelectedIndex = 0;
            ddlBuildingTypes.SelectedIndex = 0;
            BindGrid();
        }


        private void ddlBuildingTypes_SelectedIndexChanged(object sender, System.EventArgs e)
        {
            BindGrid();
        }


        private void ddlSearch_SelectedIndexChanged(object sender, System.EventArgs e)
        {
            BindGrid();
        }


        private void grdProjects_ItemCommand(object source, System.Web.UI.WebControls.DataGridCommandEventArgs e)
        {
            if (e.CommandName == "Sort")
            {
                int sortOrder = SortOrder;
                switch ((string)e.CommandArgument)
                {
                    case "Name": if (sortOrder == 1) sortOrder = -1; else sortOrder = 1;
                        SortOrder = sortOrder;
                        BindGrid(); break;
                    case "Code": if (sortOrder == 2) sortOrder = -2; else sortOrder = 2;
                        SortOrder = sortOrder;
                        BindGrid(); break;
                    case "StartDate": if (sortOrder == 3) sortOrder = -3; else sortOrder = 3;
                        SortOrder = sortOrder;
                        BindGrid(); break;
                    case "BuildingType": if (sortOrder == 4) sortOrder = -4; else sortOrder = 4;
                        SortOrder = sortOrder;
                        BindGrid(); break;
                    case "Area": if (sortOrder == 5) sortOrder = -5; else sortOrder = 5;
                        SortOrder = sortOrder;
                        BindGrid(); break;
                    case "Address": if (sortOrder == 6) sortOrder = -6; else sortOrder = 6;
                        SortOrder = sortOrder;
                        BindGrid(); break;
                    case "Client": if (sortOrder == 7) sortOrder = -7; else sortOrder = 7;
                        SortOrder = sortOrder;
                        BindGrid(); break;
                    case "Leader": if (sortOrder == 8) sortOrder = -8; else sortOrder = 8;
                        SortOrder = sortOrder;
                        BindGrid(); break;

                }
                return;
            }
            else if (e.CommandName == "Edit")
            {
                string sProjectID = ((DataGrid)source).DataKeys[e.Item.ItemIndex].ToString();
                Response.Redirect("EditProject.aspx" + "?pid=" + sProjectID);
                return;
            }
            ImageButton ib = (ImageButton)e.CommandSource;
            string s = ib.ID;
            //			switch (ib.ID)
            //			{
            ////				case "btnDelete": int projectID = (int)((DataGrid)source).DataKeys[e.Item.ItemIndex];
            ////					if (!DeleteProject(projectID))
            ////					{
            ////						//lblError.Text = Resource.ResourceManager["editProject_ErrorDelete"];
            ////						return;
            ////					}
            ////					else
            ////						//log.Info(string.Format("Project {0} has been DELETED by {1}",projectID,string.Concat(LoggedUser.FullName," ( userID ",LoggedUser.UserID," )")));
            ////					BindGrid();
            ////					break;
            //				case "btnEdit": string sProjectID = ((DataGrid)source).DataKeys[e.Item.ItemIndex].ToString();
            //					//Response.Redirect("EditProject.aspx"+"?pid="+sProjectID); break;
            ////				case "btnSubprojects": string sProjectID1 = ((DataGrid)source).DataKeys[e.Item.ItemIndex].ToString();
            ////					Response.Redirect("Subprojects.aspx"+"?pid="+sProjectID1); break;
            ////				case "btnProfile": string sProjectID2 = ((DataGrid)source).DataKeys[e.Item.ItemIndex].ToString();
            ////					Response.Redirect("Profile.aspx"+"?pid="+sProjectID2); break;
            ////				case "btnSubContracters": string sProjectID3 = ((DataGrid)source).DataKeys[e.Item.ItemIndex].ToString();
            ////					Response.Redirect("ProjectSubcontracters.aspx"+"?pid="+sProjectID3); break;
            //				default:{return;}	
            //					
            //			}
        }

	
		#endregion

	}
}
