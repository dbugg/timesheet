using System;
using System.Data;
using Asa.Timesheet.Data;
using GrapeCity.ActiveReports;
using GrapeCity.ActiveReports.Controls;
using GrapeCity.ActiveReports.SectionReportModel;
using GrapeCity.ActiveReports.Document.Section;
using GrapeCity.ActiveReports.Document;

namespace Asa.Timesheet.WebPages.Reports
{
    public class ClientSubreport2 : GrapeCity.ActiveReports.SectionReport
	{
		private decimal _EURpayd = 0;
		private decimal _EURnopayd = 0;
		private const string _boldStyle = " font-weight: bold; ";
		private const string _notBoldStyle = "text-align: right; ";
		private const string _EmptyStyle = "";

        public ClientSubreport2(DataView dv)
        {


            InitializeComponent();
            this.DataSource = dv;


        }

        private void Detail_Format(object sender, System.EventArgs eArgs)
        {
            //edited by Ivailo date: 10.12.2007
            //notes: if it's ���� hide some fields
            if (txtPaymentID.Text.Length == 0)
            {
                this.TextBox5.Text = "";
                this.TextBox9.Text = "";
                this.TextBox4.Text = "";
                if (txtSubprojectID.Text.Length == 0)
                {
                    this.TextBox2.Text = "";
                    this.TextBox10.Style = string.Concat(this.TextBox10.Style, " ", _boldStyle);
                    this.TextBox3.Style = string.Concat(this.TextBox3.Style, " ", _boldStyle);
                    this.TextBox1.Style = string.Concat(this.TextBox1.Style, " ", _boldStyle);
                }
                else
                {
                    this.TextBox10.Style = _notBoldStyle;
                    this.TextBox3.Style = _notBoldStyle;
                    this.TextBox1.Style = _EmptyStyle;
                }
            }
            else
            {
                this.TextBox10.Style = _notBoldStyle;
                this.TextBox3.Style = _notBoldStyle;
                this.TextBox1.Style = _EmptyStyle;
            }
            if (txtPaymentID.Text.Length > 0)
            {
                decimal eur = UIHelpers.ParseDecimal(this.TextBox3.Text);
                if (this.TextBox5.Text == Resource.ResourceManager["Reports_Yes"])
                    _EURpayd += eur;
                else
                    _EURnopayd += eur;
            }
        }
        private void ReportFooter_Format(object sender, System.EventArgs eArgs)
        {
            decimal fix = decimal.Parse(System.Configuration.ConfigurationManager.AppSettings["fixedBGNEUR"]);
            this.txtPaydEUR.Text = _EURpayd.ToString("#,##0");
            decimal bgn = _EURpayd * fix;
            this.txtPaydBGN.Text = bgn.ToString("#,##0");
            this.txtNoPaydEUR.Text = _EURnopayd.ToString("#,##0");
            bgn = _EURnopayd * fix;
            this.txtNOPaydBGN.Text = bgn.ToString("#,##0");
            bgn = _EURnopayd + _EURpayd;
            this.txtAllEUR.Text = bgn.ToString("#,##0");
            bgn = bgn * fix;
            this.txtAllBGN.Text = bgn.ToString("#,##0");
        }

		#region ActiveReports Designer generated code

































        public void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ClientSubreport2));
            this.Detail = new GrapeCity.ActiveReports.SectionReportModel.Detail();
            this.ReportHeader = new GrapeCity.ActiveReports.SectionReportModel.ReportHeader();
            this.ReportFooter = new GrapeCity.ActiveReports.SectionReportModel.ReportFooter();
            this.GroupHeader1 = new GrapeCity.ActiveReports.SectionReportModel.GroupHeader();
            this.GroupFooter1 = new GrapeCity.ActiveReports.SectionReportModel.GroupFooter();
            this.Label22 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label23 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label24 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Line6 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.Label25 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label26 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label27 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label28 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.txtPaymentID = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtSubprojectID = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.TextBox1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.TextBox2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.TextBox3 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.TextBox4 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.TextBox5 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.TextBox9 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.TextBox10 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.Line5 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.Line3 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.txtPaydEUR = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.Label19 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.txtPaydBGN = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtNoPaydEUR = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.Label20 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.txtNOPaydBGN = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtAllEUR = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.Label21 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.txtAllBGN = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            ((System.ComponentModel.ISupportInitialize)(this.Label22)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label23)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label24)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label25)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label26)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label27)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label28)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPaymentID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSubprojectID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TextBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TextBox2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TextBox3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TextBox4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TextBox5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TextBox9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TextBox10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPaydEUR)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label19)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPaydBGN)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtNoPaydEUR)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label20)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtNOPaydBGN)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAllEUR)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label21)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAllBGN)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            // 
            // Detail
            // 
            this.Detail.ColumnSpacing = 0F;
            this.Detail.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
						this.txtPaymentID,
						this.txtSubprojectID,
						this.TextBox1,
						this.TextBox2,
						this.TextBox3,
						this.TextBox4,
						this.TextBox5,
						this.TextBox9,
						this.TextBox10,
						this.Line5});
            this.Detail.Height = 0.2638889F;
            this.Detail.Name = "Detail";
            // 
            // ReportHeader
            // 
            this.ReportHeader.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
						this.Label22,
						this.Label23,
						this.Label24,
						this.Line6,
						this.Label25,
						this.Label26,
						this.Label27,
						this.Label28});
            this.ReportHeader.Height = 0.3534722F;
            this.ReportHeader.Name = "ReportHeader";
            // 
            // ReportFooter
            // 
            this.ReportFooter.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
						this.Line3,
						this.txtPaydEUR,
						this.Label19,
						this.txtPaydBGN,
						this.txtNoPaydEUR,
						this.Label20,
						this.txtNOPaydBGN,
						this.txtAllEUR,
						this.Label21,
						this.txtAllBGN});
            this.ReportFooter.Height = 0.6756945F;
            this.ReportFooter.Name = "ReportFooter";
            // 
            // GroupHeader1
            // 
            this.GroupHeader1.DataField = "ProjectID";
            this.GroupHeader1.Height = 0F;
            this.GroupHeader1.Name = "GroupHeader1";
            // 
            // GroupFooter1
            // 
            this.GroupFooter1.Height = 0F;
            this.GroupFooter1.Name = "GroupFooter1";
            // 
            // Label22
            // 
            this.Label22.Height = 0.2F;
            this.Label22.HyperLink = null;
            this.Label22.Left = 0F;
            this.Label22.Name = "Label22";
            this.Label22.Style = "font-weight: bold";
            this.Label22.Text = "??????";
            this.Label22.Top = 0.0625F;
            this.Label22.Width = 2.5F;
            // 
            // Label23
            // 
            this.Label23.Height = 0.2F;
            this.Label23.HyperLink = null;
            this.Label23.Left = 2.5F;
            this.Label23.Name = "Label23";
            this.Label23.Style = "font-weight: bold";
            this.Label23.Text = "????";
            this.Label23.Top = 0.0625F;
            this.Label23.Width = 2.063F;
            // 
            // Label24
            // 
            this.Label24.Height = 0.2F;
            this.Label24.HyperLink = null;
            this.Label24.Left = 4.563F;
            this.Label24.Name = "Label24";
            this.Label24.Style = "font-weight: bold; text-align: right";
            this.Label24.Text = "???? (EUR)";
            this.Label24.Top = 0.0625F;
            this.Label24.Width = 1F;
            // 
            // Line6
            // 
            this.Line6.Height = 0F;
            this.Line6.Left = 0.006944418F;
            this.Line6.LineWeight = 1F;
            this.Line6.Name = "Line6";
            this.Line6.Top = 0.3125F;
            this.Line6.Width = 9.1875F;
            this.Line6.X1 = 0.006944418F;
            this.Line6.X2 = 9.194445F;
            this.Line6.Y1 = 0.3125F;
            this.Line6.Y2 = 0.3125F;
            // 
            // Label25
            // 
            this.Label25.Height = 0.2F;
            this.Label25.HyperLink = null;
            this.Label25.Left = 6.5F;
            this.Label25.Name = "Label25";
            this.Label25.Style = "font-weight: bold; text-align: right";
            this.Label25.Text = "???? ???????";
            this.Label25.Top = 0.0625F;
            this.Label25.Width = 1.062501F;
            // 
            // Label26
            // 
            this.Label26.Height = 0.2F;
            this.Label26.HyperLink = null;
            this.Label26.Left = 8.3125F;
            this.Label26.Name = "Label26";
            this.Label26.Style = "font-weight: bold; text-align: right";
            this.Label26.Text = "???????";
            this.Label26.Top = 0.0625F;
            this.Label26.Width = 0.812F;
            // 
            // Label27
            // 
            this.Label27.Height = 0.2F;
            this.Label27.HyperLink = null;
            this.Label27.Left = 7.563F;
            this.Label27.Name = "Label27";
            this.Label27.Style = "font-weight: bold; text-align: right";
            this.Label27.Text = "???????";
            this.Label27.Top = 0.0625F;
            this.Label27.Width = 0.75F;
            // 
            // Label28
            // 
            this.Label28.Height = 0.2F;
            this.Label28.HyperLink = null;
            this.Label28.Left = 5.5625F;
            this.Label28.Name = "Label28";
            this.Label28.Style = "font-weight: bold; text-align: right";
            this.Label28.Text = "???? (??.)";
            this.Label28.Top = 0.0625F;
            this.Label28.Width = 0.938F;
            // 
            // txtPaymentID
            // 
            this.txtPaymentID.DataField = "PaymentID";
            this.txtPaymentID.Height = 0.2F;
            this.txtPaymentID.Left = 4.75F;
            this.txtPaymentID.Name = "txtPaymentID";
            this.txtPaymentID.Top = 0F;
            this.txtPaymentID.Visible = false;
            this.txtPaymentID.Width = 0.875F;
            // 
            // txtSubprojectID
            // 
            this.txtSubprojectID.DataField = "SubprojectID";
            this.txtSubprojectID.Height = 0.2F;
            this.txtSubprojectID.Left = 4.563F;
            this.txtSubprojectID.Name = "txtSubprojectID";
            this.txtSubprojectID.Top = 0F;
            this.txtSubprojectID.Visible = false;
            this.txtSubprojectID.Width = 1F;
            // 
            // TextBox1
            // 
            this.TextBox1.DataField = "projectname";
            this.TextBox1.Height = 0.2F;
            this.TextBox1.Left = 0F;
            this.TextBox1.Name = "TextBox1";
            this.TextBox1.Text = "TextBox1";
            this.TextBox1.Top = 0F;
            this.TextBox1.Width = 2.5F;
            // 
            // TextBox2
            // 
            this.TextBox2.DataField = "SubprojectType";
            this.TextBox2.Height = 0.2F;
            this.TextBox2.Left = 2.5F;
            this.TextBox2.Name = "TextBox2";
            this.TextBox2.Text = "TextBox2";
            this.TextBox2.Top = 0F;
            this.TextBox2.Width = 2.063F;
            // 
            // TextBox3
            // 
            this.TextBox3.DataField = "Amount";
            this.TextBox3.Height = 0.2F;
            this.TextBox3.Left = 4.5625F;
            this.TextBox3.Name = "TextBox3";
            this.TextBox3.OutputFormat = resources.GetString("TextBox3.OutputFormat");
            this.TextBox3.Style = "text-align: right";
            this.TextBox3.Text = "TextBox3";
            this.TextBox3.Top = 0F;
            this.TextBox3.Width = 1F;
            // 
            // TextBox4
            // 
            this.TextBox4.DataField = "PaymentDate";
            this.TextBox4.Height = 0.2F;
            this.TextBox4.Left = 6.5F;
            this.TextBox4.Name = "TextBox4";
            this.TextBox4.OutputFormat = resources.GetString("TextBox4.OutputFormat");
            this.TextBox4.Style = "text-align: right";
            this.TextBox4.Text = "TextBox4";
            this.TextBox4.Top = 0F;
            this.TextBox4.Width = 1.0625F;
            // 
            // TextBox5
            // 
            this.TextBox5.DataField = "DoneString";
            this.TextBox5.Height = 0.2F;
            this.TextBox5.Left = 8.3125F;
            this.TextBox5.Name = "TextBox5";
            this.TextBox5.Style = "text-align: right; ddo-char-set: 204";
            this.TextBox5.Text = "TextBox5";
            this.TextBox5.Top = 0F;
            this.TextBox5.Width = 0.812F;
            // 
            // TextBox9
            // 
            this.TextBox9.DataField = "PaymentPercent";
            this.TextBox9.Height = 0.2F;
            this.TextBox9.Left = 7.5625F;
            this.TextBox9.Name = "TextBox9";
            this.TextBox9.OutputFormat = resources.GetString("TextBox9.OutputFormat");
            this.TextBox9.Style = "text-align: right";
            this.TextBox9.Text = "TextBox9";
            this.TextBox9.Top = 0F;
            this.TextBox9.Width = 0.75F;
            // 
            // TextBox10
            // 
            this.TextBox10.DataField = "AmountBGN";
            this.TextBox10.Height = 0.2F;
            this.TextBox10.Left = 5.563F;
            this.TextBox10.Name = "TextBox10";
            this.TextBox10.OutputFormat = resources.GetString("TextBox10.OutputFormat");
            this.TextBox10.Style = "text-align: right";
            this.TextBox10.Text = "TextBox10";
            this.TextBox10.Top = 0F;
            this.TextBox10.Width = 0.938F;
            // 
            // Line5
            // 
            this.Line5.Height = 0F;
            this.Line5.Left = 0.006944444F;
            this.Line5.LineWeight = 1F;
            this.Line5.Name = "Line5";
            this.Line5.Top = 0.2569444F;
            this.Line5.Width = 9.1875F;
            this.Line5.X1 = 0.006944444F;
            this.Line5.X2 = 9.194445F;
            this.Line5.Y1 = 0.2569444F;
            this.Line5.Y2 = 0.2569444F;
            // 
            // Line3
            // 
            this.Line3.Height = 0F;
            this.Line3.Left = 0F;
            this.Line3.LineWeight = 1F;
            this.Line3.Name = "Line3";
            this.Line3.Top = 0.0625F;
            this.Line3.Width = 9.1875F;
            this.Line3.X1 = 0F;
            this.Line3.X2 = 9.1875F;
            this.Line3.Y1 = 0.0625F;
            this.Line3.Y2 = 0.0625F;
            // 
            // txtPaydEUR
            // 
            this.txtPaydEUR.DataField = "Amount";
            this.txtPaydEUR.Height = 0.2F;
            this.txtPaydEUR.Left = 4.563F;
            this.txtPaydEUR.Name = "txtPaydEUR";
            this.txtPaydEUR.OutputFormat = resources.GetString("txtPaydEUR.OutputFormat");
            this.txtPaydEUR.Style = "font-weight: bold; text-align: right";
            this.txtPaydEUR.Top = 0.0625F;
            this.txtPaydEUR.Width = 1F;
            // 
            // Label19
            // 
            this.Label19.Height = 0.2F;
            this.Label19.HyperLink = null;
            this.Label19.Left = 0F;
            this.Label19.Name = "Label19";
            this.Label19.Style = "font-weight: bold";
            this.Label19.Text = "???? ???????";
            this.Label19.Top = 0.0625F;
            this.Label19.Width = 4.563F;
            // 
            // txtPaydBGN
            // 
            this.txtPaydBGN.DataField = "AmountBGN";
            this.txtPaydBGN.Height = 0.2F;
            this.txtPaydBGN.Left = 5.563F;
            this.txtPaydBGN.Name = "txtPaydBGN";
            this.txtPaydBGN.OutputFormat = resources.GetString("txtPaydBGN.OutputFormat");
            this.txtPaydBGN.Style = "font-weight: bold; text-align: right";
            this.txtPaydBGN.Top = 0.0625F;
            this.txtPaydBGN.Width = 0.938F;
            // 
            // txtNoPaydEUR
            // 
            this.txtNoPaydEUR.DataField = "Amount";
            this.txtNoPaydEUR.Height = 0.2F;
            this.txtNoPaydEUR.Left = 4.5625F;
            this.txtNoPaydEUR.Name = "txtNoPaydEUR";
            this.txtNoPaydEUR.OutputFormat = resources.GetString("txtNoPaydEUR.OutputFormat");
            this.txtNoPaydEUR.Style = "font-weight: bold; text-align: right";
            this.txtNoPaydEUR.Top = 0.25F;
            this.txtNoPaydEUR.Width = 1F;
            // 
            // Label20
            // 
            this.Label20.Height = 0.2F;
            this.Label20.HyperLink = null;
            this.Label20.Left = 0F;
            this.Label20.Name = "Label20";
            this.Label20.Style = "font-weight: bold";
            this.Label20.Text = "???? ?????????";
            this.Label20.Top = 0.25F;
            this.Label20.Width = 4.563F;
            // 
            // txtNOPaydBGN
            // 
            this.txtNOPaydBGN.DataField = "AmountBGN";
            this.txtNOPaydBGN.Height = 0.2F;
            this.txtNOPaydBGN.Left = 5.563F;
            this.txtNOPaydBGN.Name = "txtNOPaydBGN";
            this.txtNOPaydBGN.OutputFormat = resources.GetString("txtNOPaydBGN.OutputFormat");
            this.txtNOPaydBGN.Style = "font-weight: bold; text-align: right";
            this.txtNOPaydBGN.Top = 0.25F;
            this.txtNOPaydBGN.Width = 0.938F;
            // 
            // txtAllEUR
            // 
            this.txtAllEUR.DataField = "Amount";
            this.txtAllEUR.Height = 0.2F;
            this.txtAllEUR.Left = 4.563F;
            this.txtAllEUR.Name = "txtAllEUR";
            this.txtAllEUR.OutputFormat = resources.GetString("txtAllEUR.OutputFormat");
            this.txtAllEUR.Style = "font-weight: bold; text-align: right";
            this.txtAllEUR.Top = 0.4375F;
            this.txtAllEUR.Width = 1F;
            // 
            // Label21
            // 
            this.Label21.Height = 0.2F;
            this.Label21.HyperLink = null;
            this.Label21.Left = 0F;
            this.Label21.Name = "Label21";
            this.Label21.Style = "font-weight: bold";
            this.Label21.Text = "????";
            this.Label21.Top = 0.4375F;
            this.Label21.Width = 4.563F;
            // 
            // txtAllBGN
            // 
            this.txtAllBGN.DataField = "AmountBGN";
            this.txtAllBGN.Height = 0.2F;
            this.txtAllBGN.Left = 5.563F;
            this.txtAllBGN.Name = "txtAllBGN";
            this.txtAllBGN.OutputFormat = resources.GetString("txtAllBGN.OutputFormat");
            this.txtAllBGN.Style = "font-weight: bold; text-align: right";
            this.txtAllBGN.Top = 0.4375F;
            this.txtAllBGN.Width = 0.938F;
            // 
            // ActiveReport1
            // 
            this.MasterReport = false;
            this.PageSettings.PaperHeight = 11F;
            this.PageSettings.PaperWidth = 8.5F;
            this.PrintWidth = 9.125F;
            this.Sections.Add(this.ReportHeader);
            this.Sections.Add(this.GroupHeader1);
            this.Sections.Add(this.Detail);
            this.Sections.Add(this.GroupFooter1);
            this.Sections.Add(this.ReportFooter);
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule(resources.GetString("$this.StyleSheet"), "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: inherit; font-style: inherit; font-variant: inherit; font-weight: bo" +
                        "ld; font-size: 16pt; font-size-adjust: inherit; font-stretch: inherit", "Heading1", "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: Times New Roman; font-style: italic; font-variant: inherit; font-wei" +
                        "ght: bold; font-size: 14pt; font-size-adjust: inherit; font-stretch: inherit", "Heading2", "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: inherit; font-style: inherit; font-variant: inherit; font-weight: bo" +
                        "ld; font-size: 13pt; font-size-adjust: inherit; font-stretch: inherit", "Heading3", "Normal"));
            ((System.ComponentModel.ISupportInitialize)(this.Label22)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label23)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label24)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label25)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label26)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label27)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label28)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPaymentID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSubprojectID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TextBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TextBox2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TextBox3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TextBox4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TextBox5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TextBox9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TextBox10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPaydEUR)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label19)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPaydBGN)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtNoPaydEUR)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label20)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtNOPaydBGN)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAllEUR)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label21)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAllBGN)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();

            // Attach Report Events
            this.Detail.Format += new System.EventHandler(this.Detail_Format);
            this.ReportFooter.Format += new System.EventHandler(this.ReportFooter_Format);
        }

		#endregion

        private ReportHeader ReportHeader;
        private Label Label22;
        private Label Label23;
        private Label Label24;
        private Line Line6;
        private Label Label25;
        private Label Label26;
        private Label Label27;
        private Label Label28;
        private GroupHeader GroupHeader1;
        private Detail Detail;
        private TextBox txtPaymentID;
        private TextBox txtSubprojectID;
        private TextBox TextBox1;
        private TextBox TextBox2;
        private TextBox TextBox3;
        private TextBox TextBox4;
        private TextBox TextBox5;
        private TextBox TextBox9;
        private TextBox TextBox10;
        private Line Line5;
        private GroupFooter GroupFooter1;
        private ReportFooter ReportFooter;
        private Line Line3;
        private TextBox txtPaydEUR;
        private Label Label19;
        private TextBox txtPaydBGN;
        private TextBox txtNoPaydEUR;
        private Label Label20;
        private TextBox txtNOPaydBGN;
        private TextBox txtAllEUR;
        private Label Label21;
        private TextBox txtAllBGN;

		
	}
}
