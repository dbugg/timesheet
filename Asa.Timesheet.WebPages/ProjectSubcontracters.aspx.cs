using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Data.SqlClient;
using DataDynamics.ActiveReports.Export.Pdf;
using Asa.Timesheet.Data;
using Asa.Timesheet.Data.Entities;
using log4net;
using Asa.Timesheet.WebPages.UserControls;

namespace Asa.Timesheet.WebPages
{
	/// <summary>
	/// Summary description for ProjectSubcontracters.
	/// </summary>
	public class ProjectSubcontracters : TimesheetPageBase
	{
		protected System.Web.UI.WebControls.PlaceHolder menuHolder;
		protected System.Web.UI.WebControls.Label lblError;
		protected System.Web.UI.WebControls.Label lblInfo;
		protected System.Web.UI.WebControls.DataGrid grdProjects;
		protected System.Web.UI.WebControls.Panel grid;
		protected System.Web.UI.WebControls.Button btnNewProject;
		protected Asa.Timesheet.WebPages.UserControls.PageHeader header;
		protected System.Web.UI.WebControls.Button btnProject;
		protected System.Web.UI.WebControls.Button btnSubc;
		protected System.Web.UI.WebControls.ImageButton ibPdfExport;
		private static readonly ILog log = LogManager.GetLogger(typeof(ProjectSubcontracters));
		private enum GridColumns
		{
			Number=0,
			SubcontracterName,
			SubcontracterType,
			StartDate,
			EndDate,
			Area,
			Rate,
			TotalAmount,
			Done,
			EditItem,
			Letter,
			DeleteItem
		}
		#region Menu
//		private void CreateMenu()
//		{
//			UserControls.MenuTable menu = new UserControls.MenuTable();
//			menu.ID = "MenuTable";
//
//			ArrayList menuItems = new ArrayList();
//			menuItems.Add(new MenuItemInfo(Resource.ResourceManager["menuItem_Hours"], "Hours.aspx", true));
//			menuItems.Add(new MenuItemInfo(Resource.ResourceManager["menuItem_Projects"], "Projects.aspx"));
//			if ((LoggedUser.IsLeader) || (LoggedUser.IsAssistant)) 
//			{
//				menuItems.Add(new MenuItemInfo(Resource.ResourceManager["menuItem_Clients"], "Clients.aspx"));
//				
//				menuItems.Add(new MenuItemInfo(Resource.ResourceManager["menuItem_Subcontracters"], "Subcontracters.aspx"));
//				if (LoggedUser.IsLeader)
//					menuItems.Add(new MenuItemInfo(Resource.ResourceManager["menuItem_Users"], "Users.aspx"));			
//			}
//			menuItems.Add(new MenuItemInfo(Resource.ResourceManager["menuItem_Emails"], "Emails.aspx"));
//			menu.AddMenuGroup(Resource.ResourceManager["menuHeader_Links"], 20, menuItems);
//			
//			//2 group
//			menuItems = new ArrayList();
//			menuItems.Add(new MenuItemInfo(Resource.ResourceManager["menuItem_NewProject"], "EditProject.aspx"));
//			if ((LoggedUser.IsLeader) || (LoggedUser.IsAssistant)) 
//			{
//				menuItems.Add(new MenuItemInfo(Resource.ResourceManager["menuItem_NewClient"], "EditClient.aspx"));
//				if (LoggedUser.IsLeader)
//					menuItems.Add(new MenuItemInfo(Resource.ResourceManager["menuItem_NewUser"], "EditUser.aspx"));		
//			}
//			menuItems.Add(new MenuItemInfo(Resource.ResourceManager["menuItem_NewEmail"], "EditEmail.aspx"));
//			menuItems.Add(new MenuItemInfo(Resource.ResourceManager["newcontracter_PageTitle"], "EditSubContracter.aspx"));
//
//			menu.AddMenuGroup(Resource.ResourceManager["menuHeader_New"], 30, menuItems);
//
//			menuItems = new ArrayList();
//			menuItems.Add(new MenuItemInfo(Resource.ResourceManager["menuItem_WorkTimeReport"], "reports/worktimes.aspx"));
//			menuItems.Add(new MenuItemInfo(Resource.ResourceManager["menuItem_MinutesReport"], "Reports/Minutes.aspx"));
//			if (LoggedUser.IsLeader || LoggedUser.IsAssistant)
//				menuItems.Add(new MenuItemInfo(Resource.ResourceManager["menuItem_MailReport"], "MailForm.aspx"));
//			if (LoggedUser.IsLeader)
//			{
//				menuItems.Add(new MenuItemInfo(Resource.ResourceManager["menuItem_NotEnteredWorkTime"], "reports/EmptyHours.aspx"));
//				menuItems.Add(new MenuItemInfo(Resource.ResourceManager["menuItem_Calendar"], "reports/Calendar.aspx"));
//			}
//
//			menu.AddMenuGroup(Resource.ResourceManager["menuHeader_Reports"], 30, menuItems);
//			
//			menuItems = new ArrayList();
//			menuItems.Add(new MenuItemInfo(Resource.ResourceManager["menuItem_Help"], SessionManager.GetHelpLink(),true,Pages.ProjectSubcontracters));
//			menu.AddMenuGroup("", 10, menuItems);
//
//			menuHolder.Controls.Add(menu);
//		}
		#endregion
		public string GetImage(object o)
		{
			if(o ==DBNull.Value)
				return "images/delete.png";
			bool b =(bool)o;
			if(b)
				return "images/done.gif";
			else
				return "images/delete.png";
		}
		private void Page_Load(object sender, System.EventArgs e)
		{
			if (!LoggedUser.HasRole) ErrorRedirect(Resource.ResourceManager["noRightsAlert"]);
			//SetDone();
			string projectName, projectCode, administrativeName,add;
			decimal area=0;int clientID=0;
			int PID=UIHelpers.GetPIDParam();
			bool phases;
			if (!ProjectsData.SelectProject(PID, out projectName, out  projectCode, out administrativeName, out area, out add, out clientID,out phases))
			{
				log.Error("Project not found " + PID);
				
			}
			UIHelpers.CreateMenu(menuHolder,LoggedUser);
			
			if (!this.IsPostBack)
			{
				header.PageTitle = Resource.ResourceManager["prsubcontracters_PageTitle"]+" "+ projectName;//+projectCode+ " "+administrativeName;
				header.UserName = LoggedUser.UserName;
				btnNewProject.Text = Resource.ResourceManager["prsubcontracters_btnNewProject"];

				if (!LoggedUser.HasPaymentRights)
				{
					grdProjects.Columns[(int)GridColumns.Rate].Visible = false;
					grdProjects.Columns[(int)GridColumns.TotalAmount].Visible = false;
					grdProjects.Columns[(int)GridColumns.DeleteItem].Visible = false;
				}
				BindGrid();
			}
			if( LoggedUser.HasPaymentRights)
			{
				CheckForError();
			}
		}

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
			this.grdProjects.ItemCommand += new System.Web.UI.WebControls.DataGridCommandEventHandler(this.grdProjects_EditCommand);
			this.btnNewProject.Click += new System.EventHandler(this.btnNewProject_Click);
			this.btnProject.Click += new System.EventHandler(this.Button1_Click);
			this.btnSubc.Click += new System.EventHandler(this.btnSubc_Click);
			this.ibPdfExport.Click += new System.Web.UI.ImageClickEventHandler(this.ibPdfExport_Click);
			this.Load += new System.EventHandler(this.Page_Load);

		}
		#endregion
		private void SetDone()
		{
			//			return;
			string sTarget=this.Page.Request["__EVENTTARGET"];
			string sArg = this.Page.Request["__EVENTARGUMENT"];
			if(sTarget==null || sTarget=="" || !sTarget.EndsWith("ckDone")
				)
				return;
			if(sArg==null || sArg=="")
				return;
			int nID = int.Parse(sArg);
			bool bChecked = this.Page.Request.Form[sTarget]!=null;//UIHelpers.GetSenderValue(grd,(int)Columns.ToSend,nID,"ckSend");
			if(nID>0)
			{
				//TODO Set Done
				Response.Redirect("ProjectSubcontracters.aspx"+"?pid="+UIHelpers.GetPIDParam());
				
				
			}
		}
		private void BindGrid()
		{
		

			try
			{
				
				grdProjects.DataSource= ProjectSubcontracterDAL.LoadCollection("ProjectSubcontractersListProc1",SQLParms.CreateProjectSubcontractersListProc1 (UIHelpers.GetPIDParam(),LoggedUser.HasPaymentRights));
				grdProjects.DataKeyField="ProjectSubcontracterID";
				grdProjects.DataBind();
			}
			catch (Exception ex)
			{
				log.Info(ex);
				lblError.Text = Resource.ResourceManager["prsubcontracters_ErrorLoadProjects"];
				return;
			}
		

			if (grdProjects.Items.Count == 0) 
			{
				grid.Visible = false;
				lblInfo.Text = Resource.ResourceManager["prsubcontracters_NoActiveProjects"];
			}
			else grid.Visible = true;

			try
			{
				SetConfirmDelete(grdProjects, "btnDelete", 1);
			}
			catch (Exception ex)
			{
				log.Error(ex);
			}
		}

		private void btnNewProject_Click(object sender, System.EventArgs e)
		{
			Response.Redirect("EditProjectSubContracter.aspx?pid="+UIHelpers.GetPIDParam());
		}
		private void Letter(int ID)
		{
			ProjectSubcontracterData psd= ProjectSubcontracterDAL.Load(ID);
			if(psd!=null)
			{
				SubcontracterData sd = SubcontracterDAL.Load(psd.SubcontracterID);
				string projectName, projectCode, administrativeName,add;
				decimal area=0;int clientID=0;
				int PID=UIHelpers.GetPIDParam();
				bool phases;
				if (!ProjectsData.SelectProject(PID, out projectName, out  projectCode, out administrativeName, out area, out add, out clientID,out phases))
				{
					log.Error("Project not found " + PID);
				
				}
				string sLast = sd.Manager;
				string[] ss=sd.Manager.Split();
				if(ss.Length>1)
					sLast= ss[ss.Length-1];
				UIHelpers.Subcontracter(this.Page,administrativeName,sd.SubcontracterName,sd.Manager,sd.Address,sLast);
			}
		}
		private void grdProjects_EditCommand(object source, System.Web.UI.WebControls.DataGridCommandEventArgs e)
		{
			if(e.CommandName=="Edit")
			{
				string sProjectID = ((DataGrid)source).DataKeys[e.Item.ItemIndex].ToString();
				Response.Redirect("EditProjectSubContracter.aspx"+"?sid="+sProjectID+"&pid="+UIHelpers.GetPIDParam()); 
				return;
			}
			ImageButton ib = (ImageButton)e.CommandSource;
			string s = ib.ID;
			switch (ib.ID)
			{
				case "btnLetter": int ID = (int)((DataGrid)source).DataKeys[e.Item.ItemIndex];
					
					
					Letter(ID);
					break;
				case "btnDelete": int projectID = (int)((DataGrid)source).DataKeys[e.Item.ItemIndex];
					ProjectSubcontracterDAL.Delete(projectID);
					
					BindGrid();
					break;
				case "btnEdit": string sProjectID = ((DataGrid)source).DataKeys[e.Item.ItemIndex].ToString();
					Response.Redirect("EditProjectSubContracter.aspx"+"?sid="+sProjectID+"&pid="+UIHelpers.GetPIDParam()); break;
			}
		}

		private void Button1_Click(object sender, System.EventArgs e)
		{
			Response.Redirect("EditProject.aspx?pid="+UIHelpers.GetPIDParam());
		}

		private void btnSubc_Click(object sender, System.EventArgs e)
		{
			Response.Redirect("Subprojects.aspx?pid="+UIHelpers.GetPIDParam());
		}

		private void ibPdfExport_Click(object sender, System.Web.UI.ImageClickEventArgs e)
		{
			PdfExport pdf = new PdfExport();
			pdf.NeverEmbedFonts = "";
			
			int projectID = this.ProjectID;

			DataDynamics.ActiveReports.ActiveReport report = null;

			try
			{
				bool hpr = LoggedUser.HasPaymentRights; 
				report = new Asa.Timesheet.WebPages.Reports.ProjectSubcontracters(projectID, this.LoggedUser.UserName,false,hpr);
				report.Run();
			}
			catch (Exception ex)
			{
				log.Error(ex);
				return;
			}
      

			System.IO.MemoryStream memoryFile = new System.IO.MemoryStream();
			pdf.Export(report.Document, memoryFile);
		
			Response.Clear();
			Response.AppendHeader( "content-disposition"
				,"attachment; filename="+"Subcontracters.pdf");
			Response.ContentType = "application/pdf";
				
			memoryFile.WriteTo(Response.OutputStream); 
			Response.End();	
		}

		#region private properties

		private int ProjectID
		{
			get 
			{
       
				try
				{
					return int.Parse(Request.Params["pid"]);				
				}
				catch 
				{
					return -1;
				}
			}		
		} 
		#endregion
		private void CheckForError()
		{
			for(int i=0;i<grdProjects.Items.Count;i++)
			{
				int psID = (int) grdProjects.DataKeys[grdProjects.Items[i].ItemIndex];
				ProjectSubcontracterData sd = ProjectSubcontracterDAL.Load(psID);
				PaymentsVector pv = PaymentDAL.LoadCollection("PaymentsListBySubcontracterProc", SQLParms.CreatePaymentsListBySubcontracterProc(psID));
				decimal dEUR=0;
				for(int j=0;j<pv.Count;j++)
				{
//					am = SetRec(sd.Rate,(PaymentSchemes)sd.PaymentSchemeID,(decimal)sd.Area,pv[j].PaymentPercent);
					dEUR+=pv[j].Amount;
				}
				PaymentSchemes ps =(PaymentSchemes)sd.PaymentSchemeID;
				if(ps==PaymentSchemes.Fixed)
					sd.TotalAmount=sd.Rate;
				else if(ps==PaymentSchemes.Area)
						sd.TotalAmount=sd.Rate*(decimal)sd.Area;
				if(dEUR!=sd.TotalAmount)
				{
					lblError.Text=string.Concat(Resource.ResourceManager["editsubprojects_errorTotal"]," B",i+1);
					break;
				}
			}
		}
//		private decimal SetRec(int rate,PaymentSchemes ps,decimal area,decimal per)
//		{
//			decimal EUR=0;
//			
//			if(ps==PaymentSchemes.Fixed)
//				EUR=rate;
//			else if(ps==PaymentSchemes.Area)
//				EUR=rate*area;
//			decimal fix=decimal.Parse( System.Configuration.ConfigurationSettings.AppSettings["fixedBGNEUR"]);
//			
//			
//			
//			decimal dTotal=0;
//			
//				
//				
//
//			
//				
//				
//				
//				
//				if(txtAmount!=null )
//				{
//
//					if(txtAmount.Text=="")
//					{
//						if(i==dlPayments.Items.Count-1 && txtPer.Text=="")
//						{
//							if(EUR>0)
//							{
//								if(!ckPaid.Checked)
//									txtAmount.Text=UIHelpers.FormatDecimal2(EUR-dTotal);
//								decimal d = UIHelpers.ParseDecimal(txtAmount.Text);
//								per= d/EUR;
//								txtPer.Text=UIHelpers.FormatDecimal4(per*100);
//							}
//						}
//						else
//						{
//							if(!ckPaid.Checked)
//								txtAmount.Text=UIHelpers.FormatDecimal2((decimal)per * EUR/100);
//						}
//					}
//					else if(txtPer.Text=="")
//					{
//						decimal d = UIHelpers.ParseDecimal(txtAmount.Text);
//						per= d/EUR;
//						txtPer.Text=UIHelpers.FormatDecimal4(per*100);
//					}
//					else 
//					{
//						if(!ckPaid.Checked)
//							txtAmount.Text=UIHelpers.FormatDecimal2((decimal)per * EUR/100);
//					}
//					dTotal+=UIHelpers.ParseDecimal(txtAmount.Text);
//		
//				}
//
//				
//			}
//		}
	}
}
