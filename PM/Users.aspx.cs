using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using Asa.Timesheet.Data;
using log4net;
using log4net.Config;
using System.Data.SqlClient;

namespace PM
{
	/// <summary>
	/// Summary description for Users.
	/// </summary>
	public class Users : BasePage
	{
		#region WebControls
	
		protected System.Web.UI.WebControls.Label lblError;
		protected System.Web.UI.WebControls.Label lblInfo;
		protected System.Web.UI.WebControls.Panel grid;
		protected System.Web.UI.WebControls.Button btnNew;
		protected System.Web.UI.WebControls.DataGrid grdAccountsSubcontracters;
		protected System.Web.UI.WebControls.DataGrid grdAccountsClients;
		protected System.Web.UI.WebControls.DataGrid grdAccountsBuilders;
		protected System.Web.UI.WebControls.DataGrid grdAccountsWatchers;
		protected System.Web.UI.WebControls.DataGrid grdAccountsOthers;
		protected System.Web.UI.WebControls.Label lbBuilderName;
		protected System.Web.UI.WebControls.Label lbWatcherName;
		protected PM.UserControls.PageHeader header;
		protected System.Web.UI.WebControls.Label lbType;
		protected System.Web.UI.WebControls.DropDownList ddlProjectStatus;
		protected System.Web.UI.WebControls.Label lbBuildingType;
		protected System.Web.UI.WebControls.DropDownList ddlBuildingTypes;
		protected System.Web.UI.WebControls.Label lbProjectName;
		protected System.Web.UI.WebControls.DropDownList ddlProjects;
		protected System.Web.UI.WebControls.Button btnSearch;
		protected System.Web.UI.WebControls.Button btnClear;
		protected System.Web.UI.WebControls.Label lbClientName;
		protected System.Web.UI.WebControls.Label lbSubName;
		protected System.Web.UI.WebControls.TextBox txtProjectPartName;
		protected System.Web.UI.WebControls.Label lbProjectPartName;
		protected System.Web.UI.WebControls.Label lbOtherName;
		private static readonly ILog log = LogManager.GetLogger(typeof(Users));
		
		#endregion

		#region enums

		private enum gridColomnsSub
		{
			Number = 0,
			SubType,
			FullName,
			AccountName,
			UserTypeHidden,
			Mail,
			Address,
			Phone
		}
		private enum gridColomns
		{
			Number = 0,
			FullName,
			AccountName,
			//UserType = 3,
			UserTypeHidden,
			Mail,
			Address,
			Phone
		}

	
		#endregion

		#region Page_Load

        private void Page_Load(object sender, System.EventArgs e)
        {
            if (!(LoggedUser.UserRoleID > 0)) ErrorRedirect(UIHelpers.GetText(Language, "noRightsAlert"));
            //notes:set bool b to show if the userRole gives user to edit more than his pass
            //			bool b = (LoggedUser. >0);//== (int)Roles.Director);
            //			if((!LoggedUser.IsUser)&&(!b)) Response.Redirect("EditUser.aspx"+"?id="+LoggedUser.UserID+"&pid="+LoggedUser.UserType); 

            LoadResourses();
            if (!this.IsPostBack)
            {

                //
                BindGrid();

            }

        }


        private void LoadResourses()
        {
            string lang = Language;
            btnNew.Text = UIHelpers.GetText(lang, "Name_btnNewUserName");

            btnSearch.Text = UIHelpers.GetText(lang, "Name_btnSearchName");
            btnClear.Text = UIHelpers.GetText(lang, "Name_btnCleanName");
            //lbProjectName.Text = UIHelpers.GetText(lang,"Name_lbProjectName");
            lbSubName.Text = UIHelpers.GetText(lang, "Name_gridNameSubName");
            lbClientName.Text = UIHelpers.GetText(lang, "Name_gridNameClientName");
            lbBuilderName.Text = UIHelpers.GetText(lang, "Name_gridNameBuilderName");
            lbWatcherName.Text = UIHelpers.GetText(lang, "Name_gridNameWatcherName");
            lbOtherName.Text = UIHelpers.GetText(lang, "Name_gridNameOtherName");
            //lbType.Text = UIHelpers.GetText(lang,"Name_TypeName");
            //lbBuildingType.Text  = UIHelpers.GetText(lang,"Name_BuildingTypeName");
            //lbProjectPartName.Text = UIHelpers.GetText(lang,"Name_lbProjectPartNameName");
            txtProjectPartName.Attributes.Add("placeholder", UIHelpers.GetText(lang, "Name_lbProjectPartNameName"));


            grdAccountsSubcontracters.Columns[(int)gridColomnsSub.SubType].HeaderText = UIHelpers.GetText(lang, "Name_FullNameColomn");
            grdAccountsSubcontracters.Columns[(int)gridColomnsSub.FullName].HeaderText = UIHelpers.GetText(lang, "Name_SubNameColomn");
            grdAccountsSubcontracters.Columns[(int)gridColomnsSub.AccountName].HeaderText = UIHelpers.GetText(lang, "Name_AccountNameColomn");
            grdAccountsSubcontracters.Columns[(int)gridColomnsSub.Address].HeaderText = UIHelpers.GetText(lang, "Name_AddressNameColomn");
            grdAccountsSubcontracters.Columns[(int)gridColomnsSub.Mail].HeaderText = UIHelpers.GetText(lang, "Name_MailNameColomn");
            grdAccountsSubcontracters.Columns[(int)gridColomnsSub.Phone].HeaderText = UIHelpers.GetText(lang, "Name_PhoneNameColomn");

            grdAccountsClients.Columns[(int)gridColomns.FullName].HeaderText = UIHelpers.GetText(lang, "Name_SubNameColomn");
            grdAccountsClients.Columns[(int)gridColomns.AccountName].HeaderText = UIHelpers.GetText(lang, "Name_AccountNameColomn");
            grdAccountsClients.Columns[(int)gridColomns.Address].HeaderText = UIHelpers.GetText(lang, "Name_AddressNameColomn");
            grdAccountsClients.Columns[(int)gridColomns.Mail].HeaderText = UIHelpers.GetText(lang, "Name_MailNameColomn");
            grdAccountsClients.Columns[(int)gridColomns.Phone].HeaderText = UIHelpers.GetText(lang, "Name_PhoneNameColomn");

            grdAccountsBuilders.Columns[(int)gridColomns.FullName].HeaderText = UIHelpers.GetText(lang, "Name_SubNameColomn");
            grdAccountsBuilders.Columns[(int)gridColomns.AccountName].HeaderText = UIHelpers.GetText(lang, "Name_AccountNameColomn");
            grdAccountsBuilders.Columns[(int)gridColomns.Address].HeaderText = UIHelpers.GetText(lang, "Name_AddressNameColomn");
            grdAccountsBuilders.Columns[(int)gridColomns.Mail].HeaderText = UIHelpers.GetText(lang, "Name_MailNameColomn");
            grdAccountsBuilders.Columns[(int)gridColomns.Phone].HeaderText = UIHelpers.GetText(lang, "Name_PhoneNameColomn");

            grdAccountsOthers.Columns[(int)gridColomns.FullName].HeaderText = UIHelpers.GetText(lang, "Name_SubNameColomn");
            grdAccountsOthers.Columns[(int)gridColomns.AccountName].HeaderText = UIHelpers.GetText(lang, "Name_AccountNameColomn");
            grdAccountsOthers.Columns[(int)gridColomns.Address].HeaderText = UIHelpers.GetText(lang, "Name_AddressNameColomn");
            grdAccountsOthers.Columns[(int)gridColomns.Mail].HeaderText = UIHelpers.GetText(lang, "Name_MailNameColomn");
            grdAccountsOthers.Columns[(int)gridColomns.Phone].HeaderText = UIHelpers.GetText(lang, "Name_PhoneNameColomn");

            grdAccountsWatchers.Columns[(int)gridColomns.FullName].HeaderText = UIHelpers.GetText(lang, "Name_SubNameColomn");
            grdAccountsWatchers.Columns[(int)gridColomns.AccountName].HeaderText = UIHelpers.GetText(lang, "Name_AccountNameColomn");
            grdAccountsWatchers.Columns[(int)gridColomns.Address].HeaderText = UIHelpers.GetText(lang, "Name_AddressNameColomn");
            grdAccountsWatchers.Columns[(int)gridColomns.Mail].HeaderText = UIHelpers.GetText(lang, "Name_MailNameColomn");
            grdAccountsWatchers.Columns[(int)gridColomns.Phone].HeaderText = UIHelpers.GetText(lang, "Name_PhoneNameColomn");

            Header.PageTitle = UIHelpers.GetText(lang, "pmPageTitle_Users");
            //TODO
        }


		#endregion

		#region Web Form Designer generated code
        override protected void OnInit(EventArgs e)
        {
            //
            // CODEGEN: This call is required by the ASP.NET Web Form Designer.
            //
            InitializeComponent();
            base.OnInit(e);
        }
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
        private void InitializeComponent()
        {
            this.ddlProjectStatus.SelectedIndexChanged += new System.EventHandler(this.ddlProjectStatus_SelectedIndexChanged);
            this.ddlBuildingTypes.SelectedIndexChanged += new System.EventHandler(this.ddlBuildingTypes_SelectedIndexChanged);
            this.btnSearch.Click += new System.EventHandler(this.btnSearch_Click);
            this.btnClear.Click += new System.EventHandler(this.btnClear_Click);
            this.grdAccountsSubcontracters.ItemCommand += new System.Web.UI.WebControls.DataGridCommandEventHandler(this.grdUsersSub_ItemCommand);
            this.grdAccountsSubcontracters.SortCommand += new System.Web.UI.WebControls.DataGridSortCommandEventHandler(this.grdAccountsSubcontracters_SortCommand);
            this.grdAccountsClients.ItemCommand += new System.Web.UI.WebControls.DataGridCommandEventHandler(this.grdUsers_ItemCommand);
            this.grdAccountsClients.SortCommand += new System.Web.UI.WebControls.DataGridSortCommandEventHandler(this.grdAccountsClients_SortCommand);
            this.grdAccountsBuilders.ItemCommand += new System.Web.UI.WebControls.DataGridCommandEventHandler(this.grdUsers_ItemCommand);
            this.grdAccountsBuilders.SortCommand += new System.Web.UI.WebControls.DataGridSortCommandEventHandler(this.grdAccountsBuilders_SortCommand);
            this.grdAccountsWatchers.ItemCommand += new System.Web.UI.WebControls.DataGridCommandEventHandler(this.grdUsers_ItemCommand);
            this.grdAccountsWatchers.SortCommand += new System.Web.UI.WebControls.DataGridSortCommandEventHandler(this.grdAccountsWatchers_SortCommand);
            this.grdAccountsOthers.ItemCommand += new System.Web.UI.WebControls.DataGridCommandEventHandler(this.grdUsers_ItemCommand);
            this.grdAccountsOthers.SortCommand += new System.Web.UI.WebControls.DataGridSortCommandEventHandler(this.grdAccountsOthers_SortCommand);
            this.btnNew.Click += new System.EventHandler(this.btnNew_Click);
            this.Load += new System.EventHandler(this.Page_Load);

        }
		#endregion

		#region DataBase

//		private bool LoadBuildingTypes(string selectedValue,DropDownList ddl)
//		{
//			SqlDataReader reader = null;
//			try
//			{
//				reader = DBManager.SelectBuildingTypes();
//				ddl.DataSource = reader;
//				ddl.DataValueField = "BuildingTypeID";
//				ddl.DataTextField = "BuildingType";
//				ddl.DataBind();
//				if(ddl.Items.FindByValue(System.Configuration.ConfigurationManager.AppSettings["BuildingTypeNotInPM"])!=null)
//					ddl.Items.Remove(ddl.Items.FindByValue(System.Configuration.ConfigurationManager.AppSettings["BuildingTypeNotInPM"]));
//				ddl.Items.Insert(0, new ListItem(Resource.ResourceManager["reports_typeBuilding"], "0"));
//				ddl.SelectedValue = selectedValue;
//			}
//			catch (Exception ex)
//			{
//				log.Error(UIHelpers.GetLogErrorMessage(ex));
//				return false;
//			}
//			finally 
//			{
//				if (reader!=null) reader.Close();
//			}
//
//			return true;
//		}


        private void BindGrid()
        {
            UIHelpers.LoadProjectStatus(ddlProjectStatus, LoggedUser.HasPaymentRights);
            UIHelpers.LoadBuildingTypes(ddlBuildingTypes);
            UIHelpers.LoadProjects(ddlProjects, ddlProjectStatus, ddlBuildingTypes);

            BindGridTableAll((int)PMUserType.Subcontracter);
            BindGridTableAll((int)PMUserType.Client);
            BindGridTableAll((int)PMUserType.Builder);
            BindGridTableAll((int)PMUserType.PMUser);
            BindGridTableAll((int)PMUserType.Supervisors);
        }


        private void BindGridTable(int pMUser)
        {
            switch (pMUser)
            {
                case ((int)PMUserType.Subcontracter):
                    {
                        grdAccountsSubcontracters.DataSource = UsersData.SelectUsersPM(true, (int)PMUserType.Subcontracter, int.Parse(ddlProjectStatus.SelectedValue), UIHelpers.ToInt(ddlProjects.SelectedValue), int.Parse(ddlBuildingTypes.SelectedValue)/*, ddlProjectStatus.SelectedValue=="2"*/, true, txtProjectPartName.Text);
                        grdAccountsSubcontracters.DataKeyField = "AccountID";
                        grdAccountsSubcontracters.DataBind();
                        if (grdAccountsSubcontracters.Items.Count == 0)
                        {

                        }
                        break;
                    }
                case ((int)PMUserType.Client):
                    {
                        grdAccountsClients.DataSource = UsersData.SelectUsersPM(!LoggedUser.HasPaymentRights, (int)PMUserType.Client, int.Parse(ddlProjectStatus.SelectedValue), UIHelpers.ToInt(ddlProjects.SelectedValue), int.Parse(ddlBuildingTypes.SelectedValue)/*, ddlProjectStatus.SelectedValue=="2"*/, true, txtProjectPartName.Text);
                        grdAccountsClients.DataKeyField = "AccountID";
                        grdAccountsClients.DataBind();
                        break;
                    }
                case ((int)PMUserType.Builder):
                    {
                        grdAccountsBuilders.DataSource = UsersData.SelectUsersPM(!LoggedUser.HasPaymentRights, (int)PMUserType.Builder, int.Parse(ddlProjectStatus.SelectedValue), UIHelpers.ToInt(ddlProjects.SelectedValue), int.Parse(ddlBuildingTypes.SelectedValue)/*, ddlProjectStatus.SelectedValue=="2"*/, true, txtProjectPartName.Text);
                        grdAccountsBuilders.DataKeyField = "AccountID";
                        grdAccountsBuilders.DataBind();
                        break;
                    }
                case ((int)PMUserType.PMUser):
                    {
                        grdAccountsOthers.DataSource = UsersData.SelectUsersPM(!LoggedUser.HasPaymentRights, (int)PMUserType.PMUser, int.Parse(ddlProjectStatus.SelectedValue), UIHelpers.ToInt(ddlProjects.SelectedValue), int.Parse(ddlBuildingTypes.SelectedValue)/*, ddlProjectStatus.SelectedValue=="2"*/, true, txtProjectPartName.Text);
                        grdAccountsOthers.DataKeyField = "AccountID";
                        grdAccountsOthers.DataBind();
                        break;
                    }
                case ((int)PMUserType.Supervisors):
                    {
                        grdAccountsWatchers.DataSource = UsersData.SelectUsersPM(!LoggedUser.HasPaymentRights, (int)PMUserType.Supervisors, int.Parse(ddlProjectStatus.SelectedValue), UIHelpers.ToInt(ddlProjects.SelectedValue), int.Parse(ddlBuildingTypes.SelectedValue)/*, ddlProjectStatus.SelectedValue=="2"*/, true, txtProjectPartName.Text);
                        grdAccountsWatchers.DataKeyField = "AccountID";
                        grdAccountsWatchers.DataBind();
                        break;
                    }

                default:
                    {
                        break;
                    }
            }
        }


        private void BindGridTableAll(int pMUser)
        {
            switch (pMUser)
            {
                case ((int)PMUserType.Subcontracter):
                    {
                        grdAccountsSubcontracters.DataSource = UsersData.SelectUsersPMAll(true, (int)PMUserType.Subcontracter, (string)ViewState["SortSubcontracters"]);
                        grdAccountsSubcontracters.DataKeyField = "AccountID";
                        grdAccountsSubcontracters.DataBind();
                        if (grdAccountsSubcontracters.Items.Count == 0)
                        {

                        }
                        break;
                    }
                case ((int)PMUserType.Client):
                    {
                        grdAccountsClients.DataSource = UsersData.SelectUsersPMAll(!LoggedUser.HasPaymentRights, (int)PMUserType.Client, (string)ViewState["SortClients"]);
                        grdAccountsClients.DataKeyField = "AccountID";
                        grdAccountsClients.DataBind();
                        break;
                    }
                case ((int)PMUserType.Builder):
                    {
                        grdAccountsBuilders.DataSource = UsersData.SelectUsersPMAll(!LoggedUser.HasPaymentRights, (int)PMUserType.Builder, (string)ViewState["SortBuilders"]);
                        grdAccountsBuilders.DataKeyField = "AccountID";
                        grdAccountsBuilders.DataBind();
                        break;
                    }
                case ((int)PMUserType.PMUser):
                    {
                        grdAccountsOthers.DataSource = UsersData.SelectUsersPMAll(!LoggedUser.HasPaymentRights, (int)PMUserType.PMUser, (string)ViewState["SortPMUsers"]);
                        grdAccountsOthers.DataKeyField = "AccountID";
                        grdAccountsOthers.DataBind();
                        break;
                    }
                case ((int)PMUserType.Supervisors):
                    {
                        grdAccountsWatchers.DataSource = UsersData.SelectUsersPMAll(!LoggedUser.HasPaymentRights, (int)PMUserType.Supervisors, (string)ViewState["SortSupervisors"]);
                        grdAccountsWatchers.DataKeyField = "AccountID";
                        grdAccountsWatchers.DataBind();
                        break;
                    }
                default:
                    {
                        break;
                    }
            }
        }

	
//		private bool LoadProjects(DropDownList ddlProjects,DropDownList ddlProjectsStatus,DropDownList ddlBuildingTypes)
//		{
//		
//			SqlDataReader reader = null;
//			
//			try
//			{
//				ddlProjects.Items.Clear();
//				ddlProjects.Items.Add(new ListItem("<"+Resource.ResourceManager["reports_ddlAllProjects"]+">","0"));
//				
//				reader = ProjectsData.SelectProjectNamesClear((ProjectsData.ProjectsByStatus)int.Parse(ddlProjectsStatus.SelectedValue), int.Parse(ddlBuildingTypes.SelectedValue));
//			
//				while (reader.Read())
//				{
//					int projectID = reader.GetInt32(0);
//					ddlProjects.Items.Add(new ListItem(reader.GetString(1), projectID.ToString()));
//					
//				}
//			}
//			catch (Exception ex)
//			{
//				log.Error(ex);
//				return false;
//			}
//
//			finally
//			{
//				if (reader!=null) reader.Close();
//			}
//			
//			
//			return true;
//		}


		#endregion

		#region EventHandlers

        private void grdUsersSub_ItemCommand(object source, System.Web.UI.WebControls.DataGridCommandEventArgs e)
        {

            if (e.CommandName == "Edit")
            {
                string sUserID = ((DataGrid)source).DataKeys[e.Item.ItemIndex].ToString();
                string UserType = (e.Item.Cells[(int)gridColomnsSub.UserTypeHidden].Text);
                Response.Redirect("EditUser.aspx" + "?id=" + sUserID + "&pid=" + UserType);
                return;
            }
        }

        private void grdUsers_ItemCommand(object source, System.Web.UI.WebControls.DataGridCommandEventArgs e)
        {

            if (e.CommandName == "Edit")
            {
                string sUserID = ((DataGrid)source).DataKeys[e.Item.ItemIndex].ToString();
                string UserType = (e.Item.Cells[(int)gridColomns.UserTypeHidden].Text);
                Response.Redirect("EditUser.aspx" + "?id=" + sUserID + "&pid=" + UserType);
                return;
            }
        }

        private void btnNew_Click(object sender, System.EventArgs e)
        {
            Response.Redirect("EditUser.aspx" + "?pid=" + (int)PMUserType.PMUser);
        }
		#endregion

		#region btnSearchClick

        private void btnSearch_Click(object sender, System.EventArgs e)
        {
            BindGridTable((int)PMUserType.Subcontracter);
            BindGridTable((int)PMUserType.Client);
            BindGridTable((int)PMUserType.Builder);
            BindGridTable((int)PMUserType.PMUser);
            BindGridTable((int)PMUserType.Supervisors);
        }
		
	

		
		#endregion

		#region btnClearClick

        private void btnClear_Click(object sender, System.EventArgs e)
        {
            ddlProjects.SelectedValue = "0";
            ddlProjectStatus.SelectedIndex = 0;
            ddlBuildingTypes.SelectedIndex = 0;
            BindGrid();
        }

		

	
		#endregion

		#region ddlProjectStatusSelectedIndexChanged

        private void ddlProjectStatus_SelectedIndexChanged(object sender, System.EventArgs e)
        {
            UIHelpers.LoadProjects(ddlProjects, ddlProjectStatus, ddlBuildingTypes);
        }

		

		
		#endregion

		#region ddlBuildingTypesSelectedIndexChanged

        private void ddlBuildingTypes_SelectedIndexChanged(object sender, System.EventArgs e)
        {
            UIHelpers.LoadProjects(ddlProjects, ddlProjectStatus, ddlBuildingTypes);
        }

	
		
		
		#endregion

        private void grdAccountsSubcontracters_SortCommand(object source, System.Web.UI.WebControls.DataGridSortCommandEventArgs e)
        {
            ViewState.Add("SortSubcontracters", UIHelpers.SetSort(grdAccountsSubcontracters, (string)ViewState["SortSubcontracters"], e.SortExpression));

            BindGrid();
        }

        private void grdAccountsClients_SortCommand(object source, System.Web.UI.WebControls.DataGridSortCommandEventArgs e)
        {
            ViewState.Add("SortClients", UIHelpers.SetSort(grdAccountsClients, (string)ViewState["SortClients"], e.SortExpression));

            BindGrid();
        }

        private void grdAccountsBuilders_SortCommand(object source, System.Web.UI.WebControls.DataGridSortCommandEventArgs e)
        {
            ViewState.Add("SortBuilders", UIHelpers.SetSort(grdAccountsBuilders, (string)ViewState["SortBuilders"], e.SortExpression));

            BindGrid();
        }

        private void grdAccountsWatchers_SortCommand(object source, System.Web.UI.WebControls.DataGridSortCommandEventArgs e)
        {
            ViewState.Add("SortSupervisors", UIHelpers.SetSort(grdAccountsWatchers, (string)ViewState["SortSupervisors"], e.SortExpression));

            BindGrid();
        }

        private void grdAccountsOthers_SortCommand(object source, System.Web.UI.WebControls.DataGridSortCommandEventArgs e)
        {

            ViewState.Add("SortPMUsers", UIHelpers.SetSort(grdAccountsOthers, (string)ViewState["SortPMUsers"], e.SortExpression));

            BindGrid();
        }
		
		
	
	}
}
