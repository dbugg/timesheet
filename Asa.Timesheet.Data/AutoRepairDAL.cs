using System;
using System.Data;
using System.Data.SqlClient; 
using System.Collections;
using Microsoft.ApplicationBlocks.Data;

namespace Asa.Timesheet.Data
{
	/*modified*/
	public class AutoRepairDAL
	{
		/// <summary>
		/// Loads a AutoRepairData object from the database using the given AutoRepairID
		/// </summary>
		/// <param name="autoRepairID">The primary key value</param>
		/// <returns>A AutoRepairData object</returns>
		public static AutoRepairData Load(int autoRepairID)
		{
			SqlParameter[] parameterValues = new SqlParameter[] { new SqlParameter("@AutoRepairID", autoRepairID) };
			using (SqlDataReader reader = SqlHelper.ExecuteReader(CommonDAL.ConnectionString, CommandType.StoredProcedure, "AutoRepairsSelProc", parameterValues))
			{
				if (reader.Read())
				{
					AutoRepairData result = new AutoRepairData();
					LoadFromReader(reader,result);
					return result;
				}
				else
					return null;
			}
		}

		/// <summary>
		/// Loads a AutoRepairData object from the database using the given where clause
		/// </summary>
		/// <param name="whereClause">The filter expression for the query</param>
		/// <returns>A AutoRepairData object</returns>
		public static AutoRepairData Load(string whereClause)
		{
			SqlParameter[] parameterValues = new SqlParameter[] { new SqlParameter("@WhereClause", whereClause) };
			using (SqlDataReader reader = SqlHelper.ExecuteReader(CommonDAL.ConnectionString, CommandType.StoredProcedure, "AutoRepairsListProc", parameterValues))
			{
				if (reader.Read())
				{
					AutoRepairData result = new AutoRepairData();
					LoadFromReader(reader,result);
					return result;
				}
				else
					return null;
			}
		}
		public static int Save( AutoRepairData source)
		{
			if (source.AutoRepairID == -1)
				return Insert(source);
			else
				return Update(source);	
		}

		public static void Delete( AutoRepairData source)
		{
			Delete(source.AutoRepairID);
		}
		/// <summary>
		/// Loads a collection of AutoRepairData objects from the database.
		/// </summary>
		/// <returns>A collection containing all of the AutoRepairData objects in the database.</returns>
		public static AutoRepairsVector LoadCollection()
		{
			SqlParameter[] parms = new SqlParameter[] {};
			return LoadCollection("AutoRepairsListProc", parms);
		}
	  
		/// <summary>
		/// Saves a collection of AutoRepairData objects to the database.
		/// </summary>
		/// <returns>A collection containing all of the AutoRepairData objects in the database.</returns>
		public static  void  SaveCollection(AutoRepairsVector col,string spName, SqlParameter[] parms)
		{
			AutoRepairsVector colOld = LoadCollection(spName,parms);
			UpdateCollection(colOld,col);
		}
		public static  void  SaveCollection(AutoRepairsVector col)
		{
			AutoRepairsVector colOld = LoadCollection();
			UpdateCollection(colOld,col);
		}
		
		/// <summary>
		/// Loads a collection of AutoRepairData objects from the database.
		/// </summary>
		/// <returns>A collection containing all of the AutoRepairData objects in the database.</returns>
		public static AutoRepairsVector LoadCollection(int nAutoID)
		{
			SqlParameter[] parms = new SqlParameter[] { new SqlParameter("@AutoID", nAutoID) };
			return LoadCollection("AutoRepairsListProc", parms);
		}

		/// <summary>
		/// Loads a collection of AutoRepairData objects from the database.
		/// </summary>
		/// <returns>A collection containing all of the AutoRepairData objects in the database.</returns>
		public static AutoRepairsVector LoadCollection(string spName, SqlParameter[] parms)
		{
			AutoRepairsVector result = new AutoRepairsVector();
			using (SqlDataReader reader = SqlHelper.ExecuteReader(CommonDAL.ConnectionString, CommandType.StoredProcedure, spName, parms))
			{
				while (reader.Read())
				{
					AutoRepairData tmp = new AutoRepairData();
					LoadFromReader(reader, tmp);
					result.Add(tmp);
				}
			}
			return result;
		}
		/// <summary>
		/// Deletes a AutoRepairData object from the database.
		/// </summary>
		/// <param name="autoRepairID">The primary key value</param>
		public static void Delete(int autoRepairID)
		{
			SqlParameter[] parameterValues = new SqlParameter[] { new SqlParameter("@AutoRepairID", autoRepairID) };
			SqlHelper.ExecuteNonQuery(CommonDAL.ConnectionString, CommandType.StoredProcedure, "AutoRepairsDelProc", parameterValues);
		}

		public static void LoadFromReader(SqlDataReader reader, AutoRepairData target)
		{
			if (reader != null && !reader.IsClosed)
			{
				//target.Key = reader.GetInt32(0);
				if (!reader.IsDBNull(0)) target.AutoRepairID = reader.GetInt32(0);
				if (!reader.IsDBNull(1)) target.AutoRepairDescription = reader.GetString(1);
				if (!reader.IsDBNull(2)) target.AutoServiceID = reader.GetInt32(2);
				if (!reader.IsDBNull(3)) target.AutomobileID = reader.GetInt32(3);
				if (!reader.IsDBNull(4)) target.DateAdded = reader.GetDateTime(4);
				
				if(reader.FieldCount>5)
					if (!reader.IsDBNull(5)) target.AutoServiceName = reader.GetString(5);
			}
		}
		
	  
		public static int Insert( AutoRepairData source)
		{
			SqlParameter[] parameterValues = GetInsertParameterValues(source);
			SqlHelper.ExecuteNonQuery(CommonDAL.ConnectionString, CommandType.StoredProcedure, "AutoRepairsInsProc", parameterValues);
			source.AutoRepairID = (int) parameterValues[0].Value;
			return source.AutoRepairID;
		}

		public static int Update( AutoRepairData source)
		{
			SqlParameter[] parameterValues = GetUpdateParameterValues(source);
			SqlHelper.ExecuteNonQuery(CommonDAL.ConnectionString, CommandType.StoredProcedure,
				"AutoRepairsUpdProc", parameterValues);
			return source.AutoRepairID;
		}
		private static void UpdateCollection(AutoRepairsVector colOld,AutoRepairsVector col)
		{
			// Delete all old items
			foreach( AutoRepairData itemOld in colOld)
			{
				if(!ExistsKey(itemOld.AutoRepairID, col))
					Delete(itemOld.AutoRepairID);
			}
			foreach( AutoRepairData item in col)
			{
				if(item.AutoRepairID <= -1)
					Insert(item);
				else
					Update(item);
			}
		}
		private static bool ExistsKey(int Key, AutoRepairsVector col)
		{
			foreach( AutoRepairData item in col)
				if(item.AutoRepairID==Key)
					return true;
			return false;
		}

		private static SqlParameter GetSqlParameter(string name, ParameterDirection direction, object value)
		{
			SqlParameter p = new SqlParameter(name, value);
			p.Direction = direction;
			return p;
		}

		private static SqlParameter[] GetInsertParameterValues( AutoRepairData source)
		{
			SqlParameter[] prams = new SqlParameter[5];
			prams[0] = GetSqlParameter("@AutoRepairID", ParameterDirection.Output, source.AutoRepairID);
			prams[1] = GetSqlParameter("@AutoRepairDescription", ParameterDirection.Input, source.AutoRepairDescription);
			prams[2] = GetSqlParameter("@AutoServiceID", ParameterDirection.Input, source.AutoServiceID);
			prams[3] = GetSqlParameter("@AutomobileID", ParameterDirection.Input, source.AutomobileID);
			if(source.DateAdded!=Constants.DateMax)
				prams[4] = GetSqlParameter("@DateAdded", ParameterDirection.Input, source.DateAdded);
			
			return prams;
		}

		private static SqlParameter[] GetUpdateParameterValues( AutoRepairData source)
		{
			return new SqlParameter[] {
										  new SqlParameter("@AutoRepairID", source.AutoRepairID),
										  new SqlParameter("@AutoRepairDescription", source.AutoRepairDescription),
										  new SqlParameter("@AutoServiceID", source.AutoServiceID),
										  new SqlParameter("@AutomobileID", source.AutomobileID)
									  };
		}
	}
}   

