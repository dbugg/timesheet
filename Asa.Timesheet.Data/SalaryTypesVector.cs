using System;
using System.Collections;

namespace Asa.Timesheet.Data
{
	/// <summary>
	///		A strongly-typed collection of <see cref="SalaryTypeData"/> objects.
	/// </summary>
	[Serializable]
	public class SalaryTypesVector : ICollection, IList, IEnumerable, ICloneable
	{
		#region Interfaces
		/// <summary>
		///		Supports type-safe iteration over a <see cref="SalaryTypesVector"/>.
		/// </summary>
		public interface ISalaryTypesVectorEnumerator
		{
			/// <summary>
			///		Gets the current element in the collection.
			/// </summary>
			SalaryTypeData Current {get;}

			/// <summary>
			///		Advances the enumerator to the next element in the collection.
			/// </summary>
			/// <exception cref="InvalidOperationException">
			///		The collection was modified after the enumerator was created.
			/// </exception>
			/// <returns>
			///		<c>true</c> if the enumerator was successfully advanced to the next element; 
			///		<c>false</c> if the enumerator has passed the end of the collection.
			/// </returns>
			bool MoveNext();

			/// <summary>
			///		Sets the enumerator to its initial position, before the first element in the collection.
			/// </summary>
			void Reset();
		}
		#endregion

		private const int DEFAULT_CAPACITY = 16;

		#region Implementation (data)
		private SalaryTypeData[] m_array;
		private int m_count = 0;
		[NonSerialized]
		private int m_version = 0;
		#endregion
	
		#region Static Wrappers
		/// <summary>
		///		Creates a synchronized (thread-safe) wrapper for a 
		///     <c>SalaryTypesVector</c> instance.
		/// </summary>
		/// <returns>
		///     An <c>SalaryTypesVector</c> wrapper that is synchronized (thread-safe).
		/// </returns>
		public static SalaryTypesVector Synchronized(SalaryTypesVector list)
		{
			if(list==null)
				throw new ArgumentNullException("list");
			return new SyncSalaryTypesVector(list);
		}
        
		/// <summary>
		///		Creates a read-only wrapper for a 
		///     <c>SalaryTypesVector</c> instance.
		/// </summary>
		/// <returns>
		///     An <c>SalaryTypesVector</c> wrapper that is read-only.
		/// </returns>
		public static SalaryTypesVector ReadOnly(SalaryTypesVector list)
		{
			if(list==null)
				throw new ArgumentNullException("list");
			return new ReadOnlySalaryTypesVector(list);
		}
		#endregion

		#region Construction
		/// <summary>
		///		Initializes a new instance of the <c>SalaryTypesVector</c> class
		///		that is empty and has the default initial capacity.
		/// </summary>
		public SalaryTypesVector()
		{
			m_array = new SalaryTypeData[DEFAULT_CAPACITY];
		}
		
		/// <summary>
		///		Initializes a new instance of the <c>SalaryTypesVector</c> class
		///		that has the specified initial capacity.
		/// </summary>
		/// <param name="capacity">
		///		The number of elements that the new <c>SalaryTypesVector</c> is initially capable of storing.
		///	</param>
		public SalaryTypesVector(int capacity)
		{
			m_array = new SalaryTypeData[capacity];
		}

		/// <summary>
		///		Initializes a new instance of the <c>SalaryTypesVector</c> class
		///		that contains elements copied from the specified <c>SalaryTypesVector</c>.
		/// </summary>
		/// <param name="c">The <c>SalaryTypesVector</c> whose elements are copied to the new collection.</param>
		public SalaryTypesVector(SalaryTypesVector c)
		{
			m_array = new SalaryTypeData[c.Count];
			AddRange(c);
		}

		/// <summary>
		///		Initializes a new instance of the <c>SalaryTypesVector</c> class
		///		that contains elements copied from the specified <see cref="SalaryTypeData"/> array.
		/// </summary>
		/// <param name="a">The <see cref="SalaryTypeData"/> array whose elements are copied to the new list.</param>
		public SalaryTypesVector(SalaryTypeData[] a)
		{
			m_array = new SalaryTypeData[a.Length];
			AddRange(a);
		}
		
		protected enum Tag 
		{
			Default
		}

		protected SalaryTypesVector(Tag t)
		{
			m_array = null;
		}
		#endregion
		#region ID methods
		public SalaryTypeData GetByID(int ID)
		{
			for(int i=0;i<this.Count;i++)
				if(this[i].SalaryTypeID==ID)
					return this[i];
			return null;
		}
		public int GetIndexByID(int ID)
		{
			for(int i=0;i<this.Count;i++)
				if(this[i].SalaryTypeID==ID)
					return i;
			return -1;
		}
		#endregion
		#region Operations (type-safe ICollection)
		/// <summary>
		///		Gets the number of elements actually contained in the <c>SalaryTypesVector</c>.
		/// </summary>
		public virtual int Count
		{
			get { return m_count; }
		}

		/// <summary>
		///		Copies the entire <c>SalaryTypesVector</c> to a one-dimensional
		///		<see cref="SalaryTypeData"/> array.
		/// </summary>
		/// <param name="array">The one-dimensional <see cref="SalaryTypeData"/> array to copy to.</param>
		public virtual void CopyTo(SalaryTypeData[] array)
		{
			this.CopyTo(array, 0);
		}

		/// <summary>
		///		Copies the entire <c>SalaryTypesVector</c> to a one-dimensional
		///		<see cref="SalaryTypeData"/> array, starting at the specified index of the target array.
		/// </summary>
		/// <param name="array">The one-dimensional <see cref="SalaryTypeData"/> array to copy to.</param>
		/// <param name="start">The zero-based index in <paramref name="array"/> at which copying begins.</param>
		public virtual void CopyTo(SalaryTypeData[] array, int start)
		{
			if (m_count > array.GetUpperBound(0) + 1 - start)
				throw new System.ArgumentException("Destination array was not long enough.");
			
			Array.Copy(m_array, 0, array, start, m_count); 
		}

		/// <summary>
		///		Gets a value indicating whether access to the collection is synchronized (thread-safe).
		/// </summary>
		/// <returns>true if access to the ICollection is synchronized (thread-safe); otherwise, false.</returns>
		public virtual bool IsSynchronized
		{
			get { return m_array.IsSynchronized; }
		}

		/// <summary>
		///		Gets an object that can be used to synchronize access to the collection.
		/// </summary>
		public virtual object SyncRoot
		{
			get { return m_array.SyncRoot; }
		}
		#endregion
		
		#region Operations (type-safe IList)
		/// <summary>
		///		Gets or sets the <see cref="SalaryTypeData"/> at the specified index.
		/// </summary>
		/// <param name="index">The zero-based index of the element to get or set.</param>
		/// <exception cref="ArgumentOutOfRangeException">
		///		<para><paramref name="index"/> is less than zero</para>
		///		<para>-or-</para>
		///		<para><paramref name="index"/> is equal to or greater than <see cref="SalaryTypesVector.Count"/>.</para>
		/// </exception>
		public virtual SalaryTypeData this[int index]
		{
			get
			{
				ValidateIndex(index); // throws
				return m_array[index]; 
			}
			set
			{
				ValidateIndex(index); // throws
				++m_version; 
				m_array[index] = value; 
			}
		}

		/// <summary>
		///		Adds a <see cref="SalaryTypeData"/> to the end of the <c>SalaryTypesVector</c>.
		/// </summary>
		/// <param name="item">The <see cref="SalaryTypeData"/> to be added to the end of the <c>SalaryTypesVector</c>.</param>
		/// <returns>The index at which the value has been added.</returns>
		public virtual int Add(SalaryTypeData item)
		{
			if (m_count == m_array.Length)
				EnsureCapacity(m_count + 1);

			m_array[m_count] = item;
			m_version++;

			return m_count++;
		}
		
		/// <summary>
		///		Removes all elements from the <c>SalaryTypesVector</c>.
		/// </summary>
		public virtual void Clear()
		{
			++m_version;
			m_array = new SalaryTypeData[DEFAULT_CAPACITY];
			m_count = 0;
		}
		
		/// <summary>
		///		Creates a shallow copy of the <see cref="SalaryTypesVector"/>.
		/// </summary>
		public virtual object Clone()
		{
			SalaryTypesVector newColl = new SalaryTypesVector(m_count);
			Array.Copy(m_array, 0, newColl.m_array, 0, m_count);
			newColl.m_count = m_count;
			newColl.m_version = m_version;

			return newColl;
		}

		/// <summary>
		///		Determines whether a given <see cref="SalaryTypeData"/> is in the <c>SalaryTypesVector</c>.
		/// </summary>
		/// <param name="item">The <see cref="SalaryTypeData"/> to check for.</param>
		/// <returns><c>true</c> if <paramref name="item"/> is found in the <c>SalaryTypesVector</c>; otherwise, <c>false</c>.</returns>
		public virtual bool Contains(SalaryTypeData item)
		{
			for (int i=0; i != m_count; ++i)
				if (m_array[i].Equals(item))
					return true;
			return false;
		}

		/// <summary>
		///		Returns the zero-based index of the first occurrence of a <see cref="SalaryTypeData"/>
		///		in the <c>SalaryTypesVector</c>.
		/// </summary>
		/// <param name="item">The <see cref="SalaryTypeData"/> to locate in the <c>SalaryTypesVector</c>.</param>
		/// <returns>
		///		The zero-based index of the first occurrence of <paramref name="item"/> 
		///		in the entire <c>SalaryTypesVector</c>, if found; otherwise, -1.
		///	</returns>
		public virtual int IndexOf(SalaryTypeData item)
		{
			for (int i=0; i != m_count; ++i)
				if (m_array[i].Equals(item))
					return i;
			return -1;
		}

		/// <summary>
		///		Inserts an element into the <c>SalaryTypesVector</c> at the specified index.
		/// </summary>
		/// <param name="index">The zero-based index at which <paramref name="item"/> should be inserted.</param>
		/// <param name="item">The <see cref="SalaryTypeData"/> to insert.</param>
		/// <exception cref="ArgumentOutOfRangeException">
		///		<para><paramref name="index"/> is less than zero</para>
		///		<para>-or-</para>
		///		<para><paramref name="index"/> is equal to or greater than <see cref="SalaryTypesVector.Count"/>.</para>
		/// </exception>
		public virtual void Insert(int index, SalaryTypeData item)
		{
			ValidateIndex(index, true); // throws
			
			if (m_count == m_array.Length)
				EnsureCapacity(m_count + 1);

			if (index < m_count)
			{
				Array.Copy(m_array, index, m_array, index + 1, m_count - index);
			}

			m_array[index] = item;
			m_count++;
			m_version++;
		}

		/// <summary>
		///		Removes the first occurrence of a specific <see cref="SalaryTypeData"/> from the <c>SalaryTypesVector</c>.
		/// </summary>
		/// <param name="item">The <see cref="SalaryTypeData"/> to remove from the <c>SalaryTypesVector</c>.</param>
		/// <exception cref="ArgumentException">
		///		The specified <see cref="SalaryTypeData"/> was not found in the <c>SalaryTypesVector</c>.
		/// </exception>
		public virtual void Remove(SalaryTypeData item)
		{		   
			int i = IndexOf(item);
			if (i < 0)
				throw new System.ArgumentException("Cannot remove the specified item because it was not found in the specified Collection.");
			
			++m_version;
			RemoveAt(i);
		}

		/// <summary>
		///		Removes the element at the specified index of the <c>SalaryTypesVector</c>.
		/// </summary>
		/// <param name="index">The zero-based index of the element to remove.</param>
		/// <exception cref="ArgumentOutOfRangeException">
		///		<para><paramref name="index"/> is less than zero</para>
		///		<para>-or-</para>
		///		<para><paramref name="index"/> is equal to or greater than <see cref="SalaryTypesVector.Count"/>.</para>
		/// </exception>
		public virtual void RemoveAt(int index)
		{
			ValidateIndex(index); // throws
			
			m_count--;

			if (index < m_count)
			{
				Array.Copy(m_array, index + 1, m_array, index, m_count - index);
			}
			
			// We can't set the deleted entry equal to null, because it might be a value type.
			// Instead, we'll create an empty single-element array of the right type and copy it 
			// over the entry we want to erase.
			SalaryTypeData[] temp = new SalaryTypeData[1];
			Array.Copy(temp, 0, m_array, m_count, 1);
			m_version++;
		}

		/// <summary>
		///		Gets a value indicating whether the collection has a fixed size.
		/// </summary>
		/// <value>true if the collection has a fixed size; otherwise, false. The default is false</value>
		public virtual bool IsFixedSize
		{
			get { return false; }
		}

		/// <summary>
		///		gets a value indicating whether the <B>IList</B> is read-only.
		/// </summary>
		/// <value>true if the collection is read-only; otherwise, false. The default is false</value>
		public virtual bool IsReadOnly
		{
			get { return false; }
		}
		#endregion

		#region Operations (type-safe IEnumerable)
		
		/// <summary>
		///		Returns an enumerator that can iterate through the <c>SalaryTypesVector</c>.
		/// </summary>
		/// <returns>An <see cref="Enumerator"/> for the entire <c>SalaryTypesVector</c>.</returns>
		public virtual ISalaryTypesVectorEnumerator GetEnumerator()
		{
			return new Enumerator(this);
		}
		#endregion

		#region Public helpers (just to mimic some nice features of ArrayList)
		
		/// <summary>
		///		Gets or sets the number of elements the <c>SalaryTypesVector</c> can contain.
		/// </summary>
		public virtual int Capacity
		{
			get { return m_array.Length; }
			
			set
			{
				if (value < m_count)
					value = m_count;

				if (value != m_array.Length)
				{
					if (value > 0)
					{
						SalaryTypeData[] temp = new SalaryTypeData[value];
						Array.Copy(m_array, temp, m_count);
						m_array = temp;
					}
					else
					{
						m_array = new SalaryTypeData[DEFAULT_CAPACITY];
					}
				}
			}
		}

		/// <summary>
		///		Adds the elements of another <c>SalaryTypesVector</c> to the current <c>SalaryTypesVector</c>.
		/// </summary>
		/// <param name="x">The <c>SalaryTypesVector</c> whose elements should be added to the end of the current <c>SalaryTypesVector</c>.</param>
		/// <returns>The new <see cref="SalaryTypesVector.Count"/> of the <c>SalaryTypesVector</c>.</returns>
		public virtual int AddRange(SalaryTypesVector x)
		{
			if (m_count + x.Count >= m_array.Length)
				EnsureCapacity(m_count + x.Count);
			
			Array.Copy(x.m_array, 0, m_array, m_count, x.Count);
			m_count += x.Count;
			m_version++;

			return m_count;
		}

		/// <summary>
		///		Adds the elements of a <see cref="SalaryTypeData"/> array to the current <c>SalaryTypesVector</c>.
		/// </summary>
		/// <param name="x">The <see cref="SalaryTypeData"/> array whose elements should be added to the end of the <c>SalaryTypesVector</c>.</param>
		/// <returns>The new <see cref="SalaryTypesVector.Count"/> of the <c>SalaryTypesVector</c>.</returns>
		public virtual int AddRange(SalaryTypeData[] x)
		{
			if (m_count + x.Length >= m_array.Length)
				EnsureCapacity(m_count + x.Length);

			Array.Copy(x, 0, m_array, m_count, x.Length);
			m_count += x.Length;
			m_version++;

			return m_count;
		}
		
		/// <summary>
		///		Sets the capacity to the actual number of elements.
		/// </summary>
		public virtual void TrimToSize()
		{
			this.Capacity = m_count;
		}

		#endregion

		#region Implementation (helpers)

		/// <exception cref="ArgumentOutOfRangeException">
		///		<para><paramref name="index"/> is less than zero</para>
		///		<para>-or-</para>
		///		<para><paramref name="index"/> is equal to or greater than <see cref="SalaryTypesVector.Count"/>.</para>
		/// </exception>
		private void ValidateIndex(int i)
		{
			ValidateIndex(i, false);
		}

		/// <exception cref="ArgumentOutOfRangeException">
		///		<para><paramref name="index"/> is less than zero</para>
		///		<para>-or-</para>
		///		<para><paramref name="index"/> is equal to or greater than <see cref="SalaryTypesVector.Count"/>.</para>
		/// </exception>
		private void ValidateIndex(int i, bool allowEqualEnd)
		{
			int max = (allowEqualEnd)?(m_count):(m_count-1);
			if (i < 0 || i > max)
				throw new System.ArgumentOutOfRangeException("Index was out of range.  Must be non-negative and less than the size of the collection.", (object)i, "Specified argument was out of the range of valid values.");
		}

		private void EnsureCapacity(int min)
		{
			int newCapacity = ((m_array.Length == 0) ? DEFAULT_CAPACITY : m_array.Length * 2);
			if (newCapacity < min)
				newCapacity = min;

			this.Capacity = newCapacity;
		}

		#endregion
		
		#region Implementation (ICollection)

		void ICollection.CopyTo(Array array, int start)
		{
			Array.Copy(m_array, 0, array, start, m_count);
		}

		#endregion

		#region Implementation (IList)

		object IList.this[int i]
		{
			get { return (object)this[i]; }
			set { this[i] = (SalaryTypeData)value; }
		}

		int IList.Add(object x)
		{
			return this.Add((SalaryTypeData)x);
		}

		bool IList.Contains(object x)
		{
			return this.Contains((SalaryTypeData)x);
		}

		int IList.IndexOf(object x)
		{
			return this.IndexOf((SalaryTypeData)x);
		}

		void IList.Insert(int pos, object x)
		{
			this.Insert(pos, (SalaryTypeData)x);
		}

		void IList.Remove(object x)
		{
			this.Remove((SalaryTypeData)x);
		}

		void IList.RemoveAt(int pos)
		{
			this.RemoveAt(pos);
		}

		#endregion

		#region Implementation (IEnumerable)

		IEnumerator IEnumerable.GetEnumerator()
		{
			return (IEnumerator)(this.GetEnumerator());
		}

		#endregion

		#region Nested enumerator class
		/// <summary>
		///		Supports simple iteration over a <see cref="SalaryTypesVector"/>.
		/// </summary>
		private class Enumerator : IEnumerator, ISalaryTypesVectorEnumerator
		{
			#region Implementation (data)
			
			private SalaryTypesVector m_collection;
			private int m_index;
			private int m_version;
			
			#endregion
		
			#region Construction
			
			/// <summary>
			///		Initializes a new instance of the <c>Enumerator</c> class.
			/// </summary>
			/// <param name="tc"></param>
			internal Enumerator(SalaryTypesVector tc)
			{
				m_collection = tc;
				m_index = -1;
				m_version = tc.m_version;
			}
			
			#endregion
	
			#region Operations (type-safe IEnumerator)
			
			/// <summary>
			///		Gets the current element in the collection.
			/// </summary>
			public SalaryTypeData Current
			{
				get { return m_collection[m_index]; }
			}

			/// <summary>
			///		Advances the enumerator to the next element in the collection.
			/// </summary>
			/// <exception cref="InvalidOperationException">
			///		The collection was modified after the enumerator was created.
			/// </exception>
			/// <returns>
			///		<c>true</c> if the enumerator was successfully advanced to the next element; 
			///		<c>false</c> if the enumerator has passed the end of the collection.
			/// </returns>
			public bool MoveNext()
			{
				if (m_version != m_collection.m_version)
					throw new System.InvalidOperationException("Collection was modified; enumeration operation may not execute.");

				++m_index;
				return (m_index < m_collection.Count) ? true : false;
			}

			/// <summary>
			///		Sets the enumerator to its initial position, before the first element in the collection.
			/// </summary>
			public void Reset()
			{
				m_index = -1;
			}
			#endregion
	
			#region Implementation (IEnumerator)
			
			object IEnumerator.Current
			{
				get { return (object)(this.Current); }
			}
			
			#endregion
		}
		#endregion
        
		#region Nested Syncronized Wrapper class
		private class SyncSalaryTypesVector : SalaryTypesVector
		{
			#region Implementation (data)
			private SalaryTypesVector m_collection;
			private object m_root;
			#endregion

			#region Construction
			internal SyncSalaryTypesVector(SalaryTypesVector list) : base(Tag.Default)
			{
				m_root = list.SyncRoot;
				m_collection = list;
			}
			#endregion
            
			#region Type-safe ICollection
			public override void CopyTo(SalaryTypeData[] array)
			{
				lock(this.m_root)
					m_collection.CopyTo(array);
			}

			public override void CopyTo(SalaryTypeData[] array, int start)
			{
				lock(this.m_root)
					m_collection.CopyTo(array,start);
			}
			public override int Count
			{
				get
				{ 
					lock(this.m_root)
						return m_collection.Count;
				}
			}

			public override bool IsSynchronized
			{
				get { return true; }
			}

			public override object SyncRoot
			{
				get { return this.m_root; }
			}
			#endregion
            
			#region Type-safe IList
			public override SalaryTypeData this[int i]
			{
				get
				{
					lock(this.m_root)
						return m_collection[i];
				}
				set
				{
					lock(this.m_root)
						m_collection[i] = value; 
				}
			}

			public override int Add(SalaryTypeData x)
			{
				lock(this.m_root)
					return m_collection.Add(x);
			}
            
			public override void Clear()
			{
				lock(this.m_root)
					m_collection.Clear();
			}

			public override bool Contains(SalaryTypeData x)
			{
				lock(this.m_root)
					return m_collection.Contains(x);
			}

			public override int IndexOf(SalaryTypeData x)
			{
				lock(this.m_root)
					return m_collection.IndexOf(x);
			}

			public override void Insert(int pos, SalaryTypeData x)
			{
				lock(this.m_root)
					m_collection.Insert(pos,x);
			}

			public override void Remove(SalaryTypeData x)
			{           
				lock(this.m_root)
					m_collection.Remove(x);
			}

			public override void RemoveAt(int pos)
			{
				lock(this.m_root)
					m_collection.RemoveAt(pos);
			}
            
			public override bool IsFixedSize
			{
				get {return m_collection.IsFixedSize;}
			}

			public override bool IsReadOnly
			{
				get {return m_collection.IsReadOnly;}
			}
			#endregion

			#region Type-safe IEnumerable
			public override ISalaryTypesVectorEnumerator GetEnumerator()
			{
				lock(m_root)
					return m_collection.GetEnumerator();
			}
			#endregion

			#region Public Helpers
			// (just to mimic some nice features of ArrayList)
			public override int Capacity
			{
				get
				{
					lock(this.m_root)
						return m_collection.Capacity;
				}
                
				set
				{
					lock(this.m_root)
						m_collection.Capacity = value;
				}
			}

			public override int AddRange(SalaryTypesVector x)
			{
				lock(this.m_root)
					return m_collection.AddRange(x);
			}

			public override int AddRange(SalaryTypeData[] x)
			{
				lock(this.m_root)
					return m_collection.AddRange(x);
			}
			#endregion
		}
		#endregion

		#region Nested Read Only Wrapper class
		private class ReadOnlySalaryTypesVector : SalaryTypesVector
		{
			#region Implementation (data)
			private SalaryTypesVector m_collection;
			#endregion

			#region Construction
			internal ReadOnlySalaryTypesVector(SalaryTypesVector list) : base(Tag.Default)
			{
				m_collection = list;
			}
			#endregion
            
			#region Type-safe ICollection
			public override void CopyTo(SalaryTypeData[] array)
			{
				m_collection.CopyTo(array);
			}

			public override void CopyTo(SalaryTypeData[] array, int start)
			{
				m_collection.CopyTo(array,start);
			}
			public override int Count
			{
				get {return m_collection.Count;}
			}

			public override bool IsSynchronized
			{
				get { return m_collection.IsSynchronized; }
			}

			public override object SyncRoot
			{
				get { return this.m_collection.SyncRoot; }
			}
			#endregion
            
			#region Type-safe IList
			public override SalaryTypeData this[int i]
			{
				get { return m_collection[i]; }
				set { throw new NotSupportedException("This is a Read Only Collection and can not be modified"); }
			}

			public override int Add(SalaryTypeData x)
			{
				throw new NotSupportedException("This is a Read Only Collection and can not be modified");
			}
            
			public override void Clear()
			{
				throw new NotSupportedException("This is a Read Only Collection and can not be modified");
			}

			public override bool Contains(SalaryTypeData x)
			{
				return m_collection.Contains(x);
			}

			public override int IndexOf(SalaryTypeData x)
			{
				return m_collection.IndexOf(x);
			}

			public override void Insert(int pos, SalaryTypeData x)
			{
				throw new NotSupportedException("This is a Read Only Collection and can not be modified");
			}

			public override void Remove(SalaryTypeData x)
			{           
				throw new NotSupportedException("This is a Read Only Collection and can not be modified");
			}

			public override void RemoveAt(int pos)
			{
				throw new NotSupportedException("This is a Read Only Collection and can not be modified");
			}
            
			public override bool IsFixedSize
			{
				get {return true;}
			}

			public override bool IsReadOnly
			{
				get {return true;}
			}
			#endregion

			#region Type-safe IEnumerable
			public override ISalaryTypesVectorEnumerator GetEnumerator()
			{
				return m_collection.GetEnumerator();
			}
			#endregion

			#region Public Helpers
			// (just to mimic some nice features of ArrayList)
			public override int Capacity
			{
				get { return m_collection.Capacity; }
                
				set { throw new NotSupportedException("This is a Read Only Collection and can not be modified"); }
			}

			public override int AddRange(SalaryTypesVector x)
			{
				throw new NotSupportedException("This is a Read Only Collection and can not be modified");
			}

			public override int AddRange(SalaryTypeData[] x)
			{
				throw new NotSupportedException("This is a Read Only Collection and can not be modified");
			}
			#endregion
		}
		#endregion
	}
}
