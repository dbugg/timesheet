using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Data.SqlClient;
using log4net;
using Asa.Timesheet.Data;
using Asa.Timesheet.Data.Entities;
using Asa.Timesheet.Data.Util;
using DataDynamics.ActiveReports.Export.Pdf;
using DataDynamics.ActiveReports.Export.Xls;
using Asa.Timesheet.WebPages.Reports;
using DataDynamics.ActiveReports.Export.Rtf;
using System.IO;
using System.Collections.Specialized;

namespace Asa.Timesheet.WebPages
{
	/// <summary>
	/// Summary description for EditProject.
	/// </summary>
	public class EditProject : TimesheetPageBase 
	{
		#region Web controls

		protected System.Web.UI.WebControls.TextBox txtProjectName;
		protected System.Web.UI.WebControls.DropDownList ddlBuildingTypes;
		protected System.Web.UI.WebControls.DropDownList ddlClients;
		protected System.Web.UI.WebControls.TextBox txtProjectCode;
		protected System.Web.UI.WebControls.CheckBox cbHasActivity;
		protected System.Web.UI.WebControls.HyperLink HyperLink1;
		protected System.Web.UI.WebControls.HyperLink HyperLink2;
		protected System.Web.UI.WebControls.HyperLink HyperLink3;
		protected System.Web.UI.WebControls.Label lblName;
		protected System.Web.UI.WebControls.Button btnCancel;
		protected System.Web.UI.WebControls.Button btnDelete;
		protected System.Web.UI.WebControls.Label lblInfo;
		protected System.Web.UI.WebControls.Label lblProjectCode;
		protected System.Web.UI.WebControls.Label lblArea;
		protected System.Web.UI.WebControls.Label lblBuildingType;
		protected System.Web.UI.WebControls.Label lblHasActivity;
		protected System.Web.UI.WebControls.Label lblStartDate;
		protected System.Web.UI.WebControls.TextBox txtAddress;
		protected System.Web.UI.WebControls.Label lblManager;
		protected System.Web.UI.WebControls.Label lblAddress;
		protected System.Web.UI.WebControls.Button btnSave;
		protected System.Web.UI.WebControls.DropDownList ddlManager;
		protected System.Web.UI.WebControls.Label lblClient;
		protected System.Web.UI.WebControls.Label lblError;
		protected System.Web.UI.WebControls.PlaceHolder menuHolder;
		protected Asa.Timesheet.WebPages.UserControls.PageHeader header;
		
	

		protected System.Web.UI.WebControls.TextBox txtAdministrativeName;
		protected System.Web.UI.WebControls.Label lblAdministrativeName;
		protected System.Web.UI.WebControls.TextBox txtStartDate;
		protected System.Web.UI.HtmlControls.HtmlImage lkCalendar1;
		protected System.Web.UI.WebControls.Label Label1;
		protected System.Web.UI.WebControls.TextBox txtEndDate;
		protected System.Web.UI.HtmlControls.HtmlImage lkCalendar2;
		protected System.Web.UI.WebControls.Label Label2;
		protected System.Web.UI.WebControls.Label Label4;
		protected System.Web.UI.WebControls.Label Label5;
		protected System.Web.UI.WebControls.Label Label6;
		protected System.Web.UI.WebControls.DataList dlPayments;
		protected System.Web.UI.WebControls.Button btnNewPayment;
		protected System.Web.UI.WebControls.DropDownList ddlScheme;
		protected System.Web.UI.WebControls.Label Label3;
		protected System.Web.UI.WebControls.TextBox txtRate;
		protected System.Web.UI.WebControls.Button btn;
		protected System.Web.UI.WebControls.Label lbEUR;
		protected System.Web.UI.WebControls.Label lbBGN;
		protected System.Web.UI.WebControls.Label Label8;
		protected System.Web.UI.WebControls.Label Label9;
		protected System.Web.UI.WebControls.Label Label10;
		protected System.Web.UI.HtmlControls.HtmlTable tbl;
		protected System.Web.UI.WebControls.Label Label11;
		protected System.Web.UI.WebControls.TextBox txtInvestor;
		
		protected System.Web.UI.WebControls.Label Label12;
		protected System.Web.UI.WebControls.TextBox txtAddInfo;
		protected System.Web.UI.WebControls.Button btnAdd;
		protected System.Web.UI.WebControls.DataGrid grdDistr;
		protected System.Web.UI.WebControls.DataGrid grd;
		protected System.Web.UI.WebControls.TextBox txtA;
		protected System.Web.UI.WebControls.Label Label14;
		protected System.Web.UI.WebControls.Button btnCalculate;
		protected System.Web.UI.WebControls.TextBox txtArea1;
		protected System.Web.UI.WebControls.TextBox txtArea;
		protected System.Web.UI.WebControls.CheckBox ckPhases;
		protected System.Web.UI.WebControls.Label lbPayments;
		protected System.Web.UI.WebControls.Label lb1;
		protected System.Web.UI.HtmlControls.HtmlTable tblP;
		protected System.Web.UI.HtmlControls.HtmlTable TableMoreClients;
		protected System.Web.UI.WebControls.Label Label7;
		protected System.Web.UI.WebControls.TextBox txtEndDateContract;
		protected System.Web.UI.HtmlControls.HtmlImage lkCalendar3;
		protected System.Web.UI.WebControls.Button btnExportPay;
		protected System.Web.UI.WebControls.Button Button2;
		protected Telerik.WebControls.RadGrid gridCalls;
		protected System.Web.UI.HtmlControls.HtmlTable Table3;
		protected System.Web.UI.HtmlControls.HtmlInputButton btnPrint;
		protected System.Web.UI.WebControls.Label lblStatus;
		protected System.Web.UI.WebControls.CheckBox ckActive;
		protected System.Web.UI.WebControls.CheckBox ckBlack;
		protected System.Web.UI.WebControls.Button btnDelete1;
		protected System.Web.UI.WebControls.Label Label16;
		protected System.Web.UI.WebControls.TextBox txtFoldersGiven;
		protected System.Web.UI.WebControls.Label Label17;
		protected System.Web.UI.WebControls.TextBox txtFoldersArchive;
		protected System.Web.UI.WebControls.ImageButton btnDocProtokol;
		protected System.Web.UI.WebControls.LinkButton lkProfile;
		protected System.Web.UI.WebControls.LinkButton lkSubpr;
		protected System.Web.UI.WebControls.LinkButton lkSubcontracters;
		protected System.Web.UI.WebControls.LinkButton lkFull;
		protected System.Web.UI.WebControls.LinkButton lbShortDocumentation;
		protected System.Web.UI.WebControls.LinkButton lkFullProjectInfo;
		protected System.Web.UI.WebControls.LinkButton lkTechInfo;
		protected System.Web.UI.WebControls.LinkButton btnContents;
		protected System.Web.UI.WebControls.ImageButton Imagebutton1;
		protected System.Web.UI.WebControls.LinkButton btnTech;
		protected System.Web.UI.WebControls.LinkButton lkProtPlus;
		protected System.Web.UI.HtmlControls.HtmlTable Table1;
		protected System.Web.UI.WebControls.TextBox txtOwner;
		protected System.Web.UI.WebControls.Label lblOwner;
		protected System.Web.UI.WebControls.LinkButton lkMissingData;
		protected System.Web.UI.WebControls.Button btnEdit;
		protected System.Web.UI.WebControls.Label lblBTCode;
		protected System.Web.UI.WebControls.Label lblProjectOrder;
		protected System.Web.UI.WebControls.Label Label18;
		protected System.Web.UI.WebControls.HyperLink hlProjectFolder;
		protected System.Web.UI.WebControls.Button btnCreateFolder;

		protected Asa.Timesheet.WebPages.UserControls.EditForm editCtrl;

		private const int PreProjectBuildingTypeID = 7;
		protected System.Web.UI.HtmlControls.HtmlInputButton Button1;
		protected System.Web.UI.WebControls.DropDownList ddlContents;
		protected System.Web.UI.HtmlControls.HtmlInputHidden hidID;
		protected System.Web.UI.WebControls.CheckBox ckConcluded;
		protected System.Web.UI.WebControls.ImageButton img;
		protected System.Web.UI.WebControls.Label Label19;
		protected System.Web.UI.WebControls.Label Label20;
		protected System.Web.UI.WebControls.TextBox txtAuthor;
		protected System.Web.UI.WebControls.Label Label21;
		protected System.Web.UI.WebControls.DropDownList ddlAuth;
		protected System.Web.UI.WebControls.ImageButton btnContentExcel;
		protected System.Web.UI.WebControls.LinkButton Linkbutton1;
		protected System.Web.UI.WebControls.ImageButton imgTechExcel;
		protected System.Web.UI.WebControls.ImageButton imgTechWord;
		protected System.Web.UI.WebControls.LinkButton Linkbutton2;
		protected System.Web.UI.WebControls.LinkButton Linkbutton3;
		protected System.Web.UI.WebControls.LinkButton lkFolder1;
		protected System.Web.UI.WebControls.LinkButton lkFolder2;
		protected System.Web.UI.WebControls.Label Label22;
		protected System.Web.UI.WebControls.TextBox Textbox1;
		protected System.Web.UI.WebControls.TextBox txtColor;
		protected System.Web.UI.HtmlControls.HtmlInputButton btnChoose;
		protected System.Web.UI.WebControls.ListBox lbSelectedUsersMails;
		protected System.Web.UI.HtmlControls.HtmlInputText hdnlbSelectedUsersMails;
		protected System.Web.UI.WebControls.Label Label23;
		protected System.Web.UI.WebControls.LinkButton lkSendMail;
		
		protected System.Web.UI.WebControls.LinkButton lbProjectBuilding;
		protected System.Web.UI.WebControls.TextBox txtProjectPhase;
		protected System.Web.UI.WebControls.LinkButton lbProjectContract;
		protected System.Web.UI.WebControls.Label lblMoreClients;
		protected System.Web.UI.WebControls.ListBox lbClients;
		protected System.Web.UI.WebControls.ListBox lbSelectedClients;
		protected System.Web.UI.WebControls.ListBox lbUsersMails;
		protected System.Web.UI.HtmlControls.HtmlInputText hdnlbClients;
		protected System.Web.UI.HtmlControls.HtmlInputText hdnlbSelectedClients;
		protected System.Web.UI.HtmlControls.HtmlInputText hdnlbUsersMails;
		#endregion
		

		private static readonly ILog log = LogManager.GetLogger(typeof(EditProject));
		
		

		#region PageLoad
		
		private void Page_Load(object sender, System.EventArgs e)
		{
			ddlContents.Visible=btnAdd.Visible=false;
			if (!this.LoggedUser.IsAuthenticated) ErrorRedirect(Resource.ResourceManager["noRightsAlert"]);
      
			lkCalendar1.Attributes["onclick"] = TimeHelper.InvokePopupCal(txtStartDate);
			lkCalendar2.Attributes["onclick"] = TimeHelper.InvokePopupCal(txtEndDate);
			lkCalendar3.Attributes["onclick"] = TimeHelper.InvokePopupCal(txtEndDateContract);
			btnChoose.Attributes["onclick"] =  "OnBtnColorClick(document.getElementById('"+ txtColor.ClientID+"').value,1);";
			if(Page.IsPostBack == false)
			{
				// Set up form for data change checking when
				// first loaded.
				this.CheckForDataChanges= true;
				this.BypassPromptIds =
					new string[] { "btnSave","btnEdit",  "btnDelete","btnCreateFolder","btnDelete1","btnCalculate",
				"btnNewPayment","btnAdd","btnExportPay",
				"lkFull","lbShortDocumentation","lkFullProjectInfo","lkTechInfo",
				"lkMissingData","Imagebutton1","btnContents","btnTech", "lkProtPlus"};

				LoadUsers(); 
				LoadClients();
				
			}
			if(!LoggedUser.HasPaymentRights)
			{
				lbProjectContract.Visible = false;
				Linkbutton1.Visible=false;
				tbl.Visible=false;
				tblP.Visible=false;
			}
			else
			{
				lblStatus.Visible=ckBlack.Visible=ckActive.Visible=ckConcluded.Visible= true;
			}

			if (! (this.LoggedUser.IsAccountant || this.LoggedUser.IsLeader))
			{
				lkFull.Visible = false;
				Linkbutton2.Visible=false;
			}
			hidID.Value=ProjectID.ToString();
			if (!this.IsPostBack)
			{
				btnNewPayment.Text = Resource.ResourceManager["editsubprojects_btnNewPayment"];
				hlProjectFolder.Text = Resource.ResourceManager["projectFolderLinkLabel"];

				ddlScheme.DataSource=SubprojectsUDL.SelectSchemes();
				ddlScheme.DataValueField="PaymentSchemeID";
				ddlScheme.DataTextField="PaymentScheme";
				ddlScheme.DataBind();

				ddlContents.DataSource=SubprojectsUDL.SelectContentTypes(-1);
				ddlContents.DataValueField="ContentTypeID";
				ddlContents.DataTextField="ContentType";
				ddlContents.DataBind();

				int projectID = ProjectID;
				if (projectID>0)
				{
					LoadFromProjectID(projectID);
					//header.EditMode=false;
					header.PageTitle = Resource.ResourceManager["editProject_EditLabel"];
					if (!LoadProject(projectID)) lblError.Text = Resource.ResourceManager["editProject_ErrorLoadProject"];
				}
				else 
				{
					SessionTable = UIHelpers.GetNewPaymentsTable(0);
					DistrTable= new ContentsVector();
					ContentsTable = new ContentsVector();
					dlPayments.DataSource=SessionTable;
					dlPayments.DataBind();

					header.PageTitle = Resource.ResourceManager["editProject_NewLabel"];
					LoadBuildingTypes("0");
					LoadClients("0");
					LoadUsers("0");
				}
        
				header.UserName = this.LoggedUser.UserName;
				if ((!this.LoggedUser.IsLeader) || (projectID == -1)) 
				{
					btnDelete.Visible = false;
					btnDelete1.Visible=false;
				}
				else
				{
					SetConfirmDelete(btnDelete, txtProjectName.Text);
					SetConfirmDelete(btnDelete1, txtProjectName.Text);
				}
			}
      
			if(Page.Request.Params["print"]=="1")
			{
				Table1.Visible=header.Visible= false;
				SetNotVisible();
			}
			else if(Page.Request.Params["print"]=="header")
			{
				UIHelpers.FrontPage(this.Page,txtAdministrativeName.Text,txtInvestor.Text,txtAddInfo.Text,txtProjectCode.Text);
				SetNotVisible();
			}
			else if(Page.Request.Params["print"]=="reg")
			{
				UIHelpers.Reg(this.Page,txtAdministrativeName.Text,ddlClients.SelectedItem.Text,txtAddInfo.Text, ddlManager.SelectedItem.Text);
				SetNotVisible();
			}
			else if(Page.Request.Params["print"]=="Letter")
			{
				UIHelpers.LetterToClient(this.Page,txtAdministrativeName.Text,txtInvestor.Text,txtAddInfo.Text, ddlManager.SelectedItem.Text);
				SetNotVisible();
			}
			else if(Page.Request.Params["print"]=="udo")
			{
				UIHelpers.Udo(this.Page,txtAdministrativeName.Text,txtAddInfo.Text);
				SetNotVisible();
			}
			else if(Page.Request.Params["print"]=="protokol")
			{
				string sManager="";
				if(ddlClients.SelectedIndex>=0)
				{
					int nClientID =int.Parse(ddlClients.SelectedValue);
					SqlDataReader reader = null;
					try
					{
						reader = ClientsData.SelectClient(nClientID);
						if (reader.Read())
						{
						
							sManager = reader["Manager"]==DBNull.Value ? String.Empty :(string) reader["Manager"];
					
						}
					
					}
					catch (Exception ex)
					{
						log.Error(ex);
					
					}
					finally
					{
						if (reader!=null) reader.Close();
					}
				}
				
					
				UIHelpers.Protokol(true,this.Page,txtAdministrativeName.Text,txtInvestor.Text,sManager, txtArea.Text,txtAddInfo.Text,txtFoldersGiven.Text);
				
				SetNotVisible();

      
        
			}
			else if(Page.Request.Params["print"]=="fax")
			{
				string sFax="";
				if(ddlClients.SelectedIndex>=0)
				{
					int nClientID =int.Parse(ddlClients.SelectedValue);
					
					SqlDataReader reader = null;
					try
					{
						reader = ClientsData.SelectClient(nClientID);
						if (reader.Read())
						{
						
							sFax = reader.IsDBNull(6) ? String.Empty : reader.GetString(6);
					
						}
					
					}
					catch (Exception ex)
					{
						log.Error(ex);
					
					}
					finally
					{
						if (reader!=null) reader.Close();
					}
					
				}
				UIHelpers.Fax(this.Page,txtAdministrativeName.Text,txtAddInfo.Text,ddlClients.SelectedItem.Text,sFax,LoggedUser.FullName);
				SetNotVisible();
			}
			else
			{  
				UIHelpers.CreateMenu(menuHolder,LoggedUser);
				SetEditMode();
        
				InitEdit();
			}

			if(!(LoggedUser.IsAssistantOnly || LoggedUser.HasPaymentRights ||LoggedUser.IsSecretary))
			{
				btnEdit.Visible=false;
				if(ProjectID<=0)
					ErrorRedirect(Resource.ResourceManager["noRightsAlert"]);
			}
			
     
		}

	
		#endregion
		
		#region Other
		
		private void SetEditMode()
		{
			//			if (ProjectID>0)
			//			{
			//				bool b=header.EditMode;
			//				
			//				btn.Enabled=btnAdd.Enabled=btnCalculate.Enabled=btnDelete.Enabled=btnExportPay.Enabled=
			//					btnNewPayment.Enabled=btnSave.Enabled= b;
			//				
			//					ckPhases.Enabled=b;
			//				lkCalendar1.Visible=lkCalendar2.Visible=lkCalendar3.Visible=b;
			//				btnDelete1.Enabled=b;
			//
			//				txtProjectName.Enabled=
			//					txtInvestor.Enabled=txtProjectCode.Enabled=ckActive.Enabled=ckBlack.Enabled=
			//					txtAdministrativeName.Enabled=txtAddInfo.Enabled=btnAdd.Visible=ddlContents.Visible=
			//					grd.Enabled=grdDistr.Enabled=
			//					txtArea.Enabled=txtArea1.Enabled=
			//					txtStartDate.Enabled=txtEndDate.Enabled=txtEndDateContract.Enabled=
			//					ddlBuildingTypes.Enabled=txtAddress.Enabled=
			//					ddlClients.Enabled=txtFoldersArchive.Enabled=txtFoldersGiven.Enabled=
			//					dlPayments.Enabled=ddlManager.Enabled=ddlScheme.Enabled=b;
			//					
			//
			//			}
		}
		
		private void SetNotVisible()
		{
			Table1.Visible=header.Visible= false;
			btn.Visible=btnAdd.Visible=btnCalculate.Visible=btnCancel.Visible=btnDelete.Visible=btnExportPay.Visible=
				btnNewPayment.Visible=btnPrint.Visible=btnSave.Visible=btnCreateFolder.Visible= false;
			if(!ckPhases.Checked)
				ckPhases.Visible=false;
			lkCalendar1.Visible=lkCalendar2.Visible=lkCalendar3.Visible=false;
			btnDelete1.Visible=false;
		}

		public string IsPaid(object b)
		{
			if(b==null || b==DBNull.Value || !(b is bool))
				return Resource.ResourceManager["boolNO"];

			return ((bool)b)?Resource.ResourceManager["boolYES"]:Resource.ResourceManager["boolNO"];
			
		}

		//		private void CreateLinks()
		//		{
		//			UserControls.MenuTable menu = new UserControls.MenuTable();
		//			menu.ID = "MenuTable";
		//
		//			ArrayList menuItems = new ArrayList();
		//			menuItems.Add(new MenuItemInfo(Resource.ResourceManager["menuItem_Hours"], "Hours.aspx", true));
		//			menuItems.Add(new MenuItemInfo(Resource.ResourceManager["menuItem_Projects"], "Projects.aspx"));
		//			if ((LoggedUser.IsLeader) || (LoggedUser.IsAssistant)) 
		//			{
		//				menuItems.Add(new MenuItemInfo(Resource.ResourceManager["menuItem_Clients"], "Clients.aspx"));
		//				menuItems.Add(new MenuItemInfo(Resource.ResourceManager["menuItem_Subcontracters"], "Subcontracters.aspx"));
		//				if (LoggedUser.IsLeader)
		//					menuItems.Add(new MenuItemInfo(Resource.ResourceManager["menuItem_Users"], "Users.aspx"));			
		//			}
		//			menuItems.Add(new MenuItemInfo(Resource.ResourceManager["menuItem_Emails"], "Emails.aspx"));
		//			menu.AddMenuGroup(Resource.ResourceManager["menuHeader_Links"], 20, menuItems);
		//		}
		private decimal RecArea()
		{
			decimal d=0;
			int nCID=int.Parse(System.Configuration.ConfigurationSettings.AppSettings["GroundArea"]);
			ContentsVector ct =  ContentDAL.LoadCollection("ContentsList0Proc", SQLParms.CreateContentsList0Proc(ProjectID));

			foreach(ContentData cd in ct)
			{
				if(cd.ContentTypeID!=nCID)
					d+=cd.Area;
			}
			return d;
		}
		
		private ContentsVector GetDistr(int projectID)
		{
			ContentsVector cv= new ContentsVector();
			for(int i=0;i<grdDistr.Items.Count;i++)
			{
				TextBox  txtKota=(TextBox) grdDistr.Items[i].FindControl("txtKota");
				TextBox  txtA=(TextBox) grdDistr.Items[i].FindControl("txtA");
				TextBox  txtMa=(TextBox) grdDistr.Items[i].FindControl("txtMa");
				decimal k=0;
				decimal a=0;
				if(txtKota.Text!="")
					k=UIHelpers.ParseDecimal(txtKota.Text);

				if(txtA.Text!="")
					a=UIHelpers.ParseDecimal(txtA.Text);
				int SSID=int.Parse(grdDistr.Items[i].Cells[0].Text);
				Label lbOzn = (Label) grd.Items[i].FindControl("lbOzn");
				string code = lbOzn.Text;
				string[] arr = code.Split('R');
				string codeNew = arr[0];
				string codeRev = arr[1];
				ContentData cd = new ContentData(-1,projectID,SSID,k,a,"", txtMa.Text,1,"",codeNew,codeRev,i,0,0,0,0); 
				cd.ContentType= grdDistr.Items[i].Cells[1].Text;
				cv.Add(cd);
			}
			return cv;
		}
		private ContentsVector GetContents(int projectID)
		{
			ContentsVector cv= new ContentsVector();
			for(int i=0;i<grd.Items.Count;i++)
			{
				TextBox  txtT=(TextBox) grd.Items[i].FindControl("txtT");
				TextBox  txtMa=(TextBox) grd.Items[i].FindControl("txtMa");
				
				int SSID=int.Parse(grd.Items[i].Cells[0].Text);
				Label lbOzn = (Label) grd.Items[i].FindControl("lbOzn");
				string code = lbOzn.Text;
				string[]arr = code.Split('R');
				string codeNew = arr[0];
				string codeRev = "";
					if (arr.Length>1)
						codeRev = arr[1];
				ContentData cd = new ContentData(-1,projectID,SSID,0,0,txtT.Text, txtMa.Text,1,"",codeNew,codeRev,i,0,0,0,0); 	
				cd.ContentType= grd.Items[i].Cells[1].Text;
				cv.Add(cd);
			}
			return cv;
		}
		private decimal Recalculate()
		{
			decimal rate=0;
			if(txtRate.Text!="")
				rate=UIHelpers.ParseDecimal(txtRate.Text);
			decimal EUR=0;
			PaymentSchemes ps =(PaymentSchemes) int.Parse(ddlScheme.SelectedValue);
			if(ps==PaymentSchemes.Fixed)
				EUR=rate;
			else if(ps==PaymentSchemes.Area)
				EUR=rate*UIHelpers.ParseDecimal(txtArea.Text);
			decimal fix=decimal.Parse( System.Configuration.ConfigurationSettings.AppSettings["fixedBGNEUR"]);
			lbEUR.Text=UIHelpers.FormatDecimal2(EUR);
			lbBGN.Text=UIHelpers.FormatDecimal2(EUR*fix);
			return EUR;
		}
		private void SetRec()
		{
			decimal EUR=Recalculate();
			decimal dTotal=0;
			for(int i=0;i<dlPayments.Items.Count;i++)
			{
				decimal per=0;
				

				TextBox txtPer=(TextBox) dlPayments.Items[i].FindControl("txtPer");
				if(txtPer!=null && txtPer.Text!="")
				{
					per = UIHelpers.ParseDecimal(txtPer.Text);
					
				}
				
				TextBox txtAmount=(TextBox) dlPayments.Items[i].FindControl("txtAmount");
				CheckBox ckPaid=(CheckBox) dlPayments.Items[i].FindControl("ckPaid");
				if(txtAmount!=null )
				{

					if(txtAmount.Text=="")
					{
						if(i==dlPayments.Items.Count-1 && txtPer.Text=="")
						{
							if(EUR>0)
							{
								if(!ckPaid.Checked)
									txtAmount.Text=UIHelpers.FormatDecimal2(EUR-dTotal);
								decimal d = UIHelpers.ParseDecimal(txtAmount.Text);
								per= d/EUR;
								txtPer.Text=UIHelpers.FormatDecimal4(per*100);
							}
						}
						else
						{
							if(!ckPaid.Checked)
								txtAmount.Text=UIHelpers.FormatDecimal2((decimal)per * EUR/100);
						}
					}
					else if(txtPer.Text=="")
					{
						decimal d = UIHelpers.ParseDecimal(txtAmount.Text);
						per= d/EUR;
						txtPer.Text=UIHelpers.FormatDecimal4(per*100);
					}
					else 
					{
						if(!ckPaid.Checked)
							txtAmount.Text=UIHelpers.FormatDecimal2((decimal)per * EUR/100);
					}
					dTotal+=UIHelpers.ParseDecimal(txtAmount.Text);
		
				}

				
			}
		}
		
		private void BindGrid()
		{
			//			grdDistr.DataSource=DistrTable;
			//			grdDistr.DataKeyField="ContentID";
			//			grdDistr.DataBind();
			grdDistr.Visible=grdDistr.Items.Count>0;
			//			grd.DataSource=ContentsTable;
			//			grd.DataKeyField="ContentID";
			//			grd.DataBind();
			grd.Visible=grd.Items.Count>0;
		}
	
		private void Reload()
		{
			SetArea();
			SetRec();
		}
		private void SetArea()
		{
			
			txtArea1.Text= UIHelpers.FormatDecimal2( RecArea());
			if(txtArea.Text=="")
				txtArea.Text=txtArea1.Text;
		}
				
		private void SetPaymentType(bool byPhases)
		{
			btn.Visible = !byPhases;
      
			if (!byPhases) return;

			decimal amountByPhases = ProjectsData.SelectSubprojectsAmount(this.ProjectID);

			decimal fix=decimal.Parse( System.Configuration.ConfigurationSettings.AppSettings["fixedBGNEUR"]);
			lbEUR.Text = UIHelpers.FormatDecimal2(amountByPhases);
			lbBGN.Text=UIHelpers.FormatDecimal2(amountByPhases*fix);
		}

		private void CreateProjectFolder(int projectID)
		{
			string rootFolderPath = SettingsManager.ProjectRootFolderPath;

			string src = rootFolderPath+"Structure";

			int buildingTypeID; 
			string buildingTypeCode;
			int number;
			string projectName;
			string projectCode;
			string folderPath;
    

			ProjectsData.SelectProjectCode(projectID, out buildingTypeID, out buildingTypeCode, 
				out number, out projectName, out projectCode, out folderPath);

			if (number == -1)
			{
				return;
			}
			if(projectCode.Length>=4 && projectCode.Substring(0,2)==lblBTCode.Text
				&& projectCode.Substring(2,2)==lblProjectOrder.Text)
			{
				projectCode=projectCode.Substring(4);
			}
			if(projectCode.StartsWith("XX"))
				projectCode=projectCode.Substring(2);
			string dest = rootFolderPath+BuildProjectFolderName(buildingTypeCode, number, projectCode, projectName,ddlBuildingTypes.SelectedItem.Text);
			if (!Directory.Exists(dest))
				CopyDirectory(src, dest);

		}

		private void InitEdit()
		{
			if (this.ProjectID == -1) return;

			string vmButtons = this.btnEdit.ClientID;
			string emButtons = this.btnSave.ClientID+";"+this.btnDelete.ClientID+";"+this.btnDelete1.ClientID+";"+this.btnChoose.ClientID;;

			string exl = this.hlProjectFolder.ClientID;

			editCtrl.InitEdit("formCell;tblP", vmButtons, emButtons, exl);
		}

		protected string GetMails1()
		{
			char _sep=',';
			string mailTo = "";
			int PrID = ProjectID;
			UserInfo ui = UsersData.SelectUserByID(ProjectsData.SelectProjectManagerID(PrID));
			mailTo = string.Concat(mailTo,ui.Mail);	
			ProjectUsersVector puv = ProjectUserDAL.LoadCollection("ProjectUsersSelByProjectProc",SQLParms.CreateProjectUsersSelByProjectProc(PrID));
			foreach(ProjectUserData pud in puv)
			{
				UserInfo ud = UsersData.SelectUserByID(pud.UserID);
				mailTo = string.Concat(mailTo,_sep,ud.Mail);				
			}   
			ProjectSubcontractersVector psv = ProjectSubcontracterDAL.LoadCollection("ProjectSubcontractersListProc",SQLParms.CreateProjectSubcontractersListProc(PrID));
			foreach(ProjectSubcontracterData psd in psv)
			{
				SubcontracterData sd = SubcontracterDAL.Load(psd.SubcontracterID);
				if(sd!=null)
				{
					if(!(sd.Email==string.Empty))
						mailTo = string.Concat(mailTo,_sep,sd.Email);
					if(!(sd.Email1==string.Empty))
						mailTo = string.Concat(mailTo,_sep,sd.Email1);
					if(!(sd.Email2==string.Empty))
						mailTo = string.Concat(mailTo,_sep,sd.Email2);
				}
			}   
			if (ProjectsData.SelectProjectClientID(PrID)!=0)
			{
				ClientData cd = ClientDAL.Load(ProjectsData.SelectProjectClientID(PrID));
				if(cd!=null)
				{
					if(!(cd.Email==string.Empty))
						mailTo = string.Concat(mailTo,_sep,cd.Email);
					if(!(cd.Email1==string.Empty))
						mailTo = string.Concat(mailTo,_sep,cd.Email1);
					if(!(cd.Email2==string.Empty))
						mailTo = string.Concat(mailTo,_sep,cd.Email2);
				}
			}
			return mailTo;
		}
		protected string GetMails()
		{			
			string subject = ProjectsData.SelectProjectName(ProjectID);
			
			return "<a href='mailto:"+GetMails1()+"?subject="+subject+"' />Писмо до екип, подизпълнители и клиенти</a>";
		}
		//Check if txtColor starts with # and if not insert # at the Begining
		private void TextColourRightInput()
		{
			if((txtColor.Text!=null)&&(txtColor.Text!=""))
				if(!(txtColor.Text.StartsWith("#")))
				{
					txtColor.Text = string.Concat("#",txtColor.Text);
				}
		}
		protected ChecksVector GetChecksTable(int n)
		{
			string name= "ChecksTable"+n.ToString();
			
			if (this.Session[name] != null)
			{
				return (ChecksVector)this.Session[name];
			}
			ChecksVector pv =  new ChecksVector();
			this.Session[name]=pv;
			return pv;
		}
		
		
		#endregion
		
		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
			this.header.EditClicked+=new Asa.Timesheet.WebPages.UserControls.PageHeader.EditHandler(header_EditClicked);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
			this.ddlBuildingTypes.SelectedIndexChanged += new System.EventHandler(this.ddlBuildingTypes_SelectedIndexChanged);
			this.btnCreateFolder.Click += new System.EventHandler(this.btnCreateFolder_Click);
			this.btnAdd.Click += new System.EventHandler(this.btnAdd_Click);
			this.btnCalculate.Click += new System.EventHandler(this.btnCalculate_Click);
			this.ddlClients.SelectedIndexChanged += new System.EventHandler(this.ddlClients_SelectedIndexChanged);
			this.btn.Click += new System.EventHandler(this.btn_Click);
			this.ckPhases.CheckedChanged += new System.EventHandler(this.ckPhases_CheckedChanged);
			this.lkProfile.Click += new System.EventHandler(this.lkProfile_Click);
			this.lkSubpr.Click += new System.EventHandler(this.lkSubpr_Click);
			this.lkSubcontracters.Click += new System.EventHandler(this.lkSubcontracters_Click);
			this.Linkbutton1.Click += new System.EventHandler(this.Linkbutton1_Click);
			this.lbProjectContract.Click += new System.EventHandler(this.lbProjectContract_Click);
			this.lbProjectBuilding.Click += new System.EventHandler(this.lbProjectBuilding_Click);
			this.lkFull.Click += new System.EventHandler(this.lkFull_Click);
			this.lbShortDocumentation.Click += new System.EventHandler(this.lbShortDocumentation_Click);
			this.Linkbutton2.Click += new System.EventHandler(this.Linkbutton2_Click);
			this.lkFullProjectInfo.Click += new System.EventHandler(this.lkFullProjectInfo_Click);
			this.btnContents.Click += new System.EventHandler(this.btnContents_Click);
			this.img.Click += new System.Web.UI.ImageClickEventHandler(this.img_Click);
			this.btnContentExcel.Click += new System.Web.UI.ImageClickEventHandler(this.btnContentExcel_Click);
			this.btnTech.Click += new System.EventHandler(this.btnTech_Click);
			this.imgTechWord.Click += new System.Web.UI.ImageClickEventHandler(this.imgTechWord_Click);
			this.imgTechExcel.Click += new System.Web.UI.ImageClickEventHandler(this.imgTechExcel_Click);
			this.lkMissingData.Click += new System.EventHandler(this.lkMissingData_Click);
			this.lkProtPlus.Click += new System.EventHandler(this.lkProtPlus_Click);
			this.lkFolder1.Click += new System.EventHandler(this.lkFolder1_Click);
			this.lkFolder2.Click += new System.EventHandler(this.lkFolder2_Click);
			this.dlPayments.ItemDataBound += new System.Web.UI.WebControls.DataListItemEventHandler(this.dlPayments_ItemDataBound);
			this.btnNewPayment.Click += new System.EventHandler(this.btnNewPayment_Click);
			this.btnExportPay.Click += new System.EventHandler(this.btnExportPay_Click);
			this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
			this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
			this.btnDelete.Click += new System.EventHandler(this.btnDelete_Click);
			this.btnDelete1.Click += new System.EventHandler(this.btnDelete1_Click);
			this.Load += new System.EventHandler(this.Page_Load);

		}
		#endregion

		#region Database 

		private bool LoadProject(int projectID)
		{
			int buildingTypeID = 0, clientID = 0, projectManagerID = 0;
			
			SqlDataReader reader = null;

			try
			{
				reader = ProjectsData.SelectProject(projectID);
				if(reader.Read())
				{

					txtProjectName.Text = reader.GetString(1);
					txtProjectCode.Text = reader.IsDBNull(2) ? String.Empty : reader.GetString(2);
					txtAdministrativeName.Text = reader.IsDBNull(12) ? String.Empty : reader.GetString(12);
					buildingTypeID = reader.IsDBNull(3) ? 0 : reader.GetInt32(3);
					txtArea.Text = reader.IsDBNull(4) ? String.Empty : UIHelpers.FormatDecimal2(reader.GetDecimal(4));
					clientID = reader.IsDBNull(5) ? 0 : reader.GetInt32(5);
					cbHasActivity.Checked = reader.GetBoolean(8);
					txtStartDate.Text=	reader.IsDBNull(9)?"":TimeHelper.FormatDate(reader.GetDateTime(9));
					//				calStartDate.SelectedDate = reader.GetDateTime(9);
					//				calStartDate.VisibleDate = calStartDate.SelectedDate;
					txtAddress.Text = reader.IsDBNull(10) ? String.Empty : reader.GetString(10);
					projectManagerID = reader.IsDBNull(11) ? 0 : reader.GetInt32(11);

					txtOwner.Text = reader.IsDBNull(26) ? String.Empty : reader.GetString(26);

					txtInvestor.Text=reader.IsDBNull(13) ? String.Empty : reader.GetString(13);
					txtAddInfo.Text=reader.IsDBNull(14) ? String.Empty : reader.GetString(14);
					ddlScheme.SelectedValue=(reader.IsDBNull(15) ? 1 : reader.GetInt32(15)).ToString();
					txtRate.Text=UIHelpers.FormatDecimal2( reader.IsDBNull(16) ? 0 : reader.GetDecimal(16));

					txtArea1.Text=reader.IsDBNull(18) ? String.Empty : UIHelpers.FormatDecimal2(reader.GetDecimal(18));
					ckPhases.Checked= reader.IsDBNull(19) ? false : reader.GetBoolean(19);
					txtEndDate.Text=	reader.IsDBNull(20)?"":TimeHelper.FormatDate(reader.GetDateTime(20));
					txtEndDateContract.Text=	reader.IsDBNull(21)?"":TimeHelper.FormatDate(reader.GetDateTime(21));
					ckActive.Checked=	reader.IsDBNull(22)?false:reader.GetBoolean(22);
					ckBlack.Checked=	reader.IsDBNull(23)?true:reader.GetBoolean(23);
					if(!reader.IsDBNull(24))
						txtFoldersGiven.Text =   reader.GetInt32(24).ToString();
					if(!reader.IsDBNull(25))
						txtFoldersArchive.Text =   reader.GetInt32(25).ToString();
					if(!reader.IsDBNull(27))
						ckConcluded.Checked=reader.GetBoolean(27);
					if(!reader.IsDBNull(28))
						txtAuthor.Text=UIHelpers.FormatDecimal2( reader.GetDecimal(28));
					if(!reader.IsDBNull(29))
						ddlAuth.SelectedValue=reader.GetInt32(29).ToString();
					if(!reader.IsDBNull(30))
						txtColor.Text=reader.GetString(30);
					if(!reader.IsDBNull(31))
						txtProjectPhase.Text = reader.GetString(31);
					if(LoggedUser.HasPaymentRights)
					{
						tblP.Visible=!ckPhases.Checked;
					}
					PaymentsVector pv = PaymentDAL.LoadCollection("PaymentsListByProjectProc", SQLParms.CreatePaymentsListBySubprojectProc(projectID));
					if(pv==null ||pv.Count==0)
						pv= UIHelpers.GetNewPaymentsTable(reader.IsDBNull(16) ? 0 : reader.GetDecimal(17));
					SessionTable=pv;
					dlPayments.DataSource=pv;
					dlPayments.DataBind();

					ContentsVector cv1 = ContentDAL.LoadCollection("ContentsList0Proc", SQLParms.CreateContentsList0Proc(projectID));
					DistrTable=cv1;
					ContentsVector cv2 = ContentDAL.LoadCollection("ContentsListAllProc", SQLParms.CreateContentsListAllProc(projectID));
					ContentsTable=cv2;
					BindGrid();
					SetRec();
				}
			}
			catch (Exception ex)
			{
				log.Info(ex);
				return false;
			}

			finally 
			{ 
				if (reader!=null) reader.Close(); 
			}

			if (! ( LoadBuildingTypes(buildingTypeID.ToString())&& LoadClients(clientID.ToString())
				&& LoadUsers(projectManagerID.ToString()) ) ) return false;

			LoadProjectCode(projectID);
			
			if(txtInvestor.Text.Trim()=="")
			{
				txtInvestor.Text=ddlClients.SelectedItem.Text;
			}
			if(txtAddInfo.Text.Trim()=="")
			{
				txtAddInfo.Text=txtAddress.Text;
			}

			SetPaymentType(ckPhases.Checked);
			
			return true;
		}

		private bool LoadProjectCode(int projectID)
		{
			int buildingTypeID = 0;
			string buildingTypeCode;
			int number;
			string projectName;
			string projectCode;
			string folderPath;

			try
			{
				ProjectsData.SelectProjectCode(projectID, out buildingTypeID, out buildingTypeCode, 
					out number, out projectName, out projectCode, out folderPath);
			}
			catch (Exception ex)
			{
				log.Error(ex.Message);

				lblError.Text = Resource.ResourceManager["errorLoadPartOfProjectInfo"];

				return false;
			}

			#region fill code labels

			if (buildingTypeID == PreProjectBuildingTypeID)
			{
				lblBTCode.Text = "XX";
				lblProjectOrder.Text = "";
       
			}
			else
			{
				lblBTCode.Text = buildingTypeCode;

				if (buildingTypeID == 0)
				{
					lblProjectOrder.Text = "";
        
				}
				else lblProjectOrder.Text = number < 10 ? "0"+number.ToString() : number.ToString();
			}

			#endregion
			if(txtProjectCode.Text.Length>=4 && txtProjectCode.Text.Substring(0,2)==lblBTCode.Text
				&& txtProjectCode.Text.Substring(2,2)==lblProjectOrder.Text)
			{
				txtProjectCode.Text=txtProjectCode.Text.Substring(4);
			}
			if(txtProjectCode.Text.StartsWith("XX"))
					txtProjectCode.Text=txtProjectCode.Text.Substring(2);
			folderPath = SettingsManager.ProjectRootFolderPath+
				BuildProjectFolderName(buildingTypeCode, number, txtProjectCode.Text, txtProjectName.Text,ddlBuildingTypes.SelectedItem.Text);

			//if (Directory.Exists(folderPath)) 
			{ 
				hlProjectFolder.Visible = true;
				//btnCreateFolder.Visible = false;
        
				hlProjectFolder.Text = 
				hlProjectFolder.NavigateUrl = folderPath;

				btnCreateFolder.Visible = true; 
			}
//			else
//			{
//				hlProjectFolder.Visible = false;
//				btnCreateFolder.Visible = true;
//
//				if (buildingTypeID==0 || buildingTypeID == PreProjectBuildingTypeID)
//				{
//					btnCreateFolder.Visible = false;
//				}
//			}

			return true;
		}
	
		private bool LoadBuildingTypes(string selectedValue)
		{
			SqlDataReader reader = null;
			try
			{
				reader = DBManager.SelectBuildingTypes();
				ddlBuildingTypes.DataSource = reader;
				ddlBuildingTypes.DataValueField = "BuildingTypeID";
				ddlBuildingTypes.DataTextField = "BuildingType";
				ddlBuildingTypes.DataBind();

				ddlBuildingTypes.Items.Insert(0, new ListItem("", "0"));
				ddlBuildingTypes.SelectedValue = selectedValue;
			}
			catch (Exception ex)
			{
				log.Info(ex);
				return false;
			}
			finally 
			{
				if (reader!=null) reader.Close();
			}

			return true;
		}

		private bool LoadClients(string selectedValue)
		{
			SqlDataReader reader = null;
			try
			{
				reader = ClientsData.SelectClients(0);
				ddlClients.DataSource = reader;
				ddlClients.DataValueField = "ClientID";
				ddlClients.DataTextField = "ClientName";
				ddlClients.DataBind();

				ddlClients.Items.Insert(0, new ListItem("", "0"));
				if(ddlClients.Items.FindByValue( selectedValue)!=null)
					ddlClients.SelectedValue = selectedValue;
				
				SelectedValueDiferent(true);
			}
			catch (Exception ex)
			{
				log.Info(ex);
				return false;
			}
			finally 
			{
				if (reader!=null) reader.Close();
			}
			return true;
		}

		private bool LoadUsers(string selectedValue)
		{
			SqlDataReader reader = null;
			try
			{
				reader = UsersData.SelectUserNames();
				ddlManager.DataSource = reader;
				ddlManager.DataValueField = "UserID";
				ddlManager.DataTextField = "FullName";
				ddlManager.DataBind();

				ddlManager.Items.Insert(0, new ListItem("", "0"));
				if(ddlManager.Items.FindByValue(selectedValue)!=null)
					ddlManager.SelectedValue = selectedValue;
			}
			catch (Exception ex)
			{
				log.Info(ex);
				return false;
			}
			finally 
			{
				if (reader!=null) reader.Close();
			}
			return true;
		}

		private void LoadFromProjectID(int ProjectID)
		{ 
			string s="";
			ProjectUsersVector puv = ProjectUserDAL.LoadCollection("ProjectUsersSelByProjectProc",SQLParms.CreateProjectUsersSelByProjectProc(ProjectID));
			foreach(ProjectUserData pud in puv)
			{
				s+=";";
				s+=pud.UserID.ToString();
			}   

			lbSelectedUsersMails.Items.Clear();
			UIHelpers.SetSelected(s, lbUsersMails, lbSelectedUsersMails);
			hdnlbSelectedUsersMails.Value=s;
			//new
			s="";
			ProjectClientsVector pcv = ProjectClientDAL.LoadCollection("ProjectClientsSelByProjectProc",SQLParms.CreateProjectClientsSelByProjectProc(ProjectID));
			foreach(ProjectClientData pud in pcv)
			{
				s+=";";
				s+=pud.ClientID.ToString();
			}   

			lbSelectedClients.Items.Clear();
			UIHelpers.SetSelected(s, lbClients, lbSelectedClients);
			hdnlbSelectedClients.Value=s;
			
		}
		
		private bool LoadUsers()
		{
			SqlDataReader reader = null;
			try
			{
				reader = UsersData.SelectUserNames(false, true);
				lbUsersMails.DataSource = reader;
				lbUsersMails.DataValueField = "UserID";
				lbUsersMails.DataTextField = "FullName";
				lbUsersMails.DataBind();
			}
			catch (Exception ex)
			{
				log.Error(ex);
				return false;
			}
			finally
			{
				if (reader!=null) reader.Close();
			}		
			return true;

		}

		private bool LoadClients()
		{
			try
			{
				ClientsVector cv = ClientDAL.LoadCollection();
				lbClients.DataSource = cv;
				lbClients.DataValueField = "ClientID";
				lbClients.DataTextField = "ClientName";
				lbClients.DataBind();
			}
			catch (Exception ex)
			{
				log.Error(ex);
				return false;
			}
			
			return true;

		}	
		
		#endregion

		#region Event handlers		
		private void ckPhases_CheckedChanged(object sender, System.EventArgs e)
		{
			tblP.Visible=!ckPhases.Checked;
			Reload();

			SetPaymentType(ckPhases.Checked);
		}
	
		//		private void btnContents_Click(object sender, System.EventArgs e)
		//		{
		//			PdfExport rtf = new PdfExport();
		//			rtf.NeverEmbedFonts = "";
		//			DataDynamics.ActiveReports.ActiveReport report = new Contents(ProjectID,LoggedUser.FullName,false);
		//			report.Run();
		//			System.IO.MemoryStream memoryFile = new System.IO.MemoryStream();
		//
		//			rtf.Export(report.Document, memoryFile);
		//			Response.Clear();
		//
		//			Response.AppendHeader( "content-disposition","attachment; filename="+"Contents.pdf");
		//			Response.ContentType = "application/pdf";
		//
		//
		//			memoryFile.WriteTo(Response.OutputStream); 
		//			Response.End(); 
		//		}

		//		private void btnTech_Click(object sender, System.EventArgs e)
		//		{
		//			PdfExport pdf = new PdfExport();
		//			XlsExport xls = new XlsExport();
		//		
		//			pdf.NeverEmbedFonts = "";
		//
		//			DataDynamics.ActiveReports.ActiveReport report = new Pokazateli(ProjectID,LoggedUser.FullName);
		//			report.Run();
		//			System.IO.MemoryStream memoryFile = new System.IO.MemoryStream();
		//
		//			pdf.Export(report.Document, memoryFile);
		//			Response.Clear();
		//
		//			Response.AppendHeader( "content-disposition","attachment; filename="+"Pokazateli.pdf");
		//			Response.ContentType = "application/pdf";
		//
		//
		//			memoryFile.WriteTo(Response.OutputStream); 
		//			Response.End(); 
		//		}
		
		private void header_EditClicked(object sender, EventArgs e)
		{
			SetEditMode();
		}

		//		private void Imagebutton1_Click(object sender, System.Web.UI.ImageClickEventArgs e)
		//		{
		//			RtfExport rtf = new RtfExport();
		//			
		//			DataDynamics.ActiveReports.ActiveReport report = new Contents(ProjectID,LoggedUser.FullName,true);
		//			report.Run();
		//			System.IO.MemoryStream memoryFile = new System.IO.MemoryStream();
		//
		//			rtf.Export(report.Document, memoryFile);
		//			Response.Clear();
		//
		//			Response.AppendHeader( "content-disposition","attachment; filename="+"Contents.rtf");
		//			Response.ContentType = "application/pdf";
		//
		//
		//			memoryFile.WriteTo(Response.OutputStream); 
		//			Response.End(); 
		//		}

	
		private void ddlBuildingTypes_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			int projectID = this.ProjectID;

			int buildingTypeID = int.Parse(ddlBuildingTypes.SelectedValue);
			int number = 0;
			string buildingTypeCode = String.Empty;
      
			try
			{
				if (projectID == -1) number = ProjectsData.SelectNextNumberForBuildingType(buildingTypeID);
				else number = ProjectsData.SelectProjectNumberByBuildingType(projectID, buildingTypeID);
        
				buildingTypeCode = ProjectsData.SelectBuildingTypeCode(buildingTypeID);
			}
			catch (Exception ex)
			{
				log.Error(ex.Message);

				lblError.Text = Resource.ResourceManager["errorLoadProjectCodeInfo"];
			}

			if (buildingTypeID == PreProjectBuildingTypeID)
			{
				lblBTCode.Text = "XX";
				lblProjectOrder.Text = String.Empty;
			} 
			else 
			{
				lblBTCode.Text = buildingTypeCode;
				lblProjectOrder.Text = number.ToString().Length == 1 ? "0"+number.ToString() : number.ToString();
			}
		}

		private void dlPayments_ItemDataBound(object sender, System.Web.UI.WebControls.DataListItemEventArgs e)
		{
			
			HtmlImage hi=(HtmlImage) e.Item.FindControl("lkCal");
			TextBox txt=(TextBox) e.Item.FindControl("txtDate");
			if(txt!=null&& hi!=null)
				hi.Attributes["onclick"] = TimeHelper.InvokePopupCal(txt);

		}
		private void grdDistr_DeleteCommand(object source, System.Web.UI.WebControls.DataGridCommandEventArgs e)
		{
			DistrTable=GetDistr(ProjectID);
			ContentsTable=GetContents(ProjectID);
			DistrTable.RemoveAt(e.Item.ItemIndex);
					
			BindGrid();
			Reload();
		}

		private void grd_DeleteCommand(object source, System.Web.UI.WebControls.DataGridCommandEventArgs e)
		{
			DistrTable=GetDistr(ProjectID);
			ContentsTable=GetContents(ProjectID);
			ContentsTable.RemoveAt(e.Item.ItemIndex);
					
			BindGrid();
			Reload();
		}

		#endregion

		#region private properties

		private int ProjectID
		{
			get 
			{
				return UIHelpers.ToInt(Request.Params["pid"]);
			}		
		}

		#endregion	
		
		#region Util

		private static string BuildProjectFolderName( string buildingTypeCode, int projectOrderByBuildingType, string codeAdd, string projectName,string buildName)
		{
			if (projectOrderByBuildingType<0  || projectName.Trim().Length == 0) return string.Empty;
 
			System.Text.StringBuilder sb = new System.Text.StringBuilder();
			sb.Append(buildingTypeCode+" "+buildName+@"\");
			if(buildingTypeCode==(PreProjectBuildingTypeID.ToString().PadLeft(2,'0')))
			{
				sb.Append("XX "+codeAdd.Trim()+" ");
				sb.Append(projectName);
      
				return sb.ToString();
			}
			sb.Append(buildingTypeCode+" ");
			if (projectOrderByBuildingType<10) sb.Append("0"+projectOrderByBuildingType.ToString()+" ");
			else sb.Append(projectOrderByBuildingType.ToString()+" ");
			sb.Append(codeAdd.Trim()+" ");
			sb.Append(projectName);
      
			return sb.ToString();
		}


		public static void CopyDirectory(string srcPath, string dstPath)
		{
			if (Directory.Exists(dstPath))
				return;
			string[] files;

			if (dstPath[dstPath.Length-1] != Path.DirectorySeparatorChar) dstPath += Path.DirectorySeparatorChar;
			if(!Directory.Exists(dstPath)) Directory.CreateDirectory(dstPath);
      
			files = Directory.GetFileSystemEntries(srcPath);
      
			foreach (string element in files)
			{
				if (Directory.Exists(element)) CopyDirectory(element, dstPath + Path.GetFileName(element));
				else File.Copy( element, dstPath + Path.GetFileName(element), false);
			}
		}

		
		#endregion

		#region properties

		private string ProjectNameOrig
		{
			get 
			{
				try
				{
					return (string)this.ViewState["ProjectNameOrig"];
				}
				catch
				{
					return String.Empty;
				}
			}
			set
			{
				this.ViewState["ProjectNameOrig"] = value;
			}
		}

		
		#endregion
		
		#region Event Handlers - Clicks
		
		#region Save and delete
		
		private void btnSave_Click(object sender, System.EventArgs e)
		{
			#region validate input
			
			SetRec();
			SetArea();
			
			string projectName = txtProjectName.Text.Trim();
						if (projectName == String.Empty) 
						{
							AlertFieldNotEntered(lblName);
							return;
						}

			string companyOwner = txtOwner.Text.Trim();

			string projectCode = lblBTCode.Text + lblProjectOrder.Text+ txtProjectCode.Text.Trim();
			if (projectCode == String.Empty) 
			{
				AlertFieldNotEntered(lblProjectCode);
				Dirty=true;
				return;
			}

			decimal totall=0;
			decimal dEUR=0;
			for(int i=0;i<dlPayments.Items.Count;i++)
			{
				decimal per=0;
				decimal am=0;

				TextBox txtPer=(TextBox) dlPayments.Items[i].FindControl("txtPer");
				if(txtPer!=null && txtPer.Text!="")
				{
					per = UIHelpers.ParseDecimal(txtPer.Text);
					totall+=per;
				}

				TextBox txtAmount=(TextBox) dlPayments.Items[i].FindControl("txtAmount");
				if(txtAmount!=null && txtAmount.Text!="")
				{
					am = UIHelpers.ParseDecimal(txtAmount.Text);
					dEUR+=am;
				
				}
				
			}

			decimal area1=UIHelpers.ParseDecimal(txtArea1.Text);
			decimal area= 0;
			if (txtArea.Text.Trim()!=String.Empty)
			{
				try
				{
					area = UIHelpers.ParseDecimal(txtArea.Text);
				}
				catch
				{
					AlertFieldNotEntered(lblArea);
					Dirty=true;
					return;
				}
			}

			decimal rate,total;
			rate=total=0;
			if(txtRate.Text!="")
				rate=UIHelpers.ParseDecimal(txtRate.Text);
			
			PaymentSchemes ps =(PaymentSchemes)int.Parse(ddlScheme.SelectedValue);
			if(ps==PaymentSchemes.Fixed)
				total=rate;
			else if(ps==PaymentSchemes.Area)
				total=rate*(decimal)area;
			if(!ckPhases.Checked  && (dEUR!=total ))
			{
				lblError.Text=string.Format(Resource.ResourceManager["editsubprojects_warn"],dEUR,total);
				Dirty=true;
				return;
			}
			
			string administrativeName = txtAdministrativeName.Text.Trim();
			if (administrativeName == String.Empty) 
			{
				administrativeName = null;
			}
			DateTime dtStart= TimeHelper.GetDate(txtStartDate.Text);
			if (dtStart==Constants.DateMax) 
			{
				txtStartDate.Text =TimeHelper.FormatDate( new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day));
				//calStartDate.VisibleDate = calStartDate.SelectedDate;
			}
			DateTime dtEnd= TimeHelper.GetDate(txtEndDate.Text);
			DateTime dtEndC= TimeHelper.GetDate(txtEndDateContract.Text);
			

			string address = txtAddress.Text.Trim();
			
			if (ddlManager.SelectedValue == "0")
			{
				AlertFieldNotEntered(lblManager);
				Dirty=true;
				return;
			}

			#endregion

			TextColourRightInput();
			bool bPrevPhases=true;
			if(this.ProjectID>0)
			{ 
				string projectName1, projectCode1, administrativeName1,add;
				decimal area2=0;
				int clientID=0;
				
				ProjectsData.SelectProject(this.ProjectID, out projectName1, out  projectCode1, out administrativeName1, out area2, out add, 
					out clientID,out bPrevPhases);
			}
			int projectID = this.ProjectID;
			decimal dAuth=0;
			if(txtAuthor.Text.Trim()!="")
				dAuth=decimal.Parse(txtAuthor.Text);
			if (projectID == -1) 
			{
				SqlDataReader reader=null;
				try
				{
					reader = ProjectsData.SelectProjects(0,  1,  projectName, -1, false);
					if (reader.Read())
					{
						lblError.Text = Resource.ResourceManager["editProject_ErrorExists"];
						Dirty=true;
						return;
					}
				}
				catch (Exception ex)
				{
					log.Info(ex);
					lblError.Text = Resource.ResourceManager["projects_ErrorLoadProjects"];
					Dirty=true;
					return;
				}
				finally
				{
					if (reader!=null) reader.Close();
				}
				
				try
				{
					projectID = ProjectsData.InsertProject(projectName, projectCode, administrativeName,
						int.Parse(ddlBuildingTypes.SelectedValue), area, int.Parse(ddlClients.SelectedValue), 
						true, ckActive.Checked, cbHasActivity.Checked, dtStart, 
						address, int.Parse(ddlManager.SelectedValue), companyOwner,
						txtInvestor.Text, txtAddInfo.Text, int.Parse(ddlScheme.SelectedValue),
						rate,total,area1,ckPhases.Checked,dtEnd,dtEndC, ckBlack.Checked, UIHelpers.ToInt(txtFoldersGiven.Text),UIHelpers.ToInt(txtFoldersArchive.Text),ckConcluded.Checked,dAuth,int.Parse(ddlAuth.SelectedValue),txtColor.Text);
					MailBO.SendNewProjectMail(projectName);
				}
				catch (Exception ex)
				{
					log.Info(ex);
					lblError.Text = Resource.ResourceManager["editProject_ErrorSave"];
					return;
				}
				
			}
			else
			{
				try
				{
					
					ProjectsData.UpdateProject(projectID, projectName, projectCode, administrativeName, 
						int.Parse(ddlBuildingTypes.SelectedValue), area, int.Parse(ddlClients.SelectedValue),
						cbHasActivity.Checked,dtStart, 
						address, int.Parse(ddlManager.SelectedValue), companyOwner,
						txtInvestor.Text, txtAddInfo.Text, int.Parse(ddlScheme.SelectedValue),
						rate,total,area1,ckPhases.Checked,dtEnd,dtEndC,ckBlack.Checked,ckActive.Checked, UIHelpers.ToInt(txtFoldersGiven.Text),UIHelpers.ToInt(txtFoldersArchive.Text),ckConcluded.Checked,dAuth,int.Parse(ddlAuth.SelectedValue),txtColor.Text);
				}
				catch (Exception ex)
				{
					log.Info(ex);
					lblError.Text = Resource.ResourceManager["editProject_ErrorSave"];
					return;
				}
			}

			

			PaymentsVector pv = new PaymentsVector();
			if(!ckPhases.Checked)
			{
				
				for(int i=0;i<dlPayments.Items.Count;i++)
				{
					decimal per=0;
					decimal am=0;

					TextBox txtPer=(TextBox) dlPayments.Items[i].FindControl("txtPer");
					if(txtPer!=null && txtPer.Text!="")
					{
						per =UIHelpers.ParseDecimal(txtPer.Text);
						//totall+=per;
					}
					TextBox txtAmount=(TextBox) dlPayments.Items[i].FindControl("txtAmount");
					if(txtAmount!=null && txtAmount.Text!="")
					{
						am = UIHelpers.ParseDecimal(txtAmount.Text);
						//dEUR+=am;
						if(am>0)
							per=am*100/dEUR;
					}
					CheckBox ckPaid=(CheckBox) dlPayments.Items[i].FindControl("ckPaid");
					TextBox txt=(TextBox) dlPayments.Items[i].FindControl("txtDate");
					TextBox txtNotes=(TextBox) dlPayments.Items[i].FindControl("txtNotes");
					if(per>0)
					{
						PaymentData pd= new PaymentData(-1, projectID,-1,per,am,ckPaid.Checked,TimeHelper.GetDate(txt.Text),am,false);
						pd.IsSubproject=false;
						pd.Notes=txtNotes.Text;
						pv.Add(pd);
					}
				}
			}			
		{
			if(!bPrevPhases && ckPhases.Checked)
			{
				SubprojectData sd = new SubprojectData(-1,this.ProjectID,int.Parse(System.Configuration.ConfigurationSettings.AppSettings["IdeaPhase"]),
					Constants.DateMax,Constants.DateMax,Resource.ResourceManager["NoteTransfer"],1,(int)area,0,0);
				SubprojectDAL.Save(sd);
				SubprojectsUDL.ExecutePaymentsSwitchProc(this.ProjectID,sd.SubprojectID);
			}
			else
				SubprojectsUDL.ExecutePaymentsDelByProjectProc(projectID);
			foreach(PaymentData pd in pv)
				PaymentDAL.Save(pd);
		}

			string[] arr = hdnlbSelectedUsersMails.Value.Split(';');
			ProjectUserUDL.ExecuteProjectUsersDelByProjectProc(projectID);
			foreach(string s in arr)
			{

				int nID=UIHelpers.ToInt(s);
				if(nID>0)
				{
					ProjectUserData pud = new ProjectUserData(-1,projectID,nID);
					ProjectUserDAL.Save(pud);
				}
			}
			//new
			string[] arr1 = hdnlbSelectedClients.Value.Split(';');
			ProjectClientUDL.ExecuteProjectClientsDelByProjectProc(projectID);
			foreach(string s in arr1)
			{

				int nID=UIHelpers.ToInt(s);
				if(nID>0)
				{
					ProjectClientData pcd = new ProjectClientData(-1,projectID,nID);
					ProjectClientDAL.Save(pcd);
				}
			}
			
			//			ContentsVector cv = new ContentsVector();
			//			cv.AddRange(GetDistr(projectID));
			//			cv.AddRange(GetContents(projectID));
			//			
			//			SubprojectsUDL.ExecuteContentsDelByProjectProc(projectID);
			log.Info("Updated Project " +projectCode+" "+ projectName +" by " + LoggedUser.FullName);
			//			foreach(ContentData cd in cv) ContentDAL.Save(cd);

			if(projectID>0)
			{
				ProjectSubcontractersVector psv= ProjectSubcontracterDAL.LoadCollection("ProjectSubcontractersListProc",SQLParms.CreateProjectSubcontractersListProc (UIHelpers.GetPIDParam()));
				foreach(ProjectSubcontracterData d in psv)
				{
					d.Area=area;
					ProjectSubcontracterDAL.Save(d);
				}

			}

			
      

			if (this.ProjectID == -1)
			{
				int buildingTypeID = int.Parse(ddlBuildingTypes.SelectedValue);
        
				if (buildingTypeID!=0 )
				{
					this.CreateProjectFolder(projectID);
				}

        
			}
      
			Response.Redirect("EditProject.aspx?PID="+projectID.ToString());

		
		}
		
		private void btnCancel_Click(object sender, System.EventArgs e)
		{
			Response.Redirect("Projects.aspx");
		}

		private void btnDelete_Click(object sender, System.EventArgs e)
		{
			try
			{
				ProjectsData.InactiveProject(ProjectID);
			}
			catch (Exception ex)
			{
				log.Info(ex);
				lblError.Text = Resource.ResourceManager["editProject_ErrorDelete"];
				return;
			}
			Response.Redirect("Projects.aspx");
		}
		
		private void btnDelete1_Click(object sender, System.EventArgs e)
		{
			try
			{
				ProjectsData.DeleteProject(ProjectID);
			}
			catch (Exception ex)
			{
				log.Info(ex);
				lblError.Text = Resource.ResourceManager["editProject_ErrorDeleteSub"];
				return;
			}
			Response.Redirect("Projects.aspx");
		
		}

		
		#endregion
		
		#region Buttons in center

		private void ddlClients_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			SelectedValueDiferent(false);
		}
		private void SelectedValueDiferent(bool fromPageLoad)
		{
			if(ddlClients.SelectedValue == "0")
			{
				lblMoreClients.Visible = false;
				TableMoreClients.Visible = false;
				LoadClients();
			}
			else
			{
				lblMoreClients.Visible = true;
				TableMoreClients.Visible = true;
				if(!fromPageLoad)
				{
					LoadClients();
					lbSelectedClients.Items.Clear();
				}
				ListItem li = lbClients.Items.FindByValue(ddlClients.SelectedValue);
				if(li != null)
				{
					lbClients.Items.Remove(li);
				}
				
			}
		}
		private void btnCreateFolder_Click(object sender, System.EventArgs e)
		{
			int buildingTypeID = 0;
			string buildingTypeCode;
			int number;
			string projectCode;
			string projectName;
			string folderPath;

			try
			{
				ProjectsData.SelectProjectCode(this.ProjectID, out buildingTypeID, out buildingTypeCode, out number,
					out projectName, out projectCode, out folderPath);
			}
			catch (Exception ex)
			{
				log.Error(ex.Message);
				lblError.Text = Resource.ResourceManager["errorCreateProjectFolder"];

				return;
			}

			if ( (buildingTypeID != int.Parse(ddlBuildingTypes.SelectedValue)) ||
				projectName != txtProjectName.Text.Trim()  )
			{
				lblError.Text = Resource.ResourceManager["errorCreateProjectFolder1"];
				return;
			}

			if (buildingTypeID == 7  )
			{
				lblBTCode.Text = "XX";
				lblProjectOrder.Text = "";

				
			}
			if(buildingTypeID == 0 )
			{
				lblBTCode.Text = "XX";
				lblProjectOrder.Text = "";

				return;
			}
			lblBTCode.Text = buildingTypeCode;
			lblProjectOrder.Text = number < 10 ? "0"+number.ToString() : number.ToString();

			string rootFolderPath = SettingsManager.ProjectRootFolderPath;
      
			string folderName = BuildProjectFolderName( buildingTypeCode, number, txtProjectCode.Text.Trim(), txtProjectName.Text, ddlBuildingTypes.SelectedItem.Text);

			string src = rootFolderPath+"Structure";
			string dest = rootFolderPath+folderName;
			if (Directory.Exists(dest))
			{
				lblError.Text = Resource.ResourceManager["errorFolderExists"];
				return;
			}
			try
			{
				CopyDirectory(src, dest);
			}
			catch (Exception ex)
			{
				log.Error(ex.Message);
				lblError.Text = Resource.ResourceManager["errorCreateProjectFolder"];
				return;
			}

			btnCreateFolder.Visible = false;
			hlProjectFolder.Visible = true;
			hlProjectFolder.Text=dest;
			hlProjectFolder.NavigateUrl = dest;

		}

		private void btnAdd_Click(object sender, System.EventArgs e)
		{
			int ID = int.Parse(ddlContents.SelectedValue);
			SqlDataReader reader=null;
			DistrTable=GetDistr(ProjectID);
			ContentsTable=GetContents(ProjectID);
			try
			{
				reader = SubprojectsUDL.SelectOneContentType(ID);
				reader.Read();
				int filter = reader.GetInt32(2);
				ContentData ct = new ContentData(-1,-1,ID,0,0,"","",1,"","","",0,0,0,0,0);
				ct.ContentType=reader.GetString(1);
				if(filter==0)
					DistrTable.Add(ct);
				else
					ContentsTable.Add(ct);
				BindGrid();
			}
			catch (Exception ex)
			{
				log.Info(ex);
				
			}

			finally 
			{ 
				if (reader!=null) reader.Close(); 
			}
			Reload();
		}

		private void btnCalculate_Click(object sender, System.EventArgs e)
		{
			Reload();
		}

		private void btn_Click(object sender, System.EventArgs e)
		{
			SetRec();
			
		}

		private void btnNewPayment_Click(object sender, System.EventArgs e)
		{
			PaymentData pd = new PaymentData(-1,-1,-1,0,0,false,new DateTime(),0,false);
			SessionTable.Add(pd);
			dlPayments.DataSource=SessionTable;
			dlPayments.DataBind();
			Reload();
		}
		private void btnExportPay_Click(object sender, System.EventArgs e)
		{
			//			Response.AppendHeader("content-disposition", "attachment; filename=Balances"+".xls");
			//			Response.ContentType = "application/vnd.ms-excel"; 
			//			//Remove the charset from the Content-Type header. 
			//			Response.Charset = ""; 
			//			this.EnableViewState = false;
			//			
			////			dlPayments.Columns[(int)BalanceColumns.Edit].Visible=false;
			////			
			////			grdBalance.DataBind();
			//			System.IO.StringWriter tw = new System.IO.StringWriter(); 
			//			System.Web.UI.HtmlTextWriter hw = new System.Web.UI.HtmlTextWriter(tw); 
			//
			//
			//			//get the HTML for the control 
			//			dlPayments.RenderControl(hw); 
			//			Response.Write(tw.ToString()); 
			//			Response.End(); 
			this.EnableViewState = true;

			System.IO.StringWriter tw = new System.IO.StringWriter();
			System.Web.UI.HtmlTextWriter hw = new System.Web.UI.HtmlTextWriter(tw);
			

			//try
		{
				
			///if (reader!=null)
		{
			gridCalls.DataSource = SessionTable;
					
					
			gridCalls.DataBind();
					
			gridCalls.AllowSorting=false;
					
			gridCalls.DataBind();
			for(int i=0;i<gridCalls.Columns.Count;i++)
			{
						
				gridCalls.Columns[i].SortExpression=null;
			}
		}
		}
			//			
			//			finally
			//			{
			//				if (reader!=null) reader.Close();
			//			}

			gridCalls.MasterTableView.ExportToExcel("Payments");
		}


		#endregion

		#region Menu - Right

		private void lkProfile_Click(object sender, System.EventArgs e)
		{
			Response.Redirect("Profile.aspx"+"?pid="+UIHelpers.GetPIDParam());
		}

		private void lkSubpr_Click(object sender, System.EventArgs e)
		{
			Response.Redirect("Subprojects.aspx"+"?pid="+UIHelpers.GetPIDParam());
		}

		private void lkSubcontracters_Click(object sender, System.EventArgs e)
		{
			Response.Redirect("ProjectSubcontracters.aspx"+"?pid="+UIHelpers.GetPIDParam());
					
		}
		
		private void Linkbutton1_Click(object sender, System.EventArgs e)
		{
			Response.Redirect("Authors.aspx"+"?pid="+UIHelpers.GetPIDParam());
		}

		private void lbProjectContract_Click(object sender, System.EventArgs e)
		{
			Response.Redirect("Contracts.aspx"+"?pid="+UIHelpers.GetPIDParam());
		}

		private void lbProjectBuilding_Click(object sender, System.EventArgs e)
		{
			Response.Redirect("Buildings.aspx"+"?pid="+UIHelpers.GetPIDParam());
		}

		private void lkFull_Click(object sender, System.EventArgs e)
		{
			PdfExport pdf = new PdfExport();
			pdf.NeverEmbedFonts = "";


			DataDynamics.ActiveReports.ActiveReport report = ProjectReport.CreateProjectReport(ProjectID, true,false);
			System.IO.MemoryStream memoryFile = new System.IO.MemoryStream();

			pdf.Export(report.Document, memoryFile);
			Response.Clear();

			Response.AppendHeader( "content-disposition","attachment; filename="+"project.pdf");
			Response.ContentType = "application/pdf";


			memoryFile.WriteTo(Response.OutputStream); 
			Response.End(); 
		}

		private void lbShortDocumentation_Click(object sender, System.EventArgs e)
		{
			PdfExport pdf = new PdfExport();
			pdf.NeverEmbedFonts = "";

			DataDynamics.ActiveReports.ActiveReport report = ProjectReport.CreateProjectReport(ProjectID, false,false);
			System.IO.MemoryStream memoryFile = new System.IO.MemoryStream();

			pdf.Export(report.Document, memoryFile);
			Response.Clear();

			Response.AppendHeader( "content-disposition","attachment; filename="+"project.pdf");
			Response.ContentType = "application/pdf";

			memoryFile.WriteTo(Response.OutputStream); 
			Response.End(); 
		}

		private void Linkbutton2_Click(object sender, System.EventArgs e)
		{
			PdfExport pdf = new PdfExport();
			pdf.NeverEmbedFonts = "";


			DataDynamics.ActiveReports.ActiveReport report = ProjectReport.CreateProjectReport(ProjectID, true,true);
			System.IO.MemoryStream memoryFile = new System.IO.MemoryStream();

			pdf.Export(report.Document, memoryFile);
			Response.Clear();

			Response.AppendHeader( "content-disposition","attachment; filename="+"project.pdf");
			Response.ContentType = "application/pdf";


			memoryFile.WriteTo(Response.OutputStream); 
			Response.End(); 
		}

		private void lkFullProjectInfo_Click(object sender, System.EventArgs e)
		{
			PdfExport pdf = new PdfExport();
			pdf.NeverEmbedFonts = "";

			DataDynamics.ActiveReports.ActiveReport report = ProjectReport.CreateProjectInfoReport(ProjectID,  LoggedUser.IsAdmin);
			System.IO.MemoryStream memoryFile = new System.IO.MemoryStream();

			pdf.Export(report.Document, memoryFile);
			Response.Clear();

			Response.AppendHeader( "content-disposition","attachment; filename="+"ProjectInfo.pdf");
			Response.ContentType = "application/pdf";

			memoryFile.WriteTo(Response.OutputStream); 
			Response.End(); 
		
		}

		private void btnContents_Click(object sender, System.EventArgs e)
		{
			ChecksVector checks = GetChecksTable(ProjectID);
			PdfExport rtf = new PdfExport();
			rtf.NeverEmbedFonts = "";
			DataDynamics.ActiveReports.ActiveReport report = new Contents(ProjectID,LoggedUser.FullName,false,-1,checks,txtProjectPhase.Text);
			report.Run();
			System.IO.MemoryStream memoryFile = new System.IO.MemoryStream();

			rtf.Export(report.Document, memoryFile);
			Response.Clear();

			Response.AppendHeader( "content-disposition","attachment; filename="+"Contents.pdf");
			Response.ContentType = "application/pdf";


			memoryFile.WriteTo(Response.OutputStream); 
			Response.End(); 
		}

		private void btnTech_Click(object sender, System.EventArgs e)
		{
			ChecksVector checks = GetChecksTable(ProjectID);
			PdfExport pdf = new PdfExport();
			XlsExport xls = new XlsExport();
		
			pdf.NeverEmbedFonts = "";

			DataDynamics.ActiveReports.ActiveReport report = new Pokazateli(ProjectID,LoggedUser.FullName,-1,false,checks,txtProjectPhase.Text);
			report.Run();
			System.IO.MemoryStream memoryFile = new System.IO.MemoryStream();

			pdf.Export(report.Document, memoryFile);
			Response.Clear();

			Response.AppendHeader( "content-disposition","attachment; filename="+"Pokazateli.pdf");
			Response.ContentType = "application/pdf";


			memoryFile.WriteTo(Response.OutputStream); 
			Response.End(); 
		}

		private void lkMissingData_Click(object sender, System.EventArgs e)
		{
			PdfExport pdf = new PdfExport();
			pdf.NeverEmbedFonts = "";

			DataDynamics.ActiveReports.ActiveReport report = new MissingDataReport(this.ProjectID);
			report.Run();

			System.IO.MemoryStream memoryFile = new System.IO.MemoryStream();

			pdf.Export(report.Document, memoryFile);
			Response.Clear();

			Response.AppendHeader( "content-disposition","attachment; filename="+"MissingData.pdf");
			Response.ContentType = "application/pdf";

			memoryFile.WriteTo(Response.OutputStream); 
			Response.End(); 
		}

		private void lkProtPlus_Click(object sender, System.EventArgs e)
		{
			string sManager="";
			if(ddlClients.SelectedIndex>=0)
			{
				int nClientID =int.Parse(ddlClients.SelectedValue);
				SqlDataReader reader = null;
				try
				{
					reader = ClientsData.SelectClient(nClientID);
					if (reader.Read())
					{
						
						sManager = reader["Manager"]==DBNull.Value ? String.Empty :(string) reader["Manager"];
					
					}
					
				}
				catch (Exception ex)
				{
					log.Error(ex);
					
				}
				finally
				{
					if (reader!=null) reader.Close();
				}
			}
					
			ChecksVector checks = GetChecksTable(ProjectID);	
			string s1=string.Format(Resource.ResourceManager["Report_Contents1"],txtInvestor.Text,sManager);
			string s2=string.Format(Resource.ResourceManager["Report_Contents2"],txtAdministrativeName.Text+", "+txtAddInfo.Text,txtArea.Text);
			string s3=string.Format(Resource.ResourceManager["Report_Contents3"],txtFoldersGiven.Text);
			RtfExport rtf = new RtfExport();
			
			DataDynamics.ActiveReports.ActiveReport report = new Reports.Protokol(ProjectID,LoggedUser.FullName,true,s1,s2,s3,checks,txtProjectPhase.Text);
			report.Run();
			System.IO.MemoryStream memoryFile = new System.IO.MemoryStream();

			rtf.Export(report.Document, memoryFile);
			Response.Clear();

			Response.AppendHeader( "content-disposition","attachment; filename="+"Protokol.rtf");
			Response.ContentType = "application/pdf";


			memoryFile.WriteTo(Response.OutputStream); 
			Response.End(); 
		}

		private void lkFolder1_Click(object sender, System.EventArgs e)
		{
			PdfExport pdf = new PdfExport();
			pdf.NeverEmbedFonts = "";


			string sColor="";
			string sName="";
			string sCode="";
			string sAdmin="";
			string sAddress="";
			//			if(txtColor.Enabled)
			//				sColor=txtColor.Text;
			//			else
		{
			SqlDataReader reader = null;

			try
			{
				reader = ProjectsData.SelectProject(ProjectID);
				if(reader.Read())
				{

					sName = reader.GetString(1);
					sCode = reader.IsDBNull(2) ? String.Empty : reader.GetString(2);
					sAdmin = reader.IsDBNull(12) ? String.Empty : reader.GetString(12);
					sAddress=reader.IsDBNull(14) ? String.Empty : reader.GetString(14);
					if(!reader.IsDBNull(30))
						sColor=reader.GetString(30);
						
				}
			}
			catch (Exception ex)
			{
				log.Info(ex);
					
			}

			finally 
			{ 
				if (reader!=null) reader.Close(); 
			}
		}
			DataDynamics.ActiveReports.ActiveReport report =new rptFolder(sColor, sAdmin,
				sAddress,sCode,sName);
			report.PageSettings.Orientation=DataDynamics.ActiveReports.Document.PageOrientation.Landscape;

			System.IO.MemoryStream memoryFile = new System.IO.MemoryStream();
			report.Run();
			pdf.Export(report.Document, memoryFile);
			Response.Clear();

			Response.AppendHeader( "content-disposition","attachment; filename="+"folder1.pdf");
			Response.ContentType = "application/pdf";


			memoryFile.WriteTo(Response.OutputStream); 
			Response.End(); 
		}

		private void lkFolder2_Click(object sender, System.EventArgs e)
		{
			PdfExport pdf = new PdfExport();
			pdf.NeverEmbedFonts = "";

			SqlDataReader reader = null;
			
		
			string sAdmin="";
			string sAddress="";
			try
			{
				reader = ProjectsData.SelectProject(ProjectID);
				if(reader.Read())
				{

					
					sAdmin = reader.IsDBNull(12) ? String.Empty : reader.GetString(12);
					sAddress=reader.IsDBNull(14) ? String.Empty : reader.GetString(14);
					
				}
			}
			catch (Exception ex)
			{
				log.Info(ex);
					
			}

			finally 
			{ 
				if (reader!=null) reader.Close(); 
			}
			
			DataDynamics.ActiveReports.ActiveReport report =new rptFolder2( sAdmin,
				sAddress);
			report.PageSettings.Orientation=DataDynamics.ActiveReports.Document.PageOrientation.Landscape;

			System.IO.MemoryStream memoryFile = new System.IO.MemoryStream();
			report.Run();
			pdf.Export(report.Document, memoryFile);
			Response.Clear();

			Response.AppendHeader( "content-disposition","attachment; filename="+"folder2.pdf");
			Response.ContentType = "application/pdf";


			memoryFile.WriteTo(Response.OutputStream); 
			Response.End(); 
		}
							
		private void img_Click(object sender, System.Web.UI.ImageClickEventArgs e)
		{
			ChecksVector checks = GetChecksTable(ProjectID);
			RtfExport rtf = new RtfExport();
			
			DataDynamics.ActiveReports.ActiveReport report = new Contents(ProjectID,LoggedUser.FullName,true,-1,checks,txtProjectPhase.Text);
			report.Run();
			System.IO.MemoryStream memoryFile = new System.IO.MemoryStream();

			rtf.Export(report.Document, memoryFile);
			Response.Clear();

			Response.AppendHeader( "content-disposition","attachment; filename="+"Contents.rtf");
			Response.ContentType = "application/pdf";


			memoryFile.WriteTo(Response.OutputStream); 
			Response.End(); 
		}
	
		private void imgTechWord_Click(object sender, System.Web.UI.ImageClickEventArgs e)
		{
			RtfExport pdf = new RtfExport();
			ChecksVector checks = GetChecksTable(ProjectID);
		
		

			DataDynamics.ActiveReports.ActiveReport report = new Pokazateli(ProjectID,LoggedUser.FullName,-1,true,checks,txtProjectPhase.Text);
			report.Run();
			System.IO.MemoryStream memoryFile = new System.IO.MemoryStream();

			pdf.Export(report.Document, memoryFile);
			Response.Clear();

			Response.AppendHeader( "content-disposition","attachment; filename="+"Pokazateli.rtf");
			Response.ContentType = "application/pdf";


			memoryFile.WriteTo(Response.OutputStream); 
			Response.End(); 
		}

		private void imgTechExcel_Click(object sender, System.Web.UI.ImageClickEventArgs e)
		{
			
			XlsExport xls = new XlsExport();
		
			ChecksVector checks = GetChecksTable(ProjectID);
			DataDynamics.ActiveReports.ActiveReport report = new Pokazateli(ProjectID,LoggedUser.FullName,-1,true,checks,txtProjectPhase.Text);
			report.Run();
			System.IO.MemoryStream memoryFile = new System.IO.MemoryStream();

			xls.Export(report.Document, memoryFile);
			Response.Clear();

			Response.AppendHeader( "content-disposition","attachment; filename="+"Pokazateli.xls");
			Response.ContentType = "application/pdf";


			memoryFile.WriteTo(Response.OutputStream); 
			Response.End(); 
		}

		private void btnContentExcel_Click(object sender, System.Web.UI.ImageClickEventArgs e)
		{
			ChecksVector checks = GetChecksTable(ProjectID);
			XlsExport rtf = new XlsExport();
			
			DataDynamics.ActiveReports.ActiveReport report = new Contents(ProjectID,LoggedUser.FullName,true,-1,checks,txtProjectPhase.Text);
			report.Run();
			System.IO.MemoryStream memoryFile = new System.IO.MemoryStream();

			rtf.Export(report.Document, memoryFile);
			Response.Clear();

			Response.AppendHeader( "content-disposition","attachment; filename="+"Contents.xls");
			Response.ContentType = "application/pdf";


			memoryFile.WriteTo(Response.OutputStream); 
			Response.End();
		}
	
		
		#endregion
		
		private void lkList_Click(object sender, System.EventArgs e)
		{
			UIHelpers.FrontPage(this.Page,txtAdministrativeName.Text,txtInvestor.Text,txtAddInfo.Text,txtProjectCode.Text);
		}

		private void lkFax_Click(object sender, System.EventArgs e)
		{
			string sFax="";
			if(ddlClients.SelectedIndex>=0)
			{
				int nClientID =int.Parse(ddlClients.SelectedValue);
				SqlDataReader reader = null;
				try
				{
					reader = ClientsData.SelectClient(nClientID);
					if (reader.Read())
					{
						
						sFax = reader.IsDBNull(6) ? String.Empty : reader.GetString(6);
					
					}
					
				}
				catch (Exception ex)
				{
					log.Error(ex);
					
				}
				finally
				{
					if (reader!=null) reader.Close();
				}
			}
			UIHelpers.Fax(this.Page,txtAdministrativeName.Text,txtAddInfo.Text,ddlClients.SelectedItem.Text,sFax,LoggedUser.FullName);
		}
			
		private void btnDocProtokol_Click(object sender, System.Web.UI.ImageClickEventArgs e)
		{
		
		}
	
		private void lkTechInfo_Click(object sender, System.EventArgs e)
		{
			PdfExport pdf = new PdfExport();
			pdf.NeverEmbedFonts = "";

			DataDynamics.ActiveReports.ActiveReport report = ProjectReport.CreateTechReport(ProjectID, LoggedUser.IsAdmin);
			System.IO.MemoryStream memoryFile = new System.IO.MemoryStream();

			pdf.Export(report.Document, memoryFile);
			Response.Clear();

			Response.AppendHeader( "content-disposition","attachment; filename="+"TechInfo.pdf");
			Response.ContentType = "application/pdf";

			memoryFile.WriteTo(Response.OutputStream); 
			Response.End(); 
		}
	
		
		#endregion

		

		
	
		
	}
}
