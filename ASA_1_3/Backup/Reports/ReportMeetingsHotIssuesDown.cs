using System;
using GrapeCity.ActiveReports;
using GrapeCity.ActiveReports.Controls;
using GrapeCity.ActiveReports.SectionReportModel;
using GrapeCity.ActiveReports.Document.Section;
using GrapeCity.ActiveReports.Document;

namespace Asa.Timesheet.WebPages.Reports
{
    public class ReportMeetingsHotIssuesDown : GrapeCity.ActiveReports.SectionReport
	{
        public ReportMeetingsHotIssuesDown(string user)
        {
            this.PageSettings.Margins.Top = 0.4f;
            this.PageSettings.Margins.Bottom = 0.2f;
            this.PageSettings.Gutter = 0F;
            this.PageSettings.Margins.Left = 0f;
            this.PageSettings.Margins.Right = 0f;
            InitializeComponent();
            txtDateNow.Text = DateTime.Now.Date.ToShortDateString();
            txtCreatedBy.Text = user + txtCreatedBy.Text;
        }

		#region ActiveReports Designer generated code







        public void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ReportMeetingsHotIssuesDown));
            this.Detail = new GrapeCity.ActiveReports.SectionReportModel.Detail();
            this.PageHeader = new GrapeCity.ActiveReports.SectionReportModel.PageHeader();
            this.PageFooter = new GrapeCity.ActiveReports.SectionReportModel.PageFooter();
            this.lbDate = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.txtDateNow = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.lblCreated = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.txtCreatedBy = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            ((System.ComponentModel.ISupportInitialize)(this.lbDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDateNow)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblCreated)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCreatedBy)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            // 
            // Detail
            // 
            this.Detail.ColumnSpacing = 0F;
            this.Detail.Height = 0.4159722F;
            this.Detail.Name = "Detail";
            // 
            // PageHeader
            // 
            this.PageHeader.Height = 0F;
            this.PageHeader.Name = "PageHeader";
            // 
            // PageFooter
            // 
            this.PageFooter.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
						this.lbDate,
						this.txtDateNow,
						this.lblCreated,
						this.txtCreatedBy});
            this.PageFooter.Height = 0.4493056F;
            this.PageFooter.Name = "PageFooter";
            // 
            // lbDate
            // 
            this.lbDate.Height = 0.2F;
            this.lbDate.HyperLink = null;
            this.lbDate.Left = 0.0625F;
            this.lbDate.Name = "lbDate";
            this.lbDate.Style = "font-family: Verdana; font-size: 9pt; ddo-char-set: 1";
            this.lbDate.Text = "????:";
            this.lbDate.Top = 0F;
            this.lbDate.Width = 0.5625F;
            // 
            // txtDateNow
            // 
            this.txtDateNow.Height = 0.2F;
            this.txtDateNow.Left = 0.625F;
            this.txtDateNow.Name = "txtDateNow";
            this.txtDateNow.Style = "font-family: Verdana; font-size: 9pt; font-weight: normal; text-align: center; dd" +
                "o-char-set: 1";
            this.txtDateNow.Text = "?.";
            this.txtDateNow.Top = 0F;
            this.txtDateNow.Width = 0.9375F;
            // 
            // lblCreated
            // 
            this.lblCreated.Height = 0.2F;
            this.lblCreated.HyperLink = null;
            this.lblCreated.Left = 5.375F;
            this.lblCreated.Name = "lblCreated";
            this.lblCreated.Style = "font-family: Verdana; font-size: 9pt; ddo-char-set: 1";
            this.lblCreated.Text = "????????: ......................................";
            this.lblCreated.Top = 0F;
            this.lblCreated.Width = 2.5625F;
            // 
            // txtCreatedBy
            // 
            this.txtCreatedBy.Height = 0.2F;
            this.txtCreatedBy.Left = 6F;
            this.txtCreatedBy.Name = "txtCreatedBy";
            this.txtCreatedBy.Style = "font-family: Verdana; font-size: 9pt; font-weight: normal; text-align: center; dd" +
                "o-char-set: 1";
            this.txtCreatedBy.Text = " - ???";
            this.txtCreatedBy.Top = 0.1875F;
            this.txtCreatedBy.Width = 1.9375F;
            // 
            // ActiveReport1
            // 
            this.MasterReport = false;
            this.PageSettings.PaperHeight = 11F;
            this.PageSettings.PaperWidth = 8.5F;
            this.PrintWidth = 8.0625F;
            this.Sections.Add(this.PageHeader);
            this.Sections.Add(this.Detail);
            this.Sections.Add(this.PageFooter);
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule(resources.GetString("$this.StyleSheet"), "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: inherit; font-style: inherit; font-variant: inherit; font-weight: bo" +
                        "ld; font-size: 16pt; font-size-adjust: inherit; font-stretch: inherit", "Heading1", "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: Times New Roman; font-style: italic; font-variant: inherit; font-wei" +
                        "ght: bold; font-size: 14pt; font-size-adjust: inherit; font-stretch: inherit", "Heading2", "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: inherit; font-style: inherit; font-variant: inherit; font-weight: bo" +
                        "ld; font-size: 13pt; font-size-adjust: inherit; font-stretch: inherit", "Heading3", "Normal"));
            ((System.ComponentModel.ISupportInitialize)(this.lbDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDateNow)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblCreated)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCreatedBy)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();
        }

		#endregion

        private PageHeader PageHeader;
        private Detail Detail;
        private PageFooter PageFooter;
        private Label lbDate;
        private TextBox txtDateNow;
        private Label lblCreated;
        private TextBox txtCreatedBy;
	}
}
