using System;
using GrapeCity.ActiveReports;
using GrapeCity.ActiveReports.Controls;
using GrapeCity.ActiveReports.SectionReportModel;
using GrapeCity.ActiveReports.Document.Section;
using GrapeCity.ActiveReports.Document;

namespace Asa.Timesheet.WebPages.Reports
{
    public class EmptyHoursRpt : GrapeCity.ActiveReports.SectionReport
	{
        public EmptyHoursRpt(bool IsPdf, string title)
        {
            _forPDF = IsPdf;
            InitializeComponent();
            lbTitle.Text = title;
            this.ReportEnd += new EventHandler(EmptyHoursRpt_ReportEnd);

        }
		private bool _forPDF=true;
		#region ActiveReports Designer generated code











        public void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(EmptyHoursRpt));
            this.Detail = new GrapeCity.ActiveReports.SectionReportModel.Detail();
            this.PageHeader = new GrapeCity.ActiveReports.SectionReportModel.PageHeader();
            this.PageFooter = new GrapeCity.ActiveReports.SectionReportModel.PageFooter();
            this.Label1 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label2 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label3 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lbTitle = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Line1 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.txtFoldersGiven = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtSubcontracter = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtFoldersArchive = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            ((System.ComponentModel.ISupportInitialize)(this.Label1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbTitle)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtFoldersGiven)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSubcontracter)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtFoldersArchive)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            // 
            // Detail
            // 
            this.Detail.ColumnSpacing = 0F;
            this.Detail.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
						this.txtFoldersGiven,
						this.txtSubcontracter,
						this.txtFoldersArchive});
            this.Detail.Height = 0.2076389F;
            this.Detail.Name = "Detail";
            // 
            // PageHeader
            // 
            this.PageHeader.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
						this.Label1,
						this.Label2,
						this.Label3,
						this.lbTitle,
						this.Line1});
            this.PageHeader.Height = 1.364583F;
            this.PageHeader.Name = "PageHeader";
            // 
            // PageFooter
            // 
            this.PageFooter.Height = 0.25F;
            this.PageFooter.Name = "PageFooter";
            // 
            // Label1
            // 
            this.Label1.Height = 0.2F;
            this.Label1.HyperLink = null;
            this.Label1.Left = 0.875F;
            this.Label1.Name = "Label1";
            this.Label1.Style = "";
            this.Label1.Text = "????????";
            this.Label1.Top = 1.0625F;
            this.Label1.Width = 2F;
            // 
            // Label2
            // 
            this.Label2.Height = 0.2F;
            this.Label2.HyperLink = null;
            this.Label2.Left = 2.875F;
            this.Label2.Name = "Label2";
            this.Label2.Style = "";
            this.Label2.Text = "??? ? ??????????? ??????? ?????";
            this.Label2.Top = 1.0625F;
            this.Label2.Width = 3.25F;
            // 
            // Label3
            // 
            this.Label3.Height = 0.2F;
            this.Label3.HyperLink = null;
            this.Label3.Left = 6.125F;
            this.Label3.Name = "Label3";
            this.Label3.Style = "";
            this.Label3.Text = "???? ???";
            this.Label3.Top = 1.0625F;
            this.Label3.Width = 0.875F;
            // 
            // lbTitle
            // 
            this.lbTitle.Height = 0.2F;
            this.lbTitle.HyperLink = null;
            this.lbTitle.Left = 0.8125F;
            this.lbTitle.Name = "lbTitle";
            this.lbTitle.Style = "font-size: 12pt; font-weight: bold; text-align: center";
            this.lbTitle.Text = "Label4";
            this.lbTitle.Top = 0F;
            this.lbTitle.Width = 6.1875F;
            // 
            // Line1
            // 
            this.Line1.Height = 0.006944418F;
            this.Line1.Left = 0.8194444F;
            this.Line1.LineWeight = 1F;
            this.Line1.Name = "Line1";
            this.Line1.Top = 1.3125F;
            this.Line1.Width = 6.368055F;
            this.Line1.X1 = 0.8194444F;
            this.Line1.X2 = 7.1875F;
            this.Line1.Y1 = 1.319444F;
            this.Line1.Y2 = 1.3125F;
            // 
            // txtFoldersGiven
            // 
            this.txtFoldersGiven.DataField = "DaysString";
            this.txtFoldersGiven.Height = 0.2F;
            this.txtFoldersGiven.Left = 2.872622F;
            this.txtFoldersGiven.Name = "txtFoldersGiven";
            this.txtFoldersGiven.Style = "text-align: left";
            this.txtFoldersGiven.Top = 0F;
            this.txtFoldersGiven.Width = 3.252378F;
            // 
            // txtSubcontracter
            // 
            this.txtSubcontracter.DataField = "UserName";
            this.txtSubcontracter.Height = 0.2F;
            this.txtSubcontracter.Left = 0.875F;
            this.txtSubcontracter.Name = "txtSubcontracter";
            this.txtSubcontracter.OutputFormat = resources.GetString("txtSubcontracter.OutputFormat");
            this.txtSubcontracter.Style = "font-size: 11pt";
            this.txtSubcontracter.Top = 0F;
            this.txtSubcontracter.Width = 2F;
            // 
            // txtFoldersArchive
            // 
            this.txtFoldersArchive.DataField = "Days";
            this.txtFoldersArchive.Height = 0.2F;
            this.txtFoldersArchive.Left = 6.125F;
            this.txtFoldersArchive.Name = "txtFoldersArchive";
            this.txtFoldersArchive.OutputFormat = resources.GetString("txtFoldersArchive.OutputFormat");
            this.txtFoldersArchive.Style = "font-size: 11pt; text-align: left";
            this.txtFoldersArchive.Top = 0F;
            this.txtFoldersArchive.Width = 0.875F;
            // 
            // ActiveReport1
            // 
            this.MasterReport = false;
            this.PageSettings.PaperHeight = 11F;
            this.PageSettings.PaperWidth = 8.5F;
            this.PrintWidth = 7.010417F;
            this.Sections.Add(this.PageHeader);
            this.Sections.Add(this.Detail);
            this.Sections.Add(this.PageFooter);
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule(resources.GetString("$this.StyleSheet"), "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: inherit; font-style: inherit; font-variant: inherit; font-weight: bo" +
                        "ld; font-size: 16pt; font-size-adjust: inherit; font-stretch: inherit", "Heading1", "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: Times New Roman; font-style: italic; font-variant: inherit; font-wei" +
                        "ght: bold; font-size: 14pt; font-size-adjust: inherit; font-stretch: inherit", "Heading2", "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: inherit; font-style: inherit; font-variant: inherit; font-weight: bo" +
                        "ld; font-size: 13pt; font-size-adjust: inherit; font-stretch: inherit", "Heading3", "Normal"));
            ((System.ComponentModel.ISupportInitialize)(this.Label1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbTitle)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtFoldersGiven)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSubcontracter)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtFoldersArchive)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();
        }

		#endregion

        private void EmptyHoursRpt_ReportEnd(object sender, EventArgs e)
        {
            if (!this._forPDF) return;

            Logo logo = new Logo();
            logo.Run();

            for (int i = 0; i < this.Document.Pages.Count; i++)
                this.Document.Pages[i].Overlay(logo.Document.Pages[0]);
        }

        private PageHeader PageHeader;
        private Label Label1;
        private Label Label2;
        private Label Label3;
        private Label lbTitle;
        private Line Line1;
        private Detail Detail;
        private TextBox txtFoldersGiven;
        private TextBox txtSubcontracter;
        private TextBox txtFoldersArchive;
        private PageFooter PageFooter;
	}
}
