using System;
using System.Data;
using System.Data.SqlClient; 
using System.Collections;
using Microsoft.ApplicationBlocks.Data;

namespace Asa.Timesheet.Data
{
	/// <summary>
	/// Summary description for ExpencesMontlyDAL.
	/// </summary>
	public class HotIssueDAL
	{
		/// <summary>
		/// Loads a ExpencesMontlyData object from the database using the given ExpencesMontlyID
		/// </summary>
		/// <param name="ExpencesMontlyID">The primary key value</param>
		/// <returns>A ExpencesMontlyData object</returns>
		public static HotIssueData Load(int HotIssueID)
		{
			SqlParameter[] parameterValues = new SqlParameter[] { new SqlParameter("@HotIssueID", HotIssueID) };
			using (SqlDataReader reader = SqlHelper.ExecuteReader(CommonDAL.ConnectionString, CommandType.StoredProcedure, "HotIssueSelProc", parameterValues))
			{
				if (reader.Read())
				{
					HotIssueData result = new HotIssueData();
					LoadFromReader(reader,result);
					return result;
				}
				else
					return null;
			}
		}
		/// <summary>
		/// Loads a ExpencesMontlyData object from the database using the given where clause
		/// </summary>
		/// <param name="whereClause">The filter expression for the query</param>
		/// <returns>A ExpencesMontlyData object</returns>
		public static HotIssueData Load(string whereClause)
		{
			SqlParameter[] parameterValues = new SqlParameter[] { new SqlParameter("@WhereClause", whereClause) };
			using (SqlDataReader reader = SqlHelper.ExecuteReader(CommonDAL.ConnectionString, CommandType.StoredProcedure, "HotIssueListProc", parameterValues))
			{
				if (reader.Read())
				{
					HotIssueData result = new HotIssueData();
					LoadFromReader(reader,result);
					return result;
				}
				else
					return null;
			}
		}
		public static int Save(HotIssueData source)
		{
			if (source.HotIssueID == -1)
				return Insert(source);
			else
				return Update(source);	
		}

		public static void Delete(HotIssueData source)
		{
			Delete(source.HotIssueID);
		}
		/// <summary>
		/// Loads a collection of ExpencesMontlyData objects from the database.
		/// </summary>
		/// <returns>A collection containing all of the ExpencesMontlyData objects in the database.</returns>
		public static HotIssuesVector LoadCollection()
		{
			SqlParameter[] parms = new SqlParameter[] {};
			return LoadCollection("HotIssueListProc", parms);
		}
		/// <summary>
		/// Loads a collection of ExpencesMontlyData objects from the database.
		/// </summary>
		/// <returns>A collection containing all of the ExpencesMontlyData objects in the database.</returns>
		public static HotIssuesVector LoadCollection(int projectID,int hotIssueCategoryID,/* int hotIssuePriorityID,*/ int hotIssueStatusID,int hotIssueIDMain,
			bool hotIssueIDMainWithActive,int buildingTypeID,int ddlProjectStatusSelectedIndex,DateTime dtStart,DateTime dtEnd, bool ASA, bool bAll,string sort)
		{
			object oASA = ASA;
			if(bAll)
				oASA =ASA?(object)DBNull.Value:ASA;
			SqlParameter[] parms = new SqlParameter[] {
				new SqlParameter("@ProjectID", (projectID>0)?(object)projectID:DBNull.Value),
				new SqlParameter("@HotIssueCategoryID", (hotIssueCategoryID>0)?(object)hotIssueCategoryID:DBNull.Value),
				//new SqlParameter("@HotIssuePriorityID", (hotIssuePriorityID>0)?(object)hotIssuePriorityID:DBNull.Value),
				new SqlParameter("@HotIssueStatusID", (hotIssueStatusID>0)?(object)hotIssueStatusID:DBNull.Value),
				new SqlParameter("@HotIssueIDMain", (hotIssueIDMain>0)?(object)hotIssueIDMain:DBNull.Value),
				new SqlParameter("@HotIssueIDMainWithActive", hotIssueIDMainWithActive),
				new SqlParameter("@BuildingTypeID", (buildingTypeID>0)?(object)buildingTypeID:DBNull.Value),
				new SqlParameter("@IsActive", (ddlProjectStatusSelectedIndex==0||ddlProjectStatusSelectedIndex==2)?(object)(ddlProjectStatusSelectedIndex==0):DBNull.Value),
				new SqlParameter("@Concluded", (ddlProjectStatusSelectedIndex==3)?(object)true:DBNull.Value),
				new SqlParameter("@StartDateSearch", (dtStart!=Constants.DateMax)?(object)dtStart:DBNull.Value),
				new SqlParameter("@EndDateSearch", (dtEnd!=Constants.DateMax)?(object)dtEnd:DBNull.Value),
				new SqlParameter("@ASA", oASA),
				new SqlParameter("@SortOrder", sort),									  };
			return LoadCollection("HotIssueListProc", parms);
		}
		public static HotIssuesVector LoadCollectionForMeeting(int projectID,DateTime dtEnd, bool bASA, int MeetingID,int hotIssueCategoryID, int hotIssueStatusID, DateTime dtStart, bool bAll, bool bOnlyMeeting, bool bEnglish, bool bAllCategories, bool bMeetingAll)
		{
			SqlParameter[] parms = new SqlParameter[] {
														  new SqlParameter("@ProjectID", (projectID>0)?(object)projectID:DBNull.Value),
														 new SqlParameter("@HotIssueCategoryID", (hotIssueCategoryID>0)?(object)hotIssueCategoryID:DBNull.Value),
														 // new SqlParameter("@HotIssuePriorityID", (hotIssuePriorityID>0)?(object)hotIssuePriorityID:DBNull.Value),
														 new SqlParameter("@HotIssueStatusID", (hotIssueStatusID==1)?(object)hotIssueStatusID:DBNull.Value),
														//  new SqlParameter("@HotIssueIDMain", (hotIssueIDMain>0)?(object)hotIssueIDMain:DBNull.Value),
														 // new SqlParameter("@HotIssueIDMainWithActive", hotIssueIDMainWithActive),
														 // new SqlParameter("@BuildingTypeID", (buildingTypeID>0)?(object)buildingTypeID:DBNull.Value),
														//  new SqlParameter("@IsActive", (ddlProjectStatusSelectedIndex==0||ddlProjectStatusSelectedIndex==2)?(object)(ddlProjectStatusSelectedIndex==0):DBNull.Value),
														//  new SqlParameter("@Concluded", (ddlProjectStatusSelectedIndex==3)?(object)true:DBNull.Value),
														  new SqlParameter("@StartDateSearch", (dtStart!=Constants.DateMax)?(object)dtStart:DBNull.Value),
														  new SqlParameter("@EndDateSearch", (dtEnd!=Constants.DateMax)?(object)dtEnd:DBNull.Value),
				new SqlParameter("@ALL", bAll?(object)DBNull.Value:!bAll),
														   new SqlParameter("@MeetingID", MeetingID>0?(object)MeetingID:DBNull.Value),
			new SqlParameter("@OnlyMeeting", bOnlyMeeting?bOnlyMeeting:(object)DBNull.Value),
			new SqlParameter("@AllCategories", bAllCategories?bAllCategories:(object)DBNull.Value),
														  new SqlParameter("@MeetingAll", bMeetingAll?bMeetingAll:(object)DBNull.Value),
				
														  };
			if(bEnglish)
				return LoadCollection("HotIssueForMeetingListProcEN", parms);
			else
				return LoadCollection("HotIssueForMeetingListProc", parms);
		}
		/// <summary>
		/// Loads a collection of ExpencesMontlyData objects from the database.
		/// </summary>
		/// <returns>A collection containing all of the ExpencesMontlyData objects in the database.</returns>
		public static HotIssuesVector LoadCollectionOldIssues(int hotIssueIDMain, bool ASA)
		{
			return LoadCollection(-1,-1/*,-1*/,-1,hotIssueIDMain,true,-1,1,Constants.DateMax,Constants.DateMax,ASA,true,null);
		}
		/// <summary>
		/// Saves a collection of ExpencesMontlyData objects to the database.
		/// </summary>
		/// <returns>A collection containing all of the ExpencesMontlyData objects in the database.</returns>
		public static  void  SaveCollection(HotIssuesVector col,string spName, SqlParameter[] parms)
		{
			HotIssuesVector colOld = LoadCollection(spName,parms);
			UpdateCollection(colOld,col);
		}
		public static  void  SaveCollection(HotIssuesVector col)
		{
			HotIssuesVector colOld = LoadCollection();
			UpdateCollection(colOld,col);
		}
		public static  void  SaveCollection(HotIssuesVector col, string whereClause )
		{
			HotIssuesVector colOld = LoadCollection(whereClause);
			UpdateCollection(colOld,col);
		}
		/// <summary>
		/// Loads a collection of ExpencesMontlyData objects from the database.
		/// </summary>
		/// <returns>A collection containing all of the ExpencesMontlyData objects in the database.</returns>
		public static HotIssuesVector LoadCollection(string whereClause)
		{
			SqlParameter[] parms = new SqlParameter[] { new SqlParameter("@WhereClause", whereClause) };
			return LoadCollection("HotIssueListProc", parms);
		}

		/// <summary>
		/// Loads a collection of ExpencesMontlyData objects from the database.
		/// </summary>
		/// <returns>A collection containing all of the ExpencesMontlyData objects in the database.</returns>
		public static HotIssuesVector LoadCollection(string spName, SqlParameter[] parms)
		{
			HotIssuesVector result = new HotIssuesVector();
			using (SqlDataReader reader = SqlHelper.ExecuteReader(CommonDAL.ConnectionString, CommandType.StoredProcedure, spName, parms))
			{
				while (reader.Read())
				{
					HotIssueData tmp = new HotIssueData();
					LoadFromReader(reader, tmp);
					result.Add(tmp);
				}
			}
			return result;
		}
		/// <summary>
		/// Deletes a ExpencesMontlyData object from the database.
		/// </summary>
		/// <param name="ExpencesMontlyID">The primary key value</param>
		public static void Delete(int HotIssueID)
		{
			SqlParameter[] parameterValues = new SqlParameter[] { new SqlParameter("@HotIssueID", HotIssueID) };
			SqlHelper.ExecuteNonQuery(CommonDAL.ConnectionString, CommandType.StoredProcedure, "HotIssueDelProc", parameterValues);
		}

		public static void LoadFromReader(SqlDataReader reader, HotIssueData target)
		{
			if (reader != null && !reader.IsClosed)
			{
				//target.Key = reader.GetInt32(0);
				if (!reader.IsDBNull(0)) target.HotIssueID  = reader.GetInt32(0);
				if (!reader.IsDBNull(1)) target.ProjectID = reader.GetInt32(1);
				if (!reader.IsDBNull(2)) target.HotIssueCategoryID = reader.GetInt32(2);
				if (!reader.IsDBNull(3)) target.HotIssueName  = reader.GetString(3);
				if (!reader.IsDBNull(4)) target.HotIssueDiscription = reader.GetString(4);
				if (!reader.IsDBNull(5)) target.HotIssueComment = reader.GetString(5);
				if (!reader.IsDBNull(6)) target.AssignedTo  = reader.GetInt32(6);
				if (!reader.IsDBNull(7)) target.AssignedToType = reader.GetInt32(7);
				if (!reader.IsDBNull(8)) target.HotIssueDeadline  = reader.GetDateTime(8);
				//if (!reader.IsDBNull(9)) target.HotIssuePriorityID  = reader.GetInt32(9);
				if (!reader.IsDBNull(9)) target.HotIssueStatusID = reader.GetInt32(9);
				if (!reader.IsDBNull(10)) target.HotIssueIDMain = reader.GetInt32(10);
				if (!reader.IsDBNull(11)) target.IsActive  = reader.GetBoolean(11);
				if (!reader.IsDBNull(12)) target.CreatedBy = reader.GetString(12);
				if (!reader.IsDBNull(13)) target.CreatedDate = reader.GetDateTime(13);
				if (!reader.IsDBNull(14)) target.ToClient = reader.GetBoolean(14);
				if (!reader.IsDBNull(15)) target.HotIssueCategoryName = reader.GetString(15);
				//if (!reader.IsDBNull(16)) target.HotIssuePriorityName = reader.GetString(16);
				if (!reader.IsDBNull(16)) target.HotIssueStatusName = reader.GetString(16);
				if (!reader.IsDBNull(17)) target.ProjectName = reader.GetString(17);
				if (!reader.IsDBNull(18)) target.ExecutionStopped = reader.GetDateTime(18);
				if (!reader.IsDBNull(19)) target.ASA = reader.GetBoolean(19);
				if (!reader.IsDBNull(20)) target.AssignedBy = reader.GetInt32(20);
				if (!reader.IsDBNull(21)) target.AssignedToMore = reader.GetString(21);
				if (!reader.IsDBNull(22)) target.HotIssuePosition = reader.GetString(22);
				if(reader.FieldCount>23)
				{
					if (!reader.IsDBNull(23)) target.OriginalDate = reader.GetDateTime(23);
					
				}
			}
			
		}
		
	  
		public static int Insert( HotIssueData source)
		{
			SqlParameter[] parameterValues = GetInsertParameterValues(source);
			SqlHelper.ExecuteNonQuery(CommonDAL.ConnectionString, CommandType.StoredProcedure, "HotIssueInsProc", parameterValues);
			source.HotIssueID = (int) parameterValues[0].Value;
			return source.HotIssueID;
		}

		public static int Update( HotIssueData source)
		{
			SqlParameter[] parameterValues = GetUpdateParameterValues(source);
			SqlHelper.ExecuteNonQuery(CommonDAL.ConnectionString, CommandType.StoredProcedure,
				"HotIssueUpdProc", parameterValues);
			return source.HotIssueID;
		}
		private static void UpdateCollection(HotIssuesVector colOld,HotIssuesVector col)
		{
			// Delete all old items
			foreach( HotIssueData itemOld in colOld)
			{
				if(!ExistsKey(itemOld.HotIssueID, col))
					Delete(itemOld.HotIssueID);
			}
			foreach( HotIssueData item in col)
			{
				if(item.HotIssueID <= -1)
					Insert(item);
				else
					Update(item);
			}
		}
		private static bool ExistsKey(int Key, HotIssuesVector col)
		{
			foreach( HotIssueData item in col)
				if(item.HotIssueID==Key)
					return true;
			return false;
		}

		private static SqlParameter GetSqlParameter(string name, ParameterDirection direction, object value)
		{
			SqlParameter p = new SqlParameter(name, value);
			p.Direction = direction;
			return p;
		}

		private static SqlParameter[] GetInsertParameterValues( HotIssueData source)
		{
			SqlParameter[] prams = new SqlParameter[20];
			prams[0] = GetSqlParameter("@HotIssueID", ParameterDirection.Output, source.HotIssueID);
			prams[1] = GetSqlParameter("@ProjectID", ParameterDirection.Input, source.ProjectID);
			prams[2] = GetSqlParameter("@HotIssueCategoryID", ParameterDirection.Input, source.HotIssueCategoryID);
			prams[3] = GetSqlParameter("@HotIssueName", ParameterDirection.Input, source.HotIssueName);
			prams[4] = GetSqlParameter("@HotIssueDiscription", ParameterDirection.Input, source.HotIssueDiscription);
			prams[5] = GetSqlParameter("@HotIssueComment", ParameterDirection.Input, source.HotIssueComment);
			prams[6] = GetSqlParameter("@AssignedTo", ParameterDirection.Input, source.AssignedTo<0?(object)DBNull.Value:source.AssignedTo);
			prams[7] = GetSqlParameter("@AssignedToType", ParameterDirection.Input, source.AssignedToType<0?(object)DBNull.Value:source.AssignedToType);
			prams[8] = GetSqlParameter("@HotIssueDeadline", ParameterDirection.Input, source.HotIssueDeadline);
			//prams[9] = GetSqlParameter("@HotIssuePriorityID", ParameterDirection.Input, source.HotIssuePriorityID);
			prams[9] = GetSqlParameter("@HotIssueStatusID", ParameterDirection.Input, source.HotIssueStatusID);
			prams[10] = GetSqlParameter("@HotIssueIDMain", ParameterDirection.Input, source.HotIssueIDMain);
			prams[11] = GetSqlParameter("@IsActive", ParameterDirection.Input, source.IsActive);
			prams[12] = GetSqlParameter("@CreatedBy", ParameterDirection.Input, source.CreatedBy);
			prams[13] = GetSqlParameter("@CreatedDate", ParameterDirection.Input, source.CreatedDate);
			prams[14] = GetSqlParameter("@ToClient", ParameterDirection.Input, source.ToClient);
			prams[15] = GetSqlParameter("@ExecutionStopped", ParameterDirection.Input, source.ExecutionStopped==new DateTime()?(object)DBNull.Value:source.ExecutionStopped);
			prams[16] = GetSqlParameter("@ASA", ParameterDirection.Input, source.ASA);
			prams[17] = GetSqlParameter("@AssignedBy", ParameterDirection.Input, source.AssignedBy);
			prams[17] = GetSqlParameter("@AssignedToText", ParameterDirection.Input, source.AssignedToMore);
			prams[18] = GetSqlParameter("@HotIssuePosition", ParameterDirection.Input, source.HotIssuePosition);
			
			return prams;
		}

		private static SqlParameter[] GetUpdateParameterValues( HotIssueData source)
		{
			SqlParameter[] prm = GetInsertParameterValues(source);
			prm[0].Direction=  ParameterDirection.Input;
			return prm;
//			return new SqlParameter[] {
//										  new SqlParameter("@HotIssueID", source.HotIssueID),
//										  new SqlParameter("@ProjectID", source.ProjectID),
//										  new SqlParameter("@HotIssueCategoryID", source.HotIssueCategoryID),
//										  new SqlParameter("@HotIssueName", source.HotIssueName),
//										  new SqlParameter("@HotIssueDiscription", source.HotIssueDiscription),
//										  new SqlParameter("@HotIssueComment", source.HotIssueComment),
//										  new SqlParameter("@AssignedTo", source.AssignedTo),
//										  new SqlParameter("@AssignedToType", source.AssignedToType),
//										  new SqlParameter("@HotIssueDeadline", source.HotIssueDeadline),
//										  //new SqlParameter("@HotIssuePriorityID", source.HotIssuePriorityID),
//										  new SqlParameter("@HotIssueStatusID", source.HotIssueStatusID),
//										  new SqlParameter("@HotIssueIDMain", source.HotIssueIDMain),
//										  new SqlParameter("@IsActive", source.IsActive),
//										  new SqlParameter("@CreatedBy", source.CreatedBy),
//										  new SqlParameter("@CreatedDate", source.CreatedDate),
//				new SqlParameter("@ToClient", source.ToClient),
//									
	//		};
		}
	}


//	public class HotIssuePriorityDAL
//	{
//		/// <summary>
//		/// Loads a HotIssuePriorityData object from the database using the given HotIssuesPriorityID
//		/// </summary>
//		/// <param name="HotIssuesPriorityID">The primary key value</param>
//		/// <returns>A HotIssuePriorityData object</returns>
//		public static HotIssuePriorityData Load(int HotIssuesPriorityID)
//		{
//			SqlParameter[] parameterValues = new SqlParameter[] { new SqlParameter("@HotIssuesPriorityID", HotIssuesPriorityID) };
//			using (SqlDataReader reader = SqlHelper.ExecuteReader(CommonDAL.ConnectionString, CommandType.StoredProcedure, "HotIssuesPrioritySelProc", parameterValues))
//			{
//				if (reader.Read())
//				{
//					HotIssuePriorityData result = new HotIssuePriorityData();
//					LoadFromReader(reader,result);
//					return result;
//				}
//				else
//					return null;
//			}
//		}
//		/// <summary>
//		/// Loads a HotIssuePriorityData object from the database using the given where clause
//		/// </summary>
//		/// <param name="whereClause">The filter expression for the query</param>
//		/// <returns>A HotIssuePriorityData object</returns>
//		public static HotIssuePriorityData Load(string whereClause)
//		{
//			SqlParameter[] parameterValues = new SqlParameter[] { new SqlParameter("@WhereClause", whereClause) };
//			using (SqlDataReader reader = SqlHelper.ExecuteReader(CommonDAL.ConnectionString, CommandType.StoredProcedure, "HotIssuesPriorityListProc", parameterValues))
//			{
//				if (reader.Read())
//				{
//					HotIssuePriorityData result = new HotIssuePriorityData();
//					LoadFromReader(reader,result);
//					return result;
//				}
//				else
//					return null;
//			}
//		}
//
////		/// <summary>
////		/// Loads a collection of HotIssuePriorityData objects from the database.
////		/// </summary>
////		/// <returns>A collection containing all of the HotIssuePriorityData objects in the database.</returns>
//		public static HotIssuesPrioritiesVector LoadCollection()
//		{
//			SqlParameter[] parms = new SqlParameter[] {};
//			return LoadCollection("HotIssuesPriorityListProc", parms);
//		}
//
////		/// <summary>
////		/// Loads a collection of HotIssuePriorityData objects from the database.
////		/// </summary>
////		/// <returns>A collection containing all of the HotIssuePriorityData objects in the database.</returns>
//		public static HotIssuesPrioritiesVector LoadCollection(string whereClause)
//		{
//			SqlParameter[] parms = new SqlParameter[] { new SqlParameter("@WhereClause", whereClause) };
//			return LoadCollection("HotIssuesPriorityListProc", parms);
//		}
//	
//		/// <summary>
//		/// Loads a collection of HotIssuePriorityData objects from the database.
//		/// </summary>
//		/// <returns>A collection containing all of the HotIssuePriorityData objects in the database.</returns>
//		public static HotIssuesPrioritiesVector LoadCollection(string spName, SqlParameter[] parms)
//		{
//			HotIssuesPrioritiesVector result = new HotIssuesPrioritiesVector();
//			using (SqlDataReader reader = SqlHelper.ExecuteReader(CommonDAL.ConnectionString, CommandType.StoredProcedure, spName, parms))
//			{
//				while (reader.Read())
//				{
//					HotIssuePriorityData tmp = new HotIssuePriorityData();
//					LoadFromReader(reader, tmp);
//					result.Add(tmp);
//				}
//			}
//			return result;
//		}
//		/// <summary>
//		/// Deletes a HotIssuePriorityData object from the database.
//		/// </summary>
//		/// <param name="HotIssuesPriorityID">The primary key value</param>
//
//	
//		public static void LoadFromReader(SqlDataReader reader, HotIssuePriorityData target)
//		{
//			if (reader != null && !reader.IsClosed)
//			{
//				//target.Key = reader.GetInt32(0);
//				if (!reader.IsDBNull(0)) target.HotIssuePriorityID  = reader.GetInt32(0);
//				if (!reader.IsDBNull(1)) target.HotIssuePriorityName = reader.GetString(1);
//			}
//				
//		}
//			
//		  
////		public static int Insert( HotIssuePriorityData source)
////		{
////			SqlParameter[] parameterValues = GetInsertParameterValues(source);
////			SqlHelper.ExecuteNonQuery(CommonDAL.ConnectionString, CommandType.StoredProcedure, "HotIssuesPriorityInsProc", parameterValues);
////			source.ExpenceID = (int) parameterValues[0].Value;
////			return source.ExpenceID;
////		}
////	
////		public static int Update( HotIssuePriorityData source)
////		{
////			SqlParameter[] parameterValues = GetUpdateParameterValues(source);
////			SqlHelper.ExecuteNonQuery(CommonDAL.ConnectionString, CommandType.StoredProcedure,
////				"HotIssuesPriorityUpdProc", parameterValues);
////			return source.ExpenceID;
////		}
////		private static void UpdateCollection(HotIssuesPrioritiesVector colOld,HotIssuesPrioritiesVector col)
////		{
////			// Delete all old items
////			foreach( HotIssuePriorityData itemOld in colOld)
////			{
////				if(!ExistsKey(itemOld.ExpenceID, col))
////					Delete(itemOld.ExpenceID);
////			}
////			foreach( HotIssuePriorityData item in col)
////			{
////				if(item.ExpenceID <= -1)
////					Insert(item);
////				else
////					Update(item);
////			}
////		}
//		private static bool ExistsKey(int Key, HotIssuesPrioritiesVector col)
//		{
//			foreach( HotIssuePriorityData item in col)
//				if(item.HotIssuePriorityID==Key)
//					return true;
//			return false;
//		}
//	
//		private static SqlParameter GetSqlParameter(string name, ParameterDirection direction, object value)
//		{
//			SqlParameter p = new SqlParameter(name, value);
//			p.Direction = direction;
//			return p;
//		}
//	
//
//
//	}
//
//	
	public class HotIssueCategoryDAL
	{
		/// <summary>
		/// Loads a HotIssueCategoryData object from the database using the given HotIssuesPriorityID
		/// </summary>
		/// <param name="HotIssuesPriorityID">The primary key value</param>
		/// <returns>A HotIssueCategoryData object</returns>
		public static HotIssueCategoryData Load(int HotIssuesPriorityID)
		{
			SqlParameter[] parameterValues = new SqlParameter[] { new SqlParameter("@HotIssueCategoryID", HotIssuesPriorityID) };
			using (SqlDataReader reader = SqlHelper.ExecuteReader(CommonDAL.ConnectionString, CommandType.StoredProcedure, "HotIssueCategorySelProc", parameterValues))
			{
				if (reader.Read())
				{
					HotIssueCategoryData result = new HotIssueCategoryData();
					LoadFromReader(reader,result);
					return result;
				}
				else
					return null;
			}
		}
		/// <summary>
		/// Deletes a HotIssueCategoryData object from the database.
		/// </summary>
		/// <param name="hotIssueCategoryID">The primary key value</param>
		public static void Delete(int hotIssueCategoryID)
		{
			SqlParameter[] parameterValues = new SqlParameter[] { new SqlParameter("@HotIssueCategoryID", hotIssueCategoryID) };
			SqlHelper.ExecuteNonQuery(CommonDAL.ConnectionString, CommandType.StoredProcedure, "HotIssueCategoryDelProc", parameterValues);
		}
		/// <summary>
		/// Loads a HotIssueCategoryData object from the database using the given where clause
		/// </summary>
		/// <param name="whereClause">The filter expression for the query</param>
		/// <returns>A HotIssueCategoryData object</returns>
//		public static HotIssueCategoryData LoadByProject(int nProjectID)
//		{
//			SqlParameter[] parameterValues = new SqlParameter[] { new SqlParameter("@ProjectID", nProjectID) };
//			using (SqlDataReader reader = SqlHelper.ExecuteReader(CommonDAL.ConnectionString, CommandType.StoredProcedure, "HotIssueCategoryListProc", parameterValues))
//			{
//				if (reader.Read())
//				{
//					HotIssueCategoryData result = new HotIssueCategoryData();
//					LoadFromReader(reader,result);
//					return result;
//				}
//				else
//					return null;
//			}
//		}

		//		/// <summary>
		//		/// Loads a collection of HotIssueCategoryData objects from the database.
		//		/// </summary>
		//		/// <returns>A collection containing all of the HotIssueCategoryData objects in the database.</returns>
		public static HotIssuesCategoriesVector LoadCollection()
		{
			SqlParameter[] parms = new SqlParameter[] {};
			return LoadCollection("HotIssuesPriorityListProc", parms);
		}

		//		/// <summary>
		//		/// Loads a collection of HotIssueCategoryData objects from the database.
		//		/// </summary>
		//		/// <returns>A collection containing all of the HotIssueCategoryData objects in the database.</returns>
		public static HotIssuesCategoriesVector LoadCollection(int nProjectID)
		{
			SqlParameter[] parms = new SqlParameter[] { new SqlParameter("@ProjectID", nProjectID) };
			return LoadCollection("HotIssueCategoryListProc", parms);
		}
	
		/// <summary>
		/// Loads a collection of HotIssueCategoryData objects from the database.
		/// </summary>
		/// <returns>A collection containing all of the HotIssueCategoryData objects in the database.</returns>
		public static HotIssuesCategoriesVector LoadCollection(string spName, SqlParameter[] parms)
		{
			HotIssuesCategoriesVector result = new HotIssuesCategoriesVector();
			using (SqlDataReader reader = SqlHelper.ExecuteReader(CommonDAL.ConnectionString, CommandType.StoredProcedure, spName, parms))
			{
				while (reader.Read())
				{
					HotIssueCategoryData tmp = new HotIssueCategoryData();
					LoadFromReader(reader, tmp);
					result.Add(tmp);
				}
			}
			return result;
		}
		/// <summary>
		/// Deletes a HotIssueCategoryData object from the database.
		/// </summary>
		/// <param name="HotIssuesPriorityID">The primary key value</param>

	
		public static void LoadFromReader(SqlDataReader reader, HotIssueCategoryData target)
		{
			if (reader != null && !reader.IsClosed)
			{
				//target.Key = reader.GetInt32(0);
				if (!reader.IsDBNull(0)) target.HotIssueCategoryID  = reader.GetInt32(0);
				if (!reader.IsDBNull(1)) target.HotIssueCategoryName = reader.GetString(1);
				if (!reader.IsDBNull(2)) target.ProjectID = reader.GetInt32(2);
				if (!reader.IsDBNull(3)) target.HotIssueCategoryNameEN = reader.GetString(3);
			}
				
		}
			
		  
				public static int Insert( HotIssueCategoryData source)
				{
					SqlParameter[] parameterValues = GetInsertParameterValues(source);
					SqlHelper.ExecuteNonQuery(CommonDAL.ConnectionString, CommandType.StoredProcedure, "HotIssueCategoryInsProc", parameterValues);
					source.HotIssueCategoryID = (int) parameterValues[0].Value;
					return source.HotIssueCategoryID;
				}

				public static int Update( HotIssueCategoryData source)
				{
					SqlParameter[] parameterValues = GetUpdateParameterValues(source);
					SqlHelper.ExecuteNonQuery(CommonDAL.ConnectionString, CommandType.StoredProcedure,
						"HotIssueCategoryUpdProc", parameterValues);
					return source.HotIssueCategoryID;
				}
//				private static void UpdateCollection(HotIssuesCategoriesVector colOld,HotIssuesCategoriesVector col)
//				{
//					// Delete all old items
//					foreach( HotIssueCategoryData itemOld in colOld)
//					{
//						if(!ExistsKey(itemOld.HotIssueCategoryID, col))
//							Delete(itemOld.HotIssueCategoryID);
//					}
//					foreach( HotIssueCategoryData item in col)
//					{
//						if(item.HotIssueCategoryID <= -1)
//							Insert(item);
//						else
//							Update(item);
//					}
//				}
		private static bool ExistsKey(int Key, HotIssuesCategoriesVector col)
		{
			foreach( HotIssueCategoryData item in col)
				if(item.HotIssueCategoryID==Key)
					return true;
			return false;
		}
	
		private static SqlParameter GetSqlParameter(string name, ParameterDirection direction, object value)
		{
			SqlParameter p = new SqlParameter(name, value);
			p.Direction = direction;
			return p;
		}
	
		private static SqlParameter[] GetInsertParameterValues( HotIssueCategoryData source)
		{
			SqlParameter[] prams = new SqlParameter[4];
			prams[0] = GetSqlParameter("@HotIssueCategoryID", ParameterDirection.Output, source.HotIssueCategoryID);
			prams[1] = GetSqlParameter("@HotIssueCategoryName", ParameterDirection.Input, source.HotIssueCategoryName);
			prams[2] = GetSqlParameter("@ProjectID", ParameterDirection.Input, source.ProjectID);
			prams[3] = GetSqlParameter("@HotIssueCategoryNameEN", ParameterDirection.Input, source.HotIssueCategoryNameEN);
			return prams;
		}

		private static SqlParameter[] GetUpdateParameterValues( HotIssueCategoryData source)
		{
			return new SqlParameter[] {
										  new SqlParameter("@HotIssueCategoryID", source.HotIssueCategoryID),
										  new SqlParameter("@HotIssueCategoryName", source.HotIssueCategoryName),
										  new SqlParameter("@ProjectID", source.ProjectID),
				new SqlParameter("@HotIssueCategoryNameEN", source.HotIssueCategoryNameEN),
										 };
		}

	}


	public class HotIssueStatusDAL
	{
		/// <summary>
		/// Loads a HotIssueStatusData object from the database using the given HotIssuesPriorityID
		/// </summary>
		/// <param name="HotIssuesPriorityID">The primary key value</param>
		/// <returns>A HotIssueStatusData object</returns>
		public static HotIssueStatusData Load(int HotIssuesPriorityID)
		{
			SqlParameter[] parameterValues = new SqlParameter[] { new SqlParameter("@HotIssuesPriorityID", HotIssuesPriorityID) };
			using (SqlDataReader reader = SqlHelper.ExecuteReader(CommonDAL.ConnectionString, CommandType.StoredProcedure, "HotIssuesPrioritySelProc", parameterValues))
			{
				if (reader.Read())
				{
					HotIssueStatusData result = new HotIssueStatusData();
					LoadFromReader(reader,result);
					return result;
				}
				else
					return null;
			}
		}
		/// <summary>
		/// Loads a HotIssueStatusData object from the database using the given where clause
		/// </summary>
		/// <param name="whereClause">The filter expression for the query</param>
		/// <returns>A HotIssueStatusData object</returns>
		public static HotIssueStatusData Load(string whereClause)
		{
			SqlParameter[] parameterValues = new SqlParameter[] { new SqlParameter("@WhereClause", whereClause) };
			using (SqlDataReader reader = SqlHelper.ExecuteReader(CommonDAL.ConnectionString, CommandType.StoredProcedure, "HotIssuesPriorityListProc", parameterValues))
			{
				if (reader.Read())
				{
					HotIssueStatusData result = new HotIssueStatusData();
					LoadFromReader(reader,result);
					return result;
				}
				else
					return null;
			}
		}

		//		/// <summary>
		//		/// Loads a collection of HotIssueStatusData objects from the database.
		//		/// </summary>
		//		/// <returns>A collection containing all of the HotIssueStatusData objects in the database.</returns>
		public static HotIssuesStatusesVector LoadCollection()
		{
			SqlParameter[] parms = new SqlParameter[] {};
			return LoadCollection("HotIssuesPriorityListProc", parms);
		}

		//		/// <summary>
		//		/// Loads a collection of HotIssueStatusData objects from the database.
		//		/// </summary>
		//		/// <returns>A collection containing all of the HotIssueStatusData objects in the database.</returns>
		public static HotIssuesStatusesVector LoadCollection(string whereClause)
		{
			SqlParameter[] parms = new SqlParameter[] { new SqlParameter("@WhereClause", whereClause) };
			return LoadCollection("HotIssuesPriorityListProc", parms);
		}
	
		/// <summary>
		/// Loads a collection of HotIssueStatusData objects from the database.
		/// </summary>
		/// <returns>A collection containing all of the HotIssueStatusData objects in the database.</returns>
		public static HotIssuesStatusesVector LoadCollection(string spName, SqlParameter[] parms)
		{
			HotIssuesStatusesVector result = new HotIssuesStatusesVector();
			using (SqlDataReader reader = SqlHelper.ExecuteReader(CommonDAL.ConnectionString, CommandType.StoredProcedure, spName, parms))
			{
				while (reader.Read())
				{
					HotIssueStatusData tmp = new HotIssueStatusData();
					LoadFromReader(reader, tmp);
					result.Add(tmp);
				}
			}
			return result;
		}
		/// <summary>
		/// Deletes a HotIssueStatusData object from the database.
		/// </summary>
		/// <param name="HotIssuesPriorityID">The primary key value</param>

	
		public static void LoadFromReader(SqlDataReader reader, HotIssueStatusData target)
		{
			if (reader != null && !reader.IsClosed)
			{
				//target.Key = reader.GetInt32(0);
				if (!reader.IsDBNull(0)) target.HotIssueStatusID  = reader.GetInt32(0);
				if (!reader.IsDBNull(1)) target.HotIssueStatusName = reader.GetString(1);
				if (!reader.IsDBNull(2)) target.HotIssueStatusNameEN = reader.GetString(2);
				
			}
				
		}
			
		  
		//		public static int Insert( HotIssueStatusData source)
		//		{
		//			SqlParameter[] parameterValues = GetInsertParameterValues(source);
		//			SqlHelper.ExecuteNonQuery(CommonDAL.ConnectionString, CommandType.StoredProcedure, "HotIssuesPriorityInsProc", parameterValues);
		//			source.ExpenceID = (int) parameterValues[0].Value;
		//			return source.ExpenceID;
		//		}
		//	
		//		public static int Update( HotIssueStatusData source)
		//		{
		//			SqlParameter[] parameterValues = GetUpdateParameterValues(source);
		//			SqlHelper.ExecuteNonQuery(CommonDAL.ConnectionString, CommandType.StoredProcedure,
		//				"HotIssuesPriorityUpdProc", parameterValues);
		//			return source.ExpenceID;
		//		}
		//		private static void UpdateCollection(HotIssuesStatusesVector colOld,HotIssuesStatusesVector col)
		//		{
		//			// Delete all old items
		//			foreach( HotIssueStatusData itemOld in colOld)
		//			{
		//				if(!ExistsKey(itemOld.ExpenceID, col))
		//					Delete(itemOld.ExpenceID);
		//			}
		//			foreach( HotIssueStatusData item in col)
		//			{
		//				if(item.ExpenceID <= -1)
		//					Insert(item);
		//				else
		//					Update(item);
		//			}
		//		}
		private static bool ExistsKey(int Key, HotIssuesStatusesVector col)
		{
			foreach( HotIssueStatusData item in col)
				if(item.HotIssueStatusID==Key)
					return true;
			return false;
		}
	
		private static SqlParameter GetSqlParameter(string name, ParameterDirection direction, object value)
		{
			SqlParameter p = new SqlParameter(name, value);
			p.Direction = direction;
			return p;
		}
	


	}


}
