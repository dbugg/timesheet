using System;
using GrapeCity.ActiveReports;
using GrapeCity.ActiveReports.Controls;
using GrapeCity.ActiveReports.SectionReportModel;
using GrapeCity.ActiveReports.Document.Section;
using GrapeCity.ActiveReports.Document;

namespace Asa.Timesheet.WebPages.Reports
{
    public class ReportMeetingHotIssuesTask : GrapeCity.ActiveReports.SectionReport
	{
        public ReportMeetingHotIssuesTask(string sDescription, string sComment, string sName, System.Drawing.Font f, bool bNormal, string sPosition, string sAccToName)
        {
            InitializeComponent();
            txtDiscription.Text = sDescription;
            //if(sPosition!="")
            //	txtComment.Text=sPosition;
            //else
            {
                txtComment.Text = sComment;
                if (bNormal)
                    txtComment.Font = new System.Drawing.Font(txtName.Font.FontFamily.Name, txtName.Font.SizeInPoints, System.Drawing.FontStyle.Italic);
            }
            txtName.Text = sName;
            txtName.Font = txtDiscription.Font = txtComment.Font = f;
            //txtAccToName.Text=sAccToName;
            //if(sDescription=="")
            //	GroupHeader2.Visible=false;
            //if(sComment=="")
            //	Detail.Visible=false;
        }

        private void ReportMeetingHotIssuesTask_ReportStart(object sender, System.EventArgs eArgs)
        {

        }

        private void GroupHeader1_Format(object sender, System.EventArgs eArgs)
        {

        }

        private void Detail_Format(object sender, System.EventArgs eArgs)
        {

        }

		#region ActiveReports Designer generated code





















        public void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ReportMeetingHotIssuesTask));
            this.Detail = new GrapeCity.ActiveReports.SectionReportModel.Detail();
            this.txtComment = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.Line8 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.Line9 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.Line25 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.Line27 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.GroupHeader1 = new GrapeCity.ActiveReports.SectionReportModel.GroupHeader();
            this.txtName = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.Line21 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.Line22 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.Line23 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.Line7 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.GroupFooter1 = new GrapeCity.ActiveReports.SectionReportModel.GroupFooter();
            this.GroupHeader2 = new GrapeCity.ActiveReports.SectionReportModel.GroupHeader();
            this.txtDiscription = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.Line19 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.Line20 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.Line24 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.Line26 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.GroupFooter2 = new GrapeCity.ActiveReports.SectionReportModel.GroupFooter();
            this.Line18 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            ((System.ComponentModel.ISupportInitialize)(this.txtComment)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDiscription)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            // 
            // Detail
            // 
            this.Detail.CanShrink = true;
            this.Detail.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
            this.txtComment,
            this.Line8,
            this.Line9,
            this.Line25,
            this.Line27});
            this.Detail.Height = 0.2604167F;
            this.Detail.Name = "Detail";
            this.Detail.Format += new System.EventHandler(this.Detail_Format);
            // 
            // txtComment
            // 
            this.txtComment.DataField = "HotIssueComment";
            this.txtComment.Height = 0.312F;
            this.txtComment.Left = 0.3125F;
            this.txtComment.Name = "txtComment";
            this.txtComment.Style = "font-family: Verdana; font-size: 9pt; font-style: italic; font-weight: normal; dd" +
    "o-char-set: 1";
            this.txtComment.Text = null;
            this.txtComment.Top = 0F;
            this.txtComment.Width = 4.25F;
            // 
            // Line8
            // 
            this.Line8.AnchorBottom = true;
            this.Line8.Height = 0.312F;
            this.Line8.Left = 7.125F;
            this.Line8.LineWeight = 1F;
            this.Line8.Name = "Line8";
            this.Line8.Top = 0F;
            this.Line8.Width = 0F;
            this.Line8.X1 = 7.125F;
            this.Line8.X2 = 7.125F;
            this.Line8.Y1 = 0F;
            this.Line8.Y2 = 0.312F;
            // 
            // Line9
            // 
            this.Line9.AnchorBottom = true;
            this.Line9.Height = 0.312F;
            this.Line9.Left = 4.625F;
            this.Line9.LineWeight = 1F;
            this.Line9.Name = "Line9";
            this.Line9.Top = 0F;
            this.Line9.Width = 0F;
            this.Line9.X1 = 4.625F;
            this.Line9.X2 = 4.625F;
            this.Line9.Y1 = 0F;
            this.Line9.Y2 = 0.312F;
            // 
            // Line25
            // 
            this.Line25.AnchorBottom = true;
            this.Line25.Height = 0.312F;
            this.Line25.Left = 0F;
            this.Line25.LineWeight = 1F;
            this.Line25.Name = "Line25";
            this.Line25.Top = 0F;
            this.Line25.Width = 0F;
            this.Line25.X1 = 0F;
            this.Line25.X2 = 0F;
            this.Line25.Y1 = 0F;
            this.Line25.Y2 = 0.312F;
            // 
            // Line27
            // 
            this.Line27.AnchorBottom = true;
            this.Line27.Height = 0.312F;
            this.Line27.Left = 8F;
            this.Line27.LineWeight = 1F;
            this.Line27.Name = "Line27";
            this.Line27.Top = 0F;
            this.Line27.Width = 0F;
            this.Line27.X1 = 8F;
            this.Line27.X2 = 8F;
            this.Line27.Y1 = 0F;
            this.Line27.Y2 = 0.312F;
            // 
            // GroupHeader1
            // 
            this.GroupHeader1.CanShrink = true;
            this.GroupHeader1.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
            this.txtName,
            this.Line21,
            this.Line22,
            this.Line23,
            this.Line7});
            this.GroupHeader1.Height = 0.21875F;
            this.GroupHeader1.KeepTogether = true;
            this.GroupHeader1.Name = "GroupHeader1";
            this.GroupHeader1.Format += new System.EventHandler(this.GroupHeader1_Format);
            // 
            // txtName
            // 
            this.txtName.DataField = "HotIssueName";
            this.txtName.Height = 0.2F;
            this.txtName.Left = 0.3125F;
            this.txtName.Name = "txtName";
            this.txtName.Style = "font-family: Verdana; font-size: 9pt; font-weight: bold; text-decoration: none; d" +
    "do-char-set: 1";
            this.txtName.Text = null;
            this.txtName.Top = 0.0004999935F;
            this.txtName.Width = 4.25F;
            // 
            // Line21
            // 
            this.Line21.AnchorBottom = true;
            this.Line21.Height = 0.2F;
            this.Line21.Left = 4.625F;
            this.Line21.LineWeight = 1F;
            this.Line21.Name = "Line21";
            this.Line21.Top = 0F;
            this.Line21.Width = 0F;
            this.Line21.X1 = 4.625F;
            this.Line21.X2 = 4.625F;
            this.Line21.Y1 = 0F;
            this.Line21.Y2 = 0.2F;
            // 
            // Line22
            // 
            this.Line22.AnchorBottom = true;
            this.Line22.Height = 0.2F;
            this.Line22.Left = 7.125F;
            this.Line22.LineWeight = 1F;
            this.Line22.Name = "Line22";
            this.Line22.Top = 0F;
            this.Line22.Width = 0F;
            this.Line22.X1 = 7.125F;
            this.Line22.X2 = 7.125F;
            this.Line22.Y1 = 0F;
            this.Line22.Y2 = 0.2F;
            // 
            // Line23
            // 
            this.Line23.AnchorBottom = true;
            this.Line23.Height = 0.2F;
            this.Line23.Left = 0F;
            this.Line23.LineWeight = 1F;
            this.Line23.Name = "Line23";
            this.Line23.Top = 0F;
            this.Line23.Width = 0F;
            this.Line23.X1 = 0F;
            this.Line23.X2 = 0F;
            this.Line23.Y1 = 0F;
            this.Line23.Y2 = 0.2F;
            // 
            // Line7
            // 
            this.Line7.AnchorBottom = true;
            this.Line7.Height = 0.2F;
            this.Line7.Left = 8F;
            this.Line7.LineWeight = 1F;
            this.Line7.Name = "Line7";
            this.Line7.Top = 0F;
            this.Line7.Width = 0F;
            this.Line7.X1 = 8F;
            this.Line7.X2 = 8F;
            this.Line7.Y1 = 0F;
            this.Line7.Y2 = 0.2F;
            // 
            // GroupFooter1
            // 
            this.GroupFooter1.Height = 0F;
            this.GroupFooter1.Name = "GroupFooter1";
            // 
            // GroupHeader2
            // 
            this.GroupHeader2.CanShrink = true;
            this.GroupHeader2.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
            this.txtDiscription,
            this.Line19,
            this.Line20,
            this.Line24,
            this.Line26});
            this.GroupHeader2.Height = 0.28125F;
            this.GroupHeader2.Name = "GroupHeader2";
            // 
            // txtDiscription
            // 
            this.txtDiscription.DataField = "HotIssueDiscription";
            this.txtDiscription.Height = 0.25F;
            this.txtDiscription.Left = 0.3125F;
            this.txtDiscription.Name = "txtDiscription";
            this.txtDiscription.Style = "font-family: Verdana; font-size: 9pt; font-weight: normal; ddo-char-set: 1";
            this.txtDiscription.Text = null;
            this.txtDiscription.Top = 0F;
            this.txtDiscription.Width = 4.25F;
            // 
            // Line19
            // 
            this.Line19.AnchorBottom = true;
            this.Line19.Height = 0.25F;
            this.Line19.Left = 7.125F;
            this.Line19.LineWeight = 1F;
            this.Line19.Name = "Line19";
            this.Line19.Top = 0F;
            this.Line19.Width = 0F;
            this.Line19.X1 = 7.125F;
            this.Line19.X2 = 7.125F;
            this.Line19.Y1 = 0F;
            this.Line19.Y2 = 0.25F;
            // 
            // Line20
            // 
            this.Line20.AnchorBottom = true;
            this.Line20.Height = 0.25F;
            this.Line20.Left = 4.625F;
            this.Line20.LineWeight = 1F;
            this.Line20.Name = "Line20";
            this.Line20.Top = 0F;
            this.Line20.Width = 0F;
            this.Line20.X1 = 4.625F;
            this.Line20.X2 = 4.625F;
            this.Line20.Y1 = 0F;
            this.Line20.Y2 = 0.25F;
            // 
            // Line24
            // 
            this.Line24.AnchorBottom = true;
            this.Line24.Height = 0.25F;
            this.Line24.Left = 0F;
            this.Line24.LineWeight = 1F;
            this.Line24.Name = "Line24";
            this.Line24.Top = 0F;
            this.Line24.Width = 0F;
            this.Line24.X1 = 0F;
            this.Line24.X2 = 0F;
            this.Line24.Y1 = 0F;
            this.Line24.Y2 = 0.25F;
            // 
            // Line26
            // 
            this.Line26.AnchorBottom = true;
            this.Line26.Height = 0.25F;
            this.Line26.Left = 8F;
            this.Line26.LineWeight = 1F;
            this.Line26.Name = "Line26";
            this.Line26.Top = 0F;
            this.Line26.Width = 0F;
            this.Line26.X1 = 8F;
            this.Line26.X2 = 8F;
            this.Line26.Y1 = 0F;
            this.Line26.Y2 = 0.25F;
            // 
            // GroupFooter2
            // 
            this.GroupFooter2.CanGrow = false;
            this.GroupFooter2.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
            this.Line18});
            this.GroupFooter2.Height = 0.01041667F;
            this.GroupFooter2.Name = "GroupFooter2";
            // 
            // Line18
            // 
            this.Line18.AnchorBottom = true;
            this.Line18.Height = 0F;
            this.Line18.Left = 0F;
            this.Line18.LineWeight = 1F;
            this.Line18.Name = "Line18";
            this.Line18.Top = 0F;
            this.Line18.Width = 8.006945F;
            this.Line18.X1 = 0F;
            this.Line18.X2 = 8.006945F;
            this.Line18.Y1 = 0F;
            this.Line18.Y2 = 0F;
            // 
            // ReportMeetingHotIssuesTask
            // 
            this.MasterReport = false;
            this.PageSettings.PaperHeight = 11F;
            this.PageSettings.PaperWidth = 8.5F;
            this.PrintWidth = 8.020833F;
            this.Sections.Add(this.GroupHeader1);
            this.Sections.Add(this.GroupHeader2);
            this.Sections.Add(this.Detail);
            this.Sections.Add(this.GroupFooter2);
            this.Sections.Add(this.GroupFooter1);
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule(resources.GetString("$this.StyleSheet"), "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: inherit; font-style: inherit; font-variant: inherit; font-weight: bo" +
            "ld; font-size: 16pt; font-size-adjust: inherit; font-stretch: inherit", "Heading1", "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: Times New Roman; font-style: italic; font-variant: inherit; font-wei" +
            "ght: bold; font-size: 14pt; font-size-adjust: inherit; font-stretch: inherit", "Heading2", "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: inherit; font-style: inherit; font-variant: inherit; font-weight: bo" +
            "ld; font-size: 13pt; font-size-adjust: inherit; font-stretch: inherit", "Heading3", "Normal"));
            this.ReportStart += new System.EventHandler(this.ReportMeetingHotIssuesTask_ReportStart);
            ((System.ComponentModel.ISupportInitialize)(this.txtComment)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDiscription)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();

        }

		#endregion

        private GroupHeader GroupHeader1;
        private TextBox txtName;
        private Line Line21;
        private Line Line22;
        private Line Line23;
        private Line Line7;
        private GroupHeader GroupHeader2;
        private Line Line19;
        private Line Line20;
        private Line Line24;
        private Line Line26;
        private Detail Detail;
        private Line Line8;
        private Line Line9;
        private Line Line25;
        private Line Line27;
        private GroupFooter GroupFooter2;
        private Line Line18;
        private TextBox txtComment;
        private TextBox txtDiscription;
        private GroupFooter GroupFooter1;
	}
}
