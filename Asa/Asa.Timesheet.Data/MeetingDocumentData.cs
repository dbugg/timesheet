using System;
using System.Collections;
using System.Text;

namespace Asa.Timesheet.Data
{
	public class MeetingDocumentData
	{
		private int _meetingDocument = -1;
		private int _meetingID = -1;
		private bool _isDeleted;

		/// <summary>
		/// Initialize an new empty Login object.
		/// </summary>
		public MeetingDocumentData()
		{
		}
		
		/// <summary>
		/// Initialize a new Login object with the given parameters.
		/// </summary>
		public MeetingDocumentData(int meetingDocument, int meetingID, bool isDeleted)
		{
			this._meetingDocument = meetingDocument;
			this._meetingID = meetingID;
			this._isDeleted = isDeleted;
		}	
		
	  
		public override string ToString()
		{
			StringBuilder builder = new StringBuilder();
			builder.Append("_meetingDocument: " + _meetingDocument.ToString()).Append("\r\n");
			builder.Append("_meetingID: " + _meetingID.ToString()).Append("\r\n");
			builder.Append("_isDeleted: " + _isDeleted.ToString()).Append("\r\n");
			builder.Append("\r\n");
			return builder.ToString();
		}	
		public object GetByIndex(int nIndex)
		{
			switch(nIndex)
			{
				case 0:
					return _meetingDocument;
				case 1:
					return _meetingID;
				case 2:
					return _isDeleted;
			}
			return null;
		}
		#region Properties
	
		public int MeetingDocument
		{
			get { return _meetingDocument; }
			set { _meetingDocument = value; }
		}
	
		public int MeetingID
		{
			get { return _meetingID; }
			set { _meetingID = value; }
		}
	
		public bool IsDeleted
		{
			get { return _isDeleted; }
			set { _isDeleted = value; }
		}
	
		#endregion
	}
}


