using System;
using System.Collections;
using System.Text;

namespace Asa.Timesheet.Data
{
	/// <summary>
	/// Summary description for FilePMData.
	/// </summary>
	public class FilePMData
	{
		private int _fileID;
		private string _fileSubject;
		private string _fileMinutes;
		private int _fileVersion;
		private string _createdBy;
		private string _fileType;
		private DateTime _dateCreated;
		private bool _mailForDownload;
		private int _catetegoryID;
		private string _fileNameSave;
		private bool _isClientVisible;
		private bool _isSubcontractorVisible;
		private bool _isBuilderVisible;
		private string _categoryName = string.Empty;
		private string _projectName = string.Empty;
		private bool _isNadzorVisible;
		private bool _isOtherVisible;
        private string _link;

        public string Link
        {
            get { return _link; }
            set { _link = value; }
        }

		/// <summary>
		/// Initialize an new empty Login object.
		/// </summary>
		public FilePMData()
		{
			
		}

		public FilePMData( int fileID,string fileSubject,string fileMinutes,int fileVersion,string createdBy,string fileType,DateTime dateCreated,bool mailForDownload,int catetegoryID,string fileNameSave,bool isClientVisible,bool isSubcontractorVisible,bool isBuilderVisible)
		{
		this._fileID=fileID;
		this._fileSubject=fileSubject;
		this._fileMinutes= fileMinutes;
		this._fileVersion=fileVersion;
		this._createdBy=createdBy;
		this._fileType=fileType;
		this._dateCreated=dateCreated;
		this._mailForDownload=mailForDownload;
		this._catetegoryID=catetegoryID;
		this._fileNameSave = fileNameSave;
		this._isClientVisible = isClientVisible;
		this._isSubcontractorVisible = isSubcontractorVisible;
		this._isBuilderVisible = isBuilderVisible;
		}
		public override string ToString()
		{
			StringBuilder builder = new StringBuilder();
			builder.Append("_fileID: " + _fileID.ToString()).Append("\r\n");
			builder.Append("_fileSubject: " + _fileSubject.ToString()).Append("\r\n");
			builder.Append("_fileMinutes: " + _fileMinutes.ToString()).Append("\r\n");
			builder.Append("_fileVersion: " + _fileVersion.ToString()).Append("\r\n");
			builder.Append("_createdBy: " + _createdBy.ToString()).Append("\r\n");
			builder.Append("_fileType: " + _fileType.ToString()).Append("\r\n");
			builder.Append("_dateCreated: " + _dateCreated.ToString()).Append("\r\n");
			builder.Append("_mailForDownload: " + _mailForDownload.ToString()).Append("\r\n");
			builder.Append("_catetegoryID: " + _catetegoryID.ToString()).Append("\r\n");
			builder.Append("_fileNameSave: " + _fileNameSave.ToString()).Append("\r\n");
			builder.Append("_catetegoryName: " + _categoryName.ToString()).Append("\r\n");
			builder.Append("_projectName: " + _projectName.ToString()).Append("\r\n");
			builder.Append("_isClientVisible: " + _isClientVisible.ToString()).Append("\r\n");
			builder.Append("_isSubcontractorVisible: " + _isSubcontractorVisible.ToString()).Append("\r\n");
			builder.Append("_isBuilderVisible: " + _isBuilderVisible.ToString()).Append("\r\n");
			builder.Append("\r\n");
			return builder.ToString();
		}	
		public object GetByIndex(int nIndex)
		{
			switch(nIndex)
			{	
				case 0:
					return _fileID;
				case 1:
					return _fileSubject;
				case 2:
					return _fileMinutes;
				case 3:
					return _fileVersion;
				case 4:
					return _createdBy;
				case 5:
					return _fileType;
				case 6:
					return _dateCreated;
				case 7:
					return _mailForDownload;
				case 8:
					return _catetegoryID;
				case 9:
					return _fileNameSave;
				case 10:
					return _isClientVisible;
				case 11:
					return _isSubcontractorVisible;
				case 12:
					return _isBuilderVisible;
				case 13:
					return _categoryName;
				case 14:
					return _projectName;
			}
			return null;
		}
		#region Properties

		public int FileID
		{
			get { return _fileID; }
			set { _fileID = value; }
		}
		public string FileSubject
		{
			get { return _fileSubject; }
			set { _fileSubject = value; }
		}
		public string FileMinutes
		{
			get { return _fileMinutes; }
			set { _fileMinutes = value; }
		}
		public int FileVersion
		{
			get { return _fileVersion; }
			set { _fileVersion = value; }
		}
		public string CreatedBy
		{
			get { return _createdBy; }
			set { _createdBy = value; }
		}
		public string FileType
		{
			get { return _fileType; }
			set { _fileType = value; }
		}
		public DateTime DateCreated
		{
			get { return _dateCreated; }
			set { _dateCreated = value; }
		}
		public bool MailForDownload
		{
			get { return _mailForDownload; }
			set { _mailForDownload = value; }
		}
		public int CatetegoryID
		{
			get { return _catetegoryID; }
			set { _catetegoryID = value; }
		}
		public string FileNameSave
		{
			get { return _fileNameSave; }
			set { _fileNameSave = value; }
		}
				public bool IsClientVisible
				{
					get { return _isClientVisible; }
					set { _isClientVisible = value; }
				}
		
				public bool IsSubcontractorVisible
				{
					get { return _isSubcontractorVisible; }
					set { _isSubcontractorVisible = value; }
				}
		
				public bool IsBuilderVisible
				{
					get { return _isBuilderVisible; }
					set { _isBuilderVisible = value; }
				}
		public bool IsNadzorVisible
		{
			get { return _isNadzorVisible; }
			set { _isNadzorVisible = value; }
		}
		public bool IsOtherVisible
		{
			get { return _isOtherVisible; }
			set { _isOtherVisible = value; }
		}
		
		public string CategoryName
		{
			get { return _categoryName; }
			set { _categoryName = value; }
		}
		public string ProjectName
		{
			get { return _projectName; }
			set { _projectName = value; }
		}

		#endregion
	}

 
}
