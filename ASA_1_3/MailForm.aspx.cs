using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Data.SqlClient;
using log4net;
using Asa.Timesheet.Data;
using Asa.Timesheet.Data.Entities;
using System.Collections.Specialized;

namespace Asa.Timesheet.WebPages
{
	/// <summary>
	/// Summary description for MailForm.
	/// </summary>
	public class MailForm : TimesheetPageBase
	{
		#region Web controls

		protected System.Web.UI.WebControls.Label lblTitle;
		protected System.Web.UI.WebControls.Label lblUser;
		protected System.Web.UI.WebControls.PlaceHolder menuHolder;
		protected System.Web.UI.WebControls.Label lblName;
		protected System.Web.UI.WebControls.Label lblCity;
		protected System.Web.UI.WebControls.TextBox txtEmail;
		protected System.Web.UI.WebControls.Button btnSave;
		protected System.Web.UI.WebControls.Button btnCancel;
		protected System.Web.UI.WebControls.Label Label1;
		protected System.Web.UI.WebControls.Label Label2;
		protected System.Web.UI.WebControls.Button btnToRight;
		protected System.Web.UI.WebControls.Button btnToLeft;
		protected System.Web.UI.WebControls.Label Label3;
		protected System.Web.UI.WebControls.ListBox lsMails;
		protected System.Web.UI.WebControls.ListBox lsMailsSel;
		protected System.Web.UI.HtmlControls.HtmlTable tblData;
		protected System.Web.UI.WebControls.TextBox txtSubject;
		
		protected System.Web.UI.WebControls.Label lblStartDay;
		protected System.Web.UI.WebControls.Label lblSelStartDate;
		protected System.Web.UI.WebControls.Label lblSelEndDate;
		protected System.Web.UI.WebControls.CheckBox ckDates;
		protected System.Web.UI.WebControls.Label lblProject;
		protected System.Web.UI.WebControls.DropDownList ddlProject;
		protected System.Web.UI.WebControls.Button btnGen;
		protected System.Web.UI.HtmlControls.HtmlTable tblSelect;
		protected System.Web.UI.WebControls.Label lbDo;
		protected System.Web.UI.WebControls.Label Label4;
		protected System.Web.UI.WebControls.Label lbErrDate;
		protected Telerik.WebControls.RadEditor ftb;
		protected Asa.Timesheet.WebPages.UserControls.PageHeader header;
		protected System.Web.UI.WebControls.Label Label5;
		protected System.Web.UI.WebControls.ListBox lbSelectedMails;
		protected System.Web.UI.WebControls.ListBox lbMails;
		protected System.Web.UI.HtmlControls.HtmlInputText hdnlbMails;
		protected System.Web.UI.HtmlControls.HtmlInputText hdnlbSelectedMails;
		protected System.Web.UI.WebControls.DropDownList ddlUsers;
		protected System.Web.UI.WebControls.Label lblError;
		protected System.Web.UI.WebControls.Label lblInfo;
		protected System.Web.UI.WebControls.ListBox lbSelectedUsersMails;
		protected System.Web.UI.WebControls.ListBox lbUsersMails;
		protected System.Web.UI.HtmlControls.HtmlInputText hdnlbUsersMails;
		protected System.Web.UI.HtmlControls.HtmlInputText hdnlbSelectedUsersMails;
		protected System.Web.UI.WebControls.Label lblUsersEmails;
		
		#endregion

		public string _username="";
		protected System.Web.UI.WebControls.Label lblDo1;
		protected System.Web.UI.WebControls.Label lblSelectUser;
		protected System.Web.UI.HtmlControls.HtmlInputText hdnErrorMessages;
		protected System.Web.UI.WebControls.Calendar calStartDate;
		protected System.Web.UI.WebControls.Calendar calEndDate;
    protected System.Web.UI.WebControls.DropDownList ddlProjectStatus;
		protected System.Web.UI.WebControls.Label Label6;
		
		private static readonly ILog log = LogManager.GetLogger(typeof(MailForm));

        private void Page_Load(object sender, System.EventArgs e)
        {

            //			if(LoggedUser.IsEmployee && this.Page.Request.Params["view"]!="hours")
            //			{
            //				ErrorRedirect(Resource.ResourceManager["noRightsAlert"]);
            //			}

            ftb.HasPermission = ftb.Editable = true;

            if (!IsPostBack)
            {
                header.PageTitle = Resource.ResourceManager["reports_mail"];
                header.UserName = LoggedUser.UserName;

                UIHelpers.LoadProjectStatusWithoutConcluded(ddlProjectStatus, (int)ProjectsData.ProjectsByStatus.Active);
                LoadProjects();
                LoadUsers();
                //BindList();

                lbMails.Attributes["ondblclick"] = "MailDblClick()";
                lbSelectedMails.Attributes["ondblclick"] = "MailDblClick()";
                lbUsersMails.Attributes["ondblclick"] = "MailDblClick()";
                lbSelectedUsersMails.Attributes["ondblclick"] = "MailDblClick()";
                btnGen.Attributes["onclick"] = "if (!CheckSelected()) return false;";
                SetErrorMessages();
            }

            UIHelpers.CreateMenu(menuHolder, LoggedUser);
            RestoreMailsLists();
            RestoreUsersMailsLists();

            if (SessionManager.CurrentMailInfo != null
                && SessionManager.CurrentMailInfo.IsFromHours
                && this.Page.Request.Params["view"] == "hours"
                )
            {
                tblSelect.Visible = false;
                string subject;
                ftb.Html = MailBO.GetMailText(SessionManager.CurrentMailInfo, this.LoggedUser.UserName, out subject);

                txtSubject.Text = subject;
            }
            else
            {
                lbDo.Visible = false;
                lblDo1.Visible = false;
            }
        }

		#region Web Form Designer generated code
        override protected void OnInit(EventArgs e)
        {
            //
            // CODEGEN: This call is required by the ASP.NET Web Form Designer.
            //
            InitializeComponent();
            base.OnInit(e);
        }
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
        private void InitializeComponent()
        {
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            this.ckDates.CheckedChanged += new System.EventHandler(this.ckDates_CheckedChanged);
            this.ddlProjectStatus.SelectedIndexChanged += new System.EventHandler(this.ddlProjectStatus_SelectedIndexChanged);
            this.btnGen.Click += new System.EventHandler(this.btnGen_Click);
            this.btnToRight.Click += new System.EventHandler(this.btnToRight_Click);
            this.btnToLeft.Click += new System.EventHandler(this.btnToLeft_Click);
            this.Load += new System.EventHandler(this.Page_Load);

        }
		#endregion
		
		#region Get mails

        private void GetMails(out StringCollection names, out StringCollection mails)
        {
            names = new StringCollection();
            mails = new StringCollection();

            //Current
            names.Add(SessionManager.LoggedUserInfo.UserName);
            mails.Add(SessionManager.LoggedUserInfo.Mail);
            // Worked on 
            if (SessionManager.CurrentMailInfo != null
                && SessionManager.CurrentMailInfo.IsFromHours
                && this.Page.Request.Params["view"] == "hours")
            {

                UserInfo ui = UsersData.SelectUserByID(SessionManager.CurrentMailInfo.UserID);
                _username = ui.UserName;
                if (ui.Mail != SessionManager.LoggedUserInfo.Mail)
                {
                    names.Add(ui.UserName);
                    mails.Add(ui.Mail);
                }
            }

            //get selected mails
            string[] s = hdnlbSelectedMails.Value.Split(';');
            for (int i = 0; i < s.Length; i++)
                if (s[i] != String.Empty)
                {
                    string[] valuetext = s[i].Split(':');
                    if (valuetext[0] != String.Empty)
                    {
                        names.Add(valuetext[1]);
                        mails.Add(valuetext[0]);
                    }
                }
            //get selected users mails
            s = hdnlbSelectedUsersMails.Value.Split(';');
            for (int i = 0; i < s.Length; i++)
                if (s[i] != String.Empty)
                {
                    string[] valuetext = s[i].Split(':');
                    if (valuetext[0] != String.Empty)
                    {
                        names.Add(valuetext[1]);
                        mails.Add(valuetext[0]);
                    }
                }

            //other emails
            string[] otherMails = txtEmail.Text.Split(new char[] { ';' });
            for (int i = 0; i < otherMails.Length; i++)
            {
                if (otherMails[i].Length > 0)
                {
                    names.Add(otherMails[i]);
                    mails.Add(otherMails[i]);
                }
            }
        }

        private StringCollection GetMails()
        {
            StringCollection sc = new StringCollection();
            //Current
            sc.Add(SessionManager.LoggedUserInfo.Mail);
            // Worked on 
            if (SessionManager.CurrentMailInfo != null
                && SessionManager.CurrentMailInfo.IsFromHours
                && this.Page.Request.Params["view"] == "hours")
            {

                UserInfo ui = UsersData.SelectUserByID(SessionManager.CurrentMailInfo.UserID);
                _username = ui.UserName;
                if (ui.Mail != SessionManager.LoggedUserInfo.Mail)
                    sc.Add(ui.Mail);
            }

            //get selected mails
            string[] s = hdnlbSelectedMails.Value.Split(';');
            for (int i = 0; i < s.Length; i++)
                if (s[i] != String.Empty)
                {
                    string[] valuetext = s[i].Split(':');
                    if (valuetext[0] != String.Empty) sc.Add(valuetext[0]);
                }
            //get selected users mails
            s = hdnlbSelectedUsersMails.Value.Split(';');
            for (int i = 0; i < s.Length; i++)
                if (s[i] != String.Empty)
                {
                    string[] valuetext = s[i].Split(':');
                    if (valuetext[0] != String.Empty) sc.Add(valuetext[0]);
                }

            //other emails
            string[] mails = txtEmail.Text.Split(new char[] { ';' });
            for (int i = 0; i < mails.Length; i++)
            {
                if (mails[i].Length > 0)
                    sc.Add(mails[i]);
            }
            return sc;
        }

		#endregion

		#region Event handlers

        private void btnSave_Click(object sender, System.EventArgs e)
        {
            StringCollection names, mails;
            GetMails(out names, out mails);

            for (int i = 0; i < mails.Count; i++)
            {
                string subject = (mails[i] == SessionManager.LoggedUserInfo.Mail) ? _username : txtSubject.Text;

                string mailTo = String.Empty, mailCopyTo = string.Empty;

                mailTo = names[i];

                for (int j = 0; j < names.Count; j++)
                    if (j != i) mailCopyTo += names[j] + "; ";

                string recipientsHeaderHtml = MailBO.CreateRecipientsHeaderHtml(mailTo, mailCopyTo);

                MailBO.SendMail(mails[i], subject, recipientsHeaderHtml + ftb.Html);

                //ftb.Html = recipientsHeaderHtml+ftb.Html;

            }

            btnSave.Visible = false;
            btnCancel.Visible = false;
            tblData.Visible = false;
            tblSelect.Visible = false;
            lblInfo.Text = Resource.ResourceManager["mailSent"];
            SessionManager.CurrentMailInfo = null;
        }

        private void btnToRight_Click(object sender, System.EventArgs e)
        {
            AddSelectedListBoxValues(lsMails, lsMailsSel);
        }

        private void btnToLeft_Click(object sender, System.EventArgs e)
        {
            AddSelectedListBoxValues(lsMailsSel, lsMails);
        }

        private void ckDates_CheckedChanged(object sender, System.EventArgs e)
        {
            bool bEnable = !ckDates.Checked;
            calStartDate.Enabled = calEndDate.Enabled = bEnable;
            if (!bEnable)
            {
                calStartDate.SelectedDate = calEndDate.SelectedDate = Constants.DateMax;
                lblSelStartDate.Text = lblSelEndDate.Text = "(" + Resource.ResourceManager["reports_SelectDate"] + ")";
            }
            else
            {
                calStartDate.SelectedDate = calEndDate.SelectedDate =
                    calStartDate.VisibleDate = calEndDate.VisibleDate = DateTime.Now.Date;
                lblSelStartDate.Text = lblSelEndDate.Text = "(" + DateTime.Now.Date.ToString("d") + ")";
            }
        }

        private void calStartDate_SelectionChanged(object sender, System.EventArgs e)
        {
            if (calStartDate.SelectedDates.Count > 1)
            {
                lblSelStartDate.Text = " (" + calStartDate.SelectedDates[0].ToString("d") + ")";

                calEndDate.SelectedDate = calStartDate.SelectedDates[calStartDate.SelectedDates.Count - 1];
                calEndDate.VisibleDate = calStartDate.SelectedDates[calStartDate.SelectedDates.Count - 1];
                lblSelEndDate.Text = "(" +
                    calStartDate.SelectedDates[calStartDate.SelectedDates.Count - 1].ToString("d") + ")";
                //calStartDate.SelectedDate = calStartDate.SelectedDates[0];
            }
            else
            {
                //peppi
                lblSelStartDate.Text = "(" + calStartDate.SelectedDate.ToString("d") + ")";
                //				calEndDate.SelectedDate = Constants.DateMax;
                //				lblSelEndDate.Text = "("+Resource.ResourceManager["reports_SelectDate"]+")";
            }
        }
        private bool AreValidDates()
        {
            if (calEndDate.SelectedDate < calStartDate.SelectedDate)
            {
                lbErrDate.Text = Resource.ResourceManager["reports_errorDate"];
                lblSelEndDate.Text = "(" + Resource.ResourceManager["reports_SelectDate"] + ")";
                return false;
            }
            return true;
        }
        private void calEndDate_SelectionChanged(object sender, System.EventArgs e)
        {
            if (AreValidDates())
                lblSelEndDate.Text = "(" + calEndDate.SelectedDate.ToString("d") + ")";

        }

        private void btnCancel_Click(object sender, System.EventArgs e)
        {
            SessionManager.CurrentMailInfo = null;
            Response.Redirect("Hours.aspx");
        }

        private void btnGen_Click(object sender, System.EventArgs e)
        {
            int projectID = int.Parse(ddlProject.SelectedValue);
            if (projectID == 0)
            {
                lblError.Text = Resource.ResourceManager["reports_ErrorProjectNotSelected"];
                AlertFieldNotEntered(lblProject);
                return;
            }

            int userID = int.Parse(ddlUsers.SelectedValue);
            if (userID == -1)
            {
                lblError.Text = Resource.ResourceManager["reports_ErrorUserNotSelected"];
                AlertFieldNotEntered(lblSelectUser);
                return;
            }

            DateTime startDate, endDate;

            if (!ckDates.Checked)
            {
                startDate = calStartDate.SelectedDate;
                endDate = calEndDate.SelectedDate;
            }
            else startDate = endDate = Constants.DateMax;

            string subject;

            //	string headerHtml, html, footerHtml;
            //	MailBO.GetMailText(projectID, userID, startDate, endDate, this.LoggedUser.UserName, out headerHtml, out html, out footerHtml, out subject);

            ftb.Html = MailBO.GetMailText(projectID, userID, startDate, endDate, this.LoggedUser.UserName, out subject);
            txtSubject.Text = subject;
        }

		#endregion

		#region za listboxovete s js

        private void RestoreMailsLists()
        {
            if (!this.IsPostBack)
            {
                StringCollection sc = LoadEmails();
                StringBuilder sb = new StringBuilder();

                for (int i = 0; i < sc.Count; i++)
                {
                    string[] valuetext = sc[i].Split(':');
                    if (valuetext.Length > 1)
                    {
                        sb.Append(sc[i] + ";");
                        lbMails.Items.Add(new ListItem(valuetext[1], valuetext[0]));
                    }
                }
                hdnlbMails.Value = sb.ToString();
                hdnlbSelectedMails.Value = String.Empty;
            }

            lbMails.Items.Clear(); lbSelectedMails.Items.Clear();

            string[] s = hdnlbMails.Value.Split(';');
            for (int i = 0; i < s.Length; i++)
                if (s[i] != String.Empty)
                {
                    string[] valuetext = s[i].Split(':');
                    if (valuetext.Length > 1)
                        lbMails.Items.Add(new ListItem(valuetext[1], valuetext[0]));
                }

            s = hdnlbSelectedMails.Value.Split(';');
            for (int i = 0; i < s.Length; i++)
                if (s[i] != String.Empty)
                {
                    string[] valuetext = s[i].Split(':');
                    if (valuetext.Length > 1)
                        lbSelectedMails.Items.Add(new ListItem(valuetext[1], valuetext[0]));
                }

        }

        private void RestoreUsersMailsLists()
        {
            if (!this.IsPostBack)
            {
                StringCollection sc = LoadUsersEmails();
                StringBuilder sb = new StringBuilder();

                for (int i = 0; i < sc.Count; i++)
                {
                    string[] valuetext = sc[i].Split(':');
                    if (valuetext.Length > 1)
                    {
                        sb.Append(sc[i] + ";");
                        lbUsersMails.Items.Add(new ListItem(valuetext[1], valuetext[0]));
                    }
                }
                hdnlbUsersMails.Value = sb.ToString();
                hdnlbSelectedUsersMails.Value = String.Empty;
            }

            lbUsersMails.Items.Clear(); lbSelectedUsersMails.Items.Clear();

            string[] s = hdnlbUsersMails.Value.Split(';');
            for (int i = 0; i < s.Length; i++)
                if (s[i] != String.Empty)
                {
                    string[] valuetext = s[i].Split(':');
                    if (valuetext.Length > 1)
                        lbUsersMails.Items.Add(new ListItem(valuetext[1], valuetext[0]));
                }

            s = hdnlbSelectedUsersMails.Value.Split(';');
            for (int i = 0; i < s.Length; i++)
                if (s[i] != String.Empty)
                {
                    string[] valuetext = s[i].Split(':');
                    if (valuetext.Length > 1)
                        lbSelectedUsersMails.Items.Add(new ListItem(valuetext[1], valuetext[0]));
                }

        }

        private StringCollection GetSelectedMailsList()
        {
            StringCollection sc = new StringCollection();

            string[] s = hdnlbSelectedMails.Value.Split(';');

            for (int i = 0; i < s.Length; i++)
            {
                string[] valuetext = s[i].Split(':');

                int emailID = 0;
                try
                {
                    emailID = int.Parse(valuetext[0]);
                }
                catch { }

                if (emailID != 0)
                {
                    string m = LoadEmail(emailID);
                    if (m != "") sc.Add(m);
                }
            }
            return sc;
        }

		#endregion

		#region DB

        private string LoadEmail(int emailID)
        {
            string mail = String.Empty;
            SqlDataReader reader = null;
            try
            {
                reader = EmailsData.SelectEmail(emailID);
                if (reader.Read())
                {

                    mail = reader.IsDBNull(2) ? String.Empty : reader.GetString(2);

                }
                else return String.Empty;
            }
            catch (Exception ex)
            {
                log.Error(ex);
                return String.Empty;
            }
            finally
            {
                if (reader != null) reader.Close();
            }
            return mail;
        }

        private StringCollection LoadUsersEmails()
        {
            StringCollection sc = new StringCollection();

            SqlDataReader reader = null;

            try
            {
                reader = UsersData.SelectUsersEmails();
                while (reader.Read())
                {
                    string email = reader.GetString(1);
                    string emailName = reader.GetString(0);
                    sc.Add(String.Concat(email, ":", emailName));
                }
            }
            catch (Exception ex)
            {
                log.Error(ex);
            }
            finally
            {
                if (reader != null) reader.Close();
            }

            return sc;
        }

        private StringCollection LoadEmails()
        {
            StringCollection sc = new StringCollection();

            SqlDataReader reader = null;

            try
            {
                reader = ReportsData.SelectAllEmails(false);

                while (reader.Read())
                {
                    string email = reader.GetString(1);
                    string emailName = reader.GetString(0);
                    sc.Add(String.Concat(email, ":", emailName));
                }
            }
            catch (Exception ex)
            {
                log.Error(ex);
            }
            finally
            {
                if (reader != null) reader.Close();
            }

            return sc;
        }

        private bool LoadProjects()
        {

            int projectStatus = int.Parse(ddlProjectStatus.SelectedValue);

            SqlDataReader reader = null;

            try
            {
                //reader = ProjectsData.SelectProjectNamesClear(true);
                reader = ProjectsData.SelectProjectNames(projectStatus);

                ddlProject.DataSource = reader;
                ddlProject.DataValueField = "ProjectID";
                ddlProject.DataTextField = "ProjectName";
                ddlProject.DataBind();
                ddlProject.Items.Insert(0, new ListItem(String.Concat("<", Resource.ResourceManager["reports_ddlAllProjects"], ">"), "0"));

                //	ddlProject.Items.Insert(0, new ListItem("<"+Resource.ResourceManager["reports_ddlAllProjects"]+">", "0"));
                //	ddlProject.SelectedValue = "0";
            }
            catch (Exception ex)
            {
                log.Error(ex);
                return false;
            }

            finally
            {
                if (reader != null) reader.Close();
            }
            return true;
        }

        private bool LoadUsers()
        {
            SqlDataReader reader = null;
            try
            {
                reader = UsersData.SelectUserNames(true, true);
                ddlUsers.DataSource = reader;
                ddlUsers.DataValueField = "UserID";
                ddlUsers.DataTextField = "FullName";
                ddlUsers.DataBind();

                ddlUsers.Items.Insert(0, new ListItem("<" + Resource.ResourceManager["reports_ddlAllUsers"] + ">", "0"));
                ddlUsers.Items.Insert(0, new ListItem("<" + Resource.ResourceManager["reports_SelectUser"] + ">", "-1"));
                ddlUsers.SelectedValue = "-1";
            }
            catch (Exception ex)
            {
                log.Error(ex);
                return false;
            }
            finally
            {
                if (reader != null) reader.Close();
            }

            return true;
        }

		#endregion

		#region Util

        private void BindList()
        {
            SqlDataReader reader = null;

            try
            {
                reader = EmailsData.SelectEmails(1);

                if (reader != null)
                {
                    lsMails.DataSource = reader;
                    lsMails.DataTextField = "EmailName";
                    lsMails.DataValueField = "EmailID";
                    lsMails.DataBind();
                }
            }
            catch (Exception ex)
            {
                log.Error(ex);
            }

        }

        public ArrayList AddSelectedListBoxValues(ListBox lsFrom, ListBox lsTo)
        {
            ArrayList al = new ArrayList();
            for (int i = 0; i < lsFrom.Items.Count; i++)
                if (lsFrom.Items[i].Selected == true)
                {
                    lsTo.Items.Add(lsFrom.Items[i]);
                    al.Add(lsFrom.Items[i]);
                }
            for (int i = 0; i < al.Count; i++)
                lsFrom.Items.Remove((ListItem)al[i]);
            lsFrom.SelectedIndex = -1;
            lsTo.SelectedIndex = -1;
            return al;
        }

        private void SetErrorMessages()
        {
            hdnErrorMessages.Value = String.Concat(Resource.ResourceManager["reports_ErrorProjectNotSelected"], ";",
                                        Resource.ResourceManager["reports_ErrorUserNotSelected"]);
        }

		#endregion

		#region Menu

//		private void CreateMenu()
//		{
//			UserControls.MenuTable menu = new UserControls.MenuTable();
//			menu.ID = "MenuTable";
//
//			// 'Links' group
//			ArrayList menuItems = new ArrayList();
//
//			menuItems.Add(new MenuItemInfo(Resource.ResourceManager["menuItem_Hours"], "Hours.aspx", true));
//			menuItems.Add(new MenuItemInfo(Resource.ResourceManager["menuItem_Projects"], "Projects.aspx"));
//			if (LoggedUser.IsLeader || LoggedUser.IsAssistant) 
//			{
//				menuItems.Add(new MenuItemInfo(Resource.ResourceManager["menuItem_Clients"], "Clients.aspx"));
//								menuItems.Add(new MenuItemInfo(Resource.ResourceManager["menuItem_Subcontracters"], "Subcontracters.aspx"));
//				if (LoggedUser.IsLeader)
//				menuItems.Add(new MenuItemInfo(Resource.ResourceManager["menuItem_Users"], "Users.aspx"));
//			}
//			
//			menu.AddMenuGroup(Resource.ResourceManager["menuHeader_Links"], 20, menuItems);
//			
//			// 'New' group
//			menuItems = new ArrayList();
//			menuItems.Add(new MenuItemInfo(Resource.ResourceManager["menuItem_NewProject"], "EditProject.aspx"));
//			if (LoggedUser.IsLeader || LoggedUser.IsAssistant)
//			{
//				menuItems.Add(new MenuItemInfo(Resource.ResourceManager["menuItem_NewClient"], "EditClient.aspx"));
//				if (LoggedUser.IsLeader)
//				menuItems.Add(new MenuItemInfo(Resource.ResourceManager["menuItem_NewUser"], "EditUser.aspx"));
//			}
//			menuItems.Add(new MenuItemInfo(Resource.ResourceManager["menuItem_NewEmail"], "EditEmail.aspx"));
//			menuItems.Add(new MenuItemInfo(Resource.ResourceManager["newcontracter_PageTitle"], "EditSubContracter.aspx"));
//
//	
//			menu.AddMenuGroup(Resource.ResourceManager["menuHeader_New"], 30, menuItems);
//
//			//Reports
//			menuItems = new ArrayList();
//			menuItems.Add(new MenuItemInfo(Resource.ResourceManager["menuItem_WorkTimeReport"], "Reports/WorkTimes.aspx"));
//			menuItems.Add(new MenuItemInfo(Resource.ResourceManager["menuItem_MinutesReport"], "Reports/Minutes.aspx"));
//			if (LoggedUser.IsLeader)
//			{
//				menuItems.Add(new MenuItemInfo(Resource.ResourceManager["menuItem_NotEnteredWorkTime"], "Reports/EmptyHours.aspx"));
//				menuItems.Add(new MenuItemInfo(Resource.ResourceManager["menuItem_Calendar"], "reports/Calendar.aspx"));
//			}
//
//			menu.AddMenuGroup(Resource.ResourceManager["menuHeader_Reports"], 30, menuItems);
//
//			menuItems = new ArrayList();
//			menuItems.Add(new MenuItemInfo(Resource.ResourceManager["menuItem_Help"], SessionManager.GetHelpLink(),true,Pages.MailForm));
//			menu.AddMenuGroup("", 10, menuItems);
//
//            menuHolder.Controls.Add(menu);
//		}

		#endregion

        private void ddlProjectStatus_SelectedIndexChanged(object sender, System.EventArgs e)
        {
            LoadProjects();
        }

	}
}
