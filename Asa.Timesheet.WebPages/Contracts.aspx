<%@ Page language="c#" Codebehind="Contracts.aspx.cs" AutoEventWireup="false" Inherits="Asa.Timesheet.WebPages.Contracts" %>
<%@ Register TagPrefix="uc1" TagName="PageHeader" Src="UserControls/PageHeader.ascx" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
	<HEAD>
		<title>Contracts</title>
		<meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
		<meta content="C#" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<LINK href="styles/timesheet.css" type="text/css" rel="stylesheet">
	</HEAD>
	<body bottomMargin="0" leftMargin="0" topMargin="0" rightMargin="0">
		<form id="Form1" method="post" runat="server">
			<table height="100%" cellSpacing="0" cellPadding="0" width="100%" border="0">
				<tr height="1">
					<td>
						<uc1:PageHeader id="header" runat="server"></uc1:PageHeader>
					</td>
				</tr>
				<TR height="1">
					<td noWrap background="images/line.gif" height="1"></td>
				</TR>
				<tr>
					<td>
						<table height="100%" cellSpacing="0" cellPadding="0" width="100%" border="0">
							<tr>
								<td style="BACKGROUND-IMAGE: url(images/left110.gif)" vAlign="top" noWrap width="110">
									<asp:PlaceHolder id="menuHolder" runat="server"></asp:PlaceHolder>
								</td>
								<td noWrap width="1" bgColor="lightgrey"></td>
								<td style="PADDING-RIGHT: 5px; PADDING-LEFT: 5px; PADDING-TOP: 3px" vAlign="top" noWrap><br>
									<asp:label id="lblError" runat="server" ForeColor="Red" EnableViewState="False" CssClass="ErrorLabel"></asp:label><asp:label id="lblInfo" runat="server" EnableViewState="False" CssClass="InfoLabel"></asp:label><br>
									<br>
									<asp:panel id="grid" style="BORDER-RIGHT: #d2b48c 2px solid; PADDING-RIGHT: 2px; BORDER-TOP: #d2b48c 2px solid; OVERFLOW-Y: scroll; PADDING-LEFT: 2px; OVERFLOW-X: auto; BORDER-LEFT: #d2b48c 2px solid; PADDING-TOP: 2px; BORDER-BOTTOM: #d2b48c 2px solid"
										runat="server" Height="450px" Width="100%">
										<TABLE id="Table3" cellSpacing="0" cellPadding="0" width="880" border="0">
											<TR>
												<TD style="WIDTH: 205px">
													<asp:label id="lbScanDocNew" runat="server" CssClass="enterDataLabel" Width="100%">Нов договор:</asp:label></TD>
												<TD><INPUT id="FileCtrl" type="file" name="File1" runat="server">
													<asp:button id="btnScanDocAdd" runat="server" CssClass="ActionButton" Text="Добави"></asp:button></TD>
											</TR>
											<TR vAlign="top">
												<TD style="WIDTH: 205px" vAlign="middle">
													<asp:label id="Label6" runat="server" CssClass="enterDataLabel" Width="100%">Сканирани договори:</asp:label></TD>
												<TD>
													<asp:datalist id="dlDocs" runat="server" RepeatDirection="Horizontal">
														<ItemTemplate>
															<asp:HyperLink id="btnScan" runat="server" ToolTip="Сканиран документ" NavigateUrl='<%# GetURL((int)DataBinder.Eval(Container, "DataItem.ProjectContractID"))%>' Target=_blank>
																<img border="0" src="images/pdf.gif" /></asp:HyperLink>
															<br />
															<asp:ImageButton id="btnDel" runat="server" ToolTip="Изтрий" ImageUrl="images/delete.gif" Visible='<%# GetVis()%>' >
															</asp:ImageButton>
														</ItemTemplate>
													</asp:datalist></TD>
											</TR>
										</TABLE>
									</asp:panel>
									<TABLE id="Table4" style="BORDER-RIGHT: tan thin solid; BORDER-TOP: tan thin solid; BORDER-LEFT: tan thin solid; BORDER-BOTTOM: tan thin solid"
										cellSpacing="0" cellPadding="4" width="100%" border="0">
										<TR>
											<TD>
												<asp:button id="btnProject" runat="server" CssClass="ActionButton" Width="216px" Text="Връщане към проекта"></asp:button></TD>
										</TR>
									</TABLE>
								</td>
							</tr>
						</table>
					</td>
				</tr>
			</table>
		</form>
	</body>
</HTML>
