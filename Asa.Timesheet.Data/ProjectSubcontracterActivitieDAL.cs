using System;
using System.Data;
using System.Data.SqlClient; 
using System.Collections;
using Microsoft.ApplicationBlocks.Data;

namespace Asa.Timesheet.Data
{
	public class ProjectSubcontracterActivitieDAL
	{
		/// <summary>
		/// Loads a ProjectSubcontracterActivitieData object from the database using the given ProjectSubcontracterActivityID
		/// </summary>
		/// <param name="projectSubcontracterActivityID">The primary key value</param>
		/// <returns>A ProjectSubcontracterActivitieData object</returns>
		public static ProjectSubcontracterActivitieData Load(int projectSubcontracterActivityID)
		{
			SqlParameter[] parameterValues = new SqlParameter[] { new SqlParameter("@ProjectSubcontracterActivityID", projectSubcontracterActivityID) };
			using (SqlDataReader reader = SqlHelper.ExecuteReader(CommonDAL.ConnectionString, CommandType.StoredProcedure, "ProjectSubcontracterActivitiesSelProc", parameterValues))
			{
				if (reader.Read())
				{
					ProjectSubcontracterActivitieData result = new ProjectSubcontracterActivitieData();
					LoadFromReader(reader,result);
					return result;
				}
				else
					return null;
			}
		}

		/// <summary>
		/// Loads a ProjectSubcontracterActivitieData object from the database using the given where clause
		/// </summary>
		/// <param name="whereClause">The filter expression for the query</param>
		/// <returns>A ProjectSubcontracterActivitieData object</returns>
		public static ProjectSubcontracterActivitieData Load(string whereClause)
		{
			SqlParameter[] parameterValues = new SqlParameter[] { new SqlParameter("@WhereClause", whereClause) };
			using (SqlDataReader reader = SqlHelper.ExecuteReader(CommonDAL.ConnectionString, CommandType.StoredProcedure, "ProjectSubcontracterActivitiesListProc", parameterValues))
			{
				if (reader.Read())
				{
					ProjectSubcontracterActivitieData result = new ProjectSubcontracterActivitieData();
					LoadFromReader(reader,result);
					return result;
				}
				else
					return null;
			}
		}
		public static int Save( ProjectSubcontracterActivitieData source)
		{
			if (source.ProjectSubcontracterActivityID == -1)
				return Insert(source);
			else
				return Update(source);	
		}

		public static void Delete( ProjectSubcontracterActivitieData source)
		{
			Delete(source.ProjectSubcontracterActivityID);
		}
		/// <summary>
		/// Loads a collection of ProjectSubcontracterActivitieData objects from the database.
		/// </summary>
		/// <returns>A collection containing all of the ProjectSubcontracterActivitieData objects in the database.</returns>
		public static ProjectSubcontracterActivitiesVector LoadCollection()
		{
			SqlParameter[] parms = new SqlParameter[] {};
			return LoadCollection("ProjectSubcontracterActivitiesListProc", parms);
		}
	  
		/// <summary>
		/// Saves a collection of ProjectSubcontracterActivitieData objects to the database.
		/// </summary>
		/// <returns>A collection containing all of the ProjectSubcontracterActivitieData objects in the database.</returns>
		public static  void  SaveCollection(ProjectSubcontracterActivitiesVector col,string spName, SqlParameter[] parms)
		{
			ProjectSubcontracterActivitiesVector colOld = LoadCollection(spName,parms);
			UpdateCollection(colOld,col);
		}
		public static  void  SaveCollection(ProjectSubcontracterActivitiesVector col)
		{
			ProjectSubcontracterActivitiesVector colOld = LoadCollection();
			UpdateCollection(colOld,col);
		}
		public static  void  SaveCollection(ProjectSubcontracterActivitiesVector col, string whereClause )
		{
			ProjectSubcontracterActivitiesVector colOld = LoadCollection(whereClause);
			UpdateCollection(colOld,col);
		}
		/// <summary>
		/// Loads a collection of ProjectSubcontracterActivitieData objects from the database.
		/// </summary>
		/// <returns>A collection containing all of the ProjectSubcontracterActivitieData objects in the database.</returns>
		public static ProjectSubcontracterActivitiesVector LoadCollection(string whereClause)
		{
			SqlParameter[] parms = new SqlParameter[] { new SqlParameter("@WhereClause", whereClause) };
			return LoadCollection("ProjectSubcontracterActivitiesListProc", parms);
		}

		/// <summary>
		/// Loads a collection of ProjectSubcontracterActivitieData objects from the database.
		/// </summary>
		/// <returns>A collection containing all of the ProjectSubcontracterActivitieData objects in the database.</returns>
		public static ProjectSubcontracterActivitiesVector LoadCollection(string spName, SqlParameter[] parms)
		{
			ProjectSubcontracterActivitiesVector result = new ProjectSubcontracterActivitiesVector();
			using (SqlDataReader reader = SqlHelper.ExecuteReader(CommonDAL.ConnectionString, CommandType.StoredProcedure, spName, parms))
			{
				while (reader.Read())
				{
					ProjectSubcontracterActivitieData tmp = new ProjectSubcontracterActivitieData();
					LoadFromReader(reader, tmp);
					result.Add(tmp);
				}
			}
			return result;
		}
		/// <summary>
		/// Deletes a ProjectSubcontracterActivitieData object from the database.
		/// </summary>
		/// <param name="projectSubcontracterActivityID">The primary key value</param>
		public static void Delete(int projectSubcontracterActivityID)
		{
			SqlParameter[] parameterValues = new SqlParameter[] { new SqlParameter("@ProjectSubcontracterActivityID", projectSubcontracterActivityID) };
			SqlHelper.ExecuteNonQuery(CommonDAL.ConnectionString, CommandType.StoredProcedure, "ProjectSubcontracterActivitiesDelProc", parameterValues);
		}

		public static void LoadFromReader(SqlDataReader reader, ProjectSubcontracterActivitieData target)
		{
			if (reader != null && !reader.IsClosed)
			{
				//target.Key = reader.GetInt32(0);
				if (!reader.IsDBNull(0)) target.ProjectSubcontracterActivityID = reader.GetInt32(0);
				if (!reader.IsDBNull(1)) target.ProjectSubcontracterID = reader.GetInt32(1);
				if (!reader.IsDBNull(2)) target.SubactivityID = reader.GetInt32(2);
				if (!reader.IsDBNull(3)) target.Done = reader.GetBoolean(3);
				if (!reader.IsDBNull(4)) target.Subactivity = reader.GetString(4);
				
			}
		}
		
	  
		public static int Insert( ProjectSubcontracterActivitieData source)
		{
			SqlParameter[] parameterValues = GetInsertParameterValues(source);
			SqlHelper.ExecuteNonQuery(CommonDAL.ConnectionString, CommandType.StoredProcedure, "ProjectSubcontracterActivitiesInsProc", parameterValues);
			source.ProjectSubcontracterActivityID = (int) parameterValues[0].Value;
			return source.ProjectSubcontracterActivityID;
		}

		public static int Update( ProjectSubcontracterActivitieData source)
		{
			SqlParameter[] parameterValues = GetUpdateParameterValues(source);
			SqlHelper.ExecuteNonQuery(CommonDAL.ConnectionString, CommandType.StoredProcedure,
				"ProjectSubcontracterActivitiesUpdProc", parameterValues);
			return source.ProjectSubcontracterActivityID;
		}
		private static void UpdateCollection(ProjectSubcontracterActivitiesVector colOld,ProjectSubcontracterActivitiesVector col)
		{
			// Delete all old items
			foreach( ProjectSubcontracterActivitieData itemOld in colOld)
			{
				if(!ExistsKey(itemOld.ProjectSubcontracterActivityID, col))
					Delete(itemOld.ProjectSubcontracterActivityID);
			}
			foreach( ProjectSubcontracterActivitieData item in col)
			{
				if(item.ProjectSubcontracterActivityID <= -1)
					Insert(item);
				else
					Update(item);
			}
		}
		private static bool ExistsKey(int Key, ProjectSubcontracterActivitiesVector col)
		{
			foreach( ProjectSubcontracterActivitieData item in col)
				if(item.ProjectSubcontracterActivityID==Key)
					return true;
			return false;
		}

		private static SqlParameter GetSqlParameter(string name, ParameterDirection direction, object value)
		{
			SqlParameter p = new SqlParameter(name, value);
			p.Direction = direction;
			return p;
		}

		private static SqlParameter[] GetInsertParameterValues( ProjectSubcontracterActivitieData source)
		{
			SqlParameter[] prams = new SqlParameter[4];
			prams[0] = GetSqlParameter("@ProjectSubcontracterActivityID", ParameterDirection.Output, source.ProjectSubcontracterActivityID);
			prams[1] = GetSqlParameter("@ProjectSubcontracterID", ParameterDirection.Input, source.ProjectSubcontracterID);
			prams[2] = GetSqlParameter("@SubactivityID", ParameterDirection.Input, source.SubactivityID);
			prams[3] = GetSqlParameter("@Done", ParameterDirection.Input, source.Done);
			return prams;
		}

		private static SqlParameter[] GetUpdateParameterValues( ProjectSubcontracterActivitieData source)
		{
			return new SqlParameter[] {
										  new SqlParameter("@ProjectSubcontracterActivityID", source.ProjectSubcontracterActivityID),
										  new SqlParameter("@ProjectSubcontracterID", source.ProjectSubcontracterID),
										  new SqlParameter("@SubactivityID", source.SubactivityID),
										  new SqlParameter("@Done", source.Done)
									  };
		}
	}
}   

