using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Data.SqlClient;
using Asa.Timesheet.Data;
using Asa.Timesheet.Data.Entities;
using log4net;

namespace Asa.Timesheet.WebPages
{
	/// <summary>
	/// Summary description for Clients.
	/// </summary>
	public class Clients : TimesheetPageBase 
	{
		#region Web controls

		protected System.Web.UI.WebControls.Label Label1;
		protected System.Web.UI.WebControls.Panel grid;
		protected System.Web.UI.WebControls.DataGrid grdClients;
		protected System.Web.UI.WebControls.Button btnNewClient;
		protected System.Web.UI.HtmlControls.HtmlTable tblCalendar;
		protected System.Web.UI.WebControls.Label lblInfo;
		protected System.Web.UI.WebControls.Label lblError;
		protected System.Web.UI.WebControls.PlaceHolder menuHolder;
		protected Asa.Timesheet.WebPages.UserControls.PageHeader header;

		#endregion
		protected Telerik.WebControls.RadGrid gridCalls;
		protected System.Web.UI.WebControls.Button btnExport;
		protected System.Web.UI.WebControls.Label lblName;
		protected System.Web.UI.WebControls.DropDownList ddlProjectsStatus;
		protected System.Web.UI.WebControls.Label Label2;
		protected System.Web.UI.WebControls.DropDownList ddlBuildingTypes;
		protected System.Web.UI.WebControls.Label Label3;
		protected System.Web.UI.WebControls.DropDownList ddlProject;
		protected System.Web.UI.WebControls.Button btnShow;
		protected System.Web.UI.WebControls.DataGrid grdSub;
		protected System.Web.UI.WebControls.Label Label4;
	
		private enum GridColumns
		{
			Number=0,
			Name = 1,
			Phone,
			Fax,
			Email,
			City,
			Address,
			
			ClientTypeID,
			EditItem,
			DeleteItem
		}
		private static readonly ILog log = LogManager.GetLogger(typeof(Clients));

        private void Page_Load(object sender, System.EventArgs e)
        {
            //if (!(LoggedUser.IsLeader || LoggedUser.IsAssistant))
            //			{
            //				ErrorRedirect(Resource.ResourceManager["noRightsAlert"]);
            //			}

            UIHelpers.CreateMenu(menuHolder, LoggedUser);
            if (UIHelpers.GetIDParam() != (int)ClientTypes.All)
            {
                grdSub.Visible = lblName.Visible = false;
            }
            if (!this.IsPostBack)
            {
                switch (UIHelpers.GetIDParam())
                {
                    case ((int)ClientTypes.Client):
                        {
                            header.PageTitle = Resource.ResourceManager["clients_PageTitle"];
                            btnNewClient.Text = Resource.ResourceManager["clients_BtnNewClient"];
                            break;
                        }
                    case ((int)ClientTypes.Builder):
                        {
                            header.PageTitle = Resource.ResourceManager["builders_PageTitle"];
                            btnNewClient.Text = Resource.ResourceManager["builders_BtnNewClient"];
                            break;
                        }
                    case ((int)ClientTypes.Supervisors):
                        {
                            header.PageTitle = Resource.ResourceManager["nazdor_PageTitle"];
                            btnNewClient.Text = Resource.ResourceManager["nazdor_BtnNewClient"];
                            break;
                        }
                    case ((int)ClientTypes.Producer):
                        {
                            header.PageTitle = Resource.ResourceManager["producers_PageTitle"];
                            btnNewClient.Text = Resource.ResourceManager["producers_BtnNewClient"];
                            break;
                        }
                    case ((int)ClientTypes.ProjectManagers):
                        {
                            header.PageTitle = Resource.ResourceManager["projectManagers_PageTitle"];
                            btnNewClient.Text = Resource.ResourceManager["projectManagers_BtnNewClient"];
                            break;
                        }
                    case ((int)ClientTypes.Brokers):
                        {
                            header.PageTitle = Resource.ResourceManager["brokers_PageTitle"];
                            btnNewClient.Text = Resource.ResourceManager["brokers_BtnNewClient"];
                            break;
                        }
                    case ((int)ClientTypes.All):
                        {
                            header.PageTitle = Resource.ResourceManager["contacts_PageTitle"];
                            btnNewClient.Text = Resource.ResourceManager["contacts_BtnNewClient"];
                            break;
                        }
                    default:
                        {
                            header.PageTitle = Resource.ResourceManager["contacts_PageTitle"];
                            btnNewClient.Text = Resource.ResourceManager["contacts_BtnNewClient"];
                            break;
                        }
                }

                header.UserName = this.LoggedUser.UserName;
                //				if(UIHelpers.GetIDParam() == (int)ClientTypes.Client)
                //					btnNewClient.Text = Resource.ResourceManager["clients_BtnNewClient"];
                //				else
                //					btnNewClient.Text = Resource.ResourceManager["builders_BtnNewClient"];
                SortOrder = 1;

                if (Request.Params["all"] == "1")
                {
                    UIHelpers.LoadProjectStatus(ddlProjectsStatus, (int)ProjectsData.ProjectsByStatus.AllProjects);
                    UIHelpers.LoadBuildingTypes(ddlBuildingTypes, ProjectsData.ProjectsByStatus.AllProjects);
                }
                else
                {
                    UIHelpers.LoadProjectStatus(ddlProjectsStatus, (int)ProjectsData.ProjectsByStatus.Active);
                    UIHelpers.LoadBuildingTypes(ddlBuildingTypes, ProjectsData.ProjectsByStatus.Active);
                }
                UIHelpers.LoadProjects(ddlProject, ddlProjectsStatus, ddlBuildingTypes);
                BindGrid();
            }
            if (!( LoggedUser.HasPaymentRights || LoggedUser.IsSecretary))
            {
                btnNewClient.Visible = false;
            }
        }


		#region Web Form Designer generated code
        override protected void OnInit(EventArgs e)
        {
            //
            // CODEGEN: This call is required by the ASP.NET Web Form Designer.
            //
            InitializeComponent();
            base.OnInit(e);
        }
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
        private void InitializeComponent()
        {
            this.ddlProjectsStatus.SelectedIndexChanged += new System.EventHandler(this.ddlProjectsStatus_SelectedIndexChanged);
            this.ddlBuildingTypes.SelectedIndexChanged += new System.EventHandler(this.ddlBuildingTypes_SelectedIndexChanged);
            this.btnShow.Click += new System.EventHandler(this.btnShow_Click);
            this.grdClients.ItemCreated += new System.Web.UI.WebControls.DataGridItemEventHandler(this.grdClients_ItemCreated);
            this.grdClients.ItemCommand += new System.Web.UI.WebControls.DataGridCommandEventHandler(this.grdClients_ItemCommand);
            this.grdClients.ItemDataBound += new System.Web.UI.WebControls.DataGridItemEventHandler(this.grdClients_ItemDataBound);
            this.btnNewClient.Click += new System.EventHandler(this.btnNewClient_Click);
            this.btnExport.Click += new System.EventHandler(this.btnExport_Click);
            this.Load += new System.EventHandler(this.Page_Load);

        }
		#endregion
	
		#region Event handlers

        private void grdClients_ItemCommand(object source, System.Web.UI.WebControls.DataGridCommandEventArgs e)
        {
            if (e.CommandName == "Sort")
            {
                int sortOrder = SortOrder;
                switch ((string)e.CommandArgument)
                {
                    case "Name": if (sortOrder == 1) sortOrder = -1; else sortOrder = 1;
                        SortOrder = sortOrder;
                        BindGrid(); break;
                    case "City": if (sortOrder == 2) sortOrder = -2; else sortOrder = 2;
                        SortOrder = sortOrder;
                        BindGrid(); break;
                    case "Address": if (sortOrder == 3) sortOrder = -3; else sortOrder = 3;
                        SortOrder = sortOrder;
                        BindGrid(); break;
                    case "EMail": if (sortOrder == 4) sortOrder = -4; else sortOrder = 4;
                        SortOrder = sortOrder;
                        BindGrid(); break;
                    case "ClientTypeID": if (sortOrder == 5) sortOrder = -5; else sortOrder = 5;
                        SortOrder = sortOrder;
                        BindGrid(); break;
                }

                return;
            }
            else if (e.CommandName == "Edit")
            {
                string sClientID = ((DataGrid)source).DataKeys[e.Item.ItemIndex].ToString();
                Response.Redirect("EditClient.aspx" + "?id=" + UIHelpers.GetIDParam().ToString() + "&cid=" + sClientID);
                return;
            }
            ImageButton ib = (ImageButton)e.CommandSource;
            string s = ib.ID;
            switch (ib.ID)
            {
                case "ibDeleteItem": int clientID = (int)((DataGrid)source).DataKeys[e.Item.ItemIndex];
                    if (!DeleteClient(clientID))
                    {
                        lblError.Text = Resource.ResourceManager["editClient_ErrorDelete"];
                        return;
                    }
                    else
                        log.Info(string.Format("Client {0} has been DELETED by {1}", clientID, string.Concat(LoggedUser.FullName, " ( userID ", LoggedUser.UserID, " )")));
                    BindGrid();
                    break;
                case "ibEditItem": string sClientID = ((DataGrid)source).DataKeys[e.Item.ItemIndex].ToString();
                    Response.Redirect("EditClient.aspx" + "?id=" + UIHelpers.GetIDParam() + "&cid=" + sClientID);
                    break;
            }
        }

        private void grdClients_ItemCreated(object sender, System.Web.UI.WebControls.DataGridItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Header)
            {
                string imgUrl = "";
                int sortOrder = SortOrder;

                if (sortOrder != 0)
                {
                    imgUrl = (sortOrder > 0) ? "images/sup.gif" : "images/sdown.gif";
                    Label sep = new Label();
                    sep.Width = 2;
                    e.Item.Cells[Math.Abs(sortOrder)].Controls.Add(sep);
                    ImageButton ib = new ImageButton();
                    ib.Height = 11; ib.Width = 11; ib.ImageUrl = imgUrl;
                    e.Item.Cells[Math.Abs(sortOrder)].Controls.Add(ib);
                }
            }
        }

        private void btnNewClient_Click(object sender, System.EventArgs e)
        {
            Response.Redirect("EditClient.aspx" + "?id=" + UIHelpers.GetIDParam().ToString());
        }

        private void grdClients_ItemDataBound(object sender, System.Web.UI.WebControls.DataGridItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Item
                || e.Item.ItemType == ListItemType.AlternatingItem)
            {

                e.Item.Cells[(int)GridColumns.ClientTypeID].Text = SetClientType(e.Item.Cells[(int)GridColumns.ClientTypeID].Text);

            }
        }
		#endregion

		#region Database 

        private void BindGrid()
        {
            SqlDataReader reader = null;

            try
            {
                int clientType = UIHelpers.GetIDParam();
                if (clientType == (int)ClientTypes.All)
                    clientType = -1;
                reader = ClientsData.SelectClientsFromProjects(SortOrder, clientType, UIHelpers.ToInt(ddlProject.SelectedValue), UIHelpers.ToInt(ddlBuildingTypes.SelectedValue), UIHelpers.ToInt(ddlProjectsStatus.SelectedValue));

                if (reader != null)
                {
                    grdClients.DataSource = reader;
                    grdClients.DataKeyField = "ClientID";
                    grdClients.DataBind();
                    if (!LoggedUser.IsLeader)
                        grdClients.Columns[(int)GridColumns.DeleteItem].Visible = false;
                }
                //				switch(UIHelpers.GetIDParam())
                //				{
                //					case ((int)ClientTypes.Client):
                //					{
                //						break;
                //					}
                //					case ((int)ClientTypes.Builder):
                //					{
                //						grdClients.Columns[(int)GridColumns.Address].Visible=false;
                //						grdClients.Columns[(int)GridColumns.City].Visible=false;
                //						break;
                //					}
                //					case ((int)ClientTypes.Producer):
                //					{
                //						grdClients.Columns[(int)GridColumns.Address].Visible=false;
                //						grdClients.Columns[(int)GridColumns.City].Visible=false;
                //						break;
                //					}
                //					case ((int)ClientTypes.All):
                //					{
                //						grdClients.Columns[(int)GridColumns.Address].Visible=false;
                //						grdClients.Columns[(int)GridColumns.City].Visible=false;
                //						break;
                //					}
                //					default:
                //					{
                //						grdClients.Columns[(int)GridColumns.Address].Visible=false;
                //						grdClients.Columns[(int)GridColumns.City].Visible=false;
                //						break;
                //					}
                //				}
                //				if(UIHelpers.GetIDParam() == (int)ClientTypes.Builder)
                //				{
                //					grdClients.Columns[(int)GridColumns.Address].Visible=false;
                //					grdClients.Columns[(int)GridColumns.City].Visible=false;
                //				}
            }
            catch (Exception ex)
            {
                log.Error(ex);
                lblError.Text = Resource.ResourceManager["clients_ErrorLoadClients"];
                return;
            }

            finally
            {
                if (reader != null) reader.Close();
            }

            try
            {
                SetConfirmDelete(grdClients, "ibDeleteItem", 1);
            }
            catch (Exception ex)
            {
                log.Error(ex);
            }
            if (UIHelpers.GetIDParam() == (int)ClientTypes.All)
                BindGridSubs();
            else
            {
                grdSub.Visible = lblName.Visible = false;
            }
        }
        private void BindGridSubs()
        {
            SqlDataReader reader = null;

            try
            {
                bool hpr = LoggedUser.HasPaymentRights;
                reader = SubprojectsUDL.SelectSubcontracters(-1, SortOrder, hpr, UIHelpers.ToInt(ddlProject.SelectedValue), false, true, UIHelpers.ToInt(ddlBuildingTypes.SelectedValue), UIHelpers.ToInt(ddlProjectsStatus.SelectedValue));
                if (reader != null)
                {
                    grdSub.DataSource = reader;
                    grdSub.DataKeyField = "SubcontracterID";
                    grdSub.DataBind();
                    if (!LoggedUser.HasPaymentRights)
                        grdSub.Columns[(int)GridColumns.DeleteItem].Visible = false;
                }
            }
            catch (Exception ex)
            {
                log.Error(ex);
                lblError.Text = Resource.ResourceManager["prsubcontracters_ErrorLoadProjects"];
                return;
            }

            finally
            {
                if (reader != null) reader.Close();
            }

            try
            {
                SetConfirmDelete(grdClients, "ibDeleteItem", 1);
            }
            catch (Exception ex)
            {
                log.Error(ex);
            }
        }
        private bool DeleteClient(int clientID)
        {
            try
            {
                ClientsData.InactiveClient(clientID);
            }
            catch (Exception ex)
            {
                log.Error(ex);
                return false;
            }

            return true;
        }
        private void btnExport_Click(object sender, System.EventArgs e)
        {
            this.EnableViewState = true;

            System.IO.StringWriter tw = new System.IO.StringWriter();
            System.Web.UI.HtmlTextWriter hw = new System.Web.UI.HtmlTextWriter(tw);
            SqlDataReader reader = null;

            try
            {
                int clientType = UIHelpers.GetIDParam();
                if (clientType == (int)ClientTypes.All)
                    clientType = -1;
                reader = ClientsData.SelectClientsFromProjects(SortOrder, clientType, UIHelpers.ToInt(ddlProject.SelectedValue), UIHelpers.ToInt(ddlBuildingTypes.SelectedValue), UIHelpers.ToInt(ddlProjectsStatus.SelectedValue));
                if (reader != null)
                {
                    gridCalls.DataSource = reader;
                    gridCalls.DataKeyField = "ClientID";


                    gridCalls.AllowSorting = false;

                    gridCalls.DataBind();
                    for (int i = 0; i < gridCalls.Columns.Count; i++)
                    {

                        gridCalls.Columns[i].SortExpression = null;
                    }
                }
                //				if(UIHelpers.GetIDParam() == (int)ClientTypes.Builder)
                //				{
                //					gridCalls.Columns[(int)GridColumns.Address].Visible=false;
                //					gridCalls.Columns[(int)GridColumns.City].Visible=false;
                //				}
            }

            finally
            {
                if (reader != null) reader.Close();
            }

            gridCalls.MasterTableView.ExportToExcel("Clients");
        }

        private string SetClientType(string clientTypeID)
        {
            int iClientTypeID = UIHelpers.ToInt(clientTypeID);
            switch (iClientTypeID)
            {
                case ((int)ClientTypes.Client): { return Resource.ResourceManager["ListItem_Client"]; }
                case ((int)ClientTypes.Builder): { return Resource.ResourceManager["ListItem_Builder"]; }
                case ((int)ClientTypes.Producer): { return Resource.ResourceManager["ListItem_Producer"]; }
                case ((int)ClientTypes.Supervisors): { return Resource.ResourceManager["ListItem_SuperVisor"]; }
                case ((int)ClientTypes.Brokers): { return Resource.ResourceManager["ListItem_Broker"]; }
                case ((int)ClientTypes.ProjectManagers): { return Resource.ResourceManager["ListItem_Manager"]; }
                default: { return ""; }
            }
        }

        private void ddlProjectsStatus_SelectedIndexChanged(object sender, System.EventArgs e)
        {
            UIHelpers.LoadProjects(ddlProject, ddlProjectsStatus, ddlBuildingTypes);
        }

        private void ddlBuildingTypes_SelectedIndexChanged(object sender, System.EventArgs e)
        {
            UIHelpers.LoadProjects(ddlProject, ddlProjectsStatus, ddlBuildingTypes);
        }

        private void btnShow_Click(object sender, System.EventArgs e)
        {
            BindGrid();
        }
		#endregion

		#region Menu

//		private void CreateMenu()
//		{
//			UserControls.MenuTable menu = new UserControls.MenuTable();
//			menu.ID = "MenuTable";
//
//			// 'Links' group
//			ArrayList menuItems = new ArrayList();
//
//			menuItems.Add(new MenuItemInfo(Resource.ResourceManager["menuItem_Hours"], "hours.aspx", true));
//			menuItems.Add(new MenuItemInfo(Resource.ResourceManager["menuItem_Projects"], "projects.aspx"));
//			menuItems.Add(new MenuItemInfo(Resource.ResourceManager["menuItem_Subcontracters"], "Subcontracters.aspx"));
//			
//			if (LoggedUser.IsLeader)
//				menuItems.Add(new MenuItemInfo(Resource.ResourceManager["menuItem_Users"], "Users.aspx"));
//			menuItems.Add(new MenuItemInfo(Resource.ResourceManager["menuItem_Emails"], "Emails.aspx"));
//
//			menu.AddMenuGroup(Resource.ResourceManager["menuHeader_Links"], 20, menuItems);
//			
//			// 'New' grop
//			menuItems = new ArrayList();
//			menuItems.Add(new MenuItemInfo(Resource.ResourceManager["menuItem_NewProject"], "editproject.aspx"));
//			menuItems.Add(new MenuItemInfo(Resource.ResourceManager["menuItem_NewClient"], "editclient.aspx"));
//			if (LoggedUser.IsLeader)
//			menuItems.Add(new MenuItemInfo(Resource.ResourceManager["menuItem_NewUser"], "EditUser.aspx"));
//			menuItems.Add(new MenuItemInfo(Resource.ResourceManager["menuItem_NewEmail"], "editemail.aspx"));
//			menuItems.Add(new MenuItemInfo(Resource.ResourceManager["newcontracter_PageTitle"], "EditSubContracter.aspx"));
//
//			menu.AddMenuGroup(Resource.ResourceManager["menuHeader_New"], 30, menuItems);
//
//			// 
//			menuItems = new ArrayList();
//			menuItems.Add(new MenuItemInfo(Resource.ResourceManager["menuItem_WorkTimeReport"], "reports/worktimes.aspx"));
//			menuItems.Add(new MenuItemInfo(Resource.ResourceManager["menuItem_MinutesReport"], "Reports/Minutes.aspx"));
//			if (LoggedUser.IsLeader || LoggedUser.IsAssistant)
//				menuItems.Add(new MenuItemInfo(Resource.ResourceManager["menuItem_MailReport"], "MailForm.aspx"));
//			if (LoggedUser.IsLeader)
//			{
//				menuItems.Add(new MenuItemInfo(Resource.ResourceManager["menuItem_NotEnteredWorkTime"], "reports/EmptyHours.aspx"));
//				menuItems.Add(new MenuItemInfo(Resource.ResourceManager["menuItem_Calendar"], "reports/Calendar.aspx"));
//
//			}
//			menu.AddMenuGroup(Resource.ResourceManager["menuHeader_Reports"], 30, menuItems);
//
//			menuItems = new ArrayList();
//			menuItems.Add(new MenuItemInfo(Resource.ResourceManager["menuItem_Help"], SessionManager.GetHelpLink(),true,GetPageFromRequest()));
//			menu.AddMenuGroup("", 10, menuItems);
//
//			menuHolder.Controls.Add(menu);
//		}

		#endregion

		
	}
}
