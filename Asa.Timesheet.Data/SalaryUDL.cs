using System;
using System.Data;
using System.Data.SqlClient; 
using System.Collections;
namespace Asa.Timesheet.Data
{
	/// <summary>
	/// Summary description for SalaryUDL.
	/// </summary>
	public class SalaryUDL
	{
		public static SalariesVector GetSalariesByUser(int UserID)
		{
			return SalarieDAL.LoadCollection("SalariesListProc",SQLParms.CreateSalariesListProc(UserID));
		}
		public static decimal GetSalaryByMonthYearType(SalariesVector sv, int Year, int Month, int Type)
		{
			foreach(SalarieData sd in sv)
			{
				if(sd.SalaryMonth==Month && sd.SalaryYear==Year && sd.SalaryTypeID==Type)
					return  sd.Salary;

			}
        
			return 0;
		}
		public static int GetSalaryIDByMonthYearType(SalariesVector sv, int Year, int Month, int Type)
		{
			foreach(SalarieData sd in sv)
			{
				if(sd.SalaryMonth==Month && sd.SalaryYear==Year && sd.SalaryTypeID ==Type)
					return  sd.SalaryID;

			}
			return -1;
		}
	}
}
