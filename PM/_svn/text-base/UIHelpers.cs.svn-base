using System;
using System.Web;
using System.Globalization;
using Asa.Timesheet.Data;
using Asa.Timesheet.Data.Entities;
using System.Data.SqlClient;
using log4net;
using log4net.Config;
using System.Web.UI.WebControls;
using System.Collections;
using System.Collections.Specialized;

namespace PM
{
	/// <summary>
	/// Summary description for UIHelpers.
	/// </summary>
	/// 
	
	public class UIHelpers
	{
		private const string _begin = "<IMG src='";
		private const string _end = "' border=0>";
		private const string _asc = "images/sup.gif";
		private const string _desc = "images/sdown.gif";
		private static readonly ILog log = LogManager.GetLogger(typeof(UIHelpers));
		private static readonly int _MAX_TEXTS=28;
		public static string  SetSort(DataGrid grd,string sCurrent, string sNew)
		{
			string sRetVal;
			bool bAsc=true;
			if(sCurrent!=null && sCurrent!="" && sCurrent.StartsWith(sNew) && !sCurrent.EndsWith("DESC"))
			{
				bAsc = false;
				sRetVal = sNew + " DESC";
			}
			else 
				sRetVal = sNew;
			SetIcon(grd,sNew,bAsc);
			return sRetVal;
		}
		public static void SetIcon(DataGrid grd,string sSort)
		{
			if(sSort!=null)
			{
				int n = sSort.IndexOf(" DESC");
				bool bAsc = n==-1;
				if(n!=-1)
					sSort = sSort.Substring(0,n).Trim();
				SetIcon(grd,sSort,bAsc);
			}
		}
		public static void SetIcon(DataGrid grd,string sSort, bool bAsc)
		{
			ResetPrevious(grd);
			int nColumn = GetColumn(grd,sSort);
			string sIcon = bAsc?_asc:_desc;
			grd.Columns[nColumn].HeaderText =string.Format("{0}{1}{2}{3}",grd.Columns[nColumn].HeaderText,_begin,sIcon,_end);
		}
		public static string GetIconString(string sSort,  bool bAsc)
		{
			string sIcon = bAsc?_asc:_desc;
			return string.Format("{0}{1}{2}{3}",sSort,_begin,sIcon,_end);
	
		}
		public static void ResetPrevious(DataGrid grd)
		{
			for(int i =0;i<grd.Columns.Count;i++)
			{
				string sHeader = grd.Columns[i].HeaderText;
				int n= sHeader.IndexOf(_begin);
				if(n!=-1)
				{
					grd.Columns[i].HeaderText = sHeader.Substring(0,n);
				}
			}
		}
		public static int GetColumn(DataGrid grd,string sSort)
		{
			for(int i =0;i<grd.Columns.Count;i++)
				if(grd.Columns[i].SortExpression==sSort)
					return i;
			return -1;
		}
		public static string CleanFolder(string s)
		{
			return s.Replace("\"","").Replace("'","").Replace("/","").Replace(@"\","").Replace("<","").Replace(">","").Replace(":","").Replace("|","").Replace("?","").Replace("*","").Trim();
		}
		public static int GetCIDParam()
		{
			int nID=-1;
			string sID= HttpContext.Current.Request.Params["cid"];
			if(sID!=null && sID!="")
			{
				nID= ToInt(sID);
			}
			return nID;
		}
		public static int GetIDParam()
		{
			int nID=-1;
			string sID= HttpContext.Current.Request.Params["id"];
			if(sID!=null && sID!="")
			{
				nID= ToInt(sID);
			}
			return nID;
		}
		public static int GetPIDParam()
		{
			int nID=-1;
			string sID= HttpContext.Current.Request.Params["pid"];
			if(sID!=null && sID!="")
			{
				nID= ToInt(sID);
			}
			return nID;
		}
		/// <summary>
		/// Converts to int without throwing excpetion if unsuccess
		/// </summary>
		/// <param name="sID">String to parse</param>
		/// <returns>The parsed integer or -1 if unsuccess</returns>
		public static int ToInt(string sID)
		{
			double resNum;
			if(!Double.TryParse(sID, NumberStyles.Integer,NumberFormatInfo.InvariantInfo,out resNum))
				return -1;
			return (int) resNum;
		}

		public static bool HasAccess(UserInfoPM loggedUser,int projectID)
		{
			int userID = loggedUser.UserID;
			PMProjectUserAccesssVector pmpav = PMProjectUserAccessDAL.LoadCollectionForUser(loggedUser.UserType,loggedUser.UserID,projectID);
			if(pmpav.Count>0)
			{
				return pmpav[0].HasAccess;
			}
			else
			{
				switch(loggedUser.UserType)
				{
					case((int)PMUserType.Client): 
					{
						ProjectClientsVector cvClients = ProjectClientDAL.LoadCollection("ProjectClientsSelByProjectProc",SQLParms.CreateProjectClientsSelByProjectProc(projectID,(int)ClientTypes.Client));
						foreach(ProjectClientData pcd in cvClients)
							if(pcd.ClientID==userID)
								return true;
						if(ProjectsData.SelectProjectClientID(projectID)==userID)
							return true;
						return false;
					}
					case((int)PMUserType.Builder): 
					{
						ProjectClientsVector cvClients = ProjectClientDAL.LoadCollection("ProjectClientsSelByProjectProc",SQLParms.CreateProjectClientsSelByProjectProc(projectID,(int)ClientTypes.Builder));
						foreach(ProjectClientData pcd in cvClients)
							if(pcd.ClientID==userID)
								return true;
						return false;
					}
					case((int)PMUserType.Subcontracter): 
					{
						ProjectSubcontractersVector psv = ProjectSubcontracterDAL.LoadCollection("ProjectSubcontractersListProc",SQLParms.CreateProjectSubcontractersListProc(projectID));
						foreach(ProjectSubcontracterData psd in psv)
							if(psd.SubcontracterID==userID)
								return true;
						return false;
					}
					default:return true;
				}
			}
		}
		public static bool HasAccessToCategory(UserInfoPM loggedUser,int CategoryID)
		{
			if(CategoryID == -1)
				return true;
			//ProjectCategoryData dat = ProjectCategoryDAL.Load(CategoryID);
			ProjectCategoriesVector pcv = ProjectCategoryDAL.LoadCollection(UIHelpers.GetPIDParam(),loggedUser.UserType,loggedUser.UserID);
			

			switch(loggedUser.UserType)
			{
				case ((int)PMUserType.Client):return (pcv.IndexOfCategory(CategoryID)!=-1);//dat.IsClientVisible;
				case ((int)PMUserType.Subcontracter):return true;//dat.IsSubcontractorVisible;
				case ((int)PMUserType.Builder):return (pcv.IndexOfCategory(CategoryID)!=-1);//dat.IsBuilderVisible;
				case ((int)PMUserType.PMUser):return (pcv.IndexOfCategory(CategoryID)!=-1);//dat.IsClientVisible;
				default:return true;	
			}
		}

		public static bool CheckForUniqueNameAccount(string newName,string oldName)
		{
			if(newName == oldName)
				return true;
			SqlDataReader reader = UsersData.SelectUsers(-1,1/*don't matter sortOrder*/);
			while(reader.Read())
			{
				string s = reader.GetString(6);
				if(s == newName)
					return false;
			}
			SubcontractersVector scv = SubcontracterDAL.LoadCollection();
			foreach(SubcontracterData scd in scv)
				if(scd.AccountName==newName)
					return false;
			ClientsVector cv = ClientDAL.LoadCollection(-1);
			foreach(ClientData cd in cv)
				if(cd.AccountName == newName)
					return false;
			return true;
		}
		public static SortedList GetAllPMUsers()
		{
			SortedList sc = new SortedList();
			SqlDataReader reader = UsersData.SelectUsers(-1,1/*don't matter sortOrder*/);
			while(reader.Read())
			{
				if(sc.IndexOfKey(reader.GetString(7))==-1 && reader.GetString(7)!="")
					sc.Add( reader.GetString(7),reader.GetString(7));
				
			}
			SubcontractersVector scv = SubcontracterDAL.LoadCollection();
			foreach(SubcontracterData scd in scv)
				if(sc.IndexOfKey( scd.SubcontracterName)==-1 &&  scd.SubcontracterName!="")
				sc.Add( scd.SubcontracterName,scd.SubcontracterName);
			
			ClientsVector cv = ClientDAL.LoadCollection(-1);
			foreach(ClientData cd in cv)
				if(sc.IndexOfKey( cd.ClientName)==-1 &&  cd.ClientName!="")
				sc.Add( cd.ClientName,cd.ClientName);
				
			return sc;
		}
		public static SortedList GetAllPMUsers(int ProjectID, bool bRights)
		{
			SortedList sc = new SortedList();

			ProjectUsersVector puv = ProjectUserDAL.LoadCollection("ProjectUsersSelByProjectProc",SQLParms.CreateProjectUsersSelByProjectProc(ProjectID));
			foreach(ProjectUserData pud in puv)
			{
				UserInfo ud = UsersData.SelectUserByID(pud.UserID);
				if(ud!=null && sc.IndexOfKey(ud.FullName) ==-1 && ud.FullName!="")
					sc.Add(ud.FullName,ud.FullName);
			
			}

			//SqlDataReader reader = UsersData.SelectUsers(-1,1/*don't matter sortOrder*/);
			/*while(reader.Read())
			{
				if(sc.IndexOfKey(reader.GetString(7))==-1 && reader.GetString(7)!="")
					sc.Add( reader.GetString(7),reader.GetString(7));
				
			}*/
			ProjectSubcontractersVector psv = ProjectSubcontracterDAL.LoadCollection("ProjectSubcontractersListProc1",SQLParms.CreateProjectSubcontractersListProc1(ProjectID,bRights));
			foreach(ProjectSubcontracterData scd in psv)
			{
				
				if(sc.IndexOfKey( scd.SubcontracterName)==-1 &&  scd.SubcontracterName!="")
					sc.Add( scd.SubcontracterName,scd.SubcontracterName);
			}
			ClientsVector cv =ClientDAL.LoadCollectionByProject(ProjectID);
				// ClientDAL.LoadCollection();
			foreach(ClientData cd in cv)
				if(sc.IndexOfKey( cd.ClientName)==-1 &&  cd.ClientName!="")
					sc.Add( cd.ClientName,cd.ClientName);
				
			return sc;
		}
		public static string GetLogErrorMessage(Exception ex)
		{
			return string.Concat("message: ",ex.Message," source: ",ex.Source);
		}
		public static void CreateMenu(PlaceHolder menuHolder,UserInfoPM user,string lang)
		{
			UserControls.MenuTable menu = new UserControls.MenuTable();
			menu.ID = "MenuTable";
			menu.Width = Unit.Pixel(150);
			// 'Links' group
			ArrayList menuItems = new ArrayList();
			menuItems.Add(new MenuItemInfo(GetText(lang,"pmPageTitle_Projects"), "/Projects.aspx",true));
			if(user.UserRoleID>0)
			{
				menuItems.Add(new MenuItemInfo(GetText(lang,"pmPageTitle_Users"), "/Users.aspx",true));
				menuItems.Add(new MenuItemInfo(GetText(lang,"pmPageTitle_Logs"), "/Logs.aspx",true));
			}
			menu.AddMenuGroup( 20, menuItems);
			
			menuHolder.Controls.Add(menu);
		}
		public static string GetText(string language,string text)
		{
			try
			{
				string ResName = string.Concat(text,"_",language);
				string s = Resource.ResourceManager[ResName];
				return s;
			}
			catch(Exception ex)
			{
				log.Error(GetLogErrorMessage(ex));
				return "";
			}

		}
		
	
		#region ProjectFilter

		public static bool LoadBuildingTypes(DropDownList ddl)
		{
			return LoadBuildingTypes("0",ddl);
		}


		public static bool LoadBuildingTypes(string selectedValue,DropDownList ddl)
		{
			SqlDataReader reader = null;
			try
			{
				reader = DBManager.SelectActiveBuildingTypes();
				ddl.DataSource = reader;
				ddl.DataValueField = "BuildingTypeID";
				ddl.DataTextField = "BuildingType";
				if(SessionManager.Lang==Languages.EN)
					ddl.DataTextField = "BuildingTypeEN";
				ddl.DataBind();
//				if(ddl.Items.FindByValue(System.Configuration.ConfigurationSettings.AppSettings["BuildingTypeNotInPM"])!=null)
//					ddl.Items.Remove(ddl.Items.FindByValue(System.Configuration.ConfigurationSettings.AppSettings["BuildingTypeNotInPM"]));
				if(SessionManager.Lang==Languages.EN)
					ddl.Items.Insert(0, new ListItem(Resource.ResourceManager["reports_typeBuilding_EN"], "0"));
				else
				ddl.Items.Insert(0, new ListItem(Resource.ResourceManager["reports_typeBuilding"], "0"));
				ddl.SelectedValue = selectedValue;
			}
			catch (Exception ex)
			{
				log.Error(UIHelpers.GetLogErrorMessage(ex));
				return false;
			}
			finally 
			{
				if (reader!=null) reader.Close();
			}

			return true;
		}

		
		public static void LoadProjectStatus(DropDownList ddl,bool hpr)
		{
			if(SessionManager.Lang==Languages.EN)
			{
				ListItem li = new ListItem(Resource.ResourceManager["Name_ActiveProjects_EN"],"1");
				ddl.Items.Add(li);
				li = new ListItem(Resource.ResourceManager["Name_PassiveProjects_EN"],"2");
				ddl.Items.Add(li);
				if(hpr)
				{
					li = new ListItem(Resource.ResourceManager["Name_AllProjects_EN"],"0");
					ddl.Items.Add(li);
					li = new ListItem(Resource.ResourceManager["Name_ConcludedlProjects_EN"],"3");
					ddl.Items.Add(li);
				}
			}
			else
			{
				ListItem li = new ListItem(Resource.ResourceManager["Name_ddlProjectStatusName1_BG"],"1");
				ddl.Items.Add(li);
				li = new ListItem(Resource.ResourceManager["Name_ddlProjectStatusName3_BG"],"2");
				ddl.Items.Add(li);
				if(hpr)
				{
					li = new ListItem(Resource.ResourceManager["Name_ddlProjectStatusName2_BG"],"0");
					ddl.Items.Add(li);
					li = new ListItem(Resource.ResourceManager["Name_ddlProjectStatusName4_BG"],"3");
					ddl.Items.Add(li);
				}
			}
			ddl.SelectedValue = "1";
		}
		public static string ReplaceEN(string text)
		{
			for(int i=1;i<_MAX_TEXTS;i++)
			{
				text=text.Replace(Resource.ResourceManager[string.Concat("NameRe"+i.ToString()+"_BG")],
					Resource.ResourceManager[string.Concat("NameRe"+i.ToString()+"_EN")]);
			}
			return text;
		}
		public static bool LoadProjects(DropDownList ddlProjects,DropDownList ddlProjectsStatus,DropDownList ddlBuildingTypes)
		{
			SqlDataReader reader = null;
			
			try
			{
				bool isBuildingTypeGradoustrShow = false;
				ddlProjects.Items.Clear();
				if(SessionManager.Lang==Languages.EN)
				ddlProjects.Items.Add(new ListItem("<"+Resource.ResourceManager["reports_ddlAllProjects_EN"]+">","0"));
				else
				ddlProjects.Items.Add(new ListItem("<"+Resource.ResourceManager["reports_ddlAllProjects"]+">","0"));
				
				if(ddlBuildingTypes.SelectedValue == System.Configuration.ConfigurationSettings.AppSettings["BuildingTypeNotInPM"])
					isBuildingTypeGradoustrShow = true;

				reader = ProjectsData.SelectProjectNamesClear((ProjectsData.ProjectsByStatus)int.Parse(ddlProjectsStatus.SelectedValue), int.Parse(ddlBuildingTypes.SelectedValue),isBuildingTypeGradoustrShow);
			
				while (reader.Read())
				{
					int projectID = reader.GetInt32(0);
					ddlProjects.Items.Add(new ListItem(reader.GetString(1), projectID.ToString()));
					
				}
			}
			catch (Exception ex)
			{
				log.Error(ex);
				return false;
			}

			finally
			{
				if (reader!=null) reader.Close();
			}
			
			
			return true;
		}

		#endregion
	
		public static string GetProfileText(int ProfileType,string language)
		{
			switch(ProfileType)
			{
				case (int)ProfileItems.Sketch:return GetText(language,"ProfileItemsSketch_Name");
				case (int)ProfileItems.VisaPlus:return GetText(language,"ProfileItemsVisaPlus_Name");
				case (int)ProfileItems.KadasterUndEnt:return GetText(language,"ProfileItemsKadasterUndEnt_Name");
				case (int)ProfileItems.KadasterEnt:return GetText(language,"ProfileItemsKadasterEnt_Name");
				case (int)ProfileItems.VIKEnt:return GetText(language,"ProfileItemsVIKEnt_Name");
				case (int)ProfileItems.ElEnt:return GetText(language,"ProfileItemsElEnt_Name");
				case (int)ProfileItems.TEnt:return GetText(language,"ProfileItemsTEnt_Name");
				case (int)ProfileItems.PathEnt:return GetText(language,"ProfileItemsPathEnt_Name");
				case (int)ProfileItems.PhoneEnt:return GetText(language,"ProfileItemsPhoneEnt_Name");
				default: return "";
			}
		}

		private const string LocalAddress = "1";
		public static string GetAppPathString()
		{
			string appPath = HttpContext.Current.Request.ApplicationPath;

			bool bIsInRoot = bool.Parse(System.Configuration.ConfigurationSettings.AppSettings["IsInRoot"]);
			if(bIsInRoot)
				appPath="http://"+HttpContext.Current.Request.Url.Host;
			if(!HttpContext.Current.Request.Url.Host.StartsWith(LocalAddress))
				appPath+=System.Configuration.ConfigurationSettings.AppSettings["Port"];
			return appPath;
		}
	}
}
