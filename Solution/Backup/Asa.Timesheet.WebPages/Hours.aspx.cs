
using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using Asa.Timesheet.Data;
using Asa.Timesheet.Data.Entities;
using Asa.Timesheet.Data.Util;
using System.Data.SqlClient;
using System.Xml;
using log4net;
using System.Text;
using System.Threading;
using System.Security.Principal;
namespace Asa.Timesheet.WebPages
{
	/// <summary>
	/// Summary description for Hours.
	/// </summary>
	public class Hours : TimesheetPageBase
	{
		#region Web controls

		protected System.Web.UI.WebControls.DataGrid GridHours;
		protected System.Web.UI.HtmlControls.HtmlInputText Total;
		protected System.Web.UI.HtmlControls.HtmlTable tblCalendar;
		protected System.Web.UI.WebControls.Button btnNewProject;
		protected System.Web.UI.WebControls.Label lblInfo;
		protected System.Web.UI.HtmlControls.HtmlInputText txtVisibleRows;
		protected System.Web.UI.HtmlControls.HtmlInputText txtNoActivity;
		protected System.Web.UI.WebControls.Button btnSave;
		protected System.Web.UI.HtmlControls.HtmlInputText txtIncorrectDataMsg;
		protected System.Web.UI.WebControls.DropDownList ddlUsers;
		protected System.Web.UI.WebControls.Panel grid;
		protected System.Web.UI.WebControls.Panel divMain;
		protected System.Web.UI.HtmlControls.HtmlTable tblGridBottom;
		protected Asa.Timesheet.WebPages.UserControls.PageHeader header;
		protected System.Web.UI.WebControls.Label lblTop;
		protected System.Web.UI.WebControls.PlaceHolder menuHolder;
		protected System.Web.UI.WebControls.Label lblError;
		protected System.Web.UI.HtmlControls.HtmlInputText txtTimeAbbr;
		protected System.Web.UI.HtmlControls.HtmlInputText txtDefaultWorkDayTimes;
		protected System.Web.UI.HtmlControls.HtmlInputText txtDefaultHoursProjects;
		protected System.Web.UI.HtmlControls.HtmlInputText txtMinGridRows;

		#endregion

		private static readonly ILog log = LogManager.GetLogger(typeof(Hours));

		#region private members

		private ArrayList _activities;
		private ArrayList _projects;
		private ArrayList _projectsAll;
		private ArrayList _times;
		private int _totalMinutes = 0;
		private bool _isEditMode;
		protected System.Web.UI.WebControls.Label lblStatus;
		protected System.Web.UI.WebControls.HyperLink HyperLink1;
		protected System.Web.UI.WebControls.Button btnCopy;
		protected System.Web.UI.WebControls.Button btnCopySave;
		protected System.Web.UI.WebControls.TextBox txtCopiedUsers;
		protected System.Web.UI.WebControls.Button Button1;
		protected System.Web.UI.WebControls.DropDownList ddlBuildingTypes1;
		protected System.Web.UI.HtmlControls.HtmlInputFile fileProtokol;
		protected System.Web.UI.WebControls.Button btnWhoElse;
		//private bool _hasGridDefaultProjects = false;
		

		#endregion
	
		#region Grid columns

		private enum GridColumns
		{
			Delete = 0,
			ProjectAndActivity,
			StartTime,
			EndTime,
			Minutes,
			AdminMinutes,
			Mail,
			WorkTimeID,
			
		}

		#endregion

		private enum GridItemStatus
		{
			Normal = 0,
			HasDefaultTimes,
			Disabled
		}


        private void Page_Load(object sender, System.EventArgs e)
        {
            if (!this.LoggedUser.IsAuthenticated) ErrorRedirect(Resource.ResourceManager["noRightsAlert"]);

            UIHelpers.CreateMenu(menuHolder, LoggedUser);

            if (!this.IsPostBack)
            {

                header.PageTitle = Resource.ResourceManager["hours_PageTitle"];
                header.UserName = this.LoggedUser.FullName;

                btnNewProject.Text = Resource.ResourceManager["hours_btnNewProject"];
                btnNewProject.Attributes["onclick"] = "javascript:ShowNewProject();return false;";

                CurrentMonth = new DateTime(DateTime.Now.Year, DateTime.Now.Month, 1).ToString("dd.MM.yyyy");
                SelectedDate = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day).ToString("dd.MM.yyyy");

                txtDefaultWorkDayTimes.Value =
                    String.Concat(SettingsManager.HoursGridDefaultStartHourID.ToString(), ",",
                    SettingsManager.HoursGridDefaultEndHourID.ToString());

                LoadUsers(true);
                BindGrid();

                btnSave.Text = Resource.ResourceManager["hours_btnSave"];

                btnSave.Attributes["onclick"] = "if (!ValidateInput()) return false; else DisableAllRows(false, null);";
                btnCopy.Attributes["onclick"] = "if (ValidateInput()) { OpenCopyWindow(); } return false;";

                btnCopySave.Attributes["onclick"] = "if (!ValidateInput()) return false; else DisableAllRows(false, null);";

                txtIncorrectDataMsg.Value = Resource.ResourceManager["hoursIncorrectDataMsg"];
                txtTimeAbbr.Value = String.Concat(Resource.ResourceManager["common_HourAbbr"], ";",
                    Resource.ResourceManager["common_MinuteAbbr"]);


                txtMinGridRows.Value = SettingsManager.HoursGridInitialRowsCount.ToString(); //Constants.TimesGridMinVisibleRows.ToString();
                //lblTop.Text = "";

                if (!(this.LoggedUser.IsLeader || this.LoggedUser.IsAccountant || this.LoggedUser.IsAdmin))
                {
                    HyperLink1.Visible = false;
                    //ddlUsers.Visible = false;	
                    ddlUsers.Style["display"] = "none";

                }

                GridHours.Columns[5].Visible = false;
                GridHours.Columns[4].ItemStyle.Width = Unit.Percentage(40);// = false;
                SetMessageStrings();
            }
            if (LoggedUser.IsAccountant)
                btnCopy.Visible = false;
            CreateCalendar(DateTime.ParseExact(CurrentMonth, "dd.MM.yyyy", System.Globalization.CultureInfo.InvariantCulture));
        }


		#region Hours grid functions

        private ShortProjectInfo FindProject(int projectID)
        {
            for (int i = 0; i < _projects.Count; i++)
            {
                if (((ShortProjectInfo)_projects[i]).ProjectID == projectID) return (ShortProjectInfo)_projects[i];
            }

            return null;
        }

        private void BindGrid()
        {
            if (ddlUsers.SelectedValue == "0")
            {
                GridHours.DataSource = null;
                GridHours.DataBind();

                lblTop.Text = String.Empty;
                lblStatus.Text = String.Empty;
                btnSave.Visible = false;
                btnNewProject.Visible = false;
                Total.Value = String.Empty;
                divMain.Visible = false;
                return;
            }
            if (this.SelectedDate == String.Empty) return;

            DateTime selectedDate = DateTime.ParseExact(SelectedDate, "dd.MM.yyyy", System.Globalization.CultureInfo.InvariantCulture);

            _projects = LoadProjectsForDay(selectedDate);
            _projectsAll = LoadAllProjectsForDay(selectedDate);
            _activities = LoadActivities();
            _times = LoadTimes(selectedDate);

            ArrayList workTimesList = null;

            bool isWorkDayEntered = IsWorkDayEntered();

            //peppi
            DateTime dtWorkDay = DateTime.ParseExact(this.SelectedDate, "dd.MM.yyyy",
                System.Globalization.CultureInfo.InvariantCulture);


            _isEditMode = true;//(this.LoggedUser.IsLeader) ||(this.LoggedUser.IsAccountant)|| (!(isWorkDayEntered && dtWorkDay < DateTime.Now.Date));
            
           
            //_isEditMode = (this.LoggedUser.IsLeader) ||(this.LoggedUser.IsAccountant)|| (!(isWorkDayEntered && dtWorkDay < DateTime.Now.Date));
            //end peppi
            
            

            if (!isWorkDayEntered)
            {
                workTimesList = WorkTimesData.SelectWorkTimesNew(int.Parse(ddlUsers.SelectedValue), this.LoggedUser.UserID);
                for (int i = workTimesList.Count - 1; i >= 0; i--)
                {
                    if (FindProject(((WorkTimeInfo)workTimesList[i]).ProjectID) == null) workTimesList.Remove(workTimesList[i]);
                }

                lblStatus.Text = Resource.ResourceManager["hours_GridStatusNew"];
            }
            else
            {
                workTimesList = WorkTimesData.SelectWorkTimesForDay(int.Parse(ddlUsers.SelectedValue), selectedDate);
                lblStatus.Text = Resource.ResourceManager["hours_GridStatusEdit"];
            }

            _isEditMode = this.CheckEditMode(selectedDate, workTimesList);
            

         

            int firstHiddenRowIndex;
            int prevCount = workTimesList.Count;

            if ((_isEditMode) && (prevCount < SettingsManager.HoursGridInitialRowsCount))
                firstHiddenRowIndex = SettingsManager.HoursGridInitialRowsCount;
            else firstHiddenRowIndex = prevCount;

            divMain.Visible = true;
            if (_isEditMode)
            {
                for (int i = prevCount; i < SettingsManager.HoursGridMaxRowsCount; i++)
                {
                    WorkTimeInfo wti = new WorkTimeInfo();
                    wti.ProjectID = 0;
                    wti.ActivityID = 0;
                    wti.StartTimeID = 0;
                    wti.EndTimeID = 0;
                    wti.Minutes = String.Empty;
                    wti.AdminMinutes = String.Empty;
                    wti.WorkTimeID = 0;
                    workTimesList.Add(wti);
                }

                btnNewProject.Visible = true;
                btnSave.Visible = true;
            }
            else
            {
                btnNewProject.Visible = false;
                lblInfo.Text = Resource.ResourceManager["hoursDayViewOnly"];
                lblStatus.Text = String.Empty;
                btnSave.Visible = false;
            }

            SetGridItemsStatuses(workTimesList);

            GridHours.DataSource = workTimesList;
            GridHours.DataBind();


            for (int j = 0; j < GridHours.Items.Count; j++)
            {

                DropDownList ddl = (DropDownList)GridHours.Items[j].Cells[(int)GridColumns.ProjectAndActivity].FindControl("ddlBuildingTypes");
                DropDownList ddlProjectActive = (DropDownList)GridHours.Items[j].Cells[(int)GridColumns.ProjectAndActivity].FindControl("ddlProjectActive");
                if (LoggedUser.IsAccountantLow)
                    ddl.Visible = false;
                else
                {
                    LoadBuildingTypes(ddl);
                    LoadProjectTypes(ddlProjectActive);
                    ddl.TabIndex = (short)j;
                    ddlProjectActive.TabIndex = (short)j;
                }

            }


            txtVisibleRows.Value = firstHiddenRowIndex.ToString();

            lblTop.Text = String.Concat(Resource.ResourceManager["hoursWorkTimeForLabel"], ddlUsers.SelectedItem.Text,
                " ", Resource.ResourceManager["hoursZaStr"], " ", selectedDate.ToString("ddd, dd MMM yyyy"), Resource.ResourceManager["hours_year"]);

            Total.Value = TimeHelper.HoursStringFromMinutes(_totalMinutes, true);
        }

        private bool CheckEditMode(DateTime selectedDate, ArrayList workTimesList)
        {
            bool RestrictEntry = bool.Parse(System.Configuration.ConfigurationManager.AppSettings["RestrictEntry"]);

            if (RestrictEntry && selectedDate.Date < DateTime.Now.Date && !this.LoggedUser.IsLeader && !this.LoggedUser.IsAccountant && !this.LoggedUser.IsAdmin)
            {
                foreach (WorkTimeInfo item in workTimesList)
                {
                    if (item.MeetingId == 0 && item.WorkTimeID != 0)
                    {
                        return false;
                    }
                }
            }
            return true;
        }
        private void LoadProjectTypes(DropDownList ddl)
        {
            UIHelpers.LoadProjectStatus(ddl, -1, true);
        }
        private bool LoadBuildingTypes(DropDownList ddlBuildingTypes)
        {
            SqlDataReader reader = null;
            try
            {
                reader = DBManager.SelectActiveBuildingTypes();
                ddlBuildingTypes.DataSource = reader;
                ddlBuildingTypes.DataValueField = "BuildingTypeID";
                ddlBuildingTypes.DataTextField = "BuildingType";
                ddlBuildingTypes.DataBind();

                ddlBuildingTypes.Items.Insert(0, new ListItem("", "0"));

            }
            catch (Exception ex)
            {
                log.Info(ex);
                return false;
            }
            finally
            {
                if (reader != null) reader.Close();
            }

            return true;
        }
        private void SetGridItemsStatuses(ArrayList workTimes)
        {
            if (!_isEditMode)
            {
                for (int i = 0; i < workTimes.Count; i++) ((WorkTimeInfo)workTimes[i]).Status = 3;
                return;
            }
            WorkTimeInfo wti = (WorkTimeInfo)workTimes[0];
            if (txtDefaultHoursProjects.Value.IndexOf("." + wti.ProjectID.ToString() + ".") >= 0)
            {
                wti.Status = 1;
                for (int i = 1; i < workTimes.Count; i++) ((WorkTimeInfo)workTimes[i]).Status = 2;
            }

        }

		#endregion

		#region Web Form Designer generated code
        override protected void OnInit(EventArgs e)
        {
            //
            // CODEGEN: This call is required by the ASP.NET Web Form Designer.
            //
            InitializeComponent();
            base.OnInit(e);
        }
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
        private void InitializeComponent()
        {
            this.ddlUsers.SelectedIndexChanged += new System.EventHandler(this.ddlUser_SelectedIndexChanged);
            this.GridHours.ItemCreated += new System.Web.UI.WebControls.DataGridItemEventHandler(this.GridHours_ItemCreated);
            this.GridHours.ItemCommand += new System.Web.UI.WebControls.DataGridCommandEventHandler(this.GridHours_ItemCommand);
            this.GridHours.ItemDataBound += new System.Web.UI.WebControls.DataGridItemEventHandler(this.GridHours_ItemDataBound);
            this.btnCopySave.Click += new System.EventHandler(this.btnCopySave_Click);
            this.btnCopy.Click += new System.EventHandler(this.btnCopy_Click);
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            this.Load += new System.EventHandler(this.Page_Load);

        }
		#endregion

		#region Database

        private bool LoadUsers(bool loggedUserDefault)
        {
            if (this.LoggedUser.IsLeader || this.LoggedUser.IsAdmin)
            {
                ddlUsers.Visible = true;

                SqlDataReader reader = null;
                try
                {
                    //reader = UsersData.SelectUserNamesOnlyActive(LoggedUser.HasPaymentRights);
                    //edited by Ivailo on 23.01.2008
                    reader = UsersData.SelectUserNamesOnlyActive(false);
                    ddlUsers.DataSource = reader;
                    ddlUsers.DataValueField = "UserID";
                    ddlUsers.DataTextField = "FullName";
                    ddlUsers.DataBind();
                    //					ddlUsers.Items.Insert(0,new ListItem("<"+Resource.ResourceManager["hours_SelectUser"]+">","0"));
                    if (ddlUsers.Items.FindByValue(this.LoggedUser.UserID.ToString()) != null)
                        ddlUsers.SelectedValue = this.LoggedUser.UserID.ToString(); //"0";
                }
                catch (Exception ex)
                {
                    log.Error(ex);
                    return false;
                }
                finally
                {
                    if (reader != null) reader.Close();
                }
            }
            if (this.LoggedUser.IsAccountant)
            {
                ddlUsers.Visible = true;

                SqlDataReader reader = null;
                try
                {
                    reader = UsersData.SelectUserNames(LoggedUser.HasPaymentRights);
                    ddlUsers.DataSource = reader;
                    ddlUsers.DataValueField = "UserID";
                    ddlUsers.DataTextField = "FullName";
                    ddlUsers.DataBind();
                    ddlUsers.Items.Insert(0, new ListItem("<" + Resource.ResourceManager["hours_SelectUser"] + ">", "0"));
                    ddlUsers.SelectedValue = this.LoggedUser.UserID.ToString(); //"0";
                }
                catch (Exception ex)
                {
                    log.Error(ex);
                    return false;
                }
                finally
                {
                    if (reader != null) reader.Close();
                }
            }

            else
            {
                ddlUsers.Items.Insert(0, new ListItem(this.LoggedUser.FullName, this.LoggedUser.UserID.ToString()));
                ddlUsers.SelectedIndex = 0;
            }

            return true;
        }

        private ArrayList LoadProjectsForDay(DateTime workDate)
        {
            System.Text.StringBuilder activityStr = new System.Text.StringBuilder(".");
            System.Text.StringBuilder defaultTimesProjectsStr = new System.Text.StringBuilder(".");

            ArrayList projects = new ArrayList();

            SqlDataReader reader = null;

            try
            {
                if (LoggedUser.IsAccountantLow)
                    reader = WorkTimesData.SelectProjectsForDayAcc(workDate);
                if (LoggedUser.IsAccountant || LoggedUser.IsLeader || LoggedUser.IsAdmin)
                    reader = WorkTimesData.SelectProjectsForDayAccMain(workDate);
                else if (!LoggedUser.IsAccountant)
                {
                    int nID = int.Parse(System.Configuration.ConfigurationManager.AppSettings["JulianID"]);
                    reader = WorkTimesData.SelectProjectsForDay(workDate, nID == LoggedUser.UserID);
                }
                while (reader.Read())
                {
                    ShortProjectInfo pi = new ShortProjectInfo(reader.GetInt32(0), reader.GetString(1));
                    projects.Add(pi);
                    bool hasActivity = reader.IsDBNull(2) ? false : reader.GetBoolean(2);
                    if (!hasActivity) activityStr.Append(pi.ProjectID.ToString() + ".");
                    bool hasDefaultWorkTimes = reader.IsDBNull(3) ? false : reader.GetBoolean(3);
                    if (hasDefaultWorkTimes) defaultTimesProjectsStr.Append(pi.ProjectID.ToString() + ".");
                }
            }
            catch (Exception ex)
            {
                log.Error(ex);
                return null;
            }
            finally
            {
                if (reader != null) reader.Close();
            }

            projects.Insert(0, new ShortProjectInfo(0, ""));

            txtDefaultHoursProjects.Value = defaultTimesProjectsStr.ToString();
            txtNoActivity.Value = activityStr.ToString();
            return projects;
        }
        private ArrayList LoadAllProjectsForDay(DateTime workDate)
        {
            System.Text.StringBuilder activityStr = new System.Text.StringBuilder(".");
            System.Text.StringBuilder defaultTimesProjectsStr = new System.Text.StringBuilder(".");

            ArrayList projects = new ArrayList();

            SqlDataReader reader = null;

            try
            {
                if (LoggedUser.IsAccountantLow)
                    reader = WorkTimesData.SelectProjectsForDayAcc(workDate);
                if (LoggedUser.IsAccountant || LoggedUser.IsLeader || LoggedUser.IsAdmin)
                    reader = WorkTimesData.SelectProjectsForDayAccMain(workDate);
                else if (!LoggedUser.IsAccountant)
                {
                    int nID = int.Parse(System.Configuration.ConfigurationManager.AppSettings["JulianID"]);
                    reader = WorkTimesData.SelectAllProjectsForDay(workDate, nID == LoggedUser.UserID);
                }
                while (reader.Read())
                {
                    ShortProjectInfo pi = new ShortProjectInfo(reader.GetInt32(0), reader.GetString(1));
                    projects.Add(pi);
                    bool hasActivity = reader.IsDBNull(2) ? false : reader.GetBoolean(2);
                    if (!hasActivity) activityStr.Append(pi.ProjectID.ToString() + ".");
                    bool hasDefaultWorkTimes = reader.IsDBNull(3) ? false : reader.GetBoolean(3);
                    if (hasDefaultWorkTimes) defaultTimesProjectsStr.Append(pi.ProjectID.ToString() + ".");
                }
            }
            catch (Exception ex)
            {
                log.Error(ex);
                return null;
            }
            finally
            {
                if (reader != null) reader.Close();
            }

            projects.Insert(0, new ShortProjectInfo(0, ""));

            txtDefaultHoursProjects.Value = defaultTimesProjectsStr.ToString();
            txtNoActivity.Value = activityStr.ToString();
            return projects;
        }
        private ArrayList LoadActivities()
        {
            ArrayList activities = new ArrayList();
            SqlDataReader reader = null;

            try
            {
                reader = DBManager.SelectActivities();

                while (reader.Read())
                {
                    ShortActivityInfo ai = new ShortActivityInfo(reader.GetInt32(0), reader.GetString(1));
                    activities.Add(ai);
                }
            }
            catch (Exception ex)
            {
                log.Error(ex);
                ErrorRedirect("Error load activities.");
            }
            finally
            {
                if (reader != null) reader.Close();
            }

            activities.Insert(0, new ShortActivityInfo(0, ""));
            return activities;
        }

        private ArrayList LoadTimes(DateTime selectedDate)
        {
            ArrayList times = new ArrayList();

            SqlDataReader reader = null;

            try
            {
                reader = DBManager.SelectTimes(selectedDate);

                while (reader.Read())
                {
                    TimeInfo ti = new TimeInfo(reader.GetInt32(0), reader.GetString(1));
                    times.Add(ti);
                }
            }
            catch (Exception ex)
            {
                log.Error(ex);
                ErrorRedirect("Error load times.");
            }
            finally
            {
                if (reader != null) reader.Close();
            }

            times.Insert(0, new TimeInfo(0, ""));
            return times;
        }

        private bool SaveWorkTimes()
        {
            string s;
            bool isWorkDayEntered = IsWorkDayEntered();
            //peppi
            DateTime dtWorkDay = DateTime.ParseExact(this.SelectedDate, "dd.MM.yyyy", System.Globalization.CultureInfo.InvariantCulture);

            bool isEditMode = true;//(this.LoggedUser.IsLeader) ||(this.LoggedUser.IsAccountant));//|| (!(isWorkDayEntered && dtWorkDay < DateTime.Now.Date)
            //end peppi
            if (isEditMode)
            {
                DateTime workDate = DateTime.ParseExact(this.SelectedDate, "dd.MM.yyyy", System.Globalization.CultureInfo.InvariantCulture);
                DateTime enteredDate = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, DateTime.Now.Hour, DateTime.Now.Minute, DateTime.Now.Second);
                ArrayList workTimesList = CreateWorkTimesList(false);
                if ((workTimesList == null)) { return false; }
                int nAuthors = int.Parse(System.Configuration.ConfigurationManager.AppSettings["AN"]);
                //if (isWorkDayEntered && dtWorkDay < DateTime.Now.Date && !this.LoggedUser.IsLeader && !this.LoggedUser.IsAccountant)
                //ArrayList prevWorkTimesList = WorkTimesData.SelectWorkTimesNew(int.Parse(ddlUsers.SelectedValue), this.LoggedUser.UserID);
                //if (CheckEditMode(dtWorkDay, prevWorkTimesList))
                //{

                //    if (prevWorkTimesList.Count >= workTimesList.Count)
                //    {
                //        lblError.Text = Resource.ResourceManager["hoursDayViewOnly"];
                //        BindGrid();
                //        return false;
                //    }
                //    else
                //    {
                //        for (int i = prevWorkTimesList.Count; i < workTimesList.Count; i++)
                //        {
                //            WorkTimeInfo wti = (WorkTimeInfo)workTimesList[i];
                //            if (wti.ActivityID != nAuthors)
                //            {
                //                lblError.Text = Resource.ResourceManager["hoursDayViewOnly"];
                //                BindGrid();
                //                return false;
                //            }

                //        }
                //    }
                //}
                try
                {
                    WorkTimesData.InsertWorkTimesForUser(int.Parse(ddlUsers.SelectedValue), workDate,
                        workTimesList, enteredDate, LoggedUser.UserID, fileProtokol);
                    log.Info(string.Format("Worktime ADD for user {0} on date {1} by {2}", ddlUsers.SelectedValue, workDate, string.Concat(LoggedUser.FullName, " ( userID ", LoggedUser.UserID, " )")));

                    s = WindowsIdentity.GetCurrent().Name;
                    int n = s.IndexOf(@"\");
                    if (n >= 0)
                        s = s.Substring(n + 1);
                    log.Info(WindowsIdentity.GetCurrent().Name);
                    if (LoggedUser.Account.Length != 0 && LoggedUser.Account != s && (!LoggedUser.IsLeader) && (!LoggedUser.IsAccountant))
                    {
                        string sWorkDates = workDate.ToShortDateString();
                        int sUserID = int.Parse(ddlUsers.SelectedValue);
                        UserInfo ui = UsersData.SelectUserByID(sUserID);
                        if (LoggedUser.FullName != ui.FullName)
                            SendDirectorMail(sWorkDates, ui.FullName, s);
                    }
                }
                catch (Exception ex)
                {
                    log.Error(ex);

                    lblError.Text = Resource.ResourceManager["hours_ErrorSaveWorkTimes"];
                    return false;
                }

            }

            return true;
            //BindGrid();	
        }

		//notes:Add by Ivailo on 23.01.2008 
		//if you enter worktime for user with different account send mail to director
        private void SendDirectorMail(string WorkDates, string UsersName, string account)
        {
            UserInfo Director = UsersData.UsersListByRoleProc(1);
            string DMail = Director.Mail;
            if (LoggedUser.FullName != UsersName)
                MailBO.SendMailWorktime(DMail, LoggedUser.FullName, WorkDates, UsersName, account);
        }

        private bool SaveWorkTimes(ArrayList userIDs)
        {
            bool isWorkDayEntered = IsWorkDayEntered();
            //peppi
            DateTime dtWorkDay = DateTime.ParseExact(this.SelectedDate, "dd.MM.yyyy", System.Globalization.CultureInfo.InvariantCulture);

            bool isEditMode = true;//(this.LoggedUser.IsLeader) ||(this.LoggedUser.IsAccountant)|| (!(isWorkDayEntered && dtWorkDay < DateTime.Now.Date));
            //end peppi
            if (isEditMode)
            {
                DateTime workDate = DateTime.ParseExact(this.SelectedDate, "dd.MM.yyyy", System.Globalization.CultureInfo.InvariantCulture);
                DateTime enteredDate = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, DateTime.Now.Hour, DateTime.Now.Minute, DateTime.Now.Second);
                ArrayList workTimesList = CreateWorkTimesList(true);
                if ((workTimesList == null)) { return false; }

                userIDs.Insert(0, int.Parse(ddlUsers.SelectedValue));
                StringBuilder sb = new StringBuilder();
                StringBuilder sbLog = new StringBuilder();
                int nCount = 0;
                try
                {
                    string UserIDsNames = string.Empty;
                    for (int i = 0; i < userIDs.Count; i++)
                    {
                        if ((int)userIDs[i] != LoggedUser.UserID)
                        {
                            UserInfo ui = UsersData.SelectUserByID((int)userIDs[i]);
                            nCount++;
                            sb.Append(ui.FullName);
                            sb.Append(", ");
                            sbLog.Append(ui.UserID);
                            sbLog.Append(", ");
                            WorkTimesData.InsertWorkTimesForUser((int)userIDs[i], workDate,
                                workTimesList, enteredDate, LoggedUser.UserID, fileProtokol);
                            UserIDsNames = string.Concat(UserIDsNames, ", ", ui.FullName);
                        }
                    }
                    lblError.Text = string.Format(Resource.ResourceManager["worktime_copied"], nCount, sb.ToString().TrimEnd(new char[] { ' ', ',' }));
                    log.Info(string.Format("Worktime for {0} for date {1} has been copied for users {2} by {3}", ddlUsers.SelectedValue, workDate, sbLog.ToString(), string.Concat(LoggedUser.FullName, " ( userID ", LoggedUser.UserID, " )")));
                    //					string s=	Request.UserHostAddress;
                    //					if(LoggedUser.Account.Length!=0 && LoggedUser.Account!=s&& (!LoggedUser.IsLeader))
                    //					{
                    //						string sDates = workDate.ToShortDateString();
                    //						SendDirectorMail(sDates,UserIDsNames,s);
                    //					}
                }
                catch (Exception ex)
                {
                    log.Error(ex);

                    lblError.Text = Resource.ResourceManager["hours_ErrorSaveWorkTimes"];
                    return false;
                }

            }

            return true;
            //BindGrid();	
        }

		#endregion

		#region Event handlers

        private void GridHours_ItemDataBound(object sender, System.Web.UI.WebControls.DataGridItemEventArgs e)
        {

            if ((e.Item.ItemType == ListItemType.Item) || (e.Item.ItemType == ListItemType.AlternatingItem))
            {
                WorkTimeInfo currentItemInfo = (WorkTimeInfo)e.Item.DataItem;

             

                #region Bind controls

                #region Projects

                DropDownList ddlProjects = (DropDownList)e.Item.FindControl("ddlProject");
                ddlProjects.DataSource = _projects;
                ddlProjects.DataValueField = "ProjectID";
                ddlProjects.DataTextField = "ProjectName";
                ddlProjects.DataBind();
                //if(!LoggedUser.IsAccountant)
                {
                    if (ddlProjects.Items.FindByValue(((WorkTimeInfo)e.Item.DataItem).ProjectID.ToString()) != null)
                        ddlProjects.SelectedValue = ((WorkTimeInfo)e.Item.DataItem).ProjectID.ToString();
                    else
                    {
                        DropDownList ddlProjectActive = (DropDownList)e.Item.FindControl("ddlProjectActive");
                        //ddlProjectActive.SelectedValue="0";
                        ddlProjects.DataSource = _projectsAll;
                        ddlProjects.DataValueField = "ProjectID";
                        ddlProjects.DataTextField = "ProjectName";
                        ddlProjects.DataBind();
                        if (ddlProjects.Items.FindByValue(((WorkTimeInfo)e.Item.DataItem).ProjectID.ToString()) != null)
                            ddlProjects.SelectedValue = ((WorkTimeInfo)e.Item.DataItem).ProjectID.ToString();
                    }
                    HtmlInputText hdnPrevProjectID = (HtmlInputText)e.Item.FindControl("hdnPrevProjectID");
                    hdnPrevProjectID.Value = ddlProjects.SelectedValue;
                }
                #endregion

                #region Activities

                DropDownList ddlActivities = (DropDownList)e.Item.FindControl("ddlActivity");
                ddlActivities.DataSource = _activities;
                ddlActivities.DataValueField = "ActivityID";
                ddlActivities.DataTextField = "ActivityName";
                ddlActivities.DataBind();
                //if(!LoggedUser.IsAccountant)
                ddlActivities.SelectedValue = currentItemInfo.ActivityID.ToString();

                #endregion

                #region Start time

                DropDownList ddlStartTime = (DropDownList)e.Item.FindControl("ddlStartTime");
                ddlStartTime.DataSource = _times;
                ddlStartTime.DataValueField = "TimeID";
                ddlStartTime.DataTextField = "TimeString";
                ddlStartTime.DataBind();
                if (ddlStartTime.Items.FindByValue(currentItemInfo.StartTimeID.ToString()) != null)
                    ddlStartTime.SelectedValue = currentItemInfo.StartTimeID.ToString();

                #endregion

                #region End time

                DropDownList ddlEndTime = (DropDownList)e.Item.FindControl("ddlEndTime");
                ddlEndTime.DataSource = _times;
                ddlEndTime.DataValueField = "TimeID";
                ddlEndTime.DataTextField = "TimeString";
                ddlEndTime.DataBind();
                if (ddlEndTime.Items.FindByValue(currentItemInfo.EndTimeID.ToString()) != null)
                    ddlEndTime.SelectedValue = currentItemInfo.EndTimeID.ToString();

                #endregion

                #region Minutes

                TextBox minutes = (TextBox)e.Item.FindControl("txtMinutes");
                minutes.Text = ((WorkTimeInfo)e.Item.DataItem).Minutes.ToString();

                //admin minutes
                TextBox adminMinutes = (TextBox)e.Item.FindControl("txtAdminMinutes");
                adminMinutes.Text = ((WorkTimeInfo)e.Item.DataItem).AdminMinutes.ToString();

                #endregion

                //worktimeID hidden field - not used now
                HtmlInputHidden hdnWorkTimeID = (HtmlInputHidden)e.Item.FindControl("hdnWorkTimeID");
                hdnWorkTimeID.Value = ((WorkTimeInfo)e.Item.DataItem).WorkTimeID.ToString();

                #region Image buttons

                ImageButton btnDelete = (ImageButton)e.Item.FindControl("btnDeleteItem");

                ImageButton btnMail = (ImageButton)e.Item.FindControl("btnMail");
                if (currentItemInfo.WorkTimeID == 0) btnMail.Style["display"] = "none";
                else
                    if (currentItemInfo.Status != 3) btnMail.Attributes["onclick"] =
                                                         String.Concat("return confirm('", Resource.ResourceManager["hours_WarningSaveBeforeMail"], "');");

                ImageButton btnWho = (ImageButton)e.Item.FindControl("btnWho");
                if (currentItemInfo.WorkTimeID == 0) btnWho.Style["display"] = "none";
                else
                    if (currentItemInfo.Status != 3)
                    {

                        WorkTimeInfo wi = new WorkTimeInfo();
                        wi.ProjectID = int.Parse(ddlProjects.SelectedValue);
                        wi.StartTimeID = int.Parse(ddlStartTime.SelectedValue);
                        wi.EndTimeID = int.Parse(ddlEndTime.SelectedValue);
                        wi.ActivityID = int.Parse(ddlActivities.SelectedValue);
                        wi.WorkDate = DateTime.ParseExact(SelectedDate, "dd.MM.yyyy", System.Globalization.CultureInfo.InvariantCulture);

                        Session["Meeting"] = wi;
                        btnWho.Attributes["onclick"] = "if (ValidateInput()) { OpenMeetingWindow(); } return false;";
                    }
                ImageButton btnMeeting = (ImageButton)e.Item.FindControl("btnMeeting");
                if (currentItemInfo.WorkTimeID == 0) btnMeeting.Style["display"] = "none";
                else
                    if (currentItemInfo.Status != 3) btnMeeting.Attributes["onclick"] =
                                                         String.Concat("return confirm('", Resource.ResourceManager["hours_WarningSaveBeforeMail"], "');");

                #endregion

                #endregion

                //prevent edit of protocols
                if (currentItemInfo.MeetingId != 0 && (!LoggedUser.IsLeader) && (!LoggedUser.IsAccountant))
                {
                    DateTime workDate = DateTime.ParseExact(this.SelectedDate, "dd.MM.yyyy", System.Globalization.CultureInfo.InvariantCulture);
                    if (workDate.Date < DateTime.Now.Date)
                    {
                        ddlProjects.Enabled = false;
                        ddlActivities.Enabled = false;

                        ddlStartTime.Enabled = false;
                        ddlEndTime.Enabled = false;

                        minutes.ReadOnly = true;
                        adminMinutes.ReadOnly = true;

                        btnDelete.Visible = false;
                    }
                }

                #region Set controls' properties

                switch (currentItemInfo.Status)
                {
                    case 0: //Editable item
                        ddlProjects.Attributes["onchange"] = "DisableActivity()";
                        if (txtNoActivity.Value.IndexOf("." + currentItemInfo.ProjectID.ToString() + ".") >= 0) ddlActivities.Enabled = false;

                        ddlStartTime.Attributes["onchange"] = "ProcessTimes();";
                        ddlEndTime.Attributes["onchange"] = "ProcessTimes();";

                        btnDelete.Visible = true;
                        btnDelete.Attributes["onclick"] = "ClearItem(); ProcessTimes(); return false;";

                        break;

                    case 1: // item with default work times (work times - disabled)
                        ddlProjects.Attributes["onchange"] = "DisableActivity()";
                        ddlActivities.Enabled = false;

                        //ddlStartTime.Enabled = false;
                        if (ddlStartTime.SelectedValue == "0" && ddlStartTime.Items.FindByValue(currentItemInfo.StartTimeID.ToString()) != null)
                            ddlStartTime.SelectedValue = SettingsManager.HoursGridDefaultStartHourID.ToString();
                        ddlStartTime.Attributes["onchange"] = "ProcessTimes();";

                        //ddlEndTime.Enabled = false;
                        if (ddlEndTime.SelectedValue == "0" && ddlEndTime.Items.FindByValue(SettingsManager.HoursGridDefaultEndHourID.ToString()) != null)
                            ddlEndTime.SelectedValue = SettingsManager.HoursGridDefaultEndHourID.ToString();
                        ddlEndTime.Attributes["onchange"] = "ProcessTimes();";

                        btnDelete.Visible = true;
                        btnDelete.Attributes["onclick"] = "ClearItem(); ProcessTimes(); return false;";

                        break;

                    case 2: // disabled item where grid has a default hours item - all event properties are set, in case user changes the default item
                        ddlProjects.Enabled = false;
                        ddlProjects.Attributes["onbeforeupdate"] = "DisableActivity()";

                        ddlActivities.Enabled = false;

                        ddlStartTime.Enabled = false;
                        ddlStartTime.Attributes["onchange"] = "ProcessTimes();";
                        ddlEndTime.Enabled = false;
                        ddlEndTime.Attributes["onchange"] = "ProcessTimes();";

                        minutes.Enabled = false;
                        adminMinutes.Enabled = false;

                        btnDelete.Visible = true;
                        btnDelete.Attributes["onclick"] = "ClearItem(); ProcessTimes(); return false;";

                        break;

                    case 3: // readonly item - all controls disabled , user can only view hours
                        ddlProjects.Enabled = false;
                        ddlActivities.Enabled = false;

                        ddlStartTime.Enabled = false;
                        ddlEndTime.Enabled = false;

                        minutes.ReadOnly = true;
                        adminMinutes.ReadOnly = true;

                        btnDelete.Visible = false;

                        break;
                }

                #endregion

                int et = TimeHelper.TimeInMinutes(ddlEndTime.SelectedItem.Text);
                int st = TimeHelper.TimeInMinutes(ddlStartTime.SelectedItem.Text);

                _totalMinutes += (et - st);
            }

        }

		
		#region Create work times list

        private ArrayList CreateWorkTimesList(bool WithChecks)
        {
            ArrayList workTimesList = new ArrayList();
            ArrayList timeIntervals = new ArrayList();

            bool bChecked = false;
            for (int i = 0; i < GridHours.Items.Count; i++)
            {
                DataGridItem item = (DataGridItem)GridHours.Items[i];
                CheckBox ck = (CheckBox)item.FindControl("ck");
                if (ck != null && WithChecks && ck.Checked)
                    bChecked = true;

            }
            if (WithChecks && !bChecked)
            {
                lblError.Text = Resource.ResourceManager["hours_ErrorCheck"];
                return null;
            }
            for (int i = 0; i < GridHours.Items.Count; i++)
            {
                DataGridItem item = (DataGridItem)GridHours.Items[i];
                CheckBox ck = (CheckBox)item.FindControl("ck");
                if (ck != null && WithChecks && !ck.Checked)
                    continue;
                WorkTimeInfo wti = new WorkTimeInfo();


                #region process controls

                int workTimeID = int.Parse(((HtmlInputHidden)item.FindControl("hdnWorkTimeID")).Value);
                wti.WorkTimeID = workTimeID;

                DropDownList ddl = (DropDownList)item.FindControl("ddlProject");
                int projectID = int.Parse(ddl.SelectedValue);
                wti.ProjectID = projectID;

                ddl = (DropDownList)item.FindControl("ddlActivity");
                int activityID = int.Parse(ddl.SelectedValue);
                wti.ActivityID = activityID;

                ddl = (DropDownList)item.FindControl("ddlStartTime");
                int startTimeID = int.Parse(ddl.SelectedValue);
                wti.StartTimeID = startTimeID;
                string sStartTime = ddl.SelectedItem.Text;

                ddl = (DropDownList)item.FindControl("ddlEndTime");
                int endTimeID = int.Parse(ddl.SelectedValue);
                wti.EndTimeID = endTimeID;
                string sEndTime = ddl.SelectedItem.Text;


                TextBox txt = (TextBox)item.FindControl("txtMinutes");
                string minutes = txt.Text.Trim();
                wti.Minutes = (minutes == String.Empty) ? null : minutes;

                txt = (TextBox)item.FindControl("txtAdminMinutes");
                string adminMinutes = txt.Text.Trim();
                wti.AdminMinutes = (adminMinutes == String.Empty) ? null : adminMinutes;

                #endregion

                System.Web.UI.WebControls.Image imgWarning = (System.Web.UI.WebControls.Image)item.FindControl("imgWarning");

                #region validation

                if (wti.ProjectID == 0)
                {
                    if ((wti.ActivityID != 0) || (wti.StartTimeID != 0) || (wti.EndTimeID != 0) || (wti.Minutes != null) || (wti.AdminMinutes != null))
                    {
                        imgWarning.Style["display"] = "";
                        lblError.Text = Resource.ResourceManager["hours_ErrorEmptyProjectField"];
                        return null;
                    }
                    else continue;
                }
                else
                {
                    string s = txtNoActivity.Value;
                    if (txtNoActivity.Value.IndexOf("." + wti.ProjectID.ToString() + ".") == -1)
                    {
                        if (wti.ActivityID == 0)
                        {
                            lblError.Text = Resource.ResourceManager["hours_ErrorMustHaveActivity"];
                            imgWarning.Style["display"] = "";
                            return null;
                        }
                    }
                    else
                    {
                        if (wti.ActivityID != 0)
                        {
                            lblError.Text = Resource.ResourceManager["hours_ErrorMustNotHaveActivity"];
                            imgWarning.Style["display"] = "";
                            return null;
                        }
                    }

                    if ((wti.StartTimeID == 0) || (wti.EndTimeID == 0))
                    {
                        imgWarning.Style["display"] = "";
                        lblError.Text = Resource.ResourceManager["hours_ErrorTimeEmpty"];
                        return null;
                    }
                }

                TimeInterval ti = new TimeInterval(TimeHelper.TimeInMinutes(sStartTime), TimeHelper.TimeInMinutes(sEndTime));
                //new
                if (CheckTimeOverlaps(ti, timeIntervals))
                {
                    lblError.Text = Resource.ResourceManager["hours_ErrorTimesOverlap"];
                    imgWarning.Style["display"] = "";
                    return null;
                }
                //endnew
                #endregion

                workTimesList.Add(wti);
                timeIntervals.Add(ti);
            }

            //	if (CheckTimesOverlap(timeIntervals)) return null;

            return workTimesList;
        }

        private bool CheckTimeOverlaps(TimeInterval current, ArrayList timeIntervals)
        {
            for (int j = 0; j < timeIntervals.Count; j++)
            {
                TimeInterval temp = (TimeInterval)timeIntervals[j];
                if (temp.StartTime < current.StartTime)
                {
                    if (temp.EndTime > current.StartTime) { return true; }
                }
                else
                {
                    if (temp.StartTime < current.EndTime) { return true; }
                }
            }

            return false;
        }

        private bool CheckTimesOverlap(ArrayList timeIntervals)
        {
            for (int i = 0; i < timeIntervals.Count; i++)
            {
                TimeInterval current = (TimeInterval)timeIntervals[i];
                for (int j = i + 1; j < timeIntervals.Count; j++)
                {
                    TimeInterval temp = (TimeInterval)timeIntervals[j];
                    if (temp.StartTime < current.StartTime)
                    {
                        if (temp.EndTime > current.StartTime) { return true; }
                    }
                    else
                    {
                        if (temp.StartTime < current.EndTime) { return true; }
                    }
                }
            }

            return false;
        }

		#endregion

        private void btnSave_Click(object sender, System.EventArgs e)
        {
            if (SaveWorkTimes()) BindGrid();
        }

        private void btnCopySave_Click(object sender, System.EventArgs e)
        {
            ArrayList userIDs = ParseSelectedUsers(this.txtCopiedUsers.Text);

            if (SaveWorkTimes(userIDs)) BindGrid();
        }

        private bool CheckData()
        {
            return true;
        }

        private void ddlUser_SelectedIndexChanged(object sender, System.EventArgs e)
        {
            BindGrid();
            CreateCalendar(DateTime.ParseExact(CurrentMonth, "dd.MM.yyyy", System.Globalization.CultureInfo.InvariantCulture));
        }

        private void GridHours_ItemCommand(object source, System.Web.UI.WebControls.DataGridCommandEventArgs e)
        {
            if (e.CommandSource is DropDownList)
            {
                DropDownList ddl = (DropDownList)e.CommandSource;
                int j = ddl.TabIndex;
                DropDownList grdProjects = (DropDownList)GridHours.Items[j].Cells[(int)GridColumns.ProjectAndActivity].FindControl("ddlProject");
                bool IsBuildingTypeGradoustrShow = false;
                if (ddl.SelectedValue == System.Configuration.ConfigurationManager.AppSettings["BuildingTypeID"])
                    IsBuildingTypeGradoustrShow = true;
                SqlDataReader reader = null;
                try
                {
                    reader = ProjectsData.SelectProjects(1, (int)ProjectsData.ProjectsByStatus.Active, null, int.Parse(ddl.SelectedValue)/*, false*/, (!LoggedUser.IsAccountant || LoggedUser.IsAccountantLow), -1, -1, IsBuildingTypeGradoustrShow);
                    if (reader != null)
                    {
                        grdProjects.DataSource = reader;

                        grdProjects.DataBind();
                    }
                }
                catch (Exception ex)
                {
                    log.Info(ex);
                    lblError.Text = Resource.ResourceManager["projects_ErrorLoadProjects"];
                    return;
                }
                finally
                {
                    if (reader != null) reader.Close();
                }



            }
            else if (e.CommandSource is ImageButton)
            {
                ImageButton ib = (ImageButton)e.CommandSource;
                if (ib.ID == "btnMail" || ib.ID == "btnMeeting")
                {
                    //SaveWorkTimes();

                    int WorkTimeID = int.Parse(GridHours.Items[e.Item.ItemIndex].Cells[(int)GridColumns.WorkTimeID].Text);

                    WorkTimeInfo wti = null;
                    try
                    {
                        wti = WorkTimesData.SelectWorkTimesForID(WorkTimeID);
                    }
                    catch (Exception ex)
                    {
                        log.Error(ex);
                        lblError.Text = Resource.ResourceManager["hours_ErrorLoadEmailData"];
                        return;
                    }

                    if (wti != null)
                    {
                        MailInfo mi = new MailInfo();
                        mi.StartDate = mi.EndDate = wti.WorkDate;
                        mi.ProjectID = wti.ProjectID;
                        mi.UserID = wti.UserID;
                        mi.Minutes.Add(wti.Minutes);
                        mi.AdminMinutes.Add(wti.AdminMinutes);
                        mi.ActivityID = wti.ActivityID;
                        mi.WorkTimeID = WorkTimeID;

                        mi.IsFromHours = true;
                        SessionManager.CurrentMailInfo = mi;
                        if (ib.ID == "btnMail")
                            Response.Redirect("MailForm.aspx?view=hours");
                        else
                        {
                            string projectName, projectCode, administrativeName, add, investor;
                            decimal area = 0;
                            investor = "";
                            int clientID = 0;
                            bool phases; bool isPUP;
                            if (!ProjectsData.SelectProject(wti.ProjectID, out projectName, out  projectCode, out administrativeName, out area, out add,
                                out clientID, out phases, out isPUP))
                            {
                                log.Error("Project not found " + wti.ProjectID);
                                return;

                            }

                            SqlDataReader reader = null;

                            try
                            {
                                reader = ProjectsData.SelectProject(wti.ProjectID);
                                if (reader.Read())
                                {



                                    investor = reader.IsDBNull(13) ? String.Empty : reader.GetString(13);

                                }
                            }
                            catch (Exception ex)
                            {
                                log.Info(ex);

                            }

                            finally
                            {
                                if (reader != null) reader.Close();
                            }
                            string sManager = "";
                            string sClient = "";

                            MeetingData md = MeetingDAL.Load(wti.WorkDate, wti.ProjectID, wti.ActivityID, wti.StartTimeID, wti.EndTimeID);
                            string sUsers, sSubs, sClients;
                            sUsers = sSubs = sClients = "";
                            if (md != null)
                            {
                                if (md.Subcontracters != null && md.Subcontracters.Length > 0)
                                {
                                    string[] s = md.Subcontracters.Split(';');
                                    foreach (string sub in s)
                                    {
                                        int ID = UIHelpers.ToInt(sub);
                                        if (ID > 0)
                                        {
                                            SubcontracterData sd = SubcontracterDAL.Load(ID);
                                            if (sd.Manager != null && sd.Manager.Trim() != "")
                                                sSubs += sd.SubcontracterName + ": " + sd.Manager + ", ";
                                            else
                                                sSubs += sd.SubcontracterName + ", ";

                                        }
                                    }
                                    sSubs = sSubs.TrimEnd(new char[] { ' ', ',' });
                                }
                                if (md.Clients != null && md.Clients.Length > 0)
                                {
                                    string[] s = md.Clients.Split(';');
                                    foreach (string sub in s)
                                    {
                                        int clientsID = UIHelpers.ToInt(sub);
                                        if (clientsID > 0)
                                        {
                                            reader = null;
                                            try
                                            {
                                                reader = ClientsData.SelectClient(clientsID);
                                                if (reader.Read())
                                                {

                                                    sManager = reader["Manager"] == DBNull.Value ? String.Empty : (string)reader["Manager"];
                                                    sClient = reader["ClientName"] == DBNull.Value ? String.Empty : (string)reader["ClientName"];
                                                }

                                            }
                                            catch (Exception ex)
                                            {
                                                log.Error(ex);

                                            }
                                            finally
                                            {
                                                if (reader != null) reader.Close();
                                            }
                                            if (sClient != null && sClient.Trim() != "")
                                                sClients += sClient + ": " + sManager + ", ";
                                            else
                                                sClients += sClient + ", ";
                                        }
                                    }
                                    sClients = sClients.TrimEnd(new char[] { ' ', ',' });
                                }
                            }
                            DataView dv = WorkTimesData.ExecuteWorkTimesListMeetingProc(wti.WorkDate, wti.ProjectID, wti.ActivityID, wti.StartTimeID, wti.EndTimeID);
                            foreach (DataRowView drv in dv)
                            {
                                sUsers += (string)drv[0] + ", ";
                            }
                            sUsers = sUsers.TrimEnd(new char[] { ' ', ',' });
                            UIHelpers.Meeting(this, administrativeName, investor, wti.WorkDate.ToShortDateString(), sManager, LoggedUser.FullName, wti.Minutes, sClients, sSubs, sUsers, add);
                        }
                    }
                    else lblError.Text = Resource.ResourceManager["hours_ErrorEmailDataNotFound"];

                }
            }
        }

		#endregion

		#region private properties

        private bool IsWorkDayEntered()
        {
            try
            {
                return WorkTimesData.IsWorkDayEntered(int.Parse(ddlUsers.SelectedValue),
                    DateTime.ParseExact(this.SelectedDate, "dd.MM.yyyy", System.Globalization.CultureInfo.InvariantCulture),
                    true);
            }
            catch (Exception ex)
            {
                log.Error(ex);
                ErrorRedirect("Error.");
                return true;
            }
        }
		#endregion

		#region Menu

		//		private void CreateMenu()
		//		{
		//			UserControls.MenuTable menu = new UserControls.MenuTable();
		//			menu.ID = "MenuTable";
		//
		//			// 'Links' group
		//			ArrayList menuItems = new ArrayList();
		//
		//			menuItems.Add(new MenuItemInfo(Resource.ResourceManager["menuItem_Projects"], "Projects.aspx"));
		//			if (LoggedUser.IsLeader || LoggedUser.IsAssistant) 
		//			{
		//				menuItems.Add(new MenuItemInfo(Resource.ResourceManager["menuItem_Clients"], "Clients.aspx"));
		//								
		//				menuItems.Add(new MenuItemInfo(Resource.ResourceManager["menuItem_Subcontracters"], "Subcontracters.aspx"));
		//					
		//			}
		//			if (LoggedUser.IsLeader || LoggedUser.IsAdmin)
		//				menuItems.Add(new MenuItemInfo(Resource.ResourceManager["menuItem_Users"], "Users.aspx"));
		//		
		//			menuItems.Add(new MenuItemInfo(Resource.ResourceManager["menuItem_Emails"], "Emails.aspx"));
		//
		//			menu.AddMenuGroup(Resource.ResourceManager["menuHeader_Links"], 20, menuItems);
		//			
		//			// 'New' group
		//			menuItems = new ArrayList();
		//			menuItems.Add(new MenuItemInfo(Resource.ResourceManager["menuItem_NewProject"], "EditProject.aspx"));
		//			if (LoggedUser.IsLeader || LoggedUser.IsAssistant)
		//			{
		//				menuItems.Add(new MenuItemInfo(Resource.ResourceManager["menuItem_NewClient"], "EditClient.aspx"));
		//			}
		//			if (LoggedUser.IsLeader || LoggedUser.IsAdmin)
		//				menuItems.Add(new MenuItemInfo(Resource.ResourceManager["menuItem_NewUser"], "EditUser.aspx"));				
		//	
		//			menuItems.Add(new MenuItemInfo(Resource.ResourceManager["menuItem_NewEmail"], "EditEmail.aspx"));
		//			menuItems.Add(new MenuItemInfo(Resource.ResourceManager["newcontracter_PageTitle"], "EditSubContracter.aspx"));
		//
		//	
		//			menu.AddMenuGroup(Resource.ResourceManager["menuHeader_New"], 30, menuItems);
		//
		//			
		//			// "Reports' group
		//			menuItems = new ArrayList();
		//			menuItems.Add(new MenuItemInfo(Resource.ResourceManager["menuItem_WorkTimeReport"], "reports/worktimes.aspx"));
		//			menuItems.Add(new MenuItemInfo(Resource.ResourceManager["menuItem_MinutesReport"], "Reports/Minutes.aspx"));
		//		
		//			
		//			if (LoggedUser.IsLeader || LoggedUser.IsAssistant)
		//				menuItems.Add(new MenuItemInfo(Resource.ResourceManager["menuItem_MailReport"], "MailForm.aspx"));
		//			if (LoggedUser.IsLeader  || LoggedUser.IsAdmin)
		//			{
		//				menuItems.Add(new MenuItemInfo(Resource.ResourceManager["menuItem_NotEnteredWorkTime"], "reports/EmptyHours.aspx"));
		//				menuItems.Add(new MenuItemInfo(Resource.ResourceManager["menuItem_Calendar"], "reports/Calendar.aspx"));
		//			}
		//
		//			menu.AddMenuGroup(Resource.ResourceManager["menuHeader_Reports"], 30, menuItems);
		//
		//			if(LoggedUser.HasPaymentRights)
		//				UIHelpers.AddMenuAnalysis(menu);
		//			
		//			menuItems = new ArrayList();
		//			menuItems.Add(new MenuItemInfo(Resource.ResourceManager["menuItem_Help"], SessionManager.GetHelpLink(),true,Pages.Hours));
		//			menu.AddMenuGroup("", 10, menuItems);
		//
		//			menuHolder.Controls.Add(menu);
		//		}

		#endregion

		#region Calendar

        private void CreateCalendar(DateTime visibleDate)
        {
            //DateTime currentMonth = DateTime.ParseExact(visibleDate, "dd.MM.yyyy", System.Globalization.CultureInfo.InvariantCulture);
            DateTime prevMonth = visibleDate.AddMonths(-1);
            DateTime nextMonth = visibleDate.AddMonths(1);
            DateTime firstDay = new DateTime(visibleDate.Year, visibleDate.Month, 1);
            DateTime lastDay = firstDay.AddMonths(1).AddDays(-1);

            DateTime selectedDate = (SelectedDate == "") ? new DateTime(1900, 1, 1)
                : DateTime.ParseExact(SelectedDate, "dd.MM.yyyy", System.Globalization.CultureInfo.InvariantCulture);

            int monthDays = DateTime.DaysInMonth(firstDay.Year, firstDay.Month);
            int dayfirst = (int)firstDay.DayOfWeek;
            if (dayfirst == 0) dayfirst = 7;

            DateTime tempDate = firstDay.AddDays(-dayfirst + 1);
            DateTime today = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day);

            tblCalendar.Rows.Clear();

            #region Month

            HtmlTableRow month = new HtmlTableRow();
            

            HtmlTableCell td = new HtmlTableCell("th");
            td.Attributes.Add("colspan", "3");
            LinkButton ib = new LinkButton();
            ib.ID = "ibCalPrev";
            ib.Text = prevMonth.ToString("MMMM");
            //ib.ImageUrl = "images/monthleft.gif";
            ib.Click += new System.EventHandler(this.Month_Click);
            td.Controls.Add(ib); month.Cells.Add(td);

            //if (!this.LoggedUser.IsLeader && !this.LoggedUser.IsAccountant && visibleDate.Month  <=  DateTime.Now.Month)
            //{
            //    ib.Visible = false;
            //    tempDate = tempDate > DateTime.Now ? tempDate : DateTime.Now;
            //}

            td = new HtmlTableCell("th");
            td.Attributes.Add("colspan", "32");
            Label lbl = new Label();
            lbl.ID = "month" + firstDay.ToString("ddMMyyyy");
            lbl.Text = firstDay.ToString("MMMM yyyy");
            lbl.Width = 150;
            td.Controls.Add(lbl);
            td.Attributes["class"] = "monthName";

            month.Cells.Add(td);

            td = new HtmlTableCell("th");
            td.Attributes.Add("colspan", "3");
            ib = new LinkButton();
            //ib.ImageUrl = "images/monthright.gif";
            ib.Text = nextMonth.ToString("MMMM");
            ib.ID = "ibCalNext";
            ib.Click += new System.EventHandler(this.Month_Click);
            td.Controls.Add(ib); month.Cells.Add(td);
            

            tblCalendar.Rows.Add(month);

            #endregion


            HtmlTableRow tr = new HtmlTableRow();
            tblCalendar.Rows.Add(tr);

            

            tblCalendar.Rows[0].Cells[0].NoWrap = true;

            ArrayList al = null;
            string s = ddlUsers.SelectedValue;//delete
            if (ddlUsers.SelectedValue == "0") al = new ArrayList();
            else al = LoadEnteredDates(tempDate, tempDate.AddDays(40));// to correct period

            for (int i = 1; i <= 38; i++)
            {
                LinkButton lb = new LinkButton();
                lb.Text = tempDate.Day.ToString();
                lb.ID = tempDate.ToString("dd.MM.yyyy");//?
                lb.Click += new EventHandler(this.Calendar_Click);

                td = new HtmlTableCell();
                td.NoWrap = true;

                bool isEntered = ((al.IndexOf(tempDate) == -1) && (ddlUsers.SelectedValue != "0")) ? false : true;
                bool isWeekend = CalendarDAL.IsHoliday(tempDate) ? true : false;
                //(((int)tempDate.DayOfWeek == 0) || ((int)tempDate.DayOfWeek == 6))? true : false;

                if ((tempDate < firstDay) || (tempDate > lastDay))
                {
                    if (isWeekend) td.Attributes["class"] = "OtherMonthDayWeekend";
                    else td.Attributes["class"] = "OtherMonthDay";
                }
                else
                {
                    if (tempDate == today)
                    {
                        if (selectedDate == tempDate) td.Attributes["class"] = "selectedDay";
                        else td.Attributes["class"] = "Today";
                    }
                    else
                    {
                        if (isWeekend)
                        {
                            td.Attributes["class"] = "WeekendDay";
                        }
                        else
                        {
                            if (selectedDate == tempDate) td.Attributes["class"] = "selectedDay";
                            else
                            {
                                if ((!isEntered) && (tempDate < today)) td.Attributes["class"] = "MonthDayNotEntered";
                                else td.Attributes["class"] = "MonthDay";

                            }
                        }
                    }

                }

                td.Controls.Add(lb);

                tblCalendar.Rows[1].Cells.Add(td);
                tempDate = tempDate.AddDays(1);
            }
        }

        private void SetSelectedDate(string selected)
        {
            DateTime firstDay = DateTime.ParseExact(CurrentMonth, "dd.MM.yyyy", System.Globalization.CultureInfo.InvariantCulture);
            DateTime lastDay = firstDay.AddMonths(1).AddDays(-1);

            DateTime today = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day);

            if (this.SelectedDate != "")
            {
                try
                {
                    LinkButton lb = (LinkButton)tblCalendar.FindControl(SelectedDate);

                    if (lb != null)
                    {
                        HtmlTableCell td = (HtmlTableCell)tblCalendar.FindControl(SelectedDate).Parent;
                        DateTime tempDate = DateTime.ParseExact(SelectedDate, "dd.MM.yyyy", System.Globalization.CultureInfo.InvariantCulture);
                        ArrayList al = /*new ArrayList()*/LoadEnteredDates(firstDay, lastDay);

                        bool isEntered = ((al.IndexOf(tempDate) == -1) && (ddlUsers.SelectedValue != "0")) ? false : true;
                        bool isWeekend = (((int)tempDate.DayOfWeek == 0) || ((int)tempDate.DayOfWeek == 6)) ? true : false;

                        if ((tempDate < firstDay) || (tempDate > lastDay))
                        {
                            if (isWeekend) td.Attributes["class"] = "OtherMonthDayWeekend";
                            else td.Attributes["class"] = "OtherMonthDay";
                        }
                        else
                        {
                            if (tempDate == today)
                            {
                                td.Attributes["class"] = "Today";
                            }
                            else
                            {
                                if (isWeekend)
                                {
                                    td.Attributes["class"] = "WeekendDay";
                                }
                                else
                                {
                                    if ((!isEntered) && (tempDate < today)) td.Attributes["class"] = "MonthDayNotEntered";
                                    else td.Attributes["class"] = "MonthDay";
                                }
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    log.Error(ex);
                    throw ex;
                }

            }

            HtmlTableCell tc = (HtmlTableCell)tblCalendar.FindControl(selected).Parent;
            tc.Attributes["class"] = "selectedday";

            SelectedDate = selected;
        }

        private void Calendar_Click(object sender, System.EventArgs e)
        {
            lblStatus.Text = "";
            SetSelectedDate(((LinkButton)sender).ID);
            BindGrid();
        }

        private void Month_Click(object sender, System.EventArgs e)
        {
            lblStatus.Text = "";
            string sMonth = ((Label)tblCalendar.Rows[0].Cells[1].Controls[0]).ID.Substring(5);

            DateTime dt = DateTime.ParseExact(sMonth, "ddMMyyyy", System.Globalization.CultureInfo.CurrentCulture);

            DateTime visibleDate;

            if (((LinkButton)sender).ID == "ibCalPrev") visibleDate = dt.AddMonths(-1);
            else visibleDate = dt.AddMonths(1);

            CreateCalendar(visibleDate);
            CurrentMonth = visibleDate.ToString("dd.MM.yyyy");

        }


		private string SelectedDate
		{
			get 
			{
				if (this.ViewState["SelectedDate"]==null) return String.Empty;
				else return (string)this.ViewState["SelectedDate"];
			}
			set 
			{
				this.ViewState["SelectedDate"] = value;
			}

		}

		private string CurrentMonth
		{
			get 
			{
				if (this.ViewState["CurrentMonth"]==null) 
				{
					return new DateTime(DateTime.Now.Year, DateTime.Now.Month, 1).ToString("dd.MM.yyyy");
				}
				else
				{
					return (string)this.ViewState["CurrentMonth"];
				}
			}
			set 
			{
				this.ViewState["CurrentMonth"] = value;
			}

		}

        private ArrayList LoadEnteredDates(DateTime startDate, DateTime endDate)
        {
            ArrayList al = new ArrayList();
            SqlDataReader reader = null;
            try
            {
                reader = WorkTimesData.SelectedEnteredDays(int.Parse(ddlUsers.SelectedValue), startDate, endDate);
                while (reader.Read())
                {
                    al.Add(reader.GetDateTime(0));
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                if (reader != null) reader.Close();
            }

            return al;
        }
		#endregion

		#region Util

        private void SetMessageStrings()
        {
            txtIncorrectDataMsg.Value = String.Concat(Resource.ResourceManager["hours_ErrorEmptyProjectField"], ";",
                Resource.ResourceManager["hours_ErrorMustHaveActivity"], ";",
                Resource.ResourceManager["hours_ErrorMustNotHaveActivity"], ";",
                Resource.ResourceManager["hours_ErrorTimesOverlap"], ";",
                Resource.ResourceManager["hours_ErrorTimeEmpty"], ";",
                Resource.ResourceManager["hours_WarningAllOtherDeleted"]);
        }

        public static ArrayList ParseSelectedUsers(string input)
        {
            string[] users = input.Split(';');
            ArrayList userIDs = new ArrayList();

            for (int i = 0; i < users.Length; i++)
            {
                try
                {
                    int id = int.Parse(users[i].Split(':')[0]);
                    userIDs.Add(id);
                }
                catch { }

            }

            return userIDs;
        }

		#endregion		

        private void btnCopy_Click(object sender, System.EventArgs e)
        {
            //if (SaveWorkTimes()) BindGrid();
            // ClientScript.RegisterStartupScript("", "<script>OpenCopyWindow()</script>");
        }
        public void ddlProjectActive_SelectedIndexChanged(object sender, EventArgs e)
        {
            DropDownList ddl = (DropDownList)sender;
            int j = ddl.TabIndex;
            DropDownList grdProjects = (DropDownList)GridHours.Items[j].Cells[(int)GridColumns.ProjectAndActivity].FindControl("ddlProject");
            DropDownList ddlBuildingTypes = (DropDownList)GridHours.Items[j].Cells[(int)GridColumns.ProjectAndActivity].FindControl("ddlBuildingTypes");
            bool IsBuildingTypeGradoustrShow = false;
            if (ddl.SelectedValue == System.Configuration.ConfigurationManager.AppSettings["BuildingTypeID"])
                IsBuildingTypeGradoustrShow = true;
            SqlDataReader reader = null;
            try
            {
                reader = ProjectsData.SelectProjects(1, int.Parse(ddl.SelectedValue), null, int.Parse(ddlBuildingTypes.SelectedValue)/*, false*/, (!LoggedUser.IsAccountant || LoggedUser.IsAccountantLow), -1, -1, IsBuildingTypeGradoustrShow);
                if (reader != null)
                {
                    grdProjects.DataSource = reader;

                    grdProjects.DataBind();
                    ListItem li = new ListItem("", "0");
                    grdProjects.Items.Insert(0, li);

                }
            }
            catch (Exception ex)
            {
                log.Info(ex);
                lblError.Text = Resource.ResourceManager["projects_ErrorLoadProjects"];
                return;
            }
            finally
            {
                if (reader != null) reader.Close();
            }

        }
        public void ddl_SelectedIndexChanged(object sender, EventArgs e)
        {
            DropDownList ddl = (DropDownList)sender;
            int j = ddl.TabIndex;
            DropDownList grdProjects = (DropDownList)GridHours.Items[j].Cells[(int)GridColumns.ProjectAndActivity].FindControl("ddlProject");
            bool IsBuildingTypeGradoustrShow = false;
            if (ddl.SelectedValue == System.Configuration.ConfigurationManager.AppSettings["BuildingTypeID"])
                IsBuildingTypeGradoustrShow = true;
            SqlDataReader reader = null;
            try
            {
                reader = ProjectsData.SelectProjects(1, (int)ProjectsData.ProjectsByStatus.Active, null, int.Parse(ddl.SelectedValue)/*, false*/, (!LoggedUser.IsAccountant || LoggedUser.IsAccountantLow), -1, -1, IsBuildingTypeGradoustrShow);
                if (reader != null)
                {
                    grdProjects.DataSource = reader;

                    grdProjects.DataBind();
                    ListItem li = new ListItem("", "0");
                    grdProjects.Items.Insert(0, li);

                }
            }
            catch (Exception ex)
            {
                log.Info(ex);
                lblError.Text = Resource.ResourceManager["projects_ErrorLoadProjects"];
                return;
            }
            finally
            {
                if (reader != null) reader.Close();
            }


        }

        private void GridHours_ItemCreated(object sender, System.Web.UI.WebControls.DataGridItemEventArgs e)
        {

        }

        private void ddlSearch_SelectedIndexChanged(object sender, System.EventArgs e)
        {

        }

        private void ddlBuildingTypes_SelectedIndexChanged(object sender, System.EventArgs e)
        {

        }
	}
}
