using System;
using System.Data;
using System.Collections;
using System.Data.SqlClient;

namespace Asa.Timesheet.Data
{
	/// <summary>
	/// Summary description for MeetingProjectUDL.
	/// </summary>
	public class MeetingProjectUDL
	{
		
			private static SqlCommand CreateMeetingProjectsDelByMeetingProc (int MeetingID)
			{
				SqlCommand cmd = new SqlCommand();
				cmd.CommandType = System.Data.CommandType.StoredProcedure;
				cmd.CommandText = "MeetingProjectsDelByMeetingProc";
				cmd.Parameters.Add(new System.Data.SqlClient.SqlParameter("@MeetingID", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.Input, true, 10, 0, "MeetingID", DataRowVersion.Current, null));
				if(MeetingID>0)
					cmd.Parameters["@MeetingID"].Value = MeetingID;

				return cmd;
			}

		
		public static int ExecuteMeetingProjectsDelByMeetingProc(int MeetingID)
		{
			int RetObj;
			System.Data.SqlClient.SqlCommand cmd;
			cmd = CreateMeetingProjectsDelByMeetingProc(MeetingID);
			SqlConnection cnDAL = new SqlConnection();

			try
			{
				cnDAL =  CommonDAL.OpenConnection();
				cmd.Connection = cnDAL;
				RetObj = cmd.ExecuteNonQuery();
				return RetObj;
		
			}
			catch(SqlException ex)
			{

				throw new ApplicationException("MeetingProjectsDelByMeetingProc",ex);
			}

			finally
			{
				CommonDAL.CloseConnection(cnDAL);
			}
		}
	}
	
}
