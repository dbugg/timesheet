using System;
using Asa.Timesheet.Data;
using System.Data;
using GrapeCity.ActiveReports;
using GrapeCity.ActiveReports.Controls;
using GrapeCity.ActiveReports.SectionReportModel;
using GrapeCity.ActiveReports.Document.Section;
using GrapeCity.ActiveReports.Document;

namespace Asa.Timesheet.WebPages.Reports
{
    public class MinutesTypeAN_SUB2 : GrapeCity.ActiveReports.SectionReport
	{
		//created by Ivailo date: 05.12.2007
		//notes:Subreport of MinutesTypeAN_main

		//notes: if this is add to style => bold
		private const string _StyleBold =  "font-weight: bold; ";

        public MinutesTypeAN_SUB2(DataView dv)
        {
            this.DataSource = dv;
            InitializeComponent();
        }

        private void Detail_Format(object sender, System.EventArgs eArgs)
        {
            //notes: set yes and no from resources 
            //and give no value to date of payment and wheater is payd
            //bold SUM
            if (this.txtID.Text.Length == 0)
            {
                this.txtPayd.Text = "";
                this.txtPaymentDate.Text = "";
                this.TextBox1.Style = string.Concat(this.TextBox1.Style, " ", _StyleBold);
                this.TextBox2.Style = string.Concat(this.TextBox2.Style, " ", _StyleBold);
                this.TextBox3.Style = string.Concat(this.TextBox3.Style, " ", _StyleBold);
            }
            else
            {
                if (this.txtPayd.Text == "1")
                    txtPayd.Text = Resource.ResourceManager["boolYES"];
                else
                    txtPayd.Text = Resource.ResourceManager["boolNO"];

            }
        }

	
		

		#region ActiveReports Designer generated code





























        public void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MinutesTypeAN_SUB2));
            this.Detail = new GrapeCity.ActiveReports.SectionReportModel.Detail();
            this.ReportHeader = new GrapeCity.ActiveReports.SectionReportModel.ReportHeader();
            this.ReportFooter = new GrapeCity.ActiveReports.SectionReportModel.ReportFooter();
            this.Label3 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label2 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label5 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label1 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label4 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Line1 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.Line2 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.Line3 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.Line4 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.Line5 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.Line6 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.Line7 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.Line8 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.TextBox1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.TextBox2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.TextBox3 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtPaymentDate = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtPayd = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtID = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.Line9 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.Line10 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.Line11 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.Line12 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.Line13 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.Line14 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.Line15 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            ((System.ComponentModel.ISupportInitialize)(this.Label3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TextBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TextBox2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TextBox3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPaymentDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPayd)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            // 
            // Detail
            // 
            this.Detail.ColumnSpacing = 0F;
            this.Detail.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
						this.TextBox1,
						this.TextBox2,
						this.TextBox3,
						this.txtPaymentDate,
						this.txtPayd,
						this.txtID,
						this.Line9,
						this.Line10,
						this.Line11,
						this.Line12,
						this.Line13,
						this.Line14,
						this.Line15});
            this.Detail.Height = 0.2F;
            this.Detail.Name = "Detail";
            // 
            // ReportHeader
            // 
            this.ReportHeader.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
						this.Label3,
						this.Label2,
						this.Label5,
						this.Label1,
						this.Label4,
						this.Line1,
						this.Line2,
						this.Line3,
						this.Line4,
						this.Line5,
						this.Line6,
						this.Line7,
						this.Line8});
            this.ReportHeader.Height = 0.2F;
            this.ReportHeader.Name = "ReportHeader";
            // 
            // ReportFooter
            // 
            this.ReportFooter.Height = 0F;
            this.ReportFooter.Name = "ReportFooter";
            // 
            // Label3
            // 
            this.Label3.Height = 0.2F;
            this.Label3.HyperLink = null;
            this.Label3.Left = 5F;
            this.Label3.Name = "Label3";
            this.Label3.Style = "font-weight: bold";
            this.Label3.Text = "???? ???? (BGN)";
            this.Label3.Top = 0F;
            this.Label3.Width = 2F;
            // 
            // Label2
            // 
            this.Label2.Height = 0.2F;
            this.Label2.HyperLink = null;
            this.Label2.Left = 3F;
            this.Label2.Name = "Label2";
            this.Label2.Style = "font-weight: bold";
            this.Label2.Text = "???? ???? (EUR)";
            this.Label2.Top = 0F;
            this.Label2.Width = 2.010417F;
            // 
            // Label5
            // 
            this.Label5.Height = 0.2F;
            this.Label5.HyperLink = null;
            this.Label5.Left = 8.3F;
            this.Label5.Name = "Label5";
            this.Label5.Style = "font-weight: bold";
            this.Label5.Text = "???????";
            this.Label5.Top = 0F;
            this.Label5.Width = 0.7F;
            // 
            // Label1
            // 
            this.Label1.Height = 0.2F;
            this.Label1.HyperLink = null;
            this.Label1.Left = 0F;
            this.Label1.Name = "Label1";
            this.Label1.Style = "font-weight: bold";
            this.Label1.Text = "??????";
            this.Label1.Top = 0F;
            this.Label1.Width = 3F;
            // 
            // Label4
            // 
            this.Label4.Height = 0.2F;
            this.Label4.HyperLink = null;
            this.Label4.Left = 7F;
            this.Label4.Name = "Label4";
            this.Label4.Style = "font-weight: bold";
            this.Label4.Text = "???? ?? ???????";
            this.Label4.Top = 0F;
            this.Label4.Width = 1.3F;
            // 
            // Line1
            // 
            this.Line1.Height = 0F;
            this.Line1.Left = 0F;
            this.Line1.LineWeight = 1F;
            this.Line1.Name = "Line1";
            this.Line1.Top = 0F;
            this.Line1.Width = 9F;
            this.Line1.X1 = 0F;
            this.Line1.X2 = 9F;
            this.Line1.Y1 = 0F;
            this.Line1.Y2 = 0F;
            // 
            // Line2
            // 
            this.Line2.Height = 0F;
            this.Line2.Left = 0F;
            this.Line2.LineWeight = 1F;
            this.Line2.Name = "Line2";
            this.Line2.Top = 0.2F;
            this.Line2.Width = 9F;
            this.Line2.X1 = 0F;
            this.Line2.X2 = 9F;
            this.Line2.Y1 = 0.2F;
            this.Line2.Y2 = 0.2F;
            // 
            // Line3
            // 
            this.Line3.Height = 0.2F;
            this.Line3.Left = 0F;
            this.Line3.LineWeight = 1F;
            this.Line3.Name = "Line3";
            this.Line3.Top = 0F;
            this.Line3.Width = 0F;
            this.Line3.X1 = 0F;
            this.Line3.X2 = 0F;
            this.Line3.Y1 = 0F;
            this.Line3.Y2 = 0.2F;
            // 
            // Line4
            // 
            this.Line4.Height = 0.2F;
            this.Line4.Left = 5F;
            this.Line4.LineWeight = 1F;
            this.Line4.Name = "Line4";
            this.Line4.Top = 0F;
            this.Line4.Width = 0F;
            this.Line4.X1 = 5F;
            this.Line4.X2 = 5F;
            this.Line4.Y1 = 0F;
            this.Line4.Y2 = 0.2F;
            // 
            // Line5
            // 
            this.Line5.Height = 0.2F;
            this.Line5.Left = 7F;
            this.Line5.LineWeight = 1F;
            this.Line5.Name = "Line5";
            this.Line5.Top = 0F;
            this.Line5.Width = 0F;
            this.Line5.X1 = 7F;
            this.Line5.X2 = 7F;
            this.Line5.Y1 = 0F;
            this.Line5.Y2 = 0.2F;
            // 
            // Line6
            // 
            this.Line6.Height = 0.2F;
            this.Line6.Left = 8.3F;
            this.Line6.LineWeight = 1F;
            this.Line6.Name = "Line6";
            this.Line6.Top = 0F;
            this.Line6.Width = 0F;
            this.Line6.X1 = 8.3F;
            this.Line6.X2 = 8.3F;
            this.Line6.Y1 = 0F;
            this.Line6.Y2 = 0.2F;
            // 
            // Line7
            // 
            this.Line7.Height = 0.2F;
            this.Line7.Left = 9F;
            this.Line7.LineWeight = 1F;
            this.Line7.Name = "Line7";
            this.Line7.Top = 0F;
            this.Line7.Width = 0F;
            this.Line7.X1 = 9F;
            this.Line7.X2 = 9F;
            this.Line7.Y1 = 0F;
            this.Line7.Y2 = 0.2F;
            // 
            // Line8
            // 
            this.Line8.Height = 0.2F;
            this.Line8.Left = 3F;
            this.Line8.LineWeight = 1F;
            this.Line8.Name = "Line8";
            this.Line8.Top = 0F;
            this.Line8.Width = 0F;
            this.Line8.X1 = 3F;
            this.Line8.X2 = 3F;
            this.Line8.Y1 = 0F;
            this.Line8.Y2 = 0.2F;
            // 
            // TextBox1
            // 
            this.TextBox1.DataField = "projectname";
            this.TextBox1.Height = 0.2F;
            this.TextBox1.Left = 0F;
            this.TextBox1.Name = "TextBox1";
            this.TextBox1.Top = 0F;
            this.TextBox1.Width = 3F;
            // 
            // TextBox2
            // 
            this.TextBox2.DataField = "Amount";
            this.TextBox2.Height = 0.2F;
            this.TextBox2.Left = 3F;
            this.TextBox2.Name = "TextBox2";
            this.TextBox2.OutputFormat = resources.GetString("TextBox2.OutputFormat");
            this.TextBox2.Style = "text-align: center";
            this.TextBox2.Top = 0F;
            this.TextBox2.Width = 2F;
            // 
            // TextBox3
            // 
            this.TextBox3.DataField = "AmountBGN";
            this.TextBox3.Height = 0.2F;
            this.TextBox3.Left = 5F;
            this.TextBox3.Name = "TextBox3";
            this.TextBox3.OutputFormat = resources.GetString("TextBox3.OutputFormat");
            this.TextBox3.Style = "text-align: center";
            this.TextBox3.Top = 0F;
            this.TextBox3.Width = 2F;
            // 
            // txtPaymentDate
            // 
            this.txtPaymentDate.DataField = "PaymentDate";
            this.txtPaymentDate.Height = 0.2F;
            this.txtPaymentDate.Left = 7F;
            this.txtPaymentDate.Name = "txtPaymentDate";
            this.txtPaymentDate.OutputFormat = resources.GetString("txtPaymentDate.OutputFormat");
            this.txtPaymentDate.Style = "text-align: center";
            this.txtPaymentDate.Top = 0F;
            this.txtPaymentDate.Width = 1.3F;
            // 
            // txtPayd
            // 
            this.txtPayd.DataField = "Done";
            this.txtPayd.Height = 0.2F;
            this.txtPayd.Left = 8.3F;
            this.txtPayd.Name = "txtPayd";
            this.txtPayd.Style = "text-align: center";
            this.txtPayd.Top = 0F;
            this.txtPayd.Width = 0.7F;
            // 
            // txtID
            // 
            this.txtID.DataField = "PaymentID";
            this.txtID.Height = 0.2F;
            this.txtID.Left = 0F;
            this.txtID.Name = "txtID";
            this.txtID.Top = 0F;
            this.txtID.Visible = false;
            this.txtID.Width = 0.125F;
            // 
            // Line9
            // 
            this.Line9.Height = 0F;
            this.Line9.Left = 0F;
            this.Line9.LineWeight = 1F;
            this.Line9.Name = "Line9";
            this.Line9.Top = 0.2F;
            this.Line9.Width = 9F;
            this.Line9.X1 = 0F;
            this.Line9.X2 = 9F;
            this.Line9.Y1 = 0.2F;
            this.Line9.Y2 = 0.2F;
            // 
            // Line10
            // 
            this.Line10.AnchorBottom = true;
            this.Line10.Height = 0.2F;
            this.Line10.Left = 3F;
            this.Line10.LineWeight = 1F;
            this.Line10.Name = "Line10";
            this.Line10.Top = 0F;
            this.Line10.Width = 0F;
            this.Line10.X1 = 3F;
            this.Line10.X2 = 3F;
            this.Line10.Y1 = 0.2F;
            this.Line10.Y2 = 0F;
            // 
            // Line11
            // 
            this.Line11.AnchorBottom = true;
            this.Line11.Height = 0.2F;
            this.Line11.Left = 5F;
            this.Line11.LineWeight = 1F;
            this.Line11.Name = "Line11";
            this.Line11.Top = 0F;
            this.Line11.Width = 0F;
            this.Line11.X1 = 5F;
            this.Line11.X2 = 5F;
            this.Line11.Y1 = 0.2F;
            this.Line11.Y2 = 0F;
            // 
            // Line12
            // 
            this.Line12.AnchorBottom = true;
            this.Line12.Height = 0.2F;
            this.Line12.Left = 0F;
            this.Line12.LineWeight = 1F;
            this.Line12.Name = "Line12";
            this.Line12.Top = 0F;
            this.Line12.Width = 0F;
            this.Line12.X1 = 0F;
            this.Line12.X2 = 0F;
            this.Line12.Y1 = 0.2F;
            this.Line12.Y2 = 0F;
            // 
            // Line13
            // 
            this.Line13.AnchorBottom = true;
            this.Line13.Height = 0.2F;
            this.Line13.Left = 7F;
            this.Line13.LineWeight = 1F;
            this.Line13.Name = "Line13";
            this.Line13.Top = 0F;
            this.Line13.Width = 0F;
            this.Line13.X1 = 7F;
            this.Line13.X2 = 7F;
            this.Line13.Y1 = 0.2F;
            this.Line13.Y2 = 0F;
            // 
            // Line14
            // 
            this.Line14.AnchorBottom = true;
            this.Line14.Height = 0.2F;
            this.Line14.Left = 8.3F;
            this.Line14.LineWeight = 1F;
            this.Line14.Name = "Line14";
            this.Line14.Top = 0F;
            this.Line14.Width = 0F;
            this.Line14.X1 = 8.3F;
            this.Line14.X2 = 8.3F;
            this.Line14.Y1 = 0.2F;
            this.Line14.Y2 = 0F;
            // 
            // Line15
            // 
            this.Line15.AnchorBottom = true;
            this.Line15.Height = 0.2F;
            this.Line15.Left = 9F;
            this.Line15.LineWeight = 1F;
            this.Line15.Name = "Line15";
            this.Line15.Top = 0F;
            this.Line15.Width = 0F;
            this.Line15.X1 = 9F;
            this.Line15.X2 = 9F;
            this.Line15.Y1 = 0.2F;
            this.Line15.Y2 = 0F;
            // 
            // ActiveReport1
            // 
            this.MasterReport = false;
            this.PageSettings.PaperHeight = 11F;
            this.PageSettings.PaperWidth = 8.5F;
            this.PrintWidth = 9.177083F;
            this.Sections.Add(this.ReportHeader);
            this.Sections.Add(this.Detail);
            this.Sections.Add(this.ReportFooter);
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule(resources.GetString("$this.StyleSheet"), "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: inherit; font-style: inherit; font-variant: inherit; font-weight: bo" +
                        "ld; font-size: 16pt; font-size-adjust: inherit; font-stretch: inherit", "Heading1", "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: Times New Roman; font-style: italic; font-variant: inherit; font-wei" +
                        "ght: bold; font-size: 14pt; font-size-adjust: inherit; font-stretch: inherit", "Heading2", "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: inherit; font-style: inherit; font-variant: inherit; font-weight: bo" +
                        "ld; font-size: 13pt; font-size-adjust: inherit; font-stretch: inherit", "Heading3", "Normal"));
            ((System.ComponentModel.ISupportInitialize)(this.Label3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TextBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TextBox2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TextBox3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPaymentDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPayd)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();

            // Attach Report Events
            this.Detail.Format += new System.EventHandler(this.Detail_Format);
        }

		#endregion

        private ReportHeader ReportHeader;
        private Label Label3;
        private Label Label2;
        private Label Label5;
        private Label Label1;
        private Label Label4;
        private Line Line1;
        private Line Line2;
        private Line Line3;
        private Line Line4;
        private Line Line5;
        private Line Line6;
        private Line Line7;
        private Line Line8;
        private Detail Detail;
        private TextBox TextBox1;
        private TextBox TextBox2;
        private TextBox TextBox3;
        private TextBox txtPaymentDate;
        private TextBox txtPayd;
        private TextBox txtID;
        private Line Line9;
        private Line Line10;
        private Line Line11;
        private Line Line12;
        private Line Line13;
        private Line Line14;
        private Line Line15;
        private ReportFooter ReportFooter;
	}
}
