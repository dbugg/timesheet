using System;
using GrapeCity.ActiveReports;
using GrapeCity.ActiveReports.Controls;
using GrapeCity.ActiveReports.SectionReportModel;
using GrapeCity.ActiveReports.Document.Section;
using GrapeCity.ActiveReports.Document;

namespace Asa.Timesheet.WebPages.Reports
{
    public class rptFolder2 : GrapeCity.ActiveReports.SectionReport
	{
        public rptFolder2(string name, string address)
        {
            InitializeComponent();
            lbObekt.Text = lbObekt1.Text = lbObekt2.Text = lbObekt3.Text = lbObekt4.Text = lbObekt5.Text = lbObekt6.Text = name;
            lbAddress.Text = lbAddress1.Text = lbAddress2.Text = lbAddress3.Text = lbAddress4.Text = lbAddress5.Text = lbAddress6.Text = address;
            this.PageSettings.Margins.Top = SectionReport.CmToInch(.3f);
            this.PageSettings.Margins.Bottom = SectionReport.CmToInch(0);
            //this.PageSettings.Gutter = 0F;
            //this.PageSettings.Margins.Left = 0.2f;
            //this.PageSettings.Margins.Right = 0;
            this.PageSettings.Orientation = GrapeCity.ActiveReports.Document.Section.PageOrientation.Landscape;
        }

		#region ActiveReports Designer generated code





































































        public void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(rptFolder2));
            this.Detail = new GrapeCity.ActiveReports.SectionReportModel.Detail();
            this.PageHeader = new GrapeCity.ActiveReports.SectionReportModel.PageHeader();
            this.PageFooter = new GrapeCity.ActiveReports.SectionReportModel.PageFooter();
            this.Picture7 = new GrapeCity.ActiveReports.SectionReportModel.Picture();
            this.Picture6 = new GrapeCity.ActiveReports.SectionReportModel.Picture();
            this.Picture5 = new GrapeCity.ActiveReports.SectionReportModel.Picture();
            this.Picture4 = new GrapeCity.ActiveReports.SectionReportModel.Picture();
            this.Picture3 = new GrapeCity.ActiveReports.SectionReportModel.Picture();
            this.Picture2 = new GrapeCity.ActiveReports.SectionReportModel.Picture();
            this.Picture1 = new GrapeCity.ActiveReports.SectionReportModel.Picture();
            this.Shape1 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
            this.Shape4 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
            this.Shape5 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
            this.Shape2 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
            this.TextBox3 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtTest = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.Label4 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lbObekt = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lbAddress = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label6 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label7 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Shape3 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
            this.Shape6 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
            this.Shape7 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
            this.Shape9 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
            this.Shape10 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
            this.TextBox4 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.TextBox5 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.TextBox6 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.TextBox7 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.TextBox8 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.TextBox9 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.TextBox10 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.TextBox11 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.TextBox12 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.TextBox13 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.TextBox14 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.TextBox15 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.Shape11 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
            this.Label8 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label9 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label10 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label11 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label12 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label13 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lbObekt1 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lbObekt2 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lbObekt3 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lbObekt4 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lbObekt5 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lbObekt6 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lbAddress1 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lbAddress2 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lbAddress3 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lbAddress4 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lbAddress5 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lbAddress6 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label26 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label27 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label28 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label29 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label30 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label31 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label32 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label33 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label34 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label35 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label36 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label37 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            ((System.ComponentModel.ISupportInitialize)(this.Picture7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Picture6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Picture5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Picture4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Picture3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Picture2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Picture1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TextBox3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTest)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbObekt)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbAddress)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TextBox4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TextBox5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TextBox6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TextBox7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TextBox8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TextBox9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TextBox10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TextBox11)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TextBox12)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TextBox13)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TextBox14)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TextBox15)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label11)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label12)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label13)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbObekt1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbObekt2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbObekt3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbObekt4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbObekt5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbObekt6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbAddress1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbAddress2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbAddress3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbAddress4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbAddress5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbAddress6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label26)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label27)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label28)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label29)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label30)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label31)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label32)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label33)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label34)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label35)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label36)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label37)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            // 
            // Detail
            // 
            this.Detail.ColumnSpacing = 0F;
            this.Detail.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
						this.Picture7,
						this.Picture6,
						this.Picture5,
						this.Picture4,
						this.Picture3,
						this.Picture2,
						this.Picture1,
						this.Shape1,
						this.Shape4,
						this.Shape5,
						this.Shape2,
						this.TextBox3,
						this.txtTest,
						this.Label4,
						this.lbObekt,
						this.lbAddress,
						this.Label6,
						this.Label7,
						this.Shape3,
						this.Shape6,
						this.Shape7,
						this.Shape9,
						this.Shape10,
						this.TextBox4,
						this.TextBox5,
						this.TextBox6,
						this.TextBox7,
						this.TextBox8,
						this.TextBox9,
						this.TextBox10,
						this.TextBox11,
						this.TextBox12,
						this.TextBox13,
						this.TextBox14,
						this.TextBox15,
						this.Shape11,
						this.Label8,
						this.Label9,
						this.Label10,
						this.Label11,
						this.Label12,
						this.Label13,
						this.lbObekt1,
						this.lbObekt2,
						this.lbObekt3,
						this.lbObekt4,
						this.lbObekt5,
						this.lbObekt6,
						this.lbAddress1,
						this.lbAddress2,
						this.lbAddress3,
						this.lbAddress4,
						this.lbAddress5,
						this.lbAddress6,
						this.Label26,
						this.Label27,
						this.Label28,
						this.Label29,
						this.Label30,
						this.Label31,
						this.Label32,
						this.Label33,
						this.Label34,
						this.Label35,
						this.Label36,
						this.Label37});
            this.Detail.Height = 7.6875F;
            this.Detail.Name = "Detail";
            // 
            // PageHeader
            // 
            this.PageHeader.Height = 0F;
            this.PageHeader.Name = "PageHeader";
            // 
            // PageFooter
            // 
            this.PageFooter.Height = 0F;
            this.PageFooter.Name = "PageFooter";
            // 
            // Picture7
            // 
            this.Picture7.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.Picture7.Height = 1.181102F;
            this.Picture7.ImageData = ((System.IO.Stream)(resources.GetObject("Picture7.ImageData")));
            this.Picture7.Left = 6.375F;
            this.Picture7.LineColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.Picture7.Name = "Picture7";
            this.Picture7.SizeMode = GrapeCity.ActiveReports.SectionReportModel.SizeModes.Zoom;
            this.Picture7.Top = 0.05208333F;
            this.Picture7.Width = 1.0625F;
            // 
            // Picture6
            // 
            this.Picture6.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.Picture6.Height = 1.181102F;
            this.Picture6.ImageData = ((System.IO.Stream)(resources.GetObject("Picture6.ImageData")));
            this.Picture6.Left = 5.375F;
            this.Picture6.LineColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.Picture6.Name = "Picture6";
            this.Picture6.SizeMode = GrapeCity.ActiveReports.SectionReportModel.SizeModes.Zoom;
            this.Picture6.Top = 0.05208333F;
            this.Picture6.Width = 1.0625F;
            // 
            // Picture5
            // 
            this.Picture5.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.Picture5.Height = 1.181102F;
            this.Picture5.ImageData = ((System.IO.Stream)(resources.GetObject("Picture5.ImageData")));
            this.Picture5.Left = 4.3125F;
            this.Picture5.LineColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.Picture5.Name = "Picture5";
            this.Picture5.SizeMode = GrapeCity.ActiveReports.SectionReportModel.SizeModes.Zoom;
            this.Picture5.Top = 0.05208333F;
            this.Picture5.Width = 1.0625F;
            // 
            // Picture4
            // 
            this.Picture4.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.Picture4.Height = 1.181102F;
            this.Picture4.ImageData = ((System.IO.Stream)(resources.GetObject("Picture4.ImageData")));
            this.Picture4.Left = 3.25F;
            this.Picture4.LineColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.Picture4.Name = "Picture4";
            this.Picture4.SizeMode = GrapeCity.ActiveReports.SectionReportModel.SizeModes.Zoom;
            this.Picture4.Top = 0.05208333F;
            this.Picture4.Width = 1.0625F;
            // 
            // Picture3
            // 
            this.Picture3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.Picture3.Height = 1.181102F;
            this.Picture3.ImageData = ((System.IO.Stream)(resources.GetObject("Picture3.ImageData")));
            this.Picture3.Left = 2.1875F;
            this.Picture3.LineColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.Picture3.Name = "Picture3";
            this.Picture3.SizeMode = GrapeCity.ActiveReports.SectionReportModel.SizeModes.Zoom;
            this.Picture3.Top = 0.05208333F;
            this.Picture3.Width = 1.0625F;
            // 
            // Picture2
            // 
            this.Picture2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.Picture2.Height = 1.181102F;
            this.Picture2.ImageData = ((System.IO.Stream)(resources.GetObject("Picture2.ImageData")));
            this.Picture2.Left = 1.0625F;
            this.Picture2.LineColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.Picture2.Name = "Picture2";
            this.Picture2.SizeMode = GrapeCity.ActiveReports.SectionReportModel.SizeModes.Zoom;
            this.Picture2.Top = 0.05208333F;
            this.Picture2.Width = 1.0625F;
            // 
            // Picture1
            // 
            this.Picture1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.Picture1.Height = 1.181102F;
            this.Picture1.ImageData = ((System.IO.Stream)(resources.GetObject("Picture1.ImageData")));
            this.Picture1.Left = 0F;
            this.Picture1.LineColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.Picture1.Name = "Picture1";
            this.Picture1.SizeMode = GrapeCity.ActiveReports.SectionReportModel.SizeModes.Zoom;
            this.Picture1.Top = 0.05208333F;
            this.Picture1.Width = 1.0625F;
            // 
            // Shape1
            // 
            this.Shape1.Height = 7.6875F;
            this.Shape1.Left = 0F;
            this.Shape1.Name = "Shape1";
            this.Shape1.RoundingRadius = 9.999999F;
            this.Shape1.Top = 0F;
            this.Shape1.Width = 1.0625F;
            // 
            // Shape4
            // 
            this.Shape4.Height = 7.6875F;
            this.Shape4.Left = 0F;
            this.Shape4.Name = "Shape4";
            this.Shape4.RoundingRadius = 9.999999F;
            this.Shape4.Top = 0F;
            this.Shape4.Width = 1.0625F;
            // 
            // Shape5
            // 
            this.Shape5.Height = 7.6875F;
            this.Shape5.Left = 0F;
            this.Shape5.Name = "Shape5";
            this.Shape5.RoundingRadius = 9.999999F;
            this.Shape5.Top = 0F;
            this.Shape5.Width = 1.0625F;
            // 
            // Shape2
            // 
            this.Shape2.Height = 4F;
            this.Shape2.Left = 0F;
            this.Shape2.Name = "Shape2";
            this.Shape2.RoundingRadius = 9.999999F;
            this.Shape2.Top = 1.3125F;
            this.Shape2.Width = 7.4375F;
            // 
            // TextBox3
            // 
            this.TextBox3.Height = 0.3187352F;
            this.TextBox3.Left = 0F;
            this.TextBox3.Name = "TextBox3";
            this.TextBox3.Style = "color: Black; font-family: Arial; font-size: 8pt; font-weight: bold; text-align: " +
                "right; white-space: inherit";
            this.TextBox3.Text = "Atelier Serafimov Architects";
            this.TextBox3.Top = 5.299048F;
            this.TextBox3.Width = 1.023622F;
            // 
            // txtTest
            // 
            this.txtTest.Height = 2.105395F;
            this.txtTest.Left = 0F;
            this.txtTest.Name = "txtTest";
            this.txtTest.Style = "color: Black; font-size: 8pt; text-align: right";
            this.txtTest.Text = resources.GetString("txtTest.Text");
            this.txtTest.Top = 5.582105F;
            this.txtTest.Width = 1.023622F;
            // 
            // Label4
            // 
            this.Label4.Angle = 900;
            this.Label4.Height = 0.5F;
            this.Label4.HyperLink = null;
            this.Label4.Left = 0F;
            this.Label4.Name = "Label4";
            this.Label4.Style = "font-family: Arial; font-size: 9pt; font-style: normal; font-weight: bold; vertic" +
                "al-align: bottom; ddo-char-set: 1";
            this.Label4.Text = "?????:";
            this.Label4.Top = 4.6875F;
            this.Label4.Width = 0.25F;
            // 
            // lbObekt
            // 
            this.lbObekt.Angle = 900;
            this.lbObekt.Height = 3.364583F;
            this.lbObekt.HyperLink = null;
            this.lbObekt.Left = 0F;
            this.lbObekt.Name = "lbObekt";
            this.lbObekt.Style = "font-family: Arial; font-size: 9pt; font-style: normal; font-weight: bold; text-a" +
                "lign: left; vertical-align: bottom; ddo-char-set: 1";
            this.lbObekt.Text = "";
            this.lbObekt.Top = 1.3125F;
            this.lbObekt.Width = 0.25F;
            // 
            // lbAddress
            // 
            this.lbAddress.Angle = 900;
            this.lbAddress.Height = 3.375F;
            this.lbAddress.HyperLink = null;
            this.lbAddress.Left = 0.25F;
            this.lbAddress.Name = "lbAddress";
            this.lbAddress.Style = "font-family: Arial; font-size: 9pt; font-style: normal; font-weight: normal; text" +
                "-align: left; vertical-align: bottom; ddo-char-set: 1";
            this.lbAddress.Text = "";
            this.lbAddress.Top = 1.3125F;
            this.lbAddress.Width = 0.25F;
            // 
            // Label6
            // 
            this.Label6.Angle = 900;
            this.Label6.Height = 3.375F;
            this.Label6.HyperLink = null;
            this.Label6.Left = 0.5F;
            this.Label6.Name = "Label6";
            this.Label6.Style = "font-family: Arial; font-size: 9pt; font-style: normal; font-weight: bold; vertic" +
                "al-align: bottom; ddo-char-set: 1";
            this.Label6.Text = "???????????";
            this.Label6.Top = 1.3125F;
            this.Label6.Width = 0.25F;
            // 
            // Label7
            // 
            this.Label7.Angle = 900;
            this.Label7.Height = 0.5F;
            this.Label7.HyperLink = null;
            this.Label7.Left = 0.5F;
            this.Label7.Name = "Label7";
            this.Label7.Style = "font-family: Arial; font-size: 9pt; font-style: normal; font-weight: bold; vertic" +
                "al-align: bottom; ddo-char-set: 1";
            this.Label7.Text = "????:";
            this.Label7.Top = 4.6875F;
            this.Label7.Width = 0.25F;
            // 
            // Shape3
            // 
            this.Shape3.Height = 7.6875F;
            this.Shape3.Left = 2.126F;
            this.Shape3.Name = "Shape3";
            this.Shape3.RoundingRadius = 9.999999F;
            this.Shape3.Top = 0F;
            this.Shape3.Width = 1.0625F;
            // 
            // Shape6
            // 
            this.Shape6.Height = 7.6875F;
            this.Shape6.Left = 3.189F;
            this.Shape6.Name = "Shape6";
            this.Shape6.RoundingRadius = 9.999999F;
            this.Shape6.Top = 0F;
            this.Shape6.Width = 1.0625F;
            // 
            // Shape7
            // 
            this.Shape7.Height = 7.6875F;
            this.Shape7.Left = 4.252F;
            this.Shape7.Name = "Shape7";
            this.Shape7.RoundingRadius = 9.999999F;
            this.Shape7.Top = 0F;
            this.Shape7.Width = 1.0625F;
            // 
            // Shape9
            // 
            this.Shape9.Height = 7.6875F;
            this.Shape9.Left = 5.315F;
            this.Shape9.Name = "Shape9";
            this.Shape9.RoundingRadius = 9.999999F;
            this.Shape9.Top = 0F;
            this.Shape9.Width = 1.0625F;
            // 
            // Shape10
            // 
            this.Shape10.Height = 7.6875F;
            this.Shape10.Left = 6.377999F;
            this.Shape10.Name = "Shape10";
            this.Shape10.RoundingRadius = 9.999999F;
            this.Shape10.Top = 0F;
            this.Shape10.Width = 1.0625F;
            // 
            // TextBox4
            // 
            this.TextBox4.Height = 0.3187352F;
            this.TextBox4.Left = 1.0625F;
            this.TextBox4.Name = "TextBox4";
            this.TextBox4.Style = "color: Black; font-family: Arial; font-size: 8pt; font-weight: bold; text-align: " +
                "right; white-space: inherit";
            this.TextBox4.Text = "Atelier Serafimov Architects";
            this.TextBox4.Top = 5.299048F;
            this.TextBox4.Width = 1.023622F;
            // 
            // TextBox5
            // 
            this.TextBox5.Height = 0.3187352F;
            this.TextBox5.Left = 2.125F;
            this.TextBox5.Name = "TextBox5";
            this.TextBox5.Style = "color: Black; font-family: Arial; font-size: 8pt; font-weight: bold; text-align: " +
                "right; white-space: inherit";
            this.TextBox5.Text = "Atelier Serafimov Architects";
            this.TextBox5.Top = 5.299048F;
            this.TextBox5.Width = 1.023622F;
            // 
            // TextBox6
            // 
            this.TextBox6.Height = 0.3187352F;
            this.TextBox6.Left = 3.1875F;
            this.TextBox6.Name = "TextBox6";
            this.TextBox6.Style = "color: Black; font-family: Arial; font-size: 8pt; font-weight: bold; text-align: " +
                "right; white-space: inherit";
            this.TextBox6.Text = "Atelier Serafimov Architects";
            this.TextBox6.Top = 5.299048F;
            this.TextBox6.Width = 1.023622F;
            // 
            // TextBox7
            // 
            this.TextBox7.Height = 0.3187352F;
            this.TextBox7.Left = 4.25F;
            this.TextBox7.Name = "TextBox7";
            this.TextBox7.Style = "color: Black; font-family: Arial; font-size: 8pt; font-weight: bold; text-align: " +
                "right; white-space: inherit";
            this.TextBox7.Text = "Atelier Serafimov Architects";
            this.TextBox7.Top = 5.299048F;
            this.TextBox7.Width = 1.023622F;
            // 
            // TextBox8
            // 
            this.TextBox8.Height = 0.3187352F;
            this.TextBox8.Left = 5.3125F;
            this.TextBox8.Name = "TextBox8";
            this.TextBox8.Style = "color: Black; font-family: Arial; font-size: 8pt; font-weight: bold; text-align: " +
                "right; white-space: inherit";
            this.TextBox8.Text = "Atelier Serafimov Architects";
            this.TextBox8.Top = 5.299048F;
            this.TextBox8.Width = 1.023622F;
            // 
            // TextBox9
            // 
            this.TextBox9.Height = 0.3187352F;
            this.TextBox9.Left = 6.375F;
            this.TextBox9.Name = "TextBox9";
            this.TextBox9.Style = "color: Black; font-family: Arial; font-size: 8pt; font-weight: bold; text-align: " +
                "right; white-space: inherit";
            this.TextBox9.Text = "Atelier Serafimov Architects";
            this.TextBox9.Top = 5.299048F;
            this.TextBox9.Width = 1.023622F;
            // 
            // TextBox10
            // 
            this.TextBox10.Height = 2.105395F;
            this.TextBox10.Left = 1.0625F;
            this.TextBox10.Name = "TextBox10";
            this.TextBox10.Style = "color: Black; font-size: 8pt; text-align: right";
            this.TextBox10.Text = resources.GetString("TextBox10.Text");
            this.TextBox10.Top = 5.582105F;
            this.TextBox10.Width = 1.023622F;
            // 
            // TextBox11
            // 
            this.TextBox11.Height = 2.105395F;
            this.TextBox11.Left = 2.17F;
            this.TextBox11.Name = "TextBox11";
            this.TextBox11.Style = "color: Black; font-size: 8pt; text-align: right";
            this.TextBox11.Text = resources.GetString("TextBox11.Text");
            this.TextBox11.Top = 5.582105F;
            this.TextBox11.Width = 1.023622F;
            // 
            // TextBox12
            // 
            this.TextBox12.Height = 2.105395F;
            this.TextBox12.Left = 3.2F;
            this.TextBox12.Name = "TextBox12";
            this.TextBox12.Style = "color: Black; font-size: 8pt; text-align: right";
            this.TextBox12.Text = resources.GetString("TextBox12.Text");
            this.TextBox12.Top = 5.582105F;
            this.TextBox12.Width = 1.023622F;
            // 
            // TextBox13
            // 
            this.TextBox13.Height = 2.105395F;
            this.TextBox13.Left = 4.29F;
            this.TextBox13.Name = "TextBox13";
            this.TextBox13.Style = "color: Black; font-size: 8pt; text-align: right";
            this.TextBox13.Text = resources.GetString("TextBox13.Text");
            this.TextBox13.Top = 5.582105F;
            this.TextBox13.Width = 1.023622F;
            // 
            // TextBox14
            // 
            this.TextBox14.Height = 2.105395F;
            this.TextBox14.Left = 5.35F;
            this.TextBox14.Name = "TextBox14";
            this.TextBox14.Style = "color: Black; font-size: 8pt; text-align: right";
            this.TextBox14.Text = resources.GetString("TextBox14.Text");
            this.TextBox14.Top = 5.582105F;
            this.TextBox14.Width = 1.023622F;
            // 
            // TextBox15
            // 
            this.TextBox15.Height = 2.105395F;
            this.TextBox15.Left = 6.375F;
            this.TextBox15.Name = "TextBox15";
            this.TextBox15.Style = "color: Black; font-size: 8pt; text-align: right";
            this.TextBox15.Text = resources.GetString("TextBox15.Text");
            this.TextBox15.Top = 5.582105F;
            this.TextBox15.Width = 1.023622F;
            // 
            // Shape11
            // 
            this.Shape11.Height = 7.6875F;
            this.Shape11.Left = 1.0625F;
            this.Shape11.Name = "Shape11";
            this.Shape11.RoundingRadius = 9.999999F;
            this.Shape11.Top = 0F;
            this.Shape11.Width = 1.0625F;
            // 
            // Label8
            // 
            this.Label8.Angle = 900;
            this.Label8.Height = 0.5F;
            this.Label8.HyperLink = null;
            this.Label8.Left = 1.125F;
            this.Label8.Name = "Label8";
            this.Label8.Style = "font-family: Arial; font-size: 9pt; font-style: normal; font-weight: bold; vertic" +
                "al-align: bottom; ddo-char-set: 1";
            this.Label8.Text = "?????:";
            this.Label8.Top = 4.6875F;
            this.Label8.Width = 0.25F;
            // 
            // Label9
            // 
            this.Label9.Angle = 900;
            this.Label9.Height = 0.5F;
            this.Label9.HyperLink = null;
            this.Label9.Left = 2.1875F;
            this.Label9.Name = "Label9";
            this.Label9.Style = "font-family: Arial; font-size: 9pt; font-style: normal; font-weight: bold; vertic" +
                "al-align: bottom; ddo-char-set: 1";
            this.Label9.Text = "?????:";
            this.Label9.Top = 4.6875F;
            this.Label9.Width = 0.25F;
            // 
            // Label10
            // 
            this.Label10.Angle = 900;
            this.Label10.Height = 0.5F;
            this.Label10.HyperLink = null;
            this.Label10.Left = 3.25F;
            this.Label10.Name = "Label10";
            this.Label10.Style = "font-family: Arial; font-size: 9pt; font-style: normal; font-weight: bold; vertic" +
                "al-align: bottom; ddo-char-set: 1";
            this.Label10.Text = "?????:";
            this.Label10.Top = 4.6875F;
            this.Label10.Width = 0.25F;
            // 
            // Label11
            // 
            this.Label11.Angle = 900;
            this.Label11.Height = 0.5F;
            this.Label11.HyperLink = null;
            this.Label11.Left = 4.375F;
            this.Label11.Name = "Label11";
            this.Label11.Style = "font-family: Arial; font-size: 9pt; font-style: normal; font-weight: bold; vertic" +
                "al-align: bottom; ddo-char-set: 1";
            this.Label11.Text = "?????:";
            this.Label11.Top = 4.6875F;
            this.Label11.Width = 0.25F;
            // 
            // Label12
            // 
            this.Label12.Angle = 900;
            this.Label12.Height = 0.5F;
            this.Label12.HyperLink = null;
            this.Label12.Left = 5.4375F;
            this.Label12.Name = "Label12";
            this.Label12.Style = "font-family: Arial; font-size: 9pt; font-style: normal; font-weight: bold; vertic" +
                "al-align: bottom; ddo-char-set: 1";
            this.Label12.Text = "?????:";
            this.Label12.Top = 4.6875F;
            this.Label12.Width = 0.25F;
            // 
            // Label13
            // 
            this.Label13.Angle = 900;
            this.Label13.Height = 0.5F;
            this.Label13.HyperLink = null;
            this.Label13.Left = 6.5625F;
            this.Label13.Name = "Label13";
            this.Label13.Style = "font-family: Arial; font-size: 9pt; font-style: normal; font-weight: bold; vertic" +
                "al-align: bottom; ddo-char-set: 1";
            this.Label13.Text = "?????:";
            this.Label13.Top = 4.6875F;
            this.Label13.Width = 0.25F;
            // 
            // lbObekt1
            // 
            this.lbObekt1.Angle = 900;
            this.lbObekt1.Height = 3.364583F;
            this.lbObekt1.HyperLink = null;
            this.lbObekt1.Left = 1.125F;
            this.lbObekt1.Name = "lbObekt1";
            this.lbObekt1.Style = "font-family: Arial; font-size: 9pt; font-style: normal; font-weight: bold; text-a" +
                "lign: left; vertical-align: bottom; ddo-char-set: 1";
            this.lbObekt1.Text = "";
            this.lbObekt1.Top = 1.3125F;
            this.lbObekt1.Width = 0.25F;
            // 
            // lbObekt2
            // 
            this.lbObekt2.Angle = 900;
            this.lbObekt2.Height = 3.364583F;
            this.lbObekt2.HyperLink = null;
            this.lbObekt2.Left = 2.1875F;
            this.lbObekt2.Name = "lbObekt2";
            this.lbObekt2.Style = "font-family: Arial; font-size: 9pt; font-style: normal; font-weight: bold; text-a" +
                "lign: left; vertical-align: bottom; ddo-char-set: 1";
            this.lbObekt2.Text = "";
            this.lbObekt2.Top = 1.3125F;
            this.lbObekt2.Width = 0.25F;
            // 
            // lbObekt3
            // 
            this.lbObekt3.Angle = 900;
            this.lbObekt3.Height = 3.364583F;
            this.lbObekt3.HyperLink = null;
            this.lbObekt3.Left = 3.25F;
            this.lbObekt3.Name = "lbObekt3";
            this.lbObekt3.Style = "font-family: Arial; font-size: 9pt; font-style: normal; font-weight: bold; text-a" +
                "lign: left; vertical-align: bottom; ddo-char-set: 1";
            this.lbObekt3.Text = "";
            this.lbObekt3.Top = 1.3125F;
            this.lbObekt3.Width = 0.25F;
            // 
            // lbObekt4
            // 
            this.lbObekt4.Angle = 900;
            this.lbObekt4.Height = 3.364583F;
            this.lbObekt4.HyperLink = null;
            this.lbObekt4.Left = 4.375F;
            this.lbObekt4.Name = "lbObekt4";
            this.lbObekt4.Style = "font-family: Arial; font-size: 9pt; font-style: normal; font-weight: bold; text-a" +
                "lign: left; vertical-align: bottom; ddo-char-set: 1";
            this.lbObekt4.Text = "";
            this.lbObekt4.Top = 1.3125F;
            this.lbObekt4.Width = 0.25F;
            // 
            // lbObekt5
            // 
            this.lbObekt5.Angle = 900;
            this.lbObekt5.Height = 3.364583F;
            this.lbObekt5.HyperLink = null;
            this.lbObekt5.Left = 5.4375F;
            this.lbObekt5.Name = "lbObekt5";
            this.lbObekt5.Style = "font-family: Arial; font-size: 9pt; font-style: normal; font-weight: bold; text-a" +
                "lign: left; vertical-align: bottom; ddo-char-set: 1";
            this.lbObekt5.Text = "";
            this.lbObekt5.Top = 1.3125F;
            this.lbObekt5.Width = 0.25F;
            // 
            // lbObekt6
            // 
            this.lbObekt6.Angle = 900;
            this.lbObekt6.Height = 3.364583F;
            this.lbObekt6.HyperLink = null;
            this.lbObekt6.Left = 6.5625F;
            this.lbObekt6.Name = "lbObekt6";
            this.lbObekt6.Style = "font-family: Arial; font-size: 9pt; font-style: normal; font-weight: bold; text-a" +
                "lign: left; vertical-align: bottom; ddo-char-set: 1";
            this.lbObekt6.Text = "";
            this.lbObekt6.Top = 1.3125F;
            this.lbObekt6.Width = 0.25F;
            // 
            // lbAddress1
            // 
            this.lbAddress1.Angle = 900;
            this.lbAddress1.Height = 3.375F;
            this.lbAddress1.HyperLink = null;
            this.lbAddress1.Left = 1.375F;
            this.lbAddress1.Name = "lbAddress1";
            this.lbAddress1.Style = "font-family: Arial; font-size: 9pt; font-style: normal; font-weight: normal; text" +
                "-align: left; vertical-align: bottom; ddo-char-set: 1";
            this.lbAddress1.Text = "";
            this.lbAddress1.Top = 1.3125F;
            this.lbAddress1.Width = 0.25F;
            // 
            // lbAddress2
            // 
            this.lbAddress2.Angle = 900;
            this.lbAddress2.Height = 3.375F;
            this.lbAddress2.HyperLink = null;
            this.lbAddress2.Left = 2.4375F;
            this.lbAddress2.Name = "lbAddress2";
            this.lbAddress2.Style = "font-family: Arial; font-size: 9pt; font-style: normal; font-weight: normal; text" +
                "-align: left; vertical-align: bottom; ddo-char-set: 1";
            this.lbAddress2.Text = "";
            this.lbAddress2.Top = 1.3125F;
            this.lbAddress2.Width = 0.25F;
            // 
            // lbAddress3
            // 
            this.lbAddress3.Angle = 900;
            this.lbAddress3.Height = 3.375F;
            this.lbAddress3.HyperLink = null;
            this.lbAddress3.Left = 3.5F;
            this.lbAddress3.Name = "lbAddress3";
            this.lbAddress3.Style = "font-family: Arial; font-size: 9pt; font-style: normal; font-weight: normal; text" +
                "-align: left; vertical-align: bottom; ddo-char-set: 1";
            this.lbAddress3.Text = "";
            this.lbAddress3.Top = 1.3125F;
            this.lbAddress3.Width = 0.25F;
            // 
            // lbAddress4
            // 
            this.lbAddress4.Angle = 900;
            this.lbAddress4.Height = 3.375F;
            this.lbAddress4.HyperLink = null;
            this.lbAddress4.Left = 4.625F;
            this.lbAddress4.Name = "lbAddress4";
            this.lbAddress4.Style = "font-family: Arial; font-size: 9pt; font-style: normal; font-weight: normal; text" +
                "-align: left; vertical-align: bottom; ddo-char-set: 1";
            this.lbAddress4.Text = "";
            this.lbAddress4.Top = 1.3125F;
            this.lbAddress4.Width = 0.25F;
            // 
            // lbAddress5
            // 
            this.lbAddress5.Angle = 900;
            this.lbAddress5.Height = 3.375F;
            this.lbAddress5.HyperLink = null;
            this.lbAddress5.Left = 5.6875F;
            this.lbAddress5.Name = "lbAddress5";
            this.lbAddress5.Style = "font-family: Arial; font-size: 9pt; font-style: normal; font-weight: normal; text" +
                "-align: left; vertical-align: bottom; ddo-char-set: 1";
            this.lbAddress5.Text = "";
            this.lbAddress5.Top = 1.3125F;
            this.lbAddress5.Width = 0.25F;
            // 
            // lbAddress6
            // 
            this.lbAddress6.Angle = 900;
            this.lbAddress6.Height = 3.375F;
            this.lbAddress6.HyperLink = null;
            this.lbAddress6.Left = 6.8125F;
            this.lbAddress6.Name = "lbAddress6";
            this.lbAddress6.Style = "font-family: Arial; font-size: 9pt; font-style: normal; font-weight: normal; text" +
                "-align: left; vertical-align: bottom; ddo-char-set: 1";
            this.lbAddress6.Text = "";
            this.lbAddress6.Top = 1.3125F;
            this.lbAddress6.Width = 0.25F;
            // 
            // Label26
            // 
            this.Label26.Angle = 900;
            this.Label26.Height = 3.375F;
            this.Label26.HyperLink = null;
            this.Label26.Left = 1.625F;
            this.Label26.Name = "Label26";
            this.Label26.Style = "font-family: Arial; font-size: 9pt; font-style: normal; font-weight: bold; vertic" +
                "al-align: bottom; ddo-char-set: 1";
            this.Label26.Text = "???????????";
            this.Label26.Top = 1.3125F;
            this.Label26.Width = 0.25F;
            // 
            // Label27
            // 
            this.Label27.Angle = 900;
            this.Label27.Height = 3.375F;
            this.Label27.HyperLink = null;
            this.Label27.Left = 2.6875F;
            this.Label27.Name = "Label27";
            this.Label27.Style = "font-family: Arial; font-size: 9pt; font-style: normal; font-weight: bold; vertic" +
                "al-align: bottom; ddo-char-set: 1";
            this.Label27.Text = "???????????";
            this.Label27.Top = 1.3125F;
            this.Label27.Width = 0.25F;
            // 
            // Label28
            // 
            this.Label28.Angle = 900;
            this.Label28.Height = 3.375F;
            this.Label28.HyperLink = null;
            this.Label28.Left = 3.75F;
            this.Label28.Name = "Label28";
            this.Label28.Style = "font-family: Arial; font-size: 9pt; font-style: normal; font-weight: bold; vertic" +
                "al-align: bottom; ddo-char-set: 1";
            this.Label28.Text = "???????????";
            this.Label28.Top = 1.3125F;
            this.Label28.Width = 0.25F;
            // 
            // Label29
            // 
            this.Label29.Angle = 900;
            this.Label29.Height = 3.375F;
            this.Label29.HyperLink = null;
            this.Label29.Left = 4.875F;
            this.Label29.Name = "Label29";
            this.Label29.Style = "font-family: Arial; font-size: 9pt; font-style: normal; font-weight: bold; vertic" +
                "al-align: bottom; ddo-char-set: 1";
            this.Label29.Text = "???????????";
            this.Label29.Top = 1.3125F;
            this.Label29.Width = 0.25F;
            // 
            // Label30
            // 
            this.Label30.Angle = 900;
            this.Label30.Height = 3.375F;
            this.Label30.HyperLink = null;
            this.Label30.Left = 5.9375F;
            this.Label30.Name = "Label30";
            this.Label30.Style = "font-family: Arial; font-size: 9pt; font-style: normal; font-weight: bold; vertic" +
                "al-align: bottom; ddo-char-set: 1";
            this.Label30.Text = "???????????";
            this.Label30.Top = 1.3125F;
            this.Label30.Width = 0.25F;
            // 
            // Label31
            // 
            this.Label31.Angle = 900;
            this.Label31.Height = 3.375F;
            this.Label31.HyperLink = null;
            this.Label31.Left = 7.0625F;
            this.Label31.Name = "Label31";
            this.Label31.Style = "font-family: Arial; font-size: 9pt; font-style: normal; font-weight: bold; vertic" +
                "al-align: bottom; ddo-char-set: 1";
            this.Label31.Text = "???????????";
            this.Label31.Top = 1.3125F;
            this.Label31.Width = 0.25F;
            // 
            // Label32
            // 
            this.Label32.Angle = 900;
            this.Label32.Height = 0.5F;
            this.Label32.HyperLink = null;
            this.Label32.Left = 1.625F;
            this.Label32.Name = "Label32";
            this.Label32.Style = "font-family: Arial; font-size: 9pt; font-style: normal; font-weight: bold; vertic" +
                "al-align: bottom; ddo-char-set: 1";
            this.Label32.Text = "????:";
            this.Label32.Top = 4.6875F;
            this.Label32.Width = 0.25F;
            // 
            // Label33
            // 
            this.Label33.Angle = 900;
            this.Label33.Height = 0.5F;
            this.Label33.HyperLink = null;
            this.Label33.Left = 2.6875F;
            this.Label33.Name = "Label33";
            this.Label33.Style = "font-family: Arial; font-size: 9pt; font-style: normal; font-weight: bold; vertic" +
                "al-align: bottom; ddo-char-set: 1";
            this.Label33.Text = "????:";
            this.Label33.Top = 4.6875F;
            this.Label33.Width = 0.25F;
            // 
            // Label34
            // 
            this.Label34.Angle = 900;
            this.Label34.Height = 0.5F;
            this.Label34.HyperLink = null;
            this.Label34.Left = 3.75F;
            this.Label34.Name = "Label34";
            this.Label34.Style = "font-family: Arial; font-size: 9pt; font-style: normal; font-weight: bold; vertic" +
                "al-align: bottom; ddo-char-set: 1";
            this.Label34.Text = "????:";
            this.Label34.Top = 4.6875F;
            this.Label34.Width = 0.25F;
            // 
            // Label35
            // 
            this.Label35.Angle = 900;
            this.Label35.Height = 0.5F;
            this.Label35.HyperLink = null;
            this.Label35.Left = 4.875F;
            this.Label35.Name = "Label35";
            this.Label35.Style = "font-family: Arial; font-size: 9pt; font-style: normal; font-weight: bold; vertic" +
                "al-align: bottom; ddo-char-set: 1";
            this.Label35.Text = "????:";
            this.Label35.Top = 4.6875F;
            this.Label35.Width = 0.25F;
            // 
            // Label36
            // 
            this.Label36.Angle = 900;
            this.Label36.Height = 0.5F;
            this.Label36.HyperLink = null;
            this.Label36.Left = 7.0625F;
            this.Label36.Name = "Label36";
            this.Label36.Style = "font-family: Arial; font-size: 9pt; font-style: normal; font-weight: bold; vertic" +
                "al-align: bottom; ddo-char-set: 1";
            this.Label36.Text = "????:";
            this.Label36.Top = 4.6875F;
            this.Label36.Width = 0.25F;
            // 
            // Label37
            // 
            this.Label37.Angle = 900;
            this.Label37.Height = 0.5F;
            this.Label37.HyperLink = null;
            this.Label37.Left = 5.9375F;
            this.Label37.Name = "Label37";
            this.Label37.Style = "font-family: Arial; font-size: 9pt; font-style: normal; font-weight: bold; vertic" +
                "al-align: bottom; ddo-char-set: 1";
            this.Label37.Text = "????:";
            this.Label37.Top = 4.6875F;
            this.Label37.Width = 0.25F;
            // 
            // ActiveReport1
            // 
            this.MasterReport = false;
            this.PageSettings.PaperHeight = 11F;
            this.PageSettings.PaperWidth = 8.5F;
            this.PrintWidth = 7.510417F;
            this.Sections.Add(this.PageHeader);
            this.Sections.Add(this.Detail);
            this.Sections.Add(this.PageFooter);
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule(resources.GetString("$this.StyleSheet"), "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: inherit; font-style: inherit; font-variant: inherit; font-weight: bo" +
                        "ld; font-size: 16pt; font-size-adjust: inherit; font-stretch: inherit", "Heading1", "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: Times New Roman; font-style: italic; font-variant: inherit; font-wei" +
                        "ght: bold; font-size: 14pt; font-size-adjust: inherit; font-stretch: inherit", "Heading2", "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: inherit; font-style: inherit; font-variant: inherit; font-weight: bo" +
                        "ld; font-size: 13pt; font-size-adjust: inherit; font-stretch: inherit", "Heading3", "Normal"));
            ((System.ComponentModel.ISupportInitialize)(this.Picture7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Picture6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Picture5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Picture4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Picture3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Picture2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Picture1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TextBox3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTest)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbObekt)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbAddress)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TextBox4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TextBox5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TextBox6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TextBox7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TextBox8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TextBox9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TextBox10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TextBox11)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TextBox12)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TextBox13)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TextBox14)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TextBox15)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label11)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label12)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label13)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbObekt1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbObekt2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbObekt3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbObekt4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbObekt5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbObekt6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbAddress1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbAddress2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbAddress3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbAddress4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbAddress5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbAddress6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label26)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label27)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label28)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label29)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label30)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label31)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label32)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label33)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label34)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label35)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label36)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label37)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();
        }

		#endregion

        private PageHeader PageHeader;
        private Detail Detail;
        private Picture Picture7;
        private Picture Picture6;
        private Picture Picture5;
        private Picture Picture4;
        private Picture Picture3;
        private Picture Picture2;
        private Picture Picture1;
        private Shape Shape1;
        private Shape Shape4;
        private Shape Shape5;
        private Shape Shape2;
        private TextBox TextBox3;
        private TextBox txtTest;
        private Label Label4;
        private Label lbObekt;
        private Label lbAddress;
        private Label Label6;
        private Label Label7;
        private Shape Shape3;
        private Shape Shape6;
        private Shape Shape7;
        private Shape Shape9;
        private Shape Shape10;
        private TextBox TextBox4;
        private TextBox TextBox5;
        private TextBox TextBox6;
        private TextBox TextBox7;
        private TextBox TextBox8;
        private TextBox TextBox9;
        private TextBox TextBox10;
        private TextBox TextBox11;
        private TextBox TextBox12;
        private TextBox TextBox13;
        private TextBox TextBox14;
        private TextBox TextBox15;
        private Shape Shape11;
        private Label Label8;
        private Label Label9;
        private Label Label10;
        private Label Label11;
        private Label Label12;
        private Label Label13;
        private Label lbObekt1;
        private Label lbObekt2;
        private Label lbObekt3;
        private Label lbObekt4;
        private Label lbObekt5;
        private Label lbObekt6;
        private Label lbAddress1;
        private Label lbAddress2;
        private Label lbAddress3;
        private Label lbAddress4;
        private Label lbAddress5;
        private Label lbAddress6;
        private Label Label26;
        private Label Label27;
        private Label Label28;
        private Label Label29;
        private Label Label30;
        private Label Label31;
        private Label Label32;
        private Label Label33;
        private Label Label34;
        private Label Label35;
        private Label Label36;
        private Label Label37;
        private PageFooter PageFooter;
	}
}
