using System;
using System.Data;
using System.Collections;
using System.Data.SqlClient;
using Microsoft.ApplicationBlocks.Data;

namespace Asa.Timesheet.Data
{
	/// <summary>
	/// Summary description for AbsenceRequestData.
	/// </summary>
	public class AbsenceRequestData
	{
		public static SqlDataReader SelectAbsenceRequest(int AbsenceRequestID)
		{
			string spName = "AbsenceRequestsSelProc";

			SqlParameter[] storedParams = SqlHelperParameterCache.GetSpParameterSet(DBManager.GetConnectionString(), spName);
			storedParams[0].Value = AbsenceRequestID;

			return SqlHelper.ExecuteReader(DBManager.GetConnectionString(), CommandType.StoredProcedure, spName, storedParams);
		}
		public static int InsertAbsenceRequest(DateTime startDate,DateTime endDate,bool decided,int projectID,int userID, int userIDSub)
		{
			string spName = "AbsenceRequestsInsProc";

			SqlParameter[] storedParams = SqlHelperParameterCache.GetSpParameterSet(
				DBManager.GetConnectionString(), spName);		

			storedParams[0].Value = startDate;
			storedParams[1].Value = endDate;
			storedParams[2].Value = decided;
			storedParams[3].Value = projectID;
			storedParams[4].Value = userID;
            storedParams[5].Value = userIDSub;
			SqlHelper.ExecuteNonQuery(DBManager.GetConnectionString(), CommandType.StoredProcedure, spName, storedParams);

			return (int)storedParams[6].Value;
		}
		public static void UpdateAbsenceRequest(int absenceRequestID,DateTime startDate,DateTime endDate,bool decided,int projectID,int userID,bool confirm)
		{
			string spName = "AbsenceRequestsUpdProc";

			SqlParameter[] storedParams = SqlHelperParameterCache.GetSpParameterSet(
				DBManager.GetConnectionString(), spName);		
			storedParams[0].Value = absenceRequestID;
			storedParams[1].Value = startDate;
			storedParams[2].Value = endDate;
			storedParams[3].Value = decided;
			storedParams[4].Value = projectID;
			storedParams[5].Value = userID;
			storedParams[6].Value = confirm;

			SqlHelper.ExecuteNonQuery(DBManager.GetConnectionString(), CommandType.StoredProcedure, spName, storedParams);
		}
        public static SqlDataReader SelectAbsenceSchedulePeopleList()
        {
            string spName = "AbsenceSchedulePeopleList";

            SqlParameter[] storedParams = SqlHelperParameterCache.GetSpParameterSet(
                DBManager.GetConnectionString(), spName);
            DateTime firstDay = DateTime.Now;
            storedParams[0].Value = firstDay;
            storedParams[1].Value = firstDay.AddDays(60).ToString();

            return SqlHelper.ExecuteReader(DBManager.GetConnectionString(), CommandType.StoredProcedure, spName, storedParams);
        }
        public static SqlDataReader SelectAbsenceScheduleAll()
        {
            string spName = "AbsenceSchedule";

            SqlParameter[] storedParams = SqlHelperParameterCache.GetSpParameterSet(
                DBManager.GetConnectionString(), spName);
            DateTime firstDay = DateTime.Now;
            storedParams[0].Value = firstDay;
            storedParams[1].Value = firstDay.AddDays(60).ToString();

            return SqlHelper.ExecuteReader(DBManager.GetConnectionString(), CommandType.StoredProcedure, spName, storedParams);
        }
	}
}
