using System;
using GrapeCity.ActiveReports;
using GrapeCity.ActiveReports.Controls;
using GrapeCity.ActiveReports.SectionReportModel;
using GrapeCity.ActiveReports.Document.Section;
using GrapeCity.ActiveReports.Document;

namespace Asa.Timesheet.Windows.Mail
{
    public class WorkTimesFooter : GrapeCity.ActiveReports.SectionReport
	{
        public WorkTimesFooter(WorkedHoursInfo whi, bool IsOverTime, bool IsTraine)
        {
            InitializeComponent();

            decimal desTotal = 0;
            if (IsOverTime)
                desTotal = whi.TotalHours - whi.TotalHoursInWorkTime;
            else
                desTotal = whi.TotalHours;
            this.txtTotalHours.Text = desTotal.ToString("0.00");


            this.txtWorkDays.Text = whi.TotalHoursInWorkTime.ToString("0.00");
            this.txtHoursNWT.Text = whi.TotalHoursInNonWorkTime.ToString("0.00");
            txtMissingHours.Text = whi.MissingHours.ToString("0.00");
            //this.txtHoursWND.Text = whi.TotalHoursWeekends.ToString("0.00");

            //this.txtWorkDays.Text = whi.TotalWorkDays.ToString();
            this.txtNonWorkDays.Text = whi.TotalHoursInNonWorkTime.ToString("0.00");//whi.TotalNonWorkDays.ToString();


            this.txtDaysIll.Text = whi.DaysIll.ToString();
            if (whi.DaysIll > 0) this.txtDaysIll.Text += ": " + whi.DaysIllString;
            this.txtDaysPO.Text = whi.DaysPlatenOtpusk.ToString();
            if (whi.DaysPlatenOtpusk > 0) this.txtDaysPO.Text += ": " + whi.DaysPlatenOtpuskString;
            this.txtDaysNO.Text = whi.DaysNeplatenOtpusk.ToString();
            if (whi.DaysNeplatenOtpusk > 0) this.txtDaysNO.Text += ": " + whi.DaysNeplatenOtpuskString;
            if (whi.TotalLateCount > 0)
            {
                txtTotalLate.Text = whi.TotalLateCount.ToString();
                txtTotalLateHours.Text = whi.TotalHoursLate;
                txtTotalLate.Visible = txtTotalLateHours.Visible = Label11.Visible = Label12.Visible = true;
            }
            if (IsOverTime)
            {
                txtWorkDays.Visible = txtNonWorkDays.Visible = false;
                Label9.Visible = Label10.Visible = false;
                GroupHeader2.Visible = false;
                GroupHeader3.Visible = false;
                GroupHeader4.Visible = false;
            }
            if (IsTraine)
            {
                GroupHeader4.Visible = GroupHeader3.Visible = false;
                txtTotalLate.Visible = txtTotalLateHours.Visible = Label11.Visible = Label12.Visible = false;
            }
        }

		#region ActiveReports Designer generated code




































        public void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(WorkTimesFooter));
            this.Detail = new GrapeCity.ActiveReports.SectionReportModel.Detail();
            this.GroupHeader1 = new GrapeCity.ActiveReports.SectionReportModel.GroupHeader();
            this.GroupFooter1 = new GrapeCity.ActiveReports.SectionReportModel.GroupFooter();
            this.GroupHeader5 = new GrapeCity.ActiveReports.SectionReportModel.GroupHeader();
            this.GroupFooter5 = new GrapeCity.ActiveReports.SectionReportModel.GroupFooter();
            this.GroupHeader2 = new GrapeCity.ActiveReports.SectionReportModel.GroupHeader();
            this.GroupFooter2 = new GrapeCity.ActiveReports.SectionReportModel.GroupFooter();
            this.GroupHeader3 = new GrapeCity.ActiveReports.SectionReportModel.GroupHeader();
            this.GroupFooter3 = new GrapeCity.ActiveReports.SectionReportModel.GroupFooter();
            this.GroupHeader4 = new GrapeCity.ActiveReports.SectionReportModel.GroupHeader();
            this.GroupFooter4 = new GrapeCity.ActiveReports.SectionReportModel.GroupFooter();
            this.txtHoursNWT = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.lblProektant = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label1 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.txtTotalHours = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.Label8 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Line1 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.Label11 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label12 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.txtTotalLate = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtTotalLateHours = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtNonWorkDays = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.Label10 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.txtWorkDays = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.Label9 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label13 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.txtMissingHours = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.Label4 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.txtDaysPO = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.Label7 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Line3 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.Label5 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.txtDaysNO = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.Label6 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.txtDaysIll = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.Line2 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            ((System.ComponentModel.ISupportInitialize)(this.txtHoursNWT)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblProektant)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTotalHours)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label11)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label12)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTotalLate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTotalLateHours)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtNonWorkDays)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtWorkDays)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label13)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtMissingHours)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDaysPO)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDaysNO)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDaysIll)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            // 
            // Detail
            // 
            this.Detail.ColumnSpacing = 0F;
            this.Detail.Height = 0F;
            this.Detail.Name = "Detail";
            this.Detail.Visible = false;
            // 
            // GroupHeader1
            // 
            this.GroupHeader1.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
						this.txtHoursNWT,
						this.lblProektant,
						this.Label1,
						this.txtTotalHours,
						this.Label8,
						this.Line1});
            this.GroupHeader1.Height = 0.3534722F;
            this.GroupHeader1.Name = "GroupHeader1";
            // 
            // GroupFooter1
            // 
            this.GroupFooter1.Height = 0F;
            this.GroupFooter1.Name = "GroupFooter1";
            this.GroupFooter1.Visible = false;
            // 
            // GroupHeader5
            // 
            this.GroupHeader5.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
						this.Label11,
						this.Label12,
						this.txtTotalLate,
						this.txtTotalLateHours,
						this.txtNonWorkDays,
						this.Label10,
						this.txtWorkDays,
						this.Label9,
						this.Label13,
						this.txtMissingHours});
            this.GroupHeader5.Height = 1F;
            this.GroupHeader5.Name = "GroupHeader5";
            // 
            // GroupFooter5
            // 
            this.GroupFooter5.Height = 0F;
            this.GroupFooter5.Name = "GroupFooter5";
            // 
            // GroupHeader2
            // 
            this.GroupHeader2.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
						this.Label4,
						this.txtDaysPO,
						this.Label7,
						this.Line3});
            this.GroupHeader2.Height = 0.4743056F;
            this.GroupHeader2.Name = "GroupHeader2";
            // 
            // GroupFooter2
            // 
            this.GroupFooter2.Height = 0F;
            this.GroupFooter2.Name = "GroupFooter2";
            // 
            // GroupHeader3
            // 
            this.GroupHeader3.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
						this.Label5,
						this.txtDaysNO});
            this.GroupHeader3.Height = 0.1965278F;
            this.GroupHeader3.Name = "GroupHeader3";
            // 
            // GroupFooter3
            // 
            this.GroupFooter3.Height = 0F;
            this.GroupFooter3.Name = "GroupFooter3";
            this.GroupFooter3.Visible = false;
            // 
            // GroupHeader4
            // 
            this.GroupHeader4.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
						this.Label6,
						this.txtDaysIll,
						this.Line2});
            this.GroupHeader4.Height = 0.275F;
            this.GroupHeader4.Name = "GroupHeader4";
            // 
            // GroupFooter4
            // 
            this.GroupFooter4.Height = 0F;
            this.GroupFooter4.Name = "GroupFooter4";
            this.GroupFooter4.Visible = false;
            // 
            // txtHoursNWT
            // 
            this.txtHoursNWT.Height = 0.2F;
            this.txtHoursNWT.Left = 3.937008F;
            this.txtHoursNWT.Name = "txtHoursNWT";
            this.txtHoursNWT.Style = "font-size: 9pt; vertical-align: middle";
            this.txtHoursNWT.Text = "txtHoursNWT";
            this.txtHoursNWT.Top = 0.007217826F;
            this.txtHoursNWT.Visible = false;
            this.txtHoursNWT.Width = 3.149606F;
            // 
            // lblProektant
            // 
            this.lblProektant.Height = 0.2F;
            this.lblProektant.HyperLink = null;
            this.lblProektant.Left = 0F;
            this.lblProektant.Name = "lblProektant";
            this.lblProektant.Style = "font-size: 9pt; font-weight: bold; vertical-align: middle";
            this.lblProektant.Text = "???? ?????????? ?????? ? ???????????? ?????:";
            this.lblProektant.Top = 0.007217826F;
            this.lblProektant.Visible = false;
            this.lblProektant.Width = 3.937008F;
            // 
            // Label1
            // 
            this.Label1.Height = 0.2F;
            this.Label1.HyperLink = null;
            this.Label1.Left = 0F;
            this.Label1.Name = "Label1";
            this.Label1.Style = "font-size: 9pt; font-weight: bold; vertical-align: middle";
            this.Label1.Text = "???? ?????????? ??????:";
            this.Label1.Top = 0.2007874F;
            this.Label1.Width = 3.937008F;
            // 
            // txtTotalHours
            // 
            this.txtTotalHours.Height = 0.2F;
            this.txtTotalHours.Left = 3.937008F;
            this.txtTotalHours.Name = "txtTotalHours";
            this.txtTotalHours.Style = "font-size: 9pt; vertical-align: middle";
            this.txtTotalHours.Text = "txtTotalHours";
            this.txtTotalHours.Top = 0.2007874F;
            this.txtTotalHours.Width = 3.149606F;
            // 
            // Label8
            // 
            this.Label8.Height = 0.2F;
            this.Label8.HyperLink = null;
            this.Label8.Left = 0F;
            this.Label8.Name = "Label8";
            this.Label8.Style = "font-size: 9pt; font-weight: bold";
            this.Label8.Text = "?? ???????? ??????";
            this.Label8.Top = 0F;
            this.Label8.Width = 3.937008F;
            // 
            // Line1
            // 
            this.Line1.Height = 0F;
            this.Line1.Left = 0F;
            this.Line1.LineWeight = 0F;
            this.Line1.Name = "Line1";
            this.Line1.Top = 0.1968504F;
            this.Line1.Width = 7.086611F;
            this.Line1.X1 = 0F;
            this.Line1.X2 = 7.086611F;
            this.Line1.Y1 = 0.1968504F;
            this.Line1.Y2 = 0.1968504F;
            // 
            // Label11
            // 
            this.Label11.Height = 0.2F;
            this.Label11.HyperLink = null;
            this.Label11.Left = 0F;
            this.Label11.Name = "Label11";
            this.Label11.Style = "font-size: 9pt; font-weight: bold; vertical-align: middle";
            this.Label11.Text = "??? ???? ??????????:";
            this.Label11.Top = 0.5625F;
            this.Label11.Visible = false;
            this.Label11.Width = 3.937008F;
            // 
            // Label12
            // 
            this.Label12.Height = 0.2F;
            this.Label12.HyperLink = null;
            this.Label12.Left = 0F;
            this.Label12.Name = "Label12";
            this.Label12.Style = "font-size: 9pt; font-weight: bold; vertical-align: middle";
            this.Label12.Text = "???? ?????? ??????????:";
            this.Label12.Top = 0.78125F;
            this.Label12.Visible = false;
            this.Label12.Width = 3.937008F;
            // 
            // txtTotalLate
            // 
            this.txtTotalLate.Height = 0.2F;
            this.txtTotalLate.Left = 3.9375F;
            this.txtTotalLate.Name = "txtTotalLate";
            this.txtTotalLate.Style = "font-size: 9pt; vertical-align: middle";
            this.txtTotalLate.Top = 0.5625F;
            this.txtTotalLate.Visible = false;
            this.txtTotalLate.Width = 3.149606F;
            // 
            // txtTotalLateHours
            // 
            this.txtTotalLateHours.Height = 0.2F;
            this.txtTotalLateHours.Left = 3.9375F;
            this.txtTotalLateHours.Name = "txtTotalLateHours";
            this.txtTotalLateHours.Style = "font-size: 9pt; vertical-align: middle";
            this.txtTotalLateHours.Top = 0.78125F;
            this.txtTotalLateHours.Visible = false;
            this.txtTotalLateHours.Width = 3.149606F;
            // 
            // txtNonWorkDays
            // 
            this.txtNonWorkDays.Height = 0.2F;
            this.txtNonWorkDays.Left = 3.9375F;
            this.txtNonWorkDays.Name = "txtNonWorkDays";
            this.txtNonWorkDays.Style = "font-size: 9pt; vertical-align: middle";
            this.txtNonWorkDays.Text = "txtNonWorkDays";
            this.txtNonWorkDays.Top = 0F;
            this.txtNonWorkDays.Width = 3.149606F;
            // 
            // Label10
            // 
            this.Label10.Height = 0.2F;
            this.Label10.HyperLink = null;
            this.Label10.Left = 0F;
            this.Label10.Name = "Label10";
            this.Label10.Style = "font-size: 9pt; font-weight: bold; vertical-align: middle";
            this.Label10.Text = "???? ???. ?????? ? ???????????? ?????:";
            this.Label10.Top = 0F;
            this.Label10.Width = 3.937008F;
            // 
            // txtWorkDays
            // 
            this.txtWorkDays.Height = 0.2F;
            this.txtWorkDays.Left = 3.9375F;
            this.txtWorkDays.Name = "txtWorkDays";
            this.txtWorkDays.Style = "font-size: 9pt; vertical-align: middle";
            this.txtWorkDays.Text = "txtWorkDays";
            this.txtWorkDays.Top = 0.1875F;
            this.txtWorkDays.Width = 3.149606F;
            // 
            // Label9
            // 
            this.Label9.Height = 0.2F;
            this.Label9.HyperLink = null;
            this.Label9.Left = 0F;
            this.Label9.Name = "Label9";
            this.Label9.Style = "font-size: 9pt; font-weight: bold; vertical-align: middle";
            this.Label9.Text = "???? ???. ?????? ? ??????? ?????:";
            this.Label9.Top = 0.1875F;
            this.Label9.Width = 3.937008F;
            // 
            // Label13
            // 
            this.Label13.Height = 0.2F;
            this.Label13.HyperLink = null;
            this.Label13.Left = 0F;
            this.Label13.Name = "Label13";
            this.Label13.Style = "font-size: 9pt; font-weight: bold; vertical-align: middle";
            this.Label13.Text = "???? ???????????? ?????? ?? ??????:";
            this.Label13.Top = 0.375F;
            this.Label13.Width = 3.937008F;
            // 
            // txtMissingHours
            // 
            this.txtMissingHours.Height = 0.2F;
            this.txtMissingHours.Left = 3.9375F;
            this.txtMissingHours.Name = "txtMissingHours";
            this.txtMissingHours.Style = "font-size: 9pt; vertical-align: middle";
            this.txtMissingHours.Top = 0.375F;
            this.txtMissingHours.Width = 3.149606F;
            // 
            // Label4
            // 
            this.Label4.Height = 0.2F;
            this.Label4.HyperLink = null;
            this.Label4.Left = 0F;
            this.Label4.Name = "Label4";
            this.Label4.Style = "font-size: 9pt; font-weight: bold";
            this.Label4.Text = "???? ??? ?????? ??????:";
            this.Label4.Top = 0.2755906F;
            this.Label4.Width = 3.937008F;
            // 
            // txtDaysPO
            // 
            this.txtDaysPO.Height = 0.2F;
            this.txtDaysPO.Left = 3.937008F;
            this.txtDaysPO.Name = "txtDaysPO";
            this.txtDaysPO.Style = "font-size: 9pt";
            this.txtDaysPO.Text = "txtDaysPO";
            this.txtDaysPO.Top = 0.2755906F;
            this.txtDaysPO.Width = 3.149606F;
            // 
            // Label7
            // 
            this.Label7.Height = 0.2F;
            this.Label7.HyperLink = null;
            this.Label7.Left = 0F;
            this.Label7.Name = "Label7";
            this.Label7.Style = "font-size: 9pt; font-weight: bold";
            this.Label7.Text = "?? ???????";
            this.Label7.Top = 0.07874014F;
            this.Label7.Width = 3.937008F;
            // 
            // Line3
            // 
            this.Line3.Height = 0F;
            this.Line3.Left = 0F;
            this.Line3.LineWeight = 1F;
            this.Line3.Name = "Line3";
            this.Line3.Top = 0.2755906F;
            this.Line3.Width = 7.086611F;
            this.Line3.X1 = 0F;
            this.Line3.X2 = 7.086611F;
            this.Line3.Y1 = 0.2755906F;
            this.Line3.Y2 = 0.2755906F;
            // 
            // Label5
            // 
            this.Label5.Height = 0.2F;
            this.Label5.HyperLink = null;
            this.Label5.Left = 0F;
            this.Label5.Name = "Label5";
            this.Label5.Style = "font-size: 9pt; font-weight: bold";
            this.Label5.Text = "???? ??? ???????? ??????:";
            this.Label5.Top = 0F;
            this.Label5.Width = 3.937008F;
            // 
            // txtDaysNO
            // 
            this.txtDaysNO.Height = 0.2F;
            this.txtDaysNO.Left = 3.937008F;
            this.txtDaysNO.Name = "txtDaysNO";
            this.txtDaysNO.Style = "font-size: 9pt";
            this.txtDaysNO.Text = "txtDaysNO";
            this.txtDaysNO.Top = 0F;
            this.txtDaysNO.Width = 3.149606F;
            // 
            // Label6
            // 
            this.Label6.Height = 0.2F;
            this.Label6.HyperLink = null;
            this.Label6.Left = 0F;
            this.Label6.Name = "Label6";
            this.Label6.Style = "font-size: 9pt; font-weight: bold";
            this.Label6.Text = "???? ??? ????????:";
            this.Label6.Top = 0F;
            this.Label6.Width = 3.937008F;
            // 
            // txtDaysIll
            // 
            this.txtDaysIll.Height = 0.2F;
            this.txtDaysIll.Left = 3.937008F;
            this.txtDaysIll.Name = "txtDaysIll";
            this.txtDaysIll.Style = "font-size: 9pt";
            this.txtDaysIll.Text = "txtDaysIll";
            this.txtDaysIll.Top = 0F;
            this.txtDaysIll.Width = 3.149606F;
            // 
            // Line2
            // 
            this.Line2.Height = 0F;
            this.Line2.Left = 0F;
            this.Line2.LineWeight = 2F;
            this.Line2.Name = "Line2";
            this.Line2.Top = 0.2362205F;
            this.Line2.Width = 7.086611F;
            this.Line2.X1 = 0F;
            this.Line2.X2 = 7.086611F;
            this.Line2.Y1 = 0.2362205F;
            this.Line2.Y2 = 0.2362205F;
            // 
            // ActiveReport1
            // 
            this.MasterReport = false;
            this.PageSettings.PaperHeight = 11F;
            this.PageSettings.PaperWidth = 8.5F;
            this.PrintWidth = 7.479167F;
            this.Sections.Add(this.GroupHeader1);
            this.Sections.Add(this.GroupHeader5);
            this.Sections.Add(this.GroupHeader2);
            this.Sections.Add(this.GroupHeader3);
            this.Sections.Add(this.GroupHeader4);
            this.Sections.Add(this.Detail);
            this.Sections.Add(this.GroupFooter4);
            this.Sections.Add(this.GroupFooter3);
            this.Sections.Add(this.GroupFooter2);
            this.Sections.Add(this.GroupFooter5);
            this.Sections.Add(this.GroupFooter1);
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule(resources.GetString("$this.StyleSheet"), "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: inherit; font-style: inherit; font-variant: inherit; font-weight: bo" +
                        "ld; font-size: 16pt; font-size-adjust: inherit; font-stretch: inherit", "Heading1", "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: Times New Roman; font-style: italic; font-variant: inherit; font-wei" +
                        "ght: bold; font-size: 14pt; font-size-adjust: inherit; font-stretch: inherit", "Heading2", "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: inherit; font-style: inherit; font-variant: inherit; font-weight: bo" +
                        "ld; font-size: 13pt; font-size-adjust: inherit; font-stretch: inherit", "Heading3", "Normal"));
            ((System.ComponentModel.ISupportInitialize)(this.txtHoursNWT)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblProektant)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTotalHours)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label11)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label12)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTotalLate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTotalLateHours)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtNonWorkDays)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtWorkDays)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label13)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtMissingHours)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDaysPO)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDaysNO)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDaysIll)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();
        }

		#endregion

        private GroupHeader GroupHeader1;
        private TextBox txtHoursNWT;
        private Label lblProektant;
        private Label Label1;
        private TextBox txtTotalHours;
        private Label Label8;
        private Line Line1;
        private GroupHeader GroupHeader5;
        private Label Label11;
        private Label Label12;
        private TextBox txtTotalLate;
        private TextBox txtTotalLateHours;
        private TextBox txtNonWorkDays;
        private Label Label10;
        private TextBox txtWorkDays;
        private Label Label9;
        private Label Label13;
        private TextBox txtMissingHours;
        private GroupHeader GroupHeader2;
        private Label Label4;
        private TextBox txtDaysPO;
        private Label Label7;
        private Line Line3;
        private GroupHeader GroupHeader3;
        private Label Label5;
        private TextBox txtDaysNO;
        private GroupHeader GroupHeader4;
        private Label Label6;
        private TextBox txtDaysIll;
        private Line Line2;
        private Detail Detail;
        private GroupFooter GroupFooter4;
        private GroupFooter GroupFooter3;
        private GroupFooter GroupFooter2;
        private GroupFooter GroupFooter5;
        private GroupFooter GroupFooter1;
	}
}
