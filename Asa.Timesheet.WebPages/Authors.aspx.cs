using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Data.SqlClient;
using Asa.Timesheet.Data;
using Asa.Timesheet.Data.Entities;
using log4net;
using Asa.Timesheet.WebPages.UserControls;
using Asa.Timesheet.Data.Util;
namespace Asa.Timesheet.WebPages
{
	/// <summary>
	/// Summary description for Authors.
	/// </summary>
	public class Authors : TimesheetPageBase
	{
		protected System.Web.UI.WebControls.PlaceHolder menuHolder;
		protected System.Web.UI.WebControls.Label lblError;
		protected System.Web.UI.WebControls.Label lblInfo;
		protected System.Web.UI.WebControls.Panel grid;
		protected System.Web.UI.WebControls.Label Label8;
		protected System.Web.UI.WebControls.Label Label9;
		protected System.Web.UI.WebControls.Label Label10;
		protected System.Web.UI.WebControls.DataList dlPayments;
		protected System.Web.UI.WebControls.Button btnNewPayment;
		protected System.Web.UI.HtmlControls.HtmlTable tbl;
		protected System.Web.UI.WebControls.Button btnSave;
		protected System.Web.UI.WebControls.Button btnProject;
		protected Asa.Timesheet.WebPages.UserControls.PageHeader header;
		protected System.Web.UI.WebControls.Label Label1;
		protected System.Web.UI.WebControls.Label Label2;
		protected System.Web.UI.WebControls.Label Label3;
		private static readonly ILog log = LogManager.GetLogger(typeof(EditProjectSubContracter));
		private void Page_Load(object sender, System.EventArgs e)
		{
			if(!IsPostBack)
			{
				int nID = UIHelpers.GetPIDParam();
				PaymentsVector pv = PaymentDAL.LoadCollection("PaymentsListByAuthorProc", SQLParms.CreatePaymentsListByAuthorsProc(nID));
				SessionTable=pv;
				dlPayments.DataSource=pv;
				dlPayments.DataBind();
				
				header.UserName = this.LoggedUser.UserName;
				int PID = UIHelpers.GetPIDParam();
				string projectName, projectCode, administrativeName,add;
				decimal area=0;
				int clientID=0;
				bool phases;
				if (!ProjectsData.SelectProject(PID, out projectName, out  projectCode, out administrativeName, out area, out add, 
					out clientID,out phases))
				{
					log.Error("Project not found " + PID);
				
				}
				else
				{
					header.PageTitle = Resource.ResourceManager["reports_AN_Title"]+" - "+  projectName;//projectCode+ " "+administrativeName;
				}
			}
			if (!(LoggedUser.HasPaymentRights))
						{
							ErrorRedirect(Resource.ResourceManager["noRightsAlert"]);
						}
			
			UIHelpers.CreateMenu(menuHolder,LoggedUser);
			
			
		}

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
			this.dlPayments.ItemDataBound += new System.Web.UI.WebControls.DataListItemEventHandler(this.dlPayments_ItemDataBound);
			this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
			this.btnNewPayment.Click += new System.EventHandler(this.btnNewPayment_Click);
			this.btnProject.Click += new System.EventHandler(this.btnProject_Click);
			this.Load += new System.EventHandler(this.Page_Load);

		}
		#endregion

		private void btnNewPayment_Click(object sender, System.EventArgs e)
		{
			PaymentData pd = new PaymentData(-1,-1,-1,0,0,false,new DateTime(),0,false);
			SessionTable.Add(pd);
			dlPayments.DataSource=SessionTable;
			dlPayments.DataBind();
		}

		private void btnSave_Click(object sender, System.EventArgs e)
		{
			PaymentsVector pv = new PaymentsVector();
			for(int i=0;i<dlPayments.Items.Count;i++)
			{
				//decimal per=0;
				decimal am=0;

				
				TextBox txtAmount=(TextBox) dlPayments.Items[i].FindControl("txtAmount");
				if(txtAmount!=null && txtAmount.Text!="")
				{
					am = UIHelpers.ParseDecimal(txtAmount.Text);
					//dEUR+=am;
					
				}
				CheckBox ckPaid=(CheckBox) dlPayments.Items[i].FindControl("ckPaid");
				TextBox txt=(TextBox) dlPayments.Items[i].FindControl("txtDate");
				Label txtStartDate=(Label) dlPayments.Items[i].FindControl("txtStartDate");
				Label txtEndDate=(Label) dlPayments.Items[i].FindControl("txtEndDate");
				TextBox txtNotes=(TextBox) dlPayments.Items[i].FindControl("txtNotes");
				if(am>0)
				{
					PaymentData pd= new PaymentData(-1, -1,-1,0,am,ckPaid.Checked,TimeHelper.GetDate(txt.Text),am,false);
					pd.Notes=txtNotes.Text;
					pd.AuthorsID=UIHelpers.GetPIDParam();
					if(txtStartDate.Text!="")
					{
						pd.AuthorsStartDate=DateTime.ParseExact(txtStartDate.Text,Constants.TextDateFormat,System.Globalization.NumberFormatInfo.InvariantInfo);
					}
					if(txtEndDate.Text!="")
					{
						pd.AuthorsEndDate=DateTime.ParseExact(txtEndDate.Text,Constants.TextDateFormat,System.Globalization.NumberFormatInfo.InvariantInfo);
					}
					pv.Add(pd);
				}
			}
			SubprojectsUDL.ExecutePaymentsDelByAuthorsProc(UIHelpers.GetPIDParam());
			foreach(PaymentData pd in pv)
				PaymentDAL.Save(pd);
			Response.Redirect("Authors.aspx?pid="+UIHelpers.GetPIDParam());
		}

		private void btnProject_Click(object sender, System.EventArgs e)
		{
			Response.Redirect("EditProject.aspx?pid="+UIHelpers.GetPIDParam());
		}

		private void dlPayments_ItemDataBound(object sender, System.Web.UI.WebControls.DataListItemEventArgs e)
		{
			HtmlImage hi=(HtmlImage) e.Item.FindControl("lkCal");
			TextBox txt=(TextBox) e.Item.FindControl("txtDate");
			if(txt!=null&& hi!=null)
				hi.Attributes["onclick"] = TimeHelper.InvokePopupCal(txt);

		}
	}
}
