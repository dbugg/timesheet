using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Data.SqlClient;
using log4net;
using Asa.Timesheet.Data;
using Asa.Timesheet.Data.Entities;
using Asa.Timesheet.Data.Util;
using GrapeCity.ActiveReports.Export.Pdf.Section;
using System.Configuration;
using GrapeCity.ActiveReports.Export.Excel.Section;
namespace Asa.Timesheet.WebPages.Reports
{
	/// <summary>
	/// Summary description for Minutes.
	/// </summary>
	public class Minutes : TimesheetPageBase
	{
		#region private members

		private int _area;
		private static readonly ILog log = LogManager.GetLogger(typeof(Minutes));

		#endregion

		#region Web controls

		protected System.Web.UI.WebControls.PlaceHolder menuHolder;
		protected System.Web.UI.WebControls.Label lblInfo;
		protected System.Web.UI.WebControls.Label lblProject;
		protected System.Web.UI.WebControls.DropDownList ddlProject;
		protected System.Web.UI.WebControls.Label lblActivity;
		protected System.Web.UI.WebControls.DataGrid grdReport;
		protected System.Web.UI.WebControls.Panel Panel1;
		protected System.Web.UI.WebControls.Label lblError;
		protected System.Web.UI.HtmlControls.HtmlInputText txtNoActivity;
		protected System.Web.UI.WebControls.DropDownList ddlActivity;
		protected System.Web.UI.WebControls.Button btnGenerateReport;
		protected System.Web.UI.WebControls.DropDownList ddlUser;
		protected System.Web.UI.WebControls.Label lblPotr;
		protected System.Web.UI.WebControls.Panel pnlGrid;
		protected System.Web.UI.WebControls.ImageButton ibPdfExport;
		protected System.Web.UI.WebControls.ImageButton ibXlsExport;
		protected System.Web.UI.WebControls.ImageButton ibGenerate;
		protected Asa.Timesheet.WebPages.UserControls.PageHeader header;

		
		protected System.Web.UI.WebControls.Label Label1;
		protected System.Web.UI.WebControls.Label lblObekt;
		protected System.Web.UI.WebControls.Label Label3;
		protected System.Web.UI.WebControls.Label lblClient;
		protected System.Web.UI.WebControls.Label lblClientAddress;
		protected System.Web.UI.WebControls.Label Label6;
		protected System.Web.UI.WebControls.Label lblProektant;
		protected System.Web.UI.HtmlControls.HtmlTable tblHeader;
		protected System.Web.UI.WebControls.Label lblCreatedBy;
		protected System.Web.UI.WebControls.Label Label2;
		protected System.Web.UI.WebControls.Label Label5;
		protected System.Web.UI.WebControls.Label lblReportUser;
		protected System.Web.UI.WebControls.Label lblReportActivity;
		protected System.Web.UI.WebControls.Label lblNoDataFound;
		protected System.Web.UI.WebControls.Label lblReportTitle;
    protected System.Web.UI.WebControls.DropDownList ddlProjectStatus;
		protected System.Web.UI.WebControls.Label lbTotal;
		protected System.Web.UI.WebControls.Label Label7;
		protected System.Web.UI.WebControls.DropDownList ddlBuildingTypes;
		protected System.Web.UI.WebControls.Label lbPayments;
		protected System.Web.UI.WebControls.Label lblNoDataFound1;
		protected System.Web.UI.WebControls.DataGrid grd;
		protected System.Web.UI.WebControls.DropDownList ddlUserStatus;
		protected System.Web.UI.WebControls.Label Label4;
		protected System.Web.UI.WebControls.Label lblCreatedByLbl;

		#endregion

        private void Page_Load(object sender, System.EventArgs e)
        {

            if (!this.LoggedUser.HasRole) ErrorRedirect(Resource.ResourceManager["noRightsAlert"]);

            if (Request.Params["Type"] == "AN")
            {
                if (!this.LoggedUser.HasPaymentRights) ErrorRedirect(Resource.ResourceManager["noRightsAlert"]);

            }
            UIHelpers.CreateMenuReports(menuHolder, LoggedUser);

            tblHeader.EnableViewState = false;
            tblHeader.Visible = false;

            if (!this.IsPostBack)
            {
                ddlProject.Attributes["onchange"] = "DisableActivity();";
                header.UserName = LoggedUser.FullName;
                if (Request.Params["Type"] == "AN")
                {
                    header.PageTitle = Resource.ResourceManager["reports_AN_Title"];
                    //removed by Ivailo date: 05.12.2007
                    //notes:making export
                    //ibPdfExport.Visible=ibXlsExport.Visible=true;
                    lblReportTitle.Text = Resource.ResourceManager["reports_AN_Header"];

                }
                else
                    header.PageTitle = Resource.ResourceManager["reports_Minutes_Title"];
                UIHelpers.LoadProjectStatus(ddlProjectStatus, (int)ProjectsData.ProjectsByStatus.Active);
                UIHelpers.LoadBuildingTypes("", ddlBuildingTypes);
                if (!(LoadProjects() &&
                LoadActivities() &&
                LoadUsers())) return;
            }

            if (Request.Params["Type"] == "AN")
            {

                ddlActivity.Visible = lblActivity.Visible = false;
                ddlActivity.SelectedValue = System.Configuration.ConfigurationManager.AppSettings["AN"];
            }
        }


		#region Web Form Designer generated code
        override protected void OnInit(EventArgs e)
        {
            //
            // CODEGEN: This call is required by the ASP.NET Web Form Designer.
            //
            InitializeComponent();
            base.OnInit(e);
        }
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
        private void InitializeComponent()
        {
            this.ddlProjectStatus.SelectedIndexChanged += new System.EventHandler(this.ddlProjectStatus_SelectedIndexChanged);
            this.ddlBuildingTypes.SelectedIndexChanged += new System.EventHandler(this.ddlBuildingTypes_SelectedIndexChanged);
            this.ddlUserStatus.SelectedIndexChanged += new System.EventHandler(this.ddlUserStatus_SelectedIndexChanged);
            this.btnGenerateReport.Click += new System.EventHandler(this.btnGenerateReport_Click);
            this.ibGenerate.Click += new System.Web.UI.ImageClickEventHandler(this.ibGenerate_Click);
            this.ibPdfExport.Click += new System.Web.UI.ImageClickEventHandler(this.ibPdfExport_Click);
            this.ibXlsExport.Click += new System.Web.UI.ImageClickEventHandler(this.ibXlsExport_Click);
            this.grdReport.ItemDataBound += new System.Web.UI.WebControls.DataGridItemEventHandler(this.grdReport_ItemDataBound);
            this.grd.ItemDataBound += new System.Web.UI.WebControls.DataGridItemEventHandler(this.grd_ItemDataBound);
            this.Load += new System.EventHandler(this.Page_Load);

        }
		#endregion

		#region Event handlers

        private void grdReport_ItemDataBound(object sender, System.Web.UI.WebControls.DataGridItemEventArgs e)
        {
            if ((e.Item.ItemType == ListItemType.AlternatingItem) || (e.Item.ItemType == ListItemType.Item))
            {
                TableCell tc;
                DataRowView drv = (DataRowView)e.Item.DataItem;

                #region Format grid cells depending on rowtype

                switch ((int)drv["RowType"])
                {
                    //user summary
                    case 1:
                        e.Item.Cells.Clear();

                        tc = new TableCell();
                        tc.ColumnSpan = 4;
                        tc.ForeColor = System.Drawing.Color.Black;
                        tc.BackColor = System.Drawing.Color.WhiteSmoke;
                        //tc.Text = Resource.ResourceManager["reports_TotalFor"]+drv["UserName"].ToString();
                        tc.Text = Resource.ResourceManager["reports_Total"];
                        e.Item.Cells.Add(tc);

                        tc = new TableCell();
                        tc.ColumnSpan = 2;
                        tc.ForeColor = System.Drawing.Color.Black;
                        //tc.HorizontalAlign = HorizontalAlign.Right;
                        tc.BackColor = System.Drawing.Color.WhiteSmoke;
                        tc.Text = TimeHelper.HoursStringFromMinutes((int)drv["Time"], false);
                        e.Item.Cells.Add(tc); break;

                    //Activity summary
                    case 2:
                        e.Item.Cells.Clear();
                        tc = new TableCell();
                        tc.ColumnSpan = 4;
                        tc.BorderWidth = 3;
                        tc.ForeColor = System.Drawing.Color.Black;
                        tc.Font.Bold = true;
                        tc.BackColor = System.Drawing.Color.WhiteSmoke;
                        //tc.Text = Resource.ResourceManager["reports_TotalFor"]+drv["ActivityName"].ToString();
                        tc.Text = Resource.ResourceManager["reports_Total"];
                        e.Item.Cells.Add(tc);

                        tc = new TableCell(); tc.ForeColor = System.Drawing.Color.Black;
                        tc.ColumnSpan = 2;
                        tc.BorderWidth = 3;
                        tc.BackColor = System.Drawing.Color.WhiteSmoke;
                        tc.Text = TimeHelper.HoursStringFromMinutes((int)drv["Time"], false); tc.Font.Bold = true;
                        e.Item.Cells.Add(tc);

                        System.Globalization.NumberFormatInfo ni = new System.Globalization.NumberFormatInfo();
                        ni.NumberDecimalDigits = 4;
                        ni.NumberDecimalSeparator = ".";


                        /*
                        tc = new TableCell();tc.ForeColor=System.Drawing.Color.Black;
                        tc.Font.Bold = true;
                        tc.ColumnSpan = 1;
                        tc.BorderWidth = 3;
                        tc.BackColor = System.Drawing.Color.WhiteSmoke;
                        tc.Text = Resource.ResourceManager["reports_KoefIzrVreme"]+ " - "+
                        (TimeHelper.HoursFromMinutes((int)drv["Time"])/_area).ToString("0.0000",ni);
                        e.Item.Cells.Add(tc);
                        */
                        break;
                    //Activity header
                    case 3: e.Item.Cells.Clear();
                        tc = new TableCell();
                        tc.ForeColor = System.Drawing.Color.Black;
                        tc.Font.Bold = true;
                        tc.ColumnSpan = 6;
                        tc.BorderWidth = 3;
                        tc.BackColor = System.Drawing.Color.WhiteSmoke;
                        tc.Text = drv["ActivityName"].ToString();
                        e.Item.Cells.Add(tc);
                        break;
                    case 6: e.Item.Cells.Clear(); tc = new TableCell(); tc.BorderWidth = 0; e.Item.Cells.Add(tc);
                        TableRow tr = (TableRow)tc.Parent;
                        tr.Height = 5;
                        break;
                    case 7:
                        e.Item.Cells.Clear();
                        tc = new TableCell();
                        tc.ColumnSpan = 4;
                        tc.BorderWidth = 1;
                        tc.ForeColor = System.Drawing.Color.White;
                        tc.Font.Bold = true;
                        tc.Font.Size = FontUnit.Point(10);
                        tc.BackColor = System.Drawing.Color.Peru;
                        //tc.Text = Resource.ResourceManager["reports_TotalFor"]+ddlProject.SelectedItem.Text;
                        tc.Text = Resource.ResourceManager["reports_Total"];
                        e.Item.Cells.Add(tc);

                        tc = new TableCell();
                        tc.ForeColor = System.Drawing.Color.White;
                        tc.ColumnSpan = 2; tc.BorderWidth = 1;
                        tc.BackColor = System.Drawing.Color.Peru;
                        tc.Text = TimeHelper.HoursStringFromMinutes((int)drv["Time"], false);
                        tc.Font.Size = FontUnit.Point(10);
                        tc.Font.Bold = true;
                        e.Item.Cells.Add(tc);
                        break;
                }

                #endregion
            }
        }

        private void btnGenerateReport_Click(object sender, System.EventArgs e)
        {
            BindGrid();
        }

        private void ibXlsExport_Click(object sender, System.Web.UI.ImageClickEventArgs e)
        {
            //edited by Ivailo date: 05.12.2007
            //notes:this export is for protocols
            if (Request.Params["Type"] != "AN")
            {
                MinutesReport minutesReport = CreateReport(true, false);
                if (minutesReport == null) return;

                XlsExport xls = new XlsExport();

                try
                {
                    minutesReport.Run();
                }
                catch (Exception ex)
                {
                    log.Error(ex);
                    lblError.Text = Resource.ResourceManager["reports_ErrorRunReportDocument"];
                    return;
                }

                System.IO.MemoryStream memoryFile = new System.IO.MemoryStream();
                xls.Export(minutesReport.Document, memoryFile);

                minutesReport.Dispose();

                Response.Clear();
                Response.AppendHeader("content-disposition"
                    , "attachment; filename=" + "minutes.xls");
                Response.ContentType = "application/xls";

                memoryFile.WriteTo(Response.OutputStream);
                Response.End();
            }
            else
            {
                //Add by Ivailo date: 05.12.2007
                //notes:this export is for AVTORSKI NADZOR

                XlsExport pdf = new XlsExport();
                //notes: get projectID
                int projectID = int.Parse(ddlProject.SelectedValue);
                //notes: check if it's not selected project
                if (projectID == 0)
                {
                    lblInfo.Text = Resource.ResourceManager["reports_Minutes_ErrorSelectProject"];
                    return;
                }
                string projectName = string.Empty;
                string clientName = string.Empty;
                string userName = string.Empty;
                //get private class HeaderInfo
                HeaderInfo hi = GetHeaderData();
                //check if it is empty(it don't have to be empty)
                if (hi == null) return;

                projectName = (hi.ProjectAdminName == String.Empty) ? hi.ProjectName : hi.ProjectAdminName;
                clientName = hi.ClientName;
                userName = hi.User;
                //set view for second subreport
                DataView dvAuthorsAnalysis = ReportsData.ExecuteReportsAuthorsAnalysis(projectID);
                //set view for first subreport and mesage of total 
                int activityID = int.Parse(ddlActivity.SelectedValue);
                int userID = int.Parse(ddlUser.SelectedValue);

                DataSet ds = UIHelpers.LoadWorkTimes(projectID, activityID, userID, ddlUserStatus.SelectedValue);
                string TotalText = "";
                DataTable source;

                try
                {
                    decimal totalmin = 0;
                    //notes:check if table in dataset has any rows
                    if (ds.Tables[0].Rows.Count != 0)
                        source = UIHelpers.CreateGridSource(ds.Tables[0], out totalmin);
                    else
                        source = new DataTable();

                    totalmin = totalmin / 60;
                    SqlDataReader reader = ProjectsData.SelectProject(projectID);
                    decimal dEur = 0;
                    if (reader.Read())
                    {
                        if (!reader.IsDBNull(28))
                            dEur = reader.GetDecimal(28);
                    }
                    TotalText = string.Format(Resource.ResourceManager["reports_AN_total"], UIHelpers.FormatDecimal2(totalmin), UIHelpers.FormatDecimal2(dEur), UIHelpers.FormatDecimal2(totalmin * dEur));

                }
                catch (Exception ex)
                {
                    log.Error(ex);
                    lblError.Text = Resource.ResourceManager["reports_Minutes_ErrorLoadGrid"];

                    return;
                }

                DataView dvMin = new DataView(source);

                //start the report
                GrapeCity.ActiveReports.SectionReport report = new MinutesTypeAN_main(projectName, clientName, userName, false, dvMin, dvAuthorsAnalysis, TotalText, LoggedUser.FullName);
                report.PageSettings.Orientation = GrapeCity.ActiveReports.Document.Section.PageOrientation.Landscape;

                report.Run();

                System.IO.MemoryStream memoryFile = new System.IO.MemoryStream();

                pdf.Export(report.Document, memoryFile);

                Response.Clear();
                Response.AppendHeader("content-disposition"
                    , "attachment; filename=" + "MinutesTypeAN.xls");
                Response.ContentType = "application/pdf";

                memoryFile.WriteTo(Response.OutputStream);
                Response.End();

            }
        }

        private void ibPdfExport_Click(object sender, System.Web.UI.ImageClickEventArgs e)
        {
            //edited by Ivailo date: 05.12.2007
            //notes:this export is for protocols
            if (Request.Params["Type"] != "AN")
            {
                MinutesReport minutesReport = CreateReport(false, true);
                if (minutesReport == null) return;

                PdfExport pdf = new PdfExport();
                pdf.NeverEmbedFonts = "";

                try
                {
                    minutesReport.Run();
                }
                catch (Exception ex)
                {
                    log.Error(ex);
                    lblError.Text = Resource.ResourceManager["reports_ErrorRunReportDocument"];
                    return;
                }

                System.IO.MemoryStream memoryFile = new System.IO.MemoryStream();
                pdf.Export(minutesReport.Document, memoryFile);

                Response.Clear();
                Response.AppendHeader("content-disposition"
                    , "attachment; filename=" + "minutes.pdf");
                Response.ContentType = "application/pdf";

                memoryFile.WriteTo(Response.OutputStream);
                Response.End();
            }
            else
            {
                //Add by Ivailo date: 05.12.2007
                //notes:this export is for AVTORSKI NADZOR
                // do nothing for now 

                PdfExport pdf = new PdfExport();
                pdf.NeverEmbedFonts = "";
                //notes: get projectID
                int projectID = int.Parse(ddlProject.SelectedValue);
                //notes: check if it's not selected project
                if (projectID == 0)
                {
                    lblInfo.Text = Resource.ResourceManager["reports_Minutes_ErrorSelectProject"];
                    return;
                }
                string projectName = string.Empty;
                string clientName = string.Empty;
                string userName = string.Empty;
                //get private class HeaderInfo
                HeaderInfo hi = GetHeaderData();
                //check if it is empty(it don't have to be empty)
                if (hi == null) return;

                projectName = (hi.ProjectAdminName == String.Empty) ? hi.ProjectName : hi.ProjectAdminName;
                clientName = hi.ClientName;
                userName = hi.User;
                //set view for second subreport
                DataView dvAuthorsAnalysis = ReportsData.ExecuteReportsAuthorsAnalysis(projectID);
                //set view for first subreport and mesage of total 
                int activityID = int.Parse(ddlActivity.SelectedValue);
                int userID = int.Parse(ddlUser.SelectedValue);

                DataSet ds = UIHelpers.LoadWorkTimes(projectID, activityID, userID, ddlUserStatus.SelectedValue);
                string TotalText = "";
                DataTable source;

                try
                {
                    decimal totalmin = 0;
                    //notes:check if table in dataset has any rows
                    if (ds.Tables[0].Rows.Count != 0)
                        source = UIHelpers.CreateGridSource(ds.Tables[0], out totalmin);
                    else
                        source = new DataTable();
                    totalmin = totalmin / 60;
                    SqlDataReader reader = ProjectsData.SelectProject(projectID);
                    decimal dEur = 0;
                    if (reader.Read())
                    {
                        if (!reader.IsDBNull(28))
                            dEur = reader.GetDecimal(28);
                    }
                    TotalText = string.Format(Resource.ResourceManager["reports_AN_total"], UIHelpers.FormatDecimal2(totalmin), UIHelpers.FormatDecimal2(dEur), UIHelpers.FormatDecimal2(totalmin * dEur));

                }
                catch (Exception ex)
                {
                    log.Error(ex);
                    lblError.Text = Resource.ResourceManager["reports_Minutes_ErrorLoadGrid"];

                    return;
                }

                DataView dvMin = new DataView(source);

                //start the report
                //notes: 4 param is wheather it's pdf or not in this case true
                GrapeCity.ActiveReports.SectionReport report = new MinutesTypeAN_main(projectName, clientName, userName, true, dvMin, dvAuthorsAnalysis, TotalText, LoggedUser.FullName);
                report.PageSettings.Orientation = GrapeCity.ActiveReports.Document.Section.PageOrientation.Landscape;

                report.Run();

                System.IO.MemoryStream memoryFile = new System.IO.MemoryStream();

                pdf.Export(report.Document, memoryFile);

                Response.Clear();
                Response.AppendHeader("content-disposition"
                    , "attachment; filename=" + "MinutesTypeAN.pdf");
                Response.ContentType = "application/pdf";

                memoryFile.WriteTo(Response.OutputStream);
                Response.End();
            }
        }

        private void ibGenerate_Click(object sender, System.Web.UI.ImageClickEventArgs e)
        {
            BindGrid();
        }

		#endregion

		#region Grid 

		#region Transform grid source dataset

		
		#endregion

        private void BindGrid()
        {
            lbTotal.Text = "";
            //	grdReport.DataSource = null;
            //	grdReport.DataBind();

            int projectID = int.Parse(ddlProject.SelectedValue);

            if (projectID == 0)
            {
                lblInfo.Text = Resource.ResourceManager["reports_Minutes_ErrorSelectProject"];
                return;
            }

            /*
            if (!SetArea(projectID)) 
            {
                lblError.Text = Resource.ResourceManager["reports_Minutes_ErrorLoadGrid"];
                return; 
            }
            */


            #region Set header info

            HeaderInfo hi = GetHeaderData();

            if (hi == null) return;

            tblHeader.Visible = true;

            lblObekt.Text = (hi.ProjectAdminName == String.Empty) ? hi.ProjectName : hi.ProjectAdminName;
            if (lblObekt.Text == String.Empty)
            {
                tblHeader.Rows[0].Visible = false; tblHeader.Rows[1].Visible = false;
                tblHeader.Rows[2].Visible = false; tblHeader.Rows[3].Visible = false;
            }

            if (hi.ClientName == String.Empty)
            {
                tblHeader.Rows[1].Visible = false; tblHeader.Rows[2].Visible = false;
            }
            else
            {
                lblClient.Text = hi.ClientName;
                lblClientAddress.Text = String.Concat(hi.ClientCity, ", ", hi.ClientAddress).Trim(',').Trim().Trim(',');
                if (lblClientAddress.Text == String.Empty) tblHeader.Rows[2].Visible = false;
            }

            lblProektant.Text = Resource.ResourceManager["reports_GlavenProektant"];

            lblReportUser.Text = hi.User;
            tblHeader.Rows[4].Visible = hi.User != String.Empty;

            lblReportActivity.Text = hi.Activity;
            tblHeader.Rows[5].Visible = hi.Activity != String.Empty && Request.Params["Type"] != "AN";

            #endregion

            lblCreatedBy.Text = this.LoggedUser.UserName;

            lblReportTitle.Visible = true;
            lblCreatedBy.Visible = true;
            lblCreatedByLbl.Visible = true;


            int activityID = int.Parse(ddlActivity.SelectedValue);
            int userID = int.Parse(ddlUser.SelectedValue);

            DataSet ds = UIHelpers.LoadWorkTimes(projectID, activityID, userID, ddlUserStatus.SelectedValue);

            if (ds == null)
            {
                lblError.Text = Resource.ResourceManager["reports_Minutes_ErrorLoadGrid"];
                return;
            }
            lbPayments.Visible = Request.Params["Type"] == "AN";
            if (Request.Params["Type"] == "AN")
            {

                lblNoDataFound1.Text = "";
                grd.DataSource = ReportsData.ExecuteReportsAuthorsAnalysis(projectID);
                grd.DataBind();
                if (grd.Items.Count == 0)
                {
                    lblNoDataFound1.Visible = true;
                    lblNoDataFound1.Text = Resource.ResourceManager["reports_WorkTimes_NoDataMatch"];
                    grd.Visible = false;
                }
                else
                    grd.Visible = true;
            }
            if (ds.Tables[0].Rows.Count == 0)
            {
                lblNoDataFound.Visible = true;
                lblNoDataFound.Text = Resource.ResourceManager["reports_WorkTimes_NoDataMatch"];
                return;
            }


            DataTable source;

            try
            {
                decimal totalmin = 0;
                source = UIHelpers.CreateGridSource(ds.Tables[0], out totalmin);
                if (Request.Params["Type"] == "AN")
                {
                    totalmin = totalmin / 60;
                    SqlDataReader reader = ProjectsData.SelectProject(projectID);
                    decimal dEur = 0;
                    if (reader.Read())
                    {
                        if (!reader.IsDBNull(28))
                            dEur = reader.GetDecimal(28);
                    }
                    lbTotal.Text = string.Format(Resource.ResourceManager["reports_AN_total"], UIHelpers.FormatDecimal2(totalmin), UIHelpers.FormatDecimal2(dEur), UIHelpers.FormatDecimal2(totalmin * dEur));
                }
            }
            catch (Exception ex)
            {
                log.Error(ex);
                lblError.Text = Resource.ResourceManager["reports_Minutes_ErrorLoadGrid"];
                //throw ex;
                return;
            }

            grdReport.DataSource = source;
            grdReport.DataBind();

        }

		#endregion

        protected string GetImage(object Done)
        {
            if (Done == null || Done == System.DBNull.Value)
                return "~/images/delete.png";
            int nDone = (int)Done;
            if (nDone == 1)
                return "~/images/done.gif";
            return "~/images/delete.png";
        }


		#region Database

		public ProjectsData.ProjectsByStatus ProjectsStatus
		{
			get
			{
				try
				{
					return  (ProjectsData.ProjectsByStatus)int.Parse(ddlProjectStatus.SelectedValue);
				}
				catch { return ProjectsData.ProjectsByStatus.AllProjects; }
			}
		}

        private bool LoadProjects()
        {
            string activityStr = ".";
            ///int projectStatus = int.Parse(ddlProjectStatus.SelectedValue);

            SqlDataReader reader = null;

            try
            {
                ddlProject.Items.Clear();
                ddlProject.Items.Add(new ListItem("<" + Resource.ResourceManager["reports_ddlAllProjects"] + ">", "0"));

                reader = ProjectsData.SelectProjectNamesClear(ProjectsStatus, int.Parse(ddlBuildingTypes.SelectedValue));

                while (reader.Read())
                {
                    int projectID = reader.GetInt32(0);
                    ddlProject.Items.Add(new ListItem(reader.GetString(1), projectID.ToString()));
                    bool hasActivity = reader.IsDBNull(2) ? false : reader.GetBoolean(2);
                    if (!hasActivity) activityStr += projectID.ToString() + ".";
                }
            }
            catch (Exception ex)
            {
                log.Error(ex);
                return false;
            }

            finally
            {
                if (reader != null) reader.Close();
            }

            txtNoActivity.Value = activityStr;
            return true;
        }

        private bool LoadActivities()
        {
            SqlDataReader reader = null;

            try
            {
                ddlActivity.Items.Clear();
                ddlActivity.Items.Add(new ListItem("<" + Resource.ResourceManager["reports_ddlAllActivities"] + ">", "0"));

                reader = DBManager.SelectActivities();

                while (reader.Read())
                {
                    ddlActivity.Items.Add(new ListItem(reader.GetString(1), reader.GetInt32(0).ToString()));
                }
            }
            catch (Exception ex)
            {
                log.Error(ex);
                return false;
            }
            finally
            {
                if (reader != null) reader.Close();
            }

            return true;
        }

        private bool LoadUsers()
        {
            if (this.LoggedUser.HasPaymentRights)
            {
                ddlUser.Visible = true;
                lblPotr.Visible = true;

                SqlDataReader reader = null;
                try
                {
                    switch (ddlUserStatus.SelectedValue)
                    {
                        case "0":
                            reader = UsersData.SelectUserNames(true, false); break;
                        case "1":
                            reader = UsersData.SelectUserNames(false, false); break;
                        case "2":
                            reader = UsersData.SelectUserNames1(); break;
                    }
                    ddlUser.DataSource = reader;
                    ddlUser.DataValueField = "UserID";
                    ddlUser.DataTextField = "FullName";
                    ddlUser.DataBind();
                    ddlUser.Items.Insert(0, new ListItem("<" + Resource.ResourceManager["reports_ddlAllUsers"] + ">", "0"));
                    ddlUser.SelectedValue = "0";
                }
                catch (Exception ex)
                {
                    log.Error(ex);
                    return false;
                }
                finally
                {
                    if (reader != null) reader.Close();
                }
            }
            else
            {
                ddlUserStatus.Visible = false;
                lblPotr.Visible = false;
                ddlUser.Visible = false;
                ddlUser.Items.Insert(0, new ListItem(this.LoggedUser.FullName, this.LoggedUser.UserID.ToString()));
                ddlUser.SelectedIndex = 0;
            }

            return true;
        }



        private bool SetArea(int projectID)
        {
            SqlDataReader reader = null;

            try
            {
                reader = ProjectsData.SelectProject(projectID);
                reader.Read();

                _area = reader.IsDBNull(4) ? 0 : reader.GetInt32(4);

            }
            catch (Exception ex)
            {

                _area = 0;

                log.Error(ex);
                return false;

            }
            finally
            {
                if (reader != null) reader.Close();
            }

            return true;

        }

        private HeaderInfo GetHeaderData()
        {
            int projectID = int.Parse(ddlProject.SelectedValue);

            HeaderInfo hi = new HeaderInfo();

            #region load report header data

            if (projectID > 0)
            {
                SqlDataReader reader = null;
                try
                {
                    reader = ReportsData.SelectReportHeaderInfo(projectID);
                    if (reader.Read())
                    {
                        hi.ProjectName = reader.GetString(1);
                        hi.ProjectCode = reader.IsDBNull(2) ? String.Empty : reader.GetString(2);
                        hi.ProjectAdminName = reader.IsDBNull(3) ? String.Empty : reader.GetString(3);

                        hi.ClientName = reader.IsDBNull(5) ? String.Empty : reader.GetString(5);
                        hi.ClientCity = reader.IsDBNull(6) ? String.Empty : reader.GetString(6);
                        hi.ClientAddress = reader.IsDBNull(7) ? String.Empty : reader.GetString(7);
                    }
                    else
                    {
                        lblInfo.Text = Resource.ResourceManager["reports_DataNotFound"];
                        return null;
                    }
                }
                catch (Exception ex)
                {
                    log.Error(ex);
                    lblInfo.Text = Resource.ResourceManager["reports_ErrorLoadData"];
                    return null;
                }
                finally
                {
                    if (reader != null) reader.Close();
                }
            }
            else
                if (projectID == -1)
                {
                    hi.ProjectAdminName = String.Empty;
                    hi.ProjectName = String.Empty;
                }
                else hi.ProjectName = Resource.ResourceManager["reports_All"];

            #endregion

            if (ddlUser.SelectedValue == "0") hi.User = Resource.ResourceManager["reports_All"];
            else hi.User = ddlUser.SelectedItem.Text;

            if (ddlActivity.SelectedValue == "0") hi.Activity = Resource.ResourceManager["reports_All"];
            else hi.Activity = ddlActivity.SelectedItem.Text;

            hi.BuildingType = String.Empty;

            hi.StartDate = DateTime.Today;
            hi.EndDate = DateTime.Today;

            hi.Proektant = Resource.ResourceManager["reports_GlavenProektant"];

            return hi;
        }

		#endregion

		#region Create report for export

        private MinutesReport CreateReport(bool includeTimesColumn, bool forPDF)
        {
            int projectID = int.Parse(ddlProject.SelectedValue);

            if (projectID == 0)
            {
                lblInfo.Text = Resource.ResourceManager["reports_Minutes_ErrorSelectProject"];
                return null;
            }

            #region Load report header info

            decimal area = 0;
            string projectName = String.Empty, projectCode = String.Empty, administrativeName = String.Empty,
                clientName = String.Empty, clientCity = String.Empty, clientAddress = String.Empty;
            /*
            if (!ProjectsData.SelectProject(projectID, out projectName, out  projectCode, out administrativeName, out area))
            {
                lblError.Text = Resource.ResourceManager["reports_Minutes_ErrorLoadGrid"];
                return null;
            }*/

            SqlDataReader reader = null;
            try
            {
                reader = ReportsData.SelectReportHeaderInfo(projectID);
                if (reader.Read())
                {
                    projectName = reader.GetString(1);
                    projectCode = reader.IsDBNull(2) ? String.Empty : reader.GetString(2);
                    administrativeName = reader.IsDBNull(3) ? String.Empty : reader.GetString(3);
                    area = reader.IsDBNull(4) ? 0 : reader.GetInt32(4);
                    clientName = reader.IsDBNull(5) ? String.Empty : reader.GetString(5);
                    clientCity = reader.IsDBNull(6) ? String.Empty : reader.GetString(6);
                    clientAddress = reader.IsDBNull(7) ? String.Empty : reader.GetString(7);
                }
                else
                {
                    lblInfo.Text = Resource.ResourceManager["reports_DataNotFound"];
                    return null;
                }
            }
            catch (Exception ex)
            {
                log.Error(ex);
                lblInfo.Text = Resource.ResourceManager["reports_ErrorLoadData"];
            }
            finally
            {
                if (reader != null) reader.Close();
            }

            #endregion

            int activityID = int.Parse(ddlActivity.SelectedValue);
            int userID = int.Parse(ddlUser.SelectedValue);

            HeaderInfo hi = GetHeaderData();

            DataSet ds = UIHelpers.LoadWorkTimes(projectID, activityID, userID, ddlUserStatus.SelectedValue);
            if (ds == null)
            {
                lblError.Text = Resource.ResourceManager["reports_Minutes_ErrorLoadGrid"];
                return null;
            }

            ds.Tables[0].Columns.Add(new DataColumn("Time", System.Type.GetType("System.Int32"), "0"));

            MinutesReport minutesReport = new MinutesReport(hi, this.LoggedUser.UserName, includeTimesColumn, forPDF);
            if (ds.Tables[0].Rows.Count > 0) minutesReport.DataSource = ds.Tables[0];

            return minutesReport;
        }

		#endregion

		#region HeaderInfo

		public class HeaderInfo
		{
			public string ProjectName = String.Empty;
			public string ProjectCode = String.Empty;
			public string ProjectAdminName = String.Empty;
			public string ClientName = String.Empty;
			public string ClientCity = String.Empty;
			public string ClientAddress = String.Empty;
			public string Proektant = String.Empty;
			public string User = String.Empty;
			public string Activity = String.Empty;
			public string BuildingType = String.Empty;
			public DateTime StartDate;
			public DateTime EndDate;

		}
		#endregion

		#region Menu

//		private void CreateMenu()
//		{
//			UserControls.MenuTable menu = new UserControls.MenuTable();
//			menu.ID = "MenuTable";
//
//			// 'Links' group
//			ArrayList menuItems = new ArrayList();
//
//			menuItems.Add(new MenuItemInfo(Resource.ResourceManager["menuItem_Hours"], "../Hours.aspx", true));
//			menuItems.Add(new MenuItemInfo(Resource.ResourceManager["menuItem_Projects"], "../Projects.aspx"));
//			if (LoggedUser.IsLeader || LoggedUser.IsAssistant) 
//			{
//				menuItems.Add(new MenuItemInfo(Resource.ResourceManager["menuItem_Clients"], Request.ApplicationPath+"/Clients.aspx"));
//								menuItems.Add(new MenuItemInfo(Resource.ResourceManager["menuItem_Subcontracters"], "../Subcontracters.aspx"));
//				if (LoggedUser.IsLeader)
//				menuItems.Add(new MenuItemInfo(Resource.ResourceManager["menuItem_Users"], "../Users.aspx"));	
//			}
//			menuItems.Add(new MenuItemInfo(Resource.ResourceManager["menuItem_Emails"], "../Emails.aspx"));
//
//			menu.AddMenuGroup(Resource.ResourceManager["menuHeader_Links"], 20, menuItems);
//			
//			// 'New' group
//			menuItems = new ArrayList();
//			menuItems.Add(new MenuItemInfo(Resource.ResourceManager["menuItem_NewProject"], "../EditProject.aspx"));
//			if (LoggedUser.IsLeader || LoggedUser.IsAssistant)
//			{
//				menuItems.Add(new MenuItemInfo(Resource.ResourceManager["menuItem_NewClient"], "../EditClient.aspx"));
//				if (LoggedUser.IsLeader)
//				menuItems.Add(new MenuItemInfo(Resource.ResourceManager["menuItem_NewUser"], "../EditUser.aspx"));
//			}
//			menuItems.Add(new MenuItemInfo(Resource.ResourceManager["menuItem_NewEmail"], "../EditEmail.aspx"));
//			menuItems.Add(new MenuItemInfo(Resource.ResourceManager["newcontracter_PageTitle"], "../EditSubContracter.aspx"));
//
//
//			menu.AddMenuGroup(Resource.ResourceManager["menuHeader_New"], 30, menuItems);
//
//			// "Reports' group
//			menuItems = new ArrayList();
//			menuItems.Add(new MenuItemInfo(Resource.ResourceManager["menuItem_WorkTimeReport"], "WorkTimes.aspx"));
//			//menuItems.Add(new MenuItemInfo(Resource.ResourceManager["menuItem_MinutesReport"], "../Minutes.aspx"));
//			if (LoggedUser.IsLeader || LoggedUser.IsAssistant)
//				menuItems.Add(new MenuItemInfo(Resource.ResourceManager["menuItem_MailReport"], "../MailForm.aspx"));
//			if (LoggedUser.IsLeader)
//			{
//				menuItems.Add(new MenuItemInfo(Resource.ResourceManager["menuItem_NotEnteredWorkTime"], "EmptyHours.aspx"));
//				menuItems.Add(new MenuItemInfo(Resource.ResourceManager["menuItem_Calendar"], "Calendar.aspx"));
//			}
//			menu.AddMenuGroup(Resource.ResourceManager["menuHeader_Reports"], 30, menuItems);
//
//			menuItems = new ArrayList();
//			menuItems.Add(new MenuItemInfo(Resource.ResourceManager["menuItem_Help"], SessionManager.GetReportsHelpLink(),true,Pages.Minutes));
//			menu.AddMenuGroup("", 10, menuItems);
//
//            menuHolder.Controls.Add(menu);
//		}

		#endregion

        private void ddlProjectStatus_SelectedIndexChanged(object sender, System.EventArgs e)
        {
            LoadProjects();
        }

        private void ddlBuildingTypes_SelectedIndexChanged(object sender, System.EventArgs e)
        {
            LoadProjects();
        }

        private void grd_ItemDataBound(object sender, System.Web.UI.WebControls.DataGridItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Item
                || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                if (e.Item.Cells[0].Text == "&nbsp;")
                {
                    e.Item.CssClass = "ReportsTotal";
                    e.Item.Cells[4].Text = "";
                    e.Item.Cells[5].Text = "";


                }


            }
        }

        private void ddlUserStatus_SelectedIndexChanged(object sender, System.EventArgs e)
        {
            LoadUsers();
        }

		

	
	
	}
}
