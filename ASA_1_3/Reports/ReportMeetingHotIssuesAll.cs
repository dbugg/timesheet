using System;
using System.Data;
using Asa.Timesheet.Data;
using System.Data.SqlClient;
using GrapeCity.ActiveReports.Data;
using GrapeCity.ActiveReports;
using GrapeCity.ActiveReports.Controls;
using GrapeCity.ActiveReports.SectionReportModel;
using GrapeCity.ActiveReports.Document.Section;
using GrapeCity.ActiveReports.Document;

namespace Asa.Timesheet.WebPages.Reports
{
    public class ReportMeetingHotIssuesAll : GrapeCity.ActiveReports.SectionReport
	{
		private DateTime _dateOfMeeting = DateTime.MinValue;
		private string _startHour = string.Empty;
		private string _endHour = string.Empty;
		private string _placeOfMeeting = string.Empty;
        private string[] _DocumentData;
		private string _tema = string.Empty;
		private int _count = 1;
		private string catName = "";
		private string _meetingNumber = "";
		private bool _all=true;
		private string _user="";
		private int _countAll=0;
		//private int _MAX_CHARS=100;
        private bool _ASA = false;
        private ReportHeader ReportHeader;
        private TextBox TextBox20;
        private SubReport srUsers;
        private TextBox textBox1;
        private TextBox txtDate1;
        private TextBox txtMeetingNumber1;
        private TextBox textBox5;
        private TextBox textBox6;
        private Label Label1;
        private Label Label2;
        private Label Label3;
        private Label Label4;
        private Label Label5;
        private Label Label6;
        private Label Label7;
        private Label Label8;
        private Label Label9;
        private Label Label10;
        private Line Line3;
        private Line Line4;
        private Line line1;
        private PageBreak PageBreak1;
        private PageHeader PageHeader;
        private TextBox lbTema;
        private TextBox TextBox25;
        private TextBox TextBox26;
        private Label label11;
        private Label label12;
        private Label label13;
        private Label label14;
        private Label label15;
        private Label label16;
        private Label label17;
        private Label label18;
        private Label label19;
        private Label label20;
        private Line line2;
        private Line line8;
        private Line line9;
        private Detail Detail;
        private Picture picUndone;
        private Picture picDone;
        private TextBox txtStop;
        private SubReport srHotIssue;
        private TextBox txtName;
        private TextBox txtNumber;
        private TextBox txtStatusDate;
        private TextBox txtDeadline;
        private PageFooter PageFooter;
        private TextBox txtContacts;
        private Line Line5;
        private TextBox page3;
        private TextBox page1;
        private Label page2;
        private Label lblCreated;
        private TextBox txtCreatedBy;
        private TextBox TextBox36;
        private TextBox TextBox37;
        private TextBox TextBox38;
        private TextBox TextBox39;
        private TextBox TextBox40;
        private TextBox TextBox41;
        private TextBox TextBox42;
        private TextBox TextBox43;
        private Picture picture1;
        private Line line19;
        private ReportFooter ReportFooter;
        private Label label21;
        private Line line10;
        private Line line18;
        private PageBreak pageBreak2;
        private Picture picture2;
        private SubReport srTask;
        private TextBox txtDiscription;
        private TextBox txtAssignedToType;
        private TextBox txtStatus;
        private TextBox txtStatusID;
        private TextBox txtPosition;
        private TextBox txtStopped;
        private GroupHeader GroupHeader1;
        private Line Line23;
        private TextBox txtCategoryName;
        private Line lineCat;
        private GroupFooter GroupFooter1;
        private TextBox txtAccToName;
        private TextBox txtComment;
        private TextBox txtAssignedTo;
        private TextBox txtHotIssueMainID;
        private TextBox txt;
        private TextBox textBox2;
        private Line Line6;
        private SubReport srProjects1;
        private Line line14;
        private TextBox txtDocuments;
		private DataView _dvProjects;
        public ReportMeetingHotIssuesAll(DateTime dateOfMeeting, string startHour, string endHour, string placeOfMeeting, string tema, DataView dvProjects, DataView dvUsers, HotIssuesVector hiv, string meetingNumber, bool bAll, string other, string mails, int nProjectID, string user, int count, bool ASA, string[] DocumentData = null)
        {
            _dvProjects = dvProjects;
            _all = bAll;
            _ASA = ASA;
            _user = user;
            _countAll = count;
            _DocumentData = DocumentData;
            InitializeComponent();

            this._placeOfMeeting = placeOfMeeting;
            this._dateOfMeeting = dateOfMeeting;
            this._startHour = startHour;
            this._endHour = endHour;


            this._tema = tema;
            this._meetingNumber = meetingNumber;
            //txtDateNow.Text=DateTime.Now.Date.ToShortDateString();
            txtCreatedBy.Text = user + txtCreatedBy.Text;


            srProjects1.Report = new ReportMeetingHotIssuesProjects(dvProjects, false);
            srUsers.Report = new ReportMeetingHotIssuesUsers(dvUsers);
            //srUsers1.Report = new ReportMeetingHotIssuesUsers(UIHelpers.HotIssueCreateDVusers(mails));
            //this.ReportEnd+=new EventHandler(SubAnalysisRpt_ReportEnd);
            //			DataView dvHIV = UIHelpers.CreateDataViewFromHotIssuesVector(hiv, bAll);
            //			dvHIV.Sort = "HotIssueCategoryID";
            //			this.DataSource= dvHIV;


            string ds = Serialization.SerializeObjectToXmlString(hiv);

            XMLDataSource source;
            source = new XMLDataSource();
            source.LoadXML((string)ds);
            source.RecordsetPattern = "//HotIssueData";
            this.DataSource = source;


            //			if(dvHIV.Table.Rows.Count==0)
            //				this.txtNoIssueFound.Visible = true;
            //			else 
            //				this.txtNoIssueFound.Visible = false;

            this.PageSettings.Margins.Top = 0.4f;
            this.PageSettings.Margins.Bottom = 0.2f;
            this.PageSettings.Gutter = 0F;
            this.PageSettings.Margins.Left = 1.2f;
            this.PageSettings.Margins.Right = 0.2f;




            //this.txtDate.Text = _placeOfMeeting + " / " + _dateOfMeeting.ToString("dd.MM.yyyy");

            //this.txtDate1.Text = _dateOfMeeting.ToString("dd.MM.yyyy");
            this._meetingNumber = _DocumentData[0]; // Manual ID
            this.txtDate1.Text = _DocumentData[1]; // Manual Date
            this.txtDocuments.Text = _DocumentData[2];
            if (_count == 0) { this.PageHeader.Visible = false; } else { this.PageHeader.Visible = true; };

        }


        private void ReportHeader_Format(object sender, System.EventArgs eArgs)
        {
            if (_meetingNumber.Trim() == "")
            {
                txtMeetingNumber1.Text = "1";
               
            }
            else
            {
                txtMeetingNumber1.Text = _meetingNumber;
            }
        }
        private void GroupHeader1_Format(object sender, System.EventArgs eArgs)
        {
            this.txtCategoryName.Text = Resource.ResourceManager["Name_Tema"] + " - " + this.txtCategoryName.Text;
            if (this.txtCategoryName.Text == catName)
            {
                //				this.txtCategoryName.Visible = lineCat.Visible= false;
                //				txtName.Location= new System.Drawing.PointF(txtName.Location.X,0f);
                //				txtNumber.Location= new System.Drawing.PointF(txtNumber.Location.X,0f);
                //				txtDiscription.Location= new System.Drawing.PointF(txtDiscription.Location.X,0.36f);
                //				txtComment.Location= new System.Drawing.PointF(txtComment.Location.X,0.625f);
                //				txtAccToName.Location= new System.Drawing.PointF(txtAccToName.Location.X,0.313f);
                //				txtStatusDate.Location= new System.Drawing.PointF(txtAccToName.Location.X,0.063f);
                //				srTask.Location= new System.Drawing.PointF(srTask.Location.X,0f);
                //				srHotIssue.Location= new System.Drawing.PointF(srHotIssue.Location.X,0.063f);
                //				picDone.Location= new System.Drawing.PointF(picDone.Location.X,0.063f);
                //				picUndone.Location= new System.Drawing.PointF(picUndone.Location.X,0.063f);
                //_count++;
                GroupHeader1.Visible = false;
            }
            else
            {
                this.txtCategoryName.Visible = lineCat.Visible = true;
                GroupHeader1.Visible = true;

                //catName = this.txtCategoryName.Text;
                //_count =1;
            }

        }

        private void Detail_Format(object sender, System.EventArgs eArgs)
        {
            if (this.txtCategoryName.Text == catName)
            {
                //				this.txtCategoryName.Visible = lineCat.Visible= false;
                //				txtName.Location= new System.Drawing.PointF(txtName.Location.X,0f);
                //				txtNumber.Location= new System.Drawing.PointF(txtNumber.Location.X,0f);
                //				txtDiscription.Location= new System.Drawing.PointF(txtDiscription.Location.X,0.36f);
                //				txtComment.Location= new System.Drawing.PointF(txtComment.Location.X,0.625f);
                //				txtAccToName.Location= new System.Drawing.PointF(txtAccToName.Location.X,0.313f);
                //				txtStatusDate.Location= new System.Drawing.PointF(txtAccToName.Location.X,0.063f);
                //				srTask.Location= new System.Drawing.PointF(srTask.Location.X,0f);
                //				srHotIssue.Location= new System.Drawing.PointF(srHotIssue.Location.X,0.063f);
                //				picDone.Location= new System.Drawing.PointF(picDone.Location.X,0.063f);
                //				picUndone.Location= new System.Drawing.PointF(picUndone.Location.X,0.063f);
                _count++;
            }
            else
            {
                catName = this.txtCategoryName.Text;
                _count = 1;
            }
            System.Drawing.Font f = new System.Drawing.Font(txtName.Font.FontFamily.Name, txtName.Font.SizeInPoints, System.Drawing.FontStyle.Regular);

            bool bNormal = true;
            try
            {
                txtStop.Visible = txtStopped.Text.Trim() != "";
                this.txtNumber.Text = string.Concat(_count.ToString(), ".");
                int mainID = UIHelpers.ToInt(this.txtHotIssueMainID.Text);


                int AccTo = UIHelpers.ToInt(this.txtAssignedTo.Text);
                int accToType = UIHelpers.ToInt(this.txtAssignedToType.Text);
                //				if(txtName!= null && txtName.Text.Length>_MAX_CHARS)
                //					txtDiscription.Visible=false;
                //				else
                //					txtDiscription.Visible=true;
                //				if(txtDiscription!= null && txtDiscription.Text.Length>_MAX_CHARS)
                //					txtComment.Visible=false;
                //				else
                //					txtComment.Visible=true;
                //txtComment.Location= new System.Drawing.PointF(txtDiscription.Location.X,txtDiscription.Location.Y);
                //txtComment.Visible=true;
                txtDiscription.Visible = false;
                string sText = txt.Text;
                txtDiscription.Visible = false;

                if (sText != null && sText.Trim().Length > 0 && AccTo > 0 && accToType > 0)
                    sText = ", " + sText;

                HotIssuesVector hv = HotIssueDAL.LoadCollectionOldIssues(mainID, _ASA);
                DateTime dt = new DateTime();
                int nStatusID = UIHelpers.ToInt(txtStatusID.Text);
                //txtDeadline.Text=UIHelpers.FormatDate(hid.
                foreach (HotIssueData hid in hv)
                {
                    if (hid.HotIssueStatusID == nStatusID)
                        dt = hid.CreatedDate;
                }
                if (dt != new DateTime())
                    txtStatusDate.Text = txtStatus.Text + " - " + UIHelpers.FormatDate(dt);
                else
                    txtStatusDate.Text = txtStatus.Text;

                this.txtAccToName.Text = UIHelpers.GetHotIssueUserNameAssigned(AccTo, accToType) + sText;
                bool bDone = (txtStatusID.Text == System.Configuration.ConfigurationManager.AppSettings["HotIssueStatusFinished"]);
                picDone.Visible = bDone;

                int nDays = int.Parse(System.Configuration.ConfigurationManager.AppSettings["EditDays"]);
                HotIssuesVector hivAll = HotIssueDAL.LoadCollection(-1, -1, -1,/*-1,*/mainID, true, -1, -1, Constants.DateMax, _dateOfMeeting.AddDays(nDays), _ASA, true, null);
                picUndone.Visible = false;
                if (hivAll.Count > 0)
                {
                    HotIssueData hidd = hivAll[0];
                    if (_dateOfMeeting > hidd.HotIssueDeadline)
                        picUndone.Visible = true;

                    //System.Drawing.Font f = null;

                    if (hidd.HotIssueStatusID == int.Parse(System.Configuration.ConfigurationManager.AppSettings["HotIssueStatusFinished"]))
                    {
                        f = new System.Drawing.Font(txtName.Font.FontFamily.Name, txtName.Font.SizeInPoints, System.Drawing.FontStyle.Italic);
                        bNormal = false;

                    }
                    else if (hidd.HotIssueStatusID == int.Parse(System.Configuration.ConfigurationManager.AppSettings["HotIssueStatusDeletedTask"]))
                    {
                        f = new System.Drawing.Font(txtName.Font.FontFamily.Name, txtName.Font.SizeInPoints, System.Drawing.FontStyle.Strikeout);
                        //txtComment.Font = f;
                        bNormal = false;
                    }
                    else
                    {
                        f = new System.Drawing.Font(txtName.Font.FontFamily.Name, txtName.Font.SizeInPoints, System.Drawing.FontStyle.Regular);
                        bNormal = true;

                    }

                    txtName.Font = txtDiscription.Font = txtStatusDate.Font = txtAccToName.Font = f;
                    /*if(bNormal)
                    {
                        f =new System.Drawing.Font(txtName.Font.FontFamily.Name,txtName.Font.SizeInPoints,System.Drawing.FontStyle.Italic);
                        txtComment.Font=f;
                    }*/
                }
                //this.srHotIssue.Report  =  new ReportMeetingHotIssuesHotIssue(mainID,_dateOfMeeting,_all,_ASA);
            }
            catch { }
            this.srTask.Report = new ReportMeetingHotIssuesTask(txtDiscription.Text, txtComment.Text, txtName.Text, f, bNormal, txtPosition.Text, txtAccToName.Text);
        }



		#region ActiveReports Designer generated code

        public void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ReportMeetingHotIssuesAll));
            this.ReportHeader = new GrapeCity.ActiveReports.SectionReportModel.ReportHeader();
            this.TextBox20 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.srUsers = new GrapeCity.ActiveReports.SectionReportModel.SubReport();
            this.Line6 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.srProjects1 = new GrapeCity.ActiveReports.SectionReportModel.SubReport();
            this.textBox1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtDate1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtMeetingNumber1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox5 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox6 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.Label1 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label2 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label3 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label4 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label5 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label6 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label7 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label8 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label9 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label10 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Line3 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.Line4 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line1 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.PageBreak1 = new GrapeCity.ActiveReports.SectionReportModel.PageBreak();
            this.picture2 = new GrapeCity.ActiveReports.SectionReportModel.Picture();
            this.textBox2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.line14 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.PageHeader = new GrapeCity.ActiveReports.SectionReportModel.PageHeader();
            this.lbTema = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.TextBox25 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.TextBox26 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.label11 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.label12 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.label13 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.label14 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.label15 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.label16 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.label17 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.label18 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.label19 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.label20 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.line2 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line8 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line9 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.Detail = new GrapeCity.ActiveReports.SectionReportModel.Detail();
            this.picUndone = new GrapeCity.ActiveReports.SectionReportModel.Picture();
            this.picDone = new GrapeCity.ActiveReports.SectionReportModel.Picture();
            this.srTask = new GrapeCity.ActiveReports.SectionReportModel.SubReport();
            this.txtStop = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.srHotIssue = new GrapeCity.ActiveReports.SectionReportModel.SubReport();
            this.txtName = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtDiscription = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtComment = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtAccToName = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtAssignedToType = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtAssignedTo = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtHotIssueMainID = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtNumber = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtStatus = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtStatusID = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txt = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtStatusDate = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtPosition = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtDeadline = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtStopped = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.PageFooter = new GrapeCity.ActiveReports.SectionReportModel.PageFooter();
            this.txtContacts = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.Line5 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.page3 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.page1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.page2 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lblCreated = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.txtCreatedBy = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.TextBox36 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.TextBox37 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.TextBox38 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.TextBox39 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.TextBox40 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.TextBox41 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.TextBox42 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.TextBox43 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.picture1 = new GrapeCity.ActiveReports.SectionReportModel.Picture();
            this.line19 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.ReportFooter = new GrapeCity.ActiveReports.SectionReportModel.ReportFooter();
            this.label21 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.line10 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line18 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.pageBreak2 = new GrapeCity.ActiveReports.SectionReportModel.PageBreak();
            this.txtDocuments = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.GroupHeader1 = new GrapeCity.ActiveReports.SectionReportModel.GroupHeader();
            this.Line23 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.txtCategoryName = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.lineCat = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.GroupFooter1 = new GrapeCity.ActiveReports.SectionReportModel.GroupFooter();
            ((System.ComponentModel.ISupportInitialize)(this.TextBox20)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDate1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtMeetingNumber1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picture2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbTema)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TextBox25)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TextBox26)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label11)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label12)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label13)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label14)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label15)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label16)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label17)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label18)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label19)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label20)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picUndone)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picDone)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtStop)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDiscription)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtComment)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAccToName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAssignedToType)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAssignedTo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtHotIssueMainID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtNumber)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtStatus)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtStatusID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtStatusDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPosition)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDeadline)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtStopped)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtContacts)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.page3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.page1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.page2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblCreated)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCreatedBy)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TextBox36)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TextBox37)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TextBox38)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TextBox39)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TextBox40)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TextBox41)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TextBox42)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TextBox43)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picture1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label21)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDocuments)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCategoryName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            // 
            // ReportHeader
            // 
            this.ReportHeader.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
            this.TextBox20,
            this.srUsers,
            this.Line6,
            this.srProjects1,
            this.textBox1,
            this.txtDate1,
            this.txtMeetingNumber1,
            this.textBox5,
            this.textBox6,
            this.Label1,
            this.Label2,
            this.Label3,
            this.Label4,
            this.Label5,
            this.Label6,
            this.Label7,
            this.Label8,
            this.Label9,
            this.Label10,
            this.Line3,
            this.Line4,
            this.line1,
            this.PageBreak1,
            this.picture2,
            this.textBox2,
            this.line14});
            this.ReportHeader.Height = 6.154168F;
            this.ReportHeader.Name = "ReportHeader";
            this.ReportHeader.Format += new System.EventHandler(this.ReportHeader_Format);
            // 
            // TextBox20
            // 
            this.TextBox20.Height = 0.2F;
            this.TextBox20.Left = 0F;
            this.TextBox20.Name = "TextBox20";
            this.TextBox20.Style = "font-family: Verdana; font-size: 9pt; font-style: normal; font-weight: bold; text" +
    "-align: left; text-decoration: none; ddo-char-set: 1";
            this.TextBox20.Text = "����������� / Attendees :";
            this.TextBox20.Top = 4.687F;
            this.TextBox20.Width = 2.5625F;
            // 
            // srUsers
            // 
            this.srUsers.CloseBorder = false;
            this.srUsers.Height = 0.1875F;
            this.srUsers.Left = 0F;
            this.srUsers.Name = "srUsers";
            this.srUsers.Report = null;
            this.srUsers.Top = 4.937F;
            this.srUsers.Width = 8F;
            // 
            // Line6
            // 
            this.Line6.Height = 0.005415916F;
            this.Line6.Left = 0F;
            this.Line6.LineWeight = 1F;
            this.Line6.Name = "Line6";
            this.Line6.Top = 4.624001F;
            this.Line6.Width = 8.062F;
            this.Line6.X1 = 0F;
            this.Line6.X2 = 8.062F;
            this.Line6.Y1 = 4.629417F;
            this.Line6.Y2 = 4.624001F;
            // 
            // srProjects1
            // 
            this.srProjects1.CanGrow = false;
            this.srProjects1.CanShrink = false;
            this.srProjects1.CloseBorder = false;
            this.srProjects1.Height = 1.562F;
            this.srProjects1.Left = 0F;
            this.srProjects1.Name = "srProjects1";
            this.srProjects1.Report = null;
            this.srProjects1.Top = 2.75F;
            this.srProjects1.Width = 7.012001F;
            // 
            // textBox1
            // 
            this.textBox1.Height = 0.4920002F;
            this.textBox1.Left = 0.01200003F;
            this.textBox1.Name = "textBox1";
            this.textBox1.Style = "font-family: Verdana; font-size: 11pt; font-weight: bold; text-align: left; text-" +
    "decoration: none; ddo-char-set: 1";
            this.textBox1.Text = "�� ��������� ���������� �� ��������� ����������� \r\n�� �����/���������� ������/���" +
    "���� ������ ��: ";
            this.textBox1.Top = 2.258917F;
            this.textBox1.Width = 5.017F;
            // 
            // txtDate1
            // 
            this.txtDate1.Height = 0.24F;
            this.txtDate1.Left = 2.21F;
            this.txtDate1.Name = "txtDate1";
            this.txtDate1.Style = "font-family: Verdana; font-size: 11pt; font-weight: bold; ddo-char-set: 1";
            this.txtDate1.Text = "$����";
            this.txtDate1.Top = 2.018917F;
            this.txtDate1.Width = 1.252F;
            // 
            // txtMeetingNumber1
            // 
            this.txtMeetingNumber1.Height = 0.24F;
            this.txtMeetingNumber1.Left = 1.2F;
            this.txtMeetingNumber1.Name = "txtMeetingNumber1";
            this.txtMeetingNumber1.Style = "font-family: Verdana; font-size: 11pt; font-weight: bold; ddo-char-set: 1";
            this.txtMeetingNumber1.Text = "$����������";
            this.txtMeetingNumber1.Top = 2.018917F;
            this.txtMeetingNumber1.Width = 0.75F;
            // 
            // textBox5
            // 
            this.textBox5.Height = 0.2396663F;
            this.textBox5.Left = 0.01200003F;
            this.textBox5.Name = "textBox5";
            this.textBox5.Style = "font-family: Verdana; font-size: 11pt; font-weight: bold; text-align: left; text-" +
    "decoration: none; ddo-char-set: 1";
            this.textBox5.Text = "�������� �";
            this.textBox5.Top = 2.018917F;
            this.textBox5.Width = 1.188F;
            // 
            // textBox6
            // 
            this.textBox6.Height = 0.24F;
            this.textBox6.Left = 1.95F;
            this.textBox6.Name = "textBox6";
            this.textBox6.Style = "font-family: Verdana; font-size: 11pt; font-weight: normal; text-align: left; tex" +
    "t-decoration: none; ddo-char-set: 1";
            this.textBox6.Text = "��";
            this.textBox6.Top = 2.018917F;
            this.textBox6.Width = 0.26F;
            // 
            // Label1
            // 
            this.Label1.Height = 0.2F;
            this.Label1.HyperLink = null;
            this.Label1.Left = 0.007F;
            this.Label1.Name = "Label1";
            this.Label1.Style = "";
            this.Label1.Text = "������������ �� ���������";
            this.Label1.Top = 0.204F;
            this.Label1.Width = 2.125F;
            // 
            // Label2
            // 
            this.Label2.Height = 0.2F;
            this.Label2.HyperLink = null;
            this.Label2.Left = 0.007F;
            this.Label2.Name = "Label2";
            this.Label2.Style = "font-style: italic; ddo-char-set: 1";
            this.Label2.Text = " �Besprechungsprotokoll GP�";
            this.Label2.Top = 0.448F;
            this.Label2.Width = 2.365F;
            // 
            // Label3
            // 
            this.Label3.Height = 0.2F;
            this.Label3.HyperLink = null;
            this.Label3.Left = 0F;
            this.Label3.Name = "Label3";
            this.Label3.Style = "";
            this.Label3.Text = "��������� �� ���������� � �ϓ";
            this.Label3.Top = 0.648F;
            this.Label3.Width = 2.125F;
            // 
            // Label4
            // 
            this.Label4.Height = 0.2F;
            this.Label4.HyperLink = null;
            this.Label4.Left = 4.5F;
            this.Label4.Name = "Label4";
            this.Label4.Style = "font-style: italic; ddo-char-set: 1";
            this.Label4.Text = "Version:";
            this.Label4.Top = 0.248F;
            this.Label4.Width = 0.802F;
            // 
            // Label5
            // 
            this.Label5.Height = 0.2F;
            this.Label5.HyperLink = null;
            this.Label5.Left = 0F;
            this.Label5.Name = "Label5";
            this.Label5.Style = "font-style: italic; ddo-char-set: 1";
            this.Label5.Text = "Dokumentbezeichnung:";
            this.Label5.Top = 0.04799998F;
            this.Label5.Width = 2.125F;
            // 
            // Label6
            // 
            this.Label6.Height = 0.2F;
            this.Label6.HyperLink = null;
            this.Label6.Left = 6.032F;
            this.Label6.Name = "Label6";
            this.Label6.Style = "font-style: italic; ddo-char-set: 1";
            this.Label6.Text = "Dokumentnummer:";
            this.Label6.Top = 0.04799998F;
            this.Label6.Width = 1.281F;
            // 
            // Label7
            // 
            this.Label7.Height = 0.2F;
            this.Label7.HyperLink = null;
            this.Label7.Left = 4.5F;
            this.Label7.Name = "Label7";
            this.Label7.Style = "";
            this.Label7.Text = "������:";
            this.Label7.Top = 0.04799998F;
            this.Label7.Width = 0.802F;
            // 
            // Label8
            // 
            this.Label8.Height = 0.2F;
            this.Label8.HyperLink = null;
            this.Label8.Left = 4.5F;
            this.Label8.Name = "Label8";
            this.Label8.Style = "";
            this.Label8.Text = "V160620";
            this.Label8.Top = 0.5149999F;
            this.Label8.Width = 0.6350002F;
            // 
            // Label9
            // 
            this.Label9.Height = 0.2F;
            this.Label9.HyperLink = null;
            this.Label9.Left = 6.032F;
            this.Label9.Name = "Label9";
            this.Label9.Style = "";
            this.Label9.Text = "����� �� ���������";
            this.Label9.Top = 0.248F;
            this.Label9.Width = 1.59F;
            // 
            // Label10
            // 
            this.Label10.Height = 0.2F;
            this.Label10.HyperLink = null;
            this.Label10.Left = 6.032F;
            this.Label10.Name = "Label10";
            this.Label10.Style = "";
            this.Label10.Text = "BG-B-03.4";
            this.Label10.Top = 0.5149999F;
            this.Label10.Width = 1.418F;
            // 
            // Line3
            // 
            this.Line3.Height = 0F;
            this.Line3.Left = 0F;
            this.Line3.LineWeight = 1F;
            this.Line3.Name = "Line3";
            this.Line3.Top = 0.448F;
            this.Line3.Width = 8.062F;
            this.Line3.X1 = 0F;
            this.Line3.X2 = 8.062F;
            this.Line3.Y1 = 0.448F;
            this.Line3.Y2 = 0.448F;
            // 
            // Line4
            // 
            this.Line4.Height = 0.7791666F;
            this.Line4.Left = 4.427F;
            this.Line4.LineWeight = 1F;
            this.Line4.Name = "Line4";
            this.Line4.Top = 0.04799998F;
            this.Line4.Width = 0.0007500648F;
            this.Line4.X1 = 4.427F;
            this.Line4.X2 = 4.42775F;
            this.Line4.Y1 = 0.04799998F;
            this.Line4.Y2 = 0.8271666F;
            // 
            // line1
            // 
            this.line1.Height = 0.7791666F;
            this.line1.Left = 5.992F;
            this.line1.LineWeight = 1F;
            this.line1.Name = "line1";
            this.line1.Top = 0.04799998F;
            this.line1.Width = 0.0007500648F;
            this.line1.X1 = 5.992F;
            this.line1.X2 = 5.99275F;
            this.line1.Y1 = 0.04799998F;
            this.line1.Y2 = 0.8271666F;
            // 
            // PageBreak1
            // 
            this.PageBreak1.Height = 0.01F;
            this.PageBreak1.Left = 0F;
            this.PageBreak1.Name = "PageBreak1";
            this.PageBreak1.Size = new System.Drawing.SizeF(6.5F, 0.01F);
            this.PageBreak1.Top = 6.125F;
            this.PageBreak1.Width = 6.5F;
            // 
            // picture2
            // 
            this.picture2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.picture2.Height = 0.314F;
            this.picture2.HyperLink = null;
            this.picture2.ImageData = ((System.IO.Stream)(resources.GetObject("picture2.ImageData")));
            this.picture2.Left = 5.062F;
            this.picture2.LineColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.picture2.Name = "picture2";
            this.picture2.PictureAlignment = GrapeCity.ActiveReports.SectionReportModel.PictureAlignment.BottomRight;
            this.picture2.SizeMode = GrapeCity.ActiveReports.SectionReportModel.SizeModes.Zoom;
            this.picture2.Top = 5.811F;
            this.picture2.Width = 2.952756F;
            // 
            // textBox2
            // 
            this.textBox2.Height = 0.3129999F;
            this.textBox2.Left = 0.012F;
            this.textBox2.Name = "textBox2";
            this.textBox2.Style = "font-family: Verdana; font-size: 7pt; font-style: normal; font-weight: normal; te" +
    "xt-align: left; text-decoration: none; vertical-align: bottom; ddo-char-set: 1";
            this.textBox2.Text = "���������� �� �������� ���� ����������� �� ������������. ���������� �� ������� ��" +
    " ����� �� ������������. � ������� �� ����������� ������������, �� ������� ����� " +
    "���� �� ������� � ���������.  ";
            this.textBox2.Top = 5.561F;
            this.textBox2.Width = 5.437F;
            // 
            // line14
            // 
            this.line14.Height = 0.001000881F;
            this.line14.Left = 0F;
            this.line14.LineWeight = 1F;
            this.line14.Name = "line14";
            this.line14.Top = 5.873F;
            this.line14.Width = 8.062F;
            this.line14.X1 = 0F;
            this.line14.X2 = 8.062F;
            this.line14.Y1 = 5.873F;
            this.line14.Y2 = 5.874001F;
            // 
            // PageHeader
            // 
            this.PageHeader.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
            this.lbTema,
            this.TextBox25,
            this.TextBox26,
            this.label11,
            this.label12,
            this.label13,
            this.label14,
            this.label15,
            this.label16,
            this.label17,
            this.label18,
            this.label19,
            this.label20,
            this.line2,
            this.line8,
            this.line9});
            this.PageHeader.Height = 1.03625F;
            this.PageHeader.Name = "PageHeader";
            // 
            // lbTema
            // 
            this.lbTema.Height = 0.2F;
            this.lbTema.Left = 0.062F;
            this.lbTema.Name = "lbTema";
            this.lbTema.Style = "font-family: Verdana; font-size: 8pt; font-weight: bold; text-align: left; ddo-ch" +
    "ar-set: 1";
            this.lbTema.Text = "����";
            this.lbTema.Top = 0.836F;
            this.lbTema.Visible = false;
            this.lbTema.Width = 1.0625F;
            // 
            // TextBox25
            // 
            this.TextBox25.Height = 0.2F;
            this.TextBox25.Left = 4.687F;
            this.TextBox25.Name = "TextBox25";
            this.TextBox25.Style = "font-family: Verdana; font-size: 8pt; font-weight: bold; text-align: center; ddo-" +
    "char-set: 1";
            this.TextBox25.Text = "������ / ���������";
            this.TextBox25.Top = 0.836F;
            this.TextBox25.Width = 2.0625F;
            // 
            // TextBox26
            // 
            this.TextBox26.Height = 0.2F;
            this.TextBox26.Left = 7.125F;
            this.TextBox26.Name = "TextBox26";
            this.TextBox26.Style = "font-family: Verdana; font-size: 8pt; font-weight: bold; text-align: center; ddo-" +
    "char-set: 1";
            this.TextBox26.Text = "����� ����";
            this.TextBox26.Top = 0.836F;
            this.TextBox26.Width = 0.9369993F;
            // 
            // label11
            // 
            this.label11.Height = 0.2F;
            this.label11.HyperLink = null;
            this.label11.Left = 0.007F;
            this.label11.Name = "label11";
            this.label11.Style = "";
            this.label11.Text = "������������ �� ���������";
            this.label11.Top = 0.2F;
            this.label11.Width = 2.125F;
            // 
            // label12
            // 
            this.label12.Height = 0.2F;
            this.label12.HyperLink = null;
            this.label12.Left = 0.007F;
            this.label12.Name = "label12";
            this.label12.Style = "font-style: italic; ddo-char-set: 1";
            this.label12.Text = " �Besprechungsprotokoll GP�";
            this.label12.Top = 0.4F;
            this.label12.Width = 2.365F;
            // 
            // label13
            // 
            this.label13.Height = 0.2F;
            this.label13.HyperLink = null;
            this.label13.Left = 4.656613E-10F;
            this.label13.Name = "label13";
            this.label13.Style = "";
            this.label13.Text = "��������� �� ���������� � �ϓ";
            this.label13.Top = 0.5999999F;
            this.label13.Width = 2.125F;
            // 
            // label14
            // 
            this.label14.Height = 0.2F;
            this.label14.HyperLink = null;
            this.label14.Left = 4.5F;
            this.label14.Name = "label14";
            this.label14.Style = "font-style: italic; ddo-char-set: 1";
            this.label14.Text = "Version:";
            this.label14.Top = 0.2000001F;
            this.label14.Width = 0.802F;
            // 
            // label15
            // 
            this.label15.Height = 0.2F;
            this.label15.HyperLink = null;
            this.label15.Left = 4.656613E-10F;
            this.label15.Name = "label15";
            this.label15.Style = "font-style: italic; ddo-char-set: 1";
            this.label15.Text = "Dokumentbezeichnung:";
            this.label15.Top = 2.980232E-08F;
            this.label15.Width = 2.125F;
            // 
            // label16
            // 
            this.label16.Height = 0.2F;
            this.label16.HyperLink = null;
            this.label16.Left = 6.032F;
            this.label16.Name = "label16";
            this.label16.Style = "font-style: italic; ddo-char-set: 1";
            this.label16.Text = "Dokumentnummer:";
            this.label16.Top = 2.980232E-08F;
            this.label16.Width = 1.281F;
            // 
            // label17
            // 
            this.label17.Height = 0.2F;
            this.label17.HyperLink = null;
            this.label17.Left = 4.5F;
            this.label17.Name = "label17";
            this.label17.Style = "";
            this.label17.Text = "������:";
            this.label17.Top = 2.980232E-08F;
            this.label17.Width = 0.802F;
            // 
            // label18
            // 
            this.label18.Height = 0.2F;
            this.label18.HyperLink = null;
            this.label18.Left = 4.5F;
            this.label18.Name = "label18";
            this.label18.Style = "";
            this.label18.Text = "V160620";
            this.label18.Top = 0.4669998F;
            this.label18.Width = 0.6350002F;
            // 
            // label19
            // 
            this.label19.Height = 0.2F;
            this.label19.HyperLink = null;
            this.label19.Left = 6.032F;
            this.label19.Name = "label19";
            this.label19.Style = "";
            this.label19.Text = "����� �� ���������";
            this.label19.Top = 0.2000001F;
            this.label19.Width = 1.59F;
            // 
            // label20
            // 
            this.label20.Height = 0.2F;
            this.label20.HyperLink = null;
            this.label20.Left = 6.032F;
            this.label20.Name = "label20";
            this.label20.Style = "";
            this.label20.Text = "BG-B-03.4";
            this.label20.Top = 0.4669998F;
            this.label20.Width = 1.418F;
            // 
            // line2
            // 
            this.line2.Height = 0F;
            this.line2.Left = 4.656613E-10F;
            this.line2.LineWeight = 1F;
            this.line2.Name = "line2";
            this.line2.Top = 0.4F;
            this.line2.Width = 8.062F;
            this.line2.X1 = 4.656613E-10F;
            this.line2.X2 = 8.062F;
            this.line2.Y1 = 0.4F;
            this.line2.Y2 = 0.4F;
            // 
            // line8
            // 
            this.line8.Height = 0.7791666F;
            this.line8.Left = 4.427F;
            this.line8.LineWeight = 1F;
            this.line8.Name = "line8";
            this.line8.Top = 2.980232E-08F;
            this.line8.Width = 0.0007500648F;
            this.line8.X1 = 4.427F;
            this.line8.X2 = 4.42775F;
            this.line8.Y1 = 2.980232E-08F;
            this.line8.Y2 = 0.7791666F;
            // 
            // line9
            // 
            this.line9.Height = 0.7791666F;
            this.line9.Left = 5.992F;
            this.line9.LineWeight = 1F;
            this.line9.Name = "line9";
            this.line9.Top = 2.980232E-08F;
            this.line9.Width = 0.0007500648F;
            this.line9.X1 = 5.992F;
            this.line9.X2 = 5.99275F;
            this.line9.Y1 = 2.980232E-08F;
            this.line9.Y2 = 0.7791666F;
            // 
            // Detail
            // 
            this.Detail.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
            this.picUndone,
            this.picDone,
            this.srTask,
            this.txtStop,
            this.srHotIssue,
            this.txtName,
            this.txtDiscription,
            this.txtComment,
            this.txtAccToName,
            this.txtAssignedToType,
            this.txtAssignedTo,
            this.txtHotIssueMainID,
            this.txtNumber,
            this.txtStatus,
            this.txtStatusID,
            this.txt,
            this.txtStatusDate,
            this.txtPosition,
            this.txtDeadline,
            this.txtStopped});
            this.Detail.Height = 0.7708334F;
            this.Detail.KeepTogether = true;
            this.Detail.Name = "Detail";
            this.Detail.Format += new System.EventHandler(this.Detail_Format);
            // 
            // picUndone
            // 
            this.picUndone.Height = 0.2F;
            this.picUndone.ImageData = ((System.IO.Stream)(resources.GetObject("picUndone.ImageData")));
            this.picUndone.Left = 6.8125F;
            this.picUndone.LineColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.picUndone.Name = "picUndone";
            this.picUndone.SizeMode = GrapeCity.ActiveReports.SectionReportModel.SizeModes.Zoom;
            this.picUndone.Top = 0.01F;
            this.picUndone.Width = 0.2F;
            // 
            // picDone
            // 
            this.picDone.Height = 0.2F;
            this.picDone.ImageData = ((System.IO.Stream)(resources.GetObject("picDone.ImageData")));
            this.picDone.Left = 6.8125F;
            this.picDone.LineColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.picDone.Name = "picDone";
            this.picDone.SizeMode = GrapeCity.ActiveReports.SectionReportModel.SizeModes.Zoom;
            this.picDone.Top = 0.01F;
            this.picDone.Width = 0.2F;
            // 
            // srTask
            // 
            this.srTask.CloseBorder = false;
            this.srTask.Height = 0.7500001F;
            this.srTask.Left = 0F;
            this.srTask.Name = "srTask";
            this.srTask.Report = null;
            this.srTask.Top = 0F;
            this.srTask.Width = 8F;
            // 
            // txtStop
            // 
            this.txtStop.CanGrow = false;
            this.txtStop.Height = 0.5F;
            this.txtStop.Left = 4.75F;
            this.txtStop.Name = "txtStop";
            this.txtStop.Style = "color: #C00000; font-family: Verdana; font-size: 9pt; font-style: normal; font-we" +
    "ight: normal; text-align: center; ddo-char-set: 0";
            this.txtStop.Text = "������ ����� �� �������, ��������� �� ���������� �� �������� ������.";
            this.txtStop.Top = 0.21F;
            this.txtStop.Visible = false;
            this.txtStop.Width = 2.25F;
            // 
            // srHotIssue
            // 
            this.srHotIssue.CanGrow = false;
            this.srHotIssue.CanShrink = false;
            this.srHotIssue.CloseBorder = false;
            this.srHotIssue.Height = 0.687F;
            this.srHotIssue.Left = 6.96F;
            this.srHotIssue.Name = "srHotIssue";
            this.srHotIssue.Report = null;
            this.srHotIssue.Top = 0F;
            this.srHotIssue.Visible = false;
            this.srHotIssue.Width = 1F;
            // 
            // txtName
            // 
            this.txtName.CanGrow = false;
            this.txtName.DataField = "HotIssueName";
            this.txtName.Height = 0.2F;
            this.txtName.Left = 0.3125F;
            this.txtName.Name = "txtName";
            this.txtName.Style = "font-family: Verdana; font-size: 9pt; font-weight: bold; text-decoration: none; d" +
    "do-char-set: 1";
            this.txtName.Text = null;
            this.txtName.Top = 0.0004999935F;
            this.txtName.Visible = false;
            this.txtName.Width = 4.3125F;
            // 
            // txtDiscription
            // 
            this.txtDiscription.CanGrow = false;
            this.txtDiscription.DataField = "HotIssueDiscription";
            this.txtDiscription.Height = 0.25F;
            this.txtDiscription.Left = 0.062F;
            this.txtDiscription.Name = "txtDiscription";
            this.txtDiscription.Style = "font-family: Verdana; font-size: 9pt; font-weight: normal; ddo-char-set: 1";
            this.txtDiscription.Text = null;
            this.txtDiscription.Top = 0.238F;
            this.txtDiscription.Visible = false;
            this.txtDiscription.Width = 4.5625F;
            // 
            // txtComment
            // 
            this.txtComment.CanGrow = false;
            this.txtComment.DataField = "HotIssueComment";
            this.txtComment.Height = 0.1870001F;
            this.txtComment.Left = 0.062F;
            this.txtComment.Name = "txtComment";
            this.txtComment.Style = "font-family: Verdana; font-size: 9pt; font-style: italic; font-weight: normal; dd" +
    "o-char-set: 1";
            this.txtComment.Text = null;
            this.txtComment.Top = 0.5F;
            this.txtComment.Visible = false;
            this.txtComment.Width = 4.5625F;
            // 
            // txtAccToName
            // 
            this.txtAccToName.CanGrow = false;
            this.txtAccToName.Height = 0.5F;
            this.txtAccToName.Left = 4.6875F;
            this.txtAccToName.Name = "txtAccToName";
            this.txtAccToName.Style = "font-family: Verdana; font-size: 9pt; font-weight: bold; text-align: center; vert" +
    "ical-align: top; ddo-char-set: 1";
            this.txtAccToName.Text = null;
            this.txtAccToName.Top = 0.1875F;
            this.txtAccToName.Width = 2.0625F;
            // 
            // txtAssignedToType
            // 
            this.txtAssignedToType.CanGrow = false;
            this.txtAssignedToType.DataField = "AssignedToType";
            this.txtAssignedToType.Height = 0.2F;
            this.txtAssignedToType.Left = 1.687F;
            this.txtAssignedToType.Name = "txtAssignedToType";
            this.txtAssignedToType.Text = null;
            this.txtAssignedToType.Top = 0.1755F;
            this.txtAssignedToType.Visible = false;
            this.txtAssignedToType.Width = 0.25F;
            // 
            // txtAssignedTo
            // 
            this.txtAssignedTo.CanGrow = false;
            this.txtAssignedTo.DataField = "AssignedTo";
            this.txtAssignedTo.Height = 0.2F;
            this.txtAssignedTo.Left = 1.375F;
            this.txtAssignedTo.Name = "txtAssignedTo";
            this.txtAssignedTo.Text = null;
            this.txtAssignedTo.Top = 0.5100001F;
            this.txtAssignedTo.Visible = false;
            this.txtAssignedTo.Width = 0.25F;
            // 
            // txtHotIssueMainID
            // 
            this.txtHotIssueMainID.CanGrow = false;
            this.txtHotIssueMainID.DataField = "HotIssueIDMain";
            this.txtHotIssueMainID.Height = 0.2F;
            this.txtHotIssueMainID.Left = 1.0625F;
            this.txtHotIssueMainID.Name = "txtHotIssueMainID";
            this.txtHotIssueMainID.Text = null;
            this.txtHotIssueMainID.Top = 0.5100001F;
            this.txtHotIssueMainID.Visible = false;
            this.txtHotIssueMainID.Width = 0.25F;
            // 
            // txtNumber
            // 
            this.txtNumber.Height = 0.2F;
            this.txtNumber.Left = 0.0625F;
            this.txtNumber.Name = "txtNumber";
            this.txtNumber.Style = "font-family: Verdana; font-size: 9pt; font-weight: bold; ddo-char-set: 1";
            this.txtNumber.Text = null;
            this.txtNumber.Top = 0F;
            this.txtNumber.Width = 0.3125F;
            // 
            // txtStatus
            // 
            this.txtStatus.DataField = "HotIssueStatusName";
            this.txtStatus.Height = 0.2F;
            this.txtStatus.Left = 2.1245F;
            this.txtStatus.Name = "txtStatus";
            this.txtStatus.Style = "font-family: Verdana; font-size: 10pt; font-weight: bold; text-decoration: none; " +
    "ddo-char-set: 1";
            this.txtStatus.Text = null;
            this.txtStatus.Top = 0.238F;
            this.txtStatus.Visible = false;
            this.txtStatus.Width = 0.1875F;
            // 
            // txtStatusID
            // 
            this.txtStatusID.DataField = "HotIssueStatusID";
            this.txtStatusID.Height = 0.2F;
            this.txtStatusID.Left = 2.4995F;
            this.txtStatusID.Name = "txtStatusID";
            this.txtStatusID.Style = "font-family: Verdana; font-size: 10pt; font-weight: bold; text-decoration: none; " +
    "ddo-char-set: 1";
            this.txtStatusID.Text = null;
            this.txtStatusID.Top = 0.238F;
            this.txtStatusID.Visible = false;
            this.txtStatusID.Width = 0.1875F;
            // 
            // txt
            // 
            this.txt.CanGrow = false;
            this.txt.DataField = "AssignedToMore";
            this.txt.Height = 0.2F;
            this.txt.Left = 2.9375F;
            this.txt.Name = "txt";
            this.txt.Text = null;
            this.txt.Top = 0.375F;
            this.txt.Visible = false;
            this.txt.Width = 0.25F;
            // 
            // txtStatusDate
            // 
            this.txtStatusDate.CanGrow = false;
            this.txtStatusDate.Height = 0.1875F;
            this.txtStatusDate.Left = 4.6875F;
            this.txtStatusDate.Name = "txtStatusDate";
            this.txtStatusDate.Style = "font-family: Verdana; font-size: 9pt; font-weight: bold; text-align: center; vert" +
    "ical-align: top; ddo-char-set: 1";
            this.txtStatusDate.Text = "TextBox35";
            this.txtStatusDate.Top = 0F;
            this.txtStatusDate.Width = 2.0625F;
            // 
            // txtPosition
            // 
            this.txtPosition.CanGrow = false;
            this.txtPosition.DataField = "HotIssuePosition";
            this.txtPosition.Height = 0.2F;
            this.txtPosition.Left = 2.937F;
            this.txtPosition.Name = "txtPosition";
            this.txtPosition.Text = null;
            this.txtPosition.Top = 0.238F;
            this.txtPosition.Visible = false;
            this.txtPosition.Width = 0.25F;
            // 
            // txtDeadline
            // 
            this.txtDeadline.DataField = "HotIssueDeadlineString";
            this.txtDeadline.Height = 0.2F;
            this.txtDeadline.Left = 7.125F;
            this.txtDeadline.Name = "txtDeadline";
            this.txtDeadline.Style = "font-family: Verdana; font-size: 9pt; text-align: right; ddo-char-set: 1";
            this.txtDeadline.Text = null;
            this.txtDeadline.Top = 0F;
            this.txtDeadline.Width = 0.8125F;
            // 
            // txtStopped
            // 
            this.txtStopped.CanGrow = false;
            this.txtStopped.DataField = "ExecutionStoppedString";
            this.txtStopped.Height = 0.2F;
            this.txtStopped.Left = 7.3125F;
            this.txtStopped.Name = "txtStopped";
            this.txtStopped.Text = null;
            this.txtStopped.Top = 0.5F;
            this.txtStopped.Visible = false;
            this.txtStopped.Width = 0.25F;
            // 
            // PageFooter
            // 
            this.PageFooter.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
            this.txtContacts,
            this.Line5,
            this.page3,
            this.page1,
            this.page2,
            this.lblCreated,
            this.txtCreatedBy,
            this.TextBox36,
            this.TextBox37,
            this.TextBox38,
            this.TextBox39,
            this.TextBox40,
            this.TextBox41,
            this.TextBox42,
            this.TextBox43,
            this.picture1,
            this.line19});
            this.PageFooter.Height = 0.9375F;
            this.PageFooter.Name = "PageFooter";
            this.PageFooter.Format += new System.EventHandler(this.PageFooter_Format);
            // 
            // txtContacts
            // 
            this.txtContacts.Height = 0.25F;
            this.txtContacts.Left = 0F;
            this.txtContacts.Name = "txtContacts";
            this.txtContacts.Style = "font-family: Verdana; font-size: 6pt; font-weight: bold; text-align: center; ddo-" +
    "char-set: 1";
            this.txtContacts.Text = "Sofia 1700, 4A, Simeonovsko shose Blvd., tel.: +3592 8621978, +3592 8623348, fax." +
    ":+3592 8621614, e-mail: office@asa-bg.com, website: www.asa-bg.com";
            this.txtContacts.Top = 0.6875F;
            this.txtContacts.Width = 8.0625F;
            // 
            // Line5
            // 
            this.Line5.Height = 0F;
            this.Line5.Left = 0F;
            this.Line5.LineWeight = 1F;
            this.Line5.Name = "Line5";
            this.Line5.Top = 0.6319444F;
            this.Line5.Width = 8F;
            this.Line5.X1 = 0F;
            this.Line5.X2 = 8F;
            this.Line5.Y1 = 0.6319444F;
            this.Line5.Y2 = 0.6319444F;
            // 
            // page3
            // 
            this.page3.CanGrow = false;
            this.page3.Height = 0.2F;
            this.page3.Left = 0.3125F;
            this.page3.Name = "page3";
            this.page3.Style = "font-family: Verdana; font-size: 8pt; font-weight: normal; text-align: left; ddo-" +
    "char-set: 1";
            this.page3.SummaryType = GrapeCity.ActiveReports.SectionReportModel.SummaryType.PageCount;
            this.page3.Text = null;
            this.page3.Top = 0.302F;
            this.page3.Width = 0.25F;
            // 
            // page1
            // 
            this.page1.CanGrow = false;
            this.page1.Height = 0.2F;
            this.page1.Left = 0F;
            this.page1.Name = "page1";
            this.page1.Style = "font-family: Verdana; font-size: 8pt; font-weight: normal; text-align: left; ddo-" +
    "char-set: 1";
            this.page1.SummaryFunc = GrapeCity.ActiveReports.SectionReportModel.SummaryFunc.Count;
            this.page1.SummaryRunning = GrapeCity.ActiveReports.SectionReportModel.SummaryRunning.All;
            this.page1.SummaryType = GrapeCity.ActiveReports.SectionReportModel.SummaryType.PageCount;
            this.page1.Text = null;
            this.page1.Top = 0.302F;
            this.page1.Width = 0.25F;
            // 
            // page2
            // 
            this.page2.Height = 0.2F;
            this.page2.HyperLink = null;
            this.page2.Left = 0.25F;
            this.page2.Name = "page2";
            this.page2.Style = "font-family: Verdana; font-size: 8pt; ddo-char-set: 1";
            this.page2.Text = "/";
            this.page2.Top = 0.302F;
            this.page2.Width = 0.0625F;
            // 
            // lblCreated
            // 
            this.lblCreated.Height = 0.2F;
            this.lblCreated.HyperLink = null;
            this.lblCreated.Left = 0F;
            this.lblCreated.Name = "lblCreated";
            this.lblCreated.Style = "font-family: Verdana; font-size: 9pt; ddo-char-set: 1";
            this.lblCreated.Text = "��������:";
            this.lblCreated.Top = 0.1145F;
            this.lblCreated.Width = 0.75F;
            // 
            // txtCreatedBy
            // 
            this.txtCreatedBy.Height = 0.2F;
            this.txtCreatedBy.Left = 0.6875F;
            this.txtCreatedBy.Name = "txtCreatedBy";
            this.txtCreatedBy.Style = "font-family: Verdana; font-size: 9pt; font-weight: normal; text-align: center; dd" +
    "o-char-set: 1";
            this.txtCreatedBy.Text = " - ���";
            this.txtCreatedBy.Top = 0.1145F;
            this.txtCreatedBy.Width = 1.9375F;
            // 
            // TextBox36
            // 
            this.TextBox36.Height = 0.1875F;
            this.TextBox36.Left = 0.006999999F;
            this.TextBox36.Name = "TextBox36";
            this.TextBox36.Style = "font-family: Verdana; font-size: 6.5pt; font-weight: normal; text-align: justify;" +
    " text-decoration: line-through; ddo-char-set: 1";
            this.TextBox36.Text = "���������� ������";
            this.TextBox36.Top = 0.437F;
            this.TextBox36.Width = 0.9375F;
            // 
            // TextBox37
            // 
            this.TextBox37.Height = 0.1875F;
            this.TextBox37.Left = 0.937F;
            this.TextBox37.Name = "TextBox37";
            this.TextBox37.Style = "font-family: Verdana; font-size: 6.5pt; font-weight: normal; text-align: justify;" +
    " text-decoration: none; ddo-char-set: 1";
            this.TextBox37.Text = "-������ ����������";
            this.TextBox37.Top = 0.4375F;
            this.TextBox37.Width = 1.0625F;
            // 
            // TextBox38
            // 
            this.TextBox38.Height = 0.1875F;
            this.TextBox38.Left = 1.937F;
            this.TextBox38.Name = "TextBox38";
            this.TextBox38.Style = "font-family: Verdana; font-size: 6.5pt; font-style: italic; font-weight: normal; " +
    "text-align: justify; text-decoration: none; ddo-char-set: 1";
            this.TextBox38.Text = "������ � �������� �����";
            this.TextBox38.Top = 0.4375F;
            this.TextBox38.Width = 1.3125F;
            // 
            // TextBox39
            // 
            this.TextBox39.Height = 0.1875F;
            this.TextBox39.Left = 3.25F;
            this.TextBox39.Name = "TextBox39";
            this.TextBox39.Style = "font-family: Verdana; font-size: 6.5pt; font-weight: normal; text-align: justify;" +
    " text-decoration: none; ddo-char-set: 1";
            this.TextBox39.Text = "- ����������";
            this.TextBox39.Top = 0.4375F;
            this.TextBox39.Width = 0.75F;
            // 
            // TextBox40
            // 
            this.TextBox40.Height = 0.1875F;
            this.TextBox40.Left = 4F;
            this.TextBox40.Name = "TextBox40";
            this.TextBox40.Style = "font-family: Verdana; font-size: 6.5pt; font-style: normal; font-weight: normal; " +
    "text-align: justify; text-decoration: underline; ddo-char-set: 1";
            this.TextBox40.Text = "���������� ������";
            this.TextBox40.Top = 0.4375F;
            this.TextBox40.Width = 1F;
            // 
            // TextBox41
            // 
            this.TextBox41.Height = 0.1875F;
            this.TextBox41.Left = 5F;
            this.TextBox41.Name = "TextBox41";
            this.TextBox41.Style = "font-family: Verdana; font-size: 6.5pt; font-weight: normal; text-align: justify;" +
    " text-decoration: none; ddo-char-set: 1";
            this.TextBox41.Text = "- �������� ������;";
            this.TextBox41.Top = 0.4375F;
            this.TextBox41.Width = 1.375F;
            // 
            // TextBox42
            // 
            this.TextBox42.Height = 0.1875F;
            this.TextBox42.Left = 6.062F;
            this.TextBox42.Name = "TextBox42";
            this.TextBox42.Style = "font-family: Verdana; font-size: 6.5pt; font-style: normal; font-weight: bold; te" +
    "xt-align: justify; text-decoration: none; ddo-char-set: 1";
            this.TextBox42.Text = "��������� ������";
            this.TextBox42.Top = 0.4375F;
            this.TextBox42.Width = 1F;
            // 
            // TextBox43
            // 
            this.TextBox43.Height = 0.1875F;
            this.TextBox43.Left = 7.0625F;
            this.TextBox43.Name = "TextBox43";
            this.TextBox43.Style = "font-family: Verdana; font-size: 6.5pt; font-weight: normal; text-align: justify;" +
    " text-decoration: none; ddo-char-set: 1";
            this.TextBox43.Text = "-�������� ������";
            this.TextBox43.Top = 0.4375F;
            this.TextBox43.Width = 0.9375F;
            // 
            // picture1
            // 
            this.picture1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.picture1.Height = 0.314F;
            this.picture1.HyperLink = null;
            this.picture1.ImageData = ((System.IO.Stream)(resources.GetObject("picture1.ImageData")));
            this.picture1.Left = 5.048F;
            this.picture1.LineColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.picture1.Name = "picture1";
            this.picture1.PictureAlignment = GrapeCity.ActiveReports.SectionReportModel.PictureAlignment.BottomRight;
            this.picture1.SizeMode = GrapeCity.ActiveReports.SectionReportModel.SizeModes.Zoom;
            this.picture1.Top = 0F;
            this.picture1.Width = 2.952756F;
            // 
            // line19
            // 
            this.line19.Height = 0.0009999275F;
            this.line19.Left = 0F;
            this.line19.LineWeight = 1F;
            this.line19.Name = "line19";
            this.line19.Top = 0.062F;
            this.line19.Width = 8.062F;
            this.line19.X1 = 0F;
            this.line19.X2 = 8.062F;
            this.line19.Y1 = 0.062F;
            this.line19.Y2 = 0.06299993F;
            // 
            // ReportFooter
            // 
            this.ReportFooter.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
            this.label21,
            this.line10,
            this.line18,
            this.pageBreak2,
            this.txtDocuments});
            this.ReportFooter.Height = 5.989583F;
            this.ReportFooter.Name = "ReportFooter";
            // 
            // label21
            // 
            this.label21.Height = 0.375F;
            this.label21.HyperLink = null;
            this.label21.Left = 0.012F;
            this.label21.Name = "label21";
            this.label21.Style = "font-size: 11pt; font-style: normal; font-weight: bold; ddo-char-set: 1";
            this.label21.Text = "��������� �� ����������:";
            this.label21.Top = 0.25F;
            this.label21.Width = 1.312F;
            // 
            // line10
            // 
            this.line10.Height = 0F;
            this.line10.Left = 1.375F;
            this.line10.LineWeight = 1F;
            this.line10.Name = "line10";
            this.line10.Top = 0.125F;
            this.line10.Width = 6.5F;
            this.line10.X1 = 1.375F;
            this.line10.X2 = 7.875F;
            this.line10.Y1 = 0.125F;
            this.line10.Y2 = 0.125F;
            // 
            // line18
            // 
            this.line18.Height = 0F;
            this.line18.Left = 1.375F;
            this.line18.LineWeight = 1F;
            this.line18.Name = "line18";
            this.line18.Top = 5.75F;
            this.line18.Width = 6.5F;
            this.line18.X1 = 1.375F;
            this.line18.X2 = 7.875F;
            this.line18.Y1 = 5.75F;
            this.line18.Y2 = 5.75F;
            // 
            // pageBreak2
            // 
            this.pageBreak2.Height = 0.01F;
            this.pageBreak2.Left = 0F;
            this.pageBreak2.Name = "pageBreak2";
            this.pageBreak2.Size = new System.Drawing.SizeF(6.5F, 0.01F);
            this.pageBreak2.Top = 0F;
            this.pageBreak2.Width = 6.5F;
            // 
            // txtDocuments
            // 
            this.txtDocuments.Height = 5.375F;
            this.txtDocuments.Left = 1.375F;
            this.txtDocuments.LineSpacing = 2F;
            this.txtDocuments.Name = "txtDocuments";
            this.txtDocuments.Style = "font-family: Versdana; font-size: 11pt; font-weight: bold; text-align: left; ddo-" +
    "char-set: 1";
            this.txtDocuments.Top = 0.25F;
            this.txtDocuments.Width = 6.5F;
            // 
            // GroupHeader1
            // 
            this.GroupHeader1.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
            this.Line23,
            this.txtCategoryName,
            this.lineCat});
            this.GroupHeader1.DataField = "HotIssueCategoryName";
            this.GroupHeader1.Height = 0.1979167F;
            this.GroupHeader1.Name = "GroupHeader1";
            this.GroupHeader1.Format += new System.EventHandler(this.GroupHeader1_Format);
            // 
            // Line23
            // 
            this.Line23.AnchorBottom = true;
            this.Line23.Height = 0.271F;
            this.Line23.Left = 0F;
            this.Line23.LineWeight = 1F;
            this.Line23.Name = "Line23";
            this.Line23.Top = 0F;
            this.Line23.Width = 0F;
            this.Line23.X1 = 0F;
            this.Line23.X2 = 0F;
            this.Line23.Y1 = 0F;
            this.Line23.Y2 = 0.271F;
            // 
            // txtCategoryName
            // 
            this.txtCategoryName.CanGrow = false;
            this.txtCategoryName.DataField = "HotIssueCategoryName";
            this.txtCategoryName.Height = 0.2F;
            this.txtCategoryName.Left = 0.012F;
            this.txtCategoryName.Name = "txtCategoryName";
            this.txtCategoryName.Style = "text-transform: uppercase; font-family: Verdana; font-size: 9.75pt; font-weight: " +
    "bold; text-decoration: none; ddo-char-set: 204";
            this.txtCategoryName.Text = null;
            this.txtCategoryName.Top = 0F;
            this.txtCategoryName.Width = 4.5F;
            // 
            // lineCat
            // 
            this.lineCat.Height = 0F;
            this.lineCat.Left = 0F;
            this.lineCat.LineWeight = 1F;
            this.lineCat.Name = "lineCat";
            this.lineCat.Top = 0.2708334F;
            this.lineCat.Width = 8F;
            this.lineCat.X1 = 0F;
            this.lineCat.X2 = 8F;
            this.lineCat.Y1 = 0.2708334F;
            this.lineCat.Y2 = 0.2708334F;
            // 
            // GroupFooter1
            // 
            this.GroupFooter1.Height = 0F;
            this.GroupFooter1.Name = "GroupFooter1";
            // 
            // ReportMeetingHotIssuesAll
            // 
            this.MasterReport = false;
            this.PageSettings.PaperHeight = 11F;
            this.PageSettings.PaperWidth = 8.5F;
            this.PrintWidth = 8.062F;
            this.Sections.Add(this.ReportHeader);
            this.Sections.Add(this.PageHeader);
            this.Sections.Add(this.GroupHeader1);
            this.Sections.Add(this.Detail);
            this.Sections.Add(this.GroupFooter1);
            this.Sections.Add(this.PageFooter);
            this.Sections.Add(this.ReportFooter);
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule(resources.GetString("$this.StyleSheet"), "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: inherit; font-style: inherit; font-variant: inherit; font-weight: bo" +
            "ld; font-size: 16pt; font-size-adjust: inherit; font-stretch: inherit", "Heading1", "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: Times New Roman; font-style: italic; font-variant: inherit; font-wei" +
            "ght: bold; font-size: 14pt; font-size-adjust: inherit; font-stretch: inherit", "Heading2", "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: inherit; font-style: inherit; font-variant: inherit; font-weight: bo" +
            "ld; font-size: 13pt; font-size-adjust: inherit; font-stretch: inherit", "Heading3", "Normal"));
            ((System.ComponentModel.ISupportInitialize)(this.TextBox20)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDate1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtMeetingNumber1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picture2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbTema)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TextBox25)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TextBox26)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label11)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label12)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label13)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label14)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label15)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label16)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label17)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label18)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label19)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label20)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picUndone)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picDone)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtStop)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDiscription)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtComment)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAccToName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAssignedToType)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAssignedTo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtHotIssueMainID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtNumber)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtStatus)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtStatusID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtStatusDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPosition)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDeadline)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtStopped)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtContacts)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.page3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.page1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.page2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblCreated)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCreatedBy)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TextBox36)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TextBox37)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TextBox38)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TextBox39)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TextBox40)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TextBox41)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TextBox42)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TextBox43)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picture1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label21)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDocuments)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCategoryName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();

        }

		#endregion

//		private void SubAnalysisRpt_ReportEnd(object sender, EventArgs e)
//		{
//			//if (!this._forPDF) return;
//
//			Logo logo = new Logo();
//			logo.Run();
//
//			for (int i=0; i<this.Document.Pages.Count; i++)
//				this.Document.Pages[i].Overlay(logo.Document.Pages[0]);
		//		}

        private void PageFooter_Format(object sender, EventArgs e)
        {
            //			if(this.PageNumber==1)
            //			{
            //				page1.Visible= page2.Visible=page3.Visible=false;
            //			}
            //			else
            //			{
            //				page1.Visible= page2.Visible=page3.Visible=true;
            //			}
            bool bLastpage = PageNumber == _countAll;
            this.txtDocuments.Text = _DocumentData[2]; // Manual document list DBUGG

            lblCreated.Visible = bLastpage;
            txtCreatedBy.Visible = bLastpage;

            if (bLastpage) {
                this.TextBox25.Visible = false;
                this.TextBox26.Visible = false;
            }

            //txtDateNow.Visible=bLastpage;
            //lbDate.Visible=bLastpage;

        }

		
	}
}
