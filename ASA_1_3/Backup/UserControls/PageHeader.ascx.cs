using System;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.UI.HtmlControls;
using Asa.Timesheet.Data;
namespace Asa.Timesheet.WebPages.UserControls
{
	
	/// <summary>
	///		Summary description for Header.
	/// </summary>
	public class PageHeader : System.Web.UI.UserControl
	{
		protected System.Web.UI.WebControls.Label lblTitle;
		protected System.Web.UI.WebControls.LinkButton lbLogOut;
		protected System.Web.UI.WebControls.Button btnLogOut;
		protected System.Web.UI.WebControls.HyperLink HyperLink1;
		protected System.Web.UI.WebControls.HyperLink HyperLink2;
		protected System.Web.UI.WebControls.Label lblUser;
        protected System.Web.UI.WebControls.Menu nav;
		public delegate void EditHandler(object sender, System.EventArgs e);
		public event EditHandler EditClicked;
		//protected Telerik.WebControls.RadMenu menuNav;

        protected virtual void FireEditClicked(object sender, System.EventArgs e)
        {
            if (EditClicked != null)
            {
                EditClicked(this, e);
            }

        }
        private void Page_Load(object sender, System.EventArgs e)
        {
            if (HttpContext.Current.User.Identity.Name != null && HttpContext.Current.User.Identity.Name != "")
            {
                nav.Visible = true;
                //if(this.Request.RawUrl.IndexOf("/reports/")!=-1)
                //CreateMenuReports();
                //else ;
                //CreateMenu();
            }
            else
                nav.Visible = false;

        }
        /*
		public void CreateMenu()
		{
			MenuGroup topGroup = new MenuGroup();
			topGroup.Flow = PresentationDirection.Horizontal;			
			menuNav.RootGroup = topGroup;
			
			// Default page (home)
			MenuItem mi =GetMenuItem(Resource.ResourceManager["menuItem_Projects"], "Projects.aspx");
			mi.CssClass ="MainItemProject";
			topGroup.AddItem(mi);
			topGroup.AddItem(GetMenuItem(Resource.ResourceManager["menuItem_Clients"], "Clients.aspx"+"?id="+((int)ClientTypes.Client).ToString()));
			topGroup.AddItem(GetMenuItem(Resource.ResourceManager["menuItem_Subcontracters"], "Subcontracters.aspx"));
			topGroup.AddItem(GetMenuItem(Resource.ResourceManager["menuItem_Users"], "Users.aspx"));

			topGroup.AddItem(GetMenuItem(Resource.ResourceManager["menuItem_Manager"], "Clients.aspx"+"?id="+((int)ClientTypes.ProjectManagers).ToString()));
			topGroup.AddItem(GetMenuItem(Resource.ResourceManager["menuItem_Builders"], "Clients.aspx"+"?id="+((int)ClientTypes.Builder).ToString()));
			topGroup.AddItem(GetMenuItem(Resource.ResourceManager["menuItem_Nadzor"], "Clients.aspx"+"?id="+((int)ClientTypes.Supervisors).ToString()));
			topGroup.AddItem(GetMenuItem(Resource.ResourceManager["menuItem_Brokers"], "Clients.aspx"+"?id="+((int)ClientTypes.Brokers).ToString()));
			topGroup.AddItem(GetMenuItem(Resource.ResourceManager["menuItem_Contacts"], "Clients.aspx"+"?id="+((int)ClientTypes.All).ToString()));
			
		
			
		}
		public void CreateMenuReports()
		{
			MenuGroup topGroup = new MenuGroup();
			topGroup.Flow = PresentationDirection.Horizontal;			
			menuNav.RootGroup = topGroup;
			
			// Default page (home)
			topGroup.AddItem(GetMenuItem(Resource.ResourceManager["menuItem_Projects"], "../Projects.aspx"));

			topGroup.AddItem(GetMenuItem(Resource.ResourceManager["menuItem_Clients"], "../Clients.aspx"+"?id="+((int)ClientTypes.Client).ToString()));
			topGroup.AddItem(GetMenuItem(Resource.ResourceManager["menuItem_Subcontracters"], "../Subcontracters.aspx"));
			topGroup.AddItem(GetMenuItem(Resource.ResourceManager["menuItem_Users"], "../Users.aspx"));
			topGroup.AddItem(GetMenuItem(Resource.ResourceManager["menuItem_Manager"], "../Clients.aspx"+"?id="+((int)ClientTypes.ProjectManagers).ToString()));

			topGroup.AddItem(GetMenuItem(Resource.ResourceManager["menuItem_Builders"], "../Clients.aspx"+"?id="+((int)ClientTypes.Builder).ToString()));
			topGroup.AddItem(GetMenuItem(Resource.ResourceManager["menuItem_Nadzor"], "../Clients.aspx"+"?id="+((int)ClientTypes.Supervisors).ToString()));
			topGroup.AddItem(GetMenuItem(Resource.ResourceManager["menuItem_Brokers"], "../Clients.aspx"+"?id="+((int)ClientTypes.Brokers).ToString()));
		
			topGroup.AddItem(GetMenuItem(Resource.ResourceManager["menuItem_Contacts"], "../Clients.aspx"+"?id="+((int)ClientTypes.All).ToString()));

		}*/
		#region Web Form Designer generated code
        override protected void OnInit(EventArgs e)
        {
            //
            // CODEGEN: This call is required by the ASP.NET Web Form Designer.
            //
            InitializeComponent();
            base.OnInit(e);
        }
		
		/// <summary>
		///		Required method for Designer support - do not modify
		///		the contents of this method with the code editor.
		/// </summary>
        private void InitializeComponent()
        {
            this.btnLogOut.Click += new System.EventHandler(this.btnLogOut_Click);
            this.lbLogOut.Click += new System.EventHandler(this.lbLogOut_Click);
            this.Load += new System.EventHandler(this.Page_Load);

        }
		#endregion
        /*private MenuItem GetMenuItem(string text)
        {
            return GetMenuItem(text, string.Empty);
        }

        private MenuItem GetMenuItem(string text, string href)
        {
            MenuItem item = new MenuItem();
            item.CssClass = "MainItem";
            item.CssClassOver = "MainItemOver";
            item.Text = text;
            item.Href = href;

            return item;
        }*/
        private void lbLogOut_Click(object sender, System.EventArgs e)
        {
            System.Web.Security.FormsAuthentication.SignOut();
            Session.Abandon();
            Response.Redirect(Request.ApplicationPath + "/Login.aspx");
        }

        private void btnLogOut_Click(object sender, System.EventArgs e)
        {
            System.Web.Security.FormsAuthentication.SignOut();
            Session.Abandon();
            Response.Redirect(Request.ApplicationPath);//+"/hours.aspx");//"/Login.aspx");
        }

        private void btnEdit_Click(object sender, System.EventArgs e)
        {
            //			btnEdit.Visible=!btnEdit.Visible;
            //			FireEditClicked(this,e);
        }
	
//		public bool EditMode
//		{
//			set { btnEdit.Visible = !value; }
//			get {return !btnEdit.Visible;}
//
//		}
		public string PageTitle
		{
			set { lblTitle.Text = value; }
		}

		public string UserName
		{
			set { lblUser.Text = value; }
		}
	}
}
