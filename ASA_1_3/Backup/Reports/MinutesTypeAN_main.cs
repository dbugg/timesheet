using System;
using Asa.Timesheet.Data;
using System.Data;
using GrapeCity.ActiveReports;
using GrapeCity.ActiveReports.Controls;
using GrapeCity.ActiveReports.SectionReportModel;
using GrapeCity.ActiveReports.Document.Section;
using GrapeCity.ActiveReports.Document;

namespace Asa.Timesheet.WebPages.Reports
{
    public class MinutesTypeAN_main : GrapeCity.ActiveReports.SectionReport
	{
		//created by Ivailo date: 05.12.2007
		//notes:Report for Minutes.aspx, type AN

		//notes: _pdf - if the report is in pdf format
		private bool _pdf = false;

        public MinutesTypeAN_main(string projectName, string clientName, string userName, bool pdf, DataView dvSub1, DataView dvSub2, string Total, string CreatedBy)
        {
            InitializeComponent();
            //notes:set logo to all pages
            this.ReportEnd += new EventHandler(MinutesTypeAN_main_ReportEnd);

            this.txtProject.Text = projectName;
            this.txtClient.Text = clientName;
            this.txtUser.Text = userName;
            this.txtProektant.Text = Resource.ResourceManager["reports_GlavenProektant"];
            _pdf = pdf;
            //notes:bind subreports data
            //check if the views has records, 
            //if not hide them
            if (dvSub1.Table.Rows.Count != 0)
                this.SubReport1.Report = new MinutesTypeAN_SUB1(dvSub1);
            else
                this.SubReport1.Visible = false;
            this.txtTotal.Text = Total;
            if (dvSub2.Table.Rows.Count != 0)
                this.SubReport2.Report = new MinutesTypeAN_SUB2(dvSub2);
            else
                this.SubReport2.Visible = false;
            this.txtDate.Text = DateTime.Now.ToShortDateString();
            this.txtCreatedBy.Text = CreatedBy;
            //notes:if there is no record at both views show message no data find
            if (!this.SubReport1.Visible && !this.SubReport2.Visible)
                txtNoRecordFind.Visible = true;
        }

        private void PageFooter_Format(object sender, System.EventArgs eArgs)
        {
            if (!this._pdf)
            {
                PageFooter.Visible = false;
            }
            else
            {
                PageFooter.Visible = true;

                int CurrentPages = this.PageNumber;
                //	this.txtPages.Text = CurrentPages.ToString();
            }
        }

		#region ActiveReports Designer generated code





















        public void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MinutesTypeAN_main));
            this.Detail = new GrapeCity.ActiveReports.SectionReportModel.Detail();
            this.ReportHeader = new GrapeCity.ActiveReports.SectionReportModel.ReportHeader();
            this.ReportFooter = new GrapeCity.ActiveReports.SectionReportModel.ReportFooter();
            this.PageHeader = new GrapeCity.ActiveReports.SectionReportModel.PageHeader();
            this.PageFooter = new GrapeCity.ActiveReports.SectionReportModel.PageFooter();
            this.lbTitle = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lbProject = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lbClient = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lbProektant = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lbUser = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.txtProject = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtClient = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtProektant = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtUser = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtDate = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.SubReport1 = new GrapeCity.ActiveReports.SectionReportModel.SubReport();
            this.SubReport2 = new GrapeCity.ActiveReports.SectionReportModel.SubReport();
            this.txtTotal = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtNoRecordFind = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.Label7 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.txtCreatedBy = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            ((System.ComponentModel.ISupportInitialize)(this.lbTitle)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbProject)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbClient)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbProektant)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbUser)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtProject)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtClient)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtProektant)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtUser)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTotal)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtNoRecordFind)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCreatedBy)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            // 
            // Detail
            // 
            this.Detail.ColumnSpacing = 0F;
            this.Detail.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
						this.SubReport1,
						this.SubReport2,
						this.txtTotal,
						this.txtNoRecordFind});
            this.Detail.Height = 1.427083F;
            this.Detail.Name = "Detail";
            // 
            // ReportHeader
            // 
            this.ReportHeader.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
						this.lbTitle,
						this.lbProject,
						this.lbClient,
						this.lbProektant,
						this.lbUser,
						this.txtProject,
						this.txtClient,
						this.txtProektant,
						this.txtUser,
						this.txtDate});
            this.ReportHeader.Height = 1.884722F;
            this.ReportHeader.Name = "ReportHeader";
            // 
            // ReportFooter
            // 
            this.ReportFooter.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
						this.Label7,
						this.txtCreatedBy});
            this.ReportFooter.Height = 0.7916667F;
            this.ReportFooter.KeepTogether = true;
            this.ReportFooter.Name = "ReportFooter";
            // 
            // PageHeader
            // 
            this.PageHeader.Height = 0F;
            this.PageHeader.Name = "PageHeader";
            // 
            // PageFooter
            // 
            this.PageFooter.Height = 0.01041667F;
            this.PageFooter.Name = "PageFooter";
            // 
            // lbTitle
            // 
            this.lbTitle.Height = 0.2F;
            this.lbTitle.HyperLink = null;
            this.lbTitle.Left = 0.6875F;
            this.lbTitle.Name = "lbTitle";
            this.lbTitle.Style = "font-size: 13pt; font-weight: bold; text-align: center";
            this.lbTitle.Text = "??????? ???????? ??????";
            this.lbTitle.Top = 0.4375F;
            this.lbTitle.Width = 9F;
            // 
            // lbProject
            // 
            this.lbProject.Height = 0.2F;
            this.lbProject.HyperLink = null;
            this.lbProject.Left = 0.6875F;
            this.lbProject.Name = "lbProject";
            this.lbProject.Style = "";
            this.lbProject.Text = "?????:";
            this.lbProject.Top = 0.8125F;
            this.lbProject.Width = 2F;
            // 
            // lbClient
            // 
            this.lbClient.Height = 0.2F;
            this.lbClient.HyperLink = null;
            this.lbClient.Left = 0.6875F;
            this.lbClient.Name = "lbClient";
            this.lbClient.Style = "";
            this.lbClient.Text = "??????????:";
            this.lbClient.Top = 1.0625F;
            this.lbClient.Width = 2F;
            // 
            // lbProektant
            // 
            this.lbProektant.Height = 0.2F;
            this.lbProektant.HyperLink = null;
            this.lbProektant.Left = 0.6875F;
            this.lbProektant.Name = "lbProektant";
            this.lbProektant.Style = "";
            this.lbProektant.Text = "?????? ?????????:";
            this.lbProektant.Top = 1.3125F;
            this.lbProektant.Width = 2F;
            // 
            // lbUser
            // 
            this.lbUser.Height = 0.2F;
            this.lbUser.HyperLink = null;
            this.lbUser.Left = 0.6875F;
            this.lbUser.Name = "lbUser";
            this.lbUser.Style = "";
            this.lbUser.Text = "????????:";
            this.lbUser.Top = 1.5625F;
            this.lbUser.Width = 2F;
            // 
            // txtProject
            // 
            this.txtProject.Height = 0.2F;
            this.txtProject.Left = 2.6875F;
            this.txtProject.Name = "txtProject";
            this.txtProject.Top = 0.8125F;
            this.txtProject.Width = 7F;
            // 
            // txtClient
            // 
            this.txtClient.Height = 0.2F;
            this.txtClient.Left = 2.6875F;
            this.txtClient.Name = "txtClient";
            this.txtClient.Top = 1.0625F;
            this.txtClient.Width = 7F;
            // 
            // txtProektant
            // 
            this.txtProektant.Height = 0.2F;
            this.txtProektant.Left = 2.6875F;
            this.txtProektant.Name = "txtProektant";
            this.txtProektant.Top = 1.3125F;
            this.txtProektant.Width = 7F;
            // 
            // txtUser
            // 
            this.txtUser.Height = 0.2F;
            this.txtUser.Left = 2.6875F;
            this.txtUser.Name = "txtUser";
            this.txtUser.Top = 1.5625F;
            this.txtUser.Width = 7F;
            // 
            // txtDate
            // 
            this.txtDate.Height = 0.2F;
            this.txtDate.Left = 7.677083F;
            this.txtDate.Name = "txtDate";
            this.txtDate.Style = "text-align: right";
            this.txtDate.Text = "TextBox1";
            this.txtDate.Top = 0F;
            this.txtDate.Width = 2F;
            // 
            // SubReport1
            // 
            this.SubReport1.CloseBorder = false;
            this.SubReport1.Height = 0.4F;
            this.SubReport1.Left = 0.6875F;
            this.SubReport1.Name = "SubReport1";
            this.SubReport1.Report = null;
            this.SubReport1.Top = 0.375F;
            this.SubReport1.Width = 9F;
            // 
            // SubReport2
            // 
            this.SubReport2.CloseBorder = false;
            this.SubReport2.Height = 0.4F;
            this.SubReport2.Left = 0.6875F;
            this.SubReport2.Name = "SubReport2";
            this.SubReport2.Report = null;
            this.SubReport2.Top = 0.9375F;
            this.SubReport2.Width = 9F;
            // 
            // txtTotal
            // 
            this.txtTotal.Height = 0.2F;
            this.txtTotal.Left = 0.6875F;
            this.txtTotal.Name = "txtTotal";
            this.txtTotal.Style = "font-weight: bold";
            this.txtTotal.Top = 0.0625F;
            this.txtTotal.Width = 9F;
            // 
            // txtNoRecordFind
            // 
            this.txtNoRecordFind.Height = 0.2F;
            this.txtNoRecordFind.Left = 0.6875F;
            this.txtNoRecordFind.Name = "txtNoRecordFind";
            this.txtNoRecordFind.Text = "?? ?? ???????? ????? ????????????? ?? ???????? ??????.";
            this.txtNoRecordFind.Top = 0.375F;
            this.txtNoRecordFind.Visible = false;
            this.txtNoRecordFind.Width = 9F;
            // 
            // Label7
            // 
            this.Label7.Height = 0.2F;
            this.Label7.HyperLink = null;
            this.Label7.Left = 1F;
            this.Label7.Name = "Label7";
            this.Label7.Style = "font-size: 11pt; text-align: left";
            this.Label7.Text = "????????: ......................................";
            this.Label7.Top = 0.32F;
            this.Label7.Width = 2F;
            // 
            // txtCreatedBy
            // 
            this.txtCreatedBy.Height = 0.2F;
            this.txtCreatedBy.Left = 3F;
            this.txtCreatedBy.Name = "txtCreatedBy";
            this.txtCreatedBy.Style = "font-size: 10pt; font-weight: normal; text-align: justify";
            this.txtCreatedBy.Top = 0.5625F;
            this.txtCreatedBy.Width = 2F;
            // 
            // ActiveReport1
            // 
            this.MasterReport = false;
            this.PageSettings.PaperHeight = 11F;
            this.PageSettings.PaperWidth = 8.5F;
            this.PrintWidth = 9.697917F;
            this.Sections.Add(this.ReportHeader);
            this.Sections.Add(this.PageHeader);
            this.Sections.Add(this.Detail);
            this.Sections.Add(this.PageFooter);
            this.Sections.Add(this.ReportFooter);
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule(resources.GetString("$this.StyleSheet"), "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: inherit; font-style: inherit; font-variant: inherit; font-weight: bo" +
                        "ld; font-size: 16pt; font-size-adjust: inherit; font-stretch: inherit", "Heading1", "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: Times New Roman; font-style: italic; font-variant: inherit; font-wei" +
                        "ght: bold; font-size: 14pt; font-size-adjust: inherit; font-stretch: inherit", "Heading2", "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: inherit; font-style: inherit; font-variant: inherit; font-weight: bo" +
                        "ld; font-size: 13pt; font-size-adjust: inherit; font-stretch: inherit", "Heading3", "Normal"));
            ((System.ComponentModel.ISupportInitialize)(this.lbTitle)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbProject)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbClient)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbProektant)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbUser)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtProject)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtClient)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtProektant)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtUser)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTotal)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtNoRecordFind)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCreatedBy)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();

            // Attach Report Events
            this.PageFooter.Format += new System.EventHandler(this.PageFooter_Format);
        }

		#endregion

		//notes: check if the report is pdf and if it is add logo of the firm to all pages
        private void MinutesTypeAN_main_ReportEnd(object sender, EventArgs e)
        {
            if (!this._pdf) return;

            Logo1 logo = new Logo1();
            logo.Run();

            for (int i = 0; i < this.Document.Pages.Count; i++)
                this.Document.Pages[i].Overlay(logo.Document.Pages[0]);
        }

        private ReportHeader ReportHeader;
        private Label lbTitle;
        private Label lbProject;
        private Label lbClient;
        private Label lbProektant;
        private Label lbUser;
        private TextBox txtProject;
        private TextBox txtClient;
        private TextBox txtProektant;
        private TextBox txtUser;
        private TextBox txtDate;
        private PageHeader PageHeader;
        private Detail Detail;
        private SubReport SubReport1;
        private SubReport SubReport2;
        private TextBox txtTotal;
        private TextBox txtNoRecordFind;
        private PageFooter PageFooter;
        private ReportFooter ReportFooter;
        private Label Label7;
        private TextBox txtCreatedBy;
	}
}
