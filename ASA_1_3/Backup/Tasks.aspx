﻿<%@ Page Title="" Language="C#" MasterPageFile="~/timesheet.Master" AutoEventWireup="true"
    CodeBehind="Tasks.aspx.cs" Inherits="Asa.Timesheet.WebPages.Tasks" %>

<%@ MasterType VirtualPath="~/timesheet.Master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style>
        .table-sm
        {
            font-size: x-small;
        }
        .table-sm td
        {
            border: none;
            white-space: pre;
        }
        .sticky
        {
            position: sticky;
            left: 0px;
            background: white;
            font-weight: 900;
            font-size: small;
        }
        .table-sm th
        {
            border: none;
            white-space: pre;
        }
        .monthName
        {
            font-size: xx-large;
            border-top: thin solid gray;
        }
        .monthName td
        {
            border: none;
            background: #fafafa;
        }
        .fa-suitcase
        {
            color: #00487c;
        }
    </style>
</asp:Content>
<asp:Content ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="container-fluid">
        <div class="row">
            <div class="col-3">
                <asp:Menu ID="NavigationMenu" StaticDisplayLevels="2" StaticSubMenuIndent="10" Orientation="Vertical"
                    runat="server">
                    <Items>
                        <asp:MenuItem Text="Home" ToolTip="Home">
                            <asp:MenuItem Text="Music" ToolTip="Music">
                                <asp:MenuItem Text="Classical" ToolTip="Classical" />
                                <asp:MenuItem Text="Rock" ToolTip="Rock" />
                                <asp:MenuItem Text="Jazz" ToolTip="Jazz" />
                            </asp:MenuItem>
                            <asp:MenuItem Text="Movies" ToolTip="Movies">
                                <asp:MenuItem Text="Action" ToolTip="Action" />
                                <asp:MenuItem Text="Drama" ToolTip="Drama" />
                                <asp:MenuItem Text="Musical" ToolTip="Musical" />
                            </asp:MenuItem>
                        </asp:MenuItem>
                    </Items>
                </asp:Menu>
            </div>
            <div class="col-9">
            <asp:LinkButton ID="ibPdfExport" runat="server" CssClass="btn btn-info"><i class="far fa-file-pdf"></i></i></asp:LinkButton>


                <asp:Label runat="server" ID="Label9" CssClass="enterDataLabel inv">№ Протокол</asp:Label>
                <asp:TextBox runat="server" ID="txtID" CssClass="EnterDataBox"></asp:TextBox>
                <asp:Label runat="server" ID="Label10" CssClass="enterDataLabel inv">Дата</asp:Label>
                <asp:TextBox runat="server" ID="txtDate" CssClass="EnterDataBox"></asp:TextBox>
                <asp:Label runat="server" ID="lblDocs" CssClass="enterDataLabel inv">Документи:</asp:Label>
                <asp:TextBox TextMode="MultiLine" Width="100%" Height="80px" runat="server" ID="txtDocuments"
                    Rows="10" Columns="80" CssClass="EnterDataBox"></asp:TextBox>
                <asp:Label ID="Label2" runat="server" CssClass="enterDataLabel inv">Проект:</asp:Label>
                <asp:DropDownList ID="ddlProject" runat="server" CssClass="EnterDataBox" AutoPostBack="True">
                </asp:DropDownList>
                <asp:Label ID="Label1" runat="server" CssClass="enterDataLabel inv">Проект:</asp:Label>
                <asp:DropDownList ID="Dropdownlist1" runat="server" CssClass="EnterDataBox" AutoPostBack="True">
                </asp:DropDownList>
                <asp:Label ID="lbClient" runat="server" CssClass="enterDataLabel inv">Клиенти:</asp:Label>
                <asp:CheckBoxList ID="plcClients" runat="server">
                </asp:CheckBoxList>
                <asp:Label ID="lbTeam" runat="server" CssClass="enterDataLabel inv">Екип:</asp:Label>
                <asp:CheckBoxList ID="plcTeam" runat="server">
                </asp:CheckBoxList>
                <asp:Label ID="lbBuilder" runat="server" CssClass="enterDataLabel inv">Строители и др.:</asp:Label>
                <asp:CheckBoxList ID="plcBuilders" runat="server">
                </asp:CheckBoxList>
                <asp:Label ID="lbSubs" runat="server" CssClass="enterDataLabel inv">Подизпълнители:</asp:Label>
                <asp:Label ID="lbSubsData" runat="server" CssClass="EnterDataBox" EnableViewState="False"></asp:Label>
                <div>
                    <asp:Label ID="lblresult" runat="server"></asp:Label>
                </div>
                <asp:TextBox ID="txtNameNew" runat="server" Width="140" />
                <asp:TextBox ID="txtDeadlineNew" runat="server" Width="140" />
                <asp:DropDownList ID="ddlStatusNew" runat="server" Width="140" ></asp:DropDownList>
                <asp:DropDownList ID="ddlAssignedToNew" runat="server" Width="140" ></asp:DropDownList>

                <asp:GridView ID="GridView1" runat="server" CssClass="table" Width="100%" AutoGenerateColumns="False"
                    CellPadding="4" PageSize="2" AllowSorting="True" OnPageIndexChanging="GridView1_PageIndexChanging"
                    OnRowCancelingEdit="GridView1_RowCancelingEdit" OnRowDeleting="GridView1_RowDeleting"
                    OnRowEditing="GridView1_RowEditing" OnRowUpdating="GridView1_RowUpdating" OnRowDataBound="GridView1_RowDataBound">
                    <HeaderStyle Wrap="False" CssClass="GridHeader"></HeaderStyle>
                    <Columns>

                        <asp:TemplateField HeaderText="SNo">
                            <ItemTemplate>
                                <span>
                                    <%#Container.DataItemIndex + 1%>
                                </span>
                                <asp:HiddenField ID="HotIssueID" Value='<%# Eval("HotIssueID") %>' runat="server" />
                            </ItemTemplate>
                        </asp:TemplateField>

                        <asp:TemplateField SortExpression="HotIssueName" HeaderText="Име" ItemStyle-ForeColor="DimGray">
                            <ItemTemplate>
                                <%# DataBinder.Eval(Container, "DataItem.HotIssueName")%>
                            </ItemTemplate>
                            <EditItemTemplate>
                                <asp:TextBox ID="HotIssueName" runat="server" Value='<%# Eval("HotIssueName") %>'></asp:TextBox>
                            </EditItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField SortExpression="HotIssueDeadline" HeaderText="Краен срок" ItemStyle-ForeColor="DimGray">
                            <ItemTemplate>
                                <%# DataBinder.Eval(Container, "DataItem.HotIssueDeadline")%>
                            </ItemTemplate>
                            <EditItemTemplate>
                                <asp:TextBox ID="txtDeadline" runat="server" Value='<%# Eval("HotIssueDeadline") %>'></asp:TextBox>
                            </EditItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Статус">
                            <ItemTemplate>
                                <%# Eval("HotIssueStatusName") %>
                            <asp:HiddenField ID="HotIssueStatusID" Value='<%# Eval("HotIssueStatusID") %>' runat="server" />
                                       </ItemTemplate>
                            <EditItemTemplate>
                                <asp:DropDownList ID="ddlStatus" runat="server"></asp:DropDownList>
                            </EditItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Отговорен">
                            <ItemTemplate>
                                <%# Eval("AssignedTo")%>
                                <asp:HiddenField ID="AssignedTo" Value='<%# Eval("AssignedTo") %>' runat="server" />
                                
                            </ItemTemplate>
                            <EditItemTemplate>
                                <asp:DropDownList ID="ddlAssigned" runat="server"></asp:DropDownList>
                            </EditItemTemplate>
                        </asp:TemplateField>
                        <asp:CommandField ShowEditButton="true" />
                        <asp:CommandField ShowDeleteButton="true" ControlStyle-CssClass="btn btn-danger btn-sm" />
                    </Columns>
                </asp:GridView>
            </div>
        </div>
    </div>
</asp:Content>
