using System;
using System.Data;
using System.Data.SqlClient; 
using System.Collections;
using Microsoft.ApplicationBlocks.Data;

namespace Asa.Timesheet.Data
{
	public class ProjectScheduleDAL
	{
		/// <summary>
		/// Loads a ProjectScheduleData object from the database using the given ProjectScheduleID
		/// </summary>
		/// <param name="ProjectScheduleID">The primary key value</param>
		/// <returns>A ProjectScheduleData object</returns>
		public static ProjectScheduleData Load(int ProjectScheduleID)
		{
			SqlParameter[] parameterValues = new SqlParameter[] { new SqlParameter("@ProjectScheduleID", ProjectScheduleID) };
			using (SqlDataReader reader = SqlHelper.ExecuteReader(CommonDAL.ConnectionString, CommandType.StoredProcedure, "ProjectScheduleSelProc", parameterValues))
			{
				if (reader.Read())
				{
					ProjectScheduleData result = new ProjectScheduleData();
					LoadFromReader(reader,result);
					return result;
				}
				else
					return null;
			}
		}

		/// <summary>
		/// Loads a ProjectScheduleData object from the database using the given where clause
		/// </summary>
		/// <param name="whereClause">The filter expression for the query</param>
		/// <returns>A ProjectScheduleData object</returns>
		public static ProjectScheduleData Load(string whereClause)
		{
			SqlParameter[] parameterValues = new SqlParameter[] { new SqlParameter("@WhereClause", whereClause) };
			using (SqlDataReader reader = SqlHelper.ExecuteReader(CommonDAL.ConnectionString, CommandType.StoredProcedure, "ProjectScheduleListProc", parameterValues))
			{
				if (reader.Read())
				{
					ProjectScheduleData result = new ProjectScheduleData();
					LoadFromReader(reader,result);
					return result;
				}
				else
					return null;
			}
		}
		public static int Save( ProjectScheduleData source)
		{
			if (source.ProjectScheduleID == -1)
				return Insert(source);
			else
				return Update(source);	
		}

		public static void Delete( ProjectScheduleData source)
		{
			Delete(source.ProjectScheduleID);
		}
		/// <summary>
		/// Loads a collection of ProjectScheduleData objects from the database.
		/// </summary>
		/// <returns>A collection containing all of the ProjectScheduleData objects in the database.</returns>
		public static ProjectSchedulesVector LoadCollection()
		{
			SqlParameter[] parms = new SqlParameter[] {};
			return LoadCollection("ProjectScheduleListProc", parms);
		}
	  
		/// <summary>
		/// Saves a collection of ProjectScheduleData objects to the database.
		/// </summary>
		/// <returns>A collection containing all of the ProjectScheduleData objects in the database.</returns>
		public static  void  SaveCollection(ProjectSchedulesVector col,string spName, SqlParameter[] parms)
		{
			ProjectSchedulesVector colOld = LoadCollection(spName,parms);
			UpdateCollection(colOld,col);
		}
		public static  void  SaveCollection(ProjectSchedulesVector col)
		{
			ProjectSchedulesVector colOld = LoadCollection();
			UpdateCollection(colOld,col);
		}
		public static  void  SaveCollection(ProjectSchedulesVector col, string whereClause )
		{
			ProjectSchedulesVector colOld = LoadCollection(whereClause);
			UpdateCollection(colOld,col);
		}
		/// <summary>
		/// Loads a collection of ProjectScheduleData objects from the database.
		/// </summary>
		/// <returns>A collection containing all of the ProjectScheduleData objects in the database.</returns>
		public static ProjectSchedulesVector LoadCollection(string whereClause)
		{
			SqlParameter[] parms = new SqlParameter[] { new SqlParameter("@WhereClause", whereClause) };
			return LoadCollection("ProjectScheduleListProc", parms);
		}

		public static ProjectSchedulesVector LoadCollectionByProject(int projectID)
		{
			SqlParameter[] parms = new SqlParameter[] { new SqlParameter("@ProjectID", projectID) };
			return LoadCollection("ProjectScheduleListProc", parms);
		}
		

		/// <summary>
		/// Loads a collection of ProjectScheduleData objects from the database.
		/// </summary>
		/// <returns>A collection containing all of the ProjectScheduleData objects in the database.</returns>
		public static ProjectSchedulesVector LoadCollection(string spName, SqlParameter[] parms)
		{
			ProjectSchedulesVector result = new ProjectSchedulesVector();
			using (SqlDataReader reader = SqlHelper.ExecuteReader(CommonDAL.ConnectionString, CommandType.StoredProcedure, spName, parms))
			{
				while (reader.Read())
				{
					ProjectScheduleData tmp = new ProjectScheduleData();
					LoadFromReader(reader, tmp);
					result.Add(tmp);
				}
			}
			return result;
		}
		/// <summary>
		/// Deletes a ProjectScheduleData object from the database.
		/// </summary>
		/// <param name="ProjectScheduleID">The primary key value</param>
		public static void Delete(int ProjectScheduleID)
		{
			SqlParameter[] parameterValues = new SqlParameter[] { new SqlParameter("@ProjectScheduleID", ProjectScheduleID) };
			SqlHelper.ExecuteNonQuery(CommonDAL.ConnectionString, CommandType.StoredProcedure, "ProjectScheduleDelProc", parameterValues);
		}

		public static void LoadFromReader(SqlDataReader reader, ProjectScheduleData target)
		{
			if (reader != null && !reader.IsClosed)
			{
				//target.Key = reader.GetInt32(0);
				if (!reader.IsDBNull(0)) target.ProjectScheduleID = reader.GetInt32(0);
				if (!reader.IsDBNull(1)) target.StartDate = reader.GetDateTime(1);
				if (!reader.IsDBNull(2)) target.EndDate = reader.GetDateTime(2);
				if (!reader.IsDBNull(3)) target.SubcontracterID = reader.GetInt32(3);
				if (!reader.IsDBNull(4)) target.IsProjectMain = reader.GetBoolean(4);
				if (!reader.IsDBNull(5)) target.ProjectID = reader.GetInt32(5);
				if (!reader.IsDBNull(6)) target.Notes = reader.GetString(6);
			}
		}
		
	  
		public static int Insert( ProjectScheduleData source)
		{
			SqlParameter[] parameterValues = GetInsertParameterValues(source);
			SqlHelper.ExecuteNonQuery(CommonDAL.ConnectionString, CommandType.StoredProcedure, "ProjectScheduleInsProc", parameterValues);
			source.ProjectScheduleID = (int) parameterValues[0].Value;
			return source.ProjectScheduleID;
		}

		public static int Update( ProjectScheduleData source)
		{
			SqlParameter[] parameterValues = GetUpdateParameterValues(source);
			SqlHelper.ExecuteNonQuery(CommonDAL.ConnectionString, CommandType.StoredProcedure,
				"ProjectScheduleUpdProc", parameterValues);
			return source.ProjectScheduleID;
		}
		private static void UpdateCollection(ProjectSchedulesVector colOld,ProjectSchedulesVector col)
		{
			// Delete all old items
			foreach( ProjectScheduleData itemOld in colOld)
			{
				if(!ExistsKey(itemOld.ProjectScheduleID, col))
					Delete(itemOld.ProjectScheduleID);
			}
			foreach( ProjectScheduleData item in col)
			{
				if(item.ProjectScheduleID <= -1)
					Insert(item);
				else
					Update(item);
			}
		}
		private static bool ExistsKey(int Key, ProjectSchedulesVector col)
		{
			foreach( ProjectScheduleData item in col)
				if(item.ProjectScheduleID==Key)
					return true;
			return false;
		}

		private static SqlParameter GetSqlParameter(string name, ParameterDirection direction, object value)
		{
			SqlParameter p = new SqlParameter(name, value);
			p.Direction = direction;
			return p;
		}

		private static SqlParameter[] GetInsertParameterValues( ProjectScheduleData source)
		{
			SqlParameter[] prams = new SqlParameter[7];
			prams[0] = GetSqlParameter("@ProjectScheduleID", ParameterDirection.Output, source.ProjectScheduleID);
			prams[1] = GetSqlParameter("@StartDate", ParameterDirection.Input, (source.StartDate!=DateTime.MinValue&&source.StartDate!=DateTime.MaxValue)? (object)source.StartDate:DBNull.Value);
			prams[2] = GetSqlParameter("@EndDate", ParameterDirection.Input, (source.EndDate!=DateTime.MinValue&&source.EndDate!=DateTime.MaxValue)? (object)source.EndDate:DBNull.Value);
			prams[3] = GetSqlParameter("@SubcontracterID", ParameterDirection.Input,(source.SubcontracterID>0)? (object)source.SubcontracterID:DBNull.Value);
			prams[4] = GetSqlParameter("@IsProjectMain", ParameterDirection.Input, source.IsProjectMain);
			prams[5] = GetSqlParameter("@ProjectID", ParameterDirection.Input, source.ProjectID);
			prams[6] = GetSqlParameter("@Notes", ParameterDirection.Input, source.Notes);
			return prams;
		}

		private static SqlParameter[] GetUpdateParameterValues( ProjectScheduleData source)
		{
			return new SqlParameter[] {
										  new SqlParameter("@ProjectScheduleID", source.ProjectScheduleID),
										  new SqlParameter("@StartDate", (source.StartDate!=DateTime.MinValue&&source.StartDate!=DateTime.MaxValue)?(object)source.StartDate:DBNull.Value),
										  new SqlParameter("@EndDate", (source.EndDate!=DateTime.MinValue&&source.EndDate!=DateTime.MaxValue)?(object)source.EndDate:DBNull.Value),
										  new SqlParameter("@SubcontracterID", (source.SubcontracterID>0)?(object)source.SubcontracterID:DBNull.Value),
										  new SqlParameter("@IsProjectMain", source.IsProjectMain),
										  new SqlParameter("@ProjectID", source.ProjectID),
										  new SqlParameter("@Notes", source.Notes)
									  };
		}
	}
}
