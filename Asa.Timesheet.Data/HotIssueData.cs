using System;
using System.Collections;
using System.Text;

namespace Asa.Timesheet.Data
{
	/// <summary>
	/// Summary description for HotIssueData.
	/// </summary>
	public class HotIssueData
	{
		private int _hotIssueID = -1;
		private int _projectID = -1;
		private int _hotIssueCategoryID = -1;
		private string _hotIssueName = string.Empty;
		private string _hotIssueDiscription = string.Empty;
		private string _hotIssueComment= string.Empty;
		private int _assignedTo = -1;
		private int _assignedToType = -1;
		private DateTime _hotIssueDeadline = DateTime.MinValue;
		//private int _hotIssuePriorityID= -1;
		private int _hotIssueStatusID= -1;
		private int _hotIssueIDMain= -1;
		private bool _isActive = true;
		private bool _toClient = false;
		private string _hotIssueCategoryName = string.Empty;
		//private string _hotIssuePriorityName = string.Empty;
		private string _hotIssueStatusName = string.Empty;
		private string _projectName = string.Empty;
		private string _createdBy = string.Empty;
		private DateTime _createdDate = DateTime.MinValue;
	    private DateTime _executionStopped;
		private bool _ASA=false;
		private int _assignedBy=-1;
		private string _assignedToMore="";
		 private DateTime _originalDate;
		private string _hotIssuePosition="";
		/// <summary>
		/// Initialize an new empty Login object.
		/// </summary>
		public HotIssueData()
		{
			
		}

		public HotIssueData( int hotIssueID, int projectID,int hotIssueCategoryID,	 string hotIssueName, string hotIssueDiscription,	 string hotIssueComment, int assignedTo, int assignedToType, DateTime hotIssueDeadline/*, int hotIssuePriorityID*/, int hotIssueStatusID, int hotIssueIDMain, bool isActive,string createdBy,DateTime createdDate,bool toClient)
		{
			this._hotIssueID = hotIssueID;
			this._projectID = projectID;
			this._hotIssueCategoryID =hotIssueCategoryID;
			this._hotIssueName = hotIssueName;
			this._hotIssueDiscription = hotIssueDiscription;
			this._hotIssueComment= hotIssueComment;
			this._assignedTo = assignedTo;
			this._assignedToType = assignedToType;
			this._hotIssueDeadline = hotIssueDeadline;
			//this._hotIssuePriorityID=hotIssuePriorityID;
			this._hotIssueStatusID= hotIssueStatusID;
			this._hotIssueIDMain= hotIssueIDMain;
			this._isActive = isActive;
			this._createdBy = createdBy;
			this._createdDate = createdDate;
			this._toClient = toClient;
		}

		//		public override string ToString()
//		{
//			StringBuilder builder = new StringBuilder();
//			builder.Append("_fileID: " + _fileID.ToString()).Append("\r\n");
//			builder.Append("_fileSubject: " + _fileSubject.ToString()).Append("\r\n");
//			builder.Append("_fileMinutes: " + _fileMinutes.ToString()).Append("\r\n");
//			builder.Append("_fileVersion: " + _fileVersion.ToString()).Append("\r\n");
//			builder.Append("_createdBy: " + _createdBy.ToString()).Append("\r\n");
//			builder.Append("_fileType: " + _fileType.ToString()).Append("\r\n");
//			builder.Append("_dateCreated: " + _dateCreated.ToString()).Append("\r\n");
//			builder.Append("_mailForDownload: " + _mailForDownload.ToString()).Append("\r\n");
//			builder.Append("_catetegoryID: " + _catetegoryID.ToString()).Append("\r\n");
//			builder.Append("_fileNameSave: " + _fileNameSave.ToString()).Append("\r\n");
//			builder.Append("_catetegoryName: " + _categoryName.ToString()).Append("\r\n");
//			builder.Append("_projectName: " + _projectName.ToString()).Append("\r\n");
//			builder.Append("_isClientVisible: " + _isClientVisible.ToString()).Append("\r\n");
//			builder.Append("_isSubcontractorVisible: " + _isSubcontractorVisible.ToString()).Append("\r\n");
//			builder.Append("_isBuilderVisible: " + _isBuilderVisible.ToString()).Append("\r\n");
//			builder.Append("\r\n");
//			return builder.ToString();
//		}	

	
		public object GetByIndex(int nIndex)
		{
			switch(nIndex)
			{	
				case 0:
					return _hotIssueID;
				case 1:
					return _projectID;
				case 2:
					return _hotIssueCategoryID;
				case 3:
					return _hotIssueName;
				case 4:
					return _hotIssueDiscription;
				case 5:
					return _hotIssueComment;
				case 6:
					return _assignedTo;
				case 7:
					return _assignedToType;
				case 8:
					return _hotIssueDeadline;
//				case 9:
//					return _hotIssuePriorityID;
				case 9:
					return _hotIssueStatusID;
				case 10:
					return _hotIssueIDMain;
				case 11:
					return _isActive;
				case 12:
					return _createdBy;
				case 13:
					return _createdDate;
				case 14:
					return _toClient;
				case 15:
					return _hotIssueCategoryName;
//				case 16:
//					return _hotIssuePriorityName;
				case 16:
					return _hotIssueStatusName;
				case 17:
					return _projectName;
			}
			return null;
		}
	
		public object GetByName(string name)
		{
			switch(name)
			{	
				case "HotIssueID":
					return _hotIssueID;
				case "ProjectID":
					return _projectID;
				case "HotIssueCategoryID":
					return _hotIssueCategoryID;
				case "HotIssueName":
					return _hotIssueName;
				case "HotIssueDiscription":
					return _hotIssueDiscription;
				case "HotIssueComment":
					return _hotIssueComment;
				case "AssignedTo":
					return _assignedTo;
				case "AssignedToType":
					return _assignedToType;
				case "HotIssueDeadline":
					return _hotIssueDeadline;
//				case "HotIssuePriorityID":
//					return _hotIssuePriorityID;
				case "HotIssueStatusID":
					return _hotIssueStatusID;
				case "HotIssueIDMain":
					return _hotIssueIDMain;
				case "IsActive":
					return _isActive;
				case "CreatedBy":
					return _createdBy;
				case "CreatedDate":
					return _createdDate;
				case "ToClient":
					return _toClient;
				case "HotIssueCategoryName":
					return _hotIssueCategoryName;
//				case "HotIssuePriorityName":
//					return _hotIssuePriorityName;
				case "HotIssueStatusName":
					return _hotIssueStatusName;
				case "ProjectName":
					return _projectName;
			}
			return null;
		}
		public string HotIssuePosition
		{
			get { return _hotIssuePosition; }
			set { _hotIssuePosition = value; }
		}
		#region Properties
		public DateTime OriginalDate
		{
			get { return _originalDate; }
			set { _originalDate = value; }
		}
		
		public DateTime ExecutionStopped
		{
			get { return _executionStopped; }
			set { _executionStopped = value; }
		}
		public bool ASA
		{
			get { return _ASA; }
			set { _ASA = value; }
		}
		public int AssignedBy
		{
			get { return _assignedBy; }
			set { _assignedBy = value; }
		}
		public int HotIssueID
		{
			get { return _hotIssueID; }
			set { _hotIssueID = value; }
		}
		public int ProjectID
		{
			get { return _projectID; }
			set { _projectID = value; }
		}
		public int HotIssueCategoryID
		{
			get { return _hotIssueCategoryID; }
			set { _hotIssueCategoryID = value; }
		}
		public string HotIssueName
		{
			get { return _hotIssueName; }
			set { _hotIssueName = value; }
		}
		public string AssignedToMore
		{
			get { return _assignedToMore; }
			set { _assignedToMore = value; }
		}
		
		public string HotIssueDiscription
		{
			get { return _hotIssueDiscription; }
			set { _hotIssueDiscription = value; }
		}
		public string HotIssueComment
		{
			get { return _hotIssueComment; }
			set { _hotIssueComment = value; }
		}
		public int AssignedTo
		{
			get { return _assignedTo; }
			set { _assignedTo = value; }
		}
		public int AssignedToType
		{
			get { return _assignedToType; }
			set { _assignedToType = value; }
		}
		public DateTime HotIssueDeadline
		{
			get { return _hotIssueDeadline; }
			set { _hotIssueDeadline = value; }
		}
		public string HotIssueDeadlineString
		{
			get { 
				if(_hotIssueDeadline==Constants.DateMin || _hotIssueDeadline==Constants.DateMax || _hotIssueDeadline==new DateTime())
					return "";
				return _hotIssueDeadline.ToString("dd.MM.yyyy");
				
				}
			set{}
			
		}
		public string ExecutionStoppedString
		{
			get 
			{
				if(_executionStopped==Constants.DateMin || _executionStopped==Constants.DateMax || _executionStopped==new DateTime())
					  return "";
				return _executionStopped.ToString("dd.MM.yyyy"); }
			set { }
		}
//		public int HotIssuePriorityID
//		{
//			get { return _hotIssuePriorityID; }
//			set { _hotIssuePriorityID = value; }
//		}
		public int HotIssueStatusID
		{
			get { return _hotIssueStatusID; }
			set { _hotIssueStatusID = value; }
		}
		public int HotIssueIDMain
		{
			get { return _hotIssueIDMain; }
			set { _hotIssueIDMain = value; }
		}
		
		public bool IsActive
		{
			get { return _isActive; }
			set { _isActive = value; }
		}
		public bool ToClient
		{
			get { return _toClient; }
			set { _toClient = value; }
		}
		public string HotIssueCategoryName
		{
			get { return _hotIssueCategoryName; }
			set { _hotIssueCategoryName = value; }
		}
//		public string HotIssuePriorityName
//		{
//			get { return _hotIssuePriorityName; }
//			set { _hotIssuePriorityName = value; }
//		}
		public string HotIssueStatusName
		{
			get { return _hotIssueStatusName; }
			set { _hotIssueStatusName = value; }
		}
		public string ProjectName
		{
			get { return _projectName; }
			set { _projectName = value; }
		}
		public string CreatedBy
		{
			get { return _createdBy; }
			set { _createdBy = value; }
		}
		public DateTime CreatedDate
		{
			get { return _createdDate; }
			set { _createdDate = value; }
		}
		#endregion
	}

	
	public class HotIssueCategoryData
	{
		private int _hotIssueCategoryID = -1;
		private string _hotIssueCategoryName = string.Empty;
		private int _projectID = -1;
		private string _hotIssueCategoryNameEN="";
	

		/// <summary>
		/// Initialize an new empty Login object.
		/// </summary>
		public HotIssueCategoryData()
		{
			
		}

		public HotIssueCategoryData( int hotIssueCategoryID,string hotIssueCategoryName,int projectID)
		{
			this._hotIssueCategoryID = hotIssueCategoryID;
			this._hotIssueCategoryName = hotIssueCategoryName;
			this._projectID = projectID;
		}
			
		public object GetByIndex(int nIndex)
		{
			switch(nIndex)
			{	
				case 0:
					return _hotIssueCategoryID;
				case 1:
					return _hotIssueCategoryName;
				case 2:
					return _projectID;
			}
			return null;
		}
	
		
		#region Properties

		public int HotIssueCategoryID
		{
			get { return _hotIssueCategoryID; }
			set { _hotIssueCategoryID = value; }
		}
		public string HotIssueCategoryNameEN
		{
			get { return _hotIssueCategoryNameEN; }
			set { _hotIssueCategoryNameEN = value; }
		}
		
		public string HotIssueCategoryName
		{
			get { return _hotIssueCategoryName; }
			set { _hotIssueCategoryName = value; }
		}
		
	
		public int ProjectID
		{
			get { return _projectID; }
			set { _projectID = value; }
		}
		#endregion
	}

		
	public class HotIssueStatusData
	{
		private int _hotIssueStatusID = -1;
		private string _hotIssueStatusName = string.Empty;
		private string _hotIssueStatusNameEN = string.Empty;

		/// <summary>
		/// Initialize an new empty Login object.
		/// </summary>
		public HotIssueStatusData()
		{
			
		}

		public HotIssueStatusData( int hotIssueStatusID,string hotIssueStatusName)
		{
			this._hotIssueStatusID = hotIssueStatusID;
			this._hotIssueStatusName = hotIssueStatusName;
		}
			
		public object GetByIndex(int nIndex)
		{
			switch(nIndex)
			{	
				case 0:
					return _hotIssueStatusID;
				case 1:
					return _hotIssueStatusName;
			}
			return null;
		}
	
		
		#region Properties

		public int HotIssueStatusID
		{
			get { return _hotIssueStatusID; }
			set { _hotIssueStatusID = value; }
		}
		public string HotIssueStatusName
		{
			get { return _hotIssueStatusName; }
			set { _hotIssueStatusName = value; }
		}
		public string HotIssueStatusNameEN
		{
			get { return _hotIssueStatusNameEN; }
			set { _hotIssueStatusNameEN = value; }
		}
	
	
		#endregion
	}

	
	public class HotIssuePriorityData
	{
		private int _hotIssuePriorityID = -1;
		private string _hotIssuePriorityName = string.Empty;
	

		/// <summary>
		/// Initialize an new empty Login object.
		/// </summary>
		public HotIssuePriorityData()
		{
			
		}

		public HotIssuePriorityData( int hotIssuePriorityID,string hotIssuePriorityName)
		{
			this._hotIssuePriorityID = hotIssuePriorityID;
			this._hotIssuePriorityName = hotIssuePriorityName;
		}
			
		public object GetByIndex(int nIndex)
		{
			switch(nIndex)
			{	
				case 0:
					return _hotIssuePriorityID;
				case 1:
					return _hotIssuePriorityName;
			}
			return null;
		}
	
		
		#region Properties

		
		public int HotIssuePriorityID
		{
			get { return _hotIssuePriorityID; }
			set { _hotIssuePriorityID = value; }
		}
		public string HotIssuePriorityName
		{
			get { return _hotIssuePriorityName; }
			set { _hotIssuePriorityName = value; }
		}
	
	
		#endregion
	}
}
