using System;
using System.Data;
using Asa.Timesheet.Data;
using Asa.Timesheet.Data.Reports;
using GrapeCity.ActiveReports;
using GrapeCity.ActiveReports.Controls;
using GrapeCity.ActiveReports.SectionReportModel;
using GrapeCity.ActiveReports.Document.Section;
using GrapeCity.ActiveReports.Document;

namespace Asa.Timesheet.WebPages.Reports
{
    public class AbsenceReport : GrapeCity.ActiveReports.SectionReport
	{
		
		//int _pos = 0;
		bool _forPDF = true;
    DateTime _startDate, _endDate;
	DataSet _dsAbsence=null;
    public AbsenceReport(bool forPDF, DateTime startDate, DateTime endDate, DataSet dsAbsence)
    {
        _dsAbsence = dsAbsence;
        _forPDF = forPDF;
        _startDate = startDate;
        _endDate = endDate;

        InitializeComponent();
    }

    private void Folders_ReportStart(object sender, System.EventArgs eArgs)
    {
        //DataSet dsReport = FoldersReportData.SelectFoldersReportData(_projectID, _projectStatus);
        //	this.DataSource = dsReport.Tables[0];
        this.txtReportTitle.Text = String.Format(
          Resource.ResourceManager["Reports_AbsenceReportTitle"], _startDate.ToShortDateString(), _endDate.ToShortDateString());
        this.SubReport1.Report = new AbsenceSubreport();
    }

    private void Folders_FetchData(object sender, FetchEventArgs eArgs)
    {
        //			if (eArgs.EOF) return;
        //
        //			DataTable dt = (DataTable)this.DataSource;
        //
        //      DataTable dtSubreport = AbsenceReportData.GetDaysStringForUser(dt.Rows[_pos]);
        //
        //			if (dtSubreport.Rows.Count > 0) this.SubReport1.Report.DataSource = dtSubreport;
        //	
        //      
        //			_pos++;
    }

    private void Folders_ReportEnd(object sender, System.EventArgs eArgs)
    {

        if (!this._forPDF) return;

        Logo logo = new Logo();
        logo.Run();

        for (int i = 0; i < this.Document.Pages.Count; i++)
            this.Document.Pages[i].Overlay(logo.Document.Pages[0]);
    }

    private void Detail_Format(object sender, System.EventArgs eArgs)
    {
        int mainID = UIHelpers.ToInt(this.txtUserID.Text);
        DataTable dtSubreport = AbsenceReportData.GetDaysStringForUserNew(mainID, null, _dsAbsence);
        if (dtSubreport.Rows.Count > 0)
        {
            if (dtSubreport.Rows.Count > 0) this.SubReport1.Report.DataSource = dtSubreport;

            //				txtAbsence.Text=(string)dtSubreport.Rows[0]["Name"];
            //				txtDaysString.Text=(string)dtSubreport.Rows[0]["DaysString"];
            //				txtFoldersArchive.Text=((int)dtSubreport.Rows[0]["DaysCount"]).ToString();
        }
        ////			else
        ////txtAbsence.Text=txtDaysString.Text=txtFoldersArchive.Text="";
        ////			if(dtSubreport.Rows.Count>1)
        ////			{
        ////				txtAbsence1.Text=(string)dtSubreport.Rows[1]["Name"];
        ////				txtDaysString1.Text=(string)dtSubreport.Rows[1]["DaysString"];
        ////				txtFoldersArchive1.Text=((int)dtSubreport.Rows[1]["DaysCount"]).ToString();
        ////			}
        ////			else
        ////				txtAbsence1.Text=txtDaysString1.Text=txtFoldersArchive1.Text="";
        ////			if(dtSubreport.Rows.Count>2)
        ////			{
        ////				txtAbsence2.Text=(string)dtSubreport.Rows[2]["Name"];
        ////				txtDaysString2.Text=(string)dtSubreport.Rows[2]["DaysString"];
        ////				txtFoldersArchive2.Text=((int)dtSubreport.Rows[2]["DaysCount"]).ToString();
        ////			}
        ////			else
        ////				txtAbsence2.Text=txtDaysString2.Text=txtFoldersArchive2.Text="";
    }

    private void ReportFooter_Format(object sender, System.EventArgs eArgs)
    {
        /*int foldersGiven = 0, foldersArchive = 0;

        FoldersReportData.GetTotalASAFolders( (DataTable)this.DataSource, out foldersGiven, out foldersArchive);
        txtTotalFoldersASAGiven.Text = foldersGiven.ToString();
        txtTotalFoldersASAArchive.Text = foldersArchive.ToString();

   
        FoldersReportData.GetTotalSubcontractersFolders( ((DataTable)this.DataSource).DataSet.Tables[1], out foldersGiven, out foldersArchive);

        txtTotalFoldersGiven.Text = foldersGiven.ToString();
        txtTotalFoldersArchive.Text = foldersArchive.ToString();

    //	txtCreatedBy.Text = _userName;*/
    }

		#region ActiveReports Designer generated code



















    public void InitializeComponent()
    {
        System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(AbsenceReport));
        this.Detail = new GrapeCity.ActiveReports.SectionReportModel.Detail();
        this.ReportHeader = new GrapeCity.ActiveReports.SectionReportModel.ReportHeader();
        this.ReportFooter = new GrapeCity.ActiveReports.SectionReportModel.ReportFooter();
        this.PageHeader = new GrapeCity.ActiveReports.SectionReportModel.PageHeader();
        this.PageFooter = new GrapeCity.ActiveReports.SectionReportModel.PageFooter();
        this.txtReportTitle = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
        this.txtAbsence1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
        this.txtDaysString1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
        this.txtFoldersArchive1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
        this.SubReport1 = new GrapeCity.ActiveReports.SectionReportModel.SubReport();
        this.Line2 = new GrapeCity.ActiveReports.SectionReportModel.Line();
        this.txtObekt = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
        this.txtFoldersArchive = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
        this.txtDaysString = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
        this.txtAbsence = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
        this.txtUserID = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
        this.txtAbsence2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
        this.txtDaysString2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
        this.txtFoldersArchive2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
        ((System.ComponentModel.ISupportInitialize)(this.txtReportTitle)).BeginInit();
        ((System.ComponentModel.ISupportInitialize)(this.txtAbsence1)).BeginInit();
        ((System.ComponentModel.ISupportInitialize)(this.txtDaysString1)).BeginInit();
        ((System.ComponentModel.ISupportInitialize)(this.txtFoldersArchive1)).BeginInit();
        ((System.ComponentModel.ISupportInitialize)(this.txtObekt)).BeginInit();
        ((System.ComponentModel.ISupportInitialize)(this.txtFoldersArchive)).BeginInit();
        ((System.ComponentModel.ISupportInitialize)(this.txtDaysString)).BeginInit();
        ((System.ComponentModel.ISupportInitialize)(this.txtAbsence)).BeginInit();
        ((System.ComponentModel.ISupportInitialize)(this.txtUserID)).BeginInit();
        ((System.ComponentModel.ISupportInitialize)(this.txtAbsence2)).BeginInit();
        ((System.ComponentModel.ISupportInitialize)(this.txtDaysString2)).BeginInit();
        ((System.ComponentModel.ISupportInitialize)(this.txtFoldersArchive2)).BeginInit();
        ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
        // 
        // Detail
        // 
        this.Detail.CanShrink = true;
        this.Detail.ColumnSpacing = 0F;
        this.Detail.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
						this.txtAbsence1,
						this.txtDaysString1,
						this.txtFoldersArchive1,
						this.SubReport1,
						this.Line2,
						this.txtObekt,
						this.txtFoldersArchive,
						this.txtDaysString,
						this.txtAbsence,
						this.txtUserID,
						this.txtAbsence2,
						this.txtDaysString2,
						this.txtFoldersArchive2});
        this.Detail.Height = 0.4895833F;
        this.Detail.KeepTogether = true;
        this.Detail.Name = "Detail";
        // 
        // ReportHeader
        // 
        this.ReportHeader.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
						this.txtReportTitle});
        this.ReportHeader.Height = 0.7868056F;
        this.ReportHeader.Name = "ReportHeader";
        // 
        // ReportFooter
        // 
        this.ReportFooter.Height = 0.1451389F;
        this.ReportFooter.Name = "ReportFooter";
        // 
        // PageHeader
        // 
        this.PageHeader.Height = 0F;
        this.PageHeader.Name = "PageHeader";
        // 
        // PageFooter
        // 
        this.PageFooter.Height = 0F;
        this.PageFooter.Name = "PageFooter";
        // 
        // txtReportTitle
        // 
        this.txtReportTitle.Height = 0.2755906F;
        this.txtReportTitle.Left = 0F;
        this.txtReportTitle.Name = "txtReportTitle";
        this.txtReportTitle.Style = "font-size: 14pt; font-weight: bold; text-align: center";
        this.txtReportTitle.Text = "txtReportTitle";
        this.txtReportTitle.Top = 0F;
        this.txtReportTitle.Width = 6.692914F;
        // 
        // txtAbsence1
        // 
        this.txtAbsence1.CanShrink = true;
        this.txtAbsence1.Height = 0.2F;
        this.txtAbsence1.Left = 0F;
        this.txtAbsence1.Name = "txtAbsence1";
        this.txtAbsence1.OutputFormat = resources.GetString("txtAbsence1.OutputFormat");
        this.txtAbsence1.Style = "font-size: 10pt; font-weight: bold";
        this.txtAbsence1.Top = 0.4375F;
        this.txtAbsence1.Visible = false;
        this.txtAbsence1.Width = 1.968504F;
        // 
        // txtDaysString1
        // 
        this.txtDaysString1.CanShrink = true;
        this.txtDaysString1.Height = 0.2F;
        this.txtDaysString1.Left = 1.968504F;
        this.txtDaysString1.Name = "txtDaysString1";
        this.txtDaysString1.Top = 0.4375F;
        this.txtDaysString1.Visible = false;
        this.txtDaysString1.Width = 3.149606F;
        // 
        // txtFoldersArchive1
        // 
        this.txtFoldersArchive1.CanShrink = true;
        this.txtFoldersArchive1.Height = 0.2F;
        this.txtFoldersArchive1.Left = 5.118111F;
        this.txtFoldersArchive1.Name = "txtFoldersArchive1";
        this.txtFoldersArchive1.OutputFormat = resources.GetString("txtFoldersArchive1.OutputFormat");
        this.txtFoldersArchive1.Style = "font-size: 10pt; text-align: center";
        this.txtFoldersArchive1.Top = 0.4375F;
        this.txtFoldersArchive1.Visible = false;
        this.txtFoldersArchive1.Width = 1.574803F;
        // 
        // SubReport1
        // 
        this.SubReport1.CloseBorder = false;
        this.SubReport1.Height = 0.1968504F;
        this.SubReport1.Left = 0.0625F;
        this.SubReport1.Name = "SubReport1";
        this.SubReport1.Report = null;
        this.SubReport1.Top = 0.25F;
        this.SubReport1.Width = 6.692914F;
        // 
        // Line2
        // 
        this.Line2.Height = 0F;
        this.Line2.Left = 0F;
        this.Line2.LineWeight = 1F;
        this.Line2.Name = "Line2";
        this.Line2.Top = 0.2362205F;
        this.Line2.Width = 6.692914F;
        this.Line2.X1 = 0F;
        this.Line2.X2 = 6.692914F;
        this.Line2.Y1 = 0.2362205F;
        this.Line2.Y2 = 0.2362205F;
        // 
        // txtObekt
        // 
        this.txtObekt.DataField = "Name";
        this.txtObekt.Height = 0.2362205F;
        this.txtObekt.Left = 0F;
        this.txtObekt.Name = "txtObekt";
        this.txtObekt.Style = "font-size: 12pt; font-weight: bold";
        this.txtObekt.Text = "txtUserName";
        this.txtObekt.Top = 0F;
        this.txtObekt.Width = 6.692914F;
        // 
        // txtFoldersArchive
        // 
        this.txtFoldersArchive.DataField = "DaysCount";
        this.txtFoldersArchive.Height = 0.2F;
        this.txtFoldersArchive.Left = 5.118111F;
        this.txtFoldersArchive.Name = "txtFoldersArchive";
        this.txtFoldersArchive.OutputFormat = resources.GetString("txtFoldersArchive.OutputFormat");
        this.txtFoldersArchive.Style = "font-size: 10pt; text-align: center";
        this.txtFoldersArchive.Top = 0.25F;
        this.txtFoldersArchive.Visible = false;
        this.txtFoldersArchive.Width = 1.574803F;
        // 
        // txtDaysString
        // 
        this.txtDaysString.DataField = "DaysString";
        this.txtDaysString.Height = 0.2F;
        this.txtDaysString.Left = 1.968504F;
        this.txtDaysString.Name = "txtDaysString";
        this.txtDaysString.Top = 0.25F;
        this.txtDaysString.Visible = false;
        this.txtDaysString.Width = 3.149606F;
        // 
        // txtAbsence
        // 
        this.txtAbsence.DataField = "Name";
        this.txtAbsence.Height = 0.2F;
        this.txtAbsence.Left = 0F;
        this.txtAbsence.Name = "txtAbsence";
        this.txtAbsence.OutputFormat = resources.GetString("txtAbsence.OutputFormat");
        this.txtAbsence.Style = "font-size: 10pt; font-weight: bold";
        this.txtAbsence.Top = 0.25F;
        this.txtAbsence.Visible = false;
        this.txtAbsence.Width = 1.968504F;
        // 
        // txtUserID
        // 
        this.txtUserID.DataField = "UserID";
        this.txtUserID.Height = 0.2F;
        this.txtUserID.Left = 4.625F;
        this.txtUserID.Name = "txtUserID";
        this.txtUserID.OutputFormat = resources.GetString("txtUserID.OutputFormat");
        this.txtUserID.Style = "font-size: 10pt; text-align: center";
        this.txtUserID.Top = 0.4375F;
        this.txtUserID.Visible = false;
        this.txtUserID.Width = 1.574803F;
        // 
        // txtAbsence2
        // 
        this.txtAbsence2.CanShrink = true;
        this.txtAbsence2.Height = 0.2F;
        this.txtAbsence2.Left = 0F;
        this.txtAbsence2.Name = "txtAbsence2";
        this.txtAbsence2.OutputFormat = resources.GetString("txtAbsence2.OutputFormat");
        this.txtAbsence2.Style = "font-size: 10pt; font-weight: bold; text-align: right";
        this.txtAbsence2.Top = 0.625F;
        this.txtAbsence2.Visible = false;
        this.txtAbsence2.Width = 1.968504F;
        // 
        // txtDaysString2
        // 
        this.txtDaysString2.CanShrink = true;
        this.txtDaysString2.Height = 0.2F;
        this.txtDaysString2.Left = 1.968504F;
        this.txtDaysString2.Name = "txtDaysString2";
        this.txtDaysString2.Top = 0.625F;
        this.txtDaysString2.Visible = false;
        this.txtDaysString2.Width = 3.149606F;
        // 
        // txtFoldersArchive2
        // 
        this.txtFoldersArchive2.CanShrink = true;
        this.txtFoldersArchive2.Height = 0.2F;
        this.txtFoldersArchive2.Left = 5.118111F;
        this.txtFoldersArchive2.Name = "txtFoldersArchive2";
        this.txtFoldersArchive2.OutputFormat = resources.GetString("txtFoldersArchive2.OutputFormat");
        this.txtFoldersArchive2.Style = "font-size: 10pt; text-align: center";
        this.txtFoldersArchive2.Top = 0.625F;
        this.txtFoldersArchive2.Visible = false;
        this.txtFoldersArchive2.Width = 1.574803F;
        // 
        // ActiveReport1
        // 
        this.MasterReport = false;
        this.PageSettings.DefaultPaperSize = false;
        this.PageSettings.Margins.Bottom = 0.4097222F;
        this.PageSettings.Margins.Left = 1.377778F;
        this.PageSettings.Margins.Right = 0.8034722F;
        this.PageSettings.Margins.Top = 0.8034722F;
        this.PageSettings.PaperHeight = 11.69306F;
        this.PageSettings.PaperKind = System.Drawing.Printing.PaperKind.A4;
        this.PageSettings.PaperWidth = 8.268056F;
        this.PrintWidth = 6.692914F;
        this.Sections.Add(this.ReportHeader);
        this.Sections.Add(this.PageHeader);
        this.Sections.Add(this.Detail);
        this.Sections.Add(this.PageFooter);
        this.Sections.Add(this.ReportFooter);
        this.StyleSheet.Add(new DDCssLib.StyleSheetRule(resources.GetString("$this.StyleSheet"), "Normal"));
        this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: inherit; font-style: inherit; font-variant: inherit; font-weight: bo" +
                    "ld; font-size: 16pt; font-size-adjust: inherit; font-stretch: inherit", "Heading1", "Normal"));
        this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: Times New Roman; font-style: italic; font-variant: inherit; font-wei" +
                    "ght: bold; font-size: 14pt; font-size-adjust: inherit; font-stretch: inherit", "Heading2", "Normal"));
        this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: inherit; font-style: inherit; font-variant: inherit; font-weight: bo" +
                    "ld; font-size: 13pt; font-size-adjust: inherit; font-stretch: inherit", "Heading3", "Normal"));
        ((System.ComponentModel.ISupportInitialize)(this.txtReportTitle)).EndInit();
        ((System.ComponentModel.ISupportInitialize)(this.txtAbsence1)).EndInit();
        ((System.ComponentModel.ISupportInitialize)(this.txtDaysString1)).EndInit();
        ((System.ComponentModel.ISupportInitialize)(this.txtFoldersArchive1)).EndInit();
        ((System.ComponentModel.ISupportInitialize)(this.txtObekt)).EndInit();
        ((System.ComponentModel.ISupportInitialize)(this.txtFoldersArchive)).EndInit();
        ((System.ComponentModel.ISupportInitialize)(this.txtDaysString)).EndInit();
        ((System.ComponentModel.ISupportInitialize)(this.txtAbsence)).EndInit();
        ((System.ComponentModel.ISupportInitialize)(this.txtUserID)).EndInit();
        ((System.ComponentModel.ISupportInitialize)(this.txtAbsence2)).EndInit();
        ((System.ComponentModel.ISupportInitialize)(this.txtDaysString2)).EndInit();
        ((System.ComponentModel.ISupportInitialize)(this.txtFoldersArchive2)).EndInit();
        ((System.ComponentModel.ISupportInitialize)(this)).EndInit();

        // Attach Report Events
        this.ReportStart += new System.EventHandler(this.Folders_ReportStart);
        this.FetchData += new GrapeCity.ActiveReports.SectionReport.FetchEventHandler(this.Folders_FetchData);
        this.ReportEnd += new System.EventHandler(this.Folders_ReportEnd);
        this.Detail.Format += new System.EventHandler(this.Detail_Format);
        this.ReportFooter.Format += new System.EventHandler(this.ReportFooter_Format);
    }

		#endregion

        private ReportHeader ReportHeader;
        private TextBox txtReportTitle;
        private PageHeader PageHeader;
        private Detail Detail;
        private TextBox txtAbsence1;
        private TextBox txtDaysString1;
        private TextBox txtFoldersArchive1;
        private SubReport SubReport1;
        private Line Line2;
        private TextBox txtObekt;
        private TextBox txtFoldersArchive;
        private TextBox txtDaysString;
        private TextBox txtAbsence;
        private TextBox txtUserID;
        private TextBox txtAbsence2;
        private TextBox txtDaysString2;
        private TextBox txtFoldersArchive2;
        private PageFooter PageFooter;
        private ReportFooter ReportFooter;
	}
}
