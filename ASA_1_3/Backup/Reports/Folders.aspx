﻿<%@ Register TagPrefix="uc1" TagName="PageHeader" Src="../UserControls/PageHeader.ascx" %>
<%@ Register TagPrefix="activereportsweb" Namespace="GrapeCity.ActiveReports.Web" Assembly="GrapeCity.ActiveReports.Web.v7, Version=7.1.7470.0, Culture=neutral, PublicKeyToken=cc4967777c49a3ff" %>
<%@ Page language="c#" Codebehind="Folders.aspx.cs" AutoEventWireup="false" Inherits="Asa.Timesheet.WebPages.Reports.FoldersReportPage" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
	<head runat="server">
		<title>Папки</title>
		<meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
		<meta content="C#" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<LINK href="../styles/timesheet.css" type="text/css" rel="stylesheet">
	</HEAD>
	<body bottomMargin="0" leftMargin="0" topMargin="0" rightMargin="0">
		<form id="Form1" method="post" runat="server">
			<table height="100%" cellSpacing="0" cellPadding="0" width="100%" border="0">
				<tr height="1">
					<td><uc1:pageheader id="header" runat="server"></uc1:pageheader></td>
				</tr>
				<tr>
					<td>
						<table height="100%" cellSpacing="0" cellPadding="0" width="100%" border="0">
							<tr>
								<td class="menuLeft"><asp:placeholder id="menuHolder" runat="server"></asp:placeholder></td>
								
								<td style="PADDING-RIGHT: 5px; PADDING-LEFT: 5px; PADDING-TOP: 3px" vAlign="top" noWrap>
									<TABLE id="tblInfo" height="30" cellSpacing="0" cellPadding="3" width="880" border="0">
										<TR>
											<td vAlign="middle" noWrap><asp:label id="lblError" runat="server" CssClass="ErrorLabel" EnableViewState="False" ForeColor="Red"></asp:label><asp:label id="lblInfo" runat="server" CssClass="InfoLabel" EnableViewState="False"></asp:label></TD>
										</TR>
									</TABLE>
									<TABLE id="Table3" cellSpacing="0" cellPadding="0" width="880" border="0">
										<TR>
											<td style="HEIGHT: 5px" colSpan="2"></TD>
										</TR>
										<TR>
											<td colSpan="2"><IMG height="1" src="../images/dot.gif" width="100%"></TD>
										</TR>
										<TR>
											<td colSpan="2" height="3"></td>
										</TR>
										<tr>
											<td>
												<table cellSpacing="0" cellPadding="3" border="0">
													<TR>
														<td style="WIDTH: 139px" vAlign="top">
															<asp:label id="Label4" runat="server" CssClass="enterDataLabel" Width="100%">Тип:</asp:label></TD>
														<td style="WIDTH: 243px" vAlign="top"><asp:dropdownlist id="ddlProjectStatus" runat="server" CssClass="EnterDataBox" Width="250px" AutoPostBack="True">
															</asp:dropdownlist></TD>
														<td vAlign="top">
														</TD>
														<td></TD>
														<td></TD>
													<TR>
														<td style="WIDTH: 139px" vAlign="top"><asp:label id="Label2" runat="server" CssClass="enterDataLabel" Width="100%">Проект:</asp:label></TD>
														<td style="WIDTH: 243px" vAlign="top"><asp:dropdownlist id="ddlProjects" runat="server" CssClass="EnterDataBox" Width="250px"></asp:dropdownlist></TD>
														<td vAlign="top">
														</TD>
														<td></TD>
														<td><asp:imagebutton id="ibGenerate" runat="server" ImageUrl="../images/ie.gif"></asp:imagebutton></TD>
														<td style="WIDTH: 8px"><asp:imagebutton id="ibPdfExport" runat="server" ImageUrl="../images/pdf1.gif" ToolTip="Справка в PDF формат"></asp:imagebutton></TD>
														<td style="WIDTH: 8px"><asp:imagebutton id="ibXlsExport" runat="server" ImageUrl="../images/xls.gif" ToolTip="Справка в Excel формат"></asp:imagebutton></TD>
													</TR>
												</table>
											</td>
										</tr>
										<TR>
											<td colSpan="2"><IMG height="1" src="../images/dot.gif" width="100%"></TD>
										</TR>
										<TR>
											<td colSpan="2"><IMG height="1" src="images/dot.gif" width="100%"></TD>
										</TR>
									</TABLE>
									<P>&nbsp;</P>
									<TABLE id="Table5" cellSpacing="0" cellPadding="0" width="100%" border="0">
										<TR>
											<td align="center"><asp:label id="lblReportTitle" runat="server" EnableViewState="False" Visible="False" Font-Bold="True"
													Font-Size="12pt"> Папки</asp:label></TD>
										</TR>
									</TABLE>
									<br>
									<br>
									<p align="center"><asp:datagrid id="grdProjects" runat="server" EnableViewState="False" BorderWidth="0px" BorderStyle="None"
											AutoGenerateColumns="False">
											<Columns>
												<asp:TemplateColumn>
													<ItemTemplate>
														<TABLE id="Table1" cellSpacing="0" cellPadding="0" border="0">
															<TR>
																<td>
																	<TABLE id="tblProjectHeader" cellSpacing="0" cellPadding="0" border="0">
																		<TR>
																			<td>
																				<asp:Label id="lblProjectLbl" runat="server" Font-Size="11pt">Обект:</asp:Label></TD>
																			<td noWrap width="20"></TD>
																			<td>
																				<asp:Label id="lblProjectName" runat="server" Font-Size="11pt" Font-Bold="True"></asp:Label></TD>
																		</TR>
																		<TR>
																			<td>
																			<td>
																			<td>
																				<asp:Label id="lblAddress" runat="server" Font-Size="11pt"></asp:Label></TD>
																		</TR>
																		<TR>
																			<td>
																				<asp:Label id="Label3" runat="server" Font-Size="11pt">Папки АСА:</asp:Label></TD>
																			<td></TD>
																			<td>
																				<TABLE id="Table4" cellSpacing="0" cellPadding="0" border="0">
																					<TR>
																						<td width="120">
																							<asp:Label id="lblFoldersGiven" runat="server" Font-Size="11pt" Font-Bold="True"></asp:Label></TD>
																						<td noWrap width="20"></TD>
																						<td width="120">
																							<asp:Label id="lblFoldersArchive" runat="server" Font-Size="11pt" Font-Bold="True"></asp:Label></TD>
																					</TR>
																				</TABLE>
																			</TD>
																		</TR>
																	</TABLE>
																</TD>
															</TR>
															<TR height="5">
																<td style="HEIGHT: 13px"></TD>
															</TR>
															<TR>
																<td>
																	<asp:datagrid id="grdFolders" runat="server" ForeColor="DimGray" EnableViewState="False" CssClass="ReportGrid"
																		Width="700px" AutoGenerateColumns="False" ShowFooter="True" CellPadding="4">
																		<HeaderStyle CssClass="reportGridHeader"></HeaderStyle>
																		<FooterStyle Font-Bold="True" ForeColor="Black"></FooterStyle>
																		<Columns>
																			<asp:BoundColumn DataField="SubcontracterName" HeaderText="Подизпълнител" FooterText="Общо за проекта:"></asp:BoundColumn>
																			<asp:BoundColumn DataField="SubcontracterType" HeaderText="Част">
																				<HeaderStyle Width="200px"></HeaderStyle>
																			</asp:BoundColumn>
																			<asp:BoundColumn DataField="FoldersGiven" HeaderText="Дадени папки">
																				<HeaderStyle Width="120px"></HeaderStyle>
																				<ItemStyle HorizontalAlign="Center"></ItemStyle>
																			</asp:BoundColumn>
																			<asp:BoundColumn DataField="FoldersArchive" HeaderText="Папки в архива">
																				<HeaderStyle Width="120px"></HeaderStyle>
																				<ItemStyle HorizontalAlign="Center"></ItemStyle>
																			</asp:BoundColumn>
																		</Columns>
																	</asp:datagrid></TD>
															</TR>
															<TR>
																<td></TD>
															</TR>
															<TR height="35">
																<td></TD>
															</TR>
														</TABLE>
													</ItemTemplate>
												</asp:TemplateColumn>
											</Columns>
										</asp:datagrid>
										<TABLE id="tblTotals" cellSpacing="0" cellPadding="2" width="700" border="0" runat="server"
											visible="false">
											<TR>
												<td><asp:label id="Label7" runat="server" EnableViewState="False" Font-Bold="True" Font-Size="11pt">Общо папки АСА:</asp:label></TD>
												<td noWrap align="center" width="120"><asp:label id="lblTotalASAGiven" runat="server" EnableViewState="False" Font-Bold="True" Font-Size="11pt"></asp:label></TD>
												<td noWrap align="center" width="120"><asp:label id="lblTotalASAArchive" runat="server" EnableViewState="False" Font-Bold="True"
														Font-Size="11pt"></asp:label></TD>
											</TR>
											<TR>
												<td><asp:label id="Label5" runat="server" EnableViewState="False" Font-Bold="True" Font-Size="11pt">Общо папки подизпълнители:</asp:label></TD>
												<td align="center"><asp:label id="lblTotalSubcGiven" runat="server" EnableViewState="False" Font-Bold="True" Font-Size="11pt"></asp:label></TD>
												<td align="center"><asp:label id="lblTotalSubcArchive" runat="server" EnableViewState="False" Font-Bold="True"
														Font-Size="11pt"></asp:label></TD>
											</TR>
										</TABLE>
									</p>
									<br>
									<asp:label id="lblNoDataFound" runat="server" CssClass="InfoLabel" EnableViewState="False"
										Visible="False"></asp:label><br>
									<br>
									<br>
									<br>
									<br>
									<asp:label id="lblCreatedByLbl" runat="server" EnableViewState="False" Visible="False" Font-Bold="True"
										Font-Size="9pt">ИЗГОТВИЛ СПРАВКАТА:</asp:label><asp:label id="lblCreatedBy" runat="server" EnableViewState="False" Visible="False" Font-Size="9pt"></asp:label><br>
									<br>
								</td>
							</tr>
						</table>
					</td>
				</tr>
			</table>
		</form>
	</body>
</HTML>
