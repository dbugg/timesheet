using System;
using System.Collections;

namespace Asa.Timesheet.Data
{
	[Serializable]
	public class HotIssuesVector : ICollection, IList, IEnumerable, ICloneable
	{
		#region Interfaces
		/// <summary>
		///		Supports type-safe iteration over a <see cref="HotIssuesVector"/>.
		/// </summary>
		public interface IHotIssuesVectorEnumerator
		{
			/// <summary>
			///		Gets the current element in the collection.
			/// </summary>
			HotIssueData Current {get;}

			/// <summary>
			///		Advances the enumerator to the next element in the collection.
			/// </summary>
			/// <exception cref="InvalidOperationException">
			///		The collection was modified after the enumerator was created.
			/// </exception>
			/// <returns>
			///		<c>true</c> if the enumerator was successfully advanced to the next element; 
			///		<c>false</c> if the enumerator has passed the end of the collection.
			/// </returns>
			bool MoveNext();

			/// <summary>
			///		Sets the enumerator to its initial position, before the first element in the collection.
			/// </summary>
			void Reset();
		}
		#endregion

		private const int DEFAULT_CAPACITY = 16;

		#region Implementation (data)
		private HotIssueData[] m_array;
		private int m_count = 0;
		[NonSerialized]
		private int m_version = 0;
		#endregion
	
		#region Static Wrappers
		/// <summary>
		///		Creates a synchronized (thread-safe) wrapper for a 
		///     <c>HotIssuesVector</c> instance.
		/// </summary>
		/// <returns>
		///     An <c>HotIssuesVector</c> wrapper that is synchronized (thread-safe).
		/// </returns>
		public static HotIssuesVector Synchronized(HotIssuesVector list)
		{
			if(list==null)
				throw new ArgumentNullException("list");
			return new SyncHotIssuesVector(list);
		}
        
		/// <summary>
		///		Creates a read-only wrapper for a 
		///     <c>HotIssuesVector</c> instance.
		/// </summary>
		/// <returns>
		///     An <c>HotIssuesVector</c> wrapper that is read-only.
		/// </returns>
		public static HotIssuesVector ReadOnly(HotIssuesVector list)
		{
			if(list==null)
				throw new ArgumentNullException("list");
			return new ReadOnlyHotIssuesVector(list);
		}
		#endregion

		#region Construction
		/// <summary>
		///		Initializes a new instance of the <c>HotIssuesVector</c> class
		///		that is empty and has the default initial capacity.
		/// </summary>
		public HotIssuesVector()
		{
			m_array = new HotIssueData[DEFAULT_CAPACITY];
		}
		
		/// <summary>
		///		Initializes a new instance of the <c>HotIssuesVector</c> class
		///		that has the specified initial capacity.
		/// </summary>
		/// <param name="capacity">
		///		The number of elements that the new <c>HotIssuesVector</c> is initially capable of storing.
		///	</param>
		public HotIssuesVector(int capacity)
		{
			m_array = new HotIssueData[capacity];
		}

		/// <summary>
		///		Initializes a new instance of the <c>HotIssuesVector</c> class
		///		that contains elements copied from the specified <c>HotIssuesVector</c>.
		/// </summary>
		/// <param name="c">The <c>HotIssuesVector</c> whose elements are copied to the new collection.</param>
		public HotIssuesVector(HotIssuesVector c)
		{
			m_array = new HotIssueData[c.Count];
			AddRange(c);
		}

		/// <summary>
		///		Initializes a new instance of the <c>HotIssuesVector</c> class
		///		that contains elements copied from the specified <see cref="HotIssueData"/> array.
		/// </summary>
		/// <param name="a">The <see cref="HotIssueData"/> array whose elements are copied to the new list.</param>
		public HotIssuesVector(HotIssueData[] a)
		{
			m_array = new HotIssueData[a.Length];
			AddRange(a);
		}
		
		protected enum Tag 
		{
			Default
		}

		protected HotIssuesVector(Tag t)
		{
			m_array = null;
		}
		#endregion
		
		#region Operations (type-safe ICollection)
		/// <summary>
		///		Gets the number of elements actually contained in the <c>HotIssuesVector</c>.
		/// </summary>
		public virtual int Count
		{
			get { return m_count; }
		}

		/// <summary>
		///		Copies the entire <c>HotIssuesVector</c> to a one-dimensional
		///		<see cref="HotIssueData"/> array.
		/// </summary>
		/// <param name="array">The one-dimensional <see cref="HotIssueData"/> array to copy to.</param>
		public virtual void CopyTo(HotIssueData[] array)
		{
			this.CopyTo(array, 0);
		}

		/// <summary>
		///		Copies the entire <c>HotIssuesVector</c> to a one-dimensional
		///		<see cref="HotIssueData"/> array, starting at the specified index of the target array.
		/// </summary>
		/// <param name="array">The one-dimensional <see cref="HotIssueData"/> array to copy to.</param>
		/// <param name="start">The zero-based index in <paramref name="array"/> at which copying begins.</param>
		public virtual void CopyTo(HotIssueData[] array, int start)
		{
			if (m_count > array.GetUpperBound(0) + 1 - start)
				throw new System.ArgumentException("Destination array was not long enough.");
			
			Array.Copy(m_array, 0, array, start, m_count); 
		}

		/// <summary>
		///		Gets a value indicating whether access to the collection is synchronized (thread-safe).
		/// </summary>
		/// <returns>true if access to the ICollection is synchronized (thread-safe); otherwise, false.</returns>
		public virtual bool IsSynchronized
		{
			get { return m_array.IsSynchronized; }
		}

		/// <summary>
		///		Gets an object that can be used to synchronize access to the collection.
		/// </summary>
		public virtual object SyncRoot
		{
			get { return m_array.SyncRoot; }
		}
		#endregion
		
		#region Operations (type-safe IList)
		/// <summary>
		///		Gets or sets the <see cref="HotIssueData"/> at the specified index.
		/// </summary>
		/// <param name="index">The zero-based index of the element to get or set.</param>
		/// <exception cref="ArgumentOutOfRangeException">
		///		<para><paramref name="index"/> is less than zero</para>
		///		<para>-or-</para>
		///		<para><paramref name="index"/> is equal to or greater than <see cref="HotIssuesVector.Count"/>.</para>
		/// </exception>
		public virtual HotIssueData this[int index]
		{
			get
			{
				ValidateIndex(index); // throws
				return m_array[index]; 
			}
			set
			{
				ValidateIndex(index); // throws
				++m_version; 
				m_array[index] = value; 
			}
		}

		/// <summary>
		///		Adds a <see cref="HotIssueData"/> to the end of the <c>HotIssuesVector</c>.
		/// </summary>
		/// <param name="item">The <see cref="HotIssueData"/> to be added to the end of the <c>HotIssuesVector</c>.</param>
		/// <returns>The index at which the value has been added.</returns>
		public virtual int Add(HotIssueData item)
		{
			if (m_count == m_array.Length)
				EnsureCapacity(m_count + 1);

			m_array[m_count] = item;
			m_version++;

			return m_count++;
		}
		
		/// <summary>
		///		Removes all elements from the <c>HotIssuesVector</c>.
		/// </summary>
		public virtual void Clear()
		{
			++m_version;
			m_array = new HotIssueData[DEFAULT_CAPACITY];
			m_count = 0;
		}
		
		/// <summary>
		///		Creates a shallow copy of the <see cref="HotIssuesVector"/>.
		/// </summary>
		public virtual object Clone()
		{
			HotIssuesVector newColl = new HotIssuesVector(m_count);
			Array.Copy(m_array, 0, newColl.m_array, 0, m_count);
			newColl.m_count = m_count;
			newColl.m_version = m_version;

			return newColl;
		}

		/// <summary>
		///		Determines whether a given <see cref="HotIssueData"/> is in the <c>HotIssuesVector</c>.
		/// </summary>
		/// <param name="item">The <see cref="HotIssueData"/> to check for.</param>
		/// <returns><c>true</c> if <paramref name="item"/> is found in the <c>HotIssuesVector</c>; otherwise, <c>false</c>.</returns>
		public virtual bool Contains(HotIssueData item)
		{
			for (int i=0; i != m_count; ++i)
				if (m_array[i].Equals(item))
					return true;
			return false;
		}

		/// <summary>
		///		Returns the zero-based index of the first occurrence of a <see cref="HotIssueData"/>
		///		in the <c>HotIssuesVector</c>.
		/// </summary>
		/// <param name="item">The <see cref="HotIssueData"/> to locate in the <c>HotIssuesVector</c>.</param>
		/// <returns>
		///		The zero-based index of the first occurrence of <paramref name="item"/> 
		///		in the entire <c>HotIssuesVector</c>, if found; otherwise, -1.
		///	</returns>
		public virtual int IndexOf(HotIssueData item)
		{
			for (int i=0; i != m_count; ++i)
				if (m_array[i].Equals(item))
					return i;
			return -1;
		}

		public virtual int FindByHotIssueID(int id)
		{
			for (int i=0; i != m_count; ++i)
				if (m_array[i].HotIssueID == id)
					return i;
			return -1;
		}
		/// <summary>
		///		Inserts an element into the <c>HotIssuesVector</c> at the specified index.
		/// </summary>
		/// <param name="index">The zero-based index at which <paramref name="item"/> should be inserted.</param>
		/// <param name="item">The <see cref="HotIssueData"/> to insert.</param>
		/// <exception cref="ArgumentOutOfRangeException">
		///		<para><paramref name="index"/> is less than zero</para>
		///		<para>-or-</para>
		///		<para><paramref name="index"/> is equal to or greater than <see cref="HotIssuesVector.Count"/>.</para>
		/// </exception>
		public virtual void Insert(int index, HotIssueData item)
		{
			ValidateIndex(index, true); // throws
			
			if (m_count == m_array.Length)
				EnsureCapacity(m_count + 1);

			if (index < m_count)
			{
				Array.Copy(m_array, index, m_array, index + 1, m_count - index);
			}

			m_array[index] = item;
			m_count++;
			m_version++;
		}

		/// <summary>
		///		Removes the first occurrence of a specific <see cref="HotIssueData"/> from the <c>HotIssuesVector</c>.
		/// </summary>
		/// <param name="item">The <see cref="HotIssueData"/> to remove from the <c>HotIssuesVector</c>.</param>
		/// <exception cref="ArgumentException">
		///		The specified <see cref="HotIssueData"/> was not found in the <c>HotIssuesVector</c>.
		/// </exception>
		public virtual void Remove(HotIssueData item)
		{		   
			int i = IndexOf(item);
			if (i < 0)
				throw new System.ArgumentException("Cannot remove the specified item because it was not found in the specified Collection.");
			
			++m_version;
			RemoveAt(i);
		}

		/// <summary>
		///		Removes the element at the specified index of the <c>HotIssuesVector</c>.
		/// </summary>
		/// <param name="index">The zero-based index of the element to remove.</param>
		/// <exception cref="ArgumentOutOfRangeException">
		///		<para><paramref name="index"/> is less than zero</para>
		///		<para>-or-</para>
		///		<para><paramref name="index"/> is equal to or greater than <see cref="HotIssuesVector.Count"/>.</para>
		/// </exception>
		public virtual void RemoveAt(int index)
		{
			ValidateIndex(index); // throws
			
			m_count--;

			if (index < m_count)
			{
				Array.Copy(m_array, index + 1, m_array, index, m_count - index);
			}
			
			// We can't set the deleted entry equal to null, because it might be a value type.
			// Instead, we'll create an empty single-element array of the right type and copy it 
			// over the entry we want to erase.
			HotIssueData[] temp = new HotIssueData[1];
			Array.Copy(temp, 0, m_array, m_count, 1);
			m_version++;
		}

		/// <summary>
		///		Gets a value indicating whether the collection has a fixed size.
		/// </summary>
		/// <value>true if the collection has a fixed size; otherwise, false. The default is false</value>
		public virtual bool IsFixedSize
		{
			get { return false; }
		}

		/// <summary>
		///		gets a value indicating whether the <B>IList</B> is read-only.
		/// </summary>
		/// <value>true if the collection is read-only; otherwise, false. The default is false</value>
		public virtual bool IsReadOnly
		{
			get { return false; }
		}
		#endregion

		#region Operations (type-safe IEnumerable)
		
		/// <summary>
		///		Returns an enumerator that can iterate through the <c>HotIssuesVector</c>.
		/// </summary>
		/// <returns>An <see cref="Enumerator"/> for the entire <c>HotIssuesVector</c>.</returns>
		public virtual IHotIssuesVectorEnumerator GetEnumerator()
		{
			return new Enumerator(this);
		}
		#endregion

		#region Public helpers (just to mimic some nice features of ArrayList)
		
		/// <summary>
		///		Gets or sets the number of elements the <c>HotIssuesVector</c> can contain.
		/// </summary>
		public virtual int Capacity
		{
			get { return m_array.Length; }
			
			set
			{
				if (value < m_count)
					value = m_count;

				if (value != m_array.Length)
				{
					if (value > 0)
					{
						HotIssueData[] temp = new HotIssueData[value];
						Array.Copy(m_array, temp, m_count);
						m_array = temp;
					}
					else
					{
						m_array = new HotIssueData[DEFAULT_CAPACITY];
					}
				}
			}
		}

		/// <summary>
		///		Adds the elements of another <c>HotIssuesVector</c> to the current <c>HotIssuesVector</c>.
		/// </summary>
		/// <param name="x">The <c>HotIssuesVector</c> whose elements should be added to the end of the current <c>HotIssuesVector</c>.</param>
		/// <returns>The new <see cref="HotIssuesVector.Count"/> of the <c>HotIssuesVector</c>.</returns>
		public virtual int AddRange(HotIssuesVector x)
		{
			if (m_count + x.Count >= m_array.Length)
				EnsureCapacity(m_count + x.Count);
			
			Array.Copy(x.m_array, 0, m_array, m_count, x.Count);
			m_count += x.Count;
			m_version++;

			return m_count;
		}

		/// <summary>
		///		Adds the elements of a <see cref="HotIssueData"/> array to the current <c>HotIssuesVector</c>.
		/// </summary>
		/// <param name="x">The <see cref="HotIssueData"/> array whose elements should be added to the end of the <c>HotIssuesVector</c>.</param>
		/// <returns>The new <see cref="HotIssuesVector.Count"/> of the <c>HotIssuesVector</c>.</returns>
		public virtual int AddRange(HotIssueData[] x)
		{
			if (m_count + x.Length >= m_array.Length)
				EnsureCapacity(m_count + x.Length);

			Array.Copy(x, 0, m_array, m_count, x.Length);
			m_count += x.Length;
			m_version++;

			return m_count;
		}
		
		/// <summary>
		///		Sets the capacity to the actual number of elements.
		/// </summary>
		public virtual void TrimToSize()
		{
			this.Capacity = m_count;
		}

		#endregion

		#region Implementation (helpers)

		/// <exception cref="ArgumentOutOfRangeException">
		///		<para><paramref name="index"/> is less than zero</para>
		///		<para>-or-</para>
		///		<para><paramref name="index"/> is equal to or greater than <see cref="HotIssuesVector.Count"/>.</para>
		/// </exception>
		private void ValidateIndex(int i)
		{
			ValidateIndex(i, false);
		}

		/// <exception cref="ArgumentOutOfRangeException">
		///		<para><paramref name="index"/> is less than zero</para>
		///		<para>-or-</para>
		///		<para><paramref name="index"/> is equal to or greater than <see cref="HotIssuesVector.Count"/>.</para>
		/// </exception>
		private void ValidateIndex(int i, bool allowEqualEnd)
		{
			int max = (allowEqualEnd)?(m_count):(m_count-1);
			if (i < 0 || i > max)
				throw new System.ArgumentOutOfRangeException("Index was out of range.  Must be non-negative and less than the size of the collection.", (object)i, "Specified argument was out of the range of valid values.");
		}

		private void EnsureCapacity(int min)
		{
			int newCapacity = ((m_array.Length == 0) ? DEFAULT_CAPACITY : m_array.Length * 2);
			if (newCapacity < min)
				newCapacity = min;

			this.Capacity = newCapacity;
		}

		#endregion
		
		#region Implementation (ICollection)

		void ICollection.CopyTo(Array array, int start)
		{
			Array.Copy(m_array, 0, array, start, m_count);
		}

		#endregion

		#region Implementation (IList)

		object IList.this[int i]
		{
			get { return (object)this[i]; }
			set { this[i] = (HotIssueData)value; }
		}

		int IList.Add(object x)
		{
			return this.Add((HotIssueData)x);
		}

		bool IList.Contains(object x)
		{
			return this.Contains((HotIssueData)x);
		}

		int IList.IndexOf(object x)
		{
			return this.IndexOf((HotIssueData)x);
		}

		void IList.Insert(int pos, object x)
		{
			this.Insert(pos, (HotIssueData)x);
		}

		void IList.Remove(object x)
		{
			this.Remove((HotIssueData)x);
		}

		void IList.RemoveAt(int pos)
		{
			this.RemoveAt(pos);
		}

		#endregion

		#region Implementation (IEnumerable)

		IEnumerator IEnumerable.GetEnumerator()
		{
			return (IEnumerator)(this.GetEnumerator());
		}

		#endregion

		#region Nested enumerator class
		/// <summary>
		///		Supports simple iteration over a <see cref="HotIssuesVector"/>.
		/// </summary>
		private class Enumerator : IEnumerator, IHotIssuesVectorEnumerator
		{
			#region Implementation (data)
			
			private HotIssuesVector m_collection;
			private int m_index;
			private int m_version;
			
			#endregion
		
			#region Construction
			
			/// <summary>
			///		Initializes a new instance of the <c>Enumerator</c> class.
			/// </summary>
			/// <param name="tc"></param>
			internal Enumerator(HotIssuesVector tc)
			{
				m_collection = tc;
				m_index = -1;
				m_version = tc.m_version;
			}
			
			#endregion
	
			#region Operations (type-safe IEnumerator)
			
			/// <summary>
			///		Gets the current element in the collection.
			/// </summary>
			public HotIssueData Current
			{
				get { return m_collection[m_index]; }
			}

			/// <summary>
			///		Advances the enumerator to the next element in the collection.
			/// </summary>
			/// <exception cref="InvalidOperationException">
			///		The collection was modified after the enumerator was created.
			/// </exception>
			/// <returns>
			///		<c>true</c> if the enumerator was successfully advanced to the next element; 
			///		<c>false</c> if the enumerator has passed the end of the collection.
			/// </returns>
			public bool MoveNext()
			{
				if (m_version != m_collection.m_version)
					throw new System.InvalidOperationException("Collection was modified; enumeration operation may not execute.");

				++m_index;
				return (m_index < m_collection.Count) ? true : false;
			}

			/// <summary>
			///		Sets the enumerator to its initial position, before the first element in the collection.
			/// </summary>
			public void Reset()
			{
				m_index = -1;
			}
			#endregion
	
			#region Implementation (IEnumerator)
			
			object IEnumerator.Current
			{
				get { return (object)(this.Current); }
			}
			
			#endregion
		}
		#endregion
        
		#region Nested Syncronized Wrapper class
		private class SyncHotIssuesVector : HotIssuesVector
		{
			#region Implementation (data)
			private HotIssuesVector m_collection;
			private object m_root;
			#endregion

			#region Construction
			internal SyncHotIssuesVector(HotIssuesVector list) : base(Tag.Default)
			{
				m_root = list.SyncRoot;
				m_collection = list;
			}
			#endregion
            
			#region Type-safe ICollection
			public override void CopyTo(HotIssueData[] array)
			{
				lock(this.m_root)
					m_collection.CopyTo(array);
			}

			public override void CopyTo(HotIssueData[] array, int start)
			{
				lock(this.m_root)
					m_collection.CopyTo(array,start);
			}
			public override int Count
			{
				get
				{ 
					lock(this.m_root)
						return m_collection.Count;
				}
			}

			public override bool IsSynchronized
			{
				get { return true; }
			}

			public override object SyncRoot
			{
				get { return this.m_root; }
			}
			#endregion
            
			#region Type-safe IList
			public override HotIssueData this[int i]
			{
				get
				{
					lock(this.m_root)
						return m_collection[i];
				}
				set
				{
					lock(this.m_root)
						m_collection[i] = value; 
				}
			}

			public override int Add(HotIssueData x)
			{
				lock(this.m_root)
					return m_collection.Add(x);
			}
            
			public override void Clear()
			{
				lock(this.m_root)
					m_collection.Clear();
			}

			public override bool Contains(HotIssueData x)
			{
				lock(this.m_root)
					return m_collection.Contains(x);
			}

			public override int IndexOf(HotIssueData x)
			{
				lock(this.m_root)
					return m_collection.IndexOf(x);
			}

			public override void Insert(int pos, HotIssueData x)
			{
				lock(this.m_root)
					m_collection.Insert(pos,x);
			}

			public override void Remove(HotIssueData x)
			{           
				lock(this.m_root)
					m_collection.Remove(x);
			}

			public override void RemoveAt(int pos)
			{
				lock(this.m_root)
					m_collection.RemoveAt(pos);
			}
            
			public override bool IsFixedSize
			{
				get {return m_collection.IsFixedSize;}
			}

			public override bool IsReadOnly
			{
				get {return m_collection.IsReadOnly;}
			}
			#endregion

			#region Type-safe IEnumerable
			public override IHotIssuesVectorEnumerator GetEnumerator()
			{
				lock(m_root)
					return m_collection.GetEnumerator();
			}
			#endregion

			#region Public Helpers
			// (just to mimic some nice features of ArrayList)
			public override int Capacity
			{
				get
				{
					lock(this.m_root)
						return m_collection.Capacity;
				}
                
				set
				{
					lock(this.m_root)
						m_collection.Capacity = value;
				}
			}

			public override int AddRange(HotIssuesVector x)
			{
				lock(this.m_root)
					return m_collection.AddRange(x);
			}

			public override int AddRange(HotIssueData[] x)
			{
				lock(this.m_root)
					return m_collection.AddRange(x);
			}
			#endregion
		}
		#endregion

		#region Nested Read Only Wrapper class
		private class ReadOnlyHotIssuesVector : HotIssuesVector
		{
			#region Implementation (data)
			private HotIssuesVector m_collection;
			#endregion

			#region Construction
			internal ReadOnlyHotIssuesVector(HotIssuesVector list) : base(Tag.Default)
			{
				m_collection = list;
			}
			#endregion
            
			#region Type-safe ICollection
			public override void CopyTo(HotIssueData[] array)
			{
				m_collection.CopyTo(array);
			}

			public override void CopyTo(HotIssueData[] array, int start)
			{
				m_collection.CopyTo(array,start);
			}
			public override int Count
			{
				get {return m_collection.Count;}
			}

			public override bool IsSynchronized
			{
				get { return m_collection.IsSynchronized; }
			}

			public override object SyncRoot
			{
				get { return this.m_collection.SyncRoot; }
			}
			#endregion
            
			#region Type-safe IList
			public override HotIssueData this[int i]
			{
				get { return m_collection[i]; }
				set { throw new NotSupportedException("This is a Read Only Collection and can not be modified"); }
			}

			public override int Add(HotIssueData x)
			{
				throw new NotSupportedException("This is a Read Only Collection and can not be modified");
			}
            
			public override void Clear()
			{
				throw new NotSupportedException("This is a Read Only Collection and can not be modified");
			}

			public override bool Contains(HotIssueData x)
			{
				return m_collection.Contains(x);
			}

			public override int IndexOf(HotIssueData x)
			{
				return m_collection.IndexOf(x);
			}

			public override void Insert(int pos, HotIssueData x)
			{
				throw new NotSupportedException("This is a Read Only Collection and can not be modified");
			}

			public override void Remove(HotIssueData x)
			{           
				throw new NotSupportedException("This is a Read Only Collection and can not be modified");
			}

			public override void RemoveAt(int pos)
			{
				throw new NotSupportedException("This is a Read Only Collection and can not be modified");
			}
            
			public override bool IsFixedSize
			{
				get {return true;}
			}

			public override bool IsReadOnly
			{
				get {return true;}
			}
			#endregion

			#region Type-safe IEnumerable
			public override IHotIssuesVectorEnumerator GetEnumerator()
			{
				return m_collection.GetEnumerator();
			}
			#endregion

			#region Public Helpers
			// (just to mimic some nice features of ArrayList)
			public override int Capacity
			{
				get { return m_collection.Capacity; }
                
				set { throw new NotSupportedException("This is a Read Only Collection and can not be modified"); }
			}

			public override int AddRange(HotIssuesVector x)
			{
				throw new NotSupportedException("This is a Read Only Collection and can not be modified");
			}

			public override int AddRange(HotIssueData[] x)
			{
				throw new NotSupportedException("This is a Read Only Collection and can not be modified");
			}
			#endregion
		}
		#endregion
	}

	
	[Serializable]
	public class HotIssuesCategoriesVector : ICollection, IList, IEnumerable, ICloneable
	{
		#region Interfaces
		/// <summary>
		///		Supports type-safe iteration over a <see cref="HotIssuesCategoriesVector"/>.
		/// </summary>
		public interface IHotIssuesCategoriesVectorEnumerator
		{
			/// <summary>
			///		Gets the current element in the collection.
			/// </summary>
			HotIssueCategoryData Current {get;}

			/// <summary>
			///		Advances the enumerator to the next element in the collection.
			/// </summary>
			/// <exception cref="InvalidOperationException">
			///		The collection was modified after the enumerator was created.
			/// </exception>
			/// <returns>
			///		<c>true</c> if the enumerator was successfully advanced to the next element; 
			///		<c>false</c> if the enumerator has passed the end of the collection.
			/// </returns>
			bool MoveNext();

			/// <summary>
			///		Sets the enumerator to its initial position, before the first element in the collection.
			/// </summary>
			void Reset();
		}
		#endregion

		private const int DEFAULT_CAPACITY = 16;
		public HotIssueCategoryData GetByCategoryID(int ID)
		{
			for(int i=0;i<this.Count;i++)
				if(this[i].HotIssueCategoryID==ID)
					return this[i];
			return null;
		}
		#region Implementation (data)
		private HotIssueCategoryData[] m_array;
		private int m_count = 0;
		[NonSerialized]
		private int m_version = 0;
		#endregion
	
		#region Static Wrappers
		/// <summary>
		///		Creates a synchronized (thread-safe) wrapper for a 
		///     <c>HotIssuesCategoriesVector</c> instance.
		/// </summary>
		/// <returns>
		///     An <c>HotIssuesCategoriesVector</c> wrapper that is synchronized (thread-safe).
		/// </returns>
		public static HotIssuesCategoriesVector Synchronized(HotIssuesCategoriesVector list)
		{
			if(list==null)
				throw new ArgumentNullException("list");
			return new SyncHotIssuesCategoriesVector(list);
		}
        
		/// <summary>
		///		Creates a read-only wrapper for a 
		///     <c>HotIssuesCategoriesVector</c> instance.
		/// </summary>
		/// <returns>
		///     An <c>HotIssuesCategoriesVector</c> wrapper that is read-only.
		/// </returns>
		public static HotIssuesCategoriesVector ReadOnly(HotIssuesCategoriesVector list)
		{
			if(list==null)
				throw new ArgumentNullException("list");
			return new ReadOnlyHotIssuesCategoriesVector(list);
		}
		#endregion

		#region Construction
		/// <summary>
		///		Initializes a new instance of the <c>HotIssuesCategoriesVector</c> class
		///		that is empty and has the default initial capacity.
		/// </summary>
		public HotIssuesCategoriesVector()
		{
			m_array = new HotIssueCategoryData[DEFAULT_CAPACITY];
		}
		
		/// <summary>
		///		Initializes a new instance of the <c>HotIssuesCategoriesVector</c> class
		///		that has the specified initial capacity.
		/// </summary>
		/// <param name="capacity">
		///		The number of elements that the new <c>HotIssuesCategoriesVector</c> is initially capable of storing.
		///	</param>
		public HotIssuesCategoriesVector(int capacity)
		{
			m_array = new HotIssueCategoryData[capacity];
		}

		/// <summary>
		///		Initializes a new instance of the <c>HotIssuesCategoriesVector</c> class
		///		that contains elements copied from the specified <c>HotIssuesCategoriesVector</c>.
		/// </summary>
		/// <param name="c">The <c>HotIssuesCategoriesVector</c> whose elements are copied to the new collection.</param>
		public HotIssuesCategoriesVector(HotIssuesCategoriesVector c)
		{
			m_array = new HotIssueCategoryData[c.Count];
			AddRange(c);
		}

		/// <summary>
		///		Initializes a new instance of the <c>HotIssuesCategoriesVector</c> class
		///		that contains elements copied from the specified <see cref="HotIssueCategoryData"/> array.
		/// </summary>
		/// <param name="a">The <see cref="HotIssueCategoryData"/> array whose elements are copied to the new list.</param>
		public HotIssuesCategoriesVector(HotIssueCategoryData[] a)
		{
			m_array = new HotIssueCategoryData[a.Length];
			AddRange(a);
		}
		
		protected enum Tag 
		{
			Default
		}

		protected HotIssuesCategoriesVector(Tag t)
		{
			m_array = null;
		}
		#endregion
		
		#region Operations (type-safe ICollection)
		/// <summary>
		///		Gets the number of elements actually contained in the <c>HotIssuesCategoriesVector</c>.
		/// </summary>
		public virtual int Count
		{
			get { return m_count; }
		}

		/// <summary>
		///		Copies the entire <c>HotIssuesCategoriesVector</c> to a one-dimensional
		///		<see cref="HotIssueCategoryData"/> array.
		/// </summary>
		/// <param name="array">The one-dimensional <see cref="HotIssueCategoryData"/> array to copy to.</param>
		public virtual void CopyTo(HotIssueCategoryData[] array)
		{
			this.CopyTo(array, 0);
		}

		/// <summary>
		///		Copies the entire <c>HotIssuesCategoriesVector</c> to a one-dimensional
		///		<see cref="HotIssueCategoryData"/> array, starting at the specified index of the target array.
		/// </summary>
		/// <param name="array">The one-dimensional <see cref="HotIssueCategoryData"/> array to copy to.</param>
		/// <param name="start">The zero-based index in <paramref name="array"/> at which copying begins.</param>
		public virtual void CopyTo(HotIssueCategoryData[] array, int start)
		{
			if (m_count > array.GetUpperBound(0) + 1 - start)
				throw new System.ArgumentException("Destination array was not long enough.");
			
			Array.Copy(m_array, 0, array, start, m_count); 
		}

		/// <summary>
		///		Gets a value indicating whether access to the collection is synchronized (thread-safe).
		/// </summary>
		/// <returns>true if access to the ICollection is synchronized (thread-safe); otherwise, false.</returns>
		public virtual bool IsSynchronized
		{
			get { return m_array.IsSynchronized; }
		}

		/// <summary>
		///		Gets an object that can be used to synchronize access to the collection.
		/// </summary>
		public virtual object SyncRoot
		{
			get { return m_array.SyncRoot; }
		}
		#endregion
		
		#region Operations (type-safe IList)
		/// <summary>
		///		Gets or sets the <see cref="HotIssueCategoryData"/> at the specified index.
		/// </summary>
		/// <param name="index">The zero-based index of the element to get or set.</param>
		/// <exception cref="ArgumentOutOfRangeException">
		///		<para><paramref name="index"/> is less than zero</para>
		///		<para>-or-</para>
		///		<para><paramref name="index"/> is equal to or greater than <see cref="HotIssuesCategoriesVector.Count"/>.</para>
		/// </exception>
		public virtual HotIssueCategoryData this[int index]
		{
			get
			{
				ValidateIndex(index); // throws
				return m_array[index]; 
			}
			set
			{
				ValidateIndex(index); // throws
				++m_version; 
				m_array[index] = value; 
			}
		}

		/// <summary>
		///		Adds a <see cref="HotIssueCategoryData"/> to the end of the <c>HotIssuesCategoriesVector</c>.
		/// </summary>
		/// <param name="item">The <see cref="HotIssueCategoryData"/> to be added to the end of the <c>HotIssuesCategoriesVector</c>.</param>
		/// <returns>The index at which the value has been added.</returns>
		public virtual int Add(HotIssueCategoryData item)
		{
			if (m_count == m_array.Length)
				EnsureCapacity(m_count + 1);

			m_array[m_count] = item;
			m_version++;

			return m_count++;
		}
		
		/// <summary>
		///		Removes all elements from the <c>HotIssuesCategoriesVector</c>.
		/// </summary>
		public virtual void Clear()
		{
			++m_version;
			m_array = new HotIssueCategoryData[DEFAULT_CAPACITY];
			m_count = 0;
		}
		
		/// <summary>
		///		Creates a shallow copy of the <see cref="HotIssuesCategoriesVector"/>.
		/// </summary>
		public virtual object Clone()
		{
			HotIssuesCategoriesVector newColl = new HotIssuesCategoriesVector(m_count);
			Array.Copy(m_array, 0, newColl.m_array, 0, m_count);
			newColl.m_count = m_count;
			newColl.m_version = m_version;

			return newColl;
		}

		/// <summary>
		///		Determines whether a given <see cref="HotIssueCategoryData"/> is in the <c>HotIssuesCategoriesVector</c>.
		/// </summary>
		/// <param name="item">The <see cref="HotIssueCategoryData"/> to check for.</param>
		/// <returns><c>true</c> if <paramref name="item"/> is found in the <c>HotIssuesCategoriesVector</c>; otherwise, <c>false</c>.</returns>
		public virtual bool Contains(HotIssueCategoryData item)
		{
			for (int i=0; i != m_count; ++i)
				if (m_array[i].Equals(item))
					return true;
			return false;
		}

		/// <summary>
		///		Returns the zero-based index of the first occurrence of a <see cref="HotIssueCategoryData"/>
		///		in the <c>HotIssuesCategoriesVector</c>.
		/// </summary>
		/// <param name="item">The <see cref="HotIssueCategoryData"/> to locate in the <c>HotIssuesCategoriesVector</c>.</param>
		/// <returns>
		///		The zero-based index of the first occurrence of <paramref name="item"/> 
		///		in the entire <c>HotIssuesCategoriesVector</c>, if found; otherwise, -1.
		///	</returns>
		public virtual int IndexOf(HotIssueCategoryData item)
		{
			for (int i=0; i != m_count; ++i)
				if (m_array[i].Equals(item))
					return i;
			return -1;
		}

		/// <summary>
		///		Inserts an element into the <c>HotIssuesCategoriesVector</c> at the specified index.
		/// </summary>
		/// <param name="index">The zero-based index at which <paramref name="item"/> should be inserted.</param>
		/// <param name="item">The <see cref="HotIssueCategoryData"/> to insert.</param>
		/// <exception cref="ArgumentOutOfRangeException">
		///		<para><paramref name="index"/> is less than zero</para>
		///		<para>-or-</para>
		///		<para><paramref name="index"/> is equal to or greater than <see cref="HotIssuesCategoriesVector.Count"/>.</para>
		/// </exception>
		public virtual void Insert(int index, HotIssueCategoryData item)
		{
			ValidateIndex(index, true); // throws
			
			if (m_count == m_array.Length)
				EnsureCapacity(m_count + 1);

			if (index < m_count)
			{
				Array.Copy(m_array, index, m_array, index + 1, m_count - index);
			}

			m_array[index] = item;
			m_count++;
			m_version++;
		}

		/// <summary>
		///		Removes the first occurrence of a specific <see cref="HotIssueCategoryData"/> from the <c>HotIssuesCategoriesVector</c>.
		/// </summary>
		/// <param name="item">The <see cref="HotIssueCategoryData"/> to remove from the <c>HotIssuesCategoriesVector</c>.</param>
		/// <exception cref="ArgumentException">
		///		The specified <see cref="HotIssueCategoryData"/> was not found in the <c>HotIssuesCategoriesVector</c>.
		/// </exception>
		public virtual void Remove(HotIssueCategoryData item)
		{		   
			int i = IndexOf(item);
			if (i < 0)
				throw new System.ArgumentException("Cannot remove the specified item because it was not found in the specified Collection.");
			
			++m_version;
			RemoveAt(i);
		}

		/// <summary>
		///		Removes the element at the specified index of the <c>HotIssuesCategoriesVector</c>.
		/// </summary>
		/// <param name="index">The zero-based index of the element to remove.</param>
		/// <exception cref="ArgumentOutOfRangeException">
		///		<para><paramref name="index"/> is less than zero</para>
		///		<para>-or-</para>
		///		<para><paramref name="index"/> is equal to or greater than <see cref="HotIssuesCategoriesVector.Count"/>.</para>
		/// </exception>
		public virtual void RemoveAt(int index)
		{
			ValidateIndex(index); // throws
			
			m_count--;

			if (index < m_count)
			{
				Array.Copy(m_array, index + 1, m_array, index, m_count - index);
			}
			
			// We can't set the deleted entry equal to null, because it might be a value type.
			// Instead, we'll create an empty single-element array of the right type and copy it 
			// over the entry we want to erase.
			HotIssueCategoryData[] temp = new HotIssueCategoryData[1];
			Array.Copy(temp, 0, m_array, m_count, 1);
			m_version++;
		}

		/// <summary>
		///		Gets a value indicating whether the collection has a fixed size.
		/// </summary>
		/// <value>true if the collection has a fixed size; otherwise, false. The default is false</value>
		public virtual bool IsFixedSize
		{
			get { return false; }
		}

		/// <summary>
		///		gets a value indicating whether the <B>IList</B> is read-only.
		/// </summary>
		/// <value>true if the collection is read-only; otherwise, false. The default is false</value>
		public virtual bool IsReadOnly
		{
			get { return false; }
		}
		#endregion

		#region Operations (type-safe IEnumerable)
		
		/// <summary>
		///		Returns an enumerator that can iterate through the <c>HotIssuesCategoriesVector</c>.
		/// </summary>
		/// <returns>An <see cref="Enumerator"/> for the entire <c>HotIssuesCategoriesVector</c>.</returns>
		public virtual IHotIssuesCategoriesVectorEnumerator GetEnumerator()
		{
			return new Enumerator(this);
		}
		#endregion

		#region Public helpers (just to mimic some nice features of ArrayList)
		
		/// <summary>
		///		Gets or sets the number of elements the <c>HotIssuesCategoriesVector</c> can contain.
		/// </summary>
		public virtual int Capacity
		{
			get { return m_array.Length; }
			
			set
			{
				if (value < m_count)
					value = m_count;

				if (value != m_array.Length)
				{
					if (value > 0)
					{
						HotIssueCategoryData[] temp = new HotIssueCategoryData[value];
						Array.Copy(m_array, temp, m_count);
						m_array = temp;
					}
					else
					{
						m_array = new HotIssueCategoryData[DEFAULT_CAPACITY];
					}
				}
			}
		}

		/// <summary>
		///		Adds the elements of another <c>HotIssuesCategoriesVector</c> to the current <c>HotIssuesCategoriesVector</c>.
		/// </summary>
		/// <param name="x">The <c>HotIssuesCategoriesVector</c> whose elements should be added to the end of the current <c>HotIssuesCategoriesVector</c>.</param>
		/// <returns>The new <see cref="HotIssuesCategoriesVector.Count"/> of the <c>HotIssuesCategoriesVector</c>.</returns>
		public virtual int AddRange(HotIssuesCategoriesVector x)
		{
			if (m_count + x.Count >= m_array.Length)
				EnsureCapacity(m_count + x.Count);
			
			Array.Copy(x.m_array, 0, m_array, m_count, x.Count);
			m_count += x.Count;
			m_version++;

			return m_count;
		}

		/// <summary>
		///		Adds the elements of a <see cref="HotIssueCategoryData"/> array to the current <c>HotIssuesCategoriesVector</c>.
		/// </summary>
		/// <param name="x">The <see cref="HotIssueCategoryData"/> array whose elements should be added to the end of the <c>HotIssuesCategoriesVector</c>.</param>
		/// <returns>The new <see cref="HotIssuesCategoriesVector.Count"/> of the <c>HotIssuesCategoriesVector</c>.</returns>
		public virtual int AddRange(HotIssueCategoryData[] x)
		{
			if (m_count + x.Length >= m_array.Length)
				EnsureCapacity(m_count + x.Length);

			Array.Copy(x, 0, m_array, m_count, x.Length);
			m_count += x.Length;
			m_version++;

			return m_count;
		}
		
		/// <summary>
		///		Sets the capacity to the actual number of elements.
		/// </summary>
		public virtual void TrimToSize()
		{
			this.Capacity = m_count;
		}

		#endregion

		#region Implementation (helpers)

		/// <exception cref="ArgumentOutOfRangeException">
		///		<para><paramref name="index"/> is less than zero</para>
		///		<para>-or-</para>
		///		<para><paramref name="index"/> is equal to or greater than <see cref="HotIssuesCategoriesVector.Count"/>.</para>
		/// </exception>
		private void ValidateIndex(int i)
		{
			ValidateIndex(i, false);
		}

		/// <exception cref="ArgumentOutOfRangeException">
		///		<para><paramref name="index"/> is less than zero</para>
		///		<para>-or-</para>
		///		<para><paramref name="index"/> is equal to or greater than <see cref="HotIssuesCategoriesVector.Count"/>.</para>
		/// </exception>
		private void ValidateIndex(int i, bool allowEqualEnd)
		{
			int max = (allowEqualEnd)?(m_count):(m_count-1);
			if (i < 0 || i > max)
				throw new System.ArgumentOutOfRangeException("Index was out of range.  Must be non-negative and less than the size of the collection.", (object)i, "Specified argument was out of the range of valid values.");
		}

		private void EnsureCapacity(int min)
		{
			int newCapacity = ((m_array.Length == 0) ? DEFAULT_CAPACITY : m_array.Length * 2);
			if (newCapacity < min)
				newCapacity = min;

			this.Capacity = newCapacity;
		}

		#endregion
		
		#region Implementation (ICollection)

		void ICollection.CopyTo(Array array, int start)
		{
			Array.Copy(m_array, 0, array, start, m_count);
		}

		#endregion

		#region Implementation (IList)

		object IList.this[int i]
		{
			get { return (object)this[i]; }
			set { this[i] = (HotIssueCategoryData)value; }
		}

		int IList.Add(object x)
		{
			return this.Add((HotIssueCategoryData)x);
		}

		bool IList.Contains(object x)
		{
			return this.Contains((HotIssueCategoryData)x);
		}

		int IList.IndexOf(object x)
		{
			return this.IndexOf((HotIssueCategoryData)x);
		}

		void IList.Insert(int pos, object x)
		{
			this.Insert(pos, (HotIssueCategoryData)x);
		}

		void IList.Remove(object x)
		{
			this.Remove((HotIssueCategoryData)x);
		}

		void IList.RemoveAt(int pos)
		{
			this.RemoveAt(pos);
		}

		#endregion

		#region Implementation (IEnumerable)

		IEnumerator IEnumerable.GetEnumerator()
		{
			return (IEnumerator)(this.GetEnumerator());
		}

		#endregion

		#region Nested enumerator class
		/// <summary>
		///		Supports simple iteration over a <see cref="HotIssuesCategoriesVector"/>.
		/// </summary>
		private class Enumerator : IEnumerator, IHotIssuesCategoriesVectorEnumerator
		{
			#region Implementation (data)
			
			private HotIssuesCategoriesVector m_collection;
			private int m_index;
			private int m_version;
			
			#endregion
		
			#region Construction
			
			/// <summary>
			///		Initializes a new instance of the <c>Enumerator</c> class.
			/// </summary>
			/// <param name="tc"></param>
			internal Enumerator(HotIssuesCategoriesVector tc)
			{
				m_collection = tc;
				m_index = -1;
				m_version = tc.m_version;
			}
			
			#endregion
	
			#region Operations (type-safe IEnumerator)
			
			/// <summary>
			///		Gets the current element in the collection.
			/// </summary>
			public HotIssueCategoryData Current
			{
				get { return m_collection[m_index]; }
			}

			/// <summary>
			///		Advances the enumerator to the next element in the collection.
			/// </summary>
			/// <exception cref="InvalidOperationException">
			///		The collection was modified after the enumerator was created.
			/// </exception>
			/// <returns>
			///		<c>true</c> if the enumerator was successfully advanced to the next element; 
			///		<c>false</c> if the enumerator has passed the end of the collection.
			/// </returns>
			public bool MoveNext()
			{
				if (m_version != m_collection.m_version)
					throw new System.InvalidOperationException("Collection was modified; enumeration operation may not execute.");

				++m_index;
				return (m_index < m_collection.Count) ? true : false;
			}

			/// <summary>
			///		Sets the enumerator to its initial position, before the first element in the collection.
			/// </summary>
			public void Reset()
			{
				m_index = -1;
			}
			#endregion
	
			#region Implementation (IEnumerator)
			
			object IEnumerator.Current
			{
				get { return (object)(this.Current); }
			}
			
			#endregion
		}
		#endregion
        
		#region Nested Syncronized Wrapper class
		private class SyncHotIssuesCategoriesVector : HotIssuesCategoriesVector
		{
			#region Implementation (data)
			private HotIssuesCategoriesVector m_collection;
			private object m_root;
			#endregion

			#region Construction
			internal SyncHotIssuesCategoriesVector(HotIssuesCategoriesVector list) : base(Tag.Default)
			{
				m_root = list.SyncRoot;
				m_collection = list;
			}
			#endregion
            
			#region Type-safe ICollection
			public override void CopyTo(HotIssueCategoryData[] array)
			{
				lock(this.m_root)
					m_collection.CopyTo(array);
			}

			public override void CopyTo(HotIssueCategoryData[] array, int start)
			{
				lock(this.m_root)
					m_collection.CopyTo(array,start);
			}
			public override int Count
			{
				get
				{ 
					lock(this.m_root)
						return m_collection.Count;
				}
			}

			public override bool IsSynchronized
			{
				get { return true; }
			}

			public override object SyncRoot
			{
				get { return this.m_root; }
			}
			#endregion
            
			#region Type-safe IList
			public override HotIssueCategoryData this[int i]
			{
				get
				{
					lock(this.m_root)
						return m_collection[i];
				}
				set
				{
					lock(this.m_root)
						m_collection[i] = value; 
				}
			}

			public override int Add(HotIssueCategoryData x)
			{
				lock(this.m_root)
					return m_collection.Add(x);
			}
            
			public override void Clear()
			{
				lock(this.m_root)
					m_collection.Clear();
			}

			public override bool Contains(HotIssueCategoryData x)
			{
				lock(this.m_root)
					return m_collection.Contains(x);
			}

			public override int IndexOf(HotIssueCategoryData x)
			{
				lock(this.m_root)
					return m_collection.IndexOf(x);
			}

			public override void Insert(int pos, HotIssueCategoryData x)
			{
				lock(this.m_root)
					m_collection.Insert(pos,x);
			}

			public override void Remove(HotIssueCategoryData x)
			{           
				lock(this.m_root)
					m_collection.Remove(x);
			}

			public override void RemoveAt(int pos)
			{
				lock(this.m_root)
					m_collection.RemoveAt(pos);
			}
            
			public override bool IsFixedSize
			{
				get {return m_collection.IsFixedSize;}
			}

			public override bool IsReadOnly
			{
				get {return m_collection.IsReadOnly;}
			}
			#endregion

			#region Type-safe IEnumerable
			public override IHotIssuesCategoriesVectorEnumerator GetEnumerator()
			{
				lock(m_root)
					return m_collection.GetEnumerator();
			}
			#endregion

			#region Public Helpers
			// (just to mimic some nice features of ArrayList)
			public override int Capacity
			{
				get
				{
					lock(this.m_root)
						return m_collection.Capacity;
				}
                
				set
				{
					lock(this.m_root)
						m_collection.Capacity = value;
				}
			}

			public override int AddRange(HotIssuesCategoriesVector x)
			{
				lock(this.m_root)
					return m_collection.AddRange(x);
			}

			public override int AddRange(HotIssueCategoryData[] x)
			{
				lock(this.m_root)
					return m_collection.AddRange(x);
			}
			#endregion
		}
		#endregion

		#region Nested Read Only Wrapper class
		private class ReadOnlyHotIssuesCategoriesVector : HotIssuesCategoriesVector
		{
			#region Implementation (data)
			private HotIssuesCategoriesVector m_collection;
			#endregion

			#region Construction
			internal ReadOnlyHotIssuesCategoriesVector(HotIssuesCategoriesVector list) : base(Tag.Default)
			{
				m_collection = list;
			}
			#endregion
            
			#region Type-safe ICollection
			public override void CopyTo(HotIssueCategoryData[] array)
			{
				m_collection.CopyTo(array);
			}

			public override void CopyTo(HotIssueCategoryData[] array, int start)
			{
				m_collection.CopyTo(array,start);
			}
			public override int Count
			{
				get {return m_collection.Count;}
			}

			public override bool IsSynchronized
			{
				get { return m_collection.IsSynchronized; }
			}

			public override object SyncRoot
			{
				get { return this.m_collection.SyncRoot; }
			}
			#endregion
            
			#region Type-safe IList
			public override HotIssueCategoryData this[int i]
			{
				get { return m_collection[i]; }
				set { throw new NotSupportedException("This is a Read Only Collection and can not be modified"); }
			}

			public override int Add(HotIssueCategoryData x)
			{
				throw new NotSupportedException("This is a Read Only Collection and can not be modified");
			}
            
			public override void Clear()
			{
				throw new NotSupportedException("This is a Read Only Collection and can not be modified");
			}

			public override bool Contains(HotIssueCategoryData x)
			{
				return m_collection.Contains(x);
			}

			public override int IndexOf(HotIssueCategoryData x)
			{
				return m_collection.IndexOf(x);
			}

			public override void Insert(int pos, HotIssueCategoryData x)
			{
				throw new NotSupportedException("This is a Read Only Collection and can not be modified");
			}

			public override void Remove(HotIssueCategoryData x)
			{           
				throw new NotSupportedException("This is a Read Only Collection and can not be modified");
			}

			public override void RemoveAt(int pos)
			{
				throw new NotSupportedException("This is a Read Only Collection and can not be modified");
			}
            
			public override bool IsFixedSize
			{
				get {return true;}
			}

			public override bool IsReadOnly
			{
				get {return true;}
			}
			#endregion

			#region Type-safe IEnumerable
			public override IHotIssuesCategoriesVectorEnumerator GetEnumerator()
			{
				return m_collection.GetEnumerator();
			}
			#endregion

			#region Public Helpers
			// (just to mimic some nice features of ArrayList)
			public override int Capacity
			{
				get { return m_collection.Capacity; }
                
				set { throw new NotSupportedException("This is a Read Only Collection and can not be modified"); }
			}

			public override int AddRange(HotIssuesCategoriesVector x)
			{
				throw new NotSupportedException("This is a Read Only Collection and can not be modified");
			}

			public override int AddRange(HotIssueCategoryData[] x)
			{
				throw new NotSupportedException("This is a Read Only Collection and can not be modified");
			}
			#endregion
		}
		#endregion
	}

	
	[Serializable]
	public class HotIssuesStatusesVector : ICollection, IList, IEnumerable, ICloneable
	{
		#region Interfaces
		/// <summary>
		///		Supports type-safe iteration over a <see cref="HotIssuesStatusesVector"/>.
		/// </summary>
		public interface IHotIssuesStatusesVectorEnumerator
		{
			/// <summary>
			///		Gets the current element in the collection.
			/// </summary>
			HotIssueStatusData Current {get;}

			/// <summary>
			///		Advances the enumerator to the next element in the collection.
			/// </summary>
			/// <exception cref="InvalidOperationException">
			///		The collection was modified after the enumerator was created.
			/// </exception>
			/// <returns>
			///		<c>true</c> if the enumerator was successfully advanced to the next element; 
			///		<c>false</c> if the enumerator has passed the end of the collection.
			/// </returns>
			bool MoveNext();

			/// <summary>
			///		Sets the enumerator to its initial position, before the first element in the collection.
			/// </summary>
			void Reset();
		}
		#endregion

		private const int DEFAULT_CAPACITY = 16;

		#region Implementation (data)
		private HotIssueStatusData[] m_array;
		private int m_count = 0;
		[NonSerialized]
		private int m_version = 0;
		#endregion
	
		#region Static Wrappers
		/// <summary>
		///		Creates a synchronized (thread-safe) wrapper for a 
		///     <c>HotIssuesStatusesVector</c> instance.
		/// </summary>
		/// <returns>
		///     An <c>HotIssuesStatusesVector</c> wrapper that is synchronized (thread-safe).
		/// </returns>
		public static HotIssuesStatusesVector Synchronized(HotIssuesStatusesVector list)
		{
			if(list==null)
				throw new ArgumentNullException("list");
			return new SyncHotIssuesStatusesVector(list);
		}
        
		/// <summary>
		///		Creates a read-only wrapper for a 
		///     <c>HotIssuesStatusesVector</c> instance.
		/// </summary>
		/// <returns>
		///     An <c>HotIssuesStatusesVector</c> wrapper that is read-only.
		/// </returns>
		public static HotIssuesStatusesVector ReadOnly(HotIssuesStatusesVector list)
		{
			if(list==null)
				throw new ArgumentNullException("list");
			return new ReadOnlyHotIssuesStatusesVector(list);
		}
		#endregion

		#region Construction
		/// <summary>
		///		Initializes a new instance of the <c>HotIssuesStatusesVector</c> class
		///		that is empty and has the default initial capacity.
		/// </summary>
		public HotIssuesStatusesVector()
		{
			m_array = new HotIssueStatusData[DEFAULT_CAPACITY];
		}
		
		/// <summary>
		///		Initializes a new instance of the <c>HotIssuesStatusesVector</c> class
		///		that has the specified initial capacity.
		/// </summary>
		/// <param name="capacity">
		///		The number of elements that the new <c>HotIssuesStatusesVector</c> is initially capable of storing.
		///	</param>
		public HotIssuesStatusesVector(int capacity)
		{
			m_array = new HotIssueStatusData[capacity];
		}

		/// <summary>
		///		Initializes a new instance of the <c>HotIssuesStatusesVector</c> class
		///		that contains elements copied from the specified <c>HotIssuesStatusesVector</c>.
		/// </summary>
		/// <param name="c">The <c>HotIssuesStatusesVector</c> whose elements are copied to the new collection.</param>
		public HotIssuesStatusesVector(HotIssuesStatusesVector c)
		{
			m_array = new HotIssueStatusData[c.Count];
			AddRange(c);
		}

		/// <summary>
		///		Initializes a new instance of the <c>HotIssuesStatusesVector</c> class
		///		that contains elements copied from the specified <see cref="HotIssueStatusData"/> array.
		/// </summary>
		/// <param name="a">The <see cref="HotIssueStatusData"/> array whose elements are copied to the new list.</param>
		public HotIssuesStatusesVector(HotIssueStatusData[] a)
		{
			m_array = new HotIssueStatusData[a.Length];
			AddRange(a);
		}
		
		protected enum Tag 
		{
			Default
		}

		protected HotIssuesStatusesVector(Tag t)
		{
			m_array = null;
		}
		#endregion
		
		#region Operations (type-safe ICollection)
		/// <summary>
		///		Gets the number of elements actually contained in the <c>HotIssuesStatusesVector</c>.
		/// </summary>
		public virtual int Count
		{
			get { return m_count; }
		}

		/// <summary>
		///		Copies the entire <c>HotIssuesStatusesVector</c> to a one-dimensional
		///		<see cref="HotIssueStatusData"/> array.
		/// </summary>
		/// <param name="array">The one-dimensional <see cref="HotIssueStatusData"/> array to copy to.</param>
		public virtual void CopyTo(HotIssueStatusData[] array)
		{
			this.CopyTo(array, 0);
		}

		/// <summary>
		///		Copies the entire <c>HotIssuesStatusesVector</c> to a one-dimensional
		///		<see cref="HotIssueStatusData"/> array, starting at the specified index of the target array.
		/// </summary>
		/// <param name="array">The one-dimensional <see cref="HotIssueStatusData"/> array to copy to.</param>
		/// <param name="start">The zero-based index in <paramref name="array"/> at which copying begins.</param>
		public virtual void CopyTo(HotIssueStatusData[] array, int start)
		{
			if (m_count > array.GetUpperBound(0) + 1 - start)
				throw new System.ArgumentException("Destination array was not long enough.");
			
			Array.Copy(m_array, 0, array, start, m_count); 
		}

		/// <summary>
		///		Gets a value indicating whether access to the collection is synchronized (thread-safe).
		/// </summary>
		/// <returns>true if access to the ICollection is synchronized (thread-safe); otherwise, false.</returns>
		public virtual bool IsSynchronized
		{
			get { return m_array.IsSynchronized; }
		}

		/// <summary>
		///		Gets an object that can be used to synchronize access to the collection.
		/// </summary>
		public virtual object SyncRoot
		{
			get { return m_array.SyncRoot; }
		}
		#endregion
		
		#region Operations (type-safe IList)
		/// <summary>
		///		Gets or sets the <see cref="HotIssueStatusData"/> at the specified index.
		/// </summary>
		/// <param name="index">The zero-based index of the element to get or set.</param>
		/// <exception cref="ArgumentOutOfRangeException">
		///		<para><paramref name="index"/> is less than zero</para>
		///		<para>-or-</para>
		///		<para><paramref name="index"/> is equal to or greater than <see cref="HotIssuesStatusesVector.Count"/>.</para>
		/// </exception>
		public virtual HotIssueStatusData this[int index]
		{
			get
			{
				ValidateIndex(index); // throws
				return m_array[index]; 
			}
			set
			{
				ValidateIndex(index); // throws
				++m_version; 
				m_array[index] = value; 
			}
		}

		/// <summary>
		///		Adds a <see cref="HotIssueStatusData"/> to the end of the <c>HotIssuesStatusesVector</c>.
		/// </summary>
		/// <param name="item">The <see cref="HotIssueStatusData"/> to be added to the end of the <c>HotIssuesStatusesVector</c>.</param>
		/// <returns>The index at which the value has been added.</returns>
		public virtual int Add(HotIssueStatusData item)
		{
			if (m_count == m_array.Length)
				EnsureCapacity(m_count + 1);

			m_array[m_count] = item;
			m_version++;

			return m_count++;
		}
		
		/// <summary>
		///		Removes all elements from the <c>HotIssuesStatusesVector</c>.
		/// </summary>
		public virtual void Clear()
		{
			++m_version;
			m_array = new HotIssueStatusData[DEFAULT_CAPACITY];
			m_count = 0;
		}
		
		/// <summary>
		///		Creates a shallow copy of the <see cref="HotIssuesStatusesVector"/>.
		/// </summary>
		public virtual object Clone()
		{
			HotIssuesStatusesVector newColl = new HotIssuesStatusesVector(m_count);
			Array.Copy(m_array, 0, newColl.m_array, 0, m_count);
			newColl.m_count = m_count;
			newColl.m_version = m_version;

			return newColl;
		}

		/// <summary>
		///		Determines whether a given <see cref="HotIssueStatusData"/> is in the <c>HotIssuesStatusesVector</c>.
		/// </summary>
		/// <param name="item">The <see cref="HotIssueStatusData"/> to check for.</param>
		/// <returns><c>true</c> if <paramref name="item"/> is found in the <c>HotIssuesStatusesVector</c>; otherwise, <c>false</c>.</returns>
		public virtual bool Contains(HotIssueStatusData item)
		{
			for (int i=0; i != m_count; ++i)
				if (m_array[i].Equals(item))
					return true;
			return false;
		}

		/// <summary>
		///		Returns the zero-based index of the first occurrence of a <see cref="HotIssueStatusData"/>
		///		in the <c>HotIssuesStatusesVector</c>.
		/// </summary>
		/// <param name="item">The <see cref="HotIssueStatusData"/> to locate in the <c>HotIssuesStatusesVector</c>.</param>
		/// <returns>
		///		The zero-based index of the first occurrence of <paramref name="item"/> 
		///		in the entire <c>HotIssuesStatusesVector</c>, if found; otherwise, -1.
		///	</returns>
		public virtual int IndexOf(HotIssueStatusData item)
		{
			for (int i=0; i != m_count; ++i)
				if (m_array[i].Equals(item))
					return i;
			return -1;
		}

		/// <summary>
		///		Inserts an element into the <c>HotIssuesStatusesVector</c> at the specified index.
		/// </summary>
		/// <param name="index">The zero-based index at which <paramref name="item"/> should be inserted.</param>
		/// <param name="item">The <see cref="HotIssueStatusData"/> to insert.</param>
		/// <exception cref="ArgumentOutOfRangeException">
		///		<para><paramref name="index"/> is less than zero</para>
		///		<para>-or-</para>
		///		<para><paramref name="index"/> is equal to or greater than <see cref="HotIssuesStatusesVector.Count"/>.</para>
		/// </exception>
		public virtual void Insert(int index, HotIssueStatusData item)
		{
			ValidateIndex(index, true); // throws
			
			if (m_count == m_array.Length)
				EnsureCapacity(m_count + 1);

			if (index < m_count)
			{
				Array.Copy(m_array, index, m_array, index + 1, m_count - index);
			}

			m_array[index] = item;
			m_count++;
			m_version++;
		}

		/// <summary>
		///		Removes the first occurrence of a specific <see cref="HotIssueStatusData"/> from the <c>HotIssuesStatusesVector</c>.
		/// </summary>
		/// <param name="item">The <see cref="HotIssueStatusData"/> to remove from the <c>HotIssuesStatusesVector</c>.</param>
		/// <exception cref="ArgumentException">
		///		The specified <see cref="HotIssueStatusData"/> was not found in the <c>HotIssuesStatusesVector</c>.
		/// </exception>
		public virtual void Remove(HotIssueStatusData item)
		{		   
			int i = IndexOf(item);
			if (i < 0)
				throw new System.ArgumentException("Cannot remove the specified item because it was not found in the specified Collection.");
			
			++m_version;
			RemoveAt(i);
		}

		/// <summary>
		///		Removes the element at the specified index of the <c>HotIssuesStatusesVector</c>.
		/// </summary>
		/// <param name="index">The zero-based index of the element to remove.</param>
		/// <exception cref="ArgumentOutOfRangeException">
		///		<para><paramref name="index"/> is less than zero</para>
		///		<para>-or-</para>
		///		<para><paramref name="index"/> is equal to or greater than <see cref="HotIssuesStatusesVector.Count"/>.</para>
		/// </exception>
		public virtual void RemoveAt(int index)
		{
			ValidateIndex(index); // throws
			
			m_count--;

			if (index < m_count)
			{
				Array.Copy(m_array, index + 1, m_array, index, m_count - index);
			}
			
			// We can't set the deleted entry equal to null, because it might be a value type.
			// Instead, we'll create an empty single-element array of the right type and copy it 
			// over the entry we want to erase.
			HotIssueStatusData[] temp = new HotIssueStatusData[1];
			Array.Copy(temp, 0, m_array, m_count, 1);
			m_version++;
		}

		/// <summary>
		///		Gets a value indicating whether the collection has a fixed size.
		/// </summary>
		/// <value>true if the collection has a fixed size; otherwise, false. The default is false</value>
		public virtual bool IsFixedSize
		{
			get { return false; }
		}

		/// <summary>
		///		gets a value indicating whether the <B>IList</B> is read-only.
		/// </summary>
		/// <value>true if the collection is read-only; otherwise, false. The default is false</value>
		public virtual bool IsReadOnly
		{
			get { return false; }
		}
		#endregion

		#region Operations (type-safe IEnumerable)
		
		/// <summary>
		///		Returns an enumerator that can iterate through the <c>HotIssuesStatusesVector</c>.
		/// </summary>
		/// <returns>An <see cref="Enumerator"/> for the entire <c>HotIssuesStatusesVector</c>.</returns>
		public virtual IHotIssuesStatusesVectorEnumerator GetEnumerator()
		{
			return new Enumerator(this);
		}
		#endregion

		#region Public helpers (just to mimic some nice features of ArrayList)
		
		/// <summary>
		///		Gets or sets the number of elements the <c>HotIssuesStatusesVector</c> can contain.
		/// </summary>
		public virtual int Capacity
		{
			get { return m_array.Length; }
			
			set
			{
				if (value < m_count)
					value = m_count;

				if (value != m_array.Length)
				{
					if (value > 0)
					{
						HotIssueStatusData[] temp = new HotIssueStatusData[value];
						Array.Copy(m_array, temp, m_count);
						m_array = temp;
					}
					else
					{
						m_array = new HotIssueStatusData[DEFAULT_CAPACITY];
					}
				}
			}
		}

		/// <summary>
		///		Adds the elements of another <c>HotIssuesStatusesVector</c> to the current <c>HotIssuesStatusesVector</c>.
		/// </summary>
		/// <param name="x">The <c>HotIssuesStatusesVector</c> whose elements should be added to the end of the current <c>HotIssuesStatusesVector</c>.</param>
		/// <returns>The new <see cref="HotIssuesStatusesVector.Count"/> of the <c>HotIssuesStatusesVector</c>.</returns>
		public virtual int AddRange(HotIssuesStatusesVector x)
		{
			if (m_count + x.Count >= m_array.Length)
				EnsureCapacity(m_count + x.Count);
			
			Array.Copy(x.m_array, 0, m_array, m_count, x.Count);
			m_count += x.Count;
			m_version++;

			return m_count;
		}

		/// <summary>
		///		Adds the elements of a <see cref="HotIssueStatusData"/> array to the current <c>HotIssuesStatusesVector</c>.
		/// </summary>
		/// <param name="x">The <see cref="HotIssueStatusData"/> array whose elements should be added to the end of the <c>HotIssuesStatusesVector</c>.</param>
		/// <returns>The new <see cref="HotIssuesStatusesVector.Count"/> of the <c>HotIssuesStatusesVector</c>.</returns>
		public virtual int AddRange(HotIssueStatusData[] x)
		{
			if (m_count + x.Length >= m_array.Length)
				EnsureCapacity(m_count + x.Length);

			Array.Copy(x, 0, m_array, m_count, x.Length);
			m_count += x.Length;
			m_version++;

			return m_count;
		}
		
		/// <summary>
		///		Sets the capacity to the actual number of elements.
		/// </summary>
		public virtual void TrimToSize()
		{
			this.Capacity = m_count;
		}

		#endregion

		#region Implementation (helpers)

		/// <exception cref="ArgumentOutOfRangeException">
		///		<para><paramref name="index"/> is less than zero</para>
		///		<para>-or-</para>
		///		<para><paramref name="index"/> is equal to or greater than <see cref="HotIssuesStatusesVector.Count"/>.</para>
		/// </exception>
		private void ValidateIndex(int i)
		{
			ValidateIndex(i, false);
		}

		/// <exception cref="ArgumentOutOfRangeException">
		///		<para><paramref name="index"/> is less than zero</para>
		///		<para>-or-</para>
		///		<para><paramref name="index"/> is equal to or greater than <see cref="HotIssuesStatusesVector.Count"/>.</para>
		/// </exception>
		private void ValidateIndex(int i, bool allowEqualEnd)
		{
			int max = (allowEqualEnd)?(m_count):(m_count-1);
			if (i < 0 || i > max)
				throw new System.ArgumentOutOfRangeException("Index was out of range.  Must be non-negative and less than the size of the collection.", (object)i, "Specified argument was out of the range of valid values.");
		}

		private void EnsureCapacity(int min)
		{
			int newCapacity = ((m_array.Length == 0) ? DEFAULT_CAPACITY : m_array.Length * 2);
			if (newCapacity < min)
				newCapacity = min;

			this.Capacity = newCapacity;
		}

		#endregion
		
		#region Implementation (ICollection)

		void ICollection.CopyTo(Array array, int start)
		{
			Array.Copy(m_array, 0, array, start, m_count);
		}

		#endregion

		#region Implementation (IList)

		object IList.this[int i]
		{
			get { return (object)this[i]; }
			set { this[i] = (HotIssueStatusData)value; }
		}

		int IList.Add(object x)
		{
			return this.Add((HotIssueStatusData)x);
		}

		bool IList.Contains(object x)
		{
			return this.Contains((HotIssueStatusData)x);
		}

		int IList.IndexOf(object x)
		{
			return this.IndexOf((HotIssueStatusData)x);
		}

		void IList.Insert(int pos, object x)
		{
			this.Insert(pos, (HotIssueStatusData)x);
		}

		void IList.Remove(object x)
		{
			this.Remove((HotIssueStatusData)x);
		}

		void IList.RemoveAt(int pos)
		{
			this.RemoveAt(pos);
		}

		#endregion

		#region Implementation (IEnumerable)

		IEnumerator IEnumerable.GetEnumerator()
		{
			return (IEnumerator)(this.GetEnumerator());
		}

		#endregion

		#region Nested enumerator class
		/// <summary>
		///		Supports simple iteration over a <see cref="HotIssuesStatusesVector"/>.
		/// </summary>
		private class Enumerator : IEnumerator, IHotIssuesStatusesVectorEnumerator
		{
			#region Implementation (data)
			
			private HotIssuesStatusesVector m_collection;
			private int m_index;
			private int m_version;
			
			#endregion
		
			#region Construction
			
			/// <summary>
			///		Initializes a new instance of the <c>Enumerator</c> class.
			/// </summary>
			/// <param name="tc"></param>
			internal Enumerator(HotIssuesStatusesVector tc)
			{
				m_collection = tc;
				m_index = -1;
				m_version = tc.m_version;
			}
			
			#endregion
	
			#region Operations (type-safe IEnumerator)
			
			/// <summary>
			///		Gets the current element in the collection.
			/// </summary>
			public HotIssueStatusData Current
			{
				get { return m_collection[m_index]; }
			}

			/// <summary>
			///		Advances the enumerator to the next element in the collection.
			/// </summary>
			/// <exception cref="InvalidOperationException">
			///		The collection was modified after the enumerator was created.
			/// </exception>
			/// <returns>
			///		<c>true</c> if the enumerator was successfully advanced to the next element; 
			///		<c>false</c> if the enumerator has passed the end of the collection.
			/// </returns>
			public bool MoveNext()
			{
				if (m_version != m_collection.m_version)
					throw new System.InvalidOperationException("Collection was modified; enumeration operation may not execute.");

				++m_index;
				return (m_index < m_collection.Count) ? true : false;
			}

			/// <summary>
			///		Sets the enumerator to its initial position, before the first element in the collection.
			/// </summary>
			public void Reset()
			{
				m_index = -1;
			}
			#endregion
	
			#region Implementation (IEnumerator)
			
			object IEnumerator.Current
			{
				get { return (object)(this.Current); }
			}
			
			#endregion
		}
		#endregion
        
		#region Nested Syncronized Wrapper class
		private class SyncHotIssuesStatusesVector : HotIssuesStatusesVector
		{
			#region Implementation (data)
			private HotIssuesStatusesVector m_collection;
			private object m_root;
			#endregion

			#region Construction
			internal SyncHotIssuesStatusesVector(HotIssuesStatusesVector list) : base(Tag.Default)
			{
				m_root = list.SyncRoot;
				m_collection = list;
			}
			#endregion
            
			#region Type-safe ICollection
			public override void CopyTo(HotIssueStatusData[] array)
			{
				lock(this.m_root)
					m_collection.CopyTo(array);
			}

			public override void CopyTo(HotIssueStatusData[] array, int start)
			{
				lock(this.m_root)
					m_collection.CopyTo(array,start);
			}
			public override int Count
			{
				get
				{ 
					lock(this.m_root)
						return m_collection.Count;
				}
			}

			public override bool IsSynchronized
			{
				get { return true; }
			}

			public override object SyncRoot
			{
				get { return this.m_root; }
			}
			#endregion
            
			#region Type-safe IList
			public override HotIssueStatusData this[int i]
			{
				get
				{
					lock(this.m_root)
						return m_collection[i];
				}
				set
				{
					lock(this.m_root)
						m_collection[i] = value; 
				}
			}

			public override int Add(HotIssueStatusData x)
			{
				lock(this.m_root)
					return m_collection.Add(x);
			}
            
			public override void Clear()
			{
				lock(this.m_root)
					m_collection.Clear();
			}

			public override bool Contains(HotIssueStatusData x)
			{
				lock(this.m_root)
					return m_collection.Contains(x);
			}

			public override int IndexOf(HotIssueStatusData x)
			{
				lock(this.m_root)
					return m_collection.IndexOf(x);
			}

			public override void Insert(int pos, HotIssueStatusData x)
			{
				lock(this.m_root)
					m_collection.Insert(pos,x);
			}

			public override void Remove(HotIssueStatusData x)
			{           
				lock(this.m_root)
					m_collection.Remove(x);
			}

			public override void RemoveAt(int pos)
			{
				lock(this.m_root)
					m_collection.RemoveAt(pos);
			}
            
			public override bool IsFixedSize
			{
				get {return m_collection.IsFixedSize;}
			}

			public override bool IsReadOnly
			{
				get {return m_collection.IsReadOnly;}
			}
			#endregion

			#region Type-safe IEnumerable
			public override IHotIssuesStatusesVectorEnumerator GetEnumerator()
			{
				lock(m_root)
					return m_collection.GetEnumerator();
			}
			#endregion

			#region Public Helpers
			// (just to mimic some nice features of ArrayList)
			public override int Capacity
			{
				get
				{
					lock(this.m_root)
						return m_collection.Capacity;
				}
                
				set
				{
					lock(this.m_root)
						m_collection.Capacity = value;
				}
			}

			public override int AddRange(HotIssuesStatusesVector x)
			{
				lock(this.m_root)
					return m_collection.AddRange(x);
			}

			public override int AddRange(HotIssueStatusData[] x)
			{
				lock(this.m_root)
					return m_collection.AddRange(x);
			}
			#endregion
		}
		#endregion

		#region Nested Read Only Wrapper class
		private class ReadOnlyHotIssuesStatusesVector : HotIssuesStatusesVector
		{
			#region Implementation (data)
			private HotIssuesStatusesVector m_collection;
			#endregion

			#region Construction
			internal ReadOnlyHotIssuesStatusesVector(HotIssuesStatusesVector list) : base(Tag.Default)
			{
				m_collection = list;
			}
			#endregion
            
			#region Type-safe ICollection
			public override void CopyTo(HotIssueStatusData[] array)
			{
				m_collection.CopyTo(array);
			}

			public override void CopyTo(HotIssueStatusData[] array, int start)
			{
				m_collection.CopyTo(array,start);
			}
			public override int Count
			{
				get {return m_collection.Count;}
			}

			public override bool IsSynchronized
			{
				get { return m_collection.IsSynchronized; }
			}

			public override object SyncRoot
			{
				get { return this.m_collection.SyncRoot; }
			}
			#endregion
            
			#region Type-safe IList
			public override HotIssueStatusData this[int i]
			{
				get { return m_collection[i]; }
				set { throw new NotSupportedException("This is a Read Only Collection and can not be modified"); }
			}

			public override int Add(HotIssueStatusData x)
			{
				throw new NotSupportedException("This is a Read Only Collection and can not be modified");
			}
            
			public override void Clear()
			{
				throw new NotSupportedException("This is a Read Only Collection and can not be modified");
			}

			public override bool Contains(HotIssueStatusData x)
			{
				return m_collection.Contains(x);
			}

			public override int IndexOf(HotIssueStatusData x)
			{
				return m_collection.IndexOf(x);
			}

			public override void Insert(int pos, HotIssueStatusData x)
			{
				throw new NotSupportedException("This is a Read Only Collection and can not be modified");
			}

			public override void Remove(HotIssueStatusData x)
			{           
				throw new NotSupportedException("This is a Read Only Collection and can not be modified");
			}

			public override void RemoveAt(int pos)
			{
				throw new NotSupportedException("This is a Read Only Collection and can not be modified");
			}
            
			public override bool IsFixedSize
			{
				get {return true;}
			}

			public override bool IsReadOnly
			{
				get {return true;}
			}
			#endregion

			#region Type-safe IEnumerable
			public override IHotIssuesStatusesVectorEnumerator GetEnumerator()
			{
				return m_collection.GetEnumerator();
			}
			#endregion

			#region Public Helpers
			// (just to mimic some nice features of ArrayList)
			public override int Capacity
			{
				get { return m_collection.Capacity; }
                
				set { throw new NotSupportedException("This is a Read Only Collection and can not be modified"); }
			}

			public override int AddRange(HotIssuesStatusesVector x)
			{
				throw new NotSupportedException("This is a Read Only Collection and can not be modified");
			}

			public override int AddRange(HotIssueStatusData[] x)
			{
				throw new NotSupportedException("This is a Read Only Collection and can not be modified");
			}
			#endregion
		}
		#endregion
	}

	
	[Serializable]
	public class HotIssuesPrioritiesVector : ICollection, IList, IEnumerable, ICloneable
	{
		#region Interfaces
		/// <summary>
		///		Supports type-safe iteration over a <see cref="HotIssuesPrioritiesVector"/>.
		/// </summary>
		public interface IHotIssuesPrioritiesVectorEnumerator
		{
			/// <summary>
			///		Gets the current element in the collection.
			/// </summary>
			HotIssuePriorityData Current {get;}

			/// <summary>
			///		Advances the enumerator to the next element in the collection.
			/// </summary>
			/// <exception cref="InvalidOperationException">
			///		The collection was modified after the enumerator was created.
			/// </exception>
			/// <returns>
			///		<c>true</c> if the enumerator was successfully advanced to the next element; 
			///		<c>false</c> if the enumerator has passed the end of the collection.
			/// </returns>
			bool MoveNext();

			/// <summary>
			///		Sets the enumerator to its initial position, before the first element in the collection.
			/// </summary>
			void Reset();
		}
		#endregion

		private const int DEFAULT_CAPACITY = 16;

		#region Implementation (data)
		private HotIssuePriorityData[] m_array;
		private int m_count = 0;
		[NonSerialized]
		private int m_version = 0;
		#endregion
	
		#region Static Wrappers
		/// <summary>
		///		Creates a synchronized (thread-safe) wrapper for a 
		///     <c>HotIssuesPrioritiesVector</c> instance.
		/// </summary>
		/// <returns>
		///     An <c>HotIssuesPrioritiesVector</c> wrapper that is synchronized (thread-safe).
		/// </returns>
		public static HotIssuesPrioritiesVector Synchronized(HotIssuesPrioritiesVector list)
		{
			if(list==null)
				throw new ArgumentNullException("list");
			return new SyncHotIssuesPrioritiesVector(list);
		}
        
		/// <summary>
		///		Creates a read-only wrapper for a 
		///     <c>HotIssuesPrioritiesVector</c> instance.
		/// </summary>
		/// <returns>
		///     An <c>HotIssuesPrioritiesVector</c> wrapper that is read-only.
		/// </returns>
		public static HotIssuesPrioritiesVector ReadOnly(HotIssuesPrioritiesVector list)
		{
			if(list==null)
				throw new ArgumentNullException("list");
			return new ReadOnlyHotIssuesPrioritiesVector(list);
		}
		#endregion

		#region Construction
		/// <summary>
		///		Initializes a new instance of the <c>HotIssuesPrioritiesVector</c> class
		///		that is empty and has the default initial capacity.
		/// </summary>
		public HotIssuesPrioritiesVector()
		{
			m_array = new HotIssuePriorityData[DEFAULT_CAPACITY];
		}
		
		/// <summary>
		///		Initializes a new instance of the <c>HotIssuesPrioritiesVector</c> class
		///		that has the specified initial capacity.
		/// </summary>
		/// <param name="capacity">
		///		The number of elements that the new <c>HotIssuesPrioritiesVector</c> is initially capable of storing.
		///	</param>
		public HotIssuesPrioritiesVector(int capacity)
		{
			m_array = new HotIssuePriorityData[capacity];
		}

		/// <summary>
		///		Initializes a new instance of the <c>HotIssuesPrioritiesVector</c> class
		///		that contains elements copied from the specified <c>HotIssuesPrioritiesVector</c>.
		/// </summary>
		/// <param name="c">The <c>HotIssuesPrioritiesVector</c> whose elements are copied to the new collection.</param>
		public HotIssuesPrioritiesVector(HotIssuesPrioritiesVector c)
		{
			m_array = new HotIssuePriorityData[c.Count];
			AddRange(c);
		}

		/// <summary>
		///		Initializes a new instance of the <c>HotIssuesPrioritiesVector</c> class
		///		that contains elements copied from the specified <see cref="HotIssuePriorityData"/> array.
		/// </summary>
		/// <param name="a">The <see cref="HotIssuePriorityData"/> array whose elements are copied to the new list.</param>
		public HotIssuesPrioritiesVector(HotIssuePriorityData[] a)
		{
			m_array = new HotIssuePriorityData[a.Length];
			AddRange(a);
		}
		
		protected enum Tag 
		{
			Default
		}

		protected HotIssuesPrioritiesVector(Tag t)
		{
			m_array = null;
		}
		#endregion
		
		#region Operations (type-safe ICollection)
		/// <summary>
		///		Gets the number of elements actually contained in the <c>HotIssuesPrioritiesVector</c>.
		/// </summary>
		public virtual int Count
		{
			get { return m_count; }
		}

		/// <summary>
		///		Copies the entire <c>HotIssuesPrioritiesVector</c> to a one-dimensional
		///		<see cref="HotIssuePriorityData"/> array.
		/// </summary>
		/// <param name="array">The one-dimensional <see cref="HotIssuePriorityData"/> array to copy to.</param>
		public virtual void CopyTo(HotIssuePriorityData[] array)
		{
			this.CopyTo(array, 0);
		}

		/// <summary>
		///		Copies the entire <c>HotIssuesPrioritiesVector</c> to a one-dimensional
		///		<see cref="HotIssuePriorityData"/> array, starting at the specified index of the target array.
		/// </summary>
		/// <param name="array">The one-dimensional <see cref="HotIssuePriorityData"/> array to copy to.</param>
		/// <param name="start">The zero-based index in <paramref name="array"/> at which copying begins.</param>
		public virtual void CopyTo(HotIssuePriorityData[] array, int start)
		{
			if (m_count > array.GetUpperBound(0) + 1 - start)
				throw new System.ArgumentException("Destination array was not long enough.");
			
			Array.Copy(m_array, 0, array, start, m_count); 
		}

		/// <summary>
		///		Gets a value indicating whether access to the collection is synchronized (thread-safe).
		/// </summary>
		/// <returns>true if access to the ICollection is synchronized (thread-safe); otherwise, false.</returns>
		public virtual bool IsSynchronized
		{
			get { return m_array.IsSynchronized; }
		}

		/// <summary>
		///		Gets an object that can be used to synchronize access to the collection.
		/// </summary>
		public virtual object SyncRoot
		{
			get { return m_array.SyncRoot; }
		}
		#endregion
		
		#region Operations (type-safe IList)
		/// <summary>
		///		Gets or sets the <see cref="HotIssuePriorityData"/> at the specified index.
		/// </summary>
		/// <param name="index">The zero-based index of the element to get or set.</param>
		/// <exception cref="ArgumentOutOfRangeException">
		///		<para><paramref name="index"/> is less than zero</para>
		///		<para>-or-</para>
		///		<para><paramref name="index"/> is equal to or greater than <see cref="HotIssuesPrioritiesVector.Count"/>.</para>
		/// </exception>
		public virtual HotIssuePriorityData this[int index]
		{
			get
			{
				ValidateIndex(index); // throws
				return m_array[index]; 
			}
			set
			{
				ValidateIndex(index); // throws
				++m_version; 
				m_array[index] = value; 
			}
		}

		/// <summary>
		///		Adds a <see cref="HotIssuePriorityData"/> to the end of the <c>HotIssuesPrioritiesVector</c>.
		/// </summary>
		/// <param name="item">The <see cref="HotIssuePriorityData"/> to be added to the end of the <c>HotIssuesPrioritiesVector</c>.</param>
		/// <returns>The index at which the value has been added.</returns>
		public virtual int Add(HotIssuePriorityData item)
		{
			if (m_count == m_array.Length)
				EnsureCapacity(m_count + 1);

			m_array[m_count] = item;
			m_version++;

			return m_count++;
		}
		
		/// <summary>
		///		Removes all elements from the <c>HotIssuesPrioritiesVector</c>.
		/// </summary>
		public virtual void Clear()
		{
			++m_version;
			m_array = new HotIssuePriorityData[DEFAULT_CAPACITY];
			m_count = 0;
		}
		
		/// <summary>
		///		Creates a shallow copy of the <see cref="HotIssuesPrioritiesVector"/>.
		/// </summary>
		public virtual object Clone()
		{
			HotIssuesPrioritiesVector newColl = new HotIssuesPrioritiesVector(m_count);
			Array.Copy(m_array, 0, newColl.m_array, 0, m_count);
			newColl.m_count = m_count;
			newColl.m_version = m_version;

			return newColl;
		}

		/// <summary>
		///		Determines whether a given <see cref="HotIssuePriorityData"/> is in the <c>HotIssuesPrioritiesVector</c>.
		/// </summary>
		/// <param name="item">The <see cref="HotIssuePriorityData"/> to check for.</param>
		/// <returns><c>true</c> if <paramref name="item"/> is found in the <c>HotIssuesPrioritiesVector</c>; otherwise, <c>false</c>.</returns>
		public virtual bool Contains(HotIssuePriorityData item)
		{
			for (int i=0; i != m_count; ++i)
				if (m_array[i].Equals(item))
					return true;
			return false;
		}

		/// <summary>
		///		Returns the zero-based index of the first occurrence of a <see cref="HotIssuePriorityData"/>
		///		in the <c>HotIssuesPrioritiesVector</c>.
		/// </summary>
		/// <param name="item">The <see cref="HotIssuePriorityData"/> to locate in the <c>HotIssuesPrioritiesVector</c>.</param>
		/// <returns>
		///		The zero-based index of the first occurrence of <paramref name="item"/> 
		///		in the entire <c>HotIssuesPrioritiesVector</c>, if found; otherwise, -1.
		///	</returns>
		public virtual int IndexOf(HotIssuePriorityData item)
		{
			for (int i=0; i != m_count; ++i)
				if (m_array[i].Equals(item))
					return i;
			return -1;
		}

		/// <summary>
		///		Inserts an element into the <c>HotIssuesPrioritiesVector</c> at the specified index.
		/// </summary>
		/// <param name="index">The zero-based index at which <paramref name="item"/> should be inserted.</param>
		/// <param name="item">The <see cref="HotIssuePriorityData"/> to insert.</param>
		/// <exception cref="ArgumentOutOfRangeException">
		///		<para><paramref name="index"/> is less than zero</para>
		///		<para>-or-</para>
		///		<para><paramref name="index"/> is equal to or greater than <see cref="HotIssuesPrioritiesVector.Count"/>.</para>
		/// </exception>
		public virtual void Insert(int index, HotIssuePriorityData item)
		{
			ValidateIndex(index, true); // throws
			
			if (m_count == m_array.Length)
				EnsureCapacity(m_count + 1);

			if (index < m_count)
			{
				Array.Copy(m_array, index, m_array, index + 1, m_count - index);
			}

			m_array[index] = item;
			m_count++;
			m_version++;
		}

		/// <summary>
		///		Removes the first occurrence of a specific <see cref="HotIssuePriorityData"/> from the <c>HotIssuesPrioritiesVector</c>.
		/// </summary>
		/// <param name="item">The <see cref="HotIssuePriorityData"/> to remove from the <c>HotIssuesPrioritiesVector</c>.</param>
		/// <exception cref="ArgumentException">
		///		The specified <see cref="HotIssuePriorityData"/> was not found in the <c>HotIssuesPrioritiesVector</c>.
		/// </exception>
		public virtual void Remove(HotIssuePriorityData item)
		{		   
			int i = IndexOf(item);
			if (i < 0)
				throw new System.ArgumentException("Cannot remove the specified item because it was not found in the specified Collection.");
			
			++m_version;
			RemoveAt(i);
		}

		/// <summary>
		///		Removes the element at the specified index of the <c>HotIssuesPrioritiesVector</c>.
		/// </summary>
		/// <param name="index">The zero-based index of the element to remove.</param>
		/// <exception cref="ArgumentOutOfRangeException">
		///		<para><paramref name="index"/> is less than zero</para>
		///		<para>-or-</para>
		///		<para><paramref name="index"/> is equal to or greater than <see cref="HotIssuesPrioritiesVector.Count"/>.</para>
		/// </exception>
		public virtual void RemoveAt(int index)
		{
			ValidateIndex(index); // throws
			
			m_count--;

			if (index < m_count)
			{
				Array.Copy(m_array, index + 1, m_array, index, m_count - index);
			}
			
			// We can't set the deleted entry equal to null, because it might be a value type.
			// Instead, we'll create an empty single-element array of the right type and copy it 
			// over the entry we want to erase.
			HotIssuePriorityData[] temp = new HotIssuePriorityData[1];
			Array.Copy(temp, 0, m_array, m_count, 1);
			m_version++;
		}

		/// <summary>
		///		Gets a value indicating whether the collection has a fixed size.
		/// </summary>
		/// <value>true if the collection has a fixed size; otherwise, false. The default is false</value>
		public virtual bool IsFixedSize
		{
			get { return false; }
		}

		/// <summary>
		///		gets a value indicating whether the <B>IList</B> is read-only.
		/// </summary>
		/// <value>true if the collection is read-only; otherwise, false. The default is false</value>
		public virtual bool IsReadOnly
		{
			get { return false; }
		}
		#endregion

		#region Operations (type-safe IEnumerable)
		
		/// <summary>
		///		Returns an enumerator that can iterate through the <c>HotIssuesPrioritiesVector</c>.
		/// </summary>
		/// <returns>An <see cref="Enumerator"/> for the entire <c>HotIssuesPrioritiesVector</c>.</returns>
		public virtual IHotIssuesPrioritiesVectorEnumerator GetEnumerator()
		{
			return new Enumerator(this);
		}
		#endregion

		#region Public helpers (just to mimic some nice features of ArrayList)
		
		/// <summary>
		///		Gets or sets the number of elements the <c>HotIssuesPrioritiesVector</c> can contain.
		/// </summary>
		public virtual int Capacity
		{
			get { return m_array.Length; }
			
			set
			{
				if (value < m_count)
					value = m_count;

				if (value != m_array.Length)
				{
					if (value > 0)
					{
						HotIssuePriorityData[] temp = new HotIssuePriorityData[value];
						Array.Copy(m_array, temp, m_count);
						m_array = temp;
					}
					else
					{
						m_array = new HotIssuePriorityData[DEFAULT_CAPACITY];
					}
				}
			}
		}

		/// <summary>
		///		Adds the elements of another <c>HotIssuesPrioritiesVector</c> to the current <c>HotIssuesPrioritiesVector</c>.
		/// </summary>
		/// <param name="x">The <c>HotIssuesPrioritiesVector</c> whose elements should be added to the end of the current <c>HotIssuesPrioritiesVector</c>.</param>
		/// <returns>The new <see cref="HotIssuesPrioritiesVector.Count"/> of the <c>HotIssuesPrioritiesVector</c>.</returns>
		public virtual int AddRange(HotIssuesPrioritiesVector x)
		{
			if (m_count + x.Count >= m_array.Length)
				EnsureCapacity(m_count + x.Count);
			
			Array.Copy(x.m_array, 0, m_array, m_count, x.Count);
			m_count += x.Count;
			m_version++;

			return m_count;
		}

		/// <summary>
		///		Adds the elements of a <see cref="HotIssuePriorityData"/> array to the current <c>HotIssuesPrioritiesVector</c>.
		/// </summary>
		/// <param name="x">The <see cref="HotIssuePriorityData"/> array whose elements should be added to the end of the <c>HotIssuesPrioritiesVector</c>.</param>
		/// <returns>The new <see cref="HotIssuesPrioritiesVector.Count"/> of the <c>HotIssuesPrioritiesVector</c>.</returns>
		public virtual int AddRange(HotIssuePriorityData[] x)
		{
			if (m_count + x.Length >= m_array.Length)
				EnsureCapacity(m_count + x.Length);

			Array.Copy(x, 0, m_array, m_count, x.Length);
			m_count += x.Length;
			m_version++;

			return m_count;
		}
		
		/// <summary>
		///		Sets the capacity to the actual number of elements.
		/// </summary>
		public virtual void TrimToSize()
		{
			this.Capacity = m_count;
		}

		#endregion

		#region Implementation (helpers)

		/// <exception cref="ArgumentOutOfRangeException">
		///		<para><paramref name="index"/> is less than zero</para>
		///		<para>-or-</para>
		///		<para><paramref name="index"/> is equal to or greater than <see cref="HotIssuesPrioritiesVector.Count"/>.</para>
		/// </exception>
		private void ValidateIndex(int i)
		{
			ValidateIndex(i, false);
		}

		/// <exception cref="ArgumentOutOfRangeException">
		///		<para><paramref name="index"/> is less than zero</para>
		///		<para>-or-</para>
		///		<para><paramref name="index"/> is equal to or greater than <see cref="HotIssuesPrioritiesVector.Count"/>.</para>
		/// </exception>
		private void ValidateIndex(int i, bool allowEqualEnd)
		{
			int max = (allowEqualEnd)?(m_count):(m_count-1);
			if (i < 0 || i > max)
				throw new System.ArgumentOutOfRangeException("Index was out of range.  Must be non-negative and less than the size of the collection.", (object)i, "Specified argument was out of the range of valid values.");
		}

		private void EnsureCapacity(int min)
		{
			int newCapacity = ((m_array.Length == 0) ? DEFAULT_CAPACITY : m_array.Length * 2);
			if (newCapacity < min)
				newCapacity = min;

			this.Capacity = newCapacity;
		}

		#endregion
		
		#region Implementation (ICollection)

		void ICollection.CopyTo(Array array, int start)
		{
			Array.Copy(m_array, 0, array, start, m_count);
		}

		#endregion

		#region Implementation (IList)

		object IList.this[int i]
		{
			get { return (object)this[i]; }
			set { this[i] = (HotIssuePriorityData)value; }
		}

		int IList.Add(object x)
		{
			return this.Add((HotIssuePriorityData)x);
		}

		bool IList.Contains(object x)
		{
			return this.Contains((HotIssuePriorityData)x);
		}

		int IList.IndexOf(object x)
		{
			return this.IndexOf((HotIssuePriorityData)x);
		}

		void IList.Insert(int pos, object x)
		{
			this.Insert(pos, (HotIssuePriorityData)x);
		}

		void IList.Remove(object x)
		{
			this.Remove((HotIssuePriorityData)x);
		}

		void IList.RemoveAt(int pos)
		{
			this.RemoveAt(pos);
		}

		#endregion

		#region Implementation (IEnumerable)

		IEnumerator IEnumerable.GetEnumerator()
		{
			return (IEnumerator)(this.GetEnumerator());
		}

		#endregion

		#region Nested enumerator class
		/// <summary>
		///		Supports simple iteration over a <see cref="HotIssuesPrioritiesVector"/>.
		/// </summary>
		private class Enumerator : IEnumerator, IHotIssuesPrioritiesVectorEnumerator
		{
			#region Implementation (data)
			
			private HotIssuesPrioritiesVector m_collection;
			private int m_index;
			private int m_version;
			
			#endregion
		
			#region Construction
			
			/// <summary>
			///		Initializes a new instance of the <c>Enumerator</c> class.
			/// </summary>
			/// <param name="tc"></param>
			internal Enumerator(HotIssuesPrioritiesVector tc)
			{
				m_collection = tc;
				m_index = -1;
				m_version = tc.m_version;
			}
			
			#endregion
	
			#region Operations (type-safe IEnumerator)
			
			/// <summary>
			///		Gets the current element in the collection.
			/// </summary>
			public HotIssuePriorityData Current
			{
				get { return m_collection[m_index]; }
			}

			/// <summary>
			///		Advances the enumerator to the next element in the collection.
			/// </summary>
			/// <exception cref="InvalidOperationException">
			///		The collection was modified after the enumerator was created.
			/// </exception>
			/// <returns>
			///		<c>true</c> if the enumerator was successfully advanced to the next element; 
			///		<c>false</c> if the enumerator has passed the end of the collection.
			/// </returns>
			public bool MoveNext()
			{
				if (m_version != m_collection.m_version)
					throw new System.InvalidOperationException("Collection was modified; enumeration operation may not execute.");

				++m_index;
				return (m_index < m_collection.Count) ? true : false;
			}

			/// <summary>
			///		Sets the enumerator to its initial position, before the first element in the collection.
			/// </summary>
			public void Reset()
			{
				m_index = -1;
			}
			#endregion
	
			#region Implementation (IEnumerator)
			
			object IEnumerator.Current
			{
				get { return (object)(this.Current); }
			}
			
			#endregion
		}
		#endregion
        
		#region Nested Syncronized Wrapper class
		private class SyncHotIssuesPrioritiesVector : HotIssuesPrioritiesVector
		{
			#region Implementation (data)
			private HotIssuesPrioritiesVector m_collection;
			private object m_root;
			#endregion

			#region Construction
			internal SyncHotIssuesPrioritiesVector(HotIssuesPrioritiesVector list) : base(Tag.Default)
			{
				m_root = list.SyncRoot;
				m_collection = list;
			}
			#endregion
            
			#region Type-safe ICollection
			public override void CopyTo(HotIssuePriorityData[] array)
			{
				lock(this.m_root)
					m_collection.CopyTo(array);
			}

			public override void CopyTo(HotIssuePriorityData[] array, int start)
			{
				lock(this.m_root)
					m_collection.CopyTo(array,start);
			}
			public override int Count
			{
				get
				{ 
					lock(this.m_root)
						return m_collection.Count;
				}
			}

			public override bool IsSynchronized
			{
				get { return true; }
			}

			public override object SyncRoot
			{
				get { return this.m_root; }
			}
			#endregion
            
			#region Type-safe IList
			public override HotIssuePriorityData this[int i]
			{
				get
				{
					lock(this.m_root)
						return m_collection[i];
				}
				set
				{
					lock(this.m_root)
						m_collection[i] = value; 
				}
			}

			public override int Add(HotIssuePriorityData x)
			{
				lock(this.m_root)
					return m_collection.Add(x);
			}
            
			public override void Clear()
			{
				lock(this.m_root)
					m_collection.Clear();
			}

			public override bool Contains(HotIssuePriorityData x)
			{
				lock(this.m_root)
					return m_collection.Contains(x);
			}

			public override int IndexOf(HotIssuePriorityData x)
			{
				lock(this.m_root)
					return m_collection.IndexOf(x);
			}

			public override void Insert(int pos, HotIssuePriorityData x)
			{
				lock(this.m_root)
					m_collection.Insert(pos,x);
			}

			public override void Remove(HotIssuePriorityData x)
			{           
				lock(this.m_root)
					m_collection.Remove(x);
			}

			public override void RemoveAt(int pos)
			{
				lock(this.m_root)
					m_collection.RemoveAt(pos);
			}
            
			public override bool IsFixedSize
			{
				get {return m_collection.IsFixedSize;}
			}

			public override bool IsReadOnly
			{
				get {return m_collection.IsReadOnly;}
			}
			#endregion

			#region Type-safe IEnumerable
			public override IHotIssuesPrioritiesVectorEnumerator GetEnumerator()
			{
				lock(m_root)
					return m_collection.GetEnumerator();
			}
			#endregion

			#region Public Helpers
			// (just to mimic some nice features of ArrayList)
			public override int Capacity
			{
				get
				{
					lock(this.m_root)
						return m_collection.Capacity;
				}
                
				set
				{
					lock(this.m_root)
						m_collection.Capacity = value;
				}
			}

			public override int AddRange(HotIssuesPrioritiesVector x)
			{
				lock(this.m_root)
					return m_collection.AddRange(x);
			}

			public override int AddRange(HotIssuePriorityData[] x)
			{
				lock(this.m_root)
					return m_collection.AddRange(x);
			}
			#endregion
		}
		#endregion

		#region Nested Read Only Wrapper class
		private class ReadOnlyHotIssuesPrioritiesVector : HotIssuesPrioritiesVector
		{
			#region Implementation (data)
			private HotIssuesPrioritiesVector m_collection;
			#endregion

			#region Construction
			internal ReadOnlyHotIssuesPrioritiesVector(HotIssuesPrioritiesVector list) : base(Tag.Default)
			{
				m_collection = list;
			}
			#endregion
            
			#region Type-safe ICollection
			public override void CopyTo(HotIssuePriorityData[] array)
			{
				m_collection.CopyTo(array);
			}

			public override void CopyTo(HotIssuePriorityData[] array, int start)
			{
				m_collection.CopyTo(array,start);
			}
			public override int Count
			{
				get {return m_collection.Count;}
			}

			public override bool IsSynchronized
			{
				get { return m_collection.IsSynchronized; }
			}

			public override object SyncRoot
			{
				get { return this.m_collection.SyncRoot; }
			}
			#endregion
            
			#region Type-safe IList
			public override HotIssuePriorityData this[int i]
			{
				get { return m_collection[i]; }
				set { throw new NotSupportedException("This is a Read Only Collection and can not be modified"); }
			}

			public override int Add(HotIssuePriorityData x)
			{
				throw new NotSupportedException("This is a Read Only Collection and can not be modified");
			}
            
			public override void Clear()
			{
				throw new NotSupportedException("This is a Read Only Collection and can not be modified");
			}

			public override bool Contains(HotIssuePriorityData x)
			{
				return m_collection.Contains(x);
			}

			public override int IndexOf(HotIssuePriorityData x)
			{
				return m_collection.IndexOf(x);
			}

			public override void Insert(int pos, HotIssuePriorityData x)
			{
				throw new NotSupportedException("This is a Read Only Collection and can not be modified");
			}

			public override void Remove(HotIssuePriorityData x)
			{           
				throw new NotSupportedException("This is a Read Only Collection and can not be modified");
			}

			public override void RemoveAt(int pos)
			{
				throw new NotSupportedException("This is a Read Only Collection and can not be modified");
			}
            
			public override bool IsFixedSize
			{
				get {return true;}
			}

			public override bool IsReadOnly
			{
				get {return true;}
			}
			#endregion

			#region Type-safe IEnumerable
			public override IHotIssuesPrioritiesVectorEnumerator GetEnumerator()
			{
				return m_collection.GetEnumerator();
			}
			#endregion

			#region Public Helpers
			// (just to mimic some nice features of ArrayList)
			public override int Capacity
			{
				get { return m_collection.Capacity; }
                
				set { throw new NotSupportedException("This is a Read Only Collection and can not be modified"); }
			}

			public override int AddRange(HotIssuesPrioritiesVector x)
			{
				throw new NotSupportedException("This is a Read Only Collection and can not be modified");
			}

			public override int AddRange(HotIssuePriorityData[] x)
			{
				throw new NotSupportedException("This is a Read Only Collection and can not be modified");
			}
			#endregion
		}
		#endregion
	}

	
}
