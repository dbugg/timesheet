using System;
using System.Collections;
using System.Text;

namespace Asa.Timesheet.Data
{
	public class SalarieData
	{
		private int _salaryID = -1;
		private int _userID = -1;
		private int _salaryYear = -1;
		private int _salaryMonth = -1;
		private int _salaryTypeID = -1;
		private decimal _salary;

		/// <summary>
		/// Initialize an new empty Login object.
		/// </summary>
		public SalarieData()
		{
		}
		
		/// <summary>
		/// Initialize a new Login object with the given parameters.
		/// </summary>
		public SalarieData(int salaryID, int userID, int salaryYear, int salaryMonth, int salaryTypeID, decimal salary)
		{
			this._salaryID = salaryID;
			this._userID = userID;
			this._salaryYear = salaryYear;
			this._salaryMonth = salaryMonth;
			this._salaryTypeID = salaryTypeID;
			this._salary = salary;
		}	
		
	  
		public override string ToString()
		{
			StringBuilder builder = new StringBuilder();
			builder.Append("_salaryID: " + _salaryID.ToString()).Append("\r\n");
			builder.Append("_userID: " + _userID.ToString()).Append("\r\n");
			builder.Append("_salaryYear: " + _salaryYear.ToString()).Append("\r\n");
			builder.Append("_salaryMonth: " + _salaryMonth.ToString()).Append("\r\n");
			builder.Append("_salaryTypeID: " + _salaryTypeID.ToString()).Append("\r\n");
			builder.Append("_salary: " + _salary.ToString()).Append("\r\n");
			builder.Append("\r\n");
			return builder.ToString();
		}	
		public object GetByIndex(int nIndex)
		{
			switch(nIndex)
			{
				case 0:
					return _salaryID;
				case 1:
					return _userID;
				case 2:
					return _salaryYear;
				case 3:
					return _salaryMonth;
				case 4:
					return _salaryTypeID;
				case 5:
					return _salary;
			}
			return null;
		}
		#region Properties
	
		public int SalaryID
		{
			get { return _salaryID; }
			set { _salaryID = value; }
		}
	
		public int UserID
		{
			get { return _userID; }
			set { _userID = value; }
		}
	
		public int SalaryYear
		{
			get { return _salaryYear; }
			set { _salaryYear = value; }
		}
	
		public int SalaryMonth
		{
			get { return _salaryMonth; }
			set { _salaryMonth = value; }
		}
	
		public int SalaryTypeID
		{
			get { return _salaryTypeID; }
			set { _salaryTypeID = value; }
		}
	
		public decimal Salary
		{
			get { return _salary; }
			set { _salary = value; }
		}
	
		#endregion
	}
}


