using System;
using System.Data;
using System.Data.SqlClient;
using Microsoft.ApplicationBlocks.Data;

namespace Asa.Timesheet.Data.Reports
{
	/// <summary>
	/// Summary description for SubcontractersReportData.
	/// </summary>
	public class SubcontractersReportData
	{
    public static DataSet SelectSubcontractersReportData(int projectID,bool hpr)
    {
      string spName = "Projects_SubcontractersReport";

      SqlParameter[] parms = SqlHelperParameterCache.GetSpParameterSet(DBManager.GetConnectionString(), spName);
      parms[0].Value = projectID;
		parms[1].Value = hpr;

      DataSet ds = SqlHelper.ExecuteDataset(DBManager.GetConnectionString(), CommandType.StoredProcedure, spName, parms);

      ds.Relations.Add("Rel1", ds.Tables[0].Columns["ProjectID"],ds.Tables[1].Columns["ProjectID"], false);

      return ds;
    }

		
	}
}
