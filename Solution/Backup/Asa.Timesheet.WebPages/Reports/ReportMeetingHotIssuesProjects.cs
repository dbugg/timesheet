using System;
using System.Data;
using GrapeCity.ActiveReports;
using GrapeCity.ActiveReports.Controls;
using GrapeCity.ActiveReports.SectionReportModel;
using GrapeCity.ActiveReports.Document.Section;
using GrapeCity.ActiveReports.Document;

namespace Asa.Timesheet.WebPages.Reports
{
    public class ReportMeetingHotIssuesProjects : GrapeCity.ActiveReports.SectionReport
	{
        public ReportMeetingHotIssuesProjects(DataView dv, bool bEn)
        {

            InitializeComponent();
            if (bEn)
            {
                TextBox4.Text = "Project No:";
                TextBox3.Text = "Name:";
            }
            this.DataSource = dv;
        }

		#region ActiveReports Designer generated code







        public void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ReportMeetingHotIssuesProjects));
            this.Detail = new GrapeCity.ActiveReports.SectionReportModel.Detail();
            this.txtProjectName = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtProjectCode = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.TextBox3 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.TextBox4 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtProjectDescription = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.PageHeader = new GrapeCity.ActiveReports.SectionReportModel.PageHeader();
            this.PageFooter = new GrapeCity.ActiveReports.SectionReportModel.PageFooter();
            ((System.ComponentModel.ISupportInitialize)(this.txtProjectName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtProjectCode)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TextBox3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TextBox4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtProjectDescription)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            // 
            // Detail
            // 
            this.Detail.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
            this.txtProjectName,
            this.txtProjectCode,
            this.TextBox3,
            this.TextBox4,
            this.txtProjectDescription,
            this.textBox2});
            this.Detail.Height = 1.102778F;
            this.Detail.Name = "Detail";
            // 
            // txtProjectName
            // 
            this.txtProjectName.DataField = "ProjectName";
            this.txtProjectName.Height = 0.313F;
            this.txtProjectName.Left = 1.25F;
            this.txtProjectName.Name = "txtProjectName";
            this.txtProjectName.Style = "font-family: Verdana; font-size: 9pt; font-weight: bold; ddo-char-set: 1";
            this.txtProjectName.Text = null;
            this.txtProjectName.Top = 0.3745F;
            this.txtProjectName.Width = 5.75F;
            // 
            // txtProjectCode
            // 
            this.txtProjectCode.CanGrow = false;
            this.txtProjectCode.DataField = "ProjectCode";
            this.txtProjectCode.Height = 0.312F;
            this.txtProjectCode.Left = 1.25F;
            this.txtProjectCode.Name = "txtProjectCode";
            this.txtProjectCode.Style = "font-family: Verdana; font-size: 9pt; font-weight: bold; ddo-char-set: 1";
            this.txtProjectCode.Text = null;
            this.txtProjectCode.Top = 0F;
            this.txtProjectCode.Width = 5.75F;
            // 
            // TextBox3
            // 
            this.TextBox3.Height = 0.3125F;
            this.TextBox3.Left = 0F;
            this.TextBox3.Name = "TextBox3";
            this.TextBox3.Style = "font-family: Verdana; font-size: 9pt; font-weight: normal; text-align: left; ddo-" +
    "char-set: 1";
            this.TextBox3.Text = "���/\r\nName:";
            this.TextBox3.Top = 0.375F;
            this.TextBox3.Width = 1.25F;
            // 
            // TextBox4
            // 
            this.TextBox4.Height = 0.312F;
            this.TextBox4.Left = 0F;
            this.TextBox4.Name = "TextBox4";
            this.TextBox4.Style = "font-family: Verdana; font-size: 9pt; font-weight: normal; text-align: left; ddo-" +
    "char-set: 1";
            this.TextBox4.SummaryRunning = GrapeCity.ActiveReports.SectionReportModel.SummaryRunning.All;
            this.TextBox4.Text = "������ �/\r\nPrj. no:";
            this.TextBox4.Top = 0F;
            this.TextBox4.Width = 1.25F;
            // 
            // txtProjectDescription
            // 
            this.txtProjectDescription.DataField = "ProjectDescription";
            this.txtProjectDescription.Height = 0.353F;
            this.txtProjectDescription.Left = 1.25F;
            this.txtProjectDescription.Name = "txtProjectDescription";
            this.txtProjectDescription.Style = "font-family: Verdana; font-size: 9pt; font-weight: bold; ddo-char-set: 1";
            this.txtProjectDescription.Text = null;
            this.txtProjectDescription.Top = 0.7500001F;
            this.txtProjectDescription.Width = 5.7495F;
            // 
            // textBox2
            // 
            this.textBox2.Height = 0.353F;
            this.textBox2.Left = 0F;
            this.textBox2.Name = "textBox2";
            this.textBox2.Style = "font-family: Verdana; font-size: 9pt; font-weight: normal; text-align: left; ddo-" +
    "char-set: 1";
            this.textBox2.Text = "�������� / \r\nDescription:";
            this.textBox2.Top = 0.7500001F;
            this.textBox2.Width = 1.25F;
            // 
            // PageHeader
            // 
            this.PageHeader.Height = 0F;
            this.PageHeader.Name = "PageHeader";
            // 
            // PageFooter
            // 
            this.PageFooter.Height = 0F;
            this.PageFooter.Name = "PageFooter";
            // 
            // ReportMeetingHotIssuesProjects
            // 
            this.MasterReport = false;
            this.PageSettings.PaperHeight = 11F;
            this.PageSettings.PaperWidth = 8.5F;
            this.PrintWidth = 7.010417F;
            this.Sections.Add(this.PageHeader);
            this.Sections.Add(this.Detail);
            this.Sections.Add(this.PageFooter);
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule(resources.GetString("$this.StyleSheet"), "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: inherit; font-style: inherit; font-variant: inherit; font-weight: bo" +
            "ld; font-size: 16pt; font-size-adjust: inherit; font-stretch: inherit", "Heading1", "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: Times New Roman; font-style: italic; font-variant: inherit; font-wei" +
            "ght: bold; font-size: 14pt; font-size-adjust: inherit; font-stretch: inherit", "Heading2", "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: inherit; font-style: inherit; font-variant: inherit; font-weight: bo" +
            "ld; font-size: 13pt; font-size-adjust: inherit; font-stretch: inherit", "Heading3", "Normal"));
            ((System.ComponentModel.ISupportInitialize)(this.txtProjectName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtProjectCode)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TextBox3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TextBox4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtProjectDescription)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();

        }

		#endregion

        private PageHeader PageHeader;
        private Detail Detail;
        private TextBox txtProjectName;
        private TextBox txtProjectCode;
        private TextBox TextBox3;
        private TextBox TextBox4;
        private TextBox txtProjectDescription;
        private TextBox textBox2;
        private PageFooter PageFooter;
	}
}
