using System;
using Asa.Timesheet.Data.Reports;
using Asa.Timesheet.Data;
using System.Data;
using GrapeCity.ActiveReports;
using GrapeCity.ActiveReports.Controls;
using GrapeCity.ActiveReports.SectionReportModel;
using GrapeCity.ActiveReports.Document.Section;
using GrapeCity.ActiveReports.Document;

namespace Asa.Timesheet.WebPages.Reports
{
    public class FeeAccountRpt : GrapeCity.ActiveReports.SectionReport
	{
		private string _PaydNotTo = string.Empty;
		private bool _pdf;
		private string _projectName = string.Empty;
		private string _createdBy= string.Empty;
private string _dateTo;

public FeeAccountRpt(bool pdf, string projectName, DataView dv, string createdBy, string dateTo)
{
    _pdf = pdf;
    _projectName = projectName;
    _createdBy = createdBy;
    _dateTo = dateTo;
    this.DataSource = dv;
    InitializeComponent();
    //			if(this.PageSettings.Orientation == GrapeCity.ActiveReports.Document.Section.PageOrientation.Landscape)
    //				this.ReportEnd += new EventHandler(SubAnalysisRpt_ReportEnd);
    //			else
    this.ReportEnd += new EventHandler(SubAnalysisRpt1_ReportEnd);

    if (!pdf)
    {
        PageFooter.Visible = false;
    }
    _PaydNotTo = this.Label6.Text;
}

private void ReportHeader_Format(object sender, System.EventArgs eArgs)
{
    if (_projectName.Length != 0)
        txtTitle.Text = string.Concat(txtTitle.Text, " ", Resource.ResourceManager["FeeAccountReportProject"], " ", _projectName);

    this.lbDate.Text = DateTime.Now.ToShortDateString();
}

private void ReportFooter_Format(object sender, System.EventArgs eArgs)
{
    txtCreatedBy.Text = _createdBy;
}


private void Detail_Format(object sender, System.EventArgs eArgs)
{
    if (this.txtOrder.Text == "2")
    {
        //				this.txtClients.Text = "";
        this.txtSquareMeters.Text = "";
        this.txtEURSM.Text = "";
    }
    if (this.txtOrder.Text == "3")
    {
        this.txtSquareMeters.Text = "";
        this.txtEURSM.Text = "";
    }
    if (this.txtOrder.Text == "4" && (this.txtDiscription.Text == string.Concat(Resource.ResourceManager["reports_Total"], ":")))
    {
        this.txtClients.Text = "";
    }
    BindPercent(txtEUR.Text, txtPayd.Text);
}



private void PageHeader_Format(object sender, System.EventArgs eArgs)
{
    this.Label6.Text = string.Concat(_PaydNotTo, " ", _dateTo);
    if (!_pdf)
        if (this.PageNumber > 1)
            this.PageHeader.Visible = false;
}

		#region ActiveReports Designer generated code










































public void InitializeComponent()
{
    System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FeeAccountRpt));
    this.Detail = new GrapeCity.ActiveReports.SectionReportModel.Detail();
    this.ReportHeader = new GrapeCity.ActiveReports.SectionReportModel.ReportHeader();
    this.ReportFooter = new GrapeCity.ActiveReports.SectionReportModel.ReportFooter();
    this.PageHeader = new GrapeCity.ActiveReports.SectionReportModel.PageHeader();
    this.PageFooter = new GrapeCity.ActiveReports.SectionReportModel.PageFooter();
    this.txtTitle = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
    this.lbDate = new GrapeCity.ActiveReports.SectionReportModel.Label();
    this.Label1 = new GrapeCity.ActiveReports.SectionReportModel.Label();
    this.Label2 = new GrapeCity.ActiveReports.SectionReportModel.Label();
    this.Label3 = new GrapeCity.ActiveReports.SectionReportModel.Label();
    this.Label4 = new GrapeCity.ActiveReports.SectionReportModel.Label();
    this.Label5 = new GrapeCity.ActiveReports.SectionReportModel.Label();
    this.Label6 = new GrapeCity.ActiveReports.SectionReportModel.Label();
    this.Line1 = new GrapeCity.ActiveReports.SectionReportModel.Line();
    this.Line3 = new GrapeCity.ActiveReports.SectionReportModel.Line();
    this.Line4 = new GrapeCity.ActiveReports.SectionReportModel.Line();
    this.Line5 = new GrapeCity.ActiveReports.SectionReportModel.Line();
    this.Line6 = new GrapeCity.ActiveReports.SectionReportModel.Line();
    this.Line7 = new GrapeCity.ActiveReports.SectionReportModel.Line();
    this.Label9 = new GrapeCity.ActiveReports.SectionReportModel.Label();
    this.Label10 = new GrapeCity.ActiveReports.SectionReportModel.Label();
    this.Label11 = new GrapeCity.ActiveReports.SectionReportModel.Label();
    this.txtDiscription = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
    this.txtSquareMeters = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
    this.txtEURSM = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
    this.txtEUR = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
    this.txtPayd = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
    this.txtNotPayd = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
    this.Line19 = new GrapeCity.ActiveReports.SectionReportModel.Line();
    this.Line20 = new GrapeCity.ActiveReports.SectionReportModel.Line();
    this.Line21 = new GrapeCity.ActiveReports.SectionReportModel.Line();
    this.Line22 = new GrapeCity.ActiveReports.SectionReportModel.Line();
    this.Line23 = new GrapeCity.ActiveReports.SectionReportModel.Line();
    this.Line26 = new GrapeCity.ActiveReports.SectionReportModel.Line();
    this.txtPaymentID = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
    this.txtClients = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
    this.txtOrder = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
    this.txtPaydPercent = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
    this.txtNotPaydPercent = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
    this.Label7 = new GrapeCity.ActiveReports.SectionReportModel.Label();
    this.txtCreatedBy = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
    this.Line27 = new GrapeCity.ActiveReports.SectionReportModel.Line();
    ((System.ComponentModel.ISupportInitialize)(this.txtTitle)).BeginInit();
    ((System.ComponentModel.ISupportInitialize)(this.lbDate)).BeginInit();
    ((System.ComponentModel.ISupportInitialize)(this.Label1)).BeginInit();
    ((System.ComponentModel.ISupportInitialize)(this.Label2)).BeginInit();
    ((System.ComponentModel.ISupportInitialize)(this.Label3)).BeginInit();
    ((System.ComponentModel.ISupportInitialize)(this.Label4)).BeginInit();
    ((System.ComponentModel.ISupportInitialize)(this.Label5)).BeginInit();
    ((System.ComponentModel.ISupportInitialize)(this.Label6)).BeginInit();
    ((System.ComponentModel.ISupportInitialize)(this.Label9)).BeginInit();
    ((System.ComponentModel.ISupportInitialize)(this.Label10)).BeginInit();
    ((System.ComponentModel.ISupportInitialize)(this.Label11)).BeginInit();
    ((System.ComponentModel.ISupportInitialize)(this.txtDiscription)).BeginInit();
    ((System.ComponentModel.ISupportInitialize)(this.txtSquareMeters)).BeginInit();
    ((System.ComponentModel.ISupportInitialize)(this.txtEURSM)).BeginInit();
    ((System.ComponentModel.ISupportInitialize)(this.txtEUR)).BeginInit();
    ((System.ComponentModel.ISupportInitialize)(this.txtPayd)).BeginInit();
    ((System.ComponentModel.ISupportInitialize)(this.txtNotPayd)).BeginInit();
    ((System.ComponentModel.ISupportInitialize)(this.txtPaymentID)).BeginInit();
    ((System.ComponentModel.ISupportInitialize)(this.txtClients)).BeginInit();
    ((System.ComponentModel.ISupportInitialize)(this.txtOrder)).BeginInit();
    ((System.ComponentModel.ISupportInitialize)(this.txtPaydPercent)).BeginInit();
    ((System.ComponentModel.ISupportInitialize)(this.txtNotPaydPercent)).BeginInit();
    ((System.ComponentModel.ISupportInitialize)(this.Label7)).BeginInit();
    ((System.ComponentModel.ISupportInitialize)(this.txtCreatedBy)).BeginInit();
    ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
    // 
    // Detail
    // 
    this.Detail.ColumnSpacing = 0F;
    this.Detail.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
						this.txtDiscription,
						this.txtSquareMeters,
						this.txtEURSM,
						this.txtEUR,
						this.txtPayd,
						this.txtNotPayd,
						this.Line19,
						this.Line20,
						this.Line21,
						this.Line22,
						this.Line23,
						this.Line26,
						this.txtPaymentID,
						this.txtClients,
						this.txtOrder,
						this.txtPaydPercent,
						this.txtNotPaydPercent});
    this.Detail.Height = 0.26875F;
    this.Detail.Name = "Detail";
    // 
    // ReportHeader
    // 
    this.ReportHeader.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
						this.txtTitle,
						this.lbDate});
    this.ReportHeader.Height = 0.7291667F;
    this.ReportHeader.Name = "ReportHeader";
    // 
    // ReportFooter
    // 
    this.ReportFooter.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
						this.Label7,
						this.txtCreatedBy,
						this.Line27});
    this.ReportFooter.Height = 0.9166667F;
    this.ReportFooter.Name = "ReportFooter";
    // 
    // PageHeader
    // 
    this.PageHeader.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
						this.Label1,
						this.Label2,
						this.Label3,
						this.Label4,
						this.Label5,
						this.Label6,
						this.Line1,
						this.Line3,
						this.Line4,
						this.Line5,
						this.Line6,
						this.Line7,
						this.Label9,
						this.Label10,
						this.Label11});
    this.PageHeader.Height = 0.26875F;
    this.PageHeader.Name = "PageHeader";
    // 
    // PageFooter
    // 
    this.PageFooter.Height = 0F;
    this.PageFooter.Name = "PageFooter";
    // 
    // txtTitle
    // 
    this.txtTitle.Height = 0.2F;
    this.txtTitle.Left = 0.4375F;
    this.txtTitle.Name = "txtTitle";
    this.txtTitle.Style = "font-size: 14pt; font-weight: bold; text-align: center; vertical-align: top; ddo-" +
        "char-set: 1";
    this.txtTitle.Text = "??????? ??????";
    this.txtTitle.Top = 0.25F;
    this.txtTitle.Width = 9.5F;
    // 
    // lbDate
    // 
    this.lbDate.Height = 0.1968504F;
    this.lbDate.HyperLink = null;
    this.lbDate.Left = 0.4375F;
    this.lbDate.Name = "lbDate";
    this.lbDate.Style = "font-size: 10pt; font-weight: bold; text-align: right; ddo-char-set: 1";
    this.lbDate.Text = "Page";
    this.lbDate.Top = 0F;
    this.lbDate.Width = 9.5F;
    // 
    // Label1
    // 
    this.Label1.Height = 0.2F;
    this.Label1.HyperLink = null;
    this.Label1.Left = 0.4375F;
    this.Label1.Name = "Label1";
    this.Label1.Style = "font-size: 10pt; font-weight: bold; text-align: left; ddo-char-set: 1";
    this.Label1.Text = "????????";
    this.Label1.Top = 0.006944418F;
    this.Label1.Width = 2.25F;
    // 
    // Label2
    // 
    this.Label2.Height = 0.2F;
    this.Label2.HyperLink = null;
    this.Label2.Left = 4.6875F;
    this.Label2.Name = "Label2";
    this.Label2.Style = "padding-left: 2px; font-size: 10pt; font-weight: bold; text-align: center; ddo-ch" +
        "ar-set: 1";
    this.Label2.Text = "??. ?????";
    this.Label2.Top = 0F;
    this.Label2.Width = 0.75F;
    // 
    // Label3
    // 
    this.Label3.Height = 0.2F;
    this.Label3.HyperLink = null;
    this.Label3.Left = 5.4375F;
    this.Label3.Name = "Label3";
    this.Label3.Style = "font-size: 10pt; font-weight: bold; text-align: center; ddo-char-set: 1";
    this.Label3.Text = "EUR / ??. ?.";
    this.Label3.Top = 0F;
    this.Label3.Width = 1F;
    // 
    // Label4
    // 
    this.Label4.Height = 0.2F;
    this.Label4.HyperLink = null;
    this.Label4.Left = 6.4375F;
    this.Label4.Name = "Label4";
    this.Label4.Style = "font-size: 9pt; font-weight: bold; text-align: center; ddo-char-set: 1";
    this.Label4.Text = "EUR";
    this.Label4.Top = 0.006944418F;
    this.Label4.Width = 0.9375F;
    // 
    // Label5
    // 
    this.Label5.Height = 0.2F;
    this.Label5.HyperLink = null;
    this.Label5.Left = 7.375F;
    this.Label5.Name = "Label5";
    this.Label5.Style = "font-size: 10pt; font-weight: bold; text-align: center; ddo-char-set: 1";
    this.Label5.Text = "???????";
    this.Label5.Top = 0F;
    this.Label5.Width = 0.8125F;
    // 
    // Label6
    // 
    this.Label6.Height = 0.2F;
    this.Label6.HyperLink = null;
    this.Label6.Left = 9F;
    this.Label6.Name = "Label6";
    this.Label6.Style = "font-size: 10pt; font-weight: bold; text-align: center; ddo-char-set: 1";
    this.Label6.Text = "??????? ???";
    this.Label6.Top = 0F;
    this.Label6.Width = 1.751F;
    // 
    // Line1
    // 
    this.Line1.Height = 0F;
    this.Line1.Left = 0.444F;
    this.Line1.LineWeight = 1F;
    this.Line1.Name = "Line1";
    this.Line1.Top = 0.006944418F;
    this.Line1.Width = 11.156F;
    this.Line1.X1 = 0.444F;
    this.Line1.X2 = 11.6F;
    this.Line1.Y1 = 0.006944418F;
    this.Line1.Y2 = 0.006944418F;
    // 
    // Line3
    // 
    this.Line3.AnchorBottom = true;
    this.Line3.Height = 0.1939999F;
    this.Line3.Left = 0.4445F;
    this.Line3.LineWeight = 1F;
    this.Line3.Name = "Line3";
    this.Line3.Top = 0.007444445F;
    this.Line3.Visible = false;
    this.Line3.Width = 0.0004999936F;
    this.Line3.X1 = 0.445F;
    this.Line3.X2 = 0.4445F;
    this.Line3.Y1 = 0.2014444F;
    this.Line3.Y2 = 0.007444445F;
    // 
    // Line4
    // 
    this.Line4.AnchorBottom = true;
    this.Line4.Height = 0.1939999F;
    this.Line4.Left = 2.6775F;
    this.Line4.LineWeight = 1F;
    this.Line4.Name = "Line4";
    this.Line4.Top = 0.007444445F;
    this.Line4.Visible = false;
    this.Line4.Width = 0F;
    this.Line4.X1 = 2.6775F;
    this.Line4.X2 = 2.6775F;
    this.Line4.Y1 = 0.2014444F;
    this.Line4.Y2 = 0.007444445F;
    // 
    // Line5
    // 
    this.Line5.AnchorBottom = true;
    this.Line5.Height = 0.1939999F;
    this.Line5.Left = 5.4175F;
    this.Line5.LineWeight = 1F;
    this.Line5.Name = "Line5";
    this.Line5.Top = 0.007444445F;
    this.Line5.Visible = false;
    this.Line5.Width = 0F;
    this.Line5.X1 = 5.4175F;
    this.Line5.X2 = 5.4175F;
    this.Line5.Y1 = 0.2014444F;
    this.Line5.Y2 = 0.007444445F;
    // 
    // Line6
    // 
    this.Line6.AnchorBottom = true;
    this.Line6.Height = 0.1939999F;
    this.Line6.Left = 6.4175F;
    this.Line6.LineWeight = 1F;
    this.Line6.Name = "Line6";
    this.Line6.Top = 0.007444445F;
    this.Line6.Visible = false;
    this.Line6.Width = 0F;
    this.Line6.X1 = 6.4175F;
    this.Line6.X2 = 6.4175F;
    this.Line6.Y1 = 0.2014444F;
    this.Line6.Y2 = 0.007444445F;
    // 
    // Line7
    // 
    this.Line7.AnchorBottom = true;
    this.Line7.Height = 0.1939999F;
    this.Line7.Left = 7.3675F;
    this.Line7.LineWeight = 1F;
    this.Line7.Name = "Line7";
    this.Line7.Top = 0.007444445F;
    this.Line7.Visible = false;
    this.Line7.Width = 0F;
    this.Line7.X1 = 7.3675F;
    this.Line7.X2 = 7.3675F;
    this.Line7.Y1 = 0.2014444F;
    this.Line7.Y2 = 0.007444445F;
    // 
    // Label9
    // 
    this.Label9.Height = 0.2F;
    this.Label9.HyperLink = null;
    this.Label9.Left = 2.6875F;
    this.Label9.Name = "Label9";
    this.Label9.Style = "font-size: 10pt; font-weight: bold; text-align: justify; vertical-align: top; ddo" +
        "-char-set: 1";
    this.Label9.Text = "??????";
    this.Label9.Top = 0F;
    this.Label9.Width = 2F;
    // 
    // Label10
    // 
    this.Label10.Height = 0.2F;
    this.Label10.HyperLink = null;
    this.Label10.Left = 8.1875F;
    this.Label10.Name = "Label10";
    this.Label10.Style = "font-size: 10pt; font-weight: bold; text-align: center; ddo-char-set: 1";
    this.Label10.Text = "??????? %";
    this.Label10.Top = 0F;
    this.Label10.Width = 0.812F;
    // 
    // Label11
    // 
    this.Label11.Height = 0.2F;
    this.Label11.HyperLink = null;
    this.Label11.Left = 10.751F;
    this.Label11.Name = "Label11";
    this.Label11.Style = "font-size: 10pt; font-weight: bold; text-align: center; ddo-char-set: 1";
    this.Label11.Text = "??????? %";
    this.Label11.Top = 0F;
    this.Label11.Width = 0.86F;
    // 
    // txtDiscription
    // 
    this.txtDiscription.DataField = "projectname";
    this.txtDiscription.Height = 0.2F;
    this.txtDiscription.Left = 0.4375F;
    this.txtDiscription.Name = "txtDiscription";
    this.txtDiscription.Style = "text-align: left";
    this.txtDiscription.Top = 0F;
    this.txtDiscription.Width = 2.25F;
    // 
    // txtSquareMeters
    // 
    this.txtSquareMeters.DataField = "Area";
    this.txtSquareMeters.Height = 0.2F;
    this.txtSquareMeters.Left = 4.6875F;
    this.txtSquareMeters.Name = "txtSquareMeters";
    this.txtSquareMeters.OutputFormat = resources.GetString("txtSquareMeters.OutputFormat");
    this.txtSquareMeters.Style = "text-align: right";
    this.txtSquareMeters.Top = 0F;
    this.txtSquareMeters.Width = 0.75F;
    // 
    // txtEURSM
    // 
    this.txtEURSM.DataField = "EURForArea";
    this.txtEURSM.Height = 0.2F;
    this.txtEURSM.Left = 5.4375F;
    this.txtEURSM.Name = "txtEURSM";
    this.txtEURSM.OutputFormat = resources.GetString("txtEURSM.OutputFormat");
    this.txtEURSM.Style = "text-align: right";
    this.txtEURSM.Top = 0F;
    this.txtEURSM.Width = 1F;
    // 
    // txtEUR
    // 
    this.txtEUR.DataField = "EUR";
    this.txtEUR.Height = 0.2F;
    this.txtEUR.Left = 6.4375F;
    this.txtEUR.Name = "txtEUR";
    this.txtEUR.OutputFormat = resources.GetString("txtEUR.OutputFormat");
    this.txtEUR.Style = "text-align: right";
    this.txtEUR.Top = 0F;
    this.txtEUR.Width = 0.9375F;
    // 
    // txtPayd
    // 
    this.txtPayd.DataField = "Amount";
    this.txtPayd.Height = 0.2F;
    this.txtPayd.Left = 7.375F;
    this.txtPayd.Name = "txtPayd";
    this.txtPayd.OutputFormat = resources.GetString("txtPayd.OutputFormat");
    this.txtPayd.Style = "text-align: right";
    this.txtPayd.Top = 0F;
    this.txtPayd.Width = 0.8125F;
    // 
    // txtNotPayd
    // 
    this.txtNotPayd.DataField = "AmountLeft";
    this.txtNotPayd.Height = 0.2F;
    this.txtNotPayd.Left = 9F;
    this.txtNotPayd.Name = "txtNotPayd";
    this.txtNotPayd.OutputFormat = resources.GetString("txtNotPayd.OutputFormat");
    this.txtNotPayd.Style = "text-align: right";
    this.txtNotPayd.Top = 0F;
    this.txtNotPayd.Width = 1.751F;
    // 
    // Line19
    // 
    this.Line19.AnchorBottom = true;
    this.Line19.Height = 0.2009444F;
    this.Line19.Left = 0.4444444F;
    this.Line19.LineWeight = 1F;
    this.Line19.Name = "Line19";
    this.Line19.Top = 0F;
    this.Line19.Visible = false;
    this.Line19.Width = 0.0004999638F;
    this.Line19.X1 = 0.4449444F;
    this.Line19.X2 = 0.4444444F;
    this.Line19.Y1 = 0.2009444F;
    this.Line19.Y2 = 0F;
    // 
    // Line20
    // 
    this.Line20.AnchorBottom = true;
    this.Line20.Height = 0.2009444F;
    this.Line20.Left = 2.677444F;
    this.Line20.LineWeight = 1F;
    this.Line20.Name = "Line20";
    this.Line20.Top = 0F;
    this.Line20.Visible = false;
    this.Line20.Width = 0F;
    this.Line20.X1 = 2.677444F;
    this.Line20.X2 = 2.677444F;
    this.Line20.Y1 = 0.2009444F;
    this.Line20.Y2 = 0F;
    // 
    // Line21
    // 
    this.Line21.AnchorBottom = true;
    this.Line21.Height = 0.2009444F;
    this.Line21.Left = 5.417445F;
    this.Line21.LineWeight = 1F;
    this.Line21.Name = "Line21";
    this.Line21.Top = 0F;
    this.Line21.Visible = false;
    this.Line21.Width = 0F;
    this.Line21.X1 = 5.417445F;
    this.Line21.X2 = 5.417445F;
    this.Line21.Y1 = 0.2009444F;
    this.Line21.Y2 = 0F;
    // 
    // Line22
    // 
    this.Line22.AnchorBottom = true;
    this.Line22.Height = 0.2009444F;
    this.Line22.Left = 6.417445F;
    this.Line22.LineWeight = 1F;
    this.Line22.Name = "Line22";
    this.Line22.Top = 0F;
    this.Line22.Visible = false;
    this.Line22.Width = 0F;
    this.Line22.X1 = 6.417445F;
    this.Line22.X2 = 6.417445F;
    this.Line22.Y1 = 0.2009444F;
    this.Line22.Y2 = 0F;
    // 
    // Line23
    // 
    this.Line23.AnchorBottom = true;
    this.Line23.Height = 0.2009444F;
    this.Line23.Left = 7.367445F;
    this.Line23.LineWeight = 1F;
    this.Line23.Name = "Line23";
    this.Line23.Top = 0F;
    this.Line23.Visible = false;
    this.Line23.Width = 0F;
    this.Line23.X1 = 7.367445F;
    this.Line23.X2 = 7.367445F;
    this.Line23.Y1 = 0.2009444F;
    this.Line23.Y2 = 0F;
    // 
    // Line26
    // 
    this.Line26.Height = 0F;
    this.Line26.Left = 0.4444444F;
    this.Line26.LineWeight = 1F;
    this.Line26.Name = "Line26";
    this.Line26.Top = 0F;
    this.Line26.Width = 11.15556F;
    this.Line26.X1 = 0.4444444F;
    this.Line26.X2 = 11.6F;
    this.Line26.Y1 = 0F;
    this.Line26.Y2 = 0F;
    // 
    // txtPaymentID
    // 
    this.txtPaymentID.DataField = "SubprojectID";
    this.txtPaymentID.Height = 0.075F;
    this.txtPaymentID.Left = 0.3125F;
    this.txtPaymentID.Name = "txtPaymentID";
    this.txtPaymentID.Top = 0F;
    this.txtPaymentID.Visible = false;
    this.txtPaymentID.Width = 0.125F;
    // 
    // txtClients
    // 
    this.txtClients.DataField = "ClientName";
    this.txtClients.Height = 0.2F;
    this.txtClients.Left = 2.6875F;
    this.txtClients.Name = "txtClients";
    this.txtClients.Style = "text-align: left";
    this.txtClients.Top = 0F;
    this.txtClients.Width = 2F;
    // 
    // txtOrder
    // 
    this.txtOrder.DataField = "ORDERCLAUSE";
    this.txtOrder.Height = 0.075F;
    this.txtOrder.Left = 0.3125F;
    this.txtOrder.Name = "txtOrder";
    this.txtOrder.Top = 0.0625F;
    this.txtOrder.Visible = false;
    this.txtOrder.Width = 0.125F;
    // 
    // txtPaydPercent
    // 
    this.txtPaydPercent.DataField = "Amount";
    this.txtPaydPercent.Height = 0.2F;
    this.txtPaydPercent.Left = 8.1875F;
    this.txtPaydPercent.Name = "txtPaydPercent";
    this.txtPaydPercent.OutputFormat = resources.GetString("txtPaydPercent.OutputFormat");
    this.txtPaydPercent.Style = "text-align: right";
    this.txtPaydPercent.Top = 0F;
    this.txtPaydPercent.Width = 0.812F;
    // 
    // txtNotPaydPercent
    // 
    this.txtNotPaydPercent.DataField = "Amount";
    this.txtNotPaydPercent.Height = 0.2F;
    this.txtNotPaydPercent.Left = 10.751F;
    this.txtNotPaydPercent.Name = "txtNotPaydPercent";
    this.txtNotPaydPercent.OutputFormat = resources.GetString("txtNotPaydPercent.OutputFormat");
    this.txtNotPaydPercent.Style = "text-align: right";
    this.txtNotPaydPercent.Top = 0F;
    this.txtNotPaydPercent.Width = 0.86F;
    // 
    // Label7
    // 
    this.Label7.Height = 0.2F;
    this.Label7.HyperLink = null;
    this.Label7.Left = 0.438F;
    this.Label7.Name = "Label7";
    this.Label7.Style = "font-size: 11pt; text-align: right";
    this.Label7.Text = "????????: ......................................";
    this.Label7.Top = 0.445F;
    this.Label7.Width = 1.312F;
    // 
    // txtCreatedBy
    // 
    this.txtCreatedBy.Height = 0.2F;
    this.txtCreatedBy.Left = 2.688F;
    this.txtCreatedBy.Name = "txtCreatedBy";
    this.txtCreatedBy.Style = "font-size: 10pt; font-weight: normal; text-align: justify";
    this.txtCreatedBy.Text = "txtCreatedBy";
    this.txtCreatedBy.Top = 0.6875F;
    this.txtCreatedBy.Width = 2F;
    // 
    // Line27
    // 
    this.Line27.Height = 0F;
    this.Line27.Left = 0.4444444F;
    this.Line27.LineWeight = 1F;
    this.Line27.Name = "Line27";
    this.Line27.Top = 0F;
    this.Line27.Width = 11.15556F;
    this.Line27.X1 = 0.4444444F;
    this.Line27.X2 = 11.6F;
    this.Line27.Y1 = 0F;
    this.Line27.Y2 = 0F;
    // 
    // ActiveReport1
    // 
    this.MasterReport = false;
    this.PageSettings.PaperHeight = 11F;
    this.PageSettings.PaperWidth = 8.5F;
    this.PrintWidth = 11.63542F;
    this.Sections.Add(this.ReportHeader);
    this.Sections.Add(this.PageHeader);
    this.Sections.Add(this.Detail);
    this.Sections.Add(this.PageFooter);
    this.Sections.Add(this.ReportFooter);
    this.StyleSheet.Add(new DDCssLib.StyleSheetRule(resources.GetString("$this.StyleSheet"), "Normal"));
    this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: inherit; font-style: inherit; font-variant: inherit; font-weight: bo" +
                "ld; font-size: 16pt; font-size-adjust: inherit; font-stretch: inherit", "Heading1", "Normal"));
    this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: Times New Roman; font-style: italic; font-variant: inherit; font-wei" +
                "ght: bold; font-size: 14pt; font-size-adjust: inherit; font-stretch: inherit", "Heading2", "Normal"));
    this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: inherit; font-style: inherit; font-variant: inherit; font-weight: bo" +
                "ld; font-size: 13pt; font-size-adjust: inherit; font-stretch: inherit", "Heading3", "Normal"));
    ((System.ComponentModel.ISupportInitialize)(this.txtTitle)).EndInit();
    ((System.ComponentModel.ISupportInitialize)(this.lbDate)).EndInit();
    ((System.ComponentModel.ISupportInitialize)(this.Label1)).EndInit();
    ((System.ComponentModel.ISupportInitialize)(this.Label2)).EndInit();
    ((System.ComponentModel.ISupportInitialize)(this.Label3)).EndInit();
    ((System.ComponentModel.ISupportInitialize)(this.Label4)).EndInit();
    ((System.ComponentModel.ISupportInitialize)(this.Label5)).EndInit();
    ((System.ComponentModel.ISupportInitialize)(this.Label6)).EndInit();
    ((System.ComponentModel.ISupportInitialize)(this.Label9)).EndInit();
    ((System.ComponentModel.ISupportInitialize)(this.Label10)).EndInit();
    ((System.ComponentModel.ISupportInitialize)(this.Label11)).EndInit();
    ((System.ComponentModel.ISupportInitialize)(this.txtDiscription)).EndInit();
    ((System.ComponentModel.ISupportInitialize)(this.txtSquareMeters)).EndInit();
    ((System.ComponentModel.ISupportInitialize)(this.txtEURSM)).EndInit();
    ((System.ComponentModel.ISupportInitialize)(this.txtEUR)).EndInit();
    ((System.ComponentModel.ISupportInitialize)(this.txtPayd)).EndInit();
    ((System.ComponentModel.ISupportInitialize)(this.txtNotPayd)).EndInit();
    ((System.ComponentModel.ISupportInitialize)(this.txtPaymentID)).EndInit();
    ((System.ComponentModel.ISupportInitialize)(this.txtClients)).EndInit();
    ((System.ComponentModel.ISupportInitialize)(this.txtOrder)).EndInit();
    ((System.ComponentModel.ISupportInitialize)(this.txtPaydPercent)).EndInit();
    ((System.ComponentModel.ISupportInitialize)(this.txtNotPaydPercent)).EndInit();
    ((System.ComponentModel.ISupportInitialize)(this.Label7)).EndInit();
    ((System.ComponentModel.ISupportInitialize)(this.txtCreatedBy)).EndInit();
    ((System.ComponentModel.ISupportInitialize)(this)).EndInit();

    // Attach Report Events
    this.ReportHeader.Format += new System.EventHandler(this.ReportHeader_Format);
    this.ReportFooter.Format += new System.EventHandler(this.ReportFooter_Format);
    this.Detail.Format += new System.EventHandler(this.Detail_Format);
    this.PageHeader.Format += new System.EventHandler(this.PageHeader_Format);
}

		#endregion

private void BindPercent(string all, string payd)
{
    decimal allD = decimal.Parse(all);
    decimal paydD = decimal.Parse(payd);
    decimal per = 100;
    if (allD != 0)
        per = 100 * paydD / allD;
    string Sper = per.ToString("0.00");
    this.txtPaydPercent.Text = string.Concat(Sper, "%");
    per = 100 - per;
    Sper = per.ToString("0.00");
    this.txtNotPaydPercent.Text = string.Concat(Sper, "%");
}
private void SubAnalysisRpt_ReportEnd(object sender, EventArgs e)
{
    if (!this._pdf) return;

    Logo logo = new Logo();
    logo.Run();

    for (int i = 0; i < this.Document.Pages.Count; i++)
        this.Document.Pages[i].Overlay(logo.Document.Pages[0]);
}
private void SubAnalysisRpt1_ReportEnd(object sender, EventArgs e)
{
    if (!this._pdf) return;

    Logo1 logo = new Logo1();
    logo.Run();

    for (int i = 0; i < this.Document.Pages.Count; i++)
        this.Document.Pages[i].Overlay(logo.Document.Pages[0]);
}

        private ReportHeader ReportHeader;
        private TextBox txtTitle;
        private Label lbDate;
        private PageHeader PageHeader;
        private Label Label1;
        private Label Label2;
        private Label Label3;
        private Label Label4;
        private Label Label5;
        private Label Label6;
        private Line Line1;
        private Line Line3;
        private Line Line4;
        private Line Line5;
        private Line Line6;
        private Line Line7;
        private Label Label9;
        private Label Label10;
        private Label Label11;
        private Detail Detail;
        private TextBox txtDiscription;
        private TextBox txtSquareMeters;
        private TextBox txtEURSM;
        private TextBox txtEUR;
        private TextBox txtPayd;
        private TextBox txtNotPayd;
        private Line Line19;
        private Line Line20;
        private Line Line21;
        private Line Line22;
        private Line Line23;
        private Line Line26;
        private TextBox txtPaymentID;
        private TextBox txtClients;
        private TextBox txtOrder;
        private TextBox txtPaydPercent;
        private TextBox txtNotPaydPercent;
        private PageFooter PageFooter;
        private ReportFooter ReportFooter;
        private Label Label7;
        private TextBox txtCreatedBy;
        private Line Line27;
	}
}
