using System;
using System.Data;
using System.Data.SqlClient; 
using System.Collections;
using Microsoft.ApplicationBlocks.Data;
using System.IO;using System.Web;
namespace Asa.Timesheet.Data
{
	public class SubprojectDAL
	{
		/// <summary>
		/// Loads a SubprojectData object from the database using the given SubprojectID
		/// </summary>
		/// <param name="subprojectID">The primary key value</param>
		/// <returns>A SubprojectData object</returns>
		/// 
		
		public static readonly string _PATH="Files/";

		public static void DropFile(int subprojectID)
		{
			if(subprojectID==-1)
				return;
			string sName= string.Concat(subprojectID,".txt");
			string sFile = HttpContext.Current.Server.MapPath( _PATH+ sName);
			if(File.Exists(sFile))
			{
				try
				{
					File.Delete(sFile);
				}
				catch{}
			}
		}
		public static string ReadFile(int subprojectID)
		{
			string sName= string.Concat(subprojectID,".txt");
			string sFile = HttpContext.Current.Server.MapPath( _PATH+ sName);
			try

			{
			if(File.Exists(sFile))
			{
					
				using (StreamReader sr = new StreamReader(sFile,System.Text.Encoding.GetEncoding(1251))) 
				{
					
					return sr.ReadToEnd();
				}
			}
			}
			catch{}
			return "";
		}
		public static void WriteFile(int subprojectID,string val)
		{
			string sName= string.Concat(subprojectID,".txt");
			string sFile = HttpContext.Current.Server.MapPath(_PATH+ sName);

			using(System.IO.StreamWriter w = new 
					  System.IO.StreamWriter(sFile,false,System.Text.Encoding.GetEncoding(1251)))
			{
				try
				{
				w.Write(val);
				w.Flush(); 
					w.Close(); 
				}
				catch{}

			}

			
		}
		public static SubprojectData Load(int subprojectID)
		{
			SqlParameter[] parameterValues = new SqlParameter[] { new SqlParameter("@SubprojectID", subprojectID) };
			using (SqlDataReader reader = SqlHelper.ExecuteReader(CommonDAL.ConnectionString, CommandType.StoredProcedure, "SubprojectsSelProc", parameterValues))
			{
				if (reader.Read())
				{
					SubprojectData result = new SubprojectData();
					LoadFromReader(reader,result);
					return result;
				}
				else
					return null;
			}
		}

		/// <summary>
		/// Loads a SubprojectData object from the database using the given where clause
		/// </summary>
		/// <param name="whereClause">The filter expression for the query</param>
		/// <returns>A SubprojectData object</returns>
		public static SubprojectData Load(string whereClause)
		{
			SqlParameter[] parameterValues = new SqlParameter[] { new SqlParameter("@WhereClause", whereClause) };
			using (SqlDataReader reader = SqlHelper.ExecuteReader(CommonDAL.ConnectionString, CommandType.StoredProcedure, "SubprojectsListProc", parameterValues))
			{
				if (reader.Read())
				{
					SubprojectData result = new SubprojectData();
					LoadFromReader(reader,result);
					return result;
				}
				else
					return null;
			}
		}
		public static int Save( SubprojectData source)
		{
			if (source.SubprojectID == -1)
				return Insert(source);
			else
				return Update(source);	
		}

		public static void Delete( SubprojectData source)
		{
			Delete(source.SubprojectID);
		}
		/// <summary>
		/// Loads a collection of SubprojectData objects from the database.
		/// </summary>
		/// <returns>A collection containing all of the SubprojectData objects in the database.</returns>
		public static SubprojectsVector LoadCollection()
		{
			SqlParameter[] parms = new SqlParameter[] {};
			return LoadCollection("SubprojectsListProc", parms);
		}
	  
		/// <summary>
		/// Saves a collection of SubprojectData objects to the database.
		/// </summary>
		/// <returns>A collection containing all of the SubprojectData objects in the database.</returns>
		public static  void  SaveCollection(SubprojectsVector col,string spName, SqlParameter[] parms)
		{
			SubprojectsVector colOld = LoadCollection(spName,parms);
			UpdateCollection(colOld,col);
		}
		public static  void  SaveCollection(SubprojectsVector col)
		{
			SubprojectsVector colOld = LoadCollection();
			UpdateCollection(colOld,col);
		}
		public static  void  SaveCollection(SubprojectsVector col, string whereClause )
		{
			SubprojectsVector colOld = LoadCollection(whereClause);
			UpdateCollection(colOld,col);
		}
		/// <summary>
		/// Loads a collection of SubprojectData objects from the database.
		/// </summary>
		/// <returns>A collection containing all of the SubprojectData objects in the database.</returns>
		public static SubprojectsVector LoadCollection(string whereClause)
		{
			SqlParameter[] parms = new SqlParameter[] { new SqlParameter("@WhereClause", whereClause) };
			return LoadCollection("SubprojectsListProc", parms);
		}

		/// <summary>
		/// Loads a collection of SubprojectData objects from the database.
		/// </summary>
		/// <returns>A collection containing all of the SubprojectData objects in the database.</returns>
		public static SubprojectsVector LoadCollection(string spName, SqlParameter[] parms)
		{
			SubprojectsVector result = new SubprojectsVector();
			using (SqlDataReader reader = SqlHelper.ExecuteReader(CommonDAL.ConnectionString, CommandType.StoredProcedure, spName, parms))
			{
				while (reader.Read())
				{
					SubprojectData tmp = new SubprojectData();
					LoadFromReader(reader, tmp);
					result.Add(tmp);
				}
			}
			return result;
		}
		/// <summary>
		/// Deletes a SubprojectData object from the database.
		/// </summary>
		/// <param name="subprojectID">The primary key value</param>
		public static void Delete(int subprojectID)
		{
			SqlParameter[] parameterValues = new SqlParameter[] { new SqlParameter("@SubprojectID", subprojectID) };
			SqlHelper.ExecuteNonQuery(CommonDAL.ConnectionString, CommandType.StoredProcedure, "SubprojectsDelProc", parameterValues);
		}

		public static void LoadFromReader(SqlDataReader reader, SubprojectData target)
		{
			if (reader != null && !reader.IsClosed)
			{
				//target.Key = reader.GetInt32(0);
				if (!reader.IsDBNull(0)) target.SubprojectID = reader.GetInt32(0);
				if (!reader.IsDBNull(1)) target.ProjectID = reader.GetInt32(1);
				if (!reader.IsDBNull(2)) target.SubprojectTypeID = reader.GetInt32(2);
				if (!reader.IsDBNull(3)) target.StartDate = reader.GetDateTime(3);
				if (!reader.IsDBNull(4)) target.EndDate = reader.GetDateTime(4);
				if (!reader.IsDBNull(5)) target.Notes = reader.GetString(5);
				if (!reader.IsDBNull(6)) target.PaymentSchemeID = reader.GetInt32(6);
				if (!reader.IsDBNull(7)) target.Area = reader.GetDecimal(7);
				if (!reader.IsDBNull(8)) target.Rate = reader.GetDecimal(8);
				if (!reader.IsDBNull(9)) target.TotalAmount = reader.GetDecimal(9);
				if (!reader.IsDBNull(10)) target.ProjectName = reader.GetString(10);
				if (!reader.IsDBNull(11)) target.ProjectCode = reader.GetString(11);
				if (!reader.IsDBNull(12)) target.SubprojectType = reader.GetString(12);

				string s= ReadFile(target.SubprojectID);
				if(s!="")
					target.Notes=s;
			}
			
		}
		
	  
		public static int Insert( SubprojectData source)
		{
			string note = source.Notes;
			if(note.Length>=Constants.MaxNotes)
			{
				source.Notes="";
				WriteFile(source.SubprojectID,note);
			}
			else
				DropFile(source.SubprojectID);
			SqlParameter[] parameterValues = GetInsertParameterValues(source);

			SqlHelper.ExecuteNonQuery(CommonDAL.ConnectionString, CommandType.StoredProcedure, "SubprojectsInsProc", parameterValues);
			source.SubprojectID = (int) parameterValues[0].Value;
			return source.SubprojectID;
		}

		public static int Update( SubprojectData source)
		{
			string note = source.Notes;
			if(note.Length>=Constants.MaxNotes)
			{
				source.Notes="";
				WriteFile(source.SubprojectID,note);
			}
			else
				DropFile(source.SubprojectID);
			SqlParameter[] parameterValues = GetUpdateParameterValues(source);
			SqlHelper.ExecuteNonQuery(CommonDAL.ConnectionString, CommandType.StoredProcedure,
				"SubprojectsUpdProc", parameterValues);
			return source.SubprojectID;
		}
		private static void UpdateCollection(SubprojectsVector colOld,SubprojectsVector col)
		{
			// Delete all old items
			foreach( SubprojectData itemOld in colOld)
			{
				if(!ExistsKey(itemOld.SubprojectID, col))
					Delete(itemOld.SubprojectID);
			}
			foreach( SubprojectData item in col)
			{
				if(item.SubprojectID <= -1)
					Insert(item);
				else
					Update(item);
			}
		}
		private static bool ExistsKey(int Key, SubprojectsVector col)
		{
			foreach( SubprojectData item in col)
				if(item.SubprojectID==Key)
					return true;
			return false;
		}

		private static SqlParameter GetSqlParameter(string name, ParameterDirection direction, object value)
		{
			SqlParameter p = new SqlParameter(name, value);
			p.Direction = direction;
			return p;
		}

		private static SqlParameter[] GetInsertParameterValues( SubprojectData source)
		{
			SqlParameter[] prams = new SqlParameter[10];
			prams[0] = GetSqlParameter("@SubprojectID", ParameterDirection.Output, source.SubprojectID);
			prams[1] = GetSqlParameter("@ProjectID", ParameterDirection.Input, source.ProjectID);
			prams[2] = GetSqlParameter("@SubprojectTypeID", ParameterDirection.Input, source.SubprojectTypeID);
			if(source.StartDate==Constants.DateMax)
				prams[3] =GetSqlParameter("@StartDate", ParameterDirection.Input, System.DBNull.Value);
			else
				prams[3] = GetSqlParameter("@StartDate", ParameterDirection.Input, source.StartDate);
			if(source.EndDate==Constants.DateMax)
				prams[4] =GetSqlParameter("@EndDate", ParameterDirection.Input, System.DBNull.Value);
			else
				prams[4] = GetSqlParameter("@EndDate", ParameterDirection.Input, source.EndDate);
			prams[5] = GetSqlParameter("@Notes", ParameterDirection.Input, source.Notes);
			prams[6] = GetSqlParameter("@PaymentSchemeID", ParameterDirection.Input, source.PaymentSchemeID);
			prams[7] = GetSqlParameter("@Area", ParameterDirection.Input, source.Area);
			prams[8] = GetSqlParameter("@Rate", ParameterDirection.Input, source.Rate);
			prams[9] = GetSqlParameter("@TotalAmount", ParameterDirection.Input, source.TotalAmount);
			return prams;
		}

		private static SqlParameter[] GetUpdateParameterValues( SubprojectData source)
		{
			SqlParameter[] prm = GetInsertParameterValues(source);
			prm[0].Direction=  ParameterDirection.Input;
			return prm;
//			return new SqlParameter[] {
//										  new SqlParameter("@SubprojectID", source.SubprojectID),
//										  new SqlParameter("@ProjectID", source.ProjectID),
//										  new SqlParameter("@SubprojectTypeID", source.SubprojectTypeID),
//										  new SqlParameter("@StartDate", source.StartDate),
//										  new SqlParameter("@EndDate", source.EndDate),
//										  new SqlParameter("@Notes", source.Notes),
//										  new SqlParameter("@PaymentSchemeID", source.PaymentSchemeID),
//										  new SqlParameter("@Area", source.Area),
//										  new SqlParameter("@Rate", source.Rate),
//										  new SqlParameter("@TotalAmount", source.TotalAmount)
//									  };
		}
	}
}   

