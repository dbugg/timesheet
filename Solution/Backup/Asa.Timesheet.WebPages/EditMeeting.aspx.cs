using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Data.SqlClient;
using log4net;
using Asa.Timesheet.Data;
using Asa.Timesheet.Data.Entities;
using Asa.Timesheet.Data.Util;
using GrapeCity.ActiveReports.Export.Pdf.Section;
using GrapeCity.ActiveReports.Export.Excel.Section;
using Asa.Timesheet.WebPages.Reports;

using System.IO;
using System.Configuration;
using GrapeCity.ActiveReports.Export.Word.Section;

namespace Asa.Timesheet.WebPages
{
	/// <summary>
	/// Summary description for EditMeeting.
	/// </summary>
	public class EditMeeting : TimesheetPageBase
	{
		#region WebControls
		private static readonly ILog log = LogManager.GetLogger(typeof(EditMeeting));
		protected System.Web.UI.WebControls.PlaceHolder menuHolder;
		protected System.Web.UI.WebControls.Label lblName;
		protected System.Web.UI.WebControls.Button btnSave;
		protected System.Web.UI.WebControls.Button btnEdit;
		protected System.Web.UI.WebControls.Button btnCancel;
		protected System.Web.UI.WebControls.Label lblInfo;
		protected System.Web.UI.WebControls.TextBox txtStartDate;
		protected System.Web.UI.HtmlControls.HtmlImage lkCalendar1;
		protected System.Web.UI.WebControls.DropDownList ddlStartTime;
		protected System.Web.UI.WebControls.Label Label1;
		protected System.Web.UI.WebControls.Label Label2;
		protected System.Web.UI.WebControls.Label Label3;
		protected System.Web.UI.WebControls.ListBox lbUsers;
		protected System.Web.UI.WebControls.ListBox lbSelectedUsers;
		protected System.Web.UI.HtmlControls.HtmlInputText hdnlbUsers;
		protected System.Web.UI.HtmlControls.HtmlInputText hdnlbSelectedUsers;
		protected System.Web.UI.HtmlControls.HtmlInputText hdn1lbSelectedUsers;
		protected System.Web.UI.WebControls.ListBox lbClients;
		protected System.Web.UI.WebControls.ListBox lbSelectedClients;
		protected System.Web.UI.HtmlControls.HtmlInputText hdnlbClients;
		protected System.Web.UI.HtmlControls.HtmlInputText hdnlbSelectedClients;
		protected System.Web.UI.HtmlControls.HtmlInputText hdn1lbSelectedClients;
		protected System.Web.UI.WebControls.DropDownList ddlProject;
		protected System.Web.UI.WebControls.DropDownList ddlProjectsStatus;
		protected System.Web.UI.WebControls.DropDownList ddlBuildingTypes;
		protected System.Web.UI.WebControls.Label Label8;
		protected System.Web.UI.WebControls.Label lblError;
		protected System.Web.UI.WebControls.ListBox lbUsersMails;
		protected System.Web.UI.WebControls.ListBox lbSelectedUsersMails;
		protected System.Web.UI.HtmlControls.HtmlInputText hdnlbUsersMails;
		protected System.Web.UI.HtmlControls.HtmlInputText hdnlbSelectedUsersMails;
		protected System.Web.UI.WebControls.DropDownList ddlEndTime;
		protected System.Web.UI.WebControls.Label Label5;
		protected System.Web.UI.WebControls.Label lbDate;
		protected System.Web.UI.WebControls.Label lbStart;
		protected System.Web.UI.WebControls.Label lbEnd;
		protected System.Web.UI.WebControls.CheckBox ckApproved;
		protected Asa.Timesheet.WebPages.UserControls.PageHeader header;
		protected System.Web.UI.HtmlControls.HtmlInputFile FileCtrl;
		protected System.Web.UI.WebControls.HyperLink btnScan;
		protected System.Web.UI.WebControls.Label Label7;
		protected System.Web.UI.WebControls.Label Label9;
		protected System.Web.UI.WebControls.TextBox txtPlace;
		protected System.Web.UI.WebControls.TextBox txtOtherPeople;
		protected System.Web.UI.WebControls.TextBox txtNotes;
		protected System.Web.UI.WebControls.Label lbPlace;
		protected System.Web.UI.WebControls.ListBox lbSelectedMeetingProjects;
		protected System.Web.UI.WebControls.ListBox lbMeetingProjects;
		protected System.Web.UI.HtmlControls.HtmlInputText hdnlbMeetingProjects;
		protected System.Web.UI.HtmlControls.HtmlInputText hdnlbSelectedMeetingProjects;
		protected System.Web.UI.HtmlControls.HtmlInputText hdnlbMeetingProjects1;
		protected System.Web.UI.WebControls.ListBox lbMeetingProjects1;
		protected System.Web.UI.WebControls.Label lbProjects;
		protected System.Web.UI.WebControls.Button btnReload;
		protected System.Web.UI.WebControls.Label Label6;
		protected System.Web.UI.WebControls.DataList dlDocs;
		protected System.Web.UI.WebControls.Button btnScanDocAdd;
		protected System.Web.UI.WebControls.Button btnDel;
		protected System.Web.UI.WebControls.Button btnDelete;
		protected System.Web.UI.WebControls.ImageButton ibPdfExport;
		protected System.Web.UI.WebControls.Label Label4;
		protected System.Web.UI.WebControls.Label lbHotIssueIme;
		protected System.Web.UI.WebControls.Label Label10;
		protected System.Web.UI.WebControls.DropDownList Dropdownlist1;
		protected System.Web.UI.WebControls.Label Label11;
		protected System.Web.UI.WebControls.DropDownList Dropdownlist2;
		protected System.Web.UI.WebControls.Label lbProjectName;
		protected System.Web.UI.WebControls.DropDownList Dropdownlist3;
		protected System.Web.UI.WebControls.Label lbHotIssueCategory;
		//protected System.Web.UI.WebControls.DropDownList ddlHotIssueCategory;
		protected System.Web.UI.WebControls.Label lbDiscription;
		protected System.Web.UI.WebControls.Label lbComment;
		protected System.Web.UI.WebControls.TextBox txtComment;
		protected System.Web.UI.WebControls.Label lbAssigned;
		protected System.Web.UI.WebControls.DropDownList ddlAssigned;
		protected System.Web.UI.WebControls.Label lbDeadline;
		protected System.Web.UI.WebControls.TextBox txtDeadline;
		protected System.Web.UI.WebControls.Label lbMainID;
//		protected System.Web.UI.WebControls.Label lbPriority;
//		protected System.Web.UI.WebControls.DropDownList ddlPriority;
		protected System.Web.UI.WebControls.Label lbStatus;
		protected System.Web.UI.WebControls.DropDownList ddlStatus;
		protected System.Web.UI.HtmlControls.HtmlImage Img4;
		protected System.Web.UI.HtmlControls.HtmlImage Img3;
		//protected System.Web.UI.HtmlControls.HtmlImage Img2;
		protected System.Web.UI.HtmlControls.HtmlImage Img1;
		protected System.Web.UI.HtmlControls.HtmlImage Img5;
		protected System.Web.UI.HtmlControls.HtmlImage imgRequired;
		protected System.Web.UI.HtmlControls.HtmlImage Img6;
		protected System.Web.UI.HtmlControls.HtmlImage Img7;
		protected System.Web.UI.WebControls.DataList dlHotIssues;
		protected System.Web.UI.WebControls.Label lbIssues;
		protected System.Web.UI.HtmlControls.HtmlTableCell HotIssueDatas;
		protected Asa.Timesheet.WebPages.UserControls.EditForm editCtrl;
		protected System.Web.UI.WebControls.Button btnEditUP;
		protected System.Web.UI.WebControls.Button btnSaveUP;
		protected System.Web.UI.WebControls.Button btnMinutes;
		protected System.Web.UI.WebControls.Button btnMinutesInv;
		protected System.Web.UI.WebControls.Label lb;
		protected System.Web.UI.WebControls.TextBox txtEmails;
		protected System.Web.UI.WebControls.Button btnNew1;
		protected System.Web.UI.WebControls.HyperLink lkMails;
		protected System.Web.UI.WebControls.CheckBox ckInclude;
		protected System.Web.UI.WebControls.Label lbError;
		protected System.Web.UI.WebControls.ImageButton btnExcel;
		protected System.Web.UI.WebControls.ImageButton btnWord;
		protected System.Web.UI.WebControls.CheckBoxList lst;
		protected System.Web.UI.WebControls.CheckBox ckAll;
		protected System.Web.UI.WebControls.Button btnOnlyMeeting;
		protected System.Web.UI.WebControls.Button btnPDFEN;
		protected System.Web.UI.WebControls.DropDownList ddlProject1;
		protected System.Web.UI.WebControls.Label Label17;
		protected System.Web.UI.WebControls.TextBox txtPlaceEN;
		protected System.Web.UI.WebControls.Button btnOnlyMeetingEN;
		protected System.Web.UI.WebControls.Button btnMinutesInvEN;
		protected System.Web.UI.WebControls.Button btnEditIssues;
		protected System.Web.UI.WebControls.Label Label18;
		protected System.Web.UI.WebControls.Label Label19;
		protected System.Web.UI.WebControls.Label Label20;
		protected System.Web.UI.WebControls.Button btnCancelUP;
		protected System.Web.UI.WebControls.Button btnWord1;
		protected System.Web.UI.WebControls.Label Label21;
		
		#endregion

		#region Page_Load

        private void Page_Load(object sender, System.EventArgs e)
        {

            lkCalendar1.Attributes["onclick"] = TimeHelper.InvokePopupCal(txtStartDate);
            if (!this.IsPostBack)
            {
                //ddlProjectsStatus.SelectedIndex=1;
                if (Page.IsPostBack == false)
                {
                    // Set up form for data change checking when
                    // first loaded.
                    this.CheckForDataChanges = true;
                    this.BypassPromptIds =
                        new string[] { "ckAll", "ckInclude", "ibPdfExport", "btnDel", "btnScanDocAdd", "btnReload", "btnSave", "btnEdit", "btnSaveUP", "btnEditUP", "btnDelete", "ddlProject", "ddlBuildingTypes", "ddlProjectsStatus", "btnReloadIssues", "btnSendEmail", "btnMinutes", "btnPDFEN", "btnMinutesInv", "btnOnlyMeeting" };
                }
                UIHelpers.LoadProjectStatus(ddlProjectsStatus);

                if (UIHelpers.GetIDParam() <= 0)
                {

                    header.PageTitle = Resource.ResourceManager["newMeeting_PageTitle"];
                    ddlProjectsStatus.SelectedValue = ((int)ProjectsData.ProjectsByStatus.Active).ToString();
                }
                else
                {
                    header.PageTitle = Resource.ResourceManager["editMeeting_PageTitle"];
                    ddlProjectsStatus.SelectedValue = ((int)ProjectsData.ProjectsByStatus.AllProjects).ToString();
                }
                header.UserName = this.LoggedUser.UserName;
                //				

                UIHelpers.LoadBuildingTypes("", ddlBuildingTypes);
                LoadProjects();
                ArrayList times = LoadTimes();

                ddlStartTime.DataSource = times;
                ddlStartTime.DataValueField = "TimeID";
                ddlStartTime.DataTextField = "TimeString";
                ddlStartTime.DataBind();
                ddlEndTime.DataSource = times;
                ddlEndTime.DataValueField = "TimeID";
                ddlEndTime.DataTextField = "TimeString";
                ddlEndTime.DataBind();


                LoadUsers();
                LoadMeetingProjects(true);
                ReloadListsFromProjects(true, int.Parse(ddlProject.SelectedValue));

                //ReloadLists();
                //BindGrid();

                if (UIHelpers.GetIDParam() > 0)
                {
                    LoadFromID(UIHelpers.GetIDParam());
                    //Load Categories
                    int nID = int.Parse(ddlProject.SelectedValue);
                    //LoadHotIssueCategories(nID);
                    //ReloadLists();
                    BindGrid();
                    //LoadHotIssues();

                    string meetingNumber = "";
                    if (nID > 0)
                        meetingNumber = MeetingDAL.SelectMeetingNumber(nID, UIHelpers.GetDate(txtStartDate));
                    string subject = ddlProject.SelectedItem.Text + ": PCM" + meetingNumber;
                    string body = "";

                    string mails = MailBO.GetMails(UIHelpers.GetIDParam(), out body);
                    lkMails.NavigateUrl = "mailto:" + mails + "?subject=" + subject + "&body=" + body;
                }
                else
                {
                    btnMinutes.Visible = btnMinutesInv.Visible = false;
                    btnSave.Text = Resource.ResourceManager["btnNewMeeting"];
                    btnSaveUP.Text = Resource.ResourceManager["btnNewMeeting"];
                    //UIHelpers.SetSelected(LoggedUser.UserID.ToString(), lbUsersMails, lbSelectedUsersMails);									
                    hdnlbSelectedUsersMails.Value = LoggedUser.UserID.ToString();
                    btnScanDocAdd.Visible = false;
                }
                //				if(!LoggedUser.IsLeader)
                //					HotIssueDatas.Visible= false;

                //				if(!LoggedUser.IsLeader)
                //					//btnSendEmail.Visible = false;
                //				else
                //				{
                //					//btnSendEmail.Visible = true;
                //					//SetConfirmButtonSendMail(btnSendEmail);
                //				}
                InitEdit();

            }


            int nPID = UIHelpers.GetPIDParam();
            if (UIHelpers.GetIDParam() < 0 && nPID > 0 && !IsPostBack)
            {
                ddlProject1.SelectedValue = ddlProject.SelectedValue = nPID.ToString();
                if (ddlProject1.Items.FindByValue(nPID.ToString()) != null)
                    ddlProject1.SelectedValue = nPID.ToString();
                LoadMeetingProjects(false);
                IsMainProjectSelected(false);
                ReloadListsFromProjects(false, int.Parse(ddlProject.SelectedValue));
                RemoveMainProjectFromProjects(false);

                //LoadHotIssueCategories(nPID);
                //ReloadLists();
                BindGrid();
                //LoadHotIssues();

            }

            UIHelpers.CreateMenu(menuHolder, LoggedUser);
        }

        protected string GetTextToClient(bool toClient)
        {
            if (toClient)
                return Resource.ResourceManager["Reports_Yes"];
            else
                return Resource.ResourceManager["Reports_No"];
        }
        private void LoadFromID(int ID)
        {
            string s = "";
            MeetingData md = MeetingDAL.Load(ID);
            MeetingProjectsVector mpv = MeetingProjectDAL.LoadCollection("MeetingProjectsSelByMeetingProc", SQLParms.CreateMeetingProjectsSelByMeetingProc(ID));
            foreach (MeetingProjectData mpd in mpv)
            {
                s += ";";
                s += mpd.ProjectID.ToString();
            }
            lbSelectedMeetingProjects.Items.Clear();
            //ddlProject1.Items.Clear();
            //ddlProject1.Items.Add(new ListItem(md.ProjectName,md.ProjectID.ToString()));
            UIHelpers.SetSelected(s, lbMeetingProjects1, lbSelectedMeetingProjects);
            hdnlbSelectedMeetingProjects.Value = s;
            IsMainProjectSelected(true);
            lbMeetingProjects.Items.Clear();
            ListItem li;
            for (int i = 0; i < lbMeetingProjects1.Items.Count; i++)
            {
                li = lbMeetingProjects1.Items[i];
                lbMeetingProjects.Items.Add(li);
                //if(li.Value!="0")
                //ddlProject1.Items.Add(li);
            }
            hdnlbMeetingProjects.Value = hdnlbMeetingProjects1.Value;

            ReloadListsFromProjects(false, md.ProjectID);
            RemoveMainProjectFromProjects(true);


            if (md == null)
                ErrorRedirect("Error load meeting.");
            if (ddlProject.Items.FindByValue(md.ProjectID.ToString()) != null)
                ddlProject.SelectedValue = md.ProjectID.ToString();
            if (ddlProject1.Items.FindByValue(md.ProjectID.ToString()) != null)
                ddlProject1.SelectedValue = md.ProjectID.ToString();
            txtStartDate.Text = TimeHelper.FormatDate(md.MeetingDate);
            if (ddlStartTime.Items.FindByValue(md.StartTimeID.ToString()) != null)
                ddlStartTime.SelectedValue = md.StartTimeID.ToString();
            if (ddlEndTime.Items.FindByValue(md.EndTimeID.ToString()) != null)
                ddlEndTime.SelectedValue = md.EndTimeID.ToString();
            ckApproved.Checked = md.Approved;

            txtPlace.Text = md.Place;
            txtPlaceEN.Text = md.PlaceEN;
            txtNotes.Text = md.Notes;
            txtOtherPeople.Text = md.OtherPeople;
            txtEmails.Text = md.OtherMails;
            ckInclude.Checked = md.ASA;
            if (md.HasScan)
                txtEmails.Enabled = false;
            //				if(md.HasScan)
            //				{
            //					if(!LoggedUser.IsLeader)
            //					{
            //							FileCtrl.Visible=false;
            //							btnScanDocAdd.Visible=false;
            //					}	
            //									btnScan.NavigateUrl=
            //										string.Concat(System.Configuration.ConfigurationManager.AppSettings["ScanPath"],ID,System.Configuration.ConfigurationManager.AppSettings["Extension"]);
            //									btnScan.Visible=true;
            //				}

            lbSelectedClients.Items.Clear();
            lbSelectedUsers.Items.Clear();
            lbSelectedUsersMails.Items.Clear();
            UIHelpers.SetSelected(md.Clients, lbClients, lbSelectedClients);
            UIHelpers.SetSelected(md.Subcontracters, lbUsers, lbSelectedUsers);
            UIHelpers.SetSelected(md.Users, lbUsersMails, lbSelectedUsersMails);
            hdnlbSelectedClients.Value = md.Clients;
            hdnlbSelectedUsers.Value = md.Subcontracters;
            hdnlbSelectedUsersMails.Value = md.Users;
            if (!LoggedUser.HasPaymentRights)
            { HideSubcontractors(); }


        }
		#endregion

		#region //ReloadListFromOneProject
//		private void ReloadLists()
//		{
//			int nProjectID=int.Parse(ddlProject.SelectedValue);
//			SubcontractersVector sv=new SubcontractersVector(); 
//			ClientsVector cv = new ClientsVector();
//			if(nProjectID>0)
//			{
//				SubcontractersVector ssv= SubcontracterDAL.LoadCollection();	
//				
//				ProjectSubcontractersVector psv= ProjectSubcontracterDAL.LoadCollection("ProjectSubcontractersListProc",SQLParms.CreateProjectSubcontractersListProc (nProjectID));
//
//				foreach(ProjectSubcontracterData psd in psv)
//				{
//					sv.Add(ssv.GetByID(psd.SubcontracterID));
//				}
//				string projectName, projectCode, administrativeName,add;
//				decimal area=0;
//				
//				int clientID=0;
//				bool phases;
//				if (!ProjectsData.SelectProject(nProjectID, out projectName, out  projectCode, out administrativeName, out area, out add, 
//					out clientID,out phases))
//				{
//					log.Error("Project not found " + nProjectID);
//					return;
//				
//				}
//				ClientData cd = ClientDAL.Load(clientID);
//				if(cd!=null)
//					cv.Add(cd);
//			}
//			else
//			{
//				sv=SubcontracterDAL.LoadCollection();
//				cv=ClientDAL.LoadCollection();
//			}
//			lbUsers.DataSource = sv;
//			lbUsers.DataValueField = "SubcontracterID";
//			lbUsers.DataTextField = "SubcontracterNameAndAll";
//			lbUsers.DataBind();
//			lbClients.DataSource = cv;
//			lbClients.DataValueField = "ClientID";
//			lbClients.DataTextField = "ClientNameAndAll";
//			lbClients.DataBind();
//		}
		#endregion
		
		#region //BindGrid
//		private void BindGrid()
//		{
//			SqlDataReader reader = null;
//
//			try
//			{
//				reader = ClientsData.SelectClients(SortOrder);
//				if (reader!=null)
//				{
//					lbClients.DataSource = reader;
//					lbClients.DataValueField = "ClientID";
//					lbClients.DataTextField = "ClientName";
//					lbClients.DataBind();
//				
//				}
//			}
//			catch (Exception ex)
//			{
//				log.Error(ex);
//				
//				return;
//			}
//
//			finally
//			{
//				if (reader!=null) reader.Close();
//			}
//
//			
//			
//		}
		#endregion
        private void BindIngeners()
        {

            SqlDataReader reader = null;
            try
            {
                string nameOfProcedure = "UsersListIngenerProc";
                reader = UsersData.SelectUsersSplit(Constants.DirectorRole, 1, nameOfProcedure, -1);

                if (reader != null)
                {
                    while (reader.Read())
                    {
                        ListItem li = new ListItem((string)reader["FullName"], ((int)reader["UserID"]).ToString());
                        lbUsersMails.Items.Add(li);

                    }
                }
            }
            catch (Exception ex)
            {
                log.Error(ex);
                lblError.Text = Resource.ResourceManager["users_ErrorLoadUsers"];
                return;
            }
            finally
            {
                if (reader != null) reader.Close();
            }


        }
        private void BindArchitects()
        {
            SqlDataReader reader = null;
            try
            {
                string nameOfProcedure = "UsersListArcgitectProc";
                reader = UsersData.SelectUsersSplit(Constants.DirectorRole, 1, nameOfProcedure, -1);

                if (reader != null)
                {
                    while (reader.Read())
                    {
                        ListItem li = new ListItem((string)reader["FullName"], ((int)reader["UserID"]).ToString());
                        lbUsersMails.Items.Add(li);

                    }
                }

            }
            catch (Exception ex)
            {
                log.Error(ex);
                lblError.Text = Resource.ResourceManager["users_ErrorLoadUsers"];
                return;
            }
            finally
            {
                if (reader != null) reader.Close();
            }



        }
        private bool LoadUsers()
        {
            BindArchitects();
            BindIngeners();
            //if (this.LoggedUser.IsLeader)
            //		{
            //			
            //
            //			SqlDataReader reader = null;
            //			try
            //			{
            //				reader = UsersData.SelectUserNames(false, true,true);
            //				lbUsersMails.DataSource = reader;
            //				lbUsersMails.DataValueField = "UserID";
            //				lbUsersMails.DataTextField = "FullName";
            //				lbUsersMails.DataBind();
            //			}
            //			catch (Exception ex)
            //			{
            //				log.Error(ex);
            //				return false;
            //			}
            //			finally
            //			{
            //				if (reader!=null) reader.Close();
            //			}
            //		}

            return true;
        }
		public ProjectsData.ProjectsByStatus ProjectsStatus
		{
			get
			{
				try
				{
					return  (ProjectsData.ProjectsByStatus)int.Parse(ddlProjectsStatus.SelectedValue);
				}
				catch { return ProjectsData.ProjectsByStatus.AllProjects; }
			}
		}
        private ArrayList LoadTimes()
        {
            ArrayList times = new ArrayList();

            SqlDataReader reader = null;

            try
            {
                reader = DBManager.SelectTimes();

                while (reader.Read())
                {
                    TimeInfo ti = new TimeInfo(reader.GetInt32(0), reader.GetString(1));
                    times.Add(ti);
                }
            }
            catch (Exception ex)
            {
                log.Error(ex);
                ErrorRedirect("Error load meeting.");
            }
            finally
            {
                if (reader != null) reader.Close();
            }

            times.Insert(0, new TimeInfo(0, ""));
            return times;
        }
        private bool LoadProjects()
        {
            return UIHelpers.LoadProjects(ddlProject, ddlProjectsStatus, ddlBuildingTypes);
            //			SqlDataReader reader = null;
            //			
            //			try
            //			{
            //				reader = ProjectsData.SelectProjectNamesClear(this.ProjectsStatus, int.Parse(ddlBuildingTypes.SelectedValue));
            //			
            //				ddlProject.DataSource = reader;
            //				ddlProject.DataValueField = "ProjectID";
            //				ddlProject.DataTextField = "ProjectName";
            //				ddlProject.DataBind();
            //
            //				ddlProject.Items.Insert(0, new ListItem("<"+Resource.ResourceManager["reports_ddlAllProjects"]+">", "-1"));
            //				ddlProject.SelectedValue = "-1";
            //			}
            //			catch (Exception ex)
            //			{
            //				log.Error(ex);
            //				return false;
            //			}
            //
            //			finally
            //			{
            //				if (reader!=null) reader.Close();
            //			}
            //			return true;
        }

        private bool IsWorkDayEntered(int User, DateTime dt)
        {
            try
            {
                return WorkTimesData.IsWorkDayEntered(User,
                    dt,
                    false);
            }
            catch (Exception ex)
            {
                log.Error(ex);
                ErrorRedirect("Error.");
                return true;
            }
        }
        private void InitEdit()
        {


            if (UIHelpers.GetIDParam() <= 0) return;

            string vmButtons = this.btnEdit.ClientID + ";" + this.btnEditUP.ClientID;
            string emButtons = this.btnSave.ClientID + ";" + this.btnSaveUP.ClientID + ";" + this.btnDelete.ClientID;//+";"+this.btnSendEmail.ClientID;

            string exl = string.Empty;

            editCtrl.InitEdit("tblForm;HotIssueDatas;btnSendEmail", vmButtons, emButtons, exl);
        }
		//Show Subcontractors and Clients from selected projects 
        private void ReloadListsFromProjects(bool FromPageLoad, int ProjectID)
        {
            LoadMeetingProjectsChanges();

            //reload minutes dropdown


            ddlProject1.Items.Clear();
            //ddlProject1.Items.Add(new ListItem(ddlProject.SelectedItem.Text, ddlProject.SelectedValue));


            for (int i = 0; i < lbSelectedMeetingProjects.Items.Count; i++)
            {
                ListItem lim = lbSelectedMeetingProjects.Items[i];
                if (lim.Value != "0")
                    ddlProject1.Items.Add(lim);
            }
            if (ddlProject1.Items.FindByValue(ddlProject.SelectedValue) != null)
                ddlProject1.SelectedValue = ddlProject.SelectedValue;
            string[] arr = hdnlbSelectedMeetingProjects.Value.Split(';');
            SubcontractersVector sv = new SubcontractersVector();
            ClientsVector cv = new ClientsVector();

            if ((hdnlbSelectedMeetingProjects.Value != null) && (hdnlbSelectedMeetingProjects.Value != ""))
            {
                SubcontractersVector ssv = SubcontracterDAL.LoadCollection();
                ProjectSubcontractersVector psv = new ProjectSubcontractersVector();
                foreach (string s in arr)
                {
                    int nProjectID = UIHelpers.ToInt(s);
                    psv = ProjectSubcontracterDAL.LoadCollection("ProjectSubcontractersListProc", SQLParms.CreateProjectSubcontractersListProc(nProjectID));
                    foreach (ProjectSubcontracterData psd in psv)
                    {
                        bool Ispsd = true;
                        foreach (SubcontracterData psd1 in sv)
                        {

                            if (psd.SubcontracterID == psd1.SubcontracterID) Ispsd = false;
                        }
                        SubcontracterData sdd = ssv.GetByID(psd.SubcontracterID);
                        if (Ispsd && sdd != null) sv.Add(sdd);

                    }
                }

                string projectName, projectCode, administrativeName, add;
                decimal area = 0;
                int n = arr.Length;
                int[] clientID = new int[n];
                bool phases; bool isPUP;
                int i = 0;
                foreach (string s in arr)
                {
                    int nProjectID = UIHelpers.ToInt(s);
                    if (!ProjectsData.SelectProject(nProjectID, out projectName, out  projectCode, out administrativeName, out area, out add,
                        out clientID[i], out phases, out isPUP))
                    {
                        if (nProjectID != -1)
                            log.Error("Project not found " + nProjectID);

                    }
                    i++;
                }
                for (i = 0; i < clientID.Length; i++)
                {
                    for (int j = i + 1; j < clientID.Length; j++)
                    {
                        if (clientID[i] == clientID[j]) clientID[i] = -1;
                    }
                }
                foreach (int index in clientID)
                    if (index != -1)
                    {
                        ClientData cd = ClientDAL.Load(index);
                        if (cd != null)
                            cv.Add(cd);
                    }
            }
            else
            {
                if (FromPageLoad)
                {
                    sv = SubcontracterDAL.LoadCollection();
                    cv = ClientDAL.LoadCollection((int)ClientTypes.Client);
                }
            }
            lbUsers.DataSource = sv;
            lbUsers.DataValueField = "SubcontracterID";
            lbUsers.DataTextField = "SubcontracterNameFirst";
            lbUsers.DataBind();
            foreach (ListItem li in lbSelectedUsers.Items)
            { lbUsers.Items.Remove(li); }

            //int ProjectID=int.Parse(ddlProject.SelectedValue);
            if (ProjectID > 0)
            {

                ProjectClientsVector pcv = ProjectClientDAL.LoadCollection("ProjectClientsSelByProjectProc", SQLParms.CreateProjectClientsSelByProjectProc(ProjectID, (int)ClientTypes.Builder));

                ProjectClientsVector pcv1 = ProjectClientDAL.LoadCollection("ProjectClientsSelByProjectProc", SQLParms.CreateProjectClientsSelByProjectProc(ProjectID, (int)ClientTypes.Supervisors));
                ProjectClientsVector pcv2 = ProjectClientDAL.LoadCollection("ProjectClientsSelByProjectProc", SQLParms.CreateProjectClientsSelByProjectProc(ProjectID, (int)ClientTypes.Client));
                ProjectClientsVector pcv3 = ProjectClientDAL.LoadCollection("ProjectClientsSelByProjectProc", SQLParms.CreateProjectClientsSelByProjectProc(ProjectID, (int)ClientTypes.Brokers));
                ProjectClientsVector pcv4 = ProjectClientDAL.LoadCollection("ProjectClientsSelByProjectProc", SQLParms.CreateProjectClientsSelByProjectProc(ProjectID, (int)ClientTypes.ProjectManagers));
                pcv.AddRange(pcv1);
                pcv.AddRange(pcv2);
                pcv.AddRange(pcv3);
                pcv.AddRange(pcv4);
                foreach (ProjectClientData pcd in pcv)
                {
                    ClientData cd = ClientDAL.Load(pcd.ClientID);
                    if (cd != null)
                        cv.Add(cd);
                }
            }
            lbClients.DataSource = cv;
            lbClients.DataValueField = "ClientID";
            lbClients.DataTextField = "ClientNameFirst";
            lbClients.DataBind();
            foreach (ListItem li in lbSelectedClients.Items)
            {
                lbClients.Items.Remove(li);
            }
            if (!LoggedUser.HasPaymentRights)
            { HideSubcontractors(); }
        }
		//5 new, for binding MeetingProjects Database
        private bool LoadMeetingProjects(bool FromPageLoad)
        {
            SqlDataReader reader = null;
            try
            {
                reader = ProjectsData.SelectProjectNamesClear(this.ProjectsStatus, int.Parse(ddlBuildingTypes.SelectedValue));
                lbMeetingProjects.DataSource = reader;
                lbMeetingProjects.DataValueField = "ProjectID";
                lbMeetingProjects.DataTextField = "ProjectName";
                lbMeetingProjects.DataBind();
                if (FromPageLoad)
                    LoadMeetingProjects1();
                else
                    LoadMeetingProjectsChanges();
            }
            catch (Exception ex)
            {
                log.Error(ex);
                return false;
            }
            finally
            {
                if (reader != null) reader.Close();
            }
            return true;
        }

        private bool LoadMeetingProjects1()
        {
            SqlDataReader reader = null;
            try
            {
                reader = ProjectsData.SelectProjectNamesClear(this.ProjectsStatus, int.Parse(ddlBuildingTypes.SelectedValue));
                lbMeetingProjects1.DataSource = reader;
                lbMeetingProjects1.DataValueField = "ProjectID";
                lbMeetingProjects1.DataTextField = "ProjectName";
                lbMeetingProjects1.DataBind();

            }
            catch (Exception ex)
            {
                log.Error(ex);
                return false;
            }
            finally
            {
                if (reader != null) reader.Close();
            }
            return true;
        }

        private void LoadMeetingProjectsChanges()
        {
            LoadMeetingProjects1();
            string str = hdnlbSelectedMeetingProjects.Value;
            lbSelectedMeetingProjects.Items.Clear();
            UIHelpers.SetSelected(str, lbMeetingProjects1, lbSelectedMeetingProjects);
            foreach (ListItem li in lbSelectedMeetingProjects.Items)
            {
                lbMeetingProjects.Items.Remove(li);
            }
        }
        private void IsMainProjectSelected(bool FromPageLoad)
        {
            if (FromPageLoad)
            {
                MeetingData md = MeetingDAL.Load(UIHelpers.GetIDParam());
                String MainProject = md.ProjectID.ToString();
                bool IsSelected = false;
                string[] arr = hdnlbSelectedMeetingProjects.Value.Split(';');
                foreach (string s in arr)
                {
                    if (MainProject == s) IsSelected = true;
                }
                if (!IsSelected)
                {
                    hdnlbSelectedMeetingProjects.Value += ";";
                    hdnlbSelectedMeetingProjects.Value += MainProject;
                    LoadMeetingProjectsChanges();
                }

            }
            else
            {
                if (ddlProject.SelectedIndex != 0)
                {
                    bool IsSelected = false;
                    string MainProject = ddlProject.SelectedValue.ToString();
                    string[] arr = hdnlbSelectedMeetingProjects.Value.Split(';');
                    foreach (string s in arr)
                    {
                        if (MainProject == s) IsSelected = true;
                    }
                    if (!IsSelected)
                    {
                        hdnlbSelectedMeetingProjects.Value += ";";
                        hdnlbSelectedMeetingProjects.Value += MainProject;
                        LoadMeetingProjectsChanges();
                    }
                }
            }
        }

        private void RemoveMainProjectFromProjects(bool FromPageLoad)
        {
            if (FromPageLoad)
            {
                MeetingData md = MeetingDAL.Load(UIHelpers.GetIDParam());
                string MainProjectID = md.ProjectID.ToString();
                ListItem li = lbSelectedMeetingProjects.Items.FindByValue(MainProjectID);
                lbSelectedMeetingProjects.Items.Remove(li);

            }
            else
            {
                if (ddlProject.SelectedIndex != 0)
                {
                    string MainProjectID = ddlProject.SelectedValue.ToString();
                    ListItem li = lbSelectedMeetingProjects.Items.FindByValue(MainProjectID);
                    lbSelectedMeetingProjects.Items.Remove(li);
                }
            }
        }

		//4 new,For more ScanDocuments
        private void BindGrid()
        {
            int ID = UIHelpers.GetIDParam();
            MeetingDocumentsVector mdv = MeetingDocumentDAL.LoadCollection("MeetingDocumentsSelByMeetingProc", SQLParms.CreateMeetingDocumentsSelByMeetingProc(ID));
            if (!(mdv.Count > 0))
            {

                MeetingData md = MeetingDAL.Load(ID);
                if (md != null)
                {
                    md.HasScan = false;
                    MeetingDAL.Save(md);
                }
            }
            dlDocs.DataSource = mdv;
            dlDocs.DataKeyField = "MeetingDocument";
            dlDocs.DataBind();

            //			if (!LoggedUser.HasPaymentRights)
            ////				grdMeetings.Columns[(int)GridColumns.DeleteItem].Visible = false;
            //				dlDocs.GridLines[GridLines.Horizontal].Visible = false;
            try
            {
                for (int i = 0; i < dlDocs.Items.Count; i++)
                {
                    string path = string.Concat(ID, System.Configuration.ConfigurationManager.AppSettings["Extension"]);
                    string js = string.Concat("return confirm('", Resource.ResourceManager["grid_ConfirmDelete"], " ", path, "?');");
                    System.Web.UI.WebControls.WebControl btn = (System.Web.UI.WebControls.WebControl)dlDocs.Items[i].FindControl("btnDel");
                    btn.Attributes["onclick"] = js;
                }
            }
            catch (Exception ex)
            {
                log.Error(ex.ToString());
            }

        }
        public void SaveMeetingDocuments()
        {

            int ID = UIHelpers.GetIDParam();
            if (FileCtrl.PostedFile.FileName.Length != 0)
            {
                string name = FileCtrl.PostedFile.FileName;
                int n = name.LastIndexOf(".");
                if (n != -1)
                {
                    string ext = name.Substring(n);
                    if (ext == System.Configuration.ConfigurationManager.AppSettings["Extension"])
                    {
                        MeetingDocumentData mdd = new MeetingDocumentData(-1, ID, false);
                        MeetingDocumentDAL.Save(mdd);
                        log.Info(string.Format("ScanDoc {0} of Meeting {2} has been INSERTED by {1}", mdd.MeetingDocument, string.Concat(LoggedUser.FullName, " ( userID ", LoggedUser.UserID, " )"), UIHelpers.GetIDParam().ToString()));
                        string path = string.Concat(System.Configuration.ConfigurationManager.AppSettings["ScanPath"], mdd.MeetingDocument, ext);
                        string mapped = Request.MapPath(path);
                        FileCtrl.PostedFile.SaveAs(mapped);
                        MeetingData md = MeetingDAL.Load(ID);
                        md.HasScan = true;
                        MeetingDAL.Save(md);
                        //Check if the Director is on the meeting
                        UserInfo Director = UsersData.UsersListByRoleProc(1);

                        bool IsDirectorThere = false;
                        string[] arr = hdnlbSelectedUsersMails.Value.Split(';');
                        foreach (string s in arr)
                        {
                            int currentID = UIHelpers.ToInt(s);
                            if (currentID == Director.UserID)
                                IsDirectorThere = true;
                        }
                        //if(!IsDirectorThere)
                        //{hdnlbSelectedUsersMails.Value = string.Concat(hdnlbSelectedUsersMails.Value,";",Director.UserID);IsDirectorThere= false;}
                        MailBO.SendMailMinutes(hdnlbSelectedUsersMails.Value, mapped, ddlProject.SelectedItem.Text, txtStartDate.Text, ddlStartTime.SelectedItem.Text, ddlEndTime.SelectedItem.Text, hdnlbSelectedUsers.Value, hdnlbSelectedClients.Value, txtPlace.Text, txtOtherPeople.Text, -1, txtNotes.Text, TimeHelper.FormatDate(DateTime.Parse(txtStartDate.Text)), txtEmails.Text);
                        if (!IsDirectorThere)
                        {
                            int index = hdnlbSelectedUsersMails.Value.LastIndexOf(";");
                            hdnlbSelectedUsersMails.Value = hdnlbSelectedUsersMails.Value.Substring(0, index);
                        }
                    }
                }

            }
        }
        protected string GetURL(int ID)
        {
            return string.Concat(System.Configuration.ConfigurationManager.AppSettings["ScanPath"], ID, System.Configuration.ConfigurationManager.AppSettings["Extension"]);
        }

        protected bool GetVis()
        {
            if (LoggedUser.HasPaymentRights)
                return true;
            else
                return false;
        }

        protected string WhoGoToMeeting()
        {
            string stringAll = string.Empty;
            string[] arr = hdnlbSelectedUsersMails.Value.Split(';');
            for (int i = 0; i < arr.Length; i++)
            {
                int Nomer = UIHelpers.ToInt(arr[i]);
                if (Nomer != -1)
                {
                    UserInfo ui = UsersData.SelectUserByID(Nomer);
                    string New = ui.FullName;
                    stringAll = string.Concat(stringAll, New, ';');
                }
            }
            string[] arr1 = hdnlbSelectedUsers.Value.Split(';');
            for (int i = 0; i < arr1.Length; i++)
            {
                int Nomer = UIHelpers.ToInt(arr1[i]);
                if (Nomer != -1)
                {
                    SubcontracterData sd = SubcontracterDAL.Load(Nomer);
                    string New = sd.SubcontracterName;
                    stringAll = string.Concat(stringAll, New, ';');
                }
            }
            string[] arr2 = hdnlbSelectedClients.Value.Split(';');
            for (int i = 0; i < arr2.Length; i++)
            {
                int Nomer = UIHelpers.ToInt(arr2[i]);
                if (Nomer != -1)
                {
                    ClientData cd = ClientDAL.Load(Nomer);
                    string New = cd.ClientName;
                    stringAll = string.Concat(stringAll, New, ';');
                }
            }
            int index = stringAll.Length - 1;
            if (index == -1)
            {
                index = 0;
            }
            return stringAll.Substring(0, index);
        }

        private void HideSubcontractors()
        {
            for (int i = lbUsers.Items.Count - 1; i > -1; i--)
            {
                SubcontracterData sd = SubcontracterDAL.Load(UIHelpers.ToInt(lbUsers.Items[i].Value));
                if (sd.Hide)
                    lbUsers.Items.RemoveAt(i);

            }
            for (int i = lbSelectedUsers.Items.Count - 1; i > -1; i--)
            {
                SubcontracterData sd = SubcontracterDAL.Load(UIHelpers.ToInt(lbSelectedUsers.Items[i].Value));
                if (sd.Hide)
                    lbSelectedUsers.Items.RemoveAt(i);

            }
        }


		#region Web Form Designer generated code
        override protected void OnInit(EventArgs e)
        {
            //
            // CODEGEN: This call is required by the ASP.NET Web Form Designer.
            //
            InitializeComponent();
            base.OnInit(e);
        }
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
        private void InitializeComponent()
        {
            this.btnSaveUP.Click += new System.EventHandler(this.btnSave_Click);
            this.btnEditIssues.Click += new System.EventHandler(this.btnEditIssues_Click);
            this.btnMinutes.Click += new System.EventHandler(this.btnMinutes_Click);
            this.btnPDFEN.Click += new System.EventHandler(this.btnPDFEN_Click);
            this.btnWord1.Click += new System.EventHandler(this.btnWord1_Click);
            this.btnExcel.Click += new System.Web.UI.ImageClickEventHandler(this.btnExcel_Click);
            this.btnWord.Click += new System.Web.UI.ImageClickEventHandler(this.btnWord_Click);
            this.btnOnlyMeeting.Click += new System.EventHandler(this.btnOnlyMeeting_Click);
            this.btnOnlyMeetingEN.Click += new System.EventHandler(this.btnOnlyMeetingEN_Click);
            this.btnMinutesInv.Click += new System.EventHandler(this.btnMinutesInv_Click);
            this.btnMinutesInvEN.Click += new System.EventHandler(this.btnMinutesInvEN_Click);
            this.ddlProjectsStatus.SelectedIndexChanged += new System.EventHandler(this.ddlProjectsStatus_SelectedIndexChanged);
            this.ddlBuildingTypes.SelectedIndexChanged += new System.EventHandler(this.ddlBuildingTypes_SelectedIndexChanged);
            this.ddlProject.SelectedIndexChanged += new System.EventHandler(this.ddlProject_SelectedIndexChanged);
            this.btnReload.Click += new System.EventHandler(this.btnReload_Click);
            this.dlDocs.ItemCommand += new System.Web.UI.WebControls.DataListCommandEventHandler(this.dlDocs_ItemCommand);
            this.btnScanDocAdd.Click += new System.EventHandler(this.btnScanDocAdd_Click);
            this.ckAll.CheckedChanged += new System.EventHandler(this.ckAll_CheckedChanged);
            this.ckInclude.CheckedChanged += new System.EventHandler(this.ckInclude_CheckedChanged);
            this.dlHotIssues.ItemDataBound += new System.Web.UI.WebControls.DataListItemEventHandler(this.dlHotIssues_ItemDataBound);
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            this.btnNew1.Click += new System.EventHandler(this.btnNew1_Click);
            this.Load += new System.EventHandler(this.Page_Load);

        }
		#endregion


		#region Event handlers
        private void ddlProjectsStatus_SelectedIndexChanged(object sender, System.EventArgs e)
        {
            LoadProjects();
            LoadMeetingProjects(false);
            IsMainProjectSelected(false);
            RemoveMainProjectFromProjects(false);
        }

        private void ddlBuildingTypes_SelectedIndexChanged(object sender, System.EventArgs e)
        {
            LoadProjects();
            LoadMeetingProjects(false);
            IsMainProjectSelected(false);
            RemoveMainProjectFromProjects(false);
        }

        private void btnSave_Click(object sender, System.EventArgs e)
        {
            Dirty = false;
            lblError.Text = lbError.Text = "";
            if (ddlProject.SelectedIndex == 0)
            {
                AlertFieldNotEntered(lblName);
                lblError.Text += lblName.Text + "<BR>";
                Dirty = true;
            }

            if (txtStartDate.Text.Trim() == "" || UIHelpers.GetDate(txtStartDate) == Constants.DateMax)
            {
                AlertFieldNotEntered(lbDate);
                lblError.Text += lbDate.Text + "<BR>";
                Dirty = true;
            }


            if (ddlStartTime.SelectedIndex == 0)
            {
                AlertFieldNotEntered(lbStart);
                lblError.Text += lbStart.Text + "<BR>";
                Dirty = true;
            }
            if (txtPlace.Text == "")
            {
                AlertFieldNotEntered(lbPlace);
                lblError.Text += lbPlace.Text + "<BR>";
                Dirty = true;

            }
            if (ddlEndTime.SelectedIndex == 0 || ddlStartTime.SelectedIndex >= ddlEndTime.SelectedIndex)
            {
                AlertFieldNotEntered(lbEnd);
                lblError.Text += lbEnd.Text + "<BR>";
                Dirty = true;

            }
            #region validate HotIssues
            for (int index = 0; index < dlHotIssues.Items.Count; index++)
            {
                DataListItem dli = dlHotIssues.Items[index];
                //int IDh = (int)((DataList)dlHotIssues).DataKeys[index];

                //				DropDownList ddlHotIssueCategory = (DropDownList)dli.FindControl("ddlHotIssueCategory");
                //				if(!(UIHelpers.ToInt(ddlHotIssueCategory.SelectedValue)>0)) 
                //				{
                //					Label lbHotIssueCategory = (Label) dli.FindControl("lbHotIssueCategory");
                //					AlertFieldNotEntered(lbHotIssueCategory);
                //					Dirty=true;
                //					return;
                //				}	
                DropDownList ddlAssigned = (DropDownList)dli.FindControl("ddlAssigned");
                if (ddlAssigned.SelectedValue == "0")
                {
                    Label lbAssigned = (Label)dli.FindControl("lbAssigned");
                    AlertFieldNotEntered(lbAssigned);
                    lblError.Text += lbAssigned.Text + "<BR>";
                    Dirty = true;

                }
                TextBox txtDeadline = (TextBox)dli.FindControl("txtDeadline");
                //if (txtDeadline.Text.Trim()=="" ||UIHelpers.GetDate(txtDeadline)==Constants.DateMax) 
                //{
                //	Label lbDeadline = (Label) dli.FindControl("lbDeadline");
                //	AlertFieldNotEntered(lbDeadline);
                //	lblError.Text+=lbDeadline.Text+"<BR>";
                //	Dirty=true;

                //}
                //				DropDownList ddlPriority = (DropDownList)dli.FindControl("ddlPriority");
                //				if(!(UIHelpers.ToInt(ddlPriority.SelectedValue)>0)) 
                //				{
                //					Label lbPriority = (Label) dli.FindControl("lbPriority");
                //					AlertFieldNotEntered(lbPriority);
                //					Dirty=true;
                //					return;
                //				}		
                DropDownList ddlStatus = (DropDownList)dli.FindControl("ddlStatus");
                if (!(UIHelpers.ToInt(ddlStatus.SelectedValue) > 0))
                {
                    Label lbStatus = (Label)dli.FindControl("lbStatus");
                    AlertFieldNotEntered(lbStatus);
                    lblError.Text += lbStatus.Text + "<BR>";
                    Dirty = true;

                }



            }

            if (Dirty)
            {
                lblError.Text = Resource.ResourceManager["Name_PleaseEnter"] + lblError.Text;
                lbError.Text = lblError.Text;
                return;
            }
            #endregion
            string stringNames = WhoGoToMeeting();
            int ID = UIHelpers.GetIDParam();
            bool bAprovedPrev = false;
            bool bInsert = ID <= 0;
            bool bDelete = false;
            bool bHasScanBefore = false;
            if (ID > 0)
            {
                MeetingData mdd = MeetingDAL.Load(ID);
                bAprovedPrev = mdd.Approved;
                bHasScanBefore = mdd.HasScan;
                if (UIHelpers.GetDate(txtStartDate) != mdd.MeetingDate
                    || int.Parse(ddlStartTime.SelectedValue) != mdd.StartTimeID
                    || int.Parse(ddlEndTime.SelectedValue) != mdd.EndTimeID
                    || mdd.Users != hdnlbSelectedUsersMails.Value)
                {
                    bInsert = true;
                    bDelete = true;
                }
            }
            DateTime dtMeet = UIHelpers.GetDate(txtStartDate);

            MeetingData md = new MeetingData(ID, dtMeet, int.Parse(ddlProject.SelectedValue), -1,
                int.Parse(ddlStartTime.SelectedValue), int.Parse(ddlEndTime.SelectedValue),
                hdnlbSelectedClients.Value, hdnlbSelectedUsers.Value,
                hdnlbSelectedUsersMails.Value, LoggedUser.UserID, ckApproved.Checked, LoggedUser.UserID);

            md.HasScan = bHasScanBefore;
            md.Place = txtPlace.Text;
            md.PlaceEN = txtPlaceEN.Text;
            md.Notes = txtNotes.Text;
            md.OtherPeople = txtOtherPeople.Text;
            md.OtherMails = txtEmails.Text;
            md.ASA = ckInclude.Checked;
            bool InsertOrUpdate = (ID < 1);
            MeetingDAL.Save(md);
            if (InsertOrUpdate)
                log.Info(string.Format("Meeting {0} has been INSERTED by {1}", md.MeetingID, string.Concat(LoggedUser.FullName, " ( userID ", LoggedUser.UserID, " )")));
            else
                log.Info(string.Format("Meeting {0} has been UPDATED by {1}", md.MeetingID, string.Concat(LoggedUser.FullName, " ( userID ", LoggedUser.UserID, " )")));
            //MeetingProjects , save
            ID = md.MeetingID;
            string[] arr = hdnlbSelectedMeetingProjects.Value.Split(';');
            MeetingProjectUDL.ExecuteMeetingProjectsDelByMeetingProc(ID);
            foreach (string s in arr)
            {

                int nID = UIHelpers.ToInt(s);
                if (nID > 0)
                {
                    MeetingProjectData mpd = new MeetingProjectData(-1, ID, nID);
                    MeetingProjectDAL.Save(mpd);
                }
            }
            //end

            /*MeetingCategorieDAL.Delete(ID);
            foreach(ListItem li in lst.Items)
            {
                if(li.Selected)
                {
                    MeetingCategorieDAL.Insert(new MeetingCategorieData(-1,ID,int.Parse(li.Value)));
                }
            }*/
            if (bDelete)
            {
                WorkTimesData.ExecuteWorkTimesDelByMeetingProc(md.MeetingID);
            }
            if (ckApproved.Checked)
            {
                string[] susers = hdnlbSelectedUsersMails.Value.Split(';');
                foreach (string s in susers)
                {
                    if (s != null && s.Length > 0)
                    {
                        try
                        {
                            int i = int.Parse(s);
                            if (!IsWorkDayEntered(i, dtMeet))
                            {
                                WorkTimesData.InsertWorkTime(null, dtMeet, i, int.Parse(ddlProject.SelectedValue),
                                    int.Parse(System.Configuration.ConfigurationManager.AppSettings["MeetingID"]),
                                    int.Parse(ddlStartTime.SelectedValue), int.Parse(ddlEndTime.SelectedValue), "", "",
                                    DateTime.Now, LoggedUser.UserID, md.MeetingID);

                            }
                        }
                        catch { }
                    }
                }
            }
            #region Save When There Was One Meeting Document
            //			if(FileCtrl.PostedFile.FileName!=null
            //				&&FileCtrl.PostedFile.FileName!="")
            //			{
            //				
            //			
            //				string name = FileCtrl.PostedFile.FileName;
            //				int n = name.LastIndexOf(".");
            //				if(n!=-1)
            //				{
            //					string ext = name.Substring(n);
            //					if(ext==System.Configuration.ConfigurationManager.AppSettings["Extension"])
            //					{
            //						string path = string.Concat(System.Configuration.ConfigurationManager.AppSettings["ScanPath"],md.MeetingID,ext);
            //						string mapped = Request.MapPath(path);
            //
            //						FileCtrl.PostedFile.SaveAs(mapped);
            //						md.HasScan=true;
            //						MeetingDAL.Save(md);
            //						md=MeetingDAL.Load(md.MeetingID);
            //						//if(!bHasScanBefore)
            //						{
            //							if(hdnlbSelectedUsersMails.Value!="" || hdnlbSelectedClients.Value!="" || hdnlbSelectedUsers.Value!="")
            //								MailBO.SendMailMinutes(hdnlbSelectedUsersMails.Value, mapped,md.ProjectName, md.MeetingDate.ToShortDateString(),md.StartHour, md.EndHour, hdnlbSelectedUsers.Value,hdnlbSelectedClients.Value,txtPlace.Text, txtOtherPeople.Text,md.ClientID,txtNotes.Text,TimeHelper.FormatDate(md.MeetingDate));
            //
            //						}
            //					}
            //				}
            //			}
            #endregion
            md = MeetingDAL.Load(md.MeetingID);
            PdfExport pdf = new PdfExport();
            pdf.NeverEmbedFonts = "";
            bool bClient = hdnlbSelectedClients.Value != "";
            bool bSub = hdnlbSelectedUsers.Value != "";
            GrapeCity.ActiveReports.SectionReport report = new rptTefter(md.ProjectName, md.MeetingDate.ToShortDateString(), bClient, bSub, stringNames);
            string path1 = string.Concat(System.Configuration.ConfigurationManager.AppSettings["ScanPath"], "NoteBook", md.MeetingID, ".pdf");
            string mapped1 = Request.MapPath(path1);
            report.Run();
            pdf.Export(report.Document, mapped1);
            string sHour = md.StartHour.Substring(0, md.StartHour.IndexOf("."));
            DateTime dtMeeting = new DateTime(md.MeetingDate.Year, md.MeetingDate.Month, md.MeetingDate.Day, int.Parse(sHour), 0, 0);
            if (bAprovedPrev == false && ckApproved.Checked && dtMeeting > DateTime.Now)
            {
                if (hdnlbSelectedUsersMails.Value != "" || hdnlbSelectedClients.Value != "" || hdnlbSelectedUsers.Value != "")
                    if (bInsert)
                    {
                        MailBO.SendMailFirst(hdnlbSelectedUsersMails.Value, md.ProjectName, md.MeetingDate.ToShortDateString(), md.StartHour, md.EndHour, hdnlbSelectedUsers.Value, hdnlbSelectedClients.Value, txtPlace.Text, txtOtherPeople.Text, md.ClientID, txtNotes.Text, TimeHelper.FormatDate(md.MeetingDate));
                        MailBO.SendMailApproved(hdnlbSelectedUsersMails.Value, mapped1, md.ProjectName, md.MeetingDate.ToShortDateString(), md.StartHour, md.EndHour, hdnlbSelectedUsers.Value, hdnlbSelectedClients.Value, txtPlace.Text, txtOtherPeople.Text, md.ClientID, txtNotes.Text, TimeHelper.FormatDate(md.MeetingDate));
                    }
                    else
                        MailBO.SendMailApproved(hdnlbSelectedUsersMails.Value, mapped1, md.ProjectName, md.MeetingDate.ToShortDateString(), md.StartHour, md.EndHour, hdnlbSelectedUsers.Value, hdnlbSelectedClients.Value, txtPlace.Text, txtOtherPeople.Text, md.ClientID, txtNotes.Text, TimeHelper.FormatDate(md.MeetingDate));
            }
            if (bInsert && !ckApproved.Checked && dtMeeting > DateTime.Now)
            {
                MailBO.SendMailOfficer(md.ProjectName, TimeHelper.FormatDate(md.MeetingDate), md.StartHour, md.EndHour);
                if (hdnlbSelectedUsersMails.Value != "" || hdnlbSelectedClients.Value != "" || hdnlbSelectedUsers.Value != "")
                    MailBO.SendMailFirst(hdnlbSelectedUsersMails.Value, md.ProjectName, md.MeetingDate.ToShortDateString(), md.StartHour, md.EndHour, hdnlbSelectedUsers.Value, hdnlbSelectedClients.Value, txtPlace.Text, txtOtherPeople.Text, md.ClientID, txtNotes.Text, TimeHelper.FormatDate(md.MeetingDate));

            }
            if (FileCtrl.PostedFile.FileName.Length != 0)
            {
                string name = FileCtrl.PostedFile.FileName;
                int n = name.LastIndexOf(".");
                if (n != -1)
                {
                    string ext = name.Substring(n);
                    if (ext == System.Configuration.ConfigurationManager.AppSettings["Extension"])
                    {
                        MeetingDocumentData mdd = new MeetingDocumentData(-1, md.MeetingID, false);
                        MeetingDocumentDAL.Save(mdd);
                        log.Info(string.Format("ScanDoc {0} of Meeting {2} has been INSERTED by {1}", mdd.MeetingDocument, string.Concat(LoggedUser.FullName, " ( userID ", LoggedUser.UserID, " )"), UIHelpers.GetIDParam().ToString()));
                        string path = string.Concat(System.Configuration.ConfigurationManager.AppSettings["ScanPath"], mdd.MeetingDocument, ext);
                        string mapped = Request.MapPath(path);
                        FileCtrl.PostedFile.SaveAs(mapped);
                        md.HasScan = true;
                        MeetingDAL.Save(md);
                        if (hdnlbSelectedUsersMails.Value != "" || hdnlbSelectedClients.Value != "" || hdnlbSelectedUsers.Value != "")
                        {
                            //Check if the Director is on the meeting
                            UserInfo Director = UsersData.UsersListByRoleProc(1);

                            bool IsDirectorThere = false;
                            string[] arr1 = hdnlbSelectedUsersMails.Value.Split(';');
                            foreach (string s in arr1)
                            {
                                int currentID = UIHelpers.ToInt(s);
                                if (currentID == Director.UserID)
                                    IsDirectorThere = true;
                            }
                            //if(!IsDirectorThere)
                            //{hdnlbSelectedUsersMails.Value = string.Concat(hdnlbSelectedUsersMails.Value,";",Director.UserID);IsDirectorThere= false;}
                            MailBO.SendMailMinutes(hdnlbSelectedUsersMails.Value, mapped, md.ProjectName, md.MeetingDate.ToShortDateString(), md.StartHour, md.EndHour, hdnlbSelectedUsers.Value, hdnlbSelectedClients.Value, txtPlace.Text, txtOtherPeople.Text, md.ClientID, txtNotes.Text, TimeHelper.FormatDate(md.MeetingDate), txtEmails.Text);
                            if (!IsDirectorThere)
                            {
                                int index = hdnlbSelectedUsersMails.Value.LastIndexOf(";");
                                hdnlbSelectedUsersMails.Value = hdnlbSelectedUsersMails.Value.Substring(0, index);
                            }
                        }
                    }
                }
            }

            //if(LoggedUser.IsLeader)
            //	SaveHotIssues();

            //Response.Redirect("Meetings.aspx");
            Response.Redirect("EditMeeting.aspx?id=" + ID.ToString());

        }
		/*private void SaveHotIssues()
		{
			HotIssuesVector hiv = HotIssueSession;
			for(int index=0;index<dlHotIssues.Items.Count;index++)
			{
				DataListItem dli = dlHotIssues.Items[index];
				int ID = (int)((DataList)dlHotIssues).DataKeys[index];
				int indexInVector = hiv.FindByHotIssueID(ID);
				
				Label lbProjectNameID = (Label)dli.FindControl("lbProjectNameID");
				Label txtHotIssueIme =(Label)dli.FindControl("txtHotIssueIme");
				Label lbCategoryText = (Label)dli.FindControl("lbCategoryText");
				Label txtDiscription = (Label)dli.FindControl("txtDiscription");
				TextBox	txtComment = (TextBox)dli.FindControl("txtComment");
				DropDownList ddlAssigned = (DropDownList)dli.FindControl("ddlAssigned");
				TextBox txtDeadline = (TextBox)dli.FindControl("txtDeadline");
				Label lbMainID = (Label)dli.FindControl("lbMainID");
				CheckBox ckClient = (CheckBox)dli.FindControl("cbToClient");
//				DropDownList ddlPriority = (DropDownList)dli.FindControl("ddlPriority");
				DropDownList ddlStatus = (DropDownList)dli.FindControl("ddlStatus");
				
				int indexOfAssigned = ddlAssigned.SelectedValue.IndexOf("_");
				if(indexOfAssigned == -1)
					continue;
				int userType = UIHelpers.ToInt(ddlAssigned.SelectedValue.Substring(0,indexOfAssigned));
				int userID = UIHelpers.ToInt(ddlAssigned.SelectedValue.Substring(indexOfAssigned+1));

				bool isEqual = true;
				bool bASA=false;
				if(indexInVector>-1)
				{
					HotIssueData hid = hiv[indexInVector];
					bASA=hid.ASA;
					if(hid.HotIssueName != txtHotIssueIme.Text)
						isEqual = false;
//					if(hid.HotIssueCategoryID != UIHelpers.ToInt(ddlHotIssueCategory.SelectedValue))
//						isEqual = false;
//					if(hid.HotIssuePriorityID != UIHelpers.ToInt(ddlPriority.SelectedValue))
//						isEqual = false;
					if(hid.HotIssueStatusID != UIHelpers.ToInt(ddlStatus.SelectedValue))
						isEqual = false;
					if(hid.AssignedTo != userID)
						isEqual = false;
					if(hid.AssignedToType != userType)
						isEqual = false;
					if(hid.ToClient != ckClient.Checked)
						isEqual = false;
					if(hid.HotIssueDiscription != txtDiscription.Text)
						isEqual = false;
					if(0 != txtComment.Text.Length)
						isEqual = false;
					if(hid.HotIssueDeadline != TimeHelper.GetDate(txtDeadline.Text))
						isEqual = false;
				}
				else
					isEqual = false;
				if(!isEqual)
				{
					if(UIHelpers.ToInt(lbMainID.Text)>0)
						HotIssueDAL.Delete(UIHelpers.ToInt(lbMainID.Text));
					HotIssueData dat = new HotIssueData(-1,UIHelpers.ToInt(lbProjectNameID.Text),
						UIHelpers.ToInt(lbCategoryText.Text),txtHotIssueIme.Text,txtDiscription.Text,txtComment.Text,
						userID,userType,TimeHelper.GetDate(txtDeadline.Text)/*,UIHelpers.ToInt(ddlPriority.SelectedValue),UIHelpers.ToInt(ddlStatus.SelectedValue),UIHelpers.ToInt(lbMainID.Text),true,LoggedUser.FullName,DateTime.Now,false);
					dat.ToClient =ckClient.Checked;
					dat.ASA = bASA;
					HotIssueDAL.Save(dat);
					
					log.Info(string.Format("HotIssue {0} has been INSERTED by {1}",dat.HotIssueID,string.Concat(LoggedUser.FullName," ( userID ",LoggedUser.UserID," )")));
			
				}

			}
			if(UIHelpers.GetIDParam()!=-1)
				SavePDF(UIHelpers.GetIDParam());
		}
		*/
        private void btnCancel_Click(object sender, System.EventArgs e)
        {
            Response.Redirect("Meetings.aspx");
        }

        private void ddlProject_SelectedIndexChanged(object sender, System.EventArgs e)
        {
            LoadMeetingProjects(false);
            IsMainProjectSelected(false);
            ReloadListsFromProjects(false, int.Parse(ddlProject.SelectedValue));
            RemoveMainProjectFromProjects(false);
            //LoadHotIssues();
            //LoadHotIssueCategories(int.Parse(ddlProject.SelectedValue));
        }

        private void btnReload_Click(object sender, System.EventArgs e)
        {
            LoadMeetingProjects(false);
            IsMainProjectSelected(false);
            ReloadListsFromProjects(false, int.Parse(ddlProject.SelectedValue));
            RemoveMainProjectFromProjects(false);
        }

        private void dlDocs_ItemCommand(object source, System.Web.UI.WebControls.DataListCommandEventArgs e)
        {
            ImageButton ib = (ImageButton)e.CommandSource;
            string s = ib.ID;
            switch (ib.ID)
            {
                case "btnDel":
                    int ID = (int)dlDocs.DataKeys[e.Item.ItemIndex];
                    MeetingDocumentData mdd = MeetingDocumentDAL.Load(ID);
                    mdd.IsDeleted = true;
                    MeetingDocumentDAL.Save(mdd);
                    log.Info(string.Format("ScanDoc {0} of Meeting {2} has been DELETED by {1}", ID, string.Concat(LoggedUser.FullName, " ( userID ", LoggedUser.UserID, " )"), UIHelpers.GetIDParam().ToString()));
                    BindGrid();
                    LoadMeetingProjects(false);
                    IsMainProjectSelected(false);
                    ReloadListsFromProjects(false, int.Parse(ddlProject.SelectedValue));
                    RemoveMainProjectFromProjects(false);
                    break;
            }
        }

        private void btnScanDocAdd_Click(object sender, System.EventArgs e)
        {
            SaveMeetingDocuments();
            BindGrid();
            LoadMeetingProjects(false);
            IsMainProjectSelected(false);
            ReloadListsFromProjects(false, int.Parse(ddlProject.SelectedValue));
            RemoveMainProjectFromProjects(false);
        }
	
		#endregion

		#region HotIssues
		
		/*public void LoadHotIssueCategories(int ProjectID)
		{
//			lst.Items.Clear();
//			HotIssuesCategoriesVector cv=HotIssueCategoryDAL.LoadCollection(ProjectID);
//			foreach(HotIssueCategoryData c in cv)
//			{
//				ListItem li = new ListItem(c.HotIssueCategoryName,c.HotIssueCategoryID.ToString());
//				lst.Items.Add(li);
//			}
			
			lst.DataSource=HotIssueCategoryDAL.LoadCollection(ProjectID);;
			lst.DataTextField="HotIssueCategoryName";
			lst.DataValueField="HotIssueCategoryID";
			lst.DataBind();

			int nID=UIHelpers.GetIDParam();
			if(!IsPostBack && nID>0)
			{
				MeetingCategoriesVector mcv = MeetingCategorieDAL.LoadCollection(nID);
				foreach(ListItem li in lst.Items)
				{
					int nValue = int.Parse(li.Value);
					if(mcv.GetByCategoryID(nValue)!=null)
						li.Selected=true;
				}
			}
			
		}*/
		//here load HotIssues
		/*private void LoadHotIssues()
		{
			int mainProjectId = UIHelpers.ToInt(ddlProject.SelectedValue);
			
		
			if(mainProjectId>0)
			{
				

				//string[] sPrIDs = hdnlbMeetingProjects.Value.Split(';');
				int nDays=int.Parse(System.Configuration.ConfigurationManager.AppSettings["EditDays"]);
				
				HotIssuesVector vecAll = HotIssueDAL.LoadCollectionForMeeting(mainProjectId,UIHelpers.GetDate(txtStartDate.Text).AddDays(nDays),ckInclude.Checked, UIHelpers.GetIDParam(),-1,-1,Constants.DateMax,true,false,false,false);
//				foreach(string s in sPrIDs)
//				{
//					if(UIHelpers.ToInt(s)>0)
//					{
//						HotIssuesVector vec = HotIssueDAL.LoadCollectionForMeeting(UIHelpers.ToInt(s),DateTime.MaxValue);
//						vecAll.AddRange(vec);
//					}
//				}		
				HotIssueSession = vecAll;

				foreach(HotIssueData hid in vecAll)
				{
					if(hid.HotIssueStatusID==int.Parse(System.Configuration.ConfigurationManager.AppSettings["HotIssueStatusOpen"]))
					{
						
						MeetingsVector mv =MeetingDAL.LoadCollection("MeetingsListProc",SQLParms.CreateMeetingsListProc(hid.OriginalDate,
							UIHelpers.GetDate(txtStartDate.Text).AddDays(-1),int.Parse(ddlProject.SelectedValue),-1,null));			
						if(mv.Count>0)
						{
							hid.HotIssueStatusID=int.Parse(System.Configuration.ConfigurationManager.AppSettings["HotIssueStatusInProcess"]);
							HotIssueDAL.Save(hid);
						}
		
					}
				}
				dlHotIssues.DataSource = vecAll;
				dlHotIssues.DataKeyField = "HotIssueID";
				dlHotIssues.DataBind();
				
			}
			else
				dlHotIssues.Visible = false;
			//notes:if main project is not selected return error
//			DateTime dtAddAllowed = TimeHelper.GetDate(txtStartDate.Text).AddDays(UIHelpers.ToDouble(System.Configuration.ConfigurationManager.AppSettings["MeetingDaysEditHotIssues"]));
//			if(dtAddAllowed <DateTime.Now)
//			{
//				SetEnableDataList(false);
//			}
//			else 
//			{
//				SetEnableDataList(true);			
//			}
		}
*/
        private void dlHotIssues_ItemDataBound(object sender, System.Web.UI.WebControls.DataListItemEventArgs e)
        {
            //			DropDownList ddlCategory = (DropDownList)e.Item.FindControl("ddlHotIssueCategory");
            //			if(ddlCategory==null)
            //			{
            //				log.Error("error load category");
            //				return;
            //			}
            //			UIHelpers.LoadHotIssueCategories(ddlCategory);
            //			Label lbCat = (Label)e.Item.FindControl("lbCategoryText");
            //			if(ddlCategory.Items.FindByValue(lbCat.Text)!=null)
            //				ddlCategory.SelectedValue= lbCat.Text;
            //			Label lbCatText = (Label)e.Item.FindControl("txtCategoryText");
            //			lbCatText.Text = ddlCategory.SelectedItem.Text;

            //			DropDownList ddlPriority = (DropDownList)e.Item.FindControl("ddlPriority");
            //			if(ddlPriority==null)
            //			{
            //				log.Error("error load priority");
            //				return;
            //			}
            //UIHelpers.LoadHotIssuePriorities(ddlPriority);
            //			Label lbPr = (Label)e.Item.FindControl("lbPriorityText");
            //			if(ddlPriority.Items.FindByValue(lbPr.Text)!=null)
            //				ddlPriority.SelectedValue= lbPr.Text;
            //			Label lbPrText = (Label)e.Item.FindControl("txtPriorityText");
            //			lbPrText.Text = ddlPriority.SelectedItem.Text;

            DropDownList ddlStatus = (DropDownList)e.Item.FindControl("ddlStatus");
            if (ddlStatus == null)
            {
                log.Error("error load status");
                return;
            }
            UIHelpers.LoadHotIssueStatuses(ddlStatus);
            Label lbSt = (Label)e.Item.FindControl("lbStatusText");
            if (ddlStatus.Items.FindByValue(lbSt.Text) != null)
                ddlStatus.SelectedValue = lbSt.Text;
            Label lbStText = (Label)e.Item.FindControl("txtStatusText");
            lbStText.Text = ddlStatus.SelectedItem.Text;

            DropDownList ddlAssigned = (DropDownList)e.Item.FindControl("ddlAssigned");
            if (ddlAssigned == null)
            {
                log.Error("error load assigned");
                return;
            }
            //UIHelpers.LoadHotIssueAssigneds(ddlAssigned,-1);
            UIHelpers.LoadHotIssueAssigneds(ddlAssigned, UIHelpers.ToInt(ddlProject.SelectedValue));
            Label lbAs = (Label)e.Item.FindControl("lbAssignedText");
            Label lbAsT = (Label)e.Item.FindControl("lbAssignedTypeText");
            if (ddlAssigned.Items.FindByValue(string.Concat(lbAsT.Text, "_", lbAs.Text)) != null)
                ddlAssigned.SelectedValue = string.Concat(lbAsT.Text, "_", lbAs.Text);
            Label txtAssignedText = (Label)e.Item.FindControl("txtAssignedText");
            txtAssignedText.Text = ddlAssigned.SelectedItem.Text;

            HtmlImage img = (HtmlImage)e.Item.FindControl("Img5");
            TextBox txtDeadLine = (TextBox)e.Item.FindControl("txtDeadline");
            img.Attributes["onclick"] = TimeHelper.InvokePopupCal(txtDeadLine);
            Label txtDeadlineText = (Label)e.Item.FindControl("txtDeadlineText");
            txtDeadlineText.Text = txtDeadLine.Text;

        }

        private void SetEnableDataList(bool enable)
        {
            for (int i = 0; i < dlHotIssues.Items.Count; i++)
            {
                //				DropDownList ddlCategory = (DropDownList)dlHotIssues.Items[i].FindControl("ddlHotIssueCategory");
                //				Label lbCatText = (Label)dlHotIssues.Items[i].FindControl("txtCategoryText");
                //				ddlCategory.Visible = enable;
                //				lbCatText.Visible = !enable ;

                //				DropDownList ddlPriority = (DropDownList)dlHotIssues.Items[i].FindControl("ddlPriority");
                //				Label lbPrText = (Label)dlHotIssues.Items[i].FindControl("txtPriorityText");
                //				ddlPriority.Visible = enable;
                //				lbPrText.Visible = !enable ;

                DropDownList ddlStatus = (DropDownList)dlHotIssues.Items[i].FindControl("ddlStatus");
                Label lbStText = (Label)dlHotIssues.Items[i].FindControl("txtStatusText");
                ddlStatus.Visible = enable;
                lbStText.Visible = !enable;


                DropDownList ddlAssigned = (DropDownList)dlHotIssues.Items[i].FindControl("ddlAssigned");
                Label txtAssignedText = (Label)dlHotIssues.Items[i].FindControl("txtAssignedText");
                ddlAssigned.Visible = enable;
                txtAssignedText.Visible = !enable;

                TextBox txtDeadLine = (TextBox)dlHotIssues.Items[i].FindControl("txtDeadline");
                Label txtDeadlineText = (Label)dlHotIssues.Items[i].FindControl("txtDeadlineText");
                txtDeadLine.Visible = enable;
                txtDeadlineText.Visible = !enable;

                TextBox txtComment = (TextBox)dlHotIssues.Items[i].FindControl("txtComment");
                Label lbComment = (Label)dlHotIssues.Items[i].FindControl("lbComment");
                lbComment.Visible = txtComment.Visible = enable;

                HtmlImage img5 = (HtmlImage)dlHotIssues.Items[i].FindControl("Img5");
                HtmlImage Img1 = (HtmlImage)dlHotIssues.Items[i].FindControl("Img1");
                HtmlImage imgRequired = (HtmlImage)dlHotIssues.Items[i].FindControl("imgRequired");
                HtmlImage Img7 = (HtmlImage)dlHotIssues.Items[i].FindControl("Img7");
                img5.Visible = Img1.Visible = imgRequired.Visible = Img7.Visible = enable;

                CheckBox cbToClient = (CheckBox)dlHotIssues.Items[i].FindControl("cbToClient");
                Label lbToClientText = (Label)dlHotIssues.Items[i].FindControl("lbToClientText");
                cbToClient.Visible = enable;
                lbToClientText.Visible = !enable;
            }
        }


        private void btnReloadIssues_Click(object sender, System.EventArgs e)
        {
            //LoadHotIssues();
        }

		
	
		private HotIssuesVector HotIssueSession
		{
			get
			{
				if(this.Session["HotIssuesVectorSession"]!=null)
					return (HotIssuesVector)this.Session["HotIssuesVectorSession"];
				else
					return new HotIssuesVector();
			}
			set
			{
				this.Session["HotIssuesVectorSession"] = value;
			}
		}

        private void SavePDF(int meetingID)
        {
            string returnPath = string.Concat(System.Configuration.ConfigurationManager.AppSettings["MeetingHotIssuesFilesPath"], meetingID, System.Configuration.ConfigurationManager.AppSettings["Extension"]);

            PdfExport pdf = new PdfExport();
            pdf.NeverEmbedFonts = "";

            MeetingData md = MeetingDAL.Load(meetingID);
            if (md == null)
                return;
            //			MeetingProjectsVector mpv = MeetingProjectDAL.LoadCollection("MeetingProjectsSelByMeetingProc",SQLParms.CreateMeetingProjectsSelByMeetingProc(meetingID));
            //			foreach(MeetingProjectData mpd in mpv)
            //				if(mpd.ProjectID==md.ProjectID)
            //				{
            //					mpv.Remove(mpd);
            //					break;
            //				}

            DataView dvProjects = UIHelpers.HotIssueCreateDVprojects(md.ProjectID, false);
            DataView dvUsers = UIHelpers.HotIssueCreateDVusers(md.Users, md.Subcontracters, md.Clients, md.OtherPeople, false);
            HotIssuesVector hiv = UIHelpers.HotIssueCreateHotIssuesVector(md, null, true, false, false, md.ProjectID);
            string meetingNumber = MeetingDAL.SelectMeetingNumber(md.ProjectID, md.MeetingDate);
            GrapeCity.ActiveReports.SectionReport report = new ReportMeetingHotIssuesAll(md.MeetingDate, md.StartHour, md.EndHour, md.Place, md.Notes, dvProjects, dvUsers, hiv, meetingNumber, true, md.OtherPeople, md.OtherMails, md.ProjectID, LoggedUser.FullName, 0, md.ASA);
            report.PageSettings.Orientation = GrapeCity.ActiveReports.Document.Section.PageOrientation.Default;
            report.Run(false);


            report = new ReportMeetingHotIssuesAll(md.MeetingDate, md.StartHour, md.EndHour, md.Place, md.Notes, dvProjects, dvUsers, hiv, meetingNumber, true, md.OtherPeople, md.OtherMails, md.ProjectID, LoggedUser.FullName, report.Document.Pages.Count, md.ASA);
            report.PageSettings.Orientation = GrapeCity.ActiveReports.Document.Section.PageOrientation.Default;
            report.Run();

            pdf.Export(report.Document, Request.MapPath(returnPath));

        }

        private void btnSendEmail_Click(object sender, System.EventArgs e)
        {
            int ID = UIHelpers.GetIDParam();
            if (ID != -1)
            {
                string pathOfPDF = SavePDFHotIssues(ID);
                if (File.Exists(pathOfPDF))
                    MailBO.SendMailWithPDFHotIssues(ID, pathOfPDF);
                else
                    log.Info("error:send PDF from meeting(file not exist)");
            }
            else
                lblError.Text = Resource.ResourceManager["Save_Before_Send_Email"];
        }

        private string SavePDFHotIssues(int meetingID)
        {
            string returnPath = string.Concat(System.Configuration.ConfigurationManager.AppSettings["MeetingHotIssuesFilesPath"], meetingID, System.Configuration.ConfigurationManager.AppSettings["Extension"]);
            if (!File.Exists(Request.MapPath(returnPath)))
                SavePDF(meetingID);
            return Request.MapPath(returnPath);

        }
		
		#endregion

        private void btnMinutes_Click(object sender, System.EventArgs e)
        {
            ExportMinutes(true, false, false, false, false);
        }

        private void ExportMinutes(bool bAll, bool bExcel, bool bWord, bool bOnlyMeeting, bool bEnglish)
        {
            int IDD = UIHelpers.GetIDParam();
            int nProjectID = int.Parse(ddlProject1.SelectedValue);
            string PathOfPDF = string.Concat(System.Configuration.ConfigurationManager.AppSettings["MeetingHotIssuesFilesPath"], IDD, System.Configuration.ConfigurationManager.AppSettings["Extension"]);

            {



                MeetingData md = MeetingDAL.Load(IDD);
                //				MeetingProjectsVector mpv = MeetingProjectDAL.LoadCollection("MeetingProjectsSelByMeetingProc",SQLParms.CreateMeetingProjectsSelByMeetingProc(IDD));
                //				foreach(MeetingProjectData mpd in mpv)
                //					if(mpd.ProjectID==md.ProjectID)
                //					{
                //						mpv.Remove(mpd);
                //						break;
                //					}

                DataView dvProjects = UIHelpers.HotIssueCreateDVprojects(nProjectID, bEnglish);
                DataView dvUsers = UIHelpers.HotIssueCreateDVusers(md.Users, md.Subcontracters, md.Clients, md.OtherPeople, bEnglish);
                HotIssuesVector hiv = UIHelpers.HotIssueCreateHotIssuesVector(md, null, bAll, bOnlyMeeting, bEnglish, nProjectID);
                string meetingNumber = MeetingDAL.SelectMeetingNumber(md.ProjectID, md.MeetingDate);
                GrapeCity.ActiveReports.SectionReport report = new ReportMeetingHotIssuesAll(md.MeetingDate, md.StartHour, md.EndHour, md.Place, md.Notes, dvProjects, dvUsers, hiv, meetingNumber, bAll, md.OtherPeople, md.OtherMails, nProjectID, LoggedUser.FullName, 0, md.ASA);
                report.PageSettings.Orientation = GrapeCity.ActiveReports.Document.Section.PageOrientation.Default;
                report.Run(false);
                if (bEnglish)
                {
                    string sName = "";
                    SqlDataReader reader = null;
                    try
                    {
                        reader = UsersData.SelectUser(LoggedUser.UserID);
                        reader.Read();

                        if (!reader.IsDBNull(27))
                            sName = reader.GetString(27);
                    }
                    catch (Exception ex)
                    {
                        log.Error(ex);

                    }
                    finally
                    {
                        if (reader != null) reader.Close();
                    }
                    report = new ReportMeetingHotIssuesAllEN(md.MeetingDate, md.StartHour, md.EndHour, md.PlaceEN, md.Notes, dvProjects, dvUsers, hiv, meetingNumber, bAll, md.OtherPeople, md.OtherMails, nProjectID, sName, report.Document.Pages.Count, md.ASA);
                }
                else
                    report = new ReportMeetingHotIssuesAll(md.MeetingDate, md.StartHour, md.EndHour, md.Place, md.Notes, dvProjects, dvUsers, hiv, meetingNumber, bAll, md.OtherPeople, md.OtherMails, nProjectID, LoggedUser.FullName, report.Document.Pages.Count, md.ASA);
                report.PageSettings.Orientation = GrapeCity.ActiveReports.Document.Section.PageOrientation.Default;
                report.Run();


                string projectName, projectCode, administrativeName, add;
                decimal area = 0;
                int clientID = 0;
                bool phases; bool isPUP;
                ProjectsData.SelectProject(nProjectID, out projectName, out  projectCode, out administrativeName, out area, out add,
                    out clientID, out phases, out isPUP);
                string sFileName = md.MeetingDate.ToString("yyMMdd") + "_" + projectCode + "_PCM" + meetingNumber;
                if (bExcel)
                {
                    XlsExport xls = new XlsExport();
                    System.IO.MemoryStream memoryFile = new System.IO.MemoryStream();

                    xls.Export(report.Document, memoryFile);

                    Response.Clear();
                    Response.AppendHeader("content-disposition"
                        , "attachment; filename=" + sFileName + ".xls");
                    Response.ContentType = "application/xls";

                    memoryFile.WriteTo(Response.OutputStream);
                    Response.End();
                }
                else if (bWord)
                {
                    RtfExport rtf = new RtfExport();
                    System.IO.MemoryStream memoryFile = new System.IO.MemoryStream();

                    rtf.Export(report.Document, memoryFile);

                    Response.Clear();
                    Response.AppendHeader("content-disposition"
                        , "attachment; filename=" + sFileName + ".rtf");
                    Response.ContentType = "application/rtf";

                    memoryFile.WriteTo(Response.OutputStream);
                    Response.End();
                }
                else
                {
                    PdfExport pdf = new PdfExport();

                    pdf.NeverEmbedFonts = "";
                    System.IO.MemoryStream memoryFile = new System.IO.MemoryStream();

                    pdf.Export(report.Document, memoryFile);

                    Response.Clear();
                    Response.AppendHeader("content-disposition"
                        , "attachment; filename=" + sFileName + ".pdf");
                    Response.ContentType = "application/pdf";

                    memoryFile.WriteTo(Response.OutputStream);
                    Response.End();
                }
            }
        }

        private void btnMinutesInv_Click(object sender, System.EventArgs e)
        {
            ExportMinutes(false, false, false, false, false);
        }

        private void btnNew1_Click(object sender, System.EventArgs e)
        {
            Response.Redirect("EditHotIssue.aspx" + "?pid=" + ddlProject.SelectedValue.ToString());
        }

        private void ckInclude_CheckedChanged(object sender, System.EventArgs e)
        {
            //LoadHotIssues();	
        }
        public bool GetExecution(object d)
        {
            if (d == null || d == DBNull.Value || !(d is DateTime))
                return false;
            DateTime dt = (DateTime)d;
            if (dt == Constants.DateMin || dt == Constants.DateMax || dt == new DateTime())
                return false;
            return true;
        }
        public string GetExecutionDate(object d)
        {
            if (d == null || d == DBNull.Value || !(d is DateTime))
                return "";
            DateTime dt = (DateTime)d;
            if (dt == Constants.DateMin || dt == Constants.DateMax || dt == new DateTime())
                return "";
            return dt.ToString("dd.MM.yyyy");
        }

        private void btnExcel_Click(object sender, System.Web.UI.ImageClickEventArgs e)
        {
            ExportMinutes(true, true, false, false, false);
        }

        private void btnWord_Click(object sender, System.Web.UI.ImageClickEventArgs e)
        {
            ExportMinutes(true, false, true, false, false);
        }

        private void ckAll_CheckedChanged(object sender, System.EventArgs e)
        {

            foreach (ListItem li in lst.Items)
            {
                li.Selected = ckAll.Checked;
            }

        }

        private void btnOnlyMeeting_Click(object sender, System.EventArgs e)
        {
            ExportMinutes(true, false, false, true, false);
        }

        private void btnPDFEN_Click(object sender, System.EventArgs e)
        {
            ExportMinutes(true, false, false, false, true);
        }

        private void btnOnlyMeetingEN_Click(object sender, System.EventArgs e)
        {
            ExportMinutes(true, false, false, true, true);
        }

        private void btnMinutesInvEN_Click(object sender, System.EventArgs e)
        {
            ExportMinutes(false, false, false, false, true);
        }

        private void btnEditIssues_Click(object sender, System.EventArgs e)
        {
            Response.Redirect("MeetingIssues.aspx?id=" + UIHelpers.GetIDParam().ToString() + "&PID=" + int.Parse(ddlProject1.SelectedValue));
        }

        private void btnWord1_Click(object sender, System.EventArgs e)
        {
            ExportMinutes(true, false, true, false, false);
        }

		

		
	}
}
