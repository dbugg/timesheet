using System;
using System.Data;
using System.Data.SqlClient; 
using System.Collections;
using Microsoft.ApplicationBlocks.Data;

namespace Asa.Timesheet.Data
{
	public class PaymentDAL
	{
		/// <summary>
		/// Loads a PaymentData object from the database using the given PaymentID
		/// </summary>
		/// <param name="paymentID">The primary key value</param>
		/// <returns>A PaymentData object</returns>
		public static PaymentData Load(int paymentID)
		{
			SqlParameter[] parameterValues = new SqlParameter[] { new SqlParameter("@PaymentID", paymentID) };
			using (SqlDataReader reader = SqlHelper.ExecuteReader(CommonDAL.ConnectionString, CommandType.StoredProcedure, "PaymentsSelProc", parameterValues))
			{
				if (reader.Read())
				{
					PaymentData result = new PaymentData();
					LoadFromReader(reader,result);
					return result;
				}
				else
					return null;
			}
		}

		/// <summary>
		/// Loads a PaymentData object from the database using the given where clause
		/// </summary>
		/// <param name="whereClause">The filter expression for the query</param>
		/// <returns>A PaymentData object</returns>
		public static PaymentData Load(string whereClause)
		{
			SqlParameter[] parameterValues = new SqlParameter[] { new SqlParameter("@WhereClause", whereClause) };
			using (SqlDataReader reader = SqlHelper.ExecuteReader(CommonDAL.ConnectionString, CommandType.StoredProcedure, "PaymentsListProc", parameterValues))
			{
				if (reader.Read())
				{
					PaymentData result = new PaymentData();
					LoadFromReader(reader,result);
					return result;
				}
				else
					return null;
			}
		}
		public static int Save( PaymentData source)
		{
			if (source.PaymentID == -1)
				return Insert(source);
			else
				return Update(source);	
		}

		public static void Delete( PaymentData source)
		{
			Delete(source.PaymentID);
		}
		/// <summary>
		/// Loads a collection of PaymentData objects from the database.
		/// </summary>
		/// <returns>A collection containing all of the PaymentData objects in the database.</returns>
		public static PaymentsVector LoadCollection()
		{
			SqlParameter[] parms = new SqlParameter[] {};
			return LoadCollection("PaymentsListProc", parms);
		}
	  
		/// <summary>
		/// Saves a collection of PaymentData objects to the database.
		/// </summary>
		/// <returns>A collection containing all of the PaymentData objects in the database.</returns>
		public static  void  SaveCollection(PaymentsVector col,string spName, SqlParameter[] parms)
		{
			PaymentsVector colOld = LoadCollection(spName,parms);
			UpdateCollection(colOld,col);
		}
		public static  void  SaveCollection(PaymentsVector col)
		{
			PaymentsVector colOld = LoadCollection();
			UpdateCollection(colOld,col);
		}
		public static  void  SaveCollection(PaymentsVector col, string whereClause )
		{
			PaymentsVector colOld = LoadCollection(whereClause);
			UpdateCollection(colOld,col);
		}
		/// <summary>
		/// Loads a collection of PaymentData objects from the database.
		/// </summary>
		/// <returns>A collection containing all of the PaymentData objects in the database.</returns>
		public static PaymentsVector LoadCollection(string whereClause)
		{
			SqlParameter[] parms = new SqlParameter[] { new SqlParameter("@WhereClause", whereClause) };
			return LoadCollection("PaymentsListProc", parms);
		}

		/// <summary>
		/// Loads a collection of PaymentData objects from the database.
		/// </summary>
		/// <returns>A collection containing all of the PaymentData objects in the database.</returns>
		public static PaymentsVector LoadCollection(string spName, SqlParameter[] parms)
		{
			PaymentsVector result = new PaymentsVector();
			using (SqlDataReader reader = SqlHelper.ExecuteReader(CommonDAL.ConnectionString, CommandType.StoredProcedure, spName, parms))
			{
				while (reader.Read())
				{
					PaymentData tmp = new PaymentData();
					LoadFromReader(reader, tmp);
					result.Add(tmp);
				}
			}
			return result;
		}
		/// <summary>
		/// Deletes a PaymentData object from the database.
		/// </summary>
		/// <param name="paymentID">The primary key value</param>
		public static void Delete(int paymentID)
		{
			SqlParameter[] parameterValues = new SqlParameter[] { new SqlParameter("@PaymentID", paymentID) };
			SqlHelper.ExecuteNonQuery(CommonDAL.ConnectionString, CommandType.StoredProcedure, "PaymentsDelProc", parameterValues);
		}

		public static void LoadFromReader(SqlDataReader reader, PaymentData target)
		{
			if (reader != null && !reader.IsClosed)
			{
				//target.Key = reader.GetInt32(0);
				if (!reader.IsDBNull(0)) target.PaymentID = reader.GetInt32(0);
				if (!reader.IsDBNull(1)) target.SubprojectID = reader.GetInt32(1);
				if (!reader.IsDBNull(2)) target.ProjectSubcontracterID = reader.GetInt32(2);
				if (!reader.IsDBNull(3)) target.PaymentPercent = reader.GetDecimal(3);
				if (!reader.IsDBNull(4)) target.Amount = reader.GetDecimal(4);
				if (!reader.IsDBNull(5)) target.Done = reader.GetBoolean(5);
				if (!reader.IsDBNull(6)) target.PaymentDate = reader.GetDateTime(6);
				if (!reader.IsDBNull(7)) target.PaidAmount = reader.GetDecimal(7);
				if (!reader.IsDBNull(8)) target.MailSent = reader.GetBoolean(8);
				if (!reader.IsDBNull(9)) target.Notes = reader.GetString(9);
				if (!reader.IsDBNull(10)) target.AuthorsID = reader.GetInt32(10);
				if (!reader.IsDBNull(11)) target.AuthorsStartDate = reader.GetDateTime(11);
				if (!reader.IsDBNull(12)) target.AuthorsEndDate = reader.GetDateTime(12);
				if (!reader.IsDBNull(13)) target.ClientID = reader.GetInt32(13);
			}
		}
		
	  
		public static int Insert( PaymentData source)
		{
			SqlParameter[] parameterValues = GetInsertParameterValues(source);
			SqlHelper.ExecuteNonQuery(CommonDAL.ConnectionString, CommandType.StoredProcedure, "PaymentsInsProc", parameterValues);
			source.PaymentID = (int) parameterValues[0].Value;
			return source.PaymentID;
		}

		public static int Update( PaymentData source)
		{
			SqlParameter[] parameterValues = GetUpdateParameterValues(source);
			SqlHelper.ExecuteNonQuery(CommonDAL.ConnectionString, CommandType.StoredProcedure,
				"PaymentsUpdProc", parameterValues);
			return source.PaymentID;
		}
		private static void UpdateCollection(PaymentsVector colOld,PaymentsVector col)
		{
			// Delete all old items
			foreach( PaymentData itemOld in colOld)
			{
				if(!ExistsKey(itemOld.PaymentID, col))
					Delete(itemOld.PaymentID);
			}
			foreach( PaymentData item in col)
			{
				if(item.PaymentID <= -1)
					Insert(item);
				else
					Update(item);
			}
		}
		private static bool ExistsKey(int Key, PaymentsVector col)
		{
			foreach( PaymentData item in col)
				if(item.PaymentID==Key)
					return true;
			return false;
		}

		private static SqlParameter GetSqlParameter(string name, ParameterDirection direction, object value)
		{
			SqlParameter p = new SqlParameter(name, value);
			p.Direction = direction;
			return p;
		}

		private static SqlParameter[] GetInsertParameterValues( PaymentData source)
		{
			SqlParameter[] prams = new SqlParameter[15];
			prams[0] = GetSqlParameter("@PaymentID", ParameterDirection.Output, source.PaymentID);
			if(source.SubprojectID<=0)
				prams[1] = GetSqlParameter("@SubprojectID", ParameterDirection.Input, DBNull.Value);
			else
				prams[1] = GetSqlParameter("@SubprojectID", ParameterDirection.Input, source.SubprojectID);
			if(source.ProjectSubcontracterID<=0)
				prams[2] = GetSqlParameter("@ProjectSubcontracterID", ParameterDirection.Input, DBNull.Value);
			else
				prams[2] = GetSqlParameter("@ProjectSubcontracterID", ParameterDirection.Input, source.ProjectSubcontracterID);
			prams[3] = GetSqlParameter("@PaymentPercent", ParameterDirection.Input, decimal.Round(source.PaymentPercent,8));
			prams[4] = GetSqlParameter("@Amount", ParameterDirection.Input, source.Amount);
			prams[5] = GetSqlParameter("@Done", ParameterDirection.Input, source.Done);
			if((source.PaymentDate==Constants.DateMax)||(source.PaymentDate==new DateTime()))
				prams[6] = GetSqlParameter("@PaymentDate", ParameterDirection.Input, DBNull.Value);
			else
				prams[6] = GetSqlParameter("@PaymentDate", ParameterDirection.Input, source.PaymentDate);
			prams[7] = GetSqlParameter("@PaidAmount", ParameterDirection.Input, source.PaidAmount);
			prams[8] = GetSqlParameter("@MailSent", ParameterDirection.Input, source.MailSent);
			prams[9] = GetSqlParameter("@IsSubproject", ParameterDirection.Input, source.IsSubproject);
			prams[10] = GetSqlParameter("@Notes", ParameterDirection.Input, source.Notes);
			if(source.AuthorsID<=0)
				prams[11] = GetSqlParameter("@AuthorsID", ParameterDirection.Input, DBNull.Value);
			else
				prams[11] = GetSqlParameter("@AuthorsID", ParameterDirection.Input, source.AuthorsID);
			if( source.AuthorsStartDate==new DateTime())
				prams[12] = GetSqlParameter("@AuthorsStartDate", ParameterDirection.Input, DBNull.Value);
			else
				prams[12] = GetSqlParameter("@AuthorsStartDate", ParameterDirection.Input, source.AuthorsStartDate);
			if( source.AuthorsEndDate==new DateTime() )
				prams[13] = GetSqlParameter("@AuthorsEndDate", ParameterDirection.Input, DBNull.Value);
			else
				prams[13] = GetSqlParameter("@AuthorsEndDate", ParameterDirection.Input, source.AuthorsEndDate);
			if(source.ClientID<0)
				prams[14] = GetSqlParameter("@ClientID", ParameterDirection.Input, DBNull.Value);
			else
				prams[14] =  GetSqlParameter("@ClientID", ParameterDirection.Input, source.ClientID);
			return prams;
		}

		private static SqlParameter[] GetUpdateParameterValues( PaymentData source)
		{
			SqlParameter[] prm = GetInsertParameterValues(source);
			prm[0].Direction=  ParameterDirection.Input;
			return prm;
//			return new SqlParameter[] {
//										  new SqlParameter("@PaymentID", source.PaymentID),
//										  new SqlParameter("@SubprojectID", source.SubprojectID),
//										  new SqlParameter("@ProjectSubcontracterID", source.ProjectSubcontracterID),
//										  new SqlParameter("@PaymentPercent", source.PaymentPercent),
//										  new SqlParameter("@Amount", source.Amount),
//										  new SqlParameter("@Done", source.Done),
//										  new SqlParameter("@PaymentDate", source.PaymentDate),
//										  new SqlParameter("@PaidAmount", source.PaidAmount),
//										  new SqlParameter("@MailSent", source.MailSent)
//									  };
		}
	}
}   

