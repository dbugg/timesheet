﻿<%@ Register TagPrefix="uc1" TagName="PageHeader" Src="../UserControls/PageHeader.ascx" %>
<%@ Register TagPrefix="activereportsweb" Namespace="GrapeCity.ActiveReports.Web" Assembly="GrapeCity.ActiveReports.Web.v7, Version=7.1.7470.0, Culture=neutral, PublicKeyToken=cc4967777c49a3ff" %>
<%@ Page language="c#" Codebehind="WorkTimes.aspx.cs" AutoEventWireup="false" Inherits="Asa.Timesheet.WebPages.Reports.WorkTimes"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
	<head runat="server">
		<title>WorkTimes</title>
		<meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
		<meta content="C#" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<LINK href="../styles/timesheet.css" type="text/css" rel="stylesheet">
		<script language="javascript">
		
		function SetGroupOrder()
		{
			var srcElement = event.srcElement;
			var newValue = srcElement.value;
			
			var arr = new Array();
			if (document.getElementById("ddlUserOrder")!=srcElement) arr.push(document.getElementById("ddlUserOrder"));
			if (document.getElementById("ddlProjectOrder")!=srcElement) arr.push(document.getElementById("ddlProjectOrder"));
			if (document.getElementById("ddlActivityOrder")!=srcElement) arr.push(document.getElementById("ddlActivityOrder"));
			if (document.getElementById("ddlBuildingTypeOrder")!=srcElement) arr.push(document.getElementById("ddlBuildingTypeOrder"));
			
			for (i=0; i<arr.length-1; i++)
			{
				for (j=i+1; j<arr.length; j++)
				if (arr[j].value<arr[i].value)
				{
					var k = arr[i]; arr[i] = arr[j]; arr[j] = k;
				}
				
			}
			
			var numbers = new Array();
			for (i=0; i<=3; i++) if (i!=newValue) numbers.push(i);
			
			for (i=0; i<numbers.length; i++) arr[i].value = numbers[i];
		}
		
		</script>
	</HEAD>
	<body bottomMargin="0" leftMargin="0" topMargin="0" rightMargin="0">
		<form id="Form1" method="post" runat="server">
			<table height="100%" cellSpacing="0" cellPadding="0" width="100%" border="0">
				<tr height="1">
					<td><uc1:pageheader id="header" runat="server"></uc1:pageheader></td>
				</tr>
				<tr style="DISPLAY: none" height="80">
					<td background="../images/copy1.gif">
						<TABLE id="Table2" height="100%" width="100%">
							<TR>
								<td style="WIDTH: 106px">&nbsp;&nbsp;&nbsp;&nbsp; <IMG height="80" alt="" src="../images/logo.jpg" width="64"></TD>
								<td noWrap><asp:label id="lblTitle" runat="server" Font-Bold="True" Font-Size="X-Small"></asp:label><br>
									<asp:label id="lblUser" runat="server" Font-Bold="True" Font-Size="Medium" ForeColor="SaddleBrown"></asp:label></TD>
							</TR>
						</TABLE>
					</td>
				</tr>
				<tr>
					<td>
						<table height="100%" cellSpacing="0" cellPadding="0" width="100%" border="0">
							<tr>
								<td class="menuLeft"><asp:placeholder id="menuHolder" runat="server"></asp:placeholder></td>
								
								<td style="PADDING-LEFT: 5px; PADDING-RIGHT: 5px; PADDING-TOP: 3px" vAlign="top" noWrap>
									<TABLE id="tblInfo" height="30" cellSpacing="0" cellPadding="3" width="880" border="0">
										<TR>
											<td vAlign="middle" noWrap><asp:label id="lblError" runat="server" ForeColor="Red" CssClass="ErrorLabel" EnableViewState="False"></asp:label><asp:label id="lblInfo" runat="server" CssClass="InfoLabel" EnableViewState="False"></asp:label></TD>
										</TR>
									</TABLE>
									<TABLE id="table" cellSpacing="0" cellPadding="0" width="880" border="0" runat="server">
										<TR>
											<td style="HEIGHT: 5px" colSpan="2"></TD>
										</TR>
										<TR>
											<td colSpan="2" height="3"><asp:checkbox id="ckDates" runat="server" CssClass="EnterDataLabel" AutoPostBack="True" Checked="True"
													Text="За целия период" Width="144px"></asp:checkbox></td>
										</TR>
										<TR>
											<td>
												<TABLE id="tblDays" cellSpacing="0" cellPadding="3" border="0" runat="server">
													<TR id="SelectDateRow">
														<td style="WIDTH: 139px; HEIGHT: 4px" vAlign="top" align="right"><asp:label id="lblStartDay" runat="server" CssClass="EnterDataLabel">Ден, седмица, нач.дата:</asp:label><asp:label id="lblSelStartDate" runat="server" Font-Bold="True" ForeColor="SaddleBrown" Width="100px"></asp:label></TD>
														<td style="WIDTH: 72px" vAlign="top">
															<TABLE id="Table1" cellSpacing="0" cellPadding="0" border="0">
																<TR>
																	<td><asp:calendar id="calStartDate" runat="server" ForeColor="#222222" Width="240px" SelectionMode="DayWeek"
																			BorderColor="#999999" CellPadding="0" BorderWidth="1px" BackColor="White" Height="240px" NextMonthText=""
																			PrevMonthText="" DayNameFormat="FirstLetter" SelectWeekText="+"
																			FirstDayOfWeek="Monday" BorderStyle="Solid" CssClass="aspCalendar" NextPrevFormat="ShortMonth" Font-Bold="True" Font-Size="11pt">
																			<TodayDayStyle ForeColor="#C73B2E" BackColor="White" BorderColor="#C73B2E" 
                                                                                BorderStyle="Solid" BorderWidth="2px" Height="32px" Width="32px"></TodayDayStyle>
																			<SelectorStyle Font-Size="10pt" Font-Names="Verdana" ForeColor="SaddleBrown" 
                                                                                BorderStyle="None"></SelectorStyle>
																			<DayStyle Font-Size="10pt" Font-Bold="False" BorderStyle="Dotted" 
                                                                                BorderColor="#CCCCCC" BorderWidth="1px" Height="30px" Width="32px"></DayStyle>
																			<NextPrevStyle Font-Bold="True" Wrap="False" HorizontalAlign="Center" 
                                                                                Height="16px" BorderStyle="None" VerticalAlign="Middle" Font-Size="9pt" 
                                                                                Font-Underline="True"></NextPrevStyle>
																			<DayHeaderStyle Font-Size="9pt" Height="1px" ForeColor="SaddleBrown"></DayHeaderStyle>
																			<SelectedDayStyle Font-Bold="True" ForeColor="#C73B2E" BorderColor="#C73B2E" 
                                                                                BackColor="White" BorderStyle="Solid" BorderWidth="2px"
                                                                                Font-Size="11pt" Height="32px" Width="32px"></SelectedDayStyle>
																			<TitleStyle Font-Size="13pt" Font-Bold="True" ForeColor="#C73B2E" 
                                                                                BackColor="White" HorizontalAlign="Center"></TitleStyle>
																			<OtherMonthDayStyle ForeColor="Gray" BackColor="#FDFDFD" Font-Size="6pt"></OtherMonthDayStyle>
																		    <WeekendDayStyle Font-Bold="True" Font-Italic="False" 
                                                                                ForeColor="#C73B2E" />
																		</asp:calendar></TD>
																	<td vAlign="top" noWrap></TD>
																</TR>
															</TABLE>
														</TD>
														<td style="WIDTH: 117px" vAlign="top" align="right"><asp:label id="lblEndDay" runat="server" CssClass="EnterDataLabel">Крайна дата:</asp:label><asp:label id="lblSelEndDate" runat="server" Font-Bold="True" ForeColor="SaddleBrown" Width="100px"></asp:label></TD>
														<td style="WIDTH: 278px; HEIGHT: 4px" vAlign="top">
															<TABLE id="Table6" cellSpacing="0" cellPadding="0" border="0">
																<TR>
																	<td><asp:calendar id="calEndDate" runat="server" ForeColor="#222222" Width="240px" SelectionMode="DayWeek"
																			BorderColor="#999999" CellPadding="0" BorderWidth="1px" BackColor="White" Height="240px" NextMonthText=""
																			PrevMonthText="" DayNameFormat="FirstLetter" SelectWeekText="+"
																			FirstDayOfWeek="Monday" BorderStyle="Solid" CssClass="aspCalendar" NextPrevFormat="ShortMonth" Font-Bold="True" Font-Size="11pt">
																			<TodayDayStyle ForeColor="#C73B2E" BackColor="White" BorderColor="#C73B2E" 
                                                                                BorderStyle="Solid" BorderWidth="2px" Height="32px" Width="32px"></TodayDayStyle>
																			<SelectorStyle Font-Size="10pt" Font-Names="Verdana" ForeColor="SaddleBrown" 
                                                                                BorderStyle="None"></SelectorStyle>
																			<DayStyle Font-Size="10pt" Font-Bold="False" BorderStyle="Dotted" 
                                                                                BorderColor="#CCCCCC" BorderWidth="1px" Height="30px" Width="32px"></DayStyle>
																			<NextPrevStyle Font-Bold="True" Wrap="False" HorizontalAlign="Center" 
                                                                                Height="16px" BorderStyle="None" VerticalAlign="Middle" Font-Size="9pt" 
                                                                                Font-Underline="True"></NextPrevStyle>
																			<DayHeaderStyle Font-Size="9pt" Height="1px" ForeColor="SaddleBrown"></DayHeaderStyle>
																			<SelectedDayStyle Font-Bold="True" ForeColor="#C73B2E" BorderColor="#C73B2E" 
                                                                                BackColor="White" BorderStyle="Solid" BorderWidth="2px"
                                                                                Font-Size="11pt" Height="32px" Width="32px"></SelectedDayStyle>
																			<TitleStyle Font-Size="13pt" Font-Bold="True" ForeColor="#C73B2E" 
                                                                                BackColor="White" HorizontalAlign="Center"></TitleStyle>
																			<OtherMonthDayStyle ForeColor="Gray" BackColor="#FDFDFD" Font-Size="6pt"></OtherMonthDayStyle>
																		    <WeekendDayStyle Font-Bold="True" Font-Italic="False" 
                                                                                ForeColor="#C73B2E" />
																		</asp:calendar></TD>
																	<td vAlign="top" noWrap></TD>
																</TR>
															</TABLE>
														</TD>
													</TR>
												</TABLE>
											</TD>
										</TR>
										<TR>
											<td style="HEIGHT: 3px" colSpan="2"></TD>
										</TR>
										<TR>
											<td colSpan="2" height="3"></td>
										</TR>
										<tr>
											<td>
												<table style="WIDTH: 728px" cellSpacing="0" cellPadding="3" border="0" runat="server" id="tblFilter">
													<TR>
														<td style="WIDTH: 145px; HEIGHT: 17px"><asp:label id="lbMonth" runat="server" CssClass="EnterDataLabel" Width="100%" Visible="False">Месец:</asp:label></TD>
														<td style="WIDTH: 243px; HEIGHT: 17px"><asp:dropdownlist id="ddlMonth" runat="server" CssClass="EnterDataBox" Width="250px" Visible="False"></asp:dropdownlist></TD>
														<td style="HEIGHT: 17px"></TD>
														<td style="HEIGHT: 17px"></TD>
														<td style="HEIGHT: 17px"></TD>
														<td style="WIDTH: 8px; HEIGHT: 17px"></TD>
														<td style="WIDTH: 8px; HEIGHT: 17px"></TD>
													</TR>
													<TR>
														<td style="WIDTH: 145px; HEIGHT: 17px"><asp:label id="Label20" runat="server" CssClass="enterDataLabel" Width="100%">Тип:</asp:label></TD>
														<td style="WIDTH: 243px; HEIGHT: 17px"><asp:dropdownlist id="ddlProjectStatus" runat="server" CssClass="EnterDataBox" AutoPostBack="True"
																Width="250px"></asp:dropdownlist></TD>
														<td style="HEIGHT: 17px"></TD>
														<td style="HEIGHT: 17px"></TD>
														<td style="HEIGHT: 17px"></TD>
														<td style="WIDTH: 8px; HEIGHT: 17px"></TD>
														<td style="WIDTH: 8px; HEIGHT: 17px"></TD>
													</TR>
													<TR>
														<td style="WIDTH: 145px; HEIGHT: 2px"><asp:label id="lblBuildingType" runat="server" CssClass="enterDataLabel" >Вид сграда:</asp:label></TD>
														<td style="WIDTH: 243px; HEIGHT: 2px"><asp:dropdownlist id="ddlBuildingType" runat="server" CssClass="EnterDataBox" AutoPostBack="true"
																Width="250px"></asp:dropdownlist></TD>
														<td style="HEIGHT: 2px"><asp:dropdownlist id="ddlBuildingTypeOrder" runat="server" Width="40px">
																<asp:ListItem Value="0">1</asp:ListItem>
																<asp:ListItem Value="1">2</asp:ListItem>
																<asp:ListItem Value="2">3</asp:ListItem>
																<asp:ListItem Value="3" Selected="True">4</asp:ListItem>
															</asp:dropdownlist></TD>
														<td></TD>
														<td style="HEIGHT: 2px"></TD>
														<td style="WIDTH: 8px; HEIGHT: 2px"></TD>
														<td style="WIDTH: 8px; HEIGHT: 2px"></TD>
													</TR>
													<TR>
														<td style="WIDTH: 145px; HEIGHT: 2px"><asp:label id="lblProject" runat="server" CssClass="enterDataLabel" Width="100%">Проект:</asp:label></TD>
														<td style="WIDTH: 243px; HEIGHT: 2px"><asp:dropdownlist id="ddlProject" runat="server" CssClass="EnterDataBox" Width="250px"></asp:dropdownlist></TD>
														<td style="HEIGHT: 2px"><asp:dropdownlist id="ddlProjectOrder" runat="server" Width="40px">
																<asp:ListItem Value="0" Selected="True">1</asp:ListItem>
																<asp:ListItem Value="1">2</asp:ListItem>
																<asp:ListItem Value="2">3</asp:ListItem>
																<asp:ListItem Value="3">4</asp:ListItem>
															</asp:dropdownlist></TD>
														<td></TD>
														<td style="HEIGHT: 2px"></TD>
														<td style="WIDTH: 8px; HEIGHT: 2px"></TD>
														<td style="WIDTH: 8px; HEIGHT: 2px"></TD>
													</TR>
													<TR>
														<td style="WIDTH: 145px" vAlign="top"><asp:label id="lblSluj" runat="server" CssClass="enterDataLabel" Width="100%">Служител:</asp:label></TD>
														<td style="WIDTH: 243px" vAlign="top"><asp:dropdownlist id="ddlUser" runat="server" CssClass="EnterDataBox" Width="250px"></asp:dropdownlist></TD>
														<td vAlign="top"><asp:dropdownlist id="ddlUserOrder" runat="server" Width="40px">
																<asp:ListItem Value="0">1</asp:ListItem>
																<asp:ListItem Value="1">2</asp:ListItem>
																<asp:ListItem Value="2" Selected="True">3</asp:ListItem>
																<asp:ListItem Value="3">4</asp:ListItem>
															</asp:dropdownlist></TD>
														<td></TD>
														<td><nobr>&nbsp;&nbsp;<asp:dropdownlist id="ddlUserStatus" runat="server" CssClass="EnterDataBox" AutoPostBack="True" Width="200px">
																	<asp:ListItem Value="0" Selected>&lt;активни и пасивни&gt;</asp:ListItem>
																	<asp:ListItem Value="1" >&lt;активни служители&gt;</asp:ListItem>
																	<asp:ListItem Value="2">&lt;пасивни служители&gt;</asp:ListItem>
																</asp:dropdownlist></nobr></TD>
														<td style="WIDTH: 8px"></TD>
														<td style="WIDTH: 8px"></TD>
													</TR>
													<TR>
														<td style="WIDTH: 145px; HEIGHT: 19px"><asp:label id="lblActivity" runat="server" CssClass="enterDataLabel"> Дейност:</asp:label></TD>
														<td style="WIDTH: 243px; HEIGHT: 19px"><asp:dropdownlist id="ddlActivity" runat="server" CssClass="EnterDataBox" Width="250px"></asp:dropdownlist></TD>
														<td style="HEIGHT: 19px"><asp:dropdownlist id="ddlActivityOrder" runat="server" Width="40px">
																<asp:ListItem Value="0">1</asp:ListItem>
																<asp:ListItem Value="1" Selected="True">2</asp:ListItem>
																<asp:ListItem Value="2">3</asp:ListItem>
																<asp:ListItem Value="3">4</asp:ListItem>
															</asp:dropdownlist></TD>
														<td style="HEIGHT: 19px"></TD>
														<td style="HEIGHT: 19px"></TD>
														<td style="WIDTH: 8px; HEIGHT: 19px"></TD>
														<td style="WIDTH: 8px; HEIGHT: 19px"></TD>
													</TR>
												</table>
												<table style="WIDTH: 728px" cellSpacing="0" cellPadding="3" border="0">
													<TR>
														<td style="WIDTH: 145px" vAlign="top" height="10"><asp:label id="lbShowExtraGrid" CssClass="enterDataLabel" Visible="False" Runat="server">Общо за служители:</asp:label></TD>
														<td style="WIDTH: 243px" vAlign="top" height="10"><asp:checkbox id="cbShowExtraGrid" Checked="False" Visible="False" Runat="server"></asp:checkbox></TD>
														<td vAlign="top" height="10"></TD>
														<td height="10" style="WIDTH: 72px"></TD>
														<td height="10"></TD>
														<td style="WIDTH: 8px" height="10"></TD>
														<td style="WIDTH: 8px" height="10"></TD>
													</TR>
													<TR>
														<td style="WIDTH: 145px" vAlign="top" height="10"><asp:label id="lbNotWorkTime" CssClass="enterDataLabel" Visible="False" Runat="server">По-малко раб. време:</asp:label><asp:label id="lbCoef" CssClass="enterDataLabel" Visible="False" Runat="server">Коефициент за заплата:</asp:label></TD>
														<td style="WIDTH: 243px" vAlign="top" height="10"><asp:checkbox id="cbNotWorkTime" Checked="False" Visible="False" Runat="server"></asp:checkbox><asp:textbox id="txtCoef" runat="server" Visible="FALSE"></asp:textbox></TD>
														<td vAlign="top" height="10"></TD>
														<td height="10" style="WIDTH: 72px"></TD>
														<td height="10"></TD>
														<td style="WIDTH: 8px" height="10"></TD>
														<td style="WIDTH: 8px" height="10"></TD>
													</TR>
													<TR>
														<td style="WIDTH: 145px" vAlign="top" height="10"><asp:label id="lbShowLate" CssClass="enterDataLabel" Visible="False" Runat="server">Закъснения<BR> (работа от-до):</asp:label></TD>
														<td style="WIDTH: 243px" vAlign="top" height="10"><asp:checkbox id="ckLate" Checked="False" Visible="False" Runat="server"></asp:checkbox></TD>
														<td vAlign="top" height="10"></TD>
														<td height="10" style="WIDTH: 72px"></TD>
														<td height="10"><asp:imagebutton id="ibGenerate" runat="server" ImageUrl="../images/ie.gif"></asp:imagebutton><asp:imagebutton id="ibPdfExport" runat="server" ImageUrl="../images/pdf1.gif" ToolTip="Справка в PDF формат"></asp:imagebutton>&nbsp;
															<asp:imagebutton id="ibXlsExport" runat="server" ImageUrl="../images/xls.gif" ToolTip="Справка в Excel формат"></asp:imagebutton></TD>
														<td style="WIDTH: 8px" height="10"></TD>
														<td style="WIDTH: 8px" height="10"></TD>
													</TR>
												</table>
											</td>
										</tr>
										<TR>
											<td style="HEIGHT: 5px" colSpan="2"></TD>
										</TR>
										<TR>
											<td colSpan="2"><IMG height="1" src="../images/dot.gif" width="100%"></TD>
										</TR>
										<TR>
											<td colSpan="2" height="3"></TD>
										</TR>
										<TR>
											<td colSpan="2"><IMG height="1" src="images/dot.gif" width="100%"></TD>
										</TR>
									</TABLE>
									<br>
									<TABLE id="tblHeader" cellSpacing="0" cellPadding="2" border="0" runat="server">
										<TR>
											<td vAlign="top"><asp:label id="Label1" runat="server" Font-Bold="True" Font-Size="9pt">ОБЕКТ:</asp:label></TD>
											<td vAlign="top"><asp:label id="lblObekt" runat="server" Font-Size="9pt"></asp:label></TD>
										</TR>
										<TR>
											<td><asp:label id="Label3" runat="server" Font-Bold="True" Font-Size="9pt">ИНВЕСТИТОР:</asp:label></TD>
											<td><asp:label id="lblClient" runat="server" Font-Size="9pt"></asp:label></TD>
										</TR>
										<TR>
											<td></TD>
											<td><asp:label id="lblClientAddress" runat="server" Font-Size="9pt"></asp:label></TD>
										</TR>
										<TR>
											<td noWrap><asp:label id="Label6" runat="server" Font-Bold="True" Font-Size="9pt">ГЛАВЕН ПРОЕКТАНТ:</asp:label></TD>
											<td><asp:label id="lblProektant" runat="server" Font-Size="9pt"></asp:label></TD>
										</TR>
										<TR>
											<td noWrap><asp:label id="Label18" runat="server" Font-Bold="True" Font-Size="9pt">ПЕРИОД:</asp:label></TD>
											<td><asp:label id="lblPeriod" runat="server" Font-Size="9pt"></asp:label></TD>
										</TR>
										<TR>
											<td style="HEIGHT: 19px" noWrap><asp:label id="Label15" runat="server" Font-Bold="True" Font-Size="9pt">СЛУЖИТЕЛ:</asp:label></TD>
											<td style="HEIGHT: 19px"><asp:label id="lblReportUser" runat="server" Font-Size="9pt"></asp:label></TD>
										</TR>
										<TR>
											<td noWrap><asp:label id="Label16" runat="server" Font-Bold="True" Font-Size="9pt">ДЕЙНОСТ:</asp:label></TD>
											<td><asp:label id="lblReportActivity" runat="server" Font-Size="9pt"></asp:label></TD>
										</TR>
										<TR>
											<td noWrap><asp:label id="Label17" runat="server" Font-Bold="True" Font-Size="9pt">ВИД СГРАДА:</asp:label></TD>
											<td><asp:label id="lblReportBuildingType" runat="server" Font-Size="9pt"></asp:label></TD>
										</TR>
									</TABLE>
									<br>
									<TABLE id="Table5" cellSpacing="0" cellPadding="0" width="100%" border="0">
										<TR>
											<td align="center"><asp:label id="lblReportTitle" runat="server" Font-Bold="True" Font-Size="11pt" EnableViewState="False"
													Visible="False">СПРАВКА ЗА РАБОТНО ВРЕМЕ</asp:label></TD>
										</TR>
									</TABLE>
									<br>
									<asp:datagrid id="grdReport" runat="server" ForeColor="DimGray" CssClass="ReportGrid" EnableViewState="False"
										Width="100%" CellPadding="4" ShowHeader="True" AutoGenerateColumns="False">
										<HeaderStyle CssClass="reportGridHeader"></HeaderStyle>
										<Columns>
											<asp:BoundColumn DataField="Group1Name"></asp:BoundColumn>
											<asp:BoundColumn DataField="Group2Name"></asp:BoundColumn>
											<asp:BoundColumn DataField="Group3Name"></asp:BoundColumn>
											<asp:BoundColumn DataField="Group4Name"></asp:BoundColumn>
											<asp:BoundColumn DataField="WorkDate" HeaderText="Дата" DataFormatString="{0:dd.MM.yyyy}" HeaderStyle-HorizontalAlign="Right"
												ItemStyle-HorizontalAlign="Right"></asp:BoundColumn>
											<asp:BoundColumn DataField="Amount" HeaderText="Заплата (BGN)" HeaderStyle-HorizontalAlign="Right"
												ItemStyle-HorizontalAlign="Right"></asp:BoundColumn>
											<asp:BoundColumn DataField="Time" HeaderText="Часове" HeaderStyle-HorizontalAlign="Right" ItemStyle-HorizontalAlign="Right"></asp:BoundColumn>
										</Columns>
									</asp:datagrid><asp:label id="lblNoDataFound" runat="server" CssClass="InfoLabel" EnableViewState="False"
										Visible="False"></asp:label><BR>
									<br>
									<asp:datagrid id="grdLate" runat="server" ForeColor="DimGray" CssClass="ReportGrid" EnableViewState="False"
										Width="800px" CellPadding="4" Visible="False" ShowHeader="True" AutoGenerateColumns="False">
										<HeaderStyle CssClass="reportGridHeader"></HeaderStyle>
										<Columns>
											<asp:BoundColumn DataField="UserID" HeaderText="" Visible="false"></asp:BoundColumn>
											<asp:BoundColumn DataField="FullName" HeaderText="Служител"></asp:BoundColumn>
											<asp:BoundColumn DataField="WorkDate" HeaderText="Дата" DataFormatString="{0:dd.MM.yyyy}">
												<HeaderStyle HorizontalAlign="Center"></HeaderStyle>
												<ItemStyle HorizontalAlign="Center"></ItemStyle>
											</asp:BoundColumn>
											<asp:BoundColumn DataField="StartTime" HeaderText="Начало">
												<HeaderStyle HorizontalAlign="Center"></HeaderStyle>
												<ItemStyle HorizontalAlign="Center"></ItemStyle>
											</asp:BoundColumn>
											<asp:BoundColumn DataField="EndTime" HeaderText="Край">
												<HeaderStyle HorizontalAlign="Center"></HeaderStyle>
												<ItemStyle HorizontalAlign="Center"></ItemStyle>
											</asp:BoundColumn>
											<asp:BoundColumn DataField="MinutesLate" HeaderText="Закъснение">
												<HeaderStyle HorizontalAlign="Right"></HeaderStyle>
												<ItemStyle HorizontalAlign="Right"></ItemStyle>
											</asp:BoundColumn>
											<asp:BoundColumn DataField="Details" HeaderText="Детайли(часове от-до)">
												<HeaderStyle HorizontalAlign="Right"></HeaderStyle>
												<ItemStyle HorizontalAlign="Right"></ItemStyle>
											</asp:BoundColumn>
											<asp:BoundColumn DataField="TotalMinutes" HeaderText="Общо работни часове">
												<HeaderStyle HorizontalAlign="Right"></HeaderStyle>
												<ItemStyle HorizontalAlign="Right"></ItemStyle>
											</asp:BoundColumn>
										</Columns>
									</asp:datagrid><br>
									<TABLE id="tblHeadFooter" cellSpacing="0" cellPadding="2" width="100%" border="0" runat="server">
										<TR>
											<td><asp:label id="Label4" runat="server" Font-Bold="True" Font-Size="9pt">ЗА ИЗБРАНИЯ ФИЛТЪР:</asp:label></TD>
											<td></TD>
										</TR>
										<TR>
											<td><asp:label id="Label2" runat="server" Font-Size="9pt">ОБЩО ИЗРАБОТЕНИ ЧАСОВЕ:</asp:label><asp:label id="lblTotalHours" runat="server" Font-Bold="True" Font-Size="9pt"></asp:label></TD>
											<td></TD>
										</TR>
									</TABLE>
									<TABLE id="tblFooter" cellSpacing="0" cellPadding="2" width="100%" border="0" runat="server">
										<TR>
											<td><asp:label id="lbSalary1" runat="server" Font-Size="9pt"></asp:label><asp:label id="lbSalary2" runat="server" Font-Bold="True" Font-Size="9pt"></asp:label></TD>
											<td></TD>
										</TR>
										<TR>
											<td noWrap><asp:label id="Label14" runat="server" Font-Size="9pt">ОБЩО ИЗР. ЧАСОВЕ В ИЗВЪНРАБОТНО ВРЕМЕ:</asp:label><asp:label id="lblTotalNonWorkDays" runat="server" Font-Bold="True" Font-Size="9pt"></asp:label></TD>
											<td></TD>
										</TR>
										<TR>
											<td noWrap><asp:label id="Label13" runat="server" Font-Size="9pt">ОБЩО ИЗР. ЧАСОВЕ В РАБОТНО ВРЕМЕ:</asp:label><asp:label id="lblTotalWorkDays" runat="server" Font-Bold="True" Font-Size="9pt"></asp:label></TD>
											<td></TD>
										</TR>
										<TR>
											<td noWrap>
												<asp:label id="Label21" runat="server" Font-Size="9pt">ОБЩО НЕИЗРАБОТЕНИ ЧАСОВЕ ЗА МЕСЕЦА:</asp:label>
												<asp:label id="lblMissingHours" runat="server" Font-Size="9pt" Font-Bold="True"></asp:label></TD>
											<td></TD>
										</TR>
										<TR>
											<td noWrap></TD>
											<td></TD>
										</TR>
										<TR>
											<td noWrap><asp:label id="Label19" runat="server" Font-Size="9pt">ОБЩ БРОЙ ЗАКЪСНЕНИЯ:</asp:label><asp:label id="lbTotalLateCount" runat="server" Font-Bold="True" Font-Size="9pt"></asp:label></TD>
											<td></TD>
										</TR>
										<TR>
											<td noWrap><asp:label id="Label22" runat="server" Font-Size="9pt">ОБЩО ЧАСОВЕ ЗАКЪСНЕНИЯ:</asp:label><asp:label id="lbTotalLate" runat="server" Font-Bold="True" Font-Size="9pt"></asp:label></TD>
											<td></TD>
										</TR>
										<TR>
											<td noWrap></TD>
											<td></TD>
										</TR>
									</TABLE>
									<TABLE id="tblFooter1" cellSpacing="0" cellPadding="2" width="100%" border="0" runat="server">
										<TR>
											<td noWrap><asp:label id="Label9" runat="server" Font-Bold="True" Font-Size="9pt">ЗА ПЕРИОДА</asp:label></TD>
											<td></TD>
										</TR>
										<TR>
											<td noWrap><asp:label id="Label10" runat="server" Font-Size="9pt">ОБЩО ДНИ ПЛАТЕН ОТПУСК:</asp:label><asp:label id="lblDaysPO" runat="server" Font-Bold="True" Font-Size="9pt"></asp:label>&nbsp;
												<asp:label id="lblDaysPOString" runat="server" Font-Size="9pt"></asp:label></TD>
											<td></TD>
										</TR>
										<TR>
											<td noWrap><asp:label id="Label11" runat="server" Font-Size="9pt">ОБЩО ДНИ НЕПЛАТЕН ОТПУСК:</asp:label><asp:label id="lblDaysNO" runat="server" Font-Bold="True" Font-Size="9pt"></asp:label>&nbsp;
												<asp:label id="lblDaysNOString" runat="server" Font-Size="9pt"></asp:label></TD>
											<td></TD>
										</TR>
										<TR>
											<td noWrap><asp:label id="Label12" runat="server" Font-Size="9pt">ОБЩО ДНИ БОЛНИЧНИ:</asp:label><asp:label id="lblDaysIll" runat="server" Font-Bold="True" Font-Size="9pt"></asp:label>&nbsp;
												<asp:label id="lblDaysIllString" runat="server" Font-Size="9pt"></asp:label></TD>
											<td></TD>
										</TR>
										<TR>
											<td noWrap></TD>
											<td></TD>
										</TR>
									</TABLE>
									<asp:datagrid id="grdOneEmployee" runat="server" ForeColor="DimGray" CssClass="ReportGrid" EnableViewState="False"
										Width="100%" CellPadding="4" Visible="False" ShowHeader="True" AutoGenerateColumns="False">
										<HeaderStyle CssClass="reportGridHeader"></HeaderStyle>
										<Columns>
											<asp:BoundColumn DataField="monthyear" HeaderText="Месец"></asp:BoundColumn>
											<asp:BoundColumn DataField="WorkedMinutesNormal" HeaderText="Норматив часове">
												<HeaderStyle HorizontalAlign="Right"></HeaderStyle>
												<ItemStyle Font-Bold="True" HorizontalAlign="Right"></ItemStyle>
											</asp:BoundColumn>
											<asp:BoundColumn DataField="TotalTime" HeaderText="Изработени часове">
												<HeaderStyle HorizontalAlign="Right"></HeaderStyle>
												<ItemStyle Font-Bold="True" HorizontalAlign="Right"></ItemStyle>
											</asp:BoundColumn>
											<asp:BoundColumn DataField="UnderTime" HeaderText="Неизработени часове">
												<HeaderStyle HorizontalAlign="Right"></HeaderStyle>
												<ItemStyle Font-Bold="True" HorizontalAlign="Right"></ItemStyle>
											</asp:BoundColumn>
											<asp:BoundColumn DataField="OverTime" HeaderText="Надработени часове">
												<HeaderStyle HorizontalAlign="Right"></HeaderStyle>
												<ItemStyle Font-Bold="True" HorizontalAlign="Right"></ItemStyle>
											</asp:BoundColumn>
											<asp:BoundColumn DataField="TotalAmount" HeaderText="Заплата за надработено време" DataFormatString="{0:n2}">
												<HeaderStyle HorizontalAlign="Right"></HeaderStyle>
												<ItemStyle Font-Bold="True" HorizontalAlign="Right"></ItemStyle>
											</asp:BoundColumn>
										</Columns>
									</asp:datagrid><asp:datagrid id="grdUsers" runat="server" ForeColor="DimGray" CssClass="ReportGrid" EnableViewState="False"
										Width="100%" CellPadding="4" ShowHeader="True" AutoGenerateColumns="False">
										<HeaderStyle CssClass="reportGridHeader"></HeaderStyle>
										<Columns>
											<asp:BoundColumn DataField="FullName" HeaderText="Служител"></asp:BoundColumn>
											<asp:BoundColumn DataField="AmountOverTime" HeaderText="Общо заплащане (BGN)" ItemStyle-HorizontalAlign="Right"
												ItemStyle-Font-Bold="true" DataFormatString="{0:n2}" HeaderStyle-HorizontalAlign="Right"></asp:BoundColumn>
											<asp:BoundColumn DataField="Amount" DataFormatString="{0:n2}" HeaderText="Общо заплата (BGN)" ItemStyle-HorizontalAlign="Right"
												HeaderStyle-HorizontalAlign="Right"></asp:BoundColumn>
											<asp:BoundColumn DataField="WorkedMinutesNormal" HeaderText="Часове в раб. време" ItemStyle-HorizontalAlign="Right"
												HeaderStyle-HorizontalAlign="Right"></asp:BoundColumn>
											<asp:BoundColumn DataField="WorkedMinutesOverTime" HeaderText="Часове в извънраб. време" ItemStyle-HorizontalAlign="Right"
												HeaderStyle-HorizontalAlign="Right"></asp:BoundColumn>
											<asp:BoundColumn DataField="WorkedMinutesWeekend" HeaderText="Часове в почивни дни" ItemStyle-HorizontalAlign="Right"
												HeaderStyle-HorizontalAlign="Right"></asp:BoundColumn>
											<asp:BoundColumn DataField="WorkedMinutes" HeaderText="Общо часове" ItemStyle-HorizontalAlign="Right"
												HeaderStyle-HorizontalAlign="Right" ItemStyle-Font-Bold="true"></asp:BoundColumn>
											<asp:BoundColumn DataField="Amount" DataFormatString="{0:n2}" HeaderText="Общо неизработена заплата (BGN)"
												HeaderStyle-HorizontalAlign="Right" ItemStyle-HorizontalAlign="Right" Visible="False"></asp:BoundColumn>
											<asp:BoundColumn DataField="WorkedMinutes" HeaderText="Общо неизработени часове" ItemStyle-HorizontalAlign="Right"
												HeaderStyle-HorizontalAlign="Right" ItemStyle-Font-Bold="true" Visible="False"></asp:BoundColumn>
											<asp:BoundColumn DataField="TotalMinutes" HeaderText="Общо часове" ItemStyle-HorizontalAlign="Right"
												HeaderStyle-HorizontalAlign="Right" ItemStyle-Font-Bold="true" Visible="False"></asp:BoundColumn>
											<asp:BoundColumn DataField="TotalAmount" HeaderText="Общо заплата(лв.)" ItemStyle-HorizontalAlign="Right"
												HeaderStyle-HorizontalAlign="Right" ItemStyle-Font-Bold="true" Visible="False" DataFormatString="{0:n2}"></asp:BoundColumn>
										</Columns>
									</asp:datagrid><br>
									<br>
									<br>
									<asp:label id="lblCreatedByLbl" runat="server" Font-Size="9pt" Font-Bold="True" EnableViewState="False"
										Visible="False">ИЗГОТВИЛ СПРАВКАТА:</asp:label><asp:label id="lblCreatedBy" runat="server" Font-Size="9pt" EnableViewState="False" Visible="False"></asp:label><br>
									<br>
								</td>
							</tr>
						</table>
					</td>
				</tr>
			</table>
		</form>
	</body>
</HTML>
