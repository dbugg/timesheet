<%@ Control Language="c#" AutoEventWireup="false" Codebehind="PageHeader.ascx.cs" Inherits="Asa.Timesheet.WebPages.UserControls.PageHeader" TargetSchema="http://schemas.microsoft.com/intellisense/ie5"%>
<script language="javascript"> 
function document.onkeydown() { 
     if ( event.keyCode ==  13 ) { 
     if(Form1.btnEnter!=null)
         Form1.btnEnter.click(); 
        event.returnValue=false; 
    } 



} 
function popup()
{
				LeftPosition = (screen.width) ? (screen.width-750)/2 : 0;
				TopPosition = (screen.height) ? (screen.height-670)/2 : 0;		
				varFeature = "height="+670+",width="+750+",top="+100+",left="+LeftPosition+",resizable=no,scrollbars=yes";
				
				var wpopup = window.open("ISO.htm","ISO",varFeature);
				wpopup.focus();
}


</script>
<table cellpadding="0" cellspacing="0" border="0" width="100%">
	<tr>
		<td background="<%=Request.ApplicationPath%>/images/copy1.gif">
			<TABLE id="Table2" height="100%" width="100%" cellpadding="0" cellspacing="0" border="0">
				<tr height="5">
					<td colspan="2"></td>
				</tr>
				<TR>
					<TD style="WIDTH: 106px; HEIGHT: 67px">&nbsp;&nbsp;&nbsp;&nbsp; <IMG height="90" alt="" src="<%=Request.ApplicationPath%>/images/logo2.jpg" width="50" border=0></TD>
					<TD noWrap vAlign="top" align="right">
						<TABLE id="Table1" cellSpacing="0" cellPadding="0" width="100%" border="0">
							<TR>
								<TD align="right" style="PADDING-RIGHT: 10px; HEIGHT: 15px">
									<asp:Button id="btnLogOut" TabIndex="-1" runat="server" Text="Изход" Width="90" Height="20px"
										BorderStyle="Solid" BorderWidth="1px" BackColor="WhiteSmoke" BorderColor="Tan"></asp:Button>
									<asp:LinkButton id="lbLogOut" ForeColor="Black" Font-Bold="True" runat="server" Font-Size="10pt"
										Style="TEXT-DECORATION:none" Visible="False">Изход</asp:LinkButton>
								</TD>
							</TR>
							<TR>
								<TD align="left">
									<asp:label id="lblTitle" ForeColor="SaddleBrown" Font-Bold="True" Font-Size="Medium" runat="server"></asp:label>&nbsp;
									<asp:Button id="btnEdit" tabIndex="5" BorderColor="Tan" BackColor="WhiteSmoke" BorderWidth="1px"
										BorderStyle="Solid" Height="20px" Width="90" Text="Редакция" runat="server" Visible="False"></asp:Button>
								</TD>
							</TR>
							<TR>
								<TD>
									<asp:label id="lblUser" runat="server" Font-Size="X-Small" Font-Bold="True" ForeColor="Black"></asp:label></TD>
							</TR>
						</TABLE>
						<asp:HyperLink style="CURSOR:hand" Target="_blank" id="HyperLink1" runat="server" ForeColor="SaddleBrown"
							NavigateUrl="" onclick="popup()" Font-Underline="True">Документи по ISO</asp:HyperLink>&nbsp;
						<BR>
						<asp:HyperLink id="HyperLink2" style="CURSOR: hand" runat="server" ForeColor="SaddleBrown"
							Font-Underline="True" NavigateUrl="mailto:info@ermena.com" Target="_blank">За въпроси по системата</asp:HyperLink>
					</TD>
				</TR>
				<TR height="4">
					<TD colSpan="2"></TD>
				</TR>
			</TABLE>
		</td>
	</tr>
</table>
