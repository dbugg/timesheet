﻿<%@ Page language="c#" Codebehind="Users.aspx.cs" AutoEventWireup="false" Inherits="Asa.Timesheet.WebPages.Users" %>
<%@ Register TagPrefix="radg" Namespace="Telerik.WebControls" Assembly="RadGrid" %>
<%@ Register TagPrefix="uc1" TagName="PageHeader" Src="UserControls/PageHeader.ascx" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
	<head runat="server">
		<title>Users</title>
		<meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
		<meta content="C#" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<LINK href="styles/timesheet.css" type="text/css" rel="stylesheet">
	</HEAD>
	<body bottomMargin="0" leftMargin="0" topMargin="0" rightMargin="0">
		<form id="Form1" method="post" runat="server">
			<table  cellSpacing="0" cellPadding="0" width="100%" border="0">
				<tr height="100">
					<td>
						<uc1:PageHeader id="header" runat="server"></uc1:PageHeader>
					</td>
				</tr>
				<tr>
					<td>
						<table height="600px" cellSpacing="0" cellPadding="0" width="100%" border="0">
							<tr>
								<td class="menuLeft"><asp:placeholder id="menuHolder" runat="server"></asp:placeholder></td>
								
								<td>
                                	<TABLE >
										<TR>
											<td><asp:button id="btnNewUser" tabIndex="1" runat="server" CssClass="ActionButton"></asp:button></td>
                                            <td STYLE="width: 80px;">&nbsp&nbsp&nbsp</td>
                                            <td><asp:DropDownList ID="ddlRoles" Runat="server" Width="200px" AutoPostBack="True"></asp:DropDownList></td>
											<td><asp:button id="btnShowOldUsers" runat="server" CssClass="ActionButton" Text="Покажи изтрити служители"></asp:button></td>
                                            <td STYLE="width: 320px; text-align: right">ЕКСПОРТ:</td>
								            <td><asp:dropdownlist id="ddlExport" runat="server"></asp:dropdownlist></TD>
									        <td><asp:button id="btnExport" runat="server" CssClass="ActionButton" Text="Експорт служители"></asp:button></td>
									        <td><asp:button id="btnExportSalaries" runat="server" CssClass="ActionButton" Text="Експорт заплати" DESIGNTIMEDRAGDROP="42"></asp:button></TD>
							</TR>
						</table>

										<asp:label id="lblError" runat="server" ForeColor="Red" EnableViewState="False" CssClass="ErrorLabel"></asp:label><asp:label id="lblInfo" runat="server" EnableViewState="False" CssClass="InfoLabel"></asp:label><br>


									<asp:panel id="grid" 
										runat="server" Width="100%" height="95%">
										<TABLE>
											<TR>
												<td>
													<asp:Label id="lbSectionArchitects" CssClass="enterDataLabel" EnableViewState="False" 
														Runat="server" Font-Bold="True">АРХИТЕКТУРЕН ОТДЕЛ</asp:Label>
													<P></P>
													<asp:datagrid id="grdUsersArchitect" runat="server" CssClass="Grid" Width="100%" AllowSorting="True"
														AutoGenerateColumns="False" CellPadding="4" PageSize="2">
														<AlternatingItemStyle CssClass="GridAltItem"></AlternatingItemStyle>
														<ItemStyle CssClass="GridItem"></ItemStyle>
														<HeaderStyle CssClass="GridHeader"></HeaderStyle>
														<Columns>
															<asp:TemplateColumn Visible="False">
																<HeaderStyle Wrap="False" Width="20px"></HeaderStyle>
																<ItemStyle Wrap="False" HorizontalAlign="Center"></ItemStyle>
																<ItemTemplate>
																	<asp:ImageButton id="btnRestore" runat="server" Width="12px" Height="12px" ToolTip="Възстанови" ImageUrl="images/undelete.gif"></asp:ImageButton>
																</ItemTemplate>
															</asp:TemplateColumn>
															<asp:TemplateColumn HeaderText="#">
																<ItemStyle ForeColor="DimGray" Width="10px"></ItemStyle>
																<ItemTemplate>
																	<asp:Label CssClass="menuTable" runat="server" Text='<%# (int)DataBinder.Eval(Container, "ItemIndex")+1 %>' ID="Label2" NAME="Label1">
																	</asp:Label>
																</ItemTemplate>
															</asp:TemplateColumn>
															<asp:TemplateColumn SortExpression="FirstName" HeaderText="Име">
																<ItemTemplate>
																	<asp:LinkButton id="lnk" runat="server" CssClass="menuTable"  CommandName="Edit">
																		<span Class="menuTable">
																			<%# DataBinder.Eval(Container, "DataItem.FirstName") %>
																		</span>
																	</asp:LinkButton>
																</ItemTemplate>
															</asp:TemplateColumn>
															<asp:BoundColumn DataField="LastName" SortExpression="LastName" HeaderText="Фамилия">
																<HeaderStyle Width="100px"></HeaderStyle>
																<ItemStyle Wrap="False" ForeColor="DimGray"></ItemStyle>
															</asp:BoundColumn>
															<asp:BoundColumn DataField="Ext" HeaderText="Вътр. телефон">
																<HeaderStyle Width="120px"></HeaderStyle>
															</asp:BoundColumn>
															<asp:BoundColumn DataField="Mobile" HeaderText="Моб. телефон">
																<HeaderStyle Width="120px"></HeaderStyle>
															</asp:BoundColumn>
															<asp:BoundColumn DataField="MobilePersonal" HeaderText="Личен моб.">
																<HeaderStyle Width="120px"></HeaderStyle>
															</asp:BoundColumn>
															<asp:BoundColumn DataField="HomePhone" HeaderText="Дом. телефон">
																<HeaderStyle Width="120px"></HeaderStyle>
															</asp:BoundColumn>
															<asp:BoundColumn DataField="Mail" HeaderText="Ел.поща">
																<HeaderStyle Width="120px"></HeaderStyle>
																<ItemStyle Wrap="False"></ItemStyle>
															</asp:BoundColumn>
															<asp:BoundColumn DataField="PrivateEmail" HeaderText="Лична е-поща">
																<HeaderStyle Width="120px"></HeaderStyle>
																<ItemStyle Wrap="False"></ItemStyle>
															</asp:BoundColumn>
															<asp:BoundColumn DataField="RoleName" HeaderText="Роля в системата">
																<HeaderStyle Width="120px"></HeaderStyle>
																<ItemStyle ForeColor="DimGray"></ItemStyle>
															</asp:BoundColumn>
															<asp:TemplateColumn>
																<HeaderStyle Wrap="False" Width="20px"></HeaderStyle>
																<ItemStyle Wrap="False" HorizontalAlign="Center"></ItemStyle>
																<ItemTemplate>
																	<asp:ImageButton id="btnEdit" runat="server" ImageUrl="images/edit1.gif" ToolTip="Редактирай"></asp:ImageButton>
																</ItemTemplate>
															</asp:TemplateColumn>
															<asp:TemplateColumn>
																<HeaderStyle Wrap="False" Width="20px"></HeaderStyle>
																<ItemStyle Wrap="False" HorizontalAlign="Center"></ItemStyle>
																<ItemTemplate>
																	<asp:ImageButton id="btnDelete" runat="server" Width="12px" Height="12px" ToolTip="Изтрий" ImageUrl="images/delete.gif"></asp:ImageButton>
																</ItemTemplate>
															</asp:TemplateColumn>
														</Columns>
													</asp:datagrid></TD>
											</TR>
											<TR>
												<td>
													<asp:Label id="lbSectionIngener" CssClass="enterDataLabel" EnableViewState="False" 
														Runat="server" Font-Bold="True">ИНЖЕНЕРЕН ОТДЕЛ</asp:Label>
													<P></P>
													<asp:datagrid id="grdUsersIngener" runat="server" CssClass="Grid" Width="100%" AllowSorting="True"
														AutoGenerateColumns="False" CellPadding="4" PageSize="2">
														<AlternatingItemStyle CssClass="GridAltItem"></AlternatingItemStyle>
														<ItemStyle CssClass="GridItem"></ItemStyle>
														<HeaderStyle CssClass="GridHeader"></HeaderStyle>
														<Columns>
															<asp:TemplateColumn Visible="False">
																<HeaderStyle Wrap="False" Width="20px"></HeaderStyle>
																<ItemStyle Wrap="False" HorizontalAlign="Center"></ItemStyle>
																<ItemTemplate>
																	<asp:ImageButton id="btnRestore" runat="server" Width="12px" Height="12px" ToolTip="Възстанови" ImageUrl="images/undelete.gif"></asp:ImageButton>
																</ItemTemplate>
															</asp:TemplateColumn>
															<asp:TemplateColumn HeaderText="#">
																<ItemStyle ForeColor="DimGray" Width="10px"></ItemStyle>
																<ItemTemplate>
																	<asp:Label CssClass="menuTable" runat="server" Text='<%# (int)DataBinder.Eval(Container, "ItemIndex")+1 %>' ID="Label3" NAME="Label1">
																	</asp:Label>
																</ItemTemplate>
															</asp:TemplateColumn>
															<asp:TemplateColumn SortExpression="FirstName" HeaderText="Име">
																<ItemTemplate>
																	<asp:LinkButton id="lnk" runat="server" CssClass="menuTable"  CommandName="Edit">
																		<span Class="menuTable">
																			<%# DataBinder.Eval(Container, "DataItem.FirstName") %>
																		</span>
																	</asp:LinkButton>
																</ItemTemplate>
															</asp:TemplateColumn>
															<asp:BoundColumn DataField="LastName" SortExpression="LastName" HeaderText="Фамилия">
																<HeaderStyle Width="100px"></HeaderStyle>
																<ItemStyle Wrap="False"></ItemStyle>
															</asp:BoundColumn>
															<asp:BoundColumn DataField="Ext" HeaderText="Вътр. телефон">
																<HeaderStyle Width="120px"></HeaderStyle>
															</asp:BoundColumn>
															<asp:BoundColumn DataField="Mobile" HeaderText="Моб. телефон">
																<HeaderStyle Width="120px"></HeaderStyle>
															</asp:BoundColumn>
															<asp:BoundColumn DataField="MobilePersonal" HeaderText="Личен моб.">
																<HeaderStyle Width="120px"></HeaderStyle>
															</asp:BoundColumn>
															<asp:BoundColumn DataField="HomePhone" HeaderText="Дом. телефон">
																<HeaderStyle Width="120px"></HeaderStyle>
															</asp:BoundColumn>
															<asp:BoundColumn DataField="Mail" HeaderText="Ел.поща">
																<HeaderStyle Width="120px"></HeaderStyle>
																<ItemStyle Wrap="False"></ItemStyle>
															</asp:BoundColumn>
															<asp:BoundColumn DataField="PrivateEmail" HeaderText="Лична е-поща">
																<HeaderStyle Width="120px"></HeaderStyle>
																<ItemStyle Wrap="False"></ItemStyle>
															</asp:BoundColumn>
															<asp:BoundColumn DataField="RoleName" HeaderText="Роля в системата">
																<HeaderStyle Width="120px"></HeaderStyle>
																<ItemStyle ForeColor="DimGray"></ItemStyle>
															</asp:BoundColumn>
															<asp:TemplateColumn>
																<HeaderStyle Wrap="False" Width="20px"></HeaderStyle>
																<ItemStyle Wrap="False" HorizontalAlign="Center"></ItemStyle>
																<ItemTemplate>
																	<asp:ImageButton id="btnEdit" runat="server" ImageUrl="images/edit1.gif" ToolTip="Редактирай"></asp:ImageButton>
																</ItemTemplate>
															</asp:TemplateColumn>
															<asp:TemplateColumn>
																<HeaderStyle Wrap="False" Width="20px"></HeaderStyle>
																<ItemStyle Wrap="False" HorizontalAlign="Center"></ItemStyle>
																<ItemTemplate>
																	<asp:ImageButton id="btnDelete" runat="server" Width="12px" Height="12px" ToolTip="Изтрий" ImageUrl="images/delete.gif"></asp:ImageButton>
																</ItemTemplate>
															</asp:TemplateColumn>
														</Columns>
													</asp:datagrid></TD>
											</TR>
											<TR>
												<td>
													<asp:Label id="lbSectionIT" CssClass="enterDataLabel" EnableViewState="False" 
														Runat="server" Font-Bold="True">IT ОТДЕЛ</asp:Label>
													<P></P>
													<asp:datagrid id="grdUsersIT" runat="server" CssClass="Grid" Width="100%" AllowSorting="True"
														AutoGenerateColumns="False" CellPadding="4" PageSize="2">
														<AlternatingItemStyle CssClass="GridAltItem"></AlternatingItemStyle>
														<ItemStyle CssClass="GridItem"></ItemStyle>
														<HeaderStyle CssClass="GridHeader"></HeaderStyle>
														<Columns>
															<asp:TemplateColumn Visible="False">
																<HeaderStyle Wrap="False" Width="20px"></HeaderStyle>
																<ItemStyle Wrap="False" HorizontalAlign="Center"></ItemStyle>
																<ItemTemplate>
																	<asp:ImageButton id="btnRestore" runat="server" Width="12px" Height="12px" ToolTip="Възстанови" ImageUrl="images/undelete.gif"></asp:ImageButton>
																</ItemTemplate>
															</asp:TemplateColumn>
															<asp:TemplateColumn HeaderText="#">
																<ItemStyle ForeColor="DimGray" Width="10px"></ItemStyle>
																<ItemTemplate>
																	<asp:Label CssClass="menuTable" runat="server" Text='<%# (int)DataBinder.Eval(Container, "ItemIndex")+1 %>' ID="Label4" NAME="Label1">
																	</asp:Label>
																</ItemTemplate>
															</asp:TemplateColumn>
															<asp:TemplateColumn SortExpression="FirstName" HeaderText="Име">
																<ItemTemplate>
																	<asp:LinkButton id="lnk" runat="server" CssClass="menuTable"  CommandName="Edit">
																		<span Class="menuTable">
																			<%# DataBinder.Eval(Container, "DataItem.FirstName") %>
																		</span>
																	</asp:LinkButton>
																</ItemTemplate>
															</asp:TemplateColumn>
															<asp:BoundColumn DataField="LastName" SortExpression="LastName" HeaderText="Фамилия">
																<HeaderStyle Width="100px"></HeaderStyle>
																<ItemStyle Wrap="False"></ItemStyle>
															</asp:BoundColumn>
															<asp:BoundColumn DataField="Ext" HeaderText="Вътр. телефон">
																<HeaderStyle Width="120px"></HeaderStyle>
															</asp:BoundColumn>
															<asp:BoundColumn DataField="Mobile" HeaderText="Моб. телефон">
																<HeaderStyle Width="120px"></HeaderStyle>
															</asp:BoundColumn>
															<asp:BoundColumn DataField="MobilePersonal" HeaderText="Личен моб.">
																<HeaderStyle Width="120px"></HeaderStyle>
															</asp:BoundColumn>
															<asp:BoundColumn DataField="HomePhone" HeaderText="Дом. телефон">
																<HeaderStyle Width="120px"></HeaderStyle>
															</asp:BoundColumn>
															<asp:BoundColumn DataField="Mail" HeaderText="Ел.поща">
																<HeaderStyle Width="120px"></HeaderStyle>
																<ItemStyle Wrap="False"></ItemStyle>
															</asp:BoundColumn>
															<asp:BoundColumn DataField="PrivateEmail" HeaderText="Лична е-поща">
																<HeaderStyle Width="120px"></HeaderStyle>
																<ItemStyle Wrap="False"></ItemStyle>
															</asp:BoundColumn>
															<asp:BoundColumn DataField="RoleName" HeaderText="Роля в системата">
																<HeaderStyle Width="120px"></HeaderStyle>
																<ItemStyle ForeColor="DimGray"></ItemStyle>
															</asp:BoundColumn>
															<asp:TemplateColumn>
																<HeaderStyle Wrap="False" Width="20px"></HeaderStyle>
																<ItemStyle Wrap="False" HorizontalAlign="Center"></ItemStyle>
																<ItemTemplate>
																	<asp:ImageButton id="btnEdit" runat="server" ImageUrl="images/edit1.gif" ToolTip="Редактирай"></asp:ImageButton>
																</ItemTemplate>
															</asp:TemplateColumn>
															<asp:TemplateColumn>
																<HeaderStyle Wrap="False" Width="20px"></HeaderStyle>
																<ItemStyle Wrap="False" HorizontalAlign="Center"></ItemStyle>
																<ItemTemplate>
																	<asp:ImageButton id="btnDelete" runat="server" Width="12px" Height="12px" ToolTip="Изтрий" ImageUrl="images/delete.gif"></asp:ImageButton>
																</ItemTemplate>
															</asp:TemplateColumn>
														</Columns>
													</asp:datagrid></TD>
											</TR>
											<TR>
												<td>
													<asp:Label id="lbSectionACC" CssClass="enterDataLabel" EnableViewState="False"
														Runat="server" Font-Bold="True">СЧЕТОВОДСТВО</asp:Label>
													<P></P>
													<asp:datagrid id="grdUsersACC" runat="server" CssClass="Grid" Width="100%" AllowSorting="True"
														AutoGenerateColumns="False" CellPadding="4" PageSize="2">
														<AlternatingItemStyle CssClass="GridAltItem"></AlternatingItemStyle>
														<ItemStyle CssClass="GridItem"></ItemStyle>
														<HeaderStyle CssClass="GridHeader"></HeaderStyle>
														<Columns>
															<asp:TemplateColumn Visible="False">
																<HeaderStyle Wrap="False" Width="20px"></HeaderStyle>
																<ItemStyle Wrap="False" HorizontalAlign="Center"></ItemStyle>
																<ItemTemplate>
																	<asp:ImageButton id="btnRestore" runat="server" Width="12px" Height="12px" ToolTip="Възстанови" ImageUrl="images/undelete.gif"></asp:ImageButton>
																</ItemTemplate>
															</asp:TemplateColumn>
															<asp:TemplateColumn HeaderText="#">
																<ItemStyle ForeColor="DimGray" Width="10px"></ItemStyle>
																<ItemTemplate>
																	<asp:Label CssClass="menuTable" runat="server" Text='<%# (int)DataBinder.Eval(Container, "ItemIndex")+1 %>' ID="Label5" NAME="Label1">
																	</asp:Label>
																</ItemTemplate>
															</asp:TemplateColumn>
															<asp:TemplateColumn SortExpression="FirstName" HeaderText="Име">
																<ItemTemplate>
																	<asp:LinkButton id="lnk" runat="server" CssClass="menuTable"  CommandName="Edit">
																		<span Class="menuTable">
																			<%# DataBinder.Eval(Container, "DataItem.FirstName") %>
																		</span>
																	</asp:LinkButton>
																</ItemTemplate>
															</asp:TemplateColumn>
															<asp:BoundColumn DataField="LastName" SortExpression="LastName" HeaderText="Фамилия">
																<HeaderStyle Width="100px"></HeaderStyle>
																<ItemStyle Wrap="False"></ItemStyle>
															</asp:BoundColumn>
															<asp:BoundColumn DataField="Ext" HeaderText="Вътр. телефон">
																<HeaderStyle Width="120px"></HeaderStyle>
															</asp:BoundColumn>
															<asp:BoundColumn DataField="Mobile" HeaderText="Моб. телефон">
																<HeaderStyle Width="120px"></HeaderStyle>
															</asp:BoundColumn>
															<asp:BoundColumn DataField="MobilePersonal" HeaderText="Личен моб.">
																<HeaderStyle Width="120px"></HeaderStyle>
															</asp:BoundColumn>
															<asp:BoundColumn DataField="HomePhone" HeaderText="Дом. телефон">
																<HeaderStyle Width="120px"></HeaderStyle>
															</asp:BoundColumn>
															<asp:BoundColumn DataField="Mail" HeaderText="Ел.поща">
																<HeaderStyle Width="120px"></HeaderStyle>
																<ItemStyle Wrap="False"></ItemStyle>
															</asp:BoundColumn>
															<asp:BoundColumn DataField="PrivateEmail" HeaderText="Лична е-поща">
																<HeaderStyle Width="120px"></HeaderStyle>
																<ItemStyle Wrap="False"></ItemStyle>
															</asp:BoundColumn>
															<asp:BoundColumn DataField="RoleName" HeaderText="Роля в системата">
																<HeaderStyle Width="120px"></HeaderStyle>
																<ItemStyle ForeColor="DimGray"></ItemStyle>
															</asp:BoundColumn>
															<asp:TemplateColumn>
																<HeaderStyle Wrap="False" Width="20px"></HeaderStyle>
																<ItemStyle Wrap="False" HorizontalAlign="Center"></ItemStyle>
																<ItemTemplate>
																	<asp:ImageButton id="btnEdit" runat="server" ImageUrl="images/edit1.gif" ToolTip="Редактирай"></asp:ImageButton>
																</ItemTemplate>
															</asp:TemplateColumn>
															<asp:TemplateColumn>
																<HeaderStyle Wrap="False" Width="20px"></HeaderStyle>
																<ItemStyle Wrap="False" HorizontalAlign="Center"></ItemStyle>
																<ItemTemplate>
																	<asp:ImageButton id="btnDelete" runat="server" Width="12px" Height="12px" ToolTip="Изтрий" ImageUrl="images/delete.gif"></asp:ImageButton>
																</ItemTemplate>
															</asp:TemplateColumn>
														</Columns>
													</asp:datagrid></TD>
											</TR>
											<TR>
												<td>
													<asp:Label id="lbSectionSecretary" CssClass="enterDataLabel" EnableViewState="False" 
														Runat="server" Font-Bold="True">АДМИНИСТРАЦИЯ</asp:Label>
													<P></P>
													<asp:datagrid id="grdUsersSecretary" runat="server" CssClass="Grid" Width="100%" AllowSorting="True"
														AutoGenerateColumns="False" CellPadding="4" PageSize="2">
														<AlternatingItemStyle CssClass="GridAltItem"></AlternatingItemStyle>
														<ItemStyle CssClass="GridItem"></ItemStyle>
														<HeaderStyle CssClass="GridHeader"></HeaderStyle>
														<Columns>
															<asp:TemplateColumn Visible="False">
																<HeaderStyle Wrap="False" Width="20px"></HeaderStyle>
																<ItemStyle Wrap="False" HorizontalAlign="Center"></ItemStyle>
																<ItemTemplate>
																	<asp:ImageButton id="btnRestore" runat="server" Width="12px" Height="12px" ToolTip="Възстанови" ImageUrl="images/undelete.gif"></asp:ImageButton>
																</ItemTemplate>
															</asp:TemplateColumn>
															<asp:TemplateColumn HeaderText="#">
																<ItemStyle ForeColor="DimGray" Width="10px"></ItemStyle>
																<ItemTemplate>
																	<asp:Label CssClass="menuTable" runat="server" Text='<%# (int)DataBinder.Eval(Container, "ItemIndex")+1 %>' ID="Label6" NAME="Label1">
																	</asp:Label>
																</ItemTemplate>
															</asp:TemplateColumn>
															<asp:TemplateColumn SortExpression="FirstName" HeaderText="Име">
																<ItemTemplate>
																	<asp:LinkButton id="lnk" runat="server" CssClass="menuTable"  CommandName="Edit">
																		<span Class="menuTable">
																			<%# DataBinder.Eval(Container, "DataItem.FirstName") %>
																		</span>
																	</asp:LinkButton>
																</ItemTemplate>
															</asp:TemplateColumn>
															<asp:BoundColumn DataField="LastName" SortExpression="LastName" HeaderText="Фамилия">
																<HeaderStyle Width="100px"></HeaderStyle>
																<ItemStyle Wrap="False"></ItemStyle>
															</asp:BoundColumn>
															<asp:BoundColumn DataField="Ext" HeaderText="Вътр. телефон">
																<HeaderStyle Width="120px"></HeaderStyle>
															</asp:BoundColumn>
															<asp:BoundColumn DataField="Mobile" HeaderText="Моб. телефон">
																<HeaderStyle Width="120px"></HeaderStyle>
															</asp:BoundColumn>
															<asp:BoundColumn DataField="MobilePersonal" HeaderText="Личен моб.">
																<HeaderStyle Width="120px"></HeaderStyle>
															</asp:BoundColumn>
															<asp:BoundColumn DataField="HomePhone" HeaderText="Дом. телефон">
																<HeaderStyle Width="120px"></HeaderStyle>
															</asp:BoundColumn>
															<asp:BoundColumn DataField="Mail" HeaderText="Ел.поща">
																<HeaderStyle Width="120px"></HeaderStyle>
																<ItemStyle Wrap="False"></ItemStyle>
															</asp:BoundColumn>
															<asp:BoundColumn DataField="PrivateEmail" HeaderText="Лична е-поща">
																<HeaderStyle Width="120px"></HeaderStyle>
																<ItemStyle Wrap="False"></ItemStyle>
															</asp:BoundColumn>
															<asp:BoundColumn DataField="RoleName" HeaderText="Роля в системата">
																<HeaderStyle Width="120px"></HeaderStyle>
																<ItemStyle ForeColor="DimGray"></ItemStyle>
															</asp:BoundColumn>
															<asp:TemplateColumn>
																<HeaderStyle Wrap="False" Width="20px"></HeaderStyle>
																<ItemStyle Wrap="False" HorizontalAlign="Center"></ItemStyle>
																<ItemTemplate>
																	<asp:ImageButton id="btnEdit" runat="server" ImageUrl="images/edit1.gif" ToolTip="Редактирай"></asp:ImageButton>
																</ItemTemplate>
															</asp:TemplateColumn>
															<asp:TemplateColumn>
																<HeaderStyle Wrap="False" Width="20px"></HeaderStyle>
																<ItemStyle Wrap="False" HorizontalAlign="Center"></ItemStyle>
																<ItemTemplate>
																	<asp:ImageButton id="btnDelete" runat="server" Width="12px" Height="12px" ToolTip="Изтрий" ImageUrl="images/delete.gif"></asp:ImageButton>
																</ItemTemplate>
															</asp:TemplateColumn>
														</Columns>
													</asp:datagrid></TD>
											</TR>
											<TR>
												<td>
													<asp:Label id="lbSectionCleaner" CssClass="enterDataLabel" EnableViewState="False" 
														Runat="server" Font-Bold="True">ХИГИЕНИСКА</asp:Label>
													<P></P>
													<asp:datagrid id="grdUsersCleaner" runat="server" CssClass="Grid" Width="100%" AllowSorting="True"
														AutoGenerateColumns="False" CellPadding="4" PageSize="2">
														<AlternatingItemStyle CssClass="GridAltItem"></AlternatingItemStyle>
														<ItemStyle CssClass="GridItem"></ItemStyle>
														<HeaderStyle CssClass="GridHeader"></HeaderStyle>
														<Columns>
															<asp:TemplateColumn Visible="False">
																<HeaderStyle Wrap="False" Width="20px"></HeaderStyle>
																<ItemStyle Wrap="False" HorizontalAlign="Center"></ItemStyle>
																<ItemTemplate>
																	<asp:ImageButton id="btnRestore" runat="server" Width="12px" Height="12px" ToolTip="Възстанови" ImageUrl="images/undelete.gif"></asp:ImageButton>
																</ItemTemplate>
															</asp:TemplateColumn>
															<asp:TemplateColumn HeaderText="#">
																<ItemStyle ForeColor="DimGray" Width="10px"></ItemStyle>
																<ItemTemplate>
																	<asp:Label CssClass="menuTable" runat="server" Text='<%# (int)DataBinder.Eval(Container, "ItemIndex")+1 %>' ID="Label7" NAME="Label1">
																	</asp:Label>
																</ItemTemplate>
															</asp:TemplateColumn>
															<asp:TemplateColumn SortExpression="FirstName" HeaderText="Име">
																<ItemTemplate>
																	<asp:LinkButton id="lnk" runat="server" CssClass="menuTable"  CommandName="Edit">
																		<span Class="menuTable">
																			<%# DataBinder.Eval(Container, "DataItem.FirstName") %>
																		</span>
																	</asp:LinkButton>
																</ItemTemplate>
															</asp:TemplateColumn>
															<asp:BoundColumn DataField="LastName" SortExpression="LastName" HeaderText="Фамилия">
																<HeaderStyle Width="100px"></HeaderStyle>
																<ItemStyle Wrap="False"></ItemStyle>
															</asp:BoundColumn>
															<asp:BoundColumn DataField="Ext" HeaderText="Вътр. телефон">
																<HeaderStyle Width="120px"></HeaderStyle>
															</asp:BoundColumn>
															<asp:BoundColumn DataField="Mobile" HeaderText="Моб. телефон">
																<HeaderStyle Width="120px"></HeaderStyle>
															</asp:BoundColumn>
															<asp:BoundColumn DataField="MobilePersonal" HeaderText="Личен моб.">
																<HeaderStyle Width="120px"></HeaderStyle>
															</asp:BoundColumn>
															<asp:BoundColumn DataField="HomePhone" HeaderText="Дом. телефон">
																<HeaderStyle Width="120px"></HeaderStyle>
															</asp:BoundColumn>
															<asp:BoundColumn DataField="Mail" HeaderText="Ел.поща">
																<HeaderStyle Width="120px"></HeaderStyle>
																<ItemStyle Wrap="False"></ItemStyle>
															</asp:BoundColumn>
															<asp:BoundColumn DataField="PrivateEmail" HeaderText="Лична е-поща">
																<HeaderStyle Width="120px"></HeaderStyle>
																<ItemStyle Wrap="False"></ItemStyle>
															</asp:BoundColumn>
															<asp:BoundColumn DataField="RoleName" HeaderText="Роля в системата">
																<HeaderStyle Width="120px"></HeaderStyle>
																<ItemStyle ForeColor="DimGray"></ItemStyle>
															</asp:BoundColumn>
															<asp:TemplateColumn>
																<HeaderStyle Wrap="False" Width="20px"></HeaderStyle>
																<ItemStyle Wrap="False" HorizontalAlign="Center"></ItemStyle>
																<ItemTemplate>
																	<asp:ImageButton id="btnEdit" runat="server" ImageUrl="images/edit1.gif" ToolTip="Редактирай"></asp:ImageButton>
																</ItemTemplate>
															</asp:TemplateColumn>
															<asp:TemplateColumn>
																<HeaderStyle Wrap="False" Width="20px"></HeaderStyle>
																<ItemStyle Wrap="False" HorizontalAlign="Center"></ItemStyle>
																<ItemTemplate>
																	<asp:ImageButton id="btnDelete" runat="server" Width="12px" Height="12px" ToolTip="Изтрий" ImageUrl="images/delete.gif"></asp:ImageButton>
																</ItemTemplate>
															</asp:TemplateColumn>
														</Columns>
													</asp:datagrid></TD>
											</TR>
											<TR>
												<td>
													<asp:Label id="lbSectionLowers" CssClass="enterDataLabel" EnableViewState="False" 
														Runat="server" Font-Bold="True">АСОЦИИРАН ЮРИСТ</asp:Label>
													<P></P>
													<asp:datagrid id="grdUsersLowers" runat="server" CssClass="Grid" Width="100%" AllowSorting="True"
														AutoGenerateColumns="False" CellPadding="4" PageSize="2">
														<AlternatingItemStyle CssClass="GridAltItem"></AlternatingItemStyle>
														<ItemStyle CssClass="GridItem"></ItemStyle>
														<HeaderStyle CssClass="GridHeader"></HeaderStyle>
														<Columns>
															<asp:TemplateColumn Visible="False">
																<HeaderStyle Wrap="False" Width="20px"></HeaderStyle>
																<ItemStyle Wrap="False" HorizontalAlign="Center"></ItemStyle>
																<ItemTemplate>
																	<asp:ImageButton id="btnRestore" runat="server" Width="12px" Height="12px" ToolTip="Възстанови" ImageUrl="images/undelete.gif"></asp:ImageButton>
																</ItemTemplate>
															</asp:TemplateColumn>
															<asp:TemplateColumn HeaderText="#">
																<ItemStyle ForeColor="DimGray" Width="10px"></ItemStyle>
																<ItemTemplate>
																	<asp:Label CssClass="menuTable" runat="server" Text='<%# (int)DataBinder.Eval(Container, "ItemIndex")+1 %>' ID="Label8" NAME="Label1">
																	</asp:Label>
																</ItemTemplate>
															</asp:TemplateColumn>
															<asp:TemplateColumn SortExpression="FirstName" HeaderText="Име">
																<ItemTemplate>
																	<asp:LinkButton id="lnk" runat="server" CssClass="menuTable"  CommandName="Edit">
																		<span Class="menuTable">
																			<%# DataBinder.Eval(Container, "DataItem.FirstName") %>
																		</span>
																	</asp:LinkButton>
																</ItemTemplate>
															</asp:TemplateColumn>
															<asp:BoundColumn DataField="LastName" SortExpression="LastName" HeaderText="Фамилия">
																<HeaderStyle Width="100px"></HeaderStyle>
																<ItemStyle Wrap="False"></ItemStyle>
															</asp:BoundColumn>
															<asp:BoundColumn DataField="Ext" HeaderText="Вътр. телефон">
																<HeaderStyle Width="120px"></HeaderStyle>
															</asp:BoundColumn>
															<asp:BoundColumn DataField="Mobile" HeaderText="Моб. телефон">
																<HeaderStyle Width="120px"></HeaderStyle>
															</asp:BoundColumn>
															<asp:BoundColumn DataField="MobilePersonal" HeaderText="Личен моб.">
																<HeaderStyle Width="120px"></HeaderStyle>
															</asp:BoundColumn>
															<asp:BoundColumn DataField="HomePhone" HeaderText="Дом. телефон">
																<HeaderStyle Width="120px"></HeaderStyle>
															</asp:BoundColumn>
															<asp:BoundColumn DataField="Mail" HeaderText="Ел.поща">
																<HeaderStyle Width="120px"></HeaderStyle>
																<ItemStyle Wrap="False"></ItemStyle>
															</asp:BoundColumn>
															<asp:BoundColumn DataField="PrivateEmail" HeaderText="Лична е-поща">
																<HeaderStyle Width="120px"></HeaderStyle>
																<ItemStyle Wrap="False"></ItemStyle>
															</asp:BoundColumn>
															<asp:BoundColumn DataField="RoleName" HeaderText="Роля в системата">
																<HeaderStyle Width="120px"></HeaderStyle>
																<ItemStyle ForeColor="DimGray"></ItemStyle>
															</asp:BoundColumn>
															<asp:TemplateColumn>
																<HeaderStyle Wrap="False" Width="20px"></HeaderStyle>
																<ItemStyle Wrap="False" HorizontalAlign="Center"></ItemStyle>
																<ItemTemplate>
																	<asp:ImageButton id="btnEdit" runat="server" ImageUrl="images/edit1.gif" ToolTip="Редактирай"></asp:ImageButton>
																</ItemTemplate>
															</asp:TemplateColumn>
															<asp:TemplateColumn>
																<HeaderStyle Wrap="False" Width="20px"></HeaderStyle>
																<ItemStyle Wrap="False" HorizontalAlign="Center"></ItemStyle>
																<ItemTemplate>
																	<asp:ImageButton id="btnDelete" runat="server" Width="12px" Height="12px" ToolTip="Изтрий" ImageUrl="images/delete.gif"></asp:ImageButton>
																</ItemTemplate>
															</asp:TemplateColumn>
														</Columns>
													</asp:datagrid></TD>
											</TR>
											<TR>
												<td>
													<asp:Label id="lbSectionDrivers" CssClass="enterDataLabel" EnableViewState="False" 
														Runat="server" Font-Bold="True">ШОФЬОРИ</asp:Label>
													<P></P>
													<asp:datagrid id="grdUsersDrivers" runat="server" CssClass="Grid" Width="100%" AllowSorting="True"
														AutoGenerateColumns="False" CellPadding="4" PageSize="2">
														<AlternatingItemStyle CssClass="GridAltItem"></AlternatingItemStyle>
														<ItemStyle CssClass="GridItem"></ItemStyle>
														<HeaderStyle CssClass="GridHeader"></HeaderStyle>
														<Columns>
															<asp:TemplateColumn Visible="False">
																<HeaderStyle Wrap="False" Width="20px"></HeaderStyle>
																<ItemStyle Wrap="False" HorizontalAlign="Center"></ItemStyle>
																<ItemTemplate>
																	<asp:ImageButton id="btnRestore" runat="server" Width="12px" Height="12px" ToolTip="Възстанови" ImageUrl="images/undelete.gif"></asp:ImageButton>
																</ItemTemplate>
															</asp:TemplateColumn>
															<asp:TemplateColumn HeaderText="#">
																<ItemStyle ForeColor="DimGray" Width="10px"></ItemStyle>
																<ItemTemplate>
																	<asp:Label CssClass="menuTable" runat="server" Text='<%# (int)DataBinder.Eval(Container, "ItemIndex")+1 %>' ID="Label10" NAME="Label1">
																	</asp:Label>
																</ItemTemplate>
															</asp:TemplateColumn>
															<asp:TemplateColumn SortExpression="FirstName" HeaderText="Име">
																<ItemTemplate>
																	<asp:LinkButton id="Linkbutton1" runat="server" CssClass="menuTable"  CommandName="Edit">
																		<span Class="menuTable">
																			<%# DataBinder.Eval(Container, "DataItem.FirstName") %>
																		</span>
																	</asp:LinkButton>
																</ItemTemplate>
															</asp:TemplateColumn>
															<asp:BoundColumn DataField="LastName" SortExpression="LastName" HeaderText="Фамилия">
																<HeaderStyle Width="100px"></HeaderStyle>
																<ItemStyle Wrap="False"></ItemStyle>
															</asp:BoundColumn>
															<asp:BoundColumn DataField="Ext" HeaderText="Вътр. телефон">
																<HeaderStyle Width="120px"></HeaderStyle>
															</asp:BoundColumn>
															<asp:BoundColumn DataField="Mobile" HeaderText="Моб. телефон">
																<HeaderStyle Width="120px"></HeaderStyle>
															</asp:BoundColumn>
															<asp:BoundColumn DataField="MobilePersonal" HeaderText="Личен моб.">
																<HeaderStyle Width="120px"></HeaderStyle>
															</asp:BoundColumn>
															<asp:BoundColumn DataField="HomePhone" HeaderText="Дом. телефон">
																<HeaderStyle Width="120px"></HeaderStyle>
															</asp:BoundColumn>
															<asp:BoundColumn DataField="Mail" HeaderText="Ел.поща">
																<HeaderStyle Width="120px"></HeaderStyle>
																<ItemStyle Wrap="False"></ItemStyle>
															</asp:BoundColumn>
															<asp:BoundColumn DataField="PrivateEmail" HeaderText="Лична е-поща">
																<HeaderStyle Width="120px"></HeaderStyle>
																<ItemStyle Wrap="False"></ItemStyle>
															</asp:BoundColumn>
															<asp:BoundColumn DataField="RoleName" HeaderText="Роля в системата">
																<HeaderStyle Width="120px"></HeaderStyle>
																<ItemStyle ForeColor="DimGray"></ItemStyle>
															</asp:BoundColumn>
															<asp:TemplateColumn>
																<HeaderStyle Wrap="False" Width="20px"></HeaderStyle>
																<ItemStyle Wrap="False" HorizontalAlign="Center"></ItemStyle>
																<ItemTemplate>
																	<asp:ImageButton id="btnEdit" runat="server" ImageUrl="images/edit1.gif" ToolTip="Редактирай"></asp:ImageButton>
																</ItemTemplate>
															</asp:TemplateColumn>
															<asp:TemplateColumn>
																<HeaderStyle Wrap="False" Width="20px"></HeaderStyle>
																<ItemStyle Wrap="False" HorizontalAlign="Center"></ItemStyle>
																<ItemTemplate>
																	<asp:ImageButton id="btnDelete" runat="server" Width="12px" Height="12px" ToolTip="Изтрий" ImageUrl="images/delete.gif"></asp:ImageButton>
																</ItemTemplate>
															</asp:TemplateColumn>
														</Columns>
													</asp:datagrid></TD>
											</TR>
											<TR>
												<td>
													<asp:Label id="lbExEmploy" CssClass="enterDataLabel" EnableViewState="False" 
														Runat="server" Font-Bold="True">ИЗТРИТИ СЛУЖИТЕЛИ</asp:Label>
													<P></P>
													<asp:datagrid id="grdExEmploy" runat="server" CssClass="Grid" Width="100%" AllowSorting="True"
														AutoGenerateColumns="False" CellPadding="4" PageSize="2">
														<AlternatingItemStyle CssClass="GridAltItem"></AlternatingItemStyle>
														<ItemStyle CssClass="GridItem"></ItemStyle>
														<HeaderStyle CssClass="GridHeader"></HeaderStyle>
														<Columns>
															<asp:TemplateColumn HeaderText="#">
																<ItemStyle ForeColor="DimGray" Width="10px"></ItemStyle>
																<ItemTemplate>
																	<asp:Label CssClass="menuTable" runat="server" Text='<%# (int)DataBinder.Eval(Container, "ItemIndex")+1 %>' ID="Label9" NAME="Label1">
																	</asp:Label>
																</ItemTemplate>
															</asp:TemplateColumn>
															<asp:TemplateColumn SortExpression="FirstName" HeaderText="Име">
																<ItemTemplate>
																	<asp:LinkButton id="Linkbutton2" runat="server" CssClass="menuTable"  CommandName="Edit">
																		<span Class="menuTable">
																			<%# DataBinder.Eval(Container, "DataItem.FirstName") %>
																		</span>
																	</asp:LinkButton>
																</ItemTemplate>
															</asp:TemplateColumn>
															<asp:BoundColumn DataField="LastName" SortExpression="LastName" HeaderText="Фамилия">
																<HeaderStyle Width="100px"></HeaderStyle>
																<ItemStyle Wrap="False"></ItemStyle>
															</asp:BoundColumn>
															<asp:BoundColumn DataField="Ext" HeaderText="Вътр. телефон">
																<HeaderStyle Width="120px"></HeaderStyle>
															</asp:BoundColumn>
															<asp:BoundColumn DataField="Mobile" HeaderText="Моб. телефон">
																<HeaderStyle Width="120px"></HeaderStyle>
															</asp:BoundColumn>
															<asp:BoundColumn DataField="MobilePersonal" HeaderText="Личен моб.">
																<HeaderStyle Width="120px"></HeaderStyle>
															</asp:BoundColumn>
															<asp:BoundColumn DataField="HomePhone" HeaderText="Дом. телефон">
																<HeaderStyle Width="120px"></HeaderStyle>
															</asp:BoundColumn>
															<asp:BoundColumn DataField="Mail" HeaderText="Ел.поща">
																<HeaderStyle Width="120px"></HeaderStyle>
																<ItemStyle Wrap="False"></ItemStyle>
															</asp:BoundColumn>
															<asp:BoundColumn DataField="PrivateEmail" HeaderText="Лична е-поща">
																<HeaderStyle Width="120px"></HeaderStyle>
																<ItemStyle Wrap="False"></ItemStyle>
															</asp:BoundColumn>
															<asp:BoundColumn DataField="RoleName" HeaderText="Роля в системата">
																<HeaderStyle Width="120px"></HeaderStyle>
																<ItemStyle ForeColor="DimGray"></ItemStyle>
															</asp:BoundColumn>
															<asp:TemplateColumn Visible="True">
																<HeaderStyle Wrap="False" Width="20px"></HeaderStyle>
																<ItemStyle Wrap="False" HorizontalAlign="Center"></ItemStyle>
																<ItemTemplate>
																	<asp:ImageButton id="btnEdit" runat="server" ImageUrl="images/edit1.gif" ToolTip="Редактирай"></asp:ImageButton>
																</ItemTemplate>
															</asp:TemplateColumn>
															<asp:TemplateColumn>
																<HeaderStyle Wrap="False" Width="20px"></HeaderStyle>
																<ItemStyle Wrap="False" HorizontalAlign="Center"></ItemStyle>
																<ItemTemplate>
																	<asp:ImageButton id="btnRestore" runat="server" Width="12px" Height="12px" ToolTip="Възстанови" ImageUrl="images/undelete.gif"></asp:ImageButton>
																</ItemTemplate>
															</asp:TemplateColumn>
														</Columns>
													</asp:datagrid></TD>
											</TR>
										</TABLE>
									</asp:panel>

					</td>
				</tr>
			</table>
			</TD></TR></TABLE></form>
	</body>
</HTML>
