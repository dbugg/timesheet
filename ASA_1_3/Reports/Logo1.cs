using System;
using GrapeCity.ActiveReports;
using GrapeCity.ActiveReports.Controls;
using GrapeCity.ActiveReports.SectionReportModel;
using GrapeCity.ActiveReports.Document.Section;
using GrapeCity.ActiveReports.Document;

namespace Asa.Timesheet.WebPages.Reports
{
    public class Logo : GrapeCity.ActiveReports.SectionReport
	{
        public Logo()
        {
            InitializeComponent();
        }

		#region ActiveReports Designer generated code






        public void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Logo));
            this.Detail = new GrapeCity.ActiveReports.SectionReportModel.Detail();
            this.ReportHeader = new GrapeCity.ActiveReports.SectionReportModel.ReportHeader();
            this.ReportFooter = new GrapeCity.ActiveReports.SectionReportModel.ReportFooter();
            this.Picture1 = new GrapeCity.ActiveReports.SectionReportModel.Picture();
            this.TextBox3 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtTest = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            ((System.ComponentModel.ISupportInitialize)(this.Picture1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TextBox3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTest)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            // 
            // Detail
            // 
            this.Detail.ColumnSpacing = 0F;
            this.Detail.Height = 0F;
            this.Detail.Name = "Detail";
            // 
            // ReportHeader
            // 
            this.ReportHeader.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
						this.Picture1,
						this.TextBox3,
						this.txtTest});
            this.ReportHeader.Height = 9.34375F;
            this.ReportHeader.Name = "ReportHeader";
            // 
            // ReportFooter
            // 
            this.ReportFooter.Height = 0F;
            this.ReportFooter.Name = "ReportFooter";
            // 
            // Picture1
            // 
            this.Picture1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.Picture1.Height = 1.181102F;
            this.Picture1.ImageData = ((System.IO.Stream)(resources.GetObject("Picture1.ImageData")));
            this.Picture1.Left = 0F;
            this.Picture1.LineColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.Picture1.Name = "Picture1";
            this.Picture1.PictureAlignment = GrapeCity.ActiveReports.SectionReportModel.PictureAlignment.TopRight;
            this.Picture1.SizeMode = GrapeCity.ActiveReports.SectionReportModel.SizeModes.Zoom;
            this.Picture1.Top = 0F;
            this.Picture1.Width = 0.9842521F;
            // 
            // TextBox3
            // 
            this.TextBox3.Height = 0.3187352F;
            this.TextBox3.Left = 0F;
            this.TextBox3.Name = "TextBox3";
            this.TextBox3.Style = "color: Black; font-family: Arial; font-size: 8pt; font-weight: bold; text-align: " +
                "right; white-space: inherit";
            this.TextBox3.Text = "Atelier Serafimov Architects";
            this.TextBox3.Top = 7.674048F;
            this.TextBox3.Width = 1.023622F;
            // 
            // txtTest
            // 
            this.txtTest.Height = 1.277067F;
            this.txtTest.Left = 0F;
            this.txtTest.Name = "txtTest";
            this.txtTest.Style = "color: Black; font-size: 8pt; text-align: right";
            this.txtTest.Text = resources.GetString("txtTest.Text");
            this.txtTest.Top = 8.019605F;
            this.txtTest.Width = 1.023622F;
            // 
            // ActiveReport1
            // 
            this.MasterReport = false;
            this.PageSettings.DefaultPaperSize = false;
            this.PageSettings.Margins.Bottom = 0.7875F;
            this.PageSettings.Margins.Left = 0.1972222F;
            this.PageSettings.Margins.Right = 0F;
            this.PageSettings.Margins.Top = 0.5909722F;
            this.PageSettings.Orientation = GrapeCity.ActiveReports.Document.Section.PageOrientation.Portrait;
            this.PageSettings.PaperHeight = 11.69306F;
            this.PageSettings.PaperKind = System.Drawing.Printing.PaperKind.A4;
            this.PageSettings.PaperWidth = 8.268056F;
            this.PrintWidth = 1.023622F;
            this.Sections.Add(this.ReportHeader);
            this.Sections.Add(this.Detail);
            this.Sections.Add(this.ReportFooter);
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule(resources.GetString("$this.StyleSheet"), "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: inherit; font-style: inherit; font-variant: inherit; font-weight: bo" +
                        "ld; font-size: 16pt; font-size-adjust: inherit; font-stretch: inherit", "Heading1", "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: Times New Roman; font-style: italic; font-variant: inherit; font-wei" +
                        "ght: bold; font-size: 14pt; font-size-adjust: inherit; font-stretch: inherit", "Heading2", "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: inherit; font-style: inherit; font-variant: inherit; font-weight: bo" +
                        "ld; font-size: 13pt; font-size-adjust: inherit; font-stretch: inherit", "Heading3", "Normal"));
            ((System.ComponentModel.ISupportInitialize)(this.Picture1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TextBox3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTest)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();
        }

		#endregion

        private ReportHeader ReportHeader;
        private Picture Picture1;
        private TextBox TextBox3;
        private TextBox txtTest;
        private Detail Detail;
        private ReportFooter ReportFooter;
	}
}
