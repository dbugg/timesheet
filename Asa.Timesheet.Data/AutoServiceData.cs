using System;
using System.Collections;
using System.Text;

namespace Asa.Timesheet.Data
{
	public class AutoServiceData
	{
		private int _autoServiceID = -1;
		private string _autoServiceName = String.Empty;
		private string _autoServicePerson = String.Empty;
		private string _autoServicePhones = String.Empty;
		private bool _isActive = true;

		/// <summary>
		/// Initialize an new empty Login object.
		/// </summary>
		public AutoServiceData()
		{
		}
		
		/// <summary>
		/// Initialize a new Login object with the given parameters.
		/// </summary>
		public AutoServiceData(int autoServiceID, string autoServiceName, string autoServicePerson, string autoServicePhones, bool isActive)
		{
			this._autoServiceID = autoServiceID;
			this._autoServiceName = autoServiceName;
			this._autoServicePerson = autoServicePerson;
			this._autoServicePhones = autoServicePhones;
			this._isActive = isActive;
		}	
		
	  
		public override string ToString()
		{
			StringBuilder builder = new StringBuilder();
			builder.Append("_autoServiceID: " + _autoServiceID.ToString()).Append("\r\n");
			builder.Append("_autoServiceName: " + _autoServiceName.ToString()).Append("\r\n");
			builder.Append("_autoServicePerson: " + _autoServicePerson.ToString()).Append("\r\n");
			builder.Append("_autoServicePhones: " + _autoServicePhones.ToString()).Append("\r\n");
			builder.Append("_isActive: " + _isActive.ToString()).Append("\r\n");
			builder.Append("\r\n");
			return builder.ToString();
		}	
		public object GetByIndex(int nIndex)
		{
			switch(nIndex)
			{
				case 0:
					return _autoServiceID;
				case 1:
					return _autoServiceName;
				case 2:
					return _autoServicePerson;
				case 3:
					return _autoServicePhones;
				case 4:
					return _isActive;
			}
			return null;
		}
		#region Properties
	
		public int AutoServiceID
		{
			get { return _autoServiceID; }
			set { _autoServiceID = value; }
		}
	
		public string AutoServiceName
		{
			get { return _autoServiceName; }
			set { _autoServiceName = value; }
		}
	
		public string AutoServicePerson
		{
			get { return _autoServicePerson; }
			set { _autoServicePerson = value; }
		}
	
		public string AutoServicePhones
		{
			get { return _autoServicePhones; }
			set { _autoServicePhones = value; }
		}
	
		public bool IsActive
		{
			get { return _isActive; }
			set { _isActive = value; }
		}
	
		#endregion
	}
}


