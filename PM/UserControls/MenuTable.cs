using System;
using System.Web;
using System.Web.UI.WebControls;
using Asa.Timesheet.Data.Entities;
using log4net;
using log4net.Config;

namespace PM.UserControls
{
	/// <summary>
	/// Summary description for MenuTable.
	/// </summary>
	public class MenuTable : System.Web.UI.WebControls.Table
	{
		public const string _ASPX=".aspx";
		private static readonly ILog log = LogManager.GetLogger(typeof(MenuTable));
		private static readonly bool isDebugEnabled = log.IsDebugEnabled;
		private string LocalAddress="1";
		public MenuTable()
		{
			this.CssClass = "menuTable";
			this.CellSpacing = 0;
			this.BorderWidth = 0;
			this.Width = Unit.Percentage(100);
		}

		public void AddGroupHeader()
		{
			TableRow tr = new TableRow();
			TableCell tc = new TableCell();
			tr = new TableRow();
			tc = new TableCell();

			//tc.Text = "&nbsp;&nbsp;"+headerName;
			tr.Cells.Add(tc);
			this.Rows.Add(tr);
		}

		public void AddMenuItem(string linkName, string navigateUrl, bool NewWindow, Pages p)
		{
			string appPath = HttpContext.Current.Request.ApplicationPath;
			bool bIsInRoot = bool.Parse(System.Configuration.ConfigurationManager.AppSettings["IsInRoot"]);
			if(bIsInRoot)
				appPath="http://"+HttpContext.Current.Request.Url.Host;
			if(!HttpContext.Current.Request.Url.Host.StartsWith(LocalAddress))
				appPath+=System.Configuration.ConfigurationManager.AppSettings["Port"];
			//TableRow tr = new TableRow();
			TableCell tc = new TableCell();
			//tr.Cells.Add(tc);
			
			string sTarget="";
			if(p!=Pages.Undefined)
				navigateUrl+="#"+p.ToString();
			if(NewWindow)
				sTarget=" target= _blank ";
			tc.Text = 
				"<a" +sTarget + " href =  \""+ appPath+navigateUrl+ "\">"+ linkName +"</a>";
			this.Rows[0].Cells.Add(tc);
		}
//		public static string GetAppPathString()
//		{
//			string appPath = HttpContext.Current.Request.ApplicationPath;
//
//			bool bIsInRoot = bool.Parse(System.Configuration.ConfigurationManager.AppSettings["IsInRoot"]);
//			if(bIsInRoot)
//				appPath="http://"+HttpContext.Current.Request.Url.Host;
//			if(!HttpContext.Current.Request.Url.Host.StartsWith(LocalAddress))
//				appPath+=System.Configuration.ConfigurationManager.AppSettings["Port"];
//			return appPath;
//		}
		public void AddMenuItem(MenuItemInfo item)
		{
			string appPath = HttpContext.Current.Request.ApplicationPath;

			bool bIsInRoot = bool.Parse(System.Configuration.ConfigurationManager.AppSettings["IsInRoot"]);
			if(bIsInRoot)
				appPath="http://"+HttpContext.Current.Request.Url.Host;
			if(!HttpContext.Current.Request.Url.Host.StartsWith(LocalAddress))
				appPath+=System.Configuration.ConfigurationManager.AppSettings["Port"];
			//TableRow tr = new TableRow();
			TableCell tc = new TableCell();

			//tr.Cells.Add(tc);
		
			string sTarget="";
			if(item!=null && item.PreviousPage!=null && item.PreviousPage!="")
				item.NavigateUrl+="#"+item.PreviousPage.ToString();
			if(item.NewWindow)
				sTarget=" target= _blank ";
			string cssClass = (item.Highlighted) ? " class = highlightedMenuItem " : " class = menuItem ";

			tc.Text = 
				"<a" +sTarget + cssClass + " href =  \""+appPath+ item.NavigateUrl + "\">"+ item.Title +"</a>";
			this.Rows[0].Cells.Add(tc);
		}

		public void AddVerticalBlank(int pixels)
		{
//			TableRow tr = new TableRow();
//			TableCell tc = new TableCell();
//			tc.Style.Add("HEIGHT", pixels.ToString());
//			tr.Cells.Add(tc);
//			this.Rows.Add(tr);
		}

		public void AddHLine()
		{
//			string appPath = HttpContext.Current.Request.ApplicationPath;
//			TableRow tr = new TableRow();
//			TableCell tc = new TableCell();
//			tc.HorizontalAlign = HorizontalAlign.Center;
//			tc.Text = "<IMG style=\"WIDTH: 100px; HEIGHT: 1px\" height=\"1\" alt=\"\" src=\""+appPath+"/images/menuline.bmp\" width=\"88\">";
//			tr.Cells.Add(tc);
//			this.Rows.Add(tr);
		}
		
		public void AddMenuGroup( int topBlank, System.Collections.ArrayList menuItems)
		{
			if (menuItems.Count == 0) return;

			AddVerticalBlank(topBlank);
			AddGroupHeader();
			AddHLine();

			for (int i=0; i<menuItems.Count; i++)
			{
				MenuItemInfo mi = (MenuItemInfo)menuItems[i];
				//AddMenuItem(mi.Title, mi.NavigateUrl, mi.NewWindow, mi.PreviousPage);	
				AddMenuItem(mi);
			}

			AddHLine();
		}
	}
}
