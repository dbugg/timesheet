﻿<%@ Page language="c#" Codebehind="EditProtokols.aspx.cs" AutoEventWireup="false" Inherits="Asa.Timesheet.WebPages.EditProtokols" %>
<%@ Register TagPrefix="uc1" TagName="EditForm" Src="UserControls/EditForm.ascx" %>
<%@ Register TagPrefix="uc1" TagName="PageHeader" Src="UserControls/PageHeader.ascx" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
	<head runat="server">
		<title>Protokoly</title>
		<meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
		<meta content="C#" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<LINK href="styles/timesheet.css" type="text/css" rel="stylesheet">
		<script language="javascript" src="PopupCalendar.js"></script>
	</HEAD>
	<body bottomMargin="0" leftMargin="0" topMargin="0" rightMargin="0">
		<form id="Form1" method="post" runat="server">
			<table height="100%" cellSpacing="0" cellPadding="0" width="100%" border="0">
				<tr height="1">
					<td><uc1:pageheader id="header" runat="server"></uc1:pageheader><uc1:editform id="editCtrl" runat="server"></uc1:editform></td>
				</tr>
				<tr>
					<td>
						<table height="100%" cellSpacing="0" cellPadding="0" width="100%" border="0">
							<tr>
								<td class="menuLeft"><asp:placeholder id="menuHolder" runat="server"></asp:placeholder></td>
								
								<td style="PADDING-LEFT: 5px; PADDING-TOP: 3px" vAlign="top" noWrap>
									<TABLE id="Table3" cellSpacing="0" cellPadding="0" width="880" border="0">
										<TR>
											<td colSpan="2"><IMG height="1" src="images/dot.gif" width="100%"></TD>
										</TR>
										<TR>
											<td colSpan="2" height="3"></td>
										</TR>
										<TR>
											<td style="WIDTH: 4px"></TD>
											<td>
												<TABLE id="Table6" cellSpacing="0" cellPadding="0" border="0">
													<TR>
														<td style="WIDTH: 97px"><asp:button id="btnSaveUP" runat="server" Text="Запиши" EnableViewState="False" CssClass="ActionButton"></asp:button><asp:button id="btnEditUP" style="DISPLAY: none" runat="server" Text="Редактирай" EnableViewState="False"
																CssClass="ActionButton"></asp:button></TD>
														<td style="WIDTH: 245px"><asp:button id="btnCancelUP" runat="server" Text="Откажи" EnableViewState="False" CssClass="ActionButton"></asp:button></TD>
														<td></TD>
													</TR>
												</TABLE>
											</TD>
										</TR>
										<TR>
											<td height="3"></TD>
										</TR>
										<TR>
											<td colSpan="2"><IMG height="1" src="images/dot.gif" width="100%"></TD>
										</TR>
										<TR>
											<td style="WIDTH: 4px"></TD>
											<td>
												<TABLE id="tblForm" cellSpacing="0" cellPadding="3" width="100%" border="0">
													<TBODY>
														<TR>
															<td style="WIDTH: 168px; HEIGHT: 29px"><asp:label id="Label1" runat="server" EnableViewState="False" CssClass="enterDataLabel" Width="100%">Тип:</asp:label></TD>
															<td style="HEIGHT: 30px"><asp:dropdownlist id="ddlProjectsStatus" runat="server" CssClass="EnterDataBox" Width="250px" AutoPostBack="True"></asp:dropdownlist></TD>
														</TR>
														<TR>
															<td style="WIDTH: 168px; HEIGHT: 36px"><asp:label id="Label8" runat="server" CssClass="enterDataLabel" Width="100%">Сграда:</asp:label></TD>
															<td style="HEIGHT: 34px"><asp:dropdownlist id="ddlBuildingTypes" runat="server" CssClass="enterDataBox" Width="250px" AutoPostBack="True"></asp:dropdownlist></TD>
														</TR>
														<TR>
															<td style="WIDTH: 168px; HEIGHT: 29px"><asp:label id="lblName" runat="server" EnableViewState="False" CssClass="enterDataLabel" Width="100%"
																	Font-Bold="True">Проект:</asp:label></TD>
															<td style="HEIGHT: 30px"><asp:dropdownlist id="ddlProject" runat="server" CssClass="EnterDataBox" Width="250px" AutoPostBack="True"></asp:dropdownlist><IMG height="16" alt="" src="images/required1.gif" width="16">
															</TD>
														</TR>
														<TR>
															<td style="WIDTH: 168px"><asp:label id="lbDate" runat="server" CssClass="enterDataLabel" Width="100%" Font-Bold="True">Дата:</asp:label></TD>
															<td><asp:textbox id="txtStartDate" runat="server" CssClass="enterDataBox" Width="88px" MaxLength="10"></asp:textbox>&nbsp;<IMG id="lkCalendar1" alt="Calendar" src="images/calendar.ico" width="18" align="absBottom"
																	runat="server">&nbsp;<IMG height="16" alt="" src="images/required1.gif" width="16"></TD>
														</TR>
														<TR>
															<td style="WIDTH: 168px"><asp:label id="Label2" runat="server" CssClass="enterDataLabel" Width="100%" Font-Bold="True">Приет/предаден:</asp:label></TD>
															<td><asp:RadioButtonList id="rbb" runat="server">
																	<asp:ListItem Value="0">Приет</asp:ListItem>
																	<asp:ListItem Value="1">Предаден</asp:ListItem>
																</asp:RadioButtonList></TD>
														</TR>
														<TR>
															<td style="WIDTH: 168px"><asp:label id="Label9" runat="server" CssClass="enterDataLabel" Width="100%">Бележки:</asp:label></TD>
															<td><asp:textbox id="txtNotes" runat="server" CssClass="enterDataBox" Width="368px" MaxLength="10"
																	TextMode="MultiLine" Height="50px"></asp:textbox></TD>
														</TR>
														<TR>
															<td style="WIDTH: 168px"><asp:label id="lbScanDocNew" runat="server" CssClass="enterDataLabel" Width="100%">Нов сканиран документ:</asp:label></TD>
															<td><INPUT id="FileCtrl" type="file" name="File1" runat="server"><asp:button id="btnScanDocAdd" runat="server" Text="Добави" CssClass="ActionButton"></asp:button></TD>
														</TR>
														<TR>
															<td style="WIDTH: 168px"><asp:label id="Label6" runat="server" CssClass="enterDataLabel" Width="100%">Сканирани документи:</asp:label></TD>
															<td><asp:datalist id="dlDocs" runat="server" RepeatDirection="Horizontal">
																	<ItemTemplate>
																		<asp:HyperLink id="btnScan" runat="server" ToolTip="Сканиран документ" NavigateUrl='<%# GetURL((int)DataBinder.Eval(Container, "DataItem.ProjectProtokolID"))%>' Target=_blank>
																			<img border="0" src="images/pdf.gif" id="notEdit" /></asp:HyperLink>
																		<br />
																		<asp:ImageButton id="btnDel" runat="server" ToolTip="Изтрий" ImageUrl="images/delete.gif" Visible='<%# GetVis()%>' >
																		</asp:ImageButton>
																	</ItemTemplate>
																</asp:datalist></TD>
														</TR>
													</TBODY>
												</TABLE>
											</TD>
										</TR>
										<TR>
											<td style="HEIGHT: 5px" colSpan="2"></TD>
										</TR>
										<TR>
											<td colSpan="2"><IMG height="1" src="images/dot.gif" width="100%"></TD>
										</TR>
										<TR>
											<td colSpan="2" height="3"></td>
										</TR>
										<TR>
											<td style="WIDTH: 4px"></TD>
											<td>
												<TABLE id="Table5" cellSpacing="0" cellPadding="0" border="0">
													<TR>
														<td style="WIDTH: 97px"><asp:button id="btnSave" runat="server" Text="Запиши" EnableViewState="False" CssClass="ActionButton"></asp:button><asp:button id="btnEdit" style="DISPLAY: none" runat="server" Text="Редактирай" EnableViewState="False"
																CssClass="ActionButton"></asp:button></TD>
														<td style="WIDTH: 245px"><asp:button id="btnCancel" runat="server" Text="Откажи" EnableViewState="False" CssClass="ActionButton"></asp:button></TD>
														<td><asp:button id="btnDelete" runat="server" Text="Изтрий" EnableViewState="False" CssClass="ActionButton"
																Visible="False"></asp:button></TD>
													</TR>
												</TABLE>
											</TD>
										</TR>
										<TR>
											<td height="3"></TD>
										</TR>
										<TR>
											<td colSpan="2"><IMG height="1" src="images/dot.gif" width="100%"></TD>
										</TR>
									</TABLE>
									<br>
									&nbsp;
									<asp:label id="lblInfo" runat="server" EnableViewState="False" CssClass="InfoLabel"></asp:label><asp:label id="lblError" runat="server" EnableViewState="False" CssClass="ErrorLabel" ForeColor="Red"></asp:label></td>
							</tr>
						</table>
					</td>
				</tr>
			</table>
		</form>
	</body>
</HTML>
