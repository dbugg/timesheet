using System;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;

namespace Asa.Timesheet.Data
{
	/// <summary>
	/// Summary description for CommonDAL.
	/// </summary>
	public class CommonDAL
	{
		public static string m_ConnectionString=ConfigurationSettings.AppSettings["ConnectionString"];

		public static string ConnectionString
		{
			get {return m_ConnectionString;}
			set 
			{
				m_ConnectionString=value;
			}
		}
		#region Common Methods
		/// <summary>
		/// Opens and returns the connection object
		/// </summary>
		public static SqlConnection OpenConnection()
		{	
			SqlConnection cnDAL = new SqlConnection (m_ConnectionString);
			cnDAL.Open();
			return cnDAL;
		}
		public static SqlConnection OpenConnection(string cnstr)
		{	
			SqlConnection cnDAL = new SqlConnection (cnstr);
			cnDAL.Open();
			return cnDAL;
		}
		/// <summary>
		/// Closes passed connection
		/// </summary>
		public static void CloseConnection(SqlConnection cn)
		{
			if(cn!=null)
			{
				if(cn.State==ConnectionState.Open)
				{
					cn.Close();
				}
				((IDisposable) cn).Dispose();
			}
		}
		
		public static DataTable CreateTable(SqlDataReader rd)
		{
			DataTable dt = new DataTable();
			for(int i = 0;i<rd.FieldCount; i++)
			{
				dt.Columns.Add (rd.GetName(i),rd.GetFieldType(i));  
			}

			return dt;
		}
		/// <summary>
		/// Creates table from SqlDataReader
		/// </summary>
		/// <returns>the created table</returns>
		public static DataTable CreateTableFromReader(SqlDataReader rd)
		{
			DataTable dt = CreateTable(rd);

			while(rd.Read())
			{
				DataRow dr = dt.NewRow(); 
				foreach (DataColumn col in dt.Columns)
				{
					
					dr[col.ColumnName] = rd[col.ColumnName];
					
				}
				dt.Rows.Add (dr); 
			}

			return dt;

		}
		private static SqlCommand Create_Arhiv ()
		{
			SqlCommand cmd = new SqlCommand();
			cmd.CommandType = System.Data.CommandType.StoredProcedure;
			cmd.CommandText = "_Arhiv";

			return cmd;
		}
		public static int Execute_Arhiv()
		{
			int RetObj;
			System.Data.SqlClient.SqlCommand cmd;
			cmd = Create_Arhiv();
			SqlConnection cnDAL = new SqlConnection();

			try
			{
				cnDAL =  CommonDAL.OpenConnection();
				cmd.Connection = cnDAL;
				RetObj = cmd.ExecuteNonQuery();
				return RetObj;
		
			}
			catch(SqlException ex)
			{

				throw new ApplicationException("_Arhiv",ex);
			}

			finally
			{
				CommonDAL.CloseConnection(cnDAL);
			}
		}

		#endregion
	}

}
