USE [asatimesheet]
GO
/****** Object:  StoredProcedure [dbo].[ReportsSelectAbsence]    Script Date: 8/26/2017 7:04:47 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO






ALTER    PROCEDURE [dbo].[ReportsSelectAbsence] 

@UserID int = null,
@ProjectID int = null,
@StartDate smalldatetime = null,
@EndDate smalldatetime = null,
@IncludeInactive bit = 0,
@IncludeSpecial bit = null

AS

SELECT  distinct   dbo.Users.UserID, dbo.Users.FullName as [Name]
FROM         dbo.Users INNER JOIN
                      dbo.WorkTimes ON dbo.Users.UserID = dbo.WorkTimes.UserID
WHERE ( (@ProjectID is not null and @ProjectID=ProjectID) or (@ProjectID is null and  ProjectID in (7,9,10,401,402))  )
	and WorkDate between @StartDate and @EndDate
and ((@IncludeInactive=1) OR (IsActive = 1)) and dbo.WorkTimes.UserID=isnull(@UserID,dbo.WorkTimes.UserID)
	and(  ( isnull(@IncludeSpecial,1) = 1) or  (RoleID!=1 And RoleID!=4) )
	and (users.WorkStartedDate is null or users.WorkStartedDate<=WorkDate) 
order by FullName

/*
select ProjectID, ProjectName as Name
from Projects where  ( (@ProjectID is not null and @ProjectID=ProjectID) or (@ProjectID is null and  ProjectID in (22,23,24))  )

*/
SELECT   dbo.WorkTimes.UserID, 
	dbo.WorkTimes.WorkDate, 
	dbo.WorkTimes.ProjectID, 
	SUM(dbo.InMinutes(Times_1.TimeString) 
                      - dbo.InMinutes(dbo.Times.TimeString)) AS TimeInMinutes
FROM         
	dbo.WorkTimes INNER JOIN
                      dbo.Times ON dbo.WorkTimes.StartTimeID = dbo.Times.TimeID INNER JOIN
                      dbo.Times Times_1 ON dbo.WorkTimes.EndTimeID = Times_1.TimeID INNER JOIN USERS
on WorkTimes.UserID = Users.UserID
WHERE  ( (@ProjectID is not null and @ProjectID=ProjectID) or (@ProjectID is null and  ProjectID in (7,9,10,401,402))  )
and (((@IncludeInactive=1) OR (IsActive = 1)) and WorkDate between @StartDate and @EndDate)
	and(  ( isnull(@IncludeSpecial,1) = 1) or  (RoleID!=1 And RoleID!=4) )
 and dbo.WorkTimes.UserID=isnull(@UserID,dbo.WorkTimes.UserID)
GROUP BY dbo.WorkTimes.UserID, dbo.WorkTimes.WorkDate, dbo.WorkTimes.ProjectID
ORDER BY dbo.WorkTimes.WorkDate





