using System;
using System.Collections;
using System.Text;

namespace Asa.Timesheet.Data
{
	public class MeetingData
	{
		private int _meetingID = -1;
		private DateTime _meetingDate;
		private int _projectID = -1;
		private int _activityID = -1;
		private int _startTimeID = -1;
		private int _endTimeID = -1;
		private string _clients = String.Empty;
		private string _subcontracters = String.Empty;
		private string _users = String.Empty;
		private int _createdByID = -1;
		private bool _approved;
		private int _approvedID = -1;
		private string _projectName = String.Empty;
		private string _startHour = String.Empty;
		private string _endHour = String.Empty;
		private bool _hasScan=false;
		private string _notes = String.Empty;
		private string _otherPeople = String.Empty;
		private string _place = String.Empty;
		private int _clientID = -1;
		/// <summary>
		/// Initialize an new empty Login object.
		/// </summary>
		public MeetingData()
		{
		}
		
		/// <summary>
		/// Initialize a new Login object with the given parameters.
		/// </summary>
		public MeetingData(int meetingID, DateTime meetingDate, int projectID, int activityID, int startTimeID, int endTimeID, string clients, string subcontracters, string users, int createdByID, bool approved, int approvedID)
		{
			this._meetingID = meetingID;
			this._meetingDate = meetingDate;
			this._projectID = projectID;
			this._activityID = activityID;
			this._startTimeID = startTimeID;
			this._endTimeID = endTimeID;
			this._clients = clients;
			this._subcontracters = subcontracters;
			this._users = users;
			this._createdByID = createdByID;
			this._approved = approved;
			this._approvedID = approvedID;
		}	
		
	  
		public override string ToString()
		{
			StringBuilder builder = new StringBuilder();
			builder.Append("_meetingID: " + _meetingID.ToString()).Append("\r\n");
			builder.Append("_meetingDate: " + _meetingDate.ToString()).Append("\r\n");
			builder.Append("_projectID: " + _projectID.ToString()).Append("\r\n");
			builder.Append("_activityID: " + _activityID.ToString()).Append("\r\n");
			builder.Append("_startTimeID: " + _startTimeID.ToString()).Append("\r\n");
			builder.Append("_endTimeID: " + _endTimeID.ToString()).Append("\r\n");
			builder.Append("_clients: " + _clients.ToString()).Append("\r\n");
			builder.Append("_subcontracters: " + _subcontracters.ToString()).Append("\r\n");
			builder.Append("_users: " + _users.ToString()).Append("\r\n");
			builder.Append("_createdByID: " + _createdByID.ToString()).Append("\r\n");
			builder.Append("_approved: " + _approved.ToString()).Append("\r\n");
			builder.Append("_approvedID: " + _approvedID.ToString()).Append("\r\n");
			builder.Append("\r\n");
			return builder.ToString();
		}	
		public object GetByIndex(int nIndex)
		{
			switch(nIndex)
			{
				case 0:
					return _meetingID;
				case 1:
					return _meetingDate;
				case 2:
					return _projectID;
				case 3:
					return _activityID;
				case 4:
					return _startTimeID;
				case 5:
					return _endTimeID;
				case 6:
					return _clients;
				case 7:
					return _subcontracters;
				case 8:
					return _users;
				case 9:
					return _createdByID;
				case 10:
					return _approved;
				case 11:
					return _approvedID;
			}
			return null;
		}
		#region Properties
	
		public string Notes
		{
			get { return _notes; }
			set { _notes = value; }
		}
		public string OtherPeople
		{
			get { return _otherPeople; }
			set { _otherPeople = value; }
		}
		public string Place
		{
			get { return _place; }
			set { _place = value; }
		}
		public int MeetingID
		{
			get { return _meetingID; }
			set { _meetingID = value; }
		}
		public int ClientID
		{
			get { return _clientID; }
			set { _clientID = value; }
		}
		
		public bool HasScan
		{
			get { return _hasScan; }
			set { _hasScan = value; }
		}
		public string ProjectName
		{
			get { return _projectName; }
			set { _projectName = value; }
		}
		public string StartHour
		{
			get { return _startHour; }
			set { _startHour = value; }
		}
		public string EndHour
		{
			get { return _endHour; }
			set { _endHour = value; }
		}
		public DateTime MeetingDate
		{
			get { return _meetingDate; }
			set { _meetingDate = value; }
		}
	
		public int ProjectID
		{
			get { return _projectID; }
			set { _projectID = value; }
		}
	
		public int ActivityID
		{
			get { return _activityID; }
			set { _activityID = value; }
		}
	
		public int StartTimeID
		{
			get { return _startTimeID; }
			set { _startTimeID = value; }
		}
	
		public int EndTimeID
		{
			get { return _endTimeID; }
			set { _endTimeID = value; }
		}
	
		public string Clients
		{
			get { return _clients; }
			set { _clients = value; }
		}
	
		public string Subcontracters
		{
			get { return _subcontracters; }
			set { _subcontracters = value; }
		}
	
		public string Users
		{
			get { return _users; }
			set { _users = value; }
		}
	
		public int CreatedByID
		{
			get { return _createdByID; }
			set { _createdByID = value; }
		}
	
		public bool Approved
		{
			get { return _approved; }
			set { _approved = value; }
		}
	
		public int ApprovedID
		{
			get { return _approvedID; }
			set { _approvedID = value; }
		}
	
		#endregion
	}
}


