USE [asatimesheet]
GO
/****** Object:  StoredProcedure [dbo].[WorkTimesSelectProjectsForDay]    Script Date: 2/8/2018 9:45:59 AM ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER ON
GO






ALTER    PROCEDURE [dbo].[WorkTimesSelectProjectsForDay]

@Date smalldatetime,
@IsBuildingTypeGradoustrShow bit = null

AS
select * from 
(
SELECT    WorkTimes.ProjectID, 
	    RTRIM([ProjectName]+ ' '+isnull([ProjectCode],'')) as ProjectName, 
	    HasActivity,
	    HasDefaultWorkTimes
FROM       WorkTimes INNER JOIN Projects 
	    ON WorkTimes.ProjectID = Projects.ProjectID
WHERE WorkDate = @Date --and concluded=0
 and (@IsBuildingTypeGradoustrShow is null or @IsBuildingTypeGradoustrShow = 1 or BuildingTypeID<>1)
--and IsVisible=1
UNION
(
SELECT
	[ProjectID],
	RTRIM([ProjectName]+ ' '+isnull([ProjectCode],'')) as ProjectName,
	[HasActivity],
	HasDefaultWorkTimes
FROM
	Projects
	WHERE IsActive = 1
	and IsVisible=1 --and concluded=0
 and (@IsBuildingTypeGradoustrShow is null or @IsBuildingTypeGradoustrShow = 1 or BuildingTypeID<>1)

)
) t

ORDER BY t.ProjectName


