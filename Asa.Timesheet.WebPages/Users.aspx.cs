using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Data.SqlClient;
using log4net;
using Asa.Timesheet.Data;
using Asa.Timesheet.Data.Entities;

namespace Asa.Timesheet.WebPages
{
	/// <summary>
	/// Summary description for Users.
	/// </summary>
	public class Users : TimesheetPageBase
	{
		#region Web controls

		protected System.Web.UI.WebControls.PlaceHolder menuHolder;
		protected System.Web.UI.WebControls.Label lblInfo;
		protected System.Web.UI.WebControls.Panel grid;
		protected System.Web.UI.WebControls.Button btnNewUser;
		protected Asa.Timesheet.WebPages.UserControls.PageHeader header;
		protected System.Web.UI.WebControls.Label lblError;
		protected Telerik.WebControls.RadGrid gridCalls;
		protected System.Web.UI.WebControls.Button btnExport;
		protected System.Web.UI.WebControls.DataGrid grdUsersArchitect;
		protected System.Web.UI.WebControls.DataGrid grdUsersIngener;
		protected System.Web.UI.WebControls.DataGrid grdUsersIT;
		protected System.Web.UI.WebControls.DataGrid grdUsers;
		protected System.Web.UI.WebControls.DataGrid grdUsersACC;
		protected System.Web.UI.WebControls.DataGrid grdUsersSecretary;
		protected System.Web.UI.WebControls.DataGrid grdUsersCleaner;
		protected System.Web.UI.WebControls.DataGrid grdUsersLowers;
		protected System.Web.UI.WebControls.Label lbSectionArchitects;
		protected System.Web.UI.WebControls.Label lbSectionIngener;
		protected System.Web.UI.WebControls.Label lbSectionIT;
		protected System.Web.UI.WebControls.Label lbSectionACC;
		protected System.Web.UI.WebControls.Label lbSectionSecretary;
		protected System.Web.UI.WebControls.Label lbSectionCleaner;
		protected System.Web.UI.WebControls.Label lbSectionLowers;
		#endregion
		
		private static readonly ILog log = LogManager.GetLogger(typeof(Users));
		
		#region Grid columns

		private enum GridColumns
		{
			Number=0,
			FirstName,
			LastName,
			Ext,
			Mobile,
			Phone,
			Email,
			RoleName,
			EditItem,
			DeleteItem
		}

		#endregion
	
		private void Page_Load(object sender, System.EventArgs e)
		{
			//if (! (LoggedUser.IsLeader ||  LoggedUser.IsAdmin)) ErrorRedirect(Resource.ResourceManager["noRightsAlert"]);
			
			UIHelpers.CreateMenu(menuHolder,LoggedUser);
			if (! ( LoggedUser.HasPaymentRights ||  LoggedUser.IsAdmin))
				btnNewUser.Visible=false;
			if (!this.IsPostBack)
			{
				header.PageTitle = Resource.ResourceManager["users_TitleLabel"];
				header.UserName = LoggedUser.UserName;

				btnNewUser.Width = 100;
				btnNewUser.Text = Resource.ResourceManager["users_btnNewUser"];
				SortOrder = 1;

				BindGrid();
			}
			
		}

	
		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
			this.grdUsersArchitect.ItemCreated += new System.Web.UI.WebControls.DataGridItemEventHandler(this.grdUsers_ItemCreated);
			this.grdUsersArchitect.ItemCommand += new System.Web.UI.WebControls.DataGridCommandEventHandler(this.grdUsers_ItemCommand);
			this.grdUsersIngener.ItemCreated += new System.Web.UI.WebControls.DataGridItemEventHandler(this.grdUsers_ItemCreated);
			this.grdUsersIngener.ItemCommand += new System.Web.UI.WebControls.DataGridCommandEventHandler(this.grdUsers_ItemCommand);
			this.grdUsersIT.ItemCreated += new System.Web.UI.WebControls.DataGridItemEventHandler(this.grdUsers_ItemCreated);
			this.grdUsersIT.ItemCommand += new System.Web.UI.WebControls.DataGridCommandEventHandler(this.grdUsers_ItemCommand);
			this.grdUsersACC.ItemCreated += new System.Web.UI.WebControls.DataGridItemEventHandler(this.grdUsers_ItemCreated);
			this.grdUsersACC.ItemCommand += new System.Web.UI.WebControls.DataGridCommandEventHandler(this.grdUsers_ItemCommand);
			this.grdUsersSecretary.ItemCreated += new System.Web.UI.WebControls.DataGridItemEventHandler(this.grdUsers_ItemCreated);
			this.grdUsersSecretary.ItemCommand += new System.Web.UI.WebControls.DataGridCommandEventHandler(this.grdUsers_ItemCommand);
			this.grdUsersCleaner.ItemCreated += new System.Web.UI.WebControls.DataGridItemEventHandler(this.grdUsers_ItemCreated);
			this.grdUsersCleaner.ItemCommand += new System.Web.UI.WebControls.DataGridCommandEventHandler(this.grdUsers_ItemCommand);
			this.grdUsersLowers.ItemCreated += new System.Web.UI.WebControls.DataGridItemEventHandler(this.grdUsers_ItemCreated);
			this.grdUsersLowers.ItemCommand += new System.Web.UI.WebControls.DataGridCommandEventHandler(this.grdUsers_ItemCommand);
			this.btnNewUser.Click += new System.EventHandler(this.btnNewUser_Click);
			this.btnExport.Click += new System.EventHandler(this.btnExport_Click);
			this.Load += new System.EventHandler(this.Page_Load);

		}
		#endregion

		#region database

		private void BindGrid()
		{
			
				BindArchitects();
				BindIngeners();
				BindIT();
				BindACC();
				BindCleaner();
				BindLowers();
				BindSecretary();
				
				
				
		}
		private void BindArchitects()
		{
			SqlDataReader reader = null;
			try
			{
				string nameOfProcedure = "UsersListArcgitectProc";
				reader = UsersData.SelectUsersSplit(Constants.DirectorRole, SortOrder,nameOfProcedure);

				if (reader!=null)
				{
					grdUsersArchitect.DataSource = reader;
					grdUsersArchitect.DataKeyField = "UserID";
					grdUsersArchitect.DataBind();
					if (!LoggedUser.IsLeader)
						grdUsersArchitect.Columns[(int)GridColumns.DeleteItem].Visible = false;
				}
				else
				{
					grdUsersArchitect.Visible = lbSectionArchitects.Visible = false;
				}
			}
			catch (Exception ex)
			{
				log.Error(ex);
				lblError.Text = Resource.ResourceManager["users_ErrorLoadUsers"];
				return;
			}
			finally 
			{
				if (reader!=null) reader.Close();
			}

			try
			{
				SetConfirmDelete(grdUsersArchitect, "btnDelete", Resource.ResourceManager["users_ConfirmDeleteDefaultName"]);
			}
			catch (Exception ex)
			{
				log.Error(ex);
			}
		}
		private void BindSecretary()
		{
			SqlDataReader reader = null;
			try
			{
				string nameOfProcedure = "UsersListSecretaryProc";
				reader = UsersData.SelectUsersSplit(Constants.DirectorRole, SortOrder,nameOfProcedure);

				if (reader!=null)
				{
					grdUsersSecretary.DataSource = reader;
					grdUsersSecretary.DataKeyField = "UserID";
					grdUsersSecretary.DataBind();
					if (!LoggedUser.IsLeader)
						grdUsersSecretary.Columns[(int)GridColumns.DeleteItem].Visible = false;
				}
				else
				{
					grdUsersSecretary.Visible = lbSectionSecretary.Visible = false;
				}
			}
			catch (Exception ex)
			{
				log.Error(ex);
				lblError.Text = Resource.ResourceManager["users_ErrorLoadUsers"];
				return;
			}
			finally 
			{
				if (reader!=null) reader.Close();
			}

			try
			{
				SetConfirmDelete(grdUsersSecretary, "btnDelete", Resource.ResourceManager["users_ConfirmDeleteDefaultName"]);
			}
			catch (Exception ex)
			{
				log.Error(ex);
			}
		}
		private void BindACC()
		{
			SqlDataReader reader = null;
			try
			{
				string nameOfProcedure = "UsersListACCProc";
				reader = UsersData.SelectUsersSplit(Constants.DirectorRole, SortOrder,nameOfProcedure);

				if (reader!=null)
				{
					grdUsersACC.DataSource = reader;
					grdUsersACC.DataKeyField = "UserID";
					grdUsersACC.DataBind();
					if (!LoggedUser.IsLeader)
						grdUsersACC.Columns[(int)GridColumns.DeleteItem].Visible = false;
				}
				else
				{
					grdUsersACC.Visible = lbSectionACC.Visible = false;
				}
			}
			catch (Exception ex)
			{
				log.Error(ex);
				lblError.Text = Resource.ResourceManager["users_ErrorLoadUsers"];
				return;
			}
			finally 
			{
				if (reader!=null) reader.Close();
			}

			try
			{
				SetConfirmDelete(grdUsersACC, "btnDelete", Resource.ResourceManager["users_ConfirmDeleteDefaultName"]);
			}
			catch (Exception ex)
			{
				log.Error(ex);
			}
		}
		private void BindCleaner()
		{
			SqlDataReader reader = null;
			try
			{
				string nameOfProcedure = "UsersListCleanerProc";
				reader = UsersData.SelectUsersSplit(Constants.DirectorRole, SortOrder,nameOfProcedure);

				if (reader!=null)
				{
					grdUsersCleaner.DataSource = reader;
					grdUsersCleaner.DataKeyField = "UserID";
					grdUsersCleaner.DataBind();
					if (!LoggedUser.IsLeader)
						grdUsersCleaner.Columns[(int)GridColumns.DeleteItem].Visible = false;
				}
				else
				{
					grdUsersCleaner.Visible = lbSectionCleaner.Visible = false;
				}
			}
			catch (Exception ex)
			{
				log.Error(ex);
				lblError.Text = Resource.ResourceManager["users_ErrorLoadUsers"];
				return;
			}
			finally 
			{
				if (reader!=null) reader.Close();
			}

			try
			{
				SetConfirmDelete(grdUsersCleaner, "btnDelete", Resource.ResourceManager["users_ConfirmDeleteDefaultName"]);
			}
			catch (Exception ex)
			{
				log.Error(ex);
			}
		}
		private void BindLowers()
		{
			SqlDataReader reader = null;
			try
			{
				string nameOfProcedure = "UsersListLawyerProc";
				reader = UsersData.SelectUsersSplit(Constants.DirectorRole, SortOrder,nameOfProcedure);

				if (reader!=null)
				{
					grdUsersLowers.DataSource = reader;
					grdUsersLowers.DataKeyField = "UserID";
					grdUsersLowers.DataBind();
					if (!LoggedUser.IsLeader)
						grdUsersLowers.Columns[(int)GridColumns.DeleteItem].Visible = false;
				}
				else
				{
					grdUsersLowers.Visible = lbSectionLowers.Visible = false;
				}
			}
			catch (Exception ex)
			{
				log.Error(ex);
				lblError.Text = Resource.ResourceManager["users_ErrorLoadUsers"];
				return;
			}
			finally 
			{
				if (reader!=null) reader.Close();
			}

			try
			{
				SetConfirmDelete(grdUsersLowers, "btnDelete", Resource.ResourceManager["users_ConfirmDeleteDefaultName"]);
			}
			catch (Exception ex)
			{
				log.Error(ex);
			}
		}
		private void BindIngeners()
		{
			SqlDataReader reader = null;
			try
			{
				string nameOfProcedure = "UsersListIngenerProc";
				reader = UsersData.SelectUsersSplit(Constants.DirectorRole, SortOrder,nameOfProcedure);

				if (reader!=null)
				{
					grdUsersIngener.DataSource = reader;
					grdUsersIngener.DataKeyField = "UserID";
					grdUsersIngener.DataBind();
					if (!LoggedUser.IsLeader)
						grdUsersIngener.Columns[(int)GridColumns.DeleteItem].Visible = false;
				}
				else
				{
					grdUsersIngener.Visible = lbSectionIngener.Visible = false;
				}
			}
			catch (Exception ex)
			{
				log.Error(ex);
				lblError.Text = Resource.ResourceManager["users_ErrorLoadUsers"];
				return;
			}
			finally 
			{
				if (reader!=null) reader.Close();
			}

			try
			{
				SetConfirmDelete(grdUsersIngener, "btnDelete", Resource.ResourceManager["users_ConfirmDeleteDefaultName"]);
			}
			catch (Exception ex)
			{
				log.Error(ex);
			}
		}
		private void BindIT()
		{
			SqlDataReader reader = null;
			try
			{
				string nameOfProcedure = "UsersListITProc";
				reader = UsersData.SelectUsersSplit(Constants.DirectorRole, SortOrder,nameOfProcedure);

				if (reader!=null)
				{
					grdUsersIT.DataSource = reader;
					grdUsersIT.DataKeyField = "UserID";
					grdUsersIT.DataBind();
					if (!LoggedUser.IsLeader)
						grdUsersIT.Columns[(int)GridColumns.DeleteItem].Visible = false;
				}
				else
				{
					grdUsersIT.Visible = lbSectionIT.Visible = false;
				}
			}
			catch (Exception ex)
			{
				log.Error(ex);
				lblError.Text = Resource.ResourceManager["users_ErrorLoadUsers"];
				return;
			}
			finally 
			{
				if (reader!=null) reader.Close();
			}

			try
			{
				SetConfirmDelete(grdUsersIT, "btnDelete", Resource.ResourceManager["users_ConfirmDeleteDefaultName"]);
			}
			catch (Exception ex)
			{
				log.Error(ex);
			}
		}
		private bool DeleteUser(int userID)
		{
			try
			{
				UsersData.InactiveUser(userID);
			}
			catch (Exception ex)
			{
				log.Error(ex);
				return false;
			}

			return true;
		}

		#endregion

		#region Event handlers
		
		private void grdUsers_ItemCommand(object source, System.Web.UI.WebControls.DataGridCommandEventArgs e)
		{
			if (e.CommandName == "Sort")
			{
				int sortOrder = SortOrder;
				switch ((string)e.CommandArgument)
				{
					case "FirstName":if (sortOrder==1) sortOrder = -1; else sortOrder = 1;
									SortOrder = sortOrder;
									BindGrid(); 
									break;
					case "LastName":if (sortOrder==2) sortOrder = -2; else sortOrder = 2;
									SortOrder = sortOrder;
									BindGrid(); break;
					case "Account":if (sortOrder==3) sortOrder = -3; else sortOrder = 3;
									SortOrder = sortOrder;
									BindGrid(); break;
					case "EMail":if (sortOrder==4) sortOrder = -4; else sortOrder = 4;
									SortOrder = sortOrder;
									BindGrid(); 
									break;
					case "Role": if (sortOrder==5) sortOrder = -5; else sortOrder = 5;
									SortOrder = sortOrder;
									BindGrid(); 
									break;
				}
				return;
			}
			else if(e.CommandName=="Edit")
			{
				string sUserID = ((DataGrid)source).DataKeys[e.Item.ItemIndex].ToString();
				Response.Redirect("EditUser.aspx"+"?uid="+sUserID);
				return;
			}
			ImageButton ib = (ImageButton)e.CommandSource;
			string s = ib.ID;
			switch (ib.ID)
			{
				case "btnDelete": int userID = (int)((DataGrid)source).DataKeys[e.Item.ItemIndex];
					if (!DeleteUser(userID))
					{
						lblError.Text = Resource.ResourceManager["editUser_ErrorDelete"];
						return;
					};
					BindGrid();
					break;
				case "btnEdit": string sUserID = ((DataGrid)source).DataKeys[e.Item.ItemIndex].ToString();
					Response.Redirect("EditUser.aspx"+"?uid="+sUserID); break;
			}
		}

		private void grdUsers_ItemCreated(object sender, System.Web.UI.WebControls.DataGridItemEventArgs e)
		{
			if (e.Item.ItemType == ListItemType.Header)
			{
				string imgUrl = "";
				int sortOrder = SortOrder;

				if (sortOrder!=0)
				{
					imgUrl = (sortOrder>0)?"images/sup.gif":"images/sdown.gif";
					Label sep = new Label(); 
					sep.Width = 2; 
					e.Item.Cells[Math.Abs(sortOrder)].Controls.Add(sep);
					ImageButton ib = new ImageButton();
					ib.Height = 11; ib.Width = 11; ib.ImageUrl = imgUrl;
					e.Item.Cells[Math.Abs(sortOrder)].Controls.Add(ib);
				}
			}
		}

		private void btnNewUser_Click(object sender, System.EventArgs e)
		{
			Response.Redirect("EditUser.aspx");
		}

		#endregion

		private void btnExport_Click(object sender, System.EventArgs e)
		{
			this.EnableViewState = true;

			System.IO.StringWriter tw = new System.IO.StringWriter();
			System.Web.UI.HtmlTextWriter hw = new System.Web.UI.HtmlTextWriter(tw);
			SqlDataReader reader = null;

			try
			{
				reader = UsersData.SelectUsers(LoggedUser.UserRoleID, SortOrder);

				if (reader!=null)
				{
					gridCalls.DataSource = reader;
					gridCalls.DataKeyField = "UserID";
					
					gridCalls.AllowSorting=false;
					
					gridCalls.DataBind();
					for(int i=0;i<gridCalls.Columns.Count;i++)
					{
						
						gridCalls.Columns[i].SortExpression=null;
					}
				}
			}
			
			finally
			{
				if (reader!=null) reader.Close();
			}

			gridCalls.MasterTableView.ExportToExcel("Users");
		}

		
	}
}
