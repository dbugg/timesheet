using System;
using System.Collections;
using System.Text;

namespace Asa.Timesheet.Data
{
	public class AutoRepairData
	{
		private int _autoRepairID = -1;
		private string _autoRepairDescription = String.Empty;
		private int _autoServiceID = -1;
		private int _automobileID = -1;
		private string _autoServiceName="";
		private DateTime _dateAdded;

		/// <summary>
		/// Initialize an new empty Login object.
		/// </summary>
		public AutoRepairData()
		{
		}
		
		/// <summary>
		/// Initialize a new Login object with the given parameters.
		/// </summary>
		public AutoRepairData(int autoRepairID, string autoRepairDescription, int autoServiceID, int automobileID)
		{
			this._autoRepairID = autoRepairID;
			this._autoRepairDescription = autoRepairDescription;
			this._autoServiceID = autoServiceID;
			this._automobileID = automobileID;
		}	
		
	  
		public override string ToString()
		{
			StringBuilder builder = new StringBuilder();
			builder.Append("_autoRepairID: " + _autoRepairID.ToString()).Append("\r\n");
			builder.Append("_autoRepairDescription: " + _autoRepairDescription.ToString()).Append("\r\n");
			builder.Append("_autoServiceID: " + _autoServiceID.ToString()).Append("\r\n");
			builder.Append("_automobileID: " + _automobileID.ToString()).Append("\r\n");
			builder.Append("\r\n");
			return builder.ToString();
		}	
		public object GetByIndex(int nIndex)
		{
			switch(nIndex)
			{
				case 0:
					return _autoRepairID;
				case 1:
					return _autoRepairDescription;
				case 2:
					return _autoServiceID;
				case 3:
					return _automobileID;
			}
			return null;
		}
		#region Properties
	
		public string AutoServiceName
		{
			get { return _autoServiceName; }
			set { _autoServiceName = value; }
		}
		public DateTime DateAdded
		{
			get { return _dateAdded; }
			set { _dateAdded = value; }
		}
		
		public int AutoRepairID
		{
			get { return _autoRepairID; }
			set { _autoRepairID = value; }
		}
	
		public string AutoRepairDescription
		{
			get { return _autoRepairDescription; }
			set { _autoRepairDescription = value; }
		}
	
		public int AutoServiceID
		{
			get { return _autoServiceID; }
			set { _autoServiceID = value; }
		}
	
		public int AutomobileID
		{
			get { return _automobileID; }
			set { _automobileID = value; }
		}
	
		#endregion
	}
}


