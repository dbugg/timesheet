using System;
using System.Data;
using System.Data.SqlClient; 
using System.Collections;
using Microsoft.ApplicationBlocks.Data;

namespace Asa.Timesheet.Data
{
	public class OtherInfoDAL
	{
		/// <summary>
		/// Loads a OtherInfoData object from the database using the given OtherInfoID
		/// </summary>
		/// <param name="otherInfoID">The primary key value</param>
		/// <returns>A OtherInfoData object</returns>
		public static OtherInfoData Load(int otherInfoID)
		{
			SqlParameter[] parameterValues = new SqlParameter[] { new SqlParameter("@OtherInfoID", otherInfoID) };
			using (SqlDataReader reader = SqlHelper.ExecuteReader(CommonDAL.ConnectionString, CommandType.StoredProcedure, "OtherInfoSelProc", parameterValues))
			{
				if (reader.Read())
				{
					OtherInfoData result = new OtherInfoData();
					LoadFromReader(reader,result);
					return result;
				}
				else
					return null;
			}
		}

		/// <summary>
		/// Loads a OtherInfoData object from the database using the given where clause
		/// </summary>
		/// <param name="whereClause">The filter expression for the query</param>
		/// <returns>A OtherInfoData object</returns>
		public static OtherInfoData Load(string whereClause)
		{
			SqlParameter[] parameterValues = new SqlParameter[] { new SqlParameter("@WhereClause", whereClause) };
			using (SqlDataReader reader = SqlHelper.ExecuteReader(CommonDAL.ConnectionString, CommandType.StoredProcedure, "OtherInfoListProc", parameterValues))
			{
				if (reader.Read())
				{
					OtherInfoData result = new OtherInfoData();
					LoadFromReader(reader,result);
					return result;
				}
				else
					return null;
			}
		}
		public static int Save( OtherInfoData source)
		{
			if (source.OtherInfoID == -1)
				return Insert(source);
			else
				return Update(source);	
		}

		public static void Delete( OtherInfoData source)
		{
			Delete(source.OtherInfoID);
		}
		/// <summary>
		/// Loads a collection of OtherInfoData objects from the database.
		/// </summary>
		/// <returns>A collection containing all of the OtherInfoData objects in the database.</returns>
		public static OtherInfoVector LoadCollection()
		{
			SqlParameter[] parms = new SqlParameter[] {};
			return LoadCollection("OtherInfoListProc", parms);
		}
	  
		/// <summary>
		/// Saves a collection of OtherInfoData objects to the database.
		/// </summary>
		/// <returns>A collection containing all of the OtherInfoData objects in the database.</returns>
		public static  void  SaveCollection(OtherInfoVector col,string spName, SqlParameter[] parms)
		{
			OtherInfoVector colOld = LoadCollection(spName,parms);
			UpdateCollection(colOld,col);
		}
		public static  void  SaveCollection(OtherInfoVector col)
		{
			OtherInfoVector colOld = LoadCollection();
			UpdateCollection(colOld,col);
		}
		public static  void  SaveCollection(OtherInfoVector col, string whereClause )
		{
			OtherInfoVector colOld = LoadCollection(whereClause);
			UpdateCollection(colOld,col);
		}
		/// <summary>
		/// Loads a collection of OtherInfoData objects from the database.
		/// </summary>
		/// <returns>A collection containing all of the OtherInfoData objects in the database.</returns>
		public static OtherInfoVector LoadCollection(string whereClause)
		{
			SqlParameter[] parms = new SqlParameter[] { new SqlParameter("@WhereClause", whereClause) };
			return LoadCollection("OtherInfoListProc", parms);
		}

		/// <summary>
		/// Loads a collection of OtherInfoData objects from the database.
		/// </summary>
		/// <returns>A collection containing all of the OtherInfoData objects in the database.</returns>
		public static OtherInfoVector LoadCollection(string spName, SqlParameter[] parms)
		{
			OtherInfoVector result = new OtherInfoVector();
			using (SqlDataReader reader = SqlHelper.ExecuteReader(CommonDAL.ConnectionString, CommandType.StoredProcedure, spName, parms))
			{
				while (reader.Read())
				{
					OtherInfoData tmp = new OtherInfoData();
					LoadFromReader(reader, tmp);
					result.Add(tmp);
				}
			}
			return result;
		}
		/// <summary>
		/// Deletes a OtherInfoData object from the database.
		/// </summary>
		/// <param name="otherInfoID">The primary key value</param>
		public static void Delete(int otherInfoID)
		{
			SqlParameter[] parameterValues = new SqlParameter[] { new SqlParameter("@OtherInfoID", otherInfoID) };
			SqlHelper.ExecuteNonQuery(CommonDAL.ConnectionString, CommandType.StoredProcedure, "OtherInfoDelProc", parameterValues);
		}

		public static void LoadFromReader(SqlDataReader reader, OtherInfoData target)
		{
			if (reader != null && !reader.IsClosed)
			{
				//target.Key = reader.GetInt32(0);
				if (!reader.IsDBNull(0)) target.OtherInfoID = reader.GetInt32(0);
				if (!reader.IsDBNull(1)) target.OtherInfo = reader.GetString(1);
			}
		}
		
	  
		public static int Insert( OtherInfoData source)
		{
			SqlParameter[] parameterValues = GetInsertParameterValues(source);
			SqlHelper.ExecuteNonQuery(CommonDAL.ConnectionString, CommandType.StoredProcedure, "OtherInfoInsProc", parameterValues);
			source.OtherInfoID = (int) parameterValues[0].Value;
			return source.OtherInfoID;
		}

		public static int Update( OtherInfoData source)
		{
			SqlParameter[] parameterValues = GetUpdateParameterValues(source);
			SqlHelper.ExecuteNonQuery(CommonDAL.ConnectionString, CommandType.StoredProcedure,
				"OtherInfoUpdProc", parameterValues);
			return source.OtherInfoID;
		}
		private static void UpdateCollection(OtherInfoVector colOld,OtherInfoVector col)
		{
			// Delete all old items
			foreach( OtherInfoData itemOld in colOld)
			{
				if(!ExistsKey(itemOld.OtherInfoID, col))
					Delete(itemOld.OtherInfoID);
			}
			foreach( OtherInfoData item in col)
			{
				if(item.OtherInfoID <= -1)
					Insert(item);
				else
					Update(item);
			}
		}
		private static bool ExistsKey(int Key, OtherInfoVector col)
		{
			foreach( OtherInfoData item in col)
				if(item.OtherInfoID==Key)
					return true;
			return false;
		}

		private static SqlParameter GetSqlParameter(string name, ParameterDirection direction, object value)
		{
			SqlParameter p = new SqlParameter(name, value);
			p.Direction = direction;
			return p;
		}

		private static SqlParameter[] GetInsertParameterValues( OtherInfoData source)
		{
			SqlParameter[] prams = new SqlParameter[2];
			prams[0] = GetSqlParameter("@OtherInfoID", ParameterDirection.Output, source.OtherInfoID);
			prams[1] = GetSqlParameter("@OtherInfo", ParameterDirection.Input, source.OtherInfo);
			return prams;
		}

		private static SqlParameter[] GetUpdateParameterValues( OtherInfoData source)
		{
			return new SqlParameter[] {
										  new SqlParameter("@OtherInfoID", source.OtherInfoID),
										  new SqlParameter("@OtherInfo", source.OtherInfo)
									  };
		}
	}
}   

