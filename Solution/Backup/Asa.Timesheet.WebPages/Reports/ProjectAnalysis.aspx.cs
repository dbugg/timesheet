using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Data.SqlClient;
using log4net;
using Asa.Timesheet.Data;
using Asa.Timesheet.Data.Entities;
using Asa.Timesheet.Data.Util;
using GrapeCity.ActiveReports.Export.Pdf.Section;
using GrapeCity.ActiveReports.Export.Excel.Section;

namespace Asa.Timesheet.WebPages.Reports
{
	/// <summary>
	/// Summary description for WorkTimes.
	/// </summary>
	public class ProjectAnalysis : TimesheetPageBase
	{
		private static readonly ILog log = LogManager.GetLogger(typeof(WorkTimes));
	
		#region Web controls

		protected System.Web.UI.WebControls.Label lblTitle;
		protected System.Web.UI.WebControls.Label lblUser;
		protected System.Web.UI.WebControls.Label lblObekt;
		protected System.Web.UI.WebControls.Label Label5;
		protected System.Web.UI.WebControls.PlaceHolder menuHolder;
		protected System.Web.UI.WebControls.DropDownList ddlPeriodType;
		protected System.Web.UI.WebControls.Label lblStartDay;
		protected System.Web.UI.WebControls.Label lblEndDay;
		protected System.Web.UI.WebControls.Calendar calStartDate;
		protected System.Web.UI.WebControls.Calendar calEndDate;
		protected System.Web.UI.WebControls.Label lblSelStartDate;
		protected System.Web.UI.WebControls.Label lblSelEndDate;
		protected System.Web.UI.WebControls.Label lblError;
		protected System.Web.UI.WebControls.Label lblInfo;
		protected System.Web.UI.WebControls.ImageButton ibGenerate;
		protected System.Web.UI.WebControls.ImageButton ibPdfExport;
		protected System.Web.UI.WebControls.ImageButton ibXlsExport;
		protected Asa.Timesheet.WebPages.UserControls.PageHeader header;
		protected System.Web.UI.WebControls.CheckBox ckDates;
		protected System.Web.UI.WebControls.DataGrid grdReport;
		protected System.Web.UI.WebControls.Label lblReportTitle;
		protected System.Web.UI.WebControls.Label lblCreatedBy;
		protected System.Web.UI.HtmlControls.HtmlTable tblFooter;
		protected System.Web.UI.WebControls.Label lblCreatedByLbl;
		protected System.Web.UI.WebControls.Label Label2;
		protected System.Web.UI.WebControls.DataGrid grdReportPhases;
		protected System.Web.UI.WebControls.DropDownList ddlType;
		protected System.Web.UI.WebControls.Label Label4;
		protected System.Web.UI.WebControls.Label lbgrdReport;
		protected System.Web.UI.HtmlControls.HtmlTable tblHeader;
		protected System.Web.UI.WebControls.Label Label6;
		protected System.Web.UI.WebControls.Label lbManHour1;
		protected System.Web.UI.WebControls.Label lbManHour2;
		protected System.Web.UI.WebControls.Label lbMH1;
		protected System.Web.UI.WebControls.Label lbMH2;
		protected System.Web.UI.HtmlControls.HtmlTable tblLegend;
		protected System.Web.UI.WebControls.DropDownList ddlProjectsStatus;
		protected System.Web.UI.WebControls.Label Label7;
		protected System.Web.UI.WebControls.DropDownList ddlBuildingTypes;
		protected System.Web.UI.WebControls.CheckBox ckClient;
		protected System.Web.UI.WebControls.DataGrid grdWorkTime;
		protected System.Web.UI.WebControls.Label lbWorkTime;
		protected System.Web.UI.WebControls.Label Label9;
		protected System.Web.UI.WebControls.Label lbFeeAccount;
		protected System.Web.UI.WebControls.CheckBox cbFeeAccount;
		protected System.Web.UI.WebControls.Label lbFinPokazateli;
		protected System.Web.UI.WebControls.CheckBox cbFinPokazateli;
		protected System.Web.UI.WebControls.DataGrid grdFeeAccount;
		protected System.Web.UI.WebControls.DropDownList ddlProjects;
		protected System.Web.UI.WebControls.Label lbFeeAccountTitle;
		protected System.Web.UI.WebControls.Label lbPayments;
		protected System.Web.UI.WebControls.Label lbFinancePokazateliTitle;
		protected System.Web.UI.WebControls.DataGrid grdFinancePokazateli;
		protected System.Web.UI.WebControls.Label lbReport1;
		protected System.Web.UI.WebControls.DataGrid grdReport1;
		protected System.Web.UI.WebControls.Label Label10;
		protected System.Web.UI.WebControls.DropDownList ddlCompany;
		protected System.Web.UI.WebControls.Label lblNoDataFound;
		private DataView _dvFinPokazateli;
		protected System.Web.UI.WebControls.Label lbBGN;
		protected System.Web.UI.WebControls.Label lbEUR;
		protected System.Web.UI.WebControls.DataGrid grdReportBGN;
		private DataView _dvBGN;
		protected System.Web.UI.WebControls.DataGrid grd;
		protected System.Web.UI.WebControls.DataGrid grdAuthors;
		protected System.Web.UI.WebControls.Label lbAuth;
		protected System.Web.UI.WebControls.Label lbAuth1;
		protected System.Web.UI.WebControls.Label Label11;
		protected System.Web.UI.WebControls.CheckBox ckAuthors;
		protected System.Web.UI.WebControls.Label lbTotal;
		private DataView _dvWorkTime;
		#endregion

		#region Enums

		private enum FeeAccountGridColomns
		{
			SubprojectID = 0,
			Projectname,
			ClientName,
			Area,
			EURForArea,
			EUR,
			Amount,
			Payd,
			AmountLeft,
			NotPayd,
			ORDERCLAUSE,
		}


		#endregion

		#region Page load

        private void Page_Load(object sender, System.EventArgs e)
        {

            if (!this.LoggedUser.HasPaymentRights) ErrorRedirect(Resource.ResourceManager["noRightsAlert"]);
            tblLegend.Visible = false;
            if (!this.IsPostBack)
            {
                header.PageTitle = Resource.ResourceManager["ProjectAnalysis_PageTitle"];
                header.UserName = this.LoggedUser.UserName;


                UIHelpers.LoadProjectStatus(ddlProjectsStatus, (int)ProjectsData.ProjectsByStatus.Active);
                UIHelpers.LoadBuildingTypes("", ddlBuildingTypes);

                //
                //								calStartDate.SelectedDate = calStartDate.VisibleDate = new DateTime(DateTime.Today.Year, DateTime.Today.Month, 1);
                //								lblSelStartDate.Text = "("+Resource.ResourceManager["reports_SelectDate"]+")";
                //								calEndDate.SelectedDate = calEndDate.VisibleDate = DateTime.Today;
                //								lblSelEndDate.Text = "("+Resource.ResourceManager["reports_SelectDate"]+")";
                ckDates.Checked = false;
                calEndDate.SelectedDate = calStartDate.SelectedDate = DateTime.Today;

                LoadProjects();
            }
            else
                UIHelpers.LoadBuildingTypes(ddlBuildingTypes.SelectedValue, ddlBuildingTypes, (ProjectsData.ProjectsByStatus)int.Parse(ddlProjectsStatus.SelectedValue));



            UIHelpers.CreateMenuReports(menuHolder, LoggedUser);

        }
        private DataSet LoadReportData()
        {
            DataSet ds = null;

            try
            {
                int nID = int.Parse(ddlProjects.SelectedValue);
                DateTime dtStart = Constants.DateMax;
                DateTime dtEnd = Constants.DateMax;
                if (!ckDates.Checked)
                {
                    dtStart = calStartDate.SelectedDate;
                    dtEnd = calEndDate.SelectedDate;
                }
                ds = ReportsData.SelectWorkTimesReport2(nID, dtStart, dtEnd, ddlProjectsStatus.SelectedIndex, UIHelpers.ToInt(ddlBuildingTypes.SelectedValue));
                grdWorkTime.DataSource = ds.Tables[0];
                _dvWorkTime = ds.Tables[0].DefaultView;
                grdWorkTime.DataBind();
                grdWorkTime.Visible = lbWorkTime.Visible = ds.Tables[0].Rows.Count > 0;

            }
            catch (Exception ex)
            {
                log.Error(ex);
                return null;
            }

            return ds;
        }
		#endregion

		#region Web Form Designer generated code
        override protected void OnInit(EventArgs e)
        {
            //
            // CODEGEN: This call is required by the ASP.NET Web Form Designer.
            //
            InitializeComponent();
            base.OnInit(e);
        }
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
        private void InitializeComponent()
        {
            this.ckDates.CheckedChanged += new System.EventHandler(this.ckDates_CheckedChanged);
            this.calStartDate.SelectionChanged += new System.EventHandler(this.calStartDate_SelectionChanged);
            this.calEndDate.SelectionChanged += new System.EventHandler(this.calEndDate_SelectionChanged);
            this.ddlProjectsStatus.SelectedIndexChanged += new System.EventHandler(this.ddlProjectsStatus_SelectedIndexChanged);
            this.ddlBuildingTypes.SelectedIndexChanged += new System.EventHandler(this.ddlBuildingTypes_SelectedIndexChanged);
            this.ibGenerate.Click += new System.Web.UI.ImageClickEventHandler(this.ibGenerate_Click);
            this.ibPdfExport.Click += new System.Web.UI.ImageClickEventHandler(this.ibPdfExport_Click);
            this.ibXlsExport.Click += new System.Web.UI.ImageClickEventHandler(this.ibXlsExport_Click);
            this.grdReport.ItemDataBound += new System.Web.UI.WebControls.DataGridItemEventHandler(this.grdReport_ItemDataBound);
            this.grdReport1.ItemDataBound += new System.Web.UI.WebControls.DataGridItemEventHandler(this.grdReport_ItemDataBound);
            this.grdReportPhases.ItemDataBound += new System.Web.UI.WebControls.DataGridItemEventHandler(this.grdReportPhases_ItemDataBound);
            this.grdWorkTime.ItemDataBound += new System.Web.UI.WebControls.DataGridItemEventHandler(this.grdWorkTime_ItemDataBound);
            this.grdFeeAccount.ItemDataBound += new System.Web.UI.WebControls.DataGridItemEventHandler(this.grdFeeAccount_ItemDataBound);
            this.grdFinancePokazateli.ItemDataBound += new System.Web.UI.WebControls.DataGridItemEventHandler(this.grdFinancePokazateli_ItemDataBound);
            this.grdReportBGN.ItemDataBound += new System.Web.UI.WebControls.DataGridItemEventHandler(this.grdReportBGN_ItemDataBound);
            this.grdAuthors.ItemDataBound += new System.Web.UI.WebControls.DataGridItemEventHandler(this.grdAuthors_ItemDataBound);
            this.grd.ItemDataBound += new System.Web.UI.WebControls.DataGridItemEventHandler(this.grd_ItemDataBound);
            this.Load += new System.EventHandler(this.Page_Load);

        }
		#endregion

		#region Menu

//		private void CreateMenu()
//		{
//			UserControls.MenuTable menu = new UserControls.MenuTable();
//			menu.ID = "MenuTable";
//
//			// 'Links' group
//			ArrayList menuItems = new ArrayList();
//
//			menuItems.Add(new MenuItemInfo(Resource.ResourceManager["menuItem_Hours"], "../Hours.aspx", true));
//			menuItems.Add(new MenuItemInfo(Resource.ResourceManager["menuItem_Projects"], "../Projects.aspx"));
//			if (LoggedUser.IsLeader || LoggedUser.IsAssistant) 
//			{
//				menuItems.Add(new MenuItemInfo(Resource.ResourceManager["menuItem_Clients"], "../Clients.aspx"));
//				menuItems.Add(new MenuItemInfo(Resource.ResourceManager["menuItem_Subcontracters"], "../Subcontracters.aspx"));
//				if (LoggedUser.IsLeader)
//					menuItems.Add(new MenuItemInfo(Resource.ResourceManager["menuItem_Users"], "../Users.aspx"));	
//			}
//			menuItems.Add(new MenuItemInfo(Resource.ResourceManager["menuItem_Emails"], "../Emails.aspx"));
//
//			menu.AddMenuGroup(Resource.ResourceManager["menuHeader_Links"], 20, menuItems);
//			
//			// 'New' group
//			menuItems = new ArrayList();
//			menuItems.Add(new MenuItemInfo(Resource.ResourceManager["menuItem_NewProject"], "../EditProject.aspx"));
//			if (LoggedUser.IsLeader || LoggedUser.IsAssistant)
//			{
//				menuItems.Add(new MenuItemInfo(Resource.ResourceManager["menuItem_NewClient"], "../EditClient.aspx"));
//				if (LoggedUser.IsLeader)
//					menuItems.Add(new MenuItemInfo(Resource.ResourceManager["menuItem_NewUser"], "../EditUser.aspx"));
//			}
//			menuItems.Add(new MenuItemInfo(Resource.ResourceManager["menuItem_NewEmail"], "../EditEmail.aspx"));
//			menuItems.Add(new MenuItemInfo(Resource.ResourceManager["newcontracter_PageTitle"], "../EditSubContracter.aspx"));
//
//
//			menu.AddMenuGroup(Resource.ResourceManager["menuHeader_New"], 30, menuItems);
//
//			// "Reports' group
//			menuItems = new ArrayList();
//			//menuItems.Add(new MenuItemInfo(Resource.ResourceManager["menuItem_WorkTimeReport"], "#"));
//			menuItems.Add(new MenuItemInfo(Resource.ResourceManager["menuItem_MinutesReport"], "Minutes.aspx"));
//			if (LoggedUser.IsLeader || LoggedUser.IsAssistant)
//				menuItems.Add(new MenuItemInfo(Resource.ResourceManager["menuItem_MailReport"], "../MailForm.aspx"));
//			
//			if (LoggedUser.IsLeader)
//			{
//				menuItems.Add(new MenuItemInfo(Resource.ResourceManager["menuItem_NotEnteredWorkTime"], "EmptyHours.aspx"));
//				menuItems.Add(new MenuItemInfo(Resource.ResourceManager["menuItem_Calendar"], "Calendar.aspx"));
//			}
//			menu.AddMenuGroup(Resource.ResourceManager["menuHeader_Reports"], 30, menuItems);
//
//			
//
//			menuItems = new ArrayList();
//			menuItems.Add(new MenuItemInfo(Resource.ResourceManager["menuItem_Help"], SessionManager.GetReportsHelpLink(),true,Pages.WorkTimes));
//			menu.AddMenuGroup("", 10, menuItems);
//
//
//			menuHolder.Controls.Add(menu);
//		}

		#endregion

        protected string GetImage(object Done)
        {
            if (Done == null || Done == System.DBNull.Value)
                return "~/images/delete.png";
            int nDone = (int)Done;
            if (nDone == 1)
                return "~/images/done.gif";
            return "~/images/delete.png";
        }
        private bool LoadProjects()
        {
            return UIHelpers.LoadProjects(ddlProjects, ddlProjectsStatus, ddlBuildingTypes);
            //			SqlDataReader reader = null;
            //			
            //			try
            //			{
            //				ddlProjects.Items.Clear();
            //				ddlProjects.Items.Add(new ListItem("<"+Resource.ResourceManager["reports_ddlAllProjects"]+">","0"));
            //				
            //				reader = ProjectsData.SelectProjectNamesClear(this.ProjectsStatus, int.Parse(ddlBuildingTypes.SelectedValue));
            //			
            //				while (reader.Read())
            //				{
            //					int projectID = reader.GetInt32(0);
            //					ddlProjects.Items.Add(new ListItem(reader.GetString(1), projectID.ToString()));
            //					
            //				}
            //			}
            //			catch (Exception ex)
            //			{
            //				log.Error(ex);
            //				return false;
            //			}
            //
            //			finally
            //			{
            //				if (reader!=null) reader.Close();
            //			}
            //			
            //			
            //			return true;
        }
        private void CreateAuthors()
        {
            lbAuth.Visible = lbAuth1.Visible = grdAuthors.Visible = grd.Visible = true;
            BindGridAuthors();
        }
        private void BindGridAuthors()
        {
            lbTotal.Text = "";
            //	grdReport.DataSource = null;
            //	grdReport.DataBind();

            int projectID = int.Parse(ddlProjects.SelectedValue);

            if (projectID == 0)
            {
                lblInfo.Text = Resource.ResourceManager["reports_Minutes_ErrorSelectProject"];
                return;
            }

            /*
            if (!SetArea(projectID)) 
            {
                lblError.Text = Resource.ResourceManager["reports_Minutes_ErrorLoadGrid"];
                return; 
            }
            */





            int activityID = int.Parse(System.Configuration.ConfigurationManager.AppSettings["AN"]);


            DataSet ds = UIHelpers.LoadWorkTimes(projectID, activityID, 0, "0");

            if (ds == null)
            {
                lblError.Text = Resource.ResourceManager["reports_Minutes_ErrorLoadGrid"];
                return;
            }


            grd.DataSource = ReportsData.ExecuteReportsAuthorsAnalysis(projectID);
            grd.DataBind();
            if (grd.Items.Count == 0)
            {
                grd.Visible = false;
            }
            else
                grd.Visible = true;

            if (ds.Tables[0].Rows.Count == 0)
            {
                return;
            }


            DataTable source;

            try
            {
                decimal totalmin = 0;
                source = UIHelpers.CreateGridSource(ds.Tables[0], out totalmin);

                {
                    totalmin = totalmin / 60;
                    SqlDataReader reader = ProjectsData.SelectProject(projectID);
                    decimal dEur = 0;
                    if (reader.Read())
                    {
                        if (!reader.IsDBNull(28))
                            dEur = reader.GetDecimal(28);
                    }
                    lbTotal.Text = string.Format(Resource.ResourceManager["reports_AN_total"], UIHelpers.FormatDecimal2(totalmin), UIHelpers.FormatDecimal2(dEur), UIHelpers.FormatDecimal2(totalmin * dEur));
                }
            }
            catch (Exception ex)
            {
                log.Error(ex);
                lblError.Text = Resource.ResourceManager["reports_Minutes_ErrorLoadGrid"];
                //throw ex;
                return;
            }

            grdAuthors.DataSource = source;
            grdAuthors.DataBind();

        }

        private void ibGenerate_Click(object sender, System.Web.UI.ImageClickEventArgs e)
        {
            lbTotal.Text = "";
            if (cbFeeAccount.Checked)
            {
                FeeAccountReport();
                if (grdFeeAccount.Items.Count > 0)
                    lbFeeAccountTitle.Visible = true;
                else
                    lbFeeAccountTitle.Visible = false;
                //return;
            }
            else
                lbFeeAccountTitle.Visible = false;
            if (ckAuthors.Checked)
            {
                CreateAuthors();
            }
            else
                lbAuth.Visible = lbAuth1.Visible = grdAuthors.Visible = grd.Visible = false;
            if (cbFinPokazateli.Checked)
            {
                CreataFinancePokazateli();
                if (grdFinancePokazateli.Items.Count > 0)
                    lbFinancePokazateliTitle.Visible = true;
                else
                    lbFinancePokazateliTitle.Visible = false;
                //return;
            }
            else
                lbFinancePokazateliTitle.Visible = false;
            //			if(ddlProjects.SelectedIndex<=0)
            //			{
            //				lblError.Text=Resource.ResourceManager["reports_SelectProject"];
            //				return;
            //			}
            int nID = int.Parse(ddlProjects.SelectedValue);
            if (nID > 0 && !ckClient.Checked)
            {
                float ratio1, ratio2;
                ReportsData.ExecuteReportsWorkManHour(nID, out ratio1, out ratio2);
                lbManHour1.Visible = lbManHour2.Visible = lbMH1.Visible = lbMH2.Visible = true;
                lbMH1.Text = UIHelpers.FormatDecimal2((decimal)ratio1);
                lbMH2.Text = UIHelpers.FormatDecimal2((decimal)ratio2);
                tblLegend.Visible = true;
            }
            else
                lbManHour1.Visible = lbManHour2.Visible = lbMH1.Visible = lbMH2.Visible = false;
            lblCreatedBy.Text = this.LoggedUser.UserName;
            //Label5.Visible=true;
            lblReportTitle.Visible = true;
            //lblObekt.Visible=true;
            lblCreatedBy.Visible = true;
            lblCreatedByLbl.Visible = true;
            Label6.Visible = true;
            int nType = int.Parse(ddlType.SelectedValue);

            string projectName, projectCode, administrativeName, add;
            decimal area = 0;
            int clientID = 0;
            bool phases; bool isPUP;
            ProjectsData.SelectProject(nID, out projectName, out  projectCode, out administrativeName, out area, out add,
                out clientID, out phases, out isPUP);
            if (!phases)
            {
                grdReport.Columns[3].HeaderText = Resource.ResourceManager["ProjectAnalysisgrdReportClientsHeaderText"];
            }
            //lblObekt.Text=administrativeName;
            DateTime dtStart = Constants.DateMax;
            DateTime dtEnd = Constants.DateMax;
            if (!ckDates.Checked)
            {
                dtStart = calStartDate.SelectedDate;
                dtEnd = calEndDate.SelectedDate;
            }

            DataView dv;
            if (nID > 0)
            {
                dv = ReportsData.ExecuteReportsProjectAnalysis(nID, dtStart, dtEnd, phases, nType, ddlProjectsStatus.SelectedIndex, UIHelpers.ToInt(ddlBuildingTypes.SelectedValue));

                dv = new DataView(AddPaydAndNotPayd(dv.Table));
                #region AddPaydAndNopayd
                //			if(dv.Table.Rows.Count!=0)
                //			{
                //				DataTable dt = dv.Table;
                //				DataRow drPayd = dt.NewRow();
                //				drPayd.ItemArray = dt.Rows[dt.Rows.Count-1].ItemArray;
                //				DataRow drNotPayd = dt.NewRow();
                //				drNotPayd.ItemArray = dt.Rows[dt.Rows.Count-1].ItemArray;
                //				DataRow drAll = dt.NewRow();
                //				drAll.ItemArray = dt.Rows[dt.Rows.Count-1].ItemArray;
                //				decimal Dpayd =0;
                //				decimal Dnotpayd = 0;
                //				foreach(DataRow dr in dt.Rows)
                //				{
                //					string paymentid = dr["PaymentID"].ToString();
                //					if(paymentid.Length != 0)
                //					{
                //						decimal am = UIHelpers.ParseDecimal(dr["Amount"].ToString());
                //						if(dr["DoneString"].ToString() == Resource.ResourceManager["Reports_Yes"])
                //							Dpayd +=am;
                //						else
                //							Dnotpayd +=am;
                //					}
                //				}
                //				dt.Rows.RemoveAt(dt.Rows.Count-1);
                //				decimal fixx = decimal.Parse( System.Configuration.ConfigurationManager.AppSettings["fixedBGNEUR"]);
                //				if(Dpayd != 0)
                //				{
                //					drPayd["Amount"] = Dpayd.ToString();
                //					Dpayd *= fixx;
                //					drPayd["AmountBGN"] = Dpayd.ToString();
                //					drPayd["projectname"] = Resource.ResourceManager["grdClientAnalisisPaid"];
                //					dt.Rows.Add(drPayd);
                //				}
                //				if(Dnotpayd != 0)
                //				{
                //					drNotPayd["Amount"] = Dnotpayd.ToString();
                //					Dnotpayd *= fixx;
                //					drNotPayd["AmountBGN"] = Dnotpayd.ToString();
                //					drNotPayd["projectname"] = Resource.ResourceManager["grdClientAnalisisNoPaid"];
                //					dt.Rows.Add(drNotPayd);
                //				}	
                //				dt.Rows.Add(drAll);
                //				dv= new DataView(dt);
                //			}
                #endregion

                grdReport.DataSource = dv;
                grdReport.DataBind();
                if (grdReport.Items.Count == 0)
                {
                    grdReport.Visible = false;
                }
                else
                    grdReport.Visible = true;
                grdReport1.Visible = lbReport1.Visible = false;
            }
            else
            {
                dv = ReportsData.ExecuteReportsProjectAnalysis(nID, dtStart, dtEnd, false, nType, ddlProjectsStatus.SelectedIndex, UIHelpers.ToInt(ddlBuildingTypes.SelectedValue));
                dv = new DataView(AddPaydAndNotPayd(dv.Table));
                grdReport.DataSource = dv;
                grdReport.DataBind();
                if (grdReport.Items.Count == 0)
                {
                    grdReport.Visible = false;
                }
                else
                    grdReport.Visible = true;

                dv = ReportsData.ExecuteReportsProjectAnalysis(nID, dtStart, dtEnd, true, nType, ddlProjectsStatus.SelectedIndex, UIHelpers.ToInt(ddlBuildingTypes.SelectedValue));
                dv = new DataView(AddPaydAndNotPayd(dv.Table));
                grdReport1.DataSource = dv;
                grdReport1.DataBind();
                if (grdReport1.Items.Count == 0)
                {
                    grdReport1.Visible = lbReport1.Visible = false;
                }
                else
                    grdReport.Visible = lbReport1.Visible = true;
            }


            grdReportPhases.DataSource = ReportsData.ExecuteReportsSubcontracterProjectAnalysis(nID, dtStart, dtEnd, nType, LoggedUser.HasPaymentRights, ddlProjectsStatus.SelectedIndex, UIHelpers.ToInt(ddlBuildingTypes.SelectedValue));
            grdReportPhases.DataBind();
            if (grdReportPhases.Items.Count == 0 || ckClient.Checked)
            {
                grdReportPhases.Visible = false;
                lbgrdReport.Visible = false;
            }
            else
                lbgrdReport.Visible = grdReportPhases.Visible = true;

            if (grdReportPhases.Items.Count == 0 && grdReport.Items.Count == 0)
            {
                lblNoDataFound.Visible = true;
                lblNoDataFound.Text = Resource.ResourceManager["reports_WorkTimes_NoDataMatch"];
            }
            if (!ckClient.Checked)
                LoadReportData();
        }
        private DataTable AddPaydAndNotPayd(DataTable dt)
        {
            if (dt.Rows.Count != 0)
            {
                DataRow drPayd = dt.NewRow();
                drPayd.ItemArray = dt.Rows[dt.Rows.Count - 1].ItemArray;
                DataRow drNotPayd = dt.NewRow();
                drNotPayd.ItemArray = dt.Rows[dt.Rows.Count - 1].ItemArray;
                DataRow drAll = dt.NewRow();
                drAll.ItemArray = dt.Rows[dt.Rows.Count - 1].ItemArray;
                decimal Dpayd = 0;
                decimal Dnotpayd = 0;
                foreach (DataRow dr in dt.Rows)
                {
                    string paymentid = dr["PaymentID"].ToString();
                    if (paymentid.Length != 0)
                    {
                        decimal am = UIHelpers.ParseDecimal(dr["Amount"].ToString());
                        if (dr["DoneString"].ToString() == Resource.ResourceManager["Reports_Yes"])
                            Dpayd += am;
                        else
                            Dnotpayd += am;
                    }
                }
                dt.Rows.RemoveAt(dt.Rows.Count - 1);
                decimal fixx = decimal.Parse(System.Configuration.ConfigurationManager.AppSettings["fixedBGNEUR"]);
                if (Dpayd != 0)
                {
                    drPayd["Amount"] = Dpayd.ToString();
                    Dpayd *= fixx;
                    drPayd["AmountBGN"] = Dpayd.ToString();
                    drPayd["projectname"] = Resource.ResourceManager["grdClientAnalisisPaid"];
                    dt.Rows.Add(drPayd);
                }
                if (Dnotpayd != 0)
                {
                    drNotPayd["Amount"] = Dnotpayd.ToString();
                    Dnotpayd *= fixx;
                    drNotPayd["AmountBGN"] = Dnotpayd.ToString();
                    drNotPayd["projectname"] = Resource.ResourceManager["grdClientAnalisisNoPaid"];
                    dt.Rows.Add(drNotPayd);
                }
                dt.Rows.Add(drAll);
                //dv= new DataView(dt);
            }
            return dt;
        }
		//tzveti
        private void ckDates_CheckedChanged(object sender, System.EventArgs e)
        {
            bool bEnable = !ckDates.Checked;
            calStartDate.Enabled = calEndDate.Enabled = bEnable;

            //			if(bEnable)
            //			{
            //				lblSelStartDate.Text = String.Concat("("+calStartDate.SelectedDate.ToString("d"), ")");
            //				
            //				lblSelEndDate.Text = String.Concat("("+calEndDate.SelectedDate.ToString("d"), ")");
            //			}
            //			else
            //			{
            //				lblSelStartDate.Text = lblSelEndDate.Text = String.Concat("(", Resource.ResourceManager["reports_SelectDate"], ")");
            //			}

        }

        private void calStartDate_SelectionChanged(object sender, System.EventArgs e)
        {
            lblSelStartDate.Text = String.Concat("(", calStartDate.SelectedDates[0].ToString("dd.MM.yyyy"), ")");
            if (calStartDate.SelectedDates.Count > 1)
            {
                calEndDate.SelectedDate = calStartDate.SelectedDates[calStartDate.SelectedDates.Count - 1];
                calEndDate.VisibleDate = calEndDate.SelectedDate;

                calStartDate.SelectedDate = calStartDate.SelectedDates[0];
                lblSelEndDate.Text = String.Concat("(", calEndDate.SelectedDate.ToString("dd.MM.yyyy"), ")"); ;

            }

        }

        private void calEndDate_SelectionChanged(object sender, System.EventArgs e)
        {
            lblSelEndDate.Text = String.Concat("(", calEndDate.SelectedDates[0].ToString("dd.MM.yyyy"), ")");

        }


        private void grdReport_ItemDataBound(object sender, DataGridItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Item
                || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                if (e.Item.Cells[1].Text == "&nbsp;")
                {
                    e.Item.CssClass = "ReportsTotal";
                    //e.Item.Cells[4].Text="";
                    //e.Item.Cells[5].Text="";
                    e.Item.Cells[3].Text = "";
                    e.Item.Cells[6].Text = "";
                    e.Item.Cells[7].Text = "";
                    e.Item.Cells[8].Text = "";
                }
                else if (e.Item.Cells[0].Text == "&nbsp;")
                {
                    e.Item.CssClass = "ReportsSubTotal";
                    //e.Item.Cells[4].Text="";
                    e.Item.Cells[3].Text = "";
                    //e.Item.Cells[5].Text="";
                    e.Item.Cells[6].Text = "";
                    e.Item.Cells[7].Text = "";
                    e.Item.Cells[8].Text = e.Item.Cells[9].Text + "%";
                }

            }
        }

        private void grdReportPhases_ItemDataBound(object sender, System.Web.UI.WebControls.DataGridItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Item
                || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                if (e.Item.Cells[1].Text == "&nbsp;")
                {
                    e.Item.CssClass = "ReportsTotal";
                    e.Item.Cells[4].Text = "";
                    //e.Item.Cells[6].Text="";
                    e.Item.Cells[7].Text = "";
                    e.Item.Cells[8].Text = "";
                    e.Item.Cells[9].Text = "";

                }
                else if (e.Item.Cells[0].Text == "&nbsp;")
                {
                    e.Item.CssClass = "ReportsSubTotal";
                    //e.Item.Cells[6].Text="";
                    e.Item.Cells[4].Text = "";
                    e.Item.Cells[7].Text = "";
                    e.Item.Cells[8].Text = "";
                    e.Item.Cells[9].Text = e.Item.Cells[10].Text + "%";
                }

            }
        }

        private void ibPdfExport_Click(object sender, System.Web.UI.ImageClickEventArgs e)
        {
            //			if(cbFeeAccount.Checked)
            //			{
            //				FeeAccountReportPDF();
            //				return;
            //			}
            PdfExport pdf = new PdfExport();
            string projectName, projectCode, administrativeName, add;
            decimal area = 0;
            int clientID = 0;
            bool phases; bool isPUP;
            ProjectsData.SelectProject(int.Parse(ddlProjects.SelectedValue), out projectName, out  projectCode, out administrativeName, out area, out add,
                out clientID, out phases, out isPUP);
            pdf.NeverEmbedFonts = "";

            int nID = int.Parse(ddlProjects.SelectedValue);
            string sh1 = "";
            string sh2 = "";
            if (nID > 0)
            {
                float ratio1, ratio2;
                ReportsData.ExecuteReportsWorkManHour(nID, out ratio1, out ratio2);

                sh1 = UIHelpers.FormatDecimal2((decimal)ratio1);
                sh2 = UIHelpers.FormatDecimal2((decimal)ratio2);
            }
            DateTime dtStart = Constants.DateMax;
            DateTime dtEnd = Constants.DateMax;
            if (!ckDates.Checked)
            {
                dtStart = calStartDate.SelectedDate;
                dtEnd = calEndDate.SelectedDate;
            }
            DataView dv1All = new DataView();
            DataView dv1 = new DataView();
            if (int.Parse(ddlProjects.SelectedValue) > 0)
            {
                dv1 = ReportsData.ExecuteReportsProjectAnalysis(int.Parse(ddlProjects.SelectedValue), dtStart, dtEnd, phases, int.Parse(ddlType.SelectedValue), ddlProjectsStatus.SelectedIndex, UIHelpers.ToInt(ddlBuildingTypes.SelectedValue));
                dv1.RowFilter = "PaymentID is not null";


            }
            else
            {
                dv1 = ReportsData.ExecuteReportsProjectAnalysis(int.Parse(ddlProjects.SelectedValue), dtStart, dtEnd, false, int.Parse(ddlType.SelectedValue), ddlProjectsStatus.SelectedIndex, UIHelpers.ToInt(ddlBuildingTypes.SelectedValue));
                dv1.RowFilter = "PaymentID is not null";
                dv1All = ReportsData.ExecuteReportsProjectAnalysis(int.Parse(ddlProjects.SelectedValue), dtStart, dtEnd, true, int.Parse(ddlType.SelectedValue), ddlProjectsStatus.SelectedIndex, UIHelpers.ToInt(ddlBuildingTypes.SelectedValue));
                dv1All.RowFilter = "PaymentID is not null";
            }

            DataView dv2 = ReportsData.ExecuteReportsSubcontracterProjectAnalysis(int.Parse(ddlProjects.SelectedValue), dtStart, dtEnd, int.Parse(ddlType.SelectedValue), LoggedUser.HasPaymentRights, ddlProjectsStatus.SelectedIndex, UIHelpers.ToInt(ddlBuildingTypes.SelectedValue));
            dv2.RowFilter = "PaymentID is not null";
            if (cbFinPokazateli.Checked)
                CreataFinancePokazateli();
            if (!ckClient.Checked)
                LoadReportData();
            GrapeCity.ActiveReports.SectionReport report = new ProjectRpt(true, lblReportTitle.Text + " " + projectName, ddlType.SelectedItem.Text, sh1, sh2, dv1, dv2, ckClient.Checked, !phases, dv1All, cbFinPokazateli.Checked, _dvFinPokazateli, LoggedUser.FullName, _dvBGN, _dvWorkTime);
            report.PageSettings.Orientation = GrapeCity.ActiveReports.Document.Section.PageOrientation.Landscape;




            report.Run();

            if (ckAuthors.Checked)
            {

                //notes: get projectID
                int projectID = int.Parse(ddlProjects.SelectedValue);
                //notes: check if it's not selected project
                if (projectID == 0)
                {
                    lblInfo.Text = Resource.ResourceManager["reports_Minutes_ErrorSelectProject"];
                    return;
                }

                string clientName = string.Empty;
                string userName = string.Empty;
                //get private class HeaderInfo
                //set view for second subreport
                DataView dvAuthorsAnalysis = ReportsData.ExecuteReportsAuthorsAnalysis(projectID);
                //set view for first subreport and mesage of total 
                int activityID = int.Parse(System.Configuration.ConfigurationManager.AppSettings["AN"]);

                DataSet ds = UIHelpers.LoadWorkTimes(projectID, activityID, 0, "0");
                string TotalText = "";
                DataTable source;

                try
                {
                    decimal totalmin = 0;
                    //notes:check if table in dataset has any rows
                    if (ds.Tables[0].Rows.Count != 0)
                        source = UIHelpers.CreateGridSource(ds.Tables[0], out totalmin);
                    else
                        source = new DataTable();
                    totalmin = totalmin / 60;
                    SqlDataReader reader = ProjectsData.SelectProject(projectID);
                    decimal dEur = 0;
                    if (reader.Read())
                    {
                        if (!reader.IsDBNull(28))
                            dEur = reader.GetDecimal(28);
                    }
                    TotalText = string.Format(Resource.ResourceManager["reports_AN_total"], UIHelpers.FormatDecimal2(totalmin), UIHelpers.FormatDecimal2(dEur), UIHelpers.FormatDecimal2(totalmin * dEur));

                }
                catch (Exception ex)
                {
                    log.Error(ex);
                    lblError.Text = Resource.ResourceManager["reports_Minutes_ErrorLoadGrid"];

                    return;
                }

                DataView dvMin = new DataView(source);

                //start the report
                //notes: 4 param is wheather it's pdf or not in this case true
                GrapeCity.ActiveReports.SectionReport rptAuth = new MinutesTypeAN_main(projectName, clientName, userName, true, dvMin, dvAuthorsAnalysis, TotalText, LoggedUser.FullName);
                rptAuth.PageSettings.Orientation = GrapeCity.ActiveReports.Document.Section.PageOrientation.Landscape;
                rptAuth.Run();
                //if (rptAuth.HasData)
                {
                    report.Document.Pages.AddRange(rptAuth.Document.Pages);
                }
            }
            System.IO.MemoryStream memoryFile = new System.IO.MemoryStream();

            pdf.Export(report.Document, memoryFile);

            Response.Clear();
            Response.AppendHeader("content-disposition"
                , "attachment; filename=" + "ProjectAnalysis.pdf");
            Response.ContentType = "application/pdf";

            memoryFile.WriteTo(Response.OutputStream);
            Response.End();
        }

        private void ibXlsExport_Click(object sender, System.Web.UI.ImageClickEventArgs e)
        {
            //			if(cbFeeAccount.Checked)
            //			{
            //				FeeAccountReportXML();
            //				return;
            //			}
            XlsExport pdf = new XlsExport();
            string projectName, projectCode, administrativeName, add;
            decimal area = 0;
            int clientID = 0;
            bool phases; bool isPUP;
            ProjectsData.SelectProject(int.Parse(ddlProjects.SelectedValue), out projectName, out  projectCode, out administrativeName, out area, out add,
                out clientID, out phases, out isPUP);


            int nID = int.Parse(ddlProjects.SelectedValue);
            string sh1 = "";
            string sh2 = "";
            if (nID > 0)
            {
                float ratio1, ratio2;
                ReportsData.ExecuteReportsWorkManHour(nID, out ratio1, out ratio2);

                sh1 = UIHelpers.FormatDecimal2((decimal)ratio1);
                sh2 = UIHelpers.FormatDecimal2((decimal)ratio2);
            }
            DateTime dtStart = Constants.DateMax;
            DateTime dtEnd = Constants.DateMax;
            if (!ckDates.Checked)
            {
                dtStart = calStartDate.SelectedDate;
                dtEnd = calEndDate.SelectedDate;
            }
            DataView dv1All = new DataView();
            DataView dv1 = new DataView();
            if (int.Parse(ddlProjects.SelectedValue) > 0)
            {
                dv1 = ReportsData.ExecuteReportsProjectAnalysis(int.Parse(ddlProjects.SelectedValue), dtStart, dtEnd, phases, int.Parse(ddlType.SelectedValue), ddlProjectsStatus.SelectedIndex, UIHelpers.ToInt(ddlBuildingTypes.SelectedValue));
                dv1.RowFilter = "PaymentID is not null";


            }
            else
            {
                dv1 = ReportsData.ExecuteReportsProjectAnalysis(int.Parse(ddlProjects.SelectedValue), dtStart, dtEnd, false, int.Parse(ddlType.SelectedValue), ddlProjectsStatus.SelectedIndex, UIHelpers.ToInt(ddlBuildingTypes.SelectedValue));
                dv1.RowFilter = "PaymentID is not null";
                dv1All = ReportsData.ExecuteReportsProjectAnalysis(int.Parse(ddlProjects.SelectedValue), dtStart, dtEnd, true, int.Parse(ddlType.SelectedValue), ddlProjectsStatus.SelectedIndex, UIHelpers.ToInt(ddlBuildingTypes.SelectedValue));
                dv1All.RowFilter = "PaymentID is not null";
            }

            DataView dv2 = ReportsData.ExecuteReportsSubcontracterProjectAnalysis(int.Parse(ddlProjects.SelectedValue), dtStart, dtEnd, int.Parse(ddlType.SelectedValue), LoggedUser.HasPaymentRights, ddlProjectsStatus.SelectedIndex, UIHelpers.ToInt(ddlBuildingTypes.SelectedValue));
            dv2.RowFilter = "PaymentID is not null";

            if (!ckClient.Checked)
                LoadReportData();
            GrapeCity.ActiveReports.SectionReport report = new ProjectRpt(false, lblReportTitle.Text + " " + projectName, ddlType.SelectedItem.Text, sh1, sh2, dv1, dv2, ckClient.Checked, !phases, dv1All, cbFinPokazateli.Checked, _dvFinPokazateli, LoggedUser.FullName, _dvBGN, _dvWorkTime);
            report.PageSettings.Orientation = GrapeCity.ActiveReports.Document.Section.PageOrientation.Landscape;
            report.Run();

            if (ckAuthors.Checked)
            {

                //notes: get projectID
                int projectID = int.Parse(ddlProjects.SelectedValue);
                //notes: check if it's not selected project
                if (projectID == 0)
                {
                    lblInfo.Text = Resource.ResourceManager["reports_Minutes_ErrorSelectProject"];
                    return;
                }

                string clientName = string.Empty;
                string userName = string.Empty;
                //get private class HeaderInfo
                //set view for second subreport
                DataView dvAuthorsAnalysis = ReportsData.ExecuteReportsAuthorsAnalysis(projectID);
                //set view for first subreport and mesage of total 
                int activityID = int.Parse(System.Configuration.ConfigurationManager.AppSettings["AN"]);

                DataSet ds = UIHelpers.LoadWorkTimes(projectID, activityID, 0, "0");
                string TotalText = "";
                DataTable source;

                try
                {
                    decimal totalmin = 0;
                    //notes:check if table in dataset has any rows
                    if (ds.Tables[0].Rows.Count != 0)
                        source = UIHelpers.CreateGridSource(ds.Tables[0], out totalmin);
                    else
                        source = new DataTable();
                    totalmin = totalmin / 60;
                    SqlDataReader reader = ProjectsData.SelectProject(projectID);
                    decimal dEur = 0;
                    if (reader.Read())
                    {
                        if (!reader.IsDBNull(28))
                            dEur = reader.GetDecimal(28);
                    }
                    TotalText = string.Format(Resource.ResourceManager["reports_AN_total"], UIHelpers.FormatDecimal2(totalmin), UIHelpers.FormatDecimal2(dEur), UIHelpers.FormatDecimal2(totalmin * dEur));

                }
                catch (Exception ex)
                {
                    log.Error(ex);
                    lblError.Text = Resource.ResourceManager["reports_Minutes_ErrorLoadGrid"];

                    return;
                }

                DataView dvMin = new DataView(source);

                //start the report
                //notes: 4 param is wheather it's pdf or not in this case true
                GrapeCity.ActiveReports.SectionReport rptAuth = new MinutesTypeAN_main(projectName, clientName, userName, false, dvMin, dvAuthorsAnalysis, TotalText, LoggedUser.FullName);
                rptAuth.PageSettings.Orientation = GrapeCity.ActiveReports.Document.Section.PageOrientation.Landscape;
                rptAuth.Run();
                //if (rptAuth.HasData)
                {
                    report.Document.Pages.AddRange(rptAuth.Document.Pages);
                }
            }
            System.IO.MemoryStream memoryFile = new System.IO.MemoryStream();
            pdf.Export(report.Document, memoryFile);

            Response.Clear();
            Response.AppendHeader("content-disposition"
                , "attachment; filename=" + "ProjectAnalysis.xls");
            Response.ContentType = "application/pdf";

            memoryFile.WriteTo(Response.OutputStream);
            Response.End();
        }

        private void ddlProjectsStatus_SelectedIndexChanged(object sender, System.EventArgs e)
        {
            LoadProjects();
        }

        private void ddlBuildingTypes_SelectedIndexChanged(object sender, System.EventArgs e)
        {
            LoadProjects();
        }

        private void grdWorkTime_ItemDataBound(object sender, System.Web.UI.WebControls.DataGridItemEventArgs e)
        {
            if ((e.Item.ItemType == ListItemType.Item) || (e.Item.ItemType == ListItemType.AlternatingItem))
            {

                if (e.Item.Cells[0].Text == Resource.ResourceManager["reports_Total"])
                {

                    e.Item.CssClass = "ReportsTotal";

                }

            }
        }

		
		#region Properties

		public ProjectsData.ProjectsByStatus ProjectsStatus
		{
			get
			{
				try
				{
					return  (ProjectsData.ProjectsByStatus)int.Parse(ddlProjectsStatus.SelectedValue);
				}
				catch { return ProjectsData.ProjectsByStatus.AllProjects; }
			}
		}

		#endregion

		#region FeeAccount

//		private void cbFeeAccount_CheckedChanged(object sender, System.EventArgs e)
//		{
//			cbFeeAccountCheckedChanged(cbFeeAccount.Checked);
//			
//		}
//      
//	
//		private void cbFeeAccountCheckedChanged(bool cbFeeAccountChecked)
//		{
//			if(cbFeeAccountChecked)
//			{
//				Label4.Visible = ddlType.Visible = ckClient.Visible = false;
//			}
//			else
//			{
//				Label4.Visible = ddlType.Visible = ckClient.Visible = true;
//			}
//		}
//		
//		
        private void FeeAccountReport()
        {
            int nID = UIHelpers.ToInt(ddlProjects.SelectedValue);
            //			lblCreatedBy.Text = this.LoggedUser.UserName;
            //			
            //			lblReportTitle.Visible=true;
            //			
            //			lblCreatedBy.Visible = true;
            //			lblCreatedByLbl.Visible = true;
            //			if(ddlProjects.SelectedValue != "0")
            //			{
            //				lblObekt.Text = ProjectsData.SelectProjectName(nID);
            //				Label5.Visible=lblObekt.Visible=true;
            //			}
            //			else
            //			{
            //				Label5.Visible=lblObekt.Visible = false;
            //			}
            DateTime dtStart = Constants.DateMax;
            DateTime dtEnd = Constants.DateMax;
            if (!ckDates.Checked)
            {
                dtStart = calStartDate.SelectedDate;
                dtEnd = calEndDate.SelectedDate;
            }
            if (!ckDates.Checked)
            {
                string HeaderText = string.Concat(grdReport.Columns[8].HeaderText, " ", dtEnd.ToShortDateString());
                grdFeeAccount.Columns[8].HeaderText = HeaderText;
            }
            else
            {
                string HeaderText = string.Concat(grdReport.Columns[8].HeaderText, " ", DateTime.Now.ToShortDateString());
                grdFeeAccount.Columns[8].HeaderText = HeaderText;
            }

            DataView dv = CreateFeeAccountDataView(nID, dtStart, dtEnd, ddlProjectsStatus.SelectedIndex);
            grdFeeAccount.DataSource = dv;

            grdFeeAccount.DataBind();
            if (grdFeeAccount.Items.Count == 0)
            {
                lblNoDataFound.Visible = true;
                grdFeeAccount.Visible = false;
            }
            else
            {
                grdFeeAccount.Visible = true;

                lblNoDataFound.Visible = false;
            }
        }


        private void FeeAccountReportPDF()
        {
            PdfExport pdf = new PdfExport();
            int nID = UIHelpers.ToInt(ddlProjects.SelectedValue);
            pdf.NeverEmbedFonts = "";
            string ProjectName = string.Empty;
            if (ddlProjects.SelectedValue != "0")
            {
                ProjectName = ProjectsData.SelectProjectName(nID);
            }
            string DateTo = DateTime.Now.ToShortDateString();
            DateTime dtStart = Constants.DateMax;
            DateTime dtEnd = Constants.DateMax;
            if (!ckDates.Checked)
            {
                dtStart = calStartDate.SelectedDate;
                dtEnd = calEndDate.SelectedDate;
                DateTo = dtEnd.ToShortDateString();
            }
            //			DataSet ds = ReportsData.ExecuteReportsFeeAccount(nID,dtStart,dtEnd,ddlProjectsStatus.SelectedIndex);
            //			DataTable dt1 = ds.Tables[0];
            //			DataTable dt2 = ds.Tables[1];
            //			DataTable dt = dt1.Clone();
            //			for(int i = dt2.Rows.Count-1;i>-1;i--)
            //			{
            //				DataRow dr = dt2.Rows[i];
            //				if(dr["ORDERCLAUSE"].ToString() == "3")
            //				{
            //					string Client = dr["ClientName"].ToString();
            //					if(Client.Length==0)
            //						dt2.Rows.RemoveAt(i);
            //				}
            //			}
            //			if(dt1.Rows.Count!=0)
            //			{
            //				for(int i=0;i<dt1.Rows.Count-1;i++)
            //				{
            //					DataRow dr = dt.NewRow();
            //					dr.ItemArray=dt1.Rows[i].ItemArray;
            //					dt.Rows.Add(dr);
            //				}
            //				if(dt2.Rows.Count==0)
            //				{
            //					DataRow dr = dt.NewRow();
            //					dr.ItemArray=dt1.Rows[dt1.Rows.Count-1].ItemArray;
            //					dt.Rows.Add(dr);
            //				}
            //			}
            //			if(dt2.Rows.Count!=0)
            //			{
            //				for(int i=0;i<dt2.Rows.Count-1;i++)
            //				{
            //					DataRow dr = dt.NewRow();
            //					dr.ItemArray=dt2.Rows[i].ItemArray;
            //					dt.Rows.Add(dr);
            //				}
            //				if(dt1.Rows.Count!=0)
            //				{
            //					DataRow dr1 = dt.NewRow();
            //					dr1.ItemArray=dt2.Rows[dt2.Rows.Count-1].ItemArray;
            //					DataRow drEnd1 = dt1.Rows[dt1.Rows.Count-1];
            //					decimal db1 = (decimal) drEnd1[4];
            //					DataRow drEnd2 = dt2.Rows[dt2.Rows.Count-1];
            //					decimal db2 = (decimal) drEnd2[4];
            //					decimal db = db1 + db2;
            //					dr1[4] = db;
            //					db =(decimal) dt1.Rows[dt1.Rows.Count-1][5]+(decimal)dt2.Rows[dt2.Rows.Count-1][5];
            //					dr1[5] = db.ToString();
            //					db =(decimal) dt1.Rows[dt1.Rows.Count-1][6]+(decimal) dt2.Rows[dt2.Rows.Count-1][6];
            //					dr1[6] = db.ToString();
            //					dt.Rows.Add(dr1);
            //				}
            //				else
            //				{
            //					DataRow dr2 = dt.NewRow();
            //					dr2.ItemArray=dt2.Rows[dt2.Rows.Count-1].ItemArray;
            //					dt.Rows.Add(dr2);
            //				}
            //			}
            //			DataView dv = new DataView(dt);
            DataView dv = CreateFeeAccountDataView(nID, dtStart, dtEnd, ddlProjectsStatus.SelectedIndex);
            GrapeCity.ActiveReports.SectionReport report = new FeeAccountRpt(true, ProjectName, dv, this.LoggedUser.FullName, DateTo);
            report.PageSettings.Orientation = GrapeCity.ActiveReports.Document.Section.PageOrientation.Landscape;

            report.Run();

            System.IO.MemoryStream memoryFile = new System.IO.MemoryStream();

            pdf.Export(report.Document, memoryFile);

            Response.Clear();
            Response.AppendHeader("content-disposition"
                , "attachment; filename=" + "FeeAccount.pdf");
            Response.ContentType = "application/pdf";

            memoryFile.WriteTo(Response.OutputStream);
            Response.End();
        }


        private void FeeAccountReportXML()
        {
            XlsExport pdf = new XlsExport();
            int nID = UIHelpers.ToInt(ddlProjects.SelectedValue);

            string ProjectName = string.Empty;
            if (ddlProjects.SelectedValue != "0")
            {
                ProjectName = ProjectsData.SelectProjectName(nID);
            }
            string DateTo = DateTime.Now.ToShortDateString();
            DateTime dtStart = Constants.DateMax;
            DateTime dtEnd = Constants.DateMax;
            if (!ckDates.Checked)
            {
                dtStart = calStartDate.SelectedDate;
                dtEnd = calEndDate.SelectedDate;
                DateTo = dtEnd.ToShortDateString();
            }
            //			DataSet ds = ReportsData.ExecuteReportsFeeAccount(nID,dtStart,dtEnd,ddlProjectsStatus.SelectedIndex);
            //			DataTable dt1 = ds.Tables[0];
            //			DataTable dt2 = ds.Tables[1];
            //			DataTable dt = dt1.Clone();
            //			for(int i = dt2.Rows.Count-1;i>-1;i--)
            //			{
            //				DataRow dr = dt2.Rows[i];
            //				if(dr["ORDERCLAUSE"].ToString() == "3")
            //				{
            //					string Client = dr["ClientName"].ToString();
            //					if(Client.Length==0)
            //						dt2.Rows.RemoveAt(i);
            //				}
            //			}
            //			if(dt1.Rows.Count!=0)
            //			{
            //				for(int i=0;i<dt1.Rows.Count-1;i++)
            //				{
            //					DataRow dr = dt.NewRow();
            //					dr.ItemArray=dt1.Rows[i].ItemArray;
            //					dt.Rows.Add(dr);
            //				}
            //				if(dt2.Rows.Count==0)
            //				{
            //					DataRow dr = dt.NewRow();
            //					dr.ItemArray=dt1.Rows[dt1.Rows.Count-1].ItemArray;
            //					dt.Rows.Add(dr);
            //				}
            //			}
            //			if(dt2.Rows.Count!=0)
            //			{
            //				for(int i=0;i<dt2.Rows.Count-1;i++)
            //				{
            //					DataRow dr = dt.NewRow();
            //					dr.ItemArray=dt2.Rows[i].ItemArray;
            //					dt.Rows.Add(dr);
            //				}
            //				if(dt1.Rows.Count!=0)
            //				{
            //					DataRow dr1 = dt.NewRow();
            //					dr1.ItemArray=dt2.Rows[dt2.Rows.Count-1].ItemArray;
            //					DataRow drEnd1 = dt1.Rows[dt1.Rows.Count-1];
            //					decimal db1 = (decimal) drEnd1[4];
            //					DataRow drEnd2 = dt2.Rows[dt2.Rows.Count-1];
            //					decimal db2 = (decimal) drEnd2[4];
            //					decimal db = db1 + db2;
            //					dr1[4] = db;
            //					db =(decimal) dt1.Rows[dt1.Rows.Count-1][5]+(decimal)dt2.Rows[dt2.Rows.Count-1][5];
            //					dr1[5] = db.ToString();
            //					db =(decimal) dt1.Rows[dt1.Rows.Count-1][6]+(decimal) dt2.Rows[dt2.Rows.Count-1][6];
            //					dr1[6] = db.ToString();
            //					dt.Rows.Add(dr1);
            //				}
            //				else
            //				{
            //					DataRow dr2 = dt.NewRow();
            //					dr2.ItemArray=dt2.Rows[dt2.Rows.Count-1].ItemArray;
            //					dt.Rows.Add(dr2);
            //				}
            //			}
            //			DataView dv = new DataView(dt);
            DataView dv = CreateFeeAccountDataView(nID, dtStart, dtEnd, ddlProjectsStatus.SelectedIndex);
            GrapeCity.ActiveReports.SectionReport report = new FeeAccountRpt(false, ProjectName, dv, this.LoggedUser.FullName, DateTo);
            report.PageSettings.Orientation = GrapeCity.ActiveReports.Document.Section.PageOrientation.Landscape;

            report.Run();

            System.IO.MemoryStream memoryFile = new System.IO.MemoryStream();

            pdf.Export(report.Document, memoryFile);

            Response.Clear();
            Response.AppendHeader("content-disposition"
                , "attachment; filename=" + "FeeAccount.xls");
            Response.ContentType = "application/pdf";

            memoryFile.WriteTo(Response.OutputStream);
            Response.End();
        }


        private void grdFeeAccount_ItemDataBound(object sender, System.Web.UI.WebControls.DataGridItemEventArgs e)
        {

            if (e.Item.ItemType == ListItemType.Item
                || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                if (UIHelpers.ToInt(e.Item.Cells[(int)FeeAccountGridColomns.ClientName].Text) > 0)
                {
                    double sum1 = UIHelpers.ToDouble(e.Item.Cells[(int)FeeAccountGridColomns.ClientName].Text);
                    double sum2 = UIHelpers.ToDouble(e.Item.Cells[(int)FeeAccountGridColomns.EURForArea].Text);
                    double delenie = sum2 / sum1;
                    e.Item.Cells[(int)FeeAccountGridColomns.Area].Text = delenie.ToString();
                }
                if (e.Item.Cells[(int)FeeAccountGridColomns.ORDERCLAUSE].Text == "2")
                {
                    e.Item.CssClass = "ReportsSubTotal";
                    //					e.Item.Cells[2].Text="";
                    e.Item.Cells[(int)FeeAccountGridColomns.Area].Text = "";
                    e.Item.Cells[(int)FeeAccountGridColomns.EURForArea].Text = "";
                }
                if (e.Item.Cells[(int)FeeAccountGridColomns.ORDERCLAUSE].Text == "4")
                {
                    e.Item.CssClass = "ReportsSubTotal";
                    //					e.Item.Cells[2].Text="";
                }
                if (e.Item.Cells[(int)FeeAccountGridColomns.ORDERCLAUSE].Text == "3")
                {
                    e.Item.Cells[(int)FeeAccountGridColomns.Area].Text = "";
                    e.Item.Cells[(int)FeeAccountGridColomns.EURForArea].Text = "";
                }
                if (e.Item.Cells[(int)FeeAccountGridColomns.Projectname].Text == string.Concat(Resource.ResourceManager["reports_Total"], ":"))
                {
                    e.Item.CssClass = "ReportsTotal";
                    e.Item.Cells[(int)FeeAccountGridColomns.ClientName].Text = "";
                    e.Item.Cells[(int)FeeAccountGridColomns.Area].Text = "";
                    e.Item.Cells[(int)FeeAccountGridColomns.EURForArea].Text = "";
                }
                decimal all = decimal.Parse(e.Item.Cells[(int)FeeAccountGridColomns.EUR].Text);
                decimal payd = decimal.Parse(e.Item.Cells[(int)FeeAccountGridColomns.Amount].Text);
                if (all == 0)
                {
                    e.Item.Cells[(int)FeeAccountGridColomns.Payd].Text = string.Concat("100.00%");
                    e.Item.Cells[(int)FeeAccountGridColomns.NotPayd].Text = string.Concat("0.00%");
                }
                else
                {
                    decimal per = 100 * payd / all;
                    string textPercent = per.ToString("0.00");
                    textPercent = string.Concat(textPercent, "%");
                    e.Item.Cells[(int)FeeAccountGridColomns.Payd].Text = textPercent;
                    per = 100 - per;
                    textPercent = per.ToString("0.00");
                    textPercent = string.Concat(textPercent, "%");
                    e.Item.Cells[(int)FeeAccountGridColomns.NotPayd].Text = textPercent;
                }
            }
        }


        private DataView CreateFeeAccountDataView(int nID, DateTime dtStart, DateTime dtEnd, int iProjectsStatus)
        {
            DataView dvReturn = new DataView();

            DataSet ds = ReportsData.ExecuteReportsFeeAccount(nID, dtStart, dtEnd, ddlProjectsStatus.SelectedIndex, UIHelpers.ToInt(ddlBuildingTypes.SelectedValue));
            DataTable dt1 = ds.Tables[0];
            DataTable dt2 = ds.Tables[1];
            DataTable dt = dt1.Clone();
            for (int i = dt2.Rows.Count - 1; i > -1; i--)
            {
                DataRow dr = dt2.Rows[i];
                if (dr["ORDERCLAUSE"].ToString() == "3")
                {
                    string Client = dr["ClientName"].ToString();
                    if (Client.Length == 0)
                        dt2.Rows.RemoveAt(i);
                }
            }
            //			foreach(DataRow dr in dt2.Rows)
            //			{
            //				if(dr["ORDERCLAUSE"].ToString() == "3")
            //				{
            //					string Client = dr["ClientName"].ToString();
            //					if(Client.Length==0)
            //						dt2.Rows.Remove(dr);
            //				}
            //			}
            if (dt1.Rows.Count != 0)
            {
                for (int i = 0; i < dt1.Rows.Count - 1; i++)
                {
                    DataRow dr = dt.NewRow();
                    dr.ItemArray = dt1.Rows[i].ItemArray;
                    dt.Rows.Add(dr);
                }
                if (dt2.Rows.Count == 0)
                {
                    DataRow dr = dt.NewRow();
                    dr.ItemArray = dt1.Rows[dt1.Rows.Count - 1].ItemArray;
                    dt.Rows.Add(dr);
                }
            }
            if (dt2.Rows.Count != 0)
            {
                for (int i = 0; i < dt2.Rows.Count - 1; i++)
                {
                    DataRow dr = dt.NewRow();
                    dr.ItemArray = dt2.Rows[i].ItemArray;
                    dt.Rows.Add(dr);
                }
                if (dt1.Rows.Count != 0)
                {
                    DataRow dr1 = dt.NewRow();
                    dr1.ItemArray = dt2.Rows[dt2.Rows.Count - 1].ItemArray;
                    DataRow drEnd1 = dt1.Rows[dt1.Rows.Count - 1];
                    decimal db1 = (decimal)drEnd1[4];
                    DataRow drEnd2 = dt2.Rows[dt2.Rows.Count - 1];
                    decimal db2 = (decimal)drEnd2[4];
                    decimal db = db1 + db2;
                    dr1[4] = db;
                    db = (decimal)dt1.Rows[dt1.Rows.Count - 1][5] + (decimal)dt2.Rows[dt2.Rows.Count - 1][5];
                    dr1[5] = db.ToString();
                    db = (decimal)dt1.Rows[dt1.Rows.Count - 1][6] + (decimal)dt2.Rows[dt2.Rows.Count - 1][6];
                    dr1[6] = db.ToString();
                    dt.Rows.Add(dr1);
                }
                else
                {
                    DataRow dr2 = dt.NewRow();
                    dr2.ItemArray = dt2.Rows[dt2.Rows.Count - 1].ItemArray;
                    dt.Rows.Add(dr2);
                }
            }
            dvReturn = new DataView(dt);

            return dvReturn;
        }

	
		#endregion
	
		#region FinancePokazateli

        private void grdFinancePokazateli_ItemDataBound(object sender, System.Web.UI.WebControls.DataGridItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Item
                || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                if (e.Item.Cells[e.Item.Cells.Count - 1].Text.StartsWith("-"))
                    e.Item.CssClass = "red";

                if (e.Item.Cells[0].Text == "-1")
                {
                    e.Item.CssClass = "ReportsTotal";
                }
            }
        }


        private void CreataFinancePokazateli()
        {

            int nID = UIHelpers.ToInt(ddlProjects.SelectedValue);
            DateTime dtStart = Constants.DateMax;
            DateTime dtEnd = Constants.DateMax;
            if (!ckDates.Checked)
            {
                dtStart = calStartDate.SelectedDate;
                dtEnd = calEndDate.SelectedDate;
            }
            bool bASA = ddlCompany.SelectedIndex == 0;
            DataSet ds = ReportsData.ExecuteReportsFinancePokazateli(nID, dtStart, dtEnd, ddlProjectsStatus.SelectedIndex, UIHelpers.ToInt(ddlBuildingTypes.SelectedValue), bASA);
            DataTable dt = ds.Tables[0];
            DataTable dt1 = UIHelpers.CreateTableForFinancePokazateli(dt);
            _dvFinPokazateli = new DataView(dt1);

            grdFinancePokazateli.DataSource = _dvFinPokazateli;

            grdFinancePokazateli.DataBind();

            _dvBGN = new DataView(UIHelpers.GetBGNTable(dt1));
            grdReportBGN.DataSource = _dvBGN;
            grdReportBGN.DataBind();
            if (grdFinancePokazateli.Items.Count == 0)
            {
                lblNoDataFound.Visible = true;
                lbEUR.Visible = lbBGN.Visible = false;
                grdFinancePokazateli.Visible = grdReportBGN.Visible = false;
            }
            else
            {
                lbEUR.Visible = lbBGN.Visible = true;
                grdFinancePokazateli.Visible = grdReportBGN.Visible = true;

                lblNoDataFound.Visible = false;
            }

        }
		
	
		#endregion

        private void grdReportBGN_ItemDataBound(object sender, System.Web.UI.WebControls.DataGridItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Item
                || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                if (e.Item.Cells[e.Item.Cells.Count - 1].Text.StartsWith("-"))
                    e.Item.CssClass = "red";

                if (e.Item.Cells[0].Text == "-1")
                {
                    e.Item.CssClass = "ReportsTotal";
                }
            }
        }

        private void grdAuthors_ItemDataBound(object sender, System.Web.UI.WebControls.DataGridItemEventArgs e)
        {
            if ((e.Item.ItemType == ListItemType.AlternatingItem) || (e.Item.ItemType == ListItemType.Item))
            {
                TableCell tc;
                DataRowView drv = (DataRowView)e.Item.DataItem;

                #region Format grid cells depending on rowtype

                switch ((int)drv["RowType"])
                {
                    //user summary
                    case 1:
                        e.Item.Cells.Clear();

                        tc = new TableCell();
                        tc.ColumnSpan = 4;
                        tc.ForeColor = System.Drawing.Color.Black;
                        tc.BackColor = System.Drawing.Color.WhiteSmoke;
                        //tc.Text = Resource.ResourceManager["reports_TotalFor"]+drv["UserName"].ToString();
                        tc.Text = Resource.ResourceManager["reports_Total"];
                        e.Item.Cells.Add(tc);

                        tc = new TableCell();
                        tc.ColumnSpan = 2;
                        tc.ForeColor = System.Drawing.Color.Black;
                        //tc.HorizontalAlign = HorizontalAlign.Right;
                        tc.BackColor = System.Drawing.Color.WhiteSmoke;
                        tc.Text = TimeHelper.HoursStringFromMinutes((int)drv["Time"], false);
                        e.Item.Cells.Add(tc); break;

                    //Activity summary
                    case 2:
                        e.Item.Cells.Clear();
                        tc = new TableCell();
                        tc.ColumnSpan = 4;
                        tc.BorderWidth = 3;
                        tc.ForeColor = System.Drawing.Color.Black;
                        tc.Font.Bold = true;
                        tc.BackColor = System.Drawing.Color.WhiteSmoke;
                        //tc.Text = Resource.ResourceManager["reports_TotalFor"]+drv["ActivityName"].ToString();
                        tc.Text = Resource.ResourceManager["reports_Total"];
                        e.Item.Cells.Add(tc);

                        tc = new TableCell(); tc.ForeColor = System.Drawing.Color.Black;
                        tc.ColumnSpan = 2;
                        tc.BorderWidth = 3;
                        tc.BackColor = System.Drawing.Color.WhiteSmoke;
                        tc.Text = TimeHelper.HoursStringFromMinutes((int)drv["Time"], false); tc.Font.Bold = true;
                        e.Item.Cells.Add(tc);

                        System.Globalization.NumberFormatInfo ni = new System.Globalization.NumberFormatInfo();
                        ni.NumberDecimalDigits = 4;
                        ni.NumberDecimalSeparator = ".";


                        /*
                            tc = new TableCell();tc.ForeColor=System.Drawing.Color.Black;
                            tc.Font.Bold = true;
                            tc.ColumnSpan = 1;
                            tc.BorderWidth = 3;
                            tc.BackColor = System.Drawing.Color.WhiteSmoke;
                            tc.Text = Resource.ResourceManager["reports_KoefIzrVreme"]+ " - "+
                            (TimeHelper.HoursFromMinutes((int)drv["Time"])/_area).ToString("0.0000",ni);
                            e.Item.Cells.Add(tc);
                            */
                        break;
                    //Activity header
                    case 3: e.Item.Cells.Clear();
                        tc = new TableCell();
                        tc.ForeColor = System.Drawing.Color.Black;
                        tc.Font.Bold = true;
                        tc.ColumnSpan = 6;
                        tc.BorderWidth = 3;
                        tc.BackColor = System.Drawing.Color.WhiteSmoke;
                        tc.Text = drv["ActivityName"].ToString();
                        e.Item.Cells.Add(tc);
                        break;
                    case 6: e.Item.Cells.Clear(); tc = new TableCell(); tc.BorderWidth = 0; e.Item.Cells.Add(tc);
                        TableRow tr = (TableRow)tc.Parent;
                        tr.Height = 5;
                        break;
                    case 7:
                        e.Item.Cells.Clear();
                        tc = new TableCell();
                        tc.ColumnSpan = 4;
                        tc.BorderWidth = 1;
                        tc.ForeColor = System.Drawing.Color.White;
                        tc.Font.Bold = true;
                        tc.Font.Size = FontUnit.Point(10);
                        tc.BackColor = System.Drawing.Color.Peru;
                        //tc.Text = Resource.ResourceManager["reports_TotalFor"]+ddlProject.SelectedItem.Text;
                        tc.Text = Resource.ResourceManager["reports_Total"];
                        e.Item.Cells.Add(tc);

                        tc = new TableCell();
                        tc.ForeColor = System.Drawing.Color.White;
                        tc.ColumnSpan = 2; tc.BorderWidth = 1;
                        tc.BackColor = System.Drawing.Color.Peru;
                        tc.Text = TimeHelper.HoursStringFromMinutes((int)drv["Time"], false);
                        tc.Font.Size = FontUnit.Point(10);
                        tc.Font.Bold = true;
                        e.Item.Cells.Add(tc);
                        break;
                }

                #endregion

            }
        }

        private void grd_ItemDataBound(object sender, System.Web.UI.WebControls.DataGridItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Item
                || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                if (e.Item.Cells[0].Text == "&nbsp;")
                {
                    e.Item.CssClass = "ReportsTotal";
                    e.Item.Cells[4].Text = "";
                    e.Item.Cells[5].Text = "";


                }


            }

        }

		
	}
}
