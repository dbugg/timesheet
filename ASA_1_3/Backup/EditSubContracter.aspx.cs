using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Data.SqlClient;
using Asa.Timesheet.Data;
using Asa.Timesheet.Data.Entities;
using log4net;
using Asa.Timesheet.WebPages.UserControls;
using Asa.Timesheet.Data.Util;
using System.Text;


namespace Asa.Timesheet.WebPages
{
	/// <summary>
	/// Summary description for EditSubContracter.
	/// </summary>
	public class EditSubContracter : TimesheetPageBase
	{
    #region Web controls

		protected System.Web.UI.WebControls.PlaceHolder menuHolder;
		protected System.Web.UI.WebControls.Label lblName;
		protected System.Web.UI.WebControls.Label lblProjectCode;
		protected System.Web.UI.WebControls.DropDownList ddlTypes;
		protected System.Web.UI.WebControls.Label Label1;
		protected System.Web.UI.WebControls.Button btnSave;
		protected System.Web.UI.WebControls.Button btnCancel;
		protected System.Web.UI.WebControls.Button btnDelete;
		protected System.Web.UI.WebControls.Label lblInfo;
		protected System.Web.UI.WebControls.Label lblError;
		protected System.Web.UI.WebControls.Label Label3;
		protected System.Web.UI.WebControls.TextBox txtName;
		protected System.Web.UI.WebControls.TextBox txtAddress;
		protected System.Web.UI.WebControls.TextBox txtRepr1;
		protected System.Web.UI.WebControls.TextBox txtRepr2;
		protected System.Web.UI.WebControls.Label Label2;
		protected Asa.Timesheet.WebPages.UserControls.PageHeader header;
		protected System.Web.UI.WebControls.Label Label5;
		protected System.Web.UI.WebControls.TextBox TextBox1;
		protected System.Web.UI.WebControls.Label Label6;
		protected System.Web.UI.WebControls.TextBox TextBox2;
		protected System.Web.UI.WebControls.Label Label7;
		protected System.Web.UI.WebControls.TextBox TextBox3;
		protected System.Web.UI.WebControls.Label lblCity;
		protected System.Web.UI.WebControls.TextBox txtCity;
		protected System.Web.UI.WebControls.Label lblAddress;
		protected System.Web.UI.WebControls.TextBox txtManager;
		protected System.Web.UI.WebControls.TextBox txtFax;
		protected System.Web.UI.WebControls.Label Label8;
		protected System.Web.UI.WebControls.TextBox Textbox4;
		protected System.Web.UI.WebControls.Label lblEmail;
		protected System.Web.UI.WebControls.TextBox txtEmail;
		protected System.Web.UI.WebControls.Label Label9;
		protected System.Web.UI.WebControls.Label Label10;
		protected System.Web.UI.WebControls.TextBox Textbox5;
		protected System.Web.UI.WebControls.Label Label11;
		protected System.Web.UI.WebControls.TextBox Textbox6;
		protected System.Web.UI.WebControls.Label Label12;
		protected System.Web.UI.WebControls.Label Label4;
		protected System.Web.UI.WebControls.Label Label13;
		protected System.Web.UI.WebControls.TextBox Textbox7;
		protected System.Web.UI.WebControls.Label Label14;
		protected System.Web.UI.WebControls.TextBox Textbox8;
		protected System.Web.UI.WebControls.TextBox txtDelo;
		protected System.Web.UI.WebControls.TextBox txtBulstat;
		protected System.Web.UI.WebControls.TextBox txtNDR;
		protected System.Web.UI.WebControls.TextBox txtPhone;
		protected System.Web.UI.WebControls.Label Label15;
		protected System.Web.UI.WebControls.TextBox txtPhone1;
		protected System.Web.UI.WebControls.TextBox txtEmail1;
		protected System.Web.UI.WebControls.TextBox txtPhone2;
		protected System.Web.UI.WebControls.TextBox txtEmail2;
		protected System.Web.UI.WebControls.TextBox txtWebsite;
    protected Asa.Timesheet.WebPages.UserControls.EditForm editCtrl;
		protected System.Web.UI.WebControls.Button btnEdit;
		protected System.Web.UI.WebControls.Label Label16;
		protected System.Web.UI.WebControls.CheckBox cbHide;
		protected System.Web.UI.WebControls.Label lbAccountName;
		protected System.Web.UI.WebControls.TextBox txtAccountName;
		protected System.Web.UI.WebControls.Label lbAccountPass;
		protected System.Web.UI.WebControls.TextBox txtAccountPass;


    #endregion
		protected System.Web.UI.WebControls.Button btnAutoGenerate;
		protected System.Web.UI.WebControls.TextBox txtHdnAccountPass;
		protected System.Web.UI.HtmlControls.HtmlInputHidden hdnAccountPass;
		protected System.Web.UI.WebControls.Button btnSaveUP;
		protected System.Web.UI.WebControls.Button btnEditUP;
		protected System.Web.UI.WebControls.Button btnCancelUP;
		protected System.Web.UI.WebControls.Label Label17;
		protected System.Web.UI.WebControls.TextBox txtNameEN;
		protected System.Web.UI.WebControls.Label Label18;
		protected System.Web.UI.WebControls.TextBox txtReprEN;



//		//notes:here is set name and pass at the load of the form
//		private string _accName = string.Empty;
//		private string _accPass = string.Empty;


		private static readonly ILog log = LogManager.GetLogger(typeof(EditSubContracter));

        private void Page_Load(object sender, System.EventArgs e)
        {
            btnAutoGenerate.Attributes["onclick"] = "document.getElementById('txtAccountPass').value=document.getElementById('hdnAccountPass').value;";
            //btnSave.Attributes["onclick"] = "document.getElementById('txtAccountPass').disable=false;";
            int nSID = UIHelpers.GetSIDParam();
            if (Page.IsPostBack == false)
            {
                // Set up form for data change checking when
                // first loaded.
                this.CheckForDataChanges = true;
                this.BypassPromptIds =
                    new string[] { "btnSave", "btnSaveUP", "btnEdit", "btnEditUP", "btnDelete", "btnAutoGenerate", "ddlTypes" };
            }
            if (!LoggedUser.HasRole) ErrorRedirect(Resource.ResourceManager["noRightsAlert"]);

            UIHelpers.CreateMenu(menuHolder, LoggedUser);
            if (!IsPostBack)
            {
                ddlTypes.DataSource = SubprojectsUDL.SelectSubcontracterTypes();
                ddlTypes.DataValueField = "SubcontracterTypeID";
                ddlTypes.DataTextField = "SubcontracterType";
                ddlTypes.DataBind();

                header.PageTitle = Resource.ResourceManager["editcontracter_PageTitle"];
                header.UserName = LoggedUser.UserName;

                if ((!this.LoggedUser.IsLeader) || nSID <= 0) btnDelete.Visible = false;
                else SetConfirmDelete(btnDelete, Resource.ResourceManager["prsubcontracters_del"]);

                if (nSID <= 0)
                {

                }
                else
                {
                    SetFromID(nSID);

                }
                lblError.Text = "";
                InitEdit();
            }


            if (!(LoggedUser.IsSecretary || LoggedUser.HasPaymentRights))
            {
                btnEdit.Visible = false;
                btnEditUP.Visible = false;
                if (nSID <= 0)
                    ErrorRedirect(Resource.ResourceManager["noRightsAlert"]);
            }
            if (!(LoggedUser.HasPaymentRights))
            {
                Label16.Visible = false;
                cbHide.Visible = false;
            }
        }


		
		#region Web Form Designer generated code
        override protected void OnInit(EventArgs e)
        {
            //
            // CODEGEN: This call is required by the ASP.NET Web Form Designer.
            //
            InitializeComponent();
            base.OnInit(e);
        }
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
        private void InitializeComponent()
        {
            this.btnSaveUP.Click += new System.EventHandler(this.btnSave_Click);
            this.btnCancelUP.Click += new System.EventHandler(this.btnCancel_Click);
            this.ddlTypes.SelectedIndexChanged += new System.EventHandler(this.ddlTypes_SelectedIndexChanged);
            this.btnAutoGenerate.Click += new System.EventHandler(this.btnAutoGenerate_Click);
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            this.btnDelete.Click += new System.EventHandler(this.btnDelete_Click);
            this.Load += new System.EventHandler(this.Page_Load);

        }
		#endregion

		#region Menu
//		private void CreateMenu()
//		{
//			UserControls.MenuTable menu = new UserControls.MenuTable();
//			menu.ID = "MenuTable";
//
//			ArrayList menuItems = new ArrayList();
//			menuItems.Add(new MenuItemInfo(Resource.ResourceManager["menuItem_Hours"], "Hours.aspx", true));
//			menuItems.Add(new MenuItemInfo(Resource.ResourceManager["menuItem_Projects"], "Projects.aspx"));
//			if ((LoggedUser.IsLeader) || (LoggedUser.IsAssistant)) 
//			{
//				menuItems.Add(new MenuItemInfo(Resource.ResourceManager["menuItem_Clients"], "Clients.aspx"));
//				menuItems.Add(new MenuItemInfo(Resource.ResourceManager["menuItem_Subcontracters"], "Subcontracters.aspx"));
//
//				if (LoggedUser.IsLeader)
//				menuItems.Add(new MenuItemInfo(Resource.ResourceManager["menuItem_Users"], "Users.aspx"));			
//			}
//			menuItems.Add(new MenuItemInfo(Resource.ResourceManager["menuItem_Emails"], "Emails.aspx"));
//			menu.AddMenuGroup(Resource.ResourceManager["menuHeader_Links"], 20, menuItems);
//			
//			//2 group
//			menuItems = new ArrayList();
//			menuItems.Add(new MenuItemInfo(Resource.ResourceManager["menuItem_NewProject"], "EditProject.aspx"));
//			if ((LoggedUser.IsLeader) || (LoggedUser.IsAssistant)) 
//			{
//				menuItems.Add(new MenuItemInfo(Resource.ResourceManager["menuItem_NewClient"], "EditClient.aspx"));
//				if (LoggedUser.IsLeader)
//				menuItems.Add(new MenuItemInfo(Resource.ResourceManager["menuItem_NewUser"], "EditUser.aspx"));		
//			}
//			menuItems.Add(new MenuItemInfo(Resource.ResourceManager["menuItem_NewEmail"], "EditEmail.aspx"));
//			menuItems.Add(new MenuItemInfo(Resource.ResourceManager["newcontracter_PageTitle"], "EditSubContracter.aspx"));
//
//			menu.AddMenuGroup(Resource.ResourceManager["menuHeader_New"], 30, menuItems);
//
//			menuItems = new ArrayList();
//			menuItems.Add(new MenuItemInfo(Resource.ResourceManager["menuItem_WorkTimeReport"], "reports/worktimes.aspx"));
//			menuItems.Add(new MenuItemInfo(Resource.ResourceManager["menuItem_MinutesReport"], "Reports/Minutes.aspx"));
//			if (LoggedUser.IsLeader || LoggedUser.IsAssistant)
//				menuItems.Add(new MenuItemInfo(Resource.ResourceManager["menuItem_MailReport"], "MailForm.aspx"));
//			if (LoggedUser.IsLeader)
//			{
//				menuItems.Add(new MenuItemInfo(Resource.ResourceManager["menuItem_NotEnteredWorkTime"], "reports/EmptyHours.aspx"));
//				menuItems.Add(new MenuItemInfo(Resource.ResourceManager["menuItem_Calendar"], "reports/Calendar.aspx"));
//			}
//
//			menu.AddMenuGroup(Resource.ResourceManager["menuHeader_Reports"], 30, menuItems);
//			
//			menuItems = new ArrayList();
//			menuItems.Add(new MenuItemInfo(Resource.ResourceManager["menuItem_Help"], SessionManager.GetHelpLink(),true,Pages.Subcontracters));
//			menu.AddMenuGroup("", 10, menuItems);
//
//			menuHolder.Controls.Add(menu);
//		}
		#endregion

        private void SetFromID(int nSID)
        {
            SubcontracterData sd = SubcontracterDAL.Load(nSID);
            if (sd != null)
            {
                txtName.Text = sd.SubcontracterName;
                ddlTypes.SelectedValue = sd.SubcontracterTypeID.ToString();
                txtAddress.Text = sd.Address;
                txtPhone.Text = sd.Phone;
                txtRepr1.Text = sd.Name1;
                txtRepr2.Text = sd.Name2;
                txtDelo.Text = sd.Delo;
                txtBulstat.Text = sd.Bulstat;
                txtNDR.Text = sd.NDR;
                txtWebsite.Text = sd.Webpage;
                txtEmail.Text = sd.Email;
                txtFax.Text = sd.Fax;
                txtPhone1.Text = sd.Phone1;
                txtEmail1.Text = sd.Email1;
                txtPhone2.Text = sd.Phone2;
                txtEmail2.Text = sd.Email2;
                txtManager.Text = sd.Manager;
                cbHide.Checked = sd.Hide;
                txtAccountName.Text = sd.AccountName;
                txtAccountPass.Text = sd.AccountPass;
                txtHdnAccountPass.Text = txtAccountPass.Text;
                txtNameEN.Text = sd.SubcontracterNameEN;
                txtReprEN.Text = sd.NameEN;
                hdnAccountPass.Value = "";
                for (int i = 0; i < txtAccountPass.Text.Length; i++) hdnAccountPass.Value += "*";
                OldAccName = sd.AccountName;
                OldAccPass = sd.AccountPass;
                //_accName = sd.AccountName;
                //_accPass = sd.AccountPass;
                //txtAccountPass.Enabled = false;
                if (ddlTypes.SelectedValue == System.Configuration.ConfigurationManager.AppSettings["SubcontractorType_Other"])
                    cbHide.Checked = true;

            }
        }
        private void btnSave_Click(object sender, System.EventArgs e)
        {
            string name = txtName.Text.Trim();
            if (name == String.Empty)
            {
                AlertFieldNotEntered(lblName);
                return;
            }

            string password = txtAccountPass.Text.Trim();
            if (password == hdnAccountPass.Value) password = txtHdnAccountPass.Text;

            if (txtAccountName.Text.Length > 0 && !(UIHelpers.CheckForUniqueNameAccount(txtAccountName.Text, OldAccName/*_accName*/)))
            {
                lblError.Text = string.Format(Resource.ResourceManager["ErrorNotUniqueName"], txtAccountName.Text);
                return;
            }
            if (txtAccountName.Text.Length == 0 || password.Length == 0)
            {
                txtAccountName.Text = "";
                txtAccountPass.Text = "";
                password = "";
            }

            int nSID = UIHelpers.GetSIDParam();
            bool insertOrUpdate = (nSID < 1);
            bool bHide = cbHide.Checked;
            if (ddlTypes.SelectedValue == System.Configuration.ConfigurationManager.AppSettings["SubcontractorType_Other"])
                bHide = true;
            SubcontracterData sd = new SubcontracterData(nSID, txtName.Text, txtAddress.Text, txtPhone.Text,
                int.Parse(ddlTypes.SelectedValue), txtRepr1.Text, txtRepr2.Text, -1, 0, true,
                txtEmail.Text, txtFax.Text, txtManager.Text, txtDelo.Text, txtBulstat.Text, txtNDR.Text, txtWebsite.Text,
                txtPhone1.Text, txtEmail1.Text, txtPhone2.Text, txtEmail2.Text, 0, bHide, txtAccountName.Text, password);
            sd.SubcontracterNameEN = txtNameEN.Text;
            sd.NameEN = txtReprEN.Text;
            SubcontracterDAL.Save(sd);
            if (insertOrUpdate)
            {
                if (txtAccountName.Text.Length > 0 && txtAccountPass.Text.Length > 0)
                    MailBO.SendMailNewAccount(txtEmail.Text, txtAccountName.Text, password);
                log.Info(string.Format("Subcontracter {0} has been INSERTED by {1}", sd.SubcontracterID, string.Concat(LoggedUser.FullName, " ( userID ", LoggedUser.UserID, " )")));
            }
            else
            {
                if (txtAccountName.Text.Length > 0 && txtAccountPass.Text.Length > 0 && (txtAccountName.Text != OldAccName/*_accName*/|| txtAccountPass.Text != OldAccPass/*_accPass*/))
                    MailBO.SendMailNewAccount(txtEmail.Text, txtAccountName.Text, txtAccountPass.Text);
                log.Info(string.Format("Subcontracter {0} has been UPDATED by {1}", nSID, string.Concat(LoggedUser.FullName, " ( userID ", LoggedUser.UserID, " )")));
            }
            Response.Redirect("Subcontracters.aspx");
        }

        private void btnAutoGenerate_Click(object sender, System.EventArgs e)
        {
            txtAccountPass.Text = Generate();
            txtHdnAccountPass.Text = txtAccountPass.Text;
            hdnAccountPass.Value = "";
            for (int i = 0; i < txtAccountPass.Text.Length; i++) hdnAccountPass.Value += "*";
        }

        private void btnCancel_Click(object sender, System.EventArgs e)
        {
            Response.Redirect("Subcontracters.aspx");
        }

        private void btnDelete_Click(object sender, System.EventArgs e)
        {
            SubcontracterDAL.Delete(UIHelpers.GetSIDParam());
            log.Info(string.Format("Subcontracter {0} has been DELETED by {1}", UIHelpers.GetSIDParam(), string.Concat(LoggedUser.FullName, " ( userID ", LoggedUser.UserID, " )")));
            Response.Redirect("Subcontracters.aspx");
        }

        private void InitEdit()
        {
            int subcontracterID = UIHelpers.GetSIDParam();

            if (subcontracterID <= 0) return;

            string vmButtons = this.btnEdit.ClientID + ";" + this.btnEditUP.ClientID;
            string emButtons = this.btnSave.ClientID + ";" + this.btnSaveUP.ClientID + ";" + this.btnDelete.ClientID;

            string exl = string.Empty;

            editCtrl.InitEdit("tblForm", vmButtons, emButtons, exl);
        }

	
		
		#region GeneratePass

		private char[] characterArray;
		private int passwordLength = 6;
		Random randNum = new Random();

		//		public PasswordGenerator()
		//		{
		//			
		//		}

        private char GetRandomCharacter()
        {
            char c = this.characterArray[(int)((this.characterArray.GetUpperBound(0) + 1) * randNum.NextDouble())];
            return c;
        }

        public string Generate()
        {
            characterArray = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789".ToCharArray();
            StringBuilder sb = new StringBuilder();
            sb.Capacity = passwordLength;
            for (int count = 0; count <= passwordLength - 1; count++)
            {
                sb.Append(GetRandomCharacter());
            }
            if ((sb != null))
            {
                string returnValue = sb.ToString();
                return returnValue;
            }
            return string.Empty;
        }

		

		#endregion

        private void ddlTypes_SelectedIndexChanged(object sender, System.EventArgs e)
        {
            if (ddlTypes.SelectedValue == System.Configuration.ConfigurationManager.AppSettings["SubcontractorType_Other"])
            {
                cbHide.Checked = true;
                cbHide.Enabled = false;
            }
            else
            {
                cbHide.Enabled = true;
            }
        }
	}
}
