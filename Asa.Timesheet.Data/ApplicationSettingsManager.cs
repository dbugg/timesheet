using System;
using System.Data;
using System.Configuration;
using log4net;
namespace Asa.Timesheet.Data
{
	/// <summary>
	/// Summary description for ApplicationSettingsManager.
	/// </summary>
	public class SettingsManager
	{

		#region Hours grid settings
		private static readonly ILog log = LogManager.GetLogger(typeof(SettingsManager));
		public static int HoursGridInitialRowsCount
		{
			get 
			{   
				string sInitialCount = System.Configuration.ConfigurationManager.AppSettings[Constants.AS_KEY_HoursGridInitialRowsCount];
				int initialCount = Constants.HoursGridInitialRowsCountDefault;

				try
				{
					initialCount = int.Parse(sInitialCount);
				}
				catch (Exception ex)
				{
					log.Error(ex.ToString());
				}

				if (initialCount > HoursGridMaxRowsCount) initialCount = HoursGridMaxRowsCount;

				return initialCount;

			}
		}

		public static int HoursGridMaxRowsCount
		{
			get 
			{   
				string sMaxCount = System.Configuration.ConfigurationManager.AppSettings[Constants.AS_KEY_HoursGridMaxRowsCount];
				int maxCount = Constants.HoursGridMaxRowsCountDefault;
				
				try
				{
					maxCount = int.Parse(sMaxCount);
				}
				catch (Exception ex)
				{
					log.Error(ex.ToString());
				}
				
				return maxCount;
			}
		}
		#endregion

		public static int HoursGridDefaultStartHourID
		{
			get 
			{   
				string sStartHourID = System.Configuration.ConfigurationManager.AppSettings[Constants.AS_KEY_HoursGridDefaultStartHourID];
				int startHourID = Constants.HoursGridDefaultStartHourIDDefault;

				try
				{
					startHourID = int.Parse(sStartHourID);
				}
				catch (Exception ex)
				{
					log.Error(ex.ToString());
				}
				return startHourID;
			}		 
		}

		public static int HoursGridDefaultEndHourID
		{
			get 
			{   
				string sEndHourID = System.Configuration.ConfigurationManager.AppSettings[Constants.AS_KEY_HoursGridDefaultEndHourID];
				int endHourID = Constants.HoursGridDefaultEndHourIDDefault;

				try
				{
					endHourID = int.Parse(sEndHourID);
				}
				catch (Exception ex)
				{
					log.Error(ex.ToString());
				}
				return endHourID;
			}		 
		}

    public static string ProjectRootFolderPath
    {
      get
      {
        return System.Configuration.ConfigurationManager.AppSettings["ProjectsRootFolderPath"];
      }
    }
	}
}
