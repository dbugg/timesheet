﻿<head>
<%@ Control Language="c#" AutoEventWireup="false" Codebehind="PageHeader.ascx.cs" Inherits="Asa.Timesheet.WebPages.UserControls.PageHeader" TargetSchema="http://schemas.microsoft.com/intellisense/ie5"%>
<script language="javascript">
   /* function document.onkeydown(){
        if (event.keyCode() == 13) {
            if (Form1.btnEnter != null)
                Form1.btnEnter.click();
            event.returnValue = false;
        }
    }*/
    function popup()  {
        LeftPosition = (screen.width) ? (screen.width - 750) / 2 : 0;
        TopPosition = (screen.height) ? (screen.height - 670) / 2 : 0;
        varFeature = "height=" + 670 + ",width=" + 750 + ",top=" + 100 + ",left=" + LeftPosition + ",resizable=no,scrollbars=yes";

        var wpopup = window.open("ISO.htm", "ISO", varFeature);
        wpopup.focus();
    }


</script>
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i,800,800i&amp;subset=cyrillic" rel="stylesheet"> 
    <link href="https://use.fontawesome.com/releases/v5.0.6/css/all.css" rel="stylesheet">
</head>

			<TABLE id="Table2" height="100%" width="100%" cellpadding="0" cellspacing="0" border="0">

				<TR>
                <td class="menuLeft"></td>
					<td > <IMG  alt="" src="~/images/logo.png" width="320" runat="server"><br />
                    <asp:Menu id="nav" runat="server" Orientation="Horizontal">
                    

        <items>
          <asp:menuitem navigateurl="~/Projects.aspx" 
            text="Проекти &#x2022;"
            tooltip="Проекти" />
            <asp:menuitem navigateurl="~/Clients.aspx?id=1"
              text="Клиенти &#x2022;"
              tooltip="Клиенти" />
              <asp:menuitem navigateurl="~/Subcontracters.aspx" 
                text="Подизпълнители &#x2022;"
                tooltip="Подизпълнители"/>
              <asp:menuitem navigateurl="~/Users.aspx"
                text="Служители &#x2022;"
                tooltip="Служители"/>
              <asp:menuitem navigateurl="~/Clients.aspx?id=7"
                text="Проект мениджъри &#x2022;"
                tooltip="Проект мениждъри"/>

            <asp:menuitem navigateurl="~/Clients.aspx?id=2"
              text="Строители &#x2022;"
              tooltip="Строители" />
              <asp:menuitem navigateurl="~/Clients.aspx?id=5"
                text="Надзорници &#x2022;"
                tooltip="Надзорници"/>
              <asp:menuitem navigateurl="~/Clients.aspx?id=8"
                text="Консултанти/брокери &#x2022;"
                tooltip="Консултанти/брокери"/>
              <asp:menuitem navigateurl="~/Clients.aspx?id=6"
                text="Контакти"
                tooltip="Контакти"/>
        </items>
        </asp:Menu>
</TD>
                    <td style="text-align: right">
                    <asp:label id="lblUser" runat="server"  Font-Bold="True" ></asp:label><br />
					
                    <asp:Button id="btnLogOut" TabIndex="-1" runat="server" Text="Изход" class="ActionButton Lang"></asp:Button>
													
                    <asp:LinkButton id="lbLogOut"  runat="server" 
										 Visible="False">Изход</asp:LinkButton>
							</td>
                </tr>
                <tr>
                <td class="menuLeft"></td>
    					<td  >
									<asp:label id="lblTitle"  runat="server"></asp:label>

                        </td>

				</TR>

			</TABLE>

