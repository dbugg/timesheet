using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Data.SqlClient;
using log4net;
using Asa.Timesheet.Data;
using Asa.Timesheet.Data.Entities;
using System.Configuration;
namespace Asa.Timesheet.WebPages
{
	/// <summary>
	/// Summary description for EditUser.
	/// </summary>
	public class EditUser : TimesheetPageBase
	{
		#region WebControls

		protected System.Web.UI.WebControls.PlaceHolder menuHolder;
		protected System.Web.UI.WebControls.Button btnSave;
		protected System.Web.UI.WebControls.Button btnCancel;
		protected System.Web.UI.WebControls.Button btnDelete;
		protected System.Web.UI.WebControls.Label lblInfo;
		protected System.Web.UI.WebControls.Label lblFirstName;
		protected System.Web.UI.WebControls.Label lblSecondName;
		protected System.Web.UI.WebControls.Label lblLastName;
		protected System.Web.UI.WebControls.TextBox txtAccount;
		protected System.Web.UI.WebControls.Label lblEmail;
		protected System.Web.UI.WebControls.TextBox txtEmail;
		protected System.Web.UI.WebControls.Label lblRole;
		protected System.Web.UI.WebControls.DropDownList ddlRole;
		protected System.Web.UI.WebControls.Label lblAccount;
		protected System.Web.UI.WebControls.TextBox txtLastName;
		protected System.Web.UI.WebControls.Label lblError;
		protected System.Web.UI.WebControls.TextBox txtFirstName;
		protected System.Web.UI.WebControls.TextBox txtSecondName;
		protected Asa.Timesheet.WebPages.UserControls.PageHeader header;
		protected System.Web.UI.WebControls.TextBox txtPassword;
		protected System.Web.UI.WebControls.Label lblPassword;
		protected System.Web.UI.WebControls.TextBox txtHdnPassword;
		protected System.Web.UI.HtmlControls.HtmlInputHidden hdnPassword;
		protected System.Web.UI.WebControls.DropDownList ddlUserRole;
		protected System.Web.UI.WebControls.DropDownList ddlPart;
		protected System.Web.UI.WebControls.TextBox txtCV;
		protected System.Web.UI.WebControls.Label Label1;
		protected System.Web.UI.WebControls.Label Label2;
		protected System.Web.UI.WebControls.Label Label3;
		protected System.Web.UI.WebControls.Label Label4;
		protected System.Web.UI.WebControls.Label Label5;
		protected System.Web.UI.WebControls.Label Label6;
		protected System.Web.UI.WebControls.TextBox txtExt;
		protected System.Web.UI.WebControls.TextBox txtHome;
		protected System.Web.UI.WebControls.DropDownList ddlOccupation;
		protected System.Web.UI.WebControls.Label Label7;
		protected System.Web.UI.WebControls.TextBox txtSalary;

		protected System.Web.UI.WebControls.Label Label8;
		protected System.Web.UI.WebControls.TextBox txtBonus;
		protected System.Web.UI.HtmlControls.HtmlTable tbl;
		protected System.Web.UI.WebControls.DataGrid grdSalaries;
		protected SalariesVector _salaries=null;
		protected SalaryTypesVector _salaryTypes=null;
		protected System.Web.UI.WebControls.Label lbSalary;
		protected System.Web.UI.WebControls.Label Label9;
		protected System.Web.UI.WebControls.CheckBox cbIsTraine;
		protected System.Web.UI.WebControls.Label Label11;
		protected System.Web.UI.WebControls.CheckBox cbIsASI;
		protected System.Web.UI.WebControls.TextBox txtMobile;
		protected System.Web.UI.WebControls.Label Label10;
		protected System.Web.UI.WebControls.TextBox txtMobilePersonal;
		protected System.Web.UI.WebControls.Label lbHasWorkTime;
		protected System.Web.UI.WebControls.CheckBox cbHasWorkTime;//Ivailo
		private static readonly ILog log = LogManager.GetLogger(typeof(EditUser));
		
		#endregion

		private enum Months
		{
			Jan=1,
			Feb=2,
			March=3,
			April=4,
			May=5,
			June=6,
			July=7,
			Aug=8,
			Sept=9,
			Oct=10,
			Nov=11,
			Dec=12

		}  
		protected string GetTotal(int Year, int Type)
		{
			decimal dTotal = 0;
			for(int i=1;i<=Enum.GetNames(typeof(Months)).Length;i++)
			{
				dTotal+=SalaryUDL.GetSalaryByMonthYearType(_salaries,Year,i,Type);
			}
			if((SalaryTypes)Type!=SalaryTypes.Bonus)
				dTotal=dTotal/Enum.GetNames(typeof(Months)).Length;
			return UIHelpers.FormatDecimal2(dTotal);
		}

		protected string GetSalary(int Year, int Type, int Month)
		{
			return UIHelpers.FormatDecimal2( SalaryUDL.GetSalaryByMonthYearType(_salaries,Year,Month,Type));
		}
		private void Page_Load(object sender, System.EventArgs e)
		{
			
			///if (! (LoggedUser.IsLeader  || LoggedUser.IsAdmin)) ErrorRedirect(Resource.ResourceManager["noRightsAlert"]);
			//if (! (LoggedUser.IsLeader || LoggedUser.IsAssistant)) ErrorRedirect(Resource.ResourceManager["noRightsAlert"]);
			if(Page.IsPostBack == false)
			{
				// Set up form for data change checking when
				// first loaded.
				this.CheckForDataChanges= true;
				this.BypassPromptIds =
					new string[] { "btnSave", "btnDelete"};
			}
			UIHelpers.CreateMenu(menuHolder,LoggedUser);
			tbl.Visible=LoggedUser.HasPaymentRights;
			if (UserID>0)
				_salaries=SalaryUDL.GetSalariesByUser(UserID);
			lbSalary.Visible=grdSalaries.Visible=(LoggedUser.HasPaymentRights&&(!LoggedUser.IsAccountantLow));
			if (!this.IsPostBack)
			{
				header.UserName = this.LoggedUser.UserName;

				int userID = UserID;
				if (userID>0) // Edit
				{
					header.PageTitle = Resource.ResourceManager["editUser_EditLabel"];
					if (!LoadUser(userID)) lblError.Text = Resource.ResourceManager["editUser_ErrorLoadUser"];
				
					if (! (LoggedUser.HasPaymentRights  || LoggedUser.IsAdmin))
					{
						txtPassword.Enabled=ddlUserRole.Enabled=
						txtCV.Enabled=txtExt.Enabled=txtHome.Enabled=txtMobile.Enabled=ddlOccupation.Enabled=
							txtAccount.Enabled=txtEmail.Enabled=txtFirstName.Enabled=txtSecondName.Enabled=
							txtLastName.Enabled= ddlRole.Enabled=cbIsASI.Enabled=cbIsTraine.Enabled = cbHasWorkTime.Enabled =false;
						btnDelete.Visible=btnSave.Visible=false;

					}

					//if (!LoggedUser.IsLeader) ddlRole.Enabled = false; //can change roles
				}
				else // New
				{
					header.PageTitle = Resource.ResourceManager["editUser_NewLabel"];
				
					// if non admin can change roles
					/*	if (!LoggedUser.IsLeader)
					{
						LoadRoles("3");
						ddlRole.Enabled = false;
					}
					else */LoadRoles("0");	
				}

				if (userID == -1) btnDelete.Visible = false;
				else SetConfirmDelete(btnDelete, Resource.ResourceManager["users_ConfirmDeleteDefaultName"]);
				
			}
		}

	
		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
			this.cbIsASI.CheckedChanged += new System.EventHandler(this.cbIsTraine_CheckedChanged);
			this.cbHasWorkTime.CheckedChanged += new System.EventHandler(this.cbHasWorkTime_CheckedChanged);
			this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
			this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
			this.btnDelete.Click += new System.EventHandler(this.btnDelete_Click);
			this.Load += new System.EventHandler(this.Page_Load);

		}
		#endregion

		#region Database

		private bool LoadRoles(string selectValue)
		{
			SqlDataReader reader = null;
			
			try
			{
				reader = DBManager.SelectRoles();
				ddlRole.DataSource = reader;
				ddlRole.DataValueField = "RoleID";
				ddlRole.DataTextField = "Role";
				ddlRole.DataBind();

				ddlRole.Items.Insert(0, new ListItem("", "0"));
				ddlRole.SelectedValue = selectValue;
				
				IfLoggedUserIsNotLeader(selectValue);
				
				
			}
			catch (Exception ex)
			{
				log.Error(ex);
				return false;
				
			}
			finally
			{
				if (reader!=null) reader.Close();
			}

			return true;

		}
		protected void IfLoggedUserIsNotLeader(string selectValue)
		{
			if (LoggedUser.HasPaymentRights)
			{
				if(LoggedUser.IsAccountant)
				{
					if(LoggedUser.IsAccountantLow)
					{
						if(selectValue == "1" || selectValue=="4" || selectValue=="5")
							ddlRole.Enabled = false;
						else
						{
							ListItem li = ddlRole.Items[5];
							ddlRole.Items.Remove(li);
							li = ddlRole.Items[4];
							ddlRole.Items.Remove(li);
							li = ddlRole.Items[1];
							ddlRole.Items.Remove(li);
						}
					}
					else
					{
						if(selectValue == "1" || selectValue=="5")
							ddlRole.Enabled = false;
						else
						{
							ListItem li = ddlRole.Items[5];
							ddlRole.Items.Remove(li);
							li = ddlRole.Items[1];
							ddlRole.Items.Remove(li);
						}
					}
				}
			}
			if (LoggedUser.IsAdmin)
			{
				if(selectValue == "1" || selectValue=="4" || selectValue=="9")
					ddlRole.Enabled = false;
				else
				{
					ListItem li = ddlRole.Items[9];
					ddlRole.Items.Remove(li);
					li = ddlRole.Items[4];
					ddlRole.Items.Remove(li);
					li = ddlRole.Items[1];
					ddlRole.Items.Remove(li);
				}
			}
		}
		protected string GetSalaryType(int Type)
		{
			
			SalaryTypeData std= _salaryTypes.GetByID(Type);
			if(std!=null)
				return std.SalaryType;
			return "";
		}
		private ArrayList GetSalaryArray()
		{
			ArrayList al = new ArrayList();
			int CurrentYear=DateTime.Now.Year;
			int StartYear=int.Parse(ConfigurationSettings.AppSettings["MinSalaryYear"]);
			for(int j=CurrentYear;j>=StartYear;j--)
			{
				for(int i =1;i<=Enum.GetNames(typeof(SalaryTypes)).Length;i++)
				{
					SalaryYearData syd = new SalaryYearData(j,i);
					al.Add(syd);
				}
			}
			return al;
		}
		private void SetSalaryGrid()
		{
			
			grdSalaries.DataSource= GetSalaryArray();
			grdSalaries.DataBind();
		}
		
		private bool LoadUser(int userID)
		{
			int roleID = 0;

			
			_salaryTypes=SalaryTypeDAL.LoadCollection();
			SetSalaryGrid();
			SqlDataReader reader = null;
			try
			{
				reader = UsersData.SelectUser(userID);
				reader.Read();

				roleID = reader.IsDBNull(1) ? 0 : reader.GetInt32(1);
				txtFirstName.Text = reader.IsDBNull(2) ? String.Empty : reader.GetString(2);
				txtSecondName.Text = reader.IsDBNull(3) ? String.Empty : reader.GetString(3);
				txtLastName.Text = reader.GetString(4);
				txtAccount.Text = reader.GetString(5);
				txtEmail.Text = reader.GetString(6);
				
				txtPassword.Text = reader.IsDBNull(9) ? String.Empty : reader.GetString(9);
				txtHdnPassword.Text = txtPassword.Text;
				for (int i=0; i<txtPassword.Text.Length; i++) hdnPassword.Value+="*";
				string occ = reader.IsDBNull(11) ? String.Empty : reader.GetString(11);
				if(ddlOccupation.Items.FindByValue(occ)!=null)
				ddlOccupation.SelectedValue=  occ;
				string r = reader.IsDBNull(12) ? String.Empty : reader.GetString(12);
				if(ddlUserRole.Items.FindByValue(r)!=null)
					ddlUserRole.SelectedValue=  r;
				txtExt.Text= reader.IsDBNull(13) ? String.Empty : reader.GetString(13);
				txtMobile.Text= reader.IsDBNull(14) ? String.Empty : reader.GetString(14);
				txtHome.Text= reader.IsDBNull(15) ? String.Empty : reader.GetString(15);
				txtCV.Text= reader.IsDBNull(16) ? String.Empty : reader.GetString(16);
				
				txtSalary.Text= reader.IsDBNull(17) ? String.Empty : reader.GetDecimal(17).ToString();
				txtBonus.Text= reader.IsDBNull(18) ? String.Empty : reader.GetDecimal(18).ToString();
				 
				if (!reader.IsDBNull(19))
					{cbIsTraine.Checked=reader.GetBoolean(19);}
				else
					{cbIsTraine.Checked=false;}
				txtMobilePersonal.Text= reader.IsDBNull(20) ? String.Empty : reader.GetString(20);
				if (!reader.IsDBNull(21))
					{cbIsASI.Checked=reader.GetBoolean(21);}
				else
					{cbIsASI.Checked=false;}
				if (!reader.IsDBNull(22))
				{cbHasWorkTime.Checked=reader.GetBoolean(22); }
				else
				{cbHasWorkTime.Checked=true;}
				gridSalaryHide(cbHasWorkTime.Checked);
			}
			catch (Exception ex)
			{
				log.Error(ex);
				return false;
			}
			finally
			{
				if (reader!=null) reader.Close();
			}

			//can not edit this user - no rights
			//if (LoggedUser.UserRoleID>roleID) ErrorRedirect(Resource.ResourceManager["noRightsAlert"]);

			if (!LoadRoles(roleID.ToString())) return false;

			return true;
		}

		private void gridSalaryHide(bool HasWorkTime)
		{
			if(HasWorkTime)
			{
				if(LoggedUser.HasPaymentRights)
					lbSalary.Visible = grdSalaries.Visible = true;
			}
			else
			{
				lbSalary.Visible = grdSalaries.Visible = false;
			}
		}
		private bool DeleteUser(int userID)
		{
			try
			{
				UsersData.InactiveUser(userID);
			}
			catch (Exception ex)
			{
				log.Error(ex);
				return false;
			}

			return true;
		}

		private bool UpdateUser(int userID, int roleID, string firstName, string secondName, 
													string lastName, string account, string email, string password,
			string occupation, string userrole, string ext, string mobile, string home, string cv, decimal salary, decimal bonus, bool istraine, string mobilePersonal, bool isASI, bool HasWorkTime)
		{
			try
			{
				UsersData.UpdateUser(userID, roleID, firstName, secondName, lastName, account, email, password,
					 occupation,  userrole,  ext,  mobile,  home,  cv,  salary,  bonus, istraine, mobilePersonal, isASI, HasWorkTime);
			}
			catch (Exception ex)
			{
				log.Error(ex);
				return false;
			}

			return true;
		}

		private bool InsertUser(int roleID, string firstName, string secondName, 
										string lastName, string account, string email, string password,
			string occupation, string userrole, string ext, string mobile, string home, string cv, decimal salary, decimal bonus, bool istraine, string mobilePersonal, bool isASI, bool HasWorkTime)
		{
			try
			{
				UsersData.InsertUser(roleID, firstName, secondName, lastName, account, email, true, password,
					 occupation,  userrole,  ext,  mobile,  home,  cv,  salary,  bonus, istraine, mobilePersonal, isASI, HasWorkTime);
			}
			catch (Exception ex)
			{
				log.Error(ex);
				return false;
			}

			return true;
		}

		#endregion

		#region Event handlers

		
		private void btnSave_Click(object sender, System.EventArgs e)
		{
			#region validate input 

			string firstName = txtFirstName.Text.Trim();
			string secondName = txtSecondName.Text.Trim();
			string lastName = txtLastName.Text.Trim();
			if (lastName == String.Empty)
			{
				AlertFieldNotEntered(lblLastName);
				return;
			}
			string account = txtAccount.Text.Trim();
			if (account == String.Empty)
			{
				AlertFieldNotEntered(lblAccount);
				return;
			}
			string email = txtEmail.Text.Trim();
			if (email == String.Empty)
			{
				AlertFieldNotEntered(lblEmail);
				return;
			}
			int roleID = int.Parse(ddlRole.SelectedValue);
			if(roleID==0)
			{
				AlertFieldNotEntered(lblRole);
				hdnPassword.Value = "";
				return;
			}
			int Role = ddlOccupation.SelectedIndex;
			if(roleID!=4 && roleID!=5 && roleID!=8 && roleID!=9 && roleID!=10 && roleID!=11 && Role == 0)
			{
				AlertFieldNotEntered(Label2);
				hdnPassword.Value = "";
				return;
			}
			
			string password = txtPassword.Text.Trim();
			if (password == String.Empty)
			{
				AlertFieldNotEntered(lblPassword);
				hdnPassword.Value = "";
				return;
			}

			if (password == hdnPassword.Value) password = txtHdnPassword.Text; 

			#endregion

			if(!LoggedUser.HasPaymentRights && 
				(roleID==Constants.DirectorRole ||roleID==Constants.AccRole ||roleID==Constants.InjPartnerRole ))
				ErrorRedirect(Resource.ResourceManager["noRightsAlert"]);
			
			
			decimal dSalary,dBonus;
			dSalary=dBonus=0;
			int userID = UserID;
			if(txtSalary.Text!="")
				dSalary=decimal.Parse(txtSalary.Text);
			if(txtBonus.Text!="")
				dBonus=decimal.Parse(txtBonus.Text);
			if (userID == -1)
			{
				if (!InsertUser(roleID, firstName, secondName, lastName, account, email, password,ddlOccupation.SelectedValue,ddlUserRole.SelectedValue,txtExt.Text,txtMobile.Text,txtHome.Text,txtCV.Text,dSalary,dBonus, cbIsTraine.Checked,txtMobilePersonal.Text,cbIsASI.Checked,cbHasWorkTime.Checked))
				{
					lblError.Text = Resource.ResourceManager["editUser_ErrorSave"];
					return;
				}
			}
			else
			{
				if (!UpdateUser(userID, roleID, firstName, secondName, lastName, account, email, password, ddlOccupation.SelectedValue,  ddlUserRole.SelectedValue,  txtExt.Text,  txtMobile.Text,  txtHome.Text,  txtCV.Text,dSalary, dBonus, cbIsTraine.Checked,txtMobilePersonal.Text,cbIsASI.Checked,cbHasWorkTime.Checked))
				{
					lblError.Text = Resource.ResourceManager["editUser_ErrorSave"];
					return;
				}
			}
             //new
			if(LoggedUser.HasPaymentRights)
			{
				ArrayList al = GetSalaryArray();
				SalariesVector sv = new SalariesVector();
				for(int i=0;i<grdSalaries.Items.Count;i++)
				{
					//DataGrid grdSalary=(DataGrid) grdSalaries.Items[i].FindControl("grdSalary");
					SalaryYearData syd = (SalaryYearData)al[i];
					TextBox txtJan=(TextBox)grdSalaries.Items[i].FindControl("txtJan");
					sv.Add(GetSalaryData(txtJan,Months.Jan, syd));
					TextBox txtFeb=(TextBox)grdSalaries.Items[i].FindControl("txtFeb");
					sv.Add(GetSalaryData(txtFeb,Months.Feb,syd));
					TextBox txtMarch=(TextBox)grdSalaries.Items[i].FindControl("txtMarch");
					sv.Add(GetSalaryData(txtMarch,Months.March,syd));
					TextBox txtApril=(TextBox)grdSalaries.Items[i].FindControl("txtApril");
					sv.Add(GetSalaryData(txtApril,Months.April,syd));
					TextBox txtMay=(TextBox)grdSalaries.Items[i].FindControl("txtMay");
					sv.Add(GetSalaryData(txtMay,Months.May,syd));
					TextBox txtJune=(TextBox)grdSalaries.Items[i].FindControl("txtJune");
					sv.Add(GetSalaryData(txtJune,Months.June,syd));	
					TextBox txtJuly=(TextBox)grdSalaries.Items[i].FindControl("txtJuly");
					sv.Add(GetSalaryData(txtJuly,Months.July,syd));
					TextBox txtAug=(TextBox)grdSalaries.Items[i].FindControl("txtAug");
					sv.Add(GetSalaryData(txtAug,Months.Aug,syd));
					TextBox txtSept=(TextBox)grdSalaries.Items[i].FindControl("txtSept");
					sv.Add(GetSalaryData(txtSept,Months.Sept,syd));
					TextBox txtOct=(TextBox)grdSalaries.Items[i].FindControl("txtOct");
					sv.Add(GetSalaryData(txtOct,Months.Oct,syd));
					TextBox txtNov=(TextBox)grdSalaries.Items[i].FindControl("txtNov");
					sv.Add(GetSalaryData(txtNov,Months.Nov,syd));
					TextBox txtDec=(TextBox)grdSalaries.Items[i].FindControl("txtDec");
					sv.Add(GetSalaryData(txtDec,Months.Dec,syd));
				}
				foreach(SalarieData sd in sv) 
					SalarieDAL.Save(sd);
			}
			Response.Redirect("EditUser.aspx?uid="+UserID.ToString());

		}
		private SalarieData GetSalaryData(TextBox tb, Months m,SalaryYearData syd)
		{
			SalarieData sd = new SalarieData();
			sd.Salary = UIHelpers.ParseDecimal(tb.Text);
			sd.SalaryMonth=(int)m;
			sd.SalaryTypeID = syd.SalaryType;
			sd.SalaryYear=syd.SalaryYear;
			sd.UserID= UserID;
			sd.SalaryID= SalaryUDL.GetSalaryIDByMonthYearType(_salaries,sd.SalaryYear,
				sd.SalaryMonth, syd.SalaryType);
			return sd;
		}
		private void btnCancel_Click(object sender, System.EventArgs e)
		{
			Response.Redirect("Users.aspx");
		}

		private void btnDelete_Click(object sender, System.EventArgs e)
		{
			if (!DeleteUser(UserID))
			{
				lblError.Text = Resource.ResourceManager["editUser_ErrorDelete"];
				return;
			}

			Response.Redirect("Users.aspx");
		}
		private void cbIsTraine_CheckedChanged(object sender, System.EventArgs e)
		{
		
		}

		private void cbHasWorkTime_CheckedChanged(object sender, System.EventArgs e)
		{
			gridSalaryHide(cbHasWorkTime.Checked);
		}
		#endregion

		

		
		#region private properties

		private int UserID
		{
			get 
			{
				try
				{
					return int.Parse(Request.Params["uid"]);
				}
				catch
				{
					return -1;
				}
			}		
		}

		#endregion

		
	}
}
