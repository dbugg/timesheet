using System;
using System.Data;
using System.Data.SqlClient; 
using System.Collections;
using Microsoft.ApplicationBlocks.Data;

namespace Asa.Timesheet.Data
{
	public class ProjectClientDAL
	{
		/// <summary>
		/// Loads a ProjectClientData object from the database using the given ProjectClientID
		/// </summary>
		/// <param name="projectClientID">The primary key value</param>
		/// <returns>A ProjectClientData object</returns>
		public static ProjectClientData Load(int projectClientID)
		{
			SqlParameter[] parameterValues = new SqlParameter[] { new SqlParameter("@ProjectClientID", projectClientID) };
			using (SqlDataReader reader = SqlHelper.ExecuteReader(CommonDAL.ConnectionString, CommandType.StoredProcedure, "ProjectClientsSelProc", parameterValues))
			{
				if (reader.Read())
				{
					ProjectClientData result = new ProjectClientData();
					LoadFromReader(reader,result);
					return result;
				}
				else
					return null;
			}
		}

		/// <summary>
		/// Loads a ProjectClientData object from the database using the given where clause
		/// </summary>
		/// <param name="whereClause">The filter expression for the query</param>
		/// <returns>A ProjectClientData object</returns>
		public static ProjectClientData Load(string whereClause)
		{
			SqlParameter[] parameterValues = new SqlParameter[] { new SqlParameter("@WhereClause", whereClause) };
			using (SqlDataReader reader = SqlHelper.ExecuteReader(CommonDAL.ConnectionString, CommandType.StoredProcedure, "ProjectClientsListProc", parameterValues))
			{
				if (reader.Read())
				{
					ProjectClientData result = new ProjectClientData();
					LoadFromReader(reader,result);
					return result;
				}
				else
					return null;
			}
		}
		public static int Save( ProjectClientData source)
		{
			if (source.ProjectClientID == -1)
				return Insert(source);
			else
				return Update(source);	
		}

		public static void Delete( ProjectClientData source)
		{
			Delete(source.ProjectClientID);
		}
		/// <summary>
		/// Loads a collection of ProjectClientData objects from the database.
		/// </summary>
		/// <returns>A collection containing all of the ProjectClientData objects in the database.</returns>
		public static ProjectClientsVector LoadCollection()
		{
			SqlParameter[] parms = new SqlParameter[] {};
			return LoadCollection("ProjectClientsListProc", parms);
		}
	  
		/// <summary>
		/// Saves a collection of ProjectClientData objects to the database.
		/// </summary>
		/// <returns>A collection containing all of the ProjectClientData objects in the database.</returns>
		public static  void  SaveCollection(ProjectClientsVector col,string spName, SqlParameter[] parms)
		{
			ProjectClientsVector colOld = LoadCollection(spName,parms);
			UpdateCollection(colOld,col);
		}
		public static  void  SaveCollection(ProjectClientsVector col)
		{
			ProjectClientsVector colOld = LoadCollection();
			UpdateCollection(colOld,col);
		}
		public static  void  SaveCollection(ProjectClientsVector col, string whereClause )
		{
			ProjectClientsVector colOld = LoadCollection(whereClause);
			UpdateCollection(colOld,col);
		}
		/// <summary>
		/// Loads a collection of ProjectClientData objects from the database.
		/// </summary>
		/// <returns>A collection containing all of the ProjectClientData objects in the database.</returns>
		public static ProjectClientsVector LoadCollection(string whereClause)
		{
			SqlParameter[] parms = new SqlParameter[] { new SqlParameter("@WhereClause", whereClause) };
			return LoadCollection("ProjectClientsListProc", parms);
		}

		/// <summary>
		/// Loads a collection of ProjectClientData objects from the database.
		/// </summary>
		/// <returns>A collection containing all of the ProjectClientData objects in the database.</returns>
		public static ProjectClientsVector LoadCollection(string spName, SqlParameter[] parms)
		{
			ProjectClientsVector result = new ProjectClientsVector();
			using (SqlDataReader reader = SqlHelper.ExecuteReader(CommonDAL.ConnectionString, CommandType.StoredProcedure, spName, parms))
			{
				while (reader.Read())
				{
					ProjectClientData tmp = new ProjectClientData();
					LoadFromReader(reader, tmp);
					result.Add(tmp);
				}
			}
			return result;
		}
		/// <summary>
		/// Deletes a ProjectClientData object from the database.
		/// </summary>
		/// <param name="projectClientID">The primary key value</param>
		public static void Delete(int projectClientID)
		{
			SqlParameter[] parameterValues = new SqlParameter[] { new SqlParameter("@ProjectClientID", projectClientID) };
			SqlHelper.ExecuteNonQuery(CommonDAL.ConnectionString, CommandType.StoredProcedure, "ProjectClientsDelProc", parameterValues);
		}

		public static void LoadFromReader(SqlDataReader reader, ProjectClientData target)
		{
			if (reader != null && !reader.IsClosed)
			{
				//target.Key = reader.GetInt32(0);
				if (!reader.IsDBNull(0)) target.ProjectClientID = reader.GetInt32(0);
				if (!reader.IsDBNull(1)) target.ProjectID = reader.GetInt32(1);
				if (!reader.IsDBNull(2)) target.ClientID = reader.GetInt32(2);
			}
		}
		
	  
		public static int Insert( ProjectClientData source)
		{
			SqlParameter[] parameterValues = GetInsertParameterValues(source);
			SqlHelper.ExecuteNonQuery(CommonDAL.ConnectionString, CommandType.StoredProcedure, "ProjectClientsInsProc", parameterValues);
			source.ProjectClientID = (int) parameterValues[0].Value;
			return source.ProjectClientID;
		}

		public static int Update( ProjectClientData source)
		{
			SqlParameter[] parameterValues = GetUpdateParameterValues(source);
			SqlHelper.ExecuteNonQuery(CommonDAL.ConnectionString, CommandType.StoredProcedure,
				"ProjectClientsUpdProc", parameterValues);
			return source.ProjectClientID;
		}
		private static void UpdateCollection(ProjectClientsVector colOld,ProjectClientsVector col)
		{
			// Delete all old items
			foreach( ProjectClientData itemOld in colOld)
			{
				if(!ExistsKey(itemOld.ProjectClientID, col))
					Delete(itemOld.ProjectClientID);
			}
			foreach( ProjectClientData item in col)
			{
				if(item.ProjectClientID <= -1)
					Insert(item);
				else
					Update(item);
			}
		}
		private static bool ExistsKey(int Key, ProjectClientsVector col)
		{
			foreach( ProjectClientData item in col)
				if(item.ProjectClientID==Key)
					return true;
			return false;
		}

		private static SqlParameter GetSqlParameter(string name, ParameterDirection direction, object value)
		{
			SqlParameter p = new SqlParameter(name, value);
			p.Direction = direction;
			return p;
		}

		private static SqlParameter[] GetInsertParameterValues( ProjectClientData source)
		{
			SqlParameter[] prams = new SqlParameter[3];
			prams[0] = GetSqlParameter("@ProjectClientID", ParameterDirection.Output, source.ProjectClientID);
			prams[1] = GetSqlParameter("@ProjectID", ParameterDirection.Input, source.ProjectID);
			prams[2] = GetSqlParameter("@ClientID", ParameterDirection.Input, source.ClientID);
			return prams;
		}

		private static SqlParameter[] GetUpdateParameterValues( ProjectClientData source)
		{
			return new SqlParameter[] {
										  new SqlParameter("@ProjectClientID", source.ProjectClientID),
										  new SqlParameter("@ProjectID", source.ProjectID),
										  new SqlParameter("@ClientID", source.ClientID)
									  };
		}
	}
}   

