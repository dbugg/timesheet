using System;

namespace Asa.Timesheet.Data
{
	/// <summary>
	/// Summary description for SalaryYearData.
	/// </summary>
	public class SalaryYearData
	{
		private int _salaryYear;
		private int _salaryType;
		public SalaryYearData()
		{
			
		}
		public SalaryYearData(int year, int type)
		{
			_salaryYear=year;
			_salaryType=type;
		}
		public int SalaryType
		{
			get{return _salaryType;}
			set{_salaryType=value;}
		}
		public int SalaryYear
		{
			get{return _salaryYear;}
			set{_salaryYear=value;}
		}
	}
}
