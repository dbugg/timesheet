using System;
using Asa.Timesheet.Data.Reports;
using Asa.Timesheet.Data;
using System.Data;
using GrapeCity.ActiveReports;
using GrapeCity.ActiveReports.Controls;
using GrapeCity.ActiveReports.SectionReportModel;
using GrapeCity.ActiveReports.Document.Section;
using GrapeCity.ActiveReports.Document;

namespace Asa.Timesheet.WebPages.Reports
{
    public class MeetingRpt : GrapeCity.ActiveReports.SectionReport
	{
        public MeetingRpt(bool pdf, string dt, string time, string placeOfMeeting, DataView dv1,
            DataView dv2, DataView dv3, DataView dv4, string otherGuest, string notes)
        {
            _forPDF = pdf;
            InitializeComponent();
            Label4.Text = dt;
            Label6.Text = time;
            Label8.Text = placeOfMeeting;
            this.ReportEnd += new EventHandler(SubAnalysisRpt_ReportEnd);
            this.SubReport1.Report = new MeetingRptSubReport1(dv1);
            if (dv2.Count > 0)
                this.SubReport2.Report = new MeetingRptSubReport1(dv2);
            else
                Label10.Visible = false;
            if (dv3.Count > 0)
                this.SubReport3.Report = new MeetingRptSubReport1(dv3);
            else
                Label11.Visible = false;
            if (dv4.Count > 0)
                this.SubReport4.Report = new MeetingRptSubReport1(dv4);
            else
                Label12.Visible = false;
            if (otherGuest.Length > 0 && (!otherGuest.StartsWith("<")))
                Label14.Text = otherGuest;
            else
                Label13.Visible = false;
            if (notes.Length > 0 && (!notes.StartsWith("<")))
            {
                int heigth = (notes.Length / 80) + 1;
                Label16.Height = (float)0.2 * heigth;
                Label16.Text = notes;
            }
            else
                Label15.Visible = false;
        }
		bool _forPDF = true;


		#region ActiveReports Designer generated code





















        public void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MeetingRpt));
            this.Detail = new GrapeCity.ActiveReports.SectionReportModel.Detail();
            this.ReportHeader = new GrapeCity.ActiveReports.SectionReportModel.ReportHeader();
            this.ReportFooter = new GrapeCity.ActiveReports.SectionReportModel.ReportFooter();
            this.Label3 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label4 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label5 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label6 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label7 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label8 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label9 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label10 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label11 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label12 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.SubReport1 = new GrapeCity.ActiveReports.SectionReportModel.SubReport();
            this.SubReport2 = new GrapeCity.ActiveReports.SectionReportModel.SubReport();
            this.SubReport3 = new GrapeCity.ActiveReports.SectionReportModel.SubReport();
            this.SubReport4 = new GrapeCity.ActiveReports.SectionReportModel.SubReport();
            this.Label13 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label14 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label15 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label16 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            ((System.ComponentModel.ISupportInitialize)(this.Label3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label11)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label12)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label13)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label14)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label15)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label16)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            // 
            // Detail
            // 
            this.Detail.CanShrink = true;
            this.Detail.ColumnSpacing = 0F;
            this.Detail.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
						this.Label9,
						this.Label10,
						this.Label11,
						this.Label12,
						this.SubReport1,
						this.SubReport2,
						this.SubReport3,
						this.SubReport4,
						this.Label13,
						this.Label14,
						this.Label15,
						this.Label16});
            this.Detail.Height = 3.4F;
            this.Detail.Name = "Detail";
            // 
            // ReportHeader
            // 
            this.ReportHeader.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
						this.Label3,
						this.Label4,
						this.Label5,
						this.Label6,
						this.Label7,
						this.Label8});
            this.ReportHeader.Height = 1F;
            this.ReportHeader.Name = "ReportHeader";
            // 
            // ReportFooter
            // 
            this.ReportFooter.Height = 0F;
            this.ReportFooter.Name = "ReportFooter";
            // 
            // Label3
            // 
            this.Label3.Height = 0.2F;
            this.Label3.HyperLink = null;
            this.Label3.Left = 4.5F;
            this.Label3.Name = "Label3";
            this.Label3.Style = "font-size: 7pt; text-align: right; ddo-char-set: 1";
            this.Label3.Text = "????:";
            this.Label3.Top = 0.0625F;
            this.Label3.Width = 1F;
            // 
            // Label4
            // 
            this.Label4.Height = 0.2F;
            this.Label4.HyperLink = null;
            this.Label4.Left = 5.5F;
            this.Label4.Name = "Label4";
            this.Label4.Style = "font-size: 7pt; ddo-char-set: 1";
            this.Label4.Text = "";
            this.Label4.Top = 0.0625F;
            this.Label4.Width = 1F;
            // 
            // Label5
            // 
            this.Label5.Height = 0.2F;
            this.Label5.HyperLink = null;
            this.Label5.Left = 4.5F;
            this.Label5.Name = "Label5";
            this.Label5.Style = "font-size: 7pt; text-align: right; vertical-align: top; ddo-char-set: 1";
            this.Label5.Text = "???:";
            this.Label5.Top = 0.25F;
            this.Label5.Width = 1F;
            // 
            // Label6
            // 
            this.Label6.Height = 0.2F;
            this.Label6.HyperLink = null;
            this.Label6.Left = 5.5F;
            this.Label6.Name = "Label6";
            this.Label6.Style = "font-size: 7pt; ddo-char-set: 1";
            this.Label6.Text = "";
            this.Label6.Top = 0.25F;
            this.Label6.Width = 1F;
            // 
            // Label7
            // 
            this.Label7.Height = 0.2F;
            this.Label7.HyperLink = null;
            this.Label7.Left = 1F;
            this.Label7.Name = "Label7";
            this.Label7.Style = "font-size: 10pt; font-weight: bold; text-align: left; ddo-char-set: 1";
            this.Label7.Text = "????? ?? ??????????:";
            this.Label7.Top = 0.5F;
            this.Label7.Width = 2F;
            // 
            // Label8
            // 
            this.Label8.Height = 0.2F;
            this.Label8.HyperLink = null;
            this.Label8.Left = 1F;
            this.Label8.Name = "Label8";
            this.Label8.Style = "font-size: 10pt; ddo-char-set: 1";
            this.Label8.Text = "";
            this.Label8.Top = 0.75F;
            this.Label8.Width = 5.5F;
            // 
            // Label9
            // 
            this.Label9.Height = 0.2F;
            this.Label9.HyperLink = null;
            this.Label9.Left = 1F;
            this.Label9.Name = "Label9";
            this.Label9.Style = "font-size: 10pt; font-weight: bold; text-align: left; ddo-char-set: 1";
            this.Label9.Text = "???????:";
            this.Label9.Top = 0.0625F;
            this.Label9.Width = 2F;
            // 
            // Label10
            // 
            this.Label10.Height = 0.2F;
            this.Label10.HyperLink = null;
            this.Label10.Left = 1F;
            this.Label10.Name = "Label10";
            this.Label10.Style = "font-size: 10pt; font-weight: bold; text-align: left; ddo-char-set: 1";
            this.Label10.Text = "?????????:";
            this.Label10.Top = 0.625F;
            this.Label10.Width = 2F;
            // 
            // Label11
            // 
            this.Label11.Height = 0.2F;
            this.Label11.HyperLink = null;
            this.Label11.Left = 1F;
            this.Label11.Name = "Label11";
            this.Label11.Style = "font-size: 10pt; font-weight: bold; text-align: left; ddo-char-set: 1";
            this.Label11.Text = "??????????????:";
            this.Label11.Top = 1.1875F;
            this.Label11.Width = 2F;
            // 
            // Label12
            // 
            this.Label12.Height = 0.2F;
            this.Label12.HyperLink = null;
            this.Label12.Left = 1F;
            this.Label12.Name = "Label12";
            this.Label12.Style = "font-size: 10pt; font-weight: bold; text-align: left; ddo-char-set: 1";
            this.Label12.Text = "???????:";
            this.Label12.Top = 1.75F;
            this.Label12.Width = 2F;
            // 
            // SubReport1
            // 
            this.SubReport1.CloseBorder = false;
            this.SubReport1.Height = 0.188F;
            this.SubReport1.Left = 1F;
            this.SubReport1.Name = "SubReport1";
            this.SubReport1.Report = null;
            this.SubReport1.Top = 0.3125F;
            this.SubReport1.Width = 5.490001F;
            // 
            // SubReport2
            // 
            this.SubReport2.CloseBorder = false;
            this.SubReport2.Height = 0.188F;
            this.SubReport2.Left = 1F;
            this.SubReport2.Name = "SubReport2";
            this.SubReport2.Report = null;
            this.SubReport2.Top = 0.875F;
            this.SubReport2.Width = 5.490001F;
            // 
            // SubReport3
            // 
            this.SubReport3.CloseBorder = false;
            this.SubReport3.Height = 0.188F;
            this.SubReport3.Left = 1F;
            this.SubReport3.Name = "SubReport3";
            this.SubReport3.Report = null;
            this.SubReport3.Top = 1.4375F;
            this.SubReport3.Width = 5.490001F;
            // 
            // SubReport4
            // 
            this.SubReport4.CloseBorder = false;
            this.SubReport4.Height = 0.188F;
            this.SubReport4.Left = 1F;
            this.SubReport4.Name = "SubReport4";
            this.SubReport4.Report = null;
            this.SubReport4.Top = 2F;
            this.SubReport4.Width = 5.490001F;
            // 
            // Label13
            // 
            this.Label13.Height = 0.2F;
            this.Label13.HyperLink = null;
            this.Label13.Left = 1F;
            this.Label13.Name = "Label13";
            this.Label13.Style = "font-size: 10pt; font-weight: bold; text-align: left; ddo-char-set: 1";
            this.Label13.Text = "????? ????????:";
            this.Label13.Top = 2.3125F;
            this.Label13.Width = 2F;
            // 
            // Label14
            // 
            this.Label14.Height = 0.2F;
            this.Label14.HyperLink = null;
            this.Label14.Left = 1F;
            this.Label14.Name = "Label14";
            this.Label14.Style = "font-size: 10pt; ddo-char-set: 1";
            this.Label14.Text = "";
            this.Label14.Top = 2.5625F;
            this.Label14.Width = 5.490001F;
            // 
            // Label15
            // 
            this.Label15.Height = 0.2F;
            this.Label15.HyperLink = null;
            this.Label15.Left = 1F;
            this.Label15.Name = "Label15";
            this.Label15.Style = "font-size: 10pt; font-weight: bold; text-align: left; ddo-char-set: 1";
            this.Label15.Text = "???????:";
            this.Label15.Top = 2.875F;
            this.Label15.Width = 2F;
            // 
            // Label16
            // 
            this.Label16.Height = 0.2F;
            this.Label16.HyperLink = null;
            this.Label16.Left = 1F;
            this.Label16.Name = "Label16";
            this.Label16.Style = "font-size: 10pt; ddo-char-set: 1";
            this.Label16.Text = "";
            this.Label16.Top = 3.125F;
            this.Label16.Width = 5.49F;
            // 
            // ActiveReport1
            // 
            this.MasterReport = false;
            this.PageSettings.PaperHeight = 11F;
            this.PageSettings.PaperWidth = 8.5F;
            this.Sections.Add(this.ReportHeader);
            this.Sections.Add(this.Detail);
            this.Sections.Add(this.ReportFooter);
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule(resources.GetString("$this.StyleSheet"), "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: inherit; font-style: inherit; font-variant: inherit; font-weight: bo" +
                        "ld; font-size: 16pt; font-size-adjust: inherit; font-stretch: inherit", "Heading1", "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: Times New Roman; font-style: italic; font-variant: inherit; font-wei" +
                        "ght: bold; font-size: 14pt; font-size-adjust: inherit; font-stretch: inherit", "Heading2", "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: inherit; font-style: inherit; font-variant: inherit; font-weight: bo" +
                        "ld; font-size: 13pt; font-size-adjust: inherit; font-stretch: inherit", "Heading3", "Normal"));
            ((System.ComponentModel.ISupportInitialize)(this.Label3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label11)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label12)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label13)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label14)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label15)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label16)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();
        }

		#endregion
        private void SubAnalysisRpt_ReportEnd(object sender, EventArgs e)
        {
            if (!this._forPDF) return;

            Logo logo = new Logo();
            logo.Run();

            for (int i = 0; i < this.Document.Pages.Count; i++)
                this.Document.Pages[i].Overlay(logo.Document.Pages[0]);
        }

        private ReportHeader ReportHeader;
        private Label Label3;
        private Label Label4;
        private Label Label5;
        private Label Label6;
        private Label Label7;
        private Label Label8;
        private Detail Detail;
        private Label Label9;
        private Label Label10;
        private Label Label11;
        private Label Label12;
        private SubReport SubReport1;
        private SubReport SubReport2;
        private SubReport SubReport3;
        private SubReport SubReport4;
        private Label Label13;
        private Label Label14;
        private Label Label15;
        private Label Label16;
        private ReportFooter ReportFooter;
	}
}
