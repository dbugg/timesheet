USE [asatimesheet]
GO
/****** Object:  StoredProcedure [dbo].[WorkTimesForDayListProc]    Script Date: 2/6/2018 3:42:55 PM ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

ALTER PROCEDURE [dbo].[WorkTimesForDayListProc]

@UserID int,
@WorkDate smalldatetime

AS

SELECT
	WorkTimeID, 
	WorkDate, 
	UserID, 
	ProjectID, 
	isnull(ActivityID, 0) as ActivityID, 
	StartTimeID, 
	EndTimeID, 
	isnull(Minutes,'') as Minutes, 
	isnull(AdminMinutes,'') as AdminMinutes,
	EnteredBy, 
	[EnteredDate],
	[MeetingID]

FROM dbo.WorkTimes 
WHERE UserID=@UserID and @WorkDate = WorkDate

ORDER BY StartTimeID