using System;
using Asa.Timesheet.Data;
using GrapeCity.ActiveReports;
using GrapeCity.ActiveReports.Controls;
using GrapeCity.ActiveReports.SectionReportModel;
using GrapeCity.ActiveReports.Document.Section;
using GrapeCity.ActiveReports.Document;

namespace Asa.Timesheet.WebPages.Reports
{
    public class BuildPermissionSubreport : GrapeCity.ActiveReports.SectionReport
	{
		string _projectName = String.Empty;
		int _startPageNumber;
		bool _fullreport=false;

        public BuildPermissionSubreport(string projectName, bool fullreport, int startPageNumber)
        {
            InitializeComponent();
            _projectName = projectName;
            _startPageNumber = startPageNumber;
            _fullreport = fullreport;
        }

        private void Detail_Format(object sender, System.EventArgs eArgs)
        {

        }

        private void PageHeader_Format(object sender, System.EventArgs eArgs)
        {
            if (this.PageNumber == 1) this.PageHeader.Visible = false;
            else
            {
                this.PageHeader.Visible = true;
                txtPageHeader.Text = _projectName + " - " +
                    Resource.ResourceManager["Reports_strBuildPermissionDocs"];
            }
            if (!_fullreport)
            {
                TextBox79.Visible = TextBox80.Visible = false;
            }
        }

        private void ReportHeader_Format(object sender, System.EventArgs eArgs)
        {
            this.txtProjectNameHeader.Text = _projectName;
            txtReportTitle.Text = Resource.ResourceManager["Reports_strBuildPermissionDocs"];
        }

        private void PageFooter_Format(object sender, System.EventArgs eArgs)
        {
            lblPageFooter.Text = _projectName;
            lblPage.Text = string.Concat(Resource.ResourceManager["rpt_Page"], " ",
                (this.PageNumber + _startPageNumber - 1).ToString());
        }

		#region ActiveReports Designer generated code





























































        public void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(BuildPermissionSubreport));
            this.Detail = new GrapeCity.ActiveReports.SectionReportModel.Detail();
            this.ReportHeader = new GrapeCity.ActiveReports.SectionReportModel.ReportHeader();
            this.ReportFooter = new GrapeCity.ActiveReports.SectionReportModel.ReportFooter();
            this.PageHeader = new GrapeCity.ActiveReports.SectionReportModel.PageHeader();
            this.PageFooter = new GrapeCity.ActiveReports.SectionReportModel.PageFooter();
            this.txtReportTitle = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtMainStatus = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtProjectNameHeader = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.Line6 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.txtPageHeader = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.Line5 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.TextBox1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.TextBox2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.TextBox3 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.TextBox4 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.TextBox5 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.TextBox6 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.TextBox7 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.TextBox8 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.TextBox9 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.TextBox10 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.TextBox11 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtStatusIdea = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.Line1 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.TextBox47 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.TextBox48 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.TextBox49 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.TextBox50 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.TextBox51 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.TextBox52 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.TextBox53 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.TextBox54 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.TextBox55 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.TextBox56 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.TextBox57 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtTechStatus = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.Line2 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.TextBox59 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.TextBox60 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.TextBox61 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.TextBox62 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.TextBox63 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.TextBox64 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.TextBox65 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.TextBox70 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.Line3 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.TextBox71 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.TextBox72 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.TextBox73 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.TextBox74 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.TextBox75 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.TextBox76 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.TextBox77 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.TextBox78 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.Line4 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.TextBox79 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.TextBox80 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.Line11 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.lblPageFooter = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lblPage = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Line7 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            ((System.ComponentModel.ISupportInitialize)(this.txtReportTitle)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtMainStatus)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtProjectNameHeader)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPageHeader)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TextBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TextBox2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TextBox3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TextBox4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TextBox5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TextBox6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TextBox7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TextBox8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TextBox9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TextBox10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TextBox11)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtStatusIdea)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TextBox47)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TextBox48)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TextBox49)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TextBox50)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TextBox51)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TextBox52)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TextBox53)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TextBox54)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TextBox55)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TextBox56)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TextBox57)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTechStatus)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TextBox59)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TextBox60)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TextBox61)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TextBox62)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TextBox63)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TextBox64)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TextBox65)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TextBox70)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TextBox71)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TextBox72)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TextBox73)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TextBox74)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TextBox75)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TextBox76)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TextBox77)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TextBox78)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TextBox79)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TextBox80)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblPageFooter)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblPage)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            // 
            // Detail
            // 
            this.Detail.ColumnSpacing = 0F;
            this.Detail.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
						this.TextBox1,
						this.TextBox2,
						this.TextBox3,
						this.TextBox4,
						this.TextBox5,
						this.TextBox6,
						this.TextBox7,
						this.TextBox8,
						this.TextBox9,
						this.TextBox10,
						this.TextBox11,
						this.txtStatusIdea,
						this.Line1,
						this.TextBox47,
						this.TextBox48,
						this.TextBox49,
						this.TextBox50,
						this.TextBox51,
						this.TextBox52,
						this.TextBox53,
						this.TextBox54,
						this.TextBox55,
						this.TextBox56,
						this.TextBox57,
						this.txtTechStatus,
						this.Line2,
						this.TextBox59,
						this.TextBox60,
						this.TextBox61,
						this.TextBox62,
						this.TextBox63,
						this.TextBox64,
						this.TextBox65,
						this.TextBox70,
						this.Line3,
						this.TextBox71,
						this.TextBox72,
						this.TextBox73,
						this.TextBox74,
						this.TextBox75,
						this.TextBox76,
						this.TextBox77,
						this.TextBox78,
						this.Line4,
						this.TextBox79,
						this.TextBox80});
            this.Detail.Height = 4.520833F;
            this.Detail.Name = "Detail";
            // 
            // ReportHeader
            // 
            this.ReportHeader.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
						this.txtReportTitle,
						this.txtMainStatus,
						this.txtProjectNameHeader,
						this.Line6});
            this.ReportHeader.Height = 0.7868056F;
            this.ReportHeader.Name = "ReportHeader";
            // 
            // ReportFooter
            // 
            this.ReportFooter.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
						this.Line7});
            this.ReportFooter.Height = 0.08333334F;
            this.ReportFooter.Name = "ReportFooter";
            // 
            // PageHeader
            // 
            this.PageHeader.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
						this.txtPageHeader,
						this.Line5});
            this.PageHeader.Height = 0.5902778F;
            this.PageHeader.Name = "PageHeader";
            // 
            // PageFooter
            // 
            this.PageFooter.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
						this.Line11,
						this.lblPageFooter,
						this.lblPage});
            this.PageFooter.Height = 0.25F;
            this.PageFooter.Name = "PageFooter";
            // 
            // txtReportTitle
            // 
            this.txtReportTitle.Height = 0.2755906F;
            this.txtReportTitle.Left = 0F;
            this.txtReportTitle.Name = "txtReportTitle";
            this.txtReportTitle.Style = "font-size: 14pt; font-weight: bold";
            this.txtReportTitle.Text = "III. ?????????? ???????????? ?? ?????????? ?? ??????  ";
            this.txtReportTitle.Top = 0F;
            this.txtReportTitle.Width = 6.299212F;
            // 
            // txtMainStatus
            // 
            this.txtMainStatus.DataField = "MainStatus";
            this.txtMainStatus.Height = 0.2755906F;
            this.txtMainStatus.Left = 6.299212F;
            this.txtMainStatus.Name = "txtMainStatus";
            this.txtMainStatus.Style = "font-size: 14pt; font-weight: bold; text-align: right";
            this.txtMainStatus.Text = "??";
            this.txtMainStatus.Top = 0F;
            this.txtMainStatus.Width = 0.7874014F;
            // 
            // txtProjectNameHeader
            // 
            this.txtProjectNameHeader.Height = 0.2362205F;
            this.txtProjectNameHeader.Left = 0F;
            this.txtProjectNameHeader.Name = "txtProjectNameHeader";
            this.txtProjectNameHeader.Style = "font-size: 12pt; font-weight: bold";
            this.txtProjectNameHeader.Text = "txtProjectNameHeader";
            this.txtProjectNameHeader.Top = 0.2362205F;
            this.txtProjectNameHeader.Width = 6.299212F;
            // 
            // Line6
            // 
            this.Line6.Height = 0F;
            this.Line6.Left = 0F;
            this.Line6.LineWeight = 2F;
            this.Line6.Name = "Line6";
            this.Line6.Top = 0.551181F;
            this.Line6.Width = 7.086611F;
            this.Line6.X1 = 0F;
            this.Line6.X2 = 7.086611F;
            this.Line6.Y1 = 0.551181F;
            this.Line6.Y2 = 0.551181F;
            // 
            // txtPageHeader
            // 
            this.txtPageHeader.Height = 0.2362205F;
            this.txtPageHeader.Left = 0F;
            this.txtPageHeader.Name = "txtPageHeader";
            this.txtPageHeader.Style = "font-size: 11pt; font-weight: bold";
            this.txtPageHeader.Text = "txtPageHeader";
            this.txtPageHeader.Top = 0F;
            this.txtPageHeader.Width = 7.086611F;
            // 
            // Line5
            // 
            this.Line5.Height = 0F;
            this.Line5.Left = 0F;
            this.Line5.LineWeight = 2F;
            this.Line5.Name = "Line5";
            this.Line5.Top = 0.243493F;
            this.Line5.Width = 7.086611F;
            this.Line5.X1 = 0F;
            this.Line5.X2 = 7.086611F;
            this.Line5.Y1 = 0.243493F;
            this.Line5.Y2 = 0.243493F;
            // 
            // TextBox1
            // 
            this.TextBox1.Height = 0.2165354F;
            this.TextBox1.Left = 0F;
            this.TextBox1.Name = "TextBox1";
            this.TextBox1.Style = "font-size: 12pt; font-weight: bold";
            this.TextBox1.Text = "????? ??????";
            this.TextBox1.Top = 0F;
            this.TextBox1.Width = 6.299212F;
            // 
            // TextBox2
            // 
            this.TextBox2.Height = 0.2F;
            this.TextBox2.Left = 0F;
            this.TextBox2.Name = "TextBox2";
            this.TextBox2.Text = "??????:";
            this.TextBox2.Top = 0.3543307F;
            this.TextBox2.Width = 1.181102F;
            // 
            // TextBox3
            // 
            this.TextBox3.DataField = "IdeaProjectDone";
            this.TextBox3.Height = 0.2F;
            this.TextBox3.Left = 1.181102F;
            this.TextBox3.Name = "TextBox3";
            this.TextBox3.OutputFormat = resources.GetString("TextBox3.OutputFormat");
            this.TextBox3.Style = "font-weight: bold; text-align: left";
            this.TextBox3.Text = "TextBox3";
            this.TextBox3.Top = 0.3543307F;
            this.TextBox3.Width = 1.181102F;
            // 
            // TextBox4
            // 
            this.TextBox4.Height = 0.2F;
            this.TextBox4.Left = 2.362205F;
            this.TextBox4.Name = "TextBox4";
            this.TextBox4.Text = "??????????:";
            this.TextBox4.Top = 0.3543307F;
            this.TextBox4.Width = 1.181102F;
            // 
            // TextBox5
            // 
            this.TextBox5.DataField = "IdeaProjectAgreed";
            this.TextBox5.Height = 0.2F;
            this.TextBox5.Left = 3.543307F;
            this.TextBox5.Name = "TextBox5";
            this.TextBox5.OutputFormat = resources.GetString("TextBox5.OutputFormat");
            this.TextBox5.Style = "font-weight: bold; text-align: left";
            this.TextBox5.Text = "TextBox5";
            this.TextBox5.Top = 0.3543307F;
            this.TextBox5.Width = 1.181102F;
            // 
            // TextBox6
            // 
            this.TextBox6.Height = 0.2F;
            this.TextBox6.Left = 4.72441F;
            this.TextBox6.Name = "TextBox6";
            this.TextBox6.Text = "??. ?????:";
            this.TextBox6.Top = 0.3543307F;
            this.TextBox6.Width = 0.9842521F;
            // 
            // TextBox7
            // 
            this.TextBox7.DataField = "IdeaProjectMunWhere";
            this.TextBox7.Height = 0.2F;
            this.TextBox7.Left = 5.708662F;
            this.TextBox7.Name = "TextBox7";
            this.TextBox7.Style = "font-weight: bold; text-align: left";
            this.TextBox7.Text = "TextBox7";
            this.TextBox7.Top = 0.3543307F;
            this.TextBox7.Width = 1.377953F;
            // 
            // TextBox8
            // 
            this.TextBox8.Height = 0.2F;
            this.TextBox8.Left = 0F;
            this.TextBox8.Name = "TextBox8";
            this.TextBox8.Text = "??????:";
            this.TextBox8.Top = 0.6299213F;
            this.TextBox8.Width = 1.181102F;
            // 
            // TextBox9
            // 
            this.TextBox9.DataField = "IdeaProjectMun";
            this.TextBox9.Height = 0.2F;
            this.TextBox9.Left = 1.181102F;
            this.TextBox9.Name = "TextBox9";
            this.TextBox9.Style = "font-weight: bold";
            this.TextBox9.Text = "TextBox9";
            this.TextBox9.Top = 0.6299213F;
            this.TextBox9.Width = 6.102362F;
            // 
            // TextBox10
            // 
            this.TextBox10.Height = 0.2F;
            this.TextBox10.Left = 0F;
            this.TextBox10.Name = "TextBox10";
            this.TextBox10.Text = "???????:";
            this.TextBox10.Top = 0.9055118F;
            this.TextBox10.Width = 1.181102F;
            // 
            // TextBox11
            // 
            this.TextBox11.DataField = "IdeaProjectNotes";
            this.TextBox11.Height = 0.2F;
            this.TextBox11.Left = 1.181102F;
            this.TextBox11.Name = "TextBox11";
            this.TextBox11.Text = "TextBox11";
            this.TextBox11.Top = 0.9055118F;
            this.TextBox11.Width = 5.905513F;
            // 
            // txtStatusIdea
            // 
            this.txtStatusIdea.DataField = "IdeaStatus";
            this.txtStatusIdea.Height = 0.2165354F;
            this.txtStatusIdea.Left = 6.299212F;
            this.txtStatusIdea.Name = "txtStatusIdea";
            this.txtStatusIdea.Style = "font-size: 12pt; font-weight: bold; text-align: right";
            this.txtStatusIdea.Text = "??";
            this.txtStatusIdea.Top = 0F;
            this.txtStatusIdea.Width = 0.7874014F;
            // 
            // Line1
            // 
            this.Line1.Height = 0F;
            this.Line1.Left = 0F;
            this.Line1.LineWeight = 1F;
            this.Line1.Name = "Line1";
            this.Line1.Top = 0.2708333F;
            this.Line1.Width = 7.086611F;
            this.Line1.X1 = 0F;
            this.Line1.X2 = 7.086611F;
            this.Line1.Y1 = 0.2708333F;
            this.Line1.Y2 = 0.2708333F;
            // 
            // TextBox47
            // 
            this.TextBox47.Height = 0.2165354F;
            this.TextBox47.Left = 0F;
            this.TextBox47.Name = "TextBox47";
            this.TextBox47.Style = "font-size: 12pt; font-weight: bold";
            this.TextBox47.Text = "?????????? ??????";
            this.TextBox47.Top = 1.364583F;
            this.TextBox47.Width = 6.299212F;
            // 
            // TextBox48
            // 
            this.TextBox48.Height = 0.2F;
            this.TextBox48.Left = 0F;
            this.TextBox48.Name = "TextBox48";
            this.TextBox48.Text = "??????:";
            this.TextBox48.Top = 1.708497F;
            this.TextBox48.Width = 1.181102F;
            // 
            // TextBox49
            // 
            this.TextBox49.DataField = "TechProjectDone";
            this.TextBox49.Height = 0.2F;
            this.TextBox49.Left = 1.181102F;
            this.TextBox49.Name = "TextBox49";
            this.TextBox49.OutputFormat = resources.GetString("TextBox49.OutputFormat");
            this.TextBox49.Style = "font-weight: bold; text-align: left";
            this.TextBox49.Text = "TextBox49";
            this.TextBox49.Top = 1.708497F;
            this.TextBox49.Width = 1.181102F;
            // 
            // TextBox50
            // 
            this.TextBox50.Height = 0.2F;
            this.TextBox50.Left = 2.362205F;
            this.TextBox50.Name = "TextBox50";
            this.TextBox50.Text = "??????????:";
            this.TextBox50.Top = 1.708497F;
            this.TextBox50.Width = 1.181102F;
            // 
            // TextBox51
            // 
            this.TextBox51.DataField = "TechProjectAgreed";
            this.TextBox51.Height = 0.2F;
            this.TextBox51.Left = 3.543307F;
            this.TextBox51.Name = "TextBox51";
            this.TextBox51.OutputFormat = resources.GetString("TextBox51.OutputFormat");
            this.TextBox51.Style = "font-weight: bold; text-align: left";
            this.TextBox51.Text = "TextBox51";
            this.TextBox51.Top = 1.708497F;
            this.TextBox51.Width = 1.181102F;
            // 
            // TextBox52
            // 
            this.TextBox52.Height = 0.2F;
            this.TextBox52.Left = 4.72441F;
            this.TextBox52.Name = "TextBox52";
            this.TextBox52.Text = "??. ?????:";
            this.TextBox52.Top = 1.708497F;
            this.TextBox52.Width = 0.9842521F;
            // 
            // TextBox53
            // 
            this.TextBox53.DataField = "TechProjectMunWhere";
            this.TextBox53.Height = 0.2F;
            this.TextBox53.Left = 5.708662F;
            this.TextBox53.Name = "TextBox53";
            this.TextBox53.Style = "font-weight: bold; text-align: left";
            this.TextBox53.Text = "TextBox53";
            this.TextBox53.Top = 1.708497F;
            this.TextBox53.Width = 1.377953F;
            // 
            // TextBox54
            // 
            this.TextBox54.Height = 0.2F;
            this.TextBox54.Left = 0F;
            this.TextBox54.Name = "TextBox54";
            this.TextBox54.Text = "??????:";
            this.TextBox54.Top = 1.984088F;
            this.TextBox54.Width = 1.181102F;
            // 
            // TextBox55
            // 
            this.TextBox55.DataField = "TechProjectMun";
            this.TextBox55.Height = 0.2F;
            this.TextBox55.Left = 1.181102F;
            this.TextBox55.Name = "TextBox55";
            this.TextBox55.Style = "font-weight: bold";
            this.TextBox55.Text = "TextBox55";
            this.TextBox55.Top = 1.984088F;
            this.TextBox55.Width = 6.102362F;
            // 
            // TextBox56
            // 
            this.TextBox56.Height = 0.2F;
            this.TextBox56.Left = 0F;
            this.TextBox56.Name = "TextBox56";
            this.TextBox56.Text = "???????:";
            this.TextBox56.Top = 2.259678F;
            this.TextBox56.Width = 1.181102F;
            // 
            // TextBox57
            // 
            this.TextBox57.DataField = "TechProjectNotes";
            this.TextBox57.Height = 0.2F;
            this.TextBox57.Left = 1.181102F;
            this.TextBox57.Name = "TextBox57";
            this.TextBox57.Text = "TextBox57";
            this.TextBox57.Top = 2.259678F;
            this.TextBox57.Width = 5.905513F;
            // 
            // txtTechStatus
            // 
            this.txtTechStatus.DataField = "TechStatus";
            this.txtTechStatus.Height = 0.2165354F;
            this.txtTechStatus.Left = 6.299212F;
            this.txtTechStatus.Name = "txtTechStatus";
            this.txtTechStatus.Style = "font-size: 12pt; font-weight: bold; text-align: right";
            this.txtTechStatus.Text = "??";
            this.txtTechStatus.Top = 1.364583F;
            this.txtTechStatus.Width = 0.7874014F;
            // 
            // Line2
            // 
            this.Line2.Height = 0F;
            this.Line2.Left = 0.006944444F;
            this.Line2.LineWeight = 1F;
            this.Line2.Name = "Line2";
            this.Line2.Top = 1.625F;
            this.Line2.Width = 7.086611F;
            this.Line2.X1 = 0.006944444F;
            this.Line2.X2 = 7.093555F;
            this.Line2.Y1 = 1.625F;
            this.Line2.Y2 = 1.625F;
            // 
            // TextBox59
            // 
            this.TextBox59.Height = 0.2165354F;
            this.TextBox59.Left = 0F;
            this.TextBox59.Name = "TextBox59";
            this.TextBox59.Style = "font-size: 12pt; font-weight: bold";
            this.TextBox59.Text = "?????????? ?? ??????";
            this.TextBox59.Top = 2.71875F;
            this.TextBox59.Width = 6.299212F;
            // 
            // TextBox60
            // 
            this.TextBox60.Height = 0.2F;
            this.TextBox60.Left = 0F;
            this.TextBox60.Name = "TextBox60";
            this.TextBox60.Text = "???????:";
            this.TextBox60.Top = 3.062664F;
            this.TextBox60.Visible = false;
            this.TextBox60.Width = 1.181102F;
            // 
            // TextBox61
            // 
            this.TextBox61.DataField = "BuildPermission";
            this.TextBox61.Height = 0.2F;
            this.TextBox61.Left = 1.181102F;
            this.TextBox61.Name = "TextBox61";
            this.TextBox61.OutputFormat = resources.GetString("TextBox61.OutputFormat");
            this.TextBox61.Style = "font-weight: bold; text-align: left";
            this.TextBox61.Text = "TextBox61";
            this.TextBox61.Top = 3.062664F;
            this.TextBox61.Visible = false;
            this.TextBox61.Width = 1.181102F;
            // 
            // TextBox62
            // 
            this.TextBox62.Height = 0.2F;
            this.TextBox62.Left = 2.362205F;
            this.TextBox62.Name = "TextBox62";
            this.TextBox62.Text = "????????:";
            this.TextBox62.Top = 3.062664F;
            this.TextBox62.Width = 1.181102F;
            // 
            // TextBox63
            // 
            this.TextBox63.DataField = "BuildPermissionDone";
            this.TextBox63.Height = 0.2F;
            this.TextBox63.Left = 3.543307F;
            this.TextBox63.Name = "TextBox63";
            this.TextBox63.OutputFormat = resources.GetString("TextBox63.OutputFormat");
            this.TextBox63.Style = "font-weight: bold; text-align: left";
            this.TextBox63.Text = "TextBox63";
            this.TextBox63.Top = 3.062664F;
            this.TextBox63.Width = 1.181102F;
            // 
            // TextBox64
            // 
            this.TextBox64.Height = 0.2F;
            this.TextBox64.Left = 4.72441F;
            this.TextBox64.Name = "TextBox64";
            this.TextBox64.Text = "???. ?????:";
            this.TextBox64.Top = 3.062664F;
            this.TextBox64.Width = 0.9842521F;
            // 
            // TextBox65
            // 
            this.TextBox65.DataField = "BuildPermissionNumber";
            this.TextBox65.Height = 0.2F;
            this.TextBox65.Left = 5.708662F;
            this.TextBox65.Name = "TextBox65";
            this.TextBox65.Style = "font-weight: bold; text-align: left";
            this.TextBox65.Text = "TextBox65";
            this.TextBox65.Top = 3.062664F;
            this.TextBox65.Width = 1.377953F;
            // 
            // TextBox70
            // 
            this.TextBox70.DataField = "BuildPermissionStatus";
            this.TextBox70.Height = 0.2165354F;
            this.TextBox70.Left = 6.299212F;
            this.TextBox70.Name = "TextBox70";
            this.TextBox70.Style = "font-size: 12pt; font-weight: bold; text-align: right";
            this.TextBox70.Text = "??";
            this.TextBox70.Top = 2.71875F;
            this.TextBox70.Width = 0.7874014F;
            // 
            // Line3
            // 
            this.Line3.Height = 0F;
            this.Line3.Left = 0.006944444F;
            this.Line3.LineWeight = 1F;
            this.Line3.Name = "Line3";
            this.Line3.Top = 2.979167F;
            this.Line3.Width = 7.086611F;
            this.Line3.X1 = 0.006944444F;
            this.Line3.X2 = 7.093555F;
            this.Line3.Y1 = 2.979167F;
            this.Line3.Y2 = 2.979167F;
            // 
            // TextBox71
            // 
            this.TextBox71.Height = 0.2165354F;
            this.TextBox71.Left = 0F;
            this.TextBox71.Name = "TextBox71";
            this.TextBox71.Style = "font-size: 12pt; font-weight: bold";
            this.TextBox71.Text = "?????";
            this.TextBox71.Top = 3.572917F;
            this.TextBox71.Width = 6.299212F;
            // 
            // TextBox72
            // 
            this.TextBox72.Height = 0.2F;
            this.TextBox72.Left = 0F;
            this.TextBox72.Name = "TextBox72";
            this.TextBox72.Text = "??????:";
            this.TextBox72.Top = 3.916831F;
            this.TextBox72.Width = 1.181102F;
            // 
            // TextBox73
            // 
            this.TextBox73.DataField = "Other";
            this.TextBox73.Height = 0.2F;
            this.TextBox73.Left = 1.181102F;
            this.TextBox73.Name = "TextBox73";
            this.TextBox73.OutputFormat = resources.GetString("TextBox73.OutputFormat");
            this.TextBox73.Style = "font-weight: bold; text-align: left";
            this.TextBox73.Text = "TextBox73";
            this.TextBox73.Top = 3.916831F;
            this.TextBox73.Width = 1.181102F;
            // 
            // TextBox74
            // 
            this.TextBox74.Height = 0.2F;
            this.TextBox74.Left = 2.362205F;
            this.TextBox74.Name = "TextBox74";
            this.TextBox74.Text = "???????:";
            this.TextBox74.Top = 3.916831F;
            this.TextBox74.Width = 1.181102F;
            // 
            // TextBox75
            // 
            this.TextBox75.DataField = "OtherDone";
            this.TextBox75.Height = 0.2F;
            this.TextBox75.Left = 3.543307F;
            this.TextBox75.Name = "TextBox75";
            this.TextBox75.OutputFormat = resources.GetString("TextBox75.OutputFormat");
            this.TextBox75.Style = "font-weight: bold; text-align: left";
            this.TextBox75.Text = "TextBox75";
            this.TextBox75.Top = 3.916831F;
            this.TextBox75.Width = 1.181102F;
            // 
            // TextBox76
            // 
            this.TextBox76.Height = 0.2F;
            this.TextBox76.Left = 4.72441F;
            this.TextBox76.Name = "TextBox76";
            this.TextBox76.Text = "???. ?????:";
            this.TextBox76.Top = 3.916831F;
            this.TextBox76.Width = 0.9842521F;
            // 
            // TextBox77
            // 
            this.TextBox77.DataField = "OtherNumber";
            this.TextBox77.Height = 0.2F;
            this.TextBox77.Left = 5.708662F;
            this.TextBox77.Name = "TextBox77";
            this.TextBox77.Style = "font-weight: bold; text-align: left";
            this.TextBox77.Text = "TextBox77";
            this.TextBox77.Top = 3.916831F;
            this.TextBox77.Width = 1.377953F;
            // 
            // TextBox78
            // 
            this.TextBox78.DataField = "OtherStatus";
            this.TextBox78.Height = 0.2165354F;
            this.TextBox78.Left = 6.299212F;
            this.TextBox78.Name = "TextBox78";
            this.TextBox78.Style = "font-size: 12pt; font-weight: bold; text-align: right";
            this.TextBox78.Text = "??";
            this.TextBox78.Top = 3.572917F;
            this.TextBox78.Width = 0.7874014F;
            // 
            // Line4
            // 
            this.Line4.Height = 0F;
            this.Line4.Left = 0.006944444F;
            this.Line4.LineWeight = 1F;
            this.Line4.Name = "Line4";
            this.Line4.Top = 3.833333F;
            this.Line4.Width = 7.086611F;
            this.Line4.X1 = 0.006944444F;
            this.Line4.X2 = 7.093555F;
            this.Line4.Y1 = 3.833333F;
            this.Line4.Y2 = 3.833333F;
            // 
            // TextBox79
            // 
            this.TextBox79.Height = 0.2F;
            this.TextBox79.Left = 0F;
            this.TextBox79.Name = "TextBox79";
            this.TextBox79.Text = "????? ???????:";
            this.TextBox79.Top = 4.192913F;
            this.TextBox79.Width = 1.181102F;
            // 
            // TextBox80
            // 
            this.TextBox80.DataField = "OtherExpenses";
            this.TextBox80.Height = 0.2F;
            this.TextBox80.Left = 1.181102F;
            this.TextBox80.Name = "TextBox80";
            this.TextBox80.OutputFormat = resources.GetString("TextBox80.OutputFormat");
            this.TextBox80.Style = "font-weight: bold; text-align: left";
            this.TextBox80.Text = "TextBox80";
            this.TextBox80.Top = 4.192913F;
            this.TextBox80.Width = 1.181102F;
            // 
            // Line11
            // 
            this.Line11.Height = 0F;
            this.Line11.Left = 0.006944444F;
            this.Line11.LineWeight = 1F;
            this.Line11.Name = "Line11";
            this.Line11.Top = 0.006944444F;
            this.Line11.Width = 7.086611F;
            this.Line11.X1 = 0.006944444F;
            this.Line11.X2 = 7.093555F;
            this.Line11.Y1 = 0.006944444F;
            this.Line11.Y2 = 0.006944444F;
            // 
            // lblPageFooter
            // 
            this.lblPageFooter.Height = 0.1968504F;
            this.lblPageFooter.HyperLink = null;
            this.lblPageFooter.Left = 0F;
            this.lblPageFooter.Name = "lblPageFooter";
            this.lblPageFooter.Style = "font-size: 8pt; font-weight: bold; text-align: left";
            this.lblPageFooter.Text = "???????????? ???????";
            this.lblPageFooter.Top = 0.02460628F;
            this.lblPageFooter.Width = 6.299212F;
            // 
            // lblPage
            // 
            this.lblPage.Height = 0.1968504F;
            this.lblPage.HyperLink = null;
            this.lblPage.Left = 6.299212F;
            this.lblPage.Name = "lblPage";
            this.lblPage.Style = "font-size: 8pt; font-weight: bold; text-align: right";
            this.lblPage.Text = "Page 25";
            this.lblPage.Top = 0.02460628F;
            this.lblPage.Width = 0.7874014F;
            // 
            // Line7
            // 
            this.Line7.Height = 0F;
            this.Line7.Left = 0F;
            this.Line7.LineWeight = 2F;
            this.Line7.Name = "Line7";
            this.Line7.Top = 0.0384952F;
            this.Line7.Width = 7.086611F;
            this.Line7.X1 = 0F;
            this.Line7.X2 = 7.086611F;
            this.Line7.Y1 = 0.0384952F;
            this.Line7.Y2 = 0.0384952F;
            // 
            // ActiveReport1
            // 
            this.MasterReport = false;
            this.PageSettings.DefaultPaperSize = false;
            this.PageSettings.Margins.Bottom = 0.7875F;
            this.PageSettings.Margins.Left = 1.377778F;
            this.PageSettings.Margins.Right = 0.39375F;
            this.PageSettings.Margins.Top = 0.7875F;
            this.PageSettings.PaperHeight = 11.69306F;
            this.PageSettings.PaperKind = System.Drawing.Printing.PaperKind.A4;
            this.PageSettings.PaperWidth = 8.268056F;
            this.PrintWidth = 7.086611F;
            this.Sections.Add(this.ReportHeader);
            this.Sections.Add(this.PageHeader);
            this.Sections.Add(this.Detail);
            this.Sections.Add(this.PageFooter);
            this.Sections.Add(this.ReportFooter);
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule(resources.GetString("$this.StyleSheet"), "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: inherit; font-style: inherit; font-variant: inherit; font-weight: bo" +
                        "ld; font-size: 16pt; font-size-adjust: inherit; font-stretch: inherit", "Heading1", "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: Times New Roman; font-style: italic; font-variant: inherit; font-wei" +
                        "ght: bold; font-size: 14pt; font-size-adjust: inherit; font-stretch: inherit", "Heading2", "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: inherit; font-style: inherit; font-variant: inherit; font-weight: bo" +
                        "ld; font-size: 13pt; font-size-adjust: inherit; font-stretch: inherit", "Heading3", "Normal"));
            ((System.ComponentModel.ISupportInitialize)(this.txtReportTitle)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtMainStatus)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtProjectNameHeader)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPageHeader)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TextBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TextBox2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TextBox3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TextBox4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TextBox5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TextBox6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TextBox7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TextBox8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TextBox9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TextBox10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TextBox11)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtStatusIdea)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TextBox47)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TextBox48)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TextBox49)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TextBox50)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TextBox51)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TextBox52)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TextBox53)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TextBox54)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TextBox55)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TextBox56)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TextBox57)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTechStatus)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TextBox59)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TextBox60)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TextBox61)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TextBox62)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TextBox63)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TextBox64)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TextBox65)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TextBox70)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TextBox71)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TextBox72)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TextBox73)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TextBox74)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TextBox75)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TextBox76)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TextBox77)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TextBox78)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TextBox79)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TextBox80)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblPageFooter)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblPage)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();

            // Attach Report Events
            this.Detail.Format += new System.EventHandler(this.Detail_Format);
            this.PageHeader.Format += new System.EventHandler(this.PageHeader_Format);
            this.ReportHeader.Format += new System.EventHandler(this.ReportHeader_Format);
            this.PageFooter.Format += new System.EventHandler(this.PageFooter_Format);
        }

		#endregion

        private ReportHeader ReportHeader;
        private TextBox txtReportTitle;
        private TextBox txtMainStatus;
        private TextBox txtProjectNameHeader;
        private Line Line6;
        private PageHeader PageHeader;
        private TextBox txtPageHeader;
        private Line Line5;
        private Detail Detail;
        private TextBox TextBox1;
        private TextBox TextBox2;
        private TextBox TextBox3;
        private TextBox TextBox4;
        private TextBox TextBox5;
        private TextBox TextBox6;
        private TextBox TextBox7;
        private TextBox TextBox8;
        private TextBox TextBox9;
        private TextBox TextBox10;
        private TextBox TextBox11;
        private TextBox txtStatusIdea;
        private Line Line1;
        private TextBox TextBox47;
        private TextBox TextBox48;
        private TextBox TextBox49;
        private TextBox TextBox50;
        private TextBox TextBox51;
        private TextBox TextBox52;
        private TextBox TextBox53;
        private TextBox TextBox54;
        private TextBox TextBox55;
        private TextBox TextBox56;
        private TextBox TextBox57;
        private TextBox txtTechStatus;
        private Line Line2;
        private TextBox TextBox59;
        private TextBox TextBox60;
        private TextBox TextBox61;
        private TextBox TextBox62;
        private TextBox TextBox63;
        private TextBox TextBox64;
        private TextBox TextBox65;
        private TextBox TextBox70;
        private Line Line3;
        private TextBox TextBox71;
        private TextBox TextBox72;
        private TextBox TextBox73;
        private TextBox TextBox74;
        private TextBox TextBox75;
        private TextBox TextBox76;
        private TextBox TextBox77;
        private TextBox TextBox78;
        private Line Line4;
        private TextBox TextBox79;
        private TextBox TextBox80;
        private PageFooter PageFooter;
        private Line Line11;
        private Label lblPageFooter;
        private Label lblPage;
        private ReportFooter ReportFooter;
        private Line Line7;
	}
}
