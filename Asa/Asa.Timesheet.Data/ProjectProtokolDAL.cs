using System;
using System.Data;
using System.Data.SqlClient; 
using System.Collections;
using Microsoft.ApplicationBlocks.Data;

namespace Asa.Timesheet.Data
{
	public class ProjectProtokolDAL
	{
		/// <summary>
		/// Loads a ProjectProtokolData object from the database using the given ProjectProtokolID
		/// </summary>
		/// <param name="projectProtokolID">The primary key value</param>
		/// <returns>A ProjectProtokolData object</returns>
		public static ProjectProtokolData Load(int projectProtokolID)
		{
			SqlParameter[] parameterValues = new SqlParameter[] { new SqlParameter("@ProjectProtokolID", projectProtokolID) };
			using (SqlDataReader reader = SqlHelper.ExecuteReader(CommonDAL.ConnectionString, CommandType.StoredProcedure, "ProjectProtokolsSelProc", parameterValues))
			{
				if (reader.Read())
				{
					ProjectProtokolData result = new ProjectProtokolData();
					LoadFromReader(reader,result);
					return result;
				}
				else
					return null;
			}
		}

		/// <summary>
		/// Loads a ProjectProtokolData object from the database using the given where clause
		/// </summary>
		/// <param name="whereClause">The filter expression for the query</param>
		/// <returns>A ProjectProtokolData object</returns>
		public static ProjectProtokolData Load(string whereClause)
		{
			SqlParameter[] parameterValues = new SqlParameter[] { new SqlParameter("@WhereClause", whereClause) };
			using (SqlDataReader reader = SqlHelper.ExecuteReader(CommonDAL.ConnectionString, CommandType.StoredProcedure, "ProjectProtokolsListProc", parameterValues))
			{
				if (reader.Read())
				{
					ProjectProtokolData result = new ProjectProtokolData();
					LoadFromReader(reader,result);
					return result;
				}
				else
					return null;
			}
		}
		public static int Save( ProjectProtokolData source)
		{
			if (source.ProjectProtokolID == -1)
				return Insert(source);
			else
				return Update(source);	
		}

		public static void Delete( ProjectProtokolData source)
		{
			Delete(source.ProjectProtokolID);
		}
		/// <summary>
		/// Loads a collection of ProjectProtokolData objects from the database.
		/// </summary>
		/// <returns>A collection containing all of the ProjectProtokolData objects in the database.</returns>
		public static ProjectProtocolsVector LoadCollection()
		{
			SqlParameter[] parms = new SqlParameter[] {};
			return LoadCollection("ProjectProtokolsListProc", parms);
		}
	  
		/// <summary>
		/// Saves a collection of ProjectProtokolData objects to the database.
		/// </summary>
		/// <returns>A collection containing all of the ProjectProtokolData objects in the database.</returns>
		public static  void  SaveCollection(ProjectProtocolsVector col,string spName, SqlParameter[] parms)
		{
			ProjectProtocolsVector colOld = LoadCollection(spName,parms);
			UpdateCollection(colOld,col);
		}
		public static  void  SaveCollection(ProjectProtocolsVector col)
		{
			ProjectProtocolsVector colOld = LoadCollection();
			UpdateCollection(colOld,col);
		}
		public static  void  SaveCollection(ProjectProtocolsVector col, string whereClause )
		{
			ProjectProtocolsVector colOld = LoadCollection(whereClause);
			UpdateCollection(colOld,col);
		}
		/// <summary>
		/// Loads a collection of ProjectProtokolData objects from the database.
		/// </summary>
		/// <returns>A collection containing all of the ProjectProtokolData objects in the database.</returns>
		public static ProjectProtocolsVector LoadCollection(string whereClause)
		{
			SqlParameter[] parms = new SqlParameter[] { new SqlParameter("@WhereClause", whereClause) };
			return LoadCollection("ProjectProtokolsListProc", parms);
		}

		/// <summary>
		/// Loads a collection of ProjectProtokolData objects from the database.
		/// </summary>
		/// <returns>A collection containing all of the ProjectProtokolData objects in the database.</returns>
		public static ProjectProtocolsVector LoadCollection(string spName, SqlParameter[] parms)
		{
			ProjectProtocolsVector result = new ProjectProtocolsVector();
			using (SqlDataReader reader = SqlHelper.ExecuteReader(CommonDAL.ConnectionString, CommandType.StoredProcedure, spName, parms))
			{
				while (reader.Read())
				{
					ProjectProtokolData tmp = new ProjectProtokolData();
					LoadFromReader(reader, tmp);
					result.Add(tmp);
				}
			}
			return result;
		}
		/// <summary>
		/// Deletes a ProjectProtokolData object from the database.
		/// </summary>
		/// <param name="projectProtokolID">The primary key value</param>
		public static void Delete(int projectProtokolID)
		{
			SqlParameter[] parameterValues = new SqlParameter[] { new SqlParameter("@ProjectProtokolID", projectProtokolID) };
			SqlHelper.ExecuteNonQuery(CommonDAL.ConnectionString, CommandType.StoredProcedure, "ProjectProtokolsDelProc", parameterValues);
		}

		public static void LoadFromReader(SqlDataReader reader, ProjectProtokolData target)
		{
			if (reader != null && !reader.IsClosed)
			{
				//target.Key = reader.GetInt32(0);
				if (!reader.IsDBNull(0)) target.ProjectProtokolID = reader.GetInt32(0);
				if (!reader.IsDBNull(1)) target.ProtokolID = reader.GetInt32(1);
				if (!reader.IsDBNull(2)) target.IsDeleted = reader.GetBoolean(2);
			}
		}
		
	  
		public static int Insert( ProjectProtokolData source)
		{
			SqlParameter[] parameterValues = GetInsertParameterValues(source);
			SqlHelper.ExecuteNonQuery(CommonDAL.ConnectionString, CommandType.StoredProcedure, "ProjectProtokolsInsProc", parameterValues);
			source.ProjectProtokolID = (int) parameterValues[0].Value;
			return source.ProjectProtokolID;
		}

		public static int Update( ProjectProtokolData source)
		{
			SqlParameter[] parameterValues = GetUpdateParameterValues(source);
			SqlHelper.ExecuteNonQuery(CommonDAL.ConnectionString, CommandType.StoredProcedure,
				"ProjectProtokolsUpdProc", parameterValues);
			return source.ProjectProtokolID;
		}
		private static void UpdateCollection(ProjectProtocolsVector colOld,ProjectProtocolsVector col)
		{
			// Delete all old items
			foreach( ProjectProtokolData itemOld in colOld)
			{
				if(!ExistsKey(itemOld.ProjectProtokolID, col))
					Delete(itemOld.ProjectProtokolID);
			}
			foreach( ProjectProtokolData item in col)
			{
				if(item.ProjectProtokolID <= -1)
					Insert(item);
				else
					Update(item);
			}
		}
		private static bool ExistsKey(int Key, ProjectProtocolsVector col)
		{
			foreach( ProjectProtokolData item in col)
				if(item.ProjectProtokolID==Key)
					return true;
			return false;
		}

		private static SqlParameter GetSqlParameter(string name, ParameterDirection direction, object value)
		{
			SqlParameter p = new SqlParameter(name, value);
			p.Direction = direction;
			return p;
		}

		private static SqlParameter[] GetInsertParameterValues( ProjectProtokolData source)
		{
			SqlParameter[] prams = new SqlParameter[3];
			prams[0] = GetSqlParameter("@ProjectProtokolID", ParameterDirection.Output, source.ProjectProtokolID);
			prams[1] = GetSqlParameter("@ProtokolID", ParameterDirection.Input, source.ProtokolID);
			prams[2] = GetSqlParameter("@IsDeleted", ParameterDirection.Input, source.IsDeleted);
			return prams;
		}

		private static SqlParameter[] GetUpdateParameterValues( ProjectProtokolData source)
		{
			return new SqlParameter[] {
										  new SqlParameter("@ProjectProtokolID", source.ProjectProtokolID),
										  new SqlParameter("@ProtokolID", source.ProtokolID),
										  new SqlParameter("@IsDeleted", source.IsDeleted)
									  };
		}
	}
}   

