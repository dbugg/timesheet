﻿<%@ Page language="c#" Codebehind="HotIssues.aspx.cs" AutoEventWireup="false" Inherits="Asa.Timesheet.WebPages.HotIssues" %>
<%@ Register TagPrefix="uc1" TagName="PageHeader" Src="UserControls/PageHeader.ascx" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
	<head runat="server">
		<title>HotIssues</title>
		<meta name="GENERATOR" Content="Microsoft Visual Studio .NET 7.1">
		<meta name="CODE_LANGUAGE" Content="C#">
		<meta name="vs_defaultClientScript" content="JavaScript">
		<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5">
		<LINK href="styles/timesheet.css" type="text/css" rel="stylesheet">
		<script language="javascript" src="PopupCalendar.js"></script>
        <style>tr.valign-top td{vertical-align: top; background: #00487c; color: White !important;} .inv{color: #ddd !important} </style>
	</HEAD>
	<body bottomMargin="0" leftMargin="0" topMargin="0" rightMargin="0">
		<form id="Form1" method="post" runat="server">
			<table height="100%" cellSpacing="0" cellPadding="0" width="100%" border="0">
				<tr height="1">
					<td>
						<uc1:PageHeader id="header" runat="server"></uc1:PageHeader>
					</td>
				</tr>
				<tr>
					<td>
						<table height="100%" cellSpacing="0" cellPadding="0" width="100%" border="0">
							<tr>
								<td class="menuLeft"><asp:placeholder id="menuHolder" runat="server"></asp:placeholder></td>
								
								<td><asp:button id="btnNew1" runat="server" CssClass="ActionButton"  Text="Новa Задача"></asp:button>
									<asp:label id="lblError" runat="server" ForeColor="Red" Visible="False" EnableViewState="False"
										CssClass="ErrorLabel"></asp:label><asp:label id="lblInfo" runat="server" Visible="False" EnableViewState="False" CssClass="InfoLabel"></asp:label><br>
									<TABLE id="Table1" cellSpacing="1" cellPadding="1" width="100%" border="0">
										<TR>
											<td>
                                                <asp:label id="lblProject" runat="server" CssClass="enterDataLabel">Тип:</asp:label>
                                                <asp:dropdownlist id="ddlProjectsStatus" runat="server" CssClass="EnterDataBox" AutoPostBack="True"></asp:dropdownlist>
                                            </td>   
                                            <td>												
                                                <asp:label id="Label1" runat="server" CssClass="enterDataLabel">Сграда:</asp:label>
												<asp:dropdownlist id="ddlBuildingTypes" runat="server" CssClass="enterDataBox" AutoPostBack="True"></asp:dropdownlist>
                                            </td>
                                            <td>
												<asp:label id="Label3" runat="server" CssClass="enterDataLabel" >Тема:</asp:label>
												<asp:dropdownlist id="ddlCategory" runat="server" CssClass="EnterDataBox" ></asp:dropdownlist>
                                                </td>
                                            <td> 
                                                <asp:label id="Label6" runat="server" CssClass="enterDataLabel" Width="100%">Статус:</asp:label>
												<asp:dropdownlist id="ddlStatus" runat="server" CssClass="EnterDataBox"></asp:dropdownlist>
                                                </td>
                                            <td>  
                                                <asp:label id="Label4" runat="server" CssClass="enterDataLabel">От:</asp:label>
											    <asp:textbox id="txtStartDate" runat="server" CssClass="enterDataBox"   style="width: 96px !important"></asp:textbox><IMG id="lkCalendar1" alt="Calendar" src="images/calendar.ico" width="18" align="absBottom"
													    runat="server">
                                            </td> 
                                            <td>
                                                <asp:label id="Label7" runat="server" CssClass="enterDataLabel">До:</asp:label>
												    <asp:textbox id="txtEndDate" runat="server" CssClass="enterDataBox"   style="width: 96px !important"></asp:textbox><IMG id="lkCalendar2" alt="Calendar" src="images/calendar.ico" width="18" align="absBottom"
													    runat="server">
                                            </td>
                                            <td style="vertical-align: bottom;">
                                            <asp:label id="Label8" runat="server" CssClass="enterDataLabel"> </asp:label>
											    <asp:button id="btnSearch" runat="server" CssClass="ActionButton" Text="Филтър" Width="240px"></asp:button>
                                            </td>
                                                </TR>
                                            <tr><td colspan="7"><hr /></td></tr>
                                            <tr class="valign-top">
                                            <td >
												<asp:label id="Label2" runat="server" CssClass="enterDataLabel inv">Проект:</asp:label>
												<asp:dropdownlist id="ddlProject" runat="server" CssClass="EnterDataBox" AutoPostBack="True"></asp:dropdownlist>
                                            </td>

                                            <td valign=top>
                                                <asp:label id="lbClient" runat="server" CssClass="enterDataLabel inv">Клиенти:</asp:label>
												<asp:checkboxlist ID="plcClients" runat="server"></asp:checkboxlist></TD>
                                            <td valign=top>
												<asp:label id="lbTeam" runat="server" CssClass="enterDataLabel inv">Екип:</asp:label>
                                                <asp:checkboxlist ID="plcTeam" runat="server"></asp:checkboxlist>

                                            <td valign=top>                                                    											
												<asp:label id="lbBuilder" runat="server" CssClass="enterDataLabel inv" >Строители и др.:</asp:label>
												<asp:checkboxlist ID="plcBuilders" runat="server"></asp:checkboxlist></TD>
                                            <td colspan="2" width="320">
												        <asp:label id="lbSubs" runat="server" CssClass="enterDataLabel inv">Подизпълнители:</asp:label>
												        <asp:label id="lbSubsData" runat="server" CssClass="EnterDataBox" EnableViewState="False"></asp:label></TD>
                                            <td style="background: white;"><asp:imagebutton id="ibPdfExport" runat="server" ImageUrl="images/pdf1.gif" ToolTip="Справка в PDF формат"></asp:imagebutton>&nbsp;
												<asp:imagebutton id="ibXlsExport" runat="server" ImageUrl="images/xls.gif" ToolTip="Справка в Excel формат"></asp:imagebutton>&nbsp;
												<asp:imagebutton id="ibWordExport" runat="server" ToolTip="Справка в Word формат" ImageUrl="images/word1.gif"
													Width="32px" Height="32px"></asp:imagebutton></td>
										</TR>
                                        <tr  class="valign-top">
                                        <td>
                                            <asp:label runat="server" id="Label9" CssClass="enterDataLabel inv">№ Протокол</asp:label>
                                            <asp:textbox runat="server" ID="txtID" CssClass="EnterDataBox"></asp:textbox>
                                        </td>
                                        <td>
                                            <asp:label runat="server" id="Label10" CssClass="enterDataLabel inv">Дата</asp:label>
                                            <asp:textbox runat="server" ID="txtDate"  CssClass="EnterDataBox"></asp:textbox>
                                        </td>
                                        <td colspan="4">
                                            <asp:label runat="server" id="lblDocs" CssClass="enterDataLabel inv">Документи:</asp:label>
                                            <asp:textbox TextMode="MultiLine" Width="100%" Height="80px" runat="server" ID="txtDocuments" Rows="10" Columns="80" CssClass="EnterDataBox"></asp:textbox>
                                        </td></tr>
									</TABLE>
                                    
									<asp:panel id="grid"
										runat="server" Width="98%" Height="800px">
										<asp:datagrid id="grdMain" runat="server" CssClass="Grid" Width="100%" AutoGenerateColumns="False"
											CellPadding="4" PageSize="2" AllowSorting="True">
											<ItemStyle CssClass="GridItem"></ItemStyle>
											<HeaderStyle Wrap="False" CssClass="GridHeader"></HeaderStyle>
											<Columns>
												<asp:BoundColumn DataField="HotIssueID" Visible="False">
													<HeaderStyle Wrap="False" Width="70px"></HeaderStyle>
													<ItemStyle ForeColor="DimGray"></ItemStyle>
												</asp:BoundColumn>
												<asp:TemplateColumn HeaderText="Задача" SortExpression="HotIssueName">
													<HeaderStyle Wrap="False" Width="33%"></HeaderStyle>
													<ItemStyle ForeColor="DimGray"></ItemStyle>
													<ItemTemplate>
														<asp:LinkButton id="lnk" runat="server" CssClass="menuTable"  CommandName="Edit">
															<span Class="menuTable">
																<%# DataBinder.Eval(Container, "DataItem.HotIssueName")%>
															</span>
														</asp:LinkButton>
													</ItemTemplate>
												</asp:TemplateColumn>
												<asp:BoundColumn DataField="ProjectName" HeaderText="Проект" SortExpression="ProjectName">
													<HeaderStyle Wrap="False" Width="70px"></HeaderStyle>
													<ItemStyle ForeColor="DimGray"></ItemStyle>
												</asp:BoundColumn>
												<asp:BoundColumn DataField="HotIssueCategoryName" HeaderText="Тема" SortExpression="HotIssueCategoryName">
													<HeaderStyle Wrap="False" Width="70px"></HeaderStyle>
													<ItemStyle ForeColor="DimGray"></ItemStyle>
												</asp:BoundColumn>
												<asp:BoundColumn DataField="HotIssueStatusID" Visible="False">
													<HeaderStyle></HeaderStyle>
													<ItemStyle ForeColor="DimGray"></ItemStyle>
												</asp:BoundColumn>

												<asp:BoundColumn DataField="OriginalDate" HeaderText="Създадено на" SortExpression="OriginalDate"
													DataFormatString="{0:d}">
													<HeaderStyle Wrap="False" Width="70px"></HeaderStyle>
													<ItemStyle ForeColor="DimGray"></ItemStyle>
												</asp:BoundColumn>
												<asp:TemplateColumn SortExpression="HotIssueDeadline" HeaderText="Краен срок" ItemStyle-ForeColor="DimGray">
													<ItemTemplate>
														<asp:Label runat="server" Text='<%# Asa.Timesheet.Data.Util.TimeHelper.FormatDate((DateTime)DataBinder.Eval(Container, "DataItem.HotIssueDeadline"))%>' ID="Label5" NAME="Label1">
														</asp:Label>
													</ItemTemplate>
												</asp:TemplateColumn>
												<asp:BoundColumn DataField="AssignedTo" Visible="False">
													<HeaderStyle Wrap="False" Width="70px"></HeaderStyle>
													<ItemStyle ForeColor="DimGray"></ItemStyle>
												</asp:BoundColumn>
												<asp:BoundColumn DataField="AssignedToType" Visible="False">
													<HeaderStyle Wrap="False" Width="70px"></HeaderStyle>
													<ItemStyle ForeColor="DimGray"></ItemStyle>
												</asp:BoundColumn>
												<asp:BoundColumn HeaderText="Възложено на">
													<HeaderStyle Wrap="False" Width="70px"></HeaderStyle>
													<ItemStyle ForeColor="DimGray"></ItemStyle>
												</asp:BoundColumn>
                                               <asp:BoundColumn DataField="HotIssueStatusName" HeaderText="Статус" SortExpression="HotIssueStatusName">
													<HeaderStyle Wrap="False" Width="70px"></HeaderStyle>
													<ItemStyle ForeColor="DimGray"></ItemStyle>
												</asp:BoundColumn>
												<asp:TemplateColumn>
													<HeaderStyle Wrap="False" Width="20px"></HeaderStyle>
													<ItemStyle Wrap="False" HorizontalAlign="Center"></ItemStyle>
													<ItemTemplate>
														<asp:Image id="imgDone" runat="server" ImageUrl="images/delete.png"></asp:Image>
													</ItemTemplate>
												</asp:TemplateColumn>
												<asp:TemplateColumn>
													<HeaderStyle Wrap="False" Width="20px"></HeaderStyle>
													<ItemStyle Wrap="False" HorizontalAlign="Center"></ItemStyle>
													<ItemTemplate>
														<asp:ImageButton id="btnDelete" runat="server" Width="12px" Height="12px" ToolTip="Изтрий" ImageUrl="images/delete.gif"></asp:ImageButton>
													</ItemTemplate>
												</asp:TemplateColumn>
											</Columns>
										</asp:datagrid>
									</asp:panel>
									<TABLE id="Table4" 
										cellSpacing="0" cellPadding="4" width="100%" border="0">
										<TBODY>
											<TR>
												<td><asp:button id="btnNew" runat="server" CssClass="ActionButton" Text="Новa Задача" Width="165px"></asp:button>&nbsp;
													<asp:button id="btnBackToProject" runat="server" CssClass="ActionButton" Text="Връщане към проекта"
														Width="165px"></asp:button>&nbsp;</TD>
											</TR>
										</TBODY>
									</TABLE>
									<asp:panel id="Panel1" 
										runat="server" Width="98%" Height="450px">
										<asp:datagrid id="grdTemi" runat="server" CssClass="Grid" Width="100%" AutoGenerateColumns="False"
											CellPadding="4" PageSize="2" AllowSorting="True">
											<ItemStyle CssClass="GridItem"></ItemStyle>
											<HeaderStyle Wrap="False" CssClass="GridHeader"></HeaderStyle>
											<Columns>
												<asp:BoundColumn DataField="HotIssueCategoryID" Visible="False">
													<HeaderStyle Wrap="False" Width="70px"></HeaderStyle>
													<ItemStyle ForeColor="DimGray"></ItemStyle>
												</asp:BoundColumn>
												<asp:TemplateColumn HeaderText="Тема">
													<HeaderStyle Wrap="False" Width="30px"></HeaderStyle>
													<ItemStyle ForeColor="DimGray"></ItemStyle>
													<ItemTemplate>
														<asp:LinkButton id="Linkbutton1" runat="server" CssClass="menuTable"  CommandName="Edit">
															<span Class="menuTable">
																<%# DataBinder.Eval(Container, "DataItem.HotIssueCategoryName")%>
															</span>
														</asp:LinkButton>
													</ItemTemplate>
												</asp:TemplateColumn>
												<asp:TemplateColumn>
													<HeaderStyle Wrap="False" Width="20px"></HeaderStyle>
													<ItemStyle Wrap="False" HorizontalAlign="Center"></ItemStyle>
													<ItemTemplate>
														<asp:ImageButton id="btnDelete" runat="server" Width="12px" Height="12px" ToolTip="Изтрий" ImageUrl="images/delete.gif"
															ite></asp:ImageButton>
													</ItemTemplate>
												</asp:TemplateColumn>
											</Columns>
										</asp:datagrid>
									</asp:panel>
								</td>
							</tr>
						</table>
					</td>
				</tr>
			</table>
		</form>
	</body>
</HTML>
