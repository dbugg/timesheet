using System;
using System.Collections;
using System.ComponentModel;
using System.Configuration;
using System.Globalization;
using System.IO;
using System.Reflection;
using System.Text;
using System.Web.Mail;
using System.Web.UI;
using System.Web.UI.WebControls;
using Asa.Timesheet.Data;
using Asa.Timesheet.Data.Entities;
using log4net;
using System.Web;
namespace PM
{
	/// <summary>
	/// Summary description for BasePage.
	/// </summary>
	public class BasePage: System.Web.UI.Page
	{

		private UserInfoPM _loggedUser = null; 
		//private int _sortOrder = 0;
		private static readonly ILog log = LogManager.GetLogger(typeof(BasePage));
        private PM.UserControls.PageHeader header;


        protected PM.UserControls.PageHeader Header
        {
            get
            {
                if (header == null)
                {
                    header = (PM.UserControls.PageHeader)this.Master.FindControl("header");
                }
                return header;
            }
        }

		
	
		protected string Language
		{
			get
			{
				if(Session["lang"] == null)
					Session["lang"] = Languages.BG.ToString();
				return (string)Session["lang"];
			}
			set
			{
				Session["lang"] = value;
			}
		}

		protected UserInfoPM LoggedUser
		{
			get 
			{	
				if (_loggedUser == null)
				{
					try
					{
						_loggedUser = SessionManager.LoggedUserInfoPM;
						return _loggedUser;
					}
					catch (Exception ex)
					{
						log.Error(ex);
						//_loggedUser = new UserInfo();
						ErrorRedirect(String.Concat(Resource.ResourceManager["error_GetLoggedUserInfo"],"<br>",
							Resource.ResourceManager["error_HitRefresh"]));
					}
				}
				return _loggedUser;
			}
		}

		protected int SortOrder
		{
			get 
			{
				if (this.ViewState["SO"] != null)
				{
					return int.Parse(this.ViewState["SO"].ToString());
				}
				else return 0;
			}
			set 
			{
				this.ViewState["SO"] = value;
			}
		}
		
		protected string OldAccName
		{
			get 
			{
				if (this.Session["accName"] != null&&this.Session["accName"] is string)
					return (string)this.Session["accName"];
				else
					return "";
			}
			set 
			{
				this.Session["accName"] = value;
			}
		}
		protected string OldAccPass
		{
			get 
			{
				if (this.Session["accPass"] != null&&this.Session["accPass"] is string)
					return (string)this.Session["accPass"];
				else
					return "";
			}
			set 
			{
				this.Session["accPass"] = value;
			}
		}
//		protected PaymentsVector SessionTable
//		{
//			get 
//			{
//				if (this.Session["SessionTable"] != null)
//				{
//					return (PaymentsVector)this.Session["SessionTable"];
//				}
//				PaymentsVector pv =  new PaymentsVector();
//				this.Session["SessionTable"]=pv;
//				return pv;
//			}
//			set 
//			{
//				this.Session["SessionTable"] = value;
//			}
//		}
//		protected ContentsVector DistrTable
//		{
//			get 
//			{
//				if (this.Session["DistrTable"] != null)
//				{
//					return (ContentsVector)this.Session["DistrTable"];
//				}
//				ContentsVector pv =  new ContentsVector();
//				this.Session["DistrTable"]=pv;
//				return pv;
//			}
//			set 
//			{
//				this.Session["DistrTable"] = value;
//			}
//		}
//		protected ChecksVector ChecksTable
//		{
//			get 
//			{
//				if (this.Session["ChecksTable"] != null)
//				{
//					return (ChecksVector)this.Session["ChecksTable"];
//				}
//				ChecksVector pv =  new ChecksVector();
//				this.Session["ChecksTable"]=pv;
//				return pv;
//			}
//			set 
//			{
//				this.Session["ChecksTable"] = value;
//			}
//		}
//		protected ContentsVector ContentsTable
//		{
//			get 
//			{
//				if (this.Session["ContentsTable"] != null)
//				{
//					return (ContentsVector)this.Session["ContentsTable"];
//				}
//				ContentsVector pv =  new ContentsVector();
//				this.Session["ContentsTable"]=pv;
//				return pv;
//			}
//			set 
//			{
//				this.Session["ContentsTable"] = value;
//			}
//		}
//		protected ContentsVector ContentsTableNew
//		{
//			get 
//			{
//				if (this.Session["ContentsTableNew"] != null)
//				{
//					return (ContentsVector)this.Session["ContentsTableNew"];
//				}
//				ContentsVector pv =  new ContentsVector();
//				this.Session["ContentsTableNew"]=pv;
//				return pv;
//			}
//			set 
//			{
//				this.Session["ContentsTableNew"] = value;
//			}
//		}
//		protected bool PlusMinus
//		{
//			get
//			{
//				if (this.Session["PlusMinus"] != null)
//				{
//					return (bool)this.Session["PlusMinus"];
//				}
//				bool b = false;
//				this.Session["PlusMinus"]=b;
//				return b;
//			}
//			set
//			{
//				this.Session["PlusMinus"] = value;
//			}
//		}
//		protected ProjectSubcontracterActivitiesVector SubcontracterActivities
//		{
//			get 
//			{
//				if (this.Session["SubcontracterActivities"] != null)
//				{
//					return (ProjectSubcontracterActivitiesVector)this.Session["SubcontracterActivities"];
//				}
//				else 
//				{
//					ProjectSubcontracterActivitiesVector psa = new ProjectSubcontracterActivitiesVector();
//					Session["SubcontracterActivities"]=psa;
//					return psa;
//				}
//			}
//			set 
//			{
//				this.Session["SubcontracterActivities"] = value;
//			}
//		}
		public static void ErrorRedirect(string errorMessage)	
		{
			if (errorMessage == null) errorMessage = string.Empty;
			HttpContext context = HttpContext.Current;
			//string url = "Error.aspx?ErrorText=" + context.Server.UrlEncode(errorMessage);
			//context.Server.Transfer(url);
			context.Session["ErrorRedirectMessage"] = errorMessage;
		
			context.Server.Transfer(context.Request.ApplicationPath+"/Error.aspx");
			//context.Response.Redirect("AsaTimesheet/error.aspx");
			
		}

		protected static void AlertFieldNotEntered(System.Web.UI.WebControls.Label lbl)
		{
			lbl.ForeColor = System.Drawing.Color.Red;
		}

		protected static void SetConfirmDelete(System.Web.UI.WebControls.WebControl ctrl, string deleteItemName)
		{
			System.Text.StringBuilder js = new System.Text.StringBuilder();
			js.Append("return confirm('");
			js.Append(Resource.ResourceManager["grid_ConfirmDelete"]);
			js.Append(" ");
			js.Append(deleteItemName);
			js.Append("?');");

			ctrl.Attributes["onclick"] = js.ToString();
		}

		protected static void SetConfirmDelete(System.Web.UI.WebControls.DataGrid grid, string btnDeleteID, int itemNameColumn)
		{
			for (int i=0; i<grid.Items.Count; i++)
			{
				string itemName = grid.Items[i].Cells[itemNameColumn].Text;
				itemName = itemName.Replace("'","");
				string js = String.Concat("return confirm('", Resource.ResourceManager["grid_ConfirmDelete"]," ",itemName, "?');");

				System.Web.UI.WebControls.WebControl btn = (System.Web.UI.WebControls.WebControl)grid.Items[i].FindControl(btnDeleteID);
				btn.Attributes["onclick"] = js;
			}
		}

		protected static void SetConfirmDelete(System.Web.UI.WebControls.DataGrid grid, string btnDeleteID, string itemName)
		{
			for (int i=0; i<grid.Items.Count; i++)
			{
				string js = String.Concat("return confirm('", Resource.ResourceManager["grid_ConfirmDelete"]," ",itemName, "?');");

				System.Web.UI.WebControls.WebControl btn = (System.Web.UI.WebControls.WebControl)grid.Items[i].FindControl(btnDeleteID);
				btn.Attributes["onclick"] = js;
			}
		}
		protected static void SetConfirmRestore(System.Web.UI.WebControls.DataGrid grid, string btnRestoreID, string itemName)
		{
			for (int i=0; i<grid.Items.Count; i++)
			{
				string js = String.Concat("return confirm('", Resource.ResourceManager["grid_ConfirmRestore"]," ",itemName, "?');");

				System.Web.UI.WebControls.WebControl btn = (System.Web.UI.WebControls.WebControl)grid.Items[i].FindControl(btnRestoreID);
				btn.Attributes["onclick"] = js;
			}
		}
		
		// Constants

		/// <summary>
		/// This constant represents the default name for the message link
		/// CSS class.  The default value is the class name <b>ErrorMsgLink</b>.
		/// </summary>
		/// <seealso cref="MsgLinkCssClass"/>
		/// <seealso cref="MakeMsgLink"/>
		public const string MsgLinkCssName = "ErrorMsgLink";

		/// <summary>
		/// This constant represents the default name for the disabled state
		/// CSS class.  The default value is the class name <b>Disabled</b>.
		/// </summary>
		/// <seealso cref="DisabledCssClass"/>
		/// <seealso cref="SetEnabledState(WebControl, bool)"/>
		public const string DisabledCssName = "Disabled";

		//=====================================================================
		// Private class members

		// This is a reference to the form that this page contains.  If the
		// page contains no form, the control points to the page itself.
		private Control ctlForm;

		// The control that should have focus when the page finishes loading
		private string focusedControl;
		//private bool findControl;

		// The dirty state flag for data change checking
		private bool isDirty;

		// Message link CSS class name.  Default as shown.
		private string msgLinkClass = BasePage.MsgLinkCssName;

		// The CSS class name to use for disabled controls
		private string disabledClass = BasePage.DisabledCssName;

		// This controls whether or not the page e-mails itself
		private bool emailRenderedPage;//, isRenderingForEMail;

		//=====================================================================
		// Properties

		/// <summary>The message link CSS class name</summary>
		/// <value>This is used to get/set the message link CSS class applied
		/// by default in the <see cref="MakeMsgLink"/> method.  Set it to
		/// String.Empty to turn it off.  The class should appear in the style
		/// sheet file associated with the application.</value>
		public string MsgLinkCssClass
		{
			get { return msgLinkClass; }
			set
			{
				if(value == null)
					msgLinkClass = BasePage.MsgLinkCssName;
				else
					msgLinkClass = value;
			}
		}

		/// <summary>The disabled control CSS class name</summary>
		/// <value>This is used to get/set the CSS class for disabled controls.
		/// The class should appear in the style sheet file associated with
		/// the application.</value>
		public string DisabledCssClass
		{
			get { return disabledClass; }
			set
			{
				if(value == null)
					disabledClass = BasePage.DisabledCssName;
				else
					disabledClass = value;
			}
		}

		/// <summary>
		/// This property is used to set or get a reference to the form
		/// control that appears on the page as indicated by the presence
		/// of a &lt;form&gt; tag.
		/// </summary>
		/// <value>If not explicitly set, it defaults to the form on
		/// the page or the page itself if there isn't one.
		/// <p/>Derived classes can use this property to access the form
		/// in order to insert additional controls into it.  For example,
		/// the derived <see cref="MenuPage"/> classes use it to insert the
		/// supporting table structure and the menu control around the form
		/// that makes up the page's content.</value>
		/// <exception cref="System.ArgumentException">
		/// This property must be set to an
		/// <see cref="System.Web.UI.HtmlControls.HtmlForm"/> or a
		/// <see cref="System.Web.UI.Page"/> object or it will throw an
		/// exception.</exception>
		[Browsable(false),
		DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
		public Control PageForm
		{
			get { return ctlForm; }
			set
			{
				// Must be derived from one of these types
				if(value is System.Web.UI.HtmlControls.HtmlForm ||
					value is System.Web.UI.Page)
					ctlForm = value;
				else
					throw new ArgumentException(
						"PageForm must be set to an HtmlForm or Page object");
			}
		}

		/// <summary>
		/// This is used to get the authentication method in effect for
		/// the application.
		/// </summary>
		/// <remarks>This property is quite useful in determining whether
		/// or not things are working as expected with regard to
		/// authentication.  To distinguish between NTLM and Kerberos
		/// authentication, it relies on the length of the
		/// <b>HTTP_AUTHORIZATION</b> server variable.  NTLM headers are
		/// much shorter than Kerberos headers.</remarks>
		/// <value>
		/// <list type="table">
		///    <listheader>
		///       <term>Return Value</term>
		///       <description>Authentication Type</description>
		///    </listheader>
		///    <item>
		///       <term>Anonymous</term>
		///       <description>Anonymous access</description>
		///    </item>
		///    <item>
		///       <term>Basic</term>
		///       <description>Basic authentication</description>
		///    </item>
		///    <item>
		///       <term>NTLM</term>
		///       <description>NTLM authentication</description>
		///    </item>
		///    <item>
		///       <term>Kerberos</term>
		///       <description>Kerberos authentication</description>
		///    </item>
		///    <item>
		///       <term>Negotiate</term>
		///       <description>NTLM or Kerberos.  Typically, when using
		/// these two types of authentication, this property only returns
		/// useful information on the first page accessed in the application.
		/// On subsequent requests, the authorization header is blank and it
		/// can only tell that some form of negotiated authentication was used.
		/// In those cases, <b>Negotiate</b> is returned.</description>
		///    </item>
		/// </list>
		/// </value>
//		[Browsable(false)]
//		public string AuthType
//		{
//			get
//			{
//				// This prevents an exception being reported in design view
//				if(this.Context == null)
//					return null;
//
//				// Figure out the authentication type
//				string authType = Request.ServerVariables["AUTH_TYPE"];
//
//				if(authType == "Negotiate")
//				{
//					// Typically, NTLM will yield a header that is 300 bytes
//					// or less while Kerberos is more like 5000 bytes.
//					// If blank, the best we can do is return "Negotiate".
//					string authorization = Request.ServerVariables["HTTP_AUTHORIZATION"];
//
//					if(authorization != null)
//						if(authorization.Length > 1000)
//							authType = "Kerberos";
//						else
//							authType = "NTLM";
//				}
//				else    // If length != 0, it's probably Basic authentication
//					if(authType.Length == 0)
//					authType = "Anonymous";
//
//				return authType;
//			}
//		}

		/// <summary>
		/// This property can be used to get the current user ID without the
		/// domain qualifier if one is present.
		/// </summary>
		/// <remarks>This property is only useful when using basic or
		/// integrated security with your web application.  It is useful
		/// for auditing purposes or looking up security related information
		/// and saves you from having to manually remove the domain name from
		/// the user ID.</remarks>
		/// <value>Returns the value of the <b>User.Identity.Name</b>
		/// property without the domain qualifier.  For example if it
		/// is <b>MYDOMAIN\EWOODRUFF</b>, this property returns
		/// <b>EWOODRUFF</b>.</value>
//		[Browsable(false)]
//		public string CurrentUser
//		{
//			get
//			{
//				// This prevents an exception being reported in design view
//				if(this.Context == null)
//					return null;
//
//				string strUser = User.Identity.Name;
//				int nPos = strUser.IndexOf('\\');
//
//				if(nPos != -1)
//					strUser = strUser.Substring(nPos + 1);
//
//				return strUser;
//			}
//		}

		/// <summary>
		/// Indicate whether or not to check for changed data entry controls.
		/// </summary>
		/// <remarks>This property is used to indicate whether or not the page
		/// should check for changes in data entry controls and track the dirty
		/// state before leaving the page in some manner (i.e. by clicking
		/// an exit button, clicking the browser's back or forward buttons,
		/// navigating to a different URL, or closing the browser).  If set
		/// to true, the page will render additional JavaScript to help ensure
		/// that the user is prompted that they will lose their changes to
		/// the controls on the page and it gives them an option to cancel
		/// leaving and stay on the current page or continuing and lose the
		/// changes <b>(Internet Explorer only)</b>.  For non-IE browsers,
		/// only dirty state tracking is available.</remarks>
		/// <seealso cref="Dirty"/>
		/// <seealso cref="ConfirmLeaveMessage"/>
		/// <seealso cref="BypassPromptIds"/>
		/// <seealso cref="SkipDataCheckIds"/>
//		public bool CheckForDataChanges
//		{
//			get
//			{
//				Object oCheck = ViewState["CheckForDataChanges"];
//				return (oCheck == null) ? false : (bool)oCheck;
//			}
//			set { ViewState["CheckForDataChanges"] = value; }
//		}

		/// <summary>
		/// This property is used to track the dirty state of the data on
		/// a form if change checking is enabled.
		/// </summary>
		/// <remarks>This property is used to track the dirty state of a
		/// data entry web form.  It can also be used to force the page to
		/// a dirty state so that the <see cref="ConfirmLeaveMessage"/>
		/// is always shown <b>(Internet Explorer only)</b>. This is useful
		/// if the form contains auto-postback controls such as a checkbox
		/// or a button that causes other controls to get enabled or disabled,
		/// get set to specific values, etc.  In such cases, it is impossible
		/// to detect changes, as we no longer have the original values from
		/// before the postback.  Setting this property to true in the event
		/// handler of postback controls will ensure that the user is prompted
		/// to save their changes.</remarks>
		/// <seealso cref="CheckForDataChanges"/>
		/// <seealso cref="ConfirmLeaveMessage"/>
		/// <seealso cref="BypassPromptIds"/>
		/// <seealso cref="SkipDataCheckIds"/>
		[Browsable(false)]
		public bool Dirty
		{
			get { return isDirty; }
			set { isDirty = value; }
		}

		/// <summary>
		/// This property is used to set or get the message displayed if
		/// change checking is enabled and the user attempts to leave without
		/// saving the changes <b>(Internet Explorer only)</b>.  A default
		/// message is used if not set.
		/// </summary>
		/// <seealso cref="CheckForDataChanges"/>
		/// <seealso cref="Dirty"/>
		/// <seealso cref="BypassPromptIds"/>
		/// <seealso cref="SkipDataCheckIds"/>
//		public string ConfirmLeaveMessage
//		{
//			get
//			{
//				Object oLeave = ViewState["ConfirmLeaveMsg"];
//				return (oLeave != null) ? (string)oLeave :
//					"You haven't saved your changes.  Leaving now " +
//					"will lose all changes made.";
//			}
//			set { ViewState["ConfirmLeaveMsg"] = value; }
//		}

		/// <summary>
		/// This is used to set a list of control IDs that should not
		/// trigger the data change check (i.e. a save button).
		/// </summary>
		/// <value>Set it to a string array of control ID values.  Any
		/// controls with IDs specified in this list will allow postback or
		/// leaving of the page without prompting to save <b>(Internet
		/// Explorer only)</b>.</value>
		/// <seealso cref="CheckForDataChanges"/>
		/// <seealso cref="Dirty"/>
		/// <seealso cref="ConfirmLeaveMessage"/>
		/// <seealso cref="SkipDataCheckIds"/>
		/// <example>
		/// C#:
		/// <code>
		/// this.BypassPromptIds = new string[] { "cboApplication",
		///     "chkLimitToTeam", "cboGroupKey", "btnSave",
		///     "btnDelete", "btnCancel" };
		/// </code>
		/// VB.NET:
		/// <code>
		/// Me.BypassPromptIds = New String() { "cboApplication", _
		///     "chkLimitToTeam", "cboGroupKey", "btnSave", _
		///     "btnDelete", "btnCancel" }
		/// </code>
		/// </example>
//		public string [] BypassPromptIds
//		{
//			get
//			{
//				Object oBypass = ViewState["BypassList"];
//				return (oBypass != null) ? (string [])oBypass : null;
//			}
//			set { ViewState["BypassList"] = value; }
//		}

		/// <summary>
		/// This is used to set a list of control IDs that should not
		/// be included when checking for data changes (i.e. changeable
		/// message text boxes, read-only or criteria fields that get
		/// modified but do not affect the state of the data to save, etc).
		/// </summary>
		/// <value>Set it to a string array of control ID values.  Any
		/// controls with IDs specified in this list will not affect the
		/// dirty state of the page <b>(any browser)</b> and will not
		/// cause prompting when leaving the page <b>(Internet Explorer
		/// only)</b>.</value>
		/// <seealso cref="CheckForDataChanges"/>
		/// <seealso cref="Dirty"/>
		/// <seealso cref="ConfirmLeaveMessage"/>
		/// <seealso cref="BypassPromptIds"/>
		/// <example>
		/// C#:
		/// <code>
		/// this.SkipDataCheckIds = new string[] { "cboDept", "cboEmployee",
		///     "txtEntryDate" };
		/// </code>
		/// VB.NET:
		/// <code>
		/// Me.SkipDataCheckIds = New String() { "cboDept", "cboEmployee", _
		///     "txtEntryDate"  }
		/// </code>
		/// </example>
//		public string [] SkipDataCheckIds
//		{
//			get
//			{
//				Object oSkipList = ViewState["SkipList"];
//				return (oSkipList != null) ? (string [])oSkipList : null;
//			}
//			set { ViewState["SkipList"] = value; }
//		}

		/// <summary>
		/// This property is used to indicate whether or not the page
		/// should e-mail itself to someone when rendered.
		/// </summary>
		/// <value>Set it to true to have the page e-mail itself.  It is
		/// set to false by default.  In order to send the page, you must
		/// handle the <see cref="EMailThisPage"/> event in your derived
		/// class.</value>
		/// <remarks>If this property is set to true at some point before the
		/// page renders itself, it will build an e-mail message containing
		/// the contents of the rendered page.  It will raise a custom event
		/// (<see cref="EMailThisPage"/>) so that the derived page can set the
		/// sender, recipient, subject, and other items as well as modify the
		/// e-mail's content and the information rendered to the client.  It
		/// also raises a custom error event (<see cref="EMailError"/>) if the
		/// e-mail cannot be sent thus giving the page a chance to take
		/// alternate actions.</remarks>
		public bool EMailRenderedPage
		{
			get { return emailRenderedPage; }
			set { emailRenderedPage = value; }
		}

		/// <summary>
		/// This read-only property can be used to determine if the page is
		/// currently in the process of rendering itself for e-mailing.
		/// </summary>
		/// <remarks>This is used by the <b>Render</b> method to determine
		/// whether or not it needs to check to see if it needs to render
		/// for e-mailing or if it is already doing so.</remarks>
//		[Browsable(false)]
//		public bool IsRenderingForEMail
//		{
//			get { return isRenderingForEMail; }
//		}

		/// <summary>
		/// The page title property.  This will be rendered in a &lt;title&gt;
		/// tag in the &lt;head&gt; section of the page's HTML.
		/// </summary>
		/// <value>Unlike <b>Page.Title</b>, this value is stored in view state
		/// so that it can be modified by events or other methods in derived
		/// classes.  If not specified, the page will not have a title unless
		/// you add it to the master page's HTML.</value>
//		public string PageTitle
//		{
//			get { return (string)ViewState["PageTitle"]; }
//			set { ViewState["PageTitle"] = value; }
//		}

		/// <summary>
		/// This property is used to set or get the information for the page
		/// description meta tag that is rendered in the <b>&lt;head&gt;</b>
		/// section.
		/// </summary>
		/// <value>The value is stored in view state so that it can be
		/// modified by events or other methods in derived classes.</value>
//		public string PageDescription
//		{
//			get
//			{
//				Object oDesc = ViewState["PageDesc"];
//				return (string)oDesc;
//			}
//			set { ViewState["PageDesc"] = value; }
//		}

		/// <summary>
		/// This property is used to set or get the information for the page
		/// keywords meta tag that is rendered in the <b>&lt;head&gt;</b>
		/// section.
		/// </summary>
		/// <value>The value is stored in view state so that it can be
		/// modified by events or other methods in derived classes.</value>
//		public string PageKeywords
//		{
//			get
//			{
//				Object oKeywords = ViewState["PageKeywords"];
//				return (string)oKeywords;
//			}
//			set { ViewState["PageKeywords"] = value; }
//		}

		/// <summary>
		/// This property is used to set or get the value for the robot
		/// instructions meta tag that is rendered in the <b>&lt;head&gt;</b>
		/// section.
		/// </summary>
		/// <value>The default is <see cref="RobotOptions.NotSet"/> which
		/// will skip rendering of the tag.  The value is stored in view state
		/// so that it can be modified by events or other methods in derived
		/// classes.</value>
//		public RobotOptions Robots
//		{
//			get
//			{
//				Object oRobots = ViewState["Robots"];
//				return (oRobots == null) ? RobotOptions.NotSet :
//					(RobotOptions)oRobots;
//			}
//			set { ViewState["Robots"] = value; }
//		}

		//=====================================================================
		// Events

		/// <summary>
		/// The delegate for the <see cref="EMailThisPage"/> event
		/// </summary>
		/// <param name="sender">The sender of the event</param>
		/// <param name="e">The event arguments</param>
		public delegate void EMailThisPageEventHandler(Object sender,
			EMailPageEventArgs e);

		/// <summary>
		/// This event is raised when the page wants to e-mail itself.  An
		/// event handler in the derived class should fill in the sender,
		/// recipient, subject, and any other items on the message.
		/// </summary>
		public event EMailThisPageEventHandler EMailThisPage;

		/// <summary>
		/// This raises the <see cref="EMailThisPage"/> event
		/// </summary>
		/// <param name="e">The event arguments</param>
		protected virtual void OnEMailThisPage(EMailPageEventArgs e)
		{
			if(EMailThisPage != null)
				EMailThisPage(this, e);
		}

		/// <summary>
		/// The delegate for the <see cref="EMailError"/> event
		/// </summary>
		/// <param name="sender">The sender of the event</param>
		/// <param name="e">The event arguments</param>
		public delegate void EMailErrorEventHandler(Object sender,
			EMailErrorEventArgs e);

		/// <summary>
		/// This event is raised when there is an error trying to send the
		/// e-mail.  It gives the derived page a chance to take alternate
		/// action.
		/// </summary>
		public event EMailErrorEventHandler EMailError;

		/// <summary>
		/// This raises the <see cref="EMailError"/> event
		/// </summary>
		/// <param name="e">The event arguments</param>
		protected virtual void OnEMailError(EMailErrorEventArgs e)
		{
			if(EMailError != null)
				EMailError(this, e);
		}

		//=====================================================================
		// Private class methods

		/// <summary>
		/// This is used to find the form on the page
		/// </summary>
		private static Control FindPageForm(Page page)
		{
			Control form = null;

			// Find the form on this page if there is one
			foreach(Control ctlItem in page.Controls)
				if(ctlItem is System.Web.UI.HtmlControls.HtmlForm)
				{
					form = ctlItem;
					break;
				}

			return form;
		}

		//=====================================================================
		// Methods, etc

		/// <summary>
		/// Default constructor
		/// </summary>
		/// <remarks>The constructor defaults the <see cref="PageForm"/>
		/// property to the page itself.  The <see cref="OnInit"/> method will
		/// attempt to locate the form control and set <see cref="PageForm"/>
		/// to it when it is called.</remarks>
		public  BasePage()
		{
			//ctlForm = this;
		}

		/// <summary>
		/// This sets the control that should have the focus when the page
		/// has finished loading by control reference.
		/// </summary>
		/// <param name="ctl">The control to give focus</param>
		/// <remarks>Use this for controls that are children of the form
		/// control and are not embedded within other controls such as data
		/// grids.
		/// <p/>To clear the focus, pass null (Nothing in VB.NET) to this
		/// method.  When doing so, you will need to use a cast to indicate
		/// that it is of the <b>WebControl</b> type due to the overload.</remarks>
		/// <overloads>This method has two overloads.</overloads>
		/// <example>
		/// C#:
		/// <code>
		/// // txtName is a control on the form
		/// this.SetFocusExtended(txtName);
		///
		/// // Clear the focus
		/// this.SetFocusExtended((WebControl)null);
		/// </code>
		/// VB.NET:
		/// <code>
		/// ' txtName is a control on the form
		/// Me.SetFocusExtended(txtName)
		///
		/// ' Clear the focus
		/// Me.SetFocusExtended(CType(Nothing, WebControl));
		/// </code>
		/// </example>
		public void SetFocusExtended(WebControl ctl)
		{
			if(ctl != null)
			{
				focusedControl = ctl.ClientID;
				//findControl = false;
			}
			else
				focusedControl = null;
		}

		/// <summary>
		/// This sets the control that should have the focus when the page
		/// has finished loading by control ID.
		/// </summary>
		/// <param name="clientId">The ID of the control to give focus</param>
		/// <remarks>This version is useful for setting the focus to a control
		/// in a data grid's edit item template.  The control doesn't always
		/// exist when you want to set focus so this allows it to be set by
		/// control ID.  The grid mangles the name based on the row so it
		/// will be located by searching for the ID ending in the specified
		/// value.
		/// <p/>To clear the focus, pass null (Nothing in VB.NET) to this
		/// method.  When doing so, you will need to use a cast to indicate
		/// that it is of the <b>String</b> type due to the overload.</remarks>
		/// <example>
		/// C#:
		/// <code>
		/// // If embedded in a data grid, the control ID when rendered will
		/// // be something like dgGrid:_ctl5:txtName.
		/// this.SetFocusExtended("txtName");
		///
		/// // Clear the focus
		/// this.SetFocusExtended((string)null);
		/// </code>
		/// VB.NET:
		/// <code>
		/// ' If embedded in a data grid, the control ID when rendered will
		/// ' be something like dgGrid:_ctl5:txtName.
		/// Me.SetFocusExtended("txtName")
		///
		/// ' Clear the focus
		/// Me.SetFocusExtended(CType(Nothing, String));
		/// </code>
		/// </example>
		public void SetFocusExtended(string clientId)
		{
			focusedControl = clientId;
			//findControl = true;
		}

		/// <summary>
		/// This method can be used to enable or disable a control and change
		/// the CSS style for the disabled state.  No class is used for the
		/// enabled state.
		/// </summary>
		/// <param name="ctl">The control to enable/disable</param>
		/// <param name="enabled">Pass true to enable, false to disable</param>
		/// <remarks>This version uses the current value of the
		/// <see cref="DisabledCssClass"/> property when disabling a control.
		/// If being enabled, the style is simply removed.</remarks>
		/// <exception cref="ArgumentNullException">This is thrown if the
		/// specified control is null</exception>
		/// <overloads>This method has four overloads.</overloads>
		/// <example>
		/// C#:
		/// <code>
		/// // txtName is a control on the form that is to be disabled
		/// this.SetEnabledState(txtName, false);
		/// </code>
		/// VB.NET:
		/// <code>
		/// ' txtName is a control on the form that is to be disabled
		/// Me.SetEnabledState(txtName, False);
		/// </code>
		/// </example>
		public void SetEnabledState(WebControl ctl, bool enabled)
		{
			this.SetEnabledState(ctl, enabled, String.Empty);
		}

		/// <summary>
		/// This method can be used to enable or disable a control and change
		/// the CSS style for the enabled and disabled states.
		/// </summary>
		/// <param name="ctl">The control to enable/disable</param>
		/// <param name="enabled">Pass true to enabled, false to disable</param>
		/// <param name="normalClass">The CSS class to apply to enabled controls</param>
		/// <remarks>This version uses the current value of the
		/// <see cref="DisabledCssClass"/> property when disabling a control.
		/// If being enabled, is uses the specified class name.</remarks>
		/// <exception cref="ArgumentNullException">This is thrown if the
		/// specified control is null</exception>
		/// <example>
		/// C#:
		/// <code>
		/// // txtName is a control on the form that is to be enabled and
		/// // is to use a specific CSS style class for the enabled state.
		/// this.SetEnabledState(txtName, true, "TextEnabled");
		/// </code>
		/// VB.NET:
		/// <code>
		/// ' txtName is a control on the form that is to be enabled and
		/// ' is to use a specific CSS style class for the enabled state.
		/// Me.SetEnabledState(txtName, True, "TextEnabled");
		/// </code>
		/// </example>
		public void SetEnabledState(WebControl ctl, bool enabled,
			string normalClass)
		{
			if(ctl == null)
				throw new ArgumentNullException("ctl", "The control cannot be null");

			ctl.Enabled = enabled;

			if(ctl is System.Web.UI.WebControls.TextBox ||
				ctl is System.Web.UI.WebControls.DropDownList ||
				ctl is System.Web.UI.WebControls.ListBox)
				if(enabled)
					ctl.CssClass = normalClass;
				else
					ctl.CssClass = this.DisabledCssClass;
		}

		/// <summary>
		/// This method can be used to enable or disable multiple controls
		/// and change the CSS style for the disabled state.  No class is
		/// used for the enabled state.
		/// </summary>
		/// <param name="enabled">Pass true to enabled, false to disable</param>
		/// <param name="ctlList">A set of the controls to enable/disable</param>
		/// <remarks>This version uses the current value of the
		/// <see cref="DisabledCssClass"/> property when disabling a control.
		/// If being enabled, the style is simply removed.</remarks>
		/// <exception cref="ArgumentNullException">This is thrown if the
		/// specified control array is null</exception>
		/// <example>
		/// C#:
		/// <code>
		/// this.SetEnabledState(false, this.ctl1, this.ctl2, this.ctl3);
		/// </code>
		/// VB.NET:
		/// <code>
		/// Me.SetEnabledState(False, Me.ctl1, Me.ctl2, Me.ctl3);
		/// </code>
		/// </example>
		public void SetEnabledState(bool enabled, params WebControl[] ctlList)
		{
			this.SetEnabledState(String.Empty, enabled, ctlList);
		}

		/// <summary>
		/// This method can be used to enable or disable multiple controls
		/// and change the CSS style for the enabled or disabled state.
		/// </summary>
		/// <param name="normalClass">The CSS class to apply to enabled controls</param>
		/// <param name="enabled">Pass true to enabled, false to disable</param>
		/// <param name="ctlList">A set of the controls to enable/disable</param>
		/// <remarks>This version uses the current value of the
		/// <see cref="DisabledCssClass"/> property when disabling a control.
		/// If being enabled, is uses the specified class name.</remarks>
		/// <exception cref="ArgumentNullException">This is thrown if the
		/// specified control array is null</exception>
		/// <example>
		/// C#:
		/// <code>
		/// this.SetEnabledState("EnabledStyle", true, this.ctl1,
		///     this.ctl2, this.ctl3);
		/// </code>
		/// VB.NET:
		/// <code>
		/// Me.SetEnabledState("EnabledStyle", True, Me.ctl1, _
		///     Me.ctl2, Me.ctl3);
		/// </code>
		/// </example>
		public void SetEnabledState(string normalClass, bool enabled,
			params WebControl[] ctlList)
		{
			if(ctlList == null)
				throw new ArgumentNullException("ctlList",
					"Control array cannot be null");

			foreach(WebControl ctl in ctlList)
			{
				ctl.Enabled = enabled;

				if(ctl is System.Web.UI.WebControls.TextBox ||
					ctl is System.Web.UI.WebControls.DropDownList ||
					ctl is System.Web.UI.WebControls.ListBox)
					if(enabled)
						ctl.CssClass = normalClass;
					else
						ctl.CssClass = this.DisabledCssClass;
			}
		}

		/// <summary>
		/// This can be used to disable or enable all edit controls on a web
		/// page, form, panel, or tab control.
		/// </summary>
		/// <param name="enabled">Set to true to enable, false to disable</param>
		/// <param name="ctlPageForm">The form, page, or other control
		/// containing the controls to disable or enable.  Normally, you will
		/// pass the value of the <see cref="PageForm"/> property.</param>
		/// <remarks>It looks a bit odd if all controls have the disabled style
		/// applied and they can be hard to read.  As such, this just disables
		/// the controls and doesn't change their style.  Buttons and links
		/// are not disabled by this method.
		/// <p/>This method is aware of the Microsoft Internet Explorer
		/// Web Controls <b>MultiPage</b> and <b>PageView</b> and will
		/// also enable or disable controls contained within them.  Note that
		/// there is no dependency on that assembly due to the way the support
		/// for it has been implemented.</remarks>
		public void SetEnabledAll(bool enabled, Control ctlPageForm)
		{
			Control form = null;
			string controlType;

			// If null, default to the current page
			if(ctlPageForm == null)
				ctlPageForm = this.PageForm;

			// Yes, I could add a reference to the MS IE Web Controls, but
			// I don't want this library to have a dependency on it so we'll
			// just check for IE Web Controls by type name string instead.
			controlType = ctlPageForm.ToString();

			// If passed a form, panel, multi-page, or page view, use it
			// directly.  If passed a page, see if it contains a form.  If so,
			// use that form.  If not, use the page.
			if(ctlPageForm is System.Web.UI.HtmlControls.HtmlForm ||
				ctlPageForm is System.Web.UI.WebControls.Panel ||
				controlType.IndexOf("MultiPage") != -1 ||
				controlType.IndexOf("PageView") != -1)
			{
				form = ctlPageForm;
			}
			else
				if(ctlPageForm is System.Web.UI.Page &&
				ctlPageForm != this.PageForm)
				form = BasePage.FindPageForm((Page)ctlPageForm);

			// Ignore anything unexpected
			if(form == null)
				return;

			// Disable each edit control on the page
			foreach(Control ctl in form.Controls)
				if(ctl is System.Web.UI.WebControls.TextBox ||
					ctl is System.Web.UI.WebControls.DropDownList ||
					ctl is System.Web.UI.WebControls.ListBox ||
					ctl is System.Web.UI.WebControls.CheckBox ||
					ctl is System.Web.UI.WebControls.CheckBoxList ||
					ctl is System.Web.UI.WebControls.RadioButton ||
					ctl is System.Web.UI.WebControls.RadioButtonList)
					((WebControl)ctl).Enabled = enabled;
				else
				{
					// As above, done this way to avoid a dependency
					controlType = ctl.ToString();
					if(ctl is System.Web.UI.WebControls.Panel ||
						controlType.IndexOf("MultiPage") != -1 ||
						controlType.IndexOf("PageView") != -1)
						this.SetEnabledAll(enabled, ctl);   // Recursive for these
				}
		}

		/// <summary>
		/// This is used to turn a message into a hyperlink so that clicking
		/// on the message will set the focus to the specified control.
		/// </summary>
		/// <param name="id">The ID of the control to focus when the message
		/// is clicked</param>
		/// <param name="msg">The message to turn into a link</param>
		/// <param name="cssClass">The CSS class name to use on the link,
		/// if any.  If passed a null, it uses the value of the
		/// <see cref="MsgLinkCssClass"/> property.</param>
		/// <returns>The specified message as a hyperlink tag</returns>
		/// <remarks>This is useful for validator messages displayed in a
		/// <see cref="System.Web.UI.WebControls.ValidationSummary"/> control.
		/// Clicking the error message takes you straight to the offending
		/// control in the form.</remarks>
		/// <example>
		/// C#:
		/// <code>
		/// // Convert a validator error message to a hyperlink
		/// val.ErrorMessage = this.MakeMsgLink(val.ControlToValidate,
		///     val.ErrorMessage, this.MsgLinkCssClass);
		/// </code>
		/// VB.NET:
		/// <code>
		/// ' Convert a validator error message to a hyperlink
		/// val.ErrorMessage = Me.MakeMsgLink(val.ControlToValidate,
		///     val.ErrorMessage, Me.MsgLinkCssClass);
		/// </code>
		/// </example>
//		public string MakeMsgLink(string id, string msg, string cssClass)
//		{
//			string newClass;
//
//			// Don't bother if it's null, empty, or already in the form of
//			// a link.
//			if(msg == null || msg.Length == 0 || msg.StartsWith("<a "))
//				return msg;
//
//			StringBuilder sb = new StringBuilder(512);
//
//			// Add the anchor tag and the optional CSS class
//			sb.Append("<a ");
//
//			newClass = (cssClass == null) ? this.MsgLinkCssClass : cssClass;
//
//			if(newClass != null && newClass.Length > 0)
//			{
//				sb.Append("class='");
//				sb.Append(newClass);
//				sb.Append("' ");
//			}
//
//			// An HREF is included that does nothing so that we can use the
//			// hover style to do stuff like underline the link when the mouse
//			// is over it.  OnClick performs the action and returns false so
//			// that we don't trigger IE's OnBeforeUnload event which may be
//			// tied to data change checking code.
//
//			// NOTE: OnPreRender registers the script containing the function.
//			// Tell the function to use the "Find Control" method to locate
//			// the ID.  That way, it works for controls embedded in data grids.
//			sb.Append("href='javascript:return false;' " +
//				"onclick='javascript: return BP_funSetFocus(\"");
//			sb.Append(id);
//			sb.Append("\", true);'>");
//			sb.Append(msg);
//			sb.Append("</a>");
//
//			return sb.ToString();
//		}

		/// <summary>
		/// OnInit is overridden to locate the form control on the page and
		/// set the <see cref="PageForm"/> property to it.  For postbacks, it
		/// will also retrieve the value of the dirty flag and store it in
		/// the <see cref="Dirty"/> property.
		/// </summary>
		/// <param name="e">Event arguments</param>
		/// <remarks>If you override this method in a derived class, you
		/// must call this version too.</remarks>
		protected override void OnInit(EventArgs e)
		{
			base.OnInit(e);

			// Retrieve the state of the Dirty flag (if used)
			this.Dirty = Convert.ToBoolean(Request.Form["BP_bIsDirty"],
				CultureInfo.InvariantCulture);
            Header.UserName = this.LoggedUser.FullName;

			//			// Find the form on the page if there is one
			//			if(ctlForm is System.Web.UI.Page)
			//			{
			//				Control form = TimesheetPageBase.FindPageForm((Page)ctlForm);
			//
			//				if(form != null)
			//					ctlForm = form;
			//			}
			//		}
		}
//		/// <summary>
//		/// OnPreRender is overridden to generate the data change checking
//		/// variables and script and the focused control script if necessary.
//		/// </summary>
//		/// <param name="e">Event arguments</param>
//		/// <remarks>If you override this method in a derived class, you
//		/// must call this version too.</remarks>
//		protected override void OnPreRender(EventArgs e)
//		{
//			StringBuilder sb;
//			string[] idList;
//			
//			base.OnPreRender(e);
//			//
//			//			// If data change checking has been requested, output the dirty
//			//			// flag, exclusion arrays, confirm message, and the script.
//			if(this.CheckForDataChanges == true)
//			{
//				// Register a hidden field so that the client can pass
//				// back changes to the Dirty flag.
//				this.RegisterHiddenField("BP_bIsDirty",
//					this.Dirty.ToString(CultureInfo.InvariantCulture).ToLower(
//					CultureInfo.InvariantCulture));
//			
//				// Register an OnSubmit function call so that we can get the
//				// state of the Dirty flag and put it in the hidden field.  It
//				// can't occur in the OnBeforeUnload event as everything has
//				// been packaged up ready for sending to the server and changes
//				// made in that event don't get sent to the server.
//				this.RegisterOnSubmitStatement("BP_DirtyCheck",
//					"BP_funCheckForChanges(true);");
//			
//				//
//				// Create a script block containing the array declarations,
//				// the data loss message variable, the dirty flag, and the
//				// change checking script.
//				sb = new StringBuilder(
//					"<script type='text/javascript'>\n<!--\n" +
//					"var BP_arrBypassList = new Array(", 4096);
//			
//				idList = this.BypassPromptIds;
//				if(idList != null)
//				{
//					sb.Append('\"');
//					sb.Append(String.Join("\",\"", idList));
//					sb.Append('\"');
//				}
//			
//				sb.Append(");\nvar BP_arrSkipList = new Array(");
//				idList = this.SkipDataCheckIds;
//				if(idList != null)
//				{
//					sb.Append('\"');
//					sb.Append(String.Join("\",\"", idList));
//					sb.Append('\"');
//				}
//			
//				sb.Append(");\nvar BP_strDataLossMsg = \"");
//				sb.Append(this.ConfirmLeaveMessage);
//				sb.Append("\";\nvar BP_strFormName = \"");
//			
//				// BP_strFormName tells the script what form to use
//				// for the change checking.
//				sb.Append(this.PageForm.UniqueID);
//				sb.Append("\";\n//-->\n</script>\n");
//			
//				// Add the reference to retrieve the script from the
//				// resource server handler.
//				sb.Append("<script type='text/javascript' src='js/DataChange.js'></script>");
//							
//			
//				this.RegisterClientScriptBlock("BP_DCCJS", sb.ToString());
//			}
//			
//		}
	

	}
}
