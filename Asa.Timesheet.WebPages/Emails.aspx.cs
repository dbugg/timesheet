using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Data.SqlClient;
using log4net;
using Asa.Timesheet.Data;
using Asa.Timesheet.Data.Entities;

namespace Asa.Timesheet.WebPages
{
	/// <summary>
	/// Summary description for Mails.
	/// </summary>
	public class Emails : TimesheetPageBase
	{

		#region Web controls

		protected System.Web.UI.WebControls.PlaceHolder menuHolder;
		protected System.Web.UI.WebControls.Label lblError;
		protected System.Web.UI.WebControls.Label lblInfo;
		protected System.Web.UI.WebControls.Button btnNewEmail;
		protected System.Web.UI.WebControls.DataGrid grdEmails;
		protected System.Web.UI.WebControls.Panel grid;
		protected Asa.Timesheet.WebPages.UserControls.PageHeader header;

		#endregion
	
		private static readonly ILog log = LogManager.GetLogger(typeof(Emails));

		private void Page_Load(object sender, System.EventArgs e)
		{	
			if (!LoggedUser.HasRole) ErrorRedirect(Resource.ResourceManager["noRightsAlert"]);
			if(Page.IsPostBack == false)
			{
				// Set up form for data change checking when
				// first loaded.
				this.CheckForDataChanges= true;
				this.BypassPromptIds =
					new string[] { "btnSave",  "btnDelete"};
			}
			UIHelpers.CreateMenu(menuHolder,LoggedUser);
			if (!this.IsPostBack)
			{
				header.PageTitle = Resource.ResourceManager["emails_PageTitle"];
				header.UserName = LoggedUser.UserName;

				btnNewEmail.Width = 100;
				btnNewEmail.Text = Resource.ResourceManager["emails_btnNewEmail"];

				SortOrder = 1;
				BindGrid();
			}
		}

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
			this.grdEmails.ItemCreated += new System.Web.UI.WebControls.DataGridItemEventHandler(this.grdEmails_ItemCreated);
			this.grdEmails.ItemCommand += new System.Web.UI.WebControls.DataGridCommandEventHandler(this.grdEmails_ItemCommand);
			this.btnNewEmail.Click += new System.EventHandler(this.btnNewEmail_Click);
			this.Load += new System.EventHandler(this.Page_Load);

		}
		#endregion

		#region Database

		private void BindGrid()
		{
			SqlDataReader reader = null;

			try
			{
				reader = EmailsData.SelectEmails(SortOrder);

				if (reader!=null)
				{
					grdEmails.DataSource = reader;
					grdEmails.DataKeyField = "EmailID";
					grdEmails.DataBind();
				}
			}
			catch (Exception ex)
			{
				log.Error(ex);
				return;
			}

			try
			{
				SetConfirmDelete(grdEmails, "ibDeleteItem", 1);
			}
			catch (Exception ex)
			{
				log.Error(ex);
			}
		}

		private bool DeleteEmail(int emailID)
		{
			try
			{
				EmailsData.DeleteEmail(emailID);
			}
			catch (Exception ex)
			{
				log.Error(ex);
				return false;
			}

			return true;
		}

		#endregion

		#region Event handlers

		private void grdEmails_ItemCommand(object source, System.Web.UI.WebControls.DataGridCommandEventArgs e)
		{

			if (e.CommandName == "Sort")
			{
				int sortOrder = SortOrder;
				switch ((string)e.CommandArgument)
				{
					case "Name": if (sortOrder==1) sortOrder = -1; else sortOrder = 1;
						SortOrder = sortOrder;
						BindGrid(); break;
					case "EMail": if (sortOrder==2) sortOrder = -2; else sortOrder = 2;
						SortOrder = sortOrder;
						BindGrid(); break;
				}
				return;
			}

			ImageButton ib = (ImageButton)e.CommandSource;
			string s = ib.ID;
			switch (ib.ID)
			{
				case "ibDeleteItem": int emailID = (int)((DataGrid)source).DataKeys[e.Item.ItemIndex];
					if (!DeleteEmail(emailID))
					{
						lblError.Text = Resource.ResourceManager["editEmail_ErrorDelete"];
						return;
					};
					BindGrid();
					break;
				case "ibEditItem": 
					string sEmailID = ((DataGrid)source).DataKeys[e.Item.ItemIndex].ToString();
					Response.Redirect("EditEmail.aspx"+"?eid="+sEmailID); 
					break;
			}
		
		}	

		private void grdEmails_ItemCreated(object sender, System.Web.UI.WebControls.DataGridItemEventArgs e)
		{
			if (e.Item.ItemType == ListItemType.Header)
			{
				string imgUrl = "";
				int sortOrder = SortOrder;

				if (sortOrder!=0)
				{
					imgUrl = (sortOrder>0)?"images/sup.gif":"images/sdown.gif";
					Label sep = new Label(); 
					sep.Width = 2; 
					e.Item.Cells[Math.Abs(sortOrder)].Controls.Add(sep);
					ImageButton ib = new ImageButton();
					ib.Height = 11; ib.Width = 11; ib.ImageUrl = imgUrl;
					e.Item.Cells[Math.Abs(sortOrder)].Controls.Add(ib);
				}
			}
		}

		private void btnNewEmail_Click(object sender, System.EventArgs e)
		{
			Response.Redirect("EditEmail.aspx");
		}
		

		#endregion

		#region Menu

//		private void CreateMenu()
//		{
//			UserControls.MenuTable menu = new UserControls.MenuTable();
//			menu.ID = "MenuTable";
//
//			// 'Links' group
//			ArrayList menuItems = new ArrayList();
//
//			menuItems.Add(new MenuItemInfo(Resource.ResourceManager["menuItem_Hours"], "Hours.aspx", true));
//			menuItems.Add(new MenuItemInfo(Resource.ResourceManager["menuItem_Projects"], "Projects.aspx"));
//			if (LoggedUser.IsLeader || LoggedUser.IsAssistant) 
//			{
//				menuItems.Add(new MenuItemInfo(Resource.ResourceManager["menuItem_Clients"], "Clients.aspx"));
//								
//				menuItems.Add(new MenuItemInfo(Resource.ResourceManager["menuItem_Subcontracters"], "Subcontracters.aspx"));
//				if (LoggedUser.IsLeader)
//				menuItems.Add(new MenuItemInfo(Resource.ResourceManager["menuItem_Users"], "Users.aspx"));
//			}
//			
//			menu.AddMenuGroup(Resource.ResourceManager["menuHeader_Links"], 20, menuItems);
//			
//			// 'New' group
//			menuItems = new ArrayList();
//			menuItems.Add(new MenuItemInfo(Resource.ResourceManager["menuItem_NewProject"], "EditProject.aspx"));
//			if (LoggedUser.IsLeader || LoggedUser.IsAssistant)
//			{
//				menuItems.Add(new MenuItemInfo(Resource.ResourceManager["menuItem_NewClient"], "EditClient.aspx"));
//				if (LoggedUser.IsLeader)
//				menuItems.Add(new MenuItemInfo(Resource.ResourceManager["menuItem_NewUser"], "EditUser.aspx"));
//			}
//			menuItems.Add(new MenuItemInfo(Resource.ResourceManager["menuItem_NewEmail"], "EditEmail.aspx"));
//			menuItems.Add(new MenuItemInfo(Resource.ResourceManager["newcontracter_PageTitle"], "EditSubContracter.aspx"));
//
//	
//			menu.AddMenuGroup(Resource.ResourceManager["menuHeader_New"], 30, menuItems);
//
//			// Reports
//			menuItems = new ArrayList();
//			menuItems.Add(new MenuItemInfo(Resource.ResourceManager["menuItem_WorkTimeReport"], "reports/worktimes.aspx"));
//			menuItems.Add(new MenuItemInfo(Resource.ResourceManager["menuItem_MinutesReport"], "Reports/Minutes.aspx"));
//			if (LoggedUser.IsLeader || LoggedUser.IsAssistant)
//				menuItems.Add(new MenuItemInfo(Resource.ResourceManager["menuItem_MailReport"], "MailForm.aspx"));
//			if (LoggedUser.IsLeader)
//			{
//				menuItems.Add(new MenuItemInfo(Resource.ResourceManager["menuItem_NotEnteredWorkTime"], "Reports/EmptyHours.aspx"));
//				menuItems.Add(new MenuItemInfo(Resource.ResourceManager["menuItem_Calendar"], "reports/Calendar.aspx"));
//			}
//
//			menu.AddMenuGroup(Resource.ResourceManager["menuHeader_Reports"], 30, menuItems);
//
//			//Help
//			menuItems = new ArrayList();
//			menuItems.Add(new MenuItemInfo(Resource.ResourceManager["menuItem_Help"], SessionManager.GetHelpLink(),true,Pages.Emails));
//			menu.AddMenuGroup("", 10, menuItems);
//
//			menuHolder.Controls.Add(menu);
//		}

		#endregion

		
		
	}
}
