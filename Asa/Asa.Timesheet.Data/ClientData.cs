using System;
using System.Collections;
using System.Text;

namespace Asa.Timesheet.Data
{
	public class ClientData
	{
		private int _clientID = -1;
		private string _clientName = String.Empty;
		private string _city = String.Empty;
		private string _address = String.Empty;
		private string _email = String.Empty;
		private string _fax = String.Empty;
		private bool _isActive = true;
		private string _manager = String.Empty;
		private int _managerTitleID = -1;
		private string _representative1 = String.Empty;
		private string _representative2 = String.Empty;
		private string _delo = String.Empty;
		private string _bulstat = String.Empty;
		private string _nDR = String.Empty;
		private string _phone = String.Empty;
		private string _webpage = String.Empty;
		private string _phone1 = String.Empty;
		private string _email1 = String.Empty;
		private string _phone2 = String.Empty;
		private string _email2 = String.Empty;
		private int _status = -1;

		/// <summary>
		/// Initialize an new empty Login object.
		/// </summary>
		public ClientData()
		{
		}
		
		/// <summary>
		/// Initialize a new Login object with the given parameters.
		/// </summary>
		public ClientData(int clientID, string clientName, string city, string address, string email, string fax, bool isActive, string manager, int managerTitleID, string representative1, string representative2, string delo, string bulstat, string nDR, string phone, string webpage, string phone1, string email1, string phone2, string email2, int status)
		{
			this._clientID = clientID;
			this._clientName = clientName;
			this._city = city;
			this._address = address;
			this._email = email;
			this._fax = fax;
			this._isActive = isActive;
			this._manager = manager;
			this._managerTitleID = managerTitleID;
			this._representative1 = representative1;
			this._representative2 = representative2;
			this._delo = delo;
			this._bulstat = bulstat;
			this._nDR = nDR;
			this._phone = phone;
			this._webpage = webpage;
			this._phone1 = phone1;
			this._email1 = email1;
			this._phone2 = phone2;
			this._email2 = email2;
			this._status = status;
		}	
		
	  
		public override string ToString()
		{
			StringBuilder builder = new StringBuilder();
			builder.Append("_clientID: " + _clientID.ToString()).Append("\r\n");
			builder.Append("_clientName: " + _clientName.ToString()).Append("\r\n");
			builder.Append("_city: " + _city.ToString()).Append("\r\n");
			builder.Append("_address: " + _address.ToString()).Append("\r\n");
			builder.Append("_email: " + _email.ToString()).Append("\r\n");
			builder.Append("_fax: " + _fax.ToString()).Append("\r\n");
			builder.Append("_isActive: " + _isActive.ToString()).Append("\r\n");
			builder.Append("_manager: " + _manager.ToString()).Append("\r\n");
			builder.Append("_managerTitleID: " + _managerTitleID.ToString()).Append("\r\n");
			builder.Append("_representative1: " + _representative1.ToString()).Append("\r\n");
			builder.Append("_representative2: " + _representative2.ToString()).Append("\r\n");
			builder.Append("_delo: " + _delo.ToString()).Append("\r\n");
			builder.Append("_bulstat: " + _bulstat.ToString()).Append("\r\n");
			builder.Append("_nDR: " + _nDR.ToString()).Append("\r\n");
			builder.Append("_phone: " + _phone.ToString()).Append("\r\n");
			builder.Append("_webpage: " + _webpage.ToString()).Append("\r\n");
			builder.Append("_phone1: " + _phone1.ToString()).Append("\r\n");
			builder.Append("_email1: " + _email1.ToString()).Append("\r\n");
			builder.Append("_phone2: " + _phone2.ToString()).Append("\r\n");
			builder.Append("_email2: " + _email2.ToString()).Append("\r\n");
			builder.Append("_status: " + _status.ToString()).Append("\r\n");
			builder.Append("\r\n");
			return builder.ToString();
		}	
		public object GetByIndex(int nIndex)
		{
			switch(nIndex)
			{
				case 0:
					return _clientID;
				case 1:
					return _clientName;
				case 2:
					return _city;
				case 3:
					return _address;
				case 4:
					return _email;
				case 5:
					return _fax;
				case 6:
					return _isActive;
				case 7:
					return _manager;
				case 8:
					return _managerTitleID;
				case 9:
					return _representative1;
				case 10:
					return _representative2;
				case 11:
					return _delo;
				case 12:
					return _bulstat;
				case 13:
					return _nDR;
				case 14:
					return _phone;
				case 15:
					return _webpage;
				case 16:
					return _phone1;
				case 17:
					return _email1;
				case 18:
					return _phone2;
				case 19:
					return _email2;
				case 20:
					return _status;
			}
			return null;
		}
		#region Properties
	
		public int ClientID
		{
			get { return _clientID; }
			set { _clientID = value; }
		}
	
		public string ClientName
		{
			get { return _clientName; }
			set { _clientName = value; }
		}
	
		public string City
		{
			get { return _city; }
			set { _city = value; }
		}
	
		public string Address
		{
			get { return _address; }
			set { _address = value; }
		}
	
		public string Email
		{
			get { return _email; }
			set { _email = value; }
		}
	
		public string Fax
		{
			get { return _fax; }
			set { _fax = value; }
		}
	
		public bool IsActive
		{
			get { return _isActive; }
			set { _isActive = value; }
		}
		public string ClientNameAndAll
		{
			get 
			{
				string s ="";
				bool bHasAdded=false;
				if(_manager.Trim()!="" )
				{
					s+=_manager;
					bHasAdded=true;
				}
				if(_representative1.Trim()!="" &&  _representative1!=_manager)
				{
					if(bHasAdded)
						s+=Resource.ResourceManager["or"];
//					else
//						s+=" - " ;
					s+=_representative1;
					bHasAdded=true;
				}
				if(_representative2.Trim()!="" && _representative2!=_representative1 &&  _representative2!=_manager)
				{
					if(bHasAdded)
						s+=Resource.ResourceManager["or"];
//					else
//						s+=" - " ;
					s+=_representative2;
					bHasAdded=true;
				}
				if(bHasAdded)
					s+=" - " ;
				 s+=_clientName; 
				return s;
			}
			set {}
		}
		public string Manager
		{
			get { return _manager; }
			set { _manager = value; }
		}
	
		public int ManagerTitleID
		{
			get { return _managerTitleID; }
			set { _managerTitleID = value; }
		}
	
		public string Representative1
		{
			get { return _representative1; }
			set { _representative1 = value; }
		}
	
		public string Representative2
		{
			get { return _representative2; }
			set { _representative2 = value; }
		}
	
		public string Delo
		{
			get { return _delo; }
			set { _delo = value; }
		}
	
		public string Bulstat
		{
			get { return _bulstat; }
			set { _bulstat = value; }
		}
	
		public string NDR
		{
			get { return _nDR; }
			set { _nDR = value; }
		}
	
		public string Phone
		{
			get { return _phone; }
			set { _phone = value; }
		}
	
		public string Webpage
		{
			get { return _webpage; }
			set { _webpage = value; }
		}
	
		public string Phone1
		{
			get { return _phone1; }
			set { _phone1 = value; }
		}
	
		public string Email1
		{
			get { return _email1; }
			set { _email1 = value; }
		}
	
		public string Phone2
		{
			get { return _phone2; }
			set { _phone2 = value; }
		}
	
		public string Email2
		{
			get { return _email2; }
			set { _email2 = value; }
		}
	
		public int Status
		{
			get { return _status; }
			set { _status = value; }
		}
	
		#endregion
	}
}


