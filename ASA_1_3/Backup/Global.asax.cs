using System;
using System.Collections;
using System.ComponentModel;
using System.Web;
using System.Web.SessionState;
using log4net;
using log4net.Config;

namespace Asa.Timesheet.WebPages 
{
	/// <summary>
	/// Summary description for Global.
	/// </summary>
	public class Global : System.Web.HttpApplication
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		/// 
		private System.ComponentModel.IContainer components = null;
		private static readonly ILog log = LogManager.GetLogger(typeof(Global));
        public Global()
        {
            InitializeComponent();
        }

        protected void Application_Start(Object sender, EventArgs e)
        {
            DOMConfigurator.Configure();
        }

        protected void Session_Start(Object sender, EventArgs e)
        {

        }

        protected void Application_BeginRequest(Object sender, EventArgs e)
        {

        }

        protected void Application_EndRequest(Object sender, EventArgs e)
        {

        }

        protected void Application_AuthenticateRequest(Object sender, EventArgs e)
        {

        }

        protected void Application_Error(Object sender, EventArgs e)
        {
            log.Error(Server.GetLastError());
            string s = "";
            if (Server.GetLastError() != null)
            {
                Exception ex = Server.GetLastError().GetBaseException();
                string file = HttpContext.Current.Request.Url.ToString();
                string page = HttpContext.Current.Request.UrlReferrer.ToString();
                //s= Server.GetLastError().InnerException.Message;
            }

            Server.ClearError();
            TimesheetPageBase.ErrorRedirect(s);


        }

        protected void Session_End(Object sender, EventArgs e)
        {

        }

        protected void Application_End(Object sender, EventArgs e)
        {

        }
			
		#region Web Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
        }
		#endregion
	}
}

