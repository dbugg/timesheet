<%@ Page language="c#" Codebehind="Hours.aspx.cs" AutoEventWireup="false" Inherits="Asa.Timesheet.WebPages.Hours" %>
<%@ Register TagPrefix="uc1" TagName="PageHeader" Src="UserControls/PageHeader.ascx" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
	<HEAD>
		<title>Hours</title>
		<meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
		<meta content="C#" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<LINK href="styles/calendar.css" type="text/css" rel="stylesheet">
		<LINK href="styles/timesheet.css" type="text/css" rel="stylesheet">
		<script language="javascript" src="js/hours.js"></script>
	</HEAD>
	<body bottomMargin="0" leftMargin="0" topMargin="0" onload="Init();" rightMargin="0">
		<form id="Form1" method="post" runat="server">
			<table height="100%" cellSpacing="0" cellPadding="0" width="100%" border="0">
				<tr height="1">
					<td><uc1:pageheader id="header" runat="server"></uc1:pageheader></td>
				</tr>
				<TR height="1">
					<td noWrap background="images/line.gif" height="1"></td>
				</TR>
				<tr>
					<td>
						<table height="100%" cellSpacing="0" cellPadding="0" width="100%" border="0">
							<tr>
								<td style="BACKGROUND-IMAGE: url(images/left110.gif)" vAlign="top" noWrap width="110"><asp:placeholder id="menuHolder" runat="server"></asp:placeholder></td>
								<td noWrap width="1" bgColor="lightgrey"></td>
								<td style="PADDING-RIGHT: 3px; PADDING-LEFT: 5px; PADDING-TOP: 3px" vAlign="top" noWrap>
									<TABLE class="calendarTable" id="tblCalendar" cellSpacing="0" cellPadding="0" runat="server">
									</TABLE>
									<INPUT id="txtVisibleRows" style="DISPLAY: none" type="text" name="txtVisibleRows" runat="server">
									<INPUT id="txtNoActivity" style="DISPLAY: none" type="text" runat="server">&nbsp;
									<INPUT id="txtIncorrectDataMsg" style="DISPLAY: none" type="text" name="txtIncorrectDataMsg"
										runat="server"> <INPUT id="txtMinGridRows" style="DISPLAY: none" type="text" name="txtMinGridRows" runat="server">
									<INPUT id="txtTimeAbbr" style="DISPLAY: none" type="text" name="Text1" runat="server">
									<INPUT id="txtDefaultWorkDayTimes" style="DISPLAY: none" type="text" name="Text1" runat="server">
									<INPUT id="txtDefaultHoursProjects" style="DISPLAY: none" type="text" name="Text1" runat="server">
									<TABLE id="tblInfo" height="50" cellSpacing="0" cellPadding="3" width="880" border="0">
										<TR>
											<TD vAlign="middle" noWrap><asp:label id="lblInfo" runat="server" EnableViewState="False" CssClass="InfoLabel"></asp:label><br>
												<asp:label id="lblError" runat="server" EnableViewState="False" CssClass="ErrorLabel"></asp:label></TD>
										</TR>
									</TABLE>
									<div style="PADDING-BOTTOM: 5px" align="right"><asp:hyperlink id="HyperLink1" runat="server" CssClass="hoursFormLink" NavigateUrl="HoursPeriod.aspx">Към режим "Въвеждане на период"</asp:hyperlink></div>
									<TABLE id="Table3" style="BORDER-RIGHT: tan thin solid; BORDER-TOP: tan thin solid; BORDER-LEFT: tan thin solid; BORDER-BOTTOM: tan thin solid"
										cellSpacing="0" cellPadding="4" width="100%" border="0">
										<TR>
											<TD><asp:dropdownlist id="ddlUsers" runat="server" CssClass="enterDataBox" Width="250px" AutoPostBack="True"></asp:dropdownlist>&nbsp;<asp:label id="lblTop" runat="server" cssclass="PageCaption"></asp:label></TD>
											<td align="right"><asp:label id="lblStatus" runat="server" CssClass="HoursGridStatusLabel"></asp:label>&nbsp;</td>
										</TR>
										<tr>
											<td colSpan="2"></td>
										</tr>
									</TABLE>
									<asp:panel id="divMain" Runat="server">
										<asp:panel id="grid" style="BORDER-RIGHT: #d2b48c 2px solid; BORDER-TOP: #d2b48c 0px solid; OVERFLOW-Y: scroll; PADDING-LEFT: 2px; BORDER-LEFT: #d2b48c 2px solid; PADDING-TOP: 2px; BORDER-BOTTOM: #d2b48c 2px solid"
											runat="server" Width="100%" Height="355px">
											<asp:datagrid id="GridHours" runat="server" CssClass="GridHours" BorderColor="SaddleBrown" CellPadding="4"
												BorderWidth="1px" BackColor="BlanchedAlmond" BorderStyle="Solid" PageSize="2" AutoGenerateColumns="False"
												GridLines="Horizontal">
												<AlternatingItemStyle BackColor="BlanchedAlmond"></AlternatingItemStyle>
												<ItemStyle BackColor="BlanchedAlmond"></ItemStyle>
												<HeaderStyle Height="5px" CssClass="HoursGridHeader"></HeaderStyle>
												<Columns>
													<asp:TemplateColumn>
														<HeaderStyle Wrap="False" Width="25px"></HeaderStyle>
														<ItemStyle Wrap="False" HorizontalAlign="Center" VerticalAlign="Top"></ItemStyle>
														<ItemTemplate>
															<asp:CheckBox ID="ck" Runat="server"></asp:CheckBox><BR>
															<asp:ImageButton id="btnDeleteItem" runat="server" ImageUrl="images/delete.gif"></asp:ImageButton>
															<asp:Image id="imgWarning" style="DISPLAY: none" runat="server" EnableViewState="False" ImageUrl="images/error.gif"></asp:Image>
														</ItemTemplate>
														<FooterStyle Wrap="False"></FooterStyle>
													</asp:TemplateColumn>
													<asp:TemplateColumn HeaderText="Проект и дейност">
														<HeaderStyle Wrap="False" Width="40%"></HeaderStyle>
														<ItemStyle VerticalAlign="Top"></ItemStyle>
														<ItemTemplate>
															<asp:DropDownList id="ddlBuildingTypes" Width="100%" Runat="server" Font-Bold="True" AutoPostBack="True"
																onselectedindexchanged="ddl_SelectedIndexChanged">
																<asp:ListItem Selected="True"></asp:ListItem>
															</asp:DropDownList>
															<asp:DropDownList id="ddlProject" Width="100%" Runat="server" Font-Bold="True">
																<asp:ListItem Selected="True"></asp:ListItem>
															</asp:DropDownList><BR>
															<asp:DropDownList id="ddlActivity" Width="100%" Runat="server" onchange="changeActivity(this)">
																<asp:ListItem></asp:ListItem>
															</asp:DropDownList><INPUT id="hdnPrevProjectID" style="DISPLAY: none" type="text" runat="server">
														</ItemTemplate>
													</asp:TemplateColumn>
													<asp:TemplateColumn HeaderText="Нач. час">
														<HeaderStyle Wrap="False"></HeaderStyle>
														<ItemStyle Wrap="False" VerticalAlign="Top"></ItemStyle>
														<ItemTemplate>
															<asp:DropDownList id="ddlStartTime" Width="65px" Runat="server">
																<asp:ListItem></asp:ListItem>
															</asp:DropDownList><INPUT id="hdnWorkTimeID" type="hidden" runat="server">
														</ItemTemplate>
													</asp:TemplateColumn>
													<asp:TemplateColumn HeaderText="Кр. час">
														<HeaderStyle Wrap="False"></HeaderStyle>
														<ItemStyle Wrap="False" VerticalAlign="Top"></ItemStyle>
														<ItemTemplate>
															<asp:DropDownList id="ddlEndTime" Width="65px" Runat="server">
																<asp:ListItem></asp:ListItem>
															</asp:DropDownList>
														</ItemTemplate>
													</asp:TemplateColumn>
													<asp:TemplateColumn HeaderText="Протокол">
														<HeaderStyle Wrap="False" Width="25%"></HeaderStyle>
														<ItemStyle Wrap="False" VerticalAlign="Top"></ItemStyle>
														<ItemTemplate>
															<asp:TextBox id="txtMinutes" runat="server" Width="100%" Height="58px" TextMode="MultiLine"></asp:TextBox>
														</ItemTemplate>
													</asp:TemplateColumn>
													<asp:TemplateColumn HeaderText="Админ. бележки">
														<HeaderStyle Width="25%"></HeaderStyle>
														<ItemStyle VerticalAlign="Top"></ItemStyle>
														<ItemTemplate>
															<asp:TextBox id="txtAdminMinutes" runat="server" ForeColor="#C00000" Width="100%" Height="48px"
																TextMode="MultiLine"></asp:TextBox>
														</ItemTemplate>
													</asp:TemplateColumn>
													<asp:TemplateColumn>
														<HeaderStyle Wrap="False" Width="25px"></HeaderStyle>
														<ItemStyle Wrap="False" HorizontalAlign="Center"></ItemStyle>
														<ItemTemplate>
															<asp:ImageButton id="btnWho" runat="server" ImageUrl="images/meeting.gif" ToolTip="Присъствали"></asp:ImageButton>
															<asp:ImageButton id="btnMail" runat="server" ImageUrl="images/email.gif" ToolTip="Прати Е-мейл"></asp:ImageButton>
															<asp:ImageButton id="btnMeeting" runat="server" ImageUrl="images/word.gif" ToolTip="Протокол от среща"></asp:ImageButton>
														</ItemTemplate>
														<FooterStyle Wrap="False"></FooterStyle>
													</asp:TemplateColumn>
													<asp:BoundColumn Visible="False" DataField="WorkTimeID" HeaderText="WorkTimeID"></asp:BoundColumn>
												</Columns>
											</asp:datagrid>
										</asp:panel>
										<TABLE id="tblGridBottom" style="BORDER-RIGHT: tan 1px solid; BORDER-LEFT: tan 1px solid; BORDER-BOTTOM: tan 1px solid; HEIGHT: 49px; BACKGROUND-COLOR: wheat"
											borderColor="tan" cellSpacing="0" cellPadding="1" width="100%" border="1" runat="server">
											<TR>
												<TD class="Total">
													<TABLE cellSpacing="0" cellPadding="2" width="100%" border="0">
														<TR>
															<TD>
																<asp:button id="btnNewProject" runat="server" CssClass="HoursGridButton" Text="Друг проект"></asp:button>&nbsp;&nbsp;
																<INPUT style="DISPLAY: none" onclick="ClearAll();" type="button" value="Нулирай всички">
															</TD>
															<TD align="right">
																<asp:TextBox id="txtCopiedUsers" style="DISPLAY: none" runat="server"></asp:TextBox>
																<asp:Button id="btnCopySave" style="DISPLAY: none" runat="server" Text="Button"></asp:Button>
																<asp:button id="btnCopy" runat="server" CssClass="HoursGridButton" Width="172px" Text="Добави друг служител"></asp:button>&nbsp;
																<asp:button id="btnSave" runat="server" CssClass="HoursGridButton" Text="Запиши"></asp:button></TD>
														</TR>
													</TABLE>
												</TD>
											</TR>
											<TR>
												<TD class="Total" width="100%" colSpan="2">&nbsp;ОБЩО ЧАСОВЕ<INPUT id="Total" style="BORDER-RIGHT: darkorange 1px solid; BORDER-TOP: darkorange 1px solid; FONT-WEIGHT: bold; FONT-SIZE: 11px; BORDER-LEFT: darkorange 1px solid; WIDTH: 60px; BORDER-BOTTOM: darkorange 1px solid; TEXT-ALIGN: right"
														readOnly type="text" size="4" name="Total" runat="server"></TD>
											</TR>
										</TABLE>
									</asp:panel></td>
							</tr>
						</table>
					</td>
				</tr>
			</table>
		</form>
	</body>
</HTML>
