using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Data.SqlClient;
using Asa.Timesheet.Data;
using Asa.Timesheet.Data.Entities;
using log4net;

namespace Asa.Timesheet.WebPages
{
	/// <summary>
	/// Summary description for Clients.
	/// </summary>
	public class Clients : TimesheetPageBase 
	{
		#region Web controls

		protected System.Web.UI.WebControls.Label Label1;
		protected System.Web.UI.WebControls.Panel grid;
		protected System.Web.UI.WebControls.DataGrid grdClients;
		protected System.Web.UI.WebControls.Button btnNewClient;
		protected System.Web.UI.HtmlControls.HtmlTable tblCalendar;
		protected System.Web.UI.WebControls.Label lblInfo;
		protected System.Web.UI.WebControls.Label lblError;
		protected System.Web.UI.WebControls.PlaceHolder menuHolder;
		protected Asa.Timesheet.WebPages.UserControls.PageHeader header;

		#endregion
		protected Telerik.WebControls.RadGrid gridCalls;
		protected System.Web.UI.WebControls.Button btnExport;
	
		private enum GridColumns
		{
			Number=0,
			Name = 1,
			City,
			
			Address,
			Email,
			EditItem,
			DeleteItem
		}
		private static readonly ILog log = LogManager.GetLogger(typeof(Clients));

		private void Page_Load(object sender, System.EventArgs e)
		{
			//if (!(LoggedUser.IsLeader || LoggedUser.IsAssistant))
//			{
//				ErrorRedirect(Resource.ResourceManager["noRightsAlert"]);
//			}
			
			UIHelpers.CreateMenu(menuHolder,LoggedUser);
			
			if (!this.IsPostBack)
			{
				header.PageTitle = Resource.ResourceManager["clients_PageTitle"];
				header.UserName = this.LoggedUser.UserName;
				btnNewClient.Text = Resource.ResourceManager["clients_BtnNewClient"];

				SortOrder = 1;
				BindGrid();
			}
			if(!(LoggedUser.IsAssistantOnly || LoggedUser.HasPaymentRights ||LoggedUser.IsSecretary))
			{
				btnNewClient.Visible=false;
			}
		}

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
			this.grdClients.ItemCreated += new System.Web.UI.WebControls.DataGridItemEventHandler(this.grdClients_ItemCreated);
			this.grdClients.ItemCommand += new System.Web.UI.WebControls.DataGridCommandEventHandler(this.grdClients_ItemCommand);
			this.btnNewClient.Click += new System.EventHandler(this.btnNewClient_Click);
			this.btnExport.Click += new System.EventHandler(this.btnExport_Click);
			this.Load += new System.EventHandler(this.Page_Load);

		}
		#endregion
	
		#region Event handlers

		private void grdClients_ItemCommand(object source, System.Web.UI.WebControls.DataGridCommandEventArgs e)
		{
			if (e.CommandName == "Sort")
			{
				int sortOrder = SortOrder;
				switch ((string)e.CommandArgument)
				{
					case "Name": if (sortOrder==1) sortOrder = -1; else sortOrder = 1;
								SortOrder = sortOrder;
								BindGrid(); break;
					case "City": if (sortOrder==2) sortOrder = -2; else sortOrder = 2;
								SortOrder = sortOrder;
								BindGrid(); break;
					case "Address": if (sortOrder==3) sortOrder = -3; else sortOrder = 3;
									SortOrder = sortOrder;
									BindGrid(); break;
					case "EMail": if (sortOrder==4) sortOrder = -4; else sortOrder = 4;
									SortOrder = sortOrder;
									BindGrid(); break;
				}

				return;
			}
			else if(e.CommandName=="Edit")
			{
				string sClientID = ((DataGrid)source).DataKeys[e.Item.ItemIndex].ToString();
				Response.Redirect("EditClient.aspx"+"?cid="+sClientID); 
				return;
			}
			ImageButton ib = (ImageButton)e.CommandSource;
			string s = ib.ID;
			switch (ib.ID)
			{
				case "ibDeleteItem": int clientID = (int)((DataGrid)source).DataKeys[e.Item.ItemIndex];
					if (!DeleteClient(clientID))
					{
						lblError.Text = Resource.ResourceManager["editClient_ErrorDelete"];
						return;
					}
					BindGrid();
					break;
				case "ibEditItem":  string sClientID = ((DataGrid)source).DataKeys[e.Item.ItemIndex].ToString();
					Response.Redirect("EditClient.aspx"+"?cid="+sClientID); 
					break;
			}
		}

		private void grdClients_ItemCreated(object sender, System.Web.UI.WebControls.DataGridItemEventArgs e)
		{
			if (e.Item.ItemType == ListItemType.Header)
			{
				string imgUrl = "";
				int sortOrder = SortOrder;

				if (sortOrder!=0)
				{
					imgUrl = (sortOrder>0)?"images/sup.gif":"images/sdown.gif";
					Label sep = new Label(); 
					sep.Width = 2; 
					e.Item.Cells[Math.Abs(sortOrder)].Controls.Add(sep);
					ImageButton ib = new ImageButton();
					ib.Height = 11; ib.Width = 11; ib.ImageUrl = imgUrl;
					e.Item.Cells[Math.Abs(sortOrder)].Controls.Add(ib);
				}
			}
		}

		private void btnNewClient_Click(object sender, System.EventArgs e)
		{
			Response.Redirect("EditClient.aspx");
		}

		#endregion

		#region Database 

		private void BindGrid()
		{
			SqlDataReader reader = null;

			try
			{
				reader = ClientsData.SelectClients(SortOrder);
				if (reader!=null)
				{
					grdClients.DataSource = reader;
					grdClients.DataKeyField = "ClientID";
					grdClients.DataBind();
					if (!LoggedUser.IsLeader)
						grdClients.Columns[(int)GridColumns.DeleteItem].Visible = false;
				}
			}
			catch (Exception ex)
			{
				log.Error(ex);
				lblError.Text = Resource.ResourceManager["clients_ErrorLoadClients"];
				return;
			}

			finally
			{
				if (reader!=null) reader.Close();
			}

			try
			{
				SetConfirmDelete(grdClients, "ibDeleteItem", 1);
			}
			catch (Exception ex)
			{
				log.Error(ex);
			}
			
		}

		private bool DeleteClient(int clientID)
		{
			try
			{
				ClientsData.InactiveClient(clientID);
			}
			catch (Exception ex)
			{
				log.Error(ex);
				return false;
			}

			return true;
		}

		#endregion

		#region Menu

//		private void CreateMenu()
//		{
//			UserControls.MenuTable menu = new UserControls.MenuTable();
//			menu.ID = "MenuTable";
//
//			// 'Links' group
//			ArrayList menuItems = new ArrayList();
//
//			menuItems.Add(new MenuItemInfo(Resource.ResourceManager["menuItem_Hours"], "hours.aspx", true));
//			menuItems.Add(new MenuItemInfo(Resource.ResourceManager["menuItem_Projects"], "projects.aspx"));
//			menuItems.Add(new MenuItemInfo(Resource.ResourceManager["menuItem_Subcontracters"], "Subcontracters.aspx"));
//			
//			if (LoggedUser.IsLeader)
//				menuItems.Add(new MenuItemInfo(Resource.ResourceManager["menuItem_Users"], "Users.aspx"));
//			menuItems.Add(new MenuItemInfo(Resource.ResourceManager["menuItem_Emails"], "Emails.aspx"));
//
//			menu.AddMenuGroup(Resource.ResourceManager["menuHeader_Links"], 20, menuItems);
//			
//			// 'New' grop
//			menuItems = new ArrayList();
//			menuItems.Add(new MenuItemInfo(Resource.ResourceManager["menuItem_NewProject"], "editproject.aspx"));
//			menuItems.Add(new MenuItemInfo(Resource.ResourceManager["menuItem_NewClient"], "editclient.aspx"));
//			if (LoggedUser.IsLeader)
//			menuItems.Add(new MenuItemInfo(Resource.ResourceManager["menuItem_NewUser"], "EditUser.aspx"));
//			menuItems.Add(new MenuItemInfo(Resource.ResourceManager["menuItem_NewEmail"], "editemail.aspx"));
//			menuItems.Add(new MenuItemInfo(Resource.ResourceManager["newcontracter_PageTitle"], "EditSubContracter.aspx"));
//
//			menu.AddMenuGroup(Resource.ResourceManager["menuHeader_New"], 30, menuItems);
//
//			// 
//			menuItems = new ArrayList();
//			menuItems.Add(new MenuItemInfo(Resource.ResourceManager["menuItem_WorkTimeReport"], "reports/worktimes.aspx"));
//			menuItems.Add(new MenuItemInfo(Resource.ResourceManager["menuItem_MinutesReport"], "Reports/Minutes.aspx"));
//			if (LoggedUser.IsLeader || LoggedUser.IsAssistant)
//				menuItems.Add(new MenuItemInfo(Resource.ResourceManager["menuItem_MailReport"], "MailForm.aspx"));
//			if (LoggedUser.IsLeader)
//			{
//				menuItems.Add(new MenuItemInfo(Resource.ResourceManager["menuItem_NotEnteredWorkTime"], "reports/EmptyHours.aspx"));
//				menuItems.Add(new MenuItemInfo(Resource.ResourceManager["menuItem_Calendar"], "reports/Calendar.aspx"));
//
//			}
//			menu.AddMenuGroup(Resource.ResourceManager["menuHeader_Reports"], 30, menuItems);
//
//			menuItems = new ArrayList();
//			menuItems.Add(new MenuItemInfo(Resource.ResourceManager["menuItem_Help"], SessionManager.GetHelpLink(),true,GetPageFromRequest()));
//			menu.AddMenuGroup("", 10, menuItems);
//
//			menuHolder.Controls.Add(menu);
//		}

		#endregion

		private void btnExport_Click(object sender, System.EventArgs e)
		{
			this.EnableViewState = true;

			System.IO.StringWriter tw = new System.IO.StringWriter();
			System.Web.UI.HtmlTextWriter hw = new System.Web.UI.HtmlTextWriter(tw);
			SqlDataReader reader = null;

			try
			{
				reader =  ClientsData.SelectClients(SortOrder);
				if (reader!=null)
				{
					gridCalls.DataSource = reader;
					gridCalls.DataKeyField = "ClientID";
					
					
					gridCalls.AllowSorting=false;
					
					gridCalls.DataBind();
					for(int i=0;i<gridCalls.Columns.Count;i++)
					{
						
						gridCalls.Columns[i].SortExpression=null;
					}
				}
			}
			
			finally
			{
				if (reader!=null) reader.Close();
			}

			gridCalls.MasterTableView.ExportToExcel("Clients");
		}

	}
}
