﻿<%@ Page language="c#" Codebehind="MeetingIssues.aspx.cs" AutoEventWireup="false" Inherits="Asa.Timesheet.WebPages.MeetingIssues" %>
<%@ Register TagPrefix="uc1" TagName="PageHeader" Src="UserControls/PageHeader.ascx" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
	<head runat="server">
		<title>MeetingIssues</title>
		<meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
		<meta content="C#" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<LINK href="styles/timesheet.css" type="text/css" rel="stylesheet">
		<script language="javascript" src="PopupCalendar.js"></script>
	</HEAD>
	<body bottomMargin="0" leftMargin="0" topMargin="0" rightMargin="0">
		<form id="Form1" method="post" runat="server">
			<table height="100%" cellSpacing="0" cellPadding="0" width="100%" border="0">
				<tr height="1">
					<td><uc1:pageheader id="header" runat="server"></uc1:pageheader></td>
				</tr>
				<tr>
					<td>
						<table height="100%" cellSpacing="0" cellPadding="0" width="100%" border="0">
							<tr>
								<td class="menuLeft"><asp:placeholder id="menuHolder" runat="server"></asp:placeholder></td>
								
								<td style="PADDING-LEFT: 5px; PADDING-TOP: 3px" vAlign="top" noWrap>
									<P>
										<TABLE id="Table1" style="WIDTH: 848px; HEIGHT: 26px" cellSpacing="1" cellPadding="1" width="848"
											border="0">
											<TR>
												<td style="WIDTH: 304px"><asp:label id="Label15" runat="server" Width="100%" EnableViewState="False" CssClass="enterDataLabel"
														Font-Bold="true">Изберете проект за печат на бележки:</asp:label></TD>
												<td><asp:dropdownlist id="ddlProject1" runat="server" Width="250px" CssClass="EnterDataBox" AutoPostBack="True"></asp:dropdownlist></TD>
												<td></TD>
												<td></TD>
											</TR>
											<TR>
												<td style="WIDTH: 304px" vAlign="top"><asp:label id="Label16" runat="server" Width="100%" CssClass="enterDataLabel" Font-Bold="true">Изберете включени теми:</asp:label></TD>
												<td><asp:checkbox id="ckAll" runat="server" AutoPostBack="True" Text="Всички теми"></asp:checkbox><asp:checkboxlist id="lst" CssClass="enterDataBox" CellPadding="0" CellSpacing="0" RepeatDirection="Vertical"
														Runat="server"></asp:checkboxlist><asp:checkbox id="ckInclude" runat="server" AutoPostBack="True" Text="Включи задачи АСА към АСА"
														Visible="False"></asp:checkbox></TD>
												<td></TD>
												<td></TD>
											</TR>
											<TR>
												<td style="WIDTH: 304px"><asp:button id="btnCancelUP" runat="server" Width="160px" EnableViewState="False" CssClass="ActionButton"
														Text="Обратно към срещата"></asp:button></TD>
												<td><asp:button id="btnSave" runat="server" CssClass="ActionButton" Text="Запиши"></asp:button>&nbsp;
													<asp:button id="btnNew" runat="server" Width="112px" CssClass="ActionButton" Text="Нова задача"></asp:button>&nbsp;
													<asp:button id="btnMark1" runat="server" CssClass="ActionButton" Width="120px" Text="Маркирай всички"></asp:button>
													<asp:button id="btnMinutes" runat="server"  EnableViewState="False" CssClass="ActionButton"
														Text="Бележки PDF"></asp:button></TD>
												<td></TD>
												<td></TD>
											</TR>
										</TABLE>
									</P>
									<asp:datalist id="dlHotIssues" runat="server" Width="100%" RepeatDirection="Horizontal" RepeatColumns="1">
										<EditItemStyle VerticalAlign="Top"></EditItemStyle>
										<ItemStyle VerticalAlign="Top"></ItemStyle>
										<ItemTemplate>
											<hr>
											<TABLE width="832px">
												<TR>
													<td class="enterDataLabel1">
														<b>Включена</b>:
													</TD>
													<td>
														<asp:CheckBox id="ckIncluded" runat="server"></asp:CheckBox>&nbsp;
														<asp:button id="btnEdit" runat="server" CssClass="ActionButton" Text="Редактирай задача" Width="150px"></asp:button>
													</TD>
													<td><b>
															<asp:label id="lbAssigned" runat="server" CssClass="enterDataLabel1" width="200px">Възложено на:</asp:label>
														</b>
													</TD>
													<td class="enterDataLabel1">
														<b>Статус:</b></TD>
													<td>
														<b>
															<asp:label id="lbDeadline" runat="server" CssClass="enterDataLabel1">Краен срок:</asp:label>
														</b>
													</TD>
												</TR>
												<TR>
													<td>
														<b>
															<ASP:LABEL id="lbHotIssueCategory" runat="server" CssClass="enterDataLabel1">Тема:</ASP:LABEL>
														</b>
													</TD>
													<td>
														<asp:label id=txtCategoryText runat="server" CssClass="EnterDataBox" text='<%# DataBinder.Eval(Container, "DataItem.HotIssueCategoryName") %>'>
														</asp:label>
														<asp:label id=lbCategoryText runat="server" Visible="False" text='<%# DataBinder.Eval(Container, "DataItem.HotIssueCategoryID") %>'>
														</asp:label></TD>
													<td>
														<asp:label id="DLlbAssignedTo" runat="server" CssClass="enterDataBox" Width="100%" Text='<%# AssignedToText(DataBinder.Eval(Container, "DataItem.AssignedTo"),DataBinder.Eval(Container, "DataItem.AssignedToType"),DataBinder.Eval(Container, "DataItem.AssignedToMore")) %>'>
														</asp:label></asp:label>
													</TD>
													<td>
														<asp:label id=lbStatusText runat="server" CssClass="enterDataLabel1" Visible="True" text='<%# DataBinder.Eval(Container, "DataItem.HotIssueStatusName") %>'>
														</asp:label></TD>
													<td>
														<asp:label id="Label1" runat="server" CssClass="enterDataBox" Width="100px" MaxLength="10" text='<%# Asa.Timesheet.Data.Util.TimeHelper.FormatDateO(DataBinder.Eval(Container, "DataItem.HotIssueDeadline")) %>'>
														</asp:label></TD>
												</TR>
												<TR>
													<td>
														<b>
															<ASP:LABEL id="lbHotIssueIme" runat="server" CssClass="enterDataLabel1">Задача:</ASP:LABEL>
														</b>
													</TD>
													<td>
														<asp:label id=txtHotIssueIme runat="server" CssClass="enterDataBox" Width="250px" Text='<%# DataBinder.Eval(Container, "DataItem.HotIssueName") %>'>
														</asp:label></TD>
													<td>
													</TD>
													<td>
														<asp:label id="Label14" runat="server" CssClass="enterDataBox" Width="250px" Text=''></asp:label>
														<asp:label id="Label12" runat="server" CssClass="error" Visible='<%# Asa.Timesheet.Data.Util.TimeHelper.FormatDateO(DataBinder.Eval(Container, "DataItem.ExecutionStopped"))!="" %>'>Спрян е срока на изпълнение:</asp:label>
														<asp:Label id=Label11 runat="server" CssClass="error" Text='<%# GetExecutionDate(DataBinder.Eval(Container, "DataItem.ExecutionStopped")) %>'>
														</asp:Label>
													</TD>
													<td>
														<asp:label id="txtStatusText" runat="server" CssClass="EnterDataBox" Visible="False"></asp:label></TD>
												</TR>
												<TR>
													<td>
														<b>
															<ASP:LABEL id="lbDiscription" runat="server" CssClass="enterDataLabel1">Описание:</ASP:LABEL>
														</b>
													</TD>
													<td>
														<asp:label id=txtDiscription runat="server" CssClass="enterDataBox" Width="250px" text='<%# DataBinder.Eval(Container, "DataItem.HotIssueDiscription") %>'>
														</asp:label></TD>
													<td>
														<asp:label id=lbMainID runat="server" Visible="False" text='<%# DataBinder.Eval(Container, "DataItem.HotIssueIDMain") %>'>
														</asp:label></TD>
													<td>
													</TD>
													<td>
													</TD>
												</TR>
												<TR>
													<td>
														<b>
															<asp:label id="lbComment" runat="server" CssClass="enterDataLabel1">Коментар:</asp:label>
														</b>
													</TD>
													<td>
														<asp:label id="DLlbComment" runat="server" CssClass="enterDataBox" Width="100%" text='<%# DataBinder.Eval(Container, "DataItem.HotIssueComment") %>'>
														</asp:label></TD>
													<td>
														<asp:label id="lbToClient" runat="server" CssClass="enterDataLabel1" Visible='<%# (bool)DataBinder.Eval(Container, "DataItem.ToClient") %>'>Към възложителя</asp:label><BR>
													</TD>
													<td>
													</TD>
													<td></TD>
												</TR>
											</TABLE>
										</ItemTemplate>
									</asp:datalist>
									<TABLE id="Table2" style="WIDTH: 848px; HEIGHT: 26px" cellSpacing="1" cellPadding="1" width="848"
										border="0">
										<TR>
											<td style="WIDTH: 304px"></TD>
											<td><asp:button id="btnSave1" runat="server" CssClass="ActionButton" Text="Запиши"></asp:button>&nbsp;
												<asp:button id="btnNew1" runat="server" Width="112px" CssClass="ActionButton" Text="Нова задача"></asp:button>&nbsp;
												<asp:button id="btnMark" runat="server" CssClass="ActionButton" Width="120px" Text="Маркирай всички"></asp:button>&nbsp;
												<asp:button id="btnPDF1" runat="server"  EnableViewState="False" CssClass="ActionButton"
													Text="Бележки PDF"></asp:button>
											</TD>
											<td></TD>
											<td></TD>
										</TR>
									</TABLE>
									<P>&nbsp;</P>
								</td>
							</tr>
						</table>
					</td>
				</tr>
			</table>
			<BR>
			<P></P>
		</form>
	</body>
</HTML>
