using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Web.Security;
using Asa.Timesheet.Data;
using log4net;
using log4net.Config;
namespace PM
{
	/// <summary>
	/// Summary description for PMLogin.
	/// </summary>
	public partial class Login : System.Web.UI.Page
	{
		private static readonly ILog log = LogManager.GetLogger(typeof(Login));
		//private static readonly ILog log = LogManager.GetLogger(typeof(Login));

        protected void Page_Load(object sender, System.EventArgs e)
        {
            Page.RegisterHiddenField("__EVENTTARGET", "btnEnter");
            LoadResourses();
            if (!this.IsPostBack)
            {
                //header.PageTitle = Resource.ResourceManager["login_PageTitle"];
                if (Session["loginattempts"] == null)
                {
                    Session["loginattempts"] = 1;
                }

            }
            if ((int)Session["loginattempts"] >= 3)
            {
                this.rowCaptcha.Visible = true;
            }


        }

        private void LoadResourses()
        {
            string lang = (Session["lang"] == null) ? Languages.BG.ToString() : (string)Session["lang"];
            lblLoginName.Text = UIHelpers.GetText(lang, "Name_LoginNameName");
            lblPassword.Text = UIHelpers.GetText(lang, "Name_PasswordName");
            btnEnter.Text = UIHelpers.GetText(lang, "Name_btnEnterName");

            //header.PageTitle =UIHelpers.GetText(lang,"pmPageTitle_Login");

        }

		#region Web Form Designer generated code
        override protected void OnInit(EventArgs e)
        {
            //
            // CODEGEN: This call is required by the ASP.NET Web Form Designer.
            //
            InitializeComponent();
            base.OnInit(e);
        }
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
        private void InitializeComponent()
        {

        }
		#endregion

        protected void btnEnter_Click(object sender, System.EventArgs e)
        {
            FormsAuthentication.SignOut();

            if (txtLoginName.Text.Trim() == String.Empty)
            {
                AlertFieldNotEntered(this.lblLoginName);
                return;
            }

            if (txtPassword.Text.Trim() == String.Empty)
            {
                AlertFieldNotEntered(this.lblPassword);
                return;
            }

            string account = String.Empty;
            int num = (int)Session["loginattempts"];

            if (num > 3)
            {
                if (num > 3)
                {
                    if (ctrlGoogleReCaptcha.Validate())
                    {
                        //submit form success
                        lblError.Text = "����� � �������!";
                    }
                    else
                    {
                        //captcha challenge failed
                        lblError.Text = "���������� ������� ���";
                    }
                }
            }
            num++;
            Session["loginattempts"] = num;

            try
            {
                account = DBManager.SelectUserAccount(txtLoginName.Text, txtPassword.Text);
            }
            catch (Exception ex)
            {
                log.Error(ex);
                lblError.Text = Resource.ResourceManager["login_ErrorCheckCredentials"];
                return;
            }

            if (account == null)
            {
                account = DBManager.SelectUserPMAccount(txtLoginName.Text, txtPassword.Text);
                if (account == null)
                {
                    lblError.Text = Resource.ResourceManager["login_ErrorInvalidCredentials"];
                    return;
                }
            }


            FormsAuthentication.RedirectFromLoginPage(txtLoginName.Text, false);

        }

        protected static void AlertFieldNotEntered(Label lbl)
        {
            lbl.ForeColor = System.Drawing.Color.Red;
        }
	}
}
