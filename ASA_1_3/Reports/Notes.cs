using System;
using GrapeCity.ActiveReports;
using GrapeCity.ActiveReports.Controls;
using GrapeCity.ActiveReports.SectionReportModel;
using GrapeCity.ActiveReports.Document.Section;
using GrapeCity.ActiveReports.Document;

namespace Asa.Timesheet.WebPages.Reports
{
    public class cs : GrapeCity.ActiveReports.SectionReport
	{
        public cs(string name, string address, string text, string user)
        {
            InitializeComponent();
            this.ReportEnd += new EventHandler(cs_ReportEnd);
            txtUser.Text = user;
            txtText.Text = text;
            txtObekt.Text = name;
            txtAddress.Text = address;
            txtDate.Text = DateTime.Today.ToShortDateString();
        }

		#region ActiveReports Designer generated code

















        public void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(cs));
            this.Detail = new GrapeCity.ActiveReports.SectionReportModel.Detail();
            this.ReportHeader = new GrapeCity.ActiveReports.SectionReportModel.ReportHeader();
            this.ReportFooter = new GrapeCity.ActiveReports.SectionReportModel.ReportFooter();
            this.PageHeader = new GrapeCity.ActiveReports.SectionReportModel.PageHeader();
            this.PageFooter = new GrapeCity.ActiveReports.SectionReportModel.PageFooter();
            this.txtReportTitle = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.TextBox1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtObekt = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtAddress = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.TextBox4 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.TextBox5 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.TextBox6 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.TextBox7 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtDate = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtText = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtUser = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.TextBox9 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            ((System.ComponentModel.ISupportInitialize)(this.txtReportTitle)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TextBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtObekt)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAddress)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TextBox4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TextBox5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TextBox6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TextBox7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtText)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtUser)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TextBox9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            // 
            // Detail
            // 
            this.Detail.ColumnSpacing = 0F;
            this.Detail.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
						this.txtText});
            this.Detail.Height = 0.4895833F;
            this.Detail.Name = "Detail";
            // 
            // ReportHeader
            // 
            this.ReportHeader.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
						this.txtReportTitle,
						this.TextBox1,
						this.txtObekt,
						this.txtAddress,
						this.TextBox4,
						this.TextBox5,
						this.TextBox6,
						this.TextBox7,
						this.txtDate});
            this.ReportHeader.Height = 4.1875F;
            this.ReportHeader.Name = "ReportHeader";
            // 
            // ReportFooter
            // 
            this.ReportFooter.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
						this.txtUser,
						this.TextBox9});
            this.ReportFooter.Height = 0.4583333F;
            this.ReportFooter.Name = "ReportFooter";
            // 
            // PageHeader
            // 
            this.PageHeader.Height = 0.01041667F;
            this.PageHeader.Name = "PageHeader";
            // 
            // PageFooter
            // 
            this.PageFooter.Height = 0.3847222F;
            this.PageFooter.Name = "PageFooter";
            // 
            // txtReportTitle
            // 
            this.txtReportTitle.Height = 0.2755906F;
            this.txtReportTitle.Left = 0F;
            this.txtReportTitle.Name = "txtReportTitle";
            this.txtReportTitle.Style = "font-size: 12pt; font-weight: bold; text-align: justify";
            this.txtReportTitle.Text = "                 ??????????? ???????";
            this.txtReportTitle.Top = 1.4375F;
            this.txtReportTitle.Width = 7.086611F;
            // 
            // TextBox1
            // 
            this.TextBox1.Height = 0.2362205F;
            this.TextBox1.Left = 0.7874014F;
            this.TextBox1.Name = "TextBox1";
            this.TextBox1.Style = "font-size: 11.25pt; font-weight: bold";
            this.TextBox1.Text = "?????:";
            this.TextBox1.Top = 2.125F;
            this.TextBox1.Width = 1.181102F;
            // 
            // txtObekt
            // 
            this.txtObekt.DataField = "ProjectName";
            this.txtObekt.Height = 0.2362205F;
            this.txtObekt.Left = 1.968504F;
            this.txtObekt.Name = "txtObekt";
            this.txtObekt.Style = "font-size: 11.25pt; font-weight: bold";
            this.txtObekt.Text = "TextBox2";
            this.txtObekt.Top = 2.125F;
            this.txtObekt.Width = 4.330709F;
            // 
            // txtAddress
            // 
            this.txtAddress.DataField = "Address";
            this.txtAddress.Height = 0.2F;
            this.txtAddress.Left = 1.968504F;
            this.txtAddress.Name = "txtAddress";
            this.txtAddress.Style = "font-size: 11pt; font-weight: normal; text-align: left";
            this.txtAddress.Text = "TextBox3";
            this.txtAddress.Top = 2.395669F;
            this.txtAddress.Width = 4.330709F;
            // 
            // TextBox4
            // 
            this.TextBox4.Height = 0.2362205F;
            this.TextBox4.Left = 0.8125F;
            this.TextBox4.Name = "TextBox4";
            this.TextBox4.Style = "font-size: 11.25pt; font-weight: bold";
            this.TextBox4.Text = "????:";
            this.TextBox4.Top = 3F;
            this.TextBox4.Width = 1.181102F;
            // 
            // TextBox5
            // 
            this.TextBox5.Height = 0.2362205F;
            this.TextBox5.Left = 2F;
            this.TextBox5.Name = "TextBox5";
            this.TextBox5.Style = "font-size: 11.25pt; font-weight: bold; vertical-align: middle";
            this.TextBox5.Text = "???????????";
            this.TextBox5.Top = 3F;
            this.TextBox5.Width = 4.3125F;
            // 
            // TextBox6
            // 
            this.TextBox6.Height = 0.2362205F;
            this.TextBox6.Left = 0.8125F;
            this.TextBox6.Name = "TextBox6";
            this.TextBox6.Style = "font-size: 11.25pt; font-weight: bold";
            this.TextBox6.Text = "????:";
            this.TextBox6.Top = 3.5F;
            this.TextBox6.Width = 1.181102F;
            // 
            // TextBox7
            // 
            this.TextBox7.Height = 0.2362205F;
            this.TextBox7.Left = 2F;
            this.TextBox7.Name = "TextBox7";
            this.TextBox7.Style = "font-size: 11.25pt; font-weight: bold; vertical-align: middle";
            this.TextBox7.Text = "??";
            this.TextBox7.Top = 3.5F;
            this.TextBox7.Width = 4.3125F;
            // 
            // txtDate
            // 
            this.txtDate.Height = 0.2362205F;
            this.txtDate.Left = 4.9375F;
            this.txtDate.Name = "txtDate";
            this.txtDate.Style = "font-family: Times New Roman; font-size: 11.25pt; font-weight: normal; vertical-a" +
                "lign: top";
            this.txtDate.Text = "TextBox8";
            this.txtDate.Top = 0F;
            this.txtDate.Width = 2.125F;
            // 
            // txtText
            // 
            this.txtText.Height = 0.2362205F;
            this.txtText.Left = 0.8125F;
            this.txtText.Name = "txtText";
            this.txtText.Style = "font-family: Times New Roman; font-size: 11.25pt; font-weight: normal; vertical-a" +
                "lign: top";
            this.txtText.Text = "TextBox8";
            this.txtText.Top = 0.0625F;
            this.txtText.Width = 5.5F;
            // 
            // txtUser
            // 
            this.txtUser.Height = 0.2362205F;
            this.txtUser.Left = 4.9375F;
            this.txtUser.Name = "txtUser";
            this.txtUser.Style = "font-family: Times New Roman; font-size: 11.25pt; font-weight: normal; vertical-a" +
                "lign: top";
            this.txtUser.Text = "TextBox8";
            this.txtUser.Top = 0F;
            this.txtUser.Width = 2.125F;
            // 
            // TextBox9
            // 
            this.TextBox9.Height = 0.2362205F;
            this.TextBox9.Left = 4.1875F;
            this.TextBox9.Name = "TextBox9";
            this.TextBox9.Style = "font-size: 9.75pt; font-weight: normal";
            this.TextBox9.Text = "????????:";
            this.TextBox9.Top = 0F;
            this.TextBox9.Width = 0.75F;
            // 
            // ActiveReport1
            // 
            this.MasterReport = false;
            this.PageSettings.DefaultPaperSize = false;
            this.PageSettings.Margins.Bottom = 0.4097222F;
            this.PageSettings.Margins.Left = 1.377778F;
            this.PageSettings.Margins.Right = 0.8034722F;
            this.PageSettings.Margins.Top = 0.8034722F;
            this.PageSettings.PaperHeight = 11.69306F;
            this.PageSettings.PaperKind = System.Drawing.Printing.PaperKind.A4;
            this.PageSettings.PaperWidth = 8.268056F;
            this.PrintWidth = 7.086611F;
            this.Sections.Add(this.ReportHeader);
            this.Sections.Add(this.PageHeader);
            this.Sections.Add(this.Detail);
            this.Sections.Add(this.PageFooter);
            this.Sections.Add(this.ReportFooter);
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule(resources.GetString("$this.StyleSheet"), "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: inherit; font-style: inherit; font-variant: inherit; font-weight: bo" +
                        "ld; font-size: 16pt; font-size-adjust: inherit; font-stretch: inherit", "Heading1", "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: Times New Roman; font-style: italic; font-variant: inherit; font-wei" +
                        "ght: bold; font-size: 14pt; font-size-adjust: inherit; font-stretch: inherit", "Heading2", "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: inherit; font-style: inherit; font-variant: inherit; font-weight: bo" +
                        "ld; font-size: 13pt; font-size-adjust: inherit; font-stretch: inherit", "Heading3", "Normal"));
            ((System.ComponentModel.ISupportInitialize)(this.txtReportTitle)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TextBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtObekt)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAddress)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TextBox4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TextBox5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TextBox6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TextBox7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtText)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtUser)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TextBox9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();
        }

		#endregion

        private void cs_ReportEnd(object sender, EventArgs e)
        {
            Logo logo = new Logo();
            logo.Run();

            for (int i = 0; i < this.Document.Pages.Count; i++)
                this.Document.Pages[i].Overlay(logo.Document.Pages[0]);
        }

        private ReportHeader ReportHeader;
        private TextBox txtReportTitle;
        private TextBox TextBox1;
        private TextBox txtObekt;
        private TextBox txtAddress;
        private TextBox TextBox4;
        private TextBox TextBox5;
        private TextBox TextBox6;
        private TextBox TextBox7;
        private TextBox txtDate;
        private PageHeader PageHeader;
        private Detail Detail;
        private TextBox txtText;
        private PageFooter PageFooter;
        private ReportFooter ReportFooter;
        private TextBox txtUser;
        private TextBox TextBox9;
	}
}
