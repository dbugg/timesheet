using System;
using System.Data;
using System.Data.SqlClient; 
using System.Collections;
using Microsoft.ApplicationBlocks.Data;

namespace Asa.Timesheet.Data
{
	public class InsuranceAgentDAL
	{
		/// <summary>
		/// Loads a InsuranceAgentData object from the database using the given InsuranceAgentID
		/// </summary>
		/// <param name="insuranceAgentID">The primary key value</param>
		/// <returns>A InsuranceAgentData object</returns>
		public static InsuranceAgentData Load(int insuranceAgentID)
		{
			SqlParameter[] parameterValues = new SqlParameter[] { new SqlParameter("@InsuranceAgentID", insuranceAgentID) };
			using (SqlDataReader reader = SqlHelper.ExecuteReader(CommonDAL.ConnectionString, CommandType.StoredProcedure, "InsuranceAgentsSelProc", parameterValues))
			{
				if (reader.Read())
				{
					InsuranceAgentData result = new InsuranceAgentData();
					LoadFromReader(reader,result);
					return result;
				}
				else
					return null;
			}
		}

		/// <summary>
		/// Loads a InsuranceAgentData object from the database using the given where clause
		/// </summary>
		/// <param name="whereClause">The filter expression for the query</param>
		/// <returns>A InsuranceAgentData object</returns>
		public static InsuranceAgentData Load(string whereClause)
		{
			SqlParameter[] parameterValues = new SqlParameter[] { new SqlParameter("@WhereClause", whereClause) };
			using (SqlDataReader reader = SqlHelper.ExecuteReader(CommonDAL.ConnectionString, CommandType.StoredProcedure, "InsuranceAgentsListProc", parameterValues))
			{
				if (reader.Read())
				{
					InsuranceAgentData result = new InsuranceAgentData();
					LoadFromReader(reader,result);
					return result;
				}
				else
					return null;
			}
		}
		public static int Save( InsuranceAgentData source)
		{
			if (source.InsuranceAgentID == -1)
				return Insert(source);
			else
				return Update(source);	
		}

		public static void Delete( InsuranceAgentData source)
		{
			Delete(source.InsuranceAgentID);
		}
		/// <summary>
		/// Loads a collection of InsuranceAgentData objects from the database.
		/// </summary>
		/// <returns>A collection containing all of the InsuranceAgentData objects in the database.</returns>
		public static InsuranceAgentsVector LoadCollection()
		{
			SqlParameter[] parms = new SqlParameter[] {};
			return LoadCollection("InsuranceAgentsListProc", parms);
		}
	  
		/// <summary>
		/// Saves a collection of InsuranceAgentData objects to the database.
		/// </summary>
		/// <returns>A collection containing all of the InsuranceAgentData objects in the database.</returns>
		public static  void  SaveCollection(InsuranceAgentsVector col,string spName, SqlParameter[] parms)
		{
			InsuranceAgentsVector colOld = LoadCollection(spName,parms);
			UpdateCollection(colOld,col);
		}
		public static  void  SaveCollection(InsuranceAgentsVector col)
		{
			InsuranceAgentsVector colOld = LoadCollection();
			UpdateCollection(colOld,col);
		}
		public static  void  SaveCollection(InsuranceAgentsVector col, string whereClause )
		{
			InsuranceAgentsVector colOld = LoadCollection(whereClause);
			UpdateCollection(colOld,col);
		}
		/// <summary>
		/// Loads a collection of InsuranceAgentData objects from the database.
		/// </summary>
		/// <returns>A collection containing all of the InsuranceAgentData objects in the database.</returns>
		public static InsuranceAgentsVector LoadCollection(string whereClause)
		{
			SqlParameter[] parms = new SqlParameter[] { new SqlParameter("@WhereClause", whereClause) };
			return LoadCollection("InsuranceAgentsListProc", parms);
		}

		/// <summary>
		/// Loads a collection of InsuranceAgentData objects from the database.
		/// </summary>
		/// <returns>A collection containing all of the InsuranceAgentData objects in the database.</returns>
		public static InsuranceAgentsVector LoadCollection(string spName, SqlParameter[] parms)
		{
			InsuranceAgentsVector result = new InsuranceAgentsVector();
			using (SqlDataReader reader = SqlHelper.ExecuteReader(CommonDAL.ConnectionString, CommandType.StoredProcedure, spName, parms))
			{
				while (reader.Read())
				{
					InsuranceAgentData tmp = new InsuranceAgentData();
					LoadFromReader(reader, tmp);
					result.Add(tmp);
				}
			}
			return result;
		}
		/// <summary>
		/// Deletes a InsuranceAgentData object from the database.
		/// </summary>
		/// <param name="insuranceAgentID">The primary key value</param>
		public static void Delete(int insuranceAgentID)
		{
			SqlParameter[] parameterValues = new SqlParameter[] { new SqlParameter("@InsuranceAgentID", insuranceAgentID) };
			SqlHelper.ExecuteNonQuery(CommonDAL.ConnectionString, CommandType.StoredProcedure, "InsuranceAgentsInactiveProc", parameterValues);
		}

		public static void LoadFromReader(SqlDataReader reader, InsuranceAgentData target)
		{
			if (reader != null && !reader.IsClosed)
			{
				//target.Key = reader.GetInt32(0);
				if (!reader.IsDBNull(0)) target.InsuranceAgentID = reader.GetInt32(0);
				if (!reader.IsDBNull(1)) target.InsuranceAgentName = reader.GetString(1);
				if (!reader.IsDBNull(2)) target.InsurancePhones = reader.GetString(2);
				if (!reader.IsDBNull(3)) target.IsActive = reader.GetBoolean(3);
			}
		}
		
	  
		public static int Insert( InsuranceAgentData source)
		{
			SqlParameter[] parameterValues = GetInsertParameterValues(source);
			SqlHelper.ExecuteNonQuery(CommonDAL.ConnectionString, CommandType.StoredProcedure, "InsuranceAgentsInsProc", parameterValues);
			source.InsuranceAgentID = (int) parameterValues[0].Value;
			return source.InsuranceAgentID;
		}

		public static int Update( InsuranceAgentData source)
		{
			SqlParameter[] parameterValues = GetUpdateParameterValues(source);
			SqlHelper.ExecuteNonQuery(CommonDAL.ConnectionString, CommandType.StoredProcedure,
				"InsuranceAgentsUpdProc", parameterValues);
			return source.InsuranceAgentID;
		}
		private static void UpdateCollection(InsuranceAgentsVector colOld,InsuranceAgentsVector col)
		{
			// Delete all old items
			foreach( InsuranceAgentData itemOld in colOld)
			{
				if(!ExistsKey(itemOld.InsuranceAgentID, col))
					Delete(itemOld.InsuranceAgentID);
			}
			foreach( InsuranceAgentData item in col)
			{
				if(item.InsuranceAgentID <= -1)
					Insert(item);
				else
					Update(item);
			}
		}
		private static bool ExistsKey(int Key, InsuranceAgentsVector col)
		{
			foreach( InsuranceAgentData item in col)
				if(item.InsuranceAgentID==Key)
					return true;
			return false;
		}

		private static SqlParameter GetSqlParameter(string name, ParameterDirection direction, object value)
		{
			SqlParameter p = new SqlParameter(name, value);
			p.Direction = direction;
			return p;
		}

		private static SqlParameter[] GetInsertParameterValues( InsuranceAgentData source)
		{
			SqlParameter[] prams = new SqlParameter[4];
			prams[0] = GetSqlParameter("@InsuranceAgentID", ParameterDirection.Output, source.InsuranceAgentID);
			prams[1] = GetSqlParameter("@InsuranceAgentName", ParameterDirection.Input, source.InsuranceAgentName);
			prams[2] = GetSqlParameter("@InsurancePhones", ParameterDirection.Input, source.InsurancePhones);
			prams[3] = GetSqlParameter("@IsActive", ParameterDirection.Input, source.IsActive);
			return prams;
		}

		private static SqlParameter[] GetUpdateParameterValues( InsuranceAgentData source)
		{
			return new SqlParameter[] {
										  new SqlParameter("@InsuranceAgentID", source.InsuranceAgentID),
										  new SqlParameter("@InsuranceAgentName", source.InsuranceAgentName),
										  new SqlParameter("@InsurancePhones", source.InsurancePhones),
										  new SqlParameter("@IsActive", source.IsActive)
									  };
		}
	}
}   

