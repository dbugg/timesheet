using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Data.SqlClient;
using log4net;
using Asa.Timesheet.Data;
using Asa.Timesheet.Data.Entities;
using Asa.Timesheet.Data.Util;
using GrapeCity.ActiveReports.Export.Pdf.Section;
using GrapeCity.ActiveReports.Export.Excel.Section;

namespace Asa.Timesheet.WebPages.Reports
{
	/// <summary>
	/// Summary description for Calendar.
	/// </summary>
	public class Calendar : TimesheetPageBase
	{
		protected System.Web.UI.WebControls.PlaceHolder menuHolder;
		protected System.Web.UI.WebControls.Label lblInfo;
		protected System.Web.UI.WebControls.CheckBox ckDates;
		protected System.Web.UI.WebControls.Label lblStartDay;
		protected System.Web.UI.WebControls.Label lblSelStartDate;
		protected System.Web.UI.WebControls.Calendar calStartDate;
		protected System.Web.UI.WebControls.Button btnOK;
		protected System.Web.UI.WebControls.Button Button1;
		protected System.Web.UI.WebControls.RadioButtonList rbb;
		protected System.Web.UI.WebControls.TextBox txtDates;
		protected Asa.Timesheet.WebPages.UserControls.PageHeader header;

	
		private const int _DAYSID=1;
        private void Page_Load(object sender, System.EventArgs e)
        {
            if (!IsPostBack)
            {

                this.header.PageTitle = Resource.ResourceManager["calendar_PageTitle"];
                this.header.UserName = this.LoggedUser.UserName;


                calStartDate.SelectedDate = DateTime.Today;
                int nDays = CalendarDAL.ExecuteDaysSelProc(_DAYSID);
                if (nDays == -1)
                    ckDates.Checked = true;
                else
                    txtDates.Text = nDays.ToString();

                SetDate();

                SetChecked();
            }
            UIHelpers.CreateMenuReports(menuHolder, LoggedUser);
        }
//		private void CreateMenu()
//		{
//			UserControls.MenuTable menu = new UserControls.MenuTable();
//			menu.ID = "MenuTable";
//
//			// 'Links' group
//			ArrayList menuItems = new ArrayList();
//
//			menuItems.Add(new MenuItemInfo(Resource.ResourceManager["menuItem_Hours"], "../Hours.aspx", true));
//			menuItems.Add(new MenuItemInfo(Resource.ResourceManager["menuItem_Projects"], "../Projects.aspx"));
//			menuItems.Add(new MenuItemInfo(Resource.ResourceManager["menuItem_Clients"], "../Clients.aspx"));
//							menuItems.Add(new MenuItemInfo(Resource.ResourceManager["menuItem_Subcontracters"], "../Subcontracters.aspx"));
//			if (LoggedUser.IsLeader)
//			menuItems.Add(new MenuItemInfo(Resource.ResourceManager["menuItem_Users"], "../Users.aspx"));	
//			menuItems.Add(new MenuItemInfo(Resource.ResourceManager["menuItem_Emails"], "../Emails.aspx"));
//
//			menu.AddMenuGroup(Resource.ResourceManager["menuHeader_Links"], 20, menuItems);
//			
//			// 'New' group
//			menuItems = new ArrayList();
//			menuItems.Add(new MenuItemInfo(Resource.ResourceManager["menuItem_NewProject"], "../EditProject.aspx"));
//			menuItems.Add(new MenuItemInfo(Resource.ResourceManager["menuItem_NewClient"], "../EditClient.aspx"));
//			if (LoggedUser.IsLeader)
//			menuItems.Add(new MenuItemInfo(Resource.ResourceManager["menuItem_NewUser"], "../EditUser.aspx"));
//			menuItems.Add(new MenuItemInfo(Resource.ResourceManager["menuItem_NewEmail"], "../EditEmail.aspx"));
//			menuItems.Add(new MenuItemInfo(Resource.ResourceManager["newcontracter_PageTitle"], "../EditSubContracter.aspx"));
//
//
//			menu.AddMenuGroup(Resource.ResourceManager["menuHeader_New"], 30, menuItems);
//
//			// "Reports' group
//			menuItems = new ArrayList();
//			
//			menuItems.Add(new MenuItemInfo(Resource.ResourceManager["menuItem_WorkTimeReport"], "WorkTimes.aspx"));
//			menuItems.Add(new MenuItemInfo(Resource.ResourceManager["menuItem_MinutesReport"], "Minutes.aspx"));
//			menuItems.Add(new MenuItemInfo(Resource.ResourceManager["menuItem_MailReport"], "../MailForm.aspx"));
//			menuItems.Add(new MenuItemInfo(Resource.ResourceManager["menuItem_NotEnteredWorkTime"], "EmptyHours.aspx"));
//
//			menu.AddMenuGroup(Resource.ResourceManager["menuHeader_Reports"], 30, menuItems);
//
//			menuItems = new ArrayList();
//			menuItems.Add(new MenuItemInfo(Resource.ResourceManager["menuItem_Help"], SessionManager.GetReportsHelpLink(),true,Pages.WorkTimes));
//			menu.AddMenuGroup("", 10, menuItems);
//
//			menuHolder.Controls.Add(menu);
//		}

		#region Web Form Designer generated code
        override protected void OnInit(EventArgs e)
        {
            //
            // CODEGEN: This call is required by the ASP.NET Web Form Designer.
            //
            InitializeComponent();
            base.OnInit(e);
        }
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
        private void InitializeComponent()
        {
            this.ckDates.CheckedChanged += new System.EventHandler(this.ckDates_CheckedChanged);
            this.calStartDate.SelectionChanged += new System.EventHandler(this.calStartDate_SelectionChanged);
            this.btnOK.Click += new System.EventHandler(this.btnOK_Click);
            this.Button1.Click += new System.EventHandler(this.Button1_Click);
            this.Load += new System.EventHandler(this.Page_Load);

        }
		#endregion

        private void ckDates_CheckedChanged(object sender, System.EventArgs e)
        {
            SetChecked();
        }
        private void LoadData()
        {
            SetDate();

            SetChecked();
        }
        private void SetDate()
        {
            lblSelStartDate.Text = String.Concat("(", calStartDate.SelectedDates[0].ToString("d"), ")");

            DateTime dt = calStartDate.SelectedDate;
            rbb.SelectedIndex = CalendarDAL.IsHoliday(dt) ? 1 : 0;
        }
        private void SetChecked()
        {
            bool bCh = ckDates.Checked;
            if (bCh)
            {
                txtDates.Text = "";
                txtDates.Enabled = false;
            }
            else
            {

                txtDates.Enabled = true;
            }
        }

        private void btnOK_Click(object sender, System.EventArgs e)
        {
            CalendarDAL.ExecuteCalendarUpdProc(calStartDate.SelectedDate, rbb.SelectedIndex == 1 ? true : false);
        }

        private void Button1_Click(object sender, System.EventArgs e)
        {
            int nDays = -1;
            if (!ckDates.Checked && txtDates.Text != "")
                nDays = int.Parse(txtDates.Text);
            CalendarDAL.ExecuteDaysUpdProc(_DAYSID, nDays);
        }

        private void calStartDate_SelectionChanged(object sender, System.EventArgs e)
        {
            SetDate();
        }
	}
}
