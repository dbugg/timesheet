<%@ Control Language="c#" AutoEventWireup="false" Codebehind="Documents.ascx.cs" Inherits="Asa.Timesheet.WebPages.UserControls.Documents" TargetSchema="http://schemas.microsoft.com/intellisense/ie5"%>
<TABLE id="Table1" cellSpacing="0" cellPadding="0" border="0">
	<TR>
		<TD><asp:label id="Label58" runat="server" CssClass="lblDiv1">Документи&nbsp;</asp:label></TD>
		<TD>
			&nbsp;
			<asp:DataList id="dlDocs" runat="server" RepeatDirection="Horizontal">
				<ItemTemplate>
					<asp:HyperLink id="btnScan" runat="server" ToolTip='<%# DataBinder.Eval(Container, "DataItem.DocumentDiscription")%>' NavigateUrl='<%# GetURL((int)DataBinder.Eval(Container, "DataItem.ProjectDocumentID"))%>' Target=_blank>
						<img border="0" src="images/pdf.gif" /></asp:HyperLink>&nbsp;
				</ItemTemplate>
			</asp:DataList></TD>
		<TD><INPUT id="FileCtrl" type="file" name="File1" runat="server"></TD>
	</TR>
	<tr>
		<td>
			<asp:Label ID="lbDescription" Runat="server" CssClass="enterDataLabel">Описание на документ</asp:Label>
		</td>
		<td>
		</td>
		<td>
			<asp:TextBox ID="txtDescription" Runat="server" Width="240px"></asp:TextBox>
		</td>
	</tr>
</TABLE>
