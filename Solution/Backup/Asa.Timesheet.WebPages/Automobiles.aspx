﻿<%@ Register TagPrefix="uc1" TagName="PageHeader" Src="UserControls/PageHeader.ascx" %>
<%@ Register TagPrefix="radg" Namespace="Telerik.WebControls" Assembly="RadGrid" %>
<%@ Page language="c#" Codebehind="Automobiles.aspx.cs" AutoEventWireup="false" Inherits="Asa.Timesheet.WebPages.Automobiles" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<html>
	<head id="Head1" runat="server">
		<title>Automobiles</title>
		<meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
		<meta content="C#" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<LINK href="styles/timesheet.css" type="text/css" rel="stylesheet">
	</HEAD>
	<body bottomMargin="0" leftMargin="0" topMargin="0" rightMargin="0">
		<form id="Form1" method="post" runat="server">
			<table cellSpacing="0" cellPadding="0" width="100%" border="0">
				<tr height="1">
					<td>
						<uc1:PageHeader id="header" runat="server"></uc1:PageHeader>
					</td>
				</tr>
				<tr>
					<td>
						<table cellSpacing="0" cellPadding="0" width="100%" border="0">
							<tr>
								<td class="menuLeft"><asp:placeholder id="menuHolder" runat="server"></asp:placeholder></td>
								<td valign="top"><br>
									<asp:label style="Z-INDEX: 0" id="Label10" runat="server" Font-Bold="True" EnableViewState="False">Автомобили</asp:label>
									<br>
									<br>
									<asp:panel id="grid"
										runat="server" Height="450px" Width="100%">
										<asp:datagrid id="grdAuto" runat="server" Width="100%" CssClass="ReportGrid" BackColor="Transparent"
											CellPadding="2" AutoGenerateColumns="False" AllowSorting="False">
											<AlternatingItemStyle CssClass="GridAltItem" BackColor="WhiteSmoke"></AlternatingItemStyle>
											<ItemStyle CssClass="GridItem"></ItemStyle>
											<HeaderStyle CssClass="GridHeader"></HeaderStyle>
											<Columns>
												<asp:TemplateColumn HeaderText="#">
													<ItemStyle Width="10px" ForeColor="DimGray"></ItemStyle>
													<ItemTemplate>
														<asp:Label CssClass="menuTable" runat="server" Text='<%# (int)DataBinder.Eval(Container, "ItemIndex")+1 %>' ID="Label1" NAME="Label1">
														</asp:Label>
													</ItemTemplate>
												</asp:TemplateColumn>
												<asp:TemplateColumn HeaderText="Марка" SortExpression="Name">
						  
													<ItemTemplate>
														<asp:LinkButton id="lnk" runat="server" CssClass="menuTable"  CommandName="Edit">
															<span Class="menuTable">
																<%# DataBinder.Eval(Container, "DataItem.Name") %>
															</span>
														</asp:LinkButton>
													</ItemTemplate>
												</asp:TemplateColumn>
												<asp:BoundColumn DataField="RegNumber" SortExpression="RegNumber" HeaderText="Рег. номер" >
                                                       <ItemStyle Width="90px" ForeColor="DimGray" HorizontalAlign=Center ></ItemStyle>
                                                </asp:BoundColumn>
							   
							
												<asp:TemplateColumn SortExpression="CivilDate" HeaderText="Изтичане Гражд. отговорност" ItemStyle-ForeColor="DimGray">
													<ItemTemplate>
														<asp:Label runat="server" Text='<%# Asa.Timesheet.Data.Util.TimeHelper.FormatDate((DateTime)DataBinder.Eval(Container, "DataItem.CivilDate"))%>' ID="Label3" NAME="Label1">
														</asp:Label>
														<asp:Label runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.CivilIns")%>' ID="Label6" NAME="Label1">
														</asp:Label>
													</ItemTemplate>
												</asp:TemplateColumn>
												<asp:TemplateColumn SortExpression="KaskoDate" HeaderText="Изтичане Каско" ItemStyle-ForeColor="DimGray">
													<ItemTemplate>
														<asp:Label runat="server" Text='<%# Asa.Timesheet.Data.Util.TimeHelper.FormatDate((DateTime)DataBinder.Eval(Container, "DataItem.KaskoDate"))%>' ID="Label4" NAME="Label1">
														</asp:Label>
														<asp:Label runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.KaskoIns")%>' ID="Label11" NAME="Label1"></asp:Label>
													</ItemTemplate>
												</asp:TemplateColumn>
												<asp:TemplateColumn SortExpression="GreencardDate" HeaderText="Изтичане Зелена карта" ItemStyle-ForeColor="DimGray">
													<ItemTemplate>
														<asp:Label runat="server" Text='<%# Asa.Timesheet.Data.Util.TimeHelper.FormatDate((DateTime)DataBinder.Eval(Container, "DataItem.GreencardDate"))%>' ID="Label7" NAME="Label1"></asp:Label>
														<asp:Label runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.GreenIns")%>' ID="Label12" NAME="Label1">
														</asp:Label>
													</ItemTemplate>
												</asp:TemplateColumn>
												<asp:TemplateColumn SortExpression="AnnualCheckDate" HeaderText="Дата на годишен техн. преглед" ItemStyle-ForeColor="DimGray">
													<ItemTemplate>
														<asp:Label runat="server" Text='<%# Asa.Timesheet.Data.Util.TimeHelper.FormatDate((DateTime)DataBinder.Eval(Container, "DataItem.AnnualCheckDate"))%>' ID="Label8" NAME="Label1">
														</asp:Label>
													</ItemTemplate>
												</asp:TemplateColumn>
												<asp:TemplateColumn SortExpression="NextChangeDate" HeaderText="Дата на следваща смяна на маслото" ItemStyle-ForeColor="DimGray">
													<ItemTemplate>
														<asp:Label runat="server" Text='<%# Asa.Timesheet.Data.Util.TimeHelper.FormatDate((DateTime)DataBinder.Eval(Container, "DataItem.NextChangeDate"))%>' ID="Label9" NAME="Label1">
														</asp:Label>
													</ItemTemplate>
												</asp:TemplateColumn>
												<asp:BoundColumn DataField="NextChangeKm" SortExpression="NextChangeKm" HeaderText="Смяна на масло на км:"></asp:BoundColumn>
												<asp:BoundColumn DataField="AutoService" SortExpression="AutoService" HeaderText="Сервиз"></asp:BoundColumn>
												<asp:TemplateColumn>
													<HeaderStyle Wrap="False" Width="20px"></HeaderStyle>
													<ItemStyle Wrap="False" HorizontalAlign="Center"></ItemStyle>
													<ItemTemplate>
														<asp:ImageButton id="ibEditItem" runat="server" ImageUrl="images/edit1.gif" ToolTip="Редактирай"></asp:ImageButton>
													</ItemTemplate>
												</asp:TemplateColumn>
												<asp:TemplateColumn>
													<HeaderStyle Wrap="False" Width="20px"></HeaderStyle>
													<ItemStyle Wrap="False" HorizontalAlign="Center"></ItemStyle>
													<ItemTemplate>
														<asp:ImageButton id="ibDeleteItem" runat="server" Width="12px" Height="12px" ImageUrl="images/delete.gif"
															ToolTip="Изтрий"></asp:ImageButton>
													</ItemTemplate>
												</asp:TemplateColumn>
											</Columns>
										</asp:datagrid>
										<BR>
										<BR>
										<asp:label id="lblName" runat="server" EnableViewState="False" Font-Bold="True">Застрахователни агенти</asp:label>
										<BR>
										<BR>
										<asp:datagrid id="grdInsuranceAgents" runat="server" Width="100%" CssClass="ReportGrid" BackColor="Transparent"
											CellPadding="4" AutoGenerateColumns="False" AllowSorting="False">
											<AlternatingItemStyle CssClass="GridAltItem" BackColor="WhiteSmoke"></AlternatingItemStyle>
											<ItemStyle CssClass="GridItem"></ItemStyle>
											<HeaderStyle CssClass="GridHeader"></HeaderStyle>
											<Columns>
												<asp:TemplateColumn HeaderText="#">
													<ItemStyle ForeColor="DimGray" Width="10px"></ItemStyle>
													<ItemTemplate>
														<asp:Label CssClass="menuTable" runat="server" Text='<%# (int)DataBinder.Eval(Container, "ItemIndex")+1 %>' ID="Label5" NAME="Label1">
														</asp:Label>
													</ItemTemplate>
												</asp:TemplateColumn>
												<asp:TemplateColumn SortExpression="InsuranceAgentName" HeaderText="Контакти">
													<ItemStyle Width="350px"></ItemStyle>
													<ItemTemplate>
														
														<asp:LinkButton id="Linkbutton1" runat="server" CssClass="menuTable" CommandName="Edit">
															<span Class="menuTable">
																<%# DataBinder.Eval(Container, "DataItem.InsuranceAgentName") %>
															</span>
														</asp:LinkButton>
													</ItemTemplate>
												</asp:TemplateColumn>
												
												<asp:BoundColumn DataField="InsurancePhones" SortExpression="InsurancePhones" HeaderText="Телефони"></asp:BoundColumn>
												<asp:TemplateColumn>
													<HeaderStyle Wrap="False" Width="20px"></HeaderStyle>
													<ItemStyle Wrap="False" HorizontalAlign="Center"></ItemStyle>
													<ItemTemplate>
														<asp:ImageButton id="ibEditItemI" runat="server" ImageUrl="images/edit1.gif" ToolTip="Редактирай"></asp:ImageButton>
													</ItemTemplate>
												</asp:TemplateColumn>
												<asp:TemplateColumn>
													<HeaderStyle Wrap="False" Width="20px"></HeaderStyle>
													<ItemStyle Wrap="False" HorizontalAlign="Center"></ItemStyle>
													<ItemTemplate>
														<asp:ImageButton id="ibDeleteItemI" runat="server" Width="12px" Height="12px" ImageUrl="images/delete.gif"
															ToolTip="Изтрий"></asp:ImageButton>
													</ItemTemplate>
												</asp:TemplateColumn>
											</Columns>
										</asp:datagrid>
										<BR>
										<asp:label style="Z-INDEX: 0" id="Label2" runat="server" EnableViewState="False" Font-Bold="True">Сервизи</asp:label>
										<BR>
										<BR>
										<asp:datagrid style="Z-INDEX: 0" id="grdAutoServices" runat="server" Width="100%" CssClass="ReportGrid"
											BackColor="Transparent" CellPadding="4" AutoGenerateColumns="False" AllowSorting="False">
											<AlternatingItemStyle CssClass="GridAltItem" BackColor="WhiteSmoke"></AlternatingItemStyle>
											<ItemStyle CssClass="GridItem"></ItemStyle>
											<HeaderStyle CssClass="GridHeader"></HeaderStyle>
											<Columns>
												<asp:TemplateColumn HeaderText="#">
													<ItemStyle ForeColor="DimGray" Width="10px"></ItemStyle>
													<ItemTemplate>
														<asp:Label CssClass="menuTable" runat="server" Text='<%# (int)DataBinder.Eval(Container, "ItemIndex")+1 %>' ID="Label5" NAME="Label1">
														</asp:Label>
													</ItemTemplate>
												</asp:TemplateColumn>
												<asp:TemplateColumn SortExpression="AutoServiceName" HeaderText="Име на сервиз">
													<ItemStyle Width="350px"></ItemStyle>
													<ItemTemplate>
														
														<asp:LinkButton id="Linkbutton2" runat="server" CssClass="menuTable"  CommandName="Edit">
															<span Class="menuTable">
																<%# DataBinder.Eval(Container, "DataItem.AutoServiceName") %>
															</span>
														</asp:LinkButton>
													</ItemTemplate>
												</asp:TemplateColumn>
												<asp:BoundColumn DataField="AutoServicePerson" SortExpression="AutoServicePerson" HeaderText="Име на контакт"></asp:BoundColumn>
												<asp:BoundColumn DataField="AutoServicePhones" SortExpression="AutoServicePhones" HeaderText="Телефони"></asp:BoundColumn>
												<asp:TemplateColumn>
													<HeaderStyle Wrap="False" Width="20px"></HeaderStyle>
													<ItemStyle Wrap="False" HorizontalAlign="Center"></ItemStyle>
													<ItemTemplate>
														<asp:ImageButton id="ibEditItemS" runat="server" ImageUrl="images/edit1.gif" ToolTip="Редактирай"></asp:ImageButton>
													</ItemTemplate>
												</asp:TemplateColumn>
												<asp:TemplateColumn>
													<HeaderStyle Wrap="False" Width="20px"></HeaderStyle>
													<ItemStyle Wrap="False" HorizontalAlign="Center"></ItemStyle>
													<ItemTemplate>
														<asp:ImageButton id="ibDeleteItemS" runat="server" Width="12px" Height="12px" ImageUrl="images/delete.gif"
															ToolTip="Изтрий"></asp:ImageButton>
													</ItemTemplate>
												</asp:TemplateColumn>
											</Columns>
										</asp:datagrid>
									</asp:panel>
									<TABLE id="Table4" 
										cellSpacing="0" cellPadding="4" width="100%" border="0">
										<TR>
											<td noWrap><asp:button id="btnNew" runat="server" CssClass="ActionButton" Width="200px" Text="Нов автомобил"></asp:button>&nbsp;
												<asp:button style="Z-INDEX: 0" id="Button1" runat="server" Width="200px" CssClass="ActionButton"
													Text="Нов сервиз"></asp:button>&nbsp;
												<asp:button style="Z-INDEX: 0" id="Button2" runat="server" Width="200px" CssClass="ActionButton"
													Text="Нов застрахователен агент"></asp:button>&nbsp;</TD>
										</TR>
									</TABLE>
								</td>
							</tr>
						</table>
					</td>
				</tr>
			</table>
		</form>
	</body>
</HTML>
